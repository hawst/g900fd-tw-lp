.class public final Lcom/google/android/videos/ui/EidrIdConverterHelper;
.super Ljava/lang/Object;
.source "EidrIdConverterHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/EidrIdConverterHelper$EidrIdConverterListener;
    }
.end annotation


# instance fields
.field private final account:Ljava/lang/String;

.field private assetsRequestCallback:Lcom/google/android/videos/async/CancelableCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/CancelableCallback",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private conversionCallback:Lcom/google/android/videos/async/CancelableCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/CancelableCallback",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final eidrId:Ljava/lang/String;

.field private final handler:Landroid/os/Handler;

.field private final listener:Lcom/google/android/videos/ui/EidrIdConverterHelper$EidrIdConverterListener;

.field private taskControl:Lcom/google/android/videos/async/TaskControl;

.field private final videosGlobals:Lcom/google/android/videos/VideosGlobals;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/ui/EidrIdConverterHelper$EidrIdConverterListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "eidrId"    # Ljava/lang/String;
    .param p4, "listener"    # Lcom/google/android/videos/ui/EidrIdConverterHelper$EidrIdConverterListener;

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->account:Ljava/lang/String;

    .line 58
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->eidrId:Ljava/lang/String;

    .line 59
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/EidrIdConverterHelper$EidrIdConverterListener;

    iput-object v0, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->listener:Lcom/google/android/videos/ui/EidrIdConverterHelper$EidrIdConverterListener;

    .line 61
    invoke-static {p1}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    .line 62
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->handler:Landroid/os/Handler;

    .line 63
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/ui/EidrIdConverterHelper;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/ui/EidrIdConverterHelper;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/EidrIdConverterHelper;->onEidrIdConverted(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/videos/ui/EidrIdConverterHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/ui/EidrIdConverterHelper;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/videos/ui/EidrIdConverterHelper;->syncPurchasesForEidrId()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/videos/ui/EidrIdConverterHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/ui/EidrIdConverterHelper;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/videos/ui/EidrIdConverterHelper;->convertThroughAssetApi()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/videos/ui/EidrIdConverterHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/ui/EidrIdConverterHelper;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/videos/ui/EidrIdConverterHelper;->onFailed()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/videos/ui/EidrIdConverterHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/EidrIdConverterHelper;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->eidrId:Ljava/lang/String;

    return-object v0
.end method

.method private convertThroughAssetApi()V
    .locals 5

    .prologue
    .line 150
    new-instance v1, Lcom/google/android/videos/api/AssetsRequest$Builder;

    invoke-direct {v1}, Lcom/google/android/videos/api/AssetsRequest$Builder;-><init>()V

    iget-object v2, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->account:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/videos/api/AssetsRequest$Builder;->account(Ljava/lang/String;)Lcom/google/android/videos/api/AssetsRequest$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->account:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/videos/store/ConfigurationStore;->getPlayCountry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/videos/api/AssetsRequest$Builder;->userCountry(Ljava/lang/String;)Lcom/google/android/videos/api/AssetsRequest$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/videos/api/AssetsRequest$Builder;->addFlags(I)Lcom/google/android/videos/api/AssetsRequest$Builder;

    move-result-object v0

    .line 155
    .local v0, "assetsRequestBuilder":Lcom/google/android/videos/api/AssetsRequest$Builder;
    iget-object v1, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->eidrId:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromEidrId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/api/AssetsRequest$Builder;->maybeAddId(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 157
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to convert "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->eidrId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 158
    invoke-direct {p0}, Lcom/google/android/videos/ui/EidrIdConverterHelper;->onFailed()V

    .line 188
    :goto_0
    return-void

    .line 162
    :cond_0
    new-instance v1, Lcom/google/android/videos/ui/EidrIdConverterHelper$3;

    invoke-direct {v1, p0}, Lcom/google/android/videos/ui/EidrIdConverterHelper$3;-><init>(Lcom/google/android/videos/ui/EidrIdConverterHelper;)V

    invoke-static {v1}, Lcom/google/android/videos/async/CancelableCallback;->create(Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/CancelableCallback;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->assetsRequestCallback:Lcom/google/android/videos/async/CancelableCallback;

    .line 185
    iget-object v1, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->listener:Lcom/google/android/videos/ui/EidrIdConverterHelper$EidrIdConverterListener;

    invoke-interface {v1}, Lcom/google/android/videos/ui/EidrIdConverterHelper$EidrIdConverterListener;->onNetworkRequest()V

    .line 186
    iget-object v1, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/videos/api/ApiRequesters;->getAssetsRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/videos/api/AssetsRequest$Builder;->build()Lcom/google/android/videos/api/AssetsRequest;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->handler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->assetsRequestCallback:Lcom/google/android/videos/async/CancelableCallback;

    invoke-static {v3, v4}, Lcom/google/android/videos/async/HandlerCallback;->create(Landroid/os/Handler;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/HandlerCallback;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    goto :goto_0
.end method

.method private onEidrIdConverted(Ljava/lang/String;)V
    .locals 1
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->listener:Lcom/google/android/videos/ui/EidrIdConverterHelper$EidrIdConverterListener;

    invoke-interface {v0, p1}, Lcom/google/android/videos/ui/EidrIdConverterHelper$EidrIdConverterListener;->onEidrIdConversionFinished(Ljava/lang/String;)V

    .line 192
    return-void
.end method

.method private onFailed()V
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->listener:Lcom/google/android/videos/ui/EidrIdConverterHelper$EidrIdConverterListener;

    invoke-interface {v0}, Lcom/google/android/videos/ui/EidrIdConverterHelper$EidrIdConverterListener;->onEidrIdConversionFailed()V

    .line 196
    return-void
.end method

.method private syncPurchasesForEidrId()V
    .locals 5

    .prologue
    .line 124
    new-instance v2, Lcom/google/android/videos/async/TaskControl;

    invoke-direct {v2}, Lcom/google/android/videos/async/TaskControl;-><init>()V

    iput-object v2, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->taskControl:Lcom/google/android/videos/async/TaskControl;

    .line 125
    iget-object v2, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->account:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->eidrId:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->createForEidrId(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    move-result-object v1

    .line 127
    .local v1, "purchaseSyncRequest":Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;
    new-instance v0, Lcom/google/android/videos/ui/EidrIdConverterHelper$2;

    invoke-direct {v0, p0}, Lcom/google/android/videos/ui/EidrIdConverterHelper$2;-><init>(Lcom/google/android/videos/ui/EidrIdConverterHelper;)V

    .line 143
    .local v0, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Ljava/lang/Void;>;"
    iget-object v2, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->listener:Lcom/google/android/videos/ui/EidrIdConverterHelper$EidrIdConverterListener;

    invoke-interface {v2}, Lcom/google/android/videos/ui/EidrIdConverterHelper$EidrIdConverterListener;->onNetworkRequest()V

    .line 144
    iget-object v2, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStoreSync()Lcom/google/android/videos/store/PurchaseStoreSync;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->taskControl:Lcom/google/android/videos/async/TaskControl;

    invoke-static {v1, v3}, Lcom/google/android/videos/async/ControllableRequest;->create(Ljava/lang/Object;Lcom/google/android/videos/async/TaskStatus;)Lcom/google/android/videos/async/ControllableRequest;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->handler:Landroid/os/Handler;

    invoke-static {v4, v0}, Lcom/google/android/videos/async/HandlerCallback;->create(Landroid/os/Handler;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/HandlerCallback;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/videos/store/PurchaseStoreSync;->syncPurchasesForVideo(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V

    .line 147
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 109
    iget-object v0, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->taskControl:Lcom/google/android/videos/async/TaskControl;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->taskControl:Lcom/google/android/videos/async/TaskControl;

    invoke-virtual {v0}, Lcom/google/android/videos/async/TaskControl;->cancel()V

    .line 111
    iput-object v1, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->taskControl:Lcom/google/android/videos/async/TaskControl;

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->conversionCallback:Lcom/google/android/videos/async/CancelableCallback;

    if-eqz v0, :cond_1

    .line 114
    iget-object v0, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->conversionCallback:Lcom/google/android/videos/async/CancelableCallback;

    invoke-virtual {v0}, Lcom/google/android/videos/async/CancelableCallback;->cancel()V

    .line 115
    iput-object v1, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->conversionCallback:Lcom/google/android/videos/async/CancelableCallback;

    .line 117
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->assetsRequestCallback:Lcom/google/android/videos/async/CancelableCallback;

    if-eqz v0, :cond_2

    .line 118
    iget-object v0, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->assetsRequestCallback:Lcom/google/android/videos/async/CancelableCallback;

    invoke-virtual {v0}, Lcom/google/android/videos/async/CancelableCallback;->cancel()V

    .line 119
    iput-object v1, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->assetsRequestCallback:Lcom/google/android/videos/async/CancelableCallback;

    .line 121
    :cond_2
    return-void
.end method

.method public convert(I)V
    .locals 7
    .param p1, "eidrIdSource"    # I

    .prologue
    .line 70
    new-instance v0, Lcom/google/android/videos/ui/EidrIdConverterHelper$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/videos/ui/EidrIdConverterHelper$1;-><init>(Lcom/google/android/videos/ui/EidrIdConverterHelper;I)V

    .line 102
    .local v0, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {v0}, Lcom/google/android/videos/async/CancelableCallback;->create(Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/CancelableCallback;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->conversionCallback:Lcom/google/android/videos/async/CancelableCallback;

    .line 103
    iget-object v1, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->account:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->eidrId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getLocalExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->handler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper;->conversionCallback:Lcom/google/android/videos/async/CancelableCallback;

    invoke-static {v5, v6}, Lcom/google/android/videos/async/HandlerCallback;->create(Landroid/os/Handler;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/HandlerCallback;

    move-result-object v5

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/videos/utils/EidrId;->convertEidrIdToVideoId(Lcom/google/android/videos/store/Database;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/ExecutorService;Lcom/google/android/videos/async/Callback;)V

    .line 106
    return-void
.end method
