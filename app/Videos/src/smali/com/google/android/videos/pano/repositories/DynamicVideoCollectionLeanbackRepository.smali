.class public final Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository;
.super Ljava/lang/Object;
.source "DynamicVideoCollectionLeanbackRepository.java"

# interfaces
.implements Lcom/google/android/repolib/observers/Updatable;
.implements Lcom/google/android/repolib/observers/UpdatablesChanged;
.implements Lcom/google/android/repolib/repositories/Repository;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository$1;,
        Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository$ResourcePredicate;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/observers/Updatable;",
        "Lcom/google/android/repolib/observers/UpdatablesChanged;",
        "Lcom/google/android/repolib/repositories/Repository",
        "<",
        "Landroid/support/v17/leanback/widget/ListRow;",
        ">;"
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final fromRepository:Lcom/google/android/repolib/repositories/Repository;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/repositories/Repository",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;",
            ">;"
        }
    .end annotation
.end field

.field private final storeStatusObservable:Lcom/google/android/videos/pano/repositories/StoreStatusObservable;

.field private final toRepository:Lcom/google/android/repolib/leanback/ListRowRepository;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/leanback/ListRowRepository",
            "<",
            "Lcom/google/android/videos/pano/model/VideoItem;",
            ">;"
        }
    .end annotation
.end field

.field private final updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/repolib/leanback/ObjectAdapterFactory;Lcom/google/android/videos/pano/repositories/StoreStatusObservable;Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "storeStatusObservable"    # Lcom/google/android/videos/pano/repositories/StoreStatusObservable;
    .param p4, "videoCollectionsRepository"    # Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/repolib/leanback/ObjectAdapterFactory",
            "<*>;",
            "Lcom/google/android/videos/pano/repositories/StoreStatusObservable;",
            "Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;",
            ")V"
        }
    .end annotation

    .prologue
    .line 43
    .local p2, "adapterFactory":Lcom/google/android/repolib/leanback/ObjectAdapterFactory;, "Lcom/google/android/repolib/leanback/ObjectAdapterFactory<*>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository;->context:Landroid/content/Context;

    .line 45
    iput-object p3, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository;->storeStatusObservable:Lcom/google/android/videos/pano/repositories/StoreStatusObservable;

    .line 46
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/repolib/leanback/RepositoryListRow;

    invoke-static {p2, v0}, Lcom/google/android/repolib/leanback/ListRowRepository;->listRowRepository(Lcom/google/android/repolib/leanback/ObjectAdapterFactory;[Lcom/google/android/repolib/leanback/RepositoryListRow;)Lcom/google/android/repolib/leanback/ListRowRepository;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository;->toRepository:Lcom/google/android/repolib/leanback/ListRowRepository;

    .line 47
    new-instance v0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository$ResourcePredicate;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository$ResourcePredicate;-><init>(Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository$1;)V

    invoke-static {p4, v0}, Lcom/google/android/repolib/repositories/FilteringRepository;->filteringRepository(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/common/Predicate;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository;->fromRepository:Lcom/google/android/repolib/repositories/Repository;

    .line 48
    invoke-static {p0}, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->asyncUpdateDispatcher(Lcom/google/android/repolib/observers/UpdatablesChanged;)Lcom/google/android/repolib/observers/UpdateDispatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    .line 49
    return-void
.end method


# virtual methods
.method public addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 77
    return-void
.end method

.method public firstUpdatableAdded()V
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository;->toRepository:Lcom/google/android/repolib/leanback/ListRowRepository;

    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/repolib/leanback/ListRowRepository;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 54
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository;->fromRepository:Lcom/google/android/repolib/repositories/Repository;

    invoke-interface {v0, p0}, Lcom/google/android/repolib/repositories/Repository;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 55
    return-void
.end method

.method public indexer()Lcom/google/android/repolib/common/Indexer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/repolib/common/Indexer",
            "<",
            "Landroid/support/v17/leanback/widget/ListRow;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository;->toRepository:Lcom/google/android/repolib/leanback/ListRowRepository;

    invoke-virtual {v0}, Lcom/google/android/repolib/leanback/ListRowRepository;->indexer()Lcom/google/android/repolib/common/Indexer;

    move-result-object v0

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Landroid/support/v17/leanback/widget/ListRow;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository;->toRepository:Lcom/google/android/repolib/leanback/ListRowRepository;

    invoke-virtual {v0}, Lcom/google/android/repolib/leanback/ListRowRepository;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public lastUpdatableRemoved()V
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository;->fromRepository:Lcom/google/android/repolib/repositories/Repository;

    invoke-interface {v0, p0}, Lcom/google/android/repolib/repositories/Repository;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 60
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository;->toRepository:Lcom/google/android/repolib/leanback/ListRowRepository;

    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/repolib/leanback/ListRowRepository;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 61
    return-void
.end method

.method public removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 82
    return-void
.end method

.method public update()V
    .locals 6

    .prologue
    .line 65
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 66
    .local v1, "repositoryListRows":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/repolib/leanback/RepositoryListRow<Lcom/google/android/videos/pano/model/VideoItem;>;>;"
    iget-object v3, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository;->fromRepository:Lcom/google/android/repolib/repositories/Repository;

    invoke-interface {v3}, Lcom/google/android/repolib/repositories/Repository;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    .line 67
    .local v2, "resource":Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;
    iget-object v3, v2, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->title:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository;->context:Landroid/content/Context;

    iget-object v5, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository;->storeStatusObservable:Lcom/google/android/videos/pano/repositories/StoreStatusObservable;

    invoke-virtual {v5}, Lcom/google/android/videos/pano/repositories/StoreStatusObservable;->getStoreStatusMonitor()Lcom/google/android/videos/store/StoreStatusMonitor;

    move-result-object v5

    invoke-static {v4, v2, v5}, Lcom/google/android/videos/pano/ui/VideoCollectionUtil;->getItemsFromVideoCollection(Landroid/content/Context;Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;Lcom/google/android/videos/store/StoreStatusMonitor;)[Lcom/google/android/videos/pano/model/VideoItem;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/repolib/repositories/ArrayRepository;->arrayRepository(Ljava/util/Collection;)Lcom/google/android/repolib/repositories/ArrayRepository;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/repolib/leanback/RepositoryListRow;->repositoryListRow(Ljava/lang/String;Lcom/google/android/repolib/repositories/Repository;)Lcom/google/android/repolib/leanback/RepositoryListRow;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 71
    .end local v2    # "resource":Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;
    :cond_0
    iget-object v3, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository;->toRepository:Lcom/google/android/repolib/leanback/ListRowRepository;

    invoke-virtual {v3, v1}, Lcom/google/android/repolib/leanback/ListRowRepository;->setItems(Ljava/util/List;)V

    .line 72
    return-void
.end method
