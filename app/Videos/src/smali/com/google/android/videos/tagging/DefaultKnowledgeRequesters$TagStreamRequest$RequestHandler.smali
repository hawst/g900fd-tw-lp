.class public final Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest$RequestHandler;
.super Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;
.source "DefaultKnowledgeRequesters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RequestHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler",
        "<",
        "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;",
        "Lcom/google/android/videos/tagging/TagStreamParser;",
        "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequest;",
        "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 649
    invoke-direct {p0}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;-><init>()V

    return-void
.end method

.method private decryptResponse([BLjava/lang/String;Ljava/lang/String;)[B
    .locals 10
    .param p1, "content"    # [B
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "contentLanguage"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    const/16 v8, 0x10

    .line 683
    const/16 v7, 0x8

    :try_start_0
    invoke-static {p2, v7}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 687
    .local v0, "base64DecodedVideoId":[B
    invoke-direct {p0, v0, v8}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest$RequestHandler;->trimOrPad([BI)[B

    move-result-object v5

    .line 688
    .local v5, "key":[B
    new-instance v6, Ljavax/crypto/spec/SecretKeySpec;

    const-string v7, "AES"

    invoke-direct {v6, v5, v7}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 692
    .local v6, "keySpec":Ljavax/crypto/spec/SecretKeySpec;
    if-nez p3, :cond_0

    new-array v3, v8, [B

    .line 694
    .local v3, "iv":[B
    :goto_0
    new-instance v4, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v4, v3}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 699
    .local v4, "ivSpec":Ljavax/crypto/spec/IvParameterSpec;
    :try_start_1
    const-string v7, "AES/CBC/PKCS5Padding"

    invoke-static {v7}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v1

    .line 706
    .local v1, "cipher":Ljavax/crypto/Cipher;
    const/4 v7, 0x2

    :try_start_2
    invoke-virtual {v1, v7, v6, v4}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V
    :try_end_2
    .catch Ljava/security/InvalidKeyException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_2 .. :try_end_2} :catch_4

    .line 715
    :try_start_3
    invoke-virtual {v1, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljavax/crypto/BadPaddingException; {:try_start_3 .. :try_end_3} :catch_7

    move-result-object v7

    return-object v7

    .line 684
    .end local v0    # "base64DecodedVideoId":[B
    .end local v1    # "cipher":Ljavax/crypto/Cipher;
    .end local v3    # "iv":[B
    .end local v4    # "ivSpec":Ljavax/crypto/spec/IvParameterSpec;
    .end local v5    # "key":[B
    .end local v6    # "keySpec":Ljavax/crypto/spec/SecretKeySpec;
    :catch_0
    move-exception v2

    .line 685
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    new-instance v7, Lcom/google/android/videos/converter/ConverterException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Cannot base64-decode video ID "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/google/android/videos/converter/ConverterException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 692
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v0    # "base64DecodedVideoId":[B
    .restart local v5    # "key":[B
    .restart local v6    # "keySpec":Ljavax/crypto/spec/SecretKeySpec;
    :cond_0
    invoke-virtual {p3}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    invoke-direct {p0, v7, v8}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest$RequestHandler;->trimOrPad([BI)[B

    move-result-object v3

    goto :goto_0

    .line 700
    .restart local v3    # "iv":[B
    .restart local v4    # "ivSpec":Ljavax/crypto/spec/IvParameterSpec;
    :catch_1
    move-exception v2

    .line 701
    .local v2, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v7, Lcom/google/android/videos/converter/ConverterException;

    invoke-direct {v7, v2}, Lcom/google/android/videos/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    .line 702
    .end local v2    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_2
    move-exception v2

    .line 703
    .local v2, "e":Ljavax/crypto/NoSuchPaddingException;
    new-instance v7, Lcom/google/android/videos/converter/ConverterException;

    invoke-direct {v7, v2}, Lcom/google/android/videos/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    .line 707
    .end local v2    # "e":Ljavax/crypto/NoSuchPaddingException;
    .restart local v1    # "cipher":Ljavax/crypto/Cipher;
    :catch_3
    move-exception v2

    .line 708
    .local v2, "e":Ljava/security/InvalidKeyException;
    new-instance v7, Lcom/google/android/videos/converter/ConverterException;

    invoke-direct {v7, v2}, Lcom/google/android/videos/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    .line 709
    .end local v2    # "e":Ljava/security/InvalidKeyException;
    :catch_4
    move-exception v2

    .line 710
    .local v2, "e":Ljava/security/InvalidAlgorithmParameterException;
    new-instance v7, Lcom/google/android/videos/converter/ConverterException;

    invoke-direct {v7, v2}, Lcom/google/android/videos/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    .line 716
    .end local v2    # "e":Ljava/security/InvalidAlgorithmParameterException;
    :catch_5
    move-exception v2

    .line 717
    .local v2, "e":Ljava/lang/RuntimeException;
    new-instance v7, Lcom/google/android/videos/converter/ConverterException;

    invoke-direct {v7, v2}, Lcom/google/android/videos/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    .line 718
    .end local v2    # "e":Ljava/lang/RuntimeException;
    :catch_6
    move-exception v2

    .line 719
    .local v2, "e":Ljavax/crypto/IllegalBlockSizeException;
    new-instance v7, Lcom/google/android/videos/converter/ConverterException;

    invoke-direct {v7, v2}, Lcom/google/android/videos/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    .line 720
    .end local v2    # "e":Ljavax/crypto/IllegalBlockSizeException;
    :catch_7
    move-exception v2

    .line 721
    .local v2, "e":Ljavax/crypto/BadPaddingException;
    new-instance v7, Lcom/google/android/videos/converter/ConverterException;

    invoke-direct {v7, v2}, Lcom/google/android/videos/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v7
.end method

.method private getParser(Lcom/google/android/videos/tagging/KnowledgeRequest;Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;)Lcom/google/android/videos/tagging/TagStreamParser;
    .locals 7
    .param p1, "knowledgeRequest"    # Lcom/google/android/videos/tagging/KnowledgeRequest;
    .param p2, "response"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 660
    iget-object v2, p2, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;->contentLanguage:Ljava/lang/String;

    .line 661
    .local v2, "contentLanguage":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 662
    iget-object v0, p1, Lcom/google/android/videos/tagging/KnowledgeRequest;->locale:Ljava/util/Locale;

    if-nez v0, :cond_0

    .line 663
    new-instance v0, Lcom/google/android/videos/converter/ConverterException;

    const-string v1, "No locale in request and Content-Language not set in response."

    invoke-direct {v0, v1}, Lcom/google/android/videos/converter/ConverterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 666
    :cond_0
    iget-object v0, p1, Lcom/google/android/videos/tagging/KnowledgeRequest;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    .line 667
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Server not returning Content-Language; assuming "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 669
    :cond_1
    iget-object v0, p2, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;->content:[B

    iget-object v1, p1, Lcom/google/android/videos/tagging/KnowledgeRequest;->videoId:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest$RequestHandler;->decryptResponse([BLjava/lang/String;Ljava/lang/String;)[B

    move-result-object v6

    .line 671
    .local v6, "decryptedContent":[B
    new-instance v0, Lcom/google/android/videos/tagging/TagStreamParser;

    iget-object v1, p1, Lcom/google/android/videos/tagging/KnowledgeRequest;->videoId:Ljava/lang/String;

    iget-object v3, p2, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;->lastModified:Ljava/lang/String;

    iget v4, p1, Lcom/google/android/videos/tagging/KnowledgeRequest;->videoItag:I

    iget-object v5, p1, Lcom/google/android/videos/tagging/KnowledgeRequest;->videoTimestamp:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/tagging/TagStreamParser;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;[B)V

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/TagStreamParser;->parse()Lcom/google/android/videos/tagging/TagStreamParser;

    move-result-object v0

    return-object v0
.end method

.method private trimOrPad([BI)[B
    .locals 3
    .param p1, "source"    # [B
    .param p2, "targetLength"    # I

    .prologue
    const/4 v2, 0x0

    .line 726
    array-length v1, p1

    if-ne v1, p2, :cond_0

    .line 731
    .end local p1    # "source":[B
    :goto_0
    return-object p1

    .line 729
    .restart local p1    # "source":[B
    :cond_0
    new-array v0, p2, [B

    .line 730
    .local v0, "result":[B
    array-length v1, p1

    invoke-static {v1, p2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object p1, v0

    .line 731
    goto :goto_0
.end method


# virtual methods
.method public createComponent(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;)Lcom/google/android/videos/tagging/TagStreamParser;
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;
    .param p2, "response"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 655
    iget-boolean v0, p2, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;->isNotFound:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;->knowledgeRequest:Lcom/google/android/videos/tagging/KnowledgeRequest;

    invoke-direct {p0, v0, p2}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest$RequestHandler;->getParser(Lcom/google/android/videos/tagging/KnowledgeRequest;Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;)Lcom/google/android/videos/tagging/TagStreamParser;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic createComponent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 649
    check-cast p1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest$RequestHandler;->createComponent(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;)Lcom/google/android/videos/tagging/TagStreamParser;

    move-result-object v0

    return-object v0
.end method

.method public createUnderlyingRequest(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;)Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequest;
    .locals 4
    .param p1, "request"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;
    .param p2, "storedResponse"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;

    .prologue
    .line 737
    new-instance v1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequest;

    iget-object v0, p1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;->knowledgeRequest:Lcom/google/android/videos/tagging/KnowledgeRequest;

    iget-object v2, v0, Lcom/google/android/videos/tagging/KnowledgeRequest;->account:Ljava/lang/String;

    # getter for: Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;->uri:Landroid/net/Uri;
    invoke-static {p1}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;->access$500(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    :cond_0
    iget-object v0, p2, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;->lastModified:Ljava/lang/String;

    goto :goto_0
.end method

.method public bridge synthetic createUnderlyingRequest(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 649
    check-cast p1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest$RequestHandler;->createUnderlyingRequest(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;)Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequest;

    move-result-object v0

    return-object v0
.end method

.method public resolveUnderlyingRequestError(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Lcom/google/android/videos/tagging/TagStreamParser;Ljava/lang/Exception;)Lcom/google/android/videos/tagging/TagStreamParser;
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;
    .param p2, "storedTagStreamParser"    # Lcom/google/android/videos/tagging/TagStreamParser;
    .param p3, "exception"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 744
    if-eqz p2, :cond_0

    const/16 v0, 0x130

    invoke-static {p3, v0}, Lcom/google/android/videos/utils/ErrorHelper;->isHttpException(Ljava/lang/Throwable;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 746
    return-object p2

    .line 748
    :cond_0
    throw p3
.end method

.method public bridge synthetic resolveUnderlyingRequestError(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Exception;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;
    .param p3, "x2"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 649
    check-cast p1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/videos/tagging/TagStreamParser;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest$RequestHandler;->resolveUnderlyingRequestError(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Lcom/google/android/videos/tagging/TagStreamParser;Ljava/lang/Exception;)Lcom/google/android/videos/tagging/TagStreamParser;

    move-result-object v0

    return-object v0
.end method
