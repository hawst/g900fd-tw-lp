.class public Lcom/google/android/videos/ui/GestureDetectorPlus;
.super Landroid/support/v4/view/GestureDetectorCompat;
.source "GestureDetectorPlus.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;
    }
.end annotation


# instance fields
.field private final listener:Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/GestureDetectorCompat;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    .line 45
    invoke-super {p0, p2}, Landroid/support/v4/view/GestureDetectorCompat;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 46
    iput-object p2, p0, Lcom/google/android/videos/ui/GestureDetectorPlus;->listener:Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;

    .line 47
    return-void
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 68
    invoke-super {p0, p1}, Landroid/support/v4/view/GestureDetectorCompat;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 69
    .local v0, "handled":Z
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 77
    :goto_0
    :pswitch_0
    return v0

    .line 71
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/videos/ui/GestureDetectorPlus;->listener:Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;

    invoke-interface {v1, p1}, Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;->onUp(Landroid/view/MotionEvent;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 72
    goto :goto_0

    .line 74
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/videos/ui/GestureDetectorPlus;->listener:Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;

    invoke-interface {v1, p1}, Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;->onCancel(Landroid/view/MotionEvent;)Z

    move-result v1

    or-int/2addr v0, v1

    goto :goto_0

    .line 69
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/view/GestureDetector$OnDoubleTapListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 62
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "The main listener is already a double-tap listener and has already been assigned during construction."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
