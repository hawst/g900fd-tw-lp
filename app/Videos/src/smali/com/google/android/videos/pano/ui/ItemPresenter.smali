.class public Lcom/google/android/videos/pano/ui/ItemPresenter;
.super Landroid/support/v17/leanback/widget/Presenter;
.source "ItemPresenter.java"


# instance fields
.field private final bitmapRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final handler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/async/Requester;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 20
    .local p1, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/async/ControllableRequest<Landroid/net/Uri;>;Landroid/graphics/Bitmap;>;"
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/Presenter;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/google/android/videos/pano/ui/ItemPresenter;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    .line 22
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/ItemPresenter;->handler:Landroid/os/Handler;

    .line 23
    return-void
.end method


# virtual methods
.method public onBindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;)V
    .locals 7
    .param p1, "viewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    .line 32
    move-object v6, p2

    check-cast v6, Lcom/google/android/videos/pano/model/Item;

    .line 33
    .local v6, "item":Lcom/google/android/videos/pano/model/Item;
    iget-object v0, p1, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    check-cast v0, Lcom/google/android/videos/pano/ui/VideoCardView;

    .line 34
    .local v0, "cardView":Lcom/google/android/videos/pano/ui/VideoCardView;
    invoke-virtual {v0}, Lcom/google/android/videos/pano/ui/VideoCardView;->reset()V

    .line 36
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/videos/pano/ui/VideoCardView;->setCardType(I)V

    .line 37
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/pano/ui/VideoCardView;->setInfoVisibility(I)V

    .line 39
    invoke-virtual {v6}, Lcom/google/android/videos/pano/model/Item;->getImageWidth()I

    move-result v1

    invoke-virtual {v6}, Lcom/google/android/videos/pano/model/Item;->getImageHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/pano/ui/VideoCardView;->setMainDimensions(II)V

    .line 40
    invoke-virtual {v6}, Lcom/google/android/videos/pano/model/Item;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/pano/ui/VideoCardView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 41
    invoke-virtual {v6}, Lcom/google/android/videos/pano/model/Item;->getDurationInSeconds()I

    move-result v3

    .line 42
    .local v3, "durationInSeconds":I
    invoke-virtual {v6}, Lcom/google/android/videos/pano/model/Item;->getSubtitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6}, Lcom/google/android/videos/pano/model/Item;->getBadgeResourceId()I

    move-result v2

    invoke-virtual {v6}, Lcom/google/android/videos/pano/model/Item;->getCheapestOffer()Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    move-result-object v4

    invoke-virtual {v6}, Lcom/google/android/videos/pano/model/Item;->getStarRating()F

    move-result v5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/videos/pano/ui/VideoCardView;->setExtra(Ljava/lang/String;IILcom/google/android/videos/utils/OfferUtil$CheapestOffer;F)V

    .line 45
    invoke-virtual {v6}, Lcom/google/android/videos/pano/model/Item;->getImageUri()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/pano/ui/ItemPresenter;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    iget-object v4, p0, Lcom/google/android/videos/pano/ui/ItemPresenter;->handler:Landroid/os/Handler;

    invoke-virtual {v6}, Lcom/google/android/videos/pano/model/Item;->getImageResourceId()I

    move-result v5

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/google/android/videos/pano/ui/VideoCardView;->setImage(Ljava/lang/String;Lcom/google/android/videos/async/Requester;Landroid/os/Handler;I)V

    .line 46
    invoke-virtual {v6}, Lcom/google/android/videos/pano/model/Item;->getProgress()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/pano/ui/VideoCardView;->setProgress(F)V

    .line 47
    return-void
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;)Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .locals 3
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 27
    new-instance v0, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    new-instance v1, Lcom/google/android/videos/pano/ui/VideoCardView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/videos/pano/ui/VideoCardView;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public onUnbindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V
    .locals 1
    .param p1, "viewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    .prologue
    .line 51
    iget-object v0, p1, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    check-cast v0, Lcom/google/android/videos/pano/ui/VideoCardView;

    .line 52
    .local v0, "cardView":Lcom/google/android/videos/pano/ui/VideoCardView;
    invoke-virtual {v0}, Lcom/google/android/videos/pano/ui/VideoCardView;->reset()V

    .line 53
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V
    .locals 1
    .param p1, "viewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    .prologue
    .line 57
    iget-object v0, p1, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    check-cast v0, Lcom/google/android/videos/pano/ui/VideoCardView;

    .line 58
    .local v0, "cardView":Lcom/google/android/videos/pano/ui/VideoCardView;
    invoke-virtual {v0}, Lcom/google/android/videos/pano/ui/VideoCardView;->cancelAnimation()V

    .line 59
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/Presenter;->onViewDetachedFromWindow(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V

    .line 60
    return-void
.end method
