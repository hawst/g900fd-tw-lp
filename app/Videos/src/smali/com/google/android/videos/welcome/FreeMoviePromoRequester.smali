.class public Lcom/google/android/videos/welcome/FreeMoviePromoRequester;
.super Ljava/lang/Object;
.source "FreeMoviePromoRequester.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/welcome/FreeMoviePromoRequester$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/api/PromotionsRequest;",
        "Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final configStore:Lcom/google/android/videos/store/ConfigurationStore;

.field private currentRequest:Lcom/google/android/videos/api/PromotionsRequest;

.field private final listener:Lcom/google/android/videos/welcome/FreeMoviePromoRequester$Listener;

.field private final promotionCallback:Lcom/google/android/videos/async/NewCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/NewCallback",
            "<",
            "Lcom/google/android/videos/api/PromotionsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final promotionsRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/PromotionsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final purchaseStoreMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/store/StoreStatusMonitor;Lcom/google/android/videos/welcome/FreeMoviePromoRequester$Listener;Landroid/os/Handler;)V
    .locals 1
    .param p2, "configStore"    # Lcom/google/android/videos/store/ConfigurationStore;
    .param p3, "purchaseStoreMonitor"    # Lcom/google/android/videos/store/StoreStatusMonitor;
    .param p4, "listener"    # Lcom/google/android/videos/welcome/FreeMoviePromoRequester$Listener;
    .param p5, "handler"    # Landroid/os/Handler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/PromotionsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;",
            ">;",
            "Lcom/google/android/videos/store/ConfigurationStore;",
            "Lcom/google/android/videos/store/StoreStatusMonitor;",
            "Lcom/google/android/videos/welcome/FreeMoviePromoRequester$Listener;",
            "Landroid/os/Handler;",
            ")V"
        }
    .end annotation

    .prologue
    .line 37
    .local p1, "promotionsRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/PromotionsRequest;Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p3, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRequester;->purchaseStoreMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    .line 39
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRequester;->promotionsRequester:Lcom/google/android/videos/async/Requester;

    .line 40
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/ConfigurationStore;

    iput-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRequester;->configStore:Lcom/google/android/videos/store/ConfigurationStore;

    .line 41
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/welcome/FreeMoviePromoRequester$Listener;

    iput-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRequester;->listener:Lcom/google/android/videos/welcome/FreeMoviePromoRequester$Listener;

    .line 42
    invoke-static {p5, p0}, Lcom/google/android/videos/async/HandlerCallback;->create(Landroid/os/Handler;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/HandlerCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRequester;->promotionCallback:Lcom/google/android/videos/async/NewCallback;

    .line 43
    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/api/PromotionsRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/api/PromotionsRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRequester;->currentRequest:Lcom/google/android/videos/api/PromotionsRequest;

    invoke-virtual {p1, v0}, Lcom/google/android/videos/api/PromotionsRequest;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 77
    :goto_0
    return-void

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRequester;->listener:Lcom/google/android/videos/welcome/FreeMoviePromoRequester$Listener;

    invoke-interface {v0, p2}, Lcom/google/android/videos/welcome/FreeMoviePromoRequester$Listener;->onPromoRequestError(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 19
    check-cast p1, Lcom/google/android/videos/api/PromotionsRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/welcome/FreeMoviePromoRequester;->onError(Lcom/google/android/videos/api/PromotionsRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/api/PromotionsRequest;Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;)V
    .locals 8
    .param p1, "request"    # Lcom/google/android/videos/api/PromotionsRequest;
    .param p2, "response"    # Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;

    .prologue
    const/4 v7, 0x0

    .line 53
    iget-object v4, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRequester;->currentRequest:Lcom/google/android/videos/api/PromotionsRequest;

    invoke-virtual {p1, v4}, Lcom/google/android/videos/api/PromotionsRequest;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 68
    :goto_0
    return-void

    .line 57
    :cond_0
    iget-object v0, p2, Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/PromotionResource;

    .local v0, "arr$":[Lcom/google/wireless/android/video/magma/proto/PromotionResource;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 58
    .local v3, "promotion":Lcom/google/wireless/android/video/magma/proto/PromotionResource;
    iget v4, v3, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->promotionType:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    iget-object v4, v3, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->asset:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    array-length v4, v4

    if-lez v4, :cond_1

    iget-object v4, v3, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->asset:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    aget-object v4, v4, v7

    iget-object v4, v4, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRequester;->purchaseStoreMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    iget-object v5, v3, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->asset:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    aget-object v5, v5, v7

    iget-object v5, v5, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v5, v5, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    iget-object v6, v3, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->asset:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    aget-object v6, v6, v7

    iget-object v6, v6, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget v6, v6, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    invoke-virtual {v4, v5, v6}, Lcom/google/android/videos/store/StoreStatusMonitor;->getStatus(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v4}, Lcom/google/android/videos/store/StoreStatusMonitor;->isPurchased(I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 63
    iget-object v4, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRequester;->listener:Lcom/google/android/videos/welcome/FreeMoviePromoRequester$Listener;

    invoke-interface {v4, v3}, Lcom/google/android/videos/welcome/FreeMoviePromoRequester$Listener;->onPromo(Lcom/google/wireless/android/video/magma/proto/PromotionResource;)V

    goto :goto_0

    .line 57
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 67
    .end local v3    # "promotion":Lcom/google/wireless/android/video/magma/proto/PromotionResource;
    :cond_2
    iget-object v4, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRequester;->listener:Lcom/google/android/videos/welcome/FreeMoviePromoRequester$Listener;

    invoke-interface {v4}, Lcom/google/android/videos/welcome/FreeMoviePromoRequester$Listener;->onNoPromo()V

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 19
    check-cast p1, Lcom/google/android/videos/api/PromotionsRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/welcome/FreeMoviePromoRequester;->onResponse(Lcom/google/android/videos/api/PromotionsRequest;Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;)V

    return-void
.end method

.method public requestPromotion(Ljava/lang/String;)V
    .locals 3
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 46
    new-instance v0, Lcom/google/android/videos/api/PromotionsRequest;

    iget-object v1, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRequester;->configStore:Lcom/google/android/videos/store/ConfigurationStore;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Lcom/google/android/videos/store/ConfigurationStore;->getPlayCountry(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/videos/api/PromotionsRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRequester;->currentRequest:Lcom/google/android/videos/api/PromotionsRequest;

    .line 48
    iget-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRequester;->promotionsRequester:Lcom/google/android/videos/async/Requester;

    iget-object v1, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRequester;->currentRequest:Lcom/google/android/videos/api/PromotionsRequest;

    iget-object v2, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRequester;->promotionCallback:Lcom/google/android/videos/async/NewCallback;

    invoke-interface {v0, v1, v2}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 49
    return-void
.end method
