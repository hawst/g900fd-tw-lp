.class public Lcom/google/android/videos/activity/ShowActivity$EpisodeSynopsisDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "ShowActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/activity/ShowActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EpisodeSynopsisDialogFragment"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 968
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static showInstance(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p0, "activity"    # Landroid/support/v4/app/FragmentActivity;
    .param p1, "episodeNumber"    # Ljava/lang/String;
    .param p2, "episodeName"    # Ljava/lang/String;
    .param p3, "synopsis"    # Ljava/lang/String;

    .prologue
    .line 974
    const v3, 0x7f0b00c6

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    aput-object p2, v4, v5

    invoke-virtual {p0, v3, v4}, Landroid/support/v4/app/FragmentActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 976
    .local v2, "title":Ljava/lang/String;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 977
    .local v0, "args":Landroid/os/Bundle;
    const-string v3, "episode_title"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 978
    const-string v3, "episode_synopsis"

    invoke-virtual {v0, v3, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 980
    new-instance v1, Lcom/google/android/videos/activity/ShowActivity$EpisodeSynopsisDialogFragment;

    invoke-direct {v1}, Lcom/google/android/videos/activity/ShowActivity$EpisodeSynopsisDialogFragment;-><init>()V

    .line 981
    .local v1, "instance":Lcom/google/android/videos/activity/ShowActivity$EpisodeSynopsisDialogFragment;
    invoke-virtual {v1, v0}, Lcom/google/android/videos/activity/ShowActivity$EpisodeSynopsisDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 982
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "EpisodeSynopsisDialogFragment"

    invoke-virtual {v1, v3, v4}, Lcom/google/android/videos/activity/ShowActivity$EpisodeSynopsisDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 983
    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 988
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ShowActivity$EpisodeSynopsisDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 989
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f040037

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 991
    .local v3, "view":Landroid/view/View;
    const v4, 0x7f0f00a8

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 992
    .local v2, "titleView":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ShowActivity$EpisodeSynopsisDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "episode_title"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 994
    const v4, 0x7f0f0102

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 995
    .local v0, "descriptionView":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ShowActivity$EpisodeSynopsisDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "episode_synopsis"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 996
    new-instance v4, Landroid/text/method/ScrollingMovementMethod;

    invoke-direct {v4}, Landroid/text/method/ScrollingMovementMethod;-><init>()V

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 998
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/videos/activity/ShowActivity$EpisodeSynopsisDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    return-object v4
.end method
