.class public Lcom/google/android/videos/tagging/TagStream$ChunkInfo;
.super Ljava/lang/Object;
.source "TagStream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/TagStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ChunkInfo"
.end annotation


# instance fields
.field public final byteCount:I

.field public final byteOffset:I

.field public final endMillis:I

.field public final startMillis:I


# direct methods
.method public constructor <init>(IIII)V
    .locals 0
    .param p1, "startMillis"    # I
    .param p2, "endMillis"    # I
    .param p3, "byteOffset"    # I
    .param p4, "byteCount"    # I

    .prologue
    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    iput p1, p0, Lcom/google/android/videos/tagging/TagStream$ChunkInfo;->startMillis:I

    .line 159
    iput p2, p0, Lcom/google/android/videos/tagging/TagStream$ChunkInfo;->endMillis:I

    .line 160
    iput p3, p0, Lcom/google/android/videos/tagging/TagStream$ChunkInfo;->byteOffset:I

    .line 161
    iput p4, p0, Lcom/google/android/videos/tagging/TagStream$ChunkInfo;->byteCount:I

    .line 162
    return-void
.end method
