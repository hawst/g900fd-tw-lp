.class public final Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;
.super Ljava/lang/Object;
.source "FrameworkMediaPlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnBufferingUpdateListener;
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnInfoListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Landroid/media/MediaPlayer$OnSeekCompleteListener;
.implements Landroid/media/MediaPlayer$OnVideoSizeChangedListener;
.implements Lcom/google/android/videos/player/legacy/MediaPlayerInterface;


# instance fields
.field private listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

.field private final mediaPlayer:Landroid/media/MediaPlayer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    .line 42
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 43
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    .line 44
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    .line 45
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    .line 46
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 47
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 48
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 49
    return-void
.end method


# virtual methods
.method public getAudioSessionId()I
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getAudioSessionId()I

    move-result v0

    return v0
.end method

.method public getCurrentPosition()I
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    return v0
.end method

.method public getDuration()I
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    return v0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    return v0
.end method

.method public onBufferingUpdate(Landroid/media/MediaPlayer;I)V
    .locals 1
    .param p1, "player"    # Landroid/media/MediaPlayer;
    .param p2, "percent"    # I

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    invoke-interface {v0, p0, p2}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;->onBufferingUpdate(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;I)V

    .line 85
    :cond_0
    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 1
    .param p1, "player"    # Landroid/media/MediaPlayer;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    invoke-interface {v0, p0}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;->onCompletion(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;)V

    .line 71
    :cond_0
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 1
    .param p1, "player"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    invoke-interface {v0, p0, p2, p3}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;->onError(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;II)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onInfo(Landroid/media/MediaPlayer;II)Z
    .locals 1
    .param p1, "player"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    invoke-interface {v0, p0, p2, p3}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;->onInfo(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;II)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 1
    .param p1, "player"    # Landroid/media/MediaPlayer;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    invoke-interface {v0, p0}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;->onPrepared(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;)V

    .line 99
    :cond_0
    return-void
.end method

.method public onSeekComplete(Landroid/media/MediaPlayer;)V
    .locals 1
    .param p1, "player"    # Landroid/media/MediaPlayer;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    invoke-interface {v0, p0}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;->onSeekComplete(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;)V

    .line 78
    :cond_0
    return-void
.end method

.method public onVideoSizeChanged(Landroid/media/MediaPlayer;II)V
    .locals 1
    .param p1, "player"    # Landroid/media/MediaPlayer;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    invoke-interface {v0, p0, p2, p3}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;->onVideoSizeChanged(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;II)V

    .line 92
    :cond_0
    return-void
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 127
    return-void
.end method

.method public prepareAsync()V
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V

    .line 117
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 142
    return-void
.end method

.method public seekTo(I)V
    .locals 1
    .param p1, "millis"    # I

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 132
    return-void
.end method

.method public setAudioStreamType(I)V
    .locals 1
    .param p1, "streamMusic"    # I

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 168
    return-void
.end method

.method public setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 106
    .local p3, "headers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1, p2, p3}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    .line 107
    return-void
.end method

.method public setDisplay(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 173
    return-void
.end method

.method public setListener(Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    .line 54
    return-void
.end method

.method public setScreenOnWhilePlaying(Z)V
    .locals 1
    .param p1, "on"    # Z

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V

    .line 157
    return-void
.end method

.method public setVideoScalingModeV16(I)V
    .locals 1
    .param p1, "mode"    # I

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setVideoScalingMode(I)V

    .line 163
    return-void
.end method

.method public start()V
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 122
    return-void
.end method
