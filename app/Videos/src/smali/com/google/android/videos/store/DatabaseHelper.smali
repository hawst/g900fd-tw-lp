.class final Lcom/google/android/videos/store/DatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "DatabaseHelper.java"


# instance fields
.field private final eventLogger:Lcom/google/android/videos/logging/EventLogger;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/videos/logging/EventLogger;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "database"    # Ljava/lang/String;
    .param p3, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;

    .prologue
    .line 84
    const/4 v0, 0x0

    const/16 v1, 0x2e

    invoke-direct {p0, p1, p2, v0, v1}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 85
    iput-object p3, p0, Lcom/google/android/videos/store/DatabaseHelper;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 86
    return-void
.end method

.method private addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "table"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "type"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 1371
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1372
    return-void
.end method

.method private addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "table"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "type"    # Ljava/lang/String;
    .param p5, "initialValue"    # Ljava/lang/String;
    .param p6, "updateInitialWhere"    # Ljava/lang/String;

    .prologue
    .line 1376
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ALTER TABLE "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ADD COLUMN "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1377
    if-eqz p5, :cond_0

    .line 1378
    if-nez p6, :cond_1

    const-string v0, ""

    .line 1379
    .local v0, "where":Ljava/lang/String;
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "UPDATE "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " SET "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1381
    .end local v0    # "where":Ljava/lang/String;
    :cond_0
    return-void

    .line 1378
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " WHERE "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private addImageColumns(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "imageTable"    # Ljava/lang/String;

    .prologue
    .line 1014
    const-string v0, "image_uri"

    const-string v1, "TEXT"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1015
    const-string v0, "image_etag"

    const-string v1, "TEXT"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1016
    const-string v0, "image_last_modified"

    const-string v1, "TEXT"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1017
    return-void
.end method

.method private createAssetsIndexV37(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 440
    const-string v0, "DROP INDEX IF EXISTS assets_seqnos_index"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 441
    const-string v0, "CREATE INDEX assets_seqnos_index ON assets(root_id,season_seqno,episode_seqno)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 446
    return-void
.end method

.method private createAssetsTableV40(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 422
    const-string v0, "CREATE TABLE assets (assets_type INT NOT NULL,assets_id TEXT NOT NULL,root_id TEXT NOT NULL,title_eidr_id TEXT,season_seqno INT NOT NULL DEFAULT -1,episode_seqno INT NOT NULL DEFAULT -1,next_episode_id TEXT,next_episode_in_same_season INT NOT NULL DEFAULT 0,end_credit_start_seconds INT NOT NULL DEFAULT 0,is_bonus_content INT NOT NULL DEFAULT 0,PRIMARY KEY (assets_type,assets_id))"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 437
    return-void
.end method

.method private createDatabase(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 176
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->createUserConfigurationTableV43(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 177
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->createPurchasedAssetsTableV46(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 178
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->createAssetsTableV40(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 179
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->createAssetsIndexV37(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 180
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->createSeasonsTableV33(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 181
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->createSeasonsShowIndexV15(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 182
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->createVideosTableV29(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 183
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->createVideosSeasonIndexV15(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 184
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->createUserAssetsTableV39(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 185
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->createUserDataTableV35(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 186
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->createPostersTableV20(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 187
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->createScreenshotsTableV20(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 188
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->createShowPostersTableV20(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 189
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->createShowBannersTableV20(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 190
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->createWishlistTableV34(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 191
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->createWishlistItemOrderIndexV34(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 192
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->createVideoFormatTableV18(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 193
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->createIcingSupportTablesV22(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 194
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->createShowsTableV45(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 195
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->createIcingSupportTriggersV33(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 196
    return-void
.end method

.method private createIcingSupportTablesV22(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 213
    const-string v0, "DROP TABLE IF EXISTS icing_movies"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 214
    const-string v0, "CREATE TABLE icing_movies (icing_movie_seq_no INTEGER PRIMARY KEY AUTOINCREMENT,icing_movie_id TEXT)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 217
    const-string v0, "DROP TABLE IF EXISTS icing_shows"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 218
    const-string v0, "CREATE TABLE icing_shows (icing_show_seq_no INTEGER PRIMARY KEY AUTOINCREMENT,icing_show_id TEXT)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 221
    return-void
.end method

.method private createIcingSupportTriggersV33(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 225
    const-string v0, "DROP TRIGGER IF EXISTS purchased_assets_update_trigger_icing_movies"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 226
    const-string v0, "CREATE TRIGGER purchased_assets_update_trigger_icing_movies AFTER UPDATE OF hidden,purchase_status ON purchased_assets WHEN OLD.asset_type = 6 BEGIN  INSERT INTO icing_movies (icing_movie_id) VALUES (OLD.asset_id); END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 236
    const-string v0, "DROP TRIGGER IF EXISTS purchased_assets_delete_trigger_icing_movies"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 237
    const-string v0, "CREATE TRIGGER purchased_assets_delete_trigger_icing_movies AFTER DELETE ON purchased_assets WHEN OLD.asset_type = 6 AND NOT (OLD.hidden IN (1, 3)) AND OLD.purchase_status = 2 BEGIN  INSERT INTO icing_movies (icing_movie_id) VALUES (OLD.asset_id); END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 247
    const-string v0, "DROP TRIGGER IF EXISTS videos_insert_trigger_icing_movies"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 248
    const-string v0, "CREATE TRIGGER videos_insert_trigger_icing_movies AFTER INSERT ON videos WHEN NEW.episode_season_id IS NULL BEGIN INSERT INTO icing_movies (icing_movie_id)  VALUES (NEW.video_id); END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 256
    const-string v0, "DROP TRIGGER IF EXISTS videos_update_trigger_icing_movies"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 257
    const-string v0, "CREATE TRIGGER videos_update_trigger_icing_movies AFTER UPDATE OF title ON videos WHEN NEW.episode_season_id IS NULL AND OLD.title != NEW.title BEGIN INSERT INTO icing_movies (icing_movie_id)  VALUES (NEW.video_id); END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 268
    const-string v0, "DROP TRIGGER IF EXISTS purchased_assets_trigger_icing_shows"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 269
    const-string v0, "CREATE TRIGGER purchased_assets_trigger_icing_shows AFTER UPDATE OF hidden,purchase_status ON purchased_assets WHEN OLD.asset_type = 20 BEGIN  INSERT INTO icing_shows (icing_show_id) SELECT show_id FROM videos,seasons ON episode_season_id IS season_id WHERE OLD.asset_id = video_id; END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 282
    const-string v0, "DROP TRIGGER IF EXISTS purchased_assets_delete_trigger_icing_shows"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 283
    const-string v0, "CREATE TRIGGER purchased_assets_delete_trigger_icing_shows AFTER DELETE ON purchased_assets WHEN OLD.asset_type = 20 AND NOT (OLD.hidden IN (1, 3)) AND OLD.purchase_status = 2 BEGIN  INSERT INTO icing_shows (icing_show_id) SELECT show_id FROM videos,seasons ON episode_season_id IS season_id WHERE OLD.asset_id = video_id; END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 296
    const-string v0, "DROP TRIGGER IF EXISTS videos_insert_trigger_icing_shows"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 297
    const-string v0, "CREATE TRIGGER videos_insert_trigger_icing_shows AFTER INSERT ON videos WHEN NEW.episode_season_id IS NOT NULL BEGIN INSERT INTO icing_shows (icing_show_id)  SELECT show_id FROM seasons WHERE NEW.episode_season_id = season_id; END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 307
    const-string v0, "DROP TRIGGER IF EXISTS seasons_insert_trigger_icing_shows"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 308
    const-string v0, "CREATE TRIGGER seasons_insert_trigger_icing_shows AFTER INSERT ON seasons BEGIN INSERT INTO icing_shows (icing_show_id) VALUES (NEW.show_id); END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 315
    const-string v0, "DROP TRIGGER IF EXISTS shows_insert_trigger_icing_shows"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 316
    const-string v0, "CREATE TRIGGER shows_insert_trigger_icing_shows AFTER INSERT ON shows BEGIN INSERT INTO icing_shows (icing_show_id) VALUES (NEW.shows_id); END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 323
    const-string v0, "DROP TRIGGER IF EXISTS shows_update_trigger_icing_shows"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 324
    const-string v0, "CREATE TRIGGER shows_update_trigger_icing_shows AFTER UPDATE OF shows_title ON shows WHEN OLD.shows_title != NEW.shows_title BEGIN INSERT INTO icing_shows (icing_show_id) VALUES (NEW.shows_id); END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 332
    return-void
.end method

.method private createPostersTableV20(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 541
    const-string v0, "CREATE TABLE posters (poster_video_id TEXT PRIMARY KEY,image_uri TEXT,image_etag TEXT,image_last_modified TEXT)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 547
    return-void
.end method

.method private createPurchasedAssetsTableV46(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 356
    const-string v0, "DROP TABLE IF EXISTS purchased_assets"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 357
    const-string v0, "CREATE TABLE purchased_assets (account TEXT NOT NULL,asset_type INT NOT NULL,asset_id TEXT NOT NULL,root_asset_id TEXT,purchase_type INT NOT NULL,format_type INT NOT NULL,purchase_status INT NOT NULL,purchase_timestamp_seconds INT NOT NULL,expiration_timestamp_seconds INT,rental_short_timer_seconds INT,hidden INT,last_playback_is_dirty INT,last_playback_start_timestamp INT,last_watched_timestamp INT,resume_timestamp INT,pinned INT,pinning_notification_active INT,pinning_status INT,pinning_status_reason INT,pinning_drm_error_code INT,is_new_notification_dismissed INT,license_type INT,license_expiration_timestamp INT,license_activation BLOB,license_force_sync INT,license_last_synced_timestamp INT,license_release_pending INT NOT NULL DEFAULT 0,license_video_format INT,license_file_path_key TEXT,license_key_id INT,license_asset_id INT,license_system_id INT,license_cenc_key_set_id BLOB,license_cenc_pssh_data BLOB,license_cenc_security_level INT,license_cenc_mimetype TEXT,streaming_pssh_data BLOB,streaming_mimetype TEXT,have_subtitles INT,download_relative_filepath TEXT,pinning_download_size INT,download_bytes_downloaded INT,download_last_modified TEXT,download_quality INT,download_extra_proto BLOB,external_storage_index INT,PRIMARY KEY (account,asset_type,asset_id) CONSTRAINT videos_non_null_columns CHECK (asset_type NOT IN (6, 20) OR (last_playback_is_dirty IS NOT NULL AND last_playback_start_timestamp IS NOT NULL AND last_watched_timestamp IS NOT NULL AND resume_timestamp IS NOT NULL AND pinned IS NOT NULL AND pinning_notification_active IS NOT NULL AND is_new_notification_dismissed IS NOT NULL AND have_subtitles IS NOT NULL)))"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 419
    return-void
.end method

.method private createScreenshotsTableV20(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 550
    const-string v0, "CREATE TABLE screenshots (screenshot_video_id TEXT PRIMARY KEY,image_uri TEXT,image_etag TEXT,image_last_modified TEXT)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 556
    return-void
.end method

.method private createSeasonsShowIndexV15(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 459
    const-string v0, "DROP INDEX IF EXISTS seasons_show_index"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 460
    const-string v0, "CREATE INDEX seasons_show_index ON seasons (show_id)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 463
    return-void
.end method

.method private createSeasonsTableV33(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 449
    const-string v0, "CREATE TABLE seasons (season_id TEXT NOT NULL PRIMARY KEY,season_title TEXT NOT NULL,season_long_title TEXT,show_id TEXT NOT NULL,episodes_synced INT NOT NULL DEFAULT 0,season_synced_timestamp INT NOT NULL DEFAULT 0)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 456
    return-void
.end method

.method private createShowBannersTableV20(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 568
    const-string v0, "CREATE TABLE show_banners (banner_show_id TEXT PRIMARY KEY,image_uri TEXT,image_etag TEXT,image_last_modified TEXT)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 574
    return-void
.end method

.method private createShowPostersTableV20(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 559
    const-string v0, "CREATE TABLE show_posters (poster_show_id TEXT PRIMARY KEY,image_uri TEXT,image_etag TEXT,image_last_modified TEXT)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 565
    return-void
.end method

.method private createShowsTableV45(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 199
    const-string v0, "CREATE TABLE shows (shows_id TEXT NOT NULL PRIMARY KEY,shows_title TEXT NOT NULL,shows_description TEXT,shows_poster_uri TEXT,shows_poster_synced INT NOT NULL DEFAULT 0,shows_banner_uri TEXT,shows_banner_synced INT NOT NULL DEFAULT 0,shows_synced_timestamp INT NOT NULL DEFAULT 0,shows_full_sync_timestamp INT NOT NULL DEFAULT 0,broadcasters TEXT)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 210
    return-void
.end method

.method private createUserAssetsTableV39(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 507
    const-string v0, "CREATE TABLE user_assets (user_assets_account TEXT NOT NULL,user_assets_type INT NOT NULL,user_assets_id TEXT NOT NULL,metadata_timestamp_at_last_sync INT NOT NULL DEFAULT 0,last_activity_timestamp INT,ppm_remaining INT,watched_to_end_credit INT,merged_expiration_timestamp INT,season_id_of_last_activity TEXT,season_seqno_of_last_activity INT,last_watched_episode_id TEXT,last_watched_episode_seqno INT,last_watched_season_seqno INT,next_show_episode_id TEXT,next_show_episode_in_same_season INT,owned_complete_show INT,owned_episode_count INT,PRIMARY KEY (user_assets_account,user_assets_type,user_assets_id))"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 530
    return-void
.end method

.method private createUserConfigurationTableV43(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 347
    const-string v0, "DROP TABLE IF EXISTS user_configuration"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 348
    const-string v0, "CREATE TABLE user_configuration (config_account TEXT NOT NULL PRIMARY KEY,config_play_country TEXT,config_proto BLOB,account_links INT NOT NULL DEFAULT 0)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 353
    return-void
.end method

.method private createUserDataTableV35(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 533
    const-string v0, "CREATE TABLE user_data (user_account TEXT NOT NULL PRIMARY KEY,sync_snapshot_token TEXT,wishlist_snapshot_token TEXT)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 538
    return-void
.end method

.method private createVideoFormatTableV18(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 335
    const-string v0, "DROP TABLE IF EXISTS video_formats"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 336
    const-string v0, "CREATE TABLE video_formats (itag INT NOT NULL PRIMARY KEY,width INT NOT NULL,height INT NOT NULL,audio_channels INT NOT NULL,drm_type INT NOT NULL,is_dash INT NOT NULL,is_multi INT NOT NULL)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 344
    return-void
.end method

.method private createVideosSeasonIndexV15(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 500
    const-string v0, "DROP INDEX IF EXISTS videos_season_index"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 501
    const-string v0, "CREATE INDEX videos_season_index ON videos (episode_season_id)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 504
    return-void
.end method

.method private createVideosTableV29(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 466
    const-string v0, "CREATE TABLE videos (video_id TEXT PRIMARY KEY,title TEXT NOT NULL,description TEXT,actors TEXT,directors TEXT,writers TEXT,producers TEXT,rating_id TEXT,rating_name TEXT,ratings TEXT,release_year INT,publish_timestamp INT,duration_seconds INT NOT NULL,claimed INT NOT NULL,has_subtitles INT NOT NULL DEFAULT 0,subtitle_mode INT NOT NULL,default_subtitle_language TEXT,audio_track_languages TEXT,captions_track_uri TEXT,related_uri TEXT,poster_uri TEXT,poster_synced INT NOT NULL DEFAULT 0,screenshot_uri TEXT,screenshot_synced INT NOT NULL DEFAULT 0,episode_season_id TEXT,episode_number_text TEXT,video_synced_timestamp INT NOT NULL DEFAULT 0,stream_formats TEXT,badge_surround_sound INT NOT NULL DEFAULT 0,badge_knowledge INT NOT NULL DEFAULT 0)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 497
    return-void
.end method

.method private createWishlistItemOrderIndexV34(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 592
    const-string v0, "DROP INDEX IF EXISTS wishlist_item_order_index"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 593
    const-string v0, "CREATE INDEX wishlist_item_order_index ON wishlist (wishlist_account, wishlist_item_order)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 596
    return-void
.end method

.method private createWishlistTableV34(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 577
    const-string v0, "CREATE TABLE wishlist (wishlist_account TEXT NOT NULL,wishlist_item_id TEXT NOT NULL,wishlist_item_type TEXT NOT NULL,wishlist_item_state INT NOT NULL,wishlist_item_order INT NOT NULL DEFAULT -1,wishlist_in_cloud INT NOT NULL DEFAULT 0,PRIMARY KEY (wishlist_account,wishlist_item_id,wishlist_item_type))"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 589
    return-void
.end method

.method private populateAssetsTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 26
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 705
    const-string v23, "INSERT INTO assets (assets_type,assets_id,root_id) SELECT 6,video_id,video_id FROM videos"

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 716
    new-instance v21, Landroid/content/ContentValues;

    invoke-direct/range {v21 .. v21}, Landroid/content/ContentValues;-><init>()V

    .line 717
    .local v21, "values":Landroid/content/ContentValues;
    const-string v23, "SELECT shows_id FROM shows"

    const/16 v24, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v20

    .line 719
    .local v20, "showsCursor":Landroid/database/Cursor;
    :goto_0
    :try_start_0
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToNext()Z

    move-result v23

    if-eqz v23, :cond_9

    .line 720
    const/16 v23, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 721
    .local v19, "showId":Ljava/lang/String;
    const-string v23, "root_id"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 722
    const-string v23, "assets_type"

    const/16 v24, 0x12

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 723
    const-string v23, "assets_id"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    const-string v23, "assets"

    const/16 v24, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    move-object/from16 v3, v21

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 727
    const/4 v12, 0x0

    .line 728
    .local v12, "nextEpisodeId":Ljava/lang/String;
    const-string v23, "SELECT season_id FROM seasons WHERE show_id = ? ORDER BY CAST(season_title AS INTEGER),season_title"

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v19, v24, v25

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v18

    .line 735
    .local v18, "seasonsCursor":Landroid/database/Cursor;
    :try_start_1
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->getCount()I

    move-result v23

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 736
    :goto_1
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v23

    if-eqz v23, :cond_8

    .line 737
    const/16 v23, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 738
    .local v16, "seasonId":Ljava/lang/String;
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->getPosition()I

    move-result v17

    .line 739
    .local v17, "seasonSeqno":I
    const-string v23, "assets_type"

    const/16 v24, 0x13

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 740
    const-string v23, "assets_id"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 741
    const-string v23, "season_seqno"

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 742
    const-string v23, "assets"

    const/16 v24, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    move-object/from16 v3, v21

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 752
    const-string v10, "SELECT video_id,CAST(episode_number_text AS INTEGER) AS episode_number_int FROM videos WHERE episode_season_id IS ? ORDER BY episode_number_int,episode_number_text"

    .line 758
    .local v10, "episodesSql":Ljava/lang/String;
    const/16 v22, 0x0

    .line 759
    .local v22, "videoIdColumn":I
    const/4 v7, 0x1

    .line 761
    .local v7, "episodeNumberColumn":I
    const-string v23, "SELECT video_id,CAST(episode_number_text AS INTEGER) AS episode_number_int FROM videos WHERE episode_season_id IS ? ORDER BY episode_number_int,episode_number_text"

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v16, v24, v25

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v9

    .line 764
    .local v9, "episodesCursor":Landroid/database/Cursor;
    const v4, 0x7fffffff

    .line 765
    .local v4, "bonusContentStartIndex":I
    const/4 v15, 0x0

    .line 766
    .local v15, "previousEpisodeNumber":I
    :goto_2
    :try_start_2
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v23

    if-eqz v23, :cond_2

    .line 767
    invoke-interface {v9}, Landroid/database/Cursor;->getPosition()I

    move-result v14

    .line 768
    .local v14, "position":I
    const/16 v23, 0x1

    move/from16 v0, v23

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 769
    .local v6, "episodeNumber":I
    if-nez v14, :cond_0

    .line 771
    const/16 v23, 0x1

    move/from16 v0, v23

    if-ne v6, v0, :cond_2

    .line 772
    const/4 v15, 0x1

    goto :goto_2

    .line 777
    :cond_0
    sub-int v23, v6, v15

    const/16 v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-gt v0, v1, :cond_1

    .line 779
    const/16 v23, 0x32

    move/from16 v0, v23

    if-ge v6, v0, :cond_2

    .line 780
    move v15, v6

    goto :goto_2

    .line 787
    :cond_1
    const/16 v23, 0x65

    move/from16 v0, v23

    if-ne v6, v0, :cond_2

    .line 788
    move v4, v14

    .line 795
    .end local v6    # "episodeNumber":I
    .end local v14    # "position":I
    :cond_2
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v23

    move/from16 v0, v23

    invoke-interface {v9, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 796
    const/4 v13, 0x0

    .line 797
    .local v13, "nextEpisodeInSameSeason":Z
    :cond_3
    :goto_3
    invoke-interface {v9}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v23

    if-eqz v23, :cond_7

    .line 798
    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 799
    .local v5, "episodeId":Ljava/lang/String;
    invoke-interface {v9}, Landroid/database/Cursor;->getPosition()I

    move-result v8

    .line 800
    .local v8, "episodeSeqno":I
    if-lt v8, v4, :cond_4

    const/4 v11, 0x1

    .line 801
    .local v11, "isBonusContent":Z
    :goto_4
    const-string v23, "assets_type"

    const/16 v24, 0x14

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 802
    const-string v23, "assets_id"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 803
    const-string v23, "episode_seqno"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 805
    const-string v24, "next_episode_id"

    if-eqz v11, :cond_5

    const/16 v23, 0x0

    :goto_5
    move-object/from16 v0, v21

    move-object/from16 v1, v24

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 807
    const-string v24, "next_episode_in_same_season"

    if-nez v11, :cond_6

    if-eqz v13, :cond_6

    const/16 v23, 0x1

    :goto_6
    invoke-static/range {v23 .. v23}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v23

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 809
    const-string v23, "is_bonus_content"

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v24

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 810
    const-string v23, "assets"

    const/16 v24, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    move-object/from16 v3, v21

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 812
    if-nez v11, :cond_3

    .line 813
    move-object v12, v5

    .line 814
    const/4 v13, 0x1

    goto/16 :goto_3

    .line 800
    .end local v11    # "isBonusContent":Z
    :cond_4
    const/4 v11, 0x0

    goto :goto_4

    .restart local v11    # "isBonusContent":Z
    :cond_5
    move-object/from16 v23, v12

    .line 805
    goto :goto_5

    .line 807
    :cond_6
    const/16 v23, 0x0

    goto :goto_6

    .line 818
    .end local v5    # "episodeId":Ljava/lang/String;
    .end local v8    # "episodeSeqno":I
    .end local v11    # "isBonusContent":Z
    :cond_7
    :try_start_3
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 821
    const-string v23, "episode_seqno"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 822
    const-string v23, "next_episode_id"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 823
    const-string v23, "next_episode_in_same_season"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 827
    .end local v4    # "bonusContentStartIndex":I
    .end local v7    # "episodeNumberColumn":I
    .end local v9    # "episodesCursor":Landroid/database/Cursor;
    .end local v10    # "episodesSql":Ljava/lang/String;
    .end local v13    # "nextEpisodeInSameSeason":Z
    .end local v15    # "previousEpisodeNumber":I
    .end local v16    # "seasonId":Ljava/lang/String;
    .end local v17    # "seasonSeqno":I
    .end local v22    # "videoIdColumn":I
    :catchall_0
    move-exception v23

    :try_start_4
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    throw v23
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 832
    .end local v12    # "nextEpisodeId":Ljava/lang/String;
    .end local v18    # "seasonsCursor":Landroid/database/Cursor;
    .end local v19    # "showId":Ljava/lang/String;
    :catchall_1
    move-exception v23

    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    throw v23

    .line 818
    .restart local v4    # "bonusContentStartIndex":I
    .restart local v7    # "episodeNumberColumn":I
    .restart local v9    # "episodesCursor":Landroid/database/Cursor;
    .restart local v10    # "episodesSql":Ljava/lang/String;
    .restart local v12    # "nextEpisodeId":Ljava/lang/String;
    .restart local v15    # "previousEpisodeNumber":I
    .restart local v16    # "seasonId":Ljava/lang/String;
    .restart local v17    # "seasonSeqno":I
    .restart local v18    # "seasonsCursor":Landroid/database/Cursor;
    .restart local v19    # "showId":Ljava/lang/String;
    .restart local v22    # "videoIdColumn":I
    :catchall_2
    move-exception v23

    :try_start_5
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v23
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 827
    .end local v4    # "bonusContentStartIndex":I
    .end local v7    # "episodeNumberColumn":I
    .end local v9    # "episodesCursor":Landroid/database/Cursor;
    .end local v10    # "episodesSql":Ljava/lang/String;
    .end local v15    # "previousEpisodeNumber":I
    .end local v16    # "seasonId":Ljava/lang/String;
    .end local v17    # "seasonSeqno":I
    .end local v22    # "videoIdColumn":I
    :cond_8
    :try_start_6
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 829
    const-string v23, "season_seqno"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_0

    .line 832
    .end local v12    # "nextEpisodeId":Ljava/lang/String;
    .end local v18    # "seasonsCursor":Landroid/database/Cursor;
    .end local v19    # "showId":Ljava/lang/String;
    :cond_9
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    .line 834
    return-void
.end method

.method private populateUserAssetsTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 842
    invoke-static {p1}, Lcom/google/android/videos/store/UserAssetsUtil;->createAllVideoRowsForUpgrade(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 843
    invoke-static {p1}, Lcom/google/android/videos/store/UserAssetsUtil;->createAllSeasonRowsForUpgrade(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 844
    invoke-static {p1}, Lcom/google/android/videos/store/UserAssetsUtil;->createAllShowRowsForUpgrade(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 845
    return-void
.end method

.method private recreateDatabase(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 167
    invoke-static {p1}, Lcom/google/android/videos/utils/Util;->wipeDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 168
    invoke-virtual {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 169
    return-void
.end method

.method private upgradeFromV10ToV11(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1269
    const-string v0, "videos"

    const-string v1, "has_subtitles"

    const-string v2, "INT NOT NULL DEFAULT 0"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1270
    const-string v0, "videos"

    const-string v1, "badge_surround_sound"

    const-string v2, "INT NOT NULL DEFAULT 0"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1271
    const-string v0, "videos"

    const-string v1, "badge_knowledge"

    const-string v2, "INT NOT NULL DEFAULT 0"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1278
    const-string v0, "UPDATE OR IGNORE videos SET has_subtitles = (captions_track_uri IS NOT NULL), badge_surround_sound = (stream_formats LIKE \'%35%\' OR stream_formats LIKE \'%36%\' OR stream_formats LIKE \'%34%\' OR stream_formats LIKE \'%40%\'), captions_track_uri = NULL, stream_formats = NULL, ratings = NULL, video_synced_timestamp = 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1288
    return-void
.end method

.method private upgradeFromV11ToV12(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1246
    const-string v0, "videos"

    const-string v1, "screenshot_uri"

    const-string v2, "TEXT"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1247
    const-string v0, "videos"

    const-string v1, "screenshot_synced"

    const-string v2, "INT NOT NULL DEFAULT 0"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1248
    const-string v0, "UPDATE OR IGNORE videos SET screenshot_uri = poster_uri, screenshot_synced = poster_uri IS NULL, poster_uri = NULL, poster_synced = 1 WHERE episode_season_id IS NOT NULL"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1254
    const-string v0, "UPDATE OR IGNORE videos SET video_synced_timestamp = 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1256
    const-string v0, "seasons"

    const-string v1, "show_banner_uri"

    const-string v2, "TEXT"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1257
    const-string v0, "seasons"

    const-string v1, "show_banner_synced"

    const-string v2, "INT NOT NULL DEFAULT 0"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1258
    const-string v0, "UPDATE OR IGNORE seasons SET season_synced_timestamp = 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1260
    const-string v0, "CREATE TABLE screenshots (screenshot_video_id TEXT PRIMARY KEY)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1263
    const-string v0, "CREATE TABLE show_banners (banner_show_id TEXT PRIMARY KEY)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1266
    return-void
.end method

.method private upgradeFromV12ToV13(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1240
    const-string v0, "videos"

    const-string v1, "rating_id"

    const-string v2, "TEXT"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1241
    const-string v0, "videos"

    const-string v1, "rating_name"

    const-string v2, "TEXT"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1242
    const-string v0, "UPDATE OR IGNORE videos SET video_synced_timestamp = 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1243
    return-void
.end method

.method private upgradeFromV13ToV14(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1232
    const-string v0, "DROP TABLE IF EXISTS user_configuration"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1233
    const-string v0, "CREATE TABLE user_configuration (config_account TEXT NOT NULL PRIMARY KEY,config_play_country TEXT,config_proto BLOB)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1237
    return-void
.end method

.method private upgradeFromV14ToV15(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1227
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->createSeasonsShowIndexV15(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1228
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->createVideosSeasonIndexV15(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1229
    return-void
.end method

.method private upgradeFromV15ToV16(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 14
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1179
    const-string v10, "SELECT item_id,formats FROM purchases"

    const/4 v11, 0x0

    invoke-virtual {p1, v10, v11}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1182
    .local v6, "purchaseCursor":Landroid/database/Cursor;
    :goto_0
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1183
    const/4 v10, 0x0

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1184
    .local v5, "itemId":Ljava/lang/String;
    const/4 v10, 0x1

    invoke-static {v6, v10}, Lcom/google/android/videos/utils/DbUtils;->getIntegerList(Landroid/database/Cursor;I)Ljava/util/List;

    move-result-object v1

    .line 1185
    .local v1, "gdataFormats":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1186
    .local v4, "itags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v10

    if-ge v2, v10, :cond_1

    .line 1187
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-static {v10}, Lcom/google/android/videos/streams/MediaStream;->gdataFormatToItag(I)I

    move-result v3

    .line 1188
    .local v3, "itag":I
    const/4 v10, -0x1

    if-eq v3, v10, :cond_0

    .line 1189
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v4, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1186
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1193
    .end local v3    # "itag":I
    :cond_1
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 1194
    .local v8, "values":Landroid/content/ContentValues;
    const-string v10, "formats"

    invoke-static {v8, v10, v4}, Lcom/google/android/videos/utils/DbUtils;->putIntegerList(Landroid/content/ContentValues;Ljava/lang/String;Ljava/util/List;)V

    .line 1195
    const-string v10, "purchases"

    const-string v11, "item_id = ?"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    aput-object v5, v12, v13

    invoke-virtual {p1, v10, v8, v11, v12}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1198
    .end local v1    # "gdataFormats":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v2    # "i":I
    .end local v4    # "itags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v5    # "itemId":Ljava/lang/String;
    .end local v8    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v10

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v10

    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1201
    const-string v10, "SELECT pinning_video_id,license_video_format FROM video_userdata"

    const/4 v11, 0x0

    invoke-virtual {p1, v10, v11}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1205
    .local v7, "userDataCursor":Landroid/database/Cursor;
    :cond_3
    :goto_2
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1206
    const/4 v10, 0x0

    invoke-interface {v7, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 1207
    .local v9, "videoId":Ljava/lang/String;
    const/4 v10, 0x1

    invoke-static {v7, v10}, Lcom/google/android/videos/utils/DbUtils;->getIntOrNull(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v0

    .line 1208
    .local v0, "gdataFormat":Ljava/lang/Integer;
    if-eqz v0, :cond_3

    .line 1209
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-static {v10}, Lcom/google/android/videos/streams/MediaStream;->gdataFormatToItag(I)I

    move-result v3

    .line 1210
    .restart local v3    # "itag":I
    const/4 v10, -0x1

    if-ne v3, v10, :cond_4

    .line 1211
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Cannot map gdata "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " to an itag while updating VideoUserData#LicenseVideoFormat. Defaulting to 0."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 1213
    const/4 v3, 0x0

    .line 1215
    :cond_4
    const-string v10, "UPDATE video_userdata SET license_video_format = ? WHERE pinning_video_id = ?"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/String;

    const/4 v12, 0x0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    aput-object v9, v11, v12

    invoke-virtual {p1, v10, v11}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_2

    .line 1222
    .end local v0    # "gdataFormat":Ljava/lang/Integer;
    .end local v3    # "itag":I
    .end local v9    # "videoId":Ljava/lang/String;
    :catchall_1
    move-exception v10

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v10

    :cond_5
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1224
    return-void
.end method

.method private upgradeFromV16ToV19(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1021
    const-string v1, "DROP TABLE IF EXISTS purchased_assets"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1022
    const-string v1, "CREATE TABLE purchased_assets (account TEXT NOT NULL,asset_type INT NOT NULL,asset_id TEXT NOT NULL,purchase_type INT NOT NULL,format_type INT NOT NULL,purchase_status INT NOT NULL,purchase_timestamp_seconds INT NOT NULL,expiration_timestamp_seconds INT,rental_short_timer_seconds INT,hidden INT,last_playback_is_dirty INT,last_playback_start_timestamp INT,last_watched_timestamp INT,resume_timestamp INT,pinned INT,pinning_notification_active INT,pinning_status INT,pinning_status_reason INT,pinning_drm_error_code INT,is_new_notification_dismissed INT,license_video_format INT,license_expiration_timestamp INT,license_last_synced_timestamp INT,license_file_path_key TEXT,license_key_id INT,license_asset_id INT,license_system_id INT,license_force_sync INT,have_subtitles INT,download_relative_filepath TEXT,pinning_download_size INT,download_bytes_downloaded INT,download_last_modified TEXT,PRIMARY KEY (account,asset_type,asset_id))"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1062
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->createVideoFormatTableV18(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1066
    const-string v1, "INSERT INTO purchased_assets SELECT account, CASE WHEN item_type = 2 THEN 19 WHEN episode_season_id IS NOT NULL THEN 20 ELSE 6 END,item_id, CASE purchase_type WHEN 1 THEN 1 WHEN 2 THEN 2 END, CASE WHEN formats LIKE \'%113%\' THEN 2 ELSE 1 END, CASE status WHEN 2 THEN 2 ELSE 0 END,purchase_timestamp / 1000,expiration_timestamp / 1000,purchase_duration_seconds,hidden,last_playback_is_dirty,last_playback_start_timestamp,last_watched_timestamp,resume_timestamp,pinned,pinning_notification_active,pinning_status,pinning_status_reason,pinning_drm_error_code,is_new_notification_dismissed,license_video_format,license_expiration_timestamp,license_last_synced_timestamp,license_file_path_key,license_key_id,license_asset_id,license_system_id,license_force_sync,have_subtitles,download_relative_filepath,pinning_download_size,download_bytes_downloaded,download_last_modified FROM purchases LEFT JOIN (video_userdata,videos ON pinning_video_id = video_id) ON account = pinning_account AND item_type = 1 AND item_id = video_id"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1126
    const-string v0, " INTO purchased_assets SELECT account,20,video_id,purchase_type,format_type,purchase_status,purchase_timestamp_seconds,expiration_timestamp_seconds,rental_short_timer_seconds,hidden,video_userdata.last_playback_is_dirty,video_userdata.last_playback_start_timestamp,video_userdata.last_watched_timestamp,video_userdata.resume_timestamp,video_userdata.pinned,video_userdata.pinning_notification_active,video_userdata.pinning_status,video_userdata.pinning_status_reason,video_userdata.pinning_drm_error_code,video_userdata.is_new_notification_dismissed,video_userdata.license_video_format,video_userdata.license_expiration_timestamp,video_userdata.license_last_synced_timestamp,video_userdata.license_file_path_key,video_userdata.license_key_id,video_userdata.license_asset_id,video_userdata.license_system_id,video_userdata.license_force_sync,video_userdata.have_subtitles,video_userdata.download_relative_filepath,video_userdata.pinning_download_size,video_userdata.download_bytes_downloaded,video_userdata.download_last_modified FROM purchased_assets,videos,video_userdata ON asset_type = 19 AND purchase_status = 2 AND asset_id IS episode_season_id AND video_id = pinning_video_id AND account = pinning_account AND format_type = "

    .line 1167
    .local v0, "insertEpisodesSqlCore":Ljava/lang/String;
    const-string v1, "REPLACE INTO purchased_assets SELECT account,20,video_id,purchase_type,format_type,purchase_status,purchase_timestamp_seconds,expiration_timestamp_seconds,rental_short_timer_seconds,hidden,video_userdata.last_playback_is_dirty,video_userdata.last_playback_start_timestamp,video_userdata.last_watched_timestamp,video_userdata.resume_timestamp,video_userdata.pinned,video_userdata.pinning_notification_active,video_userdata.pinning_status,video_userdata.pinning_status_reason,video_userdata.pinning_drm_error_code,video_userdata.is_new_notification_dismissed,video_userdata.license_video_format,video_userdata.license_expiration_timestamp,video_userdata.license_last_synced_timestamp,video_userdata.license_file_path_key,video_userdata.license_key_id,video_userdata.license_asset_id,video_userdata.license_system_id,video_userdata.license_force_sync,video_userdata.have_subtitles,video_userdata.download_relative_filepath,video_userdata.pinning_download_size,video_userdata.download_bytes_downloaded,video_userdata.download_last_modified FROM purchased_assets,videos,video_userdata ON asset_type = 19 AND purchase_status = 2 AND asset_id IS episode_season_id AND video_id = pinning_video_id AND account = pinning_account AND format_type = 2"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1171
    const-string v1, "INSERT OR IGNORE INTO purchased_assets SELECT account,20,video_id,purchase_type,format_type,purchase_status,purchase_timestamp_seconds,expiration_timestamp_seconds,rental_short_timer_seconds,hidden,video_userdata.last_playback_is_dirty,video_userdata.last_playback_start_timestamp,video_userdata.last_watched_timestamp,video_userdata.resume_timestamp,video_userdata.pinned,video_userdata.pinning_notification_active,video_userdata.pinning_status,video_userdata.pinning_status_reason,video_userdata.pinning_drm_error_code,video_userdata.is_new_notification_dismissed,video_userdata.license_video_format,video_userdata.license_expiration_timestamp,video_userdata.license_last_synced_timestamp,video_userdata.license_file_path_key,video_userdata.license_key_id,video_userdata.license_asset_id,video_userdata.license_system_id,video_userdata.license_force_sync,video_userdata.have_subtitles,video_userdata.download_relative_filepath,video_userdata.pinning_download_size,video_userdata.download_bytes_downloaded,video_userdata.download_last_modified FROM purchased_assets,videos,video_userdata ON asset_type = 19 AND purchase_status = 2 AND asset_id IS episode_season_id AND video_id = pinning_video_id AND account = pinning_account AND format_type = 1"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1174
    const-string v1, "DROP TABLE IF EXISTS purchases"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1175
    const-string v1, "DROP TABLE IF EXISTS video_userdata"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1176
    return-void
.end method

.method private upgradeFromV19ToV20(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1002
    const-string v0, "CREATE TABLE user_data (user_account TEXT NOT NULL PRIMARY KEY,sync_snapshot_token TEXT)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1007
    const-string v0, "posters"

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/store/DatabaseHelper;->addImageColumns(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 1008
    const-string v0, "show_posters"

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/store/DatabaseHelper;->addImageColumns(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 1009
    const-string v0, "show_banners"

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/store/DatabaseHelper;->addImageColumns(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 1010
    const-string v0, "screenshots"

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/store/DatabaseHelper;->addImageColumns(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 1011
    return-void
.end method

.method private upgradeFromV20ToV21(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 997
    const-string v0, "seasons"

    const-string v1, "season_long_title"

    const-string v2, "TEXT"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 998
    return-void
.end method

.method private upgradeFromV21ToV22(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 991
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->createIcingSupportTablesV22(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 994
    return-void
.end method

.method private upgradeFromV22ToV23(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 987
    const-string v0, "DELETE FROM video_formats"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 988
    return-void
.end method

.method private upgradeFromV23ToV24(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 975
    const-string v0, "UPDATE wishlist SET wishlist_item_type = CASE wishlist_item_type WHEN 1 THEN 6 WHEN 2 THEN 19 WHEN 3 THEN 18 WHEN 4 THEN 2 WHEN 5 THEN 4 ELSE -1 END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 983
    return-void
.end method

.method private upgradeFromV24ToV25(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 967
    const-string v0, "purchased_assets"

    const-string v1, "download_quality"

    const-string v2, "INT"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 968
    const-string v0, "purchased_assets"

    const-string v1, "license_release_pending"

    const-string v2, "INT NOT NULL DEFAULT 0"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 970
    return-void
.end method

.method private upgradeFromV25ToV26(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 7
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 956
    const-string v2, "purchased_assets"

    const-string v3, "external_storage_index"

    const-string v4, "INT"

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "(pinned IS NOT NULL AND pinned > 0)"

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 960
    const-string v0, "UPDATE purchased_assets SET download_quality = 1 WHERE download_quality IS NULL AND (pinned IS NOT NULL AND pinned > 0)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 964
    return-void
.end method

.method private upgradeFromV26ToV27(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 952
    const-string v0, "purchased_assets"

    const-string v1, "download_extra_proto"

    const-string v2, "BLOB"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 953
    return-void
.end method

.method private upgradeFromV27ToV28(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 948
    const-string v0, "purchased_assets"

    const-string v1, "license_cenc_key_set_id"

    const-string v2, "BLOB"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 949
    return-void
.end method

.method private upgradeFromV28ToV29(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 943
    const-string v0, "videos"

    const-string v1, "audio_track_languages"

    const-string v2, "TEXT"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 944
    const-string v0, "UPDATE OR IGNORE videos SET video_synced_timestamp = 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 945
    return-void
.end method

.method private upgradeFromV29ToV30(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 939
    const-string v0, "UPDATE OR IGNORE seasons SET season_synced_timestamp = 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 940
    return-void
.end method

.method private upgradeFromV30ToV31(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 931
    const-string v0, "UPDATE OR IGNORE seasons SET show_banner_synced = 1 WHERE show_banner_uri IS NULL"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 934
    return-void
.end method

.method private upgradeFromV31ToV33(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 896
    const-string v0, "seasons_temp"

    .line 897
    .local v0, "temporaryTableName":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ALTER TABLE seasons RENAME TO "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 898
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->createSeasonsTableV33(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 899
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->createSeasonsShowIndexV15(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 900
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSERT INTO seasons SELECT season_id, season_title, season_long_title, show_id, episodes_synced, season_synced_timestamp FROM "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 905
    const-string v1, "CREATE TABLE shows (shows_id TEXT NOT NULL PRIMARY KEY,shows_title TEXT NOT NULL,shows_description TEXT,shows_poster_uri TEXT,shows_poster_synced INT NOT NULL DEFAULT 0,shows_banner_uri TEXT,shows_banner_synced INT NOT NULL DEFAULT 0,shows_synced_timestamp INT NOT NULL DEFAULT 0)"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 914
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSERT INTO shows (shows_id, shows_title, shows_description, shows_poster_uri, shows_poster_synced, shows_banner_uri, shows_banner_synced, shows_synced_timestamp) SELECT DISTINCT show_id, show_title, show_description, show_poster_uri, show_poster_synced, show_banner_uri, show_banner_synced, season_synced_timestamp FROM "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " GROUP BY "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "show_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 925
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DROP TABLE IF EXISTS "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 926
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->createIcingSupportTriggersV33(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 927
    return-void
.end method

.method private upgradeFromV33ToV35(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 885
    const-string v0, "wishlist"

    const-string v1, "wishlist_item_order"

    const-string v2, "INT NOT NULL DEFAULT -1"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 886
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->createWishlistItemOrderIndexV34(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 888
    const-string v0, "user_data"

    const-string v1, "wishlist_snapshot_token"

    const-string v2, "TEXT"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 889
    return-void
.end method

.method private upgradeFromV35ToV36(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 848
    const-string v0, "shows"

    const-string v1, "shows_full_sync_timestamp"

    const-string v2, "INT NOT NULL DEFAULT 0"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 849
    const-string v0, "purchased_assets"

    const-string v1, "root_asset_id"

    const-string v2, "TEXT"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 852
    const-string v0, "UPDATE purchased_assets SET root_asset_id = asset_id WHERE asset_type = 6"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 858
    const-string v0, "UPDATE purchased_assets SET root_asset_id = (SELECT show_id FROM seasons WHERE asset_id = season_id) WHERE asset_type = 19"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 866
    const-string v0, "UPDATE purchased_assets SET root_asset_id = (SELECT show_id FROM seasons, videos WHERE asset_id = video_id AND episode_season_id IS season_id) WHERE asset_type = 20"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 876
    const-string v0, "UPDATE user_data SET sync_snapshot_token = NULL WHERE EXISTS (SELECT NULL FROM purchased_assets WHERE account = user_account AND root_asset_id IS NULL)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 882
    return-void
.end method

.method private upgradeFromV36ToV37(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 660
    const-string v0, "CREATE TABLE assets (assets_type INT NOT NULL,assets_id TEXT NOT NULL,root_id TEXT NOT NULL,season_seqno INT NOT NULL DEFAULT -1,episode_seqno INT NOT NULL DEFAULT -1,next_episode_id TEXT,next_episode_in_same_season INT NOT NULL DEFAULT 0,end_credit_start_seconds INT NOT NULL DEFAULT 0,is_bonus_content INT NOT NULL DEFAULT 0,PRIMARY KEY (assets_type,assets_id))"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 674
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->createAssetsIndexV37(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 675
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->populateAssetsTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 677
    const-string v0, "CREATE TABLE user_assets (user_assets_account TEXT NOT NULL,user_assets_type INT NOT NULL,user_assets_id TEXT NOT NULL,last_activity_timestamp INT,ppm_remaining INT,watched_to_end_credit INT,merged_expiration_timestamp INT,season_id_of_last_activity TEXT,last_watched_episode_id TEXT,next_show_episode_id TEXT,next_show_episode_in_same_season INT,owned_complete_show INT,PRIMARY KEY (user_assets_account,user_assets_type,user_assets_id))"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 695
    return-void
.end method

.method private upgradeFromV37ToV38(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 653
    const-string v0, "user_assets"

    const-string v1, "season_seqno_of_last_activity"

    const-string v2, "INT"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 654
    const-string v0, "user_assets"

    const-string v1, "last_watched_episode_seqno"

    const-string v2, "INT"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 655
    const-string v0, "user_assets"

    const-string v1, "last_watched_season_seqno"

    const-string v2, "INT"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 656
    const-string v0, "user_assets"

    const-string v1, "owned_episode_count"

    const-string v2, "INT"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 657
    return-void
.end method

.method private upgradeFromV38ToV39(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 643
    const-string v0, "user_assets"

    const-string v1, "metadata_timestamp_at_last_sync"

    const-string v2, "INT NOT NULL DEFAULT 0"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 648
    const-string v0, "DELETE FROM user_assets"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 649
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->populateUserAssetsTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 650
    return-void
.end method

.method private upgradeFromV39ToV40(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 635
    const-string v0, "assets"

    const-string v1, "title_eidr_id"

    const-string v2, "TEXT"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 638
    const-string v0, "UPDATE OR IGNORE videos SET video_synced_timestamp = 0 WHERE episode_season_id IS NULL"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 640
    return-void
.end method

.method private upgradeFromV3ToV4(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1362
    const-string v0, "ALTER TABLE video_userdata ADD COLUMN license_force_sync INT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1364
    const-string v0, "ALTER TABLE purchases ADD COLUMN purchase_duration_seconds INT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1366
    const-string v0, "UPDATE purchases SET purchase_duration_seconds = (duration_hours * 60 * 60)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1368
    return-void
.end method

.method private upgradeFromV40ToV41(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 631
    const-string v0, "purchased_assets"

    const-string v1, "license_activation"

    const-string v2, "BLOB"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    return-void
.end method

.method private upgradeFromV41ToV42(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 618
    const-string v0, "purchased_assets"

    const-string v1, "license_type"

    const-string v2, "INT"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 619
    const-string v0, "purchased_assets"

    const-string v1, "license_cenc_pssh_data"

    const-string v2, "BLOB"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 621
    const-string v0, "UPDATE OR IGNORE purchased_assets SET license_type = 2 WHERE download_extra_proto IS NOT NULL"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 624
    const-string v0, "UPDATE OR IGNORE purchased_assets SET license_type = 1 WHERE download_extra_proto IS NULL AND download_relative_filepath IS NOT NULL"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 628
    return-void
.end method

.method private upgradeFromV42ToV43(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 613
    const-string v0, "user_configuration"

    const-string v1, "account_links"

    const-string v2, "INT NOT NULL DEFAULT 0"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    return-void
.end method

.method private upgradeFromV43ToV44(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 609
    const-string v0, "purchased_assets"

    const-string v1, "license_cenc_security_level"

    const-string v2, "INT"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    return-void
.end method

.method private upgradeFromV44ToV45(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 605
    const-string v0, "shows"

    const-string v1, "broadcasters"

    const-string v2, "TEXT"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 606
    return-void
.end method

.method private upgradeFromV45ToV46(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 599
    const-string v0, "purchased_assets"

    const-string v1, "license_cenc_mimetype"

    const-string v2, "TEXT"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 600
    const-string v0, "purchased_assets"

    const-string v1, "streaming_pssh_data"

    const-string v2, "BLOB"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    const-string v0, "purchased_assets"

    const-string v1, "streaming_mimetype"

    const-string v2, "TEXT"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 602
    return-void
.end method

.method private upgradeFromV4ToV5(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1334
    const-string v0, "CREATE TABLE seasons (season_id TEXT NOT NULL PRIMARY KEY,season_title TEXT NOT NULL,show_id TEXT NOT NULL,show_title TEXT NOT NULL,show_description TEXT,show_poster_uri TEXT,show_poster_synced INT NOT NULL DEFAULT 0,episodes_synced INT NOT NULL DEFAULT 0,season_synced_timestamp INT NOT NULL DEFAULT 0)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1344
    const-string v0, "CREATE TABLE show_posters (poster_show_id TEXT PRIMARY KEY)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1347
    const-string v0, "ALTER TABLE videos ADD COLUMN episode_season_id TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1349
    const-string v0, "ALTER TABLE videos ADD COLUMN episode_number_text TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1351
    const-string v0, "ALTER TABLE videos ADD COLUMN ratings TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1353
    const-string v0, "ALTER TABLE purchases ADD COLUMN hidden INT NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1355
    const-string v0, "ALTER TABLE videos ADD COLUMN video_synced_timestamp INT NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1357
    const-string v0, "ALTER TABLE seasons ADD COLUMN season_synced_timestamp INT NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1359
    return-void
.end method

.method private upgradeFromV5ToV6(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1329
    const-string v0, "ALTER TABLE video_userdata ADD COLUMN download_last_modified TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1331
    return-void
.end method

.method private upgradeFromV6ToV7(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1320
    const-string v0, "ALTER TABLE videos ADD COLUMN stream_formats TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1324
    const-string v0, "UPDATE OR IGNORE videos SET video_synced_timestamp = 0 WHERE episode_season_id IS NULL"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1326
    return-void
.end method

.method private upgradeFromV7ToV8(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 7
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1312
    const-string v0, "video_userdata"

    const-string v1, "last_playback_is_dirty"

    const-string v2, "INT NOT NULL DEFAULT 0"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1314
    const-string v2, "video_userdata"

    const-string v3, "last_playback_start_timestamp"

    const-string v4, "INT NOT NULL DEFAULT 0"

    const-string v5, "last_watched_timestamp"

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1317
    return-void
.end method

.method private upgradeFromV8ToV9(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1298
    const-string v0, "CREATE TABLE wishlist (wishlist_account TEXT NOT NULL,wishlist_item_id TEXT NOT NULL,wishlist_item_type TEXT NOT NULL,wishlist_item_state INT NOT NULL,wishlist_in_cloud INT NOT NULL DEFAULT 0,PRIMARY KEY (wishlist_account,wishlist_item_id,wishlist_item_type))"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1309
    return-void
.end method

.method private upgradeFromV9ToV10(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 7
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1291
    const-string v2, "video_userdata"

    const-string v3, "is_new_notification_dismissed"

    const-string v4, "INT NOT NULL DEFAULT 0"

    const-string v5, "1"

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1293
    const-string v0, "videos"

    const-string v1, "publish_timestamp"

    const-string v2, "INT"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/store/DatabaseHelper;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1294
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 91
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->createDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 92
    return-void
.end method

.method public onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 163
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->recreateDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 164
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 96
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Upgrading database from version "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 97
    const/16 v1, 0x2e

    if-ne p3, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    .line 98
    iget-object v1, p0, Lcom/google/android/videos/store/DatabaseHelper;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    invoke-interface {v1, p2, p3}, Lcom/google/android/videos/logging/EventLogger;->onDatabaseUpgrade(II)V

    .line 100
    packed-switch p2, :pswitch_data_0

    .line 150
    :pswitch_0
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->recreateDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 151
    const-string v1, "Upgrade not supported; recreated database"

    invoke-static {v1}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 159
    :goto_1
    return-void

    .line 97
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 103
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV3ToV4(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 104
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV4ToV5(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 105
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV5ToV6(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 106
    :pswitch_4
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV6ToV7(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 107
    :pswitch_5
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV7ToV8(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 108
    :pswitch_6
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV8ToV9(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 109
    :pswitch_7
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV9ToV10(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 110
    :pswitch_8
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV10ToV11(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 111
    :pswitch_9
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV11ToV12(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 112
    :pswitch_a
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV12ToV13(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 113
    :pswitch_b
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV13ToV14(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 114
    :pswitch_c
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV14ToV15(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 115
    :pswitch_d
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV15ToV16(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 116
    :pswitch_e
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV16ToV19(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 118
    :pswitch_f
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV19ToV20(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 119
    :pswitch_10
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV20ToV21(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 120
    :pswitch_11
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV21ToV22(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 121
    :pswitch_12
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV22ToV23(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 122
    :pswitch_13
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV23ToV24(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 123
    :pswitch_14
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV24ToV25(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 124
    :pswitch_15
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV25ToV26(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 125
    :pswitch_16
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV26ToV27(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 126
    :pswitch_17
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV27ToV28(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 127
    :pswitch_18
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV28ToV29(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 128
    :pswitch_19
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV29ToV30(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 129
    :pswitch_1a
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV30ToV31(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 130
    :pswitch_1b
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV31ToV33(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 132
    :pswitch_1c
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV33ToV35(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 134
    :pswitch_1d
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV35ToV36(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 135
    :pswitch_1e
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV36ToV37(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 136
    :pswitch_1f
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV37ToV38(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 137
    :pswitch_20
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV38ToV39(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 138
    :pswitch_21
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV39ToV40(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 139
    :pswitch_22
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV40ToV41(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 140
    :pswitch_23
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV41ToV42(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 141
    :pswitch_24
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV42ToV43(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 142
    :pswitch_25
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV43ToV44(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 143
    :pswitch_26
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV44ToV45(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 144
    :pswitch_27
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->upgradeFromV45ToV46(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 146
    :pswitch_28
    const-string v1, "Upgraded database"

    invoke-static {v1}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 154
    :catch_0
    move-exception v0

    .line 155
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v1, "Failed to upgrade the database"

    invoke-static {v1, v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 156
    iget-object v1, p0, Lcom/google/android/videos/store/DatabaseHelper;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    invoke-interface {v1, p2, p3, v0}, Lcom/google/android/videos/logging/EventLogger;->onDatabaseUpgradeError(IILjava/lang/Exception;)V

    .line 157
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/DatabaseHelper;->recreateDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto/16 :goto_1

    .line 100
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_0
        :pswitch_0
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_0
        :pswitch_1c
        :pswitch_0
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
    .end packed-switch
.end method
