.class public final Lcom/google/android/videos/proto/BandwidthBucketHistory;
.super Lcom/google/protobuf/nano/MessageNano;
.source "BandwidthBucketHistory.java"


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/videos/proto/BandwidthBucketHistory;


# instance fields
.field public bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

.field public fadingCounter:I

.field public key:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 33
    invoke-virtual {p0}, Lcom/google/android/videos/proto/BandwidthBucketHistory;->clear()Lcom/google/android/videos/proto/BandwidthBucketHistory;

    .line 34
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/videos/proto/BandwidthBucketHistory;
    .locals 2

    .prologue
    .line 12
    sget-object v0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->_emptyArray:[Lcom/google/android/videos/proto/BandwidthBucketHistory;

    if-nez v0, :cond_1

    .line 13
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 15
    :try_start_0
    sget-object v0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->_emptyArray:[Lcom/google/android/videos/proto/BandwidthBucketHistory;

    if-nez v0, :cond_0

    .line 16
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/videos/proto/BandwidthBucketHistory;

    sput-object v0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->_emptyArray:[Lcom/google/android/videos/proto/BandwidthBucketHistory;

    .line 18
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    :cond_1
    sget-object v0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->_emptyArray:[Lcom/google/android/videos/proto/BandwidthBucketHistory;

    return-object v0

    .line 18
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/videos/proto/BandwidthBucketHistory;
    .locals 1

    .prologue
    .line 37
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->key:Ljava/lang/String;

    .line 38
    invoke-static {}, Lcom/google/android/videos/proto/BandwidthBucket;->emptyArray()[Lcom/google/android/videos/proto/BandwidthBucket;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    .line 39
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->fadingCounter:I

    .line 40
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->cachedSize:I

    .line 41
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 103
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 104
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->key:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 105
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->key:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 108
    :cond_0
    iget-object v3, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    array-length v3, v3

    if-lez v3, :cond_2

    .line 109
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 110
    iget-object v3, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    aget-object v0, v3, v1

    .line 111
    .local v0, "element":Lcom/google/android/videos/proto/BandwidthBucket;
    if-eqz v0, :cond_1

    .line 112
    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 109
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 117
    .end local v0    # "element":Lcom/google/android/videos/proto/BandwidthBucket;
    .end local v1    # "i":I
    :cond_2
    iget v3, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->fadingCounter:I

    if-eqz v3, :cond_3

    .line 118
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->fadingCounter:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 121
    :cond_3
    return v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 46
    if-ne p1, p0, :cond_1

    .line 67
    :cond_0
    :goto_0
    return v1

    .line 49
    :cond_1
    instance-of v3, p1, Lcom/google/android/videos/proto/BandwidthBucketHistory;

    if-nez v3, :cond_2

    move v1, v2

    .line 50
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 52
    check-cast v0, Lcom/google/android/videos/proto/BandwidthBucketHistory;

    .line 53
    .local v0, "other":Lcom/google/android/videos/proto/BandwidthBucketHistory;
    iget-object v3, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->key:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 54
    iget-object v3, v0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->key:Ljava/lang/String;

    if-eqz v3, :cond_4

    move v1, v2

    .line 55
    goto :goto_0

    .line 57
    :cond_3
    iget-object v3, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->key:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->key:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 58
    goto :goto_0

    .line 60
    :cond_4
    iget-object v3, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    iget-object v4, v0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 62
    goto :goto_0

    .line 64
    :cond_5
    iget v3, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->fadingCounter:I

    iget v4, v0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->fadingCounter:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 65
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 72
    const/16 v0, 0x11

    .line 73
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->key:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 75
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 77
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->fadingCounter:I

    add-int v0, v1, v2

    .line 78
    return v0

    .line 73
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->key:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/BandwidthBucketHistory;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 129
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 130
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 134
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 135
    :sswitch_0
    return-object p0

    .line 140
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->key:Ljava/lang/String;

    goto :goto_0

    .line 144
    :sswitch_2
    const/16 v5, 0x12

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 146
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    if-nez v5, :cond_2

    move v1, v4

    .line 147
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/videos/proto/BandwidthBucket;

    .line 149
    .local v2, "newArray":[Lcom/google/android/videos/proto/BandwidthBucket;
    if-eqz v1, :cond_1

    .line 150
    iget-object v5, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 152
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 153
    new-instance v5, Lcom/google/android/videos/proto/BandwidthBucket;

    invoke-direct {v5}, Lcom/google/android/videos/proto/BandwidthBucket;-><init>()V

    aput-object v5, v2, v1

    .line 154
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 155
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 152
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 146
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/videos/proto/BandwidthBucket;
    :cond_2
    iget-object v5, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    array-length v1, v5

    goto :goto_1

    .line 158
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/videos/proto/BandwidthBucket;
    :cond_3
    new-instance v5, Lcom/google/android/videos/proto/BandwidthBucket;

    invoke-direct {v5}, Lcom/google/android/videos/proto/BandwidthBucket;-><init>()V

    aput-object v5, v2, v1

    .line 159
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 160
    iput-object v2, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    goto :goto_0

    .line 164
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/videos/proto/BandwidthBucket;
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v5

    iput v5, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->fadingCounter:I

    goto :goto_0

    .line 130
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/android/videos/proto/BandwidthBucketHistory;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/BandwidthBucketHistory;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    iget-object v2, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->key:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 85
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->key:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 87
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 88
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 89
    iget-object v2, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    aget-object v0, v2, v1

    .line 90
    .local v0, "element":Lcom/google/android/videos/proto/BandwidthBucket;
    if-eqz v0, :cond_1

    .line 91
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 88
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 95
    .end local v0    # "element":Lcom/google/android/videos/proto/BandwidthBucket;
    .end local v1    # "i":I
    :cond_2
    iget v2, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->fadingCounter:I

    if-eqz v2, :cond_3

    .line 96
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->fadingCounter:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 98
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 99
    return-void
.end method
