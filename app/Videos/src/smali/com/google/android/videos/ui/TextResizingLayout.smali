.class public Lcom/google/android/videos/ui/TextResizingLayout;
.super Landroid/view/ViewGroup;
.source "TextResizingLayout.java"


# instance fields
.field private canExpand:Z

.field private expanded:Z

.field private maxLines:I

.field private final readMoreView:Landroid/widget/TextView;

.field private textView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/ui/TextResizingLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    .line 36
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/videos/ui/TextResizingLayout;->readMoreView:Landroid/widget/TextView;

    .line 39
    iget-object v1, p0, Lcom/google/android/videos/ui/TextResizingLayout;->readMoreView:Landroid/widget/TextView;

    const v2, 0x7f100183

    invoke-virtual {v1, p1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 40
    iget-object v1, p0, Lcom/google/android/videos/ui/TextResizingLayout;->readMoreView:Landroid/widget/TextView;

    const v2, 0x7f0b018b

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 41
    iget-object v1, p0, Lcom/google/android/videos/ui/TextResizingLayout;->readMoreView:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/google/android/videos/ui/TextResizingLayout;->addView(Landroid/view/View;)V

    .line 43
    const/4 v1, 0x1

    new-array v1, v1, [I

    const v2, 0x1010153

    aput v2, v1, v3

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 45
    .local v0, "typedArray":Landroid/content/res/TypedArray;
    const v1, 0x7fffffff

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/videos/ui/TextResizingLayout;->maxLines:I

    .line 46
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 48
    invoke-virtual {p0, v3}, Lcom/google/android/videos/ui/TextResizingLayout;->setWillNotDraw(Z)V

    .line 49
    return-void
.end method

.method private static defaultSize(II)I
    .locals 1
    .param p0, "measureSpec"    # I
    .param p1, "padding"    # I

    .prologue
    .line 103
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 109
    .end local p1    # "padding":I
    :goto_0
    return p1

    .line 105
    .restart local p1    # "padding":I
    :sswitch_0
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    goto :goto_0

    .line 107
    :sswitch_1
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    goto :goto_0

    .line 103
    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x40000000 -> :sswitch_0
    .end sparse-switch
.end method

.method private updateMaxLines()V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/videos/ui/TextResizingLayout;->textView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 71
    iget-object v1, p0, Lcom/google/android/videos/ui/TextResizingLayout;->textView:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/google/android/videos/ui/TextResizingLayout;->expanded:Z

    if-eqz v0, :cond_1

    const v0, 0x7fffffff

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 73
    :cond_0
    return-void

    .line 71
    :cond_1
    iget v0, p0, Lcom/google/android/videos/ui/TextResizingLayout;->maxLines:I

    goto :goto_0
.end method


# virtual methods
.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 2
    .param p1, "child"    # Landroid/view/View;
    .param p2, "index"    # I
    .param p3, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/videos/ui/TextResizingLayout;->readMoreView:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    .line 54
    invoke-super {p0, p1, p2, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 62
    :goto_0
    return-void

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/TextResizingLayout;->textView:Landroid/widget/TextView;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    const-string v1, "A TextView has already been added"

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 58
    instance-of v0, p1, Landroid/widget/TextView;

    const-string v1, "child must be a TextView"

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Preconditions;->checkState(ZLjava/lang/String;)V

    move-object v0, p1

    .line 59
    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/TextResizingLayout;->textView:Landroid/widget/TextView;

    .line 60
    invoke-super {p0, p1, p2, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 61
    invoke-direct {p0}, Lcom/google/android/videos/ui/TextResizingLayout;->updateMaxLines()V

    goto :goto_0

    .line 57
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 9
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 115
    iget-object v6, p0, Lcom/google/android/videos/ui/TextResizingLayout;->textView:Landroid/widget/TextView;

    if-nez v6, :cond_1

    .line 132
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/videos/ui/TextResizingLayout;->getPaddingLeft()I

    move-result v4

    .line 119
    .local v4, "textLeft":I
    invoke-virtual {p0}, Lcom/google/android/videos/ui/TextResizingLayout;->getPaddingTop()I

    move-result v5

    .line 120
    .local v5, "textTop":I
    iget-object v6, p0, Lcom/google/android/videos/ui/TextResizingLayout;->textView:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/google/android/videos/ui/TextResizingLayout;->textView:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v7, v4

    iget-object v8, p0, Lcom/google/android/videos/ui/TextResizingLayout;->textView:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v8, v5

    invoke-virtual {v6, v4, v5, v7, v8}, Landroid/widget/TextView;->layout(IIII)V

    .line 125
    iget-boolean v6, p0, Lcom/google/android/videos/ui/TextResizingLayout;->canExpand:Z

    if-eqz v6, :cond_0

    .line 126
    invoke-virtual {p0}, Lcom/google/android/videos/ui/TextResizingLayout;->getPaddingLeft()I

    move-result v1

    .line 127
    .local v1, "readMoreLeft":I
    iget-object v6, p0, Lcom/google/android/videos/ui/TextResizingLayout;->readMoreView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v6

    add-int v2, v1, v6

    .line 128
    .local v2, "readMoreRight":I
    sub-int v6, p5, p3

    invoke-virtual {p0}, Lcom/google/android/videos/ui/TextResizingLayout;->getPaddingBottom()I

    move-result v7

    sub-int v0, v6, v7

    .line 129
    .local v0, "readMoreBottom":I
    iget-object v6, p0, Lcom/google/android/videos/ui/TextResizingLayout;->readMoreView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    sub-int v3, v0, v6

    .line 130
    .local v3, "readMoreTop":I
    iget-object v6, p0, Lcom/google/android/videos/ui/TextResizingLayout;->readMoreView:Landroid/widget/TextView;

    invoke-virtual {v6, v1, v3, v2, v0}, Landroid/widget/TextView;->layout(IIII)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 9
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/4 v7, 0x0

    .line 77
    invoke-virtual {p0}, Lcom/google/android/videos/ui/TextResizingLayout;->getPaddingLeft()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/videos/ui/TextResizingLayout;->getPaddingRight()I

    move-result v8

    add-int v1, v6, v8

    .line 78
    .local v1, "horizontalPadding":I
    invoke-virtual {p0}, Lcom/google/android/videos/ui/TextResizingLayout;->getPaddingTop()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/videos/ui/TextResizingLayout;->getPaddingBottom()I

    move-result v8

    add-int v4, v6, v8

    .line 79
    .local v4, "verticalPadding":I
    iget-object v6, p0, Lcom/google/android/videos/ui/TextResizingLayout;->textView:Landroid/widget/TextView;

    if-nez v6, :cond_0

    .line 80
    invoke-static {p1, v1}, Lcom/google/android/videos/ui/TextResizingLayout;->defaultSize(II)I

    move-result v6

    invoke-static {p2, v4}, Lcom/google/android/videos/ui/TextResizingLayout;->defaultSize(II)I

    move-result v7

    invoke-virtual {p0, v6, v7}, Lcom/google/android/videos/ui/TextResizingLayout;->setMeasuredDimension(II)V

    .line 100
    :goto_0
    return-void

    .line 85
    :cond_0
    iget-object v6, p0, Lcom/google/android/videos/ui/TextResizingLayout;->textView:Landroid/widget/TextView;

    invoke-virtual {p0, v6, p1, p2}, Lcom/google/android/videos/ui/TextResizingLayout;->measureChild(Landroid/view/View;II)V

    .line 86
    iget-object v6, p0, Lcom/google/android/videos/ui/TextResizingLayout;->textView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    .line 87
    .local v2, "layout":Landroid/text/Layout;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/text/Layout;->getLineCount()I

    move-result v6

    iget v8, p0, Lcom/google/android/videos/ui/TextResizingLayout;->maxLines:I

    if-le v6, v8, :cond_2

    const/4 v6, 0x1

    :goto_1
    iput-boolean v6, p0, Lcom/google/android/videos/ui/TextResizingLayout;->canExpand:Z

    .line 88
    iget-boolean v6, p0, Lcom/google/android/videos/ui/TextResizingLayout;->canExpand:Z

    invoke-virtual {p0, v6}, Lcom/google/android/videos/ui/TextResizingLayout;->setFocusable(Z)V

    .line 89
    iget-boolean v6, p0, Lcom/google/android/videos/ui/TextResizingLayout;->canExpand:Z

    invoke-virtual {p0, v6}, Lcom/google/android/videos/ui/TextResizingLayout;->setClickable(Z)V

    .line 90
    iget-object v8, p0, Lcom/google/android/videos/ui/TextResizingLayout;->readMoreView:Landroid/widget/TextView;

    iget-boolean v6, p0, Lcom/google/android/videos/ui/TextResizingLayout;->canExpand:Z

    if-eqz v6, :cond_3

    iget-boolean v6, p0, Lcom/google/android/videos/ui/TextResizingLayout;->expanded:Z

    if-nez v6, :cond_3

    move v6, v7

    :goto_2
    invoke-virtual {v8, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 91
    iget-object v6, p0, Lcom/google/android/videos/ui/TextResizingLayout;->textView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    .line 92
    .local v5, "width":I
    iget-object v6, p0, Lcom/google/android/videos/ui/TextResizingLayout;->textView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    .line 93
    .local v0, "height":I
    iget-boolean v6, p0, Lcom/google/android/videos/ui/TextResizingLayout;->canExpand:Z

    if-eqz v6, :cond_1

    iget-boolean v6, p0, Lcom/google/android/videos/ui/TextResizingLayout;->expanded:Z

    if-nez v6, :cond_1

    .line 94
    invoke-static {v7, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 95
    .local v3, "measureSpec":I
    iget-object v6, p0, Lcom/google/android/videos/ui/TextResizingLayout;->readMoreView:Landroid/widget/TextView;

    invoke-virtual {v6, v3, v3}, Landroid/widget/TextView;->measure(II)V

    .line 96
    iget-object v6, p0, Lcom/google/android/videos/ui/TextResizingLayout;->readMoreView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 97
    iget-object v6, p0, Lcom/google/android/videos/ui/TextResizingLayout;->readMoreView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v0, v6

    .line 99
    .end local v3    # "measureSpec":I
    :cond_1
    add-int v6, v5, v1

    add-int v7, v0, v4

    invoke-virtual {p0, v6, v7}, Lcom/google/android/videos/ui/TextResizingLayout;->setMeasuredDimension(II)V

    goto :goto_0

    .end local v0    # "height":I
    .end local v5    # "width":I
    :cond_2
    move v6, v7

    .line 87
    goto :goto_1

    .line 90
    :cond_3
    const/16 v6, 0x8

    goto :goto_2
.end method

.method public performClick()Z
    .locals 1

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/google/android/videos/ui/TextResizingLayout;->canExpand:Z

    if-eqz v0, :cond_0

    .line 137
    iget-boolean v0, p0, Lcom/google/android/videos/ui/TextResizingLayout;->expanded:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/videos/ui/TextResizingLayout;->expanded:Z

    .line 138
    invoke-direct {p0}, Lcom/google/android/videos/ui/TextResizingLayout;->updateMaxLines()V

    .line 139
    invoke-virtual {p0}, Lcom/google/android/videos/ui/TextResizingLayout;->invalidate()V

    .line 141
    :cond_0
    invoke-super {p0}, Landroid/view/ViewGroup;->performClick()Z

    move-result v0

    return v0

    .line 137
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setMaxLines(I)V
    .locals 0
    .param p1, "maxLines"    # I

    .prologue
    .line 65
    iput p1, p0, Lcom/google/android/videos/ui/TextResizingLayout;->maxLines:I

    .line 66
    invoke-direct {p0}, Lcom/google/android/videos/ui/TextResizingLayout;->updateMaxLines()V

    .line 67
    return-void
.end method
