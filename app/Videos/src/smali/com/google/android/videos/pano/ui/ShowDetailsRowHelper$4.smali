.class Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$4;
.super Lcom/google/android/videos/pano/model/Item;
.source "ShowDetailsRowHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->addEpisode(Landroid/support/v17/leanback/widget/ArrayObjectAdapter;Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;

.field final synthetic val$episode:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;

.field final synthetic val$episodeAsset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

.field final synthetic val$imageUri:Ljava/lang/String;

.field final synthetic val$metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

.field final synthetic val$purchase:Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;


# direct methods
.method constructor <init>(Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;Ljava/lang/String;Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;Lcom/google/wireless/android/video/magma/proto/AssetResource;)V
    .locals 0

    .prologue
    .line 281
    iput-object p1, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$4;->this$0:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;

    iput-object p2, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$4;->val$metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iput-object p3, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$4;->val$episode:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;

    iput-object p4, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$4;->val$imageUri:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$4;->val$purchase:Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;

    iput-object p6, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$4;->val$episodeAsset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-direct {p0}, Lcom/google/android/videos/pano/model/Item;-><init>()V

    return-void
.end method


# virtual methods
.method public getBadgeResourceId()I
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$4;->val$episode:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;

    iget-boolean v0, v0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->purchased:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0200e7

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCheapestOffer()Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$4;->val$episode:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;

    iget-boolean v0, v0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->purchased:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$4;->val$episode:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;

    iget-object v0, v0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->cheapestOffer:Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    goto :goto_0
.end method

.method public getDurationInSeconds()I
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$4;->val$metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->durationSec:I

    return v0
.end method

.method public getImageHeight()I
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$4;->this$0:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;

    iget-object v0, v0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->getPosterHeight(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public getImageUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$4;->val$imageUri:Ljava/lang/String;

    return-object v0
.end method

.method public getImageWidth()I
    .locals 2

    .prologue
    .line 322
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$4;->this$0:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;

    iget-object v0, v0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e01b9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public getProgress()F
    .locals 2

    .prologue
    .line 352
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$4;->val$episode:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;

    iget-boolean v0, v0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->purchased:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$4;->val$metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->durationSec:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$4;->val$episode:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;

    iget v0, v0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->resumeTimeMillis:I

    int-to-float v0, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$4;->val$metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget v1, v1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->durationSec:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getStarRating()F
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$4;->val$episodeAsset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->viewerRating:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    invoke-static {v0}, Lcom/google/android/videos/pano/model/VideoItem;->getStarRating([Lcom/google/wireless/android/video/magma/proto/ViewerRating;)F

    move-result v0

    return v0
.end method

.method public getSubtitle()Ljava/lang/String;
    .locals 3

    .prologue
    .line 290
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$4;->val$episode:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;

    iget-wide v0, v0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->expirationTimestamp:J

    iget-object v2, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$4;->this$0:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;

    iget-object v2, v2, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->context:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/google/android/videos/pano/ui/PanoHelper;->getExpirationTitle(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$4;->this$0:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;

    iget-object v1, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$4;->val$metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # invokes: Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->getEpisodeTitle(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->access$400(Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/app/Activity;Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "viewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    .prologue
    .line 301
    iget-object v2, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$4;->val$episode:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;

    iget-boolean v2, v2, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->purchased:Z

    if-eqz v2, :cond_1

    .line 303
    const/4 v0, 0x0

    .line 304
    .local v0, "bundle":Landroid/os/Bundle;
    iget-object v2, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$4;->this$0:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;

    iget-object v3, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$4;->val$episode:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;

    # invokes: Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->getOnOwnedEpisodeClickAction(Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;)Landroid/content/Intent;
    invoke-static {v2, v3}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->access$500(Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;)Landroid/content/Intent;

    move-result-object v1

    .line 305
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p2, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    instance-of v2, v2, Lcom/google/android/videos/pano/ui/VideoCardView;

    if-eqz v2, :cond_0

    .line 306
    iget-object v2, p2, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    check-cast v2, Lcom/google/android/videos/pano/ui/VideoCardView;

    invoke-virtual {v2}, Lcom/google/android/videos/pano/ui/VideoCardView;->getImageView()Landroid/widget/ImageView;

    move-result-object v2

    const-string v3, "hero"

    invoke-static {p1, v2, v3}, Landroid/support/v4/app/ActivityOptionsCompat;->makeSceneTransitionAnimation(Landroid/app/Activity;Landroid/view/View;Ljava/lang/String;)Landroid/support/v4/app/ActivityOptionsCompat;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/ActivityOptionsCompat;->toBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 309
    invoke-static {v1}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->markAsRunningAnimation(Landroid/content/Intent;)V

    .line 311
    :cond_0
    invoke-virtual {p1, v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 318
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 314
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$4;->this$0:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;

    iget-object v2, v2, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->onPurchaseActionListener:Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$OnPurchaseActionListener;

    iget-object v3, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$4;->val$purchase:Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;

    invoke-interface {v2, v3}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$OnPurchaseActionListener;->onEpisodePurchase(Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;)V

    goto :goto_0
.end method
