.class Lcom/google/android/videos/VideosApplication$SearchCollapseListener;
.super Ljava/lang/Object;
.source "VideosApplication.java"

# interfaces
.implements Landroid/support/v7/widget/SearchView$OnQueryTextListener;
.implements Landroid/support/v7/widget/SearchView$OnSuggestionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/VideosApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SearchCollapseListener"
.end annotation


# instance fields
.field private final searchItem:Landroid/view/MenuItem;


# direct methods
.method private constructor <init>(Landroid/view/MenuItem;)V
    .locals 1
    .param p1, "searchItem"    # Landroid/view/MenuItem;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    iput-object v0, p0, Lcom/google/android/videos/VideosApplication$SearchCollapseListener;->searchItem:Landroid/view/MenuItem;

    .line 34
    return-void
.end method

.method synthetic constructor <init>(Landroid/view/MenuItem;Lcom/google/android/videos/VideosApplication$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/view/MenuItem;
    .param p2, "x1"    # Lcom/google/android/videos/VideosApplication$1;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/google/android/videos/VideosApplication$SearchCollapseListener;-><init>(Landroid/view/MenuItem;)V

    return-void
.end method


# virtual methods
.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 1
    .param p1, "newText"    # Ljava/lang/String;

    .prologue
    .line 55
    const/4 v0, 0x0

    return v0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 1
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/videos/VideosApplication$SearchCollapseListener;->searchItem:Landroid/view/MenuItem;

    invoke-static {v0}, Landroid/support/v4/view/MenuItemCompat;->collapseActionView(Landroid/view/MenuItem;)Z

    .line 50
    const/4 v0, 0x0

    return v0
.end method

.method public onSuggestionClick(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/videos/VideosApplication$SearchCollapseListener;->searchItem:Landroid/view/MenuItem;

    invoke-static {v0}, Landroid/support/v4/view/MenuItemCompat;->collapseActionView(Landroid/view/MenuItem;)Z

    .line 39
    const/4 v0, 0x0

    return v0
.end method

.method public onSuggestionSelect(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 44
    const/4 v0, 0x0

    return v0
.end method
