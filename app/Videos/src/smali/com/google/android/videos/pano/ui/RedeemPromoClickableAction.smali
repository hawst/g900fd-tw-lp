.class final Lcom/google/android/videos/pano/ui/RedeemPromoClickableAction;
.super Lcom/google/android/videos/pano/ui/ClickableAction;
.source "RedeemPromoClickableAction.java"


# instance fields
.field private final account:Ljava/lang/String;

.field private final assetId:Ljava/lang/String;

.field private final assetType:I

.field private final promoRedeemer:Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;

.field private final promotionCode:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # J
    .param p3, "account"    # Ljava/lang/String;
    .param p4, "assetId"    # Ljava/lang/String;
    .param p5, "assetType"    # I
    .param p6, "promotionCode"    # Ljava/lang/String;
    .param p7, "redeemer"    # Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;
    .param p8, "title"    # Ljava/lang/String;

    .prologue
    .line 17
    const-string v0, ""

    invoke-direct {p0, p1, p2, p8, v0}, Lcom/google/android/videos/pano/ui/ClickableAction;-><init>(JLjava/lang/String;Ljava/lang/String;)V

    .line 18
    iput-object p3, p0, Lcom/google/android/videos/pano/ui/RedeemPromoClickableAction;->account:Ljava/lang/String;

    .line 19
    iput-object p4, p0, Lcom/google/android/videos/pano/ui/RedeemPromoClickableAction;->assetId:Ljava/lang/String;

    .line 20
    iput-object p6, p0, Lcom/google/android/videos/pano/ui/RedeemPromoClickableAction;->promotionCode:Ljava/lang/String;

    .line 21
    iput-object p7, p0, Lcom/google/android/videos/pano/ui/RedeemPromoClickableAction;->promoRedeemer:Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;

    .line 22
    iput p5, p0, Lcom/google/android/videos/pano/ui/RedeemPromoClickableAction;->assetType:I

    .line 23
    return-void
.end method


# virtual methods
.method public onClick(Landroid/app/Activity;)V
    .locals 5
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/RedeemPromoClickableAction;->promoRedeemer:Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;

    iget-object v1, p0, Lcom/google/android/videos/pano/ui/RedeemPromoClickableAction;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/pano/ui/RedeemPromoClickableAction;->assetId:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/videos/pano/ui/RedeemPromoClickableAction;->assetType:I

    iget-object v4, p0, Lcom/google/android/videos/pano/ui/RedeemPromoClickableAction;->promotionCode:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;->redeem(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 28
    return-void
.end method
