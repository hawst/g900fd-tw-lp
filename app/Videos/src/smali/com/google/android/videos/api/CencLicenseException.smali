.class public Lcom/google/android/videos/api/CencLicenseException;
.super Lcom/google/android/videos/converter/ConverterException;
.source "CencLicenseException.java"


# instance fields
.field public final data:[B

.field public final statusCode:I


# direct methods
.method public constructor <init>(I[B)V
    .locals 2
    .param p1, "statusCode"    # I
    .param p2, "data"    # [B

    .prologue
    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Status: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/converter/ConverterException;-><init>(Ljava/lang/String;)V

    .line 95
    iput p1, p0, Lcom/google/android/videos/api/CencLicenseException;->statusCode:I

    .line 96
    iput-object p2, p0, Lcom/google/android/videos/api/CencLicenseException;->data:[B

    .line 97
    return-void
.end method


# virtual methods
.method public failImmediately()Z
    .locals 2

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/google/android/videos/api/CencLicenseException;->isAuthRetryable()Z

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x1f40

    iget v1, p0, Lcom/google/android/videos/api/CencLicenseException;->statusCode:I

    if-gt v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/videos/api/CencLicenseException;->statusCode:I

    const/16 v1, 0x2328

    if-lt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAuthRetryable()Z
    .locals 2

    .prologue
    .line 108
    const/16 v0, 0x13ec

    iget v1, p0, Lcom/google/android/videos/api/CencLicenseException;->statusCode:I

    if-gt v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/videos/api/CencLicenseException;->statusCode:I

    const/16 v1, 0x1450

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CencLicenseException ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/videos/api/CencLicenseException;->statusCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
