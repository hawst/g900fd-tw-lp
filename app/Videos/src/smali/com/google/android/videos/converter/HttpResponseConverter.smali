.class public abstract Lcom/google/android/videos/converter/HttpResponseConverter;
.super Ljava/lang/Object;
.source "HttpResponseConverter.java"

# interfaces
.implements Lcom/google/android/videos/converter/ResponseConverter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/converter/ResponseConverter",
        "<",
        "Lorg/apache/http/HttpResponse;",
        "TT;>;"
    }
.end annotation


# static fields
.field public static final BYTE_ARRAY:Lcom/google/android/videos/converter/HttpResponseConverter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/converter/HttpResponseConverter",
            "<[B>;"
        }
    .end annotation
.end field

.field public static final IDENTITY:Lcom/google/android/videos/converter/HttpResponseConverter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/converter/HttpResponseConverter",
            "<",
            "Lorg/apache/http/HttpEntity;",
            ">;"
        }
    .end annotation
.end field

.field public static final VOID:Lcom/google/android/videos/converter/HttpResponseConverter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/converter/HttpResponseConverter",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lcom/google/android/videos/converter/HttpResponseConverter$1;

    invoke-direct {v0}, Lcom/google/android/videos/converter/HttpResponseConverter$1;-><init>()V

    sput-object v0, Lcom/google/android/videos/converter/HttpResponseConverter;->VOID:Lcom/google/android/videos/converter/HttpResponseConverter;

    .line 49
    new-instance v0, Lcom/google/android/videos/converter/HttpResponseConverter$2;

    invoke-direct {v0}, Lcom/google/android/videos/converter/HttpResponseConverter$2;-><init>()V

    sput-object v0, Lcom/google/android/videos/converter/HttpResponseConverter;->IDENTITY:Lcom/google/android/videos/converter/HttpResponseConverter;

    .line 61
    new-instance v0, Lcom/google/android/videos/converter/HttpResponseConverter$3;

    invoke-direct {v0}, Lcom/google/android/videos/converter/HttpResponseConverter$3;-><init>()V

    sput-object v0, Lcom/google/android/videos/converter/HttpResponseConverter;->BYTE_ARRAY:Lcom/google/android/videos/converter/HttpResponseConverter;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    .local p0, "this":Lcom/google/android/videos/converter/HttpResponseConverter;, "Lcom/google/android/videos/converter/HttpResponseConverter<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected final checkHttpError(Lorg/apache/http/HttpResponse;)V
    .locals 1
    .param p1, "response"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/HttpResponseException;
        }
    .end annotation

    .prologue
    .line 90
    .local p0, "this":Lcom/google/android/videos/converter/HttpResponseConverter;, "Lcom/google/android/videos/converter/HttpResponseConverter<TT;>;"
    invoke-virtual {p0, p1}, Lcom/google/android/videos/converter/HttpResponseConverter;->isError(Lorg/apache/http/HttpResponse;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    invoke-virtual {p0, p1}, Lcom/google/android/videos/converter/HttpResponseConverter;->createHttpException(Lorg/apache/http/HttpResponse;)Lorg/apache/http/client/HttpResponseException;

    move-result-object v0

    throw v0

    .line 93
    :cond_0
    return-void
.end method

.method public bridge synthetic convertResponse(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32
    .local p0, "this":Lcom/google/android/videos/converter/HttpResponseConverter;, "Lcom/google/android/videos/converter/HttpResponseConverter<TT;>;"
    check-cast p1, Lorg/apache/http/HttpResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/converter/HttpResponseConverter;->convertResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public convertResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 1
    .param p1, "response"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/HttpResponse;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 77
    .local p0, "this":Lcom/google/android/videos/converter/HttpResponseConverter;, "Lcom/google/android/videos/converter/HttpResponseConverter<TT;>;"
    invoke-virtual {p0, p1}, Lcom/google/android/videos/converter/HttpResponseConverter;->checkHttpError(Lorg/apache/http/HttpResponse;)V

    .line 78
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/converter/HttpResponseConverter;->convertResponseEntity(Lorg/apache/http/HttpEntity;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected convertResponseEntity(Lorg/apache/http/HttpEntity;)Ljava/lang/Object;
    .locals 1
    .param p1, "entity"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/HttpEntity;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 86
    .local p0, "this":Lcom/google/android/videos/converter/HttpResponseConverter;, "Lcom/google/android/videos/converter/HttpResponseConverter<TT;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method protected createHttpException(Lorg/apache/http/HttpResponse;)Lorg/apache/http/client/HttpResponseException;
    .locals 4
    .param p1, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 101
    .local p0, "this":Lcom/google/android/videos/converter/HttpResponseConverter;, "Lcom/google/android/videos/converter/HttpResponseConverter<TT;>;"
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    .line 102
    .local v1, "status":Lorg/apache/http/StatusLine;
    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    .line 103
    .local v2, "statusCode":I
    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v0

    .line 104
    .local v0, "reason":Ljava/lang/String;
    new-instance v3, Lorg/apache/http/client/HttpResponseException;

    invoke-direct {v3, v2, v0}, Lorg/apache/http/client/HttpResponseException;-><init>(ILjava/lang/String;)V

    return-object v3
.end method

.method protected isError(Lorg/apache/http/HttpResponse;)Z
    .locals 2
    .param p1, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 97
    .local p0, "this":Lcom/google/android/videos/converter/HttpResponseConverter;, "Lcom/google/android/videos/converter/HttpResponseConverter<TT;>;"
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    const/16 v1, 0x12c

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
