.class Lcom/google/android/videos/remote/RemotePlaybackHelper$1;
.super Lcom/google/android/videos/remote/Timeout;
.source "RemotePlaybackHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/remote/RemotePlaybackHelper;->load(Lcom/google/android/videos/player/DirectorInitializer$InitializationData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/remote/RemotePlaybackHelper;

.field final synthetic val$initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;


# direct methods
.method constructor <init>(Lcom/google/android/videos/remote/RemotePlaybackHelper;Landroid/os/Handler;JLcom/google/android/videos/player/DirectorInitializer$InitializationData;)V
    .locals 1
    .param p2, "x0"    # Landroid/os/Handler;
    .param p3, "x1"    # J

    .prologue
    .line 243
    iput-object p1, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper$1;->this$0:Lcom/google/android/videos/remote/RemotePlaybackHelper;

    iput-object p5, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper$1;->val$initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/videos/remote/Timeout;-><init>(Landroid/os/Handler;J)V

    return-void
.end method


# virtual methods
.method protected onTimeout()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 246
    const-string v0, "Timeout trying to push video to remote device"

    invoke-static {v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 247
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper$1;->this$0:Lcom/google/android/videos/remote/RemotePlaybackHelper;

    # setter for: Lcom/google/android/videos/remote/RemotePlaybackHelper;->shouldInitializeSubtitles:Z
    invoke-static {v0, v1}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->access$002(Lcom/google/android/videos/remote/RemotePlaybackHelper;Z)Z

    .line 248
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper$1;->this$0:Lcom/google/android/videos/remote/RemotePlaybackHelper;

    # getter for: Lcom/google/android/videos/remote/RemotePlaybackHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;
    invoke-static {v0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->access$100(Lcom/google/android/videos/remote/RemotePlaybackHelper;)Lcom/google/android/videos/player/overlay/ControllerOverlay;

    move-result-object v0

    invoke-interface {v0, v1, v1}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setState(ZZ)V

    .line 249
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper$1;->this$0:Lcom/google/android/videos/remote/RemotePlaybackHelper;

    # getter for: Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;
    invoke-static {v0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->access$200(Lcom/google/android/videos/remote/RemotePlaybackHelper;)Lcom/google/android/videos/remote/RemoteControl;

    move-result-object v0

    const/16 v1, -0x3ea

    new-instance v2, Lcom/google/android/videos/remote/RemotePlaybackHelper$1$1;

    invoke-direct {v2, p0}, Lcom/google/android/videos/remote/RemotePlaybackHelper$1$1;-><init>(Lcom/google/android/videos/remote/RemotePlaybackHelper$1;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/remote/RemoteControl;->onError(ILcom/google/android/videos/utils/RetryAction;)V

    .line 256
    return-void
.end method
