.class interface abstract Lcom/google/android/videos/pano/repositories/ShowsQuery;
.super Ljava/lang/Object;
.source "ShowsQuery.java"


# static fields
.field public static final PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 18
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "shows_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "shows_poster_uri"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "shows_title"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "shows_banner_uri"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/videos/pano/repositories/ShowsQuery;->PROJECTION:[Ljava/lang/String;

    return-void
.end method
