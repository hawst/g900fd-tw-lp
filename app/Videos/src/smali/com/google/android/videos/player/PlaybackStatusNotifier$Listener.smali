.class public interface abstract Lcom/google/android/videos/player/PlaybackStatusNotifier$Listener;
.super Ljava/lang/Object;
.source "PlaybackStatusNotifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/PlaybackStatusNotifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onPlaybackSessionStatusChanged(Z)V
.end method

.method public abstract onStreamingStatusChanged(Z)V
.end method
