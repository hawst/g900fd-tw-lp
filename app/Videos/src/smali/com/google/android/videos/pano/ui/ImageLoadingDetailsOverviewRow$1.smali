.class Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow$1;
.super Ljava/lang/Object;
.source "ImageLoadingDetailsOverviewRow.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;->setImageUri(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Landroid/net/Uri;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;


# direct methods
.method constructor <init>(Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow$1;->this$0:Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/net/Uri;Ljava/lang/Exception;)V
    .locals 2
    .param p1, "request"    # Landroid/net/Uri;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow$1;->this$0:Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;->setBitmapInternal(Landroid/graphics/Bitmap;)V
    invoke-static {v0, v1}, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;->access$100(Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;Landroid/graphics/Bitmap;)V

    .line 51
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 39
    check-cast p1, Landroid/net/Uri;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow$1;->onError(Landroid/net/Uri;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Landroid/net/Uri;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "request"    # Landroid/net/Uri;
    .param p2, "response"    # Landroid/graphics/Bitmap;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow$1;->this$0:Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;

    # getter for: Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;->imageUri:Landroid/net/Uri;
    invoke-static {v0}, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;->access$000(Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow$1;->this$0:Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;

    # invokes: Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;->setBitmapInternal(Landroid/graphics/Bitmap;)V
    invoke-static {v0, p2}, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;->access$100(Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;Landroid/graphics/Bitmap;)V

    .line 46
    :cond_0
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 39
    check-cast p1, Landroid/net/Uri;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Landroid/graphics/Bitmap;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow$1;->onResponse(Landroid/net/Uri;Landroid/graphics/Bitmap;)V

    return-void
.end method
