.class public interface abstract Lcom/google/android/videos/store/MasterSubCursor;
.super Ljava/lang/Object;
.source "MasterSubCursor.java"

# interfaces
.implements Landroid/database/Cursor;


# virtual methods
.method public abstract getCurrentSubRange()Lcom/google/android/videos/store/MasterSubCursor;
.end method

.method public abstract getMasterInt(I)I
.end method

.method public abstract getMasterLong(I)J
.end method

.method public abstract getMasterString(I)Ljava/lang/String;
.end method

.method public abstract getNonEmptySubRangeCount()I
.end method
