.class Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;
.super Ljava/lang/Object;
.source "SecondScreenEntitiesPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EntityEvent"
.end annotation


# instance fields
.field public final eventType:I

.field public final knowledgeEntity:Lcom/google/android/videos/tagging/KnowledgeEntity;

.field public final timestamp:I


# direct methods
.method public constructor <init>(Lcom/google/android/videos/tagging/KnowledgeEntity;II)V
    .locals 0
    .param p1, "knowledgeEntity"    # Lcom/google/android/videos/tagging/KnowledgeEntity;
    .param p2, "timestamp"    # I
    .param p3, "eventType"    # I

    .prologue
    .line 302
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 303
    iput-object p1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;->knowledgeEntity:Lcom/google/android/videos/tagging/KnowledgeEntity;

    .line 304
    iput p2, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;->timestamp:I

    .line 305
    iput p3, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;->eventType:I

    .line 306
    return-void
.end method
