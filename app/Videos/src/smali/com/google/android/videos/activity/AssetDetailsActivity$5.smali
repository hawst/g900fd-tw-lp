.class Lcom/google/android/videos/activity/AssetDetailsActivity$5;
.super Ljava/lang/Object;
.source "AssetDetailsActivity.java"

# interfaces
.implements Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/activity/AssetDetailsActivity;->addSearchExpandListener(Landroid/view/MenuItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/activity/AssetDetailsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/videos/activity/AssetDetailsActivity;)V
    .locals 0

    .prologue
    .line 457
    iput-object p1, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$5;->this$0:Lcom/google/android/videos/activity/AssetDetailsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemActionCollapse(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x0

    .line 460
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$5;->this$0:Lcom/google/android/videos/activity/AssetDetailsActivity;

    iget-object v0, v0, Lcom/google/android/videos/activity/AssetDetailsActivity;->mediaRouteProvider:Lcom/google/android/videos/remote/MediaRouteProvider;

    invoke-interface {v0, v1}, Lcom/google/android/videos/remote/MediaRouteProvider;->setForceHidden(Z)V

    .line 461
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$5;->this$0:Lcom/google/android/videos/activity/AssetDetailsActivity;

    iget-object v0, v0, Lcom/google/android/videos/activity/AssetDetailsActivity;->playLayout:Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;->unlockHeader()V

    .line 462
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$5;->this$0:Lcom/google/android/videos/activity/AssetDetailsActivity;

    # getter for: Lcom/google/android/videos/activity/AssetDetailsActivity;->actionBarBackgroundUpdater:Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;
    invoke-static {v0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->access$100(Lcom/google/android/videos/activity/AssetDetailsActivity;)Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;->forceActionBarOpaque(Z)V

    .line 463
    const/4 v0, 0x1

    return v0
.end method

.method public onMenuItemActionExpand(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 468
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$5;->this$0:Lcom/google/android/videos/activity/AssetDetailsActivity;

    iget-object v0, v0, Lcom/google/android/videos/activity/AssetDetailsActivity;->mediaRouteProvider:Lcom/google/android/videos/remote/MediaRouteProvider;

    invoke-interface {v0, v1}, Lcom/google/android/videos/remote/MediaRouteProvider;->setForceHidden(Z)V

    .line 469
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$5;->this$0:Lcom/google/android/videos/activity/AssetDetailsActivity;

    # getter for: Lcom/google/android/videos/activity/AssetDetailsActivity;->actionBarBackgroundUpdater:Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;
    invoke-static {v0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->access$100(Lcom/google/android/videos/activity/AssetDetailsActivity;)Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;->forceActionBarOpaque(Z)V

    .line 470
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$5;->this$0:Lcom/google/android/videos/activity/AssetDetailsActivity;

    iget-object v0, v0, Lcom/google/android/videos/activity/AssetDetailsActivity;->playLayout:Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;->lockHeader(Z)V

    .line 471
    return v1
.end method
