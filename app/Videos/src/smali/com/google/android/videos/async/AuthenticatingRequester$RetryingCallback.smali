.class final Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;
.super Ljava/lang/Object;
.source "AuthenticatingRequester.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/async/AuthenticatingRequester;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "RetryingCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<TS;TE;>;"
    }
.end annotation


# instance fields
.field private final authToken:Ljava/lang/String;

.field private final callback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;"
        }
    .end annotation
.end field

.field private final originalRequest:Lcom/google/android/videos/async/Request;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TR;"
        }
    .end annotation
.end field

.field private retried:Z

.field final synthetic this$0:Lcom/google/android/videos/async/AuthenticatingRequester;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/async/AuthenticatingRequester;Lcom/google/android/videos/async/Callback;Ljava/lang/String;Lcom/google/android/videos/async/Request;)V
    .locals 0
    .param p3, "authToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;",
            "Ljava/lang/String;",
            "TR;)V"
        }
    .end annotation

    .prologue
    .line 76
    .local p0, "this":Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;, "Lcom/google/android/videos/async/AuthenticatingRequester<TR;TS;TE;>.RetryingCallback;"
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TR;TE;>;"
    .local p4, "originalRequest":Lcom/google/android/videos/async/Request;, "TR;"
    iput-object p1, p0, Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;->this$0:Lcom/google/android/videos/async/AuthenticatingRequester;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object p2, p0, Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;->callback:Lcom/google/android/videos/async/Callback;

    .line 78
    iput-object p3, p0, Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;->authToken:Ljava/lang/String;

    .line 79
    iput-object p4, p0, Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;->originalRequest:Lcom/google/android/videos/async/Request;

    .line 80
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/async/AuthenticatingRequester;Lcom/google/android/videos/async/Callback;Ljava/lang/String;Lcom/google/android/videos/async/Request;Lcom/google/android/videos/async/AuthenticatingRequester$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/async/AuthenticatingRequester;
    .param p2, "x1"    # Lcom/google/android/videos/async/Callback;
    .param p3, "x2"    # Ljava/lang/String;
    .param p4, "x3"    # Lcom/google/android/videos/async/Request;
    .param p5, "x4"    # Lcom/google/android/videos/async/AuthenticatingRequester$1;

    .prologue
    .line 68
    .local p0, "this":Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;, "Lcom/google/android/videos/async/AuthenticatingRequester<TR;TS;TE;>.RetryingCallback;"
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;-><init>(Lcom/google/android/videos/async/AuthenticatingRequester;Lcom/google/android/videos/async/Callback;Ljava/lang/String;Lcom/google/android/videos/async/Request;)V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 5
    .param p2, "exception"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    .prologue
    .line 89
    .local p0, "this":Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;, "Lcom/google/android/videos/async/AuthenticatingRequester<TR;TS;TE;>.RetryingCallback;"
    .local p1, "request":Ljava/lang/Object;, "TS;"
    iget-boolean v3, p0, Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;->retried:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;->this$0:Lcom/google/android/videos/async/AuthenticatingRequester;

    iget-object v4, p0, Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;->originalRequest:Lcom/google/android/videos/async/Request;

    invoke-virtual {v3, v4, p2}, Lcom/google/android/videos/async/AuthenticatingRequester;->canRetry(Lcom/google/android/videos/async/Request;Ljava/lang/Exception;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 90
    :cond_0
    iget-object v3, p0, Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;->callback:Lcom/google/android/videos/async/Callback;

    iget-object v4, p0, Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;->originalRequest:Lcom/google/android/videos/async/Request;

    invoke-interface {v3, v4, p2}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 111
    :goto_0
    return-void

    .line 92
    :cond_1
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;->retried:Z

    .line 93
    iget-object v3, p0, Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;->authToken:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 94
    iget-object v3, p0, Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;->this$0:Lcom/google/android/videos/async/AuthenticatingRequester;

    # getter for: Lcom/google/android/videos/async/AuthenticatingRequester;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;
    invoke-static {v3}, Lcom/google/android/videos/async/AuthenticatingRequester;->access$100(Lcom/google/android/videos/async/AuthenticatingRequester;)Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;->authToken:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->invalidateAuthToken(Ljava/lang/String;)V

    .line 96
    :cond_2
    const/4 v2, 0x0

    .line 98
    .local v2, "newAuthToken":Ljava/lang/String;
    :try_start_0
    iget-object v3, p0, Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;->this$0:Lcom/google/android/videos/async/AuthenticatingRequester;

    iget-object v4, p0, Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;->originalRequest:Lcom/google/android/videos/async/Request;

    iget-object v4, v4, Lcom/google/android/videos/async/Request;->account:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/videos/async/AuthenticatingRequester;->getAuthToken(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 106
    :cond_3
    :try_start_1
    iget-object v3, p0, Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;->this$0:Lcom/google/android/videos/async/AuthenticatingRequester;

    iget-object v4, p0, Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;->originalRequest:Lcom/google/android/videos/async/Request;

    invoke-virtual {v3, v4, v2, p0}, Lcom/google/android/videos/async/AuthenticatingRequester;->makeAuthenticatedRequest(Lcom/google/android/videos/async/Request;Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V
    :try_end_1
    .catch Lcom/google/android/videos/converter/ConverterException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 107
    :catch_0
    move-exception v0

    .line 108
    .local v0, "converterException":Lcom/google/android/videos/converter/ConverterException;
    iget-object v3, p0, Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;->callback:Lcom/google/android/videos/async/Callback;

    iget-object v4, p0, Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;->originalRequest:Lcom/google/android/videos/async/Request;

    invoke-interface {v3, v4, v0}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0

    .line 99
    .end local v0    # "converterException":Lcom/google/android/videos/converter/ConverterException;
    :catch_1
    move-exception v1

    .line 100
    .local v1, "e":Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException;
    iget-object v3, p0, Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;->originalRequest:Lcom/google/android/videos/async/Request;

    iget-boolean v3, v3, Lcom/google/android/videos/async/Request;->requireAuthentication:Z

    if-eqz v3, :cond_3

    .line 101
    iget-object v3, p0, Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;->callback:Lcom/google/android/videos/async/Callback;

    iget-object v4, p0, Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;->originalRequest:Lcom/google/android/videos/async/Request;

    invoke-interface {v3, v4, v1}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;TE;)V"
        }
    .end annotation

    .prologue
    .line 84
    .local p0, "this":Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;, "Lcom/google/android/videos/async/AuthenticatingRequester<TR;TS;TE;>.RetryingCallback;"
    .local p1, "request":Ljava/lang/Object;, "TS;"
    .local p2, "response":Ljava/lang/Object;, "TE;"
    iget-object v0, p0, Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;->callback:Lcom/google/android/videos/async/Callback;

    iget-object v1, p0, Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;->originalRequest:Lcom/google/android/videos/async/Request;

    invoke-interface {v0, v1, p2}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 85
    return-void
.end method
