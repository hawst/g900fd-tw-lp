.class public interface abstract Lcom/google/android/videos/remote/MediaRouteProvider;
.super Ljava/lang/Object;
.source "MediaRouteProvider.java"


# virtual methods
.method public abstract getMediaRouter()Landroid/support/v7/media/MediaRouter;
.end method

.method public abstract getSupportedRouteTypes()Landroid/support/v7/media/MediaRouteSelector;
.end method

.method public abstract onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
.end method

.method public abstract onStart()V
.end method

.method public abstract onStop()V
.end method

.method public abstract setForceHidden(Z)V
.end method

.method public abstract showPicker()V
.end method
