.class Lcom/google/android/videos/player/DirectorInitializer$11;
.super Ljava/lang/Object;
.source "DirectorInitializer.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/player/DirectorInitializer;->initAssetsCallback()Lcom/google/android/videos/async/Callback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/api/AssetsRequest;",
        "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/player/DirectorInitializer;


# direct methods
.method constructor <init>(Lcom/google/android/videos/player/DirectorInitializer;)V
    .locals 0

    .prologue
    .line 875
    iput-object p1, p0, Lcom/google/android/videos/player/DirectorInitializer$11;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/api/AssetsRequest;Ljava/lang/Exception;)V
    .locals 3
    .param p1, "request"    # Lcom/google/android/videos/api/AssetsRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 883
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer$11;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;
    invoke-static {v0}, Lcom/google/android/videos/player/DirectorInitializer;->access$200(Lcom/google/android/videos/player/DirectorInitializer;)Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    move-result-object v0

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->recordTaskEnd(IZ)V

    .line 884
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Could not fetch assets for ids: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/videos/api/AssetsRequest;->ids:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 885
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer$11;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    const/4 v1, 0x2

    const/4 v2, 0x1

    # invokes: Lcom/google/android/videos/player/DirectorInitializer;->onInitializationError(ILjava/lang/Exception;Z)V
    invoke-static {v0, v1, p2, v2}, Lcom/google/android/videos/player/DirectorInitializer;->access$2900(Lcom/google/android/videos/player/DirectorInitializer;ILjava/lang/Exception;Z)V

    .line 886
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 875
    check-cast p1, Lcom/google/android/videos/api/AssetsRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/player/DirectorInitializer$11;->onError(Lcom/google/android/videos/api/AssetsRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/api/AssetsRequest;
    .param p2, "response"    # Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    .prologue
    .line 878
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer$11;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;
    invoke-static {v0}, Lcom/google/android/videos/player/DirectorInitializer;->access$200(Lcom/google/android/videos/player/DirectorInitializer;)Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->recordTaskEnd(I)V

    .line 879
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer$11;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # invokes: Lcom/google/android/videos/player/DirectorInitializer;->onTrailerAssetsResponse(Lcom/google/wireless/android/video/magma/proto/AssetListResponse;)V
    invoke-static {v0, p2}, Lcom/google/android/videos/player/DirectorInitializer;->access$3800(Lcom/google/android/videos/player/DirectorInitializer;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;)V

    .line 880
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 875
    check-cast p1, Lcom/google/android/videos/api/AssetsRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/player/DirectorInitializer$11;->onResponse(Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;)V

    return-void
.end method
