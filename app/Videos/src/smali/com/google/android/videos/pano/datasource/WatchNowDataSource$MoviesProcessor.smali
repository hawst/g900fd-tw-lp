.class Lcom/google/android/videos/pano/datasource/WatchNowDataSource$MoviesProcessor;
.super Lcom/google/android/videos/pano/datasource/WatchNowDataSource$ItemsProcessor;
.source "WatchNowDataSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/datasource/WatchNowDataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MoviesProcessor"
.end annotation


# instance fields
.field private final moviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

.field final synthetic this$0:Lcom/google/android/videos/pano/datasource/WatchNowDataSource;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;)V
    .locals 1

    .prologue
    .line 231
    iput-object p1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$MoviesProcessor;->this$0:Lcom/google/android/videos/pano/datasource/WatchNowDataSource;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$ItemsProcessor;-><init>(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;Lcom/google/android/videos/pano/datasource/WatchNowDataSource$1;)V

    .line 233
    new-instance v0, Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-direct {v0}, Lcom/google/android/videos/adapter/MoviesDataSource;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$MoviesProcessor;->moviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;Lcom/google/android/videos/pano/datasource/WatchNowDataSource$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/pano/datasource/WatchNowDataSource;
    .param p2, "x1"    # Lcom/google/android/videos/pano/datasource/WatchNowDataSource$1;

    .prologue
    .line 231
    invoke-direct {p0, p1}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$MoviesProcessor;-><init>(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;)V

    return-void
.end method


# virtual methods
.method protected createCursorRequest()Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 8

    .prologue
    .line 237
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 238
    .local v2, "nowTimestamp":J
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$MoviesProcessor;->this$0:Lcom/google/android/videos/pano/datasource/WatchNowDataSource;

    invoke-virtual {v0}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->getAccount()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$MoviesProcessor;->this$0:Lcom/google/android/videos/pano/datasource/WatchNowDataSource;

    # getter for: Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->config:Lcom/google/android/videos/Config;
    invoke-static {v0}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->access$700(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;)Lcom/google/android/videos/Config;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/Config;->recentActiveMillis()J

    move-result-wide v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Lcom/google/android/videos/adapter/MoviesDataSource$Query;->createNewRequest(Ljava/lang/String;JJ[Ljava/lang/String;Z)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v0

    return-object v0
.end method

.method protected createItemFromCursor(Landroid/database/Cursor;Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;
    .locals 20
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "account"    # Ljava/lang/String;

    .prologue
    .line 245
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$MoviesProcessor;->moviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Lcom/google/android/videos/adapter/MoviesDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    .line 246
    .local v4, "id":Ljava/lang/String;
    const/4 v5, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-static {v0, v1, v4, v5}, Lcom/google/android/videos/activity/LauncherActivity;->createWatchMovieDeepLinkingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v11

    .line 247
    .local v11, "intent":Landroid/content/Intent;
    invoke-static/range {p2 .. p2}, Lcom/google/android/videos/pano/ui/PanoHelper;->getDefaultPosterHeight(Landroid/content/Context;)I

    move-result v10

    .line 248
    .local v10, "posterHeight":I
    int-to-float v5, v10

    const v6, 0x3f31a787

    mul-float/2addr v5, v6

    float-to-int v9, v5

    .line 250
    .local v9, "posterWidth":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$MoviesProcessor;->moviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Lcom/google/android/videos/adapter/MoviesDataSource;->getReleaseYear(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v18

    .line 251
    .local v18, "year":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$MoviesProcessor;->moviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Lcom/google/android/videos/adapter/MoviesDataSource;->getExpirationTimestamp(Landroid/database/Cursor;)J

    move-result-wide v12

    .line 252
    .local v12, "expirationTimestamp":J
    move-object/from16 v0, p2

    invoke-static {v12, v13, v0}, Lcom/google/android/videos/pano/ui/PanoHelper;->getExpirationTitle(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 253
    .local v2, "expirationTitle":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$MoviesProcessor;->moviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Lcom/google/android/videos/adapter/MoviesDataSource;->getDurationSeconds(Landroid/database/Cursor;)I

    move-result v15

    .line 254
    .local v15, "durationInSeconds":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$MoviesProcessor;->moviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Lcom/google/android/videos/adapter/MoviesDataSource;->getResumeTimestamp(Landroid/database/Cursor;)I

    move-result v17

    .line 255
    .local v17, "resumeTimestamp":I
    new-instance v3, Lcom/google/android/videos/pano/model/MovieItem;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$MoviesProcessor;->moviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Lcom/google/android/videos/adapter/MoviesDataSource;->getTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    move-object v6, v2

    :goto_0
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$MoviesProcessor;->moviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/google/android/videos/adapter/MoviesDataSource;->getPosterUri(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$MoviesProcessor;->moviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    move-object/from16 v0, p1

    invoke-virtual {v8, v0}, Lcom/google/android/videos/adapter/MoviesDataSource;->getScreenshotUri(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v8

    if-eqz v18, :cond_1

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v14

    :goto_1
    if-nez v15, :cond_2

    const/16 v16, 0x0

    :goto_2
    invoke-direct/range {v3 .. v16}, Lcom/google/android/videos/pano/model/MovieItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/content/Intent;JIIF)V

    .line 269
    .local v3, "item":Lcom/google/android/videos/pano/model/VideoItem;
    new-instance v5, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$MoviesProcessor;->moviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Lcom/google/android/videos/adapter/MoviesDataSource;->getLastActivityTimestamp(Landroid/database/Cursor;)J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-direct {v5, v3, v6, v7, v8}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;-><init>(Lcom/google/android/videos/pano/model/VideoItem;JLcom/google/android/videos/pano/datasource/WatchNowDataSource$1;)V

    return-object v5

    .line 255
    .end local v3    # "item":Lcom/google/android/videos/pano/model/VideoItem;
    :cond_0
    const v6, 0x7f0b0096

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    :cond_1
    const/4 v14, 0x0

    goto :goto_1

    :cond_2
    move/from16 v0, v17

    div-int/lit16 v0, v0, 0x3e8

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    int-to-float v0, v15

    move/from16 v19, v0

    div-float v16, v16, v19

    goto :goto_2
.end method
