.class Lcom/google/android/videos/ui/ShuffleAddItemAnimator$1;
.super Landroid/animation/AnimatorListenerAdapter;
.source "ShuffleAddItemAnimator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/ui/ShuffleAddItemAnimator;->runAddAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/ui/ShuffleAddItemAnimator;

.field final synthetic val$animation:Landroid/view/ViewPropertyAnimator;

.field final synthetic val$holder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/videos/ui/ShuffleAddItemAnimator;Landroid/view/View;Landroid/support/v7/widget/RecyclerView$ViewHolder;Landroid/view/ViewPropertyAnimator;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/videos/ui/ShuffleAddItemAnimator$1;->this$0:Lcom/google/android/videos/ui/ShuffleAddItemAnimator;

    iput-object p2, p0, Lcom/google/android/videos/ui/ShuffleAddItemAnimator$1;->val$view:Landroid/view/View;

    iput-object p3, p0, Lcom/google/android/videos/ui/ShuffleAddItemAnimator$1;->val$holder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    iput-object p4, p0, Lcom/google/android/videos/ui/ShuffleAddItemAnimator$1;->val$animation:Landroid/view/ViewPropertyAnimator;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/videos/ui/ShuffleAddItemAnimator$1;->val$animation:Landroid/view/ViewPropertyAnimator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 51
    iget-object v0, p0, Lcom/google/android/videos/ui/ShuffleAddItemAnimator$1;->this$0:Lcom/google/android/videos/ui/ShuffleAddItemAnimator;

    iget-object v1, p0, Lcom/google/android/videos/ui/ShuffleAddItemAnimator$1;->val$holder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/ShuffleAddItemAnimator;->onAddAnimationEnd(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 52
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/videos/ui/ShuffleAddItemAnimator$1;->val$view:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 45
    iget-object v0, p0, Lcom/google/android/videos/ui/ShuffleAddItemAnimator$1;->this$0:Lcom/google/android/videos/ui/ShuffleAddItemAnimator;

    iget-object v1, p0, Lcom/google/android/videos/ui/ShuffleAddItemAnimator$1;->val$holder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/ShuffleAddItemAnimator;->dispatchAddStarting(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 46
    return-void
.end method
