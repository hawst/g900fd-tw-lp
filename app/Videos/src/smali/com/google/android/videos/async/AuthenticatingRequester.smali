.class public abstract Lcom/google/android/videos/async/AuthenticatingRequester;
.super Ljava/lang/Object;
.source "AuthenticatingRequester.java"

# interfaces
.implements Lcom/google/android/videos/async/Requester;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/async/AuthenticatingRequester$1;,
        Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Lcom/google/android/videos/async/Request;",
        "S:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Requester",
        "<TR;TE;>;"
    }
.end annotation


# instance fields
.field private final accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/accounts/AccountManagerWrapper;)V
    .locals 0
    .param p1, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;

    .prologue
    .line 31
    .local p0, "this":Lcom/google/android/videos/async/AuthenticatingRequester;, "Lcom/google/android/videos/async/AuthenticatingRequester<TR;TS;TE;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/google/android/videos/async/AuthenticatingRequester;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    .line 33
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/videos/async/AuthenticatingRequester;)Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/async/AuthenticatingRequester;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/videos/async/AuthenticatingRequester;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    return-object v0
.end method


# virtual methods
.method protected canRetry(Lcom/google/android/videos/async/Request;Ljava/lang/Exception;)Z
    .locals 1
    .param p2, "exception"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Ljava/lang/Exception;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 58
    .local p0, "this":Lcom/google/android/videos/async/AuthenticatingRequester;, "Lcom/google/android/videos/async/AuthenticatingRequester<TR;TS;TE;>;"
    .local p1, "request":Lcom/google/android/videos/async/Request;, "TR;"
    const/16 v0, 0x191

    invoke-static {p2, v0}, Lcom/google/android/videos/utils/ErrorHelper;->isHttpException(Ljava/lang/Throwable;I)Z

    move-result v0

    return v0
.end method

.method protected final getAuthToken(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException;
        }
    .end annotation

    .prologue
    .line 62
    .local p0, "this":Lcom/google/android/videos/async/AuthenticatingRequester;, "Lcom/google/android/videos/async/AuthenticatingRequester<TR;TS;TE;>;"
    iget-object v0, p0, Lcom/google/android/videos/async/AuthenticatingRequester;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->blockingGetAuthToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected abstract makeAuthenticatedRequest(Lcom/google/android/videos/async/Request;Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/async/Callback",
            "<TS;TE;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation
.end method

.method public request(Lcom/google/android/videos/async/Request;Lcom/google/android/videos/async/Callback;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;)V"
        }
    .end annotation

    .prologue
    .line 37
    .local p0, "this":Lcom/google/android/videos/async/AuthenticatingRequester;, "Lcom/google/android/videos/async/AuthenticatingRequester<TR;TS;TE;>;"
    .local p1, "request":Lcom/google/android/videos/async/Request;, "TR;"
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TR;TE;>;"
    const/4 v3, 0x0

    .line 39
    .local v3, "authToken":Ljava/lang/String;
    :try_start_0
    iget-object v0, p1, Lcom/google/android/videos/async/Request;->account:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/videos/async/AuthenticatingRequester;->getAuthToken(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 47
    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p2

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/async/AuthenticatingRequester$RetryingCallback;-><init>(Lcom/google/android/videos/async/AuthenticatingRequester;Lcom/google/android/videos/async/Callback;Ljava/lang/String;Lcom/google/android/videos/async/Request;Lcom/google/android/videos/async/AuthenticatingRequester$1;)V

    invoke-virtual {p0, p1, v3, v0}, Lcom/google/android/videos/async/AuthenticatingRequester;->makeAuthenticatedRequest(Lcom/google/android/videos/async/Request;Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V
    :try_end_1
    .catch Lcom/google/android/videos/converter/ConverterException; {:try_start_1 .. :try_end_1} :catch_1

    .line 52
    :goto_0
    return-void

    .line 40
    :catch_0
    move-exception v6

    .line 41
    .local v6, "e":Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException;
    iget-boolean v0, p1, Lcom/google/android/videos/async/Request;->requireAuthentication:Z

    if-eqz v0, :cond_0

    .line 42
    invoke-interface {p2, p1, v6}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0

    .line 49
    .end local v6    # "e":Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException;
    :catch_1
    move-exception v6

    .line 50
    .local v6, "e":Lcom/google/android/videos/converter/ConverterException;
    invoke-interface {p2, p1, v6}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public bridge synthetic request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/google/android/videos/async/Callback;

    .prologue
    .line 26
    .local p0, "this":Lcom/google/android/videos/async/AuthenticatingRequester;, "Lcom/google/android/videos/async/AuthenticatingRequester<TR;TS;TE;>;"
    check-cast p1, Lcom/google/android/videos/async/Request;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/async/AuthenticatingRequester;->request(Lcom/google/android/videos/async/Request;Lcom/google/android/videos/async/Callback;)V

    return-void
.end method
