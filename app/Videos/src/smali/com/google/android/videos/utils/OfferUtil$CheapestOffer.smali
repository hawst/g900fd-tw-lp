.class public Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
.super Ljava/lang/Object;
.source "OfferUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/utils/OfferUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CheapestOffer"
.end annotation


# instance fields
.field public final offer:Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

.field public final singleOffer:Z


# direct methods
.method constructor <init>(Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;Z)V
    .locals 1
    .param p1, "offer"    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    .param p2, "singleOffer"    # Z

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    iput-object v0, p0, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->offer:Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    .line 24
    iput-boolean p2, p0, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->singleOffer:Z

    .line 25
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 49
    if-ne p1, p0, :cond_1

    .line 58
    :cond_0
    :goto_0
    return v1

    .line 53
    :cond_1
    instance-of v3, p1, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    if-nez v3, :cond_2

    move v1, v2

    .line 54
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 57
    check-cast v0, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    .line 58
    .local v0, "other":Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    iget-boolean v3, p0, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->singleOffer:Z

    iget-boolean v4, v0, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->singleOffer:Z

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->offer:Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    iget-object v4, v0, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->offer:Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getFormattedAmount(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->isFree()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0193

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->offer:Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedAmount:Ljava/lang/String;

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 41
    const/16 v0, 0x11

    .line 42
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->offer:Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 43
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->singleOffer:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    add-int v0, v2, v1

    .line 44
    return v0

    .line 43
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isFree()Z
    .locals 4

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->offer:Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    iget-wide v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->priceMicros:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHd()Z
    .locals 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->offer:Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    iget v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formatType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
