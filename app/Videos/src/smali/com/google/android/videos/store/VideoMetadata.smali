.class public Lcom/google/android/videos/store/VideoMetadata;
.super Ljava/lang/Object;
.source "VideoMetadata.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J


# instance fields
.field public final actors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final captionAvailable:Z

.field public final classification:Ljava/lang/String;

.field public final description:Ljava/lang/String;

.field public final directors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final durationSecs:I

.field public final id:Ljava/lang/String;

.field public final isRental:Z

.field public final knowledgeEnabled:Z

.field public final producers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final purchasedHd:Z

.field public final releaseYear:I

.field public final surroundSoundPurchasedAndAvailable:Z

.field public final title:Ljava/lang/String;

.field public final writers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;ZZZZZ)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "releaseYear"    # I
    .param p5, "durationSecs"    # I
    .param p10, "classification"    # Ljava/lang/String;
    .param p11, "isRental"    # Z
    .param p12, "purchasedHd"    # Z
    .param p13, "surroundSoundPurchasedAndAvailable"    # Z
    .param p14, "knowledgeEnabled"    # Z
    .param p15, "captionAvailable"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "ZZZZZ)V"
        }
    .end annotation

    .prologue
    .line 39
    .local p6, "directors":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p7, "writers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p8, "actors":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p9, "producers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/google/android/videos/store/VideoMetadata;->id:Ljava/lang/String;

    .line 41
    iput-object p2, p0, Lcom/google/android/videos/store/VideoMetadata;->title:Ljava/lang/String;

    .line 42
    iput-object p3, p0, Lcom/google/android/videos/store/VideoMetadata;->description:Ljava/lang/String;

    .line 43
    iput p4, p0, Lcom/google/android/videos/store/VideoMetadata;->releaseYear:I

    .line 44
    iput p5, p0, Lcom/google/android/videos/store/VideoMetadata;->durationSecs:I

    .line 45
    iput-object p6, p0, Lcom/google/android/videos/store/VideoMetadata;->directors:Ljava/util/List;

    .line 46
    iput-object p7, p0, Lcom/google/android/videos/store/VideoMetadata;->writers:Ljava/util/List;

    .line 47
    iput-object p8, p0, Lcom/google/android/videos/store/VideoMetadata;->actors:Ljava/util/List;

    .line 48
    iput-object p9, p0, Lcom/google/android/videos/store/VideoMetadata;->producers:Ljava/util/List;

    .line 49
    iput-object p10, p0, Lcom/google/android/videos/store/VideoMetadata;->classification:Ljava/lang/String;

    .line 50
    iput-boolean p11, p0, Lcom/google/android/videos/store/VideoMetadata;->isRental:Z

    .line 51
    iput-boolean p12, p0, Lcom/google/android/videos/store/VideoMetadata;->purchasedHd:Z

    .line 52
    iput-boolean p13, p0, Lcom/google/android/videos/store/VideoMetadata;->surroundSoundPurchasedAndAvailable:Z

    .line 53
    iput-boolean p14, p0, Lcom/google/android/videos/store/VideoMetadata;->knowledgeEnabled:Z

    .line 54
    iput-boolean p15, p0, Lcom/google/android/videos/store/VideoMetadata;->captionAvailable:Z

    .line 55
    return-void
.end method
