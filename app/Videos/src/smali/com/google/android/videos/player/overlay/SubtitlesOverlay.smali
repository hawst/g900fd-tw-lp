.class public interface abstract Lcom/google/android/videos/player/overlay/SubtitlesOverlay;
.super Ljava/lang/Object;
.source "SubtitlesOverlay.java"

# interfaces
.implements Lcom/google/android/videos/player/PlayerView$PlayerOverlay;


# virtual methods
.method public abstract clear()V
.end method

.method public abstract hide()V
.end method

.method public abstract setCaptionStyle(Lcom/google/android/exoplayer/text/CaptionStyleCompat;)V
.end method

.method public abstract setFontScale(F)V
.end method

.method public abstract update(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;",
            ">;)V"
        }
    .end annotation
.end method
