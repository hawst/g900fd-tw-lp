.class Lcom/google/android/videos/store/StoreStatusMonitor$1;
.super Ljava/lang/Object;
.source "StoreStatusMonitor.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/store/StoreStatusMonitor;-><init>(Landroid/content/Context;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/WishlistStore;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/store/StoreStatusMonitor;


# direct methods
.method constructor <init>(Lcom/google/android/videos/store/StoreStatusMonitor;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/google/android/videos/store/StoreStatusMonitor$1;->this$0:Lcom/google/android/videos/store/StoreStatusMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 94
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 86
    check-cast p1, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/store/StoreStatusMonitor$1;->onError(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .param p2, "response"    # Landroid/database/Cursor;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/videos/store/StoreStatusMonitor$1;->this$0:Lcom/google/android/videos/store/StoreStatusMonitor;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/store/StoreStatusMonitor;->processPurchasesQueryResult(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V

    .line 90
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 86
    check-cast p1, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/store/StoreStatusMonitor$1;->onResponse(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V

    return-void
.end method
