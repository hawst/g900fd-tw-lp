.class public Lcom/google/android/videos/ui/FlowAnimationUtil;
.super Ljava/lang/Object;
.source "FlowAnimationUtil.java"


# direct methods
.method public static animateFlowAppearing(Landroid/support/v7/widget/RecyclerView;Lcom/google/android/videos/flow/Flow;)V
    .locals 1
    .param p0, "view"    # Landroid/support/v7/widget/RecyclerView;
    .param p1, "flow"    # Lcom/google/android/videos/flow/Flow;

    .prologue
    .line 18
    invoke-virtual {p1}, Lcom/google/android/videos/flow/Flow;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 19
    new-instance v0, Lcom/google/android/videos/ui/FlowAnimationUtil$1;

    invoke-direct {v0, p1}, Lcom/google/android/videos/ui/FlowAnimationUtil$1;-><init>(Lcom/google/android/videos/flow/Flow;)V

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->post(Ljava/lang/Runnable;)Z

    .line 26
    :cond_0
    return-void
.end method
