.class public Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;
.super Ljava/lang/Object;
.source "PlayCommonNetworkStackWrapper.java"

# interfaces
.implements Lcom/google/android/repolib/observers/Updatable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper$1;,
        Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper$NetworkStateReceiver;
    }
.end annotation


# instance fields
.field private count:I

.field private final networkStateReceiver:Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper$NetworkStateReceiver;

.field private final networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

.field private final playCommonNetworkStack:Lcom/google/android/play/utils/PlayCommonNetworkStack;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/videos/utils/NetworkStatus;)V
    .locals 2
    .param p1, "applicationContext"    # Landroid/content/Context;
    .param p2, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p2, p0, Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 20
    new-instance v0, Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper$NetworkStateReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper$NetworkStateReceiver;-><init>(Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper$1;)V

    iput-object v0, p0, Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;->networkStateReceiver:Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper$NetworkStateReceiver;

    .line 21
    new-instance v0, Lcom/google/android/play/utils/PlayCommonNetworkStack;

    iget-object v1, p0, Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;->networkStateReceiver:Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper$NetworkStateReceiver;

    invoke-direct {v0, p1, v1}, Lcom/google/android/play/utils/PlayCommonNetworkStack;-><init>(Landroid/content/Context;Lcom/google/android/play/image/FifeUrlUtils$NetworkInfoCache;)V

    iput-object v0, p0, Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;->playCommonNetworkStack:Lcom/google/android/play/utils/PlayCommonNetworkStack;

    .line 22
    return-void
.end method


# virtual methods
.method public getPlayCommonNetworkStack()Lcom/google/android/play/utils/PlayCommonNetworkStack;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;->playCommonNetworkStack:Lcom/google/android/play/utils/PlayCommonNetworkStack;

    return-object v0
.end method

.method public declared-synchronized register()V
    .locals 2

    .prologue
    .line 25
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;->count:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;->count:I

    if-nez v0, :cond_0

    .line 26
    iget-object v0, p0, Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0, p0}, Lcom/google/android/videos/utils/NetworkStatus;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 28
    :cond_0
    monitor-exit p0

    return-void

    .line 25
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized unregister()V
    .locals 1

    .prologue
    .line 31
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;->count:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;->count:I

    if-nez v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0, p0}, Lcom/google/android/videos/utils/NetworkStatus;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34
    :cond_0
    monitor-exit p0

    return-void

    .line 31
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public update()V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;->networkStateReceiver:Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper$NetworkStateReceiver;

    iget-object v1, p0, Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v1}, Lcom/google/android/videos/utils/NetworkStatus;->getNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper$NetworkStateReceiver;->setNetworkInfo(Landroid/net/NetworkInfo;)V

    .line 43
    return-void
.end method
