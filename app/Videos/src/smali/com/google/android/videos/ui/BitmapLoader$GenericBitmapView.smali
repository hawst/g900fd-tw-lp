.class public interface abstract Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;
.super Ljava/lang/Object;
.source "BitmapLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/BitmapLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "GenericBitmapView"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<B:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract getThumbnailTag()Ljava/lang/Object;
.end method

.method public abstract setThumbnail(Ljava/lang/Object;Z)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TB;Z)V"
        }
    .end annotation
.end method

.method public abstract setThumbnailTag(Ljava/lang/Object;)V
.end method
