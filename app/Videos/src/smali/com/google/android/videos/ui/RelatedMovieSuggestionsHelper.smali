.class public Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;
.super Ljava/lang/Object;
.source "RelatedMovieSuggestionsHelper.java"

# interfaces
.implements Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;
.implements Lcom/google/android/videos/ui/SuggestionsHelper$Listener;


# instance fields
.field private final account:Ljava/lang/String;

.field private final activity:Landroid/app/Activity;

.field private final eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private lastVideoId:Ljava/lang/String;

.field private final maxSuggestions:I

.field private final recommendationsRequestFactory:Lcom/google/android/videos/api/RecommendationsRequest$Factory;

.field private final suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

.field private final suggestionsDataSource:Lcom/google/android/videos/adapter/ArrayDataSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/adapter/ArrayDataSource",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;"
        }
    .end annotation
.end field

.field private final suggestionsHelper:Lcom/google/android/videos/ui/SuggestionsHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/ui/SuggestionsHelper",
            "<",
            "Lcom/google/android/videos/api/RecommendationsRequest;",
            ">;"
        }
    .end annotation
.end field

.field private suggestionsLoaded:Z

.field private final uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/api/RecommendationsRequest$Factory;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/utils/ErrorHelper;Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;Ljava/lang/String;ILcom/google/android/videos/logging/UiEventLoggingHelper;)V
    .locals 6
    .param p1, "activity"    # Landroid/app/Activity;
    .param p3, "recommendationsRequestFactory"    # Lcom/google/android/videos/api/RecommendationsRequest$Factory;
    .param p4, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;
    .param p5, "errorHelper"    # Lcom/google/android/videos/utils/ErrorHelper;
    .param p6, "suggestionOverflowMenuHelper"    # Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;
    .param p7, "account"    # Ljava/lang/String;
    .param p8, "maxSuggestions"    # I
    .param p9, "uiEventLoggingHelper"    # Lcom/google/android/videos/logging/UiEventLoggingHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/RecommendationsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;",
            ">;",
            "Lcom/google/android/videos/api/RecommendationsRequest$Factory;",
            "Lcom/google/android/videos/logging/EventLogger;",
            "Lcom/google/android/videos/utils/ErrorHelper;",
            "Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;",
            "Ljava/lang/String;",
            "I",
            "Lcom/google/android/videos/logging/UiEventLoggingHelper;",
            ")V"
        }
    .end annotation

    .prologue
    .line 55
    .local p2, "suggestionsRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/RecommendationsRequest;Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->suggestionsLoaded:Z

    .line 56
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->activity:Landroid/app/Activity;

    .line 57
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/api/RecommendationsRequest$Factory;

    iput-object v0, p0, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->recommendationsRequestFactory:Lcom/google/android/videos/api/RecommendationsRequest$Factory;

    .line 59
    invoke-static {p9}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/logging/UiEventLoggingHelper;

    iput-object v0, p0, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    .line 60
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/logging/EventLogger;

    iput-object v0, p0, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 61
    invoke-static {p6}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    iput-object v0, p0, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    .line 62
    invoke-static {p7}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->account:Ljava/lang/String;

    .line 63
    iput p8, p0, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->maxSuggestions:I

    .line 65
    new-instance v0, Lcom/google/android/videos/adapter/ArrayDataSource;

    invoke-direct {v0}, Lcom/google/android/videos/adapter/ArrayDataSource;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->suggestionsDataSource:Lcom/google/android/videos/adapter/ArrayDataSource;

    .line 66
    new-instance v0, Lcom/google/android/videos/ui/SuggestionsHelper;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p0

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/ui/SuggestionsHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/ui/SuggestionsHelper$Listener;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/utils/ErrorHelper;)V

    iput-object v0, p0, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->suggestionsHelper:Lcom/google/android/videos/ui/SuggestionsHelper;

    .line 72
    return-void
.end method


# virtual methods
.method public getSuggestionsDataSource()Lcom/google/android/videos/adapter/ArrayDataSource;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/adapter/ArrayDataSource",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->suggestionsDataSource:Lcom/google/android/videos/adapter/ArrayDataSource;

    return-object v0
.end method

.method public final init(Ljava/lang/String;)V
    .locals 5
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 79
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 80
    iget-object v0, p0, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->lastVideoId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->suggestionsLoaded:Z

    if-eqz v0, :cond_0

    .line 90
    :goto_0
    return-void

    .line 83
    :cond_0
    iput-object p1, p0, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->lastVideoId:Ljava/lang/String;

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->suggestionsLoaded:Z

    .line 85
    iget-object v0, p0, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->suggestionsDataSource:Lcom/google/android/videos/adapter/ArrayDataSource;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/ArrayDataSource;->updateArray([Ljava/lang/Object;)V

    .line 86
    iget-object v0, p0, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->suggestionsHelper:Lcom/google/android/videos/ui/SuggestionsHelper;

    iget-object v1, p0, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->recommendationsRequestFactory:Lcom/google/android/videos/api/RecommendationsRequest$Factory;

    iget-object v2, p0, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->account:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->maxSuggestions:I

    const/16 v4, 0x1b

    invoke-interface {v1, v2, p1, v3, v4}, Lcom/google/android/videos/api/RecommendationsRequest$Factory;->createForRelatedMovies(Ljava/lang/String;Ljava/lang/String;II)Lcom/google/android/videos/api/RecommendationsRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/SuggestionsHelper;->init(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onItemClick(Lcom/google/android/videos/adapter/DataSource;ILandroid/view/View;)V
    .locals 5
    .param p2, "position"    # I
    .param p3, "view"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/adapter/DataSource",
            "<*>;I",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 115
    .local p1, "dataSource":Lcom/google/android/videos/adapter/DataSource;, "Lcom/google/android/videos/adapter/DataSource<*>;"
    iget-object v1, p0, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->suggestionsDataSource:Lcom/google/android/videos/adapter/ArrayDataSource;

    invoke-virtual {v1, p2}, Lcom/google/android/videos/adapter/ArrayDataSource;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 116
    .local v0, "selectedItem":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    invoke-virtual {p3}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f0f00d2

    if-ne v1, v2, :cond_0

    .line 117
    iget-object v1, p0, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    invoke-virtual {v1, v0, p3}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->showSuggestionMenu(Lcom/google/wireless/android/video/magma/proto/AssetResource;Landroid/view/View;)V

    .line 123
    :goto_0
    return-void

    .line 119
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    invoke-static {p3}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->findUiElementNode(Landroid/view/View;)Lcom/google/android/videos/logging/UiElementNode;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->sendClickEvent(Lcom/google/android/videos/logging/UiElementNode;)V

    .line 120
    iget-object v1, p0, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->activity:Landroid/app/Activity;

    iget-object v2, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v2, v2, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->account:Ljava/lang/String;

    const/4 v4, 0x6

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/videos/utils/PlayStoreUtil;->viewMovieDetails(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public onSuggestionsAvailable([Lcom/google/wireless/android/video/magma/proto/AssetResource;)V
    .locals 1
    .param p1, "suggestions"    # [Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    .line 104
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->suggestionsLoaded:Z

    .line 105
    iget-object v0, p0, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->suggestionsDataSource:Lcom/google/android/videos/adapter/ArrayDataSource;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/adapter/ArrayDataSource;->updateArray([Ljava/lang/Object;)V

    .line 106
    return-void
.end method

.method public onSuggestionsError(Ljava/lang/String;)V
    .locals 0
    .param p1, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 111
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->suggestionsHelper:Lcom/google/android/videos/ui/SuggestionsHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/SuggestionsHelper;->reset()V

    .line 100
    return-void
.end method
