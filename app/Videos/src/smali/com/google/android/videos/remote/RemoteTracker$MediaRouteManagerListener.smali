.class Lcom/google/android/videos/remote/RemoteTracker$MediaRouteManagerListener;
.super Ljava/lang/Object;
.source "RemoteTracker.java"

# interfaces
.implements Lcom/google/android/videos/remote/MediaRouteManager$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/remote/RemoteTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MediaRouteManagerListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/remote/RemoteTracker;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/remote/RemoteTracker;)V
    .locals 0

    .prologue
    .line 480
    iput-object p1, p0, Lcom/google/android/videos/remote/RemoteTracker$MediaRouteManagerListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/remote/RemoteTracker;Lcom/google/android/videos/remote/RemoteTracker$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/remote/RemoteTracker;
    .param p2, "x1"    # Lcom/google/android/videos/remote/RemoteTracker$1;

    .prologue
    .line 480
    invoke-direct {p0, p1}, Lcom/google/android/videos/remote/RemoteTracker$MediaRouteManagerListener;-><init>(Lcom/google/android/videos/remote/RemoteTracker;)V

    return-void
.end method


# virtual methods
.method public onRemoteControlSelected(Lcom/google/android/videos/remote/RemoteControl;)V
    .locals 2
    .param p1, "remoteControl"    # Lcom/google/android/videos/remote/RemoteControl;

    .prologue
    .line 483
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker$MediaRouteManagerListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # getter for: Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;
    invoke-static {v0}, Lcom/google/android/videos/remote/RemoteTracker;->access$600(Lcom/google/android/videos/remote/RemoteTracker;)Lcom/google/android/videos/remote/RemoteControl;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 484
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker$MediaRouteManagerListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # getter for: Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;
    invoke-static {v0}, Lcom/google/android/videos/remote/RemoteTracker;->access$600(Lcom/google/android/videos/remote/RemoteTracker;)Lcom/google/android/videos/remote/RemoteControl;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteTracker$MediaRouteManagerListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # getter for: Lcom/google/android/videos/remote/RemoteTracker;->controlListener:Lcom/google/android/videos/remote/RemoteTracker$ControlListener;
    invoke-static {v1}, Lcom/google/android/videos/remote/RemoteTracker;->access$700(Lcom/google/android/videos/remote/RemoteTracker;)Lcom/google/android/videos/remote/RemoteTracker$ControlListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/remote/RemoteControl;->removeListener(Lcom/google/android/videos/remote/RemoteControlListener;)V

    .line 485
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker$MediaRouteManagerListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # invokes: Lcom/google/android/videos/remote/RemoteTracker;->maybeCacheResumeTimestamp()V
    invoke-static {v0}, Lcom/google/android/videos/remote/RemoteTracker;->access$800(Lcom/google/android/videos/remote/RemoteTracker;)V

    .line 486
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker$MediaRouteManagerListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/videos/remote/RemoteTracker;->flushResumeTimestampIfDirty(Z)V
    invoke-static {v0, v1}, Lcom/google/android/videos/remote/RemoteTracker;->access$900(Lcom/google/android/videos/remote/RemoteTracker;Z)V

    .line 488
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker$MediaRouteManagerListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # setter for: Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;
    invoke-static {v0, p1}, Lcom/google/android/videos/remote/RemoteTracker;->access$602(Lcom/google/android/videos/remote/RemoteTracker;Lcom/google/android/videos/remote/RemoteControl;)Lcom/google/android/videos/remote/RemoteControl;

    .line 489
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker$MediaRouteManagerListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # getter for: Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;
    invoke-static {v0}, Lcom/google/android/videos/remote/RemoteTracker;->access$600(Lcom/google/android/videos/remote/RemoteTracker;)Lcom/google/android/videos/remote/RemoteControl;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 490
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker$MediaRouteManagerListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # getter for: Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;
    invoke-static {v0}, Lcom/google/android/videos/remote/RemoteTracker;->access$600(Lcom/google/android/videos/remote/RemoteTracker;)Lcom/google/android/videos/remote/RemoteControl;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteTracker$MediaRouteManagerListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # getter for: Lcom/google/android/videos/remote/RemoteTracker;->controlListener:Lcom/google/android/videos/remote/RemoteTracker$ControlListener;
    invoke-static {v1}, Lcom/google/android/videos/remote/RemoteTracker;->access$700(Lcom/google/android/videos/remote/RemoteTracker;)Lcom/google/android/videos/remote/RemoteTracker$ControlListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/remote/RemoteControl;->addListener(Lcom/google/android/videos/remote/RemoteControlListener;)V

    .line 493
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker$MediaRouteManagerListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # invokes: Lcom/google/android/videos/remote/RemoteTracker;->updateAll()V
    invoke-static {v0}, Lcom/google/android/videos/remote/RemoteTracker;->access$1000(Lcom/google/android/videos/remote/RemoteTracker;)V

    .line 494
    return-void
.end method
