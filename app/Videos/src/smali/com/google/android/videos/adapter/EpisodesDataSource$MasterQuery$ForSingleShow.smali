.class public abstract Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForSingleShow;
.super Ljava/lang/Object;
.source "EpisodesDataSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ForSingleShow"
.end annotation


# static fields
.field private static final PROJECTION:[Ljava/lang/String;

.field private static final SUB_REQUEST_CREATOR:Lcom/google/android/videos/store/PurchaseStore$SubRequestCreator;

.field private static final SUB_REQUEST_CREATOR_NO_UNPURCHASED:Lcom/google/android/videos/store/PurchaseStore$SubRequestCreator;

.field private static final SUB_REQUEST_CREATOR_PINNED:Lcom/google/android/videos/store/PurchaseStore$SubRequestCreator;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 382
    sget-object v0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery;->BASE_PROJECTION:[Ljava/lang/String;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "-1"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "assets_id"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/DbUtils;->extend([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForSingleShow;->PROJECTION:[Ljava/lang/String;

    .line 398
    new-instance v0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForSingleShow$1;

    invoke-direct {v0}, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForSingleShow$1;-><init>()V

    sput-object v0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForSingleShow;->SUB_REQUEST_CREATOR:Lcom/google/android/videos/store/PurchaseStore$SubRequestCreator;

    .line 412
    new-instance v0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForSingleShow$2;

    invoke-direct {v0}, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForSingleShow$2;-><init>()V

    sput-object v0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForSingleShow;->SUB_REQUEST_CREATOR_PINNED:Lcom/google/android/videos/store/PurchaseStore$SubRequestCreator;

    .line 422
    new-instance v0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForSingleShow$3;

    invoke-direct {v0}, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForSingleShow$3;-><init>()V

    sput-object v0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForSingleShow;->SUB_REQUEST_CREATOR_NO_UNPURCHASED:Lcom/google/android/videos/store/PurchaseStore$SubRequestCreator;

    return-void
.end method

.method public static createRequest(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 11
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "showId"    # Ljava/lang/String;
    .param p2, "allowUnpurchased"    # Z
    .param p3, "downloadedOnly"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 434
    const/4 v0, 0x2

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v0

    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v1

    .line 438
    .local v6, "whereArgs":[Ljava/lang/String;
    if-eqz p3, :cond_0

    const-string v5, "user_assets_account = ? AND user_assets_id = ? AND (pinned IS NOT NULL AND pinned > 0)"

    .line 440
    .local v5, "where":Ljava/lang/String;
    :goto_0
    if-eqz p3, :cond_1

    .line 441
    sget-object v10, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForSingleShow;->SUB_REQUEST_CREATOR_PINNED:Lcom/google/android/videos/store/PurchaseStore$SubRequestCreator;

    .line 447
    .local v10, "subRequestCreator":Lcom/google/android/videos/store/PurchaseStore$SubRequestCreator;
    :goto_1
    new-instance v0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    const-string v2, "user_assets, shows ON user_assets_type = 18 AND user_assets_id = shows_id, purchased_assets ON account = user_assets_account AND asset_type = 20 AND root_asset_id = user_assets_id, assets ON root_id = user_assets_id AND assets_type = 19"

    sget-object v3, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForSingleShow;->PROJECTION:[Ljava/lang/String;

    const-string v8, "season_seqno"

    const/4 v9, -0x1

    move-object v7, v4

    invoke-direct/range {v0 .. v10}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/videos/store/PurchaseStore$SubRequestCreator;)V

    return-object v0

    .line 438
    .end local v5    # "where":Ljava/lang/String;
    .end local v10    # "subRequestCreator":Lcom/google/android/videos/store/PurchaseStore$SubRequestCreator;
    :cond_0
    const-string v5, "user_assets_account = ? AND user_assets_id = ?"

    goto :goto_0

    .line 442
    .restart local v5    # "where":Ljava/lang/String;
    :cond_1
    if-eqz p2, :cond_2

    .line 443
    sget-object v10, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForSingleShow;->SUB_REQUEST_CREATOR:Lcom/google/android/videos/store/PurchaseStore$SubRequestCreator;

    .restart local v10    # "subRequestCreator":Lcom/google/android/videos/store/PurchaseStore$SubRequestCreator;
    goto :goto_1

    .line 445
    .end local v10    # "subRequestCreator":Lcom/google/android/videos/store/PurchaseStore$SubRequestCreator;
    :cond_2
    sget-object v10, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForSingleShow;->SUB_REQUEST_CREATOR_NO_UNPURCHASED:Lcom/google/android/videos/store/PurchaseStore$SubRequestCreator;

    .restart local v10    # "subRequestCreator":Lcom/google/android/videos/store/PurchaseStore$SubRequestCreator;
    goto :goto_1
.end method
