.class Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;
.super Lcom/google/android/recline/app/PlaybackControlGlue;
.source "PanoControllerOverlay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/playback/PanoControllerOverlay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PlaybackControlGlue"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;


# direct methods
.method constructor <init>(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)V
    .locals 3

    .prologue
    .line 665
    iput-object p1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    .line 666
    # getter for: Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->activity:Landroid/support/v4/app/FragmentActivity;
    invoke-static {p1}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->access$500(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    # getter for: Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->overlayFragment:Lcom/google/android/recline/app/PlaybackOverlayFragment;
    invoke-static {p1}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->access$600(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)Lcom/google/android/recline/app/PlaybackOverlayFragment;

    move-result-object v1

    # getter for: Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->speedUpFactors:[I
    invoke-static {p1}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->access$700(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)[I

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/recline/app/PlaybackControlGlue;-><init>(Landroid/content/Context;Lcom/google/android/recline/app/PlaybackOverlayFragment;[I)V

    .line 667
    return-void
.end method


# virtual methods
.method public getCurrentPosition()I
    .locals 1

    .prologue
    .line 676
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    # getter for: Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->currentTimeMillis:I
    invoke-static {v0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->access$800(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)I

    move-result v0

    return v0
.end method

.method public getCurrentSpeedId()I
    .locals 1

    .prologue
    .line 681
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    # getter for: Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->currentPlaybackSpeedId:I
    invoke-static {v0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->access$900(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)I

    move-result v0

    return v0
.end method

.method public getMediaArt()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 686
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaDuration()I
    .locals 1

    .prologue
    .line 691
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    # getter for: Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->totalTimeMillis:I
    invoke-static {v0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->access$1000(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)I

    move-result v0

    return v0
.end method

.method public getSupportedActions()J
    .locals 2

    .prologue
    .line 706
    const-wide/16 v0, 0xe0

    return-wide v0
.end method

.method public hasValidMedia()Z
    .locals 1

    .prologue
    .line 711
    const/4 v0, 0x1

    return v0
.end method

.method protected onRowChanged(Lcom/google/android/recline/widget/PlaybackControlsRow;)V
    .locals 0
    .param p1, "arg0"    # Lcom/google/android/recline/widget/PlaybackControlsRow;

    .prologue
    .line 722
    return-void
.end method

.method public onStateChanged()V
    .locals 0

    .prologue
    .line 671
    invoke-super {p0}, Lcom/google/android/recline/app/PlaybackControlGlue;->onStateChanged()V

    .line 672
    return-void
.end method

.method protected pausePlayback()V
    .locals 2

    .prologue
    .line 726
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->currentPlaybackSpeedId:I
    invoke-static {v0, v1}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->access$902(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;I)I

    .line 727
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    # getter for: Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;
    invoke-static {v0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->access$1200(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;->onPause()V

    .line 728
    return-void
.end method

.method protected skipToNext()V
    .locals 0

    .prologue
    .line 733
    return-void
.end method

.method protected skipToPrevious()V
    .locals 0

    .prologue
    .line 738
    return-void
.end method

.method protected startPlayback(I)V
    .locals 9
    .param p1, "speed"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v4, 0x1

    .line 742
    iget-object v5, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    # setter for: Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->currentPlaybackSpeedId:I
    invoke-static {v5, p1}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->access$902(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;I)I

    .line 743
    if-lez p1, :cond_1

    move v1, v4

    .line 746
    .local v1, "direction":I
    :goto_0
    if-ne p1, v4, :cond_2

    .line 747
    const/4 v3, 0x1

    .line 771
    .local v3, "newSpeedUp":I
    :goto_1
    iget-object v5, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    # setter for: Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->speedUp:I
    invoke-static {v5, v3}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->access$1302(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;I)I

    .line 773
    iget-object v5, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    # getter for: Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->speedUp:I
    invoke-static {v5}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->access$1300(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)I

    move-result v5

    if-eq v5, v4, :cond_4

    iget-object v5, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    # getter for: Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->isSeeking:Z
    invoke-static {v5}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->access$1400(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 774
    iget-object v5, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    # setter for: Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->isSeeking:Z
    invoke-static {v5, v4}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->access$1402(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;Z)Z

    .line 775
    iget-object v4, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    # getter for: Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;
    invoke-static {v4}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->access$1200(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    move-result-object v4

    invoke-interface {v4, v8}, Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;->onScrubbingStart(Z)V

    .line 776
    iget-object v4, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    # getter for: Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->handler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->access$400(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)Landroid/os/Handler;

    move-result-object v4

    const-wide/16 v6, 0x15e

    invoke-virtual {v4, v8, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 783
    :cond_0
    :goto_2
    return-void

    .line 743
    .end local v1    # "direction":I
    .end local v3    # "newSpeedUp":I
    :cond_1
    const/4 v1, -0x1

    goto :goto_0

    .line 750
    .restart local v1    # "direction":I
    :cond_2
    if-lez p1, :cond_3

    move v0, p1

    .line 751
    .local v0, "absSpeed":I
    :goto_3
    packed-switch v0, :pswitch_data_0

    .line 765
    const/4 v2, 0x1

    .line 768
    .local v2, "magnitude":I
    :goto_4
    mul-int v3, v1, v2

    .restart local v3    # "newSpeedUp":I
    goto :goto_1

    .line 750
    .end local v0    # "absSpeed":I
    .end local v2    # "magnitude":I
    .end local v3    # "newSpeedUp":I
    :cond_3
    neg-int v0, p1

    goto :goto_3

    .line 753
    .restart local v0    # "absSpeed":I
    :pswitch_0
    iget-object v5, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    # getter for: Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->speedUpFactors:[I
    invoke-static {v5}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->access$700(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)[I

    move-result-object v5

    aget v2, v5, v8

    .line 754
    .restart local v2    # "magnitude":I
    goto :goto_4

    .line 756
    .end local v2    # "magnitude":I
    :pswitch_1
    iget-object v5, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    # getter for: Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->speedUpFactors:[I
    invoke-static {v5}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->access$700(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)[I

    move-result-object v5

    aget v2, v5, v4

    .line 757
    .restart local v2    # "magnitude":I
    goto :goto_4

    .line 759
    .end local v2    # "magnitude":I
    :pswitch_2
    iget-object v5, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    # getter for: Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->speedUpFactors:[I
    invoke-static {v5}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->access$700(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)[I

    move-result-object v5

    const/4 v6, 0x2

    aget v2, v5, v6

    .line 760
    .restart local v2    # "magnitude":I
    goto :goto_4

    .line 762
    .end local v2    # "magnitude":I
    :pswitch_3
    iget-object v5, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    # getter for: Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->speedUpFactors:[I
    invoke-static {v5}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->access$700(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)[I

    move-result-object v5

    const/4 v6, 0x3

    aget v2, v5, v6

    .line 763
    .restart local v2    # "magnitude":I
    goto :goto_4

    .line 777
    .end local v0    # "absSpeed":I
    .end local v2    # "magnitude":I
    .restart local v3    # "newSpeedUp":I
    :cond_4
    iget-object v5, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    # getter for: Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->speedUp:I
    invoke-static {v5}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->access$1300(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)I

    move-result v5

    if-ne v5, v4, :cond_0

    .line 778
    iget-object v5, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    # setter for: Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->isSeeking:Z
    invoke-static {v5, v8}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->access$1402(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;Z)Z

    .line 779
    iget-object v5, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    # getter for: Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;
    invoke-static {v5}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->access$1200(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    # getter for: Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->currentTimeMillis:I
    invoke-static {v6}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->access$800(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)I

    move-result v6

    invoke-interface {v5, v8, v6, v4}, Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;->onSeekTo(ZIZ)V

    .line 780
    iget-object v4, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    # getter for: Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->handler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->access$400(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 781
    iget-object v4, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    # getter for: Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;
    invoke-static {v4}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->access$1200(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;->onPlay()V

    goto :goto_2

    .line 751
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
