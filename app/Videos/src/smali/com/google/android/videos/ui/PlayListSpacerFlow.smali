.class public Lcom/google/android/videos/ui/PlayListSpacerFlow;
.super Lcom/google/android/videos/flow/SingleViewFlow;
.source "PlayListSpacerFlow.java"


# instance fields
.field private final contentGapInHalfCards:I

.field private final headerMode:I


# direct methods
.method public constructor <init>(II)V
    .locals 1
    .param p1, "headerMode"    # I
    .param p2, "contentGapInHalfCards"    # I

    .prologue
    .line 19
    const v0, 0x7f04009d

    invoke-direct {p0, v0}, Lcom/google/android/videos/flow/SingleViewFlow;-><init>(I)V

    .line 20
    iput p1, p0, Lcom/google/android/videos/ui/PlayListSpacerFlow;->headerMode:I

    .line 21
    iput p2, p0, Lcom/google/android/videos/ui/PlayListSpacerFlow;->contentGapInHalfCards:I

    .line 22
    return-void
.end method

.method public static getHeaderContentGap(Landroid/content/Context;I)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "contentGapInHalfCards"    # I

    .prologue
    .line 39
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e00f9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    mul-int/2addr v0, p1

    return v0
.end method

.method public static getSpacerHeight(Landroid/content/Context;II)I
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "tabMode"    # I
    .param p2, "contentGapInHalfCards"    # I

    .prologue
    .line 34
    invoke-static {p0, p2}, Lcom/google/android/videos/ui/PlayListSpacerFlow;->getHeaderContentGap(Landroid/content/Context;I)I

    move-result v0

    invoke-static {p0, p1, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getMinimumHeaderHeight(Landroid/content/Context;II)I

    move-result v0

    return v0
.end method


# virtual methods
.method public createViewHolder(ILandroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 5
    .param p1, "viewType"    # I
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 26
    invoke-super {p0, p1, p2}, Lcom/google/android/videos/flow/SingleViewFlow;->createViewHolder(ILandroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v1

    .line 27
    .local v1, "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    iget-object v2, v1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    .line 28
    .local v0, "lp":Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Lcom/google/android/videos/ui/PlayListSpacerFlow;->headerMode:I

    iget v4, p0, Lcom/google/android/videos/ui/PlayListSpacerFlow;->contentGapInHalfCards:I

    invoke-static {v2, v3, v4}, Lcom/google/android/videos/ui/PlayListSpacerFlow;->getSpacerHeight(Landroid/content/Context;II)I

    move-result v2

    invoke-static {v2}, Lcom/google/android/play/utils/Compound;->intLengthToCompound(I)I

    move-result v2

    iput v2, v0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->heightCompound:I

    .line 30
    return-object v1
.end method
