.class public Lcom/google/android/videos/pinning/NotificationsCallbackService;
.super Landroid/app/IntentService;
.source "NotificationsCallbackService.java"


# instance fields
.field private downloadNotificationManager:Lcom/google/android/videos/pinning/DownloadNotificationManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/google/android/videos/pinning/NotificationsCallbackService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 43
    return-void
.end method

.method private static buildDataUri(Ljava/lang/String;Ljava/lang/String;Z)Landroid/net/Uri;
    .locals 3
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "isShowId"    # Z

    .prologue
    .line 235
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    const-string v2, "videos"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 236
    .local v0, "builder":Landroid/net/Uri$Builder;
    if-eqz p2, :cond_0

    const-string v1, "show"

    :goto_0
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 237
    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 238
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1

    .line 236
    :cond_0
    const-string v1, "video"

    goto :goto_0
.end method

.method public static createCompletedDeletedPendingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p3, "seasonId"    # Ljava/lang/String;
    .param p4, "showId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/app/PendingIntent;"
        }
    .end annotation

    .prologue
    .local p2, "videoIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 195
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v0, v1

    .line 196
    .local v0, "id":Ljava/lang/String;
    :goto_0
    const-string v1, "com.google.android.videos.COMPLETED_DELETED"

    invoke-static {p0, v1}, Lcom/google/android/videos/pinning/NotificationsCallbackService;->createIntentForAction(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-static {p1, v0, v1}, Lcom/google/android/videos/pinning/NotificationsCallbackService;->buildDataUri(Ljava/lang/String;Ljava/lang/String;Z)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "video_ids"

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {p2, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "season_id"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "show_id"

    invoke-virtual {v1, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/android/videos/pinning/NotificationsCallbackService;->createServicePendingIntent(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1

    .end local v0    # "id":Ljava/lang/String;
    :cond_0
    move-object v0, p4

    .line 195
    goto :goto_0

    .restart local v0    # "id":Ljava/lang/String;
    :cond_1
    move v1, v2

    .line 196
    goto :goto_1
.end method

.method public static createCompletedPendingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p3, "seasonId"    # Ljava/lang/String;
    .param p4, "showId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/app/PendingIntent;"
        }
    .end annotation

    .prologue
    .local p2, "videoIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 181
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v0, v1

    .line 182
    .local v0, "id":Ljava/lang/String;
    :goto_0
    const-string v1, "com.google.android.videos.COMPLETED"

    invoke-static {p0, v1}, Lcom/google/android/videos/pinning/NotificationsCallbackService;->createIntentForAction(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-static {p1, v0, v1}, Lcom/google/android/videos/pinning/NotificationsCallbackService;->buildDataUri(Ljava/lang/String;Ljava/lang/String;Z)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "video_ids"

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {p2, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "season_id"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "show_id"

    invoke-virtual {v1, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/android/videos/pinning/NotificationsCallbackService;->createServicePendingIntent(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1

    .end local v0    # "id":Ljava/lang/String;
    :cond_0
    move-object v0, p4

    .line 181
    goto :goto_0

    .restart local v0    # "id":Ljava/lang/String;
    :cond_1
    move v1, v2

    .line 182
    goto :goto_1
.end method

.method public static createDownloadingOngoingPendingIntentForVideo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "seasonId"    # Ljava/lang/String;
    .param p4, "showId"    # Ljava/lang/String;

    .prologue
    .line 149
    const-string v0, "com.google.android.videos.DOWNLOAD_ONGOING"

    invoke-static {p0, v0}, Lcom/google/android/videos/pinning/NotificationsCallbackService;->createIntentForAction(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "authAccount"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "video_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "season_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "show_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/videos/pinning/NotificationsCallbackService;->createServicePendingIntent(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public static createErrorDeletedPendingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "seasonId"    # Ljava/lang/String;
    .param p4, "showId"    # Ljava/lang/String;

    .prologue
    .line 215
    const-string v0, "com.google.android.videos.ERROR_DELETED"

    invoke-static {p0, v0}, Lcom/google/android/videos/pinning/NotificationsCallbackService;->createIntentForAction(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p1, p2, v1}, Lcom/google/android/videos/pinning/NotificationsCallbackService;->buildDataUri(Ljava/lang/String;Ljava/lang/String;Z)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "season_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "show_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/videos/pinning/NotificationsCallbackService;->createServicePendingIntent(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public static createErrorPendingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "seasonId"    # Ljava/lang/String;
    .param p4, "showId"    # Ljava/lang/String;

    .prologue
    .line 206
    const-string v0, "com.google.android.videos.ERROR"

    invoke-static {p0, v0}, Lcom/google/android/videos/pinning/NotificationsCallbackService;->createIntentForAction(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p1, p2, v1}, Lcom/google/android/videos/pinning/NotificationsCallbackService;->buildDataUri(Ljava/lang/String;Ljava/lang/String;Z)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "season_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "show_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/videos/pinning/NotificationsCallbackService;->createServicePendingIntent(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private static createIntentForAction(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 231
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/videos/pinning/NotificationsCallbackService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static createPendingDeletedPendingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p3, "seasonId"    # Ljava/lang/String;
    .param p4, "showId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/app/PendingIntent;"
        }
    .end annotation

    .prologue
    .line 169
    .local p2, "videoIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/videos/pinning/NotificationsCallbackService;->createCompletedDeletedPendingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public static createPendingPendingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p3, "seasonId"    # Ljava/lang/String;
    .param p4, "showId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/app/PendingIntent;"
        }
    .end annotation

    .prologue
    .line 161
    .local p2, "videoIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/videos/pinning/NotificationsCallbackService;->createCompletedPendingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private static createServicePendingIntent(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 227
    const/4 v0, 0x0

    const/high16 v1, 0x8000000

    invoke-static {p0, v0, p1, v1}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private toDeepLinking(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "seasonId"    # Ljava/lang/String;
    .param p4, "showId"    # Ljava/lang/String;
    .param p5, "pinningErrorDialog"    # Z

    .prologue
    const/high16 v5, 0x10000000

    .line 116
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 118
    :cond_0
    invoke-static {p0, p1, v5}, Lcom/google/android/videos/activity/LauncherActivity;->createManageDownloadsDeepLinkingIntent(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/pinning/NotificationsCallbackService;->startActivity(Landroid/content/Intent;)V

    .line 141
    :goto_0
    return-void

    .line 120
    :cond_1
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 121
    if-eqz p5, :cond_2

    invoke-static {p0, p1, p2, v5}, Lcom/google/android/videos/activity/LauncherActivity;->createMovieDeepLinkingIntentWithPinningError(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v6

    .line 126
    .local v6, "intent":Landroid/content/Intent;
    :goto_1
    invoke-virtual {p0, v6}, Lcom/google/android/videos/pinning/NotificationsCallbackService;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 121
    .end local v6    # "intent":Landroid/content/Intent;
    :cond_2
    invoke-static {p0, p1, p2, v5}, Lcom/google/android/videos/activity/LauncherActivity;->createMovieDeepLinkingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v6

    goto :goto_1

    .line 127
    :cond_3
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 128
    if-eqz p5, :cond_4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p4

    move-object v3, p3

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/activity/LauncherActivity;->createEpisodeDeepLinkingIntentWithPinningError(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v6

    .line 133
    .restart local v6    # "intent":Landroid/content/Intent;
    :goto_2
    invoke-virtual {p0, v6}, Lcom/google/android/videos/pinning/NotificationsCallbackService;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .end local v6    # "intent":Landroid/content/Intent;
    :cond_4
    move-object v0, p0

    move-object v1, p1

    move-object v2, p4

    move-object v3, p3

    move-object v4, p2

    .line 128
    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/activity/LauncherActivity;->createEpisodeDeepLinkingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v6

    goto :goto_2

    .line 134
    :cond_5
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 135
    invoke-static {p0, p1, p4, p3, v5}, Lcom/google/android/videos/activity/LauncherActivity;->createSeasonDeepLinkingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/pinning/NotificationsCallbackService;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 138
    :cond_6
    invoke-static {p0, p1, p4, v5}, Lcom/google/android/videos/activity/LauncherActivity;->createShowDeepLinkingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/pinning/NotificationsCallbackService;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method public onCreate()V
    .locals 2

    .prologue
    .line 47
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 48
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v0

    .line 49
    .local v0, "videosGlobals":Lcom/google/android/videos/VideosGlobals;
    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getDownloadNotificationManager()Lcom/google/android/videos/pinning/DownloadNotificationManager;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/pinning/NotificationsCallbackService;->downloadNotificationManager:Lcom/google/android/videos/pinning/DownloadNotificationManager;

    .line 50
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 11
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v10, 0x1

    const/4 v5, 0x0

    .line 54
    if-eqz p1, :cond_0

    .line 55
    const-string v0, "season_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 56
    .local v3, "seasonId":Ljava/lang/String;
    const-string v0, "show_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 57
    .local v4, "showId":Ljava/lang/String;
    const-string v0, "com.google.android.videos.DOWNLOAD_ONGOING"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 58
    const-string v0, "authAccount"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 59
    .local v1, "account":Ljava/lang/String;
    const-string v0, "video_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .local v2, "videoId":Ljava/lang/String;
    move-object v0, p0

    .line 60
    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/pinning/NotificationsCallbackService;->toDeepLinking(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 112
    .end local v1    # "account":Ljava/lang/String;
    .end local v2    # "videoId":Ljava/lang/String;
    .end local v3    # "seasonId":Ljava/lang/String;
    .end local v4    # "showId":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 61
    .restart local v3    # "seasonId":Ljava/lang/String;
    .restart local v4    # "showId":Ljava/lang/String;
    :cond_1
    const-string v0, "com.google.android.videos.COMPLETED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 62
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v7

    .line 63
    .local v7, "uri":Landroid/net/Uri;
    if-eqz v7, :cond_5

    const-string v0, "videos"

    invoke-virtual {v7}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 64
    invoke-virtual {v7}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    .line 65
    .restart local v1    # "account":Ljava/lang/String;
    const-string v0, "video_ids"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 66
    .local v8, "videoIds":[Ljava/lang/String;
    if-eqz v8, :cond_2

    array-length v0, v8

    if-nez v0, :cond_3

    .line 67
    :cond_2
    const-string v0, "Invalid intent: videoIds is empty"

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 69
    :cond_3
    array-length v0, v8

    if-ne v0, v10, :cond_4

    aget-object v2, v8, v5

    .line 70
    .restart local v2    # "videoId":Ljava/lang/String;
    :goto_1
    iget-object v0, p0, Lcom/google/android/videos/pinning/NotificationsCallbackService;->downloadNotificationManager:Lcom/google/android/videos/pinning/DownloadNotificationManager;

    invoke-virtual {v0, v1, v8}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->dismissCompletedNotification(Ljava/lang/String;[Ljava/lang/String;)V

    move-object v0, p0

    .line 71
    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/pinning/NotificationsCallbackService;->toDeepLinking(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 69
    .end local v2    # "videoId":Ljava/lang/String;
    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    .line 74
    .end local v1    # "account":Ljava/lang/String;
    .end local v8    # "videoIds":[Ljava/lang/String;
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not parse URI: "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 76
    .end local v7    # "uri":Landroid/net/Uri;
    :cond_6
    const-string v0, "com.google.android.videos.COMPLETED_DELETED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 77
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v7

    .line 78
    .restart local v7    # "uri":Landroid/net/Uri;
    if-eqz v7, :cond_9

    const-string v0, "videos"

    invoke-virtual {v7}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 79
    invoke-virtual {v7}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    .line 80
    .restart local v1    # "account":Ljava/lang/String;
    const-string v0, "video_ids"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 81
    .restart local v8    # "videoIds":[Ljava/lang/String;
    if-eqz v8, :cond_7

    array-length v0, v8

    if-nez v0, :cond_8

    .line 82
    :cond_7
    const-string v0, "Invalid intent: videoIds is empty"

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 84
    :cond_8
    iget-object v0, p0, Lcom/google/android/videos/pinning/NotificationsCallbackService;->downloadNotificationManager:Lcom/google/android/videos/pinning/DownloadNotificationManager;

    invoke-virtual {v0, v1, v8}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->dismissCompletedNotification(Ljava/lang/String;[Ljava/lang/String;)V

    goto/16 :goto_0

    .line 87
    .end local v1    # "account":Ljava/lang/String;
    .end local v8    # "videoIds":[Ljava/lang/String;
    :cond_9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not parse URI: "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 89
    .end local v7    # "uri":Landroid/net/Uri;
    :cond_a
    const-string v0, "com.google.android.videos.ERROR"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 90
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v7

    .line 91
    .restart local v7    # "uri":Landroid/net/Uri;
    if-eqz v7, :cond_b

    const-string v0, "videos"

    invoke-virtual {v7}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 92
    invoke-virtual {v7}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    .line 93
    .restart local v1    # "account":Ljava/lang/String;
    invoke-virtual {v7}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    .line 94
    .restart local v2    # "videoId":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/videos/pinning/NotificationsCallbackService;->downloadNotificationManager:Lcom/google/android/videos/pinning/DownloadNotificationManager;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->dismissErrorNotification(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p0

    move v5, v10

    .line 95
    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/pinning/NotificationsCallbackService;->toDeepLinking(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 97
    .end local v1    # "account":Ljava/lang/String;
    .end local v2    # "videoId":Ljava/lang/String;
    :cond_b
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not parse URI: "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 99
    .end local v7    # "uri":Landroid/net/Uri;
    :cond_c
    const-string v0, "com.google.android.videos.ERROR_DELETED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v7

    .line 101
    .restart local v7    # "uri":Landroid/net/Uri;
    if-eqz v7, :cond_d

    const-string v0, "videos"

    invoke-virtual {v7}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 102
    invoke-virtual {v7}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    .line 103
    .restart local v1    # "account":Ljava/lang/String;
    invoke-virtual {v7}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    .line 104
    .restart local v2    # "videoId":Ljava/lang/String;
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v6

    .line 105
    .local v6, "globals":Lcom/google/android/videos/VideosGlobals;
    invoke-virtual {v6}, Lcom/google/android/videos/VideosGlobals;->getPinHelper()Lcom/google/android/videos/ui/PinHelper;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/ui/PinHelper;->unpinVideo(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 107
    .end local v1    # "account":Ljava/lang/String;
    .end local v2    # "videoId":Ljava/lang/String;
    .end local v6    # "globals":Lcom/google/android/videos/VideosGlobals;
    :cond_d
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not parse URI: "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
