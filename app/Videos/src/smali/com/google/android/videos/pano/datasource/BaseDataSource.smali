.class public abstract Lcom/google/android/videos/pano/datasource/BaseDataSource;
.super Lcom/google/android/videos/adapter/ArrayDataSource;
.source "BaseDataSource.java"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Lcom/google/android/repolib/observers/Observable;
.implements Lcom/google/android/repolib/observers/Updatable;
.implements Lcom/google/android/repolib/observers/UpdatablesChanged;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/videos/adapter/ArrayDataSource",
        "<TT;>;",
        "Landroid/os/Handler$Callback;",
        "Lcom/google/android/repolib/observers/Observable;",
        "Lcom/google/android/repolib/observers/Updatable;",
        "Lcom/google/android/repolib/observers/UpdatablesChanged;"
    }
.end annotation


# instance fields
.field protected final context:Landroid/content/Context;

.field protected final handler:Landroid/os/Handler;

.field private lastUpdateTimestamp:J

.field private final signInManager:Lcom/google/android/videos/accounts/SignInManager;

.field private final updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/videos/accounts/SignInManager;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "signInManager"    # Lcom/google/android/videos/accounts/SignInManager;

    .prologue
    .line 40
    .local p0, "this":Lcom/google/android/videos/pano/datasource/BaseDataSource;, "Lcom/google/android/videos/pano/datasource/BaseDataSource<TT;>;"
    invoke-direct {p0}, Lcom/google/android/videos/adapter/ArrayDataSource;-><init>()V

    .line 41
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/videos/pano/datasource/BaseDataSource;->context:Landroid/content/Context;

    .line 43
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/videos/pano/datasource/BaseDataSource;->handler:Landroid/os/Handler;

    .line 45
    iput-object p2, p0, Lcom/google/android/videos/pano/datasource/BaseDataSource;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    .line 46
    new-instance v0, Lcom/google/android/videos/pano/datasource/BaseDataSource$1;

    invoke-direct {v0, p0, p2}, Lcom/google/android/videos/pano/datasource/BaseDataSource$1;-><init>(Lcom/google/android/videos/pano/datasource/BaseDataSource;Lcom/google/android/videos/accounts/SignInManager;)V

    invoke-static {v0}, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->asyncUpdateDispatcher(Lcom/google/android/repolib/observers/UpdatablesChanged;)Lcom/google/android/repolib/observers/UpdateDispatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pano/datasource/BaseDataSource;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    .line 59
    return-void
.end method


# virtual methods
.method public addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 126
    .local p0, "this":Lcom/google/android/videos/pano/datasource/BaseDataSource;, "Lcom/google/android/videos/pano/datasource/BaseDataSource<TT;>;"
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/BaseDataSource;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 127
    return-void
.end method

.method protected getAccount()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    .local p0, "this":Lcom/google/android/videos/pano/datasource/BaseDataSource;, "Lcom/google/android/videos/pano/datasource/BaseDataSource<TT;>;"
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/BaseDataSource;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    invoke-virtual {v0}, Lcom/google/android/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final handleMessage(Landroid/os/Message;)Z
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .local p0, "this":Lcom/google/android/videos/pano/datasource/BaseDataSource;, "Lcom/google/android/videos/pano/datasource/BaseDataSource<TT;>;"
    const-wide/16 v8, 0x1388

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 73
    iget v6, p1, Landroid/os/Message;->what:I

    packed-switch v6, :pswitch_data_0

    .line 95
    :goto_0
    return v4

    .line 75
    :pswitch_0
    iget-object v6, p0, Lcom/google/android/videos/pano/datasource/BaseDataSource;->handler:Landroid/os/Handler;

    invoke-virtual {v6, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 76
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/videos/pano/datasource/BaseDataSource;->lastUpdateTimestamp:J

    .line 77
    invoke-virtual {p0, v5}, Lcom/google/android/videos/pano/datasource/BaseDataSource;->updateInternal(Z)V

    move v4, v5

    .line 78
    goto :goto_0

    .line 80
    :pswitch_1
    iget-object v6, p0, Lcom/google/android/videos/pano/datasource/BaseDataSource;->handler:Landroid/os/Handler;

    invoke-virtual {v6, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 81
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 82
    .local v0, "currentTimeMillis":J
    iget-wide v6, p0, Lcom/google/android/videos/pano/datasource/BaseDataSource;->lastUpdateTimestamp:J

    sub-long v2, v0, v6

    .line 83
    .local v2, "timeFromLastUpdate":J
    cmp-long v6, v2, v8

    if-ltz v6, :cond_0

    .line 84
    iput-wide v0, p0, Lcom/google/android/videos/pano/datasource/BaseDataSource;->lastUpdateTimestamp:J

    .line 85
    invoke-virtual {p0, v4}, Lcom/google/android/videos/pano/datasource/BaseDataSource;->updateInternal(Z)V

    :goto_1
    move v4, v5

    .line 90
    goto :goto_0

    .line 87
    :cond_0
    iget-object v6, p0, Lcom/google/android/videos/pano/datasource/BaseDataSource;->handler:Landroid/os/Handler;

    sub-long/2addr v8, v2

    invoke-virtual {v6, v4, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    .line 92
    .end local v0    # "currentTimeMillis":J
    .end local v2    # "timeFromLastUpdate":J
    :pswitch_2
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, [Ljava/lang/Object;

    check-cast v4, [Ljava/lang/Object;

    invoke-virtual {p0, v4}, Lcom/google/android/videos/pano/datasource/BaseDataSource;->updateArray([Ljava/lang/Object;)V

    move v4, v5

    .line 93
    goto :goto_0

    .line 73
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 131
    .local p0, "this":Lcom/google/android/videos/pano/datasource/BaseDataSource;, "Lcom/google/android/videos/pano/datasource/BaseDataSource<TT;>;"
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/BaseDataSource;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 132
    return-void
.end method

.method public final scheduleUpdate(Z)V
    .locals 3
    .param p1, "forced"    # Z

    .prologue
    .local p0, "this":Lcom/google/android/videos/pano/datasource/BaseDataSource;, "Lcom/google/android/videos/pano/datasource/BaseDataSource<TT;>;"
    const/4 v2, 0x1

    .line 100
    if-nez p1, :cond_0

    .line 101
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/BaseDataSource;->handler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 109
    :goto_0
    return-void

    .line 104
    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 105
    invoke-virtual {p0, v2}, Lcom/google/android/videos/pano/datasource/BaseDataSource;->updateInternal(Z)V

    goto :goto_0

    .line 107
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/BaseDataSource;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public update()V
    .locals 1

    .prologue
    .line 121
    .local p0, "this":Lcom/google/android/videos/pano/datasource/BaseDataSource;, "Lcom/google/android/videos/pano/datasource/BaseDataSource<TT;>;"
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/videos/pano/datasource/BaseDataSource;->scheduleUpdate(Z)V

    .line 122
    return-void
.end method

.method public updateArray([Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;)V"
        }
    .end annotation

    .prologue
    .line 136
    .local p0, "this":Lcom/google/android/videos/pano/datasource/BaseDataSource;, "Lcom/google/android/videos/pano/datasource/BaseDataSource<TT;>;"
    .local p1, "newData":[Ljava/lang/Object;, "[TT;"
    invoke-super {p0, p1}, Lcom/google/android/videos/adapter/ArrayDataSource;->updateArray([Ljava/lang/Object;)V

    .line 137
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/BaseDataSource;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0}, Lcom/google/android/repolib/observers/UpdateDispatcher;->update()V

    .line 138
    return-void
.end method

.method protected final updateArrayOnMainThread([Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;)V"
        }
    .end annotation

    .prologue
    .line 112
    .local p0, "this":Lcom/google/android/videos/pano/datasource/BaseDataSource;, "Lcom/google/android/videos/pano/datasource/BaseDataSource<TT;>;"
    .local p1, "newData":[Ljava/lang/Object;, "[TT;"
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 113
    invoke-virtual {p0, p1}, Lcom/google/android/videos/pano/datasource/BaseDataSource;->updateArray([Ljava/lang/Object;)V

    .line 117
    :goto_0
    return-void

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/BaseDataSource;->handler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method protected abstract updateInternal(Z)V
.end method
