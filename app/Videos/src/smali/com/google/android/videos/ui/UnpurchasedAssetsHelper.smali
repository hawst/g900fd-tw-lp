.class public Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;
.super Ljava/lang/Object;
.source "UnpurchasedAssetsHelper.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/api/AssetsRequest;",
        "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private final assetResources:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;"
        }
    .end annotation
.end field

.field private final assetsCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final assetsRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final dataSource:Lcom/google/android/videos/adapter/DataSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/adapter/DataSource",
            "<*>;"
        }
    .end annotation
.end field

.field private final flags:I

.field private final itemType:I

.field private playCountry:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/app/Activity;Lcom/google/android/videos/adapter/DataSource;Lcom/google/android/videos/async/Requester;II)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p4, "itemType"    # I
    .param p5, "flags"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/google/android/videos/adapter/DataSource",
            "<*>;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 62
    .local p2, "dataSource":Lcom/google/android/videos/adapter/DataSource;, "Lcom/google/android/videos/adapter/DataSource<*>;"
    .local p3, "assetsRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p2, p0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->dataSource:Lcom/google/android/videos/adapter/DataSource;

    .line 64
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->assetResources:Ljava/util/Map;

    .line 65
    iput-object p3, p0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->assetsRequester:Lcom/google/android/videos/async/Requester;

    .line 66
    invoke-static {p1, p0}, Lcom/google/android/videos/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/ActivityCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->assetsCallback:Lcom/google/android/videos/async/Callback;

    .line 67
    iput p4, p0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->itemType:I

    .line 68
    iput p5, p0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->flags:I

    .line 69
    return-void
.end method

.method public static createForEpisodes(Landroid/app/Activity;Lcom/google/android/videos/adapter/DataSource;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;
    .locals 6
    .param p0, "activity"    # Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/google/android/videos/adapter/DataSource",
            "<*>;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;)",
            "Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;"
        }
    .end annotation

    .prologue
    .line 57
    .local p1, "dataSource":Lcom/google/android/videos/adapter/DataSource;, "Lcom/google/android/videos/adapter/DataSource<*>;"
    .local p2, "assetsRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;>;"
    new-instance v0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    const/16 v4, 0x14

    const/16 v5, 0x8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/adapter/DataSource;Lcom/google/android/videos/async/Requester;II)V

    return-object v0
.end method

.method public static createForMovies(Landroid/app/Activity;Lcom/google/android/videos/adapter/DataSource;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;
    .locals 6
    .param p0, "activity"    # Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/google/android/videos/adapter/DataSource",
            "<*>;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;)",
            "Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;"
        }
    .end annotation

    .prologue
    .line 50
    .local p1, "dataSource":Lcom/google/android/videos/adapter/DataSource;, "Lcom/google/android/videos/adapter/DataSource<*>;"
    .local p2, "assetsRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;>;"
    new-instance v0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    const/4 v4, 0x6

    const/16 v5, 0x19

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/adapter/DataSource;Lcom/google/android/videos/async/Requester;II)V

    return-object v0
.end method

.method public static createForShows(Landroid/app/Activity;Lcom/google/android/videos/adapter/DataSource;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;
    .locals 6
    .param p0, "activity"    # Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/google/android/videos/adapter/DataSource",
            "<*>;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;)",
            "Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;"
        }
    .end annotation

    .prologue
    .line 44
    .local p1, "dataSource":Lcom/google/android/videos/adapter/DataSource;, "Lcom/google/android/videos/adapter/DataSource<*>;"
    .local p2, "assetsRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;>;"
    new-instance v0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    const/16 v4, 0x12

    const/16 v5, 0x11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/adapter/DataSource;Lcom/google/android/videos/async/Requester;II)V

    return-object v0
.end method


# virtual methods
.method public addMatchingAssetResources([Lcom/google/wireless/android/video/magma/proto/AssetResource;)V
    .locals 5
    .param p1, "resources"    # [Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    .line 95
    if-eqz p1, :cond_0

    array-length v3, p1

    if-nez v3, :cond_1

    .line 109
    :cond_0
    :goto_0
    return-void

    .line 98
    :cond_1
    const/4 v2, 0x0

    .line 99
    .local v2, "notify":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v3, p1

    if-ge v1, v3, :cond_3

    .line 100
    aget-object v0, p1, v1

    .line 101
    .local v0, "assetResource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget v3, v3, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    iget v4, p0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->itemType:I

    if-ne v3, v4, :cond_2

    .line 102
    iget-object v3, p0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->assetResources:Ljava/util/Map;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v4, v4, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    const/4 v2, 0x1

    .line 99
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 106
    .end local v0    # "assetResource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :cond_3
    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->dataSource:Lcom/google/android/videos/adapter/DataSource;

    if-eqz v3, :cond_0

    .line 107
    iget-object v3, p0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->dataSource:Lcom/google/android/videos/adapter/DataSource;

    invoke-interface {v3}, Lcom/google/android/videos/adapter/DataSource;->notifyChanged()V

    goto :goto_0
.end method

.method public getAssetResource(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .locals 1
    .param p1, "itemId"    # Ljava/lang/String;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->assetResources:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    return-object v0
.end method

.method public init(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "playCountry"    # Ljava/lang/String;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->account:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->playCountry:Ljava/lang/String;

    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->assetResources:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 76
    :cond_1
    iput-object p1, p0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->account:Ljava/lang/String;

    .line 77
    iput-object p2, p0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->playCountry:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public onError(Lcom/google/android/videos/api/AssetsRequest;Ljava/lang/Exception;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/api/AssetsRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 152
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unable to obtain data for batch "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/videos/api/AssetsRequest;->ids:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 153
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 29
    check-cast p1, Lcom/google/android/videos/api/AssetsRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->onError(Lcom/google/android/videos/api/AssetsRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;)V
    .locals 3
    .param p1, "request"    # Lcom/google/android/videos/api/AssetsRequest;
    .param p2, "response"    # Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    .prologue
    .line 142
    iget-object v1, p1, Lcom/google/android/videos/api/AssetsRequest;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->account:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/google/android/videos/api/AssetsRequest;->userCountry:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->playCountry:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 148
    :cond_0
    :goto_0
    return-void

    .line 146
    :cond_1
    if-nez p2, :cond_2

    const/4 v0, 0x0

    .line 147
    .local v0, "resources":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->addMatchingAssetResources([Lcom/google/wireless/android/video/magma/proto/AssetResource;)V

    goto :goto_0

    .line 146
    .end local v0    # "resources":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :cond_2
    iget-object v0, p2, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    goto :goto_1
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 29
    check-cast p1, Lcom/google/android/videos/api/AssetsRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->onResponse(Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;)V

    return-void
.end method

.method public processUnpurchasedItems(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 116
    .local p1, "itemIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 117
    .local v3, "requestBuilder":Lcom/google/android/videos/api/AssetsRequest$Builder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 118
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 119
    .local v2, "itemId":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->assetResources:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 120
    if-nez v3, :cond_0

    .line 121
    new-instance v4, Lcom/google/android/videos/api/AssetsRequest$Builder;

    invoke-direct {v4}, Lcom/google/android/videos/api/AssetsRequest$Builder;-><init>()V

    iget-object v5, p0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->account:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/android/videos/api/AssetsRequest$Builder;->account(Ljava/lang/String;)Lcom/google/android/videos/api/AssetsRequest$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->playCountry:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/android/videos/api/AssetsRequest$Builder;->userCountry(Ljava/lang/String;)Lcom/google/android/videos/api/AssetsRequest$Builder;

    move-result-object v4

    iget v5, p0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->flags:I

    invoke-virtual {v4, v5}, Lcom/google/android/videos/api/AssetsRequest$Builder;->addFlags(I)Lcom/google/android/videos/api/AssetsRequest$Builder;

    move-result-object v3

    .line 126
    :cond_0
    iget v4, p0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->itemType:I

    invoke-static {v4, v2}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromAssetTypeAndId(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 127
    .local v0, "assetId":Ljava/lang/String;
    invoke-virtual {v3, v0}, Lcom/google/android/videos/api/AssetsRequest$Builder;->maybeAddId(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v3}, Lcom/google/android/videos/api/AssetsRequest$Builder;->getIdCount()I

    move-result v4

    if-eqz v4, :cond_1

    .line 129
    iget-object v4, p0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->assetsRequester:Lcom/google/android/videos/async/Requester;

    invoke-virtual {v3}, Lcom/google/android/videos/api/AssetsRequest$Builder;->build()Lcom/google/android/videos/api/AssetsRequest;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->assetsCallback:Lcom/google/android/videos/async/Callback;

    invoke-interface {v4, v5, v6}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 130
    invoke-virtual {v3}, Lcom/google/android/videos/api/AssetsRequest$Builder;->clearIds()V

    .line 131
    invoke-virtual {v3, v0}, Lcom/google/android/videos/api/AssetsRequest$Builder;->maybeAddId(Ljava/lang/String;)Z

    .line 117
    .end local v0    # "assetId":Ljava/lang/String;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 135
    .end local v2    # "itemId":Ljava/lang/String;
    :cond_2
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lcom/google/android/videos/api/AssetsRequest$Builder;->getIdCount()I

    move-result v4

    if-eqz v4, :cond_3

    .line 136
    iget-object v4, p0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->assetsRequester:Lcom/google/android/videos/async/Requester;

    invoke-virtual {v3}, Lcom/google/android/videos/api/AssetsRequest$Builder;->build()Lcom/google/android/videos/api/AssetsRequest;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->assetsCallback:Lcom/google/android/videos/async/Callback;

    invoke-interface {v4, v5, v6}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 138
    :cond_3
    return-void
.end method

.method public reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 81
    iget-object v0, p0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->assetResources:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 82
    iput-object v1, p0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->account:Ljava/lang/String;

    .line 83
    iput-object v1, p0, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->playCountry:Ljava/lang/String;

    .line 84
    return-void
.end method
