.class public Lcom/google/android/videos/pinning/PinningStatusHelper;
.super Ljava/lang/Object;
.source "PinningStatusHelper.java"


# direct methods
.method public static getPendingReasonTextId(I)I
    .locals 2
    .param p0, "pinningStatusReason"    # I

    .prologue
    .line 75
    and-int/lit8 v1, p0, 0x8

    if-eqz v1, :cond_0

    .line 76
    const v0, 0x7f0b017c

    .line 89
    .local v0, "textId":I
    :goto_0
    return v0

    .line 77
    .end local v0    # "textId":I
    :cond_0
    and-int/lit8 v1, p0, 0x2

    if-eqz v1, :cond_1

    .line 78
    const v0, 0x7f0b017d

    .restart local v0    # "textId":I
    goto :goto_0

    .line 79
    .end local v0    # "textId":I
    :cond_1
    and-int/lit8 v1, p0, 0x4

    if-eqz v1, :cond_2

    .line 80
    const v0, 0x7f0b017f

    .restart local v0    # "textId":I
    goto :goto_0

    .line 81
    .end local v0    # "textId":I
    :cond_2
    and-int/lit8 v1, p0, 0x10

    if-eqz v1, :cond_3

    .line 82
    const v0, 0x7f0b017e

    .restart local v0    # "textId":I
    goto :goto_0

    .line 83
    .end local v0    # "textId":I
    :cond_3
    and-int/lit8 v1, p0, 0x20

    if-eqz v1, :cond_4

    .line 84
    const v0, 0x7f0b0180

    .restart local v0    # "textId":I
    goto :goto_0

    .line 86
    .end local v0    # "textId":I
    :cond_4
    const v0, 0x7f0b0181

    .restart local v0    # "textId":I
    goto :goto_0
.end method

.method public static getProgressFraction(ZLjava/lang/Long;Ljava/lang/Long;)F
    .locals 6
    .param p0, "haveLicense"    # Z
    .param p1, "downloadSize"    # Ljava/lang/Long;
    .param p2, "bytesDownloaded"    # Ljava/lang/Long;

    .prologue
    const v1, 0x3d4ccccd    # 0.05f

    .line 94
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    const/4 v0, 0x1

    .line 96
    .local v0, "mediaDownloadStarted":Z
    :goto_0
    if-nez v0, :cond_2

    .line 98
    if-eqz p0, :cond_1

    .line 101
    :goto_1
    return v1

    .line 94
    .end local v0    # "mediaDownloadStarted":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 98
    .restart local v0    # "mediaDownloadStarted":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 101
    :cond_2
    const v2, 0x3f733333    # 0.95f

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-float v3, v4

    mul-float/2addr v2, v3

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-float v3, v4

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    goto :goto_1
.end method

.method public static humanizeFailedReason(Landroid/content/Context;ILjava/lang/Long;Ljava/lang/Integer;Z)Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "pinningStatusReason"    # I
    .param p2, "downloadSize"    # Ljava/lang/Long;
    .param p3, "drmErrorCode"    # Ljava/lang/Integer;
    .param p4, "isRental"    # Z

    .prologue
    const v1, 0x7f0b016f

    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 21
    packed-switch p1, :pswitch_data_0

    .line 68
    :pswitch_0
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    .line 23
    :pswitch_1
    const v1, 0x7f0b0169

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p3, v2, v6

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 25
    :pswitch_2
    const v1, 0x7f0b016a

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 27
    :pswitch_3
    if-eqz p4, :cond_0

    const v0, 0x7f0b016b

    .line 29
    .local v0, "stringId":I
    :goto_1
    new-array v1, v2, [Ljava/lang/Object;

    aput-object p3, v1, v6

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 27
    .end local v0    # "stringId":I
    :cond_0
    const v0, 0x7f0b016c

    goto :goto_1

    .line 31
    :pswitch_4
    const v1, 0x7f0b016e

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p3, v2, v6

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 33
    :pswitch_5
    const v1, 0x7f0b0170

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p3, v2, v6

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 36
    :pswitch_6
    const v1, 0x7f0b0171

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p3, v2, v6

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 39
    :pswitch_7
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 41
    :pswitch_8
    const v1, 0x7f0b016d

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 43
    :pswitch_9
    if-eqz p2, :cond_1

    .line 44
    const v1, 0x7f0b0172

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {p0, v4, v5}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 47
    :cond_1
    const v1, 0x7f0b0173

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 50
    :pswitch_a
    const v1, 0x7f0b0174

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 52
    :pswitch_b
    const v1, 0x7f0b0175

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 54
    :pswitch_c
    const v1, 0x7f0b0176

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 56
    :pswitch_d
    const v1, 0x7f0b0177

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 21
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_2
    .end packed-switch
.end method
