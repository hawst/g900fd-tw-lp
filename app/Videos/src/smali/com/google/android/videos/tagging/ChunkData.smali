.class public Lcom/google/android/videos/tagging/ChunkData;
.super Ljava/lang/Object;
.source "ChunkData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/tagging/ChunkData$Parser;
    }
.end annotation


# instance fields
.field private final keyframeTimes:[I

.field private final keyframes:[[Lcom/google/android/videos/tagging/Tag;


# direct methods
.method constructor <init>([I[[Lcom/google/android/videos/tagging/Tag;)V
    .locals 2
    .param p1, "keyframeTimes"    # [I
    .param p2, "keyframes"    # [[Lcom/google/android/videos/tagging/Tag;

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    array-length v0, p1

    array-length v1, p2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "keyframeTimes and keyframes length mismatch"

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;)V

    .line 39
    iput-object p1, p0, Lcom/google/android/videos/tagging/ChunkData;->keyframeTimes:[I

    .line 40
    iput-object p2, p0, Lcom/google/android/videos/tagging/ChunkData;->keyframes:[[Lcom/google/android/videos/tagging/Tag;

    .line 41
    return-void

    .line 37
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getActiveTags(ILjava/util/List;I)V
    .locals 5
    .param p1, "atMillis"    # I
    .param p3, "skippedMillisAtInterpolationStart"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/Tag;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p2, "tags":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/Tag;>;"
    iget-object v4, p0, Lcom/google/android/videos/tagging/ChunkData;->keyframeTimes:[I

    invoke-static {v4, p1}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v1

    .line 56
    .local v1, "index":I
    if-gez v1, :cond_1

    .line 57
    xor-int/lit8 v4, v1, -0x1

    add-int/lit8 v1, v4, -0x1

    .line 58
    if-gez v1, :cond_1

    .line 71
    :cond_0
    return-void

    .line 63
    :cond_1
    iget-object v4, p0, Lcom/google/android/videos/tagging/ChunkData;->keyframes:[[Lcom/google/android/videos/tagging/Tag;

    aget-object v3, v4, v1

    .line 64
    .local v3, "tagsAtKeyframe":[Lcom/google/android/videos/tagging/Tag;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v3

    if-ge v0, v4, :cond_0

    .line 65
    aget-object v4, v3, v0

    invoke-virtual {v4, p1}, Lcom/google/android/videos/tagging/Tag;->getFirstTagAt(I)Lcom/google/android/videos/tagging/Tag;

    move-result-object v2

    .line 66
    .local v2, "tag":Lcom/google/android/videos/tagging/Tag;
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/google/android/videos/tagging/Tag;->interpolatesIn()Z

    move-result v4

    if-nez v4, :cond_2

    iget v4, v2, Lcom/google/android/videos/tagging/Tag;->timeMillis:I

    add-int/2addr v4, p3

    if-gt v4, p1, :cond_3

    .line 68
    :cond_2
    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
