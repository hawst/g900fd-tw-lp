.class public Lcom/google/android/videos/api/RecommendationsRequest$DefaultFactory;
.super Ljava/lang/Object;
.source "RecommendationsRequest.java"

# interfaces
.implements Lcom/google/android/videos/api/RecommendationsRequest$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/api/RecommendationsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DefaultFactory"
.end annotation


# instance fields
.field private final configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

.field private final telephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method public constructor <init>(Landroid/telephony/TelephonyManager;Lcom/google/android/videos/store/ConfigurationStore;)V
    .locals 0
    .param p1, "telephonyManager"    # Landroid/telephony/TelephonyManager;
    .param p2, "configurationStore"    # Lcom/google/android/videos/store/ConfigurationStore;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/google/android/videos/api/RecommendationsRequest$DefaultFactory;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 63
    iput-object p2, p0, Lcom/google/android/videos/api/RecommendationsRequest$DefaultFactory;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    .line 64
    return-void
.end method

.method private create(Ljava/lang/String;IILcom/google/wireless/android/video/magma/proto/AssetResourceId;I)Lcom/google/android/videos/api/RecommendationsRequest;
    .locals 10
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "max"    # I
    .param p3, "type"    # I
    .param p4, "assetRestrictId"    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .param p5, "flags"    # I

    .prologue
    .line 68
    new-instance v0, Lcom/google/android/videos/api/RecommendationsRequest;

    iget-object v1, p0, Lcom/google/android/videos/api/RecommendationsRequest$DefaultFactory;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v5

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    iget-object v1, p0, Lcom/google/android/videos/api/RecommendationsRequest$DefaultFactory;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    invoke-virtual {v1, p1}, Lcom/google/android/videos/store/ConfigurationStore;->getPlayCountry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v8, p5

    invoke-direct/range {v0 .. v9}, Lcom/google/android/videos/api/RecommendationsRequest;-><init>(Ljava/lang/String;IILcom/google/wireless/android/video/magma/proto/AssetResourceId;Ljava/lang/String;Ljava/util/Locale;Ljava/lang/String;ILcom/google/android/videos/api/RecommendationsRequest$1;)V

    return-object v0
.end method


# virtual methods
.method public createForRelatedMovies(Ljava/lang/String;Ljava/lang/String;II)Lcom/google/android/videos/api/RecommendationsRequest;
    .locals 6
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "max"    # I
    .param p4, "flags"    # I

    .prologue
    const/4 v3, 0x6

    .line 86
    new-instance v4, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-direct {v4}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;-><init>()V

    .line 87
    .local v4, "assetRestrictId":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    iput-object p2, v4, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    .line 88
    iput v3, v4, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    move-object v0, p0

    move-object v1, p1

    move v2, p3

    move v5, p4

    .line 89
    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/api/RecommendationsRequest$DefaultFactory;->create(Ljava/lang/String;IILcom/google/wireless/android/video/magma/proto/AssetResourceId;I)Lcom/google/android/videos/api/RecommendationsRequest;

    move-result-object v0

    return-object v0
.end method

.method public createForRelatedShows(Ljava/lang/String;Ljava/lang/String;II)Lcom/google/android/videos/api/RecommendationsRequest;
    .locals 6
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;
    .param p3, "max"    # I
    .param p4, "flags"    # I

    .prologue
    const/16 v3, 0x12

    .line 95
    new-instance v4, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-direct {v4}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;-><init>()V

    .line 96
    .local v4, "assetRestrictId":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    iput-object p2, v4, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    .line 97
    iput v3, v4, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    move-object v0, p0

    move-object v1, p1

    move v2, p3

    move v5, p4

    .line 98
    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/api/RecommendationsRequest$DefaultFactory;->create(Ljava/lang/String;IILcom/google/wireless/android/video/magma/proto/AssetResourceId;I)Lcom/google/android/videos/api/RecommendationsRequest;

    move-result-object v0

    return-object v0
.end method

.method public createForShows(Ljava/lang/String;II)Lcom/google/android/videos/api/RecommendationsRequest;
    .locals 6
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "max"    # I
    .param p3, "flags"    # I

    .prologue
    .line 80
    const/16 v3, 0x12

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/api/RecommendationsRequest$DefaultFactory;->create(Ljava/lang/String;IILcom/google/wireless/android/video/magma/proto/AssetResourceId;I)Lcom/google/android/videos/api/RecommendationsRequest;

    move-result-object v0

    return-object v0
.end method
