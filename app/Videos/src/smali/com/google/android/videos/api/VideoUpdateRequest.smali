.class public Lcom/google/android/videos/api/VideoUpdateRequest;
.super Lcom/google/android/videos/async/Request;
.source "VideoUpdateRequest.java"


# instance fields
.field public final playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

.field public final videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "playback"    # Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/google/android/videos/async/Request;-><init>(Ljava/lang/String;)V

    .line 16
    iput-object p2, p0, Lcom/google/android/videos/api/VideoUpdateRequest;->videoId:Ljava/lang/String;

    .line 17
    iput-object p3, p0, Lcom/google/android/videos/api/VideoUpdateRequest;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    .line 18
    return-void
.end method
