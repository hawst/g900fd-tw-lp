.class final Lcom/google/android/videos/store/SubtitlesClient$6;
.super Ljava/lang/Object;
.source "SubtitlesClient.java"

# interfaces
.implements Lcom/google/android/videos/converter/RequestConverter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/store/SubtitlesClient;->createSubtitlesRequester(Lorg/apache/http/client/HttpClient;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/videos/utils/XmlParser;Lcom/google/android/videos/store/AbstractFileStore;)Lcom/google/android/videos/async/Requester;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/converter/RequestConverter",
        "<",
        "Landroid/util/Pair",
        "<",
        "Lcom/google/android/videos/subtitles/SubtitleTrack;",
        "Ljava/lang/Integer;",
        ">;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$converter:Lcom/google/android/videos/subtitles/SubtitlesConverter;


# direct methods
.method constructor <init>(Lcom/google/android/videos/subtitles/SubtitlesConverter;)V
    .locals 0

    .prologue
    .line 200
    iput-object p1, p0, Lcom/google/android/videos/store/SubtitlesClient$6;->val$converter:Lcom/google/android/videos/subtitles/SubtitlesConverter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 200
    check-cast p1, Landroid/util/Pair;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/store/SubtitlesClient$6;->convertRequest(Landroid/util/Pair;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public convertRequest(Landroid/util/Pair;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lorg/apache/http/client/methods/HttpUriRequest;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 204
    .local p1, "request":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/videos/subtitles/SubtitleTrack;Ljava/lang/Integer;>;"
    iget-object v1, p0, Lcom/google/android/videos/store/SubtitlesClient$6;->val$converter:Lcom/google/android/videos/subtitles/SubtitlesConverter;

    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/videos/subtitles/SubtitleTrack;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/subtitles/SubtitlesConverter;->convertRequest(Lcom/google/android/videos/subtitles/SubtitleTrack;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method
