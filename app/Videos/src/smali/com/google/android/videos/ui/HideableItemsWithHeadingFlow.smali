.class public Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow;
.super Lcom/google/android/videos/ui/ItemsWithHeadingFlow;
.source "HideableItemsWithHeadingFlow.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Lcom/google/android/videos/ui/playnext/ClusterItemView;",
        "D::",
        "Lcom/google/android/videos/adapter/DataSource",
        "<*>;>",
        "Lcom/google/android/videos/ui/ItemsWithHeadingFlow",
        "<TV;TD;>;"
    }
.end annotation


# instance fields
.field private hide:Z


# direct methods
.method public constructor <init>(Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;Lcom/google/android/videos/adapter/DataSource;ILcom/google/android/videos/ui/playnext/ClusterItemView$Binder;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;I)V
    .locals 1
    .param p1, "sectionHeadingBinder"    # Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;
    .param p3, "layoutId"    # I
    .param p5, "clickListener"    # Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;
    .param p6, "maxItems"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;",
            "TD;I",
            "Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder",
            "<TV;TD;>;",
            "Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 22
    .local p0, "this":Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow;, "Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow<TV;TD;>;"
    .local p2, "dataSource":Lcom/google/android/videos/adapter/DataSource;, "TD;"
    .local p4, "clusterItemViewBinder":Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;, "Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder<TV;TD;>;"
    invoke-direct/range {p0 .. p6}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;-><init>(Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;Lcom/google/android/videos/adapter/DataSource;ILcom/google/android/videos/ui/playnext/ClusterItemView$Binder;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;I)V

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow;->hide:Z

    .line 25
    return-void
.end method


# virtual methods
.method public bindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 2
    .param p1, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 36
    .local p0, "this":Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow;, "Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow<TV;TD;>;"
    iget-boolean v0, p0, Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow;->hide:Z

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 42
    :goto_0
    return-void

    .line 39
    :cond_0
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 40
    invoke-super {p0, p1, p2}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->bindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V

    goto :goto_0
.end method

.method public setHideItems(Z)V
    .locals 1
    .param p1, "hide"    # Z

    .prologue
    .line 28
    .local p0, "this":Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow;, "Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow<TV;TD;>;"
    iget-boolean v0, p0, Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow;->hide:Z

    if-eq v0, p1, :cond_0

    .line 29
    iput-boolean p1, p0, Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow;->hide:Z

    .line 30
    invoke-virtual {p0}, Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow;->notifyItemStatesChanged()V

    .line 32
    :cond_0
    return-void
.end method
