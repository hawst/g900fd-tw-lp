.class Lcom/google/android/videos/pinning/LegacyLicenseTask$Refresh;
.super Lcom/google/android/videos/pinning/LegacyLicenseTask;
.source "LegacyLicenseTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pinning/LegacyLicenseTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Refresh"
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/videos/pinning/DownloadKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/videos/pinning/Task$Listener;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/drm/DrmManager;Lcom/google/android/videos/accounts/AccountManagerWrapper;ZLcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/store/ItagInfoStore;)V
    .locals 13
    .param p1, "key"    # Lcom/google/android/videos/pinning/DownloadKey;
    .param p2, "wakeLock"    # Landroid/os/PowerManager$WakeLock;
    .param p3, "wifiLock"    # Landroid/net/wifi/WifiManager$WifiLock;
    .param p4, "listener"    # Lcom/google/android/videos/pinning/Task$Listener;
    .param p5, "database"    # Lcom/google/android/videos/store/Database;
    .param p6, "drmManager"    # Lcom/google/android/videos/drm/DrmManager;
    .param p7, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .param p8, "legacyDownloadsHaveAppLevelDrm"    # Z
    .param p9, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;
    .param p10, "itagInfoStore"    # Lcom/google/android/videos/store/ItagInfoStore;

    .prologue
    .line 196
    const/4 v1, 0x2

    const/4 v12, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v12}, Lcom/google/android/videos/pinning/LegacyLicenseTask;-><init>(ILcom/google/android/videos/pinning/DownloadKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/videos/pinning/Task$Listener;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/drm/DrmManager;Lcom/google/android/videos/accounts/AccountManagerWrapper;ZLcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/store/ItagInfoStore;Lcom/google/android/videos/pinning/LegacyLicenseTask$1;)V

    .line 198
    return-void
.end method

.method private logError(Ljava/lang/Throwable;)V
    .locals 3
    .param p1, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 258
    iget-object v1, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask$Refresh;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget-object v0, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask$Refresh;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v0, v0, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/videos/pinning/LegacyLicenseTask$Refresh;->getThrowableToLog(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/videos/logging/EventLogger;->onLicenseRefreshError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 259
    return-void
.end method

.method private unpinAndClearLicense()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 251
    invoke-static {}, Lcom/google/android/videos/pinning/PinningDbHelper;->getClearedLicenseContentValues()Landroid/content/ContentValues;

    move-result-object v0

    .line 252
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "pinned"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 253
    const-string v1, "pinning_notification_active"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 254
    iget-object v2, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask$Refresh;->database:Lcom/google/android/videos/store/Database;

    iget-object v1, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask$Refresh;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v1, Lcom/google/android/videos/pinning/DownloadKey;

    invoke-static {v2, v1, v0}, Lcom/google/android/videos/pinning/PinningDbHelper;->updatePinningStateForVideo(Lcom/google/android/videos/store/Database;Lcom/google/android/videos/pinning/DownloadKey;Landroid/content/ContentValues;)V

    .line 255
    return-void
.end method


# virtual methods
.method protected buildRequest(Ljava/lang/String;Lcom/google/android/videos/streams/MediaStream;Lcom/google/android/videos/drm/DrmManager$Identifiers;)Lcom/google/android/videos/drm/DrmRequest;
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "stream"    # Lcom/google/android/videos/streams/MediaStream;
    .param p3, "ids"    # Lcom/google/android/videos/drm/DrmManager$Identifiers;

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask$Refresh;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v0, v0, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-static {p1, p2, v0, p3}, Lcom/google/android/videos/drm/DrmRequest;->createOfflineSyncRequest(Ljava/lang/String;Lcom/google/android/videos/streams/MediaStream;Ljava/lang/String;Lcom/google/android/videos/drm/DrmManager$Identifiers;)Lcom/google/android/videos/drm/DrmRequest;

    move-result-object v0

    return-object v0
.end method

.method protected onCompleted()V
    .locals 2

    .prologue
    .line 265
    iget-object v1, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask$Refresh;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget-object v0, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask$Refresh;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v0, v0, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/EventLogger;->onLicenseRefreshCompleted(Ljava/lang/String;)V

    .line 266
    return-void
.end method

.method protected onError(Ljava/lang/Throwable;ZZ)V
    .locals 0
    .param p1, "cause"    # Ljava/lang/Throwable;
    .param p2, "maxRetries"    # Z
    .param p3, "fatal"    # Z

    .prologue
    .line 270
    invoke-direct {p0, p1}, Lcom/google/android/videos/pinning/LegacyLicenseTask$Refresh;->logError(Ljava/lang/Throwable;)V

    .line 271
    return-void
.end method

.method protected onRequestCompleted(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Lcom/google/android/videos/drm/DrmResponse;)V
    .locals 4
    .param p1, "details"    # Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;
    .param p2, "response"    # Lcom/google/android/videos/drm/DrmResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/Task$TaskException;
        }
    .end annotation

    .prologue
    .line 208
    iget-boolean v1, p2, Lcom/google/android/videos/drm/DrmResponse;->refreshed:Z

    if-nez v1, :cond_0

    .line 210
    new-instance v1, Lcom/google/android/videos/pinning/Task$TaskException;

    const-string v2, "License still valid, but wasn\'t refreshed"

    invoke-direct {v1, v2}, Lcom/google/android/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 212
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 213
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "license_last_synced_timestamp"

    iget-wide v2, p2, Lcom/google/android/videos/drm/DrmResponse;->timestamp:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 214
    const-string v1, "license_force_sync"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 215
    iget-object v2, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask$Refresh;->database:Lcom/google/android/videos/store/Database;

    iget-object v1, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask$Refresh;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v1, Lcom/google/android/videos/pinning/DownloadKey;

    invoke-static {v2, v1, v0}, Lcom/google/android/videos/pinning/PinningDbHelper;->updatePinningStateForVideo(Lcom/google/android/videos/store/Database;Lcom/google/android/videos/pinning/DownloadKey;Landroid/content/ContentValues;)V

    .line 216
    return-void
.end method

.method protected onRequestError(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/lang/Throwable;)V
    .locals 4
    .param p1, "details"    # Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;
    .param p2, "cause"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/Task$TaskException;
        }
    .end annotation

    .prologue
    .line 220
    instance-of v1, p2, Lcom/google/android/videos/drm/DrmFallbackException;

    if-eqz v1, :cond_0

    .line 221
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pinning/LegacyLicenseTask$Refresh;->onRequestImpossible(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/lang/Throwable;)V

    .line 236
    :goto_0
    return-void

    .line 225
    :cond_0
    instance-of v1, p2, Lcom/google/android/videos/drm/DrmException;

    if-eqz v1, :cond_3

    move-object v0, p2

    .line 226
    check-cast v0, Lcom/google/android/videos/drm/DrmException;

    .line 227
    .local v0, "drmException":Lcom/google/android/videos/drm/DrmException;
    iget-object v1, v0, Lcom/google/android/videos/drm/DrmException;->drmError:Lcom/google/android/videos/drm/DrmException$DrmError;

    sget-object v2, Lcom/google/android/videos/drm/DrmException$DrmError;->NO_LICENSE:Lcom/google/android/videos/drm/DrmException$DrmError;

    if-ne v1, v2, :cond_1

    .line 230
    invoke-direct {p0}, Lcom/google/android/videos/pinning/LegacyLicenseTask$Refresh;->unpinAndClearLicense()V

    .line 231
    invoke-direct {p0, p2}, Lcom/google/android/videos/pinning/LegacyLicenseTask$Refresh;->logError(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 233
    :cond_1
    iget-object v1, v0, Lcom/google/android/videos/drm/DrmException;->drmError:Lcom/google/android/videos/drm/DrmException$DrmError;

    sget-object v2, Lcom/google/android/videos/drm/DrmException$DrmError;->KEY_VERIFICATION_FAILED:Lcom/google/android/videos/drm/DrmException$DrmError;

    if-eq v1, v2, :cond_2

    iget-object v1, v0, Lcom/google/android/videos/drm/DrmException;->drmError:Lcom/google/android/videos/drm/DrmException$DrmError;

    sget-object v2, Lcom/google/android/videos/drm/DrmException$DrmError;->ROOTED_DEVICE:Lcom/google/android/videos/drm/DrmException$DrmError;

    if-ne v1, v2, :cond_3

    .line 235
    :cond_2
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pinning/LegacyLicenseTask$Refresh;->onRequestImpossible(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 240
    .end local v0    # "drmException":Lcom/google/android/videos/drm/DrmException;
    :cond_3
    new-instance v1, Lcom/google/android/videos/pinning/Task$TaskException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to refresh "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask$Refresh;->key:Lcom/google/android/videos/pinning/Task$Key;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, p2}, Lcom/google/android/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected onRequestImpossible(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "details"    # Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;
    .param p2, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 246
    invoke-direct {p0}, Lcom/google/android/videos/pinning/LegacyLicenseTask$Refresh;->unpinAndClearLicense()V

    .line 247
    invoke-direct {p0, p2}, Lcom/google/android/videos/pinning/LegacyLicenseTask$Refresh;->logError(Ljava/lang/Throwable;)V

    .line 248
    return-void
.end method
