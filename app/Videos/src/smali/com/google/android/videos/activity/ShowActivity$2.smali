.class Lcom/google/android/videos/activity/ShowActivity$2;
.super Ljava/lang/Object;
.source "ShowActivity.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/activity/ShowActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/activity/ShowActivity;


# direct methods
.method constructor <init>(Lcom/google/android/videos/activity/ShowActivity;)V
    .locals 0

    .prologue
    .line 307
    iput-object p1, p0, Lcom/google/android/videos/activity/ShowActivity$2;->this$0:Lcom/google/android/videos/activity/ShowActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "request"    # Ljava/lang/Object;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 314
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity$2;->this$0:Lcom/google/android/videos/activity/ShowActivity;

    # invokes: Lcom/google/android/videos/activity/ShowActivity;->onSyncError(Ljava/lang/Exception;)V
    invoke-static {v0, p2}, Lcom/google/android/videos/activity/ShowActivity;->access$100(Lcom/google/android/videos/activity/ShowActivity;Ljava/lang/Exception;)V

    .line 315
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 307
    check-cast p2, Ljava/lang/Void;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/activity/ShowActivity$2;->onResponse(Ljava/lang/Object;Ljava/lang/Void;)V

    return-void
.end method

.method public onResponse(Ljava/lang/Object;Ljava/lang/Void;)V
    .locals 1
    .param p1, "request"    # Ljava/lang/Object;
    .param p2, "response"    # Ljava/lang/Void;

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity$2;->this$0:Lcom/google/android/videos/activity/ShowActivity;

    # invokes: Lcom/google/android/videos/activity/ShowActivity;->onSyncSuccess()V
    invoke-static {v0}, Lcom/google/android/videos/activity/ShowActivity;->access$000(Lcom/google/android/videos/activity/ShowActivity;)V

    .line 311
    return-void
.end method
