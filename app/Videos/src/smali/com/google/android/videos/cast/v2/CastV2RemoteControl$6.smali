.class Lcom/google/android/videos/cast/v2/CastV2RemoteControl$6;
.super Ljava/lang/Object;
.source "CastV2RemoteControl.java"

# interfaces
.implements Lcom/google/android/gms/common/api/ResultCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->onGmsConnected()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/common/api/ResultCallback",
        "<",
        "Lcom/google/android/gms/cast/Cast$ApplicationConnectionResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;


# direct methods
.method constructor <init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)V
    .locals 0

    .prologue
    .line 587
    iput-object p1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$6;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResult(Lcom/google/android/gms/cast/Cast$ApplicationConnectionResult;)V
    .locals 3
    .param p1, "result"    # Lcom/google/android/gms/cast/Cast$ApplicationConnectionResult;

    .prologue
    const/4 v2, 0x0

    .line 590
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Connection result "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/cast/Cast$ApplicationConnectionResult;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 591
    invoke-interface {p1}, Lcom/google/android/gms/cast/Cast$ApplicationConnectionResult;->getApplicationMetadata()Lcom/google/android/gms/cast/ApplicationMetadata;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$6;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # getter for: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->receiverAppId:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$1900(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/cast/Cast$ApplicationConnectionResult;->getApplicationMetadata()Lcom/google/android/gms/cast/ApplicationMetadata;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/cast/ApplicationMetadata;->getApplicationId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 594
    :cond_0
    const-string v0, "Remote device is now running a different application. Disconnecting."

    invoke-static {v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 595
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$6;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # invokes: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->disconnect(Z)V
    invoke-static {v0, v2}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$1800(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Z)V

    .line 605
    :goto_0
    return-void

    .line 599
    :cond_1
    invoke-interface {p1}, Lcom/google/android/gms/cast/Cast$ApplicationConnectionResult;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 600
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$6;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    invoke-interface {p1}, Lcom/google/android/gms/cast/Cast$ApplicationConnectionResult;->getSessionId()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->onWaitForStatus(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$1600(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Ljava/lang/String;)V

    goto :goto_0

    .line 602
    :cond_2
    const-string v0, "Launch receiver app failed. Disconnecting."

    invoke-static {v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 603
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$6;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # invokes: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->disconnect(Z)V
    invoke-static {v0, v2}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$1800(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Z)V

    goto :goto_0
.end method

.method public bridge synthetic onResult(Lcom/google/android/gms/common/api/Result;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/gms/common/api/Result;

    .prologue
    .line 587
    check-cast p1, Lcom/google/android/gms/cast/Cast$ApplicationConnectionResult;

    .end local p1    # "x0":Lcom/google/android/gms/common/api/Result;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$6;->onResult(Lcom/google/android/gms/cast/Cast$ApplicationConnectionResult;)V

    return-void
.end method
