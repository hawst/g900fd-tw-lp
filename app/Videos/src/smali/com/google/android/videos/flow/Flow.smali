.class public abstract Lcom/google/android/videos/flow/Flow;
.super Ljava/lang/Object;
.source "Flow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/flow/Flow$FlowChangesListener;
    }
.end annotation


# instance fields
.field private isVisible:Z

.field private listener:Lcom/google/android/videos/flow/Flow$FlowChangesListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/flow/Flow;->isVisible:Z

    return-void
.end method

.method private notifyItemsRemoved(IIZ)V
    .locals 1
    .param p1, "positionStart"    # I
    .param p2, "positionCount"    # I
    .param p3, "wasVisible"    # Z

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/videos/flow/Flow;->listener:Lcom/google/android/videos/flow/Flow$FlowChangesListener;

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    .line 140
    iget-object v0, p0, Lcom/google/android/videos/flow/Flow;->listener:Lcom/google/android/videos/flow/Flow$FlowChangesListener;

    invoke-interface {v0, p0, p1, p2}, Lcom/google/android/videos/flow/Flow$FlowChangesListener;->onItemsRemoved(Lcom/google/android/videos/flow/Flow;II)V

    .line 142
    :cond_0
    return-void
.end method


# virtual methods
.method public abstract bindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
.end method

.method public abstract getCount()I
.end method

.method public abstract getItemIdentifier(I)Ljava/lang/Object;
.end method

.method public abstract getViewType(I)I
.end method

.method public final getVisibleCount()I
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/android/videos/flow/Flow;->isVisible:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/videos/flow/Flow;->getCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isVisible()Z
    .locals 1

    .prologue
    .line 163
    iget-boolean v0, p0, Lcom/google/android/videos/flow/Flow;->isVisible:Z

    return v0
.end method

.method protected final notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/videos/flow/Flow;->listener:Lcom/google/android/videos/flow/Flow$FlowChangesListener;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/flow/Flow;->isVisible:Z

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/google/android/videos/flow/Flow;->listener:Lcom/google/android/videos/flow/Flow$FlowChangesListener;

    invoke-interface {v0, p0}, Lcom/google/android/videos/flow/Flow$FlowChangesListener;->onDataSetChanged(Lcom/google/android/videos/flow/Flow;)V

    .line 112
    :cond_0
    return-void
.end method

.method protected final notifyItemsChanged(II)V
    .locals 1
    .param p1, "positionStart"    # I
    .param p2, "positionCount"    # I

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/videos/flow/Flow;->listener:Lcom/google/android/videos/flow/Flow$FlowChangesListener;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/flow/Flow;->isVisible:Z

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/google/android/videos/flow/Flow;->listener:Lcom/google/android/videos/flow/Flow$FlowChangesListener;

    invoke-interface {v0, p0, p1, p2}, Lcom/google/android/videos/flow/Flow$FlowChangesListener;->onItemsChanged(Lcom/google/android/videos/flow/Flow;II)V

    .line 155
    :cond_0
    return-void
.end method

.method protected final notifyItemsInserted(II)V
    .locals 1
    .param p1, "positionStart"    # I
    .param p2, "positionCount"    # I

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/videos/flow/Flow;->listener:Lcom/google/android/videos/flow/Flow$FlowChangesListener;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/flow/Flow;->isVisible:Z

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/google/android/videos/flow/Flow;->listener:Lcom/google/android/videos/flow/Flow$FlowChangesListener;

    invoke-interface {v0, p0, p1, p2}, Lcom/google/android/videos/flow/Flow$FlowChangesListener;->onItemsInserted(Lcom/google/android/videos/flow/Flow;II)V

    .line 125
    :cond_0
    return-void
.end method

.method protected final notifyItemsRemoved(II)V
    .locals 1
    .param p1, "positionStart"    # I
    .param p2, "positionCount"    # I

    .prologue
    .line 135
    iget-boolean v0, p0, Lcom/google/android/videos/flow/Flow;->isVisible:Z

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/videos/flow/Flow;->notifyItemsRemoved(IIZ)V

    .line 136
    return-void
.end method

.method protected abstract onInitViewTypes(Landroid/util/SparseArray;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/videos/flow/ViewHolderCreator",
            "<*>;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation
.end method

.method setFlowChangesListener(Lcom/google/android/videos/flow/Flow$FlowChangesListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/videos/flow/Flow$FlowChangesListener;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/google/android/videos/flow/Flow;->listener:Lcom/google/android/videos/flow/Flow$FlowChangesListener;

    .line 40
    return-void
.end method

.method public setVisible(Z)V
    .locals 3
    .param p1, "isVisible"    # Z

    .prologue
    const/4 v2, 0x0

    .line 173
    iget-boolean v1, p0, Lcom/google/android/videos/flow/Flow;->isVisible:Z

    if-ne v1, p1, :cond_1

    .line 192
    :cond_0
    :goto_0
    return-void

    .line 177
    :cond_1
    iput-boolean p1, p0, Lcom/google/android/videos/flow/Flow;->isVisible:Z

    .line 178
    iget-object v1, p0, Lcom/google/android/videos/flow/Flow;->listener:Lcom/google/android/videos/flow/Flow$FlowChangesListener;

    if-eqz v1, :cond_0

    .line 182
    invoke-virtual {p0}, Lcom/google/android/videos/flow/Flow;->getCount()I

    move-result v0

    .line 183
    .local v0, "count":I
    if-eqz v0, :cond_0

    .line 187
    if-eqz p1, :cond_2

    .line 188
    invoke-virtual {p0, v2, v0}, Lcom/google/android/videos/flow/Flow;->notifyItemsInserted(II)V

    goto :goto_0

    .line 190
    :cond_2
    const/4 v1, 0x1

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/videos/flow/Flow;->notifyItemsRemoved(IIZ)V

    goto :goto_0
.end method
