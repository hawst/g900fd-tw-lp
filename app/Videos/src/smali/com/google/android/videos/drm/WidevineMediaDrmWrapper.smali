.class public final Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;
.super Ljava/lang/Object;
.source "WidevineMediaDrmWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$1;,
        Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$ProvisioningCallback;,
        Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$LicenseCallback;,
        Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$NoWidevinePsshException;,
        Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$InvalidKeySetIdException;
    }
.end annotation


# static fields
.field public static final WIDEVINE_UUID:Ljava/util/UUID;

.field private static defaultSecurityLevel:I


# instance fields
.field private final account:Ljava/lang/String;

.field private activation:Lcom/google/android/videos/drm/Activation;

.field private final config:Lcom/google/android/videos/Config;

.field private currentClientCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/lang/Object;",
            "[B>;"
        }
    .end annotation
.end field

.field private currentClientRequest:Ljava/lang/Object;

.field private currentClientRequestHasTriedProvisioning:Z

.field private currentInternalProvisionRequestReason:I

.field private currentInternalRequest:Ljava/lang/Object;

.field private final keyRequestType:I

.field private final keySetId:[B

.field private final licenseCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/api/CencLicenseRequest;",
            "[B>;"
        }
    .end annotation
.end field

.field private final licenseRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/CencLicenseRequest;",
            "[B>;"
        }
    .end annotation
.end field

.field private final mediaDrm:Landroid/media/MediaDrm;

.field private mimeType:Ljava/lang/String;

.field private opened:Z

.field private final optionalKeyRequestParameters:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

.field private final provisioningCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "[B>;"
        }
    .end annotation
.end field

.field private final provisioningRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "[B>;"
        }
    .end annotation
.end field

.field private final securityLevel:I

.field private sessionId:[B

.field private final videoId:Ljava/lang/String;

.field private widevinePsshData:[B


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 66
    new-instance v0, Ljava/util/UUID;

    const-wide v2, -0x121074568629b532L    # -3.563403477674908E221

    const-wide v4, -0x5c37d8232ae2de13L

    invoke-direct {v0, v2, v3, v4, v5}, Ljava/util/UUID;-><init>(JJ)V

    sput-object v0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->WIDEVINE_UUID:Ljava/util/UUID;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/videos/VideosGlobals;Ljava/lang/String;Ljava/lang/String;I[BILandroid/media/MediaDrm$OnEventListener;Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;)V
    .locals 5
    .param p1, "videosGlobals"    # Lcom/google/android/videos/VideosGlobals;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "videoId"    # Ljava/lang/String;
    .param p4, "keyRequestType"    # I
    .param p5, "keySetId"    # [B
    .param p6, "forcedSecurityLevel"    # I
    .param p7, "listener"    # Landroid/media/MediaDrm$OnEventListener;
    .param p8, "preparationLogger"    # Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/media/UnsupportedSchemeException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 137
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->account:Ljava/lang/String;

    .line 138
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->videoId:Ljava/lang/String;

    .line 139
    iput p4, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->keyRequestType:I

    .line 140
    iput-object p5, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->keySetId:[B

    .line 141
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->config:Lcom/google/android/videos/Config;

    .line 142
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/api/ApiRequesters;->getCencLicenseRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->licenseRequester:Lcom/google/android/videos/async/Requester;

    .line 143
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/api/ApiRequesters;->getBytesRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->provisioningRequester:Lcom/google/android/videos/async/Requester;

    .line 144
    iput-object p8, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    .line 148
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->getDefaultSecurityLevel(Lcom/google/android/videos/logging/EventLogger;)I

    .line 150
    new-instance v0, Landroid/media/MediaDrm;

    sget-object v1, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->WIDEVINE_UUID:Ljava/util/UUID;

    invoke-direct {v0, v1}, Landroid/media/MediaDrm;-><init>(Ljava/util/UUID;)V

    iput-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->mediaDrm:Landroid/media/MediaDrm;

    .line 153
    if-lez p6, :cond_1

    sget v0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->defaultSecurityLevel:I

    if-le p6, v0, :cond_1

    .line 154
    iget-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->mediaDrm:Landroid/media/MediaDrm;

    const-string v1, "securityLevel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "L"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaDrm;->setPropertyString(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    iget-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->mediaDrm:Landroid/media/MediaDrm;

    invoke-static {v0}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->getSecurityLevel(Landroid/media/MediaDrm;)I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->securityLevel:I

    .line 161
    :goto_0
    if-eqz p7, :cond_0

    .line 162
    iget-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->mediaDrm:Landroid/media/MediaDrm;

    invoke-virtual {v0, p7}, Landroid/media/MediaDrm;->setOnEventListener(Landroid/media/MediaDrm$OnEventListener;)V

    .line 164
    :cond_0
    new-instance v0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$LicenseCallback;

    invoke-direct {v0, p0, v4}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$LicenseCallback;-><init>(Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$1;)V

    iput-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->licenseCallback:Lcom/google/android/videos/async/Callback;

    .line 165
    new-instance v0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$ProvisioningCallback;

    invoke-direct {v0, p0, v4}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$ProvisioningCallback;-><init>(Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$1;)V

    iput-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->provisioningCallback:Lcom/google/android/videos/async/Callback;

    .line 166
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->optionalKeyRequestParameters:Ljava/util/HashMap;

    .line 167
    iget-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->optionalKeyRequestParameters:Ljava/util/HashMap;

    const-string v1, "aid"

    iget-object v2, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->config:Lcom/google/android/videos/Config;

    invoke-interface {v2}, Lcom/google/android/videos/Config;->gservicesId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    iget-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->optionalKeyRequestParameters:Ljava/util/HashMap;

    const-string v1, "android_id"

    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "android_id"

    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    return-void

    .line 158
    :cond_1
    sget v0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->defaultSecurityLevel:I

    iput v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->securityLevel:I

    goto :goto_0
.end method

.method private buildLicenseUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "serverUri"    # Ljava/lang/String;

    .prologue
    .line 440
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "source"

    const-string v3, "YOUTUBE"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "video_id"

    invoke-virtual {v1, v2, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 443
    .local v0, "builder":Landroid/net/Uri$Builder;
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static declared-synchronized getDefaultSecurityLevel(Lcom/google/android/videos/logging/EventLogger;)I
    .locals 4
    .param p0, "logger"    # Lcom/google/android/videos/logging/EventLogger;

    .prologue
    .line 179
    const-class v3, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;

    monitor-enter v3

    :try_start_0
    sget v2, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->defaultSecurityLevel:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    .line 181
    :try_start_1
    new-instance v1, Landroid/media/MediaDrm;

    sget-object v2, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->WIDEVINE_UUID:Ljava/util/UUID;

    invoke-direct {v1, v2}, Landroid/media/MediaDrm;-><init>(Ljava/util/UUID;)V

    .line 182
    .local v1, "mediaDrm":Landroid/media/MediaDrm;
    invoke-static {v1}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->getSecurityLevel(Landroid/media/MediaDrm;)I

    move-result v2

    sput v2, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->defaultSecurityLevel:I

    .line 183
    invoke-virtual {v1}, Landroid/media/MediaDrm;->release()V
    :try_end_1
    .catch Landroid/media/UnsupportedSchemeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 187
    .end local v1    # "mediaDrm":Landroid/media/MediaDrm;
    :goto_0
    :try_start_2
    sget v2, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->defaultSecurityLevel:I

    invoke-interface {p0, v2}, Lcom/google/android/videos/logging/EventLogger;->onModularDrmInit(I)V

    .line 189
    :cond_0
    sget v2, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->defaultSecurityLevel:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v3

    return v2

    .line 184
    :catch_0
    move-exception v0

    .line 185
    .local v0, "e":Landroid/media/UnsupportedSchemeException;
    const/4 v2, -0x2

    :try_start_3
    sput v2, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->defaultSecurityLevel:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 179
    .end local v0    # "e":Landroid/media/UnsupportedSchemeException;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public static getOfflineTaskInstance(Lcom/google/android/videos/VideosGlobals;Ljava/lang/String;Ljava/lang/String;[BIZ)Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;
    .locals 9
    .param p0, "videosGlobals"    # Lcom/google/android/videos/VideosGlobals;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "keySetId"    # [B
    .param p4, "forcedSecurityLevel"    # I
    .param p5, "isRelease"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/media/UnsupportedSchemeException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 119
    if-eqz p5, :cond_0

    const/4 v4, 0x3

    .line 120
    .local v4, "keyRequestType":I
    :goto_0
    new-instance v0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    move v6, p4

    move-object v8, v7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;-><init>(Lcom/google/android/videos/VideosGlobals;Ljava/lang/String;Ljava/lang/String;I[BILandroid/media/MediaDrm$OnEventListener;Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;)V

    return-object v0

    .line 119
    .end local v4    # "keyRequestType":I
    :cond_0
    const/4 v4, 0x2

    goto :goto_0
.end method

.method public static getPlaybackInstance(Lcom/google/android/videos/VideosGlobals;Ljava/lang/String;Ljava/lang/String;[BILandroid/media/MediaDrm$OnEventListener;Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;)Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;
    .locals 9
    .param p0, "videosGlobals"    # Lcom/google/android/videos/VideosGlobals;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "keySetId"    # [B
    .param p4, "forcedSecurityLevel"    # I
    .param p5, "listener"    # Landroid/media/MediaDrm$OnEventListener;
    .param p6, "preparationLogger"    # Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/media/UnsupportedSchemeException;
        }
    .end annotation

    .prologue
    .line 128
    if-eqz p3, :cond_0

    const/4 v4, 0x2

    .line 129
    .local v4, "keyRequestType":I
    :goto_0
    new-instance v0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    move v6, p4

    move-object v7, p5

    move-object v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;-><init>(Lcom/google/android/videos/VideosGlobals;Ljava/lang/String;Ljava/lang/String;I[BILandroid/media/MediaDrm$OnEventListener;Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;)V

    return-object v0

    .line 128
    .end local v4    # "keyRequestType":I
    :cond_0
    const/4 v4, 0x1

    goto :goto_0
.end method

.method private static getSecurityLevel(Landroid/media/MediaDrm;)I
    .locals 2
    .param p0, "mediaDrm"    # Landroid/media/MediaDrm;

    .prologue
    .line 193
    const-string v1, "securityLevel"

    invoke-virtual {p0, v1}, Landroid/media/MediaDrm;->getPropertyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 194
    .local v0, "securityLevelProperty":Ljava/lang/String;
    const-string v1, "L1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const-string v1, "L3"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    goto :goto_0
.end method

.method private openInternal()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 244
    :try_start_0
    iget-object v1, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->mediaDrm:Landroid/media/MediaDrm;

    invoke-virtual {v1}, Landroid/media/MediaDrm;->openSession()[B

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->sessionId:[B

    .line 245
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->opened:Z

    .line 246
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->reportResponseToClient([B)V
    :try_end_0
    .catch Landroid/media/NotProvisionedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 253
    :goto_0
    return-void

    .line 247
    :catch_0
    move-exception v0

    .line 248
    .local v0, "e":Landroid/media/NotProvisionedException;
    invoke-direct {p0, v2, v0}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->requestProvisioning(ILandroid/media/NotProvisionedException;)V

    goto :goto_0

    .line 249
    .end local v0    # "e":Landroid/media/NotProvisionedException;
    :catch_1
    move-exception v0

    .line 250
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "Unexpected error when opening session"

    invoke-static {v1, v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 251
    invoke-direct {p0, v0}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->reportErrorToClient(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private recordTaskEnd(IZ)V
    .locals 1
    .param p1, "task"    # I
    .param p2, "successful"    # Z

    .prologue
    .line 531
    iget-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    if-eqz v0, :cond_0

    .line 532
    iget-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->recordTaskEnd(IZ)V

    .line 534
    :cond_0
    return-void
.end method

.method private recordTaskStart(I)V
    .locals 1
    .param p1, "task"    # I

    .prologue
    .line 525
    iget-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    if-eqz v0, :cond_0

    .line 526
    iget-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->recordTaskStart(I)V

    .line 528
    :cond_0
    return-void
.end method

.method private reportErrorToClient(Ljava/lang/Exception;)V
    .locals 3
    .param p1, "e"    # Ljava/lang/Exception;

    .prologue
    const/4 v2, 0x0

    .line 504
    iget-object v1, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentClientRequest:Ljava/lang/Object;

    .line 505
    .local v1, "clientRequest":Ljava/lang/Object;
    iget-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentClientCallback:Lcom/google/android/videos/async/Callback;

    .line 506
    .local v0, "clientCallback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Ljava/lang/Object;[B>;"
    iput-object v2, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentClientRequest:Ljava/lang/Object;

    .line 507
    iput-object v2, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentClientCallback:Lcom/google/android/videos/async/Callback;

    .line 510
    invoke-interface {v0, v1, p1}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 511
    return-void
.end method

.method private reportResponseToClient([B)V
    .locals 3
    .param p1, "response"    # [B

    .prologue
    const/4 v2, 0x0

    .line 494
    iget-object v1, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentClientRequest:Ljava/lang/Object;

    .line 495
    .local v1, "clientRequest":Ljava/lang/Object;
    iget-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentClientCallback:Lcom/google/android/videos/async/Callback;

    .line 496
    .local v0, "clientCallback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Ljava/lang/Object;[B>;"
    iput-object v2, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentClientRequest:Ljava/lang/Object;

    .line 497
    iput-object v2, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentClientCallback:Lcom/google/android/videos/async/Callback;

    .line 500
    invoke-interface {v0, v1, p1}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 501
    return-void
.end method

.method private requestLicenseInternal()V
    .locals 13

    .prologue
    .line 409
    const/16 v0, 0x8

    :try_start_0
    invoke-direct {p0, v0}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->recordTaskStart(I)V
    :try_end_0
    .catch Landroid/media/NotProvisionedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 411
    const/4 v12, 0x0

    .line 413
    .local v12, "success":Z
    :try_start_1
    iget v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->keyRequestType:I

    const/4 v3, 0x3

    if-ne v0, v3, :cond_1

    iget-object v1, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->keySetId:[B

    .line 414
    .local v1, "scope":[B
    :goto_0
    iget-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->mediaDrm:Landroid/media/MediaDrm;

    iget-object v2, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->widevinePsshData:[B

    iget-object v3, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->mimeType:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->keyRequestType:I

    iget-object v5, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->optionalKeyRequestParameters:Ljava/util/HashMap;

    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaDrm;->getKeyRequest([B[BLjava/lang/String;ILjava/util/HashMap;)Landroid/media/MediaDrm$KeyRequest;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v11

    .line 416
    .local v11, "keyRequest":Landroid/media/MediaDrm$KeyRequest;
    const/4 v12, 0x1

    .line 418
    const/16 v0, 0x8

    :try_start_2
    invoke-direct {p0, v0, v12}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->recordTaskEnd(IZ)V

    .line 421
    invoke-virtual {v11}, Landroid/media/MediaDrm$KeyRequest;->getDefaultUrl()Ljava/lang/String;

    move-result-object v5

    .line 422
    .local v5, "licenseUrl":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 423
    iget-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->videoId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->config:Lcom/google/android/videos/Config;

    invoke-interface {v3}, Lcom/google/android/videos/Config;->wvCencDrmServerUri()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v3}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->buildLicenseUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 426
    :cond_0
    new-instance v2, Lcom/google/android/videos/api/CencLicenseRequest;

    iget v3, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->keyRequestType:I

    iget-object v4, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->account:Ljava/lang/String;

    invoke-virtual {v11}, Landroid/media/MediaDrm$KeyRequest;->getData()[B

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->keySetId:[B

    if-eqz v0, :cond_2

    const/4 v7, 0x1

    :goto_1
    iget-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->activation:Lcom/google/android/videos/drm/Activation;

    if-nez v0, :cond_3

    const-wide/16 v8, 0x0

    :goto_2
    invoke-direct/range {v2 .. v9}, Lcom/google/android/videos/api/CencLicenseRequest;-><init>(ILjava/lang/String;Ljava/lang/String;[BZJ)V

    .line 429
    .local v2, "licenseRequest":Lcom/google/android/videos/api/CencLicenseRequest;
    iput-object v2, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentInternalRequest:Ljava/lang/Object;

    .line 430
    iget-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->licenseRequester:Lcom/google/android/videos/async/Requester;

    iget-object v3, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->licenseCallback:Lcom/google/android/videos/async/Callback;

    invoke-interface {v0, v2, v3}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    :try_end_2
    .catch Landroid/media/NotProvisionedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 437
    .end local v1    # "scope":[B
    .end local v2    # "licenseRequest":Lcom/google/android/videos/api/CencLicenseRequest;
    .end local v5    # "licenseUrl":Ljava/lang/String;
    .end local v11    # "keyRequest":Landroid/media/MediaDrm$KeyRequest;
    .end local v12    # "success":Z
    :goto_3
    return-void

    .line 413
    .restart local v12    # "success":Z
    :cond_1
    :try_start_3
    iget-object v1, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->sessionId:[B
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 418
    :catchall_0
    move-exception v0

    const/16 v3, 0x8

    :try_start_4
    invoke-direct {p0, v3, v12}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->recordTaskEnd(IZ)V

    throw v0
    :try_end_4
    .catch Landroid/media/NotProvisionedException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .line 431
    .end local v12    # "success":Z
    :catch_0
    move-exception v10

    .line 432
    .local v10, "e":Landroid/media/NotProvisionedException;
    const/4 v0, 0x2

    invoke-direct {p0, v0, v10}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->requestProvisioning(ILandroid/media/NotProvisionedException;)V

    goto :goto_3

    .line 426
    .end local v10    # "e":Landroid/media/NotProvisionedException;
    .restart local v1    # "scope":[B
    .restart local v5    # "licenseUrl":Ljava/lang/String;
    .restart local v11    # "keyRequest":Landroid/media/MediaDrm$KeyRequest;
    .restart local v12    # "success":Z
    :cond_2
    const/4 v7, 0x0

    goto :goto_1

    :cond_3
    :try_start_5
    iget-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->activation:Lcom/google/android/videos/drm/Activation;

    invoke-virtual {v0}, Lcom/google/android/videos/drm/Activation;->elapsedMillis()J
    :try_end_5
    .catch Landroid/media/NotProvisionedException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    move-result-wide v8

    goto :goto_2

    .line 433
    .end local v1    # "scope":[B
    .end local v5    # "licenseUrl":Ljava/lang/String;
    .end local v11    # "keyRequest":Landroid/media/MediaDrm$KeyRequest;
    .end local v12    # "success":Z
    :catch_1
    move-exception v10

    .line 434
    .local v10, "e":Ljava/lang/Exception;
    const-string v0, "Unexpected error during license request"

    invoke-static {v0, v10}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 435
    invoke-direct {p0, v10}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->reportErrorToClient(Ljava/lang/Exception;)V

    goto :goto_3
.end method

.method private requestProvisioning(ILandroid/media/NotProvisionedException;)V
    .locals 8
    .param p1, "reason"    # I
    .param p2, "exception"    # Landroid/media/NotProvisionedException;

    .prologue
    .line 286
    iget-boolean v6, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentClientRequestHasTriedProvisioning:Z

    if-eqz v6, :cond_0

    .line 288
    invoke-direct {p0, p2}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->reportErrorToClient(Ljava/lang/Exception;)V

    .line 311
    :goto_0
    return-void

    .line 291
    :cond_0
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentClientRequestHasTriedProvisioning:Z

    .line 292
    iput p1, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentInternalProvisionRequestReason:I

    .line 295
    :try_start_0
    iget-object v6, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->mediaDrm:Landroid/media/MediaDrm;

    invoke-virtual {v6}, Landroid/media/MediaDrm;->getProvisionRequest()Landroid/media/MediaDrm$ProvisionRequest;

    move-result-object v4

    .line 296
    .local v4, "provisioningRequest":Landroid/media/MediaDrm$ProvisionRequest;
    iget-object v6, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->config:Lcom/google/android/videos/Config;

    invoke-virtual {v4}, Landroid/media/MediaDrm$ProvisionRequest;->getDefaultUrl()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/google/android/videos/Config;->wvProvisioningServerUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 297
    .local v0, "baseUri":Ljava/lang/String;
    new-instance v1, Ljava/lang/String;

    invoke-virtual {v4}, Landroid/media/MediaDrm$ProvisionRequest;->getData()[B

    move-result-object v6

    const-string v7, "UTF-8"

    invoke-direct {v1, v6, v7}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 298
    .local v1, "dataString":Ljava/lang/String;
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    const-string v7, "signedRequest"

    invoke-virtual {v6, v7, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    .line 301
    .local v5, "uri":Landroid/net/Uri;
    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/google/android/videos/async/HttpUriRequestFactory;->createPost(Landroid/net/Uri;[B)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v3

    .line 302
    .local v3, "httpProvisioningRequest":Lorg/apache/http/client/methods/HttpUriRequest;
    iput-object v3, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentInternalRequest:Ljava/lang/Object;

    .line 303
    iget-object v6, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->provisioningRequester:Lcom/google/android/videos/async/Requester;

    iget-object v7, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->provisioningCallback:Lcom/google/android/videos/async/Callback;

    invoke-interface {v6, v3, v7}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 304
    .end local v0    # "baseUri":Ljava/lang/String;
    .end local v1    # "dataString":Ljava/lang/String;
    .end local v3    # "httpProvisioningRequest":Lorg/apache/http/client/methods/HttpUriRequest;
    .end local v4    # "provisioningRequest":Landroid/media/MediaDrm$ProvisionRequest;
    .end local v5    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v2

    .line 306
    .local v2, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v6, Ljava/lang/RuntimeException;

    invoke-direct {v6, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v6

    .line 307
    .end local v2    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v2

    .line 308
    .local v2, "e":Ljava/lang/Exception;
    const-string v6, "Unexpected error during provision request"

    invoke-static {v6, v2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 309
    invoke-direct {p0, v2}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->reportErrorToClient(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private shouldHandle(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "request"    # Ljava/lang/Object;

    .prologue
    .line 539
    iget-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentInternalRequest:Ljava/lang/Object;

    if-ne p1, v0, :cond_0

    .line 540
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentInternalRequest:Ljava/lang/Object;

    .line 541
    const/4 v0, 0x1

    .line 543
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private toLogString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 515
    const/16 v1, 0x3f

    invoke-virtual {p2, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 516
    .local v0, "indexOfQuery":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 517
    const/4 v1, 0x0

    invoke-virtual {p2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p2

    .line 520
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->videoId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->account:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/videos/utils/Hashing;->sha1(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentClientRequest:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 2

    .prologue
    .line 259
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->opened:Z

    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 260
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->opened:Z

    .line 261
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->widevinePsshData:[B

    .line 262
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentInternalRequest:Ljava/lang/Object;

    .line 263
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentClientRequest:Ljava/lang/Object;

    .line 264
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentClientCallback:Lcom/google/android/videos/async/Callback;

    .line 265
    iget-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->mediaDrm:Landroid/media/MediaDrm;

    iget-object v1, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->sessionId:[B

    invoke-virtual {v0, v1}, Landroid/media/MediaDrm;->closeSession([B)V

    .line 266
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->sessionId:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 267
    monitor-exit p0

    return-void

    .line 259
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getSecurityLevel()I
    .locals 1

    .prologue
    .line 202
    iget v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->securityLevel:I

    return v0
.end method

.method public declared-synchronized getSessionId()[B
    .locals 1

    .prologue
    .line 209
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->sessionId:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized loadPlaybackLicense(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    .locals 2
    .param p1, "request"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/lang/Object;",
            "[B>;)V"
        }
    .end annotation

    .prologue
    .line 360
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Ljava/lang/Object;[B>;"
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->opened:Z

    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 361
    iget v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->keyRequestType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->keySetId:[B

    if-eqz v0, :cond_0

    .line 362
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->restoreLicense(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 366
    :goto_0
    monitor-exit p0

    return-void

    .line 364
    :cond_0
    :try_start_1
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->requestLicense(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 360
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized onLicenseError(Lcom/google/android/videos/api/CencLicenseRequest;Ljava/lang/Exception;)V
    .locals 4
    .param p1, "request"    # Lcom/google/android/videos/api/CencLicenseRequest;
    .param p2, "e"    # Ljava/lang/Exception;

    .prologue
    .line 479
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->shouldHandle(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 491
    :goto_0
    monitor-exit p0

    return-void

    .line 483
    :cond_0
    :try_start_1
    instance-of v2, p2, Lcom/google/android/videos/api/CencLicenseException;

    if-eqz v2, :cond_1

    .line 484
    move-object v0, p2

    check-cast v0, Lcom/google/android/videos/api/CencLicenseException;

    move-object v1, v0

    .line 485
    .local v1, "cencLicenseException":Lcom/google/android/videos/api/CencLicenseException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "License status error ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, Lcom/google/android/videos/api/CencLicenseException;->statusCode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/google/android/videos/api/CencLicenseRequest;->url:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->toLogString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 490
    .end local v1    # "cencLicenseException":Lcom/google/android/videos/api/CencLicenseException;
    :goto_1
    invoke-direct {p0, p2}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->reportErrorToClient(Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 479
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 488
    :cond_1
    :try_start_2
    const-string v2, "License request error"

    iget-object v3, p1, Lcom/google/android/videos/api/CencLicenseRequest;->url:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->toLogString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method declared-synchronized onLicenseResponse(Lcom/google/android/videos/api/CencLicenseRequest;[B)V
    .locals 7
    .param p1, "request"    # Lcom/google/android/videos/api/CencLicenseRequest;
    .param p2, "response"    # [B

    .prologue
    const/4 v6, 0x2

    .line 447
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->shouldHandle(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-nez v4, :cond_0

    .line 476
    :goto_0
    monitor-exit p0

    return-void

    .line 451
    :cond_0
    :try_start_1
    iget v4, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->keyRequestType:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_3

    iget-object v2, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->keySetId:[B

    .line 452
    .local v2, "scope":[B
    :goto_1
    const/16 v4, 0x9

    invoke-direct {p0, v4}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->recordTaskStart(I)V
    :try_end_1
    .catch Landroid/media/NotProvisionedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/media/DeniedByServerException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 454
    const/4 v3, 0x0

    .line 456
    .local v3, "success":Z
    :try_start_2
    iget-object v4, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->mediaDrm:Landroid/media/MediaDrm;

    invoke-virtual {v4, v2, p2}, Landroid/media/MediaDrm;->provideKeyResponse([B[B)[B
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v1

    .line 457
    .local v1, "keySetId":[B
    const/4 v3, 0x1

    .line 459
    const/16 v4, 0x9

    :try_start_3
    invoke-direct {p0, v4, v3}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->recordTaskEnd(IZ)V

    .line 461
    iget v4, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->keyRequestType:I

    if-ne v4, v6, :cond_2

    if-eqz v1, :cond_1

    array-length v4, v1

    if-nez v4, :cond_2

    .line 464
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->keySetId:[B

    .line 466
    :cond_2
    invoke-direct {p0, v1}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->reportResponseToClient([B)V
    :try_end_3
    .catch Landroid/media/NotProvisionedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Landroid/media/DeniedByServerException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 467
    .end local v1    # "keySetId":[B
    .end local v2    # "scope":[B
    .end local v3    # "success":Z
    :catch_0
    move-exception v0

    .line 468
    .local v0, "e":Landroid/media/NotProvisionedException;
    const/4 v4, 0x2

    :try_start_4
    invoke-direct {p0, v4, v0}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->requestProvisioning(ILandroid/media/NotProvisionedException;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 447
    .end local v0    # "e":Landroid/media/NotProvisionedException;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 451
    :cond_3
    :try_start_5
    iget-object v2, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->sessionId:[B

    goto :goto_1

    .line 459
    .restart local v2    # "scope":[B
    .restart local v3    # "success":Z
    :catchall_1
    move-exception v4

    const/16 v5, 0x9

    invoke-direct {p0, v5, v3}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->recordTaskEnd(IZ)V

    throw v4
    :try_end_5
    .catch Landroid/media/NotProvisionedException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Landroid/media/DeniedByServerException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 469
    .end local v2    # "scope":[B
    .end local v3    # "success":Z
    :catch_1
    move-exception v0

    .line 470
    .local v0, "e":Landroid/media/DeniedByServerException;
    :try_start_6
    const-string v4, "License denied by server"

    iget-object v5, p1, Lcom/google/android/videos/api/CencLicenseRequest;->url:Ljava/lang/String;

    invoke-direct {p0, v4, v5}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->toLogString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 471
    invoke-direct {p0, v0}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->reportErrorToClient(Ljava/lang/Exception;)V

    goto :goto_0

    .line 472
    .end local v0    # "e":Landroid/media/DeniedByServerException;
    :catch_2
    move-exception v0

    .line 473
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "Unexpected error when providing the key response"

    iget-object v5, p1, Lcom/google/android/videos/api/CencLicenseRequest;->url:Ljava/lang/String;

    invoke-direct {p0, v4, v5}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->toLogString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 474
    invoke-direct {p0, v0}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->reportErrorToClient(Ljava/lang/Exception;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0
.end method

.method declared-synchronized onProvisioningError(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/Exception;)V
    .locals 2
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p2, "e"    # Ljava/lang/Exception;

    .prologue
    .line 340
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->shouldHandle(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 345
    :goto_0
    monitor-exit p0

    return-void

    .line 343
    :cond_0
    :try_start_1
    const-string v0, "Provisioning request error"

    invoke-interface {p1}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->toLogString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 344
    invoke-direct {p0, p2}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->reportErrorToClient(Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 340
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized onProvisioningResponse(Lorg/apache/http/client/methods/HttpUriRequest;[B)V
    .locals 4
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p2, "response"    # [B

    .prologue
    .line 314
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->shouldHandle(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    .line 337
    :goto_0
    monitor-exit p0

    return-void

    .line 318
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->mediaDrm:Landroid/media/MediaDrm;

    invoke-virtual {v1, p2}, Landroid/media/MediaDrm;->provideProvisionResponse([B)V

    .line 319
    iget v1, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentInternalProvisionRequestReason:I

    packed-switch v1, :pswitch_data_0

    .line 330
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown provision reason: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentInternalProvisionRequestReason:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catch Landroid/media/DeniedByServerException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 333
    :catch_0
    move-exception v0

    .line 334
    .local v0, "e":Landroid/media/DeniedByServerException;
    :try_start_2
    const-string v1, "Provisioning denied by server"

    invoke-interface {p1}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->toLogString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 335
    invoke-direct {p0, v0}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->reportErrorToClient(Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 314
    .end local v0    # "e":Landroid/media/DeniedByServerException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 321
    :pswitch_0
    const/4 v1, 0x0

    :try_start_3
    invoke-direct {p0, v1}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->reportResponseToClient([B)V

    goto :goto_0

    .line 324
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->openInternal()V

    goto :goto_0

    .line 327
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->requestLicenseInternal()V
    :try_end_3
    .catch Landroid/media/DeniedByServerException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 319
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public declared-synchronized open(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    .locals 2
    .param p2, "mimeType"    # Ljava/lang/String;
    .param p3, "request"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/util/UUID;",
            "[B>;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/lang/Object;",
            "[B>;)V"
        }
    .end annotation

    .prologue
    .local p1, "psshData":Ljava/util/Map;, "Ljava/util/Map<Ljava/util/UUID;[B>;"
    .local p4, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Ljava/lang/Object;[B>;"
    const/4 v0, 0x0

    .line 229
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->opened:Z

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 230
    iput-object p3, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentClientRequest:Ljava/lang/Object;

    .line 231
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Callback;

    iput-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentClientCallback:Lcom/google/android/videos/async/Callback;

    .line 232
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentClientRequestHasTriedProvisioning:Z

    .line 233
    iput-object p2, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->mimeType:Ljava/lang/String;

    .line 234
    sget-object v0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->WIDEVINE_UUID:Ljava/util/UUID;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->widevinePsshData:[B

    .line 235
    iget-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->widevinePsshData:[B

    if-nez v0, :cond_1

    .line 236
    new-instance v0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$NoWidevinePsshException;

    invoke-direct {v0}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$NoWidevinePsshException;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->reportErrorToClient(Ljava/lang/Exception;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 240
    :goto_0
    monitor-exit p0

    return-void

    .line 239
    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->openInternal()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 229
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized requestLicense(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    .locals 1
    .param p1, "request"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/lang/Object;",
            "[B>;)V"
        }
    .end annotation

    .prologue
    .line 400
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Ljava/lang/Object;[B>;"
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->opened:Z

    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 401
    iput-object p1, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentClientRequest:Ljava/lang/Object;

    .line 402
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Callback;

    iput-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentClientCallback:Lcom/google/android/videos/async/Callback;

    .line 403
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentClientRequestHasTriedProvisioning:Z

    .line 404
    invoke-direct {p0}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->requestLicenseInternal()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 405
    monitor-exit p0

    return-void

    .line 400
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized requestProvisioning(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    .locals 2
    .param p1, "request"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/lang/Object;",
            "[B>;)V"
        }
    .end annotation

    .prologue
    .line 278
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Ljava/lang/Object;[B>;"
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->opened:Z

    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 279
    iput-object p1, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentClientRequest:Ljava/lang/Object;

    .line 280
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Callback;

    iput-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentClientCallback:Lcom/google/android/videos/async/Callback;

    .line 281
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentClientRequestHasTriedProvisioning:Z

    .line 282
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->requestProvisioning(ILandroid/media/NotProvisionedException;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 283
    monitor-exit p0

    return-void

    .line 278
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized restoreLicense(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    .locals 4
    .param p1, "request"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/lang/Object;",
            "[B>;)V"
        }
    .end annotation

    .prologue
    .line 378
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Ljava/lang/Object;[B>;"
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->opened:Z

    invoke-static {v1}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 379
    iput-object p1, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentClientRequest:Ljava/lang/Object;

    .line 380
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/async/Callback;

    iput-object v1, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentClientCallback:Lcom/google/android/videos/async/Callback;

    .line 381
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->currentClientRequestHasTriedProvisioning:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 383
    :try_start_1
    iget-object v1, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->mediaDrm:Landroid/media/MediaDrm;

    iget-object v2, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->sessionId:[B

    iget-object v3, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->keySetId:[B

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaDrm;->restoreKeys([B[B)V

    .line 384
    iget-object v1, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->keySetId:[B

    invoke-direct {p0, v1}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->reportResponseToClient([B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 388
    :goto_0
    monitor-exit p0

    return-void

    .line 385
    :catch_0
    move-exception v0

    .line 386
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    new-instance v1, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$InvalidKeySetIdException;

    invoke-direct {v1, v0}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$InvalidKeySetIdException;-><init>(Ljava/lang/Throwable;)V

    invoke-interface {p2, p1, v1}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 378
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized setActivation(Lcom/google/android/videos/drm/Activation;)V
    .locals 1
    .param p1, "activation"    # Lcom/google/android/videos/drm/Activation;

    .prologue
    .line 218
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->activation:Lcom/google/android/videos/drm/Activation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 219
    monitor-exit p0

    return-void

    .line 218
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
