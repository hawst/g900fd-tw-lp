.class public Lcom/google/android/videos/activity/DmaReconsentFragment$InProgressDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "DmaReconsentFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/activity/DmaReconsentFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "InProgressDialogFragment"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 205
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static dismiss(Landroid/support/v4/app/FragmentActivity;)V
    .locals 4
    .param p0, "activity"    # Landroid/support/v4/app/FragmentActivity;

    .prologue
    .line 216
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "InProgressDialogFragment"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 217
    .local v1, "fragment":Landroid/support/v4/app/Fragment;
    instance-of v2, v1, Landroid/support/v4/app/DialogFragment;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 218
    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 219
    .local v0, "dialogFragment":Landroid/support/v4/app/DialogFragment;
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    .line 221
    .end local v0    # "dialogFragment":Landroid/support/v4/app/DialogFragment;
    :cond_0
    return-void
.end method

.method public static showInstance(Landroid/support/v4/app/FragmentActivity;)V
    .locals 3
    .param p0, "activity"    # Landroid/support/v4/app/FragmentActivity;

    .prologue
    .line 211
    new-instance v0, Lcom/google/android/videos/activity/DmaReconsentFragment$InProgressDialogFragment;

    invoke-direct {v0}, Lcom/google/android/videos/activity/DmaReconsentFragment$InProgressDialogFragment;-><init>()V

    .line 212
    .local v0, "instance":Lcom/google/android/videos/activity/DmaReconsentFragment$InProgressDialogFragment;
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "InProgressDialogFragment"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/activity/DmaReconsentFragment$InProgressDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 213
    return-void
.end method


# virtual methods
.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 235
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 236
    invoke-virtual {p0}, Lcom/google/android/videos/activity/DmaReconsentFragment$InProgressDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/activity/DmaReconsentFragment;->getInstance(Landroid/support/v4/app/FragmentActivity;)Lcom/google/android/videos/activity/DmaReconsentFragment;

    move-result-object v0

    .line 237
    .local v0, "dmaReconsentFragment":Lcom/google/android/videos/activity/DmaReconsentFragment;
    if-eqz v0, :cond_0

    .line 238
    invoke-virtual {v0}, Lcom/google/android/videos/activity/DmaReconsentFragment;->onCancel()V

    .line 240
    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 225
    invoke-virtual {p0}, Lcom/google/android/videos/activity/DmaReconsentFragment$InProgressDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 226
    .local v0, "activity":Landroid/app/Activity;
    new-instance v1, Landroid/app/Dialog;

    invoke-direct {v1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 227
    .local v1, "dialog":Landroid/app/Dialog;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 228
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const v3, 0x7f0a00c7

    invoke-virtual {v2, v3}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 229
    new-instance v2, Landroid/widget/ProgressBar;

    invoke-virtual {v1}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 230
    return-object v1
.end method
