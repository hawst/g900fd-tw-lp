.class public Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadSingle;
.super Ljava/lang/Object;
.source "DownloadNotificationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pinning/DownloadNotificationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DownloadSingle"
.end annotation


# instance fields
.field public final account:Ljava/lang/String;

.field public final poster:Landroid/graphics/Bitmap;

.field public final seasonId:Ljava/lang/String;

.field public final showId:Ljava/lang/String;

.field public final showTitle:Ljava/lang/String;

.field public final title:Ljava/lang/String;

.field public final videoId:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)V
    .locals 1
    .param p1, "videoData"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->account:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$000(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadSingle;->account:Ljava/lang/String;

    .line 82
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->seasonId:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$100(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadSingle;->seasonId:Ljava/lang/String;

    .line 83
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->videoId:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$200(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadSingle;->videoId:Ljava/lang/String;

    .line 84
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->showId:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$300(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadSingle;->showId:Ljava/lang/String;

    .line 85
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->title:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$400(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadSingle;->title:Ljava/lang/String;

    .line 86
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->showTitle:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$500(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadSingle;->showTitle:Ljava/lang/String;

    .line 87
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->poster:Landroid/graphics/Bitmap;
    invoke-static {p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$600(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadSingle;->poster:Landroid/graphics/Bitmap;

    .line 88
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;Lcom/google/android/videos/pinning/DownloadNotificationManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
    .param p2, "x1"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$1;

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadSingle;-><init>(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)V

    return-void
.end method
