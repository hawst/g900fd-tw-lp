.class public Lcom/google/android/videos/activity/CaptionSettingsActivity;
.super Lcom/google/android/videos/activity/VideosPreferenceActivity;
.source "CaptionSettingsActivity.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private assetManager:Landroid/content/res/AssetManager;

.field private captionPreferences:Lcom/google/android/videos/player/CaptionPreferences;

.field private captionPreview:Lcom/google/android/videos/settings/CaptionPreviewPreference;

.field private captioningPreferenceScreen:Landroid/preference/PreferenceScreen;

.field private customCaptionPreset:Landroid/preference/PreferenceCategory;

.field private eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private preferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/videos/activity/VideosPreferenceActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/activity/CaptionSettingsActivity;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/activity/CaptionSettingsActivity;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->preferences:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method private addColors(Ljava/lang/String;ZI)V
    .locals 9
    .param p1, "preference"    # Ljava/lang/String;
    .param p2, "addNoneColor"    # Z
    .param p3, "defaultColor"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 104
    iget-object v7, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->captioningPreferenceScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v7, p1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Lcom/google/android/videos/settings/ColorPreference;

    .line 105
    .local v5, "pref":Lcom/google/android/videos/settings/ColorPreference;
    if-eqz p2, :cond_1

    const/4 v2, 0x2

    .line 107
    .local v2, "newColorCount":I
    :goto_0
    invoke-virtual {v5}, Lcom/google/android/videos/settings/ColorPreference;->getTitles()[Ljava/lang/CharSequence;

    move-result-object v0

    .line 108
    .local v0, "colorTitles":[Ljava/lang/CharSequence;
    invoke-virtual {v5}, Lcom/google/android/videos/settings/ColorPreference;->getValues()[I

    move-result-object v1

    .line 109
    .local v1, "colorValues":[I
    array-length v7, v1

    add-int/2addr v7, v2

    new-array v4, v7, [I

    .line 110
    .local v4, "newColorValues":[I
    array-length v7, v0

    add-int/2addr v7, v2

    new-array v3, v7, [Ljava/lang/CharSequence;

    .line 111
    .local v3, "newColorTitles":[Ljava/lang/CharSequence;
    array-length v7, v1

    invoke-static {v1, v8, v4, v2, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 112
    array-length v7, v0

    invoke-static {v0, v8, v3, v2, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 113
    aput v6, v4, v8

    .line 114
    const v7, 0x7f0b0253

    invoke-virtual {p0, v7}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v8

    .line 115
    if-eqz p2, :cond_0

    .line 116
    aput v8, v4, v6

    .line 117
    const v7, 0x7f0b0252

    invoke-virtual {p0, v7}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v6

    .line 119
    :cond_0
    invoke-virtual {v5, v3}, Lcom/google/android/videos/settings/ColorPreference;->setTitles([Ljava/lang/CharSequence;)V

    .line 120
    invoke-virtual {v5, v4}, Lcom/google/android/videos/settings/ColorPreference;->setValues([I)V

    .line 121
    invoke-virtual {v5, p3}, Lcom/google/android/videos/settings/ColorPreference;->setUseTrackSettingsColor(I)V

    .line 122
    return-void

    .end local v0    # "colorTitles":[Ljava/lang/CharSequence;
    .end local v1    # "colorValues":[I
    .end local v2    # "newColorCount":I
    .end local v3    # "newColorTitles":[Ljava/lang/CharSequence;
    .end local v4    # "newColorValues":[I
    :cond_1
    move v2, v6

    .line 105
    goto :goto_0
.end method

.method private belongsToGroup(Landroid/preference/PreferenceGroup;Ljava/lang/String;)Z
    .locals 4
    .param p1, "group"    # Landroid/preference/PreferenceGroup;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 228
    invoke-virtual {p1}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v2

    .line 229
    .local v2, "preferenceCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_2

    .line 230
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    .line 231
    .local v1, "preference":Landroid/preference/Preference;
    invoke-virtual {v1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    instance-of v3, v1, Landroid/preference/PreferenceGroup;

    if-eqz v3, :cond_1

    check-cast v1, Landroid/preference/PreferenceGroup;

    .end local v1    # "preference":Landroid/preference/Preference;
    invoke-direct {p0, v1, p2}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->belongsToGroup(Landroid/preference/PreferenceGroup;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 234
    :cond_0
    const/4 v3, 0x1

    .line 237
    :goto_1
    return v3

    .line 229
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 237
    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private static getOpacity(I)I
    .locals 1
    .param p0, "color"    # I

    .prologue
    const/high16 v0, -0x1000000

    .line 160
    if-nez p0, :cond_0

    :goto_0
    return v0

    :cond_0
    and-int/2addr v0, p0

    goto :goto_0
.end method

.method private static getSolidColor(I)I
    .locals 1
    .param p0, "color"    # I

    .prologue
    .line 165
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/high16 v0, -0x1000000

    or-int/2addr v0, p0

    goto :goto_0
.end method

.method private installActionBarToggleSwitch()V
    .locals 9

    .prologue
    const/16 v8, 0x10

    const/4 v7, -0x2

    const/4 v6, 0x0

    .line 241
    new-instance v3, Landroid/widget/Switch;

    invoke-direct {v3, p0}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    .line 242
    .local v3, "toggleSwitch":Landroid/widget/Switch;
    invoke-virtual {p0}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e01d8

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 244
    .local v2, "rightPadding":I
    invoke-virtual {v3, v6, v6, v2, v6}, Landroid/widget/Switch;->setPadding(IIII)V

    .line 245
    iget-object v4, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->preferences:Landroid/content/SharedPreferences;

    const-string v5, "captioning_enabled"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/Switch;->setChecked(Z)V

    .line 246
    new-instance v4, Lcom/google/android/videos/activity/CaptionSettingsActivity$1;

    invoke-direct {v4, p0}, Lcom/google/android/videos/activity/CaptionSettingsActivity$1;-><init>(Lcom/google/android/videos/activity/CaptionSettingsActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 255
    invoke-virtual {p0}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 256
    .local v0, "actionBar":Landroid/app/ActionBar;
    invoke-virtual {v0, v8, v8}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 257
    new-instance v1, Landroid/app/ActionBar$LayoutParams;

    const v4, 0x800015

    invoke-direct {v1, v7, v7, v4}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    .line 260
    .local v1, "layoutParams":Landroid/app/ActionBar$LayoutParams;
    invoke-virtual {v0, v3, v1}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 261
    return-void
.end method

.method private setColorPreference(Ljava/lang/String;I)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->captioningPreferenceScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/settings/ColorPreference;

    invoke-virtual {v0, p2}, Lcom/google/android/videos/settings/ColorPreference;->setValue(I)V

    .line 172
    return-void
.end method

.method private setListPreference(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->captioningPreferenceScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    invoke-virtual {v0, p2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 178
    return-void
.end method

.method private updateCaptionEnabled()V
    .locals 4

    .prologue
    .line 219
    iget-object v1, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->preferences:Landroid/content/SharedPreferences;

    const-string v2, "captioning_enabled"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 220
    .local v0, "captionEnabled":Z
    iget-object v1, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->captioningPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-string v2, "captioning_standard"

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 221
    iget-object v1, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->customCaptionPreset:Landroid/preference/PreferenceCategory;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->setEnabled(Z)V

    .line 222
    return-void
.end method

.method private updateListPreferenceSummary(Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 182
    iget-object v1, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->captioningPreferenceScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, p1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    .line 183
    .local v0, "preference":Landroid/preference/ListPreference;
    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 184
    invoke-virtual {v0}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 185
    return-void
.end method

.method private updatePreferences()V
    .locals 12

    .prologue
    .line 125
    const/4 v9, -0x1

    iget-object v10, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->preferences:Landroid/content/SharedPreferences;

    iget-object v11, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->assetManager:Landroid/content/res/AssetManager;

    invoke-static {v9, v10, v11}, Lcom/google/android/videos/subtitles/CaptionStyleUtil;->createCaptionStyleFromPreferences(ILandroid/content/SharedPreferences;Landroid/content/res/AssetManager;)Lcom/google/android/exoplayer/text/CaptionStyleCompat;

    move-result-object v6

    .line 127
    .local v6, "style":Lcom/google/android/exoplayer/text/CaptionStyleCompat;
    iget-object v9, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->preferences:Landroid/content/SharedPreferences;

    const-string v10, "captioning_background_color"

    iget v11, v6, Lcom/google/android/exoplayer/text/CaptionStyleCompat;->backgroundColor:I

    invoke-static {v11}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->getSolidColor(I)I

    move-result v11

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 129
    .local v1, "backgroundSolidColor":I
    iget-object v9, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->preferences:Landroid/content/SharedPreferences;

    const-string v10, "captioning_background_opacity"

    iget v11, v6, Lcom/google/android/exoplayer/text/CaptionStyleCompat;->backgroundColor:I

    invoke-static {v11}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->getOpacity(I)I

    move-result v11

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 131
    .local v0, "backgroundOpacity":I
    iget-object v9, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->preferences:Landroid/content/SharedPreferences;

    const-string v10, "captioning_window_color"

    iget v11, v6, Lcom/google/android/exoplayer/text/CaptionStyleCompat;->windowColor:I

    invoke-static {v11}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->getSolidColor(I)I

    move-result v11

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    .line 133
    .local v8, "windowSolidColor":I
    iget-object v9, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->preferences:Landroid/content/SharedPreferences;

    const-string v10, "captioning_window_opacity"

    iget v11, v6, Lcom/google/android/exoplayer/text/CaptionStyleCompat;->windowColor:I

    invoke-static {v11}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->getOpacity(I)I

    move-result v11

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    .line 135
    .local v7, "windowOpacity":I
    iget-object v9, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->preferences:Landroid/content/SharedPreferences;

    const-string v10, "captioning_foreground_color"

    iget v11, v6, Lcom/google/android/exoplayer/text/CaptionStyleCompat;->foregroundColor:I

    invoke-static {v11}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->getSolidColor(I)I

    move-result v11

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 137
    .local v4, "foregroundSolidColor":I
    iget-object v9, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->preferences:Landroid/content/SharedPreferences;

    const-string v10, "captioning_foreground_opacity"

    iget v11, v6, Lcom/google/android/exoplayer/text/CaptionStyleCompat;->foregroundColor:I

    invoke-static {v11}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->getOpacity(I)I

    move-result v11

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 139
    .local v3, "foregroundOpacity":I
    iget-object v9, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->preferences:Landroid/content/SharedPreferences;

    const-string v10, "captioning_typeface"

    const-string v11, ""

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 140
    .local v5, "rawTypeface":Ljava/lang/String;
    iget-object v9, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->preferences:Landroid/content/SharedPreferences;

    const-string v10, "captioning_edge_type"

    iget v11, v6, Lcom/google/android/exoplayer/text/CaptionStyleCompat;->edgeType:I

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 142
    .local v2, "edgeType":I
    const-string v9, "captioning_font_scale"

    iget-object v10, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->captionPreferences:Lcom/google/android/videos/player/CaptionPreferences;

    invoke-virtual {v10}, Lcom/google/android/videos/player/CaptionPreferences;->getFontScale()F

    move-result v10

    invoke-static {v10}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v9, v10}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->setListPreference(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    const-string v9, "captioning_foreground_color"

    invoke-direct {p0, v9, v4}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->setColorPreference(Ljava/lang/String;I)V

    .line 145
    const-string v9, "captioning_foreground_opacity"

    invoke-direct {p0, v9, v3}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->setColorPreference(Ljava/lang/String;I)V

    .line 146
    const-string v9, "captioning_background_color"

    invoke-direct {p0, v9, v1}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->setColorPreference(Ljava/lang/String;I)V

    .line 147
    const-string v9, "captioning_background_opacity"

    invoke-direct {p0, v9, v0}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->setColorPreference(Ljava/lang/String;I)V

    .line 148
    const-string v9, "captioning_window_color"

    invoke-direct {p0, v9, v8}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->setColorPreference(Ljava/lang/String;I)V

    .line 149
    const-string v9, "captioning_window_opacity"

    invoke-direct {p0, v9, v7}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->setColorPreference(Ljava/lang/String;I)V

    .line 150
    const-string v9, "captioning_edge_color"

    iget v10, v6, Lcom/google/android/exoplayer/text/CaptionStyleCompat;->edgeColor:I

    invoke-direct {p0, v9, v10}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->setColorPreference(Ljava/lang/String;I)V

    .line 151
    iget-object v9, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->captioningPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-string v10, "captioning_edge_type"

    invoke-virtual {v9, v10}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v9

    check-cast v9, Lcom/google/android/videos/settings/EdgeTypePreference;

    invoke-virtual {v9, v2}, Lcom/google/android/videos/settings/EdgeTypePreference;->setValue(I)V

    .line 154
    const-string v9, "captioning_typeface"

    invoke-direct {p0, v9, v5}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->setListPreference(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    return-void
.end method


# virtual methods
.method protected configureSettings(Lcom/google/android/videos/activity/VideosPreferenceFragment;)V
    .locals 6
    .param p1, "fragment"    # Lcom/google/android/videos/activity/VideosPreferenceFragment;

    .prologue
    const/4 v5, 0x1

    .line 69
    invoke-virtual {p1}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v2

    const-string v3, "youtube"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    .line 72
    const v2, 0x7f080002

    invoke-virtual {p1, v2}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->addPreferencesFromResource(I)V

    .line 74
    invoke-virtual {p1}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->captioningPreferenceScreen:Landroid/preference/PreferenceScreen;

    .line 75
    iget-object v2, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->captioningPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-string v3, "captioning_preview"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/settings/CaptionPreviewPreference;

    iput-object v2, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->captionPreview:Lcom/google/android/videos/settings/CaptionPreviewPreference;

    .line 78
    invoke-direct {p0}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->installActionBarToggleSwitch()V

    .line 80
    iget-object v2, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->captioningPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-string v3, "captioning_custom"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/PreferenceCategory;

    iput-object v2, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->customCaptionPreset:Landroid/preference/PreferenceCategory;

    .line 82
    iget-object v2, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->captioningPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-string v3, "captioning_preset"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/settings/PresetPreference;

    .line 84
    .local v1, "preset":Lcom/google/android/videos/settings/PresetPreference;
    invoke-virtual {v1, p0}, Lcom/google/android/videos/settings/PresetPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 86
    sget-object v0, Lcom/google/android/exoplayer/text/CaptionStyleCompat;->DEFAULT:Lcom/google/android/exoplayer/text/CaptionStyleCompat;

    .line 87
    .local v0, "defStyle":Lcom/google/android/exoplayer/text/CaptionStyleCompat;
    const-string v2, "captioning_foreground_color"

    const/4 v3, 0x0

    iget v4, v0, Lcom/google/android/exoplayer/text/CaptionStyleCompat;->foregroundColor:I

    invoke-direct {p0, v2, v3, v4}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->addColors(Ljava/lang/String;ZI)V

    .line 88
    const-string v2, "captioning_background_color"

    iget v3, v0, Lcom/google/android/exoplayer/text/CaptionStyleCompat;->backgroundColor:I

    invoke-direct {p0, v2, v5, v3}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->addColors(Ljava/lang/String;ZI)V

    .line 89
    const-string v2, "captioning_window_color"

    iget v3, v0, Lcom/google/android/exoplayer/text/CaptionStyleCompat;->windowColor:I

    invoke-direct {p0, v2, v5, v3}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->addColors(Ljava/lang/String;ZI)V

    .line 90
    invoke-direct {p0}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->updatePreferences()V

    .line 92
    const-string v2, "captioning_locale"

    invoke-direct {p0, v2}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->updateListPreferenceSummary(Ljava/lang/String;)V

    .line 93
    const-string v2, "captioning_font_scale"

    invoke-direct {p0, v2}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->updateListPreferenceSummary(Ljava/lang/String;)V

    .line 94
    const-string v2, "captioning_typeface"

    invoke-direct {p0, v2}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->updateListPreferenceSummary(Ljava/lang/String;)V

    .line 96
    invoke-virtual {v1}, Lcom/google/android/videos/settings/PresetPreference;->getValue()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 97
    iget-object v2, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->captioningPreferenceScreen:Landroid/preference/PreferenceScreen;

    iget-object v3, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->customCaptionPreset:Landroid/preference/PreferenceCategory;

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 100
    :cond_0
    invoke-direct {p0}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->updateCaptionEnabled()V

    .line 101
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/google/android/videos/activity/VideosPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 47
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v0

    .line 48
    .local v0, "videosGlobals":Lcom/google/android/videos/VideosGlobals;
    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->preferences:Landroid/content/SharedPreferences;

    .line 49
    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 50
    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getCaptionPreferences()Lcom/google/android/videos/player/CaptionPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->captionPreferences:Lcom/google/android/videos/player/CaptionPreferences;

    .line 51
    invoke-virtual {p0}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->assetManager:Landroid/content/res/AssetManager;

    .line 52
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 63
    invoke-super {p0}, Lcom/google/android/videos/activity/VideosPreferenceActivity;->onPause()V

    .line 64
    iget-object v0, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 65
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    .line 189
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    .line 190
    .local v1, "key":Ljava/lang/String;
    const-string v3, "captioning_preset"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 191
    iget-object v3, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->captionPreview:Lcom/google/android/videos/settings/CaptionPreviewPreference;

    invoke-virtual {v3}, Lcom/google/android/videos/settings/CaptionPreviewPreference;->refresh()V

    .line 192
    const/4 v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {p2, v3}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 193
    iget-object v3, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->captioningPreferenceScreen:Landroid/preference/PreferenceScreen;

    iget-object v4, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->customCaptionPreset:Landroid/preference/PreferenceCategory;

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 205
    .end local p2    # "newValue":Ljava/lang/Object;
    :cond_0
    :goto_0
    const/4 v3, 0x1

    return v3

    .line 195
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_1
    iget-object v3, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->captioningPreferenceScreen:Landroid/preference/PreferenceScreen;

    iget-object v4, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->customCaptionPreset:Landroid/preference/PreferenceCategory;

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0

    .line 197
    :cond_2
    instance-of v3, p1, Landroid/preference/ListPreference;

    if-eqz v3, :cond_3

    move-object v2, p1

    .line 198
    check-cast v2, Landroid/preference/ListPreference;

    .line 199
    .local v2, "listPreference":Landroid/preference/ListPreference;
    check-cast p2, Ljava/lang/String;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {v2, p2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 200
    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 201
    .end local v2    # "listPreference":Landroid/preference/ListPreference;
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_3
    instance-of v3, p1, Lcom/google/android/videos/settings/EdgeTypePreference;

    if-eqz v3, :cond_0

    move-object v0, p1

    .line 202
    check-cast v0, Lcom/google/android/videos/settings/EdgeTypePreference;

    .line 203
    .local v0, "edgeTypePreference":Lcom/google/android/videos/settings/EdgeTypePreference;
    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/videos/settings/EdgeTypePreference;->setValue(I)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 56
    invoke-super {p0}, Lcom/google/android/videos/activity/VideosPreferenceActivity;->onResume()V

    .line 57
    iget-object v0, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    invoke-interface {v0}, Lcom/google/android/videos/logging/EventLogger;->onCaptionSettingsPageOpened()V

    .line 58
    iget-object v0, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 59
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->captioningPreferenceScreen:Landroid/preference/PreferenceScreen;

    invoke-direct {p0, v0, p2}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->belongsToGroup(Landroid/preference/PreferenceGroup;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity;->captionPreview:Lcom/google/android/videos/settings/CaptionPreviewPreference;

    invoke-virtual {v0}, Lcom/google/android/videos/settings/CaptionPreviewPreference;->refresh()V

    .line 213
    :cond_0
    const-string v0, "captioning_enabled"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 214
    invoke-direct {p0}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->updateCaptionEnabled()V

    .line 216
    :cond_1
    return-void
.end method
