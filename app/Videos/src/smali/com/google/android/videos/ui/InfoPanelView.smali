.class public Lcom/google/android/videos/ui/InfoPanelView;
.super Landroid/view/View;
.source "InfoPanelView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 20
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/videos/ui/InfoPanelView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 24
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/videos/ui/InfoPanelView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    invoke-virtual {p0}, Lcom/google/android/videos/ui/InfoPanelView;->getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;

    move-result-object v0

    invoke-interface {v0, p0, p1, p2, p3}, Lcom/google/android/play/cardview/CardViewGroupDelegate;->initialize(Landroid/view/View;Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method


# virtual methods
.method public getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/google/android/play/cardview/CardViewGroupDelegates;->IMPL:Lcom/google/android/play/cardview/CardViewGroupDelegate;

    return-object v0
.end method
