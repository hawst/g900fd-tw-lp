.class public Lcom/google/android/videos/pano/activity/MovieDetailsActivity;
.super Lcom/google/android/videos/pano/activity/BaseDetailsActivity;
.source "MovieDetailsActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;-><init>()V

    return-void
.end method

.method public static createIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 27
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/videos/pano/activity/MovieDetailsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "video_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected addRows(Ljava/util/ArrayList;Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;)V
    .locals 5
    .param p2, "detailsRowHelper"    # Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v17/leanback/widget/Row;",
            ">;",
            "Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;",
            ")V"
        }
    .end annotation

    .prologue
    .line 58
    .local p1, "rows":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/support/v17/leanback/widget/Row;>;"
    iget-object v3, p0, Lcom/google/android/videos/pano/activity/MovieDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v3}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/videos/Config;->suggestionsEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 59
    invoke-static {p0}, Lcom/google/android/videos/pano/ui/PanoHelper;->getDefaultPosterHeight(Landroid/content/Context;)I

    move-result v0

    .line 60
    .local v0, "height":I
    int-to-float v3, v0

    const v4, 0x3f31a787

    mul-float/2addr v3, v4

    float-to-int v2, v3

    .line 61
    .local v2, "width":I
    invoke-virtual {p2, v2, v0}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->getRelatedListRow(II)Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v1

    .line 62
    .local v1, "related":Landroid/support/v17/leanback/widget/ListRow;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/ListRow;->getAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/ListRow;->getAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 63
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 71
    .end local v0    # "height":I
    .end local v1    # "related":Landroid/support/v17/leanback/widget/ListRow;
    .end local v2    # "width":I
    :cond_0
    return-void
.end method

.method protected getListItemHelper()Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;
    .locals 18

    .prologue
    .line 37
    invoke-static/range {p0 .. p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v17

    .line 38
    .local v17, "videosGlobals":Lcom/google/android/videos/VideosGlobals;
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/videos/VideosGlobals;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v16

    .line 39
    .local v16, "apiRequesters":Lcom/google/android/videos/api/ApiRequesters;
    new-instance v1, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/pano/activity/MovieDetailsActivity;->account:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/pano/activity/MovieDetailsActivity;->itemId:Ljava/lang/String;

    invoke-interface/range {v16 .. v16}, Lcom/google/android/videos/api/ApiRequesters;->getAssetsCachingRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v6

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/videos/VideosGlobals;->getRecommendationsRequestFactory()Lcom/google/android/videos/api/RecommendationsRequest$Factory;

    move-result-object v7

    invoke-interface/range {v16 .. v16}, Lcom/google/android/videos/api/ApiRequesters;->getRecommendationsRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v8

    invoke-interface/range {v16 .. v16}, Lcom/google/android/videos/api/ApiRequesters;->getReviewsRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v9

    invoke-interface/range {v16 .. v16}, Lcom/google/android/videos/api/ApiRequesters;->getCategoryListRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v10

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStore()Lcom/google/android/videos/store/PurchaseStore;

    move-result-object v11

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/videos/VideosGlobals;->getWishlistStore()Lcom/google/android/videos/store/WishlistStore;

    move-result-object v12

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/videos/VideosGlobals;->getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v13

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/videos/VideosGlobals;->getBitmapRequesters()Lcom/google/android/videos/bitmap/BitmapRequesters;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/videos/bitmap/BitmapRequesters;->getControllableBitmapRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v14

    const v2, 0x1020002

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/videos/pano/activity/MovieDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v15

    move-object/from16 v2, p0

    move-object/from16 v5, p0

    invoke-direct/range {v1 .. v15}, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$OnPurchaseActionListener;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/api/RecommendationsRequest$Factory;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/WishlistStore;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/async/Requester;Landroid/view/View;)V

    return-object v1
.end method

.method protected getMainItemId(Landroid/content/Intent;)Ljava/lang/String;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 32
    const-string v0, "video_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
