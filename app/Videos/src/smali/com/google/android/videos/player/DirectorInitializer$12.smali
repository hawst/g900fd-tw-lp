.class Lcom/google/android/videos/player/DirectorInitializer$12;
.super Ljava/lang/Object;
.source "DirectorInitializer.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/player/DirectorInitializer;->initOfflineSubtitleTracksCallback()Lcom/google/android/videos/async/Callback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Ljava/lang/String;",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/videos/subtitles/SubtitleTrack;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/player/DirectorInitializer;


# direct methods
.method constructor <init>(Lcom/google/android/videos/player/DirectorInitializer;)V
    .locals 0

    .prologue
    .line 891
    iput-object p1, p0, Lcom/google/android/videos/player/DirectorInitializer$12;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 891
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/player/DirectorInitializer$12;->onError(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public onError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 2
    .param p1, "request"    # Ljava/lang/String;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 899
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Could not load subtitle track list for video with id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 900
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0, p1, v0}, Lcom/google/android/videos/player/DirectorInitializer$12;->onResponse(Ljava/lang/String;Ljava/util/List;)V

    .line 901
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 891
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/util/List;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/player/DirectorInitializer$12;->onResponse(Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method

.method public onResponse(Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .param p1, "request"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 894
    .local p2, "response":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/subtitles/SubtitleTrack;>;"
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer$12;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # setter for: Lcom/google/android/videos/player/DirectorInitializer;->offlineCaptionTracks:Ljava/util/List;
    invoke-static {v0, p2}, Lcom/google/android/videos/player/DirectorInitializer;->access$3902(Lcom/google/android/videos/player/DirectorInitializer;Ljava/util/List;)Ljava/util/List;

    .line 895
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer$12;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # invokes: Lcom/google/android/videos/player/DirectorInitializer;->onTaskCompleted()V
    invoke-static {v0}, Lcom/google/android/videos/player/DirectorInitializer;->access$3300(Lcom/google/android/videos/player/DirectorInitializer;)V

    .line 896
    return-void
.end method
