.class Lcom/google/android/videos/store/SubtitlesClient$1;
.super Ljava/lang/Object;
.source "SubtitlesClient.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/store/SubtitlesClient;->requestSubtitlesInternal(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/subtitles/SubtitleTrack;ILcom/google/android/videos/async/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Landroid/util/Pair",
        "<",
        "Lcom/google/android/videos/subtitles/SubtitleTrack;",
        "Ljava/lang/Integer;",
        ">;",
        "Lcom/google/android/videos/subtitles/Subtitles;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/store/SubtitlesClient;

.field final synthetic val$callback:Lcom/google/android/videos/async/Callback;


# direct methods
.method constructor <init>(Lcom/google/android/videos/store/SubtitlesClient;Lcom/google/android/videos/async/Callback;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/android/videos/store/SubtitlesClient$1;->this$0:Lcom/google/android/videos/store/SubtitlesClient;

    iput-object p2, p0, Lcom/google/android/videos/store/SubtitlesClient$1;->val$callback:Lcom/google/android/videos/async/Callback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/util/Pair;Ljava/lang/Exception;)V
    .locals 2
    .param p2, "exception"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    .prologue
    .line 81
    .local p1, "request":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/videos/subtitles/SubtitleTrack;Ljava/lang/Integer;>;"
    iget-object v0, p0, Lcom/google/android/videos/store/SubtitlesClient$1;->val$callback:Lcom/google/android/videos/async/Callback;

    iget-object v1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-interface {v0, v1, p2}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 82
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 74
    check-cast p1, Landroid/util/Pair;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/store/SubtitlesClient$1;->onError(Landroid/util/Pair;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Landroid/util/Pair;Lcom/google/android/videos/subtitles/Subtitles;)V
    .locals 2
    .param p2, "response"    # Lcom/google/android/videos/subtitles/Subtitles;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/videos/subtitles/Subtitles;",
            ")V"
        }
    .end annotation

    .prologue
    .line 77
    .local p1, "request":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/videos/subtitles/SubtitleTrack;Ljava/lang/Integer;>;"
    iget-object v0, p0, Lcom/google/android/videos/store/SubtitlesClient$1;->val$callback:Lcom/google/android/videos/async/Callback;

    iget-object v1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-interface {v0, v1, p2}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 78
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 74
    check-cast p1, Landroid/util/Pair;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/videos/subtitles/Subtitles;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/store/SubtitlesClient$1;->onResponse(Landroid/util/Pair;Lcom/google/android/videos/subtitles/Subtitles;)V

    return-void
.end method
