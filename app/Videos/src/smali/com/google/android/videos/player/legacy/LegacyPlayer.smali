.class public final Lcom/google/android/videos/player/legacy/LegacyPlayer;
.super Ljava/lang/Object;
.source "LegacyPlayer.java"

# interfaces
.implements Lcom/google/android/videos/player/PlayerSurface$Listener;
.implements Lcom/google/android/videos/player/VideosPlayer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/legacy/LegacyPlayer$Listener;
    }
.end annotation


# instance fields
.field private final eventHandler:Landroid/os/Handler;

.field private hq:Z

.field private final internalPlayer:Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;

.field private final listener:Lcom/google/android/videos/player/legacy/LegacyPlayer$Listener;

.field private pendingPrepare:Z

.field private playWhenReady:Z

.field private playbackState:I

.field private playbackStateIsMasked:Z

.field private playerSurface:Lcom/google/android/videos/player/PlayerSurface;

.field private positionOffset:I

.field private final qualityDropper:Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;

.field private selectedStreamIndex:I

.field private streamSelection:Lcom/google/android/videos/streams/LegacyStreamSelection;

.field private subtitleRenderer:Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;

.field private surfaceCreated:Z

.field private videoInfo:Lcom/google/android/videos/player/VideoInfo;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/content/SharedPreferences;Landroid/view/Display;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/videos/drm/DrmManager;Lcom/google/android/videos/streams/LegacyStreamsSelector;ZZZLcom/google/android/videos/player/legacy/LegacyPlayer$Listener;Lcom/google/android/videos/Config;Lcom/google/android/videos/player/PlaybackResumeState;)V
    .locals 13
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "preferences"    # Landroid/content/SharedPreferences;
    .param p3, "display"    # Landroid/view/Display;
    .param p4, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;
    .param p5, "drmManager"    # Lcom/google/android/videos/drm/DrmManager;
    .param p6, "streamsSelector"    # Lcom/google/android/videos/streams/LegacyStreamsSelector;
    .param p7, "surroundSound"    # Z
    .param p8, "enableVirtualizer"    # Z
    .param p9, "displaySupportsProtectedBuffers"    # Z
    .param p10, "listener"    # Lcom/google/android/videos/player/legacy/LegacyPlayer$Listener;
    .param p11, "config"    # Lcom/google/android/videos/Config;
    .param p12, "playbackResumeState"    # Lcom/google/android/videos/player/PlaybackResumeState;

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    invoke-static/range {p10 .. p10}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/player/legacy/LegacyPlayer$Listener;

    iput-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->listener:Lcom/google/android/videos/player/legacy/LegacyPlayer$Listener;

    .line 79
    const/4 v1, 0x1

    iput v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->playbackState:I

    .line 80
    new-instance v1, Lcom/google/android/videos/player/legacy/LegacyPlayer$1;

    invoke-direct {v1, p0}, Lcom/google/android/videos/player/legacy/LegacyPlayer$1;-><init>(Lcom/google/android/videos/player/legacy/LegacyPlayer;)V

    iput-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->eventHandler:Landroid/os/Handler;

    .line 86
    new-instance v1, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;

    move-object/from16 v0, p11

    invoke-direct {v1, p0, v0}, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;-><init>(Lcom/google/android/videos/player/VideosPlayer;Lcom/google/android/videos/Config;)V

    iput-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->qualityDropper:Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;

    .line 87
    new-instance v1, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;

    iget-object v11, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->eventHandler:Landroid/os/Handler;

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    move-object/from16 v12, p12

    invoke-direct/range {v1 .. v12}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Landroid/view/Display;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/videos/drm/DrmManager;Lcom/google/android/videos/streams/LegacyStreamsSelector;ZZZLandroid/os/Handler;Lcom/google/android/videos/player/PlaybackResumeState;)V

    iput-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->internalPlayer:Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;

    .line 90
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/player/legacy/LegacyPlayer;Landroid/os/Message;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/player/legacy/LegacyPlayer;
    .param p1, "x1"    # Landroid/os/Message;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/legacy/LegacyPlayer;->handleInternalEvent(Landroid/os/Message;)V

    return-void
.end method

.method private handleInternalEvent(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 269
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 319
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected event: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 323
    :cond_0
    :goto_0
    return-void

    .line 271
    :pswitch_0
    iget v2, p1, Landroid/os/Message;->arg1:I

    iput v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->playbackState:I

    .line 272
    iget v2, p1, Landroid/os/Message;->arg2:I

    if-eqz v2, :cond_1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->playbackStateIsMasked:Z

    .line 273
    invoke-direct {p0}, Lcom/google/android/videos/player/legacy/LegacyPlayer;->onStateChanged()V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 272
    goto :goto_1

    .line 277
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/videos/streams/LegacyStreamSelection;

    iput-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->streamSelection:Lcom/google/android/videos/streams/LegacyStreamSelection;

    .line 278
    iget v0, p1, Landroid/os/Message;->arg1:I

    iput v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->selectedStreamIndex:I

    .line 279
    invoke-direct {p0}, Lcom/google/android/videos/player/legacy/LegacyPlayer;->updatePositionOffset()V

    .line 280
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->qualityDropper:Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;

    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->streamSelection:Lcom/google/android/videos/streams/LegacyStreamSelection;

    iget-boolean v1, v1, Lcom/google/android/videos/streams/LegacyStreamSelection;->supportsQualityToggle:Z

    invoke-virtual {v0, v1}, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->onStreamsSelected(Z)V

    .line 281
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->listener:Lcom/google/android/videos/player/legacy/LegacyPlayer$Listener;

    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->streamSelection:Lcom/google/android/videos/streams/LegacyStreamSelection;

    invoke-interface {v0, p0, v1}, Lcom/google/android/videos/player/legacy/LegacyPlayer$Listener;->onLegacyInitialized(Lcom/google/android/videos/player/legacy/LegacyPlayer;Lcom/google/android/videos/streams/LegacyStreamSelection;)V

    goto :goto_0

    .line 285
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->listener:Lcom/google/android/videos/player/legacy/LegacyPlayer$Listener;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Exception;

    invoke-interface {v1, p0, v0}, Lcom/google/android/videos/player/legacy/LegacyPlayer$Listener;->onLegacyInitializationError(Lcom/google/android/videos/player/legacy/LegacyPlayer;Ljava/lang/Exception;)V

    goto :goto_0

    .line 289
    :pswitch_3
    iget-object v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->listener:Lcom/google/android/videos/player/legacy/LegacyPlayer$Listener;

    iget v3, p1, Landroid/os/Message;->arg1:I

    if-eqz v3, :cond_2

    move v1, v0

    :cond_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/videos/player/legacy/MediaPlayerException;

    invoke-interface {v2, p0, v1, v0}, Lcom/google/android/videos/player/legacy/LegacyPlayer$Listener;->onLegacyPlayerError(Lcom/google/android/videos/player/legacy/LegacyPlayer;ZLcom/google/android/videos/player/legacy/MediaPlayerException;)V

    goto :goto_0

    .line 296
    :pswitch_4
    iget v2, p1, Landroid/os/Message;->arg1:I

    if-nez v2, :cond_3

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->pendingPrepare:Z

    .line 297
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->playerSurface:Lcom/google/android/videos/player/PlayerSurface;

    invoke-interface {v0}, Lcom/google/android/videos/player/PlayerSurface;->recreateSurface()V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 296
    goto :goto_2

    .line 301
    :pswitch_5
    iget-object v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->playerSurface:Lcom/google/android/videos/player/PlayerSurface;

    iget v3, p1, Landroid/os/Message;->arg1:I

    if-eqz v3, :cond_4

    :goto_3
    invoke-interface {v2, v0}, Lcom/google/android/videos/player/PlayerSurface;->setZoomSupported(Z)V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_3

    .line 305
    :pswitch_6
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->playerSurface:Lcom/google/android/videos/player/PlayerSurface;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/videos/player/PlayerSurface;->setVideoSize(II)V

    goto :goto_0

    .line 309
    :pswitch_7
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->subtitleRenderer:Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;

    invoke-virtual {v0}, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->start()V

    goto :goto_0

    .line 313
    :pswitch_8
    iget-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->playWhenReady:Z

    if-nez v0, :cond_0

    .line 314
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->listener:Lcom/google/android/videos/player/legacy/LegacyPlayer$Listener;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->positionOffset:I

    sub-int/2addr v1, v2

    invoke-interface {v0, p0, v1}, Lcom/google/android/videos/player/legacy/LegacyPlayer$Listener;->onLegacyPausedFrameTimestamp(Lcom/google/android/videos/player/legacy/LegacyPlayer;I)V

    goto :goto_0

    .line 269
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_6
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private onStateChanged()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 326
    iget v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->playbackState:I

    packed-switch v0, :pswitch_data_0

    .line 343
    :goto_0
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->listener:Lcom/google/android/videos/player/legacy/LegacyPlayer$Listener;

    iget-boolean v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->playWhenReady:Z

    iget v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->playbackState:I

    invoke-interface {v0, p0, v1, v2}, Lcom/google/android/videos/player/legacy/LegacyPlayer$Listener;->onLegacyPlayerStateChanged(Lcom/google/android/videos/player/legacy/LegacyPlayer;ZI)V

    .line 344
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->qualityDropper:Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;

    iget v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->playbackState:I

    iget-boolean v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->playbackStateIsMasked:Z

    iget-boolean v3, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->playWhenReady:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->onStateChanged(IZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 345
    invoke-direct {p0, v4, v5}, Lcom/google/android/videos/player/legacy/LegacyPlayer;->setHq(ZZ)V

    .line 347
    :cond_0
    return-void

    .line 330
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->subtitleRenderer:Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;

    invoke-virtual {v0, v5}, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->stop(Z)V

    goto :goto_0

    .line 333
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->subtitleRenderer:Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;

    iget-boolean v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->playbackStateIsMasked:Z

    invoke-virtual {v0, v1}, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->stop(Z)V

    goto :goto_0

    .line 336
    :pswitch_2
    iget-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->playWhenReady:Z

    if-eqz v0, :cond_1

    .line 337
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->playerSurface:Lcom/google/android/videos/player/PlayerSurface;

    invoke-interface {v0}, Lcom/google/android/videos/player/PlayerSurface;->openShutter()V

    goto :goto_0

    .line 339
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->subtitleRenderer:Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;

    invoke-virtual {v0, v4}, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->stop(Z)V

    goto :goto_0

    .line 326
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private setHq(ZZ)V
    .locals 1
    .param p1, "hq"    # Z
    .param p2, "wasAutomaticDrop"    # Z

    .prologue
    .line 144
    iget-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->hq:Z

    if-eq v0, p1, :cond_0

    .line 145
    iput-boolean p1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->hq:Z

    .line 146
    invoke-direct {p0}, Lcom/google/android/videos/player/legacy/LegacyPlayer;->updatePositionOffset()V

    .line 147
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->internalPlayer:Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->setHq(Z)V

    .line 148
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->listener:Lcom/google/android/videos/player/legacy/LegacyPlayer$Listener;

    invoke-interface {v0, p0, p1, p2}, Lcom/google/android/videos/player/legacy/LegacyPlayer$Listener;->onLegacyHqToggled(Lcom/google/android/videos/player/legacy/LegacyPlayer;ZZ)V

    .line 150
    :cond_0
    return-void
.end method

.method private updatePositionOffset()V
    .locals 3

    .prologue
    .line 260
    invoke-virtual {p0}, Lcom/google/android/videos/player/legacy/LegacyPlayer;->getSelectedStream()Lcom/google/android/videos/streams/MediaStream;

    move-result-object v0

    .line 261
    .local v0, "stream":Lcom/google/android/videos/streams/MediaStream;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    iget v1, v1, Lcom/google/android/videos/streams/ItagInfo;->drmType:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const/16 v1, 0xc8

    :goto_0
    iput v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->positionOffset:I

    .line 263
    return-void

    .line 261
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getAudioTracks()[Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->streamSelection:Lcom/google/android/videos/streams/LegacyStreamSelection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->streamSelection:Lcom/google/android/videos/streams/LegacyStreamSelection;

    iget-object v0, v0, Lcom/google/android/videos/streams/LegacyStreamSelection;->audioInfos:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBufferedPercentage()I
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->internalPlayer:Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;

    invoke-virtual {v0}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->getBufferedPercentage()I

    move-result v0

    return v0
.end method

.method public getCurrentPosition()I
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->internalPlayer:Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;

    invoke-virtual {v0}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->getCurrentPosition()I

    move-result v0

    iget v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->positionOffset:I

    sub-int/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public getHq()Z
    .locals 1

    .prologue
    .line 154
    iget-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->hq:Z

    return v0
.end method

.method public getPlayWhenReady()Z
    .locals 1

    .prologue
    .line 130
    iget-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->playWhenReady:Z

    return v0
.end method

.method public getPlaybackState()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->playbackState:I

    return v0
.end method

.method public getSelectedAudioTrack()I
    .locals 1

    .prologue
    .line 234
    iget v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->selectedStreamIndex:I

    return v0
.end method

.method public getSelectedStream()Lcom/google/android/videos/streams/MediaStream;
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->streamSelection:Lcom/google/android/videos/streams/LegacyStreamSelection;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->hq:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->streamSelection:Lcom/google/android/videos/streams/LegacyStreamSelection;

    iget-object v0, v0, Lcom/google/android/videos/streams/LegacyStreamSelection;->hi:Ljava/util/List;

    iget v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->selectedStreamIndex:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/streams/MediaStream;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->streamSelection:Lcom/google/android/videos/streams/LegacyStreamSelection;

    iget-object v0, v0, Lcom/google/android/videos/streams/LegacyStreamSelection;->lo:Ljava/util/List;

    iget v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->selectedStreamIndex:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/streams/MediaStream;

    goto :goto_0
.end method

.method public getStreamSelection()Lcom/google/android/videos/streams/LegacyStreamSelection;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->streamSelection:Lcom/google/android/videos/streams/LegacyStreamSelection;

    return-object v0
.end method

.method public prepare(Lcom/google/android/videos/player/VideoInfo;)V
    .locals 1
    .param p1, "videoInfo"    # Lcom/google/android/videos/player/VideoInfo;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    .line 109
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->streamSelection:Lcom/google/android/videos/streams/LegacyStreamSelection;

    .line 110
    invoke-direct {p0}, Lcom/google/android/videos/player/legacy/LegacyPlayer;->updatePositionOffset()V

    .line 111
    iget-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->surfaceCreated:Z

    if-eqz v0, :cond_0

    .line 112
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->pendingPrepare:Z

    .line 113
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->internalPlayer:Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->prepare(Lcom/google/android/videos/player/VideoInfo;)V

    .line 117
    :goto_0
    return-void

    .line 115
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->pendingPrepare:Z

    goto :goto_0
.end method

.method public release()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 205
    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->internalPlayer:Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;

    invoke-virtual {v1}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->getCurrentPosition()I

    move-result v0

    .line 206
    .local v0, "finalPosition":I
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->pendingPrepare:Z

    .line 207
    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->internalPlayer:Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;

    invoke-virtual {v1}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->release()V

    .line 208
    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->subtitleRenderer:Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;

    invoke-virtual {v1}, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->release()V

    .line 209
    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->playerSurface:Lcom/google/android/videos/player/PlayerSurface;

    invoke-interface {v1, v2}, Lcom/google/android/videos/player/PlayerSurface;->setListener(Lcom/google/android/videos/player/PlayerSurface$Listener;)V

    .line 210
    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->eventHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 211
    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->listener:Lcom/google/android/videos/player/legacy/LegacyPlayer$Listener;

    invoke-interface {v1, v0}, Lcom/google/android/videos/player/legacy/LegacyPlayer$Listener;->onLegacyReleased(I)V

    .line 212
    return-void
.end method

.method public seekTo(I)V
    .locals 1
    .param p1, "timeMillis"    # I

    .prologue
    .line 184
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/videos/player/legacy/LegacyPlayer;->seekTo(IZ)V

    .line 185
    return-void
.end method

.method public seekTo(IZ)V
    .locals 2
    .param p1, "timeMillis"    # I
    .param p2, "ensureAccurateTime"    # Z

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->subtitleRenderer:Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->stop(Z)V

    .line 200
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->internalPlayer:Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->seekTo(IZ)V

    .line 201
    return-void
.end method

.method public setHq(Z)V
    .locals 1
    .param p1, "hq"    # Z

    .prologue
    .line 140
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/player/legacy/LegacyPlayer;->setHq(ZZ)V

    .line 141
    return-void
.end method

.method public setPlayWhenReady(Z)V
    .locals 1
    .param p1, "playWhenReady"    # Z

    .prologue
    .line 121
    iget-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->playWhenReady:Z

    if-eq v0, p1, :cond_0

    .line 122
    iput-boolean p1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->playWhenReady:Z

    .line 123
    invoke-direct {p0}, Lcom/google/android/videos/player/legacy/LegacyPlayer;->onStateChanged()V

    .line 124
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->internalPlayer:Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->setPlayWhenReady(Z)V

    .line 126
    :cond_0
    return-void
.end method

.method public setSelectedAudioTrack(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 226
    iget v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->selectedStreamIndex:I

    if-eq p1, v0, :cond_0

    .line 227
    iput p1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->selectedStreamIndex:I

    .line 228
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->internalPlayer:Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->setSelectedStreamIndex(I)V

    .line 230
    :cond_0
    return-void
.end method

.method public setSubtitles(Lcom/google/android/videos/subtitles/Subtitles;)V
    .locals 1
    .param p1, "subtitles"    # Lcom/google/android/videos/subtitles/Subtitles;

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->subtitleRenderer:Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->setSubtitles(Lcom/google/android/videos/subtitles/Subtitles;)V

    .line 217
    return-void
.end method

.method public setTrickPlayEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 136
    return-void
.end method

.method public setViews(Lcom/google/android/videos/player/PlayerSurface;Lcom/google/android/videos/player/overlay/SubtitlesOverlay;)V
    .locals 2
    .param p1, "playerSurface"    # Lcom/google/android/videos/player/PlayerSurface;
    .param p2, "subtitlesOverlay"    # Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    .prologue
    .line 99
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/player/PlayerSurface;

    iput-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->playerSurface:Lcom/google/android/videos/player/PlayerSurface;

    .line 100
    invoke-interface {p1, p0}, Lcom/google/android/videos/player/PlayerSurface;->setListener(Lcom/google/android/videos/player/PlayerSurface$Listener;)V

    .line 101
    invoke-interface {p1}, Lcom/google/android/videos/player/PlayerSurface;->isSurfaceCreated()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->surfaceCreated:Z

    .line 102
    new-instance v0, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;

    invoke-direct {v0, p0, p2}, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;-><init>(Lcom/google/android/videos/player/legacy/LegacyPlayer;Lcom/google/android/videos/player/overlay/SubtitlesOverlay;)V

    iput-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->subtitleRenderer:Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;

    .line 103
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->internalPlayer:Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;

    invoke-interface {p1}, Lcom/google/android/videos/player/PlayerSurface;->getSurfaceHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->setSurfaceHolder(Landroid/view/SurfaceHolder;)V

    .line 104
    return-void
.end method

.method public surfaceChanged()V
    .locals 0

    .prologue
    .line 250
    return-void
.end method

.method public surfaceCreated()V
    .locals 1

    .prologue
    .line 241
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->surfaceCreated:Z

    .line 242
    iget-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->pendingPrepare:Z

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/legacy/LegacyPlayer;->prepare(Lcom/google/android/videos/player/VideoInfo;)V

    .line 245
    :cond_0
    return-void
.end method

.method public surfaceDestroyed()V
    .locals 1

    .prologue
    .line 254
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->surfaceCreated:Z

    .line 255
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->playerSurface:Lcom/google/android/videos/player/PlayerSurface;

    invoke-interface {v0}, Lcom/google/android/videos/player/PlayerSurface;->closeShutter()V

    .line 256
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayer;->internalPlayer:Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;

    invoke-virtual {v0}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->blockingStop()V

    .line 257
    return-void
.end method
