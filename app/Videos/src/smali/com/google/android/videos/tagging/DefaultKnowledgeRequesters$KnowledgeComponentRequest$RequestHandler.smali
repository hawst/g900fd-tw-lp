.class public abstract Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;
.super Ljava/lang/Object;
.source "DefaultKnowledgeRequesters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "RequestHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        "T:",
        "Ljava/lang/Object;",
        "S:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 369
    .local p0, "this":Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;, "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler<TR;TE;TT;TS;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract createComponent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;TS;)TE;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation
.end method

.method public abstract createUnderlyingRequest(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;TS;)TT;"
        }
    .end annotation
.end method

.method public resolveUnderlyingRequestError(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Exception;)Ljava/lang/Object;
    .locals 0
    .param p3, "exception"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;TE;",
            "Ljava/lang/Exception;",
            ")TE;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 406
    .local p0, "this":Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;, "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler<TR;TE;TT;TS;>;"
    .local p1, "knowledgeComponentRequest":Ljava/lang/Object;, "TR;"
    .local p2, "knowledgeComponentFromStore":Ljava/lang/Object;, "TE;"
    throw p3
.end method

.method public shouldUpdate(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;TS;)Z"
        }
    .end annotation

    .prologue
    .line 392
    .local p0, "this":Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;, "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler<TR;TE;TT;TS;>;"
    .local p1, "storedResponse":Ljava/lang/Object;, "TS;"
    .local p2, "freshResponse":Ljava/lang/Object;, "TS;"
    const/4 v0, 0x1

    return v0
.end method
