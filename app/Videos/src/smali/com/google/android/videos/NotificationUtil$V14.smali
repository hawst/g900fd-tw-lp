.class Lcom/google/android/videos/NotificationUtil$V14;
.super Lcom/google/android/videos/NotificationUtil;
.source "NotificationUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/NotificationUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "V14"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 166
    invoke-direct {p0, p1}, Lcom/google/android/videos/NotificationUtil;-><init>(Landroid/content/Context;)V

    .line 167
    return-void
.end method

.method private getNewEpisodesBuilder(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;
    .locals 4
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "poster"    # Landroid/graphics/Bitmap;
    .param p4, "detailsIntent"    # Landroid/app/PendingIntent;
    .param p5, "cancelIntent"    # Landroid/app/PendingIntent;

    .prologue
    const/4 v3, 0x0

    .line 297
    if-eqz p3, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/NotificationUtil$V14;->context:Landroid/content/Context;

    invoke-static {v1, p3}, Lcom/google/android/videos/NotificationUtil$V14;->scaleBitmapForNotification(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 300
    .local v0, "icon":Landroid/graphics/Bitmap;
    :goto_0
    invoke-virtual {p0, v3}, Lcom/google/android/videos/NotificationUtil$V14;->createBuilder(I)Landroid/app/Notification$Builder;

    move-result-object v1

    const v2, 0x7f0200d8

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, p4}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, p5}, Landroid/app/Notification$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v1

    return-object v1

    .line 297
    .end local v0    # "icon":Landroid/graphics/Bitmap;
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/NotificationUtil$V14;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0200dc

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected build(Landroid/app/Notification$Builder;)Landroid/app/Notification;
    .locals 1
    .param p1, "builder"    # Landroid/app/Notification$Builder;

    .prologue
    .line 284
    invoke-virtual {p1}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method protected createBuilder(I)Landroid/app/Notification$Builder;
    .locals 2
    .param p1, "defaults"    # I

    .prologue
    .line 274
    new-instance v0, Landroid/app/Notification$Builder;

    iget-object v1, p0, Lcom/google/android/videos/NotificationUtil$V14;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setOnlyAlertOnce(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    return-object v0
.end method

.method public createDownloadCompletedNotification(Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;
    .locals 8
    .param p1, "completed"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;
    .param p2, "openIntent"    # Landroid/app/PendingIntent;
    .param p3, "deleteIntent"    # Landroid/app/PendingIntent;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 213
    iget-object v5, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;->showId:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;->videoIds:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-le v5, v3, :cond_0

    move v0, v3

    .line 215
    .local v0, "showWithMultipleEpisodes":Z
    :goto_0
    if-eqz v0, :cond_1

    iget-object v2, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;->showTitle:Ljava/lang/String;

    .line 216
    .local v2, "title":Ljava/lang/String;
    :goto_1
    if-eqz v0, :cond_2

    iget-object v5, p0, Lcom/google/android/videos/NotificationUtil$V14;->context:Landroid/content/Context;

    const v6, 0x7f0b01a7

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v7, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;->videoIds:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v3, v4

    invoke-virtual {v5, v6, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 220
    .local v1, "text":Ljava/lang/String;
    :goto_2
    iget-object v3, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;->poster:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v3}, Lcom/google/android/videos/NotificationUtil$V14;->getDownloadBuilder(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    move-result-object v3

    const v5, 0x7f0200d9

    invoke-virtual {v3, v5}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {v3, p2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {v3, p3}, Landroid/app/Notification$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/videos/NotificationUtil$V14;->context:Landroid/content/Context;

    const v6, 0x7f0b01a5

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/videos/NotificationUtil$V14;->build(Landroid/app/Notification$Builder;)Landroid/app/Notification;

    move-result-object v3

    return-object v3

    .end local v0    # "showWithMultipleEpisodes":Z
    .end local v1    # "text":Ljava/lang/String;
    .end local v2    # "title":Ljava/lang/String;
    :cond_0
    move v0, v4

    .line 213
    goto :goto_0

    .line 215
    .restart local v0    # "showWithMultipleEpisodes":Z
    :cond_1
    iget-object v2, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;->title:Ljava/lang/String;

    goto :goto_1

    .line 216
    .restart local v2    # "title":Ljava/lang/String;
    :cond_2
    iget-object v3, p0, Lcom/google/android/videos/NotificationUtil$V14;->context:Landroid/content/Context;

    const v5, 0x7f0b01a6

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method

.method public createDownloadErrorNotification(Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;
    .locals 7
    .param p1, "error"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;
    .param p2, "openIntent"    # Landroid/app/PendingIntent;
    .param p3, "deleteIntent"    # Landroid/app/PendingIntent;

    .prologue
    const/4 v6, 0x0

    .line 234
    iget-object v1, p0, Lcom/google/android/videos/NotificationUtil$V14;->context:Landroid/content/Context;

    iget v2, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;->pinningStatusReason:I

    iget-wide v4, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;->downloadSize:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;->drmErrorCode:Ljava/lang/Integer;

    iget-boolean v5, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;->isRental:Z

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/videos/pinning/PinningStatusHelper;->humanizeFailedReason(Landroid/content/Context;ILjava/lang/Long;Ljava/lang/Integer;Z)Ljava/lang/String;

    move-result-object v0

    .line 236
    .local v0, "text":Ljava/lang/String;
    iget-object v1, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;->poster:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v1}, Lcom/google/android/videos/NotificationUtil$V14;->getDownloadBuilder(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    move-result-object v1

    const v2, 0x7f0200da

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/app/Notification$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/NotificationUtil$V14;->context:Landroid/content/Context;

    const v3, 0x7f0b01a8

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/videos/NotificationUtil$V14;->build(Landroid/app/Notification$Builder;)Landroid/app/Notification;

    move-result-object v1

    return-object v1
.end method

.method public createDownloadPendingNotification(Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;
    .locals 6
    .param p1, "pending"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;
    .param p2, "pendingIntent"    # Landroid/app/PendingIntent;
    .param p3, "deleteIntent"    # Landroid/app/PendingIntent;

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 195
    iget-object v4, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;->showId:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;->videoIds:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-le v4, v0, :cond_0

    .line 197
    .local v0, "showWithMultipleEpisodes":Z
    :goto_0
    if-eqz v0, :cond_1

    iget-object v2, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;->showTitle:Ljava/lang/String;

    .line 198
    .local v2, "title":Ljava/lang/String;
    :goto_1
    iget-object v4, p0, Lcom/google/android/videos/NotificationUtil$V14;->context:Landroid/content/Context;

    iget v5, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;->pinningStatusReason:I

    invoke-static {v5}, Lcom/google/android/videos/pinning/PinningStatusHelper;->getPendingReasonTextId(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 200
    .local v1, "text":Ljava/lang/String;
    iget-object v4, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;->poster:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v4}, Lcom/google/android/videos/NotificationUtil$V14;->getDownloadBuilder(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    move-result-object v4

    const v5, 0x7f0200da

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-virtual {v4, p2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-virtual {v4, p3}, Landroid/app/Notification$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/videos/NotificationUtil$V14;->build(Landroid/app/Notification$Builder;)Landroid/app/Notification;

    move-result-object v3

    return-object v3

    .end local v0    # "showWithMultipleEpisodes":Z
    .end local v1    # "text":Ljava/lang/String;
    .end local v2    # "title":Ljava/lang/String;
    :cond_0
    move v0, v3

    .line 195
    goto :goto_0

    .line 197
    .restart local v0    # "showWithMultipleEpisodes":Z
    :cond_1
    iget-object v2, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;->title:Ljava/lang/String;

    goto :goto_1
.end method

.method public createDownloadsOngoingNotification(Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;Landroid/app/PendingIntent;)Landroid/app/Notification;
    .locals 6
    .param p1, "ongoing"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;
    .param p2, "ongoingIntent"    # Landroid/app/PendingIntent;

    .prologue
    .line 173
    iget-object v2, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->poster:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v2}, Lcom/google/android/videos/NotificationUtil$V14;->getDownloadBuilder(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    move-result-object v2

    const v3, 0x1080081

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v2

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/NotificationUtil$V14;->context:Landroid/content/Context;

    const v4, 0x7f0b01a0

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    iget-object v3, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->singleTitle:Ljava/lang/String;

    iget v4, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->count:I

    invoke-virtual {p0, v3, v4}, Lcom/google/android/videos/NotificationUtil$V14;->getTitleForDownloadInProgress(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {p0, p1}, Lcom/google/android/videos/NotificationUtil$V14;->getDownloadProgressText(Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 181
    .local v0, "notificationBuilder":Landroid/app/Notification$Builder;
    iget v2, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->count:I

    const/4 v3, 0x2

    if-lt v2, v3, :cond_0

    .line 182
    iget v2, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->count:I

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setNumber(I)Landroid/app/Notification$Builder;

    .line 185
    :cond_0
    iget-boolean v2, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->bytesTotalIsIndeterminate:Z

    if-eqz v2, :cond_1

    const/4 v1, 0x0

    .line 187
    .local v1, "progress":I
    :goto_0
    const/16 v2, 0x64

    iget-boolean v3, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->bytesTotalIsIndeterminate:Z

    invoke-virtual {v0, v2, v1, v3}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    .line 189
    invoke-virtual {p0, v0}, Lcom/google/android/videos/NotificationUtil$V14;->build(Landroid/app/Notification$Builder;)Landroid/app/Notification;

    move-result-object v2

    return-object v2

    .line 185
    .end local v1    # "progress":I
    :cond_1
    iget-wide v2, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->bytesTransferred:J

    const-wide/16 v4, 0x64

    mul-long/2addr v2, v4

    iget-wide v4, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->bytesTotal:J

    div-long/2addr v2, v4

    long-to-int v1, v2

    goto :goto_0
.end method

.method public createNewEpisodeNotification(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;
    .locals 8
    .param p1, "episodeTitle"    # Ljava/lang/String;
    .param p2, "showTitle"    # Ljava/lang/String;
    .param p3, "showPoster"    # Landroid/graphics/Bitmap;
    .param p4, "detailsIntent"    # Landroid/app/PendingIntent;
    .param p5, "playIntent"    # Landroid/app/PendingIntent;
    .param p6, "pinIntent"    # Landroid/app/PendingIntent;
    .param p7, "cancelIntent"    # Landroid/app/PendingIntent;

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 252
    iget-object v0, p0, Lcom/google/android/videos/NotificationUtil$V14;->context:Landroid/content/Context;

    const v3, 0x7f0b01a9

    new-array v4, v7, [Ljava/lang/Object;

    aput-object p2, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 253
    .local v1, "title":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/videos/NotificationUtil$V14;->context:Landroid/content/Context;

    const v3, 0x7f0b01ab

    new-array v4, v7, [Ljava/lang/Object;

    aput-object p1, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .local v2, "text":Ljava/lang/String;
    move-object v0, p0

    move-object v3, p3

    move-object v4, p4

    move-object v5, p7

    .line 254
    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/NotificationUtil$V14;->getNewEpisodesBuilder(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v6

    .line 256
    .local v6, "builder":Landroid/app/Notification$Builder;
    invoke-virtual {p0, v6, p5, p6, p1}, Lcom/google/android/videos/NotificationUtil$V14;->setNotificationBigStyle(Landroid/app/Notification$Builder;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)V

    .line 257
    invoke-virtual {p0, v6}, Lcom/google/android/videos/NotificationUtil$V14;->build(Landroid/app/Notification$Builder;)Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method public createNewEpisodesNotification(ILjava/lang/String;Landroid/graphics/Bitmap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;
    .locals 8
    .param p1, "episodeNumbers"    # I
    .param p2, "showTitle"    # Ljava/lang/String;
    .param p3, "showPosterBitmap"    # Landroid/graphics/Bitmap;
    .param p4, "detailsIntent"    # Landroid/app/PendingIntent;
    .param p5, "cancelIntent"    # Landroid/app/PendingIntent;

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 265
    iget-object v0, p0, Lcom/google/android/videos/NotificationUtil$V14;->context:Landroid/content/Context;

    const v3, 0x7f0b01aa

    new-array v4, v5, [Ljava/lang/Object;

    aput-object p2, v4, v7

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 266
    .local v1, "title":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/videos/NotificationUtil$V14;->context:Landroid/content/Context;

    const v3, 0x7f0b01ac

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .local v2, "text":Ljava/lang/String;
    move-object v0, p0

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 267
    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/NotificationUtil$V14;->getNewEpisodesBuilder(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v6

    .line 270
    .local v6, "builder":Landroid/app/Notification$Builder;
    invoke-virtual {p0, v6}, Lcom/google/android/videos/NotificationUtil$V14;->build(Landroid/app/Notification$Builder;)Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method protected getDownloadBuilder(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;
    .locals 4
    .param p1, "poster"    # Landroid/graphics/Bitmap;

    .prologue
    .line 288
    if-eqz p1, :cond_0

    move-object v1, p1

    .line 290
    .local v1, "largeIcon":Landroid/graphics/Bitmap;
    :goto_0
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/videos/NotificationUtil$V14;->createBuilder(I)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 292
    .local v0, "downloadBuilder":Landroid/app/Notification$Builder;
    return-object v0

    .line 288
    .end local v0    # "downloadBuilder":Landroid/app/Notification$Builder;
    .end local v1    # "largeIcon":Landroid/graphics/Bitmap;
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/NotificationUtil$V14;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200dc

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0
.end method

.method protected setNotificationBigStyle(Landroid/app/Notification$Builder;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)V
    .locals 0
    .param p1, "builder"    # Landroid/app/Notification$Builder;
    .param p2, "playIntent"    # Landroid/app/PendingIntent;
    .param p3, "pinIntent"    # Landroid/app/PendingIntent;
    .param p4, "episodeNumber"    # Ljava/lang/String;

    .prologue
    .line 280
    return-void
.end method
