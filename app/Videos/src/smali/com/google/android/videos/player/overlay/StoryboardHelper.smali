.class public final Lcom/google/android/videos/player/overlay/StoryboardHelper;
.super Ljava/lang/Object;
.source "StoryboardHelper.java"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Handler$Callback;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/store/StoryboardImageRequest;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private animatorSet:Landroid/animation/AnimatorSet;

.field private final circleRadius:I

.field private controllerOverlay:Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

.field private currentImageUrl:Ljava/lang/String;

.field private currentStoryboardImageRequest:Lcom/google/android/videos/store/StoryboardImageRequest;

.field private currentThumbnailNumber:I

.field private final currentThumbnailRect:Landroid/graphics/RectF;

.field private final handler:Landroid/os/Handler;

.field private hiresStoryboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

.field private leftCropping:I

.field private loresStoryboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

.field private overlayColorDrawable:Landroid/graphics/drawable/ColorDrawable;

.field private final scrubberCoords:Landroid/graphics/Rect;

.field private scrubberTimeMillis:I

.field private final storyboardClient:Lcom/google/android/videos/store/StoryboardClient;

.field private final storyboardImageCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/store/StoryboardImageRequest;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private storyboardRequestState:I

.field private storyboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

.field private thumbnailAndOverlayColorDrawable:Landroid/graphics/drawable/LayerDrawable;

.field private thumbnailDrawable:Landroid/graphics/drawable/Drawable;

.field private thumbnailView:Landroid/widget/ImageView;

.field private thumbnailViewBottomMargin:I

.field private thumbnailViewHeight:I

.field private final thumbnailViewRect:Landroid/graphics/RectF;

.field private thumbnailViewWidth:I

.field private timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

.field private final transformationMatrix:Landroid/graphics/Matrix;

.field private final videoId:Ljava/lang/String;

.field private visibleFrameWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/videos/store/StoryboardClient;Ljava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "storyboardClient"    # Lcom/google/android/videos/store/StoryboardClient;
    .param p3, "videoId"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->currentThumbnailNumber:I

    .line 104
    iput-object p2, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->storyboardClient:Lcom/google/android/videos/store/StoryboardClient;

    .line 105
    iput-object p3, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->videoId:Ljava/lang/String;

    .line 106
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->handler:Landroid/os/Handler;

    .line 107
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->handler:Landroid/os/Handler;

    invoke-static {v1, p0}, Lcom/google/android/videos/async/HandlerCallback;->create(Landroid/os/Handler;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/HandlerCallback;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->storyboardImageCallback:Lcom/google/android/videos/async/Callback;

    .line 108
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1, v3, v3, v3, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailViewRect:Landroid/graphics/RectF;

    .line 109
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1, v3, v3, v3, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->currentThumbnailRect:Landroid/graphics/RectF;

    .line 110
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->scrubberCoords:Landroid/graphics/Rect;

    .line 111
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    iput-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->transformationMatrix:Landroid/graphics/Matrix;

    .line 112
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 113
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f0e01db

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->circleRadius:I

    .line 116
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const v2, 0x7f0a0079

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->overlayColorDrawable:Landroid/graphics/drawable/ColorDrawable;

    .line 117
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->storyboardRequestState:I

    .line 118
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/player/overlay/StoryboardHelper;)Landroid/graphics/drawable/ColorDrawable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/overlay/StoryboardHelper;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->overlayColorDrawable:Landroid/graphics/drawable/ColorDrawable;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/player/overlay/StoryboardHelper;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/overlay/StoryboardHelper;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/player/overlay/StoryboardHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/player/overlay/StoryboardHelper;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->restoreThumbnailView()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/videos/player/overlay/StoryboardHelper;F)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/player/overlay/StoryboardHelper;
    .param p1, "x1"    # F

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->transformThumbnailViewToCircle(F)V

    return-void
.end method

.method private clampThumbnailNumber(Lcom/google/wireless/android/video/magma/proto/Storyboard;II)I
    .locals 5
    .param p1, "storyboard"    # Lcom/google/wireless/android/video/magma/proto/Storyboard;
    .param p2, "imageIndex"    # I
    .param p3, "thumbnailNumber"    # I

    .prologue
    .line 532
    iget v3, p1, Lcom/google/wireless/android/video/magma/proto/Storyboard;->maxFramesPerRow:I

    iget v4, p1, Lcom/google/wireless/android/video/magma/proto/Storyboard;->maxFramesPerColumn:I

    mul-int v2, v3, v4

    .line 533
    .local v2, "thumbnailsPerImage":I
    mul-int v0, p2, v2

    .line 534
    .local v0, "firstThumbnailIndex":I
    add-int v3, v0, v2

    add-int/lit8 v1, v3, -0x1

    .line 535
    .local v1, "lastThumbnailIndex":I
    invoke-static {p3, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v3

    return v3
.end method

.method private getStoryboard(Z)Lcom/google/wireless/android/video/magma/proto/Storyboard;
    .locals 5
    .param p1, "hires"    # Z

    .prologue
    .line 465
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getPaddingTop()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getPaddingBottom()I

    move-result v4

    add-int v2, v3, v4

    .line 466
    .local v2, "verticalPadding":I
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 467
    .local v1, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    iget v3, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    sub-int v0, v3, v2

    .line 468
    .local v0, "displayHeight":I
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->storyboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    if-eqz p1, :cond_0

    .end local v0    # "displayHeight":I
    :goto_0
    invoke-static {v3, v0}, Lcom/google/android/videos/utils/Util;->getBestStoryboard([Lcom/google/wireless/android/video/magma/proto/Storyboard;I)Lcom/google/wireless/android/video/magma/proto/Storyboard;

    move-result-object v3

    return-object v3

    .restart local v0    # "displayHeight":I
    :cond_0
    div-int/lit8 v0, v0, 0x4

    goto :goto_0
.end method

.method private static getThumbnailNumber(Lcom/google/wireless/android/video/magma/proto/Storyboard;I)I
    .locals 4
    .param p0, "storyboard"    # Lcom/google/wireless/android/video/magma/proto/Storyboard;
    .param p1, "timeMillis"    # I

    .prologue
    .line 399
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->videoLengthMs:I

    if-le p1, v0, :cond_0

    .line 400
    iget p1, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->videoLengthMs:I

    .line 402
    :cond_0
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->numberOfFrames:I

    if-nez v0, :cond_1

    .line 403
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->samplingIntervalMs:I

    div-int v0, p1, v0

    .line 406
    :goto_0
    return v0

    :cond_1
    int-to-long v0, p1

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->numberOfFrames:I

    add-int/lit8 v2, v2, -0x1

    int-to-long v2, v2

    mul-long/2addr v0, v2

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->videoLengthMs:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_0
.end method

.method private getTranslationY()I
    .locals 2

    .prologue
    .line 278
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->refreshScrubberCenterPosition()V

    .line 279
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v1}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailViewBottomMargin:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailViewHeight:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->scrubberCoords:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    return v0
.end method

.method private makeCurrentStoryboardImageRequest()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 472
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->currentStoryboardImageRequest:Lcom/google/android/videos/store/StoryboardImageRequest;

    if-nez v0, :cond_0

    .line 483
    :goto_0
    return-void

    .line 475
    :cond_0
    iget v0, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->storyboardRequestState:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->storyboardRequestState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 477
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->storyboardClient:Lcom/google/android/videos/store/StoryboardClient;

    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->currentStoryboardImageRequest:Lcom/google/android/videos/store/StoryboardImageRequest;

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->storyboardImageCallback:Lcom/google/android/videos/async/Callback;

    invoke-virtual {v0, v1, v3, v2}, Lcom/google/android/videos/store/StoryboardClient;->requestCachedStoryboardImage(Lcom/google/android/videos/store/StoryboardImageRequest;ILcom/google/android/videos/async/Callback;)V

    goto :goto_0

    .line 480
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->storyboardClient:Lcom/google/android/videos/store/StoryboardClient;

    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->currentStoryboardImageRequest:Lcom/google/android/videos/store/StoryboardImageRequest;

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->storyboardImageCallback:Lcom/google/android/videos/async/Callback;

    invoke-virtual {v0, v1, v3, v2}, Lcom/google/android/videos/store/StoryboardClient;->requestStoryboardImage(Lcom/google/android/videos/store/StoryboardImageRequest;ILcom/google/android/videos/async/Callback;)V

    goto :goto_0
.end method

.method private onCurrentStoryboardImageRequestFailed()V
    .locals 2

    .prologue
    const/4 v0, 0x4

    .line 548
    iget v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->storyboardRequestState:I

    packed-switch v1, :pswitch_data_0

    .line 559
    :goto_0
    return-void

    .line 550
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->loresStoryboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->updateImage(I)V

    goto :goto_0

    .line 554
    :pswitch_1
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->updateImage(I)V

    goto :goto_0

    .line 557
    :pswitch_2
    invoke-direct {p0, v0}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->updateImage(I)V

    goto :goto_0

    .line 548
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private onCurrentStoryboardImageRequestSucceeded()V
    .locals 1

    .prologue
    .line 539
    iget v0, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->storyboardRequestState:I

    packed-switch v0, :pswitch_data_0

    .line 545
    :goto_0
    return-void

    .line 542
    :pswitch_0
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->updateImage(I)V

    goto :goto_0

    .line 539
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private refreshScrubberCenterPosition()V
    .locals 5

    .prologue
    .line 324
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->scrubberCoords:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v1}, Lcom/google/android/videos/player/overlay/TimeBar;->getScrubberCenterX()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v2}, Lcom/google/android/videos/player/overlay/TimeBar;->getScrubberCenterY()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v3}, Lcom/google/android/videos/player/overlay/TimeBar;->getScrubberCenterX()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v4}, Lcom/google/android/videos/player/overlay/TimeBar;->getScrubberCenterY()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 326
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->scrubberCoords:Landroid/graphics/Rect;

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 328
    return-void
.end method

.method private restoreThumbnailView()V
    .locals 6

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    .line 331
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->refreshScrubberCenterPosition()V

    .line 332
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 333
    .local v0, "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    iget v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailViewWidth:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 334
    iget v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailViewHeight:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 335
    const/16 v1, 0x50

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 336
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v1}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v2}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailViewWidth:I

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->scrubberCoords:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget v4, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailViewWidth:I

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 339
    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 340
    iget v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailViewBottomMargin:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 341
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 342
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 343
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 344
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setTranslationY(F)V

    .line 345
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    const v2, 0x7f0201e1

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 346
    return-void
.end method

.method private setUpCircleAnimator(FFFFZ)Landroid/animation/ObjectAnimator;
    .locals 10
    .param p1, "startScale"    # F
    .param p2, "endScale"    # F
    .param p3, "startTranslationY"    # F
    .param p4, "endTranslationY"    # F
    .param p5, "show"    # Z

    .prologue
    const/high16 v9, 0x3fc00000    # 1.5f

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 301
    sget-object v4, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v5, v8, [F

    aput p3, v5, v6

    aput p4, v5, v7

    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    .line 303
    .local v3, "holderTranslationY":Landroid/animation/PropertyValuesHolder;
    sget-object v4, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    new-array v5, v8, [F

    aput p1, v5, v6

    aput p2, v5, v7

    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    .line 305
    .local v1, "holderScaleX":Landroid/animation/PropertyValuesHolder;
    sget-object v4, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    new-array v5, v8, [F

    aput p1, v5, v6

    aput p2, v5, v7

    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    .line 307
    .local v2, "holderScaleY":Landroid/animation/PropertyValuesHolder;
    iget-object v4, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    const/4 v5, 0x3

    new-array v5, v5, [Landroid/animation/PropertyValuesHolder;

    aput-object v3, v5, v6

    aput-object v1, v5, v7

    aput-object v2, v5, v8

    invoke-static {v4, v5}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 309
    .local v0, "animator":Landroid/animation/ObjectAnimator;
    if-eqz p5, :cond_0

    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4, v9}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    :goto_0
    invoke-virtual {v0, v4}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 311
    new-instance v4, Lcom/google/android/videos/player/overlay/StoryboardHelper$4;

    invoke-direct {v4, p0, p1}, Lcom/google/android/videos/player/overlay/StoryboardHelper$4;-><init>(Lcom/google/android/videos/player/overlay/StoryboardHelper;F)V

    invoke-virtual {v0, v4}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 317
    return-object v0

    .line 309
    :cond_0
    new-instance v4, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v4, v9}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    goto :goto_0
.end method

.method private setUpRevealAnimator(IIFFZ)Landroid/animation/Animator;
    .locals 2
    .param p1, "centerX"    # I
    .param p2, "centerY"    # I
    .param p3, "startRadius"    # F
    .param p4, "endRadius"    # F
    .param p5, "show"    # Z

    .prologue
    .line 286
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    invoke-static {v1, p1, p2, p3, p4}, Landroid/view/ViewAnimationUtils;->createCircularReveal(Landroid/view/View;IIFF)Landroid/animation/Animator;

    move-result-object v0

    .line 288
    .local v0, "animator":Landroid/animation/Animator;
    if-eqz p5, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/play/animation/PlayInterpolators;->slowOutFastIn(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 290
    new-instance v1, Lcom/google/android/videos/player/overlay/StoryboardHelper$3;

    invoke-direct {v1, p0}, Lcom/google/android/videos/player/overlay/StoryboardHelper$3;-><init>(Lcom/google/android/videos/player/overlay/StoryboardHelper;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 296
    return-object v0

    .line 288
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/play/animation/PlayInterpolators;->fastOutLinearIn(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v1

    goto :goto_0
.end method

.method private transformThumbnailViewToCircle(F)V
    .locals 3
    .param p1, "scale"    # F

    .prologue
    .line 349
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->refreshScrubberCenterPosition()V

    .line 350
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 351
    .local v0, "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    iget v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->circleRadius:I

    mul-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 352
    iget v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->circleRadius:I

    mul-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 353
    const/16 v1, 0x30

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 354
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->scrubberCoords:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->circleRadius:I

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v2}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 356
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->scrubberCoords:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget v2, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->circleRadius:I

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v2}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 357
    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 358
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 359
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 360
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 361
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    const v2, 0x7f0201d2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 362
    return-void
.end method

.method private updateImage(I)V
    .locals 10
    .param p1, "desiredState"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 426
    if-eq p1, v6, :cond_0

    const/4 v8, 0x4

    if-ne p1, v8, :cond_2

    :cond_0
    move v5, v6

    .line 429
    .local v5, "wantHires":Z
    :goto_0
    if-eqz v5, :cond_3

    iget-object v3, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->hiresStoryboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

    .line 431
    .local v3, "storyboard":Lcom/google/wireless/android/video/magma/proto/Storyboard;
    :goto_1
    iget v8, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->scrubberTimeMillis:I

    invoke-static {v3, v8}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->getThumbnailNumber(Lcom/google/wireless/android/video/magma/proto/Storyboard;I)I

    move-result v4

    .line 432
    .local v4, "thumbnailNumber":I
    iget v8, v3, Lcom/google/wireless/android/video/magma/proto/Storyboard;->maxFramesPerRow:I

    iget v9, v3, Lcom/google/wireless/android/video/magma/proto/Storyboard;->maxFramesPerColumn:I

    mul-int/2addr v8, v9

    div-int v0, v4, v8

    .line 434
    .local v0, "imageIndex":I
    iget-object v8, v3, Lcom/google/wireless/android/video/magma/proto/Storyboard;->urls:[Ljava/lang/String;

    aget-object v1, v8, v0

    .line 436
    .local v1, "newStoryboardUrl":Ljava/lang/String;
    iget-object v8, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->currentStoryboardImageRequest:Lcom/google/android/videos/store/StoryboardImageRequest;

    if-eqz v8, :cond_4

    move v2, v6

    .line 438
    .local v2, "requestInFlight":Z
    :goto_2
    if-eqz v2, :cond_5

    iget-object v8, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->currentStoryboardImageRequest:Lcom/google/android/videos/store/StoryboardImageRequest;

    iget-object v8, v8, Lcom/google/android/videos/store/StoryboardImageRequest;->storyboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

    if-ne v3, v8, :cond_5

    iget-object v8, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->currentStoryboardImageRequest:Lcom/google/android/videos/store/StoryboardImageRequest;

    iget v8, v8, Lcom/google/android/videos/store/StoryboardImageRequest;->imageIndex:I

    if-ne v0, v8, :cond_5

    .line 462
    :cond_1
    :goto_3
    return-void

    .end local v0    # "imageIndex":I
    .end local v1    # "newStoryboardUrl":Ljava/lang/String;
    .end local v2    # "requestInFlight":Z
    .end local v3    # "storyboard":Lcom/google/wireless/android/video/magma/proto/Storyboard;
    .end local v4    # "thumbnailNumber":I
    .end local v5    # "wantHires":Z
    :cond_2
    move v5, v7

    .line 426
    goto :goto_0

    .line 429
    .restart local v5    # "wantHires":Z
    :cond_3
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->loresStoryboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

    goto :goto_1

    .restart local v0    # "imageIndex":I
    .restart local v1    # "newStoryboardUrl":Ljava/lang/String;
    .restart local v3    # "storyboard":Lcom/google/wireless/android/video/magma/proto/Storyboard;
    .restart local v4    # "thumbnailNumber":I
    :cond_4
    move v2, v7

    .line 436
    goto :goto_2

    .line 443
    .restart local v2    # "requestInFlight":Z
    :cond_5
    iput p1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->storyboardRequestState:I

    .line 444
    if-nez v2, :cond_6

    iget-object v8, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->currentImageUrl:Ljava/lang/String;

    invoke-static {v8, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 446
    invoke-direct {p0, v3, v4}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->updateTransformationMatrix(Lcom/google/wireless/android/video/magma/proto/Storyboard;I)V

    .line 447
    iget-object v6, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->transformationMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v6, v8}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 448
    iget-object v6, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->handler:Landroid/os/Handler;

    invoke-virtual {v6, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 449
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->onCurrentStoryboardImageRequestSucceeded()V

    goto :goto_3

    .line 453
    :cond_6
    new-instance v8, Lcom/google/android/videos/store/StoryboardImageRequest;

    iget-object v9, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->videoId:Ljava/lang/String;

    invoke-direct {v8, v9, v3, v0}, Lcom/google/android/videos/store/StoryboardImageRequest;-><init>(Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/Storyboard;I)V

    iput-object v8, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->currentStoryboardImageRequest:Lcom/google/android/videos/store/StoryboardImageRequest;

    .line 455
    iget v8, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->storyboardRequestState:I

    if-ne v8, v6, :cond_7

    .line 457
    iget-object v6, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->handler:Landroid/os/Handler;

    const-wide/16 v8, 0x1f4

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 459
    :cond_7
    if-nez v2, :cond_1

    .line 460
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->makeCurrentStoryboardImageRequest()V

    goto :goto_3
.end method

.method private updateThumbnailViewDrawable()V
    .locals 5

    .prologue
    const/high16 v4, 0x1020000

    const/4 v3, 0x0

    .line 514
    sget v1, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v2, 0x15

    if-ge v1, v2, :cond_0

    .line 515
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 527
    :goto_0
    return-void

    .line 518
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailAndOverlayColorDrawable:Landroid/graphics/drawable/LayerDrawable;

    if-nez v1, :cond_1

    .line 519
    const/4 v1, 0x2

    new-array v0, v1, [Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailDrawable:Landroid/graphics/drawable/Drawable;

    aput-object v1, v0, v3

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->overlayColorDrawable:Landroid/graphics/drawable/ColorDrawable;

    aput-object v2, v0, v1

    .line 520
    .local v0, "layers":[Landroid/graphics/drawable/Drawable;
    new-instance v1, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v1, v0}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    iput-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailAndOverlayColorDrawable:Landroid/graphics/drawable/LayerDrawable;

    .line 521
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailAndOverlayColorDrawable:Landroid/graphics/drawable/LayerDrawable;

    invoke-virtual {v1, v3, v4}, Landroid/graphics/drawable/LayerDrawable;->setId(II)V

    .line 526
    .end local v0    # "layers":[Landroid/graphics/drawable/Drawable;
    :goto_1
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailAndOverlayColorDrawable:Landroid/graphics/drawable/LayerDrawable;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 523
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailAndOverlayColorDrawable:Landroid/graphics/drawable/LayerDrawable;

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v4, v2}, Landroid/graphics/drawable/LayerDrawable;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    goto :goto_1
.end method

.method private updateThumbnailViewSize(Lcom/google/wireless/android/video/magma/proto/Storyboard;)V
    .locals 11
    .param p1, "storyboard"    # Lcom/google/wireless/android/video/magma/proto/Storyboard;

    .prologue
    const/4 v9, 0x0

    const/4 v10, 0x0

    .line 365
    if-nez p1, :cond_1

    .line 396
    :cond_0
    :goto_0
    return-void

    .line 369
    :cond_1
    iget-object v7, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 370
    .local v3, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    iget v7, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailViewHeight:I

    if-eqz v7, :cond_2

    iget v7, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget v8, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailViewHeight:I

    if-ne v7, v8, :cond_0

    :cond_2
    iget v7, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailViewWidth:I

    if-eqz v7, :cond_3

    iget v7, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v8, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailViewWidth:I

    if-ne v7, v8, :cond_0

    .line 375
    :cond_3
    iget-object v7, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getPaddingTop()I

    move-result v7

    iget-object v8, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getPaddingBottom()I

    move-result v8

    add-int v6, v7, v8

    .line 376
    .local v6, "verticalPadding":I
    iget-object v7, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getPaddingLeft()I

    move-result v7

    iget-object v8, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getPaddingRight()I

    move-result v8

    add-int v2, v7, v8

    .line 378
    .local v2, "horizontalPadding":I
    iget v7, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    sub-int v0, v7, v6

    .line 379
    .local v0, "displayHeight":I
    iget v7, p1, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameWidth:I

    mul-int/lit8 v7, v7, 0x9

    iget v8, p1, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameHeight:I

    mul-int/lit8 v8, v8, 0x10

    if-gt v7, v8, :cond_4

    .line 380
    iput v9, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->leftCropping:I

    .line 381
    iget v7, p1, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameWidth:I

    add-int/lit8 v7, v7, -0x2

    iput v7, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->visibleFrameWidth:I

    .line 389
    :goto_1
    iget v7, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->visibleFrameWidth:I

    mul-int/2addr v7, v0

    iget v8, p1, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameHeight:I

    div-int v1, v7, v8

    .line 390
    .local v1, "displayWidth":I
    add-int v7, v1, v2

    iput v7, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 392
    iget-object v7, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailViewRect:Landroid/graphics/RectF;

    int-to-float v8, v1

    int-to-float v9, v0

    invoke-virtual {v7, v10, v10, v8, v9}, Landroid/graphics/RectF;->set(FFFF)V

    .line 393
    iget-object v7, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v7, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 394
    iget v7, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v7, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailViewWidth:I

    .line 395
    iget v7, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput v7, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailViewHeight:I

    goto :goto_0

    .line 383
    .end local v1    # "displayWidth":I
    :cond_4
    iget v7, p1, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameWidth:I

    iget v8, p1, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameHeight:I

    mul-int/lit8 v8, v8, 0x10

    div-int/lit8 v8, v8, 0x9

    sub-int v5, v7, v8

    .line 384
    .local v5, "totalCropping":I
    const/4 v7, 0x2

    div-int/lit8 v8, v5, 0x2

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 385
    .local v4, "rightCropping":I
    sub-int v7, v5, v4

    invoke-static {v9, v7}, Ljava/lang/Math;->max(II)I

    move-result v7

    iput v7, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->leftCropping:I

    .line 386
    iget v7, p1, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameWidth:I

    iget v8, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->leftCropping:I

    sub-int/2addr v7, v8

    sub-int/2addr v7, v4

    iput v7, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->visibleFrameWidth:I

    goto :goto_1
.end method

.method private updateThumbnailViewVisibility(I)V
    .locals 18
    .param p1, "visibility"    # I

    .prologue
    .line 183
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getVisibility()I

    move-result v2

    move/from16 v0, p1

    if-ne v2, v0, :cond_1

    .line 275
    :cond_0
    :goto_0
    return-void

    .line 186
    :cond_1
    sget v2, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v3, 0x15

    if-ge v2, v3, :cond_2

    .line 187
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 188
    const/4 v2, 0x4

    move/from16 v0, p1

    if-ne v0, v2, :cond_0

    .line 189
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 193
    :cond_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailViewWidth:I

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailViewHeight:I

    if-eqz v2, :cond_0

    .line 197
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->animatorSet:Landroid/animation/AnimatorSet;

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->isStarted()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 201
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->end()V

    .line 205
    :cond_3
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->animatorSet:Landroid/animation/AnimatorSet;

    .line 207
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailViewWidth:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailViewWidth:I

    mul-int/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailViewHeight:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailViewHeight:I

    mul-int/2addr v3, v5

    add-int/2addr v2, v3

    int-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    add-double/2addr v2, v6

    double-to-int v14, v2

    .line 209
    .local v14, "diagonal":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailViewWidth:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailViewHeight:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    div-int/lit8 v16, v2, 0x4

    .line 210
    .local v16, "revealStartRadius":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->circleRadius:I

    div-int v2, v16, v2

    int-to-float v4, v2

    .line 212
    .local v4, "scale":F
    if-nez p1, :cond_4

    .line 214
    const/high16 v2, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->transformThumbnailViewToCircle(F)V

    .line 215
    invoke-direct/range {p0 .. p0}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->refreshScrubberCenterPosition()V

    .line 216
    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->getTranslationY()I

    move-result v2

    int-to-float v6, v2

    const/4 v7, 0x1

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->setUpCircleAnimator(FFFFZ)Landroid/animation/ObjectAnimator;

    move-result-object v11

    .line 218
    .local v11, "circleAppearAnimator":Landroid/animation/ObjectAnimator;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailViewWidth:I

    div-int/lit8 v6, v2, 0x2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailViewHeight:I

    div-int/lit8 v7, v2, 0x2

    move/from16 v0, v16

    int-to-float v8, v0

    div-int/lit8 v2, v14, 0x2

    int-to-float v9, v2

    const/4 v10, 0x1

    move-object/from16 v5, p0

    invoke-direct/range {v5 .. v10}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->setUpRevealAnimator(IIFFZ)Landroid/animation/Animator;

    move-result-object v13

    .line 220
    .local v13, "circleToThumbnailAnimator":Landroid/animation/Animator;
    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v15

    .line 222
    .local v15, "overlayColorAlphaAnimator":Landroid/animation/ValueAnimator;
    const-wide/16 v2, 0xc8

    invoke-virtual {v13, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 223
    const-wide/16 v2, 0xc8

    invoke-virtual {v11, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 224
    const-wide/16 v2, 0xc8

    invoke-virtual {v15, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 226
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v2, v13}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v2

    invoke-virtual {v2, v11}, Landroid/animation/AnimatorSet$Builder;->after(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 227
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v2, v13}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v2

    invoke-virtual {v2, v15}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 243
    .end local v11    # "circleAppearAnimator":Landroid/animation/ObjectAnimator;
    .end local v13    # "circleToThumbnailAnimator":Landroid/animation/Animator;
    :goto_1
    new-instance v2, Lcom/google/android/videos/player/overlay/StoryboardHelper$1;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/videos/player/overlay/StoryboardHelper$1;-><init>(Lcom/google/android/videos/player/overlay/StoryboardHelper;)V

    invoke-virtual {v15, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 250
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->animatorSet:Landroid/animation/AnimatorSet;

    new-instance v3, Lcom/google/android/videos/player/overlay/StoryboardHelper$2;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v3, v0, v1}, Lcom/google/android/videos/player/overlay/StoryboardHelper$2;-><init>(Lcom/google/android/videos/player/overlay/StoryboardHelper;I)V

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 274
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    goto/16 :goto_0

    .line 230
    .end local v15    # "overlayColorAlphaAnimator":Landroid/animation/ValueAnimator;
    :cond_4
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailViewWidth:I

    div-int/lit8 v6, v2, 0x2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailViewHeight:I

    div-int/lit8 v7, v2, 0x2

    div-int/lit8 v2, v14, 0x2

    int-to-float v8, v2

    move/from16 v0, v16

    int-to-float v9, v0

    const/4 v10, 0x0

    move-object/from16 v5, p0

    invoke-direct/range {v5 .. v10}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->setUpRevealAnimator(IIFFZ)Landroid/animation/Animator;

    move-result-object v17

    .line 232
    .local v17, "thumbnailToCircleAnimator":Landroid/animation/Animator;
    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_1

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v15

    .line 233
    .restart local v15    # "overlayColorAlphaAnimator":Landroid/animation/ValueAnimator;
    const/4 v5, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->getTranslationY()I

    move-result v2

    int-to-float v6, v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->setUpCircleAnimator(FFFFZ)Landroid/animation/ObjectAnimator;

    move-result-object v12

    .line 236
    .local v12, "circleDisappearAnimator":Landroid/animation/ObjectAnimator;
    const-wide/16 v2, 0xfa

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 237
    const-wide/16 v2, 0xfa

    invoke-virtual {v12, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 238
    const-wide/16 v2, 0xfa

    invoke-virtual {v15, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 240
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->animatorSet:Landroid/animation/AnimatorSet;

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v2

    invoke-virtual {v2, v15}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 241
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v2, v12}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet$Builder;->after(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_1

    .line 220
    nop

    :array_0
    .array-data 4
        0xff
        0x0
    .end array-data

    .line 232
    :array_1
    .array-data 4
        0x0
        0xff
    .end array-data
.end method

.method private updateTransformationMatrix(Lcom/google/wireless/android/video/magma/proto/Storyboard;I)V
    .locals 8
    .param p1, "storyboard"    # Lcom/google/wireless/android/video/magma/proto/Storyboard;
    .param p2, "thumbnailNumber"    # I

    .prologue
    .line 411
    iget v3, p1, Lcom/google/wireless/android/video/magma/proto/Storyboard;->maxFramesPerRow:I

    iget v4, p1, Lcom/google/wireless/android/video/magma/proto/Storyboard;->maxFramesPerColumn:I

    mul-int/2addr v3, v4

    rem-int v2, p2, v3

    .line 413
    .local v2, "indexInStoryboard":I
    iget v3, p1, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameWidth:I

    iget v4, p1, Lcom/google/wireless/android/video/magma/proto/Storyboard;->maxFramesPerColumn:I

    rem-int v4, v2, v4

    mul-int v0, v3, v4

    .line 415
    .local v0, "frameX":I
    iget v3, p1, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameHeight:I

    iget v4, p1, Lcom/google/wireless/android/video/magma/proto/Storyboard;->maxFramesPerColumn:I

    div-int v4, v2, v4

    mul-int v1, v3, v4

    .line 418
    .local v1, "frameY":I
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->currentThumbnailRect:Landroid/graphics/RectF;

    iget v4, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->leftCropping:I

    add-int/2addr v4, v0

    int-to-float v4, v4

    int-to-float v5, v1

    iget v6, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->leftCropping:I

    add-int/2addr v6, v0

    iget v7, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->visibleFrameWidth:I

    add-int/2addr v6, v7

    int-to-float v6, v6

    iget v7, p1, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameHeight:I

    add-int/2addr v7, v1

    int-to-float v7, v7

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 421
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->transformationMatrix:Landroid/graphics/Matrix;

    iget-object v4, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->currentThumbnailRect:Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailViewRect:Landroid/graphics/RectF;

    sget-object v6, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v3, v4, v5, v6}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 423
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 3
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 575
    iget v1, p1, Landroid/os/Message;->what:I

    if-nez v1, :cond_0

    .line 576
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 577
    iput-object v2, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->currentImageUrl:Ljava/lang/String;

    .line 578
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->handler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 579
    const/4 v0, 0x1

    .line 581
    :cond_0
    return v0
.end method

.method public onError(Lcom/google/android/videos/store/StoryboardImageRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/store/StoryboardImageRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 563
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->currentStoryboardImageRequest:Lcom/google/android/videos/store/StoryboardImageRequest;

    if-ne v0, p1, :cond_0

    .line 564
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->currentStoryboardImageRequest:Lcom/google/android/videos/store/StoryboardImageRequest;

    .line 565
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->onCurrentStoryboardImageRequestFailed()V

    .line 569
    :goto_0
    return-void

    .line 567
    :cond_0
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->makeCurrentStoryboardImageRequest()V

    goto :goto_0
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 49
    check-cast p1, Lcom/google/android/videos/store/StoryboardImageRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->onError(Lcom/google/android/videos/store/StoryboardImageRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onInflated(Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;Landroid/widget/ImageView;Lcom/google/android/videos/player/overlay/TimeBar;)V
    .locals 2
    .param p1, "overlay"    # Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;
    .param p2, "thumbnailView"    # Landroid/widget/ImageView;
    .param p3, "timeBar"    # Lcom/google/android/videos/player/overlay/TimeBar;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    .line 123
    iput-object p2, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    .line 124
    iput-object p3, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    .line 125
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->currentThumbnailNumber:I

    .line 126
    invoke-virtual {p2}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e01da

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailViewBottomMargin:I

    .line 128
    return-void
.end method

.method public onResponse(Lcom/google/android/videos/store/StoryboardImageRequest;Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "request"    # Lcom/google/android/videos/store/StoryboardImageRequest;
    .param p2, "response"    # Landroid/graphics/Bitmap;

    .prologue
    .line 489
    iget-object v2, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->currentStoryboardImageRequest:Lcom/google/android/videos/store/StoryboardImageRequest;

    if-nez v2, :cond_0

    .line 511
    :goto_0
    return-void

    .line 492
    :cond_0
    iget-object v0, p1, Lcom/google/android/videos/store/StoryboardImageRequest;->storyboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

    .line 493
    .local v0, "storyboard":Lcom/google/wireless/android/video/magma/proto/Storyboard;
    invoke-direct {p0, v0}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->updateThumbnailViewSize(Lcom/google/wireless/android/video/magma/proto/Storyboard;)V

    .line 494
    iget v2, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->scrubberTimeMillis:I

    invoke-static {v0, v2}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->getThumbnailNumber(Lcom/google/wireless/android/video/magma/proto/Storyboard;I)I

    move-result v1

    .line 497
    .local v1, "thumbnailNumber":I
    iget v2, p1, Lcom/google/android/videos/store/StoryboardImageRequest;->imageIndex:I

    invoke-direct {p0, v0, v2, v1}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->clampThumbnailNumber(Lcom/google/wireless/android/video/magma/proto/Storyboard;II)I

    move-result v1

    .line 498
    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->updateTransformationMatrix(Lcom/google/wireless/android/video/magma/proto/Storyboard;I)V

    .line 499
    iget-object v2, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->transformationMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 500
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v3, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3, p2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v2, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailDrawable:Landroid/graphics/drawable/Drawable;

    .line 501
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->updateThumbnailViewDrawable()V

    .line 502
    iget-object v2, v0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->urls:[Ljava/lang/String;

    iget v3, p1, Lcom/google/android/videos/store/StoryboardImageRequest;->imageIndex:I

    aget-object v2, v2, v3

    iput-object v2, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->currentImageUrl:Ljava/lang/String;

    .line 504
    iget-object v2, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->handler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 505
    iget-object v2, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->currentStoryboardImageRequest:Lcom/google/android/videos/store/StoryboardImageRequest;

    if-ne v2, p1, :cond_1

    .line 506
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->currentStoryboardImageRequest:Lcom/google/android/videos/store/StoryboardImageRequest;

    .line 507
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->onCurrentStoryboardImageRequestSucceeded()V

    goto :goto_0

    .line 509
    :cond_1
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->makeCurrentStoryboardImageRequest()V

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 49
    check-cast p1, Lcom/google/android/videos/store/StoryboardImageRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Landroid/graphics/Bitmap;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->onResponse(Lcom/google/android/videos/store/StoryboardImageRequest;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public onScrubbingContinue(II)V
    .locals 3
    .param p1, "scrubberTimeMillis"    # I
    .param p2, "progressMillis"    # I

    .prologue
    .line 156
    iput p1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->scrubberTimeMillis:I

    .line 157
    if-ne p1, p2, :cond_1

    .line 161
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00c3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailDrawable:Landroid/graphics/drawable/Drawable;

    .line 162
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->updateThumbnailViewDrawable()V

    .line 171
    :cond_0
    :goto_0
    return-void

    .line 163
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->hiresStoryboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

    if-eqz v1, :cond_0

    .line 164
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->hiresStoryboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

    invoke-static {v1, p1}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->getThumbnailNumber(Lcom/google/wireless/android/video/magma/proto/Storyboard;I)I

    move-result v0

    .line 165
    .local v0, "newThumbnailNumber":I
    iget v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->currentThumbnailNumber:I

    if-eq v1, v0, :cond_0

    .line 166
    iput v0, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->currentThumbnailNumber:I

    .line 167
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->updateThumbnailViewVisibility(I)V

    .line 168
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->updateImage(I)V

    goto :goto_0
.end method

.method public onScrubbingEnd()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 174
    iput-object v2, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->currentStoryboardImageRequest:Lcom/google/android/videos/store/StoryboardImageRequest;

    .line 175
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->handler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 177
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->updateThumbnailViewVisibility(I)V

    .line 178
    iput-object v2, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->currentImageUrl:Ljava/lang/String;

    .line 179
    return-void
.end method

.method public setStoryboards([Lcom/google/wireless/android/video/magma/proto/Storyboard;)V
    .locals 8
    .param p1, "storyboards"    # [Lcom/google/wireless/android/video/magma/proto/Storyboard;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 131
    iput-object v7, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->hiresStoryboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

    .line 132
    iput-object v7, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->loresStoryboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

    .line 133
    const/4 v5, -0x1

    iput v5, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->currentThumbnailNumber:I

    .line 135
    if-nez p1, :cond_1

    .line 136
    iput-object v7, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->storyboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 140
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    array-length v5, p1

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 141
    .local v1, "filteredStoryboard":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/wireless/android/video/magma/proto/Storyboard;>;"
    move-object v0, p1

    .local v0, "arr$":[Lcom/google/wireless/android/video/magma/proto/Storyboard;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_4

    aget-object v4, v0, v2

    .line 142
    .local v4, "storyboard":Lcom/google/wireless/android/video/magma/proto/Storyboard;
    iget v5, v4, Lcom/google/wireless/android/video/magma/proto/Storyboard;->videoLengthMs:I

    if-eqz v5, :cond_3

    iget v5, v4, Lcom/google/wireless/android/video/magma/proto/Storyboard;->numberOfFrames:I

    if-gt v5, v6, :cond_2

    iget v5, v4, Lcom/google/wireless/android/video/magma/proto/Storyboard;->samplingIntervalMs:I

    if-lez v5, :cond_3

    .line 144
    :cond_2
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 147
    .end local v4    # "storyboard":Lcom/google/wireless/android/video/magma/proto/Storyboard;
    :cond_4
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v5, v5, [Lcom/google/wireless/android/video/magma/proto/Storyboard;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/google/wireless/android/video/magma/proto/Storyboard;

    iput-object v5, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->storyboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    .line 148
    invoke-direct {p0, v6}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->getStoryboard(Z)Lcom/google/wireless/android/video/magma/proto/Storyboard;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->hiresStoryboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

    .line 149
    const/4 v5, 0x0

    invoke-direct {p0, v5}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->getStoryboard(Z)Lcom/google/wireless/android/video/magma/proto/Storyboard;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->loresStoryboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

    .line 150
    iget-object v5, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->loresStoryboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

    iget-object v6, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->hiresStoryboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

    if-ne v5, v6, :cond_0

    .line 151
    iput-object v7, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper;->loresStoryboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

    goto :goto_0
.end method
