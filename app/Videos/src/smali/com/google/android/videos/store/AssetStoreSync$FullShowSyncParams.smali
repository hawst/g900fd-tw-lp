.class final Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;
.super Ljava/lang/Object;
.source "AssetStoreSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/AssetStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "FullShowSyncParams"
.end annotation


# instance fields
.field private final episodeIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final seasonIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final showId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "showId"    # Ljava/lang/String;

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;->showId:Ljava/lang/String;

    .line 73
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;->seasonIds:Ljava/util/Set;

    .line 74
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;->episodeIds:Ljava/util/Set;

    .line 75
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;->seasonIds:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;->episodeIds:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public addDescendant(ILjava/lang/String;)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    const/16 v1, 0x13

    .line 78
    if-eq p1, v1, :cond_0

    const/16 v0, 0x14

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    .line 79
    if-ne p1, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;->seasonIds:Ljava/util/Set;

    :goto_1
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 80
    return-void

    .line 78
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 79
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;->episodeIds:Ljava/util/Set;

    goto :goto_1
.end method
