.class Lcom/google/android/videos/ui/TabContainer$2;
.super Ljava/lang/Object;
.source "TabContainer.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/ui/TabContainer;->setViewPager(Landroid/support/v4/view/ViewPager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/ui/TabContainer;


# direct methods
.method constructor <init>(Lcom/google/android/videos/ui/TabContainer;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/google/android/videos/ui/TabContainer$2;->this$0:Lcom/google/android/videos/ui/TabContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 3

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/videos/ui/TabContainer$2;->this$0:Lcom/google/android/videos/ui/TabContainer;

    iget-object v1, p0, Lcom/google/android/videos/ui/TabContainer$2;->this$0:Lcom/google/android/videos/ui/TabContainer;

    # getter for: Lcom/google/android/videos/ui/TabContainer;->mViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/google/android/videos/ui/TabContainer;->access$100(Lcom/google/android/videos/ui/TabContainer;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    const/4 v2, 0x0

    # invokes: Lcom/google/android/videos/ui/TabContainer;->scrollToChild(II)V
    invoke-static {v0, v1, v2}, Lcom/google/android/videos/ui/TabContainer;->access$200(Lcom/google/android/videos/ui/TabContainer;II)V

    .line 109
    iget-object v0, p0, Lcom/google/android/videos/ui/TabContainer$2;->this$0:Lcom/google/android/videos/ui/TabContainer;

    # getter for: Lcom/google/android/videos/ui/TabContainer;->mTabStrip:Lcom/google/android/videos/ui/TabStrip;
    invoke-static {v0}, Lcom/google/android/videos/ui/TabContainer;->access$300(Lcom/google/android/videos/ui/TabContainer;)Lcom/google/android/videos/ui/TabStrip;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/ui/TabStrip;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 110
    return-void
.end method
