.class Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;
.super Ljava/lang/Object;
.source "BbaOneVideoChunkSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FormatChunkSizes"
.end annotation


# instance fields
.field private estimatedSize:I

.field public final format:Lcom/google/android/exoplayer/chunk/Format;

.field public sizes:[I


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/chunk/Format;)V
    .locals 0
    .param p1, "format"    # Lcom/google/android/exoplayer/chunk/Format;

    .prologue
    .line 218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219
    iput-object p1, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;->format:Lcom/google/android/exoplayer/chunk/Format;

    .line 220
    return-void
.end method


# virtual methods
.method public estimatedChunkSize(Lcom/google/android/exoplayer/chunk/Format;I)V
    .locals 4
    .param p1, "baseFormat"    # Lcom/google/android/exoplayer/chunk/Format;
    .param p2, "baseChunkSize"    # I

    .prologue
    .line 227
    int-to-long v0, p2

    iget-object v2, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;->format:Lcom/google/android/exoplayer/chunk/Format;

    iget v2, v2, Lcom/google/android/exoplayer/chunk/Format;->bitrate:I

    int-to-long v2, v2

    mul-long/2addr v0, v2

    iget v2, p1, Lcom/google/android/exoplayer/chunk/Format;->bitrate:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;->estimatedSize:I

    .line 228
    return-void
.end method

.method public getChunkSize(I)I
    .locals 1
    .param p1, "chunkIndex"    # I

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;->sizes:[I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;->estimatedSize:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;->sizes:[I

    aget v0, v0, p1

    goto :goto_0
.end method
