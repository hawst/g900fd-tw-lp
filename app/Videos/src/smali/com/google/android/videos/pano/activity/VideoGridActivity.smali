.class public final Lcom/google/android/videos/pano/activity/VideoGridActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "VideoGridActivity.java"

# interfaces
.implements Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;
.implements Lcom/google/android/repolib/observers/Updatable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/pano/activity/VideoGridActivity$InitiallyHiddenErrorFragment;
    }
.end annotation


# instance fields
.field private backgroundHelper:Lcom/google/android/videos/pano/ui/BackgroundHelper;

.field private controllableBitmapRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private errorFragment:Landroid/support/v17/leanback/app/ErrorSupportFragment;

.field private gridFragment:Landroid/support/v17/leanback/app/VerticalGridSupportFragment;

.field private repository:Lcom/google/android/repolib/repositories/Repository;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/repositories/Repository",
            "<+",
            "Lcom/google/android/videos/pano/model/VideoItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 299
    return-void
.end method

.method public static createMovieGenreIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "categoryId"    # Ljava/lang/String;

    .prologue
    .line 109
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/videos/pano/activity/VideoGridActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "target"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "title_override"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "category_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "num_columns"

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static createMyMoviesIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 87
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/videos/pano/activity/VideoGridActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "target"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "title_resource_id"

    const v2, 0x7f0b0094

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "num_columns"

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static createMyShowsIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 94
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/videos/pano/activity/VideoGridActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "target"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "title_resource_id"

    const v2, 0x7f0b0095

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "num_columns"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static createShowGenreIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "categoryId"    # Ljava/lang/String;

    .prologue
    .line 118
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/videos/pano/activity/VideoGridActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "target"

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "title_override"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "category_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "num_columns"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static createWishlistIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 101
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/videos/pano/activity/VideoGridActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "target"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "title_resource_id"

    const v2, 0x7f0b0097

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "num_columns"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 30
    .param p1, "savedInstance"    # Landroid/os/Bundle;

    .prologue
    .line 132
    invoke-super/range {p0 .. p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 133
    invoke-static/range {p0 .. p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v29

    .line 134
    .local v29, "videosGlobals":Lcom/google/android/videos/VideosGlobals;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/pano/activity/VideoGridActivity;->getIntent()Landroid/content/Intent;

    move-result-object v23

    .line 135
    .local v23, "intent":Landroid/content/Intent;
    const-string v4, "target"

    const/4 v5, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v25

    .line 136
    .local v25, "target":I
    const-string v4, "title_resource_id"

    const/4 v5, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v27

    .line 137
    .local v27, "titleResourceId":I
    const-string v4, "VideoGridActivity"

    invoke-static {v4}, Lcom/google/android/repolib/ui/DefaultErrorHandler;->errorHandler(Ljava/lang/String;)Lcom/google/android/repolib/ui/DefaultErrorHandler;

    move-result-object v7

    .line 138
    .local v7, "errorHandler":Lcom/google/android/repolib/common/Action;, "Lcom/google/android/repolib/common/Action<Ljava/lang/Throwable;>;"
    invoke-virtual/range {v29 .. v29}, Lcom/google/android/videos/VideosGlobals;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v3

    .line 139
    .local v3, "signInManager":Lcom/google/android/videos/accounts/SignInManager;
    invoke-virtual/range {v29 .. v29}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStore()Lcom/google/android/videos/store/PurchaseStore;

    move-result-object v24

    .line 140
    .local v24, "purchaseStore":Lcom/google/android/videos/store/PurchaseStore;
    invoke-virtual/range {v29 .. v29}, Lcom/google/android/videos/VideosGlobals;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v20

    .line 141
    .local v20, "apiRequesters":Lcom/google/android/videos/api/ApiRequesters;
    invoke-virtual/range {v29 .. v29}, Lcom/google/android/videos/VideosGlobals;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v6

    .line 142
    .local v6, "database":Lcom/google/android/videos/store/Database;
    invoke-virtual/range {v29 .. v29}, Lcom/google/android/videos/VideosGlobals;->getLocalExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v8

    .line 144
    .local v8, "executor":Ljava/util/concurrent/Executor;
    invoke-virtual/range {v29 .. v29}, Lcom/google/android/videos/VideosGlobals;->getBitmapRequesters()Lcom/google/android/videos/bitmap/BitmapRequesters;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/videos/bitmap/BitmapRequesters;->getControllableBitmapRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v21

    .line 146
    .local v21, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/async/ControllableRequest<Landroid/net/Uri;>;Landroid/graphics/Bitmap;>;"
    new-instance v19, Lcom/google/android/repolib/leanback/ObjectAdapterFactory;

    invoke-direct/range {v19 .. v19}, Lcom/google/android/repolib/leanback/ObjectAdapterFactory;-><init>()V

    .line 147
    .local v19, "adapterFactory":Lcom/google/android/repolib/leanback/ObjectAdapterFactory;, "Lcom/google/android/repolib/leanback/ObjectAdapterFactory<Lcom/google/android/videos/pano/model/VideoItem;>;"
    const-class v4, Lcom/google/android/videos/pano/model/ShowItem;

    const v5, 0x7f040082

    new-instance v9, Lcom/google/android/videos/pano/binders/ShowBinder;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v9, v0, v1}, Lcom/google/android/videos/pano/binders/ShowBinder;-><init>(Lcom/google/android/videos/async/Requester;Landroid/app/Activity;)V

    invoke-static {v5, v9}, Lcom/google/android/repolib/ui/ViewBinder;->viewBinder(ILcom/google/android/repolib/common/Binder;)Lcom/google/android/repolib/ui/ViewBinder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Lcom/google/android/repolib/leanback/ObjectAdapterFactory;->bind(Ljava/lang/Class;Lcom/google/android/repolib/ui/ViewBinder;)V

    .line 149
    const-class v4, Lcom/google/android/videos/pano/model/MovieItem;

    const v5, 0x7f04007f

    new-instance v9, Lcom/google/android/videos/pano/binders/MovieBinder;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v9, v0, v1}, Lcom/google/android/videos/pano/binders/MovieBinder;-><init>(Lcom/google/android/videos/async/Requester;Landroid/app/Activity;)V

    invoke-static {v5, v9}, Lcom/google/android/repolib/ui/ViewBinder;->viewBinder(ILcom/google/android/repolib/common/Binder;)Lcom/google/android/repolib/ui/ViewBinder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Lcom/google/android/repolib/leanback/ObjectAdapterFactory;->bind(Ljava/lang/Class;Lcom/google/android/repolib/ui/ViewBinder;)V

    .line 152
    const-string v4, "title_override"

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v27, :cond_2

    const/4 v4, 0x0

    :goto_0
    invoke-static {v5, v4}, Lcom/google/android/videos/utils/Util;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    .line 156
    .local v26, "title":Ljava/lang/String;
    invoke-virtual/range {v29 .. v29}, Lcom/google/android/videos/VideosGlobals;->getBitmapRequesters()Lcom/google/android/videos/bitmap/BitmapRequesters;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/videos/bitmap/BitmapRequesters;->getControllableBitmapRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->controllableBitmapRequester:Lcom/google/android/videos/async/Requester;

    .line 159
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/pano/activity/VideoGridActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string v5, "grid_fragment"

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    check-cast v4, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->gridFragment:Landroid/support/v17/leanback/app/VerticalGridSupportFragment;

    .line 161
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->gridFragment:Landroid/support/v17/leanback/app/VerticalGridSupportFragment;

    if-nez v4, :cond_0

    .line 162
    new-instance v4, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;

    invoke-direct {v4}, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->gridFragment:Landroid/support/v17/leanback/app/VerticalGridSupportFragment;

    .line 163
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/pano/activity/VideoGridActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    const v5, 0x1020002

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->gridFragment:Landroid/support/v17/leanback/app/VerticalGridSupportFragment;

    const-string v10, "grid_fragment"

    invoke-virtual {v4, v5, v9, v10}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 168
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/pano/activity/VideoGridActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string v5, "error_fragment"

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    check-cast v4, Landroid/support/v17/leanback/app/ErrorSupportFragment;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->errorFragment:Landroid/support/v17/leanback/app/ErrorSupportFragment;

    .line 170
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->errorFragment:Landroid/support/v17/leanback/app/ErrorSupportFragment;

    if-nez v4, :cond_1

    .line 171
    new-instance v4, Lcom/google/android/videos/pano/activity/VideoGridActivity$InitiallyHiddenErrorFragment;

    invoke-direct {v4}, Lcom/google/android/videos/pano/activity/VideoGridActivity$InitiallyHiddenErrorFragment;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->errorFragment:Landroid/support/v17/leanback/app/ErrorSupportFragment;

    .line 172
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/pano/activity/VideoGridActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    const v5, 0x1020002

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->errorFragment:Landroid/support/v17/leanback/app/ErrorSupportFragment;

    const-string v10, "error_fragment"

    invoke-virtual {v4, v5, v9, v10}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 178
    :cond_1
    const/4 v4, 0x3

    move/from16 v0, v25

    if-ne v0, v4, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/pano/activity/VideoGridActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e01b3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v22

    .line 180
    .local v22, "columnWidth":I
    :goto_1
    new-instance v28, Lcom/google/android/videos/pano/activity/VideoGridActivity$1;

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/google/android/videos/pano/activity/VideoGridActivity$1;-><init>(Lcom/google/android/videos/pano/activity/VideoGridActivity;I)V

    .line 188
    .local v28, "verticalGridPresenter":Landroid/support/v17/leanback/widget/VerticalGridPresenter;
    const-string v4, "num_columns"

    const/4 v5, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->setNumberOfColumns(I)V

    .line 189
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->gridFragment:Landroid/support/v17/leanback/app/VerticalGridSupportFragment;

    move-object/from16 v0, v28

    invoke-virtual {v4, v0}, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->setGridPresenter(Landroid/support/v17/leanback/widget/VerticalGridPresenter;)V

    .line 190
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->gridFragment:Landroid/support/v17/leanback/app/VerticalGridSupportFragment;

    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->setOnItemViewSelectedListener(Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V

    .line 191
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->gridFragment:Landroid/support/v17/leanback/app/VerticalGridSupportFragment;

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->setTitle(Ljava/lang/String;)V

    .line 192
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->errorFragment:Landroid/support/v17/leanback/app/ErrorSupportFragment;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/pano/activity/VideoGridActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v9, 0x7f02009a

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v17/leanback/app/ErrorSupportFragment;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 193
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->errorFragment:Landroid/support/v17/leanback/app/ErrorSupportFragment;

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Landroid/support/v17/leanback/app/ErrorSupportFragment;->setTitle(Ljava/lang/String;)V

    .line 195
    packed-switch v25, :pswitch_data_0

    .line 228
    new-instance v4, Ljava/lang/IllegalArgumentException;

    invoke-direct {v4}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v4

    .line 152
    .end local v22    # "columnWidth":I
    .end local v26    # "title":Ljava/lang/String;
    .end local v28    # "verticalGridPresenter":Landroid/support/v17/leanback/widget/VerticalGridPresenter;
    :cond_2
    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/google/android/videos/pano/activity/VideoGridActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 178
    .restart local v26    # "title":Ljava/lang/String;
    :cond_3
    const/16 v22, 0x0

    goto :goto_1

    .line 197
    .restart local v22    # "columnWidth":I
    .restart local v28    # "verticalGridPresenter":Landroid/support/v17/leanback/widget/VerticalGridPresenter;
    :pswitch_0
    invoke-static/range {p0 .. p0}, Lcom/google/android/videos/pano/repositories/MovieFromCursorFactory;->movieFromCursorFactory(Landroid/content/Context;)Lcom/google/android/repolib/common/Factory;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-static {v3, v0}, Lcom/google/android/videos/pano/repositories/MovieCursorCreator;->movieCursorCreator(Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/PurchaseStore;)Lcom/google/android/repolib/common/Creator;

    move-result-object v5

    invoke-static/range {v3 .. v8}, Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;->purchaseRequestRepository(Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/repolib/common/Factory;Lcom/google/android/repolib/common/Creator;Lcom/google/android/videos/store/Database;Lcom/google/android/repolib/common/Action;Ljava/util/concurrent/Executor;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->repository:Lcom/google/android/repolib/repositories/Repository;

    .line 199
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->errorFragment:Landroid/support/v17/leanback/app/ErrorSupportFragment;

    const v5, 0x7f0b00b7

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/videos/pano/activity/VideoGridActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v17/leanback/app/ErrorSupportFragment;->setMessage(Ljava/lang/CharSequence;)V

    .line 231
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->gridFragment:Landroid/support/v17/leanback/app/VerticalGridSupportFragment;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->repository:Lcom/google/android/repolib/repositories/Repository;

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Lcom/google/android/repolib/leanback/ObjectAdapterFactory;->createFrom(Lcom/google/android/repolib/repositories/Repository;)Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->setAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 232
    return-void

    .line 202
    :pswitch_1
    invoke-static/range {p0 .. p0}, Lcom/google/android/videos/pano/repositories/ShowFromCursorFactory;->showFromCursorFactory(Landroid/content/Context;)Lcom/google/android/repolib/common/Factory;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-static {v3, v0}, Lcom/google/android/videos/pano/repositories/ShowCursorCreator;->showCursorCreator(Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/PurchaseStore;)Lcom/google/android/repolib/common/Creator;

    move-result-object v5

    invoke-static/range {v3 .. v8}, Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;->purchaseRequestRepository(Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/repolib/common/Factory;Lcom/google/android/repolib/common/Creator;Lcom/google/android/videos/store/Database;Lcom/google/android/repolib/common/Action;Ljava/util/concurrent/Executor;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->repository:Lcom/google/android/repolib/repositories/Repository;

    .line 204
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->errorFragment:Landroid/support/v17/leanback/app/ErrorSupportFragment;

    const v5, 0x7f0b00b8

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/videos/pano/activity/VideoGridActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v17/leanback/app/ErrorSupportFragment;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 207
    :pswitch_2
    new-instance v4, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;

    invoke-virtual/range {v29 .. v29}, Lcom/google/android/videos/VideosGlobals;->getWishlistStore()Lcom/google/android/videos/store/WishlistStore;

    move-result-object v5

    invoke-interface/range {v20 .. v20}, Lcom/google/android/videos/api/ApiRequesters;->getAssetsRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v9

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v3, v5, v9}, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;-><init>(Landroid/content/Context;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/WishlistStore;Lcom/google/android/videos/async/Requester;)V

    invoke-static {v4}, Lcom/google/android/videos/pano/repositories/DataSourceRepository;->dataSourceRepository(Lcom/google/android/videos/pano/datasource/BaseDataSource;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->repository:Lcom/google/android/repolib/repositories/Repository;

    .line 209
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->errorFragment:Landroid/support/v17/leanback/app/ErrorSupportFragment;

    const v5, 0x7f0b00b9

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/videos/pano/activity/VideoGridActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v17/leanback/app/ErrorSupportFragment;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 212
    :pswitch_3
    const/16 v9, 0x32

    const-string v4, "category_id"

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual/range {v29 .. v29}, Lcom/google/android/videos/VideosGlobals;->getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v13

    invoke-virtual/range {v29 .. v29}, Lcom/google/android/videos/VideosGlobals;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v14

    invoke-virtual/range {v29 .. v29}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStore()Lcom/google/android/videos/store/PurchaseStore;

    move-result-object v15

    invoke-virtual/range {v29 .. v29}, Lcom/google/android/videos/VideosGlobals;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v16

    invoke-virtual/range {v29 .. v29}, Lcom/google/android/videos/VideosGlobals;->getLocaleObservable()Lcom/google/android/repolib/observers/Observable;

    move-result-object v17

    invoke-virtual/range {v29 .. v29}, Lcom/google/android/videos/VideosGlobals;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/videos/api/ApiRequesters;->getVideoCollectionGetRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v18

    move-object/from16 v11, p0

    move-object v12, v3

    invoke-static/range {v9 .. v18}, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->createGenreTopMoviesRepository(ILjava/lang/String;Landroid/content/Context;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/repolib/observers/Observable;Lcom/google/android/videos/async/Requester;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->repository:Lcom/google/android/repolib/repositories/Repository;

    goto/16 :goto_2

    .line 220
    :pswitch_4
    const/16 v9, 0x32

    const-string v4, "category_id"

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual/range {v29 .. v29}, Lcom/google/android/videos/VideosGlobals;->getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v13

    invoke-virtual/range {v29 .. v29}, Lcom/google/android/videos/VideosGlobals;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v14

    invoke-virtual/range {v29 .. v29}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStore()Lcom/google/android/videos/store/PurchaseStore;

    move-result-object v15

    invoke-virtual/range {v29 .. v29}, Lcom/google/android/videos/VideosGlobals;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v16

    invoke-virtual/range {v29 .. v29}, Lcom/google/android/videos/VideosGlobals;->getLocaleObservable()Lcom/google/android/repolib/observers/Observable;

    move-result-object v17

    invoke-virtual/range {v29 .. v29}, Lcom/google/android/videos/VideosGlobals;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/videos/api/ApiRequesters;->getVideoCollectionGetRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v18

    move-object/from16 v11, p0

    move-object v12, v3

    invoke-static/range {v9 .. v18}, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->createGenreTopShowsRepository(ILjava/lang/String;Landroid/content/Context;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/repolib/observers/Observable;Lcom/google/android/videos/async/Requester;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->repository:Lcom/google/android/repolib/repositories/Repository;

    goto/16 :goto_2

    .line 195
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onItemSelected(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Landroid/support/v17/leanback/widget/Row;)V
    .locals 7
    .param p1, "itemViewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .param p2, "item"    # Ljava/lang/Object;
    .param p3, "rowViewHolder"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .param p4, "row"    # Landroid/support/v17/leanback/widget/Row;

    .prologue
    .line 260
    iget-object v6, p0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->repository:Lcom/google/android/repolib/repositories/Repository;

    invoke-interface {v6}, Lcom/google/android/repolib/repositories/Repository;->indexer()Lcom/google/android/repolib/common/Indexer;

    move-result-object v2

    .line 261
    .local v2, "indexer":Lcom/google/android/repolib/common/Indexer;, "Lcom/google/android/repolib/common/Indexer<+Lcom/google/android/videos/pano/model/VideoItem;>;"
    invoke-interface {v2}, Lcom/google/android/repolib/common/Indexer;->size()I

    move-result v5

    .line 262
    .local v5, "size":I
    iget-object v6, p0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->gridFragment:Landroid/support/v17/leanback/app/VerticalGridSupportFragment;

    invoke-virtual {v6}, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->getGridPresenter()Landroid/support/v17/leanback/widget/VerticalGridPresenter;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->getNumberOfColumns()I

    move-result v3

    .line 263
    .local v3, "numberOfColumns":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v5, :cond_0

    .line 264
    invoke-interface {v2, v1}, Lcom/google/android/repolib/common/Indexer;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/videos/pano/model/VideoItem;

    .line 265
    .local v4, "repoItem":Lcom/google/android/videos/pano/model/VideoItem;
    invoke-virtual {p2, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 266
    if-ge v1, v3, :cond_1

    invoke-virtual {v4}, Lcom/google/android/videos/pano/model/VideoItem;->getBackgroundUri()Ljava/lang/String;

    move-result-object v0

    .line 267
    .local v0, "backgroundUri":Ljava/lang/String;
    :goto_1
    iget-object v6, p0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->backgroundHelper:Lcom/google/android/videos/pano/ui/BackgroundHelper;

    invoke-virtual {v6, v0}, Lcom/google/android/videos/pano/ui/BackgroundHelper;->setBackgroundUri(Ljava/lang/String;)V

    .line 271
    .end local v0    # "backgroundUri":Ljava/lang/String;
    .end local v4    # "repoItem":Lcom/google/android/videos/pano/model/VideoItem;
    :cond_0
    return-void

    .line 266
    .restart local v4    # "repoItem":Lcom/google/android/videos/pano/model/VideoItem;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 263
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 236
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    .line 237
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->refreshContentRestrictions()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/VideoGridActivity;->finish()V

    .line 240
    :cond_0
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 295
    invoke-static {p0}, Lcom/google/android/videos/utils/PlayStoreUtil;->startPanoSearch(Landroid/app/Activity;)V

    .line 296
    const/4 v0, 0x1

    return v0
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 244
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStart()V

    .line 245
    new-instance v0, Lcom/google/android/videos/pano/ui/BackgroundHelper;

    iget-object v1, p0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->controllableBitmapRequester:Lcom/google/android/videos/async/Requester;

    invoke-direct {v0, p0, v1}, Lcom/google/android/videos/pano/ui/BackgroundHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/async/Requester;)V

    iput-object v0, p0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->backgroundHelper:Lcom/google/android/videos/pano/ui/BackgroundHelper;

    .line 246
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->repository:Lcom/google/android/repolib/repositories/Repository;

    invoke-interface {v0, p0}, Lcom/google/android/repolib/repositories/Repository;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 247
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->backgroundHelper:Lcom/google/android/videos/pano/ui/BackgroundHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/pano/ui/BackgroundHelper;->release()V

    .line 252
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->backgroundHelper:Lcom/google/android/videos/pano/ui/BackgroundHelper;

    .line 253
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->repository:Lcom/google/android/repolib/repositories/Repository;

    invoke-interface {v0, p0}, Lcom/google/android/repolib/repositories/Repository;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 254
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStop()V

    .line 255
    return-void
.end method

.method public update()V
    .locals 5

    .prologue
    .line 277
    iget-object v4, p0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->gridFragment:Landroid/support/v17/leanback/app/VerticalGridSupportFragment;

    invoke-virtual {v4}, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->getAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;

    .line 278
    .local v0, "adapter":Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;
    invoke-virtual {v0}, Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;->update()V

    .line 279
    invoke-virtual {v0}, Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;->size()I

    move-result v1

    .line 280
    .local v1, "count":I
    if-lez v1, :cond_0

    .line 281
    const v4, 0x7f0f0175

    invoke-virtual {p0, v4}, Lcom/google/android/videos/pano/activity/VideoGridActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v17/leanback/widget/VerticalGridView;

    .line 282
    .local v2, "gridView":Landroid/support/v17/leanback/widget/VerticalGridView;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/support/v17/leanback/widget/VerticalGridView;->getSelectedPosition()I

    move-result v4

    if-lt v4, v1, :cond_0

    .line 283
    add-int/lit8 v4, v1, -0x1

    invoke-virtual {v2, v4}, Landroid/support/v17/leanback/widget/VerticalGridView;->setSelectedPosition(I)V

    .line 286
    .end local v2    # "gridView":Landroid/support/v17/leanback/widget/VerticalGridView;
    :cond_0
    iget-object v4, p0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->errorFragment:Landroid/support/v17/leanback/app/ErrorSupportFragment;

    invoke-virtual {v4}, Landroid/support/v17/leanback/app/ErrorSupportFragment;->getView()Landroid/view/View;

    move-result-object v3

    .line 287
    .local v3, "view":Landroid/view/View;
    if-eqz v3, :cond_1

    .line 288
    if-nez v1, :cond_2

    iget-object v4, p0, Lcom/google/android/videos/pano/activity/VideoGridActivity;->errorFragment:Landroid/support/v17/leanback/app/ErrorSupportFragment;

    invoke-virtual {v4}, Landroid/support/v17/leanback/app/ErrorSupportFragment;->getMessage()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 291
    :cond_1
    return-void

    .line 288
    :cond_2
    const/16 v4, 0x8

    goto :goto_0
.end method
