.class Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeComparator;
.super Ljava/lang/Object;
.source "ShowDetailsRowHelper.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EpisodeComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 718
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$1;

    .prologue
    .line 718
    invoke-direct {p0}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeComparator;-><init>()V

    return-void
.end method

.method static synthetic access$600(Ljava/lang/String;)I
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 718
    invoke-static {p0}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeComparator;->parse(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private static parse(Ljava/lang/String;)I
    .locals 4
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 729
    const/4 v2, 0x0

    .line 730
    .local v2, "result":I
    const/4 v1, 0x0

    .local v1, "offset":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 731
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    add-int/lit8 v0, v3, -0x30

    .line 732
    .local v0, "digit":I
    if-ltz v0, :cond_0

    const/16 v3, 0x9

    if-le v0, v3, :cond_1

    .line 737
    .end local v0    # "digit":I
    :cond_0
    return v2

    .line 735
    .restart local v0    # "digit":I
    :cond_1
    mul-int/lit8 v3, v2, 0xa

    add-int v2, v3, v0

    .line 730
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public compare(Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;)I
    .locals 5
    .param p1, "lhs"    # Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;
    .param p2, "rhs"    # Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;

    .prologue
    .line 722
    iget-object v3, p1, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->assetResource:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v3, v3, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v1, v3, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->sequenceNumber:Ljava/lang/String;

    .line 723
    .local v1, "lhsSequenceNumber":Ljava/lang/String;
    iget-object v3, p2, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->assetResource:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v3, v3, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v2, v3, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->sequenceNumber:Ljava/lang/String;

    .line 724
    .local v2, "rhsSequenceNumber":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeComparator;->parse(Ljava/lang/String;)I

    move-result v3

    invoke-static {v2}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeComparator;->parse(Ljava/lang/String;)I

    move-result v4

    sub-int v0, v3, v4

    .line 725
    .local v0, "diff":I
    if-eqz v0, :cond_0

    .end local v0    # "diff":I
    :goto_0
    return v0

    .restart local v0    # "diff":I
    :cond_0
    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 718
    check-cast p1, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeComparator;->compare(Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;)I

    move-result v0

    return v0
.end method
