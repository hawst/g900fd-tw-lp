.class abstract Lcom/google/android/videos/ui/CursorHelper$HomeWishlistCursorHelper;
.super Lcom/google/android/videos/ui/CursorHelper$HomeCursorHelper;
.source "CursorHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/CursorHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "HomeWishlistCursorHelper"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/ui/CursorHelper$HomeCursorHelper",
        "<",
        "Lcom/google/android/videos/store/WishlistStore$WishlistRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/videos/activity/HomeActivity;Lcom/google/android/videos/Config;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/WishlistStore;Lcom/google/android/videos/ui/SyncHelper;)V
    .locals 0
    .param p1, "activity"    # Lcom/google/android/videos/activity/HomeActivity;
    .param p2, "config"    # Lcom/google/android/videos/Config;
    .param p3, "uiHandler"    # Landroid/os/Handler;
    .param p4, "database"    # Lcom/google/android/videos/store/Database;
    .param p5, "store"    # Lcom/google/android/videos/store/WishlistStore;
    .param p6, "syncHelper"    # Lcom/google/android/videos/ui/SyncHelper;

    .prologue
    .line 607
    invoke-direct/range {p0 .. p6}, Lcom/google/android/videos/ui/CursorHelper$HomeCursorHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/Config;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/ui/SyncHelper;)V

    .line 608
    return-void
.end method


# virtual methods
.method public onMovieMetadataUpdated(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 617
    .local p1, "movieIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/google/android/videos/ui/CursorHelper$HomeWishlistCursorHelper;->scheduleUpdate()V

    .line 618
    return-void
.end method

.method public onShowMetadataUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1, "showId"    # Ljava/lang/String;

    .prologue
    .line 622
    invoke-virtual {p0}, Lcom/google/android/videos/ui/CursorHelper$HomeWishlistCursorHelper;->scheduleUpdate()V

    .line 623
    return-void
.end method

.method public onWishlistUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 612
    invoke-virtual {p0, p1}, Lcom/google/android/videos/ui/CursorHelper$HomeWishlistCursorHelper;->maybeScheduleUpdate(Ljava/lang/String;)V

    .line 613
    return-void
.end method
