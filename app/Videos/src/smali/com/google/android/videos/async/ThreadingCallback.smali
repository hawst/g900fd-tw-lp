.class public abstract Lcom/google/android/videos/async/ThreadingCallback;
.super Ljava/lang/Object;
.source "ThreadingCallback.java"

# interfaces
.implements Lcom/google/android/videos/async/NewCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/async/ThreadingCallback$1;,
        Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/NewCallback",
        "<TR;TE;>;"
    }
.end annotation


# static fields
.field private static final queue:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable",
            "<**>;>;"
        }
    .end annotation
.end field


# instance fields
.field private final target:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    sput-object v0, Lcom/google/android/videos/async/ThreadingCallback;->queue:Ljava/util/concurrent/LinkedBlockingQueue;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/videos/async/Callback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;)V"
        }
    .end annotation

    .prologue
    .line 33
    .local p0, "this":Lcom/google/android/videos/async/ThreadingCallback;, "Lcom/google/android/videos/async/ThreadingCallback<TR;TE;>;"
    .local p1, "target":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TR;TE;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Callback;

    iput-object v0, p0, Lcom/google/android/videos/async/ThreadingCallback;->target:Lcom/google/android/videos/async/Callback;

    .line 35
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;

    .prologue
    .line 25
    invoke-static {p0}, Lcom/google/android/videos/async/ThreadingCallback;->releaseRunnable(Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;)V

    return-void
.end method

.method private static obtainRunnable()Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable",
            "<TR;TE;>;"
        }
    .end annotation

    .prologue
    .line 66
    sget-object v1, Lcom/google/android/videos/async/ThreadingCallback;->queue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/LinkedBlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;

    .line 67
    .local v0, "runnable":Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;, "Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable<TR;TE;>;"
    if-eqz v0, :cond_0

    .end local v0    # "runnable":Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;, "Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable<TR;TE;>;"
    :goto_0
    return-object v0

    .restart local v0    # "runnable":Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;, "Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable<TR;TE;>;"
    :cond_0
    new-instance v0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;

    .end local v0    # "runnable":Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;, "Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable<TR;TE;>;"
    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;-><init>(Lcom/google/android/videos/async/ThreadingCallback$1;)V

    goto :goto_0
.end method

.method private static releaseRunnable(Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable",
            "<**>;)V"
        }
    .end annotation

    .prologue
    .line 72
    .local p0, "runnable":Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;, "Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable<**>;"
    :try_start_0
    sget-object v1, Lcom/google/android/videos/async/ThreadingCallback;->queue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v1, p0}, Ljava/util/concurrent/LinkedBlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    :goto_0
    return-void

    .line 73
    :catch_0
    move-exception v0

    .line 74
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v1, "Interrupted when releasing runnable to the queue"

    invoke-static {v1, v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public onCancelled(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    .prologue
    .line 53
    .local p0, "this":Lcom/google/android/videos/async/ThreadingCallback;, "Lcom/google/android/videos/async/ThreadingCallback<TR;TE;>;"
    .local p1, "request":Ljava/lang/Object;, "TR;"
    invoke-static {}, Lcom/google/android/videos/async/ThreadingCallback;->obtainRunnable()Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;

    move-result-object v0

    .line 54
    .local v0, "runnable":Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;, "Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable<TR;TE;>;"
    iget-object v1, p0, Lcom/google/android/videos/async/ThreadingCallback;->target:Lcom/google/android/videos/async/Callback;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->prepareForOnCancelled(Lcom/google/android/videos/async/Callback;Ljava/lang/Object;)V

    .line 55
    invoke-virtual {p0, v0}, Lcom/google/android/videos/async/ThreadingCallback;->post(Ljava/lang/Runnable;)V

    .line 56
    return-void
.end method

.method public final onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2
    .param p2, "exception"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    .prologue
    .line 46
    .local p0, "this":Lcom/google/android/videos/async/ThreadingCallback;, "Lcom/google/android/videos/async/ThreadingCallback<TR;TE;>;"
    .local p1, "request":Ljava/lang/Object;, "TR;"
    invoke-static {}, Lcom/google/android/videos/async/ThreadingCallback;->obtainRunnable()Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;

    move-result-object v0

    .line 47
    .local v0, "runnable":Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;, "Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable<TR;TE;>;"
    iget-object v1, p0, Lcom/google/android/videos/async/ThreadingCallback;->target:Lcom/google/android/videos/async/Callback;

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->prepareForOnError(Lcom/google/android/videos/async/Callback;Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 48
    invoke-virtual {p0, v0}, Lcom/google/android/videos/async/ThreadingCallback;->post(Ljava/lang/Runnable;)V

    .line 49
    return-void
.end method

.method public final onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;TE;)V"
        }
    .end annotation

    .prologue
    .line 39
    .local p0, "this":Lcom/google/android/videos/async/ThreadingCallback;, "Lcom/google/android/videos/async/ThreadingCallback<TR;TE;>;"
    .local p1, "request":Ljava/lang/Object;, "TR;"
    .local p2, "response":Ljava/lang/Object;, "TE;"
    invoke-static {}, Lcom/google/android/videos/async/ThreadingCallback;->obtainRunnable()Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;

    move-result-object v0

    .line 40
    .local v0, "runnable":Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;, "Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable<TR;TE;>;"
    iget-object v1, p0, Lcom/google/android/videos/async/ThreadingCallback;->target:Lcom/google/android/videos/async/Callback;

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->prepareForOnResponse(Lcom/google/android/videos/async/Callback;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 41
    invoke-virtual {p0, v0}, Lcom/google/android/videos/async/ThreadingCallback;->post(Ljava/lang/Runnable;)V

    .line 42
    return-void
.end method

.method protected abstract post(Ljava/lang/Runnable;)V
.end method
