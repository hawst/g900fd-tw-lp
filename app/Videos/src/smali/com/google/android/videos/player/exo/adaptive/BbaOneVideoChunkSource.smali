.class public Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;
.super Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;
.source "BbaOneVideoChunkSource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$PiecewiseQueueSizeToChunkSizeMapper;,
        Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;
    }
.end annotation


# instance fields
.field private allChunkSizes:[Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;

.field private chunkSizesRetrievedCount:I

.field private lastQueueSize:I

.field private final mapper:Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$PiecewiseQueueSizeToChunkSizeMapper;

.field private startUpMode:Z


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/upstream/DataSource;Landroid/content/Context;Lcom/google/android/videos/Config;[Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;ZLcom/google/android/exoplayer/upstream/BandwidthMeter;Lcom/google/android/videos/utils/NetworkStatus;)V
    .locals 8
    .param p1, "dataSource"    # Lcom/google/android/exoplayer/upstream/DataSource;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "config"    # Lcom/google/android/videos/Config;
    .param p4, "representations"    # [Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    .param p5, "disableHdOnMobileNetwork"    # Z
    .param p6, "bandwidthMeter"    # Lcom/google/android/exoplayer/upstream/BandwidthMeter;
    .param p7, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;

    .prologue
    .line 33
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p6

    move-object v5, p7

    move v6, p5

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;-><init>(Lcom/google/android/exoplayer/upstream/DataSource;Landroid/content/Context;Lcom/google/android/videos/Config;Lcom/google/android/exoplayer/upstream/BandwidthMeter;Lcom/google/android/videos/utils/NetworkStatus;Z[Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;)V

    .line 28
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->lastQueueSize:I

    .line 35
    new-instance v0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$PiecewiseQueueSizeToChunkSizeMapper;

    invoke-interface {p3}, Lcom/google/android/videos/Config;->exoBbaOneLowThresholdQueueSize()I

    move-result v1

    invoke-interface {p3}, Lcom/google/android/videos/Config;->exoBbaOneSlope()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$PiecewiseQueueSizeToChunkSizeMapper;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->mapper:Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$PiecewiseQueueSizeToChunkSizeMapper;

    .line 37
    return-void
.end method

.method private estimateChunkSizes(Lcom/google/android/exoplayer/chunk/Format;I)V
    .locals 2
    .param p1, "baseFormat"    # Lcom/google/android/exoplayer/chunk/Format;
    .param p2, "baseChunkSize"    # I

    .prologue
    .line 203
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->allChunkSizes:[Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 204
    iget-object v1, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->allChunkSizes:[Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;->sizes:[I

    if-nez v1, :cond_0

    .line 205
    iget-object v1, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->allChunkSizes:[Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1, p2}, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;->estimatedChunkSize(Lcom/google/android/exoplayer/chunk/Format;I)V

    .line 203
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 208
    :cond_1
    return-void
.end method

.method private maybeFinishExitingTrickPlay(Lcom/google/android/exoplayer/chunk/Format;Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;Z)Z
    .locals 3
    .param p1, "selected"    # Lcom/google/android/exoplayer/chunk/Format;
    .param p2, "evaluation"    # Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;
    .param p3, "exitingTrickPlay"    # Z

    .prologue
    const/4 v2, 0x1

    .line 186
    if-eqz p3, :cond_1

    iget v0, p2, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->queueSize:I

    if-le v0, v2, :cond_1

    .line 187
    iget-object v0, p2, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->format:Lcom/google/android/exoplayer/chunk/Format;

    iget v0, v0, Lcom/google/android/exoplayer/chunk/Format;->height:I

    const/16 v1, 0x2d0

    if-ge v0, v1, :cond_0

    iget-object v0, p2, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->format:Lcom/google/android/exoplayer/chunk/Format;

    iget v0, v0, Lcom/google/android/exoplayer/chunk/Format;->height:I

    iget v1, p1, Lcom/google/android/exoplayer/chunk/Format;->height:I

    if-ge v0, v1, :cond_0

    .line 188
    const/4 v0, 0x2

    iput v0, p2, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->queueSize:I

    .line 190
    iput-boolean v2, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->startUpMode:Z

    .line 191
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->lastQueueSize:I

    .line 194
    :cond_0
    const/4 p3, 0x0

    .line 196
    :cond_1
    return p3
.end method

.method private maybeRunStartUpMode([Lcom/google/android/exoplayer/chunk/Format;Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;JJIZZZ)Lcom/google/android/exoplayer/chunk/Format;
    .locals 11
    .param p1, "formats"    # [Lcom/google/android/exoplayer/chunk/Format;
    .param p2, "evaluation"    # Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;
    .param p3, "bitrateEstimate"    # J
    .param p5, "historicalBitrateEstimate"    # J
    .param p7, "bbaTargetBitrate"    # I
    .param p8, "isFastNetwork"    # Z
    .param p9, "isHdDisabled"    # Z
    .param p10, "exitingTrickPlay"    # Z

    .prologue
    .line 135
    iget-object v2, p2, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->format:Lcom/google/android/exoplayer/chunk/Format;

    if-eqz v2, :cond_0

    iget v2, p2, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->queueSize:I

    if-eqz v2, :cond_0

    if-eqz p10, :cond_1

    .line 136
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->startUpMode:Z

    .line 137
    const/4 v2, -0x1

    iput v2, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->lastQueueSize:I

    .line 139
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->startUpMode:Z

    if-eqz v2, :cond_3

    .line 143
    iget-object v2, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->formatSelectionHelper:Lcom/google/android/videos/player/exo/adaptive/FormatSelectionHelper;

    invoke-virtual {v2, p3, p4}, Lcom/google/android/videos/player/exo/adaptive/FormatSelectionHelper;->getAvailableBitrateForVideo(J)I

    move-result v10

    .line 144
    .local v10, "videoBitrate":I
    int-to-long v2, v10

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    move/from16 v0, p7

    if-gt v0, v10, :cond_4

    :cond_2
    iget v2, p2, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->queueSize:I

    iget v3, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->lastQueueSize:I

    if-lt v2, v3, :cond_4

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->startUpMode:Z

    .line 147
    .end local v10    # "videoBitrate":I
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->startUpMode:Z

    if-nez v2, :cond_5

    .line 148
    const/4 v2, 0x0

    .line 150
    :goto_1
    return-object v2

    .line 144
    .restart local v10    # "videoBitrate":I
    :cond_4
    const/4 v2, 0x0

    goto :goto_0

    .line 150
    .end local v10    # "videoBitrate":I
    :cond_5
    iget-object v2, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->formatSelectionHelper:Lcom/google/android/videos/player/exo/adaptive/FormatSelectionHelper;

    move-object v3, p1

    move-wide v4, p3

    move-wide/from16 v6, p5

    move/from16 v8, p9

    move/from16 v9, p8

    invoke-virtual/range {v2 .. v9}, Lcom/google/android/videos/player/exo/adaptive/FormatSelectionHelper;->determineIdealFormat([Lcom/google/android/exoplayer/chunk/Format;JJZZ)Lcom/google/android/exoplayer/chunk/Format;

    move-result-object v2

    goto :goto_1
.end method

.method private runBbaOneMode(IIZ)Lcom/google/android/exoplayer/chunk/Format;
    .locals 4
    .param p1, "targetedChunkSize"    # I
    .param p2, "nextChunkIndex"    # I
    .param p3, "isHdDisabled"    # Z

    .prologue
    .line 163
    iget-object v2, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->allChunkSizes:[Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;

    iget-object v3, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->allChunkSizes:[Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    aget-object v2, v2, v3

    iget-object v1, v2, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;->format:Lcom/google/android/exoplayer/chunk/Format;

    .line 164
    .local v1, "selectedFormat":Lcom/google/android/exoplayer/chunk/Format;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->allChunkSizes:[Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 165
    iget-object v2, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->allChunkSizes:[Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;

    aget-object v2, v2, v0

    invoke-virtual {v2, p2}, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;->getChunkSize(I)I

    move-result v2

    if-lt p1, v2, :cond_2

    if-eqz p3, :cond_0

    iget-object v2, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->allChunkSizes:[Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;->format:Lcom/google/android/exoplayer/chunk/Format;

    iget v2, v2, Lcom/google/android/exoplayer/chunk/Format;->width:I

    const/16 v3, 0x2d0

    if-ge v2, v3, :cond_2

    .line 167
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->allChunkSizes:[Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;

    aget-object v2, v2, v0

    iget-object v1, v2, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;->format:Lcom/google/android/exoplayer/chunk/Format;

    .line 171
    :cond_1
    return-object v1

    .line 164
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private runTrickMode([Lcom/google/android/exoplayer/chunk/Format;Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;J)V
    .locals 3
    .param p1, "formats"    # [Lcom/google/android/exoplayer/chunk/Format;
    .param p2, "evaluation"    # Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;
    .param p3, "bitrateEstimate"    # J

    .prologue
    .line 110
    iget-object v1, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->formatSelectionHelper:Lcom/google/android/videos/player/exo/adaptive/FormatSelectionHelper;

    invoke-virtual {v1, p1, p3, p4}, Lcom/google/android/videos/player/exo/adaptive/FormatSelectionHelper;->determineTrickPlayFormat([Lcom/google/android/exoplayer/chunk/Format;J)Lcom/google/android/exoplayer/chunk/Format;

    move-result-object v0

    .line 111
    .local v0, "format":Lcom/google/android/exoplayer/chunk/Format;
    iget-object v1, p2, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->format:Lcom/google/android/exoplayer/chunk/Format;

    if-eq v0, v1, :cond_0

    .line 112
    const/4 v1, 0x4

    iput v1, p2, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->trigger:I

    .line 113
    iput-object v0, p2, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->format:Lcom/google/android/exoplayer/chunk/Format;

    .line 115
    :cond_0
    iget v1, p2, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->queueSize:I

    iput v1, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->lastQueueSize:I

    .line 116
    return-void
.end method


# virtual methods
.method public evaluate(Ljava/util/List;JJ[Lcom/google/android/exoplayer/chunk/Format;JZZZZLcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;)Z
    .locals 25
    .param p2, "seekPositionUs"    # J
    .param p4, "playbackPositionUs"    # J
    .param p6, "formats"    # [Lcom/google/android/exoplayer/chunk/Format;
    .param p7, "bitrateEstimate"    # J
    .param p9, "isFastNetwork"    # Z
    .param p10, "isHdDisabled"    # Z
    .param p11, "trickPlayEnabled"    # Z
    .param p12, "exitingTrickPlay"    # Z
    .param p13, "evaluation"    # Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/google/android/exoplayer/chunk/MediaChunk;",
            ">;JJ[",
            "Lcom/google/android/exoplayer/chunk/Format;",
            "JZZZZ",
            "Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 44
    .local p1, "queue":Ljava/util/List;, "Ljava/util/List<+Lcom/google/android/exoplayer/chunk/MediaChunk;>;"
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->allChunkSizes:[Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;

    if-nez v7, :cond_0

    .line 45
    move-object/from16 v0, p6

    array-length v7, v0

    new-array v7, v7, [Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->allChunkSizes:[Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;

    .line 46
    const/16 v21, 0x0

    .local v21, "i":I
    :goto_0
    move-object/from16 v0, p6

    array-length v7, v0

    move/from16 v0, v21

    if-ge v0, v7, :cond_0

    .line 47
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->allChunkSizes:[Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;

    new-instance v8, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;

    aget-object v9, p6, v21

    invoke-direct {v8, v9}, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;-><init>(Lcom/google/android/exoplayer/chunk/Format;)V

    aput-object v8, v7, v21

    .line 46
    add-int/lit8 v21, v21, 0x1

    goto :goto_0

    .line 51
    .end local v21    # "i":I
    :cond_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 52
    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->getChunkIndex(J)I

    move-result v22

    .line 56
    .local v22, "nextChunkIndex":I
    :goto_1
    if-gez v22, :cond_2

    move/from16 v20, p12

    .line 99
    .end local p12    # "exitingTrickPlay":Z
    .local v20, "exitingTrickPlay":Z
    :goto_2
    return v20

    .line 54
    .end local v20    # "exitingTrickPlay":Z
    .end local v22    # "nextChunkIndex":I
    .restart local p12    # "exitingTrickPlay":Z
    :cond_1
    move-object/from16 v0, p13

    iget v7, v0, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->queueSize:I

    add-int/lit8 v7, v7, -0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/exoplayer/chunk/MediaChunk;

    iget v0, v7, Lcom/google/android/exoplayer/chunk/MediaChunk;->nextChunkIndex:I

    move/from16 v22, v0

    .restart local v22    # "nextChunkIndex":I
    goto :goto_1

    .line 61
    :cond_2
    move-object/from16 v0, p13

    iget-object v7, v0, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->format:Lcom/google/android/exoplayer/chunk/Format;

    if-eqz v7, :cond_4

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->chunkSizesRetrievedCount:I

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->allChunkSizes:[Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;

    array-length v8, v8

    if-eq v7, v8, :cond_4

    .line 62
    const/16 v21, 0x0

    .restart local v21    # "i":I
    :goto_3
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->allChunkSizes:[Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;

    array-length v7, v7

    move/from16 v0, v21

    if-ge v0, v7, :cond_3

    .line 63
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->allChunkSizes:[Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;

    aget-object v7, v7, v21

    iget-object v7, v7, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;->sizes:[I

    if-nez v7, :cond_5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->allChunkSizes:[Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;

    aget-object v7, v7, v21

    iget-object v7, v7, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;->format:Lcom/google/android/exoplayer/chunk/Format;

    iget-object v7, v7, Lcom/google/android/exoplayer/chunk/Format;->id:Ljava/lang/String;

    move-object/from16 v0, p13

    iget-object v8, v0, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->format:Lcom/google/android/exoplayer/chunk/Format;

    iget-object v8, v8, Lcom/google/android/exoplayer/chunk/Format;->id:Ljava/lang/String;

    if-ne v7, v8, :cond_5

    .line 64
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->allChunkSizes:[Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;

    aget-object v7, v7, v21

    move-object/from16 v0, p13

    iget-object v8, v0, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->format:Lcom/google/android/exoplayer/chunk/Format;

    iget-object v8, v8, Lcom/google/android/exoplayer/chunk/Format;->id:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->getChunkSizes(Ljava/lang/String;)[I

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;->sizes:[I

    .line 65
    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->chunkSizesRetrievedCount:I

    add-int/lit8 v7, v7, 0x1

    move-object/from16 v0, p0

    iput v7, v0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->chunkSizesRetrievedCount:I

    .line 69
    :cond_3
    move-object/from16 v0, p13

    iget-object v7, v0, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->format:Lcom/google/android/exoplayer/chunk/Format;

    move-object/from16 v0, p13

    iget-object v8, v0, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->format:Lcom/google/android/exoplayer/chunk/Format;

    iget-object v8, v8, Lcom/google/android/exoplayer/chunk/Format;->id:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->getChunkSizes(Ljava/lang/String;)[I

    move-result-object v8

    aget v8, v8, v22

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v8}, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->estimateChunkSizes(Lcom/google/android/exoplayer/chunk/Format;I)V

    .line 72
    .end local v21    # "i":I
    :cond_4
    if-eqz p11, :cond_6

    .line 73
    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move-object/from16 v2, p13

    move-wide/from16 v3, p7

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->runTrickMode([Lcom/google/android/exoplayer/chunk/Format;Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;J)V

    move/from16 v20, p12

    .line 74
    .end local p12    # "exitingTrickPlay":Z
    .restart local v20    # "exitingTrickPlay":Z
    goto/16 :goto_2

    .line 62
    .end local v20    # "exitingTrickPlay":Z
    .restart local v21    # "i":I
    .restart local p12    # "exitingTrickPlay":Z
    :cond_5
    add-int/lit8 v21, v21, 0x1

    goto :goto_3

    .line 77
    .end local v21    # "i":I
    :cond_6
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->mapper:Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$PiecewiseQueueSizeToChunkSizeMapper;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->allChunkSizes:[Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->allChunkSizes:[Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;

    array-length v9, v9

    add-int/lit8 v9, v9, -0x1

    aget-object v8, v8, v9

    move/from16 v0, v22

    invoke-virtual {v8, v0}, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;->getChunkSize(I)I

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->allChunkSizes:[Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;

    const/4 v10, 0x0

    aget-object v9, v9, v10

    move/from16 v0, v22

    invoke-virtual {v9, v0}, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$FormatChunkSizes;->getChunkSize(I)I

    move-result v9

    move-object/from16 v0, p13

    iget v10, v0, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->queueSize:I

    invoke-virtual {v7, v8, v9, v10}, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$PiecewiseQueueSizeToChunkSizeMapper;->getChunkSize(III)I

    move-result v6

    .line 80
    .local v6, "bbaTargetChunkSize":I
    move-object/from16 v0, p13

    iget-object v7, v0, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->format:Lcom/google/android/exoplayer/chunk/Format;

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v7, v1}, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->getChunkDurationUs(Lcom/google/android/exoplayer/chunk/Format;I)J

    move-result-wide v18

    .line 81
    .local v18, "chunkDurationUs":J
    const/4 v14, -0x1

    .line 82
    .local v14, "bbaTargetBitrate":I
    const-wide/16 v8, -0x1

    cmp-long v7, v18, v8

    if-eqz v7, :cond_7

    const/4 v7, -0x1

    if-eq v6, v7, :cond_7

    .line 84
    int-to-long v8, v6

    const-wide/32 v10, 0x7a1200

    mul-long/2addr v8, v10

    div-long v8, v8, v18

    long-to-int v14, v8

    .line 87
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->getHistoricalBitrateEstimate()J

    move-result-wide v12

    move-object/from16 v7, p0

    move-object/from16 v8, p6

    move-object/from16 v9, p13

    move-wide/from16 v10, p7

    move/from16 v15, p9

    move/from16 v16, p10

    move/from16 v17, p12

    invoke-direct/range {v7 .. v17}, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->maybeRunStartUpMode([Lcom/google/android/exoplayer/chunk/Format;Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;JJIZZZ)Lcom/google/android/exoplayer/chunk/Format;

    move-result-object v23

    .line 90
    .local v23, "selectedFormat":Lcom/google/android/exoplayer/chunk/Format;
    if-nez v23, :cond_8

    .line 91
    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, p10

    invoke-direct {v0, v6, v1, v2}, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->runBbaOneMode(IIZ)Lcom/google/android/exoplayer/chunk/Format;

    move-result-object v23

    .line 93
    :cond_8
    move-object/from16 v0, p13

    iget-object v7, v0, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->format:Lcom/google/android/exoplayer/chunk/Format;

    move-object/from16 v0, v23

    if-eq v0, v7, :cond_9

    .line 94
    if-eqz p12, :cond_a

    const/4 v7, 0x4

    :goto_4
    move-object/from16 v0, p13

    iput v7, v0, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->trigger:I

    .line 96
    :cond_9
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, p13

    move/from16 v3, p12

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->maybeFinishExitingTrickPlay(Lcom/google/android/exoplayer/chunk/Format;Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;Z)Z

    move-result p12

    .line 97
    move-object/from16 v0, v23

    move-object/from16 v1, p13

    iput-object v0, v1, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->format:Lcom/google/android/exoplayer/chunk/Format;

    .line 98
    move-object/from16 v0, p13

    iget v7, v0, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->queueSize:I

    move-object/from16 v0, p0

    iput v7, v0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;->lastQueueSize:I

    move/from16 v20, p12

    .line 99
    .end local p12    # "exitingTrickPlay":Z
    .restart local v20    # "exitingTrickPlay":Z
    goto/16 :goto_2

    .line 94
    .end local v20    # "exitingTrickPlay":Z
    .restart local p12    # "exitingTrickPlay":Z
    :cond_a
    const/4 v7, 0x2

    goto :goto_4
.end method
