.class public Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;
.super Ljava/lang/Object;
.source "SecondScreenEntitiesPlayer.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;,
        Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEventComparator;
    }
.end annotation


# instance fields
.field private final entitiesAdapter:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;

.field private entityEventPosition:I

.field private final entityEvents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final handler:Landroid/os/Handler;

.field private knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

.field private lastPlayerTime:J

.field private lastPlayerTimeUpdateTimestamp:J

.field private playerTimeSupplier:Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;

.field private state:I


# direct methods
.method public constructor <init>(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;)V
    .locals 2
    .param p1, "entitiesAdapter"    # Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;

    .prologue
    const-wide/16 v0, -0x1

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-wide v0, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->lastPlayerTime:J

    .line 52
    iput-wide v0, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->lastPlayerTimeUpdateTimestamp:J

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entityEvents:Ljava/util/List;

    .line 56
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->handler:Landroid/os/Handler;

    .line 57
    iput-object p1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entitiesAdapter:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;

    .line 58
    return-void
.end method

.method private addInitialEntities(I)V
    .locals 5
    .param p1, "startTimeMillis"    # I

    .prologue
    .line 252
    iget-object v4, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

    iget-object v4, v4, Lcom/google/android/videos/tagging/KnowledgeBundle;->tagStream:Lcom/google/android/videos/tagging/TagStream;

    invoke-virtual {v4}, Lcom/google/android/videos/tagging/TagStream;->getAllKnowledgeEntities()Ljava/util/List;

    move-result-object v1

    .line 253
    .local v1, "knowledgeEntities":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/KnowledgeEntity;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    .line 254
    .local v3, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_1

    .line 255
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/tagging/KnowledgeEntity;

    .line 256
    .local v2, "knowledgeEntity":Lcom/google/android/videos/tagging/KnowledgeEntity;
    invoke-virtual {v2, p1}, Lcom/google/android/videos/tagging/KnowledgeEntity;->appearsAt(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 257
    iget-object v4, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entitiesAdapter:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;

    invoke-virtual {v4, v2}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->addItem(Lcom/google/android/videos/tagging/KnowledgeEntity;)V

    .line 254
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 260
    .end local v2    # "knowledgeEntity":Lcom/google/android/videos/tagging/KnowledgeEntity;
    :cond_1
    return-void
.end method

.method private resumeKnowledgeAt(I)V
    .locals 7
    .param p1, "timeMillis"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 263
    iget v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->state:I

    if-ne v1, v6, :cond_0

    .line 278
    :goto_0
    return-void

    .line 266
    :cond_0
    new-instance v0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p1, v4}, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;-><init>(Lcom/google/android/videos/tagging/KnowledgeEntity;II)V

    .line 267
    .local v0, "entityEvent":Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;
    iget-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entityEvents:Ljava/util/List;

    sget-object v2, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEventComparator;->INSTANCE:Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEventComparator;

    invoke-static {v1, v0, v2}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v1

    iput v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entityEventPosition:I

    .line 269
    iget v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entityEventPosition:I

    if-gez v1, :cond_1

    .line 270
    iget v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entityEventPosition:I

    neg-int v1, v1

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entityEventPosition:I

    .line 272
    :cond_1
    iget v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entityEventPosition:I

    iget-object v2, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entityEvents:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 273
    iget-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entityEvents:Ljava/util/List;

    iget v2, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entityEventPosition:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entityEventPosition:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "entityEvent":Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;
    check-cast v0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;

    .line 274
    .restart local v0    # "entityEvent":Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;
    iget-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->handler:Landroid/os/Handler;

    invoke-static {v2, v4, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    iget v3, v0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;->timestamp:I

    sub-int/2addr v3, p1

    int-to-long v4, v3

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 277
    :cond_2
    iput v6, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->state:I

    goto :goto_0
.end method

.method private updatePlayerTime(J)Z
    .locals 11
    .param p1, "playerTime"    # J

    .prologue
    const-wide/16 v6, -0x1

    .line 162
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 163
    .local v0, "checkTime":J
    const/4 v2, 0x0

    .line 164
    .local v2, "seekOccurred":Z
    iget-wide v4, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->lastPlayerTime:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    iget-wide v4, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->lastPlayerTimeUpdateTimestamp:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_1

    .line 175
    :cond_0
    :goto_0
    iput-wide p1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->lastPlayerTime:J

    .line 176
    iput-wide v0, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->lastPlayerTimeUpdateTimestamp:J

    .line 177
    return v2

    .line 166
    :cond_1
    iget-wide v4, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->lastPlayerTime:J

    cmp-long v3, p1, v4

    if-gez v3, :cond_2

    .line 168
    const/4 v2, 0x1

    goto :goto_0

    .line 169
    :cond_2
    iget-wide v4, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->lastPlayerTime:J

    sub-long v4, p1, v4

    iget-wide v6, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->lastPlayerTimeUpdateTimestamp:J

    sub-long v6, v0, v6

    const-wide/16 v8, 0x3e8

    add-long/2addr v6, v8

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    .line 173
    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public clearKnowledge()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 188
    iget-object v0, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 189
    iget-object v0, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entitiesAdapter:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->clear()V

    .line 190
    iput v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->state:I

    .line 191
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v8, 0x1

    .line 205
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;

    .line 206
    .local v1, "entityEvent":Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;
    iget-object v4, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->playerTimeSupplier:Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;

    invoke-interface {v4}, Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;->getPlayerTimeMillis()I

    move-result v3

    .line 207
    .local v3, "timeMillis":I
    iget v4, v1, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;->timestamp:I

    sub-int v0, v4, v3

    .line 208
    .local v0, "delayMillis":I
    const/16 v4, 0x32

    if-le v0, v4, :cond_1

    .line 209
    iget-object v4, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->handler:Landroid/os/Handler;

    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v5

    int-to-long v6, v0

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 245
    :cond_0
    :goto_0
    return v8

    .line 211
    :cond_1
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v4

    const/16 v5, 0x1388

    if-le v4, v5, :cond_2

    .line 213
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->clearKnowledge()V

    .line 214
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->maybeStartOrResume()V

    goto :goto_0

    .line 217
    :cond_2
    iget v4, v1, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;->eventType:I

    packed-switch v4, :pswitch_data_0

    .line 239
    :goto_1
    iget v4, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entityEventPosition:I

    iget-object v5, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entityEvents:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_0

    .line 240
    iget-object v4, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entityEvents:Ljava/util/List;

    iget v5, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entityEventPosition:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entityEventPosition:I

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;

    .line 241
    .local v2, "nextEntityEvent":Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;
    iget v4, v2, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;->timestamp:I

    sub-int v0, v4, v3

    .line 242
    iget-object v6, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->handler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->handler:Landroid/os/Handler;

    const/4 v5, 0x0

    invoke-static {v4, v5, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v7

    if-lez v0, :cond_3

    int-to-long v4, v0

    :goto_2
    invoke-virtual {v6, v7, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 219
    .end local v2    # "nextEntityEvent":Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;
    :pswitch_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "###add item: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;->knowledgeEntity:Lcom/google/android/videos/tagging/KnowledgeEntity;

    iget-object v5, v5, Lcom/google/android/videos/tagging/KnowledgeEntity;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " at time "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "event time: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;->timestamp:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 221
    iget-object v4, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entitiesAdapter:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;

    iget-object v5, v1, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;->knowledgeEntity:Lcom/google/android/videos/tagging/KnowledgeEntity;

    invoke-virtual {v4, v5}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->addItem(Lcom/google/android/videos/tagging/KnowledgeEntity;)V

    goto :goto_1

    .line 224
    :pswitch_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "###highlight item: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;->knowledgeEntity:Lcom/google/android/videos/tagging/KnowledgeEntity;

    iget-object v5, v5, Lcom/google/android/videos/tagging/KnowledgeEntity;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " at time "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "event time: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;->timestamp:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 226
    iget-object v4, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entitiesAdapter:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;

    iget-object v5, v1, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;->knowledgeEntity:Lcom/google/android/videos/tagging/KnowledgeEntity;

    invoke-virtual {v4, v5}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->undimItem(Lcom/google/android/videos/tagging/KnowledgeEntity;)V

    goto/16 :goto_1

    .line 229
    :pswitch_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "###dim item: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;->knowledgeEntity:Lcom/google/android/videos/tagging/KnowledgeEntity;

    iget-object v5, v5, Lcom/google/android/videos/tagging/KnowledgeEntity;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " at time "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "event time: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;->timestamp:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 231
    iget-object v4, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entitiesAdapter:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;

    iget-object v5, v1, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;->knowledgeEntity:Lcom/google/android/videos/tagging/KnowledgeEntity;

    invoke-virtual {v4, v5}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->dimItem(Lcom/google/android/videos/tagging/KnowledgeEntity;)V

    goto/16 :goto_1

    .line 234
    :pswitch_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "###remove item: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;->knowledgeEntity:Lcom/google/android/videos/tagging/KnowledgeEntity;

    iget-object v5, v5, Lcom/google/android/videos/tagging/KnowledgeEntity;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " at time "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "event time: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;->timestamp:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 236
    iget-object v4, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entitiesAdapter:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;

    iget-object v5, v1, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;->knowledgeEntity:Lcom/google/android/videos/tagging/KnowledgeEntity;

    invoke-virtual {v4, v5}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->removeItem(Lcom/google/android/videos/tagging/KnowledgeEntity;)V

    goto/16 :goto_1

    .line 242
    .restart local v2    # "nextEntityEvent":Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;
    :cond_3
    const-wide/16 v4, 0x0

    goto/16 :goto_2

    .line 217
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public init(Lcom/google/android/videos/tagging/KnowledgeBundle;Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;Lcom/google/android/videos/Config;)V
    .locals 20
    .param p1, "knowledgeBundle"    # Lcom/google/android/videos/tagging/KnowledgeBundle;
    .param p2, "playerTimeSupplier"    # Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;
    .param p3, "config"    # Lcom/google/android/videos/Config;

    .prologue
    .line 70
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

    move-object/from16 v16, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    if-ne v0, v1, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entityEvents:Ljava/util/List;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->isEmpty()Z

    move-result v16

    if-nez v16, :cond_1

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 73
    :cond_1
    invoke-interface/range {p3 .. p3}, Lcom/google/android/videos/Config;->knowledgeDimEntitiesAfterDisappearingForMillis()I

    move-result v4

    .line 74
    .local v4, "dimAfterMillis":I
    invoke-interface/range {p3 .. p3}, Lcom/google/android/videos/Config;->knowledgeDontDimEntitiesReappearingWithinMillis()I

    move-result v7

    .line 75
    .local v7, "dontDimMillis":I
    invoke-interface/range {p3 .. p3}, Lcom/google/android/videos/Config;->knowledgeRemoveEntitiesAfterDisappearingForMillis()I

    move-result v13

    .line 76
    .local v13, "removeAfterMillis":I
    invoke-interface/range {p3 .. p3}, Lcom/google/android/videos/Config;->knowledgeDontRemoveEntitiesReappearingWithinMillis()I

    move-result v8

    .line 77
    .local v8, "dontRemoveMillis":I
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->playerTimeSupplier:Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;

    .line 78
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

    .line 79
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entitiesAdapter:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;

    move-object/from16 v16, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/videos/tagging/KnowledgeBundle;->imageRequester:Lcom/google/android/videos/async/Requester;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->setImageRequester(Lcom/google/android/videos/async/Requester;)V

    .line 80
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/videos/tagging/KnowledgeBundle;->tagStream:Lcom/google/android/videos/tagging/TagStream;

    move-object/from16 v16, v0

    if-eqz v16, :cond_0

    .line 84
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/videos/tagging/KnowledgeBundle;->tagStream:Lcom/google/android/videos/tagging/TagStream;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/videos/tagging/TagStream;->getAllKnowledgeEntities()Ljava/util/List;

    move-result-object v11

    .line 85
    .local v11, "knowledgeEntities":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/KnowledgeEntity;>;"
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v14

    .line 86
    .local v14, "size":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    if-ge v9, v14, :cond_b

    .line 87
    invoke-interface {v11, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/videos/tagging/KnowledgeEntity;

    .line 88
    .local v12, "knowledgeEntity":Lcom/google/android/videos/tagging/KnowledgeEntity;
    iget-object v3, v12, Lcom/google/android/videos/tagging/KnowledgeEntity;->appearances:[I

    .line 89
    .local v3, "appearances":[I
    if-nez v3, :cond_3

    .line 86
    :cond_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 94
    :cond_3
    const/4 v15, 0x0

    .line 95
    .local v15, "state":I
    const/4 v10, 0x0

    .local v10, "j":I
    :goto_2
    array-length v0, v3

    move/from16 v16, v0

    move/from16 v0, v16

    if-ge v10, v0, :cond_2

    .line 96
    and-int/lit8 v16, v10, 0x1

    if-nez v16, :cond_7

    .line 97
    const/16 v16, 0x2

    move/from16 v0, v16

    if-ne v15, v0, :cond_5

    .line 101
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entityEvents:Ljava/util/List;

    move-object/from16 v16, v0

    new-instance v17, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;

    aget v18, v3, v10

    const/16 v19, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v0, v12, v1, v2}, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;-><init>(Lcom/google/android/videos/tagging/KnowledgeEntity;II)V

    invoke-interface/range {v16 .. v17}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    :goto_3
    const/4 v15, 0x2

    .line 95
    :cond_4
    :goto_4
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 103
    :cond_5
    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_6

    .line 104
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entityEvents:Ljava/util/List;

    move-object/from16 v16, v0

    new-instance v17, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;

    aget v18, v3, v10

    const/16 v19, 0x2

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v0, v12, v1, v2}, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;-><init>(Lcom/google/android/videos/tagging/KnowledgeEntity;II)V

    invoke-interface/range {v16 .. v17}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 107
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entityEvents:Ljava/util/List;

    move-object/from16 v16, v0

    new-instance v17, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;

    aget v18, v3, v10

    const/16 v19, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v0, v12, v1, v2}, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;-><init>(Lcom/google/android/videos/tagging/KnowledgeEntity;II)V

    invoke-interface/range {v16 .. v17}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 112
    :cond_7
    aget v16, v3, v10

    add-int v6, v16, v13

    .line 113
    .local v6, "disappearTimeMillis":I
    add-int/lit8 v16, v10, 0x1

    array-length v0, v3

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    if-ge v0, v1, :cond_9

    add-int/lit8 v16, v10, 0x1

    aget v16, v3, v16

    sub-int v16, v16, v6

    move/from16 v0, v16

    if-gt v0, v8, :cond_9

    .line 121
    :goto_5
    aget v16, v3, v10

    add-int v5, v16, v4

    .line 122
    .local v5, "dimTimeMillis":I
    add-int/lit8 v16, v10, 0x1

    array-length v0, v3

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    if-ge v0, v1, :cond_8

    add-int/lit8 v16, v10, 0x1

    aget v16, v3, v16

    sub-int v16, v16, v5

    move/from16 v0, v16

    if-le v0, v7, :cond_4

    .line 124
    :cond_8
    if-ge v5, v6, :cond_4

    .line 128
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entityEvents:Ljava/util/List;

    move-object/from16 v16, v0

    new-instance v17, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;

    const/16 v18, 0x3

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v0, v12, v5, v1}, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;-><init>(Lcom/google/android/videos/tagging/KnowledgeEntity;II)V

    invoke-interface/range {v16 .. v17}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    if-nez v15, :cond_a

    :goto_6
    goto/16 :goto_4

    .line 117
    .end local v5    # "dimTimeMillis":I
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entityEvents:Ljava/util/List;

    move-object/from16 v16, v0

    new-instance v17, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;

    const/16 v18, 0x4

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v0, v12, v6, v1}, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;-><init>(Lcom/google/android/videos/tagging/KnowledgeEntity;II)V

    invoke-interface/range {v16 .. v17}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    const/4 v15, 0x0

    goto :goto_5

    .line 130
    .restart local v5    # "dimTimeMillis":I
    :cond_a
    const/4 v15, 0x1

    goto :goto_6

    .line 135
    .end local v3    # "appearances":[I
    .end local v5    # "dimTimeMillis":I
    .end local v6    # "disappearTimeMillis":I
    .end local v10    # "j":I
    .end local v12    # "knowledgeEntity":Lcom/google/android/videos/tagging/KnowledgeEntity;
    .end local v15    # "state":I
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entityEvents:Ljava/util/List;

    move-object/from16 v16, v0

    sget-object v17, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEventComparator;->INSTANCE:Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEventComparator;

    invoke-static/range {v16 .. v17}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 136
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->maybeStartOrResume()V

    goto/16 :goto_0
.end method

.method public maybeStartOrResume()V
    .locals 4

    .prologue
    .line 140
    iget-object v2, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->playerTimeSupplier:Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;

    if-nez v2, :cond_1

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 143
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->playerTimeSupplier:Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;

    invoke-interface {v2}, Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;->getPlayerTimeMillis()I

    move-result v0

    .line 144
    .local v0, "playerTimeMillis":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 147
    int-to-long v2, v0

    invoke-direct {p0, v2, v3}, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->updatePlayerTime(J)Z

    move-result v1

    .line 148
    .local v1, "seekOccurred":Z
    iget v2, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->state:I

    if-eqz v2, :cond_2

    if-eqz v1, :cond_3

    .line 149
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->clearKnowledge()V

    .line 150
    invoke-direct {p0, v0}, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->addInitialEntities(I)V

    .line 152
    :cond_3
    invoke-direct {p0, v0}, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->resumeKnowledgeAt(I)V

    goto :goto_0
.end method

.method public pauseKnowledge()V
    .locals 2

    .prologue
    .line 181
    iget v0, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->state:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 182
    iget-object v0, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->handler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 183
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->state:I

    .line 185
    :cond_0
    return-void
.end method

.method public reset()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    .line 194
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->clearKnowledge()V

    .line 195
    iget-object v0, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entityEvents:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 196
    iput-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

    .line 197
    iput-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->playerTimeSupplier:Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;

    .line 198
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->entityEventPosition:I

    .line 199
    iput-wide v2, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->lastPlayerTime:J

    .line 200
    iput-wide v2, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->lastPlayerTimeUpdateTimestamp:J

    .line 201
    return-void
.end method
