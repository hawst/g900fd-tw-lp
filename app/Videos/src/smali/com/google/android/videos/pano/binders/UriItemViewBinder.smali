.class public Lcom/google/android/videos/pano/binders/UriItemViewBinder;
.super Ljava/lang/Object;
.source "UriItemViewBinder.java"

# interfaces
.implements Lcom/google/android/repolib/common/Binder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Binder",
        "<",
        "Lcom/google/android/videos/pano/model/UriItem;",
        "Lcom/google/android/videos/pano/ui/LibraryCardView;",
        ">;"
    }
.end annotation


# instance fields
.field private final clickListener:Lcom/google/android/repolib/common/Binder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/common/Binder",
            "<",
            "Landroid/view/View;",
            "Lcom/google/android/videos/pano/model/UriItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/repolib/common/Action;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/repolib/common/Action",
            "<",
            "Lcom/google/android/videos/pano/model/UriItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    .local p1, "action":Lcom/google/android/repolib/common/Action;, "Lcom/google/android/repolib/common/Action<Lcom/google/android/videos/pano/model/UriItem;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {p1}, Lcom/google/android/repolib/ui/TagActionOnClickListener;->tagActionOnClickListener(Lcom/google/android/repolib/common/Action;)Lcom/google/android/repolib/common/Binder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pano/binders/UriItemViewBinder;->clickListener:Lcom/google/android/repolib/common/Binder;

    .line 24
    return-void
.end method

.method public static uriItemViewBinder(Lcom/google/android/repolib/common/Action;)Lcom/google/android/repolib/common/Binder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/repolib/common/Action",
            "<",
            "Lcom/google/android/videos/pano/model/UriItem;",
            ">;)",
            "Lcom/google/android/repolib/common/Binder",
            "<",
            "Lcom/google/android/videos/pano/model/UriItem;",
            "Lcom/google/android/videos/pano/ui/LibraryCardView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    .local p0, "action":Lcom/google/android/repolib/common/Action;, "Lcom/google/android/repolib/common/Action<Lcom/google/android/videos/pano/model/UriItem;>;"
    new-instance v0, Lcom/google/android/videos/pano/binders/UriItemViewBinder;

    invoke-direct {v0, p0}, Lcom/google/android/videos/pano/binders/UriItemViewBinder;-><init>(Lcom/google/android/repolib/common/Action;)V

    return-object v0
.end method


# virtual methods
.method public bind(Lcom/google/android/videos/pano/model/UriItem;Lcom/google/android/videos/pano/ui/LibraryCardView;)V
    .locals 1
    .param p1, "item"    # Lcom/google/android/videos/pano/model/UriItem;
    .param p2, "view"    # Lcom/google/android/videos/pano/ui/LibraryCardView;

    .prologue
    .line 32
    invoke-virtual {p1}, Lcom/google/android/videos/pano/model/UriItem;->getImageResourceId()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/google/android/videos/pano/ui/LibraryCardView;->setImageResource(I)V

    .line 33
    invoke-virtual {p1}, Lcom/google/android/videos/pano/model/UriItem;->getIconTitle()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/google/android/videos/pano/ui/LibraryCardView;->setText(I)V

    .line 34
    iget-object v0, p0, Lcom/google/android/videos/pano/binders/UriItemViewBinder;->clickListener:Lcom/google/android/repolib/common/Binder;

    invoke-interface {v0, p2, p1}, Lcom/google/android/repolib/common/Binder;->bind(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 35
    return-void
.end method

.method public bridge synthetic bind(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 19
    check-cast p1, Lcom/google/android/videos/pano/model/UriItem;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/videos/pano/ui/LibraryCardView;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pano/binders/UriItemViewBinder;->bind(Lcom/google/android/videos/pano/model/UriItem;Lcom/google/android/videos/pano/ui/LibraryCardView;)V

    return-void
.end method
