.class Lcom/google/android/videos/async/ControllableAsyncRequester$1$1;
.super Ljava/lang/Object;
.source "ControllableAsyncRequester.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/async/ControllableAsyncRequester$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<TR;TE;>;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/videos/async/ControllableAsyncRequester$1;


# direct methods
.method constructor <init>(Lcom/google/android/videos/async/ControllableAsyncRequester$1;)V
    .locals 0

    .prologue
    .line 46
    .local p0, "this":Lcom/google/android/videos/async/ControllableAsyncRequester$1$1;, "Lcom/google/android/videos/async/ControllableAsyncRequester$1.1;"
    iput-object p1, p0, Lcom/google/android/videos/async/ControllableAsyncRequester$1$1;->this$1:Lcom/google/android/videos/async/ControllableAsyncRequester$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2
    .param p2, "exception"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    .prologue
    .line 53
    .local p0, "this":Lcom/google/android/videos/async/ControllableAsyncRequester$1$1;, "Lcom/google/android/videos/async/ControllableAsyncRequester$1.1;"
    .local p1, "unused":Ljava/lang/Object;, "TR;"
    iget-object v0, p0, Lcom/google/android/videos/async/ControllableAsyncRequester$1$1;->this$1:Lcom/google/android/videos/async/ControllableAsyncRequester$1;

    iget-object v0, v0, Lcom/google/android/videos/async/ControllableAsyncRequester$1;->val$callback:Lcom/google/android/videos/async/Callback;

    iget-object v1, p0, Lcom/google/android/videos/async/ControllableAsyncRequester$1$1;->this$1:Lcom/google/android/videos/async/ControllableAsyncRequester$1;

    iget-object v1, v1, Lcom/google/android/videos/async/ControllableAsyncRequester$1;->val$request:Lcom/google/android/videos/async/ControllableRequest;

    invoke-interface {v0, v1, p2}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 54
    return-void
.end method

.method public onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;TE;)V"
        }
    .end annotation

    .prologue
    .line 49
    .local p0, "this":Lcom/google/android/videos/async/ControllableAsyncRequester$1$1;, "Lcom/google/android/videos/async/ControllableAsyncRequester$1.1;"
    .local p1, "unused":Ljava/lang/Object;, "TR;"
    .local p2, "response":Ljava/lang/Object;, "TE;"
    iget-object v0, p0, Lcom/google/android/videos/async/ControllableAsyncRequester$1$1;->this$1:Lcom/google/android/videos/async/ControllableAsyncRequester$1;

    iget-object v0, v0, Lcom/google/android/videos/async/ControllableAsyncRequester$1;->val$callback:Lcom/google/android/videos/async/Callback;

    iget-object v1, p0, Lcom/google/android/videos/async/ControllableAsyncRequester$1$1;->this$1:Lcom/google/android/videos/async/ControllableAsyncRequester$1;

    iget-object v1, v1, Lcom/google/android/videos/async/ControllableAsyncRequester$1;->val$request:Lcom/google/android/videos/async/ControllableRequest;

    invoke-interface {v0, v1, p2}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 50
    return-void
.end method
