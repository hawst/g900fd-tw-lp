.class public final Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
.super Lcom/google/android/videos/tagging/KnowledgeEntity;
.source "KnowledgeEntity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/KnowledgeEntity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Person"
.end annotation


# instance fields
.field public final characterNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final dateOfBirth:Ljava/lang/String;

.field public final dateOfDeath:Ljava/lang/String;

.field public final filmography:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Film;",
            ">;"
        }
    .end annotation
.end field

.field public final placeOfBirth:Ljava/lang/String;


# direct methods
.method constructor <init>(I[ILjava/lang/String;Lcom/google/android/videos/tagging/KnowledgeEntity$Image;[ILjava/util/Collection;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;)V
    .locals 1
    .param p1, "localId"    # I
    .param p2, "splitIds"    # [I
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "image"    # Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
    .param p5, "appearances"    # [I
    .param p7, "dateOfBirth"    # Ljava/lang/String;
    .param p8, "dateOfDeath"    # Ljava/lang/String;
    .param p9, "placeOfBirth"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I[I",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            "[I",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Film;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 39
    .local p6, "characterNames":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .local p10, "filmography":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/videos/tagging/KnowledgeEntity$Film;>;"
    invoke-direct/range {p0 .. p5}, Lcom/google/android/videos/tagging/KnowledgeEntity;-><init>(I[ILjava/lang/String;Lcom/google/android/videos/tagging/KnowledgeEntity$Image;[I)V

    .line 40
    invoke-static {p6}, Lcom/google/android/videos/utils/CollectionUtil;->immutableCopyOf(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->characterNames:Ljava/util/List;

    .line 41
    iput-object p7, p0, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->dateOfBirth:Ljava/lang/String;

    .line 42
    iput-object p8, p0, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->dateOfDeath:Ljava/lang/String;

    .line 43
    iput-object p9, p0, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->placeOfBirth:Ljava/lang/String;

    .line 44
    invoke-static {p10}, Lcom/google/android/videos/utils/CollectionUtil;->immutableCopyOf(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->filmography:Ljava/util/List;

    .line 45
    return-void
.end method
