.class public Lcom/google/android/videos/store/BackwardCompatibleObjectInputStream;
.super Ljava/io/ObjectInputStream;
.source "BackwardCompatibleObjectInputStream.java"


# instance fields
.field private final mappings:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/io/ObjectStreamClass;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "input"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/StreamCorruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0, p1}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 58
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/store/BackwardCompatibleObjectInputStream;->mappings:Ljava/util/Map;

    .line 59
    return-void
.end method


# virtual methods
.method protected readClassDescriptor()Ljava/io/ObjectStreamClass;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 78
    invoke-super {p0}, Ljava/io/ObjectInputStream;->readClassDescriptor()Ljava/io/ObjectStreamClass;

    move-result-object v3

    .line 79
    .local v3, "loadedStreamClass":Ljava/io/ObjectStreamClass;
    iget-object v4, p0, Lcom/google/android/videos/store/BackwardCompatibleObjectInputStream;->mappings:Ljava/util/Map;

    invoke-virtual {v3}, Ljava/io/ObjectStreamClass;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 80
    .local v1, "delegateStreamClasses":Ljava/util/List;, "Ljava/util/List<Ljava/io/ObjectStreamClass;>;"
    if-eqz v1, :cond_1

    .line 81
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 82
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/ObjectStreamClass;

    .line 83
    .local v0, "delegateStreamClass":Ljava/io/ObjectStreamClass;
    invoke-virtual {v0}, Ljava/io/ObjectStreamClass;->getSerialVersionUID()J

    move-result-wide v4

    invoke-virtual {v3}, Ljava/io/ObjectStreamClass;->getSerialVersionUID()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    .line 88
    .end local v0    # "delegateStreamClass":Ljava/io/ObjectStreamClass;
    .end local v2    # "i":I
    :goto_1
    return-object v0

    .line 81
    .restart local v0    # "delegateStreamClass":Ljava/io/ObjectStreamClass;
    .restart local v2    # "i":I
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .end local v0    # "delegateStreamClass":Ljava/io/ObjectStreamClass;
    .end local v2    # "i":I
    :cond_1
    move-object v0, v3

    .line 88
    goto :goto_1
.end method

.method public registerDelegate(Ljava/lang/Class;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/io/Serializable;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/io/Serializable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/io/Serializable;>;"
    .local p2, "backwardCompatibleClazz":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/io/Serializable;>;"
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/videos/store/BackwardCompatibleObjectInputStream;->registerDelegate(Ljava/lang/String;Ljava/lang/Class;)V

    .line 64
    return-void
.end method

.method public registerDelegate(Ljava/lang/String;Ljava/lang/Class;)V
    .locals 2
    .param p1, "className"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/io/Serializable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 68
    .local p2, "backwardCompatibleClazz":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/io/Serializable;>;"
    iget-object v1, p0, Lcom/google/android/videos/store/BackwardCompatibleObjectInputStream;->mappings:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 69
    .local v0, "legacyClasses":Ljava/util/List;, "Ljava/util/List<Ljava/io/ObjectStreamClass;>;"
    if-nez v0, :cond_0

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "legacyClasses":Ljava/util/List;, "Ljava/util/List<Ljava/io/ObjectStreamClass;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 71
    .restart local v0    # "legacyClasses":Ljava/util/List;, "Ljava/util/List<Ljava/io/ObjectStreamClass;>;"
    iget-object v1, p0, Lcom/google/android/videos/store/BackwardCompatibleObjectInputStream;->mappings:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    :cond_0
    invoke-static {p2}, Ljava/io/ObjectStreamClass;->lookup(Ljava/lang/Class;)Ljava/io/ObjectStreamClass;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    return-void
.end method
