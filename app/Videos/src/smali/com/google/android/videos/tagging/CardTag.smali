.class public Lcom/google/android/videos/tagging/CardTag;
.super Ljava/lang/Object;
.source "CardTag.java"


# static fields
.field public static final CURRENT_ACTORS:Lcom/google/android/videos/tagging/CardTag;

.field public static final RECENT_ACTORS:Lcom/google/android/videos/tagging/CardTag;


# instance fields
.field public final cardType:I

.field public final knowledgeEntity:Lcom/google/android/videos/tagging/KnowledgeEntity;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 18
    new-instance v0, Lcom/google/android/videos/tagging/CardTag;

    const/4 v1, 0x4

    invoke-direct {v0, v1, v2}, Lcom/google/android/videos/tagging/CardTag;-><init>(ILcom/google/android/videos/tagging/KnowledgeEntity;)V

    sput-object v0, Lcom/google/android/videos/tagging/CardTag;->RECENT_ACTORS:Lcom/google/android/videos/tagging/CardTag;

    .line 23
    new-instance v0, Lcom/google/android/videos/tagging/CardTag;

    const/4 v1, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/videos/tagging/CardTag;-><init>(ILcom/google/android/videos/tagging/KnowledgeEntity;)V

    sput-object v0, Lcom/google/android/videos/tagging/CardTag;->CURRENT_ACTORS:Lcom/google/android/videos/tagging/CardTag;

    return-void
.end method

.method private constructor <init>(ILcom/google/android/videos/tagging/KnowledgeEntity;)V
    .locals 0
    .param p1, "cardType"    # I
    .param p2, "knowledgeEntity"    # Lcom/google/android/videos/tagging/KnowledgeEntity;

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput p1, p0, Lcom/google/android/videos/tagging/CardTag;->cardType:I

    .line 50
    iput-object p2, p0, Lcom/google/android/videos/tagging/CardTag;->knowledgeEntity:Lcom/google/android/videos/tagging/KnowledgeEntity;

    .line 51
    return-void
.end method

.method public static forSong(Lcom/google/android/videos/tagging/KnowledgeEntity$Song;)Lcom/google/android/videos/tagging/CardTag;
    .locals 2
    .param p0, "song"    # Lcom/google/android/videos/tagging/KnowledgeEntity$Song;

    .prologue
    .line 34
    new-instance v0, Lcom/google/android/videos/tagging/CardTag;

    const/4 v1, 0x3

    invoke-direct {v0, v1, p0}, Lcom/google/android/videos/tagging/CardTag;-><init>(ILcom/google/android/videos/tagging/KnowledgeEntity;)V

    return-object v0
.end method
