.class final Lcom/google/android/videos/GservicesConfig;
.super Ljava/lang/Object;
.source "GservicesConfig.java"

# interfaces
.implements Lcom/google/android/videos/Config;


# static fields
.field private static final BASE_API_URI:Landroid/net/Uri;

.field private static final BASE_KNOWLEDGE_URI:Landroid/net/Uri;

.field private static final YOUTUBE_STATS_URI:Landroid/net/Uri;

.field private static final legacyVideoHeightCapAllowedLegacyItags:[Ljava/lang/String;

.field private static final legacyVideoHeightCapThresholds:[I


# instance fields
.field private final context:Landroid/content/Context;

.field private defaultDisplayVideoHeightCap:I

.field private final drmManagerProvider:Lcom/google/android/videos/drm/DrmManager$Provider;

.field private final eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private final isEduDevice:Z

.field private final isRestrictedUser:Z

.field private final isTv:Z

.field private final preferences:Landroid/content/SharedPreferences;

.field private final resolver:Landroid/content/ContentResolver;

.field private final uriRewriter:Lcom/google/android/videos/utils/UriRewriter;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v1, 0x4

    .line 171
    const-string v0, "https://www.googleapis.com/android_video/v1/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/videos/GservicesConfig;->BASE_API_URI:Landroid/net/Uri;

    .line 173
    const-string v0, "https://play.google.com/video/downloads/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/videos/GservicesConfig;->BASE_KNOWLEDGE_URI:Landroid/net/Uri;

    .line 175
    const-string v0, "https://s.youtube.com/api/stats/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/videos/GservicesConfig;->YOUTUBE_STATS_URI:Landroid/net/Uri;

    .line 190
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/videos/GservicesConfig;->legacyVideoHeightCapThresholds:[I

    .line 200
    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "36,119"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "36,119,18,61,81"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "36,119,18,61,81,59,62,114"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "36,119,18,61,81,59,62,114,22,63,88,113"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/videos/GservicesConfig;->legacyVideoHeightCapAllowedLegacyItags:[Ljava/lang/String;

    return-void

    .line 190
    nop

    :array_0
    .array-data 4
        0xf0
        0x168
        0x1e0
        0x2d0
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/ContentResolver;Lcom/google/android/videos/utils/UriRewriter;Landroid/content/pm/PackageManager;Lcom/google/android/videos/logging/EventLogger;Landroid/content/SharedPreferences;Lcom/google/android/videos/drm/DrmManager$Provider;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resolver"    # Landroid/content/ContentResolver;
    .param p3, "uriRewriter"    # Lcom/google/android/videos/utils/UriRewriter;
    .param p4, "packageManager"    # Landroid/content/pm/PackageManager;
    .param p5, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;
    .param p6, "preferences"    # Landroid/content/SharedPreferences;
    .param p7, "drmManagerProvider"    # Lcom/google/android/videos/drm/DrmManager$Provider;

    .prologue
    const/16 v3, 0x12

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 241
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/GservicesConfig;->defaultDisplayVideoHeightCap:I

    .line 246
    iput-object p2, p0, Lcom/google/android/videos/GservicesConfig;->resolver:Landroid/content/ContentResolver;

    .line 247
    iput-object p1, p0, Lcom/google/android/videos/GservicesConfig;->context:Landroid/content/Context;

    .line 248
    iput-object p3, p0, Lcom/google/android/videos/GservicesConfig;->uriRewriter:Lcom/google/android/videos/utils/UriRewriter;

    .line 249
    iput-object p5, p0, Lcom/google/android/videos/GservicesConfig;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 250
    iput-object p6, p0, Lcom/google/android/videos/GservicesConfig;->preferences:Landroid/content/SharedPreferences;

    .line 251
    iput-object p7, p0, Lcom/google/android/videos/GservicesConfig;->drmManagerProvider:Lcom/google/android/videos/drm/DrmManager$Provider;

    .line 252
    invoke-static {p4}, Lcom/google/android/videos/utils/Util;->isTv(Landroid/content/pm/PackageManager;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/videos/GservicesConfig;->isTv:Z

    .line 253
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    if-lt v0, v3, :cond_0

    invoke-direct {p0}, Lcom/google/android/videos/GservicesConfig;->isEduDeviceV18()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/videos/GservicesConfig;->isEduDevice:Z

    .line 254
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    if-lt v0, v3, :cond_1

    invoke-direct {p0}, Lcom/google/android/videos/GservicesConfig;->isRestrictedUserV18()Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/videos/GservicesConfig;->isRestrictedUser:Z

    .line 255
    return-void

    :cond_0
    move v0, v2

    .line 253
    goto :goto_0

    :cond_1
    move v1, v2

    .line 254
    goto :goto_1
.end method

.method public static bulkCache(Landroid/content/ContentResolver;)V
    .locals 3
    .param p0, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 1243
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "videos"

    aput-object v2, v0, v1

    invoke-static {p0, v0}, Lcom/google/android/gsf/Gservices;->bulkCacheByPrefix(Landroid/content/ContentResolver;[Ljava/lang/String;)V

    .line 1244
    return-void
.end method

.method private defaultDisplayVideoHeightCap()I
    .locals 3

    .prologue
    const v2, 0x7fffffff

    const/16 v1, 0x2d0

    .line 1208
    iget-boolean v0, p0, Lcom/google/android/videos/GservicesConfig;->isTv:Z

    if-eqz v0, :cond_0

    .line 1209
    const-string v0, "video_height_cap_tv"

    invoke-direct {p0, v0, v2}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1215
    :goto_0
    return v0

    .line 1210
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/GservicesConfig;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/videos/utils/DisplayUtil;->isLargeTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1211
    const-string v0, "video_height_cap_large_tablet"

    invoke-direct {p0, v0, v2}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0

    .line 1212
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/GservicesConfig;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/videos/utils/DisplayUtil;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1213
    const-string v0, "video_height_cap_tablet"

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0

    .line 1215
    :cond_2
    const-string v0, "video_height_cap_phone"

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public static deviceCountry(Landroid/content/ContentResolver;)Ljava/lang/String;
    .locals 3
    .param p0, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    const/4 v2, 0x0

    .line 1270
    const-string v1, "country_override"

    invoke-static {p0, v1, v2}, Lcom/google/android/videos/GservicesConfig;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1271
    .local v0, "countryCode":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/videos/GservicesConfig;->isValidCountryCode(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1273
    const-string v1, "device_country"

    invoke-static {p0, v1, v2}, Lcom/google/android/videos/GservicesConfig;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1275
    :cond_0
    invoke-static {v0}, Lcom/google/android/videos/GservicesConfig;->isValidCountryCode(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1277
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    .line 1279
    :cond_1
    invoke-static {v0}, Lcom/google/android/videos/GservicesConfig;->isValidCountryCode(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1282
    const-string v0, "??"

    .line 1284
    :cond_2
    invoke-static {v0}, Lcom/google/android/videos/utils/Util;->toUpperInvariant(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getAllowedLegacyItags(Landroid/view/Display;)Ljava/util/List;
    .locals 4
    .param p1, "display"    # Landroid/view/Display;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Display;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 570
    const-string v2, "allowedItags"

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/google/android/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 571
    .local v0, "allowed":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 572
    invoke-direct {p0, p1}, Lcom/google/android/videos/GservicesConfig;->getLegacyVideoHeightCapThresholdIndex(Landroid/view/Display;)I

    move-result v1

    .line 573
    .local v1, "thresholdIndex":I
    sget-object v2, Lcom/google/android/videos/GservicesConfig;->legacyVideoHeightCapAllowedLegacyItags:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 575
    .end local v1    # "thresholdIndex":I
    :cond_0
    const-string v2, ","

    invoke-static {v0, v2}, Lcom/google/android/videos/utils/Util;->splitIntegersToList(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    return-object v2
.end method

.method private static getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z
    .locals 2
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Z

    .prologue
    .line 1298
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "videos:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p2}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private getBoolean(Ljava/lang/String;Z)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Z

    .prologue
    .line 1294
    iget-object v0, p0, Lcom/google/android/videos/GservicesConfig;->resolver:Landroid/content/ContentResolver;

    invoke-static {v0, p1, p2}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private getDashFormats(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Set;
    .locals 5
    .param p1, "defaultFormats"    # Ljava/lang/String;
    .param p2, "whitelistKey"    # Ljava/lang/String;
    .param p3, "blacklistKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 495
    const-string v3, ","

    invoke-static {p1, v3}, Lcom/google/android/videos/utils/Util;->splitIntegersToSet(Ljava/lang/String;Ljava/lang/String;)Ljava/util/HashSet;

    move-result-object v1

    .line 497
    .local v1, "dashItags":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-direct {p0, p2, v4}, Lcom/google/android/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 498
    .local v2, "whitelist":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 499
    const-string v3, ","

    invoke-static {v2, v3}, Lcom/google/android/videos/utils/Util;->splitIntegersToList(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 502
    :cond_0
    invoke-direct {p0, p3, v4}, Lcom/google/android/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 503
    .local v0, "blacklist":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 504
    const-string v3, ","

    invoke-static {v0, v3}, Lcom/google/android/videos/utils/Util;->splitIntegersToList(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 506
    :cond_1
    return-object v1
.end method

.method private getDefaultFormats(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .param p2, "order"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 594
    .local p1, "allowed":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const-string v4, ","

    invoke-static {p2, v4}, Lcom/google/android/videos/utils/Util;->splitIntegersToList(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 595
    .local v2, "orderList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 596
    .local v3, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 597
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 598
    .local v1, "itag":I
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 599
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 596
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 602
    .end local v1    # "itag":I
    :cond_1
    return-object v3
.end method

.method public static getExperimentId(Landroid/content/ContentResolver;)Ljava/lang/String;
    .locals 2
    .param p0, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 1260
    const-string v0, "experiment_id"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getFloat(Ljava/lang/String;F)F
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # F

    .prologue
    .line 1322
    iget-object v0, p0, Lcom/google/android/videos/GservicesConfig;->resolver:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "videos:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lcom/google/android/gsf/Gservices;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method private static getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    .locals 2
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # I

    .prologue
    .line 1314
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "videos:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p2}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private getInt(Ljava/lang/String;I)I
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # I

    .prologue
    .line 1310
    iget-object v0, p0, Lcom/google/android/videos/GservicesConfig;->resolver:Landroid/content/ContentResolver;

    invoke-static {v0, p1, p2}, Lcom/google/android/videos/GservicesConfig;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private getLegacyFormats(Landroid/view/Display;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p1, "display"    # Landroid/view/Display;
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "formatsOrder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Display;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 563
    const/4 v1, 0x0

    invoke-direct {p0, p2, v1}, Lcom/google/android/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 564
    .local v0, "formats":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, ","

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Util;->splitIntegersToList(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/videos/GservicesConfig;->getAllowedLegacyItags(Landroid/view/Display;)Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1, p3}, Lcom/google/android/videos/GservicesConfig;->getDefaultFormats(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    goto :goto_0
.end method

.method private getLegacyVideoHeightCapThresholdIndex(Landroid/view/Display;)I
    .locals 3
    .param p1, "display"    # Landroid/view/Display;

    .prologue
    .line 579
    invoke-virtual {p0, p1}, Lcom/google/android/videos/GservicesConfig;->videoHeightCap(Landroid/view/Display;)I

    move-result v1

    .line 581
    .local v1, "videoHeightCap":I
    const/16 v2, 0x168

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 582
    sget-object v2, Lcom/google/android/videos/GservicesConfig;->legacyVideoHeightCapThresholds:[I

    array-length v2, v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "index":I
    :goto_0
    if-ltz v0, :cond_1

    .line 583
    sget-object v2, Lcom/google/android/videos/GservicesConfig;->legacyVideoHeightCapThresholds:[I

    aget v2, v2, v0

    if-lt v1, v2, :cond_0

    .line 587
    .end local v0    # "index":I
    :goto_1
    return v0

    .line 582
    .restart local v0    # "index":I
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 587
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private getLong(Ljava/lang/String;J)J
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # J

    .prologue
    .line 1318
    iget-object v0, p0, Lcom/google/android/videos/GservicesConfig;->resolver:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "videos:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2, p3}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private getRewrittenUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Ljava/lang/String;

    .prologue
    .line 819
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/GservicesConfig;->getUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 820
    .local v0, "uri":Landroid/net/Uri;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/GservicesConfig;->uriRewriter:Lcom/google/android/videos/utils/UriRewriter;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/utils/UriRewriter;->rewrite(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0
.end method

.method private static getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Ljava/lang/String;

    .prologue
    .line 1306
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "videos:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p2}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Ljava/lang/String;

    .prologue
    .line 1302
    iget-object v0, p0, Lcom/google/android/videos/GservicesConfig;->resolver:Landroid/content/ContentResolver;

    invoke-static {v0, p1, p2}, Lcom/google/android/videos/GservicesConfig;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Ljava/lang/String;

    .prologue
    .line 824
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 825
    .local v0, "value":Ljava/lang/String;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0
.end method

.method public static gmsCoreAvailable(Landroid/content/ContentResolver;Landroid/content/Context;)Z
    .locals 2
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1251
    const-string v1, "gms_core_available"

    invoke-static {p1}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {p0, v1, v0}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static gservicesId(Landroid/content/ContentResolver;)J
    .locals 4
    .param p0, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 1247
    const-string v0, "android_id"

    const-wide/16 v2, 0x0

    invoke-static {p0, v0, v2, v3}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private isEduDeviceV18()Z
    .locals 3

    .prologue
    .line 259
    iget-object v1, p0, Lcom/google/android/videos/GservicesConfig;->context:Landroid/content/Context;

    const-string v2, "device_policy"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    .line 261
    .local v0, "dpm":Landroid/app/admin/DevicePolicyManager;
    if-eqz v0, :cond_0

    const-string v1, "com.google.android.apps.enterprise.dmagent"

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->isDeviceOwnerApp(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isRestrictedUserV18()Z
    .locals 4

    .prologue
    .line 266
    iget-object v2, p0, Lcom/google/android/videos/GservicesConfig;->context:Landroid/content/Context;

    const-string v3, "user"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    .line 267
    .local v0, "userManager":Landroid/os/UserManager;
    invoke-virtual {v0}, Landroid/os/UserManager;->getUserRestrictions()Landroid/os/Bundle;

    move-result-object v1

    .line 268
    .local v1, "userRestrictions":Landroid/os/Bundle;
    const-string v2, "no_modify_accounts"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    return v2
.end method

.method private static isValidCountryCode(Ljava/lang/String;)Z
    .locals 2
    .param p0, "countryCode"    # Ljava/lang/String;

    .prologue
    .line 1288
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static playLogServerUrl(Landroid/content/ContentResolver;)Ljava/lang/String;
    .locals 2
    .param p0, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 1264
    const-string v0, "play_analytics_server"

    const-string v1, "https://android.clients.google.com/play/log"

    invoke-static {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private preferModularDrm()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 957
    iget-object v2, p0, Lcom/google/android/videos/GservicesConfig;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    invoke-static {v2}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->getDefaultSecurityLevel(Lcom/google/android/videos/logging/EventLogger;)I

    move-result v2

    if-ne v2, v0, :cond_1

    .line 980
    :cond_0
    :goto_0
    return v0

    .line 963
    :cond_1
    const-string v2, "force_prefer_modular_drm"

    invoke-direct {p0, v2, v1}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    .line 968
    iget-object v2, p0, Lcom/google/android/videos/GservicesConfig;->preferences:Landroid/content/SharedPreferences;

    const-string v3, "prefer_modular_drm"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    .line 973
    iget-object v2, p0, Lcom/google/android/videos/GservicesConfig;->drmManagerProvider:Lcom/google/android/videos/drm/DrmManager$Provider;

    invoke-interface {v2}, Lcom/google/android/videos/drm/DrmManager$Provider;->getDrmManager()Lcom/google/android/videos/drm/DrmManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/videos/drm/DrmManager;->getDrmLevel()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_2

    .line 976
    iget-object v1, p0, Lcom/google/android/videos/GservicesConfig;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "prefer_modular_drm"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 980
    goto :goto_0
.end method

.method public static tokenCacheDurationMs(Landroid/content/ContentResolver;)I
    .locals 2
    .param p0, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 1256
    const-string v0, "token_cache_duration"

    const v1, 0x36ee80

    invoke-static {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public allowDownloads()Z
    .locals 2

    .prologue
    .line 661
    const-string v1, "allow_downloads"

    iget-boolean v0, p0, Lcom/google/android/videos/GservicesConfig;->isTv:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public allowSurroundSoundFormats()Z
    .locals 2

    .prologue
    .line 461
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v1, 0x10

    if-le v0, v1, :cond_0

    const-string v0, "allowSurroundSound"

    iget-boolean v1, p0, Lcom/google/android/videos/GservicesConfig;->isTv:Z

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public anyVerticalEnabled(Ljava/lang/String;)Z
    .locals 1
    .param p1, "countryCode"    # Ljava/lang/String;

    .prologue
    .line 631
    invoke-virtual {p0, p1}, Lcom/google/android/videos/GservicesConfig;->showsVerticalEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/videos/GservicesConfig;->moviesVerticalEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public appendDoNotCountParam()Z
    .locals 2

    .prologue
    .line 745
    const-string v0, "appendDncParam"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public atHomeRobotTokenRequestUri()Ljava/lang/String;
    .locals 3

    .prologue
    .line 450
    iget-object v0, p0, Lcom/google/android/videos/GservicesConfig;->uriRewriter:Lcom/google/android/videos/utils/UriRewriter;

    const-string v1, "at_home_robot_token_request_uri"

    const-string v2, "https://play.google.com/video/license/GetRobotToken"

    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/UriRewriter;->rewrite(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public audioVirtualizerEnabled()Z
    .locals 2

    .prologue
    .line 780
    const-string v0, "audio_virtualizer_enabled"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public baseApiUri()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 704
    iget-object v0, p0, Lcom/google/android/videos/GservicesConfig;->uriRewriter:Lcom/google/android/videos/utils/UriRewriter;

    sget-object v1, Lcom/google/android/videos/GservicesConfig;->BASE_API_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/UriRewriter;->rewrite(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public baseKnowledgeUri()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 709
    iget-object v0, p0, Lcom/google/android/videos/GservicesConfig;->uriRewriter:Lcom/google/android/videos/utils/UriRewriter;

    sget-object v1, Lcom/google/android/videos/GservicesConfig;->BASE_KNOWLEDGE_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/UriRewriter;->rewrite(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public blacklistedVersionsRegex()Ljava/lang/String;
    .locals 2

    .prologue
    .line 646
    const-string v0, "blacklisted_versions"

    const-string v1, ""

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public castDebuggingEnabled()Z
    .locals 2

    .prologue
    .line 803
    const-string v0, "cast_debugging_enabled"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public castV2Enabled()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 785
    sget v1, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v2, 0x9

    if-lt v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/videos/GservicesConfig;->isTv:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/videos/GservicesConfig;->panoEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "cast_v2_enabled"

    invoke-direct {p0, v1, v0}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public castV2ReceiverAppId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 793
    const-string v0, "cast_v2_receiver_app_id"

    const-string v1, "9381F2BD"

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public dashVideoFormats()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 478
    const-string v0, "133,134,135,136,137,142,143,144,145,146,212,213,214,215,216,217,222,223,224,225,226,227"

    const-string v1, "dashVideoItagWhitelist"

    const-string v2, "dashVideoItagBlacklist"

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/videos/GservicesConfig;->getDashFormats(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public dashVideoHighEdgeFormats()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 483
    const-string v0, "213,215,223,225"

    const-string v1, "dashVideoHighEdgeItagWhitelist"

    const-string v2, "dashVideoHighEdgeItagBlacklist"

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/videos/GservicesConfig;->getDashFormats(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public dashVideoLowEdgeFormats()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 489
    const-string v0, "136,145"

    const-string v1, "dashVideoLowEdgeItagWhitelist"

    const-string v2, "dashVideoLowEdgeItagBlacklist"

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/videos/GservicesConfig;->getDashFormats(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public deviceCapabilitiesFilterEnabled()Z
    .locals 2

    .prologue
    .line 938
    const-string v0, "device_capabilities_filter_enabled"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public deviceCountry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/android/videos/GservicesConfig;->resolver:Landroid/content/ContentResolver;

    invoke-static {v0}, Lcom/google/android/videos/GservicesConfig;->deviceCountry(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public disableWatchNow()Z
    .locals 2

    .prologue
    .line 613
    const-string v0, "disable_watch_now"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public dogfoodEnabled()Z
    .locals 2

    .prologue
    .line 656
    const-string v0, "dogfood_enabled"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public exoAbrAlgorithm()I
    .locals 2

    .prologue
    .line 1137
    const-string v0, "exo_abr_algo"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public exoAlternateRedirectEnabled()Z
    .locals 2

    .prologue
    .line 1108
    const-string v0, "exo_alternate_redirect_enabled_n"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public exoAudioTrackMinBufferMultiplicationFactor()F
    .locals 2

    .prologue
    .line 1017
    const-string v0, "exo_audiotrack_min_buffer_multiplication_factor"

    const/high16 v1, 0x40800000    # 4.0f

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getFloat(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public exoBandwidthBucketHistoryMinCount()I
    .locals 2

    .prologue
    .line 1161
    const-string v0, "exo_bandwidth_bucket_history_min_count"

    const/16 v1, 0x1f4

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public exoBandwidthBucketHistorySelectionPercentile()F
    .locals 2

    .prologue
    .line 1166
    const-string v0, "exo_bandwidth_bucket_history_selection_percentile"

    const v1, 0x3f666666    # 0.9f

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getFloat(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public exoBandwidthFraction()F
    .locals 2

    .prologue
    .line 1098
    const-string v0, "exo_bandwidth_fraction"

    const v1, 0x3f4ccccd    # 0.8f

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getFloat(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public exoBbaOneLowThresholdQueueSize()I
    .locals 2

    .prologue
    .line 1142
    const-string v0, "exo_bba_one_low_threshold_queue_size"

    const/4 v1, 0x3

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public exoBbaOneSlope()I
    .locals 2

    .prologue
    .line 1147
    const-string v0, "exo_bba_one_slope"

    const v1, 0x30d40

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public exoBufferChunkCount()I
    .locals 2

    .prologue
    .line 1028
    iget-boolean v0, p0, Lcom/google/android/videos/GservicesConfig;->isTv:Z

    if-eqz v0, :cond_0

    .line 1029
    const-string v0, "exo_buffer_chunk_count_tv_n"

    const/16 v1, 0x3e8

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1031
    :goto_0
    return v0

    :cond_0
    const-string v0, "exo_buffer_chunk_count_n"

    const/16 v1, 0x28a

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public exoBufferChunkSize()I
    .locals 2

    .prologue
    .line 1023
    const-string v0, "exo_buffer_chunk_size_n"

    const v1, 0x19000

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public exoEarlyPlaybackCutoffTimeMs()I
    .locals 2

    .prologue
    .line 1171
    const-string v0, "exo_early_playback_cutoff_time_ms"

    const/16 v1, 0x7530

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public exoHighPoolLoad()F
    .locals 2

    .prologue
    .line 1089
    iget-boolean v0, p0, Lcom/google/android/videos/GservicesConfig;->isTv:Z

    if-eqz v0, :cond_0

    .line 1090
    const-string v0, "exo_high_pool_load_tv"

    const v1, 0x7f7fffff    # Float.MAX_VALUE

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getFloat(Ljava/lang/String;F)F

    move-result v0

    .line 1092
    :goto_0
    return v0

    :cond_0
    const-string v0, "exo_high_pool_load"

    const v1, 0x3f666666    # 0.9f

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getFloat(Ljava/lang/String;F)F

    move-result v0

    goto :goto_0
.end method

.method public exoHighWatermarkMs()I
    .locals 2

    .prologue
    .line 1071
    iget-boolean v0, p0, Lcom/google/android/videos/GservicesConfig;->isTv:Z

    if-eqz v0, :cond_0

    .line 1072
    const-string v0, "exo_high_watermark_ms_tv"

    const v1, 0x7fffffff

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1074
    :goto_0
    return v0

    :cond_0
    const-string v0, "exo_high_watermark_ms"

    const v1, 0xafc8

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public exoLoadTimeoutMs()I
    .locals 2

    .prologue
    .line 1103
    const-string v0, "exo_load_timeout_ms"

    const/16 v1, 0x2710

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public exoLowPoolLoad()F
    .locals 2

    .prologue
    .line 1080
    iget-boolean v0, p0, Lcom/google/android/videos/GservicesConfig;->isTv:Z

    if-eqz v0, :cond_0

    .line 1081
    const-string v0, "exo_low_pool_load_tv"

    const v1, 0x7f7fffff    # Float.MAX_VALUE

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getFloat(Ljava/lang/String;F)F

    move-result v0

    .line 1083
    :goto_0
    return v0

    :cond_0
    const-string v0, "exo_low_pool_load"

    const v1, 0x3ecccccd    # 0.4f

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getFloat(Ljava/lang/String;F)F

    move-result v0

    goto :goto_0
.end method

.method public exoLowWatermarkMs()I
    .locals 2

    .prologue
    .line 1062
    iget-boolean v0, p0, Lcom/google/android/videos/GservicesConfig;->isTv:Z

    if-eqz v0, :cond_0

    .line 1063
    const-string v0, "exo_low_watermark_ms_tv"

    const v1, 0x7fffffff

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1065
    :goto_0
    return v0

    :cond_0
    const-string v0, "exo_low_watermark_ms"

    const/16 v1, 0x7530

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public exoLqVideoHeightCap(Landroid/view/Display;Z)I
    .locals 3
    .param p1, "display"    # Landroid/view/Display;
    .param p2, "isFastNetwork"    # Z

    .prologue
    .line 1055
    if-eqz p2, :cond_0

    const-string v1, "exo_mq_video_height_cap"

    const/16 v2, 0x1e0

    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1057
    .local v0, "lqVideoHeightCap":I
    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/videos/GservicesConfig;->exoOnlineVideoHeightCap(Landroid/view/Display;)I

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    return v1

    .line 1055
    .end local v0    # "lqVideoHeightCap":I
    :cond_0
    const-string v1, "exo_lq_video_height_cap"

    const/16 v2, 0x168

    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public exoMinBufferMs()I
    .locals 2

    .prologue
    .line 1176
    const-string v0, "exo_min_buffer_ms"

    const/16 v1, 0x1f4

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public exoMinLoadableRetryCount()I
    .locals 2

    .prologue
    .line 1113
    const-string v0, "exo_min_loadable_retries"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public exoMinRebufferMs()I
    .locals 2

    .prologue
    .line 1181
    const-string v0, "exo_min_rebuffer_ms"

    const/16 v1, 0x1388

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public exoOfflineVideoHeightCap()I
    .locals 3

    .prologue
    .line 1048
    iget-object v1, p0, Lcom/google/android/videos/GservicesConfig;->context:Landroid/content/Context;

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 1050
    .local v0, "display":Landroid/view/Display;
    invoke-virtual {p0, v0}, Lcom/google/android/videos/GservicesConfig;->videoHeightCap(Landroid/view/Display;)I

    move-result v1

    return v1
.end method

.method public exoOnlineVideoHeightCap(Landroid/view/Display;)I
    .locals 1
    .param p1, "display"    # Landroid/view/Display;

    .prologue
    .line 1042
    invoke-virtual {p0, p1}, Lcom/google/android/videos/GservicesConfig;->videoHeightCap(Landroid/view/Display;)I

    move-result v0

    return v0
.end method

.method public exoPlayClearSamplesWithoutKeys()Z
    .locals 2

    .prologue
    .line 1186
    const-string v0, "exo_play_clear_samples_without_keys"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public exoSmoothFrameRelease()Z
    .locals 3

    .prologue
    .line 1191
    const-string v1, "exo_smooth_frames"

    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public exoStartResolutionAlgorithm()I
    .locals 2

    .prologue
    .line 1152
    iget-boolean v0, p0, Lcom/google/android/videos/GservicesConfig;->isTv:Z

    if-eqz v0, :cond_0

    .line 1153
    const-string v0, "exo_startres_tv"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1155
    :goto_0
    return v0

    :cond_0
    const-string v0, "exo_startres_mobile"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public exoUseBlockBufferPool()Z
    .locals 3

    .prologue
    .line 1037
    const-string v1, "exo_use_block_buffer_pool"

    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v2, 0x14

    if-ge v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public exoVsyncFrameRelease(Landroid/view/Display;)Z
    .locals 4
    .param p1, "display"    # Landroid/view/Display;

    .prologue
    const/4 v0, 0x0

    .line 1196
    invoke-virtual {p1}, Landroid/view/Display;->getDisplayId()I

    move-result v1

    if-nez v1, :cond_1

    .line 1197
    const-string v1, "exo_vsync_frames_primary"

    sget v2, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-direct {p0, v1, v0}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 1199
    :goto_0
    return v0

    :cond_1
    const-string v1, "exo_vsync_frames_secondary"

    invoke-direct {p0, v1, v0}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public externalApiEnabled()Z
    .locals 2

    .prologue
    .line 1231
    const-string v0, "external_api_enabled"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public fallbackDrmErrorCodes()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 466
    const-string v1, "fallbackDrmErrorCodes"

    const-string v2, "49,108"

    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 467
    .local v0, "formats":Ljava/lang/String;
    const-string v1, ","

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Util;->splitIntegersToList(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    return-object v1
.end method

.method public feedbackProductId()I
    .locals 2

    .prologue
    .line 719
    const-string v0, "feedback_product_id"

    const v1, 0x11077

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public feedbackSubmitUrlTemplate()Ljava/lang/String;
    .locals 2

    .prologue
    .line 740
    iget-object v0, p0, Lcom/google/android/videos/GservicesConfig;->uriRewriter:Lcom/google/android/videos/utils/UriRewriter;

    const-string v1, "https://www.google.com/tools/feedback/submit?at={$GF_TOKEN}"

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/UriRewriter;->rewrite(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public feedbackTokenExtractorRegex()Ljava/lang/String;
    .locals 2

    .prologue
    .line 735
    const-string v0, "feedback_token_extractor_regex"

    const-string v1, "\\bGF_TOKEN[^A-Za-z0-9=]*=\\s*[\'\"]([^\'\"]+)[\'\"];"

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public feedbackTokenUrl()Ljava/lang/String;
    .locals 4

    .prologue
    .line 729
    iget-object v0, p0, Lcom/google/android/videos/GservicesConfig;->uriRewriter:Lcom/google/android/videos/utils/UriRewriter;

    const-string v1, "https://www.google.com/tools/feedback/mobile_feedback?productId={$PRODUCT_ID}"

    const-string v2, "{$PRODUCT_ID}"

    invoke-virtual {p0}, Lcom/google/android/videos/GservicesConfig;->feedbackProductId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/UriRewriter;->rewrite(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public fieldProvisionedFormats()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 607
    const-string v1, "fieldProvisionedItags"

    const-string v2, "114,62,61,81,119"

    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 608
    .local v0, "formats":Ljava/lang/String;
    const-string v1, ","

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Util;->splitIntegersToList(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    return-object v1
.end method

.method public forceAppLevelDrm()Z
    .locals 2

    .prologue
    .line 456
    const-string v0, "force_app_level_drm"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public forceMirrorMode()Z
    .locals 2

    .prologue
    .line 890
    const-string v0, "force_mirror_mode"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public freeMovieWelcomeEnabled()Z
    .locals 2

    .prologue
    .line 750
    const-string v0, "free_movie_welcome_enabled"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public gcmMessagingEnabled()Z
    .locals 2

    .prologue
    .line 675
    invoke-virtual {p0}, Lcom/google/android/videos/GservicesConfig;->gcmRegistrationEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 677
    const/4 v0, 0x0

    .line 679
    :goto_0
    return v0

    :cond_0
    const-string v0, "gcm_messaging_enabled"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public gcmRegistrationEnabled()Z
    .locals 2

    .prologue
    .line 666
    invoke-virtual {p0}, Lcom/google/android/videos/GservicesConfig;->gmsCoreAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 668
    const/4 v0, 0x0

    .line 670
    :goto_0
    return v0

    :cond_0
    const-string v0, "gcm_registration_enabled"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public generateHttp204Url()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1226
    const-string v0, "generate_http_204_url"

    const-string v1, "http://clients3.google.com/generate_204"

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getExperimentId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/videos/GservicesConfig;->resolver:Landroid/content/ContentResolver;

    invoke-static {v0}, Lcom/google/android/videos/GservicesConfig;->getExperimentId(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPlayLogImpressionSettleTimeMillis()I
    .locals 2

    .prologue
    .line 1236
    const-string v0, "impression_settle_time_ms"

    const/16 v1, 0x7d0

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public gmsCoreAvailable()Z
    .locals 2

    .prologue
    .line 830
    iget-object v0, p0, Lcom/google/android/videos/GservicesConfig;->resolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/google/android/videos/GservicesConfig;->context:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/google/android/videos/GservicesConfig;->gmsCoreAvailable(Landroid/content/ContentResolver;Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public gservicesId()J
    .locals 4

    .prologue
    .line 1221
    iget-object v0, p0, Lcom/google/android/videos/GservicesConfig;->resolver:Landroid/content/ContentResolver;

    const-string v1, "android_id"

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public knowledgeDimEntitiesAfterDisappearingForMillis()I
    .locals 2

    .prologue
    .line 870
    const-string v0, "knowledge_dim_entities_after_disappearing_for_millis"

    const/16 v1, 0x2710

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public knowledgeDontDimEntitiesReappearingWithinMillis()I
    .locals 2

    .prologue
    .line 875
    const-string v0, "knowledge_dont_dim_entities_reappearing_within_millis"

    const/16 v1, 0x4e20

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public knowledgeDontRemoveEntitiesReappearingWithinMillis()I
    .locals 2

    .prologue
    .line 885
    const-string v0, "knowledge_dont_remove_entities_reappearing_within_millis"

    const/16 v1, 0x4e20

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public knowledgeEnabled()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 836
    sget v2, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v3, 0xe

    if-ge v2, v3, :cond_1

    .line 850
    :cond_0
    :goto_0
    return v1

    .line 841
    :cond_1
    const-string v2, "knowledge_enabled_tablets"

    invoke-direct {p0, v2, v1}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 845
    iget-object v2, p0, Lcom/google/android/videos/GservicesConfig;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/videos/utils/DisplayUtil;->isTablet(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v1, v0

    .line 846
    goto :goto_0

    .line 850
    :cond_2
    const-string v2, "knowledge_enabled_phones"

    sget v3, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v4, 0x10

    if-lt v3, v4, :cond_3

    :goto_1
    invoke-direct {p0, v2, v0}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public knowledgeFeedbackTypeId()I
    .locals 2

    .prologue
    .line 724
    const-string v0, "knowledge_feedback_type_id"

    const v1, 0x17898

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public knowledgeRecheckDataAfterMillis()J
    .locals 4

    .prologue
    .line 855
    const-string v0, "knowledge_recheck_data_after_millis"

    const-wide/32 v2, 0x240c8400

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/videos/GservicesConfig;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public knowledgeRemoveEntitiesAfterDisappearingForMillis()I
    .locals 2

    .prologue
    .line 880
    const-string v0, "knowledge_remove_entities_after_disappearing_for_millis"

    const/16 v1, 0x2710

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public knowledgeShowRecentActorsWithinMillis()I
    .locals 2

    .prologue
    .line 865
    const-string v0, "knowledge_show_recent_actors_within_millis"

    const/16 v1, 0x2710

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public latestVersion()I
    .locals 2

    .prologue
    .line 651
    const-string v0, "latest_version"

    const/16 v1, 0x762a

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public legacyBufferingEventInitialIgnoreWindowMillis()J
    .locals 4

    .prologue
    .line 416
    const-string v0, "legacyBufferingEventInitialIgnoreWindow"

    const-wide/16 v2, 0x1388

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/videos/GservicesConfig;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public legacyBufferingEventWindowMillis()J
    .locals 4

    .prologue
    .line 411
    const-string v0, "legacyBufferingEventWindow"

    const-wide/32 v2, 0xea60

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/videos/GservicesConfig;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public legacyBufferingEventsForQualityDrop()I
    .locals 2

    .prologue
    .line 422
    const-string v0, "legacyBufferingEventsForQualityDrop"

    const/4 v1, 0x3

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public maxConcurrentLicenseTasks()I
    .locals 2

    .prologue
    .line 307
    const-string v0, "maxConcurrentLicenseTasks"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public maxConcurrentOrBackedOffPinningTasks()I
    .locals 2

    .prologue
    .line 316
    const-string v0, "maxConcurrentPinningTasks"

    const/4 v1, 0x3

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public maxConcurrentUpdateUserdataTasks()I
    .locals 2

    .prologue
    .line 324
    const-string v0, "maxConcurrentUpdateUserdataTasks"

    const/4 v1, 0x3

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public maxLicenseRefreshTaskRetryDelayMillis()I
    .locals 2

    .prologue
    .line 356
    const-string v0, "maxLicenseRefreshRetryDelayMillis"

    const v1, 0x2932e00

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public maxLicenseReleaseTaskRetryDelayMillis()I
    .locals 2

    .prologue
    .line 372
    const-string v0, "maxLicenseReleaseRetryDelayMillis"

    const v1, 0x6ddd00

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public maxNewContentNotificationDelayMillis()J
    .locals 4

    .prologue
    .line 895
    const-string v0, "max_new_content_notification_delay_millis"

    const-wide/32 v2, 0x240c8400

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/videos/GservicesConfig;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public maxPinningTaskRetries()I
    .locals 2

    .prologue
    .line 332
    const-string v0, "maxPinningTaskRetries"

    const/16 v1, 0x14

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public maxPinningTaskRetryDelayMillis()I
    .locals 2

    .prologue
    .line 388
    const-string v0, "maxPinningTaskRetryDelayMillis"

    const v1, 0xea60

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public maxSuggestionClusters()I
    .locals 2

    .prologue
    .line 923
    const-string v0, "playstore_max_suggestion_clusters"

    const/16 v1, 0x14

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public maxSuggestionsPerCluster()I
    .locals 2

    .prologue
    .line 928
    const-string v0, "playstore_max_suggestions_per_cluster"

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public maxUpdateUserdataTaskRetries()I
    .locals 2

    .prologue
    .line 340
    const-string v0, "maxUpdateUserdataTaskRetries"

    const/4 v1, 0x3

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public maxUpdateUserdataTaskRetryDelayMillis()I
    .locals 2

    .prologue
    .line 405
    const-string v0, "maxUpdateUserdataTaskRetryDelayMillis"

    const v1, 0x36ee80

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public minIntervalBetweenHerrevadReportSeconds()I
    .locals 2

    .prologue
    .line 283
    const-string v0, "minIntervalBetweenHerrevadReportSeconds"

    const/16 v1, 0x78

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public minLicenseRefreshTaskRetryDelayMillis()I
    .locals 2

    .prologue
    .line 348
    const-string v0, "minLicenseRefreshRetryDelayMillis"

    const/16 v1, 0x7530

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public minLicenseReleaseTaskRetryDelayMillis()I
    .locals 2

    .prologue
    .line 364
    const-string v0, "minLicenseReleaseRetryDelayMillis"

    const/16 v1, 0x1388

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public minPinningTaskRetryDelayMillis()I
    .locals 2

    .prologue
    .line 380
    const-string v0, "minPinningTaskRetryDelayMillis"

    const/16 v1, 0x2710

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public minUpdateUserdataTaskRetryDelayMillis()I
    .locals 2

    .prologue
    .line 396
    const-string v0, "minUpdateUserdataTaskRetryDelayMillis"

    const/16 v1, 0x2710

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public minimumVersion()I
    .locals 2

    .prologue
    .line 641
    const-string v0, "minimum_version"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public moviesVerticalEnabled(Ljava/lang/String;)Z
    .locals 3
    .param p1, "countryCode"    # Ljava/lang/String;

    .prologue
    .line 618
    const-string v1, "movie_vertical_countries"

    const-string v2, "AU,BR,CA,FR,DE,IN,MX,JP,RU,KR,ES,GB,US"

    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 620
    .local v0, "movieCountryCodes":Ljava/lang/String;
    invoke-virtual {v0, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    return v1
.end method

.method public moviesWelcomeFreeBrowseUri()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 760
    const-string v0, "movies_welcome_browse_uri"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getRewrittenUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public multiAudioEnabled()Z
    .locals 2

    .prologue
    .line 933
    const-string v0, "multi_audio_r"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public needsSystemUpdate()Z
    .locals 2

    .prologue
    .line 636
    const-string v0, "needs_system_update"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public orderedDashDownloadFormats()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 527
    const-string v1, "orderedDashDownloadItags"

    const-string v2, "224,222,143,142,259,150,149"

    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 530
    .local v0, "formats":Ljava/lang/String;
    const-string v1, ","

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Util;->splitIntegersToList(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    return-object v1
.end method

.method public orderedDashHqAudioFormats()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 513
    const-string v1, "orderedDashHqAudioItags"

    const-string v2, "261,258,259,256,150,141,149,140"

    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 514
    .local v0, "formats":Ljava/lang/String;
    const-string v1, ","

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Util;->splitIntegersToList(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    return-object v1
.end method

.method public orderedDashMqAudioFormats()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 521
    const-string v1, "orderedDashMqAudioItags"

    const-string v2, "259,256,149,140"

    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 522
    .local v0, "formats":Ljava/lang/String;
    const-string v1, ","

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Util;->splitIntegersToList(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    return-object v1
.end method

.method public orderedDownloadFormats()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 536
    iget-object v1, p0, Lcom/google/android/videos/GservicesConfig;->context:Landroid/content/Context;

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 538
    .local v0, "display":Landroid/view/Display;
    const-string v1, "orderedDownloadItags"

    const-string v2, "192,63,62,61,81,119"

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/videos/GservicesConfig;->getLegacyFormats(Landroid/view/Display;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public orderedHqStreamingFormats(Landroid/view/Display;)Ljava/util/List;
    .locals 2
    .param p1, "display"    # Landroid/view/Display;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Display;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 543
    const-string v0, "orderedHqItags"

    const-string v1, "180,159,186,113,63,22"

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getLegacyFormats(Landroid/view/Display;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public orderedLqStreamingFormats(Landroid/view/Display;)Ljava/util/List;
    .locals 2
    .param p1, "display"    # Landroid/view/Display;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Display;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 557
    const-string v0, "orderedLqItags"

    const-string v1, "119,81,61,114,62,36,18,59"

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getLegacyFormats(Landroid/view/Display;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public orderedMqStreamingFormats(Landroid/view/Display;)Ljava/util/List;
    .locals 2
    .param p1, "display"    # Landroid/view/Display;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Display;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 550
    const-string v0, "orderedMqItags"

    const-string v1, "114,62,61,81,119,59,18,36"

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getLegacyFormats(Landroid/view/Display;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public panoEnabled()Z
    .locals 3

    .prologue
    .line 808
    const-string v1, "pano_enabled"

    iget-object v0, p0, Lcom/google/android/videos/GservicesConfig;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v2, "android.software.leanback"

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/GservicesConfig;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v2, "android.software.leanback_only"

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public playbackDebugLoggingEnabled()Z
    .locals 2

    .prologue
    .line 1012
    const-string v0, "playback_debug_logging_enabled"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public recentActiveMillis()J
    .locals 4

    .prologue
    .line 901
    const-string v0, "recent_active_millis"

    const-wide/32 v2, 0x4d3f6400

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/videos/GservicesConfig;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public refreshLicensesOlderThanMillis()J
    .locals 4

    .prologue
    .line 299
    const-string v0, "refreshLicenseIfOlderThanMillis_n"

    const-wide/32 v2, 0x240c8400

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/videos/GservicesConfig;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public resyncFullShowAfterMillis()J
    .locals 4

    .prologue
    .line 699
    const-string v0, "resync_full_show_after_millis"

    const-wide/32 v2, 0x44aa200

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/videos/GservicesConfig;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public resyncSeasonAfterMillis()J
    .locals 4

    .prologue
    .line 684
    const-string v0, "resync_season_metadata_after_millis"

    const-wide/32 v2, 0x240c8400

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/videos/GservicesConfig;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public resyncShowAfterMillis()J
    .locals 4

    .prologue
    .line 694
    const-string v0, "resync_show_metadata_after_millis"

    const-wide/32 v2, 0x240c8400

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/videos/GservicesConfig;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public resyncVideoAfterMillis()J
    .locals 4

    .prologue
    .line 689
    const-string v0, "resync_video_metadata_after_millis"

    const-wide/32 v2, 0x240c8400

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/videos/GservicesConfig;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public retryCencDrmErrorCodes()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 472
    const-string v1, "retryCencDrmErrorCodes"

    const-string v2, "7000,8000"

    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 473
    .local v0, "formats":Ljava/lang/String;
    const-string v1, ","

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Util;->splitIntegersToList(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    return-object v1
.end method

.method public showsVerticalEnabled(Ljava/lang/String;)Z
    .locals 3
    .param p1, "countryCode"    # Ljava/lang/String;

    .prologue
    .line 625
    const-string v1, "show_vertical_countries"

    const-string v2, "US"

    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 626
    .local v0, "showCountryCodes":Ljava/lang/String;
    invoke-virtual {v0, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    return v1
.end method

.method public showsWelcomeFreeBrowseUri()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 765
    const-string v0, "shows_welcome_browse_uri"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getRewrittenUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public soundWelcomeVideoId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 755
    const-string v0, "sound_welcome_video_id"

    const-string v1, ""

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public suggestionsEnabled()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 916
    const-string v1, "playstore_suggestions_enabled_q"

    invoke-direct {p0, v1, v0}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "edu_check_enabled"

    invoke-direct {p0, v1, v0}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/videos/GservicesConfig;->isEduDevice:Z

    if-nez v1, :cond_2

    :cond_0
    const-string v1, "restricted_user_check_enabled"

    invoke-direct {p0, v1, v0}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/videos/GservicesConfig;->isRestrictedUser:Z

    if-nez v1, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public supportsAdaptivePlayback()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 985
    sget v4, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v5, 0x13

    if-ge v4, v5, :cond_1

    .line 1006
    :cond_0
    :goto_0
    return v2

    .line 988
    :cond_1
    const-string v4, "force_adaptive_on_o"

    invoke-direct {p0, v4, v2}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_2

    move v2, v3

    .line 990
    goto :goto_0

    .line 992
    :cond_2
    const-string v3, "force_adaptive_off_o"

    invoke-direct {p0, v3, v2}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_0

    .line 997
    :try_start_0
    const-string v3, "video/avc"

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/google/android/exoplayer/MediaCodecUtil;->getDecoderInfo(Ljava/lang/String;Z)Lcom/google/android/exoplayer/DecoderInfo;

    move-result-object v0

    .line 998
    .local v0, "decoderInfo":Lcom/google/android/exoplayer/DecoderInfo;
    iget-boolean v2, v0, Lcom/google/android/exoplayer/DecoderInfo;->adaptive:Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 999
    .end local v0    # "decoderInfo":Lcom/google/android/exoplayer/DecoderInfo;
    :catch_0
    move-exception v1

    .line 1001
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    const-string v3, "Failed to determine whether adaptive playback is supported"

    invoke-static {v3, v1}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1003
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v1

    .line 1005
    .local v1, "e":Ljava/lang/IllegalStateException;
    const-string v3, "Failed to determine whether adaptive playback is supported"

    invoke-static {v3, v1}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public transferServicePingIntervalMillis()J
    .locals 4

    .prologue
    .line 291
    const-string v0, "transferServicePingInterval_n"

    const-wide/32 v2, 0x55d4a80

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/videos/GservicesConfig;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public useDashForDownloads()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 951
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    .line 952
    .local v0, "sdkInt":I
    const/16 v2, 0x16

    if-lt v0, v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/videos/GservicesConfig;->preferModularDrm()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "use_dash_streams_for_downloads_r"

    invoke-direct {p0, v2, v1}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public useDashForStreaming()Z
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 943
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    .line 944
    .local v0, "sdkInt":I
    const/16 v1, 0x13

    if-lt v0, v1, :cond_2

    invoke-direct {p0}, Lcom/google/android/videos/GservicesConfig;->preferModularDrm()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v4, "use_dash_streams_m"

    const/16 v1, 0x14

    if-lt v0, v1, :cond_1

    move v1, v2

    :goto_0
    invoke-direct {p0, v4, v1}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "exo_non_adaptive_enabled"

    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/videos/GservicesConfig;->supportsAdaptivePlayback()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    :goto_1
    return v2

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_1
.end method

.method public usePlaybackPreparationLogger()Z
    .locals 2

    .prologue
    .line 860
    const-string v0, "use_playback_preparation_logger"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public useSslForDownloads()Z
    .locals 2

    .prologue
    .line 770
    const-string v0, "use_ssl_for_downloads"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public useSslForStreaming()Z
    .locals 2

    .prologue
    .line 775
    const-string v0, "use_ssl_for_streaming"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public videoHeightCap(Landroid/view/Display;)I
    .locals 5
    .param p1, "display"    # Landroid/view/Display;

    .prologue
    const/4 v4, -0x1

    .line 1118
    const-string v2, "video_height_cap"

    invoke-direct {p0, v2, v4}, Lcom/google/android/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 1119
    .local v1, "videoHeightCap":I
    if-ne v1, v4, :cond_0

    .line 1121
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 1122
    .local v0, "displaySize":Landroid/graphics/Point;
    invoke-virtual {p1, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 1123
    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v3, v0, Landroid/graphics/Point;->y:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1125
    .end local v0    # "displaySize":Landroid/graphics/Point;
    :cond_0
    invoke-virtual {p1}, Landroid/view/Display;->getDisplayId()I

    move-result v2

    if-nez v2, :cond_2

    .line 1127
    iget v2, p0, Lcom/google/android/videos/GservicesConfig;->defaultDisplayVideoHeightCap:I

    if-ne v2, v4, :cond_1

    .line 1128
    invoke-direct {p0}, Lcom/google/android/videos/GservicesConfig;->defaultDisplayVideoHeightCap()I

    move-result v2

    iput v2, p0, Lcom/google/android/videos/GservicesConfig;->defaultDisplayVideoHeightCap:I

    .line 1130
    :cond_1
    iget v2, p0, Lcom/google/android/videos/GservicesConfig;->defaultDisplayVideoHeightCap:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1132
    :cond_2
    return v1
.end method

.method public wvCencDrmServerUri()Ljava/lang/String;
    .locals 3

    .prologue
    .line 442
    iget-object v0, p0, Lcom/google/android/videos/GservicesConfig;->uriRewriter:Lcom/google/android/videos/utils/UriRewriter;

    const-string v1, "wv_cenc_drm_server_uri"

    const-string v2, "https://play.google.com/video/license/GetCencLicense"

    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/UriRewriter;->rewrite(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public wvClassicDrmServerUri()Ljava/lang/String;
    .locals 3

    .prologue
    .line 437
    iget-object v0, p0, Lcom/google/android/videos/GservicesConfig;->uriRewriter:Lcom/google/android/videos/utils/UriRewriter;

    const-string v1, "wv_drm_server_uri"

    const-string v2, "https://play.google.com/video/license/GetEMMs.cgi"

    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/UriRewriter;->rewrite(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public wvPortalName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 427
    const-string v0, "wv_portal_name"

    const-string v1, "YouTube"

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public wvProvisioningServerUri(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "defaultUri"    # Ljava/lang/String;

    .prologue
    .line 432
    iget-object v0, p0, Lcom/google/android/videos/GservicesConfig;->uriRewriter:Lcom/google/android/videos/utils/UriRewriter;

    const-string v1, "wv_provisioning_server_uri"

    invoke-direct {p0, v1, p1}, Lcom/google/android/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/UriRewriter;->rewrite(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public youtubeStatsUri()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 714
    iget-object v0, p0, Lcom/google/android/videos/GservicesConfig;->uriRewriter:Lcom/google/android/videos/utils/UriRewriter;

    sget-object v1, Lcom/google/android/videos/GservicesConfig;->YOUTUBE_STATS_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/UriRewriter;->rewrite(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
