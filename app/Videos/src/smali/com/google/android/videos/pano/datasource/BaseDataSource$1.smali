.class Lcom/google/android/videos/pano/datasource/BaseDataSource$1;
.super Ljava/lang/Object;
.source "BaseDataSource.java"

# interfaces
.implements Lcom/google/android/repolib/observers/UpdatablesChanged;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/pano/datasource/BaseDataSource;-><init>(Landroid/content/Context;Lcom/google/android/videos/accounts/SignInManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pano/datasource/BaseDataSource;

.field final synthetic val$signInManager:Lcom/google/android/videos/accounts/SignInManager;


# direct methods
.method constructor <init>(Lcom/google/android/videos/pano/datasource/BaseDataSource;Lcom/google/android/videos/accounts/SignInManager;)V
    .locals 0

    .prologue
    .line 46
    .local p0, "this":Lcom/google/android/videos/pano/datasource/BaseDataSource$1;, "Lcom/google/android/videos/pano/datasource/BaseDataSource.1;"
    iput-object p1, p0, Lcom/google/android/videos/pano/datasource/BaseDataSource$1;->this$0:Lcom/google/android/videos/pano/datasource/BaseDataSource;

    iput-object p2, p0, Lcom/google/android/videos/pano/datasource/BaseDataSource$1;->val$signInManager:Lcom/google/android/videos/accounts/SignInManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public firstUpdatableAdded()V
    .locals 2

    .prologue
    .line 49
    .local p0, "this":Lcom/google/android/videos/pano/datasource/BaseDataSource$1;, "Lcom/google/android/videos/pano/datasource/BaseDataSource.1;"
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/BaseDataSource$1;->this$0:Lcom/google/android/videos/pano/datasource/BaseDataSource;

    invoke-virtual {v0}, Lcom/google/android/videos/pano/datasource/BaseDataSource;->firstUpdatableAdded()V

    .line 50
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/BaseDataSource$1;->val$signInManager:Lcom/google/android/videos/accounts/SignInManager;

    iget-object v1, p0, Lcom/google/android/videos/pano/datasource/BaseDataSource$1;->this$0:Lcom/google/android/videos/pano/datasource/BaseDataSource;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/accounts/SignInManager;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 51
    return-void
.end method

.method public lastUpdatableRemoved()V
    .locals 2

    .prologue
    .line 55
    .local p0, "this":Lcom/google/android/videos/pano/datasource/BaseDataSource$1;, "Lcom/google/android/videos/pano/datasource/BaseDataSource.1;"
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/BaseDataSource$1;->val$signInManager:Lcom/google/android/videos/accounts/SignInManager;

    iget-object v1, p0, Lcom/google/android/videos/pano/datasource/BaseDataSource$1;->this$0:Lcom/google/android/videos/pano/datasource/BaseDataSource;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/accounts/SignInManager;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 56
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/BaseDataSource$1;->this$0:Lcom/google/android/videos/pano/datasource/BaseDataSource;

    invoke-virtual {v0}, Lcom/google/android/videos/pano/datasource/BaseDataSource;->lastUpdatableRemoved()V

    .line 57
    return-void
.end method
