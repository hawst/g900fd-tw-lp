.class public final Lcom/google/android/videos/tagging/KnowledgeEntity$Song;
.super Lcom/google/android/videos/tagging/KnowledgeEntity;
.source "KnowledgeEntity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/KnowledgeEntity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Song"
.end annotation


# instance fields
.field public final googlePlayUrl:Ljava/lang/String;

.field public final performer:Ljava/lang/String;


# direct methods
.method constructor <init>(ILjava/lang/String;Lcom/google/android/videos/tagging/KnowledgeEntity$Image;[ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "localId"    # I
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "albumArt"    # Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
    .param p4, "appearances"    # [I
    .param p5, "performer"    # Ljava/lang/String;
    .param p6, "googlePlayUrl"    # Ljava/lang/String;

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/videos/tagging/KnowledgeEntity;-><init>(ILjava/lang/String;Lcom/google/android/videos/tagging/KnowledgeEntity$Image;[I)V

    .line 60
    iput-object p5, p0, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;->performer:Ljava/lang/String;

    .line 61
    iput-object p6, p0, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;->googlePlayUrl:Ljava/lang/String;

    .line 62
    return-void
.end method
