.class public final Lcom/google/android/videos/utils/ByteArray;
.super Ljava/lang/Object;
.source "ByteArray.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/utils/ByteArray$Serializer;
    }
.end annotation


# instance fields
.field public final capacity:I

.field public final data:[B

.field private limit:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "capacity"    # I

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput p1, p0, Lcom/google/android/videos/utils/ByteArray;->capacity:I

    .line 21
    iput p1, p0, Lcom/google/android/videos/utils/ByteArray;->limit:I

    .line 22
    new-array v0, p1, [B

    iput-object v0, p0, Lcom/google/android/videos/utils/ByteArray;->data:[B

    .line 23
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1
    .param p1, "data"    # [B

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    array-length v0, p1

    iput v0, p0, Lcom/google/android/videos/utils/ByteArray;->capacity:I

    .line 27
    array-length v0, p1

    iput v0, p0, Lcom/google/android/videos/utils/ByteArray;->limit:I

    .line 28
    iput-object p1, p0, Lcom/google/android/videos/utils/ByteArray;->data:[B

    .line 29
    return-void
.end method

.method public static dataEqual(Lcom/google/android/videos/utils/ByteArray;Lcom/google/android/videos/utils/ByteArray;)Z
    .locals 5
    .param p0, "b1"    # Lcom/google/android/videos/utils/ByteArray;
    .param p1, "b2"    # Lcom/google/android/videos/utils/ByteArray;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 44
    if-ne p0, p1, :cond_1

    .line 55
    :cond_0
    :goto_0
    return v1

    .line 47
    :cond_1
    iget v3, p0, Lcom/google/android/videos/utils/ByteArray;->limit:I

    iget v4, p1, Lcom/google/android/videos/utils/ByteArray;->limit:I

    if-eq v3, v4, :cond_2

    move v1, v2

    .line 48
    goto :goto_0

    .line 50
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v3, p0, Lcom/google/android/videos/utils/ByteArray;->limit:I

    if-ge v0, v3, :cond_0

    .line 51
    iget-object v3, p0, Lcom/google/android/videos/utils/ByteArray;->data:[B

    aget-byte v3, v3, v0

    iget-object v4, p0, Lcom/google/android/videos/utils/ByteArray;->data:[B

    aget-byte v4, v4, v0

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 52
    goto :goto_0

    .line 50
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 60
    new-instance v0, Lcom/google/android/videos/utils/ByteArray$Serializer;

    invoke-direct {v0}, Lcom/google/android/videos/utils/ByteArray$Serializer;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/android/videos/utils/ByteArray$Serializer;->setData(Lcom/google/android/videos/utils/ByteArray;)Lcom/google/android/videos/utils/ByteArray$Serializer;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public limit()I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/google/android/videos/utils/ByteArray;->limit:I

    return v0
.end method

.method public setLimit(I)V
    .locals 1
    .param p1, "limit"    # I

    .prologue
    .line 36
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/utils/ByteArray;->data:[B

    array-length v0, v0

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 37
    iput p1, p0, Lcom/google/android/videos/utils/ByteArray;->limit:I

    .line 38
    return-void

    .line 36
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
