.class public abstract Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;
.super Ljava/lang/Object;
.source "Cards.java"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/videos/store/StoreStatusMonitor$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/Cards;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40c
    name = "SecondaryActionButtonHelper"
.end annotation


# instance fields
.field protected final account:Ljava/lang/String;

.field private action:I

.field protected final activity:Landroid/app/Activity;

.field protected final button:Landroid/widget/Button;

.field protected final eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field protected final itemId:Ljava/lang/String;

.field protected final itemName:Ljava/lang/String;

.field protected final storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;


# direct methods
.method protected constructor <init>(Landroid/app/Activity;Landroid/widget/Button;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/store/StoreStatusMonitor;Ljava/lang/String;Lcom/google/android/videos/logging/EventLogger;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "button"    # Landroid/widget/Button;
    .param p3, "itemName"    # Ljava/lang/String;
    .param p4, "itemId"    # Ljava/lang/String;
    .param p5, "storeStatusMonitor"    # Lcom/google/android/videos/store/StoreStatusMonitor;
    .param p6, "account"    # Ljava/lang/String;
    .param p7, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;

    .prologue
    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160
    iput-object p1, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->activity:Landroid/app/Activity;

    .line 161
    iput-object p2, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->button:Landroid/widget/Button;

    .line 162
    iput-object p3, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->itemName:Ljava/lang/String;

    .line 163
    iput-object p4, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->itemId:Ljava/lang/String;

    .line 164
    iput-object p5, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    .line 165
    iput-object p6, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->account:Ljava/lang/String;

    .line 166
    iput-object p7, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 167
    return-void
.end method

.method private setButtonPaddingLeft(I)V
    .locals 5
    .param p1, "dimenResId"    # I

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->button:Landroid/widget/Button;

    iget-object v1, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->button:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->button:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/widget/Button;->getPaddingRight()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->button:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/Button;->setPadding(IIII)V

    .line 256
    return-void
.end method

.method private update()V
    .locals 8

    .prologue
    const v5, 0x7f0b0116

    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 225
    iget-object v1, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    iget-object v2, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->itemId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->itemType()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/store/StoreStatusMonitor;->getStatus(Ljava/lang/String;I)I

    move-result v0

    .line 226
    .local v0, "status":I
    invoke-static {v0}, Lcom/google/android/videos/store/StoreStatusMonitor;->isPurchased(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 227
    iget-object v1, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->button:Landroid/widget/Button;

    invoke-virtual {v1, v4, v4, v4, v4}, Landroid/widget/Button;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 228
    const v1, 0x7f0e01a2

    invoke-direct {p0, v1}, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->setButtonPaddingLeft(I)V

    .line 229
    iget-object v1, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->button:Landroid/widget/Button;

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setText(I)V

    .line 230
    iget-object v1, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->button:Landroid/widget/Button;

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setSelected(Z)V

    .line 231
    iget-object v1, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->button:Landroid/widget/Button;

    iget-object v2, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->button:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 232
    const/4 v1, 0x3

    iput v1, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->action:I

    .line 251
    :goto_0
    return-void

    .line 235
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->button:Landroid/widget/Button;

    const v2, 0x7f0201ef

    invoke-virtual {v1, v2, v6, v6, v6}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 237
    const v1, 0x7f0e0192

    invoke-direct {p0, v1}, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->setButtonPaddingLeft(I)V

    .line 238
    invoke-static {v0}, Lcom/google/android/videos/store/StoreStatusMonitor;->isWishlisted(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 239
    iget-object v1, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->button:Landroid/widget/Button;

    const v2, 0x7f0b011b

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    .line 240
    iget-object v1, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->button:Landroid/widget/Button;

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setSelected(Z)V

    .line 241
    iget-object v1, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->button:Landroid/widget/Button;

    iget-object v2, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->button:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01b3

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->itemName:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 243
    const/4 v1, 0x2

    iput v1, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->action:I

    goto :goto_0

    .line 245
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->button:Landroid/widget/Button;

    const v2, 0x7f0b011a

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    .line 246
    iget-object v1, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->button:Landroid/widget/Button;

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setSelected(Z)V

    .line 247
    iget-object v1, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->button:Landroid/widget/Button;

    iget-object v2, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->button:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01b2

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->itemName:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 249
    iput v7, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->action:I

    goto :goto_0
.end method


# virtual methods
.method protected abstract itemType()I
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->button:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 185
    iget v0, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->action:I

    packed-switch v0, :pswitch_data_0

    .line 197
    :cond_0
    :goto_0
    return-void

    .line 187
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->onClickToPlay()V

    goto :goto_0

    .line 190
    :pswitch_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->onClickToWishlist(Z)V

    goto :goto_0

    .line 193
    :pswitch_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->onClickToWishlist(Z)V

    goto :goto_0

    .line 185
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method protected onClickToPlay()V
    .locals 0

    .prologue
    .line 201
    return-void
.end method

.method protected onClickToWishlist(Z)V
    .locals 7
    .param p1, "isAdd"    # Z

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->itemId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->itemType()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->playStoreEventSource()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->button:Landroid/widget/Button;

    move v4, p1

    invoke-static/range {v0 .. v6}, Lcom/google/android/videos/store/WishlistService;->requestSetWishlisted(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZILandroid/view/View;)V

    .line 206
    return-void
.end method

.method public onStoreStatusChanged(Lcom/google/android/videos/store/StoreStatusMonitor;)V
    .locals 0
    .param p1, "sender"    # Lcom/google/android/videos/store/StoreStatusMonitor;

    .prologue
    .line 221
    invoke-direct {p0}, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->update()V

    .line 222
    return-void
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/store/StoreStatusMonitor;->addListener(Lcom/google/android/videos/store/StoreStatusMonitor$Listener;)V

    .line 211
    invoke-direct {p0}, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->update()V

    .line 212
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/store/StoreStatusMonitor;->removeListener(Lcom/google/android/videos/store/StoreStatusMonitor$Listener;)V

    .line 217
    return-void
.end method

.method protected abstract playStoreEventSource()I
.end method

.method public final setup()V
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->button:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 174
    iget-object v0, p0, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->button:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    invoke-direct {p0}, Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;->update()V

    .line 176
    return-void
.end method
