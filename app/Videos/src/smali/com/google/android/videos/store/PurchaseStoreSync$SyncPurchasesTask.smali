.class Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;
.super Lcom/google/android/videos/store/SyncTaskManager$SyncTask;
.source "PurchaseStoreSync.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/PurchaseStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncPurchasesTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/store/SyncTaskManager$SyncTask;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/api/UserLibraryRequest;",
        "Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private exception:Ljava/lang/Exception;

.field private final firstRequest:Lcom/google/android/videos/api/UserLibraryRequest;

.field private final libraryMovies:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final libraryShows:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;",
            ">;"
        }
    .end annotation
.end field

.field private nextRequest:Lcom/google/android/videos/api/UserLibraryRequest;

.field private notModified:Z

.field private final purchasedAssetEidrIds:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final purchasedAssets:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;"
        }
    .end annotation
.end field

.field private responseSnapshotToken:Ljava/lang/String;

.field private final seasonToShowIdMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private shouldTerminate:Z

.field private final syncRequest:Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

.field final synthetic this$0:Lcom/google/android/videos/store/PurchaseStoreSync;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/store/PurchaseStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Lcom/google/android/videos/api/UserLibraryRequest;)V
    .locals 1
    .param p2, "priority"    # I
    .param p4, "syncRequest"    # Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;
    .param p5, "firstRequest"    # Lcom/google/android/videos/api/UserLibraryRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/store/SyncTaskManager$SharedStates",
            "<*>;",
            "Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;",
            "Lcom/google/android/videos/api/UserLibraryRequest;",
            ")V"
        }
    .end annotation

    .prologue
    .line 402
    .local p3, "states":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<*>;"
    iput-object p1, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    .line 403
    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->syncTaskManager:Lcom/google/android/videos/store/SyncTaskManager;
    invoke-static {p1}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$300(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/SyncTaskManager;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;-><init>(Lcom/google/android/videos/store/SyncTaskManager;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V

    .line 404
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    iput-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->syncRequest:Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    .line 405
    iput-object p5, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->firstRequest:Lcom/google/android/videos/api/UserLibraryRequest;

    .line 407
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->purchasedAssets:Ljava/util/Map;

    .line 408
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->purchasedAssetEidrIds:Ljava/util/HashSet;

    .line 409
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->seasonToShowIdMap:Ljava/util/Map;

    .line 410
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->libraryMovies:Ljava/util/List;

    .line 411
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->libraryShows:Ljava/util/Map;

    .line 412
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/store/PurchaseStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Lcom/google/android/videos/api/UserLibraryRequest;Lcom/google/android/videos/store/PurchaseStoreSync$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/store/PurchaseStoreSync;
    .param p2, "x1"    # I
    .param p3, "x2"    # Lcom/google/android/videos/store/SyncTaskManager$SharedStates;
    .param p4, "x3"    # Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;
    .param p5, "x4"    # Lcom/google/android/videos/api/UserLibraryRequest;
    .param p6, "x5"    # Lcom/google/android/videos/store/PurchaseStoreSync$1;

    .prologue
    .line 361
    invoke-direct/range {p0 .. p5}, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;-><init>(Lcom/google/android/videos/store/PurchaseStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Lcom/google/android/videos/api/UserLibraryRequest;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/videos/store/PurchaseStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V
    .locals 6
    .param p2, "priority"    # I
    .param p4, "account"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/store/SyncTaskManager$SharedStates",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 398
    .local p3, "states":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<*>;"
    invoke-static {p4}, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->createForFullSync(Ljava/lang/String;)Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;-><init>(Lcom/google/android/videos/store/PurchaseStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Lcom/google/android/videos/api/UserLibraryRequest;)V

    .line 399
    return-void
.end method

.method private addShowDescendant(ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "id"    # Ljava/lang/String;
    .param p3, "showId"    # Ljava/lang/String;

    .prologue
    .line 665
    iget-object v1, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->libraryShows:Ljava/util/Map;

    invoke-interface {v1, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;

    .line 666
    .local v0, "params":Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;
    if-nez v0, :cond_0

    .line 667
    new-instance v0, Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;

    .end local v0    # "params":Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;
    invoke-direct {v0, p3}, Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;-><init>(Ljava/lang/String;)V

    .line 668
    .restart local v0    # "params":Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;
    iget-object v1, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->libraryShows:Ljava/util/Map;

    invoke-interface {v1, p3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 670
    :cond_0
    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;->addDescendant(ILjava/lang/String;)V

    .line 671
    return-void
.end method

.method private buildFirstRequest()Lcom/google/android/videos/api/UserLibraryRequest;
    .locals 15

    .prologue
    .line 468
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->syncRequest:Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    iget-object v11, v0, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->account:Ljava/lang/String;

    .line 469
    .local v11, "account":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->syncRequest:Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    iget-object v14, v0, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->videoId:Ljava/lang/String;

    .line 470
    .local v14, "videoId":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->syncRequest:Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    iget-object v13, v0, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->eidrId:Ljava/lang/String;

    .line 471
    .local v13, "eidrId":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->syncRequest:Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    iget-boolean v0, v0, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->isFullSync:Z

    if-eqz v0, :cond_1

    .line 474
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v0}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$100(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "user_data"

    sget-object v2, Lcom/google/android/videos/store/PurchaseStoreSync$SnapshotTokenQuery;->PROJECTION:[Ljava/lang/String;

    const-string v3, "user_account = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v11, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 478
    .local v12, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 481
    .local v3, "snapshotToken":Ljava/lang/String;
    :goto_0
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 483
    new-instance v0, Lcom/google/android/videos/api/UserLibraryRequest;

    const/4 v2, 0x1

    const/16 v4, 0x32

    const/4 v5, 0x0

    const/4 v1, 0x0

    new-array v6, v1, [Ljava/lang/String;

    move-object v1, v11

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/api/UserLibraryRequest;-><init>(Ljava/lang/String;ZLjava/lang/String;ILjava/lang/String;[Ljava/lang/String;)V

    move-object v4, v0

    .line 491
    .end local v3    # "snapshotToken":Ljava/lang/String;
    .end local v12    # "cursor":Landroid/database/Cursor;
    :goto_1
    return-object v4

    .line 478
    .restart local v12    # "cursor":Landroid/database/Cursor;
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 481
    :catchall_0
    move-exception v0

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    throw v0

    .line 485
    .end local v12    # "cursor":Landroid/database/Cursor;
    :cond_1
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 487
    new-instance v4, Lcom/google/android/videos/api/UserLibraryRequest;

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/16 v8, 0x32

    const/4 v9, 0x0

    const/4 v0, 0x2

    new-array v10, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {v14}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromMovieId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v0

    const/4 v0, 0x1

    invoke-static {v14}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromEpisodeId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v0

    move-object v5, v11

    invoke-direct/range {v4 .. v10}, Lcom/google/android/videos/api/UserLibraryRequest;-><init>(Ljava/lang/String;ZLjava/lang/String;ILjava/lang/String;[Ljava/lang/String;)V

    goto :goto_1

    .line 491
    :cond_2
    new-instance v4, Lcom/google/android/videos/api/UserLibraryRequest;

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/16 v8, 0x32

    const/4 v9, 0x0

    const/4 v0, 0x1

    new-array v10, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {v13}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromEidrId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v0

    move-object v5, v11

    invoke-direct/range {v4 .. v10}, Lcom/google/android/videos/api/UserLibraryRequest;-><init>(Ljava/lang/String;ZLjava/lang/String;ILjava/lang/String;[Ljava/lang/String;)V

    goto :goto_1
.end method

.method private doPostSyncCleanup()V
    .locals 5

    .prologue
    .line 782
    new-instance v0, Lcom/google/android/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;

    iget-object v1, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    iget v2, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->priority:I

    iget-object v3, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    iget-object v4, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->syncRequest:Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    iget-object v4, v4, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->account:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;-><init>(Lcom/google/android/videos/store/PurchaseStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->schedule()V

    .line 783
    new-instance v0, Lcom/google/android/videos/store/PurchaseStoreSync$PurgePurchasesTask;

    iget-object v1, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    iget v2, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->priority:I

    iget-object v3, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/videos/store/PurchaseStoreSync$PurgePurchasesTask;-><init>(Lcom/google/android/videos/store/PurchaseStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V

    invoke-virtual {v0}, Lcom/google/android/videos/store/PurchaseStoreSync$PurgePurchasesTask;->schedule()V

    .line 784
    return-void
.end method

.method private finalizeFullSync(Ljava/lang/String;)V
    .locals 18
    .param p1, "newSnapshotToken"    # Ljava/lang/String;

    .prologue
    .line 691
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v3}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$100(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 692
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/16 v16, 0x0

    .line 693
    .local v16, "success":Z
    const/4 v15, 0x0

    .line 695
    .local v15, "rowsAffected":I
    :try_start_0
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->storeSnapshotToken(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 696
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 697
    .local v14, "rowIdsToRemove":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v3, "purchased_assets"

    sget-object v4, Lcom/google/android/videos/store/PurchaseStoreSync$MyLibraryQuery;->PROJECTION:[Ljava/lang/String;

    const-string v5, "account = ?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->syncRequest:Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    iget-object v8, v8, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->account:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v13

    .line 701
    .local v13, "cursor":Landroid/database/Cursor;
    :cond_0
    :goto_0
    :try_start_1
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 702
    const/4 v3, 0x1

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 703
    .local v12, "assetType":I
    const/4 v3, 0x2

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 704
    .local v10, "assetId":Ljava/lang/String;
    invoke-static {v12, v10}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromAssetTypeAndId(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 705
    .local v11, "assetResourceId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->purchasedAssets:Ljava/util/Map;

    invoke-interface {v3, v11}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 706
    const/4 v3, 0x0

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v14, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 710
    .end local v10    # "assetId":Ljava/lang/String;
    .end local v11    # "assetResourceId":Ljava/lang/String;
    .end local v12    # "assetType":I
    :catchall_0
    move-exception v3

    :try_start_2
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 724
    .end local v13    # "cursor":Landroid/database/Cursor;
    .end local v14    # "rowIdsToRemove":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catchall_1
    move-exception v3

    move-object v4, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v3}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$100(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v5

    if-nez v15, :cond_4

    const/4 v3, 0x0

    :goto_1
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->syncRequest:Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    iget-object v8, v8, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->account:Ljava/lang/String;

    aput-object v8, v6, v7

    move/from16 v0, v16

    invoke-virtual {v5, v2, v0, v3, v6}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v4

    .line 710
    .restart local v13    # "cursor":Landroid/database/Cursor;
    .restart local v14    # "rowIdsToRemove":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    :try_start_3
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 712
    invoke-interface {v14}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 713
    new-instance v17, Landroid/content/ContentValues;

    invoke-direct/range {v17 .. v17}, Landroid/content/ContentValues;-><init>()V

    .line 714
    .local v17, "values":Landroid/content/ContentValues;
    const-string v3, "purchase_status"

    const/4 v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 715
    const-string v4, "purchased_assets"

    const-string v3, "ROWID"

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v3, v5}, Lcom/google/android/videos/utils/DbUtils;->buildInMultipleParamsClause(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v14, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v2, v4, v0, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v15

    .line 718
    if-eqz v15, :cond_2

    .line 719
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->syncRequest:Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    iget-object v3, v3, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->account:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/videos/store/UserAssetsUtil;->purgeForAccount(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 722
    .end local v17    # "values":Landroid/content/ContentValues;
    :cond_2
    const/16 v16, 0x1

    .line 724
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v3}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$100(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v4

    if-nez v15, :cond_3

    const/4 v3, 0x0

    :goto_2
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->syncRequest:Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    iget-object v7, v7, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->account:Ljava/lang/String;

    aput-object v7, v5, v6

    move/from16 v0, v16

    invoke-virtual {v4, v2, v0, v3, v5}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 728
    return-void

    .line 724
    :cond_3
    const/4 v3, 0x1

    goto :goto_2

    .end local v13    # "cursor":Landroid/database/Cursor;
    .end local v14    # "rowIdsToRemove":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_4
    const/4 v3, 0x1

    goto :goto_1
.end method

.method private finalizeVideoSync()V
    .locals 13

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 736
    iget-object v7, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->syncRequest:Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    iget-object v6, v7, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->videoId:Ljava/lang/String;

    .line 737
    .local v6, "videoId":Ljava/lang/String;
    iget-object v7, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->syncRequest:Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    iget-object v2, v7, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->eidrId:Ljava/lang/String;

    .line 738
    .local v2, "eidrId":Ljava/lang/String;
    iget-object v7, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->syncRequest:Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    iget-object v0, v7, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->account:Ljava/lang/String;

    .line 740
    .local v0, "account":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 741
    iget-object v7, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->purchasedAssetEidrIds:Ljava/util/HashSet;

    invoke-virtual {v7, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 771
    :cond_0
    :goto_0
    return-void

    .line 744
    :cond_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Did not see video "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, " during a single-video sync"

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 745
    iget-object v7, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v7}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$100(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v7

    invoke-static {v7, v0, v2}, Lcom/google/android/videos/utils/EidrId;->convertEidrIdToVideoId(Lcom/google/android/videos/store/Database;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 746
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 750
    :cond_2
    iget-object v7, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->purchasedAssets:Ljava/util/Map;

    invoke-static {v6}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromMovieId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v7, v10}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->purchasedAssets:Ljava/util/Map;

    invoke-static {v6}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromEpisodeId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v7, v10}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 752
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Did not see video "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, " during a single-video sync"

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 753
    iget-object v7, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v7}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$100(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 754
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v4, 0x0

    .line 755
    .local v4, "success":Z
    const/4 v3, 0x0

    .line 757
    .local v3, "rowsAffected":I
    :try_start_0
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 758
    .local v5, "values":Landroid/content/ContentValues;
    const-string v7, "purchase_status"

    const/4 v10, -0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v5, v7, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 759
    const-string v7, "purchased_assets"

    const-string v10, "account = ? AND asset_type IN (6,20) AND asset_id = ?"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/String;

    const/4 v12, 0x0

    aput-object v0, v11, v12

    const/4 v12, 0x1

    aput-object v6, v11, v12

    invoke-virtual {v1, v7, v5, v10, v11}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 761
    if-eqz v3, :cond_3

    .line 762
    invoke-static {v1, v0, v6}, Lcom/google/android/videos/store/UserAssetsUtil;->purgeForAccountAndVideo(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 764
    :cond_3
    const/4 v4, 0x1

    .line 766
    iget-object v7, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v7}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$100(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v10

    if-nez v3, :cond_4

    move v7, v8

    :goto_1
    new-array v9, v9, [Ljava/lang/Object;

    aput-object v0, v9, v8

    invoke-virtual {v10, v1, v4, v7, v9}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_4
    move v7, v9

    goto :goto_1

    .end local v5    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v7

    move-object v10, v7

    iget-object v7, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v7}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$100(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v11

    if-nez v3, :cond_5

    move v7, v8

    :goto_2
    new-array v9, v9, [Ljava/lang/Object;

    aput-object v0, v9, v8

    invoke-virtual {v11, v1, v4, v7, v9}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v10

    :cond_5
    move v7, v9

    goto :goto_2
.end method

.method private loadExistingPurchases()V
    .locals 12

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 633
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v0}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$100(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "purchased_assets"

    sget-object v2, Lcom/google/android/videos/store/PurchaseStoreSync$MyLibraryQuery;->PROJECTION:[Ljava/lang/String;

    const-string v3, "account = ?"

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->syncRequest:Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    iget-object v7, v7, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->account:Ljava/lang/String;

    aput-object v7, v4, v6

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 637
    .local v10, "cursor":Landroid/database/Cursor;
    :goto_0
    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 638
    const/4 v0, 0x1

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 639
    .local v9, "assetType":I
    const/4 v0, 0x2

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 640
    .local v8, "assetId":Ljava/lang/String;
    const/4 v0, 0x3

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 641
    .local v11, "rootId":Ljava/lang/String;
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 642
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Existing asset "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v9, v8}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromAssetTypeAndId(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " does not have a root!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 660
    .end local v8    # "assetId":Ljava/lang/String;
    .end local v9    # "assetType":I
    .end local v11    # "rootId":Ljava/lang/String;
    :catchall_0
    move-exception v0

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v0

    .line 647
    .restart local v8    # "assetId":Ljava/lang/String;
    .restart local v9    # "assetType":I
    .restart local v11    # "rootId":Ljava/lang/String;
    :cond_0
    sparse-switch v9, :sswitch_data_0

    goto :goto_0

    .line 649
    :sswitch_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->libraryMovies:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 653
    :sswitch_1
    invoke-direct {p0, v9, v8, v11}, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->addShowDescendant(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 660
    .end local v8    # "assetId":Ljava/lang/String;
    .end local v9    # "assetType":I
    .end local v11    # "rootId":Ljava/lang/String;
    :cond_1
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 662
    return-void

    .line 647
    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0x13 -> :sswitch_1
        0x14 -> :sswitch_1
    .end sparse-switch
.end method

.method private mergePurchases([Lcom/google/wireless/android/video/magma/proto/AssetResource;)V
    .locals 9
    .param p1, "resources"    # [Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    .line 545
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v6, p1

    if-ge v4, v6, :cond_3

    .line 546
    aget-object v0, p1, v4

    .line 547
    .local v0, "asset":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    invoke-static {v0}, Lcom/google/android/videos/store/PurchaseStoreUtil;->isValidUserLibraryAsset(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 545
    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 550
    :cond_1
    iget-object v1, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 551
    .local v1, "assetId":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    invoke-static {v1}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromAssetResourceId(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Ljava/lang/String;

    move-result-object v2

    .line 552
    .local v2, "assetIdString":Ljava/lang/String;
    iget-object v3, v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->titleEidrId:Ljava/lang/String;

    .line 553
    .local v3, "eidrId":Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->purchasedAssets:Ljava/util/Map;

    invoke-interface {v6, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-static {v0, v6}, Lcom/google/android/videos/store/PurchaseStoreUtil;->mergePurchasedAsset(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/AssetResource;)Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v5

    .line 555
    .local v5, "merged":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    if-eqz v5, :cond_2

    .line 556
    iget-object v6, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->purchasedAssets:Ljava/util/Map;

    invoke-interface {v6, v2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 557
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 558
    iget-object v6, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->purchasedAssetEidrIds:Ljava/util/HashSet;

    invoke-virtual {v6, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 561
    :cond_2
    iget v6, v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    const/16 v7, 0x13

    if-ne v6, v7, :cond_0

    .line 562
    iget-object v6, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->seasonToShowIdMap:Ljava/util/Map;

    iget-object v7, v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    iget-object v8, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v8, v8, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 565
    .end local v0    # "asset":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .end local v1    # "assetId":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .end local v2    # "assetIdString":Ljava/lang/String;
    .end local v3    # "eidrId":Ljava/lang/String;
    .end local v5    # "merged":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :cond_3
    return-void
.end method

.method private storePurchases()V
    .locals 14

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 572
    iget-object v11, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->exception:Ljava/lang/Exception;

    if-nez v11, :cond_1

    move v3, v9

    .line 573
    .local v3, "purchaseFeedComplete":Z
    :goto_0
    iget-object v11, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v11}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$100(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    .line 574
    .local v8, "transaction":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v11, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->syncRequest:Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    iget-object v0, v11, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->account:Ljava/lang/String;

    .line 575
    .local v0, "account":Ljava/lang/String;
    const/4 v7, 0x0

    .line 577
    .local v7, "success":Z
    :try_start_0
    iget-object v11, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->purchasedAssets:Ljava/util/Map;

    invoke-interface {v11}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 578
    .local v4, "purchasedAsset":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    iget-object v1, v4, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 579
    .local v1, "assetId":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    iget v11, v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    sparse-switch v11, :sswitch_data_0

    goto :goto_1

    .line 581
    :sswitch_0
    iget-object v11, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->libraryMovies:Ljava/util/List;

    iget-object v12, v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 582
    iget-object v11, v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-static {v8, v0, v4, v11}, Lcom/google/android/videos/store/PurchaseStoreUtil;->storePurchase(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/lang/String;)V

    .line 583
    iget-object v11, v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-static {v8, v0, v11}, Lcom/google/android/videos/store/UserAssetsUtil;->refreshVideoRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 624
    .end local v1    # "assetId":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "purchasedAsset":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :catchall_0
    move-exception v11

    iget-object v12, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v12}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$100(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v12

    new-array v13, v9, [Ljava/lang/Object;

    aput-object v0, v13, v10

    invoke-virtual {v12, v8, v7, v9, v13}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v11

    .end local v0    # "account":Ljava/lang/String;
    .end local v3    # "purchaseFeedComplete":Z
    .end local v7    # "success":Z
    .end local v8    # "transaction":Landroid/database/sqlite/SQLiteDatabase;
    :cond_1
    move v3, v10

    .line 572
    goto :goto_0

    .line 587
    .restart local v0    # "account":Ljava/lang/String;
    .restart local v1    # "assetId":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v3    # "purchaseFeedComplete":Z
    .restart local v4    # "purchasedAsset":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .restart local v7    # "success":Z
    .restart local v8    # "transaction":Landroid/database/sqlite/SQLiteDatabase;
    :sswitch_1
    :try_start_1
    iget-object v11, v4, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v5, v11, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    .line 588
    .local v5, "seasonId":Ljava/lang/String;
    iget-object v11, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->seasonToShowIdMap:Ljava/util/Map;

    invoke-interface {v11, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 589
    .local v6, "showId":Ljava/lang/String;
    if-eqz v6, :cond_2

    .line 590
    const/16 v11, 0x14

    iget-object v12, v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-direct {p0, v11, v12, v6}, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->addShowDescendant(ILjava/lang/String;Ljava/lang/String;)V

    .line 591
    const/16 v11, 0x13

    invoke-direct {p0, v11, v5, v6}, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->addShowDescendant(ILjava/lang/String;Ljava/lang/String;)V

    .line 592
    invoke-static {v8, v0, v4, v6}, Lcom/google/android/videos/store/PurchaseStoreUtil;->storePurchase(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/lang/String;)V

    .line 593
    iget-object v11, v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-static {v8, v0, v11}, Lcom/google/android/videos/store/UserAssetsUtil;->refreshVideoRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 594
    :cond_2
    if-eqz v3, :cond_0

    .line 597
    new-instance v11, Lcom/google/android/videos/store/SyncTaskManager$DataException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Episode "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " in user library missing show"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Lcom/google/android/videos/store/SyncTaskManager$DataException;-><init>(Ljava/lang/String;)V

    iput-object v11, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->exception:Ljava/lang/Exception;

    .line 599
    const/16 v11, 0x14

    iget-object v12, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->exception:Ljava/lang/Exception;

    invoke-virtual {p0, v11, v12}, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->onSyncError(ILjava/lang/Exception;)V

    goto/16 :goto_1

    .line 607
    .end local v5    # "seasonId":Ljava/lang/String;
    .end local v6    # "showId":Ljava/lang/String;
    :sswitch_2
    iget-object v11, v4, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v6, v11, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    .line 608
    .restart local v6    # "showId":Ljava/lang/String;
    const/16 v11, 0x13

    iget-object v12, v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-direct {p0, v11, v12, v6}, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->addShowDescendant(ILjava/lang/String;Ljava/lang/String;)V

    .line 609
    invoke-static {v8, v0, v4, v6}, Lcom/google/android/videos/store/PurchaseStoreUtil;->storePurchase(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 616
    .end local v1    # "assetId":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .end local v4    # "purchasedAsset":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .end local v6    # "showId":Ljava/lang/String;
    :cond_3
    iget-object v11, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->seasonToShowIdMap:Ljava/util/Map;

    invoke-interface {v11}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 617
    .restart local v5    # "seasonId":Ljava/lang/String;
    invoke-static {v8, v0, v5}, Lcom/google/android/videos/store/UserAssetsUtil;->refreshSeasonRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 619
    .end local v5    # "seasonId":Ljava/lang/String;
    :cond_4
    iget-object v11, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->libraryShows:Ljava/util/Map;

    invoke-interface {v11}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 620
    .restart local v6    # "showId":Ljava/lang/String;
    invoke-static {v8, v0, v6}, Lcom/google/android/videos/store/UserAssetsUtil;->refreshShowRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 622
    .end local v6    # "showId":Ljava/lang/String;
    :cond_5
    const/4 v7, 0x1

    .line 624
    iget-object v11, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v11}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$100(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v11

    new-array v12, v9, [Ljava/lang/Object;

    aput-object v0, v12, v10

    invoke-virtual {v11, v8, v7, v9, v12}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 626
    return-void

    .line 579
    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0x13 -> :sswitch_2
        0x14 -> :sswitch_1
    .end sparse-switch
.end method

.method private storeSnapshotToken(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "newSnapshotToken"    # Ljava/lang/String;
    .param p2, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 774
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 775
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "user_account"

    iget-object v2, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->syncRequest:Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    iget-object v2, v2, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->account:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 776
    const-string v1, "sync_snapshot_token"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 777
    const-string v1, "user_data"

    sget-object v2, Lcom/google/android/videos/store/PurchaseStoreSync$SnapshotTokenQuery;->EQUAL_COLUMNS:[Ljava/lang/String;

    invoke-static {p2, v1, v0, v2}, Lcom/google/android/videos/utils/DbUtils;->updateOrReplace(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;[Ljava/lang/String;)J

    .line 779
    return-void
.end method

.method private syncMetadata()V
    .locals 7

    .prologue
    .line 679
    iget-object v2, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->assetStoreSync:Lcom/google/android/videos/store/AssetStoreSync;
    invoke-static {v2}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$800(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/AssetStoreSync;

    move-result-object v2

    iget v3, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->priority:I

    iget-object v4, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    iget-object v5, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->syncRequest:Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    iget-object v5, v5, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->account:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->libraryMovies:Ljava/util/List;

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/videos/store/AssetStoreSync;->syncMovieMetadata(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;Ljava/util/List;)V

    .line 682
    iget-object v2, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->libraryShows:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;

    .line 683
    .local v1, "params":Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;
    iget-object v2, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->assetStoreSync:Lcom/google/android/videos/store/AssetStoreSync;
    invoke-static {v2}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$800(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/AssetStoreSync;

    move-result-object v2

    iget v3, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->priority:I

    iget-object v4, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    iget-object v5, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->syncRequest:Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    iget-object v5, v5, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->account:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5, v1}, Lcom/google/android/videos/store/AssetStoreSync;->syncFullShowMetadata(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;)V

    goto :goto_0

    .line 685
    .end local v1    # "params":Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;
    :cond_0
    return-void
.end method


# virtual methods
.method protected doSync()V
    .locals 10

    .prologue
    .line 417
    iget-object v5, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->userConfigurationSyncTimes:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v5}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/videos/store/PurchaseStoreSync;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->syncRequest:Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    iget-object v6, v6, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->account:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 418
    .local v1, "lastUserConfigurationSyncTime":Ljava/lang/Long;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 419
    .local v2, "now":J
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v5, v6, v2

    if-gtz v5, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/32 v8, 0x5265c00

    add-long/2addr v6, v8

    cmp-long v5, v6, v2

    if-gez v5, :cond_1

    .line 422
    :cond_0
    :try_start_0
    iget-object v5, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;
    invoke-static {v5}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$500(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->syncRequest:Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    iget-object v6, v6, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->account:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/google/android/videos/store/ConfigurationStore;->blockingSyncUserConfiguration(Ljava/lang/String;)V

    .line 423
    iget-object v5, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->userConfigurationSyncTimes:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v5}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/videos/store/PurchaseStoreSync;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->syncRequest:Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    iget-object v6, v6, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->account:Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 430
    :cond_1
    :goto_0
    iget-object v5, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->firstRequest:Lcom/google/android/videos/api/UserLibraryRequest;

    if-eqz v5, :cond_2

    iget-object v4, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->firstRequest:Lcom/google/android/videos/api/UserLibraryRequest;

    .line 431
    .local v4, "request":Lcom/google/android/videos/api/UserLibraryRequest;
    :goto_1
    if-eqz v4, :cond_3

    iget-boolean v5, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->shouldTerminate:Z

    if-nez v5, :cond_3

    .line 432
    iget-object v5, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->syncUserLibraryRequester:Lcom/google/android/videos/async/Requester;
    invoke-static {v5}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$600(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/async/Requester;

    move-result-object v5

    invoke-interface {v5, v4, p0}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 433
    iget-object v4, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->nextRequest:Lcom/google/android/videos/api/UserLibraryRequest;

    .line 434
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->nextRequest:Lcom/google/android/videos/api/UserLibraryRequest;

    goto :goto_1

    .line 424
    .end local v4    # "request":Lcom/google/android/videos/api/UserLibraryRequest;
    :catch_0
    move-exception v0

    .line 425
    .local v0, "e":Ljava/lang/Throwable;
    const-string v5, "Could not sync user config, will use device country for asset requests"

    invoke-static {v5, v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 430
    .end local v0    # "e":Ljava/lang/Throwable;
    :cond_2
    invoke-direct {p0}, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->buildFirstRequest()Lcom/google/android/videos/api/UserLibraryRequest;

    move-result-object v4

    goto :goto_1

    .line 436
    .restart local v4    # "request":Lcom/google/android/videos/api/UserLibraryRequest;
    :cond_3
    iget-boolean v5, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->shouldTerminate:Z

    if-eqz v5, :cond_4

    .line 465
    :goto_2
    return-void

    .line 439
    :cond_4
    iget-object v5, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->exception:Ljava/lang/Exception;

    if-eqz v5, :cond_5

    .line 440
    const/16 v5, 0x1e

    iget-object v6, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->exception:Ljava/lang/Exception;

    invoke-virtual {p0, v5, v6}, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->onSyncError(ILjava/lang/Exception;)V

    .line 445
    :cond_5
    iget-boolean v5, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->notModified:Z

    if-eqz v5, :cond_7

    .line 446
    invoke-direct {p0}, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->loadExistingPurchases()V

    .line 453
    :goto_3
    invoke-direct {p0}, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->syncMetadata()V

    .line 456
    iget-boolean v5, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->notModified:Z

    if-nez v5, :cond_6

    iget-object v5, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->exception:Ljava/lang/Exception;

    if-nez v5, :cond_6

    .line 457
    iget-object v5, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->syncRequest:Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    iget-boolean v5, v5, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->isFullSync:Z

    if-eqz v5, :cond_8

    .line 458
    iget-object v5, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->responseSnapshotToken:Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->finalizeFullSync(Ljava/lang/String;)V

    .line 464
    :cond_6
    :goto_4
    invoke-direct {p0}, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->doPostSyncCleanup()V

    goto :goto_2

    .line 448
    :cond_7
    invoke-direct {p0}, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->storePurchases()V

    .line 450
    iget-object v5, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->context:Landroid/content/Context;
    invoke-static {v5}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$700(Lcom/google/android/videos/store/PurchaseStoreSync;)Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->context:Landroid/content/Context;
    invoke-static {v6}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$700(Lcom/google/android/videos/store/PurchaseStoreSync;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/videos/pinning/TransferService;->createIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_3

    .line 460
    :cond_8
    invoke-direct {p0}, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->finalizeVideoSync()V

    goto :goto_4
.end method

.method public onError(Lcom/google/android/videos/api/UserLibraryRequest;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "apiRequest"    # Lcom/google/android/videos/api/UserLibraryRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 538
    iput-object p2, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->exception:Ljava/lang/Exception;

    .line 539
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 361
    check-cast p1, Lcom/google/android/videos/api/UserLibraryRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->onError(Lcom/google/android/videos/api/UserLibraryRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/api/UserLibraryRequest;Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;)V
    .locals 10
    .param p1, "apiRequest"    # Lcom/google/android/videos/api/UserLibraryRequest;
    .param p2, "response"    # Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;

    .prologue
    const/4 v9, 0x1

    const/4 v3, 0x0

    .line 498
    iget-object v6, p2, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->snapshotToken:Ljava/lang/String;

    .line 499
    .local v6, "responseSnapshotToken":Ljava/lang/String;
    iget-object v0, p1, Lcom/google/android/videos/api/UserLibraryRequest;->snapshotToken:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 500
    iget-object v0, p1, Lcom/google/android/videos/api/UserLibraryRequest;->snapshotToken:Ljava/lang/String;

    invoke-static {v0, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 503
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v0}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$100(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    .line 504
    .local v8, "transaction":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v7, 0x0

    .line 506
    .local v7, "success":Z
    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, v0, v8}, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->storeSnapshotToken(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 507
    const/4 v7, 0x1

    .line 509
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v0}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$100(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v8, v7, v3, v1}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 512
    new-instance v0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;

    iget-object v1, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    iget v2, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->priority:I

    iget-object v3, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    iget-object v4, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->syncRequest:Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    invoke-virtual {p1}, Lcom/google/android/videos/api/UserLibraryRequest;->cloneWithNoTokens()Lcom/google/android/videos/api/UserLibraryRequest;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;-><init>(Lcom/google/android/videos/store/PurchaseStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Lcom/google/android/videos/api/UserLibraryRequest;)V

    invoke-virtual {v0}, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->schedule()V

    .line 514
    iput-boolean v9, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->shouldTerminate:Z

    .line 534
    .end local v7    # "success":Z
    .end local v8    # "transaction":Landroid/database/sqlite/SQLiteDatabase;
    :goto_0
    return-void

    .line 509
    .restart local v7    # "success":Z
    .restart local v8    # "transaction":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v1}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$100(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v8, v7, v3, v2}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v0

    .line 517
    .end local v7    # "success":Z
    .end local v8    # "transaction":Landroid/database/sqlite/SQLiteDatabase;
    :cond_0
    iget-object v0, p1, Lcom/google/android/videos/api/UserLibraryRequest;->pageToken:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 519
    iput-boolean v9, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->notModified:Z

    goto :goto_0

    .line 524
    :cond_1
    iget-object v0, p2, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-direct {p0, v0}, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->mergePurchases([Lcom/google/wireless/android/video/magma/proto/AssetResource;)V

    .line 526
    iget-object v0, p2, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    if-eqz v0, :cond_2

    iget-object v0, p2, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->nextPageToken:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 528
    iget-object v0, p2, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->nextPageToken:Ljava/lang/String;

    invoke-virtual {p1, v6, v0}, Lcom/google/android/videos/api/UserLibraryRequest;->cloneWithNewTokens(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/api/UserLibraryRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->nextRequest:Lcom/google/android/videos/api/UserLibraryRequest;

    goto :goto_0

    .line 532
    :cond_2
    iput-object v6, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->responseSnapshotToken:Ljava/lang/String;

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 361
    check-cast p1, Lcom/google/android/videos/api/UserLibraryRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->onResponse(Lcom/google/android/videos/api/UserLibraryRequest;Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;)V

    return-void
.end method
