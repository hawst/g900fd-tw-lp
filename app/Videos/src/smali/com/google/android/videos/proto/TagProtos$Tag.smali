.class public final Lcom/google/android/videos/proto/TagProtos$Tag;
.super Lcom/google/protobuf/nano/MessageNano;
.source "TagProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/proto/TagProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Tag"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/videos/proto/TagProtos$Tag;


# instance fields
.field public circle:I

.field public faceRectShape:J

.field public offCameraShape:I

.field public splitId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 644
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 645
    invoke-virtual {p0}, Lcom/google/android/videos/proto/TagProtos$Tag;->clear()Lcom/google/android/videos/proto/TagProtos$Tag;

    .line 646
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/videos/proto/TagProtos$Tag;
    .locals 2

    .prologue
    .line 621
    sget-object v0, Lcom/google/android/videos/proto/TagProtos$Tag;->_emptyArray:[Lcom/google/android/videos/proto/TagProtos$Tag;

    if-nez v0, :cond_1

    .line 622
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 624
    :try_start_0
    sget-object v0, Lcom/google/android/videos/proto/TagProtos$Tag;->_emptyArray:[Lcom/google/android/videos/proto/TagProtos$Tag;

    if-nez v0, :cond_0

    .line 625
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/videos/proto/TagProtos$Tag;

    sput-object v0, Lcom/google/android/videos/proto/TagProtos$Tag;->_emptyArray:[Lcom/google/android/videos/proto/TagProtos$Tag;

    .line 627
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 629
    :cond_1
    sget-object v0, Lcom/google/android/videos/proto/TagProtos$Tag;->_emptyArray:[Lcom/google/android/videos/proto/TagProtos$Tag;

    return-object v0

    .line 627
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/videos/proto/TagProtos$Tag;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 649
    iput v0, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->splitId:I

    .line 650
    iput v0, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->offCameraShape:I

    .line 651
    iput v0, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->circle:I

    .line 652
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->faceRectShape:J

    .line 653
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->cachedSize:I

    .line 654
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 712
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 713
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->splitId:I

    if-eqz v1, :cond_0

    .line 714
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->splitId:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 717
    :cond_0
    iget v1, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->offCameraShape:I

    if-eqz v1, :cond_1

    .line 718
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->offCameraShape:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 721
    :cond_1
    iget-wide v2, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->faceRectShape:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 722
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->faceRectShape:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 725
    :cond_2
    iget v1, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->circle:I

    if-eqz v1, :cond_3

    .line 726
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->circle:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 729
    :cond_3
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 659
    if-ne p1, p0, :cond_1

    .line 678
    :cond_0
    :goto_0
    return v1

    .line 662
    :cond_1
    instance-of v3, p1, Lcom/google/android/videos/proto/TagProtos$Tag;

    if-nez v3, :cond_2

    move v1, v2

    .line 663
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 665
    check-cast v0, Lcom/google/android/videos/proto/TagProtos$Tag;

    .line 666
    .local v0, "other":Lcom/google/android/videos/proto/TagProtos$Tag;
    iget v3, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->splitId:I

    iget v4, v0, Lcom/google/android/videos/proto/TagProtos$Tag;->splitId:I

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 667
    goto :goto_0

    .line 669
    :cond_3
    iget v3, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->offCameraShape:I

    iget v4, v0, Lcom/google/android/videos/proto/TagProtos$Tag;->offCameraShape:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 670
    goto :goto_0

    .line 672
    :cond_4
    iget v3, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->circle:I

    iget v4, v0, Lcom/google/android/videos/proto/TagProtos$Tag;->circle:I

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 673
    goto :goto_0

    .line 675
    :cond_5
    iget-wide v4, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->faceRectShape:J

    iget-wide v6, v0, Lcom/google/android/videos/proto/TagProtos$Tag;->faceRectShape:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    move v1, v2

    .line 676
    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    .line 683
    const/16 v0, 0x11

    .line 684
    .local v0, "result":I
    iget v1, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->splitId:I

    add-int/lit16 v0, v1, 0x20f

    .line 685
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->offCameraShape:I

    add-int v0, v1, v2

    .line 686
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->circle:I

    add-int v0, v1, v2

    .line 687
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->faceRectShape:J

    iget-wide v4, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->faceRectShape:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 689
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/TagProtos$Tag;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 737
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 738
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 742
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 743
    :sswitch_0
    return-object p0

    .line 748
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->splitId:I

    goto :goto_0

    .line 752
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->offCameraShape:I

    goto :goto_0

    .line 756
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->faceRectShape:J

    goto :goto_0

    .line 760
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->circle:I

    goto :goto_0

    .line 738
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 615
    invoke-virtual {p0, p1}, Lcom/google/android/videos/proto/TagProtos$Tag;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/TagProtos$Tag;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 695
    iget v0, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->splitId:I

    if-eqz v0, :cond_0

    .line 696
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->splitId:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 698
    :cond_0
    iget v0, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->offCameraShape:I

    if-eqz v0, :cond_1

    .line 699
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->offCameraShape:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 701
    :cond_1
    iget-wide v0, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->faceRectShape:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 702
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->faceRectShape:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 704
    :cond_2
    iget v0, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->circle:I

    if-eqz v0, :cond_3

    .line 705
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->circle:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 707
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 708
    return-void
.end method
