.class public Lcom/google/android/videos/ui/WishlistFlowHelper;
.super Ljava/lang/Object;
.source "WishlistFlowHelper.java"


# instance fields
.field private final flow:Lcom/google/android/videos/flow/Flow;

.field private final moviesFlow:Lcom/google/android/videos/flow/Flow;

.field private final noWishlistContentFlow:Lcom/google/android/videos/ui/NoWishlistContentFlow;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/videos/adapter/WishlistDataSource;Lcom/google/android/videos/adapter/WishlistDataSource;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;Lcom/google/android/videos/ui/playnext/WishlistClusterItemView$Binder;)V
    .locals 10
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "wishlistMoviesDataSource"    # Lcom/google/android/videos/adapter/WishlistDataSource;
    .param p3, "wishlistShowsDataSource"    # Lcom/google/android/videos/adapter/WishlistDataSource;
    .param p4, "wishlistItemClickListener"    # Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;
    .param p5, "wishlistItemBinder"    # Lcom/google/android/videos/ui/playnext/WishlistClusterItemView$Binder;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    new-instance v9, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;

    new-instance v0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;

    const v2, 0x7f0b00c4

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;-><init>(Landroid/content/Context;IIILandroid/view/View$OnClickListener;ILjava/lang/Object;)V

    const v4, 0x7f04002f

    const/4 v7, -0x1

    move-object v1, v9

    move-object v2, v0

    move-object v3, p2

    move-object v5, p5

    move-object v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;-><init>(Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;Lcom/google/android/videos/adapter/DataSource;ILcom/google/android/videos/ui/playnext/ClusterItemView$Binder;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;I)V

    iput-object v9, p0, Lcom/google/android/videos/ui/WishlistFlowHelper;->moviesFlow:Lcom/google/android/videos/flow/Flow;

    .line 46
    new-instance v8, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;

    new-instance v0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;

    const v2, 0x7f0b00c5

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;-><init>(Landroid/content/Context;IIILandroid/view/View$OnClickListener;ILjava/lang/Object;)V

    const v4, 0x7f04002f

    const/4 v7, -0x1

    move-object v1, v8

    move-object v2, v0

    move-object v3, p3

    move-object v5, p5

    move-object v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;-><init>(Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;Lcom/google/android/videos/adapter/DataSource;ILcom/google/android/videos/ui/playnext/ClusterItemView$Binder;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;I)V

    .line 55
    .local v8, "showsFlow":Lcom/google/android/videos/flow/Flow;
    new-instance v0, Lcom/google/android/videos/ui/NoWishlistContentFlow;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getMinimumHeaderHeight(Landroid/content/Context;II)I

    move-result v1

    invoke-direct {v0, v1}, Lcom/google/android/videos/ui/NoWishlistContentFlow;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/videos/ui/WishlistFlowHelper;->noWishlistContentFlow:Lcom/google/android/videos/ui/NoWishlistContentFlow;

    .line 58
    new-instance v0, Lcom/google/android/videos/flow/SequentialFlow;

    const/4 v1, 0x4

    new-array v1, v1, [Lcom/google/android/videos/flow/Flow;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/videos/ui/WishlistFlowHelper;->noWishlistContentFlow:Lcom/google/android/videos/ui/NoWishlistContentFlow;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Lcom/google/android/videos/ui/PlayListSpacerFlow;

    const/4 v4, 0x2

    const/4 v5, -0x6

    invoke-direct {v3, v4, v5}, Lcom/google/android/videos/ui/PlayListSpacerFlow;-><init>(II)V

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/videos/ui/WishlistFlowHelper;->moviesFlow:Lcom/google/android/videos/flow/Flow;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object v8, v1, v2

    invoke-direct {v0, v1}, Lcom/google/android/videos/flow/SequentialFlow;-><init>([Lcom/google/android/videos/flow/Flow;)V

    iput-object v0, p0, Lcom/google/android/videos/ui/WishlistFlowHelper;->flow:Lcom/google/android/videos/flow/Flow;

    .line 62
    return-void
.end method

.method public static getHeaderBottomMargin(Landroid/content/Context;)I
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 70
    const/4 v0, -0x6

    invoke-static {p0, v0}, Lcom/google/android/videos/ui/PlayListSpacerFlow;->getHeaderContentGap(Landroid/content/Context;I)I

    move-result v0

    return v0
.end method

.method public static getHeaderHeight(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    const/4 v0, 0x2

    const/4 v1, -0x6

    invoke-static {p0, v0, v1}, Lcom/google/android/videos/ui/PlayListSpacerFlow;->getSpacerHeight(Landroid/content/Context;II)I

    move-result v0

    return v0
.end method


# virtual methods
.method public getFlow()Lcom/google/android/videos/flow/Flow;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistFlowHelper;->flow:Lcom/google/android/videos/flow/Flow;

    return-object v0
.end method

.method public getShowsHeadingPosition()I
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistFlowHelper;->moviesFlow:Lcom/google/android/videos/flow/Flow;

    invoke-virtual {v0}, Lcom/google/android/videos/flow/Flow;->getVisibleCount()I

    move-result v0

    return v0
.end method

.method public hideNoWishlistedMovies()V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistFlowHelper;->noWishlistContentFlow:Lcom/google/android/videos/ui/NoWishlistContentFlow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/NoWishlistContentFlow;->setVisible(Z)V

    .line 87
    return-void
.end method

.method public showNoWishlistedMovies()V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistFlowHelper;->noWishlistContentFlow:Lcom/google/android/videos/ui/NoWishlistContentFlow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/NoWishlistContentFlow;->setVisible(Z)V

    .line 83
    return-void
.end method

.method public updateNoWishlistedMoviesText(Z)V
    .locals 1
    .param p1, "showsVerticalEnabled"    # Z

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistFlowHelper;->noWishlistContentFlow:Lcom/google/android/videos/ui/NoWishlistContentFlow;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/ui/NoWishlistContentFlow;->updateText(Z)V

    .line 91
    return-void
.end method
