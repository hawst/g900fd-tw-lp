.class public Lcom/google/android/videos/api/AssetsRequest$Builder;
.super Ljava/lang/Object;
.source "AssetsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/api/AssetsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private flags:I

.field private idCount:I

.field private final ids:Ljava/lang/StringBuilder;

.field private locale:Ljava/util/Locale;

.field private requireAuthentication:Z

.field private userCountry:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/api/AssetsRequest$Builder;->ids:Ljava/lang/StringBuilder;

    .line 84
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/api/AssetsRequest$Builder;->requireAuthentication:Z

    .line 85
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/api/AssetsRequest$Builder;->locale:Ljava/util/Locale;

    .line 86
    return-void
.end method


# virtual methods
.method public account(Ljava/lang/String;)Lcom/google/android/videos/api/AssetsRequest$Builder;
    .locals 0
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/google/android/videos/api/AssetsRequest$Builder;->account:Ljava/lang/String;

    .line 90
    return-object p0
.end method

.method public addFlags(I)Lcom/google/android/videos/api/AssetsRequest$Builder;
    .locals 1
    .param p1, "flags"    # I

    .prologue
    .line 104
    iget v0, p0, Lcom/google/android/videos/api/AssetsRequest$Builder;->flags:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/videos/api/AssetsRequest$Builder;->flags:I

    .line 105
    return-object p0
.end method

.method public build()Lcom/google/android/videos/api/AssetsRequest;
    .locals 7

    .prologue
    .line 148
    new-instance v0, Lcom/google/android/videos/api/AssetsRequest;

    iget-object v1, p0, Lcom/google/android/videos/api/AssetsRequest$Builder;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/api/AssetsRequest$Builder;->ids:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/api/AssetsRequest$Builder;->userCountry:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/videos/api/AssetsRequest$Builder;->locale:Ljava/util/Locale;

    iget v5, p0, Lcom/google/android/videos/api/AssetsRequest$Builder;->flags:I

    iget-boolean v6, p0, Lcom/google/android/videos/api/AssetsRequest$Builder;->requireAuthentication:Z

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/api/AssetsRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;IZ)V

    return-object v0
.end method

.method public clearIds()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 143
    iget-object v0, p0, Lcom/google/android/videos/api/AssetsRequest$Builder;->ids:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 144
    iput v1, p0, Lcom/google/android/videos/api/AssetsRequest$Builder;->idCount:I

    .line 145
    return-void
.end method

.method public getIdCount()I
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Lcom/google/android/videos/api/AssetsRequest$Builder;->idCount:I

    return v0
.end method

.method public maybeAddId(Ljava/lang/String;)Z
    .locals 4
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 125
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 126
    iget v2, p0, Lcom/google/android/videos/api/AssetsRequest$Builder;->idCount:I

    const/16 v3, 0x64

    if-lt v2, v3, :cond_1

    .line 139
    :cond_0
    :goto_0
    return v1

    .line 129
    :cond_1
    iget v2, p0, Lcom/google/android/videos/api/AssetsRequest$Builder;->idCount:I

    if-nez v2, :cond_3

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 131
    .local v0, "wouldBeLength":I
    :goto_1
    const/16 v2, 0x708

    if-gt v0, v2, :cond_0

    .line 134
    iget v1, p0, Lcom/google/android/videos/api/AssetsRequest$Builder;->idCount:I

    if-eqz v1, :cond_2

    .line 135
    iget-object v1, p0, Lcom/google/android/videos/api/AssetsRequest$Builder;->ids:Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/api/AssetsRequest$Builder;->ids:Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    iget v1, p0, Lcom/google/android/videos/api/AssetsRequest$Builder;->idCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/videos/api/AssetsRequest$Builder;->idCount:I

    .line 139
    const/4 v1, 0x1

    goto :goto_0

    .line 129
    .end local v0    # "wouldBeLength":I
    :cond_3
    iget-object v2, p0, Lcom/google/android/videos/api/AssetsRequest$Builder;->ids:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    const-string v3, ","

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int v0, v2, v3

    goto :goto_1
.end method

.method public requireAuthentication(Z)Lcom/google/android/videos/api/AssetsRequest$Builder;
    .locals 0
    .param p1, "requireAuthentication"    # Z

    .prologue
    .line 109
    iput-boolean p1, p0, Lcom/google/android/videos/api/AssetsRequest$Builder;->requireAuthentication:Z

    .line 110
    return-object p0
.end method

.method public userCountry(Ljava/lang/String;)Lcom/google/android/videos/api/AssetsRequest$Builder;
    .locals 0
    .param p1, "userCountry"    # Ljava/lang/String;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/google/android/videos/api/AssetsRequest$Builder;->userCountry:Ljava/lang/String;

    .line 95
    return-object p0
.end method
