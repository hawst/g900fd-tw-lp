.class public interface abstract Lcom/google/android/videos/utils/NetworkStatus;
.super Ljava/lang/Object;
.source "NetworkStatus.java"

# interfaces
.implements Lcom/google/android/repolib/observers/Observable;


# virtual methods
.method public abstract getNetworkInfo()Landroid/net/NetworkInfo;
.end method

.method public abstract getNetworkSubtype()I
.end method

.method public abstract getNetworkType()I
.end method

.method public abstract isChargeableNetwork()Z
.end method

.method public abstract isFastNetwork()Z
.end method

.method public abstract isMobileNetwork()Z
.end method

.method public abstract isMobileNetworkCapable()Z
.end method

.method public abstract isNetworkAvailable()Z
.end method

.method public abstract isWiFiNetwork()Z
.end method
