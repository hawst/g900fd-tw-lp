.class Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;
.super Ljava/lang/Object;
.source "BlockBufferPool.java"

# interfaces
.implements Lcom/google/android/exoplayer/upstream/Allocation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/exo/BlockBufferPool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AllocationImpl"
.end annotation


# instance fields
.field public buffers:[[B

.field public fragmentIndices:[I

.field public fragmentOffsets:[I

.field final synthetic this$0:Lcom/google/android/videos/player/exo/BlockBufferPool;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/player/exo/BlockBufferPool;)V
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;->this$0:Lcom/google/android/videos/player/exo/BlockBufferPool;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/player/exo/BlockBufferPool;Lcom/google/android/videos/player/exo/BlockBufferPool$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/player/exo/BlockBufferPool;
    .param p2, "x1"    # Lcom/google/android/videos/player/exo/BlockBufferPool$1;

    .prologue
    .line 164
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;-><init>(Lcom/google/android/videos/player/exo/BlockBufferPool;)V

    return-void
.end method


# virtual methods
.method public capacity()I
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;->buffers:[[B

    array-length v0, v0

    iget-object v1, p0, Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;->this$0:Lcom/google/android/videos/player/exo/BlockBufferPool;

    iget v1, v1, Lcom/google/android/videos/player/exo/BlockBufferPool;->fragmentLength:I

    mul-int/2addr v0, v1

    return v0
.end method

.method public ensureCapacity(I)V
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;->this$0:Lcom/google/android/videos/player/exo/BlockBufferPool;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/videos/player/exo/BlockBufferPool;->ensureCapacity(Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;I)V

    .line 179
    return-void
.end method

.method public getBuffers()[[B
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;->buffers:[[B

    return-object v0
.end method

.method public getFragmentLength(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;->this$0:Lcom/google/android/videos/player/exo/BlockBufferPool;

    iget v0, v0, Lcom/google/android/videos/player/exo/BlockBufferPool;->fragmentLength:I

    return v0
.end method

.method public getFragmentOffset(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;->fragmentOffsets:[I

    aget v0, v0, p1

    return v0
.end method

.method public init([[B[I[I)V
    .locals 0
    .param p1, "buffers"    # [[B
    .param p2, "fragmentIndices"    # [I
    .param p3, "fragmentOffsets"    # [I

    .prologue
    .line 171
    iput-object p1, p0, Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;->buffers:[[B

    .line 172
    iput-object p2, p0, Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;->fragmentIndices:[I

    .line 173
    iput-object p3, p0, Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;->fragmentOffsets:[I

    .line 174
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;->buffers:[[B

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;->this$0:Lcom/google/android/videos/player/exo/BlockBufferPool;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/player/exo/BlockBufferPool;->release(Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;)V

    .line 205
    const/4 v0, 0x0

    check-cast v0, [[B

    iput-object v0, p0, Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;->buffers:[[B

    .line 207
    :cond_0
    return-void
.end method
