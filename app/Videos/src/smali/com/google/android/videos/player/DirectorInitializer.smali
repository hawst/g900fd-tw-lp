.class public Lcom/google/android/videos/player/DirectorInitializer;
.super Ljava/lang/Object;
.source "DirectorInitializer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;,
        Lcom/google/android/videos/player/DirectorInitializer$PurchaseQuery;,
        Lcom/google/android/videos/player/DirectorInitializer$DirectorAuthenticatee;,
        Lcom/google/android/videos/player/DirectorInitializer$InitializationData;,
        Lcom/google/android/videos/player/DirectorInitializer$Listener;
    }
.end annotation


# instance fields
.field private final account:Ljava/lang/String;

.field private final accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

.field private final activity:Landroid/app/Activity;

.field private final assetsCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private cancelableAuthenticatee:Lcom/google/android/videos/async/CancelableAuthenticatee;

.field private final cancelableCallbacks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/async/CancelableCallback",
            "<**>;>;"
        }
    .end annotation
.end field

.field private cencKeySetId:[B

.field private cencSecurityLevel:I

.field private final config:Lcom/google/android/videos/Config;

.field private final configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

.field private drmIdentifiers:Lcom/google/android/videos/drm/DrmManager$Identifiers;

.field private durationMillis:I

.field private final errorHelper:Lcom/google/android/videos/utils/ErrorHelper;

.field private final handler:Landroid/os/Handler;

.field private haveAudioInDeviceLanguage:Z

.field private initializationError:Ljava/lang/Exception;

.field private initializationErrorCanRetry:Z

.field private initializationErrorLevel:I

.field private initializationErrorMessage:Ljava/lang/String;

.field private initializationErrorType:I

.field private final isEpisode:Z

.field private final isRemotePlayback:Z

.field private isRental:Z

.field private final itagInfoStore:Lcom/google/android/videos/store/ItagInfoStore;

.field private lastWatchedTimestamp:J

.field private final legacyDownloadsHaveAppLevelDrm:Z

.field private final listener:Lcom/google/android/videos/player/DirectorInitializer$Listener;

.field private final localExecutor:Ljava/util/concurrent/Executor;

.field private final networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

.field private offlineCaptionTracks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;"
        }
    .end annotation
.end field

.field private offlineMediaStreams:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;"
        }
    .end annotation
.end field

.field private final offlineStoryboardCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/Storyboard;",
            ">;"
        }
    .end annotation
.end field

.field private offlineStoryboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

.field private final offlineSubtitleTracksCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;>;"
        }
    .end annotation
.end field

.field private volatile pendingInitializationTasks:I

.field private pinnedStorage:I

.field private final playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

.field private posterUri:Ljava/lang/String;

.field private final preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

.field private purchaseStatus:I

.field private final purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

.field private final purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

.field private released:Z

.field private resumeTimeMillis:I

.field private shortClockMillis:J

.field private showShortClockDialog:Z

.field private showTitle:Ljava/lang/String;

.field private startOfCreditSec:I

.field private final storedPurchaseCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final storyboardClient:Lcom/google/android/videos/store/StoryboardClient;

.field private streams:Lcom/google/android/videos/streams/Streams;

.field private final streamsCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/api/MpdGetRequest;",
            "Lcom/google/android/videos/streams/Streams;",
            ">;"
        }
    .end annotation
.end field

.field private final streamsRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/MpdGetRequest;",
            "Lcom/google/android/videos/streams/Streams;",
            ">;"
        }
    .end annotation
.end field

.field private subtitleDefaultLanguage:Ljava/lang/String;

.field private subtitleMode:I

.field private final subtitlesClient:Lcom/google/android/videos/store/SubtitlesClient;

.field private final syncPurchasesCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private volatile syncTaskControl:Lcom/google/android/videos/async/TaskControl;

.field private final useDashStreams:Z

.field private final videoGetCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/api/VideoGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
            ">;"
        }
    .end annotation
.end field

.field private final videoGetRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/VideoGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
            ">;"
        }
    .end annotation
.end field

.field private final videoId:Ljava/lang/String;

.field private videoTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/VideosGlobals;Landroid/app/Activity;Lcom/google/android/videos/player/DirectorInitializer$Listener;Ljava/lang/String;ZZLjava/lang/String;Lcom/google/android/videos/player/PlaybackResumeState;ZZLcom/google/android/videos/player/logging/PlaybackPreparationLogger;)V
    .locals 9
    .param p1, "videosGlobals"    # Lcom/google/android/videos/VideosGlobals;
    .param p2, "activity"    # Landroid/app/Activity;
    .param p3, "listener"    # Lcom/google/android/videos/player/DirectorInitializer$Listener;
    .param p4, "videoId"    # Ljava/lang/String;
    .param p5, "isEpisode"    # Z
    .param p6, "isTrailer"    # Z
    .param p7, "account"    # Ljava/lang/String;
    .param p8, "playbackResumeState"    # Lcom/google/android/videos/player/PlaybackResumeState;
    .param p9, "isRemotePlayback"    # Z
    .param p10, "useDashStreams"    # Z
    .param p11, "preparationLogger"    # Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    .prologue
    .line 221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 222
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/Activity;

    iput-object v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->activity:Landroid/app/Activity;

    .line 223
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/videos/player/DirectorInitializer$Listener;

    iput-object v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->listener:Lcom/google/android/videos/player/DirectorInitializer$Listener;

    .line 224
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iput-object v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->videoId:Ljava/lang/String;

    .line 225
    iput-boolean p5, p0, Lcom/google/android/videos/player/DirectorInitializer;->isEpisode:Z

    .line 226
    if-eqz p6, :cond_0

    move-object/from16 v5, p7

    :goto_0
    iput-object v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->account:Ljava/lang/String;

    .line 227
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    .line 228
    move/from16 v0, p9

    iput-boolean v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->isRemotePlayback:Z

    .line 229
    move/from16 v0, p10

    iput-boolean v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->useDashStreams:Z

    .line 230
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->config:Lcom/google/android/videos/Config;

    .line 231
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    .line 232
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getErrorHelper()Lcom/google/android/videos/utils/ErrorHelper;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->errorHelper:Lcom/google/android/videos/utils/ErrorHelper;

    .line 233
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getItagInfoStore()Lcom/google/android/videos/store/ItagInfoStore;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->itagInfoStore:Lcom/google/android/videos/store/ItagInfoStore;

    .line 234
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStore()Lcom/google/android/videos/store/PurchaseStore;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    .line 235
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStoreSync()Lcom/google/android/videos/store/PurchaseStoreSync;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

    .line 236
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    .line 237
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getSubtitlesClient()Lcom/google/android/videos/store/SubtitlesClient;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->subtitlesClient:Lcom/google/android/videos/store/SubtitlesClient;

    .line 238
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getStoryboardClient()Lcom/google/android/videos/store/StoryboardClient;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->storyboardClient:Lcom/google/android/videos/store/StoryboardClient;

    .line 239
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 240
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->legacyDownloadsHaveAppLevelDrm()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->legacyDownloadsHaveAppLevelDrm:Z

    .line 241
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getLocalExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->localExecutor:Ljava/util/concurrent/Executor;

    .line 242
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    .line 243
    const-wide/16 v6, -0x1

    iput-wide v6, p0, Lcom/google/android/videos/player/DirectorInitializer;->shortClockMillis:J

    .line 244
    const/4 v5, -0x1

    iput v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->pinnedStorage:I

    .line 245
    const/4 v5, 0x0

    iput v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->initializationErrorLevel:I

    .line 247
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v2

    .line 248
    .local v2, "apiRequesters":Lcom/google/android/videos/api/ApiRequesters;
    invoke-interface {v2}, Lcom/google/android/videos/api/ApiRequesters;->getStreamsRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->streamsRequester:Lcom/google/android/videos/async/Requester;

    .line 249
    invoke-interface {v2}, Lcom/google/android/videos/api/ApiRequesters;->getAssetsCachingRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v3

    .line 251
    .local v3, "assetsCachingRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;>;"
    invoke-interface {v2}, Lcom/google/android/videos/api/ApiRequesters;->getVideoGetRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->videoGetRequester:Lcom/google/android/videos/async/Requester;

    .line 253
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->cancelableCallbacks:Ljava/util/List;

    .line 254
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    iput-object v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->handler:Landroid/os/Handler;

    .line 256
    invoke-direct {p0}, Lcom/google/android/videos/player/DirectorInitializer;->initAssetsCallback()Lcom/google/android/videos/async/Callback;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->assetsCallback:Lcom/google/android/videos/async/Callback;

    .line 257
    invoke-direct {p0}, Lcom/google/android/videos/player/DirectorInitializer;->initOfflineSubtitleTracksCallback()Lcom/google/android/videos/async/Callback;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->offlineSubtitleTracksCallback:Lcom/google/android/videos/async/Callback;

    .line 258
    invoke-direct {p0}, Lcom/google/android/videos/player/DirectorInitializer;->initOfflineStoryboardCallback()Lcom/google/android/videos/async/Callback;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->offlineStoryboardCallback:Lcom/google/android/videos/async/Callback;

    .line 259
    invoke-direct {p0}, Lcom/google/android/videos/player/DirectorInitializer;->initStoredPurchaseCallback()Lcom/google/android/videos/async/Callback;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->storedPurchaseCallback:Lcom/google/android/videos/async/Callback;

    .line 260
    invoke-direct {p0}, Lcom/google/android/videos/player/DirectorInitializer;->initStreamsCallback()Lcom/google/android/videos/async/Callback;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->streamsCallback:Lcom/google/android/videos/async/Callback;

    .line 261
    invoke-direct {p0}, Lcom/google/android/videos/player/DirectorInitializer;->initSyncPurchaseCallback()Lcom/google/android/videos/async/Callback;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->syncPurchasesCallback:Lcom/google/android/videos/async/Callback;

    .line 262
    invoke-direct {p0}, Lcom/google/android/videos/player/DirectorInitializer;->initVideoGetCallback()Lcom/google/android/videos/async/Callback;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->videoGetCallback:Lcom/google/android/videos/async/Callback;

    .line 264
    if-eqz p6, :cond_1

    .line 265
    new-instance v5, Lcom/google/android/videos/api/AssetsRequest$Builder;

    invoke-direct {v5}, Lcom/google/android/videos/api/AssetsRequest$Builder;-><init>()V

    move-object/from16 v0, p7

    invoke-virtual {v5, v0}, Lcom/google/android/videos/api/AssetsRequest$Builder;->account(Ljava/lang/String;)Lcom/google/android/videos/api/AssetsRequest$Builder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/videos/player/DirectorInitializer;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    move-object/from16 v0, p7

    invoke-virtual {v6, v0}, Lcom/google/android/videos/store/ConfigurationStore;->getPlayCountry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/videos/api/AssetsRequest$Builder;->userCountry(Ljava/lang/String;)Lcom/google/android/videos/api/AssetsRequest$Builder;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/google/android/videos/api/AssetsRequest$Builder;->addFlags(I)Lcom/google/android/videos/api/AssetsRequest$Builder;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/google/android/videos/api/AssetsRequest$Builder;->requireAuthentication(Z)Lcom/google/android/videos/api/AssetsRequest$Builder;

    move-result-object v4

    .line 270
    .local v4, "requestBuilder":Lcom/google/android/videos/api/AssetsRequest$Builder;
    invoke-static {p4}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromMovieId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/videos/api/AssetsRequest$Builder;->maybeAddId(Ljava/lang/String;)Z

    .line 271
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Getting trailer asset for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 272
    const/4 v5, 0x5

    move-object/from16 v0, p11

    invoke-virtual {v0, v5}, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->recordTaskStart(I)V

    .line 273
    invoke-virtual {v4}, Lcom/google/android/videos/api/AssetsRequest$Builder;->build()Lcom/google/android/videos/api/AssetsRequest;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/videos/player/DirectorInitializer;->assetsCallback:Lcom/google/android/videos/async/Callback;

    invoke-direct {p0, v6}, Lcom/google/android/videos/player/DirectorInitializer;->decorateCallback(Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/Callback;

    move-result-object v6

    invoke-interface {v3, v5, v6}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 281
    .end local v4    # "requestBuilder":Lcom/google/android/videos/api/AssetsRequest$Builder;
    :goto_1
    return-void

    .line 226
    .end local v2    # "apiRequesters":Lcom/google/android/videos/api/ApiRequesters;
    .end local v3    # "assetsCachingRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;>;"
    :cond_0
    invoke-static/range {p7 .. p7}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 275
    .restart local v2    # "apiRequesters":Lcom/google/android/videos/api/ApiRequesters;
    .restart local v3    # "assetsCachingRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;>;"
    :cond_1
    const/4 v5, 0x4

    move-object/from16 v0, p11

    invoke-virtual {v0, v5}, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->recordTaskStart(I)V

    .line 276
    iget-object v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    sget-object v6, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->COLUMNS:[Ljava/lang/String;

    move-object/from16 v0, p7

    invoke-static {v0, v6, p4}, Lcom/google/android/videos/store/PurchaseRequests;->createPurchaseRequestForUser(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/videos/player/DirectorInitializer;->storedPurchaseCallback:Lcom/google/android/videos/async/Callback;

    const/4 v8, 0x0

    invoke-direct {p0, v7, v8}, Lcom/google/android/videos/player/DirectorInitializer;->decorateCallback(Lcom/google/android/videos/async/Callback;Z)Lcom/google/android/videos/async/Callback;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/google/android/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/videos/async/Callback;)V

    goto :goto_1
.end method

.method static synthetic access$100(Lcom/google/android/videos/player/DirectorInitializer;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->videoId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/videos/player/DirectorInitializer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->initializationErrorCanRetry:Z

    return v0
.end method

.method static synthetic access$1100(Lcom/google/android/videos/player/DirectorInitializer;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;

    .prologue
    .line 81
    iget v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->initializationErrorType:I

    return v0
.end method

.method static synthetic access$1200(Lcom/google/android/videos/player/DirectorInitializer;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->initializationErrorMessage:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/videos/player/DirectorInitializer;)Ljava/lang/Exception;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->initializationError:Ljava/lang/Exception;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/videos/player/DirectorInitializer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->isRental:Z

    return v0
.end method

.method static synthetic access$1500(Lcom/google/android/videos/player/DirectorInitializer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->showShortClockDialog:Z

    return v0
.end method

.method static synthetic access$1600(Lcom/google/android/videos/player/DirectorInitializer;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;

    .prologue
    .line 81
    iget-wide v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->shortClockMillis:J

    return-wide v0
.end method

.method static synthetic access$1700(Lcom/google/android/videos/player/DirectorInitializer;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;

    .prologue
    .line 81
    iget v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->durationMillis:I

    return v0
.end method

.method static synthetic access$1800(Lcom/google/android/videos/player/DirectorInitializer;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;

    .prologue
    .line 81
    iget v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->resumeTimeMillis:I

    return v0
.end method

.method static synthetic access$1900(Lcom/google/android/videos/player/DirectorInitializer;)Lcom/google/android/videos/streams/Streams;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->streams:Lcom/google/android/videos/streams/Streams;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/google/android/videos/player/DirectorInitializer;Lcom/google/android/videos/streams/Streams;)Lcom/google/android/videos/streams/Streams;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;
    .param p1, "x1"    # Lcom/google/android/videos/streams/Streams;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/google/android/videos/player/DirectorInitializer;->streams:Lcom/google/android/videos/streams/Streams;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/videos/player/DirectorInitializer;)Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/android/videos/player/DirectorInitializer;)Lcom/google/android/videos/drm/DrmManager$Identifiers;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->drmIdentifiers:Lcom/google/android/videos/drm/DrmManager$Identifiers;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/google/android/videos/player/DirectorInitializer;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;

    .prologue
    .line 81
    iget v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->subtitleMode:I

    return v0
.end method

.method static synthetic access$2200(Lcom/google/android/videos/player/DirectorInitializer;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->subtitleDefaultLanguage:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/google/android/videos/player/DirectorInitializer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->haveAudioInDeviceLanguage:Z

    return v0
.end method

.method static synthetic access$2400(Lcom/google/android/videos/player/DirectorInitializer;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->videoTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/google/android/videos/player/DirectorInitializer;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->showTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/google/android/videos/player/DirectorInitializer;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->posterUri:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/google/android/videos/player/DirectorInitializer;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;

    .prologue
    .line 81
    iget v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->pinnedStorage:I

    return v0
.end method

.method static synthetic access$2800(Lcom/google/android/videos/player/DirectorInitializer;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/google/android/videos/player/DirectorInitializer;->onNotAuthenticated()V

    return-void
.end method

.method static synthetic access$2900(Lcom/google/android/videos/player/DirectorInitializer;ILjava/lang/Exception;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/Exception;
    .param p3, "x3"    # Z

    .prologue
    .line 81
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/player/DirectorInitializer;->onInitializationError(ILjava/lang/Exception;Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/videos/player/DirectorInitializer;Z)Lcom/google/android/videos/api/MpdGetRequest;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;
    .param p1, "x1"    # Z

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/DirectorInitializer;->generateStreamsRequest(Z)Lcom/google/android/videos/api/MpdGetRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3000(Lcom/google/android/videos/player/DirectorInitializer;Landroid/database/Cursor;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/DirectorInitializer;->onSyncedPurchaseCursor(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic access$3100(Lcom/google/android/videos/player/DirectorInitializer;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/Callback;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;
    .param p1, "x1"    # Lcom/google/android/videos/async/Callback;

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/DirectorInitializer;->decorateCallback(Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/Callback;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3200(Lcom/google/android/videos/player/DirectorInitializer;)Lcom/google/android/videos/store/PurchaseStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/google/android/videos/player/DirectorInitializer;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/google/android/videos/player/DirectorInitializer;->onTaskCompleted()V

    return-void
.end method

.method static synthetic access$3400(Lcom/google/android/videos/player/DirectorInitializer;Landroid/database/Cursor;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/DirectorInitializer;->onStoredPurchaseCursor(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic access$3500(Lcom/google/android/videos/player/DirectorInitializer;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/google/android/videos/player/DirectorInitializer;->onNoOfflineStream()V

    return-void
.end method

.method static synthetic access$3600(Lcom/google/android/videos/player/DirectorInitializer;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;
    .param p1, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/DirectorInitializer;->onStreamsError(Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$3700(Lcom/google/android/videos/player/DirectorInitializer;Lcom/google/wireless/android/video/magma/proto/VideoResource;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;
    .param p1, "x1"    # Lcom/google/wireless/android/video/magma/proto/VideoResource;

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/DirectorInitializer;->onGetVideoResource(Lcom/google/wireless/android/video/magma/proto/VideoResource;)V

    return-void
.end method

.method static synthetic access$3800(Lcom/google/android/videos/player/DirectorInitializer;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;
    .param p1, "x1"    # Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/DirectorInitializer;->onTrailerAssetsResponse(Lcom/google/wireless/android/video/magma/proto/AssetListResponse;)V

    return-void
.end method

.method static synthetic access$3902(Lcom/google/android/videos/player/DirectorInitializer;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/google/android/videos/player/DirectorInitializer;->offlineCaptionTracks:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/videos/player/DirectorInitializer;)Lcom/google/android/videos/async/Requester;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->streamsRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method static synthetic access$4002(Lcom/google/android/videos/player/DirectorInitializer;[Lcom/google/wireless/android/video/magma/proto/Storyboard;)[Lcom/google/wireless/android/video/magma/proto/Storyboard;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;
    .param p1, "x1"    # [Lcom/google/wireless/android/video/magma/proto/Storyboard;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/google/android/videos/player/DirectorInitializer;->offlineStoryboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    return-object p1
.end method

.method static synthetic access$4100(Lcom/google/android/videos/player/DirectorInitializer;IZIILjava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z
    .param p3, "x3"    # I
    .param p4, "x4"    # I
    .param p5, "x5"    # Ljava/lang/Exception;

    .prologue
    .line 81
    invoke-direct/range {p0 .. p5}, Lcom/google/android/videos/player/DirectorInitializer;->onInitializationError(IZIILjava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/videos/player/DirectorInitializer;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;

    .prologue
    .line 81
    iget v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->cencSecurityLevel:I

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/videos/player/DirectorInitializer;)[B
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->cencKeySetId:[B

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/videos/player/DirectorInitializer;)Lcom/google/android/videos/player/DirectorInitializer$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->listener:Lcom/google/android/videos/player/DirectorInitializer$Listener;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/videos/player/DirectorInitializer;)Lcom/google/android/videos/async/TaskControl;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->syncTaskControl:Lcom/google/android/videos/async/TaskControl;

    return-object v0
.end method

.method static synthetic access$802(Lcom/google/android/videos/player/DirectorInitializer;Lcom/google/android/videos/async/TaskControl;)Lcom/google/android/videos/async/TaskControl;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;
    .param p1, "x1"    # Lcom/google/android/videos/async/TaskControl;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/google/android/videos/player/DirectorInitializer;->syncTaskControl:Lcom/google/android/videos/async/TaskControl;

    return-object p1
.end method

.method static synthetic access$900(Lcom/google/android/videos/player/DirectorInitializer;)Lcom/google/android/videos/store/PurchaseStoreSync;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

    return-object v0
.end method

.method private checkAuthentication(Lcom/google/android/videos/player/DirectorInitializer$DirectorAuthenticatee;)V
    .locals 4
    .param p1, "directorAuthenticatee"    # Lcom/google/android/videos/player/DirectorInitializer$DirectorAuthenticatee;

    .prologue
    .line 756
    new-instance v0, Lcom/google/android/videos/async/CancelableAuthenticatee;

    invoke-direct {v0, p1}, Lcom/google/android/videos/async/CancelableAuthenticatee;-><init>(Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;)V

    iput-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->cancelableAuthenticatee:Lcom/google/android/videos/async/CancelableAuthenticatee;

    .line 757
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    iget-object v1, p0, Lcom/google/android/videos/player/DirectorInitializer;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/player/DirectorInitializer;->activity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/videos/player/DirectorInitializer;->cancelableAuthenticatee:Lcom/google/android/videos/async/CancelableAuthenticatee;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getAuthToken(Ljava/lang/String;Landroid/app/Activity;Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;)V

    .line 758
    return-void
.end method

.method private containsLanguage(Ljava/util/List;Ljava/lang/String;)Z
    .locals 4
    .param p2, "language"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 653
    .local p1, "languages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 655
    new-instance v3, Ljava/util/Locale;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {v3, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 656
    .local v1, "languageCode":Ljava/lang/String;
    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 657
    const/4 v2, 0x1

    .line 660
    .end local v1    # "languageCode":Ljava/lang/String;
    :goto_1
    return v2

    .line 653
    .restart local v1    # "languageCode":Ljava/lang/String;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 660
    .end local v1    # "languageCode":Ljava/lang/String;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private decorateCallback(Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/Callback;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;)",
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;"
        }
    .end annotation

    .prologue
    .line 777
    .local p1, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TR;TE;>;"
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/player/DirectorInitializer;->decorateCallback(Lcom/google/android/videos/async/Callback;Z)Lcom/google/android/videos/async/Callback;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized decorateCallback(Lcom/google/android/videos/async/Callback;Z)Lcom/google/android/videos/async/Callback;
    .locals 2
    .param p2, "uiThreadCallback"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;Z)",
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;"
        }
    .end annotation

    .prologue
    .line 787
    .local p1, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TR;TE;>;"
    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/google/android/videos/player/DirectorInitializer;->pendingInitializationTasks:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/videos/player/DirectorInitializer;->pendingInitializationTasks:I

    .line 788
    invoke-static {p1}, Lcom/google/android/videos/async/CancelableCallback;->create(Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/CancelableCallback;

    move-result-object v0

    .line 789
    .local v0, "cancelableCallback":Lcom/google/android/videos/async/CancelableCallback;, "Lcom/google/android/videos/async/CancelableCallback<TR;TE;>;"
    iget-object v1, p0, Lcom/google/android/videos/player/DirectorInitializer;->cancelableCallbacks:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 790
    if-eqz p2, :cond_0

    .line 791
    iget-object v1, p0, Lcom/google/android/videos/player/DirectorInitializer;->activity:Landroid/app/Activity;

    invoke-static {v1, v0}, Lcom/google/android/videos/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/ActivityCallback;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 793
    .end local v0    # "cancelableCallback":Lcom/google/android/videos/async/CancelableCallback;, "Lcom/google/android/videos/async/CancelableCallback<TR;TE;>;"
    :cond_0
    monitor-exit p0

    return-object v0

    .line 787
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private generateStreamsRequest(Z)Lcom/google/android/videos/api/MpdGetRequest;
    .locals 8
    .param p1, "requireAuthentication"    # Z

    .prologue
    .line 945
    new-instance v0, Lcom/google/android/videos/api/MpdGetRequest;

    iget-object v1, p0, Lcom/google/android/videos/player/DirectorInitializer;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/player/DirectorInitializer;->videoId:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/videos/player/DirectorInitializer;->isEpisode:Z

    iget-boolean v4, p0, Lcom/google/android/videos/player/DirectorInitializer;->useDashStreams:Z

    iget-object v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->config:Lcom/google/android/videos/Config;

    invoke-interface {v5}, Lcom/google/android/videos/Config;->useSslForStreaming()Z

    move-result v6

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    move v5, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/api/MpdGetRequest;-><init>(Ljava/lang/String;Ljava/lang/String;ZZZZLjava/util/Locale;)V

    return-object v0
.end method

.method private getDownloadPath(Landroid/database/Cursor;)Ljava/io/File;
    .locals 9
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v1, 0x2

    .line 461
    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->activity:Landroid/app/Activity;

    iget v2, p0, Lcom/google/android/videos/player/DirectorInitializer;->pinnedStorage:I

    invoke-static {v0, v2}, Lcom/google/android/videos/utils/OfflineUtil;->getRootFilesDir(Landroid/content/Context;I)Ljava/io/File;

    move-result-object v8

    .line 462
    .local v8, "rootFilesDir":Ljava/io/File;
    sget v0, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->DOWNLOAD_RELATIVE_FILEPATH:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 463
    .local v7, "relativeFilePath":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v8, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/videos/utils/MediaNotMountedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 474
    .end local v7    # "relativeFilePath":Ljava/lang/String;
    .end local v8    # "rootFilesDir":Ljava/io/File;
    :goto_0
    return-object v0

    .line 464
    :catch_0
    move-exception v5

    .line 465
    .local v5, "e":Lcom/google/android/videos/utils/MediaNotMountedException;
    iget v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->pinnedStorage:I

    if-nez v0, :cond_0

    .line 466
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->errorHelper:Lcom/google/android/videos/utils/ErrorHelper;

    invoke-virtual {v0, v5}, Lcom/google/android/videos/utils/ErrorHelper;->getHumanizedRepresentation(Ljava/lang/Throwable;)Landroid/util/Pair;

    move-result-object v6

    .line 467
    .local v6, "reason":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    const/4 v2, 0x0

    iget-object v0, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v4, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/player/DirectorInitializer;->onInitializationError(IZILjava/lang/String;Ljava/lang/Exception;)V

    .line 474
    .end local v6    # "reason":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 471
    :cond_0
    const/4 v2, 0x1

    const/16 v3, 0x15

    const v4, 0x7f0b0168

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/player/DirectorInitializer;->onInitializationError(IZIILjava/lang/Exception;)V

    goto :goto_1
.end method

.method private getDownloadedStreams(Landroid/database/Cursor;Ljava/io/File;J)Ljava/util/List;
    .locals 23
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "downloadPath"    # Ljava/io/File;
    .param p3, "size"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/io/File;",
            "J)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 480
    invoke-static/range {p2 .. p2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v19

    .line 482
    .local v19, "streamUri":Landroid/net/Uri;
    const/4 v14, 0x0

    .line 483
    .local v14, "downloadExtra":Lcom/google/android/videos/proto/DownloadExtra;
    sget v5, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->DOWNLOAD_EXTRA:I

    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v15, 0x0

    .line 485
    .local v15, "downloadExtraBlob":[B
    :goto_0
    if-eqz v15, :cond_0

    .line 486
    invoke-static {v15}, Lcom/google/android/videos/proto/DownloadExtra;->parseFrom([B)Lcom/google/android/videos/proto/DownloadExtra;

    move-result-object v14

    .line 488
    :cond_0
    sget v5, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->LICENSE_TYPE:I

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v5, v6}, Lcom/google/android/videos/utils/DbUtils;->getIntOrDefault(Landroid/database/Cursor;II)I

    move-result v17

    .line 490
    .local v17, "licenseType":I
    const/4 v5, 0x1

    move/from16 v0, v17

    if-ne v0, v5, :cond_5

    .line 493
    if-eqz v14, :cond_3

    .line 494
    iget-object v5, v14, Lcom/google/android/videos/proto/DownloadExtra;->streamInfos:[Lcom/google/android/videos/proto/StreamInfo;

    const/4 v6, 0x0

    aget-object v18, v5, v6

    .line 506
    .local v18, "streamInfo":Lcom/google/android/videos/proto/StreamInfo;
    :goto_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/player/DirectorInitializer;->itagInfoStore:Lcom/google/android/videos/store/ItagInfoStore;

    move-object/from16 v0, v18

    iget v6, v0, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    invoke-virtual {v5, v6}, Lcom/google/android/videos/store/ItagInfoStore;->getItagInfo(I)Lcom/google/android/videos/streams/ItagInfo;

    move-result-object v4

    .line 507
    .local v4, "itagInfo":Lcom/google/android/videos/streams/ItagInfo;
    if-nez v4, :cond_1

    .line 511
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/videos/player/DirectorInitializer;->legacyDownloadsHaveAppLevelDrm:Z

    invoke-static {v5, v6}, Lcom/google/android/videos/utils/OfflineUtil;->isAppLevelDrmEncrypted(Ljava/lang/String;Z)Z

    move-result v11

    .line 513
    .local v11, "appLevelDrm":Z
    if-eqz v11, :cond_4

    const/4 v9, 0x1

    .line 514
    .local v9, "drmType":I
    :goto_2
    new-instance v4, Lcom/google/android/videos/streams/ItagInfo;

    .end local v4    # "itagInfo":Lcom/google/android/videos/streams/ItagInfo;
    const/16 v5, 0x2d0

    const/16 v6, 0x1e0

    const/4 v7, 0x2

    const/4 v8, 0x0

    const/4 v10, 0x0

    invoke-direct/range {v4 .. v10}, Lcom/google/android/videos/streams/ItagInfo;-><init>(IIIZIZ)V

    .line 515
    .restart local v4    # "itagInfo":Lcom/google/android/videos/streams/ItagInfo;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ItagInfo not found for downloaded video. Reconstructing itag: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 518
    .end local v9    # "drmType":I
    .end local v11    # "appLevelDrm":Z
    :cond_1
    new-instance v21, Lcom/google/android/videos/streams/MediaStream;

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/videos/streams/MediaStream;-><init>(Landroid/net/Uri;Lcom/google/android/videos/streams/ItagInfo;Lcom/google/android/videos/proto/StreamInfo;)V

    .line 519
    .local v21, "videoStream":Lcom/google/android/videos/streams/MediaStream;
    invoke-static/range {v21 .. v21}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v16

    .line 533
    .end local v4    # "itagInfo":Lcom/google/android/videos/streams/ItagInfo;
    .end local v18    # "streamInfo":Lcom/google/android/videos/proto/StreamInfo;
    .local v16, "downloadedStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    :goto_3
    return-object v16

    .line 483
    .end local v15    # "downloadExtraBlob":[B
    .end local v16    # "downloadedStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    .end local v17    # "licenseType":I
    .end local v21    # "videoStream":Lcom/google/android/videos/streams/MediaStream;
    :cond_2
    sget v5, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->DOWNLOAD_EXTRA:I

    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v15

    goto :goto_0

    .line 498
    .restart local v15    # "downloadExtraBlob":[B
    .restart local v17    # "licenseType":I
    :cond_3
    new-instance v18, Lcom/google/android/videos/proto/StreamInfo;

    invoke-direct/range {v18 .. v18}, Lcom/google/android/videos/proto/StreamInfo;-><init>()V

    .line 499
    .restart local v18    # "streamInfo":Lcom/google/android/videos/proto/StreamInfo;
    move-wide/from16 v0, p3

    move-object/from16 v2, v18

    iput-wide v0, v2, Lcom/google/android/videos/proto/StreamInfo;->sizeInBytes:J

    .line 500
    sget v5, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->LICENSE_LEGACY_VIDEO_FORMAT:I

    const/4 v6, -0x1

    move-object/from16 v0, p1

    invoke-static {v0, v5, v6}, Lcom/google/android/videos/utils/DbUtils;->getIntOrDefault(Landroid/database/Cursor;II)I

    move-result v5

    move-object/from16 v0, v18

    iput v5, v0, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    .line 502
    sget v5, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->DOWNLOAD_LAST_MODIFIED:I

    move-object/from16 v0, p1

    invoke-static {v0, v5}, Lcom/google/android/videos/utils/DbUtils;->getDateAsTimestamp(Landroid/database/Cursor;I)J

    move-result-wide v6

    move-object/from16 v0, v18

    iput-wide v6, v0, Lcom/google/android/videos/proto/StreamInfo;->lastModifiedTimestamp:J

    goto :goto_1

    .line 513
    .restart local v4    # "itagInfo":Lcom/google/android/videos/streams/ItagInfo;
    .restart local v11    # "appLevelDrm":Z
    :cond_4
    const/4 v9, 0x2

    goto :goto_2

    .line 522
    .end local v4    # "itagInfo":Lcom/google/android/videos/streams/ItagInfo;
    .end local v11    # "appLevelDrm":Z
    .end local v18    # "streamInfo":Lcom/google/android/videos/proto/StreamInfo;
    :cond_5
    iget-object v5, v14, Lcom/google/android/videos/proto/DownloadExtra;->streamInfos:[Lcom/google/android/videos/proto/StreamInfo;

    const/4 v6, 0x0

    aget-object v20, v5, v6

    .line 523
    .local v20, "videoExtra":Lcom/google/android/videos/proto/StreamInfo;
    new-instance v21, Lcom/google/android/videos/streams/MediaStream;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/player/DirectorInitializer;->itagInfoStore:Lcom/google/android/videos/store/ItagInfoStore;

    move-object/from16 v0, v20

    iget v6, v0, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    invoke-virtual {v5, v6}, Lcom/google/android/videos/store/ItagInfoStore;->getItagInfo(I)Lcom/google/android/videos/streams/ItagInfo;

    move-result-object v5

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/videos/streams/MediaStream;-><init>(Landroid/net/Uri;Lcom/google/android/videos/streams/ItagInfo;Lcom/google/android/videos/proto/StreamInfo;)V

    .line 525
    .restart local v21    # "videoStream":Lcom/google/android/videos/streams/MediaStream;
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 526
    .restart local v16    # "downloadedStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 528
    iget-object v5, v14, Lcom/google/android/videos/proto/DownloadExtra;->streamInfos:[Lcom/google/android/videos/proto/StreamInfo;

    const/4 v6, 0x1

    aget-object v12, v5, v6

    .line 529
    .local v12, "audioExtra":Lcom/google/android/videos/proto/StreamInfo;
    new-instance v13, Lcom/google/android/videos/streams/MediaStream;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/player/DirectorInitializer;->itagInfoStore:Lcom/google/android/videos/store/ItagInfoStore;

    iget v6, v12, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    invoke-virtual {v5, v6}, Lcom/google/android/videos/store/ItagInfoStore;->getItagInfo(I)Lcom/google/android/videos/streams/ItagInfo;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-direct {v13, v0, v5, v12}, Lcom/google/android/videos/streams/MediaStream;-><init>(Landroid/net/Uri;Lcom/google/android/videos/streams/ItagInfo;Lcom/google/android/videos/proto/StreamInfo;)V

    .line 531
    .local v13, "audioStream":Lcom/google/android/videos/streams/MediaStream;
    move-object/from16 v0, v16

    invoke-interface {v0, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3
.end method

.method private getOfflineFileSizeCallback(J)Lcom/google/android/videos/async/Callback;
    .locals 1
    .param p1, "expectedSize"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/io/File;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 923
    new-instance v0, Lcom/google/android/videos/player/DirectorInitializer$14;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/videos/player/DirectorInitializer$14;-><init>(Lcom/google/android/videos/player/DirectorInitializer;J)V

    return-object v0
.end method

.method private initAssetsCallback()Lcom/google/android/videos/async/Callback;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 875
    new-instance v0, Lcom/google/android/videos/player/DirectorInitializer$11;

    invoke-direct {v0, p0}, Lcom/google/android/videos/player/DirectorInitializer$11;-><init>(Lcom/google/android/videos/player/DirectorInitializer;)V

    return-object v0
.end method

.method private initOfflineStoryboardCallback()Lcom/google/android/videos/async/Callback;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/Storyboard;",
            ">;"
        }
    .end annotation

    .prologue
    .line 906
    new-instance v0, Lcom/google/android/videos/player/DirectorInitializer$13;

    invoke-direct {v0, p0}, Lcom/google/android/videos/player/DirectorInitializer$13;-><init>(Lcom/google/android/videos/player/DirectorInitializer;)V

    return-object v0
.end method

.method private initOfflineSubtitleTracksCallback()Lcom/google/android/videos/async/Callback;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 891
    new-instance v0, Lcom/google/android/videos/player/DirectorInitializer$12;

    invoke-direct {v0, p0}, Lcom/google/android/videos/player/DirectorInitializer$12;-><init>(Lcom/google/android/videos/player/DirectorInitializer;)V

    return-object v0
.end method

.method private initStoredPurchaseCallback()Lcom/google/android/videos/async/Callback;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 827
    new-instance v0, Lcom/google/android/videos/player/DirectorInitializer$8;

    invoke-direct {v0, p0}, Lcom/google/android/videos/player/DirectorInitializer$8;-><init>(Lcom/google/android/videos/player/DirectorInitializer;)V

    return-object v0
.end method

.method private initStreamsCallback()Lcom/google/android/videos/async/Callback;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/api/MpdGetRequest;",
            "Lcom/google/android/videos/streams/Streams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 842
    new-instance v0, Lcom/google/android/videos/player/DirectorInitializer$9;

    invoke-direct {v0, p0}, Lcom/google/android/videos/player/DirectorInitializer$9;-><init>(Lcom/google/android/videos/player/DirectorInitializer;)V

    return-object v0
.end method

.method private initSyncPurchaseCallback()Lcom/google/android/videos/async/Callback;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 798
    new-instance v0, Lcom/google/android/videos/player/DirectorInitializer$6;

    invoke-direct {v0, p0}, Lcom/google/android/videos/player/DirectorInitializer$6;-><init>(Lcom/google/android/videos/player/DirectorInitializer;)V

    .line 810
    .local v0, "freshPurchaseCallback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;>;"
    new-instance v1, Lcom/google/android/videos/player/DirectorInitializer$7;

    invoke-direct {v1, p0, v0}, Lcom/google/android/videos/player/DirectorInitializer$7;-><init>(Lcom/google/android/videos/player/DirectorInitializer;Lcom/google/android/videos/async/Callback;)V

    return-object v1
.end method

.method private initVideoGetCallback()Lcom/google/android/videos/async/Callback;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/api/VideoGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 859
    new-instance v0, Lcom/google/android/videos/player/DirectorInitializer$10;

    invoke-direct {v0, p0}, Lcom/google/android/videos/player/DirectorInitializer$10;-><init>(Lcom/google/android/videos/player/DirectorInitializer;)V

    return-object v0
.end method

.method private static isDownloadStarted(Landroid/database/Cursor;)Z
    .locals 12
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    const-wide/16 v10, 0x0

    const/4 v8, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 447
    sget v5, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->LICENSE_TYPE:I

    invoke-static {p0, v5, v4}, Lcom/google/android/videos/utils/DbUtils;->getIntOrDefault(Landroid/database/Cursor;II)I

    move-result v2

    .line 449
    .local v2, "licenseType":I
    new-array v5, v8, [I

    sget v6, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->DOWNLOAD_RELATIVE_FILEPATH:I

    aput v6, v5, v4

    sget v6, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->DOWNLOAD_SIZE:I

    aput v6, v5, v3

    invoke-static {p0, v5}, Lcom/google/android/videos/utils/DbUtils;->isAnyNull(Landroid/database/Cursor;[I)Z

    move-result v5

    if-nez v5, :cond_2

    move v1, v3

    .line 452
    .local v1, "havePathAndSize":Z
    :goto_0
    sget v5, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->DOWNLOAD_BYTES_DOWNLOADED:I

    invoke-static {p0, v5, v10, v11}, Lcom/google/android/videos/utils/DbUtils;->getLongOrDefault(Landroid/database/Cursor;IJ)J

    move-result-wide v6

    cmp-long v5, v6, v10

    if-lez v5, :cond_3

    move v0, v3

    .line 455
    .local v0, "haveContent":Z
    :goto_1
    if-eqz v1, :cond_4

    if-ne v2, v3, :cond_0

    if-nez v0, :cond_1

    :cond_0
    if-ne v2, v8, :cond_4

    :cond_1
    :goto_2
    return v3

    .end local v0    # "haveContent":Z
    .end local v1    # "havePathAndSize":Z
    :cond_2
    move v1, v4

    .line 449
    goto :goto_0

    .restart local v1    # "havePathAndSize":Z
    :cond_3
    move v0, v4

    .line 452
    goto :goto_1

    .restart local v0    # "haveContent":Z
    :cond_4
    move v3, v4

    .line 455
    goto :goto_2
.end method

.method private maybePreemptDrm(Landroid/database/Cursor;)V
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v4, 0x0

    .line 408
    iget-object v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->offlineMediaStreams:Ljava/util/List;

    if-eqz v5, :cond_1

    const/4 v0, 0x1

    .line 409
    .local v0, "isOffline":Z
    :goto_0
    if-eqz v0, :cond_2

    iget-object v5, p0, Lcom/google/android/videos/player/DirectorInitializer;->offlineMediaStreams:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/videos/streams/MediaStream;

    iget-object v4, v4, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    iget-boolean v3, v4, Lcom/google/android/videos/streams/ItagInfo;->isDash:Z

    .line 410
    .local v3, "useDash":Z
    :goto_1
    iget-boolean v4, p0, Lcom/google/android/videos/player/DirectorInitializer;->showShortClockDialog:Z

    if-nez v4, :cond_0

    iget-boolean v4, p0, Lcom/google/android/videos/player/DirectorInitializer;->isRemotePlayback:Z

    if-nez v4, :cond_0

    if-nez v3, :cond_3

    .line 425
    :cond_0
    :goto_2
    return-void

    .end local v0    # "isOffline":Z
    .end local v3    # "useDash":Z
    :cond_1
    move v0, v4

    .line 408
    goto :goto_0

    .line 409
    .restart local v0    # "isOffline":Z
    :cond_2
    iget-boolean v3, p0, Lcom/google/android/videos/player/DirectorInitializer;->useDashStreams:Z

    goto :goto_1

    .line 413
    .restart local v3    # "useDash":Z
    :cond_3
    if-eqz v0, :cond_4

    const/16 v4, 0xf

    :goto_3
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 415
    .local v1, "mimeType":Ljava/lang/String;
    if-eqz v0, :cond_5

    const/16 v4, 0xe

    :goto_4
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    .line 417
    .local v2, "psshData":[B
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 418
    iget-object v4, p0, Lcom/google/android/videos/player/DirectorInitializer;->handler:Landroid/os/Handler;

    new-instance v5, Lcom/google/android/videos/player/DirectorInitializer$2;

    invoke-direct {v5, p0, v2, v1, v0}, Lcom/google/android/videos/player/DirectorInitializer$2;-><init>(Lcom/google/android/videos/player/DirectorInitializer;[BLjava/lang/String;Z)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_2

    .line 413
    .end local v1    # "mimeType":Ljava/lang/String;
    .end local v2    # "psshData":[B
    :cond_4
    const/16 v4, 0x11

    goto :goto_3

    .line 415
    .restart local v1    # "mimeType":Ljava/lang/String;
    :cond_5
    const/16 v4, 0x10

    goto :goto_4
.end method

.method private onAllTasksCompleted()V
    .locals 4

    .prologue
    .line 720
    iget v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->initializationErrorLevel:I

    if-eqz v0, :cond_0

    .line 721
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/videos/player/DirectorInitializer$4;

    invoke-direct {v1, p0}, Lcom/google/android/videos/player/DirectorInitializer$4;-><init>(Lcom/google/android/videos/player/DirectorInitializer;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 751
    :goto_0
    return-void

    .line 731
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    invoke-virtual {v0}, Lcom/google/android/videos/player/PlaybackResumeState;->hasResumeTime()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 732
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    invoke-virtual {v0}, Lcom/google/android/videos/player/PlaybackResumeState;->getResumeTimeMillis()I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->resumeTimeMillis:I

    .line 738
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->streams:Lcom/google/android/videos/streams/Streams;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->offlineMediaStreams:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 739
    new-instance v0, Lcom/google/android/videos/streams/Streams;

    iget-object v1, p0, Lcom/google/android/videos/player/DirectorInitializer;->offlineMediaStreams:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/videos/player/DirectorInitializer;->offlineCaptionTracks:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/videos/player/DirectorInitializer;->offlineStoryboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/videos/streams/Streams;-><init>(Ljava/util/List;Ljava/util/List;[Lcom/google/wireless/android/video/magma/proto/Storyboard;)V

    iput-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->streams:Lcom/google/android/videos/streams/Streams;

    .line 742
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/videos/player/DirectorInitializer$5;

    invoke-direct {v1, p0}, Lcom/google/android/videos/player/DirectorInitializer$5;-><init>(Lcom/google/android/videos/player/DirectorInitializer;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 733
    :cond_3
    iget v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->resumeTimeMillis:I

    iget v1, p0, Lcom/google/android/videos/player/DirectorInitializer;->durationMillis:I

    iget v2, p0, Lcom/google/android/videos/player/DirectorInitializer;->startOfCreditSec:I

    invoke-static {v0, v1, v2}, Lcom/google/android/videos/utils/Util;->isAtEndOfMovie(III)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 734
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->resumeTimeMillis:I

    goto :goto_1
.end method

.method private declared-synchronized onGetVideoResource(Lcom/google/wireless/android/video/magma/proto/VideoResource;)V
    .locals 4
    .param p1, "videoResource"    # Lcom/google/wireless/android/video/magma/proto/VideoResource;

    .prologue
    .line 578
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->released:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 588
    :goto_0
    monitor-exit p0

    return-void

    .line 582
    :cond_0
    :try_start_1
    iget-object v0, p1, Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    iget-wide v0, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->stopTimestampMsec:J

    iget-wide v2, p0, Lcom/google/android/videos/player/DirectorInitializer;->lastWatchedTimestamp:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 584
    iget-object v0, p1, Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    iget-wide v0, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->positionMsec:J

    long-to-int v0, v0

    iput v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->resumeTimeMillis:I

    .line 585
    iget-object v0, p1, Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    iget-wide v0, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->stopTimestampMsec:J

    iput-wide v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->lastWatchedTimestamp:J

    .line 587
    :cond_1
    invoke-direct {p0}, Lcom/google/android/videos/player/DirectorInitializer;->onTaskCompleted()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 578
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private onInitializationError(ILjava/lang/Exception;Z)V
    .locals 7
    .param p1, "errorLevel"    # I
    .param p2, "exception"    # Ljava/lang/Exception;
    .param p3, "couldBeNoNetwork"    # Z

    .prologue
    const/4 v2, 0x1

    .line 681
    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 682
    const v4, 0x7f0b017b

    move-object v0, p0

    move v1, p1

    move v3, v2

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/player/DirectorInitializer;->onInitializationError(IZIILjava/lang/Exception;)V

    .line 688
    :goto_0
    return-void

    .line 685
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->errorHelper:Lcom/google/android/videos/utils/ErrorHelper;

    invoke-virtual {v0, p2}, Lcom/google/android/videos/utils/ErrorHelper;->getHumanizedRepresentation(Ljava/lang/Throwable;)Landroid/util/Pair;

    move-result-object v6

    .line 686
    .local v6, "humanized":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v0, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v4, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    move-object v0, p0

    move v1, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/player/DirectorInitializer;->onInitializationError(IZILjava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private onInitializationError(IZIILjava/lang/Exception;)V
    .locals 6
    .param p1, "errorLevel"    # I
    .param p2, "canRetry"    # Z
    .param p3, "errorType"    # I
    .param p4, "errorMessageId"    # I
    .param p5, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 692
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->activity:Landroid/app/Activity;

    invoke-virtual {v0, p4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/player/DirectorInitializer;->onInitializationError(IZILjava/lang/String;Ljava/lang/Exception;)V

    .line 694
    return-void
.end method

.method private declared-synchronized onInitializationError(IZILjava/lang/String;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "errorLevel"    # I
    .param p2, "canRetry"    # Z
    .param p3, "errorType"    # I
    .param p4, "errorMessage"    # Ljava/lang/String;
    .param p5, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 698
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->initializationErrorLevel:I

    if-le p1, v0, :cond_0

    .line 699
    iput p1, p0, Lcom/google/android/videos/player/DirectorInitializer;->initializationErrorLevel:I

    .line 700
    iput-boolean p2, p0, Lcom/google/android/videos/player/DirectorInitializer;->initializationErrorCanRetry:Z

    .line 701
    iput p3, p0, Lcom/google/android/videos/player/DirectorInitializer;->initializationErrorType:I

    .line 702
    iput-object p4, p0, Lcom/google/android/videos/player/DirectorInitializer;->initializationErrorMessage:Ljava/lang/String;

    .line 703
    iput-object p5, p0, Lcom/google/android/videos/player/DirectorInitializer;->initializationError:Ljava/lang/Exception;

    .line 705
    :cond_0
    invoke-direct {p0}, Lcom/google/android/videos/player/DirectorInitializer;->onTaskCompleted()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 706
    monitor-exit p0

    return-void

    .line 698
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized onNoOfflineStream()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 538
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/videos/player/DirectorInitializer;->released:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 575
    :goto_0
    monitor-exit p0

    return-void

    .line 542
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/videos/player/DirectorInitializer;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    invoke-virtual {v1}, Lcom/google/android/videos/player/PlaybackResumeState;->hasResumeTime()Z

    move-result v1

    if-nez v1, :cond_1

    .line 544
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Getting video userdata for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/player/DirectorInitializer;->videoId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 545
    iget-object v1, p0, Lcom/google/android/videos/player/DirectorInitializer;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->recordTaskStart(I)V

    .line 546
    iget-object v1, p0, Lcom/google/android/videos/player/DirectorInitializer;->videoGetRequester:Lcom/google/android/videos/async/Requester;

    new-instance v2, Lcom/google/android/videos/api/VideoGetRequest;

    iget-object v3, p0, Lcom/google/android/videos/player/DirectorInitializer;->account:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/videos/player/DirectorInitializer;->videoId:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/google/android/videos/api/VideoGetRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/videos/player/DirectorInitializer;->videoGetCallback:Lcom/google/android/videos/async/Callback;

    invoke-direct {p0, v3}, Lcom/google/android/videos/player/DirectorInitializer;->decorateCallback(Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/Callback;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 550
    :cond_1
    iget v1, p0, Lcom/google/android/videos/player/DirectorInitializer;->purchaseStatus:I

    if-eq v1, v5, :cond_2

    .line 554
    iget-object v1, p0, Lcom/google/android/videos/player/DirectorInitializer;->syncPurchasesCallback:Lcom/google/android/videos/async/Callback;

    invoke-direct {p0, v1}, Lcom/google/android/videos/player/DirectorInitializer;->decorateCallback(Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/Callback;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/NewCallback;

    .line 556
    .local v0, "decoratedSyncPurchasesCallback":Lcom/google/android/videos/async/NewCallback;, "Lcom/google/android/videos/async/NewCallback<Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Ljava/lang/Void;>;"
    new-instance v1, Lcom/google/android/videos/player/DirectorInitializer$3;

    invoke-direct {v1, p0, v0}, Lcom/google/android/videos/player/DirectorInitializer$3;-><init>(Lcom/google/android/videos/player/DirectorInitializer;Lcom/google/android/videos/async/NewCallback;)V

    invoke-direct {p0, v1}, Lcom/google/android/videos/player/DirectorInitializer;->checkAuthentication(Lcom/google/android/videos/player/DirectorInitializer$DirectorAuthenticatee;)V

    .line 568
    .end local v0    # "decoratedSyncPurchasesCallback":Lcom/google/android/videos/async/NewCallback;, "Lcom/google/android/videos/async/NewCallback<Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Ljava/lang/Void;>;"
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/videos/player/DirectorInitializer;->isRemotePlayback:Z

    if-nez v1, :cond_3

    .line 569
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Getting mpd of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/player/DirectorInitializer;->videoId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 570
    iget-object v1, p0, Lcom/google/android/videos/player/DirectorInitializer;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->recordTaskStart(I)V

    .line 571
    iget-object v1, p0, Lcom/google/android/videos/player/DirectorInitializer;->streamsRequester:Lcom/google/android/videos/async/Requester;

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/google/android/videos/player/DirectorInitializer;->generateStreamsRequest(Z)Lcom/google/android/videos/api/MpdGetRequest;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/player/DirectorInitializer;->streamsCallback:Lcom/google/android/videos/async/Callback;

    invoke-direct {p0, v3}, Lcom/google/android/videos/player/DirectorInitializer;->decorateCallback(Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/Callback;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 574
    :cond_3
    invoke-direct {p0}, Lcom/google/android/videos/player/DirectorInitializer;->onTaskCompleted()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 538
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private onNotAuthenticated()V
    .locals 1

    .prologue
    .line 666
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->listener:Lcom/google/android/videos/player/DirectorInitializer$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/player/DirectorInitializer$Listener;->onNotAuthenticated()V

    .line 667
    return-void
.end method

.method private declared-synchronized onStoredPurchaseCursor(Landroid/database/Cursor;)V
    .locals 12
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v2, 0x2

    .line 322
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->released:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 405
    :goto_0
    monitor-exit p0

    return-void

    .line 327
    :cond_0
    :try_start_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 328
    invoke-direct {p0}, Lcom/google/android/videos/player/DirectorInitializer;->onNoOfflineStream()V
    :try_end_1
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 403
    :try_start_2
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 322
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 331
    :cond_1
    :try_start_3
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/DirectorInitializer;->readCursor(Landroid/database/Cursor;)V

    .line 332
    sget v0, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->LAST_WATCHED_TIMESTAMP:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->lastWatchedTimestamp:J

    .line 333
    sget v0, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->RESUME_TIMESTAMP:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->resumeTimeMillis:I

    .line 334
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->purchaseStatus:I

    .line 335
    iget v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->purchaseStatus:I

    if-eq v0, v2, :cond_2

    .line 336
    invoke-direct {p0}, Lcom/google/android/videos/player/DirectorInitializer;->onNoOfflineStream()V
    :try_end_3
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 403
    :try_start_4
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 340
    :cond_2
    :try_start_5
    sget v0, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->IS_PINNED:I

    invoke-static {p1, v0}, Lcom/google/android/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 342
    invoke-direct {p0}, Lcom/google/android/videos/player/DirectorInitializer;->onNoOfflineStream()V

    .line 344
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/DirectorInitializer;->maybePreemptDrm(Landroid/database/Cursor;)V
    :try_end_5
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 403
    :try_start_6
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    .line 348
    :cond_3
    :try_start_7
    sget v0, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->STORAGE_INDEX:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->pinnedStorage:I

    .line 352
    iget-boolean v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->isRemotePlayback:Z

    if-eqz v0, :cond_4

    .line 353
    invoke-direct {p0}, Lcom/google/android/videos/player/DirectorInitializer;->onNoOfflineStream()V
    :try_end_7
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 403
    :try_start_8
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_0

    .line 361
    :cond_4
    :try_start_9
    invoke-static {p1}, Lcom/google/android/videos/player/DirectorInitializer;->isDownloadStarted(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 362
    const/4 v1, 0x2

    const/4 v2, 0x1

    const/16 v3, 0xd

    const v4, 0x7f0b0163

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/player/DirectorInitializer;->onInitializationError(IZIILjava/lang/Exception;)V
    :try_end_9
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 403
    :try_start_a
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto :goto_0

    .line 367
    :cond_5
    :try_start_b
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/DirectorInitializer;->getDownloadPath(Landroid/database/Cursor;)Ljava/io/File;
    :try_end_b
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    move-result-object v8

    .line 368
    .local v8, "downloadPath":Ljava/io/File;
    if-nez v8, :cond_6

    .line 403
    :try_start_c
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto :goto_0

    .line 372
    :cond_6
    :try_start_d
    sget v0, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->DOWNLOAD_SIZE:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 373
    .local v10, "size":J
    invoke-direct {p0, p1, v8, v10, v11}, Lcom/google/android/videos/player/DirectorInitializer;->getDownloadedStreams(Landroid/database/Cursor;Ljava/io/File;J)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->offlineMediaStreams:Ljava/util/List;

    .line 375
    sget v0, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->LICENSE_CENC_KEY_SET_ID:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->cencKeySetId:[B

    .line 376
    sget v0, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->LICENSE_CENC_SECURITY_LEVEL:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->cencSecurityLevel:I

    .line 377
    new-instance v1, Lcom/google/android/videos/drm/DrmManager$Identifiers;

    sget v0, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->LICENSE_LEGACY_KEY_ID:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    sget v0, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->LICENSE_LEGACY_ASSET_ID:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    sget v0, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->LICENSE_LEGACY_SYSTEM_ID:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-direct/range {v1 .. v7}, Lcom/google/android/videos/drm/DrmManager$Identifiers;-><init>(JJJ)V

    iput-object v1, p0, Lcom/google/android/videos/player/DirectorInitializer;->drmIdentifiers:Lcom/google/android/videos/drm/DrmManager$Identifiers;

    .line 382
    sget v0, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->IS_DOWNLOAD_COMPLETED:I

    invoke-static {p1, v0}, Lcom/google/android/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 385
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->localExecutor:Ljava/util/concurrent/Executor;

    invoke-direct {p0, v10, v11}, Lcom/google/android/videos/player/DirectorInitializer;->getOfflineFileSizeCallback(J)Lcom/google/android/videos/async/Callback;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/videos/player/DirectorInitializer;->decorateCallback(Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/Callback;

    move-result-object v1

    invoke-static {v0, v8, v1}, Lcom/google/android/videos/utils/OfflineUtil;->getRecursiveFileSizeAsync(Ljava/util/concurrent/Executor;Ljava/io/File;Lcom/google/android/videos/async/Callback;)V

    .line 389
    :cond_7
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->subtitlesClient:Lcom/google/android/videos/store/SubtitlesClient;

    iget-object v1, p0, Lcom/google/android/videos/player/DirectorInitializer;->videoId:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/videos/player/DirectorInitializer;->pinnedStorage:I

    iget-object v3, p0, Lcom/google/android/videos/player/DirectorInitializer;->offlineSubtitleTracksCallback:Lcom/google/android/videos/async/Callback;

    invoke-direct {p0, v3}, Lcom/google/android/videos/player/DirectorInitializer;->decorateCallback(Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/Callback;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/videos/store/SubtitlesClient;->requestOfflineSubtitleTracks(Ljava/lang/String;ILcom/google/android/videos/async/Callback;)V

    .line 392
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->storyboardClient:Lcom/google/android/videos/store/StoryboardClient;

    iget-object v1, p0, Lcom/google/android/videos/player/DirectorInitializer;->videoId:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/videos/player/DirectorInitializer;->pinnedStorage:I

    iget-object v3, p0, Lcom/google/android/videos/player/DirectorInitializer;->offlineStoryboardCallback:Lcom/google/android/videos/async/Callback;

    invoke-direct {p0, v3}, Lcom/google/android/videos/player/DirectorInitializer;->decorateCallback(Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/Callback;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/videos/store/StoryboardClient;->requestOfflineStoryboard(Ljava/lang/String;ILcom/google/android/videos/async/Callback;)V

    .line 395
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/DirectorInitializer;->maybePreemptDrm(Landroid/database/Cursor;)V

    .line 397
    invoke-direct {p0}, Lcom/google/android/videos/player/DirectorInitializer;->onTaskCompleted()V
    :try_end_d
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_d .. :try_end_d} :catch_0
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 403
    :try_start_e
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto/16 :goto_0

    .line 398
    .end local v8    # "downloadPath":Ljava/io/File;
    .end local v10    # "size":J
    :catch_0
    move-exception v5

    .line 399
    .local v5, "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    :try_start_f
    const-string v0, "Failed to deserialize proto"

    invoke-static {v0, v5}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 400
    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x3

    const v4, 0x7f0b0146

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/player/DirectorInitializer;->onInitializationError(IZIILjava/lang/Exception;)V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 403
    :try_start_10
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .end local v5    # "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    :catchall_1
    move-exception v0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0
.end method

.method private onStreamsError(Ljava/lang/Exception;)V
    .locals 6
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    const/4 v1, 0x1

    .line 670
    instance-of v0, p1, Lcom/google/android/videos/streams/MissingStreamException;

    if-eqz v0, :cond_0

    .line 671
    const/4 v2, 0x0

    const/4 v3, 0x6

    const v4, 0x7f0b01f4

    move-object v0, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/player/DirectorInitializer;->onInitializationError(IZIILjava/lang/Exception;)V

    .line 677
    :goto_0
    return-void

    .line 675
    :cond_0
    invoke-direct {p0, v1, p1, v1}, Lcom/google/android/videos/player/DirectorInitializer;->onInitializationError(ILjava/lang/Exception;Z)V

    goto :goto_0
.end method

.method private onSyncedPurchaseCursor(Landroid/database/Cursor;)V
    .locals 7
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v1, 0x2

    .line 592
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_0

    .line 593
    const/4 v1, 0x2

    const/4 v2, 0x0

    const/16 v3, 0xc

    const v4, 0x7f0b014a

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/player/DirectorInitializer;->onInitializationError(IZIILjava/lang/Exception;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 611
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 613
    :goto_0
    return-void

    .line 597
    :cond_0
    :try_start_1
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/DirectorInitializer;->readCursor(Landroid/database/Cursor;)V

    .line 598
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 599
    .local v6, "status":I
    if-ne v6, v1, :cond_1

    .line 600
    invoke-direct {p0}, Lcom/google/android/videos/player/DirectorInitializer;->onTaskCompleted()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 611
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 601
    :cond_1
    const/4 v0, 0x6

    if-ne v6, v0, :cond_2

    .line 602
    const/4 v1, 0x2

    const/4 v2, 0x1

    const/16 v3, 0x16

    const v4, 0x7f0b0156

    const/4 v5, 0x0

    move-object v0, p0

    :try_start_2
    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/player/DirectorInitializer;->onInitializationError(IZIILjava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 611
    .end local v6    # "status":I
    :catchall_0
    move-exception v0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 606
    .restart local v6    # "status":I
    :cond_2
    const/4 v1, 0x2

    const/4 v2, 0x1

    const/16 v3, 0xa

    const v4, 0x7f0b0155

    const/4 v5, 0x0

    move-object v0, p0

    :try_start_3
    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/player/DirectorInitializer;->onInitializationError(IZIILjava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method private declared-synchronized onTaskCompleted()V
    .locals 1

    .prologue
    .line 711
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->released:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->pendingInitializationTasks:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->pendingInitializationTasks:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v0, :cond_1

    .line 715
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 714
    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/videos/player/DirectorInitializer;->onAllTasksCompleted()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 711
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private onTrailerAssetsResponse(Lcom/google/wireless/android/video/magma/proto/AssetListResponse;)V
    .locals 8
    .param p1, "assetList"    # Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    .prologue
    const/4 v2, 0x0

    .line 284
    iget-object v0, p1, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    array-length v0, v0

    if-nez v0, :cond_0

    .line 285
    const/4 v1, 0x2

    const/16 v3, 0xe

    const v4, 0x7f0b01f5

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/player/DirectorInitializer;->onInitializationError(IZIILjava/lang/Exception;)V

    .line 318
    :goto_0
    return-void

    .line 289
    :cond_0
    iget-object v0, p1, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    aget-object v0, v0, v2

    iget-object v7, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    .line 290
    .local v7, "metadata":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    iget-object v0, v7, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->videoTitle:Ljava/lang/String;

    .line 291
    iget v0, v7, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->durationSec:I

    mul-int/lit16 v0, v0, 0x3e8

    iput v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->durationMillis:I

    .line 292
    iget v0, v7, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->startOfCreditSec:I

    iput v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->startOfCreditSec:I

    .line 293
    iget-boolean v0, v7, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasCaption:Z

    if-eqz v0, :cond_1

    .line 294
    iget v0, v7, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->captionMode:I

    iput v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->subtitleMode:I

    .line 295
    iget-object v0, v7, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->captionDefaultLanguage:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->subtitleDefaultLanguage:Ljava/lang/String;

    .line 297
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->isRemotePlayback:Z

    if-nez v0, :cond_2

    .line 298
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->videoId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/videos/player/DirectorInitializer;->config:Lcom/google/android/videos/Config;

    invoke-interface {v1}, Lcom/google/android/videos/Config;->soundWelcomeVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 299
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Getting mpd of "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/player/DirectorInitializer;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 300
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->recordTaskStart(I)V

    .line 301
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->streamsRequester:Lcom/google/android/videos/async/Requester;

    invoke-direct {p0, v2}, Lcom/google/android/videos/player/DirectorInitializer;->generateStreamsRequest(Z)Lcom/google/android/videos/api/MpdGetRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/player/DirectorInitializer;->streamsCallback:Lcom/google/android/videos/async/Callback;

    invoke-direct {p0, v2}, Lcom/google/android/videos/player/DirectorInitializer;->decorateCallback(Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/Callback;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 317
    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/google/android/videos/player/DirectorInitializer;->onTaskCompleted()V

    goto :goto_0

    .line 305
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer;->streamsCallback:Lcom/google/android/videos/async/Callback;

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/DirectorInitializer;->decorateCallback(Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/Callback;

    move-result-object v6

    .line 307
    .local v6, "decoratedStreamsCallback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/android/videos/streams/Streams;>;"
    new-instance v0, Lcom/google/android/videos/player/DirectorInitializer$1;

    invoke-direct {v0, p0, v6}, Lcom/google/android/videos/player/DirectorInitializer$1;-><init>(Lcom/google/android/videos/player/DirectorInitializer;Lcom/google/android/videos/async/Callback;)V

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/DirectorInitializer;->checkAuthentication(Lcom/google/android/videos/player/DirectorInitializer$DirectorAuthenticatee;)V

    goto :goto_1
.end method

.method private declared-synchronized readCursor(Landroid/database/Cursor;)V
    .locals 10
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const-wide/16 v6, -0x1

    const/4 v2, 0x1

    .line 616
    monitor-enter p0

    const/16 v3, 0xa

    :try_start_0
    invoke-static {p1, v3}, Lcom/google/android/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/videos/player/DirectorInitializer;->isRental:Z

    .line 617
    const/4 v3, 0x4

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    mul-int/lit16 v3, v3, 0x3e8

    iput v3, p0, Lcom/google/android/videos/player/DirectorInitializer;->durationMillis:I

    .line 618
    const/16 v3, 0xb

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, p0, Lcom/google/android/videos/player/DirectorInitializer;->startOfCreditSec:I

    .line 619
    const/4 v3, 0x2

    invoke-interface {p1, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_2

    move-wide v4, v6

    :goto_0
    iput-wide v4, p0, Lcom/google/android/videos/player/DirectorInitializer;->shortClockMillis:J

    .line 623
    iget-wide v4, p0, Lcom/google/android/videos/player/DirectorInitializer;->shortClockMillis:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v4, v6

    iget-wide v6, p0, Lcom/google/android/videos/player/DirectorInitializer;->shortClockMillis:J

    cmp-long v3, v4, v6

    if-lez v3, :cond_3

    :goto_1
    iput-boolean v2, p0, Lcom/google/android/videos/player/DirectorInitializer;->showShortClockDialog:Z

    .line 626
    const/4 v2, 0x3

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/player/DirectorInitializer;->videoTitle:Ljava/lang/String;

    .line 627
    const/16 v2, 0x9

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/player/DirectorInitializer;->showTitle:Ljava/lang/String;

    .line 629
    const/16 v2, 0xc

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/player/DirectorInitializer;->posterUri:Ljava/lang/String;

    .line 630
    iget-object v2, p0, Lcom/google/android/videos/player/DirectorInitializer;->posterUri:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 631
    const/16 v2, 0xd

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/player/DirectorInitializer;->posterUri:Ljava/lang/String;

    .line 634
    :cond_0
    const/4 v2, 0x5

    invoke-static {p1, v2}, Lcom/google/android/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v0

    .line 635
    .local v0, "hasSubtitles":Z
    if-eqz v0, :cond_1

    .line 636
    const/16 v2, 0x8

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/videos/player/DirectorInitializer;->subtitleMode:I

    .line 637
    const/4 v2, 0x6

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/player/DirectorInitializer;->subtitleDefaultLanguage:Ljava/lang/String;

    .line 639
    :cond_1
    const/4 v2, 0x7

    invoke-static {p1, v2}, Lcom/google/android/videos/utils/DbUtils;->getStringList(Landroid/database/Cursor;I)Ljava/util/List;

    move-result-object v1

    .line 640
    .local v1, "languages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 643
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/videos/player/DirectorInitializer;->haveAudioInDeviceLanguage:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 647
    :goto_2
    monitor-exit p0

    return-void

    .line 619
    .end local v0    # "hasSubtitles":Z
    .end local v1    # "languages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    const/4 v3, 0x2

    :try_start_1
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const-wide/16 v8, 0x3e8

    mul-long/2addr v4, v8

    goto :goto_0

    .line 623
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 645
    .restart local v0    # "hasSubtitles":Z
    .restart local v1    # "languages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_4
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/player/DirectorInitializer;->containsLanguage(Ljava/util/List;Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/videos/player/DirectorInitializer;->haveAudioInDeviceLanguage:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 616
    .end local v0    # "hasSubtitles":Z
    .end local v1    # "languages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method


# virtual methods
.method public declared-synchronized release()V
    .locals 4

    .prologue
    .line 428
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/videos/player/DirectorInitializer;->syncTaskControl:Lcom/google/android/videos/async/TaskControl;

    invoke-static {v2}, Lcom/google/android/videos/async/TaskControl;->cancel(Lcom/google/android/videos/async/TaskControl;)V

    .line 429
    iget-object v2, p0, Lcom/google/android/videos/player/DirectorInitializer;->cancelableCallbacks:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/CancelableCallback;

    .line 430
    .local v0, "callback":Lcom/google/android/videos/async/CancelableCallback;, "Lcom/google/android/videos/async/CancelableCallback<**>;"
    invoke-virtual {v0}, Lcom/google/android/videos/async/CancelableCallback;->cancel()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 428
    .end local v0    # "callback":Lcom/google/android/videos/async/CancelableCallback;, "Lcom/google/android/videos/async/CancelableCallback<**>;"
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 432
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/videos/player/DirectorInitializer;->cancelableCallbacks:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 433
    iget-object v2, p0, Lcom/google/android/videos/player/DirectorInitializer;->cancelableAuthenticatee:Lcom/google/android/videos/async/CancelableAuthenticatee;

    if-eqz v2, :cond_1

    .line 434
    iget-object v2, p0, Lcom/google/android/videos/player/DirectorInitializer;->cancelableAuthenticatee:Lcom/google/android/videos/async/CancelableAuthenticatee;

    invoke-virtual {v2}, Lcom/google/android/videos/async/CancelableAuthenticatee;->cancel()V

    .line 435
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/videos/player/DirectorInitializer;->cancelableAuthenticatee:Lcom/google/android/videos/async/CancelableAuthenticatee;

    .line 437
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/player/DirectorInitializer;->handler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 438
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/videos/player/DirectorInitializer;->released:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 439
    monitor-exit p0

    return-void
.end method
