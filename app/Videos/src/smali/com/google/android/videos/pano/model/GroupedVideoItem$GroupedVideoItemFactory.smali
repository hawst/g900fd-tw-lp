.class public final Lcom/google/android/videos/pano/model/GroupedVideoItem$GroupedVideoItemFactory;
.super Ljava/lang/Object;
.source "GroupedVideoItem.java"

# interfaces
.implements Lcom/google/android/repolib/common/Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/model/GroupedVideoItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GroupedVideoItemFactory"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Factory",
        "<",
        "Lcom/google/android/videos/pano/model/GroupedVideoItem;",
        "Lcom/google/android/videos/pano/model/VideoItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final groupId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "groupId"    # I

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput p2, p0, Lcom/google/android/videos/pano/model/GroupedVideoItem$GroupedVideoItemFactory;->groupId:I

    .line 38
    iput-object p1, p0, Lcom/google/android/videos/pano/model/GroupedVideoItem$GroupedVideoItemFactory;->context:Landroid/content/Context;

    .line 39
    return-void
.end method


# virtual methods
.method public createFrom(Lcom/google/android/videos/pano/model/VideoItem;)Lcom/google/android/videos/pano/model/GroupedVideoItem;
    .locals 3
    .param p1, "videoItem"    # Lcom/google/android/videos/pano/model/VideoItem;

    .prologue
    .line 44
    new-instance v1, Lcom/google/android/videos/pano/model/GroupedVideoItem;

    iget v0, p0, Lcom/google/android/videos/pano/model/GroupedVideoItem$GroupedVideoItemFactory;->groupId:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/pano/model/GroupedVideoItem$GroupedVideoItemFactory;->context:Landroid/content/Context;

    iget v2, p0, Lcom/google/android/videos/pano/model/GroupedVideoItem$GroupedVideoItemFactory;->groupId:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v2, 0x0

    invoke-direct {v1, p1, v0, v2}, Lcom/google/android/videos/pano/model/GroupedVideoItem;-><init>(Lcom/google/android/videos/pano/model/VideoItem;Ljava/lang/String;Lcom/google/android/videos/pano/model/GroupedVideoItem$1;)V

    return-object v1

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public bridge synthetic createFrom(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 30
    check-cast p1, Lcom/google/android/videos/pano/model/VideoItem;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/pano/model/GroupedVideoItem$GroupedVideoItemFactory;->createFrom(Lcom/google/android/videos/pano/model/VideoItem;)Lcom/google/android/videos/pano/model/GroupedVideoItem;

    move-result-object v0

    return-object v0
.end method
