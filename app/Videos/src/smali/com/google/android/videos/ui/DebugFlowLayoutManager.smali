.class public Lcom/google/android/videos/ui/DebugFlowLayoutManager;
.super Lcom/google/android/play/layout/FlowLayoutManager;
.source "DebugFlowLayoutManager.java"


# instance fields
.field private final debugInfo:Ljava/lang/String;

.field private flowAdapter:Lcom/google/android/videos/flow/FlowAdapter;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "debugInfo"    # Ljava/lang/String;

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/google/android/play/layout/FlowLayoutManager;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/google/android/videos/ui/DebugFlowLayoutManager;->debugInfo:Ljava/lang/String;

    .line 18
    return-void
.end method

.method private dumpDebugInfo(Ljava/lang/RuntimeException;)V
    .locals 2
    .param p1, "e"    # Ljava/lang/RuntimeException;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/videos/ui/DebugFlowLayoutManager;->debugInfo:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "FLM("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/ui/DebugFlowLayoutManager;->debugInfo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") encountered "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lcom/google/android/videos/ui/DebugFlowLayoutManager;->flowAdapter:Lcom/google/android/videos/flow/FlowAdapter;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/google/android/videos/ui/DebugFlowLayoutManager;->flowAdapter:Lcom/google/android/videos/flow/FlowAdapter;

    invoke-virtual {v0}, Lcom/google/android/videos/flow/FlowAdapter;->dumpDebugInfo()V

    .line 63
    :cond_0
    return-void
.end method


# virtual methods
.method public onAdapterChanged(Landroid/support/v7/widget/RecyclerView$Adapter;Landroid/support/v7/widget/RecyclerView$Adapter;)V
    .locals 2
    .param p1, "oldAdapter"    # Landroid/support/v7/widget/RecyclerView$Adapter;
    .param p2, "newAdapter"    # Landroid/support/v7/widget/RecyclerView$Adapter;

    .prologue
    const/4 v1, 0x0

    .line 23
    instance-of v0, p2, Lcom/google/android/videos/flow/FlowAdapter;

    if-eqz v0, :cond_0

    move-object v0, p2

    .line 24
    check-cast v0, Lcom/google/android/videos/flow/FlowAdapter;

    iput-object v0, p0, Lcom/google/android/videos/ui/DebugFlowLayoutManager;->flowAdapter:Lcom/google/android/videos/flow/FlowAdapter;

    .line 25
    iget-object v0, p0, Lcom/google/android/videos/ui/DebugFlowLayoutManager;->flowAdapter:Lcom/google/android/videos/flow/FlowAdapter;

    iget-object v1, p0, Lcom/google/android/videos/ui/DebugFlowLayoutManager;->debugInfo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/flow/FlowAdapter;->setUpDebugInfo(Ljava/lang/String;)V

    .line 32
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/google/android/play/layout/FlowLayoutManager;->onAdapterChanged(Landroid/support/v7/widget/RecyclerView$Adapter;Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 33
    return-void

    .line 27
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/DebugFlowLayoutManager;->flowAdapter:Lcom/google/android/videos/flow/FlowAdapter;

    if-eqz v0, :cond_1

    .line 28
    iget-object v0, p0, Lcom/google/android/videos/ui/DebugFlowLayoutManager;->flowAdapter:Lcom/google/android/videos/flow/FlowAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/flow/FlowAdapter;->setUpDebugInfo(Ljava/lang/String;)V

    .line 30
    :cond_1
    iput-object v1, p0, Lcom/google/android/videos/ui/DebugFlowLayoutManager;->flowAdapter:Lcom/google/android/videos/flow/FlowAdapter;

    goto :goto_0
.end method

.method public onLayoutChildren(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)V
    .locals 1
    .param p1, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;
    .param p2, "state"    # Landroid/support/v7/widget/RecyclerView$State;

    .prologue
    .line 38
    :try_start_0
    invoke-super {p0, p1, p2}, Lcom/google/android/play/layout/FlowLayoutManager;->onLayoutChildren(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    return-void

    .line 39
    :catch_0
    move-exception v0

    .line 40
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-direct {p0, v0}, Lcom/google/android/videos/ui/DebugFlowLayoutManager;->dumpDebugInfo(Ljava/lang/RuntimeException;)V

    .line 41
    throw v0
.end method

.method public scrollVerticallyBy(ILandroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)I
    .locals 2
    .param p1, "dy"    # I
    .param p2, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;
    .param p3, "state"    # Landroid/support/v7/widget/RecyclerView$State;

    .prologue
    .line 48
    :try_start_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/play/layout/FlowLayoutManager;->scrollVerticallyBy(ILandroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    return v1

    .line 49
    :catch_0
    move-exception v0

    .line 50
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-direct {p0, v0}, Lcom/google/android/videos/ui/DebugFlowLayoutManager;->dumpDebugInfo(Ljava/lang/RuntimeException;)V

    .line 51
    throw v0
.end method
