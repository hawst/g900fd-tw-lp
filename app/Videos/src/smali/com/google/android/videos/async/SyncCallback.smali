.class public Lcom/google/android/videos/async/SyncCallback;
.super Ljava/lang/Object;
.source "SyncCallback.java"

# interfaces
.implements Lcom/google/android/videos/async/NewCallback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/NewCallback",
        "<TR;TE;>;"
    }
.end annotation


# instance fields
.field private final conditionVar:Landroid/os/ConditionVariable;

.field private volatile exception:Ljava/lang/Exception;

.field private volatile isCancelled:Z

.field private volatile response:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>()V
    .locals 2

    .prologue
    .line 28
    .local p0, "this":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<TR;TE;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Landroid/os/ConditionVariable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/videos/async/SyncCallback;->conditionVar:Landroid/os/ConditionVariable;

    .line 30
    return-void
.end method

.method public static create()Lcom/google/android/videos/async/SyncCallback;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/google/android/videos/async/SyncCallback",
            "<TR;TE;>;"
        }
    .end annotation

    .prologue
    .line 25
    new-instance v0, Lcom/google/android/videos/async/SyncCallback;

    invoke-direct {v0}, Lcom/google/android/videos/async/SyncCallback;-><init>()V

    return-object v0
.end method


# virtual methods
.method public getResponse()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/concurrent/ExecutionException;,
            Ljava/util/concurrent/CancellationException;
        }
    .end annotation

    .prologue
    .line 54
    .local p0, "this":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<TR;TE;>;"
    const-wide/16 v2, 0x0

    :try_start_0
    invoke-virtual {p0, v2, v3}, Lcom/google/android/videos/async/SyncCallback;->getResponse(J)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 55
    :catch_0
    move-exception v0

    .line 56
    .local v0, "e":Ljava/util/concurrent/TimeoutException;
    new-instance v1, Ljava/util/concurrent/ExecutionException;

    const-string v2, "Timeout received when waiting forever"

    invoke-direct {v1, v2, v0}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public getResponse(J)Ljava/lang/Object;
    .locals 3
    .param p1, "timeoutMillis"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TE;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/concurrent/ExecutionException;,
            Ljava/util/concurrent/CancellationException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    .line 74
    .local p0, "this":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<TR;TE;>;"
    iget-object v1, p0, Lcom/google/android/videos/async/SyncCallback;->conditionVar:Landroid/os/ConditionVariable;

    invoke-virtual {v1, p1, p2}, Landroid/os/ConditionVariable;->block(J)Z

    move-result v0

    .line 75
    .local v0, "opened":Z
    if-nez v0, :cond_0

    .line 76
    new-instance v1, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v1}, Ljava/util/concurrent/TimeoutException;-><init>()V

    throw v1

    .line 78
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/videos/async/SyncCallback;->isCancelled:Z

    if-eqz v1, :cond_1

    .line 79
    new-instance v1, Ljava/util/concurrent/CancellationException;

    invoke-direct {v1}, Ljava/util/concurrent/CancellationException;-><init>()V

    throw v1

    .line 81
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/async/SyncCallback;->exception:Ljava/lang/Exception;

    if-eqz v1, :cond_2

    .line 82
    new-instance v1, Ljava/util/concurrent/ExecutionException;

    iget-object v2, p0, Lcom/google/android/videos/async/SyncCallback;->exception:Ljava/lang/Exception;

    invoke-direct {v1, v2}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 84
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/async/SyncCallback;->response:Ljava/lang/Object;

    return-object v1
.end method

.method public onCancelled(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p0, "this":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<TR;TE;>;"
    .local p1, "request":Ljava/lang/Object;, "TR;"
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/async/SyncCallback;->isCancelled:Z

    .line 49
    iget-object v0, p0, Lcom/google/android/videos/async/SyncCallback;->conditionVar:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 50
    return-void
.end method

.method public onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1
    .param p2, "exception"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    .prologue
    .line 41
    .local p0, "this":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<TR;TE;>;"
    .local p1, "request":Ljava/lang/Object;, "TR;"
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/async/SyncCallback;->response:Ljava/lang/Object;

    .line 42
    iput-object p2, p0, Lcom/google/android/videos/async/SyncCallback;->exception:Ljava/lang/Exception;

    .line 43
    iget-object v0, p0, Lcom/google/android/videos/async/SyncCallback;->conditionVar:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 44
    return-void
.end method

.method public onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;TE;)V"
        }
    .end annotation

    .prologue
    .line 34
    .local p0, "this":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<TR;TE;>;"
    .local p1, "req":Ljava/lang/Object;, "TR;"
    .local p2, "response":Ljava/lang/Object;, "TE;"
    iput-object p2, p0, Lcom/google/android/videos/async/SyncCallback;->response:Ljava/lang/Object;

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/async/SyncCallback;->exception:Ljava/lang/Exception;

    .line 36
    iget-object v0, p0, Lcom/google/android/videos/async/SyncCallback;->conditionVar:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 37
    return-void
.end method
