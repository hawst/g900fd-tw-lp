.class Lcom/google/android/videos/activity/AssetDetailsActivity$3;
.super Ljava/lang/Object;
.source "AssetDetailsActivity.java"

# interfaces
.implements Lcom/google/android/videos/ui/ImmersiveHeaderListLayout$ActionBarAlphaListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/activity/AssetDetailsActivity;->setUpActivityTransitionV21()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/activity/AssetDetailsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/videos/activity/AssetDetailsActivity;)V
    .locals 0

    .prologue
    .line 259
    iput-object p1, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$3;->this$0:Lcom/google/android/videos/activity/AssetDetailsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActionBarAlphaChanged(F)V
    .locals 6
    .param p1, "alpha"    # F

    .prologue
    .line 263
    iget-object v3, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$3;->this$0:Lcom/google/android/videos/activity/AssetDetailsActivity;

    invoke-virtual {v3}, Lcom/google/android/videos/activity/AssetDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00eb

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 264
    .local v1, "assetDetailsColor":I
    iget-object v3, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$3;->this$0:Lcom/google/android/videos/activity/AssetDetailsActivity;

    invoke-virtual {v3}, Lcom/google/android/videos/activity/AssetDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00ea

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 266
    .local v0, "appColor":I
    iget-object v3, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$3;->this$0:Lcom/google/android/videos/activity/AssetDetailsActivity;

    # getter for: Lcom/google/android/videos/activity/AssetDetailsActivity;->argbEvaluator:Landroid/animation/ArgbEvaluator;
    invoke-static {v3}, Lcom/google/android/videos/activity/AssetDetailsActivity;->access$000(Lcom/google/android/videos/activity/AssetDetailsActivity;)Landroid/animation/ArgbEvaluator;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, p1, v4, v5}, Landroid/animation/ArgbEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 267
    .local v2, "statusBarColor":I
    const/high16 v3, -0x1000000

    or-int/2addr v2, v3

    .line 269
    iget-object v3, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$3;->this$0:Lcom/google/android/videos/activity/AssetDetailsActivity;

    iget-object v3, v3, Lcom/google/android/videos/activity/AssetDetailsActivity;->playDrawerLayout:Lcom/google/android/videos/ui/StatusBarDrawerLayout;

    invoke-virtual {v3, v2}, Lcom/google/android/videos/ui/StatusBarDrawerLayout;->setStatusBarBackgroundColor(I)V

    .line 270
    return-void
.end method
