.class public Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;
.super Landroid/os/Handler;
.source "FullscreenUiVisibilityHelper.java"

# interfaces
.implements Landroid/view/View$OnSystemUiVisibilityChangeListener;
.implements Lcom/google/android/videos/player/PlayerView$SystemWindowInsetsListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper$Listener;
    }
.end annotation


# static fields
.field private static final BASE_FULLSCREEN_FLAGS:I

.field private static HIDDEN_FULLSCREEN_FLAGS:I

.field private static HIDE_SYSTEM_UI_FLAGS:I


# instance fields
.field private final actionBar:Landroid/support/v7/app/ActionBar;

.field private final actionBarHeight:I

.field private currentVisibilityFlags:I

.field private defaultWindowFullscreenSet:Z

.field private fullscreen:Z

.field private final hasActionBar:Z

.field private insets:Landroid/graphics/Rect;

.field private final listener:Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper$Listener;

.field private final playerView:Lcom/google/android/videos/player/PlayerView;

.field private released:Z

.field private systemUiHiddenInFullscreen:Z

.field private final window:Landroid/view/Window;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    invoke-static {}, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->getBaseSystemUiFlags()I

    move-result v0

    sput v0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->BASE_FULLSCREEN_FLAGS:I

    return-void
.end method

.method public constructor <init>(Landroid/view/Window;Landroid/support/v7/app/ActionBar;Lcom/google/android/videos/player/PlayerView;Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper$Listener;)V
    .locals 5
    .param p1, "window"    # Landroid/view/Window;
    .param p2, "actionBar"    # Landroid/support/v7/app/ActionBar;
    .param p3, "playerView"    # Lcom/google/android/videos/player/PlayerView;
    .param p4, "listener"    # Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper$Listener;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 61
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 63
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    sget v1, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->HIDE_SYSTEM_UI_FLAGS:I

    if-nez v1, :cond_0

    .line 66
    invoke-virtual {p3}, Lcom/google/android/videos/player/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->getHideSystemUiFlags(Landroid/content/Context;)I

    move-result v1

    sput v1, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->HIDE_SYSTEM_UI_FLAGS:I

    .line 67
    sget v1, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->BASE_FULLSCREEN_FLAGS:I

    sget v4, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->HIDE_SYSTEM_UI_FLAGS:I

    or-int/2addr v1, v4

    sput v1, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->HIDDEN_FULLSCREEN_FLAGS:I

    .line 70
    :cond_0
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/Window;

    iput-object v1, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->window:Landroid/view/Window;

    .line 71
    iput-object p3, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->playerView:Lcom/google/android/videos/player/PlayerView;

    .line 72
    iput-object p2, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->actionBar:Landroid/support/v7/app/ActionBar;

    .line 73
    iput-object p4, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->listener:Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper$Listener;

    .line 75
    invoke-virtual {p3, p0}, Lcom/google/android/videos/player/PlayerView;->setSystemWindowInsetsListener(Lcom/google/android/videos/player/PlayerView$SystemWindowInsetsListener;)V

    .line 76
    invoke-virtual {p3, p0}, Lcom/google/android/videos/player/PlayerView;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 77
    invoke-virtual {p3}, Lcom/google/android/videos/player/PlayerView;->getSystemUiVisibility()I

    move-result v1

    iput v1, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->currentVisibilityFlags:I

    .line 79
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Landroid/support/v7/app/ActionBar;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->hasActionBar:Z

    .line 83
    if-eqz p2, :cond_2

    .line 84
    invoke-virtual {p1}, Landroid/view/Window;->getContext()Landroid/content/Context;

    move-result-object v1

    new-array v2, v2, [I

    const v4, 0x7f010035

    aput v4, v2, v3

    invoke-virtual {v1, v2}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 86
    .local v0, "vals":Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->actionBarHeight:I

    .line 87
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 91
    .end local v0    # "vals":Landroid/content/res/TypedArray;
    :goto_1
    return-void

    :cond_1
    move v1, v3

    .line 79
    goto :goto_0

    .line 89
    :cond_2
    iput v3, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->actionBarHeight:I

    goto :goto_1
.end method

.method private applyDesiredFlags()V
    .locals 2

    .prologue
    .line 188
    invoke-direct {p0}, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->maybeScheduleFlagApplication()V

    .line 190
    iget-boolean v1, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->fullscreen:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->systemUiHiddenInFullscreen:Z

    if-eqz v1, :cond_0

    sget v0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->HIDDEN_FULLSCREEN_FLAGS:I

    .line 192
    .local v0, "desiredFlags":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->playerView:Lcom/google/android/videos/player/PlayerView;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/player/PlayerView;->setSystemUiVisibility(I)V

    .line 193
    return-void

    .line 190
    .end local v0    # "desiredFlags":I
    :cond_0
    sget v0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->BASE_FULLSCREEN_FLAGS:I

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getBaseSystemUiFlags()I
    .locals 3

    .prologue
    .line 215
    const/4 v0, 0x0

    .line 216
    .local v0, "baseFlags":I
    sget v1, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_0

    .line 217
    or-int/lit16 v0, v0, 0x700

    .line 221
    :cond_0
    return v0
.end method

.method private static getHideSystemUiFlags(Landroid/content/Context;)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/16 v3, 0x13

    .line 226
    const/4 v0, 0x3

    .line 227
    .local v0, "hideSystemUiFlags":I
    sget v1, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_0

    .line 228
    or-int/lit8 v0, v0, 0x4

    .line 230
    :cond_0
    sget v1, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    if-gt v1, v3, :cond_1

    sget v1, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    if-ne v1, v3, :cond_2

    invoke-static {p0}, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->useImmersiveFlagOnV19(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 231
    :cond_1
    or-int/lit16 v0, v0, 0x800

    .line 233
    :cond_2
    return v0
.end method

.method private maybeScheduleFlagApplication()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 172
    invoke-virtual {p0, v2}, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->removeMessages(I)V

    .line 173
    iget v0, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->currentVisibilityFlags:I

    invoke-direct {p0, v0}, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->shouldApplyVisibilityFlags(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174
    const-wide/16 v0, 0x9c4

    invoke-virtual {p0, v2, v0, v1}, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->sendEmptyMessageDelayed(IJ)Z

    .line 176
    :cond_0
    return-void
.end method

.method private shouldApplyVisibilityFlags(I)Z
    .locals 6
    .param p1, "visibility"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 179
    iget-boolean v4, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->fullscreen:Z

    iget-boolean v5, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->systemUiHiddenInFullscreen:Z

    and-int v1, v4, v5

    .line 180
    .local v1, "shouldBeHidden":Z
    sget v4, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->HIDE_SYSTEM_UI_FLAGS:I

    and-int/2addr v4, p1

    sget v5, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->HIDE_SYSTEM_UI_FLAGS:I

    if-ne v4, v5, :cond_0

    move v0, v2

    .line 181
    .local v0, "isHidden":Z
    :goto_0
    iget-boolean v4, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->released:Z

    if-nez v4, :cond_1

    if-eq v1, v0, :cond_1

    :goto_1
    return v2

    .end local v0    # "isHidden":Z
    :cond_0
    move v0, v3

    .line 180
    goto :goto_0

    .restart local v0    # "isHidden":Z
    :cond_1
    move v2, v3

    .line 181
    goto :goto_1
.end method

.method private updatePaddingForInsetViews()V
    .locals 10

    .prologue
    const/16 v9, 0x10

    const/4 v8, 0x0

    .line 196
    const/4 v1, 0x0

    .local v1, "i":I
    iget-object v4, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->playerView:Lcom/google/android/videos/player/PlayerView;

    invoke-virtual {v4}, Lcom/google/android/videos/player/PlayerView;->getChildCount()I

    move-result v2

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_5

    .line 197
    iget-object v4, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->playerView:Lcom/google/android/videos/player/PlayerView;

    invoke-virtual {v4, v1}, Lcom/google/android/videos/player/PlayerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 198
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/player/PlayerView$LayoutParams;

    .line 199
    .local v3, "params":Lcom/google/android/videos/player/PlayerView$LayoutParams;
    iget-boolean v4, v3, Lcom/google/android/videos/player/PlayerView$LayoutParams;->isInsetView:Z

    if-eqz v4, :cond_1

    .line 200
    iget-boolean v4, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->fullscreen:Z

    if-eqz v4, :cond_0

    sget v4, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    if-lt v4, v9, :cond_2

    iget-object v4, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->insets:Landroid/graphics/Rect;

    if-nez v4, :cond_2

    .line 201
    :cond_0
    invoke-virtual {v0, v8, v8, v8, v8}, Landroid/view/View;->setPadding(IIII)V

    .line 196
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 202
    :cond_2
    sget v4, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    if-lt v4, v9, :cond_3

    .line 203
    iget-object v4, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->insets:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->insets:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->insets:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    iget-object v7, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->insets:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_1

    .line 204
    :cond_3
    iget-boolean v4, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->hasActionBar:Z

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->window:Landroid/view/Window;

    const/16 v5, 0x9

    invoke-virtual {v4, v5}, Landroid/view/Window;->hasFeature(I)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 205
    iget v4, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->actionBarHeight:I

    invoke-virtual {v0, v8, v4, v8, v8}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_1

    .line 207
    :cond_4
    invoke-virtual {v0, v8, v8, v8, v8}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_1

    .line 211
    .end local v0    # "child":Landroid/view/View;
    .end local v3    # "params":Lcom/google/android/videos/player/PlayerView$LayoutParams;
    :cond_5
    return-void
.end method

.method private static useImmersiveFlagOnV19(Landroid/content/Context;)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 239
    sget-object v5, Landroid/os/Build;->ID:Ljava/lang/String;

    if-eqz v5, :cond_1

    sget-object v5, Landroid/os/Build;->ID:Ljava/lang/String;

    const-string v6, "KTU"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 257
    :cond_0
    :goto_0
    return v3

    .line 245
    :cond_1
    :try_start_0
    const-string v5, "user"

    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/UserManager;

    .line 246
    .local v2, "userManager":Landroid/os/UserManager;
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v1

    .line 247
    .local v1, "handle":Landroid/os/UserHandle;
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v2, v1}, Landroid/os/UserManager;->getSerialNumberForUser(Landroid/os/UserHandle;)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    .line 249
    const-string v3, "Disabling immersive mode for secondary user"

    invoke-static {v3}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v3, v4

    .line 250
    goto :goto_0

    .line 252
    .end local v1    # "handle":Landroid/os/UserHandle;
    .end local v2    # "userManager":Landroid/os/UserManager;
    :catch_0
    move-exception v0

    .line 254
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "Disabling immersive mode, unable to determine user type"

    invoke-static {v3, v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v3, v4

    .line 255
    goto :goto_0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 159
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 164
    :goto_0
    return-void

    .line 161
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->applyDesiredFlags()V

    goto :goto_0

    .line 159
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onSystemUiVisibilityChange(I)V
    .locals 2
    .param p1, "visibility"    # I

    .prologue
    .line 135
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->playerView:Lcom/google/android/videos/player/PlayerView;

    invoke-virtual {v0}, Lcom/google/android/videos/player/PlayerView;->getSystemUiVisibility()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 138
    iget-object v0, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->playerView:Lcom/google/android/videos/player/PlayerView;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/player/PlayerView;->setSystemUiVisibility(I)V

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->listener:Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper$Listener;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->currentVisibilityFlags:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    and-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_1

    .line 142
    iget-object v0, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->listener:Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper$Listener;->onNavigationShown()V

    .line 144
    :cond_1
    iput p1, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->currentVisibilityFlags:I

    .line 146
    invoke-direct {p0}, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->maybeScheduleFlagApplication()V

    .line 147
    return-void
.end method

.method public onSystemWindowInsets(Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "insets"    # Landroid/graphics/Rect;

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->insets:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 152
    iput-object p1, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->insets:Landroid/graphics/Rect;

    .line 153
    invoke-direct {p0}, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->updatePaddingForInsetViews()V

    .line 155
    :cond_0
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->removeMessages(I)V

    .line 168
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->released:Z

    .line 169
    return-void
.end method

.method public setFullscreen(Z)V
    .locals 4
    .param p1, "fullscreen"    # Z

    .prologue
    const/16 v2, 0x400

    const/4 v1, 0x0

    .line 94
    iget-boolean v0, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->fullscreen:Z

    if-eq v0, p1, :cond_4

    .line 95
    if-eqz p1, :cond_0

    .line 96
    iget-object v0, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->window:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->defaultWindowFullscreenSet:Z

    .line 100
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->fullscreen:Z

    .line 101
    invoke-direct {p0}, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->applyDesiredFlags()V

    .line 102
    invoke-direct {p0}, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->updatePaddingForInsetViews()V

    .line 103
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v0, v3, :cond_3

    .line 104
    iget-object v0, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->window:Landroid/view/Window;

    if-nez p1, :cond_1

    iget-boolean v3, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->defaultWindowFullscreenSet:Z

    if-eqz v3, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setFlags(II)V

    .line 109
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->hasActionBar:Z

    if-eqz v0, :cond_4

    .line 110
    if-eqz p1, :cond_6

    iget-object v0, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->window:Landroid/view/Window;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/view/Window;->hasFeature(I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 111
    iget-object v0, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->actionBar:Landroid/support/v7/app/ActionBar;

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->hide()V

    .line 117
    :cond_4
    :goto_1
    return-void

    :cond_5
    move v0, v1

    .line 96
    goto :goto_0

    .line 112
    :cond_6
    if-nez p1, :cond_4

    .line 113
    iget-object v0, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->actionBar:Landroid/support/v7/app/ActionBar;

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->show()V

    goto :goto_1
.end method

.method public setSystemUiHidden(Z)V
    .locals 2
    .param p1, "hidden"    # Z

    .prologue
    .line 120
    iput-boolean p1, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->systemUiHiddenInFullscreen:Z

    .line 121
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->removeMessages(I)V

    .line 122
    invoke-direct {p0}, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->applyDesiredFlags()V

    .line 123
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->fullscreen:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->hasActionBar:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->window:Landroid/view/Window;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/view/Window;->hasFeature(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    if-eqz p1, :cond_1

    .line 126
    iget-object v0, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->actionBar:Landroid/support/v7/app/ActionBar;

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->hide()V

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 128
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->actionBar:Landroid/support/v7/app/ActionBar;

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->show()V

    goto :goto_0
.end method
