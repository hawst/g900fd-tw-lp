.class public final Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;
.super Ljava/lang/Object;
.source "PinningDbHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pinning/PinningDbHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DownloadDetails"
.end annotation


# instance fields
.field public final activation:Lcom/google/android/videos/drm/Activation;

.field public final cencKeySetId:[B

.field public final cencPsshData:[B

.field public final cencSecurityLevel:I

.field public final durationMs:J

.field public final extra:Lcom/google/android/videos/proto/DownloadExtra;

.field public final haveSubtitles:Z

.field public final isEpisode:Z

.field public final licenseType:I

.field public final mergedExpirationTimestamp:J

.field public final quality:I

.field public final relativeFilePath:Ljava/lang/String;

.field public final storage:I


# direct methods
.method public constructor <init>(Ljava/lang/String;IZZIIJJLcom/google/android/videos/proto/DownloadExtra;Lcom/google/android/videos/drm/Activation;[B[BI)V
    .locals 3
    .param p1, "relativeFilePath"    # Ljava/lang/String;
    .param p2, "licenseType"    # I
    .param p3, "haveSubtitles"    # Z
    .param p4, "isEpisode"    # Z
    .param p5, "quality"    # I
    .param p6, "storage"    # I
    .param p7, "mergedExpirationTimestamp"    # J
    .param p9, "durationMs"    # J
    .param p11, "extra"    # Lcom/google/android/videos/proto/DownloadExtra;
    .param p12, "activation"    # Lcom/google/android/videos/drm/Activation;
    .param p13, "cencKeySetId"    # [B
    .param p14, "cencPsshData"    # [B
    .param p15, "cencSecurityLevel"    # I

    .prologue
    .line 314
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 315
    iput-object p1, p0, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->relativeFilePath:Ljava/lang/String;

    .line 316
    iput p2, p0, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->licenseType:I

    .line 317
    iput-boolean p3, p0, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->haveSubtitles:Z

    .line 318
    iput-boolean p4, p0, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->isEpisode:Z

    .line 319
    iput p5, p0, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->quality:I

    .line 320
    iput p6, p0, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->storage:I

    .line 321
    iput-wide p7, p0, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->mergedExpirationTimestamp:J

    .line 322
    iput-wide p9, p0, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->durationMs:J

    .line 323
    iput-object p11, p0, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->extra:Lcom/google/android/videos/proto/DownloadExtra;

    .line 324
    iput-object p12, p0, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->activation:Lcom/google/android/videos/drm/Activation;

    .line 325
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->cencKeySetId:[B

    .line 326
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->cencPsshData:[B

    .line 327
    move/from16 v0, p15

    iput v0, p0, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->cencSecurityLevel:I

    .line 328
    return-void
.end method
