.class public Lcom/google/android/videos/ui/DogfoodHelper;
.super Ljava/lang/Object;
.source "DogfoodHelper.java"


# direct methods
.method public static addPawsIfNeeded(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "backgroundDrawable"    # I

    .prologue
    .line 21
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/videos/ui/DogfoodHelper;->addPawsIfNeeded(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method private static addPawsIfNeeded(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "backgroundDrawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 25
    invoke-static {p0}, Lcom/google/android/videos/ui/DogfoodHelper;->isDogfoodEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/videos/ui/DogfoodHelper;->createPawsOverlay(Landroid/content/Context;Landroid/graphics/drawable/Drawable;Z)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .end local p1    # "backgroundDrawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    return-object p1
.end method

.method public static addPawsIfNeeded(Landroid/content/Context;Landroid/app/ActionBar;I)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "actionBar"    # Landroid/app/ActionBar;
    .param p2, "backgroundDrawable"    # I

    .prologue
    .line 35
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/google/android/videos/ui/DogfoodHelper;->addPawsIfNeeded(Landroid/content/Context;Landroid/app/ActionBar;Landroid/graphics/drawable/Drawable;)V

    .line 36
    return-void
.end method

.method private static addPawsIfNeeded(Landroid/content/Context;Landroid/app/ActionBar;Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "actionBar"    # Landroid/app/ActionBar;
    .param p2, "backgroundDrawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 54
    invoke-static {p0}, Lcom/google/android/videos/ui/DogfoodHelper;->isDogfoodEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 60
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 61
    const/4 v1, 0x1

    invoke-static {p0, p2, v1}, Lcom/google/android/videos/ui/DogfoodHelper;->createPawsOverlay(Landroid/content/Context;Landroid/graphics/drawable/Drawable;Z)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 62
    .local v0, "background":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 64
    .end local v0    # "background":Landroid/graphics/drawable/Drawable;
    :cond_0
    return-void
.end method

.method public static addPawsIfNeeded(Landroid/content/Context;Landroid/support/v7/app/ActionBar;Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "actionBar"    # Landroid/support/v7/app/ActionBar;
    .param p2, "backgroundDrawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 40
    invoke-static {p0}, Lcom/google/android/videos/ui/DogfoodHelper;->isDogfoodEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 46
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/support/v7/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 47
    const/4 v1, 0x1

    invoke-static {p0, p2, v1}, Lcom/google/android/videos/ui/DogfoodHelper;->createPawsOverlay(Landroid/content/Context;Landroid/graphics/drawable/Drawable;Z)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 48
    .local v0, "background":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p1, v0}, Landroid/support/v7/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 50
    .end local v0    # "background":Landroid/graphics/drawable/Drawable;
    :cond_0
    return-void
.end method

.method private static createPawsOverlay(Landroid/content/Context;Landroid/graphics/drawable/Drawable;Z)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "backgroundDrawable"    # Landroid/graphics/drawable/Drawable;
    .param p2, "forActionBar"    # Z

    .prologue
    .line 68
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020068

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 70
    .local v0, "paws":Landroid/graphics/drawable/BitmapDrawable;
    sget-object v1, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeX(Landroid/graphics/Shader$TileMode;)V

    .line 71
    if-nez p2, :cond_0

    .line 72
    sget-object v1, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeY(Landroid/graphics/Shader$TileMode;)V

    .line 74
    :cond_0
    if-nez p1, :cond_1

    .line 77
    .end local v0    # "paws":Landroid/graphics/drawable/BitmapDrawable;
    :goto_0
    return-object v0

    .restart local v0    # "paws":Landroid/graphics/drawable/BitmapDrawable;
    :cond_1
    new-instance v1, Landroid/graphics/drawable/LayerDrawable;

    const/4 v2, 0x2

    new-array v2, v2, [Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-direct {v1, v2}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    move-object v0, v1

    goto :goto_0
.end method

.method private static isDogfoodEnabled(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 81
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v2

    .line 82
    .local v2, "videosGlobals":Lcom/google/android/videos/VideosGlobals;
    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getApplicationVersionCode()I

    move-result v3

    div-int/lit16 v0, v3, 0x3e8

    .line 83
    .local v0, "currentMajorVersion":I
    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/videos/Config;->latestVersion()I

    move-result v3

    div-int/lit16 v1, v3, 0x3e8

    .line 84
    .local v1, "latestMajorVersion":I
    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/videos/Config;->dogfoodEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    if-le v0, v1, :cond_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method
