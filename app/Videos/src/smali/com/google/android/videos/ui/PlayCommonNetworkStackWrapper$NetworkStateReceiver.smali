.class Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper$NetworkStateReceiver;
.super Ljava/lang/Object;
.source "PlayCommonNetworkStackWrapper.java"

# interfaces
.implements Lcom/google/android/play/image/FifeUrlUtils$NetworkInfoCache;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NetworkStateReceiver"
.end annotation


# instance fields
.field private networkInfo:Landroid/net/NetworkInfo;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper$1;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper$NetworkStateReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public getNetworkInfo(Landroid/content/Context;)Landroid/net/NetworkInfo;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper$NetworkStateReceiver;->networkInfo:Landroid/net/NetworkInfo;

    return-object v0
.end method

.method public setNetworkInfo(Landroid/net/NetworkInfo;)V
    .locals 0
    .param p1, "networkInfo"    # Landroid/net/NetworkInfo;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper$NetworkStateReceiver;->networkInfo:Landroid/net/NetworkInfo;

    .line 55
    return-void
.end method
