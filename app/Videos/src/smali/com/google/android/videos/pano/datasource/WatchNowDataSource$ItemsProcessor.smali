.class abstract Lcom/google/android/videos/pano/datasource/WatchNowDataSource$ItemsProcessor;
.super Ljava/lang/Object;
.source "WatchNowDataSource.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/datasource/WatchNowDataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "ItemsProcessor"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private isReady:Z

.field public final itemsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/videos/pano/datasource/WatchNowDataSource;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;)V
    .locals 1

    .prologue
    .line 325
    iput-object p1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$ItemsProcessor;->this$0:Lcom/google/android/videos/pano/datasource/WatchNowDataSource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 327
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$ItemsProcessor;->itemsList:Ljava/util/ArrayList;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;Lcom/google/android/videos/pano/datasource/WatchNowDataSource$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/pano/datasource/WatchNowDataSource;
    .param p2, "x1"    # Lcom/google/android/videos/pano/datasource/WatchNowDataSource$1;

    .prologue
    .line 325
    invoke-direct {p0, p1}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$ItemsProcessor;-><init>(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;)V

    return-void
.end method


# virtual methods
.method public final clear()V
    .locals 1

    .prologue
    .line 341
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$ItemsProcessor;->isReady:Z

    .line 342
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$ItemsProcessor;->itemsList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 343
    return-void
.end method

.method protected abstract createCursorRequest()Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
.end method

.method protected abstract createItemFromCursor(Landroid/database/Cursor;Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;
.end method

.method public final getPurchases()V
    .locals 3

    .prologue
    .line 336
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$ItemsProcessor;->isReady:Z

    .line 337
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$ItemsProcessor;->this$0:Lcom/google/android/videos/pano/datasource/WatchNowDataSource;

    # getter for: Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;
    invoke-static {v0}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->access$1000(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;)Lcom/google/android/videos/store/PurchaseStore;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$ItemsProcessor;->createCursorRequest()Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$ItemsProcessor;->this$0:Lcom/google/android/videos/pano/datasource/WatchNowDataSource;

    # invokes: Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->decorateCallback(Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/Callback;
    invoke-static {v2, p0}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->access$900(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/Callback;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/videos/async/Callback;)V

    .line 338
    return-void
.end method

.method public final isReady()Z
    .locals 1

    .prologue
    .line 332
    iget-boolean v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$ItemsProcessor;->isReady:Z

    return v0
.end method

.method public final onError(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 362
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$ItemsProcessor;->this$0:Lcom/google/android/videos/pano/datasource/WatchNowDataSource;

    # invokes: Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->onError(Ljava/lang/Exception;)V
    invoke-static {v0, p2}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->access$1200(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;Ljava/lang/Exception;)V

    .line 363
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 325
    check-cast p1, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$ItemsProcessor;->onError(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public final onResponse(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V
    .locals 3
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 347
    iget-object v1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$ItemsProcessor;->this$0:Lcom/google/android/videos/pano/datasource/WatchNowDataSource;

    invoke-virtual {v1}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->getAccount()Ljava/lang/String;

    move-result-object v0

    .line 348
    .local v0, "account":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$ItemsProcessor;->itemsList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 350
    :goto_0
    :try_start_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 351
    iget-object v1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$ItemsProcessor;->itemsList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$ItemsProcessor;->this$0:Lcom/google/android/videos/pano/datasource/WatchNowDataSource;

    iget-object v2, v2, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->context:Landroid/content/Context;

    invoke-virtual {p0, p2, v2, v0}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$ItemsProcessor;->createItemFromCursor(Landroid/database/Cursor;Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 354
    :catchall_0
    move-exception v1

    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_0
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    .line 356
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$ItemsProcessor;->isReady:Z

    .line 357
    iget-object v1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$ItemsProcessor;->this$0:Lcom/google/android/videos/pano/datasource/WatchNowDataSource;

    # invokes: Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->mergeAndUpdate()V
    invoke-static {v1}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->access$1100(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;)V

    .line 358
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 325
    check-cast p1, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$ItemsProcessor;->onResponse(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V

    return-void
.end method
