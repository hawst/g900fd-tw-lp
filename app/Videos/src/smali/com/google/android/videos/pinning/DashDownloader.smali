.class Lcom/google/android/videos/pinning/DashDownloader;
.super Lcom/google/android/videos/pinning/Downloader;
.source "DashDownloader.java"


# instance fields
.field private audioStream:Lcom/google/android/videos/streams/MediaStream;

.field private final context:Landroid/content/Context;

.field private final exoCacheProvider:Lcom/google/android/videos/pinning/ExoCacheProvider;

.field private relativeDownloadDir:Ljava/lang/String;

.field private videoStream:Lcom/google/android/videos/streams/MediaStream;


# direct methods
.method constructor <init>(Lcom/google/android/videos/VideosGlobals;Lcom/google/android/videos/pinning/Downloader$ProgressListener;Lcom/google/android/videos/pinning/DownloadKey;Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/io/File;Lcom/google/android/videos/async/TaskStatus;Z)V
    .locals 1
    .param p1, "videosGlobals"    # Lcom/google/android/videos/VideosGlobals;
    .param p2, "progressListener"    # Lcom/google/android/videos/pinning/Downloader$ProgressListener;
    .param p3, "key"    # Lcom/google/android/videos/pinning/DownloadKey;
    .param p4, "details"    # Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;
    .param p5, "rootFilesDir"    # Ljava/io/File;
    .param p6, "taskStatus"    # Lcom/google/android/videos/async/TaskStatus;
    .param p7, "debug"    # Z

    .prologue
    .line 82
    invoke-direct/range {p0 .. p7}, Lcom/google/android/videos/pinning/Downloader;-><init>(Lcom/google/android/videos/VideosGlobals;Lcom/google/android/videos/pinning/Downloader$ProgressListener;Lcom/google/android/videos/pinning/DownloadKey;Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/io/File;Lcom/google/android/videos/async/TaskStatus;Z)V

    .line 83
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pinning/DashDownloader;->context:Landroid/content/Context;

    .line 84
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getExoCacheProvider()Lcom/google/android/videos/pinning/ExoCacheProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pinning/DashDownloader;->exoCacheProvider:Lcom/google/android/videos/pinning/ExoCacheProvider;

    .line 85
    return-void
.end method

.method private acquireLicense(Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;)V
    .locals 13
    .param p1, "mediaDrmWrapper"    # Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/PinningTask$PinningException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x1

    .line 186
    new-instance v2, Ljava/io/File;

    iget-object v6, p0, Lcom/google/android/videos/pinning/DashDownloader;->rootFilesDir:Ljava/io/File;

    iget-object v7, p0, Lcom/google/android/videos/pinning/DashDownloader;->relativeDownloadDir:Ljava/lang/String;

    invoke-direct {v2, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 187
    .local v2, "downloadDir":Ljava/io/File;
    iget-object v6, p0, Lcom/google/android/videos/pinning/DashDownloader;->exoCacheProvider:Lcom/google/android/videos/pinning/ExoCacheProvider;

    invoke-virtual {v6, v2}, Lcom/google/android/videos/pinning/ExoCacheProvider;->acquireDownloadCache(Ljava/io/File;)Lcom/google/android/exoplayer/upstream/cache/Cache;

    move-result-object v0

    .line 188
    .local v0, "cache":Lcom/google/android/exoplayer/upstream/cache/Cache;
    invoke-direct {p0, v0}, Lcom/google/android/videos/pinning/DashDownloader;->buildDownloadDataSource(Lcom/google/android/exoplayer/upstream/cache/Cache;)Lcom/google/android/exoplayer/upstream/DataSource;

    move-result-object v1

    .line 189
    .local v1, "dataSource":Lcom/google/android/exoplayer/upstream/DataSource;
    sget-object v6, Lcom/google/android/exoplayer/upstream/NetworkLock;->instance:Lcom/google/android/exoplayer/upstream/NetworkLock;

    invoke-virtual {v6, v12}, Lcom/google/android/exoplayer/upstream/NetworkLock;->add(I)V

    .line 191
    :try_start_0
    iget-object v6, p0, Lcom/google/android/videos/pinning/DashDownloader;->videoStream:Lcom/google/android/videos/streams/MediaStream;

    const-string v7, "video/"

    iget-object v8, p0, Lcom/google/android/videos/pinning/DashDownloader;->key:Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v8, v8, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/videos/pinning/DashDownloader;->details:Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;

    iget-wide v10, v9, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->durationMs:J

    invoke-static {v6, v7, v8, v10, v11}, Lcom/google/android/videos/player/exo/ExoPlayerUtil;->toRepresentation(Lcom/google/android/videos/streams/MediaStream;Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    move-result-object v5

    .line 193
    .local v5, "videoRepresentation":Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    iget-object v6, p0, Lcom/google/android/videos/pinning/DashDownloader;->videoStream:Lcom/google/android/videos/streams/MediaStream;

    invoke-static {v1, v5, v6}, Lcom/google/android/videos/player/exo/ExoPlayerUtil;->prepareExtractor(Lcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;Lcom/google/android/videos/streams/MediaStream;)Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;

    move-result-object v4

    .line 195
    .local v4, "videoExtractor":Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    iget-object v6, p0, Lcom/google/android/videos/pinning/DashDownloader;->relativeDownloadDir:Ljava/lang/String;

    const-string v7, "Acquiring license"

    invoke-virtual {p0, v6, v7}, Lcom/google/android/videos/pinning/DashDownloader;->debug(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    invoke-virtual {v4}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->getPsshInfo()Ljava/util/Map;

    move-result-object v6

    invoke-virtual {v4}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->getFormat()Lcom/google/android/exoplayer/MediaFormat;

    move-result-object v7

    invoke-direct {p0, p1, v6, v7}, Lcom/google/android/videos/pinning/DashDownloader;->acquireLicense(Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;Ljava/util/Map;Lcom/google/android/exoplayer/MediaFormat;)V

    .line 197
    iget-object v6, p0, Lcom/google/android/videos/pinning/DashDownloader;->relativeDownloadDir:Ljava/lang/String;

    const-string v7, "License acquired"

    invoke-virtual {p0, v6, v7}, Lcom/google/android/videos/pinning/DashDownloader;->debug(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 205
    sget-object v6, Lcom/google/android/exoplayer/upstream/NetworkLock;->instance:Lcom/google/android/exoplayer/upstream/NetworkLock;

    invoke-virtual {v6, v12}, Lcom/google/android/exoplayer/upstream/NetworkLock;->remove(I)V

    .line 207
    :try_start_1
    invoke-interface {v1}, Lcom/google/android/exoplayer/upstream/DataSource;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 212
    .end local v4    # "videoExtractor":Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    .end local v5    # "videoRepresentation":Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    :goto_0
    return-void

    .line 208
    .restart local v4    # "videoExtractor":Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    .restart local v5    # "videoRepresentation":Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    :catch_0
    move-exception v3

    .line 209
    .local v3, "e":Ljava/io/IOException;
    const-string v6, "Failed to close data source"

    invoke-static {v6, v3}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 198
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "videoExtractor":Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    .end local v5    # "videoRepresentation":Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    :catch_1
    move-exception v3

    .line 199
    .restart local v3    # "e":Ljava/io/IOException;
    :try_start_2
    new-instance v6, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    const-string v7, "I/O exception while downloading video"

    const/4 v8, 0x0

    const/16 v9, 0xe

    invoke-direct {v6, v7, v3, v8, v9}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;ZI)V

    throw v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 205
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    sget-object v7, Lcom/google/android/exoplayer/upstream/NetworkLock;->instance:Lcom/google/android/exoplayer/upstream/NetworkLock;

    invoke-virtual {v7, v12}, Lcom/google/android/exoplayer/upstream/NetworkLock;->remove(I)V

    .line 207
    :try_start_3
    invoke-interface {v1}, Lcom/google/android/exoplayer/upstream/DataSource;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    .line 210
    :goto_1
    throw v6

    .line 202
    :catch_2
    move-exception v6

    .line 205
    sget-object v6, Lcom/google/android/exoplayer/upstream/NetworkLock;->instance:Lcom/google/android/exoplayer/upstream/NetworkLock;

    invoke-virtual {v6, v12}, Lcom/google/android/exoplayer/upstream/NetworkLock;->remove(I)V

    .line 207
    :try_start_4
    invoke-interface {v1}, Lcom/google/android/exoplayer/upstream/DataSource;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_0

    .line 208
    :catch_3
    move-exception v3

    .line 209
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v6, "Failed to close data source"

    invoke-static {v6, v3}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 208
    .end local v3    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v3

    .line 209
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v7, "Failed to close data source"

    invoke-static {v7, v3}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private acquireLicense(Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;Ljava/util/Map;Lcom/google/android/exoplayer/MediaFormat;)V
    .locals 16
    .param p1, "mediaDrmWrapper"    # Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;
    .param p3, "format"    # Lcom/google/android/exoplayer/MediaFormat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;",
            "Ljava/util/Map",
            "<",
            "Ljava/util/UUID;",
            "[B>;",
            "Lcom/google/android/exoplayer/MediaFormat;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/PinningTask$PinningException;
        }
    .end annotation

    .prologue
    .line 216
    .local p2, "psshInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/util/UUID;[B>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/pinning/DashDownloader;->videoStream:Lcom/google/android/videos/streams/MediaStream;

    iget-object v2, v2, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    iget v2, v2, Lcom/google/android/videos/streams/ItagInfo;->drmType:I

    if-nez v2, :cond_0

    .line 258
    :goto_0
    return-void

    .line 220
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v12

    .line 221
    .local v12, "openCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/Object;[B>;"
    const-string v2, "video/"

    const-string v3, "OpenSessionForOffline"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v2, v3, v12}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->open(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 222
    invoke-virtual {v12}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;

    .line 224
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v10

    .line 225
    .local v10, "licenseCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/Object;[B>;"
    const-string v2, "AcquireLicenseForPinning"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->requestLicense(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 226
    invoke-virtual {v10}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [B

    .line 229
    .local v11, "offlineLicense":[B
    new-instance v9, Lcom/google/android/videos/proto/DownloadExtra;

    invoke-direct {v9}, Lcom/google/android/videos/proto/DownloadExtra;-><init>()V

    .line 230
    .local v9, "extra":Lcom/google/android/videos/proto/DownloadExtra;
    const/4 v2, 0x2

    new-array v2, v2, [Lcom/google/android/videos/proto/StreamInfo;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/pinning/DashDownloader;->videoStream:Lcom/google/android/videos/streams/MediaStream;

    iget-object v5, v5, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    aput-object v5, v2, v3

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/pinning/DashDownloader;->audioStream:Lcom/google/android/videos/streams/MediaStream;

    iget-object v5, v5, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    aput-object v5, v2, v3

    iput-object v2, v9, Lcom/google/android/videos/proto/DownloadExtra;->streamInfos:[Lcom/google/android/videos/proto/StreamInfo;

    .line 233
    invoke-static {}, Lcom/google/android/videos/pinning/PinningDbHelper;->getClearedLicenseContentValues()Landroid/content/ContentValues;

    move-result-object v13

    .line 234
    .local v13, "values":Landroid/content/ContentValues;
    const-string v2, "download_relative_filepath"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/pinning/DashDownloader;->relativeDownloadDir:Ljava/lang/String;

    invoke-virtual {v13, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    const-string v2, "download_extra_proto"

    invoke-static {v9}, Lcom/google/android/videos/proto/DownloadExtra;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v3

    invoke-virtual {v13, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 236
    const-string v2, "license_type"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v13, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 237
    const-string v2, "license_last_synced_timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v13, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 238
    const-string v2, "license_force_sync"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v13, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 239
    const-string v2, "license_cenc_key_set_id"

    invoke-virtual {v13, v2, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 240
    const-string v3, "license_cenc_pssh_data"

    sget-object v2, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->WIDEVINE_UUID:Ljava/util/UUID;

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    invoke-virtual {v13, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 242
    const-string v2, "license_cenc_mimetype"

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/android/exoplayer/MediaFormat;->mimeType:Ljava/lang/String;

    invoke-virtual {v13, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    const-string v2, "license_cenc_security_level"

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->getSecurityLevel()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v13, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 244
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/pinning/DashDownloader;->database:Lcom/google/android/videos/store/Database;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/pinning/DashDownloader;->key:Lcom/google/android/videos/pinning/DownloadKey;

    invoke-static {v2, v3, v13}, Lcom/google/android/videos/pinning/PinningDbHelper;->updatePinningStateForVideo(Lcom/google/android/videos/store/Database;Lcom/google/android/videos/pinning/DownloadKey;Landroid/content/ContentValues;)V
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 256
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->close()V

    goto/16 :goto_0

    .line 245
    .end local v9    # "extra":Lcom/google/android/videos/proto/DownloadExtra;
    .end local v10    # "licenseCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/Object;[B>;"
    .end local v11    # "offlineLicense":[B
    .end local v12    # "openCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/Object;[B>;"
    .end local v13    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v8

    .line 246
    .local v8, "e":Ljava/util/concurrent/ExecutionException;
    :try_start_1
    invoke-virtual {v8}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    if-nez v2, :cond_1

    move-object v4, v8

    .line 247
    .local v4, "cause":Ljava/lang/Throwable;
    :goto_1
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/videos/pinning/DashDownloader;->parseErrorFromDrmException(Ljava/lang/Throwable;)I

    move-result v6

    .line 248
    .local v6, "failedReason":I
    instance-of v2, v4, Lcom/google/android/videos/api/CencLicenseException;

    if-eqz v2, :cond_2

    .line 249
    new-instance v2, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    const-string v3, "Error during license request"

    const/4 v5, 0x1

    move-object v0, v4

    check-cast v0, Lcom/google/android/videos/api/CencLicenseException;

    move-object v7, v0

    iget v7, v7, Lcom/google/android/videos/api/CencLicenseException;->statusCode:I

    invoke-direct/range {v2 .. v7}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;ZII)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 256
    .end local v4    # "cause":Ljava/lang/Throwable;
    .end local v6    # "failedReason":I
    .end local v8    # "e":Ljava/util/concurrent/ExecutionException;
    :catchall_0
    move-exception v2

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->close()V

    throw v2

    .line 246
    .restart local v8    # "e":Ljava/util/concurrent/ExecutionException;
    :cond_1
    :try_start_2
    invoke-virtual {v8}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    goto :goto_1

    .line 252
    .restart local v4    # "cause":Ljava/lang/Throwable;
    .restart local v6    # "failedReason":I
    :cond_2
    new-instance v2, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    const-string v3, "Error during license request"

    const/4 v5, 0x1

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;ZI)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method private buildDownloadDataSource(Lcom/google/android/exoplayer/upstream/cache/Cache;)Lcom/google/android/exoplayer/upstream/DataSource;
    .locals 5
    .param p1, "cache"    # Lcom/google/android/exoplayer/upstream/cache/Cache;

    .prologue
    const/4 v4, 0x1

    .line 379
    iget-object v2, p0, Lcom/google/android/videos/pinning/DashDownloader;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-static {v2}, Lcom/google/android/videos/player/exo/ExoPlayerUtil;->buildDownloadHttpDataSource(Lcom/google/android/videos/VideosGlobals;)Lcom/google/android/exoplayer/upstream/DataSource;

    move-result-object v0

    .line 380
    .local v0, "httpDataSource":Lcom/google/android/exoplayer/upstream/DataSource;
    new-instance v1, Lcom/google/android/exoplayer/upstream/PriorityDataSource;

    invoke-direct {v1, v4, v0}, Lcom/google/android/exoplayer/upstream/PriorityDataSource;-><init>(ILcom/google/android/exoplayer/upstream/DataSource;)V

    .line 381
    .local v1, "priorityDataSource":Lcom/google/android/exoplayer/upstream/DataSource;
    new-instance v2, Lcom/google/android/exoplayer/upstream/cache/CacheDataSource;

    const/4 v3, 0x0

    invoke-direct {v2, p1, v1, v4, v3}, Lcom/google/android/exoplayer/upstream/cache/CacheDataSource;-><init>(Lcom/google/android/exoplayer/upstream/cache/Cache;Lcom/google/android/exoplayer/upstream/DataSource;ZZ)V

    return-object v2
.end method

.method private downloadChunk(Lcom/google/android/exoplayer/upstream/DataSource;Landroid/net/Uri;J[BLcom/google/android/exoplayer/dash/mpd/Representation;Lcom/google/android/exoplayer/parser/SegmentIndex;I)I
    .locals 19
    .param p1, "dataSource"    # Lcom/google/android/exoplayer/upstream/DataSource;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "streamOffset"    # J
    .param p5, "buffer"    # [B
    .param p6, "representation"    # Lcom/google/android/exoplayer/dash/mpd/Representation;
    .param p7, "segmentIndex"    # Lcom/google/android/exoplayer/parser/SegmentIndex;
    .param p8, "startSegment"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/util/concurrent/CancellationException;
        }
    .end annotation

    .prologue
    .line 394
    add-int/lit8 v13, p8, 0x1

    .line 395
    .local v13, "endSegment":I
    const-wide/16 v10, 0x0

    .line 396
    .local v10, "chunkDurationUs":J
    :goto_0
    move-object/from16 v0, p7

    iget v3, v0, Lcom/google/android/exoplayer/parser/SegmentIndex;->length:I

    if-ge v13, v3, :cond_0

    const-wide/32 v4, 0x3938700

    cmp-long v3, v10, v4

    if-gtz v3, :cond_0

    .line 397
    move-object/from16 v0, p7

    iget-object v3, v0, Lcom/google/android/exoplayer/parser/SegmentIndex;->durationsUs:[J

    aget-wide v4, v3, v13

    add-long/2addr v10, v4

    .line 398
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 400
    :cond_0
    move-object/from16 v0, p7

    iget-object v3, v0, Lcom/google/android/exoplayer/parser/SegmentIndex;->offsets:[J

    add-int/lit8 v4, v13, -0x1

    aget-wide v4, v3, v4

    move-object/from16 v0, p7

    iget-object v3, v0, Lcom/google/android/exoplayer/parser/SegmentIndex;->offsets:[J

    aget-wide v16, v3, p8

    sub-long v4, v4, v16

    move-object/from16 v0, p7

    iget-object v3, v0, Lcom/google/android/exoplayer/parser/SegmentIndex;->sizes:[I

    add-int/lit8 v8, v13, -0x1

    aget v3, v3, v8

    int-to-long v0, v3

    move-wide/from16 v16, v0

    add-long v6, v4, v16

    .line 402
    .local v6, "chunkLength":J
    new-instance v2, Lcom/google/android/exoplayer/upstream/DataSpec;

    move-object/from16 v0, p7

    iget-object v3, v0, Lcom/google/android/exoplayer/parser/SegmentIndex;->offsets:[J

    aget-wide v4, v3, p8

    add-long v4, v4, p3

    invoke-virtual/range {p6 .. p6}, Lcom/google/android/exoplayer/dash/mpd/Representation;->getCacheKey()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v3, p2

    invoke-direct/range {v2 .. v8}, Lcom/google/android/exoplayer/upstream/DataSpec;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    .line 406
    .local v2, "dataSpec":Lcom/google/android/exoplayer/upstream/DataSpec;
    const/4 v9, 0x0

    .line 407
    .local v9, "done":Z
    :goto_1
    if-nez v9, :cond_4

    .line 409
    const/4 v15, 0x0

    .line 410
    .local v15, "shouldProceed":Z
    :goto_2
    if-nez v15, :cond_1

    .line 412
    :try_start_0
    sget-object v3, Lcom/google/android/exoplayer/upstream/NetworkLock;->instance:Lcom/google/android/exoplayer/upstream/NetworkLock;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/android/exoplayer/upstream/NetworkLock;->proceed(I)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 413
    const/4 v15, 0x1

    goto :goto_2

    .line 414
    :catch_0
    move-exception v12

    .line 415
    .local v12, "e":Ljava/lang/InterruptedException;
    new-instance v3, Ljava/util/concurrent/CancellationException;

    invoke-direct {v3}, Ljava/util/concurrent/CancellationException;-><init>()V

    throw v3

    .line 419
    .end local v12    # "e":Ljava/lang/InterruptedException;
    :cond_1
    :try_start_1
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Lcom/google/android/exoplayer/upstream/DataSource;->open(Lcom/google/android/exoplayer/upstream/DataSpec;)J

    .line 420
    const/4 v14, 0x0

    .line 421
    .local v14, "read":I
    :goto_3
    const/4 v3, -0x1

    if-eq v14, v3, :cond_3

    .line 422
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/pinning/DashDownloader;->isCanceled()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 423
    new-instance v3, Ljava/util/concurrent/CancellationException;

    invoke-direct {v3}, Ljava/util/concurrent/CancellationException;-><init>()V

    throw v3
    :try_end_1
    .catch Lcom/google/android/exoplayer/upstream/NetworkLock$PriorityTooLowException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 428
    .end local v14    # "read":I
    :catch_1
    move-exception v3

    .line 431
    invoke-interface/range {p1 .. p1}, Lcom/google/android/exoplayer/upstream/DataSource;->close()V

    goto :goto_1

    .line 425
    .restart local v14    # "read":I
    :cond_2
    const/4 v3, 0x0

    const/high16 v4, 0x20000

    :try_start_2
    move-object/from16 v0, p1

    move-object/from16 v1, p5

    invoke-interface {v0, v1, v3, v4}, Lcom/google/android/exoplayer/upstream/DataSource;->read([BII)I
    :try_end_2
    .catch Lcom/google/android/exoplayer/upstream/NetworkLock$PriorityTooLowException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v14

    goto :goto_3

    .line 427
    :cond_3
    const/4 v9, 0x1

    .line 431
    invoke-interface/range {p1 .. p1}, Lcom/google/android/exoplayer/upstream/DataSource;->close()V

    goto :goto_1

    .end local v14    # "read":I
    :catchall_0
    move-exception v3

    invoke-interface/range {p1 .. p1}, Lcom/google/android/exoplayer/upstream/DataSource;->close()V

    throw v3

    .line 435
    .end local v15    # "shouldProceed":Z
    :cond_4
    return v13
.end method

.method private getDashStreamTimestampForKnowledge(Lcom/google/android/videos/streams/Streams;)J
    .locals 6
    .param p1, "streams"    # Lcom/google/android/videos/streams/Streams;

    .prologue
    .line 285
    iget-object v3, p1, Lcom/google/android/videos/streams/Streams;->mediaStreams:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    .line 286
    .local v1, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 287
    iget-object v3, p1, Lcom/google/android/videos/streams/Streams;->mediaStreams:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/streams/MediaStream;

    .line 288
    .local v2, "stream":Lcom/google/android/videos/streams/MediaStream;
    iget-object v3, v2, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget v3, v3, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    const/16 v4, 0x90

    if-ne v3, v4, :cond_0

    .line 289
    iget-object v3, v2, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-wide v4, v3, Lcom/google/android/videos/proto/StreamInfo;->lastModifiedTimestamp:J

    .line 294
    .end local v2    # "stream":Lcom/google/android/videos/streams/MediaStream;
    :goto_1
    return-wide v4

    .line 286
    .restart local v2    # "stream":Lcom/google/android/videos/streams/MediaStream;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 292
    .end local v2    # "stream":Lcom/google/android/videos/streams/MediaStream;
    :cond_1
    const-string v3, "Can\'t find itag 144 in available streams, knowledge bundle will be downloaded without a timestamp."

    invoke-static {v3}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 294
    const-wide/16 v4, 0x0

    goto :goto_1
.end method

.method private parseErrorFromDrmException(Ljava/lang/Throwable;)I
    .locals 3
    .param p1, "exception"    # Ljava/lang/Throwable;

    .prologue
    const/4 v1, 0x1

    .line 262
    instance-of v2, p1, Lcom/google/android/videos/api/CencLicenseException;

    if-nez v2, :cond_0

    .line 280
    :goto_0
    return v1

    :cond_0
    move-object v0, p1

    .line 265
    check-cast v0, Lcom/google/android/videos/api/CencLicenseException;

    .line 266
    .local v0, "cencException":Lcom/google/android/videos/api/CencLicenseException;
    iget v2, v0, Lcom/google/android/videos/api/CencLicenseException;->statusCode:I

    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    .line 268
    :sswitch_0
    const/4 v1, 0x2

    goto :goto_0

    .line 270
    :sswitch_1
    const/4 v1, 0x3

    goto :goto_0

    .line 272
    :sswitch_2
    const/16 v1, 0x14

    goto :goto_0

    .line 274
    :sswitch_3
    const/16 v1, 0x15

    goto :goto_0

    .line 276
    :sswitch_4
    const/16 v1, 0x16

    goto :goto_0

    .line 278
    :sswitch_5
    const/16 v1, 0x17

    goto :goto_0

    .line 266
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x65 -> :sswitch_2
        0x66 -> :sswitch_3
        0x67 -> :sswitch_4
        0x68 -> :sswitch_5
        0x191 -> :sswitch_1
    .end sparse-switch
.end method

.method private setupExistingDownload(Lcom/google/android/videos/streams/Streams;)V
    .locals 14
    .param p1, "streams"    # Lcom/google/android/videos/streams/Streams;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/PinningTask$PinningException;
        }
    .end annotation

    .prologue
    .line 110
    iget-object v10, p0, Lcom/google/android/videos/pinning/DashDownloader;->details:Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;

    iget-object v10, v10, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->relativeFilePath:Ljava/lang/String;

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 112
    new-instance v10, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    const-string v11, "empty relativeFilePath"

    const/4 v12, 0x1

    const/16 v13, 0x12

    invoke-direct {v10, v11, v12, v13}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    throw v10

    .line 115
    :cond_0
    iget-object v10, p0, Lcom/google/android/videos/pinning/DashDownloader;->details:Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;

    iget-object v10, v10, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->relativeFilePath:Ljava/lang/String;

    iput-object v10, p0, Lcom/google/android/videos/pinning/DashDownloader;->relativeDownloadDir:Ljava/lang/String;

    .line 117
    iget-object v10, p0, Lcom/google/android/videos/pinning/DashDownloader;->details:Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;

    iget-object v10, v10, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->extra:Lcom/google/android/videos/proto/DownloadExtra;

    iget-object v10, v10, Lcom/google/android/videos/proto/DownloadExtra;->streamInfos:[Lcom/google/android/videos/proto/StreamInfo;

    const/4 v11, 0x0

    aget-object v9, v10, v11

    .line 118
    .local v9, "videoStreamInfo":Lcom/google/android/videos/proto/StreamInfo;
    iget-object v10, p0, Lcom/google/android/videos/pinning/DashDownloader;->details:Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;

    iget-object v10, v10, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->extra:Lcom/google/android/videos/proto/DownloadExtra;

    iget-object v10, v10, Lcom/google/android/videos/proto/DownloadExtra;->streamInfos:[Lcom/google/android/videos/proto/StreamInfo;

    const/4 v11, 0x1

    aget-object v2, v10, v11

    .line 119
    .local v2, "audioStreamInfo":Lcom/google/android/videos/proto/StreamInfo;
    iget-object v10, p1, Lcom/google/android/videos/streams/Streams;->mediaStreams:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/videos/streams/MediaStream;

    .line 120
    .local v6, "stream":Lcom/google/android/videos/streams/MediaStream;
    iget-object v10, v6, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget v10, v10, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    iget v11, v9, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    if-ne v10, v11, :cond_5

    .line 121
    iput-object v6, p0, Lcom/google/android/videos/pinning/DashDownloader;->videoStream:Lcom/google/android/videos/streams/MediaStream;

    .line 126
    :cond_2
    :goto_0
    iget-object v10, p0, Lcom/google/android/videos/pinning/DashDownloader;->videoStream:Lcom/google/android/videos/streams/MediaStream;

    if-eqz v10, :cond_1

    iget-object v10, p0, Lcom/google/android/videos/pinning/DashDownloader;->audioStream:Lcom/google/android/videos/streams/MediaStream;

    if-eqz v10, :cond_1

    .line 131
    .end local v6    # "stream":Lcom/google/android/videos/streams/MediaStream;
    :cond_3
    iget-object v10, p0, Lcom/google/android/videos/pinning/DashDownloader;->videoStream:Lcom/google/android/videos/streams/MediaStream;

    if-nez v10, :cond_6

    const/4 v7, 0x1

    .line 132
    .local v7, "videoMissing":Z
    :goto_1
    iget-object v10, p0, Lcom/google/android/videos/pinning/DashDownloader;->audioStream:Lcom/google/android/videos/streams/MediaStream;

    if-nez v10, :cond_7

    const/4 v0, 0x1

    .line 133
    .local v0, "audioMissing":Z
    :goto_2
    if-nez v7, :cond_4

    if-eqz v0, :cond_a

    .line 134
    :cond_4
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v7, :cond_8

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget v12, v9, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, " "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    :goto_3
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    if-eqz v0, :cond_9

    iget v10, v2, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    :goto_4
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 137
    .local v4, "itags":Ljava/lang/String;
    new-instance v10, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "licensed format(s) is no longer permitted: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x1

    const/4 v13, 0x6

    invoke-direct {v10, v11, v12, v13}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    throw v10

    .line 122
    .end local v0    # "audioMissing":Z
    .end local v4    # "itags":Ljava/lang/String;
    .end local v7    # "videoMissing":Z
    .restart local v6    # "stream":Lcom/google/android/videos/streams/MediaStream;
    :cond_5
    iget-object v10, v6, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget v10, v10, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    iget v11, v2, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    if-ne v10, v11, :cond_2

    iget-object v10, v2, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    iget-object v11, v6, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-object v11, v11, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    const/4 v12, 0x0

    invoke-static {v10, v11, v12}, Lcom/google/android/videos/utils/AudioInfoUtil;->areEqual(Lcom/google/wireless/android/video/magma/proto/AudioInfo;Lcom/google/wireless/android/video/magma/proto/AudioInfo;Z)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 124
    iput-object v6, p0, Lcom/google/android/videos/pinning/DashDownloader;->audioStream:Lcom/google/android/videos/streams/MediaStream;

    goto :goto_0

    .line 131
    .end local v6    # "stream":Lcom/google/android/videos/streams/MediaStream;
    :cond_6
    const/4 v7, 0x0

    goto :goto_1

    .line 132
    .restart local v7    # "videoMissing":Z
    :cond_7
    const/4 v0, 0x0

    goto :goto_2

    .line 134
    .restart local v0    # "audioMissing":Z
    :cond_8
    const-string v10, ""

    goto :goto_3

    :cond_9
    const-string v10, ""

    goto :goto_4

    .line 141
    :cond_a
    iget-wide v10, v9, Lcom/google/android/videos/proto/StreamInfo;->lastModifiedTimestamp:J

    iget-object v12, p0, Lcom/google/android/videos/pinning/DashDownloader;->videoStream:Lcom/google/android/videos/streams/MediaStream;

    iget-object v12, v12, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-wide v12, v12, Lcom/google/android/videos/proto/StreamInfo;->lastModifiedTimestamp:J

    cmp-long v10, v10, v12

    if-eqz v10, :cond_c

    const/4 v8, 0x1

    .line 143
    .local v8, "videoModified":Z
    :goto_5
    iget-wide v10, v2, Lcom/google/android/videos/proto/StreamInfo;->lastModifiedTimestamp:J

    iget-object v12, p0, Lcom/google/android/videos/pinning/DashDownloader;->audioStream:Lcom/google/android/videos/streams/MediaStream;

    iget-object v12, v12, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-wide v12, v12, Lcom/google/android/videos/proto/StreamInfo;->lastModifiedTimestamp:J

    cmp-long v10, v10, v12

    if-eqz v10, :cond_d

    const/4 v1, 0x1

    .line 145
    .local v1, "audioModified":Z
    :goto_6
    if-nez v8, :cond_b

    if-eqz v1, :cond_10

    .line 146
    :cond_b
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v8, :cond_e

    const-string v10, "video "

    :goto_7
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    if-eqz v1, :cond_f

    const-string v10, "audio"

    :goto_8
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 147
    .local v5, "modified":Ljava/lang/String;
    new-instance v10, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Streams modified since the downloaded started "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x1

    const/16 v13, 0xe

    invoke-direct {v10, v11, v12, v13}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    throw v10

    .line 141
    .end local v1    # "audioModified":Z
    .end local v5    # "modified":Ljava/lang/String;
    .end local v8    # "videoModified":Z
    :cond_c
    const/4 v8, 0x0

    goto :goto_5

    .line 143
    .restart local v8    # "videoModified":Z
    :cond_d
    const/4 v1, 0x0

    goto :goto_6

    .line 146
    .restart local v1    # "audioModified":Z
    :cond_e
    const-string v10, ""

    goto :goto_7

    :cond_f
    const-string v10, ""

    goto :goto_8

    .line 150
    :cond_10
    return-void
.end method

.method private setupNewDownload(Lcom/google/android/videos/streams/Streams;)V
    .locals 13
    .param p1, "streams"    # Lcom/google/android/videos/streams/Streams;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/PinningTask$PinningException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 153
    iget-object v0, p0, Lcom/google/android/videos/pinning/DashDownloader;->key:Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v0, v0, Lcom/google/android/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/videos/pinning/DashDownloader;->key:Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v1, v1, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/OfflineUtil;->getDirPathForDashDownload(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pinning/DashDownloader;->relativeDownloadDir:Ljava/lang/String;

    .line 157
    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/pinning/DashDownloader;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    iget-object v1, p0, Lcom/google/android/videos/pinning/DashDownloader;->key:Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v1, v1, Lcom/google/android/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/pinning/DashDownloader;->key:Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v2, v2, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->getOfflineTaskInstance(Lcom/google/android/videos/VideosGlobals;Ljava/lang/String;Ljava/lang/String;[BIZ)Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;
    :try_end_0
    .catch Landroid/media/UnsupportedSchemeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    .line 165
    .local v9, "mediaDrmWrapper":Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;
    :try_start_1
    iget-object v0, p0, Lcom/google/android/videos/pinning/DashDownloader;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getDashStreamsSelector()Lcom/google/android/videos/streams/DashStreamsSelector;

    move-result-object v10

    .line 166
    .local v10, "streamsSelector":Lcom/google/android/videos/streams/DashStreamsSelector;
    iget-object v1, p1, Lcom/google/android/videos/streams/Streams;->mediaStreams:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/videos/pinning/DashDownloader;->details:Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;

    iget v2, v0, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->quality:I

    invoke-virtual {v9}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->getSecurityLevel()I

    move-result v0

    if-eq v0, v11, :cond_0

    move v0, v11

    :goto_0
    invoke-virtual {v10, v1, v2, v0}, Lcom/google/android/videos/streams/DashStreamsSelector;->getOfflineVideoStream(Ljava/util/List;IZ)Lcom/google/android/videos/streams/MediaStream;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pinning/DashDownloader;->videoStream:Lcom/google/android/videos/streams/MediaStream;

    .line 168
    iget-object v0, p1, Lcom/google/android/videos/streams/Streams;->mediaStreams:Ljava/util/List;

    iget-boolean v1, p0, Lcom/google/android/videos/pinning/DashDownloader;->surroundSound:Z

    invoke-virtual {v10, v0, v1}, Lcom/google/android/videos/streams/DashStreamsSelector;->getOfflineAudioStreams(Ljava/util/List;Z)Ljava/util/List;

    move-result-object v7

    .line 170
    .local v7, "audioStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    iget-object v0, p0, Lcom/google/android/videos/pinning/DashDownloader;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/videos/pinning/DashDownloader;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-static {v7, v0, v1}, Lcom/google/android/videos/utils/AudioInfoUtil;->getPreferredStreamIndex(Ljava/util/List;Landroid/content/Context;Landroid/content/SharedPreferences;)I

    move-result v6

    .line 172
    .local v6, "audioStreamIndex":I
    invoke-interface {v7, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/streams/MediaStream;

    iput-object v0, p0, Lcom/google/android/videos/pinning/DashDownloader;->audioStream:Lcom/google/android/videos/streams/MediaStream;
    :try_end_1
    .catch Lcom/google/android/videos/streams/MissingStreamException; {:try_start_1 .. :try_end_1} :catch_1

    .line 178
    iget-object v0, p0, Lcom/google/android/videos/pinning/DashDownloader;->videoStream:Lcom/google/android/videos/streams/MediaStream;

    iget-object v0, v0, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-wide v0, v0, Lcom/google/android/videos/proto/StreamInfo;->sizeInBytes:J

    iget-object v2, p0, Lcom/google/android/videos/pinning/DashDownloader;->audioStream:Lcom/google/android/videos/streams/MediaStream;

    iget-object v2, v2, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-wide v2, v2, Lcom/google/android/videos/proto/StreamInfo;->sizeInBytes:J

    add-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/videos/pinning/DashDownloader;->persistDownloadSize(J)V

    .line 179
    invoke-virtual {p0}, Lcom/google/android/videos/pinning/DashDownloader;->isCanceled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 180
    new-instance v0, Ljava/util/concurrent/CancellationException;

    invoke-direct {v0}, Ljava/util/concurrent/CancellationException;-><init>()V

    throw v0

    .line 159
    .end local v6    # "audioStreamIndex":I
    .end local v7    # "audioStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    .end local v9    # "mediaDrmWrapper":Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;
    .end local v10    # "streamsSelector":Lcom/google/android/videos/streams/DashStreamsSelector;
    :catch_0
    move-exception v8

    .line 160
    .local v8, "e":Landroid/media/UnsupportedSchemeException;
    new-instance v0, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    const-string v1, "Error when acquiring license"

    invoke-direct {v0, v1, v11, v11}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    throw v0

    .end local v8    # "e":Landroid/media/UnsupportedSchemeException;
    .restart local v9    # "mediaDrmWrapper":Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;
    .restart local v10    # "streamsSelector":Lcom/google/android/videos/streams/DashStreamsSelector;
    :cond_0
    move v0, v12

    .line 166
    goto :goto_0

    .line 173
    .end local v10    # "streamsSelector":Lcom/google/android/videos/streams/DashStreamsSelector;
    :catch_1
    move-exception v8

    .line 174
    .local v8, "e":Lcom/google/android/videos/streams/MissingStreamException;
    new-instance v0, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    const-string v1, "Failed to obtain valid streams"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v11, v2}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;ZI)V

    throw v0

    .line 182
    .end local v8    # "e":Lcom/google/android/videos/streams/MissingStreamException;
    .restart local v6    # "audioStreamIndex":I
    .restart local v7    # "audioStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    .restart local v10    # "streamsSelector":Lcom/google/android/videos/streams/DashStreamsSelector;
    :cond_1
    invoke-direct {p0, v9}, Lcom/google/android/videos/pinning/DashDownloader;->acquireLicense(Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;)V

    .line 183
    return-void
.end method


# virtual methods
.method public downloadMedia()V
    .locals 35
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/PinningTask$PinningException;
        }
    .end annotation

    .prologue
    .line 299
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/pinning/DashDownloader;->relativeDownloadDir:Ljava/lang/String;

    const-string v6, "Download started"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v6}, Lcom/google/android/videos/pinning/DashDownloader;->debug(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    new-instance v22, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/pinning/DashDownloader;->rootFilesDir:Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/pinning/DashDownloader;->relativeDownloadDir:Ljava/lang/String;

    move-object/from16 v0, v22

    invoke-direct {v0, v3, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 301
    .local v22, "downloadDir":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/pinning/DashDownloader;->exoCacheProvider:Lcom/google/android/videos/pinning/ExoCacheProvider;

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Lcom/google/android/videos/pinning/ExoCacheProvider;->acquireDownloadCache(Ljava/io/File;)Lcom/google/android/exoplayer/upstream/cache/Cache;

    move-result-object v12

    .line 302
    .local v12, "cache":Lcom/google/android/exoplayer/upstream/cache/Cache;
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/google/android/videos/pinning/DashDownloader;->buildDownloadDataSource(Lcom/google/android/exoplayer/upstream/cache/Cache;)Lcom/google/android/exoplayer/upstream/DataSource;

    move-result-object v4

    .line 303
    .local v4, "dataSource":Lcom/google/android/exoplayer/upstream/DataSource;
    sget-object v3, Lcom/google/android/exoplayer/upstream/NetworkLock;->instance:Lcom/google/android/exoplayer/upstream/NetworkLock;

    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Lcom/google/android/exoplayer/upstream/NetworkLock;->add(I)V

    .line 305
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/pinning/DashDownloader;->videoStream:Lcom/google/android/videos/streams/MediaStream;

    iget-object v3, v3, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-wide v6, v3, Lcom/google/android/videos/proto/StreamInfo;->sizeInBytes:J

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/pinning/DashDownloader;->audioStream:Lcom/google/android/videos/streams/MediaStream;

    iget-object v3, v3, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-wide v0, v3, Lcom/google/android/videos/proto/StreamInfo;->sizeInBytes:J

    move-wide/from16 v16, v0

    add-long v30, v6, v16

    .line 306
    .local v30, "totalSize":J
    const-wide/16 v6, 0x64

    div-long v6, v30, v6

    const-wide/32 v16, 0x200000

    move-wide/from16 v0, v16

    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v26

    .line 308
    .local v26, "progressGranularity":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/pinning/DashDownloader;->videoStream:Lcom/google/android/videos/streams/MediaStream;

    const-string v6, "video/"

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/videos/pinning/DashDownloader;->key:Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v7, v7, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/videos/pinning/DashDownloader;->details:Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;

    iget-wide v0, v13, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->durationMs:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-static {v3, v6, v7, v0, v1}, Lcom/google/android/videos/player/exo/ExoPlayerUtil;->toRepresentation(Lcom/google/android/videos/streams/MediaStream;Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    move-result-object v9

    .line 310
    .local v9, "videoRepresentation":Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/pinning/DashDownloader;->videoStream:Lcom/google/android/videos/streams/MediaStream;

    invoke-static {v4, v9, v3}, Lcom/google/android/videos/player/exo/ExoPlayerUtil;->prepareExtractor(Lcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;Lcom/google/android/videos/streams/MediaStream;)Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;

    move-result-object v34

    .line 312
    .local v34, "videoExtractor":Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    invoke-virtual/range {v34 .. v34}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->getIndex()Lcom/google/android/exoplayer/parser/SegmentIndex;

    move-result-object v10

    .line 313
    .local v10, "videoSegmentIndex":Lcom/google/android/exoplayer/parser/SegmentIndex;
    iget-object v5, v9, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->uri:Landroid/net/Uri;

    .line 315
    .local v5, "videoUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/pinning/DashDownloader;->audioStream:Lcom/google/android/videos/streams/MediaStream;

    const-string v6, "audio/"

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/videos/pinning/DashDownloader;->key:Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v7, v7, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/videos/pinning/DashDownloader;->details:Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;

    iget-wide v0, v13, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->durationMs:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-static {v3, v6, v7, v0, v1}, Lcom/google/android/videos/player/exo/ExoPlayerUtil;->toRepresentation(Lcom/google/android/videos/streams/MediaStream;Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    move-result-object v19

    .line 317
    .local v19, "audioRepresentation":Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/pinning/DashDownloader;->audioStream:Lcom/google/android/videos/streams/MediaStream;

    move-object/from16 v0, v19

    invoke-static {v4, v0, v3}, Lcom/google/android/videos/player/exo/ExoPlayerUtil;->prepareExtractor(Lcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;Lcom/google/android/videos/streams/MediaStream;)Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;

    move-result-object v2

    .line 319
    .local v2, "audioExtractor":Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    invoke-virtual {v2}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->getIndex()Lcom/google/android/exoplayer/parser/SegmentIndex;

    move-result-object v20

    .line 320
    .local v20, "audioSegmentIndex":Lcom/google/android/exoplayer/parser/SegmentIndex;
    move-object/from16 v0, v19

    iget-object v15, v0, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->uri:Landroid/net/Uri;

    .line 322
    .local v15, "audioUri":Landroid/net/Uri;
    const/high16 v3, 0x20000

    new-array v8, v3, [B

    .line 326
    .local v8, "buffer":[B
    const/4 v11, 0x0

    .line 327
    .local v11, "nextVideoSegment":I
    const/16 v21, 0x0

    .line 329
    .local v21, "nextAudioSegment":I
    :goto_0
    iget v3, v10, Lcom/google/android/exoplayer/parser/SegmentIndex;->length:I

    if-ge v11, v3, :cond_1

    .line 331
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/pinning/DashDownloader;->videoStream:Lcom/google/android/videos/streams/MediaStream;

    iget-object v3, v3, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-wide v6, v3, Lcom/google/android/videos/proto/StreamInfo;->dashIndexEnd:J

    const-wide/16 v16, 0x1

    add-long v6, v6, v16

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v11}, Lcom/google/android/videos/pinning/DashDownloader;->downloadChunk(Lcom/google/android/exoplayer/upstream/DataSource;Landroid/net/Uri;J[BLcom/google/android/exoplayer/dash/mpd/Representation;Lcom/google/android/exoplayer/parser/SegmentIndex;I)I

    move-result v11

    .line 336
    iget-object v3, v10, Lcom/google/android/exoplayer/parser/SegmentIndex;->timesUs:[J

    add-int/lit8 v6, v11, -0x1

    aget-wide v6, v3, v6

    iget-object v3, v10, Lcom/google/android/exoplayer/parser/SegmentIndex;->durationsUs:[J

    add-int/lit8 v13, v11, -0x1

    aget-wide v16, v3, v13

    add-long v32, v6, v16

    .line 340
    .local v32, "videoChunkTimeUs":J
    :goto_1
    move-object/from16 v0, v20

    iget v3, v0, Lcom/google/android/exoplayer/parser/SegmentIndex;->length:I

    move/from16 v0, v21

    if-ge v0, v3, :cond_0

    move-object/from16 v0, v20

    iget-object v3, v0, Lcom/google/android/exoplayer/parser/SegmentIndex;->timesUs:[J

    aget-wide v6, v3, v21

    cmp-long v3, v6, v32

    if-gtz v3, :cond_0

    .line 341
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/pinning/DashDownloader;->audioStream:Lcom/google/android/videos/streams/MediaStream;

    iget-object v3, v3, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-wide v6, v3, Lcom/google/android/videos/proto/StreamInfo;->dashIndexEnd:J

    const-wide/16 v16, 0x1

    add-long v16, v16, v6

    move-object/from16 v13, p0

    move-object v14, v4

    move-object/from16 v18, v8

    invoke-direct/range {v13 .. v21}, Lcom/google/android/videos/pinning/DashDownloader;->downloadChunk(Lcom/google/android/exoplayer/upstream/DataSource;Landroid/net/Uri;J[BLcom/google/android/exoplayer/dash/mpd/Representation;Lcom/google/android/exoplayer/parser/SegmentIndex;I)I

    move-result v21

    goto :goto_1

    .line 347
    :cond_0
    invoke-interface {v12}, Lcom/google/android/exoplayer/upstream/cache/Cache;->getCacheSpace()J

    move-result-wide v24

    const/16 v28, 0x0

    move-object/from16 v23, p0

    invoke-virtual/range {v23 .. v28}, Lcom/google/android/videos/pinning/DashDownloader;->doProgress(JJZ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 362
    .end local v2    # "audioExtractor":Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    .end local v5    # "videoUri":Landroid/net/Uri;
    .end local v8    # "buffer":[B
    .end local v9    # "videoRepresentation":Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    .end local v10    # "videoSegmentIndex":Lcom/google/android/exoplayer/parser/SegmentIndex;
    .end local v11    # "nextVideoSegment":I
    .end local v15    # "audioUri":Landroid/net/Uri;
    .end local v19    # "audioRepresentation":Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    .end local v20    # "audioSegmentIndex":Lcom/google/android/exoplayer/parser/SegmentIndex;
    .end local v21    # "nextAudioSegment":I
    .end local v26    # "progressGranularity":J
    .end local v30    # "totalSize":J
    .end local v32    # "videoChunkTimeUs":J
    .end local v34    # "videoExtractor":Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    :catch_0
    move-exception v29

    .line 363
    .local v29, "e":Ljava/io/IOException;
    :try_start_1
    new-instance v3, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    const-string v6, "I/O exception while downloading video"

    const/4 v7, 0x0

    const/16 v13, 0xe

    move-object/from16 v0, v29

    invoke-direct {v3, v6, v0, v7, v13}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;ZI)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 369
    .end local v29    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    sget-object v6, Lcom/google/android/exoplayer/upstream/NetworkLock;->instance:Lcom/google/android/exoplayer/upstream/NetworkLock;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/google/android/exoplayer/upstream/NetworkLock;->remove(I)V

    .line 371
    :try_start_2
    invoke-interface {v4}, Lcom/google/android/exoplayer/upstream/DataSource;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    .line 374
    :goto_2
    throw v3

    .line 351
    .restart local v2    # "audioExtractor":Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    .restart local v5    # "videoUri":Landroid/net/Uri;
    .restart local v8    # "buffer":[B
    .restart local v9    # "videoRepresentation":Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    .restart local v10    # "videoSegmentIndex":Lcom/google/android/exoplayer/parser/SegmentIndex;
    .restart local v11    # "nextVideoSegment":I
    .restart local v15    # "audioUri":Landroid/net/Uri;
    .restart local v19    # "audioRepresentation":Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    .restart local v20    # "audioSegmentIndex":Lcom/google/android/exoplayer/parser/SegmentIndex;
    .restart local v21    # "nextAudioSegment":I
    .restart local v26    # "progressGranularity":J
    .restart local v30    # "totalSize":J
    .restart local v34    # "videoExtractor":Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    :cond_1
    :goto_3
    :try_start_3
    move-object/from16 v0, v20

    iget v3, v0, Lcom/google/android/exoplayer/parser/SegmentIndex;->length:I

    move/from16 v0, v21

    if-ge v0, v3, :cond_2

    .line 352
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/pinning/DashDownloader;->audioStream:Lcom/google/android/videos/streams/MediaStream;

    iget-object v3, v3, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-wide v6, v3, Lcom/google/android/videos/proto/StreamInfo;->dashIndexEnd:J

    const-wide/16 v16, 0x1

    add-long v16, v16, v6

    move-object/from16 v13, p0

    move-object v14, v4

    move-object/from16 v18, v8

    invoke-direct/range {v13 .. v21}, Lcom/google/android/videos/pinning/DashDownloader;->downloadChunk(Lcom/google/android/exoplayer/upstream/DataSource;Landroid/net/Uri;J[BLcom/google/android/exoplayer/dash/mpd/Representation;Lcom/google/android/exoplayer/parser/SegmentIndex;I)I

    move-result v21

    .line 355
    invoke-interface {v12}, Lcom/google/android/exoplayer/upstream/cache/Cache;->getCacheSpace()J

    move-result-wide v24

    const/16 v28, 0x0

    move-object/from16 v23, p0

    invoke-virtual/range {v23 .. v28}, Lcom/google/android/videos/pinning/DashDownloader;->doProgress(JJZ)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 366
    .end local v2    # "audioExtractor":Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    .end local v5    # "videoUri":Landroid/net/Uri;
    .end local v8    # "buffer":[B
    .end local v9    # "videoRepresentation":Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    .end local v10    # "videoSegmentIndex":Lcom/google/android/exoplayer/parser/SegmentIndex;
    .end local v11    # "nextVideoSegment":I
    .end local v15    # "audioUri":Landroid/net/Uri;
    .end local v19    # "audioRepresentation":Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    .end local v20    # "audioSegmentIndex":Lcom/google/android/exoplayer/parser/SegmentIndex;
    .end local v21    # "nextAudioSegment":I
    .end local v26    # "progressGranularity":J
    .end local v30    # "totalSize":J
    .end local v34    # "videoExtractor":Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    :catch_1
    move-exception v3

    .line 369
    sget-object v3, Lcom/google/android/exoplayer/upstream/NetworkLock;->instance:Lcom/google/android/exoplayer/upstream/NetworkLock;

    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Lcom/google/android/exoplayer/upstream/NetworkLock;->remove(I)V

    .line 371
    :try_start_4
    invoke-interface {v4}, Lcom/google/android/exoplayer/upstream/DataSource;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 376
    :goto_4
    return-void

    .line 359
    .restart local v2    # "audioExtractor":Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    .restart local v5    # "videoUri":Landroid/net/Uri;
    .restart local v8    # "buffer":[B
    .restart local v9    # "videoRepresentation":Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    .restart local v10    # "videoSegmentIndex":Lcom/google/android/exoplayer/parser/SegmentIndex;
    .restart local v11    # "nextVideoSegment":I
    .restart local v15    # "audioUri":Landroid/net/Uri;
    .restart local v19    # "audioRepresentation":Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    .restart local v20    # "audioSegmentIndex":Lcom/google/android/exoplayer/parser/SegmentIndex;
    .restart local v21    # "nextAudioSegment":I
    .restart local v26    # "progressGranularity":J
    .restart local v30    # "totalSize":J
    .restart local v34    # "videoExtractor":Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    :cond_2
    :try_start_5
    invoke-interface {v12}, Lcom/google/android/exoplayer/upstream/cache/Cache;->getCacheSpace()J

    move-result-wide v24

    const/16 v28, 0x1

    move-object/from16 v23, p0

    invoke-virtual/range {v23 .. v28}, Lcom/google/android/videos/pinning/DashDownloader;->doProgress(JJZ)V

    .line 361
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/pinning/DashDownloader;->relativeDownloadDir:Ljava/lang/String;

    const-string v6, "Download completed"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v6}, Lcom/google/android/videos/pinning/DashDownloader;->debug(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 369
    sget-object v3, Lcom/google/android/exoplayer/upstream/NetworkLock;->instance:Lcom/google/android/exoplayer/upstream/NetworkLock;

    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Lcom/google/android/exoplayer/upstream/NetworkLock;->remove(I)V

    .line 371
    :try_start_6
    invoke-interface {v4}, Lcom/google/android/exoplayer/upstream/DataSource;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_4

    .line 372
    :catch_2
    move-exception v29

    .line 373
    .restart local v29    # "e":Ljava/io/IOException;
    const-string v3, "Failed to close data source"

    move-object/from16 v0, v29

    invoke-static {v3, v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 372
    .end local v2    # "audioExtractor":Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    .end local v5    # "videoUri":Landroid/net/Uri;
    .end local v8    # "buffer":[B
    .end local v9    # "videoRepresentation":Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    .end local v10    # "videoSegmentIndex":Lcom/google/android/exoplayer/parser/SegmentIndex;
    .end local v11    # "nextVideoSegment":I
    .end local v15    # "audioUri":Landroid/net/Uri;
    .end local v19    # "audioRepresentation":Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    .end local v20    # "audioSegmentIndex":Lcom/google/android/exoplayer/parser/SegmentIndex;
    .end local v21    # "nextAudioSegment":I
    .end local v26    # "progressGranularity":J
    .end local v29    # "e":Ljava/io/IOException;
    .end local v30    # "totalSize":J
    .end local v34    # "videoExtractor":Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    :catch_3
    move-exception v29

    .line 373
    .restart local v29    # "e":Ljava/io/IOException;
    const-string v3, "Failed to close data source"

    move-object/from16 v0, v29

    invoke-static {v3, v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 372
    .end local v29    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v29

    .line 373
    .restart local v29    # "e":Ljava/io/IOException;
    const-string v6, "Failed to close data source"

    move-object/from16 v0, v29

    invoke-static {v6, v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method public setupDownload()Lcom/google/android/videos/pinning/Downloader$SetupResult;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/PinningTask$PinningException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 89
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/videos/pinning/DashDownloader;->details:Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;

    iget-boolean v2, v2, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->isEpisode:Z

    invoke-virtual {p0, v1, v2}, Lcom/google/android/videos/pinning/DashDownloader;->getStreams(ZZ)Lcom/google/android/videos/streams/Streams;

    move-result-object v8

    .line 90
    .local v8, "streams":Lcom/google/android/videos/streams/Streams;
    invoke-virtual {p0}, Lcom/google/android/videos/pinning/DashDownloader;->isCanceled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 105
    :cond_0
    :goto_0
    return-object v0

    .line 93
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/pinning/DashDownloader;->details:Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;

    iget v1, v1, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->licenseType:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 94
    invoke-direct {p0, v8}, Lcom/google/android/videos/pinning/DashDownloader;->setupExistingDownload(Lcom/google/android/videos/streams/Streams;)V

    .line 98
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/videos/pinning/DashDownloader;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 102
    iget-object v0, p0, Lcom/google/android/videos/pinning/DashDownloader;->videoStream:Lcom/google/android/videos/streams/MediaStream;

    iget-object v0, v0, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-wide v0, v0, Lcom/google/android/videos/proto/StreamInfo;->sizeInBytes:J

    iget-object v2, p0, Lcom/google/android/videos/pinning/DashDownloader;->audioStream:Lcom/google/android/videos/streams/MediaStream;

    iget-object v2, v2, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-wide v2, v2, Lcom/google/android/videos/proto/StreamInfo;->sizeInBytes:J

    add-long v6, v0, v2

    .line 103
    .local v6, "size":J
    iget-object v0, p0, Lcom/google/android/videos/pinning/DashDownloader;->relativeDownloadDir:Ljava/lang/String;

    invoke-virtual {p0, v6, v7, v0}, Lcom/google/android/videos/pinning/DashDownloader;->checkSufficientFreeSpace(JLjava/lang/String;)V

    .line 105
    new-instance v0, Lcom/google/android/videos/pinning/Downloader$SetupResult;

    iget-object v1, p0, Lcom/google/android/videos/pinning/DashDownloader;->videoStream:Lcom/google/android/videos/streams/MediaStream;

    invoke-direct {p0, v8}, Lcom/google/android/videos/pinning/DashDownloader;->getDashStreamTimestampForKnowledge(Lcom/google/android/videos/streams/Streams;)J

    move-result-wide v2

    iget-object v4, v8, Lcom/google/android/videos/streams/Streams;->captions:Ljava/util/List;

    iget-object v5, p0, Lcom/google/android/videos/pinning/DashDownloader;->context:Landroid/content/Context;

    iget-object v9, v8, Lcom/google/android/videos/streams/Streams;->storyboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    invoke-static {v5, v9}, Lcom/google/android/videos/utils/Util;->getBestStoryboard(Landroid/content/Context;[Lcom/google/wireless/android/video/magma/proto/Storyboard;)Lcom/google/wireless/android/video/magma/proto/Storyboard;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/pinning/Downloader$SetupResult;-><init>(Lcom/google/android/videos/streams/MediaStream;JLjava/util/List;Lcom/google/wireless/android/video/magma/proto/Storyboard;)V

    goto :goto_0

    .line 96
    .end local v6    # "size":J
    :cond_2
    invoke-direct {p0, v8}, Lcom/google/android/videos/pinning/DashDownloader;->setupNewDownload(Lcom/google/android/videos/streams/Streams;)V

    goto :goto_1
.end method
