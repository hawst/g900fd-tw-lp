.class public final Lcom/google/android/videos/proto/StreamInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "StreamInfo.java"


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/videos/proto/StreamInfo;


# instance fields
.field public audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

.field public cencPsshDataDEPRECATED:[B

.field public codec:Ljava/lang/String;

.field public dashIndexEnd:J

.field public dashIndexStart:J

.field public dashInitEnd:J

.field public dashInitStart:J

.field public itag:I

.field public lastModifiedTimestamp:J

.field public sizeInBytes:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 54
    invoke-virtual {p0}, Lcom/google/android/videos/proto/StreamInfo;->clear()Lcom/google/android/videos/proto/StreamInfo;

    .line 55
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/videos/proto/StreamInfo;
    .locals 2

    .prologue
    .line 12
    sget-object v0, Lcom/google/android/videos/proto/StreamInfo;->_emptyArray:[Lcom/google/android/videos/proto/StreamInfo;

    if-nez v0, :cond_1

    .line 13
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 15
    :try_start_0
    sget-object v0, Lcom/google/android/videos/proto/StreamInfo;->_emptyArray:[Lcom/google/android/videos/proto/StreamInfo;

    if-nez v0, :cond_0

    .line 16
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/videos/proto/StreamInfo;

    sput-object v0, Lcom/google/android/videos/proto/StreamInfo;->_emptyArray:[Lcom/google/android/videos/proto/StreamInfo;

    .line 18
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    :cond_1
    sget-object v0, Lcom/google/android/videos/proto/StreamInfo;->_emptyArray:[Lcom/google/android/videos/proto/StreamInfo;

    return-object v0

    .line 18
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/videos/proto/StreamInfo;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 58
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    .line 59
    iput-wide v2, p0, Lcom/google/android/videos/proto/StreamInfo;->sizeInBytes:J

    .line 60
    iput-wide v2, p0, Lcom/google/android/videos/proto/StreamInfo;->dashIndexEnd:J

    .line 61
    iput-wide v2, p0, Lcom/google/android/videos/proto/StreamInfo;->lastModifiedTimestamp:J

    .line 62
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/videos/proto/StreamInfo;->cencPsshDataDEPRECATED:[B

    .line 63
    iput-wide v2, p0, Lcom/google/android/videos/proto/StreamInfo;->dashInitStart:J

    .line 64
    iput-wide v2, p0, Lcom/google/android/videos/proto/StreamInfo;->dashInitEnd:J

    .line 65
    iput-wide v2, p0, Lcom/google/android/videos/proto/StreamInfo;->dashIndexStart:J

    .line 66
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/videos/proto/StreamInfo;->codec:Ljava/lang/String;

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    .line 68
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/proto/StreamInfo;->cachedSize:I

    .line 69
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 186
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 187
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    if-eqz v1, :cond_0

    .line 188
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 191
    :cond_0
    iget-wide v2, p0, Lcom/google/android/videos/proto/StreamInfo;->sizeInBytes:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 192
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/videos/proto/StreamInfo;->sizeInBytes:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 195
    :cond_1
    iget-wide v2, p0, Lcom/google/android/videos/proto/StreamInfo;->dashIndexEnd:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 196
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/videos/proto/StreamInfo;->dashIndexEnd:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 199
    :cond_2
    iget-wide v2, p0, Lcom/google/android/videos/proto/StreamInfo;->lastModifiedTimestamp:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 200
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/videos/proto/StreamInfo;->lastModifiedTimestamp:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 203
    :cond_3
    iget-object v1, p0, Lcom/google/android/videos/proto/StreamInfo;->cencPsshDataDEPRECATED:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_4

    .line 204
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/videos/proto/StreamInfo;->cencPsshDataDEPRECATED:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 207
    :cond_4
    iget-wide v2, p0, Lcom/google/android/videos/proto/StreamInfo;->dashInitStart:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    .line 208
    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/google/android/videos/proto/StreamInfo;->dashInitStart:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 211
    :cond_5
    iget-wide v2, p0, Lcom/google/android/videos/proto/StreamInfo;->dashInitEnd:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_6

    .line 212
    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/google/android/videos/proto/StreamInfo;->dashInitEnd:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 215
    :cond_6
    iget-wide v2, p0, Lcom/google/android/videos/proto/StreamInfo;->dashIndexStart:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_7

    .line 216
    const/16 v1, 0x8

    iget-wide v2, p0, Lcom/google/android/videos/proto/StreamInfo;->dashIndexStart:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 219
    :cond_7
    iget-object v1, p0, Lcom/google/android/videos/proto/StreamInfo;->codec:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 220
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/videos/proto/StreamInfo;->codec:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 223
    :cond_8
    iget-object v1, p0, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    if-eqz v1, :cond_9

    .line 224
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 227
    :cond_9
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 74
    if-ne p1, p0, :cond_1

    .line 121
    :cond_0
    :goto_0
    return v1

    .line 77
    :cond_1
    instance-of v3, p1, Lcom/google/android/videos/proto/StreamInfo;

    if-nez v3, :cond_2

    move v1, v2

    .line 78
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 80
    check-cast v0, Lcom/google/android/videos/proto/StreamInfo;

    .line 81
    .local v0, "other":Lcom/google/android/videos/proto/StreamInfo;
    iget v3, p0, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    iget v4, v0, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 82
    goto :goto_0

    .line 84
    :cond_3
    iget-wide v4, p0, Lcom/google/android/videos/proto/StreamInfo;->sizeInBytes:J

    iget-wide v6, v0, Lcom/google/android/videos/proto/StreamInfo;->sizeInBytes:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_4

    move v1, v2

    .line 85
    goto :goto_0

    .line 87
    :cond_4
    iget-wide v4, p0, Lcom/google/android/videos/proto/StreamInfo;->dashIndexEnd:J

    iget-wide v6, v0, Lcom/google/android/videos/proto/StreamInfo;->dashIndexEnd:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_5

    move v1, v2

    .line 88
    goto :goto_0

    .line 90
    :cond_5
    iget-wide v4, p0, Lcom/google/android/videos/proto/StreamInfo;->lastModifiedTimestamp:J

    iget-wide v6, v0, Lcom/google/android/videos/proto/StreamInfo;->lastModifiedTimestamp:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_6

    move v1, v2

    .line 91
    goto :goto_0

    .line 93
    :cond_6
    iget-object v3, p0, Lcom/google/android/videos/proto/StreamInfo;->cencPsshDataDEPRECATED:[B

    iget-object v4, v0, Lcom/google/android/videos/proto/StreamInfo;->cencPsshDataDEPRECATED:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_7

    move v1, v2

    .line 94
    goto :goto_0

    .line 96
    :cond_7
    iget-wide v4, p0, Lcom/google/android/videos/proto/StreamInfo;->dashInitStart:J

    iget-wide v6, v0, Lcom/google/android/videos/proto/StreamInfo;->dashInitStart:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_8

    move v1, v2

    .line 97
    goto :goto_0

    .line 99
    :cond_8
    iget-wide v4, p0, Lcom/google/android/videos/proto/StreamInfo;->dashInitEnd:J

    iget-wide v6, v0, Lcom/google/android/videos/proto/StreamInfo;->dashInitEnd:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_9

    move v1, v2

    .line 100
    goto :goto_0

    .line 102
    :cond_9
    iget-wide v4, p0, Lcom/google/android/videos/proto/StreamInfo;->dashIndexStart:J

    iget-wide v6, v0, Lcom/google/android/videos/proto/StreamInfo;->dashIndexStart:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_a

    move v1, v2

    .line 103
    goto :goto_0

    .line 105
    :cond_a
    iget-object v3, p0, Lcom/google/android/videos/proto/StreamInfo;->codec:Ljava/lang/String;

    if-nez v3, :cond_b

    .line 106
    iget-object v3, v0, Lcom/google/android/videos/proto/StreamInfo;->codec:Ljava/lang/String;

    if-eqz v3, :cond_c

    move v1, v2

    .line 107
    goto :goto_0

    .line 109
    :cond_b
    iget-object v3, p0, Lcom/google/android/videos/proto/StreamInfo;->codec:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/proto/StreamInfo;->codec:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    move v1, v2

    .line 110
    goto :goto_0

    .line 112
    :cond_c
    iget-object v3, p0, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    if-nez v3, :cond_d

    .line 113
    iget-object v3, v0, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    if-eqz v3, :cond_0

    move v1, v2

    .line 114
    goto :goto_0

    .line 117
    :cond_d
    iget-object v3, p0, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    iget-object v4, v0, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 118
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/16 v8, 0x20

    .line 126
    const/16 v0, 0x11

    .line 127
    .local v0, "result":I
    iget v1, p0, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    add-int/lit16 v0, v1, 0x20f

    .line 128
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v4, p0, Lcom/google/android/videos/proto/StreamInfo;->sizeInBytes:J

    iget-wide v6, p0, Lcom/google/android/videos/proto/StreamInfo;->sizeInBytes:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int v0, v1, v3

    .line 130
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v4, p0, Lcom/google/android/videos/proto/StreamInfo;->dashIndexEnd:J

    iget-wide v6, p0, Lcom/google/android/videos/proto/StreamInfo;->dashIndexEnd:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int v0, v1, v3

    .line 132
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v4, p0, Lcom/google/android/videos/proto/StreamInfo;->lastModifiedTimestamp:J

    iget-wide v6, p0, Lcom/google/android/videos/proto/StreamInfo;->lastModifiedTimestamp:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int v0, v1, v3

    .line 134
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/videos/proto/StreamInfo;->cencPsshDataDEPRECATED:[B

    invoke-static {v3}, Ljava/util/Arrays;->hashCode([B)I

    move-result v3

    add-int v0, v1, v3

    .line 135
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v4, p0, Lcom/google/android/videos/proto/StreamInfo;->dashInitStart:J

    iget-wide v6, p0, Lcom/google/android/videos/proto/StreamInfo;->dashInitStart:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int v0, v1, v3

    .line 137
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v4, p0, Lcom/google/android/videos/proto/StreamInfo;->dashInitEnd:J

    iget-wide v6, p0, Lcom/google/android/videos/proto/StreamInfo;->dashInitEnd:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int v0, v1, v3

    .line 139
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v4, p0, Lcom/google/android/videos/proto/StreamInfo;->dashIndexStart:J

    iget-wide v6, p0, Lcom/google/android/videos/proto/StreamInfo;->dashIndexStart:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int v0, v1, v3

    .line 141
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/videos/proto/StreamInfo;->codec:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 143
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    if-nez v3, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 145
    return v0

    .line 141
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/proto/StreamInfo;->codec:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 143
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    invoke-virtual {v2}, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/StreamInfo;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 235
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 236
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 240
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 241
    :sswitch_0
    return-object p0

    .line 246
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    goto :goto_0

    .line 250
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/videos/proto/StreamInfo;->sizeInBytes:J

    goto :goto_0

    .line 254
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/videos/proto/StreamInfo;->dashIndexEnd:J

    goto :goto_0

    .line 258
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/videos/proto/StreamInfo;->lastModifiedTimestamp:J

    goto :goto_0

    .line 262
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/proto/StreamInfo;->cencPsshDataDEPRECATED:[B

    goto :goto_0

    .line 266
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/videos/proto/StreamInfo;->dashInitStart:J

    goto :goto_0

    .line 270
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/videos/proto/StreamInfo;->dashInitEnd:J

    goto :goto_0

    .line 274
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/videos/proto/StreamInfo;->dashIndexStart:J

    goto :goto_0

    .line 278
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/proto/StreamInfo;->codec:Ljava/lang/String;

    goto :goto_0

    .line 282
    :sswitch_a
    iget-object v1, p0, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    if-nez v1, :cond_1

    .line 283
    new-instance v1, Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    invoke-direct {v1}, Lcom/google/wireless/android/video/magma/proto/AudioInfo;-><init>()V

    iput-object v1, p0, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    .line 285
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 236
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/android/videos/proto/StreamInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/StreamInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 151
    iget v0, p0, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    if-eqz v0, :cond_0

    .line 152
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 154
    :cond_0
    iget-wide v0, p0, Lcom/google/android/videos/proto/StreamInfo;->sizeInBytes:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 155
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/videos/proto/StreamInfo;->sizeInBytes:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 157
    :cond_1
    iget-wide v0, p0, Lcom/google/android/videos/proto/StreamInfo;->dashIndexEnd:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 158
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/videos/proto/StreamInfo;->dashIndexEnd:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 160
    :cond_2
    iget-wide v0, p0, Lcom/google/android/videos/proto/StreamInfo;->lastModifiedTimestamp:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 161
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/android/videos/proto/StreamInfo;->lastModifiedTimestamp:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 163
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/proto/StreamInfo;->cencPsshDataDEPRECATED:[B

    sget-object v1, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_4

    .line 164
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/videos/proto/StreamInfo;->cencPsshDataDEPRECATED:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 166
    :cond_4
    iget-wide v0, p0, Lcom/google/android/videos/proto/StreamInfo;->dashInitStart:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_5

    .line 167
    const/4 v0, 0x6

    iget-wide v2, p0, Lcom/google/android/videos/proto/StreamInfo;->dashInitStart:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 169
    :cond_5
    iget-wide v0, p0, Lcom/google/android/videos/proto/StreamInfo;->dashInitEnd:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_6

    .line 170
    const/4 v0, 0x7

    iget-wide v2, p0, Lcom/google/android/videos/proto/StreamInfo;->dashInitEnd:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 172
    :cond_6
    iget-wide v0, p0, Lcom/google/android/videos/proto/StreamInfo;->dashIndexStart:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_7

    .line 173
    const/16 v0, 0x8

    iget-wide v2, p0, Lcom/google/android/videos/proto/StreamInfo;->dashIndexStart:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 175
    :cond_7
    iget-object v0, p0, Lcom/google/android/videos/proto/StreamInfo;->codec:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 176
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/videos/proto/StreamInfo;->codec:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 178
    :cond_8
    iget-object v0, p0, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    if-eqz v0, :cond_9

    .line 179
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 181
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 182
    return-void
.end method
