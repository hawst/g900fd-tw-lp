.class public Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;
.super Lcom/google/android/videos/ui/playnext/CardItemView;
.source "EpisodeClusterItemView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView$DetailsViewBinder;,
        Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView$Binder;
    }
.end annotation


# instance fields
.field private downloadView:Lcom/google/android/videos/ui/DownloadView;

.field private getMoreButtonView:Landroid/widget/TextView;

.field private overflowView:Landroid/view/View;

.field private priceView:Landroid/widget/TextView;

.field private releaseDateView:Landroid/widget/TextView;

.field private showPosterBitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

.field private showPosterRequest:Ljava/lang/String;

.field private showThumbnailView:Landroid/widget/ImageView;

.field private statusTextView:Landroid/widget/TextView;

.field private synopsisView:Landroid/view/View;

.field private thumbnailBitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

.field private thumbnailRequest:Ljava/lang/Object;

.field private titleView:Landroid/widget/TextView;

.field private watchProgress:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 56
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/ui/playnext/CardItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 65
    return-void
.end method

.method private static setVisibility(Landroid/view/View;I)V
    .locals 0
    .param p0, "view"    # Landroid/view/View;
    .param p1, "visibility"    # I

    .prologue
    .line 180
    if-eqz p0, :cond_0

    .line 181
    invoke-virtual {p0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 183
    :cond_0
    return-void
.end method

.method private updateViewForEpisode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ)V
    .locals 5
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "showTitle"    # Ljava/lang/String;
    .param p3, "statusText"    # Ljava/lang/String;
    .param p4, "dimmed"    # Z
    .param p5, "publishTimestampSeconds"    # J

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 135
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->titleView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 136
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->titleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->showThumbnailView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->showThumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 139
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->showThumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 141
    :cond_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 142
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->statusTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 148
    :goto_0
    const-wide/16 v0, 0x0

    cmp-long v0, p5, v0

    if-nez v0, :cond_3

    .line 149
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->releaseDateView:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->setVisibility(Landroid/view/View;I)V

    .line 155
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->getMoreButtonView:Landroid/widget/TextView;

    invoke-static {v0, v3}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->setVisibility(Landroid/view/View;I)V

    .line 156
    invoke-virtual {p0, p4}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->setDimmedStyle(Z)V

    .line 157
    return-void

    .line 144
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->statusTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 145
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->statusTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 150
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->releaseDateView:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 151
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->releaseDateView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 152
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->releaseDateView:Landroid/widget/TextView;

    invoke-static {p5, p6}, Lcom/google/android/videos/utils/TimeUtil;->getDateString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method


# virtual methods
.method protected collectClickableViews([Landroid/view/View;)V
    .locals 2
    .param p1, "clickableViews"    # [Landroid/view/View;

    .prologue
    .line 218
    const/4 v0, 0x0

    aput-object p0, p1, v0

    .line 219
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->downloadView:Lcom/google/android/videos/ui/DownloadView;

    aput-object v1, p1, v0

    .line 220
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->overflowView:Landroid/view/View;

    aput-object v1, p1, v0

    .line 221
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->synopsisView:Landroid/view/View;

    aput-object v1, p1, v0

    .line 222
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->getMoreButtonView:Landroid/widget/TextView;

    aput-object v1, p1, v0

    .line 223
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 69
    invoke-super {p0}, Lcom/google/android/videos/ui/playnext/CardItemView;->onFinishInflate()V

    .line 70
    const v0, 0x7f0f00a8

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->titleView:Landroid/widget/TextView;

    .line 71
    const v0, 0x7f0f00d0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->synopsisView:Landroid/view/View;

    .line 72
    const v0, 0x7f0f00d1

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/DownloadView;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->downloadView:Lcom/google/android/videos/ui/DownloadView;

    .line 73
    const v0, 0x7f0f00d2

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->overflowView:Landroid/view/View;

    .line 74
    const v0, 0x7f0f00d3

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->priceView:Landroid/widget/TextView;

    .line 75
    const v0, 0x7f0f00cd

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->statusTextView:Landroid/widget/TextView;

    .line 76
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->thumbnailView:Landroid/widget/ImageView;

    const v1, 0x7f0a00d7

    invoke-static {v0, v1}, Lcom/google/android/videos/ui/BitmapLoader;->createDefaultBitmapView(Landroid/widget/ImageView;I)Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->thumbnailBitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

    .line 78
    const v0, 0x7f0f00d5

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->showThumbnailView:Landroid/widget/ImageView;

    .line 79
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->showThumbnailView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->showThumbnailView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/videos/ui/BitmapLoader;->createSelectableDefaultBitmapView(Landroid/widget/ImageView;I)Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->showPosterBitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

    .line 82
    :cond_0
    const v0, 0x7f0f00ce

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->watchProgress:Landroid/widget/ProgressBar;

    .line 83
    const v0, 0x7f0f00d6

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->getMoreButtonView:Landroid/widget/TextView;

    .line 84
    const v0, 0x7f0f00cf

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->releaseDateView:Landroid/widget/TextView;

    .line 85
    return-void
.end method

.method protected onGenerateBitmapViewRequest(ILjava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .param p1, "id"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Q:",
            "Ljava/lang/Object;",
            ">(I",
            "Ljava/lang/Class",
            "<TQ;>;)TQ;"
        }
    .end annotation

    .prologue
    .line 206
    .local p2, "requestType":Ljava/lang/Class;, "Ljava/lang/Class<TQ;>;"
    sparse-switch p1, :sswitch_data_0

    .line 212
    invoke-super {p0, p1, p2}, Lcom/google/android/videos/ui/playnext/CardItemView;->onGenerateBitmapViewRequest(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    .line 208
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->thumbnailRequest:Ljava/lang/Object;

    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 210
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->showPosterRequest:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 206
    :sswitch_data_0
    .sparse-switch
        0x7f0f00c9 -> :sswitch_0
        0x7f0f00d5 -> :sswitch_1
    .end sparse-switch
.end method

.method prepareThumbnailBitmapView(Lcom/google/android/videos/async/Requester;Ljava/lang/Object;Ljava/lang/Class;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Q:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/videos/async/Requester",
            "<TQ;",
            "Landroid/graphics/Bitmap;",
            ">;TQ;",
            "Ljava/lang/Class",
            "<TQ;>;)V"
        }
    .end annotation

    .prologue
    .local p1, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TQ;Landroid/graphics/Bitmap;>;"
    .local p2, "request":Ljava/lang/Object;, "TQ;"
    .local p3, "requestType":Ljava/lang/Class;, "Ljava/lang/Class<TQ;>;"
    const/4 v4, 0x0

    const v1, 0x7f0f00c9

    .line 187
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 188
    :cond_0
    iput-object v4, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->thumbnailRequest:Ljava/lang/Object;

    .line 189
    invoke-virtual {p0, v1}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->unregisterBitmapView(I)V

    .line 195
    :goto_0
    return-void

    .line 191
    :cond_1
    iput-object p2, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->thumbnailRequest:Ljava/lang/Object;

    .line 192
    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->thumbnailBitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

    move-object v0, p0

    move-object v3, p1

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->registerBitmapView(ILcom/google/android/videos/ui/BitmapLoader$BitmapView;Lcom/google/android/videos/async/Requester;Landroid/graphics/Bitmap;Ljava/lang/Class;)V

    goto :goto_0
.end method

.method updateViewForPurchasedEpisode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/Integer;FIIZLjava/lang/String;Ljava/lang/String;J)V
    .locals 11
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "showTitle"    # Ljava/lang/String;
    .param p3, "statusText"    # Ljava/lang/String;
    .param p4, "showPin"    # Z
    .param p5, "pinned"    # Z
    .param p6, "pinningStatus"    # Ljava/lang/Integer;
    .param p7, "progress"    # F
    .param p8, "duration"    # I
    .param p9, "resumeTimestamp"    # I
    .param p10, "dimmed"    # Z
    .param p11, "account"    # Ljava/lang/String;
    .param p12, "videoId"    # Ljava/lang/String;
    .param p13, "publishTimestampSeconds"    # J

    .prologue
    .line 115
    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move/from16 v7, p10

    move-wide/from16 v8, p13

    invoke-direct/range {v3 .. v9}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->updateViewForEpisode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ)V

    .line 116
    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->overflowView:Landroid/view/View;

    const/16 v3, 0x8

    invoke-static {v2, v3}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->setVisibility(Landroid/view/View;I)V

    .line 117
    if-nez p4, :cond_1

    .line 118
    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->downloadView:Lcom/google/android/videos/ui/DownloadView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/google/android/videos/ui/DownloadView;->setVisibility(I)V

    .line 119
    const/4 v2, -0x1

    invoke-virtual {p0, v2}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->setNextFocusRightId(I)V

    .line 126
    :goto_0
    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->watchProgress:Landroid/widget/ProgressBar;

    if-eqz v2, :cond_0

    .line 127
    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->watchProgress:Landroid/widget/ProgressBar;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 128
    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->watchProgress:Landroid/widget/ProgressBar;

    move/from16 v0, p8

    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 129
    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->watchProgress:Landroid/widget/ProgressBar;

    move/from16 v0, p9

    div-int/lit16 v3, v0, 0x3e8

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 131
    :cond_0
    return-void

    .line 121
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->downloadView:Lcom/google/android/videos/ui/DownloadView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/videos/ui/DownloadView;->setVisibility(I)V

    .line 122
    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->downloadView:Lcom/google/android/videos/ui/DownloadView;

    invoke-virtual {v2, p1}, Lcom/google/android/videos/ui/DownloadView;->setTitle(Ljava/lang/String;)V

    .line 123
    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->downloadView:Lcom/google/android/videos/ui/DownloadView;

    move-object/from16 v3, p11

    move-object/from16 v4, p12

    move/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/videos/ui/DownloadView;->update(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Integer;F)V

    .line 124
    const v2, 0x7f0f00d1

    invoke-virtual {p0, v2}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->setNextFocusRightId(I)V

    goto :goto_0
.end method

.method updateViewForUnpurchasedEpisode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;ZJ)V
    .locals 8
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "showTitle"    # Ljava/lang/String;
    .param p3, "statusText"    # Ljava/lang/String;
    .param p4, "offers"    # [Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    .param p5, "dimmed"    # Z
    .param p6, "publishTimestampSeconds"    # J

    .prologue
    .line 94
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p5

    move-wide v6, p6

    invoke-direct/range {v1 .. v7}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->updateViewForEpisode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ)V

    .line 95
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->downloadView:Lcom/google/android/videos/ui/DownloadView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/videos/ui/DownloadView;->setVisibility(I)V

    .line 96
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->overflowView:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 97
    const v1, 0x7f0f00d2

    invoke-virtual {p0, v1}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->setNextFocusRightId(I)V

    .line 98
    const/4 v1, 0x1

    invoke-static {v1, p4}, Lcom/google/android/videos/utils/OfferUtil;->getCheapestOffer(Z[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;)Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    move-result-object v0

    .line 99
    .local v0, "cheapestOffer":Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    if-nez v0, :cond_0

    .line 100
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->priceView:Landroid/widget/TextView;

    const v2, 0x7f0b00d1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 104
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->watchProgress:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-static {v1, v2}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->setVisibility(Landroid/view/View;I)V

    .line 105
    return-void

    .line 102
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->priceView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->getFormattedAmount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
