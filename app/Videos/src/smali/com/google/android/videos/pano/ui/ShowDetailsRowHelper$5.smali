.class Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$5;
.super Lcom/google/android/videos/pano/model/VideoItem;
.source "ShowDetailsRowHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->addSeasonPass(Landroid/support/v17/leanback/widget/ArrayObjectAdapter;Lcom/google/wireless/android/video/magma/proto/AssetResource;[Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;Ljava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;

.field final synthetic val$purchase:Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;


# direct methods
.method constructor <init>(Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/content/Intent;ZJIIFLcom/google/android/videos/utils/OfferUtil$CheapestOffer;[Lcom/google/wireless/android/video/magma/proto/ViewerRating;Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;)V
    .locals 19
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # Ljava/lang/String;
    .param p4, "x2"    # Ljava/lang/String;
    .param p5, "x3"    # Ljava/lang/String;
    .param p6, "x4"    # Ljava/lang/String;
    .param p7, "x5"    # I
    .param p8, "x6"    # I
    .param p9, "x7"    # Landroid/content/Intent;
    .param p10, "x8"    # Z
    .param p11, "x9"    # J
    .param p13, "x10"    # I
    .param p14, "x11"    # I
    .param p15, "x12"    # F
    .param p16, "x13"    # Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    .param p17, "x14"    # [Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    .prologue
    .line 392
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$5;->this$0:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;

    move-object/from16 v0, p18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$5;->val$purchase:Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move-object/from16 v10, p9

    move/from16 v11, p10

    move-wide/from16 v12, p11

    move/from16 v14, p13

    move/from16 v15, p14

    move/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    invoke-direct/range {v2 .. v18}, Lcom/google/android/videos/pano/model/VideoItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/content/Intent;ZJIIFLcom/google/android/videos/utils/OfferUtil$CheapestOffer;[Lcom/google/wireless/android/video/magma/proto/ViewerRating;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/app/Activity;Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "viewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$5;->this$0:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;

    iget-object v0, v0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->onPurchaseActionListener:Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$OnPurchaseActionListener;

    iget-object v1, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$5;->val$purchase:Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;

    invoke-interface {v0, v1}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$OnPurchaseActionListener;->onSeasonPassPurchase(Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;)V

    .line 397
    return-void
.end method
