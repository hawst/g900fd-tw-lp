.class public Lcom/google/android/videos/tagging/DefaultKnowledgeClient;
.super Ljava/lang/Object;
.source "DefaultKnowledgeClient.java"

# interfaces
.implements Lcom/google/android/videos/tagging/KnowledgeClient;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/tagging/DefaultKnowledgeClient$PinnedFileStore;,
        Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;,
        Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;
    }
.end annotation


# instance fields
.field private final config:Lcom/google/android/videos/Config;

.field private final desiredPosterWidth:I

.field private final feedbackClient:Lcom/google/android/videos/tagging/FeedbackClient;

.field private final filmographyResourcesPinningRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;>;"
        }
    .end annotation
.end field

.field private final filmographyResourcesRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;>;"
        }
    .end annotation
.end field

.field private final globalBitmapCache:Lcom/google/android/videos/bitmap/BitmapLruCache;

.field private final imagePinningRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final imageRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final tagStreamPinningRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;",
            "Lcom/google/android/videos/tagging/TagStreamParser;",
            ">;"
        }
    .end annotation
.end field

.field private final tagStreamRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;",
            "Lcom/google/android/videos/tagging/TagStreamParser;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/videos/Config;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/bitmap/BitmapLruCache;IILjava/lang/String;)V
    .locals 27
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "config"    # Lcom/google/android/videos/Config;
    .param p3, "localExecutor"    # Ljava/util/concurrent/Executor;
    .param p4, "networkExecutor"    # Ljava/util/concurrent/Executor;
    .param p5, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .param p6, "httpClient"    # Lorg/apache/http/client/HttpClient;
    .param p9, "globalBitmapCache"    # Lcom/google/android/videos/bitmap/BitmapLruCache;
    .param p10, "maximumDesiredBitmapWidth"    # I
    .param p11, "desiredPosterWidth"    # I
    .param p12, "applicationVersion"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/videos/Config;",
            "Ljava/util/concurrent/Executor;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/videos/accounts/AccountManagerWrapper;",
            "Lorg/apache/http/client/HttpClient;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;",
            "Lcom/google/android/videos/bitmap/BitmapLruCache;",
            "II",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 64
    .local p7, "asyncAssetsCachingRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;>;"
    .local p8, "asyncBitmapBytesRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArray;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 65
    invoke-static/range {p2 .. p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/videos/Config;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient;->config:Lcom/google/android/videos/Config;

    .line 66
    invoke-static/range {p9 .. p9}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/videos/bitmap/BitmapLruCache;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient;->globalBitmapCache:Lcom/google/android/videos/bitmap/BitmapLruCache;

    .line 67
    move/from16 v0, p11

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/videos/tagging/DefaultKnowledgeClient;->desiredPosterWidth:I

    .line 69
    invoke-interface/range {p2 .. p2}, Lcom/google/android/videos/Config;->knowledgeRecheckDataAfterMillis()J

    move-result-wide v6

    .line 71
    .local v6, "recheckDataAfterMillis":J
    move-object/from16 v0, p5

    move-object/from16 v1, p6

    move-object/from16 v2, p4

    invoke-static {v0, v1, v2}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters;->createTagStreamHttpRequester(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Ljava/util/concurrent/Executor;)Lcom/google/android/videos/async/Requester;

    move-result-object v9

    .line 74
    .local v9, "tagStreamHttpRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequest;Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;>;"
    new-instance v10, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest$RequestHandler;

    invoke-direct {v10}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest$RequestHandler;-><init>()V

    .line 75
    .local v10, "tagStreamRequestHandler":Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest$RequestHandler;
    invoke-static {v9, v10}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters;->createKnowledgeComponentRequester(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;)Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient;->tagStreamRequester:Lcom/google/android/videos/async/Requester;

    .line 77
    new-instance v4, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$PinnedFileStore;

    new-instance v5, Lcom/google/android/videos/cache/SerializingConverter;

    invoke-direct {v5}, Lcom/google/android/videos/cache/SerializingConverter;-><init>()V

    move-object/from16 v0, p1

    invoke-direct {v4, v0, v5}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$PinnedFileStore;-><init>(Landroid/content/Context;Lcom/google/android/videos/cache/Converter;)V

    const/4 v5, 0x1

    move-object/from16 v8, p3

    invoke-static/range {v4 .. v10}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters;->createFileStoreBackedKnowledgeComponentRequester(Lcom/google/android/videos/store/AbstractFileStore;ZJLjava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;)Lcom/google/android/videos/async/Requester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient;->tagStreamPinningRequester:Lcom/google/android/videos/async/Requester;

    .line 83
    invoke-static/range {p7 .. p7}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters;->createAssetResourcesRequester(Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/Requester;

    move-result-object v17

    .line 85
    .local v17, "assetResourcesRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;Lcom/google/android/videos/utils/ByteArray;>;"
    new-instance v18, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest$RequestHander;

    invoke-direct/range {v18 .. v18}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest$RequestHander;-><init>()V

    .line 87
    .local v18, "filmographyResourcesRequestHandler":Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest$RequestHander;
    invoke-static/range {v17 .. v18}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters;->createKnowledgeComponentRequester(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;)Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient;->filmographyResourcesRequester:Lcom/google/android/videos/async/Requester;

    .line 89
    new-instance v12, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$PinnedFileStore;

    new-instance v4, Lcom/google/android/videos/cache/SerializingConverter;

    invoke-direct {v4}, Lcom/google/android/videos/cache/SerializingConverter;-><init>()V

    move-object/from16 v0, p1

    invoke-direct {v12, v0, v4}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$PinnedFileStore;-><init>(Landroid/content/Context;Lcom/google/android/videos/cache/Converter;)V

    const/4 v13, 0x1

    move-wide v14, v6

    move-object/from16 v16, p3

    invoke-static/range {v12 .. v18}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters;->createFileStoreBackedKnowledgeComponentRequester(Lcom/google/android/videos/store/AbstractFileStore;ZJLjava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;)Lcom/google/android/videos/async/Requester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient;->filmographyResourcesPinningRequester:Lcom/google/android/videos/async/Requester;

    .line 95
    new-instance v26, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest$RequestHandler;

    move-object/from16 v0, v26

    move/from16 v1, p10

    invoke-direct {v0, v1}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest$RequestHandler;-><init>(I)V

    .line 97
    .local v26, "imageRequestHandler":Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest$RequestHandler;
    move-object/from16 v0, p8

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters;->createKnowledgeComponentRequester(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;)Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient;->imageRequester:Lcom/google/android/videos/async/Requester;

    .line 99
    new-instance v20, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$PinnedFileStore;

    new-instance v4, Lcom/google/android/videos/cache/SerializingConverter;

    invoke-direct {v4}, Lcom/google/android/videos/cache/SerializingConverter;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$PinnedFileStore;-><init>(Landroid/content/Context;Lcom/google/android/videos/cache/Converter;)V

    const/16 v21, 0x0

    move-wide/from16 v22, v6

    move-object/from16 v24, p3

    move-object/from16 v25, p8

    invoke-static/range {v20 .. v26}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters;->createFileStoreBackedKnowledgeComponentRequester(Lcom/google/android/videos/store/AbstractFileStore;ZJLjava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;)Lcom/google/android/videos/async/Requester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient;->imagePinningRequester:Lcom/google/android/videos/async/Requester;

    .line 104
    new-instance v4, Lcom/google/android/videos/tagging/DefaultFeedbackClient;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p12

    invoke-direct {v4, v0, v1, v2}, Lcom/google/android/videos/tagging/DefaultFeedbackClient;-><init>(Landroid/content/Context;Lcom/google/android/videos/Config;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient;->feedbackClient:Lcom/google/android/videos/tagging/FeedbackClient;

    .line 105
    return-void
.end method

.method static synthetic access$400(Lcom/google/android/videos/tagging/DefaultKnowledgeClient;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/DefaultKnowledgeClient;

    .prologue
    .line 43
    iget v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient;->desiredPosterWidth:I

    return v0
.end method

.method private requestKnowledgeBundleInternal(Lcom/google/android/videos/tagging/KnowledgeRequest;ILcom/google/android/videos/async/Callback;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Z)V
    .locals 10
    .param p1, "request"    # Lcom/google/android/videos/tagging/KnowledgeRequest;
    .param p2, "storage"    # I
    .param p7, "isPinningRequest"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/tagging/KnowledgeRequest;",
            "I",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeRequest;",
            "Lcom/google/android/videos/tagging/KnowledgeBundle;",
            ">;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;",
            "Lcom/google/android/videos/tagging/TagStreamParser;",
            ">;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;>;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;",
            "Landroid/graphics/Bitmap;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 131
    .local p3, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/tagging/KnowledgeRequest;Lcom/google/android/videos/tagging/KnowledgeBundle;>;"
    .local p4, "selectedTagStreamRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Lcom/google/android/videos/tagging/TagStreamParser;>;"
    .local p5, "selectedResourcesRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;Ljava/util/Map<Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/AssetResource;>;>;"
    .local p6, "selectedImageRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;Landroid/graphics/Bitmap;>;"
    iget-object v1, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient;->config:Lcom/google/android/videos/Config;

    invoke-static {p1, p2, v1}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;->from(Lcom/google/android/videos/tagging/KnowledgeRequest;ILcom/google/android/videos/Config;)Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;

    move-result-object v9

    .line 132
    .local v9, "tagStreamRequest":Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;
    new-instance v7, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;

    iget-object v1, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient;->globalBitmapCache:Lcom/google/android/videos/bitmap/BitmapLruCache;

    move-object/from16 v0, p6

    invoke-direct {v7, p1, p2, v1, v0}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;-><init>(Lcom/google/android/videos/tagging/KnowledgeRequest;ILcom/google/android/videos/bitmap/BitmapLruCache;Lcom/google/android/videos/async/Requester;)V

    .line 134
    .local v7, "localImageRequester":Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;
    move/from16 v0, p7

    invoke-virtual {v7, v0}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->setAlwaysRequest(Z)V

    .line 135
    new-instance v1, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move/from16 v8, p7

    invoke-direct/range {v1 .. v8}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;-><init>(Lcom/google/android/videos/tagging/DefaultKnowledgeClient;Lcom/google/android/videos/tagging/KnowledgeRequest;Lcom/google/android/videos/async/Callback;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;Z)V

    invoke-virtual {v1, v9}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->start(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;)V

    .line 137
    return-void
.end method


# virtual methods
.method public getFeedbackClient()Lcom/google/android/videos/tagging/FeedbackClient;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient;->feedbackClient:Lcom/google/android/videos/tagging/FeedbackClient;

    return-object v0
.end method

.method public requestKnowledgeBundle(Lcom/google/android/videos/tagging/KnowledgeRequest;Lcom/google/android/videos/async/Callback;)V
    .locals 8
    .param p1, "request"    # Lcom/google/android/videos/tagging/KnowledgeRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/tagging/KnowledgeRequest;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeRequest;",
            "Lcom/google/android/videos/tagging/KnowledgeBundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 110
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/tagging/KnowledgeRequest;Lcom/google/android/videos/tagging/KnowledgeBundle;>;"
    const/4 v2, -0x1

    iget-object v4, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient;->tagStreamRequester:Lcom/google/android/videos/async/Requester;

    iget-object v5, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient;->filmographyResourcesRequester:Lcom/google/android/videos/async/Requester;

    iget-object v6, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient;->imageRequester:Lcom/google/android/videos/async/Requester;

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient;->requestKnowledgeBundleInternal(Lcom/google/android/videos/tagging/KnowledgeRequest;ILcom/google/android/videos/async/Callback;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Z)V

    .line 112
    return-void
.end method

.method public requestPinnedKnowledgeBundle(Lcom/google/android/videos/tagging/KnowledgeRequest;ILcom/google/android/videos/async/Callback;)V
    .locals 8
    .param p1, "request"    # Lcom/google/android/videos/tagging/KnowledgeRequest;
    .param p2, "storage"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/tagging/KnowledgeRequest;",
            "I",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeRequest;",
            "Lcom/google/android/videos/tagging/KnowledgeBundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 117
    .local p3, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/tagging/KnowledgeRequest;Lcom/google/android/videos/tagging/KnowledgeBundle;>;"
    iget-object v4, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient;->tagStreamPinningRequester:Lcom/google/android/videos/async/Requester;

    iget-object v5, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient;->filmographyResourcesPinningRequester:Lcom/google/android/videos/async/Requester;

    iget-object v6, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient;->imagePinningRequester:Lcom/google/android/videos/async/Requester;

    const/4 v7, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient;->requestKnowledgeBundleInternal(Lcom/google/android/videos/tagging/KnowledgeRequest;ILcom/google/android/videos/async/Callback;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Z)V

    .line 119
    return-void
.end method
