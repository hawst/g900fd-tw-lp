.class Lcom/google/android/videos/store/ConfigurationStore$LoadConfiguration;
.super Ljava/lang/Object;
.source "ConfigurationStore.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/ConfigurationStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadConfiguration"
.end annotation


# instance fields
.field private final callback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<-",
            "Ljava/lang/Integer;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final request:I

.field final synthetic this$0:Lcom/google/android/videos/store/ConfigurationStore;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/store/ConfigurationStore;ILcom/google/android/videos/async/Callback;)V
    .locals 1
    .param p2, "request"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/async/Callback",
            "<-",
            "Ljava/lang/Integer;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 215
    .local p3, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<-Ljava/lang/Integer;Ljava/lang/Void;>;"
    iput-object p1, p0, Lcom/google/android/videos/store/ConfigurationStore$LoadConfiguration;->this$0:Lcom/google/android/videos/store/ConfigurationStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 216
    iput p2, p0, Lcom/google/android/videos/store/ConfigurationStore$LoadConfiguration;->request:I

    .line 217
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Callback;

    iput-object v0, p0, Lcom/google/android/videos/store/ConfigurationStore$LoadConfiguration;->callback:Lcom/google/android/videos/async/Callback;

    .line 218
    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 222
    iget-object v0, p0, Lcom/google/android/videos/store/ConfigurationStore$LoadConfiguration;->this$0:Lcom/google/android/videos/store/ConfigurationStore;

    # getter for: Lcom/google/android/videos/store/ConfigurationStore;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v0}, Lcom/google/android/videos/store/ConfigurationStore;->access$100(Lcom/google/android/videos/store/ConfigurationStore;)Lcom/google/android/videos/store/Database;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "user_configuration"

    # getter for: Lcom/google/android/videos/store/ConfigurationStore;->LOAD_PROJECTION:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/videos/store/ConfigurationStore;->access$000()[Ljava/lang/String;

    move-result-object v2

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 225
    .local v9, "cursor":Landroid/database/Cursor;
    :goto_0
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 227
    .local v8, "account":Ljava/lang/String;
    const/4 v0, 0x1

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 228
    .local v10, "playCountry":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/videos/store/ConfigurationStore$LoadConfiguration;->this$0:Lcom/google/android/videos/store/ConfigurationStore;

    # getter for: Lcom/google/android/videos/store/ConfigurationStore;->userCountryCodes:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v0}, Lcom/google/android/videos/store/ConfigurationStore;->access$200(Lcom/google/android/videos/store/ConfigurationStore;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-virtual {v0, v8, v10}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    iget-object v0, p0, Lcom/google/android/videos/store/ConfigurationStore$LoadConfiguration;->this$0:Lcom/google/android/videos/store/ConfigurationStore;

    # getter for: Lcom/google/android/videos/store/ConfigurationStore;->userAccountLinks:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v0}, Lcom/google/android/videos/store/ConfigurationStore;->access$300(Lcom/google/android/videos/store/ConfigurationStore;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v8, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 232
    .end local v8    # "account":Ljava/lang/String;
    .end local v10    # "playCountry":Ljava/lang/String;
    :catchall_0
    move-exception v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 234
    iget-object v0, p0, Lcom/google/android/videos/store/ConfigurationStore$LoadConfiguration;->callback:Lcom/google/android/videos/async/Callback;

    iget v1, p0, Lcom/google/android/videos/store/ConfigurationStore$LoadConfiguration;->request:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 235
    return-void
.end method
