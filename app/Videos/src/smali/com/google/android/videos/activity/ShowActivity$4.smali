.class Lcom/google/android/videos/activity/ShowActivity$4;
.super Landroid/app/SharedElementCallback;
.source "ShowActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/activity/ShowActivity;->initEnterSharedElementCallback()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private posterHeroHeight:I

.field private posterHeroWidth:I

.field final synthetic this$0:Lcom/google/android/videos/activity/ShowActivity;


# direct methods
.method constructor <init>(Lcom/google/android/videos/activity/ShowActivity;)V
    .locals 0

    .prologue
    .line 411
    iput-object p1, p0, Lcom/google/android/videos/activity/ShowActivity$4;->this$0:Lcom/google/android/videos/activity/ShowActivity;

    invoke-direct {p0}, Landroid/app/SharedElementCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateSnapshotView(Landroid/content/Context;Landroid/os/Parcelable;)Landroid/view/View;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "snapshot"    # Landroid/os/Parcelable;

    .prologue
    .line 470
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity$4;->this$0:Lcom/google/android/videos/activity/ShowActivity;

    iget-boolean v0, v0, Lcom/google/android/videos/activity/ShowActivity;->runningEnterTransition:Z

    if-eqz v0, :cond_0

    instance-of v0, p2, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 472
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity$4;->this$0:Lcom/google/android/videos/activity/ShowActivity;

    iget-object v0, v0, Lcom/google/android/videos/activity/ShowActivity;->posterHeroView:Landroid/widget/ImageView;

    check-cast p2, Landroid/graphics/Bitmap;

    .end local p2    # "snapshot":Landroid/os/Parcelable;
    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 474
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public onMapSharedElements(Ljava/util/List;Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "names":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "sharedElements":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/view/View;>;"
    const/4 v0, 0x1

    .line 453
    iget-object v3, p0, Lcom/google/android/videos/activity/ShowActivity$4;->this$0:Lcom/google/android/videos/activity/ShowActivity;

    iget-boolean v3, v3, Lcom/google/android/videos/activity/ShowActivity;->runningEnterTransition:Z

    if-eqz v3, :cond_1

    .line 466
    :cond_0
    :goto_0
    return-void

    .line 458
    :cond_1
    const/4 v3, 0x2

    new-array v1, v3, [I

    .line 459
    .local v1, "location":[I
    iget-object v3, p0, Lcom/google/android/videos/activity/ShowActivity$4;->this$0:Lcom/google/android/videos/activity/ShowActivity;

    iget-object v3, v3, Lcom/google/android/videos/activity/ShowActivity;->posterHeroView:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->getLocationOnScreen([I)V

    .line 460
    iget-object v3, p0, Lcom/google/android/videos/activity/ShowActivity$4;->this$0:Lcom/google/android/videos/activity/ShowActivity;

    iget-object v3, v3, Lcom/google/android/videos/activity/ShowActivity;->posterHeroView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getHeight()I

    move-result v3

    aget v4, v1, v0

    add-int/2addr v3, v4

    if-ltz v3, :cond_2

    .line 461
    .local v0, "heroVisible":Z
    :goto_1
    if-nez v0, :cond_0

    .line 462
    iget-object v3, p0, Lcom/google/android/videos/activity/ShowActivity$4;->this$0:Lcom/google/android/videos/activity/ShowActivity;

    const v4, 0x7f0b0077

    iget-object v5, p0, Lcom/google/android/videos/activity/ShowActivity$4;->this$0:Lcom/google/android/videos/activity/ShowActivity;

    # getter for: Lcom/google/android/videos/activity/ShowActivity;->showId:Ljava/lang/String;
    invoke-static {v5}, Lcom/google/android/videos/activity/ShowActivity;->access$600(Lcom/google/android/videos/activity/ShowActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/google/android/videos/ui/TransitionUtil;->encodeTransitionName(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 464
    .local v2, "posterTransitionName":Ljava/lang/String;
    invoke-interface {p1, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 460
    .end local v0    # "heroVisible":Z
    .end local v2    # "posterTransitionName":Ljava/lang/String;
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onSharedElementEnd(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 434
    .local p1, "sharedElementNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "sharedElements":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .local p3, "sharedElementSnapshots":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    iget-object v4, p0, Lcom/google/android/videos/activity/ShowActivity$4;->this$0:Lcom/google/android/videos/activity/ShowActivity;

    iget-boolean v4, v4, Lcom/google/android/videos/activity/ShowActivity;->runningEnterTransition:Z

    if-nez v4, :cond_0

    .line 449
    :goto_0
    return-void

    .line 440
    :cond_0
    iget-object v4, p0, Lcom/google/android/videos/activity/ShowActivity$4;->this$0:Lcom/google/android/videos/activity/ShowActivity;

    iget-object v4, v4, Lcom/google/android/videos/activity/ShowActivity;->posterHeroView:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLeft()I

    move-result v1

    .line 441
    .local v1, "left":I
    iget-object v4, p0, Lcom/google/android/videos/activity/ShowActivity$4;->this$0:Lcom/google/android/videos/activity/ShowActivity;

    iget-object v4, v4, Lcom/google/android/videos/activity/ShowActivity;->posterHeroView:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getTop()I

    move-result v3

    .line 442
    .local v3, "top":I
    iget-object v4, p0, Lcom/google/android/videos/activity/ShowActivity$4;->this$0:Lcom/google/android/videos/activity/ShowActivity;

    iget-object v4, v4, Lcom/google/android/videos/activity/ShowActivity;->posterHeroView:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getRight()I

    move-result v2

    .line 443
    .local v2, "right":I
    iget-object v4, p0, Lcom/google/android/videos/activity/ShowActivity$4;->this$0:Lcom/google/android/videos/activity/ShowActivity;

    iget-object v4, v4, Lcom/google/android/videos/activity/ShowActivity;->posterHeroView:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getBottom()I

    move-result v0

    .line 444
    .local v0, "bottom":I
    add-int v4, v2, v1

    iget v5, p0, Lcom/google/android/videos/activity/ShowActivity$4;->posterHeroWidth:I

    sub-int/2addr v4, v5

    div-int/lit8 v1, v4, 0x2

    .line 445
    iget v4, p0, Lcom/google/android/videos/activity/ShowActivity$4;->posterHeroWidth:I

    add-int v2, v1, v4

    .line 446
    add-int v4, v3, v0

    iget v5, p0, Lcom/google/android/videos/activity/ShowActivity$4;->posterHeroHeight:I

    sub-int/2addr v4, v5

    div-int/lit8 v3, v4, 0x2

    .line 447
    iget v4, p0, Lcom/google/android/videos/activity/ShowActivity$4;->posterHeroHeight:I

    add-int v0, v3, v4

    .line 448
    iget-object v4, p0, Lcom/google/android/videos/activity/ShowActivity$4;->this$0:Lcom/google/android/videos/activity/ShowActivity;

    iget-object v4, v4, Lcom/google/android/videos/activity/ShowActivity;->posterHeroView:Landroid/widget/ImageView;

    invoke-virtual {v4, v1, v3, v2, v0}, Landroid/widget/ImageView;->layout(IIII)V

    goto :goto_0
.end method

.method public onSharedElementStart(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 418
    .local p1, "sharedElementNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "sharedElements":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .local p3, "sharedElementSnapshots":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity$4;->this$0:Lcom/google/android/videos/activity/ShowActivity;

    iget-boolean v0, v0, Lcom/google/android/videos/activity/ShowActivity;->runningEnterTransition:Z

    if-eqz v0, :cond_0

    .line 420
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity$4;->this$0:Lcom/google/android/videos/activity/ShowActivity;

    iget-object v0, v0, Lcom/google/android/videos/activity/ShowActivity;->posterHeroView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/activity/ShowActivity$4;->posterHeroWidth:I

    .line 421
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity$4;->this$0:Lcom/google/android/videos/activity/ShowActivity;

    iget-object v0, v0, Lcom/google/android/videos/activity/ShowActivity;->posterHeroView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/activity/ShowActivity$4;->posterHeroHeight:I

    .line 428
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity$4;->this$0:Lcom/google/android/videos/activity/ShowActivity;

    iget-object v0, v0, Lcom/google/android/videos/activity/ShowActivity;->posterHeroView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 429
    return-void
.end method
