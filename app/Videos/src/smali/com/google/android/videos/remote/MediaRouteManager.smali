.class public Lcom/google/android/videos/remote/MediaRouteManager;
.super Landroid/support/v7/media/MediaRouter$Callback;
.source "MediaRouteManager.java"

# interfaces
.implements Lcom/google/android/repolib/observers/Updatable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/remote/MediaRouteManager$Listener;
    }
.end annotation


# instance fields
.field private final castMediaRouter:Lcom/google/android/videos/cast/CastMediaRouter;

.field private currentRemote:Lcom/google/android/videos/remote/RemoteControl;

.field private final eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private final listeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/google/android/videos/remote/MediaRouteManager$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private final mediaRouter:Landroid/support/v7/media/MediaRouter;

.field private final networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

.field private final preferences:Landroid/content/SharedPreferences;

.field private final remoteSelectedCallback:Lcom/google/android/videos/remote/MediaRouteManager$Listener;

.field private restoringRoute:Z

.field private final routeSelector:Landroid/support/v7/media/MediaRouteSelector;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/videos/logging/EventLogger;Landroid/content/SharedPreferences;Lcom/google/android/videos/cast/CastMediaRouter;Lcom/google/android/videos/utils/NetworkStatus;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;
    .param p3, "preferences"    # Landroid/content/SharedPreferences;
    .param p4, "castMediaRouter"    # Lcom/google/android/videos/cast/CastMediaRouter;
    .param p5, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;

    .prologue
    const/4 v4, 0x0

    .line 59
    invoke-direct {p0}, Landroid/support/v7/media/MediaRouter$Callback;-><init>()V

    .line 52
    new-instance v3, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v3, p0, Lcom/google/android/videos/remote/MediaRouteManager;->listeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 60
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/logging/EventLogger;

    iput-object v3, p0, Lcom/google/android/videos/remote/MediaRouteManager;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 62
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/SharedPreferences;

    iput-object v3, p0, Lcom/google/android/videos/remote/MediaRouteManager;->preferences:Landroid/content/SharedPreferences;

    .line 63
    iput-object p4, p0, Lcom/google/android/videos/remote/MediaRouteManager;->castMediaRouter:Lcom/google/android/videos/cast/CastMediaRouter;

    .line 64
    iput-object p5, p0, Lcom/google/android/videos/remote/MediaRouteManager;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 65
    iget-object v3, p0, Lcom/google/android/videos/remote/MediaRouteManager;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v3, p0}, Lcom/google/android/videos/utils/NetworkStatus;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 69
    :try_start_0
    invoke-static {p1}, Landroid/support/v7/media/MediaRouter;->getInstance(Landroid/content/Context;)Landroid/support/v7/media/MediaRouter;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 77
    .local v2, "systemMediaRouter":Landroid/support/v7/media/MediaRouter;
    iput-object v2, p0, Lcom/google/android/videos/remote/MediaRouteManager;->mediaRouter:Landroid/support/v7/media/MediaRouter;

    .line 79
    new-instance v3, Lcom/google/android/videos/remote/MediaRouteManager$1;

    invoke-direct {v3, p0}, Lcom/google/android/videos/remote/MediaRouteManager$1;-><init>(Lcom/google/android/videos/remote/MediaRouteManager;)V

    iput-object v3, p0, Lcom/google/android/videos/remote/MediaRouteManager;->remoteSelectedCallback:Lcom/google/android/videos/remote/MediaRouteManager$Listener;

    .line 86
    new-instance v3, Landroid/support/v7/media/MediaRouteSelector$Builder;

    invoke-direct {v3}, Landroid/support/v7/media/MediaRouteSelector$Builder;-><init>()V

    const-string v4, "android.media.intent.category.LIVE_VIDEO"

    invoke-virtual {v3, v4}, Landroid/support/v7/media/MediaRouteSelector$Builder;->addControlCategory(Ljava/lang/String;)Landroid/support/v7/media/MediaRouteSelector$Builder;

    move-result-object v3

    const-string v4, "android.media.intent.category.LIVE_AUDIO"

    invoke-virtual {v3, v4}, Landroid/support/v7/media/MediaRouteSelector$Builder;->addControlCategory(Ljava/lang/String;)Landroid/support/v7/media/MediaRouteSelector$Builder;

    move-result-object v1

    .line 89
    .local v1, "routeBuilder":Landroid/support/v7/media/MediaRouteSelector$Builder;
    if-eqz p4, :cond_0

    .line 90
    invoke-interface {p4}, Lcom/google/android/videos/cast/CastMediaRouter;->getRouteSelector()Landroid/support/v7/media/MediaRouteSelector;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/support/v7/media/MediaRouteSelector$Builder;->addSelector(Landroid/support/v7/media/MediaRouteSelector;)Landroid/support/v7/media/MediaRouteSelector$Builder;

    .line 91
    invoke-interface {p4}, Lcom/google/android/videos/cast/CastMediaRouter;->start()V

    .line 93
    :cond_0
    invoke-virtual {v1}, Landroid/support/v7/media/MediaRouteSelector$Builder;->build()Landroid/support/v7/media/MediaRouteSelector;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/remote/MediaRouteManager;->routeSelector:Landroid/support/v7/media/MediaRouteSelector;

    .line 95
    invoke-direct {p0}, Lcom/google/android/videos/remote/MediaRouteManager;->maybeRestoreRoute()V

    .line 96
    .end local v1    # "routeBuilder":Landroid/support/v7/media/MediaRouteSelector$Builder;
    .end local v2    # "systemMediaRouter":Landroid/support/v7/media/MediaRouter;
    :goto_0
    return-void

    .line 70
    :catch_0
    move-exception v0

    .line 72
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    iput-object v4, p0, Lcom/google/android/videos/remote/MediaRouteManager;->mediaRouter:Landroid/support/v7/media/MediaRouter;

    .line 73
    iput-object v4, p0, Lcom/google/android/videos/remote/MediaRouteManager;->routeSelector:Landroid/support/v7/media/MediaRouteSelector;

    .line 74
    iput-object v4, p0, Lcom/google/android/videos/remote/MediaRouteManager;->remoteSelectedCallback:Lcom/google/android/videos/remote/MediaRouteManager$Listener;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/videos/remote/MediaRouteManager;Lcom/google/android/videos/remote/RemoteControl;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/remote/MediaRouteManager;
    .param p1, "x1"    # Lcom/google/android/videos/remote/RemoteControl;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/videos/remote/MediaRouteManager;->onRemoteControlSelected(Lcom/google/android/videos/remote/RemoteControl;)V

    return-void
.end method

.method private maybeRestoreRoute()V
    .locals 7

    .prologue
    .line 177
    iget-object v4, p0, Lcom/google/android/videos/remote/MediaRouteManager;->mediaRouter:Landroid/support/v7/media/MediaRouter;

    if-nez v4, :cond_1

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 180
    :cond_1
    iget-boolean v4, p0, Lcom/google/android/videos/remote/MediaRouteManager;->restoringRoute:Z

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/videos/remote/MediaRouteManager;->mediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-virtual {v4}, Landroid/support/v7/media/MediaRouter;->getSelectedRoute()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v7/media/MediaRouter$RouteInfo;->isDefault()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 183
    iget-object v4, p0, Lcom/google/android/videos/remote/MediaRouteManager;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/videos/remote/MediaRouteManager;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v4}, Lcom/google/android/videos/remote/RemoteControl;->getRouteInfo()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/videos/remote/MediaRouteManager;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v4}, Lcom/google/android/videos/remote/RemoteControl;->getRouteInfo()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v7/media/MediaRouter$RouteInfo;->isSelected()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 186
    const-string v4, "sessionRestore there\'s already a remote, skip"

    invoke-static {v4}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 190
    :cond_2
    iget-object v4, p0, Lcom/google/android/videos/remote/MediaRouteManager;->preferences:Landroid/content/SharedPreferences;

    const-string v5, "castv2_route_id"

    const-string v6, ""

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 191
    .local v1, "routeIdToRestore":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sessionRestore routeId: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 192
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 193
    iget-object v4, p0, Lcom/google/android/videos/remote/MediaRouteManager;->mediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-virtual {v4}, Landroid/support/v7/media/MediaRouter;->getRoutes()Ljava/util/List;

    move-result-object v3

    .line 194
    .local v3, "routeInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/support/v7/media/MediaRouter$RouteInfo;>;"
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sessionRestore No. of available routes: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 195
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 196
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 197
    .local v2, "routeInfo":Landroid/support/v7/media/MediaRouter$RouteInfo;
    invoke-virtual {v2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 198
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sessionRestore found matching route, start restoring..."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 201
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/google/android/videos/remote/MediaRouteManager;->restoringRoute:Z

    .line 202
    invoke-virtual {v2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->select()V

    goto/16 :goto_0

    .line 195
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private notifyRemoteControlChanged()V
    .locals 3

    .prologue
    .line 210
    iget-object v2, p0, Lcom/google/android/videos/remote/MediaRouteManager;->listeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/remote/MediaRouteManager$Listener;

    .line 211
    .local v1, "listener":Lcom/google/android/videos/remote/MediaRouteManager$Listener;
    iget-object v2, p0, Lcom/google/android/videos/remote/MediaRouteManager;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    invoke-interface {v1, v2}, Lcom/google/android/videos/remote/MediaRouteManager$Listener;->onRemoteControlSelected(Lcom/google/android/videos/remote/RemoteControl;)V

    goto :goto_0

    .line 213
    .end local v1    # "listener":Lcom/google/android/videos/remote/MediaRouteManager$Listener;
    :cond_0
    return-void
.end method

.method private onRemoteControlSelected(Lcom/google/android/videos/remote/RemoteControl;)V
    .locals 1
    .param p1, "remoteControl"    # Lcom/google/android/videos/remote/RemoteControl;

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    if-eq v0, p1, :cond_0

    .line 136
    iput-object p1, p0, Lcom/google/android/videos/remote/MediaRouteManager;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    .line 137
    invoke-direct {p0}, Lcom/google/android/videos/remote/MediaRouteManager;->notifyRemoteControlChanged()V

    .line 139
    :cond_0
    return-void
.end method

.method private requestRemoteForRouteInfo(Landroid/support/v7/media/MediaRouter$RouteInfo;Lcom/google/android/videos/remote/MediaRouteManager$Listener;)V
    .locals 2
    .param p1, "routeInfo"    # Landroid/support/v7/media/MediaRouter$RouteInfo;
    .param p2, "callback"    # Lcom/google/android/videos/remote/MediaRouteManager$Listener;

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->castMediaRouter:Lcom/google/android/videos/cast/CastMediaRouter;

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/videos/remote/MediaRouteManager;->isCastDevice(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    iget-boolean v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->restoringRoute:Z

    if-eqz v0, :cond_1

    .line 219
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->restoringRoute:Z

    .line 227
    :goto_0
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->castMediaRouter:Lcom/google/android/videos/cast/CastMediaRouter;

    invoke-interface {v0, p1, p2}, Lcom/google/android/videos/cast/CastMediaRouter;->createRemote(Landroid/support/v7/media/MediaRouter$RouteInfo;Lcom/google/android/videos/remote/MediaRouteManager$Listener;)V

    .line 229
    :cond_0
    return-void

    .line 222
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "castv2_route_id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "castv2_session_id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method


# virtual methods
.method public getCurrentRouteInfo()Landroid/support/v7/media/MediaRouter$RouteInfo;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->mediaRouter:Landroid/support/v7/media/MediaRouter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->mediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter;->getSelectedRoute()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentlySelectedRemote()Lcom/google/android/videos/remote/RemoteControl;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    return-object v0
.end method

.method public getRouteSelector()Landroid/support/v7/media/MediaRouteSelector;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->routeSelector:Landroid/support/v7/media/MediaRouteSelector;

    return-object v0
.end method

.method public isCastDevice(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z
    .locals 1
    .param p1, "routeInfo"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->castMediaRouter:Lcom/google/android/videos/cast/CastMediaRouter;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->castMediaRouter:Lcom/google/android/videos/cast/CastMediaRouter;

    invoke-interface {v0, p1}, Lcom/google/android/videos/cast/CastMediaRouter;->isCastDevice(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v0

    goto :goto_0
.end method

.method public onRouteAdded(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 2
    .param p1, "router"    # Landroid/support/v7/media/MediaRouter;
    .param p2, "routeInfo"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    .line 143
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "new route added "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->castMediaRouter:Lcom/google/android/videos/cast/CastMediaRouter;

    if-eqz v0, :cond_0

    invoke-virtual {p0, p2}, Lcom/google/android/videos/remote/MediaRouteManager;->isCastDevice(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    invoke-interface {v0}, Lcom/google/android/videos/logging/EventLogger;->onCastDisplayDetected()V

    .line 146
    invoke-direct {p0}, Lcom/google/android/videos/remote/MediaRouteManager;->maybeRestoreRoute()V

    .line 148
    :cond_0
    return-void
.end method

.method public onRouteSelected(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 2
    .param p1, "router"    # Landroid/support/v7/media/MediaRouter;
    .param p2, "routeInfo"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    .line 152
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "routeInfo: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 153
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->remoteSelectedCallback:Lcom/google/android/videos/remote/MediaRouteManager$Listener;

    invoke-direct {p0, p2, v0}, Lcom/google/android/videos/remote/MediaRouteManager;->requestRemoteForRouteInfo(Landroid/support/v7/media/MediaRouter$RouteInfo;Lcom/google/android/videos/remote/MediaRouteManager$Listener;)V

    .line 154
    return-void
.end method

.method public onRouteUnselected(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 2
    .param p1, "router"    # Landroid/support/v7/media/MediaRouter;
    .param p2, "routeInfo"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    .line 158
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "routeInfo: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 159
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v0}, Lcom/google/android/videos/remote/RemoteControl;->getRouteInfo()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v0}, Lcom/google/android/videos/remote/RemoteControl;->getRouteInfo()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 163
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v0}, Lcom/google/android/videos/remote/RemoteControl;->stop()V

    .line 164
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/videos/remote/MediaRouteManager;->onRemoteControlSelected(Lcom/google/android/videos/remote/RemoteControl;)V

    .line 166
    :cond_1
    return-void
.end method

.method public register(Lcom/google/android/videos/remote/MediaRouteManager$Listener;)V
    .locals 3
    .param p1, "listener"    # Lcom/google/android/videos/remote/MediaRouteManager$Listener;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->mediaRouter:Landroid/support/v7/media/MediaRouter;

    if-nez v0, :cond_1

    .line 112
    :cond_0
    :goto_0
    return-void

    .line 102
    :cond_1
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->listeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    const-string v1, "listener already registered"

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 104
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->listeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 105
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->mediaRouter:Landroid/support/v7/media/MediaRouter;

    iget-object v1, p0, Lcom/google/android/videos/remote/MediaRouteManager;->routeSelector:Landroid/support/v7/media/MediaRouteSelector;

    invoke-virtual {v0, v1, p0}, Landroid/support/v7/media/MediaRouter;->addCallback(Landroid/support/v7/media/MediaRouteSelector;Landroid/support/v7/media/MediaRouter$Callback;)V

    .line 106
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->mediaRouter:Landroid/support/v7/media/MediaRouter;

    iget-object v1, p0, Lcom/google/android/videos/remote/MediaRouteManager;->mediaRouter:Landroid/support/v7/media/MediaRouter;

    iget-object v2, p0, Lcom/google/android/videos/remote/MediaRouteManager;->routeSelector:Landroid/support/v7/media/MediaRouteSelector;

    invoke-virtual {v1, v2}, Landroid/support/v7/media/MediaRouter;->updateSelectedRoute(Landroid/support/v7/media/MediaRouteSelector;)Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/videos/remote/MediaRouteManager;->onRouteSelected(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V

    .line 108
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->listeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 109
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    invoke-interface {p1, v0}, Lcom/google/android/videos/remote/MediaRouteManager$Listener;->onRemoteControlSelected(Lcom/google/android/videos/remote/RemoteControl;)V

    goto :goto_0

    .line 103
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public unregister(Lcom/google/android/videos/remote/MediaRouteManager$Listener;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/videos/remote/MediaRouteManager$Listener;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->mediaRouter:Landroid/support/v7/media/MediaRouter;

    if-nez v0, :cond_1

    .line 124
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->listeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "listener not registered"

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->listeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 120
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->listeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->mediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-virtual {v0, p0}, Landroid/support/v7/media/MediaRouter;->removeCallback(Landroid/support/v7/media/MediaRouter$Callback;)V

    .line 122
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    goto :goto_0
.end method

.method public update()V
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteManager;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234
    const-string v0, "sessionRestore network reconnected"

    invoke-static {v0}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 235
    invoke-direct {p0}, Lcom/google/android/videos/remote/MediaRouteManager;->maybeRestoreRoute()V

    .line 237
    :cond_0
    return-void
.end method
