.class public final Lcom/google/android/videos/pano/ui/LibraryCardView;
.super Landroid/widget/LinearLayout;
.source "LibraryCardView.java"


# instance fields
.field private imageView:Lcom/google/android/videos/pano/ui/RequestingImageView;

.field private titleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 26
    const v0, 0x7f010104

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    return-void
.end method


# virtual methods
.method public hasOverlappingRendering()Z
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 31
    const v0, 0x7f0f0156

    invoke-virtual {p0, v0}, Lcom/google/android/videos/pano/ui/LibraryCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/pano/ui/RequestingImageView;

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/LibraryCardView;->imageView:Lcom/google/android/videos/pano/ui/RequestingImageView;

    .line 32
    const v0, 0x7f0f012e

    invoke-virtual {p0, v0}, Lcom/google/android/videos/pano/ui/LibraryCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/LibraryCardView;->titleView:Landroid/widget/TextView;

    .line 33
    return-void
.end method

.method public setImageResource(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/LibraryCardView;->imageView:Lcom/google/android/videos/pano/ui/RequestingImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/pano/ui/RequestingImageView;->setImageResource(I)V

    .line 42
    return-void
.end method

.method public setImageUri(Landroid/net/Uri;Lcom/google/android/videos/async/Requester;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    .local p2, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/async/ControllableRequest<Landroid/net/Uri;>;Landroid/graphics/Bitmap;>;"
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/LibraryCardView;->imageView:Lcom/google/android/videos/pano/ui/RequestingImageView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/pano/ui/RequestingImageView;->setImageUri(Landroid/net/Uri;Lcom/google/android/videos/async/Requester;)V

    .line 47
    return-void
.end method

.method public setText(I)V
    .locals 1
    .param p1, "text"    # I

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/LibraryCardView;->titleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 55
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/LibraryCardView;->titleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 51
    return-void
.end method
