.class public Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;
.super Lcom/google/android/play/headerlist/PlayHeaderListLayout;
.source "ImmersiveHeaderListLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/ImmersiveHeaderListLayout$ActionBarAlphaListener;
    }
.end annotation


# instance fields
.field private alphaListener:Lcom/google/android/videos/ui/ImmersiveHeaderListLayout$ActionBarAlphaListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 22
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    return-void
.end method


# virtual methods
.method protected applyActionBarTitleAlpha(Landroid/support/v7/widget/Toolbar;F)V
    .locals 1
    .param p1, "toolbar"    # Landroid/support/v7/widget/Toolbar;
    .param p2, "alpha"    # F

    .prologue
    .line 31
    invoke-super {p0, p1, p2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->applyActionBarTitleAlpha(Landroid/support/v7/widget/Toolbar;F)V

    .line 32
    iget-object v0, p0, Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;->alphaListener:Lcom/google/android/videos/ui/ImmersiveHeaderListLayout$ActionBarAlphaListener;

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;->alphaListener:Lcom/google/android/videos/ui/ImmersiveHeaderListLayout$ActionBarAlphaListener;

    invoke-interface {v0, p2}, Lcom/google/android/videos/ui/ImmersiveHeaderListLayout$ActionBarAlphaListener;->onActionBarAlphaChanged(F)V

    .line 35
    :cond_0
    return-void
.end method

.method public setActionBarAlphaListener(Lcom/google/android/videos/ui/ImmersiveHeaderListLayout$ActionBarAlphaListener;)V
    .locals 0
    .param p1, "alphaListener"    # Lcom/google/android/videos/ui/ImmersiveHeaderListLayout$ActionBarAlphaListener;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;->alphaListener:Lcom/google/android/videos/ui/ImmersiveHeaderListLayout$ActionBarAlphaListener;

    .line 39
    return-void
.end method
