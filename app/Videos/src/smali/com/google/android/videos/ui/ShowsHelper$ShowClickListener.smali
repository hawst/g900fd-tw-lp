.class final Lcom/google/android/videos/ui/ShowsHelper$ShowClickListener;
.super Ljava/lang/Object;
.source "ShowsHelper.java"

# interfaces
.implements Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/ShowsHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ShowClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/ui/ShowsHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/ui/ShowsHelper;)V
    .locals 0

    .prologue
    .line 248
    iput-object p1, p0, Lcom/google/android/videos/ui/ShowsHelper$ShowClickListener;->this$0:Lcom/google/android/videos/ui/ShowsHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/ui/ShowsHelper;Lcom/google/android/videos/ui/ShowsHelper$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/ui/ShowsHelper;
    .param p2, "x1"    # Lcom/google/android/videos/ui/ShowsHelper$1;

    .prologue
    .line 248
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/ShowsHelper$ShowClickListener;-><init>(Lcom/google/android/videos/ui/ShowsHelper;)V

    return-void
.end method


# virtual methods
.method public onItemClick(Lcom/google/android/videos/adapter/DataSource;ILandroid/view/View;)V
    .locals 7
    .param p2, "position"    # I
    .param p3, "view"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/adapter/DataSource",
            "<*>;I",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 252
    .local p1, "dataSource":Lcom/google/android/videos/adapter/DataSource;, "Lcom/google/android/videos/adapter/DataSource<*>;"
    iget-object v5, p0, Lcom/google/android/videos/ui/ShowsHelper$ShowClickListener;->this$0:Lcom/google/android/videos/ui/ShowsHelper;

    # getter for: Lcom/google/android/videos/ui/ShowsHelper;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;
    invoke-static {v5}, Lcom/google/android/videos/ui/ShowsHelper;->access$100(Lcom/google/android/videos/ui/ShowsHelper;)Lcom/google/android/videos/logging/UiEventLoggingHelper;

    move-result-object v5

    invoke-static {p3}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->findUiElementNode(Landroid/view/View;)Lcom/google/android/videos/logging/UiElementNode;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->sendClickEvent(Lcom/google/android/videos/logging/UiElementNode;)V

    .line 253
    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/DataSource;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/Cursor;

    .line 254
    .local v1, "cursor":Landroid/database/Cursor;
    invoke-static {v1}, Lcom/google/android/videos/adapter/ShowsDataSource;->getShowId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    .line 255
    .local v4, "showId":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/videos/ui/ShowsHelper$ShowClickListener;->this$0:Lcom/google/android/videos/ui/ShowsHelper;

    # getter for: Lcom/google/android/videos/ui/ShowsHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;
    invoke-static {v5}, Lcom/google/android/videos/ui/ShowsHelper;->access$200(Lcom/google/android/videos/ui/ShowsHelper;)Lcom/google/android/videos/activity/HomeActivity;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/videos/ui/ShowsHelper$ShowClickListener;->this$0:Lcom/google/android/videos/ui/ShowsHelper;

    # getter for: Lcom/google/android/videos/ui/ShowsHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;
    invoke-static {v6}, Lcom/google/android/videos/ui/ShowsHelper;->access$300(Lcom/google/android/videos/ui/ShowsHelper;)Lcom/google/android/videos/ui/SyncHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v4}, Lcom/google/android/videos/activity/ShowActivity;->createShowIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 256
    .local v2, "intent":Landroid/content/Intent;
    const-string v5, "up_is_back"

    const/4 v6, 0x1

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 257
    sget v5, Lcom/google/android/exoplayer/util/Util;->SDK_INT:I

    const/16 v6, 0x15

    if-lt v5, v6, :cond_0

    .line 258
    new-instance v3, Lcom/google/android/videos/activity/ShowActivity$SharedElements;

    invoke-direct {v3}, Lcom/google/android/videos/activity/ShowActivity$SharedElements;-><init>()V

    .line 259
    .local v3, "shared":Lcom/google/android/videos/activity/ShowActivity$SharedElements;
    invoke-static {p3}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->findThumbnailView(Landroid/view/View;)Landroid/view/View;

    move-result-object v5

    iput-object v5, v3, Lcom/google/android/videos/activity/ShowActivity$SharedElements;->poster:Landroid/view/View;

    .line 260
    iget-object v5, p0, Lcom/google/android/videos/ui/ShowsHelper$ShowClickListener;->this$0:Lcom/google/android/videos/ui/ShowsHelper;

    # getter for: Lcom/google/android/videos/ui/ShowsHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;
    invoke-static {v5}, Lcom/google/android/videos/ui/ShowsHelper;->access$200(Lcom/google/android/videos/ui/ShowsHelper;)Lcom/google/android/videos/activity/HomeActivity;

    move-result-object v5

    invoke-static {v5, v2, v3}, Lcom/google/android/videos/activity/ShowActivity;->createAnimationBundleV21(Landroid/app/Activity;Landroid/content/Intent;Lcom/google/android/videos/activity/ShowActivity$SharedElements;)Landroid/os/Bundle;

    move-result-object v0

    .line 261
    .local v0, "bundle":Landroid/os/Bundle;
    iget-object v5, p0, Lcom/google/android/videos/ui/ShowsHelper$ShowClickListener;->this$0:Lcom/google/android/videos/ui/ShowsHelper;

    # getter for: Lcom/google/android/videos/ui/ShowsHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;
    invoke-static {v5}, Lcom/google/android/videos/ui/ShowsHelper;->access$200(Lcom/google/android/videos/ui/ShowsHelper;)Lcom/google/android/videos/activity/HomeActivity;

    move-result-object v5

    invoke-static {v5, v2, v0}, Landroid/support/v4/app/ActivityCompat;->startActivity(Landroid/app/Activity;Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 265
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v3    # "shared":Lcom/google/android/videos/activity/ShowActivity$SharedElements;
    :goto_0
    return-void

    .line 263
    :cond_0
    iget-object v5, p0, Lcom/google/android/videos/ui/ShowsHelper$ShowClickListener;->this$0:Lcom/google/android/videos/ui/ShowsHelper;

    # getter for: Lcom/google/android/videos/ui/ShowsHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;
    invoke-static {v5}, Lcom/google/android/videos/ui/ShowsHelper;->access$200(Lcom/google/android/videos/ui/ShowsHelper;)Lcom/google/android/videos/activity/HomeActivity;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/google/android/videos/activity/HomeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
