.class Lcom/google/android/videos/tagging/Tag$FaceRectTag;
.super Lcom/google/android/videos/tagging/Tag;
.source "Tag.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/Tag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FaceRectTag"
.end annotation


# instance fields
.field private final angle:F

.field private final faceWidth:F

.field private final x:F

.field private final y:F


# direct methods
.method private constructor <init>(IIJZ)V
    .locals 7
    .param p1, "splitId"    # I
    .param p2, "timeMillis"    # I
    .param p3, "faceRectData"    # J
    .param p5, "interpolates"    # Z

    .prologue
    const-wide/16 v4, 0x1ff

    const v2, 0x43ff8000    # 511.0f

    .line 232
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p5, v0}, Lcom/google/android/videos/tagging/Tag;-><init>(IIZLcom/google/android/videos/tagging/Tag$1;)V

    .line 233
    and-long v0, p3, v4

    long-to-float v0, v0

    div-float/2addr v0, v2

    iput v0, p0, Lcom/google/android/videos/tagging/Tag$FaceRectTag;->x:F

    .line 234
    const/16 v0, 0x9

    shr-long v0, p3, v0

    and-long/2addr v0, v4

    long-to-float v0, v0

    div-float/2addr v0, v2

    iput v0, p0, Lcom/google/android/videos/tagging/Tag$FaceRectTag;->y:F

    .line 235
    const/16 v0, 0x12

    shr-long v0, p3, v0

    and-long/2addr v0, v4

    long-to-float v0, v0

    div-float/2addr v0, v2

    iput v0, p0, Lcom/google/android/videos/tagging/Tag$FaceRectTag;->faceWidth:F

    .line 236
    const/16 v0, 0x1b

    shr-long v0, p3, v0

    const-wide/16 v2, 0x7f

    and-long/2addr v0, v2

    const-wide/16 v2, 0x168

    mul-long/2addr v0, v2

    long-to-float v0, v0

    const/high16 v1, 0x42f00000    # 120.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/videos/tagging/Tag$FaceRectTag;->angle:F

    .line 237
    return-void
.end method

.method synthetic constructor <init>(IIJZLcom/google/android/videos/tagging/Tag$1;)V
    .locals 1
    .param p1, "x0"    # I
    .param p2, "x1"    # I
    .param p3, "x2"    # J
    .param p5, "x3"    # Z
    .param p6, "x4"    # Lcom/google/android/videos/tagging/Tag$1;

    .prologue
    .line 222
    invoke-direct/range {p0 .. p5}, Lcom/google/android/videos/tagging/Tag$FaceRectTag;-><init>(IIJZ)V

    return-void
.end method


# virtual methods
.method public getTagShape(IFFF)Lcom/google/android/videos/tagging/Tag$TagShape;
    .locals 8
    .param p1, "atMillis"    # I
    .param p2, "width"    # F
    .param p3, "height"    # F
    .param p4, "minArea"    # F

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 241
    new-instance v1, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;-><init>(Lcom/google/android/videos/tagging/Tag$1;)V

    .line 242
    .local v1, "oval":Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/tagging/Tag$FaceRectTag;->shouldInterpolate(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/videos/tagging/Tag$FaceRectTag;->getNextTag()Lcom/google/android/videos/tagging/Tag;

    move-result-object v2

    instance-of v2, v2, Lcom/google/android/videos/tagging/Tag$FaceRectTag;

    if-eqz v2, :cond_0

    .line 243
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/Tag$FaceRectTag;->getNextTag()Lcom/google/android/videos/tagging/Tag;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/tagging/Tag$FaceRectTag;

    .line 244
    .local v0, "nextTag":Lcom/google/android/videos/tagging/Tag$FaceRectTag;
    iget v2, p0, Lcom/google/android/videos/tagging/Tag$FaceRectTag;->x:F

    iget v3, v0, Lcom/google/android/videos/tagging/Tag$FaceRectTag;->timeMillis:I

    iget v4, v0, Lcom/google/android/videos/tagging/Tag$FaceRectTag;->x:F

    invoke-virtual {p0, p1, v2, v3, v4}, Lcom/google/android/videos/tagging/Tag$FaceRectTag;->interpolate(IFIF)F

    move-result v2

    mul-float/2addr v2, p2

    iput v2, v1, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->centerX:F

    .line 245
    iget v2, p0, Lcom/google/android/videos/tagging/Tag$FaceRectTag;->y:F

    iget v3, v0, Lcom/google/android/videos/tagging/Tag$FaceRectTag;->timeMillis:I

    iget v4, v0, Lcom/google/android/videos/tagging/Tag$FaceRectTag;->y:F

    invoke-virtual {p0, p1, v2, v3, v4}, Lcom/google/android/videos/tagging/Tag$FaceRectTag;->interpolate(IFIF)F

    move-result v2

    mul-float/2addr v2, p3

    iput v2, v1, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->centerY:F

    .line 246
    iget v2, p0, Lcom/google/android/videos/tagging/Tag$FaceRectTag;->faceWidth:F

    iget v3, v0, Lcom/google/android/videos/tagging/Tag$FaceRectTag;->timeMillis:I

    iget v4, v0, Lcom/google/android/videos/tagging/Tag$FaceRectTag;->faceWidth:F

    invoke-virtual {p0, p1, v2, v3, v4}, Lcom/google/android/videos/tagging/Tag$FaceRectTag;->interpolate(IFIF)F

    move-result v2

    mul-float/2addr v2, p2

    div-float/2addr v2, v5

    iput v2, v1, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->ovalA:F

    .line 248
    iget v2, p0, Lcom/google/android/videos/tagging/Tag$FaceRectTag;->angle:F

    iget v3, v0, Lcom/google/android/videos/tagging/Tag$FaceRectTag;->timeMillis:I

    iget v4, v0, Lcom/google/android/videos/tagging/Tag$FaceRectTag;->angle:F

    invoke-virtual {p0, p1, v2, v3, v4}, Lcom/google/android/videos/tagging/Tag$FaceRectTag;->interpolateAngle(IFIF)F

    move-result v2

    iput v2, v1, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->angle:F

    .line 255
    .end local v0    # "nextTag":Lcom/google/android/videos/tagging/Tag$FaceRectTag;
    :goto_0
    iget v2, v1, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->ovalA:F

    float-to-double v4, p4

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    div-double/2addr v4, v6

    const-wide v6, 0x3ff5555560000000L    # 1.3333333730697632

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iput v2, v1, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->ovalA:F

    .line 256
    iget v2, v1, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->ovalA:F

    const v3, 0x3faaaaab

    mul-float/2addr v2, v3

    iput v2, v1, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->ovalB:F

    .line 257
    return-object v1

    .line 250
    :cond_0
    iget v2, p0, Lcom/google/android/videos/tagging/Tag$FaceRectTag;->x:F

    mul-float/2addr v2, p2

    iput v2, v1, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->centerX:F

    .line 251
    iget v2, p0, Lcom/google/android/videos/tagging/Tag$FaceRectTag;->y:F

    mul-float/2addr v2, p3

    iput v2, v1, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->centerY:F

    .line 252
    iget v2, p0, Lcom/google/android/videos/tagging/Tag$FaceRectTag;->faceWidth:F

    mul-float/2addr v2, p2

    div-float/2addr v2, v5

    iput v2, v1, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->ovalA:F

    .line 253
    iget v2, p0, Lcom/google/android/videos/tagging/Tag$FaceRectTag;->angle:F

    iput v2, v1, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->angle:F

    goto :goto_0
.end method
