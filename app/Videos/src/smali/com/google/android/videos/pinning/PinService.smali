.class public Lcom/google/android/videos/pinning/PinService;
.super Landroid/app/IntentService;
.source "PinService.java"


# instance fields
.field private database:Lcom/google/android/videos/store/Database;

.field private eventLogger:Lcom/google/android/videos/logging/EventLogger;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 73
    const-class v0, Lcom/google/android/videos/pinning/PinService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 74
    return-void
.end method

.method private clearError(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/videos/pinning/PinService;->database:Lcom/google/android/videos/store/Database;

    invoke-static {v0, p1, p2}, Lcom/google/android/videos/pinning/PinningDbHelper;->clearError(Lcom/google/android/videos/store/Database;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    invoke-static {p0}, Lcom/google/android/videos/pinning/TransferService;->createIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/pinning/PinService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 128
    :cond_0
    return-void
.end method

.method private static getPinIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZII)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "pin"    # Z
    .param p4, "quality"    # I
    .param p5, "location"    # I

    .prologue
    .line 39
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 40
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 41
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/videos/pinning/PinService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 42
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.google.android.videos.SET_PINNED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 43
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 44
    const-string v1, "pinned"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 45
    const-string v1, "video_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 46
    if-eqz p3, :cond_0

    .line 47
    const-string v1, "quality"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 48
    const-string v1, "storage"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 50
    :cond_0
    return-object v0
.end method

.method public static requestClearError(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;

    .prologue
    .line 63
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 64
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 65
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/videos/pinning/PinService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 66
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.google.android.videos.CLEAR_ERROR"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 67
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 68
    const-string v1, "video_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 69
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 70
    return-void
.end method

.method public static requestPin(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "quality"    # I
    .param p4, "storage"    # I

    .prologue
    .line 59
    const/4 v3, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/pinning/PinService;->getPinIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZII)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 60
    return-void
.end method

.method public static requestUnpin(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;

    .prologue
    const/4 v4, -0x1

    .line 54
    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, v4

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/pinning/PinService;->getPinIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZII)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 55
    return-void
.end method

.method private setPinned(Ljava/lang/String;Ljava/lang/String;ZII)V
    .locals 2
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "pin"    # Z
    .param p4, "quality"    # I
    .param p5, "storage"    # I

    .prologue
    .line 113
    iget-object v1, p0, Lcom/google/android/videos/pinning/PinService;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    invoke-interface {v1, p2, p3, p4, p5}, Lcom/google/android/videos/logging/EventLogger;->onPinAction(Ljava/lang/String;ZII)V

    .line 114
    if-eqz p3, :cond_1

    iget-object v1, p0, Lcom/google/android/videos/pinning/PinService;->database:Lcom/google/android/videos/store/Database;

    invoke-static {v1, p1, p2, p4, p5}, Lcom/google/android/videos/pinning/PinningDbHelper;->pin(Lcom/google/android/videos/store/Database;Ljava/lang/String;Ljava/lang/String;II)Z

    move-result v0

    .line 117
    .local v0, "pinToggled":Z
    :goto_0
    if-eqz v0, :cond_2

    .line 118
    invoke-static {p0}, Lcom/google/android/videos/pinning/TransferService;->createIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/videos/pinning/PinService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 122
    :cond_0
    :goto_1
    return-void

    .line 114
    .end local v0    # "pinToggled":Z
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/pinning/PinService;->database:Lcom/google/android/videos/store/Database;

    invoke-static {v1, p1, p2}, Lcom/google/android/videos/pinning/PinningDbHelper;->unpin(Lcom/google/android/videos/store/Database;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 119
    .restart local v0    # "pinToggled":Z
    :cond_2
    if-nez p3, :cond_0

    .line 120
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/pinning/PinService;->clearError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 78
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 83
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 84
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v0

    .line 85
    .local v0, "videosGlobals":Lcom/google/android/videos/VideosGlobals;
    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/pinning/PinService;->database:Lcom/google/android/videos/store/Database;

    .line 86
    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/pinning/PinService;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 87
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 91
    if-eqz p1, :cond_2

    .line 92
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    .line 93
    .local v6, "action":Ljava/lang/String;
    const-string v0, "com.google.android.videos.SET_PINNED"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 94
    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 95
    .local v1, "account":Ljava/lang/String;
    const-string v0, "video_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 96
    .local v2, "videoId":Ljava/lang/String;
    const-string v0, "pinned"

    invoke-virtual {p1, v0, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 98
    .local v3, "pinned":Z
    if-eqz v3, :cond_0

    const-string v0, "quality"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    move v0, v8

    :goto_0
    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 99
    const-string v0, "quality"

    invoke-virtual {p1, v0, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 101
    .local v4, "quality":I
    if-eqz v3, :cond_1

    const-string v0, "storage"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_1
    :goto_1
    invoke-static {v8}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 102
    const-string v0, "storage"

    invoke-virtual {p1, v0, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .local v5, "storage":I
    move-object v0, p0

    .line 103
    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/pinning/PinService;->setPinned(Ljava/lang/String;Ljava/lang/String;ZII)V

    .line 110
    .end local v1    # "account":Ljava/lang/String;
    .end local v2    # "videoId":Ljava/lang/String;
    .end local v3    # "pinned":Z
    .end local v4    # "quality":I
    .end local v5    # "storage":I
    .end local v6    # "action":Ljava/lang/String;
    :cond_2
    :goto_2
    return-void

    .restart local v1    # "account":Ljava/lang/String;
    .restart local v2    # "videoId":Ljava/lang/String;
    .restart local v3    # "pinned":Z
    .restart local v6    # "action":Ljava/lang/String;
    :cond_3
    move v0, v7

    .line 98
    goto :goto_0

    .restart local v4    # "quality":I
    :cond_4
    move v8, v7

    .line 101
    goto :goto_1

    .line 104
    .end local v1    # "account":Ljava/lang/String;
    .end local v2    # "videoId":Ljava/lang/String;
    .end local v3    # "pinned":Z
    .end local v4    # "quality":I
    :cond_5
    const-string v0, "com.google.android.videos.CLEAR_ERROR"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 105
    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 106
    .restart local v1    # "account":Ljava/lang/String;
    const-string v0, "video_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 107
    .restart local v2    # "videoId":Ljava/lang/String;
    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/pinning/PinService;->clearError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method
