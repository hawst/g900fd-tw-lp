.class public Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;
.super Ljava/lang/Object;
.source "BandwidthBucketHistoryManager.java"


# instance fields
.field private final context:Landroid/content/Context;

.field private currentHistory:Lcom/google/android/videos/proto/BandwidthBucketHistory;

.field private histories:Lcom/google/android/videos/cache/InMemoryLruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/cache/InMemoryLruCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/proto/BandwidthBucketHistory;",
            ">;"
        }
    .end annotation
.end field

.field private final minTotalCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "minTotalCount"    # I

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->context:Landroid/content/Context;

    .line 46
    iput p2, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->minTotalCount:I

    .line 47
    return-void
.end method

.method private createBandwidthBucket(JI)Lcom/google/android/videos/proto/BandwidthBucket;
    .locals 1
    .param p1, "lowBound"    # J
    .param p3, "count"    # I

    .prologue
    .line 231
    new-instance v0, Lcom/google/android/videos/proto/BandwidthBucket;

    invoke-direct {v0}, Lcom/google/android/videos/proto/BandwidthBucket;-><init>()V

    .line 232
    .local v0, "bandwidthBucket":Lcom/google/android/videos/proto/BandwidthBucket;
    iput-wide p1, v0, Lcom/google/android/videos/proto/BandwidthBucket;->lowBound:J

    .line 233
    iput p3, v0, Lcom/google/android/videos/proto/BandwidthBucket;->count:I

    .line 234
    return-object v0
.end method

.method private createBandwidthBucketHistory(Ljava/lang/String;)Lcom/google/android/videos/proto/BandwidthBucketHistory;
    .locals 7
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 213
    new-instance v0, Lcom/google/android/videos/proto/BandwidthBucketHistory;

    invoke-direct {v0}, Lcom/google/android/videos/proto/BandwidthBucketHistory;-><init>()V

    .line 214
    .local v0, "history":Lcom/google/android/videos/proto/BandwidthBucketHistory;
    iput-object p1, v0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->key:Ljava/lang/String;

    .line 216
    const/16 v1, 0xa

    new-array v1, v1, [Lcom/google/android/videos/proto/BandwidthBucket;

    iput-object v1, v0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    .line 217
    iget-object v1, v0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    const-wide/32 v2, 0x100001

    invoke-direct {p0, v2, v3, v6}, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->createBandwidthBucket(JI)Lcom/google/android/videos/proto/BandwidthBucket;

    move-result-object v2

    aput-object v2, v1, v6

    .line 218
    iget-object v1, v0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    const/4 v2, 0x1

    const-wide/32 v4, 0xe0001

    invoke-direct {p0, v4, v5, v6}, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->createBandwidthBucket(JI)Lcom/google/android/videos/proto/BandwidthBucket;

    move-result-object v3

    aput-object v3, v1, v2

    .line 219
    iget-object v1, v0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    const/4 v2, 0x2

    const-wide/32 v4, 0xc0001

    invoke-direct {p0, v4, v5, v6}, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->createBandwidthBucket(JI)Lcom/google/android/videos/proto/BandwidthBucket;

    move-result-object v3

    aput-object v3, v1, v2

    .line 220
    iget-object v1, v0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    const/4 v2, 0x3

    const-wide/32 v4, 0xa0001

    invoke-direct {p0, v4, v5, v6}, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->createBandwidthBucket(JI)Lcom/google/android/videos/proto/BandwidthBucket;

    move-result-object v3

    aput-object v3, v1, v2

    .line 221
    iget-object v1, v0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    const/4 v2, 0x4

    const-wide/32 v4, 0x80001

    invoke-direct {p0, v4, v5, v6}, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->createBandwidthBucket(JI)Lcom/google/android/videos/proto/BandwidthBucket;

    move-result-object v3

    aput-object v3, v1, v2

    .line 222
    iget-object v1, v0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    const/4 v2, 0x5

    const-wide/32 v4, 0x60001

    invoke-direct {p0, v4, v5, v6}, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->createBandwidthBucket(JI)Lcom/google/android/videos/proto/BandwidthBucket;

    move-result-object v3

    aput-object v3, v1, v2

    .line 223
    iget-object v1, v0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    const/4 v2, 0x6

    const-wide/32 v4, 0x40001

    invoke-direct {p0, v4, v5, v6}, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->createBandwidthBucket(JI)Lcom/google/android/videos/proto/BandwidthBucket;

    move-result-object v3

    aput-object v3, v1, v2

    .line 224
    iget-object v1, v0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    const/4 v2, 0x7

    const-wide/32 v4, 0x20001

    invoke-direct {p0, v4, v5, v6}, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->createBandwidthBucket(JI)Lcom/google/android/videos/proto/BandwidthBucket;

    move-result-object v3

    aput-object v3, v1, v2

    .line 225
    iget-object v1, v0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    const/16 v2, 0x8

    const-wide/32 v4, 0x10001

    invoke-direct {p0, v4, v5, v6}, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->createBandwidthBucket(JI)Lcom/google/android/videos/proto/BandwidthBucket;

    move-result-object v3

    aput-object v3, v1, v2

    .line 226
    iget-object v1, v0, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    const/16 v2, 0x9

    const-wide/16 v4, 0x0

    invoke-direct {p0, v4, v5, v6}, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->createBandwidthBucket(JI)Lcom/google/android/videos/proto/BandwidthBucket;

    move-result-object v3

    aput-object v3, v1, v2

    .line 227
    return-object v0
.end method

.method private getNetworkIdentifier(Landroid/net/NetworkInfo;)Ljava/lang/String;
    .locals 7
    .param p1, "networkInfo"    # Landroid/net/NetworkInfo;

    .prologue
    const/4 v4, 0x0

    .line 184
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getType()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    move-object v1, v4

    .line 209
    :cond_0
    :goto_0
    return-object v1

    .line 186
    :sswitch_0
    iget-object v5, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->context:Landroid/content/Context;

    const-string v6, "wifi"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/wifi/WifiManager;

    .line 187
    .local v3, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    .line 188
    .local v0, "connectionInfo":Landroid/net/wifi/WifiInfo;
    if-nez v0, :cond_1

    move-object v1, v4

    .line 189
    goto :goto_0

    .line 191
    :cond_1
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v1

    .line 192
    .local v1, "networkIdentifier":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    move-object v1, v4

    .line 193
    goto :goto_0

    .line 198
    .end local v0    # "connectionInfo":Landroid/net/wifi/WifiInfo;
    .end local v1    # "networkIdentifier":Ljava/lang/String;
    .end local v3    # "wifiManager":Landroid/net/wifi/WifiManager;
    :sswitch_1
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getType()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 199
    .restart local v1    # "networkIdentifier":Ljava/lang/String;
    goto :goto_0

    .line 201
    .end local v1    # "networkIdentifier":Ljava/lang/String;
    :sswitch_2
    iget-object v4, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->context:Landroid/content/Context;

    const-string v5, "phone"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 203
    .local v2, "telephonyManager":Landroid/telephony/TelephonyManager;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 205
    .restart local v1    # "networkIdentifier":Ljava/lang/String;
    goto :goto_0

    .line 184
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x1 -> :sswitch_0
        0x6 -> :sswitch_1
        0x9 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public addCount(J)V
    .locals 7
    .param p1, "bitrate"    # J

    .prologue
    .line 104
    const-wide/16 v4, 0x8

    div-long v0, p1, v4

    .line 105
    .local v0, "byterate":J
    iget-object v3, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->currentHistory:Lcom/google/android/videos/proto/BandwidthBucketHistory;

    if-nez v3, :cond_1

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 108
    :cond_1
    iget-object v3, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->currentHistory:Lcom/google/android/videos/proto/BandwidthBucketHistory;

    iget v4, v3, Lcom/google/android/videos/proto/BandwidthBucketHistory;->fadingCounter:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v3, Lcom/google/android/videos/proto/BandwidthBucketHistory;->fadingCounter:I

    .line 109
    iget-object v3, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->currentHistory:Lcom/google/android/videos/proto/BandwidthBucketHistory;

    iget v3, v3, Lcom/google/android/videos/proto/BandwidthBucketHistory;->fadingCounter:I

    rem-int/lit16 v3, v3, 0x1680

    if-nez v3, :cond_2

    .line 110
    iget-object v3, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->currentHistory:Lcom/google/android/videos/proto/BandwidthBucketHistory;

    const/4 v4, 0x0

    iput v4, v3, Lcom/google/android/videos/proto/BandwidthBucketHistory;->fadingCounter:I

    .line 112
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->currentHistory:Lcom/google/android/videos/proto/BandwidthBucketHistory;

    iget-object v3, v3, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    array-length v3, v3

    if-ge v2, v3, :cond_2

    .line 113
    iget-object v3, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->currentHistory:Lcom/google/android/videos/proto/BandwidthBucketHistory;

    iget-object v3, v3, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    aget-object v3, v3, v2

    iget v4, v3, Lcom/google/android/videos/proto/BandwidthBucket;->count:I

    int-to-float v4, v4

    const v5, 0x3f333333    # 0.7f

    mul-float/2addr v4, v5

    float-to-int v4, v4

    iput v4, v3, Lcom/google/android/videos/proto/BandwidthBucket;->count:I

    .line 112
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 116
    .end local v2    # "i":I
    :cond_2
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    iget-object v3, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->currentHistory:Lcom/google/android/videos/proto/BandwidthBucketHistory;

    iget-object v3, v3, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    array-length v3, v3

    if-ge v2, v3, :cond_0

    .line 117
    iget-object v3, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->currentHistory:Lcom/google/android/videos/proto/BandwidthBucketHistory;

    iget-object v3, v3, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    aget-object v3, v3, v2

    iget-wide v4, v3, Lcom/google/android/videos/proto/BandwidthBucket;->lowBound:J

    cmp-long v3, v0, v4

    if-ltz v3, :cond_3

    .line 118
    iget-object v3, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->currentHistory:Lcom/google/android/videos/proto/BandwidthBucketHistory;

    iget-object v3, v3, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    aget-object v3, v3, v2

    iget v4, v3, Lcom/google/android/videos/proto/BandwidthBucket;->count:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v3, Lcom/google/android/videos/proto/BandwidthBucket;->count:I

    goto :goto_0

    .line 116
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method public getBitrate(F)J
    .locals 8
    .param p1, "percentile"    # F

    .prologue
    const-wide/16 v6, -0x1

    .line 130
    const/4 v4, 0x0

    cmpl-float v4, p1, v4

    if-ltz v4, :cond_0

    const/high16 v4, 0x3f800000    # 1.0f

    cmpg-float v4, p1, v4

    if-gtz v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, Lcom/google/android/exoplayer/util/Assertions;->checkArgument(Z)V

    .line 131
    iget-object v4, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->currentHistory:Lcom/google/android/videos/proto/BandwidthBucketHistory;

    if-nez v4, :cond_1

    move-wide v4, v6

    .line 152
    :goto_1
    return-wide v4

    .line 130
    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    .line 134
    :cond_1
    const/4 v3, 0x0

    .line 135
    .local v3, "totalCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    iget-object v4, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->currentHistory:Lcom/google/android/videos/proto/BandwidthBucketHistory;

    iget-object v4, v4, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    array-length v4, v4

    if-ge v1, v4, :cond_2

    .line 136
    iget-object v4, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->currentHistory:Lcom/google/android/videos/proto/BandwidthBucketHistory;

    iget-object v4, v4, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    aget-object v4, v4, v1

    iget v4, v4, Lcom/google/android/videos/proto/BandwidthBucket;->count:I

    add-int/2addr v3, v4

    .line 135
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 138
    :cond_2
    iget v4, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->minTotalCount:I

    if-ge v3, v4, :cond_3

    move-wide v4, v6

    .line 139
    goto :goto_1

    .line 141
    :cond_3
    int-to-float v4, v3

    mul-float/2addr v4, p1

    float-to-int v2, v4

    .line 142
    .local v2, "targetCount":I
    const/4 v0, 0x0

    .line 143
    .local v0, "accumulatedCount":I
    const/4 v1, 0x0

    :goto_3
    iget-object v4, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->currentHistory:Lcom/google/android/videos/proto/BandwidthBucketHistory;

    iget-object v4, v4, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    array-length v4, v4

    if-ge v1, v4, :cond_5

    .line 145
    iget-object v4, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->currentHistory:Lcom/google/android/videos/proto/BandwidthBucketHistory;

    iget-object v4, v4, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    aget-object v4, v4, v1

    iget v4, v4, Lcom/google/android/videos/proto/BandwidthBucket;->count:I

    add-int/2addr v0, v4

    .line 146
    if-lt v0, v2, :cond_4

    .line 149
    iget-object v4, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->currentHistory:Lcom/google/android/videos/proto/BandwidthBucketHistory;

    iget-object v4, v4, Lcom/google/android/videos/proto/BandwidthBucketHistory;->bandwidthBuckets:[Lcom/google/android/videos/proto/BandwidthBucket;

    aget-object v4, v4, v1

    iget-wide v4, v4, Lcom/google/android/videos/proto/BandwidthBucket;->lowBound:J

    const-wide/16 v6, 0x8

    mul-long/2addr v4, v6

    goto :goto_1

    .line 143
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_5
    move-wide v4, v6

    .line 152
    goto :goto_1
.end method

.method public loadHistory()V
    .locals 9

    .prologue
    .line 75
    new-instance v3, Ljava/io/File;

    iget-object v6, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->context:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v6

    const-string v7, "bandwidth_bucket_history"

    invoke-direct {v3, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 76
    .local v3, "historyFile":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 78
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 79
    .local v5, "inStream":Ljava/io/FileInputStream;
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v6

    long-to-int v6, v6

    new-array v1, v6, [B

    .line 80
    .local v1, "bytes":[B
    invoke-virtual {v5, v1}, Ljava/io/FileInputStream;->read([B)I

    .line 81
    invoke-static {v1}, Lcom/google/android/videos/proto/BandwidthBucketHistories;->parseFrom([B)Lcom/google/android/videos/proto/BandwidthBucketHistories;

    move-result-object v0

    .line 82
    .local v0, "bucketHistories":Lcom/google/android/videos/proto/BandwidthBucketHistories;
    const/16 v6, 0x14

    invoke-static {v6}, Lcom/google/android/videos/cache/InMemoryLruCache;->create(I)Lcom/google/android/videos/cache/InMemoryLruCache;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->histories:Lcom/google/android/videos/cache/InMemoryLruCache;

    .line 83
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    iget-object v6, v0, Lcom/google/android/videos/proto/BandwidthBucketHistories;->histories:[Lcom/google/android/videos/proto/BandwidthBucketHistory;

    array-length v6, v6

    if-ge v4, v6, :cond_0

    .line 84
    iget-object v6, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->histories:Lcom/google/android/videos/cache/InMemoryLruCache;

    iget-object v7, v0, Lcom/google/android/videos/proto/BandwidthBucketHistories;->histories:[Lcom/google/android/videos/proto/BandwidthBucketHistory;

    aget-object v7, v7, v4

    iget-object v7, v7, Lcom/google/android/videos/proto/BandwidthBucketHistory;->key:Ljava/lang/String;

    iget-object v8, v0, Lcom/google/android/videos/proto/BandwidthBucketHistories;->histories:[Lcom/google/android/videos/proto/BandwidthBucketHistory;

    aget-object v8, v8, v4

    invoke-virtual {v6, v7, v8}, Lcom/google/android/videos/cache/InMemoryLruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 83
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 86
    :cond_0
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 95
    .end local v0    # "bucketHistories":Lcom/google/android/videos/proto/BandwidthBucketHistories;
    .end local v1    # "bytes":[B
    .end local v4    # "i":I
    .end local v5    # "inStream":Ljava/io/FileInputStream;
    :cond_1
    :goto_1
    return-void

    .line 87
    :catch_0
    move-exception v2

    .line 88
    .local v2, "e":Ljava/io/FileNotFoundException;
    const-string v6, "representation_selection_history file not found"

    invoke-static {v6, v2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 89
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v2

    .line 90
    .local v2, "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    const-string v6, "invalid protobuf"

    invoke-static {v6, v2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 91
    .end local v2    # "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    :catch_2
    move-exception v2

    .line 92
    .local v2, "e":Ljava/io/IOException;
    const-string v6, "error loading histories from representation_selection_history"

    invoke-static {v6, v2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public saveHistory()V
    .locals 6

    .prologue
    .line 53
    iget-object v4, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->histories:Lcom/google/android/videos/cache/InMemoryLruCache;

    if-nez v4, :cond_0

    .line 69
    :goto_0
    return-void

    .line 56
    :cond_0
    new-instance v2, Ljava/io/File;

    iget-object v4, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v4

    const-string v5, "bandwidth_bucket_history"

    invoke-direct {v2, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 57
    .local v2, "historyFile":Ljava/io/File;
    new-instance v0, Lcom/google/android/videos/proto/BandwidthBucketHistories;

    invoke-direct {v0}, Lcom/google/android/videos/proto/BandwidthBucketHistories;-><init>()V

    .line 58
    .local v0, "bucketHistories":Lcom/google/android/videos/proto/BandwidthBucketHistories;
    iget-object v4, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->histories:Lcom/google/android/videos/cache/InMemoryLruCache;

    invoke-virtual {v4}, Lcom/google/android/videos/cache/InMemoryLruCache;->getValues()Ljava/util/ArrayList;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->histories:Lcom/google/android/videos/cache/InMemoryLruCache;

    invoke-virtual {v5}, Lcom/google/android/videos/cache/InMemoryLruCache;->size()I

    move-result v5

    new-array v5, v5, [Lcom/google/android/videos/proto/BandwidthBucketHistory;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/google/android/videos/proto/BandwidthBucketHistory;

    iput-object v4, v0, Lcom/google/android/videos/proto/BandwidthBucketHistories;->histories:[Lcom/google/android/videos/proto/BandwidthBucketHistory;

    .line 61
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 62
    .local v3, "outStream":Ljava/io/FileOutputStream;
    invoke-static {v0}, Lcom/google/android/videos/proto/BandwidthBucketHistories;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/FileOutputStream;->write([B)V

    .line 63
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 64
    .end local v3    # "outStream":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v1

    .line 65
    .local v1, "e":Ljava/io/FileNotFoundException;
    const-string v4, "representation_selection_history file not found"

    invoke-static {v4, v1}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 66
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v1

    .line 67
    .local v1, "e":Ljava/io/IOException;
    const-string v4, "error saving histories to representation_selection_history"

    invoke-static {v4, v1}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public updateCurrentHistory(Landroid/net/NetworkInfo;)V
    .locals 6
    .param p1, "networkInfo"    # Landroid/net/NetworkInfo;

    .prologue
    const/4 v2, 0x0

    .line 162
    if-nez p1, :cond_1

    .line 163
    iput-object v2, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->currentHistory:Lcom/google/android/videos/proto/BandwidthBucketHistory;

    .line 180
    :cond_0
    :goto_0
    return-void

    .line 166
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->getNetworkIdentifier(Landroid/net/NetworkInfo;)Ljava/lang/String;

    move-result-object v1

    .line 167
    .local v1, "networkIdentifier":Ljava/lang/String;
    if-nez v1, :cond_2

    .line 168
    iput-object v2, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->currentHistory:Lcom/google/android/videos/proto/BandwidthBucketHistory;

    goto :goto_0

    .line 171
    :cond_2
    iget-object v2, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->histories:Lcom/google/android/videos/cache/InMemoryLruCache;

    if-nez v2, :cond_3

    .line 172
    const/16 v2, 0x14

    invoke-static {v2}, Lcom/google/android/videos/cache/InMemoryLruCache;->create(I)Lcom/google/android/videos/cache/InMemoryLruCache;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->histories:Lcom/google/android/videos/cache/InMemoryLruCache;

    .line 174
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getType()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v1}, Lcom/google/android/videos/utils/Hashing;->sha1(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x2

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 175
    .local v0, "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->histories:Lcom/google/android/videos/cache/InMemoryLruCache;

    invoke-virtual {v2, v0}, Lcom/google/android/videos/cache/InMemoryLruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/proto/BandwidthBucketHistory;

    iput-object v2, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->currentHistory:Lcom/google/android/videos/proto/BandwidthBucketHistory;

    .line 176
    iget-object v2, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->currentHistory:Lcom/google/android/videos/proto/BandwidthBucketHistory;

    if-nez v2, :cond_0

    .line 177
    invoke-direct {p0, v0}, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->createBandwidthBucketHistory(Ljava/lang/String;)Lcom/google/android/videos/proto/BandwidthBucketHistory;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->currentHistory:Lcom/google/android/videos/proto/BandwidthBucketHistory;

    .line 178
    iget-object v2, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->histories:Lcom/google/android/videos/cache/InMemoryLruCache;

    iget-object v3, p0, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->currentHistory:Lcom/google/android/videos/proto/BandwidthBucketHistory;

    invoke-virtual {v2, v0, v3}, Lcom/google/android/videos/cache/InMemoryLruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method
