.class public final Lcom/google/android/videos/api/UserLibraryRequest;
.super Lcom/google/android/videos/async/Request;
.source "UserLibraryRequest.java"


# instance fields
.field public final assetResourceIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final includeParents:Z

.field public final maxResults:I

.field public final pageToken:Ljava/lang/String;

.field public final snapshotToken:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLjava/lang/String;ILjava/lang/String;Ljava/util/List;)V
    .locals 4
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "includeParents"    # Z
    .param p3, "snapshotToken"    # Ljava/lang/String;
    .param p4, "maxResults"    # I
    .param p5, "pageToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p6, "assetResourceIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/videos/async/Request;-><init>(Ljava/lang/String;)V

    .line 62
    iput-boolean p2, p0, Lcom/google/android/videos/api/UserLibraryRequest;->includeParents:Z

    .line 63
    iput-object p3, p0, Lcom/google/android/videos/api/UserLibraryRequest;->snapshotToken:Ljava/lang/String;

    .line 64
    if-gtz p4, :cond_0

    const/4 v0, -0x1

    if-ne p4, v0, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "maxResults must be positive or NO_MAX_RESULTS"

    invoke-static {v0, v3}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;)V

    .line 66
    iput p4, p0, Lcom/google/android/videos/api/UserLibraryRequest;->maxResults:I

    .line 67
    iput-object p5, p0, Lcom/google/android/videos/api/UserLibraryRequest;->pageToken:Ljava/lang/String;

    .line 68
    if-eqz p6, :cond_1

    invoke-interface {p6}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    const-string v0, "assetResourceIds must be null or non-empty"

    invoke-static {v1, v0}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;)V

    .line 70
    if-nez p6, :cond_4

    const/4 v0, 0x0

    :goto_1
    iput-object v0, p0, Lcom/google/android/videos/api/UserLibraryRequest;->assetResourceIds:Ljava/util/List;

    .line 72
    return-void

    :cond_3
    move v0, v1

    .line 64
    goto :goto_0

    .line 70
    :cond_4
    invoke-static {p6}, Lcom/google/android/videos/utils/CollectionUtil;->immutableCopyOf(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    goto :goto_1
.end method

.method public varargs constructor <init>(Ljava/lang/String;ZLjava/lang/String;ILjava/lang/String;[Ljava/lang/String;)V
    .locals 7
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "includeParents"    # Z
    .param p3, "snapshotToken"    # Ljava/lang/String;
    .param p4, "maxResults"    # I
    .param p5, "pageToken"    # Ljava/lang/String;
    .param p6, "assetResourceIds"    # [Ljava/lang/String;

    .prologue
    .line 54
    if-eqz p6, :cond_0

    array-length v0, p6

    if-nez v0, :cond_0

    const/4 v6, 0x0

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/api/UserLibraryRequest;-><init>(Ljava/lang/String;ZLjava/lang/String;ILjava/lang/String;Ljava/util/List;)V

    .line 57
    return-void

    .line 54
    :cond_0
    invoke-static {p6}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v0}, Lcom/google/android/videos/utils/CollectionUtil;->immutableList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    goto :goto_0
.end method


# virtual methods
.method public cloneWithNewTokens(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/api/UserLibraryRequest;
    .locals 7
    .param p1, "newSnapshotToken"    # Ljava/lang/String;
    .param p2, "newPageToken"    # Ljava/lang/String;

    .prologue
    .line 80
    new-instance v0, Lcom/google/android/videos/api/UserLibraryRequest;

    iget-object v1, p0, Lcom/google/android/videos/api/UserLibraryRequest;->account:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/android/videos/api/UserLibraryRequest;->includeParents:Z

    iget v4, p0, Lcom/google/android/videos/api/UserLibraryRequest;->maxResults:I

    iget-object v6, p0, Lcom/google/android/videos/api/UserLibraryRequest;->assetResourceIds:Ljava/util/List;

    move-object v3, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/api/UserLibraryRequest;-><init>(Ljava/lang/String;ZLjava/lang/String;ILjava/lang/String;Ljava/util/List;)V

    return-object v0
.end method

.method public cloneWithNoTokens()Lcom/google/android/videos/api/UserLibraryRequest;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 75
    new-instance v0, Lcom/google/android/videos/api/UserLibraryRequest;

    iget-object v1, p0, Lcom/google/android/videos/api/UserLibraryRequest;->account:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/android/videos/api/UserLibraryRequest;->includeParents:Z

    iget v4, p0, Lcom/google/android/videos/api/UserLibraryRequest;->maxResults:I

    iget-object v6, p0, Lcom/google/android/videos/api/UserLibraryRequest;->assetResourceIds:Ljava/util/List;

    move-object v5, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/api/UserLibraryRequest;-><init>(Ljava/lang/String;ZLjava/lang/String;ILjava/lang/String;Ljava/util/List;)V

    return-object v0
.end method
