.class public interface abstract Lcom/google/android/videos/Config;
.super Ljava/lang/Object;
.source "Config.java"


# virtual methods
.method public abstract allowDownloads()Z
.end method

.method public abstract allowSurroundSoundFormats()Z
.end method

.method public abstract anyVerticalEnabled(Ljava/lang/String;)Z
.end method

.method public abstract appendDoNotCountParam()Z
.end method

.method public abstract atHomeRobotTokenRequestUri()Ljava/lang/String;
.end method

.method public abstract audioVirtualizerEnabled()Z
.end method

.method public abstract baseApiUri()Landroid/net/Uri;
.end method

.method public abstract baseKnowledgeUri()Landroid/net/Uri;
.end method

.method public abstract blacklistedVersionsRegex()Ljava/lang/String;
.end method

.method public abstract castDebuggingEnabled()Z
.end method

.method public abstract castV2Enabled()Z
.end method

.method public abstract castV2ReceiverAppId()Ljava/lang/String;
.end method

.method public abstract dashVideoFormats()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract dashVideoHighEdgeFormats()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract dashVideoLowEdgeFormats()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract deviceCapabilitiesFilterEnabled()Z
.end method

.method public abstract deviceCountry()Ljava/lang/String;
.end method

.method public abstract disableWatchNow()Z
.end method

.method public abstract dogfoodEnabled()Z
.end method

.method public abstract exoAbrAlgorithm()I
.end method

.method public abstract exoAlternateRedirectEnabled()Z
.end method

.method public abstract exoAudioTrackMinBufferMultiplicationFactor()F
.end method

.method public abstract exoBandwidthBucketHistoryMinCount()I
.end method

.method public abstract exoBandwidthBucketHistorySelectionPercentile()F
.end method

.method public abstract exoBandwidthFraction()F
.end method

.method public abstract exoBbaOneLowThresholdQueueSize()I
.end method

.method public abstract exoBbaOneSlope()I
.end method

.method public abstract exoBufferChunkCount()I
.end method

.method public abstract exoBufferChunkSize()I
.end method

.method public abstract exoEarlyPlaybackCutoffTimeMs()I
.end method

.method public abstract exoHighPoolLoad()F
.end method

.method public abstract exoHighWatermarkMs()I
.end method

.method public abstract exoLoadTimeoutMs()I
.end method

.method public abstract exoLowPoolLoad()F
.end method

.method public abstract exoLowWatermarkMs()I
.end method

.method public abstract exoLqVideoHeightCap(Landroid/view/Display;Z)I
.end method

.method public abstract exoMinBufferMs()I
.end method

.method public abstract exoMinLoadableRetryCount()I
.end method

.method public abstract exoMinRebufferMs()I
.end method

.method public abstract exoOfflineVideoHeightCap()I
.end method

.method public abstract exoOnlineVideoHeightCap(Landroid/view/Display;)I
.end method

.method public abstract exoPlayClearSamplesWithoutKeys()Z
.end method

.method public abstract exoSmoothFrameRelease()Z
.end method

.method public abstract exoStartResolutionAlgorithm()I
.end method

.method public abstract exoUseBlockBufferPool()Z
.end method

.method public abstract exoVsyncFrameRelease(Landroid/view/Display;)Z
.end method

.method public abstract externalApiEnabled()Z
.end method

.method public abstract fallbackDrmErrorCodes()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract feedbackProductId()I
.end method

.method public abstract feedbackSubmitUrlTemplate()Ljava/lang/String;
.end method

.method public abstract feedbackTokenExtractorRegex()Ljava/lang/String;
.end method

.method public abstract feedbackTokenUrl()Ljava/lang/String;
.end method

.method public abstract fieldProvisionedFormats()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract forceAppLevelDrm()Z
.end method

.method public abstract forceMirrorMode()Z
.end method

.method public abstract freeMovieWelcomeEnabled()Z
.end method

.method public abstract gcmMessagingEnabled()Z
.end method

.method public abstract gcmRegistrationEnabled()Z
.end method

.method public abstract generateHttp204Url()Ljava/lang/String;
.end method

.method public abstract getExperimentId()Ljava/lang/String;
.end method

.method public abstract getPlayLogImpressionSettleTimeMillis()I
.end method

.method public abstract gmsCoreAvailable()Z
.end method

.method public abstract gservicesId()J
.end method

.method public abstract knowledgeDimEntitiesAfterDisappearingForMillis()I
.end method

.method public abstract knowledgeDontDimEntitiesReappearingWithinMillis()I
.end method

.method public abstract knowledgeDontRemoveEntitiesReappearingWithinMillis()I
.end method

.method public abstract knowledgeEnabled()Z
.end method

.method public abstract knowledgeFeedbackTypeId()I
.end method

.method public abstract knowledgeRecheckDataAfterMillis()J
.end method

.method public abstract knowledgeRemoveEntitiesAfterDisappearingForMillis()I
.end method

.method public abstract knowledgeShowRecentActorsWithinMillis()I
.end method

.method public abstract latestVersion()I
.end method

.method public abstract legacyBufferingEventInitialIgnoreWindowMillis()J
.end method

.method public abstract legacyBufferingEventWindowMillis()J
.end method

.method public abstract legacyBufferingEventsForQualityDrop()I
.end method

.method public abstract maxConcurrentLicenseTasks()I
.end method

.method public abstract maxConcurrentOrBackedOffPinningTasks()I
.end method

.method public abstract maxConcurrentUpdateUserdataTasks()I
.end method

.method public abstract maxLicenseRefreshTaskRetryDelayMillis()I
.end method

.method public abstract maxLicenseReleaseTaskRetryDelayMillis()I
.end method

.method public abstract maxNewContentNotificationDelayMillis()J
.end method

.method public abstract maxPinningTaskRetries()I
.end method

.method public abstract maxPinningTaskRetryDelayMillis()I
.end method

.method public abstract maxSuggestionClusters()I
.end method

.method public abstract maxSuggestionsPerCluster()I
.end method

.method public abstract maxUpdateUserdataTaskRetries()I
.end method

.method public abstract maxUpdateUserdataTaskRetryDelayMillis()I
.end method

.method public abstract minIntervalBetweenHerrevadReportSeconds()I
.end method

.method public abstract minLicenseRefreshTaskRetryDelayMillis()I
.end method

.method public abstract minLicenseReleaseTaskRetryDelayMillis()I
.end method

.method public abstract minPinningTaskRetryDelayMillis()I
.end method

.method public abstract minUpdateUserdataTaskRetryDelayMillis()I
.end method

.method public abstract minimumVersion()I
.end method

.method public abstract moviesVerticalEnabled(Ljava/lang/String;)Z
.end method

.method public abstract moviesWelcomeFreeBrowseUri()Landroid/net/Uri;
.end method

.method public abstract multiAudioEnabled()Z
.end method

.method public abstract needsSystemUpdate()Z
.end method

.method public abstract orderedDashDownloadFormats()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract orderedDashHqAudioFormats()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract orderedDashMqAudioFormats()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract orderedDownloadFormats()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract orderedHqStreamingFormats(Landroid/view/Display;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Display;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract orderedLqStreamingFormats(Landroid/view/Display;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Display;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract orderedMqStreamingFormats(Landroid/view/Display;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Display;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract panoEnabled()Z
.end method

.method public abstract playbackDebugLoggingEnabled()Z
.end method

.method public abstract recentActiveMillis()J
.end method

.method public abstract refreshLicensesOlderThanMillis()J
.end method

.method public abstract resyncFullShowAfterMillis()J
.end method

.method public abstract resyncSeasonAfterMillis()J
.end method

.method public abstract resyncShowAfterMillis()J
.end method

.method public abstract resyncVideoAfterMillis()J
.end method

.method public abstract retryCencDrmErrorCodes()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract showsVerticalEnabled(Ljava/lang/String;)Z
.end method

.method public abstract showsWelcomeFreeBrowseUri()Landroid/net/Uri;
.end method

.method public abstract soundWelcomeVideoId()Ljava/lang/String;
.end method

.method public abstract suggestionsEnabled()Z
.end method

.method public abstract supportsAdaptivePlayback()Z
.end method

.method public abstract transferServicePingIntervalMillis()J
.end method

.method public abstract useDashForDownloads()Z
.end method

.method public abstract useDashForStreaming()Z
.end method

.method public abstract usePlaybackPreparationLogger()Z
.end method

.method public abstract useSslForDownloads()Z
.end method

.method public abstract useSslForStreaming()Z
.end method

.method public abstract videoHeightCap(Landroid/view/Display;)I
.end method

.method public abstract wvCencDrmServerUri()Ljava/lang/String;
.end method

.method public abstract wvClassicDrmServerUri()Ljava/lang/String;
.end method

.method public abstract wvPortalName()Ljava/lang/String;
.end method

.method public abstract wvProvisioningServerUri(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract youtubeStatsUri()Landroid/net/Uri;
.end method
