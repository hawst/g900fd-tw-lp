.class public final Lcom/google/android/videos/async/ControllableAsyncRequester;
.super Ljava/lang/Object;
.source "ControllableAsyncRequester.java"

# interfaces
.implements Lcom/google/android/videos/async/Requester;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Requester",
        "<",
        "Lcom/google/android/videos/async/ControllableRequest",
        "<TR;>;TE;>;"
    }
.end annotation


# instance fields
.field private final executor:Ljava/util/concurrent/Executor;

.field private final target:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<TR;TE;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)V
    .locals 0
    .param p1, "executor"    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/videos/async/Requester",
            "<TR;TE;>;)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p0, "this":Lcom/google/android/videos/async/ControllableAsyncRequester;, "Lcom/google/android/videos/async/ControllableAsyncRequester<TR;TE;>;"
    .local p2, "target":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TR;TE;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/android/videos/async/ControllableAsyncRequester;->executor:Ljava/util/concurrent/Executor;

    .line 32
    iput-object p2, p0, Lcom/google/android/videos/async/ControllableAsyncRequester;->target:Lcom/google/android/videos/async/Requester;

    .line 33
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/async/ControllableAsyncRequester;)Lcom/google/android/videos/async/Requester;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/async/ControllableAsyncRequester;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/videos/async/ControllableAsyncRequester;->target:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public static create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/ControllableAsyncRequester;
    .locals 1
    .param p0, "executor"    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/videos/async/Requester",
            "<TR;TE;>;)",
            "Lcom/google/android/videos/async/ControllableAsyncRequester",
            "<TR;TE;>;"
        }
    .end annotation

    .prologue
    .line 25
    .local p1, "target":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TR;TE;>;"
    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    new-instance v0, Lcom/google/android/videos/async/ControllableAsyncRequester;

    invoke-direct {v0, p0, p1}, Lcom/google/android/videos/async/ControllableAsyncRequester;-><init>(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)V

    return-object v0
.end method


# virtual methods
.method public request(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/Callback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<TR;>;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<TR;>;TE;>;)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p0, "this":Lcom/google/android/videos/async/ControllableAsyncRequester;, "Lcom/google/android/videos/async/ControllableAsyncRequester<TR;TE;>;"
    .local p1, "request":Lcom/google/android/videos/async/ControllableRequest;, "Lcom/google/android/videos/async/ControllableRequest<TR;>;"
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/async/ControllableRequest<TR;>;TE;>;"
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    :try_start_0
    iget-object v1, p0, Lcom/google/android/videos/async/ControllableAsyncRequester;->executor:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/google/android/videos/async/ControllableAsyncRequester$1;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/videos/async/ControllableAsyncRequester$1;-><init>(Lcom/google/android/videos/async/ControllableAsyncRequester;Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/Callback;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    :goto_0
    return-void

    .line 72
    :catch_0
    move-exception v0

    .line 73
    .local v0, "e":Ljava/util/concurrent/RejectedExecutionException;
    invoke-interface {p2, p1, v0}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public bridge synthetic request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/google/android/videos/async/Callback;

    .prologue
    .line 17
    .local p0, "this":Lcom/google/android/videos/async/ControllableAsyncRequester;, "Lcom/google/android/videos/async/ControllableAsyncRequester<TR;TE;>;"
    check-cast p1, Lcom/google/android/videos/async/ControllableRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/async/ControllableAsyncRequester;->request(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/Callback;)V

    return-void
.end method
