.class abstract Lcom/google/android/videos/player/overlay/AudioCcDialog$ListHelper;
.super Landroid/widget/ArrayAdapter;
.source "AudioCcDialog.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/overlay/AudioCcDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "ListHelper"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/widget/ArrayAdapter",
        "<TT;>;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field private final itemPadding:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 196
    .local p0, "this":Lcom/google/android/videos/player/overlay/AudioCcDialog$ListHelper;, "Lcom/google/android/videos/player/overlay/AudioCcDialog$ListHelper<TT;>;"
    .local p2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 197
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 198
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/videos/player/overlay/AudioCcDialog$ListHelper;->add(Ljava/lang/Object;)V

    .line 197
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 200
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e01c2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/videos/player/overlay/AudioCcDialog$ListHelper;->itemPadding:I

    .line 202
    return-void
.end method


# virtual methods
.method protected adjustItemPadding(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 213
    .local p0, "this":Lcom/google/android/videos/player/overlay/AudioCcDialog$ListHelper;, "Lcom/google/android/videos/player/overlay/AudioCcDialog$ListHelper<TT;>;"
    iget v0, p0, Lcom/google/android/videos/player/overlay/AudioCcDialog$ListHelper;->itemPadding:I

    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    iget v2, p0, Lcom/google/android/videos/player/overlay/AudioCcDialog$ListHelper;->itemPadding:I

    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 214
    return-void
.end method

.method public getFooter(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 209
    .local p0, "this":Lcom/google/android/videos/player/overlay/AudioCcDialog$ListHelper;, "Lcom/google/android/videos/player/overlay/AudioCcDialog$ListHelper<TT;>;"
    const/4 v0, 0x0

    return-object v0
.end method
