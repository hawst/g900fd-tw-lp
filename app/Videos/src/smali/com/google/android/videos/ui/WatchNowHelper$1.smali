.class Lcom/google/android/videos/ui/WatchNowHelper$1;
.super Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;
.source "WatchNowHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/ui/WatchNowHelper;-><init>(Lcom/google/android/videos/Config;Lcom/google/android/videos/store/ConfigurationStore;Landroid/content/SharedPreferences;Lcom/google/android/videos/activity/HomeActivity;Landroid/view/View;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/PurchaseStoreSync;Lcom/google/android/videos/store/PosterStore;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/utils/ErrorHelper;Lcom/google/android/videos/ui/SyncHelper;Lcom/google/android/videos/ui/PinHelper;Lcom/google/android/videos/bitmap/BitmapRequesters;Ljava/util/concurrent/Executor;Lcom/google/android/videos/api/ApiRequesters;Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;Lcom/google/android/videos/remote/MediaRouteProvider;Lcom/google/android/videos/remote/RemoteTracker;Lcom/google/android/videos/utils/DownloadedOnlyManager;ZLcom/google/android/videos/logging/UiElementNode;Lcom/google/android/videos/logging/UiEventLoggingHelper;Lcom/google/android/videos/ui/AssetImageUriCreator;Lcom/google/android/videos/utils/NetworkStatus;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/ui/WatchNowHelper;


# direct methods
.method constructor <init>(Lcom/google/android/videos/ui/WatchNowHelper;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/store/StoreStatusMonitor;I)V
    .locals 0
    .param p2, "x0"    # Lcom/google/android/videos/store/ConfigurationStore;
    .param p3, "x1"    # Lcom/google/android/videos/store/StoreStatusMonitor;
    .param p4, "x2"    # I

    .prologue
    .line 175
    iput-object p1, p0, Lcom/google/android/videos/ui/WatchNowHelper$1;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;-><init>(Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/store/StoreStatusMonitor;I)V

    return-void
.end method


# virtual methods
.method protected onFilteredSuggestions([Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;)V
    .locals 1
    .param p1, "suggestions"    # [Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper$1;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # invokes: Lcom/google/android/videos/ui/WatchNowHelper;->allowCursorChanges()Z
    invoke-static {v0}, Lcom/google/android/videos/ui/WatchNowHelper;->access$000(Lcom/google/android/videos/ui/WatchNowHelper;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper$1;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # setter for: Lcom/google/android/videos/ui/WatchNowHelper;->pendingVideoSuggestions:[Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;
    invoke-static {v0, p1}, Lcom/google/android/videos/ui/WatchNowHelper;->access$102(Lcom/google/android/videos/ui/WatchNowHelper;[Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;)[Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;

    .line 183
    :goto_0
    return-void

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper$1;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->watchNowRecommendationDataSource:Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;
    invoke-static {v0}, Lcom/google/android/videos/ui/WatchNowHelper;->access$200(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;->updateVideos([Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;)V

    goto :goto_0
.end method
