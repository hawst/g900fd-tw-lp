.class interface abstract Lcom/google/android/videos/activity/MovieDetailsActivity$DownloadStatusQuery;
.super Ljava/lang/Object;
.source "MovieDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/activity/MovieDetailsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "DownloadStatusQuery"
.end annotation


# static fields
.field public static final COLUMNS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1055
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "(pinned IS NOT NULL AND pinned > 0)"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "(license_type IS NOT NULL AND license_type != 0)"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "pinning_status"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "pinning_status_reason"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "pinning_download_size"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "download_bytes_downloaded"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "pinning_drm_error_code"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/videos/activity/MovieDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    return-void
.end method
