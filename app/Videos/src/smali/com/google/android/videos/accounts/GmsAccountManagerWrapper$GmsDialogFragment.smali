.class public Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GmsDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "GmsAccountManagerWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GmsDialogFragment"
.end annotation


# instance fields
.field private dialog:Landroid/app/Dialog;

.field private gmsAccountManagerWrapper:Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 251
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static show(Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;Landroid/support/v4/app/FragmentActivity;I)V
    .locals 3
    .param p0, "gmsAccountManagerWrapper"    # Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;
    .param p1, "activity"    # Landroid/support/v4/app/FragmentActivity;
    .param p2, "errorCode"    # I

    .prologue
    .line 255
    new-instance v0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GmsDialogFragment;

    invoke-direct {v0}, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GmsDialogFragment;-><init>()V

    .line 256
    .local v0, "dialogFragment":Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GmsDialogFragment;
    iput-object p0, v0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GmsDialogFragment;->gmsAccountManagerWrapper:Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;

    .line 257
    const/16 v1, 0x388

    invoke-static {p2, p1, v1}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->getErrorDialog(ILandroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GmsDialogFragment;->dialog:Landroid/app/Dialog;

    .line 259
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-class v2, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GmsDialogFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GmsDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 260
    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GmsDialogFragment;->gmsAccountManagerWrapper:Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;

    const/16 v1, 0x388

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->onActivityResult(IILandroid/content/Intent;)Z

    .line 273
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 267
    iget-object v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GmsDialogFragment;->dialog:Landroid/app/Dialog;

    return-object v0
.end method
