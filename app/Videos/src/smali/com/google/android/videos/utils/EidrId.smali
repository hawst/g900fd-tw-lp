.class public final Lcom/google/android/videos/utils/EidrId;
.super Ljava/lang/Object;
.source "EidrId.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/utils/EidrId$ConvertEidrQuery;
    }
.end annotation


# direct methods
.method public static convertEidrIdToVideoId(Lcom/google/android/videos/store/Database;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p0, "database"    # Lcom/google/android/videos/store/Database;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "eidrId"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 65
    invoke-virtual {p0}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 67
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "purchased_assets, assets ON asset_type = 6 AND assets_type = 6 AND assets_id = asset_id"

    sget-object v2, Lcom/google/android/videos/utils/EidrId$ConvertEidrQuery;->PROJECTION:[Ljava/lang/String;

    const-string v3, "account = ? AND title_eidr_id = ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    aput-object p2, v4, v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 71
    .local v9, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 72
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 75
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 77
    :goto_0
    return-object v5

    .line 75
    :cond_0
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public static convertEidrIdToVideoId(Lcom/google/android/videos/store/Database;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/ExecutorService;Lcom/google/android/videos/async/Callback;)V
    .locals 1
    .param p0, "database"    # Lcom/google/android/videos/store/Database;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "eidr"    # Ljava/lang/String;
    .param p3, "localExecutor"    # Ljava/util/concurrent/ExecutorService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/store/Database;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 90
    .local p4, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Lcom/google/android/videos/utils/EidrId$1;

    invoke-direct {v0, p0, p1, p2, p4}, Lcom/google/android/videos/utils/EidrId$1;-><init>(Lcom/google/android/videos/store/Database;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V

    invoke-interface {p3, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 102
    return-void
.end method

.method public static convertToCompactEidr(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "fullEidrId"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 28
    if-nez p0, :cond_0

    .line 32
    .end local p0    # "fullEidrId":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 31
    .restart local p0    # "fullEidrId":Ljava/lang/String;
    :cond_0
    const-string v1, "[\\.\\-/]"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 32
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    .end local p0    # "fullEidrId":Ljava/lang/String;
    :goto_1
    move-object v0, p0

    goto :goto_0

    .restart local p0    # "fullEidrId":Ljava/lang/String;
    :cond_1
    move-object p0, v0

    goto :goto_1
.end method

.method public static convertToFullEidr(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "compactEidrId"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x2

    .line 41
    invoke-static {p0}, Lcom/google/android/videos/utils/EidrId;->isValidCompactEidr(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 42
    const/4 v2, 0x0

    .line 54
    :goto_0
    return-object v2

    .line 44
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x22

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 45
    .local v1, "stringBuilder":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    .line 46
    const/16 v2, 0x2e

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 47
    const/4 v2, 0x6

    invoke-virtual {v1, p0, v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    .line 48
    const/16 v2, 0x2f

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 49
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    const/4 v2, 0x5

    if-ge v0, v2, :cond_1

    .line 50
    mul-int/lit8 v2, v0, 0x4

    add-int/lit8 v2, v2, 0x6

    mul-int/lit8 v3, v0, 0x4

    add-int/lit8 v3, v3, 0xa

    invoke-virtual {v1, p0, v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    .line 51
    const/16 v2, 0x2d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 49
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 53
    :cond_1
    const/16 v2, 0x1a

    const/16 v3, 0x1b

    invoke-virtual {v1, p0, v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    .line 54
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static isValidCompactEidr(Ljava/lang/String;)Z
    .locals 2
    .param p0, "compactEidrId"    # Ljava/lang/String;

    .prologue
    .line 36
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x1b

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
