.class public Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;
.super Landroid/support/v17/leanback/widget/DetailsOverviewRow;
.source "ImageLoadingDetailsOverviewRow.java"


# instance fields
.field private bitmap:Landroid/graphics/Bitmap;

.field private context:Landroid/content/Context;

.field private imageUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Ljava/lang/Object;

    .prologue
    .line 29
    invoke-direct {p0, p2}, Landroid/support/v17/leanback/widget/DetailsOverviewRow;-><init>(Ljava/lang/Object;)V

    .line 30
    iput-object p1, p0, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;->context:Landroid/content/Context;

    .line 31
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;->imageUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;->setBitmapInternal(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private declared-synchronized setBitmapInternal(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 60
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;->bitmap:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    monitor-exit p0

    return-void

    .line 60
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public setImageUri(Ljava/lang/String;)V
    .locals 4
    .param p1, "imageUriString"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 34
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 35
    iget-object v2, p0, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v1

    .line 36
    .local v1, "videosGlobals":Lcom/google/android/videos/VideosGlobals;
    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getBitmapRequesters()Lcom/google/android/videos/bitmap/BitmapRequesters;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/videos/bitmap/BitmapRequesters;->getBitmapRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v0

    .line 38
    .local v0, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Landroid/graphics/Bitmap;>;"
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;->imageUri:Landroid/net/Uri;

    .line 39
    iget-object v2, p0, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;->imageUri:Landroid/net/Uri;

    new-instance v3, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow$1;

    invoke-direct {v3, p0}, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow$1;-><init>(Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;)V

    invoke-interface {v0, v2, v3}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 57
    .end local v0    # "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Landroid/graphics/Bitmap;>;"
    .end local v1    # "videosGlobals":Lcom/google/android/videos/VideosGlobals;
    :goto_0
    return-void

    .line 54
    :cond_0
    iput-object v3, p0, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;->imageUri:Landroid/net/Uri;

    .line 55
    iget-object v2, p0, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;->context:Landroid/content/Context;

    invoke-virtual {p0, v2, v3}, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;->setImageBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public declared-synchronized updateBitmapIfLoaded()V
    .locals 2

    .prologue
    .line 64
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;->setImageBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    :cond_0
    monitor-exit p0

    return-void

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
