.class public Lcom/google/android/videos/async/CancelableAuthenticatee;
.super Ljava/lang/Object;
.source "CancelableAuthenticatee.java"

# interfaces
.implements Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;


# instance fields
.field private volatile canceled:Z

.field private final target:Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;)V
    .locals 0
    .param p1, "target"    # Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/google/android/videos/async/CancelableAuthenticatee;->target:Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    .line 20
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/async/CancelableAuthenticatee;->canceled:Z

    .line 24
    return-void
.end method

.method public isCanceled()Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/google/android/videos/async/CancelableAuthenticatee;->canceled:Z

    return v0
.end method

.method public onAuthenticated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "authToken"    # Ljava/lang/String;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/google/android/videos/async/CancelableAuthenticatee;->canceled:Z

    if-nez v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/google/android/videos/async/CancelableAuthenticatee;->target:Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    invoke-interface {v0, p1, p2}, Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;->onAuthenticated(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    :cond_0
    return-void
.end method

.method public onAuthenticationError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/google/android/videos/async/CancelableAuthenticatee;->canceled:Z

    if-nez v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/google/android/videos/async/CancelableAuthenticatee;->target:Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    invoke-interface {v0, p1, p2}, Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;->onAuthenticationError(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 49
    :cond_0
    return-void
.end method

.method public onNotAuthenticated(Ljava/lang/String;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/google/android/videos/async/CancelableAuthenticatee;->canceled:Z

    if-nez v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/google/android/videos/async/CancelableAuthenticatee;->target:Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    invoke-interface {v0, p1}, Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;->onNotAuthenticated(Ljava/lang/String;)V

    .line 42
    :cond_0
    return-void
.end method
