.class public Lcom/google/android/videos/accounts/AccountsChangedService;
.super Landroid/app/IntentService;
.source "AccountsChangedService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/accounts/AccountsChangedService$AccountsChangedReceiver;
    }
.end annotation


# instance fields
.field private videosGlobals:Lcom/google/android/videos/VideosGlobals;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/google/android/videos/accounts/AccountsChangedService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 28
    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 1

    .prologue
    .line 32
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 33
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/accounts/AccountsChangedService;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    .line 34
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 44
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v2

    .line 45
    .local v2, "purchaseStoreCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/Integer;Ljava/lang/Void;>;"
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v3

    .line 46
    .local v3, "wishlistStoreCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/Integer;Ljava/lang/Void;>;"
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v0

    .line 47
    .local v0, "configurationStoreCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/Integer;Ljava/lang/Void;>;"
    iget-object v4, p0, Lcom/google/android/videos/accounts/AccountsChangedService;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStoreSync()Lcom/google/android/videos/store/PurchaseStoreSync;

    move-result-object v4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v5, v7}, Lcom/google/android/videos/async/ControllableRequest;->create(Ljava/lang/Object;Lcom/google/android/videos/async/TaskStatus;)Lcom/google/android/videos/async/ControllableRequest;

    move-result-object v5

    invoke-virtual {v4, v5, v2}, Lcom/google/android/videos/store/PurchaseStoreSync;->cleanup(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V

    .line 49
    iget-object v4, p0, Lcom/google/android/videos/accounts/AccountsChangedService;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getWishlistStoreSync()Lcom/google/android/videos/store/WishlistStoreSync;

    move-result-object v4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v5, v7}, Lcom/google/android/videos/async/ControllableRequest;->create(Ljava/lang/Object;Lcom/google/android/videos/async/TaskStatus;)Lcom/google/android/videos/async/ControllableRequest;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Lcom/google/android/videos/store/WishlistStoreSync;->cleanup(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V

    .line 51
    iget-object v4, p0, Lcom/google/android/videos/accounts/AccountsChangedService;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v4

    invoke-virtual {v4, v6, v0}, Lcom/google/android/videos/store/ConfigurationStore;->cleanup(ILcom/google/android/videos/async/Callback;)V

    .line 53
    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;

    .line 54
    invoke-virtual {v3}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;

    .line 55
    invoke-virtual {v0}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    :goto_0
    return-void

    .line 56
    :catch_0
    move-exception v1

    .line 57
    .local v1, "e":Ljava/util/concurrent/ExecutionException;
    const-string v4, "Failed to purge stores"

    invoke-static {v4, v1}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/videos/accounts/AccountsChangedService;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/accounts/SignInManager;->clearSignedInAccountIfRemoved()V

    .line 39
    invoke-super {p0, p1, p2, p3}, Landroid/app/IntentService;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    return v0
.end method
