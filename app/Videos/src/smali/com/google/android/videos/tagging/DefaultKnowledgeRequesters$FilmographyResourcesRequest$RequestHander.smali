.class public final Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest$RequestHander;
.super Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$BytesRequestHandler;
.source "DefaultKnowledgeRequesters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RequestHander"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$BytesRequestHandler",
        "<",
        "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
        ">;",
        "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 771
    invoke-direct {p0}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$BytesRequestHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic createComponent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 771
    check-cast p1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/videos/utils/ByteArray;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest$RequestHander;->createComponent(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;Lcom/google/android/videos/utils/ByteArray;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public createComponent(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;Lcom/google/android/videos/utils/ByteArray;)Ljava/util/Map;
    .locals 7
    .param p1, "request"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;
    .param p2, "underlyingResponse"    # Lcom/google/android/videos/utils/ByteArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 777
    new-instance v3, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    invoke-direct {v3}, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;-><init>()V

    .line 778
    .local v3, "response":Lcom/google/wireless/android/video/magma/proto/AssetListResponse;
    iget-object v4, p2, Lcom/google/android/videos/utils/ByteArray;->data:[B

    const/4 v5, 0x0

    invoke-virtual {p2}, Lcom/google/android/videos/utils/ByteArray;->limit()I

    move-result v6

    invoke-static {v3, v4, v5, v6}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[BII)Lcom/google/protobuf/nano/MessageNano;

    .line 779
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 780
    .local v1, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/AssetResource;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v4, v3, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    array-length v4, v4

    if-ge v0, v4, :cond_2

    .line 781
    iget-object v4, v3, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    aget-object v2, v4, v0

    .line 782
    .local v2, "resource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    iget-object v4, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v4, :cond_0

    iget-object v4, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v4, v4, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->mid:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 783
    :cond_0
    new-instance v4, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;

    const-string v5, "Stored AssetResource has no MID"

    invoke-direct {v4, v5}, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 785
    :cond_1
    iget-object v4, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v4, v4, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->mid:Ljava/lang/String;

    invoke-interface {v1, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 780
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 787
    .end local v2    # "resource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :cond_2
    return-object v1
.end method

.method public createUnderlyingRequest(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;Lcom/google/android/videos/utils/ByteArray;)Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;
    .locals 4
    .param p1, "request"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;
    .param p2, "storedResponse"    # Lcom/google/android/videos/utils/ByteArray;

    .prologue
    .line 793
    new-instance v0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;

    iget-object v1, p1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;->knowledgeRequest:Lcom/google/android/videos/tagging/KnowledgeRequest;

    iget-object v1, v1, Lcom/google/android/videos/tagging/KnowledgeRequest;->account:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;->knowledgeRequest:Lcom/google/android/videos/tagging/KnowledgeRequest;

    iget-object v2, v2, Lcom/google/android/videos/tagging/KnowledgeRequest;->userCountry:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;->filmographyIds:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    return-object v0
.end method

.method public bridge synthetic createUnderlyingRequest(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 771
    check-cast p1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/videos/utils/ByteArray;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest$RequestHander;->createUnderlyingRequest(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;Lcom/google/android/videos/utils/ByteArray;)Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;

    move-result-object v0

    return-object v0
.end method
