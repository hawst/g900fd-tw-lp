.class Lcom/google/android/videos/pano/ui/RequestingImageView$1;
.super Ljava/lang/Object;
.source "RequestingImageView.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/pano/ui/RequestingImageView;->setImageUri(Landroid/net/Uri;Lcom/google/android/videos/async/Requester;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/async/ControllableRequest",
        "<",
        "Landroid/net/Uri;",
        ">;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pano/ui/RequestingImageView;


# direct methods
.method constructor <init>(Lcom/google/android/videos/pano/ui/RequestingImageView;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/videos/pano/ui/RequestingImageView$1;->this$0:Lcom/google/android/videos/pano/ui/RequestingImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/async/ControllableRequest;Ljava/lang/Exception;)V
    .locals 0
    .param p2, "exception"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    .prologue
    .line 57
    .local p1, "request":Lcom/google/android/videos/async/ControllableRequest;, "Lcom/google/android/videos/async/ControllableRequest<Landroid/net/Uri;>;"
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 49
    check-cast p1, Lcom/google/android/videos/async/ControllableRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pano/ui/RequestingImageView$1;->onError(Lcom/google/android/videos/async/ControllableRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/async/ControllableRequest;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p2, "response"    # Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ")V"
        }
    .end annotation

    .prologue
    .line 52
    .local p1, "request":Lcom/google/android/videos/async/ControllableRequest;, "Lcom/google/android/videos/async/ControllableRequest<Landroid/net/Uri;>;"
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/RequestingImageView$1;->this$0:Lcom/google/android/videos/pano/ui/RequestingImageView;

    invoke-virtual {v0, p2}, Lcom/google/android/videos/pano/ui/RequestingImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 53
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 49
    check-cast p1, Lcom/google/android/videos/async/ControllableRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Landroid/graphics/Bitmap;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pano/ui/RequestingImageView$1;->onResponse(Lcom/google/android/videos/async/ControllableRequest;Landroid/graphics/Bitmap;)V

    return-void
.end method
