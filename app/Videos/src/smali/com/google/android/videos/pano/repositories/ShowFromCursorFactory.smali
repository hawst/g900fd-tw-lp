.class public final Lcom/google/android/videos/pano/repositories/ShowFromCursorFactory;
.super Ljava/lang/Object;
.source "ShowFromCursorFactory.java"

# interfaces
.implements Lcom/google/android/repolib/common/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Factory",
        "<",
        "Lcom/google/android/videos/pano/model/ShowItem;",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/videos/pano/repositories/ShowFromCursorFactory;->context:Landroid/content/Context;

    .line 19
    return-void
.end method

.method public static showFromCursorFactory(Landroid/content/Context;)Lcom/google/android/repolib/common/Factory;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lcom/google/android/repolib/common/Factory",
            "<",
            "Lcom/google/android/videos/pano/model/ShowItem;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/videos/pano/repositories/ShowFromCursorFactory;

    invoke-direct {v0, p0}, Lcom/google/android/videos/pano/repositories/ShowFromCursorFactory;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public createFrom(Landroid/database/Cursor;)Lcom/google/android/videos/pano/model/ShowItem;
    .locals 17
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 28
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 29
    .local v4, "id":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/pano/repositories/ShowFromCursorFactory;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/videos/pano/ui/PanoHelper;->getDefaultPosterHeight(Landroid/content/Context;)I

    move-result v9

    .line 30
    .local v9, "posterDimensions":I
    new-instance v3, Lcom/google/android/videos/pano/model/ShowItem;

    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/pano/repositories/ShowFromCursorFactory;->context:Landroid/content/Context;

    invoke-static {v2, v4}, Lcom/google/android/videos/pano/activity/ShowDetailsActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v11

    const-wide v12, 0x7fffffffffffffffL

    const/4 v14, 0x0

    const/4 v15, -0x1

    const/16 v16, 0x0

    move v10, v9

    invoke-direct/range {v3 .. v16}, Lcom/google/android/videos/pano/model/ShowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/content/Intent;JIIF)V

    return-object v3
.end method

.method public bridge synthetic createFrom(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 14
    check-cast p1, Landroid/database/Cursor;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/pano/repositories/ShowFromCursorFactory;->createFrom(Landroid/database/Cursor;)Lcom/google/android/videos/pano/model/ShowItem;

    move-result-object v0

    return-object v0
.end method
