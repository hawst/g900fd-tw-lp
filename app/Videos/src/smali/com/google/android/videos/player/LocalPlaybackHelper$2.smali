.class Lcom/google/android/videos/player/LocalPlaybackHelper$2;
.super Ljava/lang/Object;
.source "LocalPlaybackHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/player/LocalPlaybackHelper;->onDownloadProgress(JJI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/player/LocalPlaybackHelper;

.field final synthetic val$percentDownloaded:I


# direct methods
.method constructor <init>(Lcom/google/android/videos/player/LocalPlaybackHelper;I)V
    .locals 0

    .prologue
    .line 690
    iput-object p1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$2;->this$0:Lcom/google/android/videos/player/LocalPlaybackHelper;

    iput p2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$2;->val$percentDownloaded:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 693
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$2;->this$0:Lcom/google/android/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/videos/player/LocalPlaybackHelper;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;
    invoke-static {v0}, Lcom/google/android/videos/player/LocalPlaybackHelper;->access$700(Lcom/google/android/videos/player/LocalPlaybackHelper;)Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    move-result-object v0

    iget v0, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->durationMillis:I

    if-lez v0, :cond_0

    .line 694
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$2;->this$0:Lcom/google/android/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;
    invoke-static {v0}, Lcom/google/android/videos/player/LocalPlaybackHelper;->access$900(Lcom/google/android/videos/player/LocalPlaybackHelper;)Lcom/google/android/videos/player/overlay/ControllerOverlay;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$2;->this$0:Lcom/google/android/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/videos/player/LocalPlaybackHelper;->localResumeTimeMillis:I
    invoke-static {v1}, Lcom/google/android/videos/player/LocalPlaybackHelper;->access$800(Lcom/google/android/videos/player/LocalPlaybackHelper;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$2;->this$0:Lcom/google/android/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/videos/player/LocalPlaybackHelper;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;
    invoke-static {v2}, Lcom/google/android/videos/player/LocalPlaybackHelper;->access$700(Lcom/google/android/videos/player/LocalPlaybackHelper;)Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    move-result-object v2

    iget v2, v2, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->durationMillis:I

    iget-object v3, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$2;->this$0:Lcom/google/android/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/videos/player/LocalPlaybackHelper;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;
    invoke-static {v3}, Lcom/google/android/videos/player/LocalPlaybackHelper;->access$700(Lcom/google/android/videos/player/LocalPlaybackHelper;)Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    move-result-object v3

    iget v3, v3, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->durationMillis:I

    iget v4, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$2;->val$percentDownloaded:I

    invoke-static {v3, v4}, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->getSeekToPercent(II)I

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setTimes(III)V

    .line 697
    :cond_0
    return-void
.end method
