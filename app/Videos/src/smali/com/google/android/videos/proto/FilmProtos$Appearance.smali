.class public final Lcom/google/android/videos/proto/FilmProtos$Appearance;
.super Lcom/google/protobuf/nano/MessageNano;
.source "FilmProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/proto/FilmProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Appearance"
.end annotation


# instance fields
.field public offset:[I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 721
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 722
    invoke-virtual {p0}, Lcom/google/android/videos/proto/FilmProtos$Appearance;->clear()Lcom/google/android/videos/proto/FilmProtos$Appearance;

    .line 723
    return-void
.end method

.method public static parseFrom([B)Lcom/google/android/videos/proto/FilmProtos$Appearance;
    .locals 1
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 853
    new-instance v0, Lcom/google/android/videos/proto/FilmProtos$Appearance;

    invoke-direct {v0}, Lcom/google/android/videos/proto/FilmProtos$Appearance;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/proto/FilmProtos$Appearance;

    return-object v0
.end method


# virtual methods
.method public clear()Lcom/google/android/videos/proto/FilmProtos$Appearance;
    .locals 1

    .prologue
    .line 726
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Appearance;->offset:[I

    .line 727
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/proto/FilmProtos$Appearance;->cachedSize:I

    .line 728
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 776
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v3

    .line 777
    .local v3, "size":I
    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Appearance;->offset:[I

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Appearance;->offset:[I

    array-length v4, v4

    if-lez v4, :cond_1

    .line 778
    const/4 v0, 0x0

    .line 779
    .local v0, "dataSize":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Appearance;->offset:[I

    array-length v4, v4

    if-ge v2, v4, :cond_0

    .line 780
    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Appearance;->offset:[I

    aget v1, v4, v2

    .line 781
    .local v1, "element":I
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v4

    add-int/2addr v0, v4

    .line 779
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 784
    .end local v1    # "element":I
    :cond_0
    add-int/2addr v3, v0

    .line 785
    add-int/lit8 v3, v3, 0x1

    .line 786
    invoke-static {v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeRawVarint32Size(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 789
    .end local v0    # "dataSize":I
    .end local v2    # "i":I
    :cond_1
    return v3
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 733
    if-ne p1, p0, :cond_1

    .line 744
    :cond_0
    :goto_0
    return v1

    .line 736
    :cond_1
    instance-of v3, p1, Lcom/google/android/videos/proto/FilmProtos$Appearance;

    if-nez v3, :cond_2

    move v1, v2

    .line 737
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 739
    check-cast v0, Lcom/google/android/videos/proto/FilmProtos$Appearance;

    .line 740
    .local v0, "other":Lcom/google/android/videos/proto/FilmProtos$Appearance;
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Appearance;->offset:[I

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Appearance;->offset:[I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([I[I)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 742
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 749
    const/16 v0, 0x11

    .line 750
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Appearance;->offset:[I

    invoke-static {v1}, Lcom/google/protobuf/nano/InternalNano;->hashCode([I)I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 752
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/FilmProtos$Appearance;
    .locals 9
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 797
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v6

    .line 798
    .local v6, "tag":I
    sparse-switch v6, :sswitch_data_0

    .line 802
    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v8

    if-nez v8, :cond_0

    .line 803
    :sswitch_0
    return-object p0

    .line 808
    :sswitch_1
    const/16 v8, 0x8

    invoke-static {p1, v8}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 810
    .local v0, "arrayLength":I
    iget-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Appearance;->offset:[I

    if-nez v8, :cond_2

    move v1, v7

    .line 811
    .local v1, "i":I
    :goto_1
    add-int v8, v1, v0

    new-array v4, v8, [I

    .line 812
    .local v4, "newArray":[I
    if-eqz v1, :cond_1

    .line 813
    iget-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Appearance;->offset:[I

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 815
    :cond_1
    :goto_2
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_3

    .line 816
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v8

    aput v8, v4, v1

    .line 817
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 815
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 810
    .end local v1    # "i":I
    .end local v4    # "newArray":[I
    :cond_2
    iget-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Appearance;->offset:[I

    array-length v1, v8

    goto :goto_1

    .line 820
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[I
    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v8

    aput v8, v4, v1

    .line 821
    iput-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Appearance;->offset:[I

    goto :goto_0

    .line 825
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v2

    .line 826
    .local v2, "length":I
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v3

    .line 828
    .local v3, "limit":I
    const/4 v0, 0x0

    .line 829
    .restart local v0    # "arrayLength":I
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getPosition()I

    move-result v5

    .line 830
    .local v5, "startPos":I
    :goto_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v8

    if-lez v8, :cond_4

    .line 831
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    .line 832
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 834
    :cond_4
    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->rewindToPosition(I)V

    .line 835
    iget-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Appearance;->offset:[I

    if-nez v8, :cond_6

    move v1, v7

    .line 836
    .restart local v1    # "i":I
    :goto_4
    add-int v8, v1, v0

    new-array v4, v8, [I

    .line 837
    .restart local v4    # "newArray":[I
    if-eqz v1, :cond_5

    .line 838
    iget-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Appearance;->offset:[I

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 840
    :cond_5
    :goto_5
    array-length v8, v4

    if-ge v1, v8, :cond_7

    .line 841
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v8

    aput v8, v4, v1

    .line 840
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 835
    .end local v1    # "i":I
    .end local v4    # "newArray":[I
    :cond_6
    iget-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Appearance;->offset:[I

    array-length v1, v8

    goto :goto_4

    .line 843
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[I
    :cond_7
    iput-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Appearance;->offset:[I

    .line 844
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 798
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 701
    invoke-virtual {p0, p1}, Lcom/google/android/videos/proto/FilmProtos$Appearance;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/FilmProtos$Appearance;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 758
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Appearance;->offset:[I

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Appearance;->offset:[I

    array-length v3, v3

    if-lez v3, :cond_1

    .line 759
    const/4 v0, 0x0

    .line 760
    .local v0, "dataSize":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Appearance;->offset:[I

    array-length v3, v3

    if-ge v2, v3, :cond_0

    .line 761
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Appearance;->offset:[I

    aget v1, v3, v2

    .line 762
    .local v1, "element":I
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v3

    add-int/2addr v0, v3

    .line 760
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 765
    .end local v1    # "element":I
    :cond_0
    const/16 v3, 0xa

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeRawVarint32(I)V

    .line 766
    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeRawVarint32(I)V

    .line 767
    const/4 v2, 0x0

    :goto_1
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Appearance;->offset:[I

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 768
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Appearance;->offset:[I

    aget v3, v3, v2

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32NoTag(I)V

    .line 767
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 771
    .end local v0    # "dataSize":I
    .end local v2    # "i":I
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 772
    return-void
.end method
