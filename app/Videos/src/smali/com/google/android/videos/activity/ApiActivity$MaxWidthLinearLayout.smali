.class public Lcom/google/android/videos/activity/ApiActivity$MaxWidthLinearLayout;
.super Landroid/widget/LinearLayout;
.source "ApiActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/activity/ApiActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MaxWidthLinearLayout"
.end annotation


# instance fields
.field private final maxWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 572
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/activity/ApiActivity$MaxWidthLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 573
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 576
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 577
    sget-object v1, Lcom/google/android/videos/R$styleable;->MaxWidthView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 578
    .local v0, "viewAttrs":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/videos/activity/ApiActivity$MaxWidthLinearLayout;->maxWidth:I

    .line 579
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 580
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 3
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 584
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 585
    .local v1, "specWidth":I
    iget v2, p0, Lcom/google/android/videos/activity/ApiActivity$MaxWidthLinearLayout;->maxWidth:I

    if-lez v2, :cond_0

    iget v2, p0, Lcom/google/android/videos/activity/ApiActivity$MaxWidthLinearLayout;->maxWidth:I

    if-ge v2, v1, :cond_0

    .line 586
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 587
    .local v0, "measureMode":I
    iget v2, p0, Lcom/google/android/videos/activity/ApiActivity$MaxWidthLinearLayout;->maxWidth:I

    invoke-static {v2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 589
    .end local v0    # "measureMode":I
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 590
    return-void
.end method
