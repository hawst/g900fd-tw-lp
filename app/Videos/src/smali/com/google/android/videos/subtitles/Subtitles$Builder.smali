.class public final Lcom/google/android/videos/subtitles/Subtitles$Builder;
.super Ljava/lang/Object;
.source "Subtitles.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/subtitles/Subtitles;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private final windowBuilders:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleWindow$Builder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/subtitles/Subtitles$Builder;->windowBuilders:Landroid/util/SparseArray;

    .line 81
    return-void
.end method

.method private getOrCreateWindowBuilder(I)Lcom/google/android/videos/subtitles/SubtitleWindow$Builder;
    .locals 2
    .param p1, "windowId"    # I

    .prologue
    .line 104
    iget-object v1, p0, Lcom/google/android/videos/subtitles/Subtitles$Builder;->windowBuilders:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/subtitles/SubtitleWindow$Builder;

    .line 105
    .local v0, "windowBuilder":Lcom/google/android/videos/subtitles/SubtitleWindow$Builder;
    if-nez v0, :cond_0

    .line 106
    new-instance v0, Lcom/google/android/videos/subtitles/SubtitleWindow$Builder;

    .end local v0    # "windowBuilder":Lcom/google/android/videos/subtitles/SubtitleWindow$Builder;
    invoke-direct {v0, p1}, Lcom/google/android/videos/subtitles/SubtitleWindow$Builder;-><init>(I)V

    .line 107
    .restart local v0    # "windowBuilder":Lcom/google/android/videos/subtitles/SubtitleWindow$Builder;
    iget-object v1, p0, Lcom/google/android/videos/subtitles/Subtitles$Builder;->windowBuilders:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 109
    :cond_0
    return-object v0
.end method


# virtual methods
.method public addLineToWindow(ILjava/lang/String;IIZ)Lcom/google/android/videos/subtitles/Subtitles$Builder;
    .locals 1
    .param p1, "windowId"    # I
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "startTimeMillis"    # I
    .param p4, "endTimeMillis"    # I
    .param p5, "append"    # Z

    .prologue
    .line 85
    invoke-direct {p0, p1}, Lcom/google/android/videos/subtitles/Subtitles$Builder;->getOrCreateWindowBuilder(I)Lcom/google/android/videos/subtitles/SubtitleWindow$Builder;

    move-result-object v0

    invoke-virtual {v0, p2, p3, p4, p5}, Lcom/google/android/videos/subtitles/SubtitleWindow$Builder;->addLine(Ljava/lang/String;IIZ)Lcom/google/android/videos/subtitles/SubtitleWindow$Builder;

    .line 86
    return-object p0
.end method

.method public addSettingsToWindow(IILcom/google/android/videos/subtitles/SubtitleWindowSettings;)Lcom/google/android/videos/subtitles/Subtitles$Builder;
    .locals 1
    .param p1, "windowId"    # I
    .param p2, "startTimeMillis"    # I
    .param p3, "settings"    # Lcom/google/android/videos/subtitles/SubtitleWindowSettings;

    .prologue
    .line 91
    invoke-direct {p0, p1}, Lcom/google/android/videos/subtitles/Subtitles$Builder;->getOrCreateWindowBuilder(I)Lcom/google/android/videos/subtitles/SubtitleWindow$Builder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/videos/subtitles/SubtitleWindow$Builder;->addSettings(ILcom/google/android/videos/subtitles/SubtitleWindowSettings;)Lcom/google/android/videos/subtitles/SubtitleWindow$Builder;

    .line 92
    return-object p0
.end method

.method public build()Lcom/google/android/videos/subtitles/Subtitles;
    .locals 4

    .prologue
    .line 96
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 97
    .local v1, "windows":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/subtitles/SubtitleWindow;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/videos/subtitles/Subtitles$Builder;->windowBuilders:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 98
    iget-object v2, p0, Lcom/google/android/videos/subtitles/Subtitles$Builder;->windowBuilders:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/subtitles/SubtitleWindow$Builder;

    invoke-virtual {v2}, Lcom/google/android/videos/subtitles/SubtitleWindow$Builder;->build()Lcom/google/android/videos/subtitles/SubtitleWindow;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 100
    :cond_0
    new-instance v2, Lcom/google/android/videos/subtitles/Subtitles;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3}, Lcom/google/android/videos/subtitles/Subtitles;-><init>(Ljava/util/List;Lcom/google/android/videos/subtitles/Subtitles$1;)V

    return-object v2
.end method
