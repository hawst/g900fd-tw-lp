.class public Lcom/google/android/videos/player/PlaybackStatusNotifier;
.super Ljava/lang/Object;
.source "PlaybackStatusNotifier.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/PlaybackStatusNotifier$Listener;
    }
.end annotation


# instance fields
.field private final listeners:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet",
            "<",
            "Lcom/google/android/videos/player/PlaybackStatusNotifier$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private sessionInProgress:Z

.field private streamingInProgress:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/player/PlaybackStatusNotifier;->listeners:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 40
    return-void
.end method


# virtual methods
.method public declared-synchronized addListener(Lcom/google/android/videos/player/PlaybackStatusNotifier$Listener;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/videos/player/PlaybackStatusNotifier$Listener;

    .prologue
    .line 51
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/player/PlaybackStatusNotifier;->listeners:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52
    monitor-exit p0

    return-void

    .line 51
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isSessionInProgress()Z
    .locals 1

    .prologue
    .line 47
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/videos/player/PlaybackStatusNotifier;->sessionInProgress:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isStreamingInProgress()Z
    .locals 1

    .prologue
    .line 43
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/videos/player/PlaybackStatusNotifier;->streamingInProgress:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized removeListener(Lcom/google/android/videos/player/PlaybackStatusNotifier$Listener;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/videos/player/PlaybackStatusNotifier$Listener;

    .prologue
    .line 55
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/player/PlaybackStatusNotifier;->listeners:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    monitor-exit p0

    return-void

    .line 55
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized setSessionInProgress(Z)V
    .locals 3
    .param p1, "sessionInProgress"    # Z

    .prologue
    .line 59
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/videos/player/PlaybackStatusNotifier;->sessionInProgress:Z

    if-eq v2, p1, :cond_0

    .line 60
    iput-boolean p1, p0, Lcom/google/android/videos/player/PlaybackStatusNotifier;->sessionInProgress:Z

    .line 61
    iget-object v2, p0, Lcom/google/android/videos/player/PlaybackStatusNotifier;->listeners:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/player/PlaybackStatusNotifier$Listener;

    .line 62
    .local v1, "listener":Lcom/google/android/videos/player/PlaybackStatusNotifier$Listener;
    invoke-interface {v1, p1}, Lcom/google/android/videos/player/PlaybackStatusNotifier$Listener;->onPlaybackSessionStatusChanged(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 59
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/google/android/videos/player/PlaybackStatusNotifier$Listener;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 65
    :cond_0
    monitor-exit p0

    return-void
.end method

.method declared-synchronized setStreamingInProgress(Z)V
    .locals 3
    .param p1, "streamingInProgress"    # Z

    .prologue
    .line 68
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/videos/player/PlaybackStatusNotifier;->streamingInProgress:Z

    if-eq v2, p1, :cond_0

    .line 69
    iput-boolean p1, p0, Lcom/google/android/videos/player/PlaybackStatusNotifier;->streamingInProgress:Z

    .line 70
    iget-object v2, p0, Lcom/google/android/videos/player/PlaybackStatusNotifier;->listeners:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/player/PlaybackStatusNotifier$Listener;

    .line 71
    .local v1, "listener":Lcom/google/android/videos/player/PlaybackStatusNotifier$Listener;
    invoke-interface {v1, p1}, Lcom/google/android/videos/player/PlaybackStatusNotifier$Listener;->onStreamingStatusChanged(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 68
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/google/android/videos/player/PlaybackStatusNotifier$Listener;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 74
    :cond_0
    monitor-exit p0

    return-void
.end method
