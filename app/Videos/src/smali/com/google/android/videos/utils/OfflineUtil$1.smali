.class final Lcom/google/android/videos/utils/OfflineUtil$1;
.super Ljava/lang/Object;
.source "OfflineUtil.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/utils/OfflineUtil;->getRecursiveFileSizeAsync(Ljava/util/concurrent/Executor;Ljava/io/File;Lcom/google/android/videos/async/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$callback:Lcom/google/android/videos/async/Callback;

.field final synthetic val$file:Ljava/io/File;


# direct methods
.method constructor <init>(Ljava/io/File;Lcom/google/android/videos/async/Callback;)V
    .locals 0

    .prologue
    .line 279
    iput-object p1, p0, Lcom/google/android/videos/utils/OfflineUtil$1;->val$file:Ljava/io/File;

    iput-object p2, p0, Lcom/google/android/videos/utils/OfflineUtil$1;->val$callback:Lcom/google/android/videos/async/Callback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 283
    :try_start_0
    iget-object v1, p0, Lcom/google/android/videos/utils/OfflineUtil$1;->val$file:Ljava/io/File;

    invoke-static {v1}, Lcom/google/android/videos/utils/OfflineUtil;->getRecursiveFileSize(Ljava/io/File;)J

    move-result-wide v2

    .line 284
    .local v2, "size":J
    iget-object v1, p0, Lcom/google/android/videos/utils/OfflineUtil$1;->val$callback:Lcom/google/android/videos/async/Callback;

    iget-object v4, p0, Lcom/google/android/videos/utils/OfflineUtil$1;->val$file:Ljava/io/File;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v1, v4, v5}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 291
    .end local v2    # "size":J
    :goto_0
    return-void

    .line 285
    :catch_0
    move-exception v0

    .line 286
    .local v0, "exception":Ljava/io/IOException;
    iget-object v1, p0, Lcom/google/android/videos/utils/OfflineUtil$1;->val$callback:Lcom/google/android/videos/async/Callback;

    iget-object v4, p0, Lcom/google/android/videos/utils/OfflineUtil$1;->val$file:Ljava/io/File;

    invoke-interface {v1, v4, v0}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0

    .line 287
    .end local v0    # "exception":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 289
    .local v0, "exception":Ljava/lang/RuntimeException;
    iget-object v1, p0, Lcom/google/android/videos/utils/OfflineUtil$1;->val$callback:Lcom/google/android/videos/async/Callback;

    iget-object v4, p0, Lcom/google/android/videos/utils/OfflineUtil$1;->val$file:Ljava/io/File;

    invoke-interface {v1, v4, v0}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method
