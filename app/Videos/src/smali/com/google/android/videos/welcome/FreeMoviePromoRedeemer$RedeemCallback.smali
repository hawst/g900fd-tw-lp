.class final Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$RedeemCallback;
.super Ljava/lang/Object;
.source "FreeMoviePromoRedeemer.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "RedeemCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/api/RedeemPromotionRequest;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$RedeemCallback;->this$0:Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;
    .param p2, "x1"    # Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$1;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$RedeemCallback;-><init>(Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;)V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/api/RedeemPromotionRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/api/RedeemPromotionRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$RedeemCallback;->this$0:Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;

    # getter for: Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;->listener:Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$Listener;
    invoke-static {v0}, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;->access$500(Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;)Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$Listener;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$Listener;->onPromoRedeemError(Ljava/lang/Exception;)V

    .line 68
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 53
    check-cast p1, Lcom/google/android/videos/api/RedeemPromotionRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$RedeemCallback;->onError(Lcom/google/android/videos/api/RedeemPromotionRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/api/RedeemPromotionRequest;Ljava/lang/Void;)V
    .locals 8
    .param p1, "request"    # Lcom/google/android/videos/api/RedeemPromotionRequest;
    .param p2, "response"    # Ljava/lang/Void;

    .prologue
    const/4 v5, 0x0

    .line 57
    iget-object v0, p1, Lcom/google/android/videos/api/RedeemPromotionRequest;->assetResourceId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/videos/api/AssetResourceUtil;->toYtMovieId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 58
    .local v4, "videoId":Ljava/lang/String;
    iget-object v0, p1, Lcom/google/android/videos/api/RedeemPromotionRequest;->account:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->createForId(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    move-result-object v7

    .line 60
    .local v7, "purchaseSyncRequest":Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;
    iget-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$RedeemCallback;->this$0:Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;

    # getter for: Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;->purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;
    invoke-static {v0}, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;->access$300(Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;)Lcom/google/android/videos/store/PurchaseStoreSync;

    move-result-object v0

    invoke-static {v7, v5}, Lcom/google/android/videos/async/ControllableRequest;->create(Ljava/lang/Object;Lcom/google/android/videos/async/TaskStatus;)Lcom/google/android/videos/async/ControllableRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$RedeemCallback;->this$0:Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;

    # getter for: Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;->purchaseStoreSyncCallback:Lcom/google/android/videos/async/NewCallback;
    invoke-static {v2}, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;->access$200(Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;)Lcom/google/android/videos/async/NewCallback;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/store/PurchaseStoreSync;->syncPurchasesForVideo(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V

    .line 62
    iget-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$RedeemCallback;->this$0:Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;

    # getter for: Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;->eventLogger:Lcom/google/android/videos/logging/EventLogger;
    invoke-static {v0}, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;->access$400(Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;)Lcom/google/android/videos/logging/EventLogger;

    move-result-object v0

    const/16 v1, 0xa

    const/4 v2, -0x1

    const/4 v3, 0x1

    move-object v6, v5

    invoke-interface/range {v0 .. v6}, Lcom/google/android/videos/logging/EventLogger;->onDirectPurchase(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 53
    check-cast p1, Lcom/google/android/videos/api/RedeemPromotionRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/lang/Void;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$RedeemCallback;->onResponse(Lcom/google/android/videos/api/RedeemPromotionRequest;Ljava/lang/Void;)V

    return-void
.end method
