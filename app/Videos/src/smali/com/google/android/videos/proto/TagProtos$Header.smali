.class public final Lcom/google/android/videos/proto/TagProtos$Header;
.super Lcom/google/protobuf/nano/MessageNano;
.source "TagProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/proto/TagProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Header"
.end annotation


# instance fields
.field public chunk:[Lcom/google/android/videos/proto/TagProtos$Chunk;

.field public encrypted:Z

.field public film:Lcom/google/android/videos/proto/FilmProtos$Film;

.field public fps:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 38
    invoke-virtual {p0}, Lcom/google/android/videos/proto/TagProtos$Header;->clear()Lcom/google/android/videos/proto/TagProtos$Header;

    .line 39
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/videos/proto/TagProtos$Header;
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/proto/TagProtos$Header;->fps:F

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/proto/TagProtos$Header;->film:Lcom/google/android/videos/proto/FilmProtos$Film;

    .line 44
    invoke-static {}, Lcom/google/android/videos/proto/TagProtos$Chunk;->emptyArray()[Lcom/google/android/videos/proto/TagProtos$Chunk;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/proto/TagProtos$Header;->chunk:[Lcom/google/android/videos/proto/TagProtos$Chunk;

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/proto/TagProtos$Header;->encrypted:Z

    .line 46
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/proto/TagProtos$Header;->cachedSize:I

    .line 47
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 123
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 124
    .local v2, "size":I
    iget v3, p0, Lcom/google/android/videos/proto/TagProtos$Header;->fps:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    if-eq v3, v4, :cond_0

    .line 126
    const/4 v3, 0x1

    iget v4, p0, Lcom/google/android/videos/proto/TagProtos$Header;->fps:F

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeFloatSize(IF)I

    move-result v3

    add-int/2addr v2, v3

    .line 129
    :cond_0
    iget-object v3, p0, Lcom/google/android/videos/proto/TagProtos$Header;->film:Lcom/google/android/videos/proto/FilmProtos$Film;

    if-eqz v3, :cond_1

    .line 130
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/videos/proto/TagProtos$Header;->film:Lcom/google/android/videos/proto/FilmProtos$Film;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 133
    :cond_1
    iget-object v3, p0, Lcom/google/android/videos/proto/TagProtos$Header;->chunk:[Lcom/google/android/videos/proto/TagProtos$Chunk;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/videos/proto/TagProtos$Header;->chunk:[Lcom/google/android/videos/proto/TagProtos$Chunk;

    array-length v3, v3

    if-lez v3, :cond_3

    .line 134
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/videos/proto/TagProtos$Header;->chunk:[Lcom/google/android/videos/proto/TagProtos$Chunk;

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 135
    iget-object v3, p0, Lcom/google/android/videos/proto/TagProtos$Header;->chunk:[Lcom/google/android/videos/proto/TagProtos$Chunk;

    aget-object v0, v3, v1

    .line 136
    .local v0, "element":Lcom/google/android/videos/proto/TagProtos$Chunk;
    if-eqz v0, :cond_2

    .line 137
    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 134
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 142
    .end local v0    # "element":Lcom/google/android/videos/proto/TagProtos$Chunk;
    .end local v1    # "i":I
    :cond_3
    iget-boolean v3, p0, Lcom/google/android/videos/proto/TagProtos$Header;->encrypted:Z

    if-eqz v3, :cond_4

    .line 143
    const/4 v3, 0x4

    iget-boolean v4, p0, Lcom/google/android/videos/proto/TagProtos$Header;->encrypted:Z

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 146
    :cond_4
    return v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 52
    if-ne p1, p0, :cond_1

    .line 81
    :cond_0
    :goto_0
    return v2

    .line 55
    :cond_1
    instance-of v4, p1, Lcom/google/android/videos/proto/TagProtos$Header;

    if-nez v4, :cond_2

    move v2, v3

    .line 56
    goto :goto_0

    :cond_2
    move-object v1, p1

    .line 58
    check-cast v1, Lcom/google/android/videos/proto/TagProtos$Header;

    .line 60
    .local v1, "other":Lcom/google/android/videos/proto/TagProtos$Header;
    iget v4, p0, Lcom/google/android/videos/proto/TagProtos$Header;->fps:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    .line 61
    .local v0, "bits":I
    iget v4, v1, Lcom/google/android/videos/proto/TagProtos$Header;->fps:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    if-eq v0, v4, :cond_3

    move v2, v3

    .line 62
    goto :goto_0

    .line 65
    :cond_3
    iget-object v4, p0, Lcom/google/android/videos/proto/TagProtos$Header;->film:Lcom/google/android/videos/proto/FilmProtos$Film;

    if-nez v4, :cond_4

    .line 66
    iget-object v4, v1, Lcom/google/android/videos/proto/TagProtos$Header;->film:Lcom/google/android/videos/proto/FilmProtos$Film;

    if-eqz v4, :cond_5

    move v2, v3

    .line 67
    goto :goto_0

    .line 70
    :cond_4
    iget-object v4, p0, Lcom/google/android/videos/proto/TagProtos$Header;->film:Lcom/google/android/videos/proto/FilmProtos$Film;

    iget-object v5, v1, Lcom/google/android/videos/proto/TagProtos$Header;->film:Lcom/google/android/videos/proto/FilmProtos$Film;

    invoke-virtual {v4, v5}, Lcom/google/android/videos/proto/FilmProtos$Film;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    move v2, v3

    .line 71
    goto :goto_0

    .line 74
    :cond_5
    iget-object v4, p0, Lcom/google/android/videos/proto/TagProtos$Header;->chunk:[Lcom/google/android/videos/proto/TagProtos$Chunk;

    iget-object v5, v1, Lcom/google/android/videos/proto/TagProtos$Header;->chunk:[Lcom/google/android/videos/proto/TagProtos$Chunk;

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    move v2, v3

    .line 76
    goto :goto_0

    .line 78
    :cond_6
    iget-boolean v4, p0, Lcom/google/android/videos/proto/TagProtos$Header;->encrypted:Z

    iget-boolean v5, v1, Lcom/google/android/videos/proto/TagProtos$Header;->encrypted:Z

    if-eq v4, v5, :cond_0

    move v2, v3

    .line 79
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 86
    const/16 v0, 0x11

    .line 87
    .local v0, "result":I
    iget v1, p0, Lcom/google/android/videos/proto/TagProtos$Header;->fps:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 89
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/videos/proto/TagProtos$Header;->film:Lcom/google/android/videos/proto/FilmProtos$Film;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int v0, v2, v1

    .line 91
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/proto/TagProtos$Header;->chunk:[Lcom/google/android/videos/proto/TagProtos$Chunk;

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 93
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/android/videos/proto/TagProtos$Header;->encrypted:Z

    if-eqz v1, :cond_1

    const/16 v1, 0x4cf

    :goto_1
    add-int v0, v2, v1

    .line 94
    return v0

    .line 89
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/proto/TagProtos$Header;->film:Lcom/google/android/videos/proto/FilmProtos$Film;

    invoke-virtual {v1}, Lcom/google/android/videos/proto/FilmProtos$Film;->hashCode()I

    move-result v1

    goto :goto_0

    .line 93
    :cond_1
    const/16 v1, 0x4d5

    goto :goto_1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/TagProtos$Header;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 154
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 155
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 159
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 160
    :sswitch_0
    return-object p0

    .line 165
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFloat()F

    move-result v5

    iput v5, p0, Lcom/google/android/videos/proto/TagProtos$Header;->fps:F

    goto :goto_0

    .line 169
    :sswitch_2
    iget-object v5, p0, Lcom/google/android/videos/proto/TagProtos$Header;->film:Lcom/google/android/videos/proto/FilmProtos$Film;

    if-nez v5, :cond_1

    .line 170
    new-instance v5, Lcom/google/android/videos/proto/FilmProtos$Film;

    invoke-direct {v5}, Lcom/google/android/videos/proto/FilmProtos$Film;-><init>()V

    iput-object v5, p0, Lcom/google/android/videos/proto/TagProtos$Header;->film:Lcom/google/android/videos/proto/FilmProtos$Film;

    .line 172
    :cond_1
    iget-object v5, p0, Lcom/google/android/videos/proto/TagProtos$Header;->film:Lcom/google/android/videos/proto/FilmProtos$Film;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 176
    :sswitch_3
    const/16 v5, 0x1a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 178
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/videos/proto/TagProtos$Header;->chunk:[Lcom/google/android/videos/proto/TagProtos$Chunk;

    if-nez v5, :cond_3

    move v1, v4

    .line 179
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/videos/proto/TagProtos$Chunk;

    .line 181
    .local v2, "newArray":[Lcom/google/android/videos/proto/TagProtos$Chunk;
    if-eqz v1, :cond_2

    .line 182
    iget-object v5, p0, Lcom/google/android/videos/proto/TagProtos$Header;->chunk:[Lcom/google/android/videos/proto/TagProtos$Chunk;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 184
    :cond_2
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_4

    .line 185
    new-instance v5, Lcom/google/android/videos/proto/TagProtos$Chunk;

    invoke-direct {v5}, Lcom/google/android/videos/proto/TagProtos$Chunk;-><init>()V

    aput-object v5, v2, v1

    .line 186
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 187
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 184
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 178
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/videos/proto/TagProtos$Chunk;
    :cond_3
    iget-object v5, p0, Lcom/google/android/videos/proto/TagProtos$Header;->chunk:[Lcom/google/android/videos/proto/TagProtos$Chunk;

    array-length v1, v5

    goto :goto_1

    .line 190
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/videos/proto/TagProtos$Chunk;
    :cond_4
    new-instance v5, Lcom/google/android/videos/proto/TagProtos$Chunk;

    invoke-direct {v5}, Lcom/google/android/videos/proto/TagProtos$Chunk;-><init>()V

    aput-object v5, v2, v1

    .line 191
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 192
    iput-object v2, p0, Lcom/google/android/videos/proto/TagProtos$Header;->chunk:[Lcom/google/android/videos/proto/TagProtos$Chunk;

    goto :goto_0

    .line 196
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/videos/proto/TagProtos$Chunk;
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/videos/proto/TagProtos$Header;->encrypted:Z

    goto :goto_0

    .line 155
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/videos/proto/TagProtos$Header;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/TagProtos$Header;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 100
    iget v2, p0, Lcom/google/android/videos/proto/TagProtos$Header;->fps:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-eq v2, v3, :cond_0

    .line 102
    const/4 v2, 0x1

    iget v3, p0, Lcom/google/android/videos/proto/TagProtos$Header;->fps:F

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeFloat(IF)V

    .line 104
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/proto/TagProtos$Header;->film:Lcom/google/android/videos/proto/FilmProtos$Film;

    if-eqz v2, :cond_1

    .line 105
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/videos/proto/TagProtos$Header;->film:Lcom/google/android/videos/proto/FilmProtos$Film;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 107
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/proto/TagProtos$Header;->chunk:[Lcom/google/android/videos/proto/TagProtos$Chunk;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/videos/proto/TagProtos$Header;->chunk:[Lcom/google/android/videos/proto/TagProtos$Chunk;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 108
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/videos/proto/TagProtos$Header;->chunk:[Lcom/google/android/videos/proto/TagProtos$Chunk;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 109
    iget-object v2, p0, Lcom/google/android/videos/proto/TagProtos$Header;->chunk:[Lcom/google/android/videos/proto/TagProtos$Chunk;

    aget-object v0, v2, v1

    .line 110
    .local v0, "element":Lcom/google/android/videos/proto/TagProtos$Chunk;
    if-eqz v0, :cond_2

    .line 111
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 108
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 115
    .end local v0    # "element":Lcom/google/android/videos/proto/TagProtos$Chunk;
    .end local v1    # "i":I
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/videos/proto/TagProtos$Header;->encrypted:Z

    if-eqz v2, :cond_4

    .line 116
    const/4 v2, 0x4

    iget-boolean v3, p0, Lcom/google/android/videos/proto/TagProtos$Header;->encrypted:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 118
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 119
    return-void
.end method
