.class public abstract Lcom/google/android/videos/adapter/EpisodesDataSource;
.super Lcom/google/android/videos/adapter/CursorDataSource;
.source "EpisodesDataSource.java"

# interfaces
.implements Lcom/google/android/videos/adapter/VideosDataSource;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/adapter/EpisodesDataSource$1;,
        Lcom/google/android/videos/adapter/EpisodesDataSource$SubQuery;,
        Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery;,
        Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;,
        Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/videos/adapter/CursorDataSource;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/adapter/EpisodesDataSource$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/adapter/EpisodesDataSource$1;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/videos/adapter/EpisodesDataSource;-><init>()V

    return-void
.end method

.method protected static getMasterSubCursor(Landroid/database/Cursor;)Lcom/google/android/videos/store/MasterSubCursor;
    .locals 1
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 232
    instance-of v0, p0, Lcom/google/android/videos/store/MasterSubCursor;

    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    .line 233
    check-cast p0, Lcom/google/android/videos/store/MasterSubCursor;

    .end local p0    # "cursor":Landroid/database/Cursor;
    return-object p0
.end method


# virtual methods
.method public final getAccount(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 36
    const/16 v0, 0x15

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getBytesDownloaded(Landroid/database/Cursor;)Ljava/lang/Long;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 84
    const/16 v0, 0x9

    invoke-static {p1, v0}, Lcom/google/android/videos/utils/DbUtils;->getLongOrNull(Landroid/database/Cursor;I)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public final getDescription(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 45
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getDownloadSize(Landroid/database/Cursor;)Ljava/lang/Long;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 79
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/android/videos/utils/DbUtils;->getLongOrNull(Landroid/database/Cursor;I)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public final getDurationSeconds(Landroid/database/Cursor;)I
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 99
    const/16 v0, 0x14

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final getEpisodeNumber(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 108
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getExpirationTimestamp(Landroid/database/Cursor;)J
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lcom/google/android/videos/adapter/EpisodesDataSource;->isPurchased(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x12

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final getHaveLicense(Landroid/database/Cursor;)Z
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 64
    const/4 v0, 0x5

    invoke-static {p1, v0}, Lcom/google/android/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v0

    return v0
.end method

.method public getItemIdentifier(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 238
    invoke-virtual {p0, p1}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getItem(I)Landroid/database/Cursor;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromEpisodeId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getPinningDrmErrorCode(Landroid/database/Cursor;)Ljava/lang/Integer;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 89
    const/16 v0, 0x8

    invoke-static {p1, v0}, Lcom/google/android/videos/utils/DbUtils;->getIntOrNull(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final getPinningStatus(Landroid/database/Cursor;)Ljava/lang/Integer;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 69
    const/4 v0, 0x6

    invoke-static {p1, v0}, Lcom/google/android/videos/utils/DbUtils;->getIntOrNull(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final getPinningStatusReason(Landroid/database/Cursor;)Ljava/lang/Integer;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 74
    const/4 v0, 0x7

    invoke-static {p1, v0}, Lcom/google/android/videos/utils/DbUtils;->getIntOrNull(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final getPublishTimestampSeconds(Landroid/database/Cursor;)J
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 242
    const/16 v0, 0x16

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getResumeTimestamp(Landroid/database/Cursor;)I
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 94
    const/16 v0, 0x13

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/google/android/videos/utils/DbUtils;->getIntOrDefault(Landroid/database/Cursor;II)I

    move-result v0

    return v0
.end method

.method public final getScreenshotUri(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 130
    const/16 v0, 0x10

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getSeasonId(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 113
    const/16 v0, 0xc

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getSeasonNumber(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 121
    const/16 v0, 0xd

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getSeasonTitle(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 117
    const/16 v0, 0xe

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getShowBannerUri(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 168
    invoke-static {p1}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getMasterSubCursor(Landroid/database/Cursor;)Lcom/google/android/videos/store/MasterSubCursor;

    move-result-object v0

    const/4 v1, 0x7

    invoke-interface {v0, v1}, Lcom/google/android/videos/store/MasterSubCursor;->getMasterString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getShowId(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 104
    invoke-static {p1}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getMasterSubCursor(Landroid/database/Cursor;)Lcom/google/android/videos/store/MasterSubCursor;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/videos/store/MasterSubCursor;->getMasterString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getShowLastActivityTimestamp(Landroid/database/Cursor;)J
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 176
    invoke-static {p1}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getMasterSubCursor(Landroid/database/Cursor;)Lcom/google/android/videos/store/MasterSubCursor;

    move-result-object v0

    const/16 v1, 0x9

    invoke-interface {v0, v1}, Lcom/google/android/videos/store/MasterSubCursor;->getMasterLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getShowPosterUri(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 172
    invoke-static {p1}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getMasterSubCursor(Landroid/database/Cursor;)Lcom/google/android/videos/store/MasterSubCursor;

    move-result-object v0

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Lcom/google/android/videos/store/MasterSubCursor;->getMasterString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getShowTitle(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 126
    invoke-static {p1}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getMasterSubCursor(Landroid/database/Cursor;)Lcom/google/android/videos/store/MasterSubCursor;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/google/android/videos/store/MasterSubCursor;->getMasterString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getTitle(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 41
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getUnpurchasedEpisodes(Ljava/util/List;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 217
    .local p1, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 218
    .local v3, "unpurchasedCount":I
    invoke-virtual {p0}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getCount()I

    move-result v1

    .line 219
    .local v1, "episodesCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 220
    invoke-virtual {p0, v2}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getItem(I)Landroid/database/Cursor;

    move-result-object v0

    .line 221
    .local v0, "cursor":Landroid/database/Cursor;
    invoke-virtual {p0, v0}, Lcom/google/android/videos/adapter/EpisodesDataSource;->isPurchased(Landroid/database/Cursor;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 222
    add-int/lit8 v3, v3, 0x1

    .line 223
    if-eqz p1, :cond_0

    .line 224
    invoke-virtual {p0, v0}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 219
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 228
    .end local v0    # "cursor":Landroid/database/Cursor;
    :cond_1
    return v3
.end method

.method public final getVideoId(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final isActive(Landroid/database/Cursor;)Z
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 142
    const/16 v0, 0x11

    invoke-static {p1, v0}, Lcom/google/android/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v0

    return v0
.end method

.method public final isLastWatched(Landroid/database/Cursor;)Z
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 152
    invoke-static {p1}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getMasterSubCursor(Landroid/database/Cursor;)Lcom/google/android/videos/store/MasterSubCursor;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lcom/google/android/videos/store/MasterSubCursor;->getMasterString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public final isNextEpisode(Landroid/database/Cursor;)Z
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 158
    invoke-static {p1}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getMasterSubCursor(Landroid/database/Cursor;)Lcom/google/android/videos/store/MasterSubCursor;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lcom/google/android/videos/store/MasterSubCursor;->getMasterString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public final isPinned(Landroid/database/Cursor;)Z
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 59
    const/4 v0, 0x4

    invoke-static {p1, v0}, Lcom/google/android/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v0

    return v0
.end method

.method public final isPurchased(Landroid/database/Cursor;)Z
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 49
    const/16 v0, 0xf

    invoke-static {p1, v0}, Lcom/google/android/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v0

    return v0
.end method

.method public final isRental(Landroid/database/Cursor;)Z
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v0, 0x1

    .line 147
    const/16 v1, 0x17

    const/4 v2, -0x1

    invoke-static {p1, v1, v2}, Lcom/google/android/videos/utils/DbUtils;->getIntOrDefault(Landroid/database/Cursor;II)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
