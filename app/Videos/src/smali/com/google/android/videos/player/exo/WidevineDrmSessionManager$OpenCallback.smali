.class Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$OpenCallback;
.super Ljava/lang/Object;
.source "WidevineDrmSessionManager.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OpenCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Ljava/lang/Object;",
        "[B>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;)V
    .locals 0

    .prologue
    .line 231
    iput-object p1, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$OpenCallback;->this$0:Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;
    .param p2, "x1"    # Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$1;

    .prologue
    .line 231
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$OpenCallback;-><init>(Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;)V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2
    .param p1, "request"    # Ljava/lang/Object;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$OpenCallback;->this$0:Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->onError(Ljava/lang/Exception;Z)V
    invoke-static {v0, p2, v1}, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->access$800(Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;Ljava/lang/Exception;Z)V

    .line 241
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 231
    check-cast p2, [B

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$OpenCallback;->onResponse(Ljava/lang/Object;[B)V

    return-void
.end method

.method public onResponse(Ljava/lang/Object;[B)V
    .locals 1
    .param p1, "request"    # Ljava/lang/Object;
    .param p2, "response"    # [B

    .prologue
    .line 235
    iget-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$OpenCallback;->this$0:Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;

    # invokes: Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->onOpened()V
    invoke-static {v0}, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->access$700(Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;)V

    .line 236
    return-void
.end method
