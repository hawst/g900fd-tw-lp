.class public Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;
.super Lcom/google/android/videos/pano/datasource/BaseDataSource;
.source "NewEpisodesDataSource.java"

# interfaces
.implements Lcom/google/android/videos/ContentNotificationManager$Processor;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/pano/datasource/BaseDataSource",
        "<",
        "Lcom/google/android/videos/pano/model/VideoItem;",
        ">;",
        "Lcom/google/android/videos/ContentNotificationManager$Processor;"
    }
.end annotation


# instance fields
.field private final cards:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/videos/pano/model/VideoItem;",
            ">;"
        }
    .end annotation
.end field

.field private final contentNotificationManager:Lcom/google/android/videos/ContentNotificationManager;

.field private final database:Lcom/google/android/videos/store/Database;

.field private final databaseListener:Lcom/google/android/videos/store/Database$Listener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/ContentNotificationManager;Lcom/google/android/videos/store/Database;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "signInManager"    # Lcom/google/android/videos/accounts/SignInManager;
    .param p3, "contentNotificationManager"    # Lcom/google/android/videos/ContentNotificationManager;
    .param p4, "database"    # Lcom/google/android/videos/store/Database;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/pano/datasource/BaseDataSource;-><init>(Landroid/content/Context;Lcom/google/android/videos/accounts/SignInManager;)V

    .line 29
    new-instance v0, Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource$1;

    invoke-direct {v0, p0}, Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource$1;-><init>(Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;)V

    iput-object v0, p0, Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;->databaseListener:Lcom/google/android/videos/store/Database$Listener;

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;->cards:Ljava/util/ArrayList;

    .line 53
    iput-object p3, p0, Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;->contentNotificationManager:Lcom/google/android/videos/ContentNotificationManager;

    .line 54
    iput-object p4, p0, Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;->database:Lcom/google/android/videos/store/Database;

    .line 55
    invoke-virtual {p3, p0}, Lcom/google/android/videos/ContentNotificationManager;->setNotificationProcessor(Lcom/google/android/videos/ContentNotificationManager$Processor;)V

    .line 56
    return-void
.end method


# virtual methods
.method public beginProcessing()V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;->cards:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 68
    return-void
.end method

.method public dismissContentNotification(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;
    .param p3, "episodeIds"    # [Ljava/lang/String;

    .prologue
    .line 108
    return-void
.end method

.method public dismissContentNotification(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoIds"    # [Ljava/lang/String;

    .prologue
    .line 103
    return-void
.end method

.method public finishProcessing()V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;->cards:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;->cards:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lcom/google/android/videos/pano/model/VideoItem;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;->updateArrayOnMainThread([Ljava/lang/Object;)V

    .line 98
    return-void
.end method

.method public firstUpdatableAdded()V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;->database:Lcom/google/android/videos/store/Database;

    iget-object v1, p0, Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;->databaseListener:Lcom/google/android/videos/store/Database$Listener;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/store/Database;->addListener(Lcom/google/android/videos/store/Database$Listener;)V

    .line 113
    return-void
.end method

.method public lastUpdatableRemoved()V
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;->database:Lcom/google/android/videos/store/Database;

    iget-object v1, p0, Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;->databaseListener:Lcom/google/android/videos/store/Database$Listener;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/store/Database;->removeListener(Lcom/google/android/videos/store/Database$Listener;)Z

    .line 118
    return-void
.end method

.method public processNewEpisodesForAccountAndShow(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 22
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;
    .param p3, "showTitle"    # Ljava/lang/String;
    .param p5, "firstSeasonId"    # Ljava/lang/String;
    .param p6, "firstEpisodeTitle"    # Ljava/lang/String;
    .param p7, "showPosterUri"    # Ljava/lang/String;
    .param p8, "showBannerUri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 78
    .local p4, "episodeIdList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v19

    .line 79
    .local v19, "size":I
    const/4 v3, 0x1

    move/from16 v0, v19

    if-ne v0, v3, :cond_0

    .line 80
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;->context:Landroid/content/Context;

    const v4, 0x7f0b01a9

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p3, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    .line 81
    .local v21, "title":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;->context:Landroid/content/Context;

    const v4, 0x7f0b01ab

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p6, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    .line 87
    .local v20, "subtitle":Ljava/lang/String;
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;->context:Landroid/content/Context;

    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p5

    invoke-static/range {v2 .. v7}, Lcom/google/android/videos/activity/LauncherActivity;->createWatchEpisodeDeepLinkingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v10

    .line 90
    .local v10, "intent":Landroid/content/Intent;
    new-instance v2, Lcom/google/android/videos/pano/model/VideoItem;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x0

    const-wide v12, 0x7fffffffffffffffL

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object/from16 v3, p2

    move-object/from16 v4, v21

    move-object/from16 v5, v20

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    invoke-direct/range {v2 .. v18}, Lcom/google/android/videos/pano/model/VideoItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/content/Intent;ZJIIFLcom/google/android/videos/utils/OfferUtil$CheapestOffer;[Lcom/google/wireless/android/video/magma/proto/ViewerRating;)V

    .line 92
    .local v2, "card":Lcom/google/android/videos/pano/model/VideoItem;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;->cards:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 93
    return-void

    .line 83
    .end local v2    # "card":Lcom/google/android/videos/pano/model/VideoItem;
    .end local v10    # "intent":Landroid/content/Intent;
    .end local v20    # "subtitle":Ljava/lang/String;
    .end local v21    # "title":Ljava/lang/String;
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;->context:Landroid/content/Context;

    const v4, 0x7f0b01aa

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p3, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    .line 84
    .restart local v21    # "title":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;->context:Landroid/content/Context;

    const v4, 0x7f0b01ac

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    .restart local v20    # "subtitle":Ljava/lang/String;
    goto :goto_0
.end method

.method protected updateInternal(Z)V
    .locals 1
    .param p1, "forced"    # Z

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;->contentNotificationManager:Lcom/google/android/videos/ContentNotificationManager;

    invoke-virtual {v0}, Lcom/google/android/videos/ContentNotificationManager;->checkForNewEpisodes()V

    .line 61
    return-void
.end method
