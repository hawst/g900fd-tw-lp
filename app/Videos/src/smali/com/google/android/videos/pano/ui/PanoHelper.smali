.class public final Lcom/google/android/videos/pano/ui/PanoHelper;
.super Ljava/lang/Object;
.source "PanoHelper.java"


# direct methods
.method public static formatPriceTitle(ILcom/google/android/videos/utils/OfferUtil$CheapestOffer;Landroid/content/Context;Z)Ljava/lang/String;
    .locals 5
    .param p0, "offerType"    # I
    .param p1, "cheapestOffer"    # Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "preorder"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 90
    if-eqz p3, :cond_1

    .line 91
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-boolean v0, p1, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->singleOffer:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0b019e

    :goto_0
    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p1, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->offer:Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    iget-object v3, v3, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedAmount:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v1, v0, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 99
    :goto_1
    return-object v0

    .line 91
    :cond_0
    const v0, 0x7f0b0197

    goto :goto_0

    .line 94
    :cond_1
    const/4 v0, 0x2

    if-ne p0, v0, :cond_3

    .line 95
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-boolean v0, p1, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->singleOffer:Z

    if-eqz v0, :cond_2

    const v0, 0x7f0b0198

    :goto_2
    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p1, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->offer:Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    iget-object v3, v3, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedAmount:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v1, v0, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    const v0, 0x7f0b0195

    goto :goto_2

    .line 99
    :cond_3
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-boolean v0, p1, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->singleOffer:Z

    if-eqz v0, :cond_4

    const v0, 0x7f0b0199

    :goto_3
    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p1, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->offer:Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    iget-object v3, v3, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedAmount:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v1, v0, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    const v0, 0x7f0b0196

    goto :goto_3
.end method

.method public static formatReleaseYear(I)Ljava/lang/String;
    .locals 1
    .param p0, "releaseYear"    # I

    .prologue
    .line 85
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static formatVideoDuration(ILandroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p0, "durationSec"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 80
    if-lez p0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0138

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    div-int/lit8 v4, p0, 0x3c

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getAssetPoster(Lcom/google/wireless/android/video/magma/proto/AssetResource;Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p0, "asset"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 122
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    sparse-switch v0, :sswitch_data_0

    .line 130
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 124
    :sswitch_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    const/4 v1, 0x3

    const v2, 0x7f0e01b2

    const v3, 0x3f31a787

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/pano/ui/PanoHelper;->getImageUri(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;IIFFLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 127
    :sswitch_1
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    const/4 v1, 0x1

    const v2, 0x7f0e01b3

    const/high16 v3, 0x3f800000    # 1.0f

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/pano/ui/PanoHelper;->getImageUri(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;IIFFLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 122
    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0x12 -> :sswitch_1
    .end sparse-switch
.end method

.method public static getAssetWallpaper(Lcom/google/wireless/android/video/magma/proto/AssetResource;Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p0, "asset"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v2, 0x7f0e01b5

    const/high16 v4, 0x3f000000    # 0.5f

    .line 135
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    sparse-switch v0, :sswitch_data_0

    .line 143
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 137
    :sswitch_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    const/4 v1, 0x4

    const v3, 0x3faaaaab

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/pano/ui/PanoHelper;->getImageUri(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;IIFFLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 140
    :sswitch_1
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    const/4 v1, 0x2

    const v3, 0x3fe38e39

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/pano/ui/PanoHelper;->getImageUri(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;IIFFLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 135
    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0x12 -> :sswitch_1
    .end sparse-switch
.end method

.method public static getContentRating(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/lang/String;
    .locals 2
    .param p0, "metadata"    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    array-length v0, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->contentRatingName:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getDefaultPosterHeight(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 148
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e01b6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public static getExpirationTitle(JLandroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p0, "expirationTimestamp"    # J
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 67
    const-wide v4, 0x7fffffffffffffffL

    cmp-long v4, p0, v4

    if-nez v4, :cond_1

    .line 75
    :cond_0
    :goto_0
    return-object v3

    .line 70
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 71
    .local v0, "nowTimestamp":J
    invoke-static {p0, p1, v0, v1}, Lcom/google/android/videos/utils/TimeUtil;->getRemainingDays(JJ)I

    move-result v2

    .line 72
    .local v2, "remainingDays":I
    const/16 v4, 0x3c

    if-gt v2, v4, :cond_0

    .line 75
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {p0, p1, v0, v1, v3}, Lcom/google/android/videos/utils/TimeUtil;->getTimeToExpirationString(JJLandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public static getImageUri(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;IIFFLandroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p0, "metadata"    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    .param p1, "imageType"    # I
    .param p2, "desiredWidthResourceId"    # I
    .param p3, "aspectRatio"    # F
    .param p4, "minScale"    # F
    .param p5, "context"    # Landroid/content/Context;

    .prologue
    .line 107
    if-eqz p0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/pano/ui/PanoHelper;->getImageUri([Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;IIFFLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 111
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getImageUri([Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;IIFFLandroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p0, "images"    # [Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .param p1, "imageType"    # I
    .param p2, "desiredWidthResourceId"    # I
    .param p3, "aspectRatio"    # F
    .param p4, "minScale"    # F
    .param p5, "context"    # Landroid/content/Context;

    .prologue
    .line 116
    invoke-virtual {p5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 117
    .local v2, "width":I
    int-to-float v0, v2

    div-float/2addr v0, p3

    float-to-int v3, v0

    .line 118
    .local v3, "height":I
    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move v4, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/api/AssetResourceUtil;->findBestImageUrl([Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;IIIFZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getSeasonTitle(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0, "metadata"    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0b00c7

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->sequenceNumber:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
