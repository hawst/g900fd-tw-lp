.class public Lcom/google/android/videos/ui/FlickeringDrawable;
.super Landroid/graphics/drawable/LayerDrawable;
.source "FlickeringDrawable.java"


# instance fields
.field private currentStep:I

.field private lastAlpha:I

.field private nextAlpha:I

.field private final random:Ljava/util/Random;


# direct methods
.method public constructor <init>([Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "layers"    # [Landroid/graphics/drawable/Drawable;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 29
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/ui/FlickeringDrawable;->random:Ljava/util/Random;

    .line 31
    const/16 v0, 0xff

    iput v0, p0, Lcom/google/android/videos/ui/FlickeringDrawable;->nextAlpha:I

    .line 32
    const/16 v0, 0xa

    iput v0, p0, Lcom/google/android/videos/ui/FlickeringDrawable;->currentStep:I

    .line 33
    return-void
.end method

.method public static varargs createFromDrawableResources(Landroid/content/res/Resources;[I)Lcom/google/android/videos/ui/FlickeringDrawable;
    .locals 4
    .param p0, "resources"    # Landroid/content/res/Resources;
    .param p1, "resIds"    # [I

    .prologue
    .line 36
    array-length v2, p1

    .line 37
    .local v2, "length":I
    if-lez v2, :cond_0

    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    .line 38
    new-array v1, v2, [Landroid/graphics/drawable/Drawable;

    .line 39
    .local v1, "layers":[Landroid/graphics/drawable/Drawable;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v2, :cond_1

    .line 40
    aget v3, p1, v0

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    aput-object v3, v1, v0

    .line 39
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 37
    .end local v0    # "i":I
    .end local v1    # "layers":[Landroid/graphics/drawable/Drawable;
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 42
    .restart local v0    # "i":I
    .restart local v1    # "layers":[Landroid/graphics/drawable/Drawable;
    :cond_1
    new-instance v3, Lcom/google/android/videos/ui/FlickeringDrawable;

    invoke-direct {v3, v1}, Lcom/google/android/videos/ui/FlickeringDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    return-object v3
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 47
    invoke-super {p0, p1}, Landroid/graphics/drawable/LayerDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 49
    invoke-virtual {p0}, Lcom/google/android/videos/ui/FlickeringDrawable;->getNumberOfLayers()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/google/android/videos/ui/FlickeringDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 50
    .local v0, "topLayer":Landroid/graphics/drawable/Drawable;
    if-nez v0, :cond_0

    .line 61
    :goto_0
    return-void

    .line 54
    :cond_0
    iget v1, p0, Lcom/google/android/videos/ui/FlickeringDrawable;->currentStep:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/videos/ui/FlickeringDrawable;->currentStep:I

    const/16 v2, 0xa

    if-lt v1, v2, :cond_1

    .line 55
    iget v1, p0, Lcom/google/android/videos/ui/FlickeringDrawable;->nextAlpha:I

    iput v1, p0, Lcom/google/android/videos/ui/FlickeringDrawable;->lastAlpha:I

    .line 56
    iget-object v1, p0, Lcom/google/android/videos/ui/FlickeringDrawable;->random:Ljava/util/Random;

    const/16 v2, 0x100

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/videos/ui/FlickeringDrawable;->nextAlpha:I

    .line 57
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/videos/ui/FlickeringDrawable;->currentStep:I

    .line 59
    :cond_1
    iget v1, p0, Lcom/google/android/videos/ui/FlickeringDrawable;->lastAlpha:I

    iget v2, p0, Lcom/google/android/videos/ui/FlickeringDrawable;->nextAlpha:I

    iget v3, p0, Lcom/google/android/videos/ui/FlickeringDrawable;->lastAlpha:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/google/android/videos/ui/FlickeringDrawable;->currentStep:I

    mul-int/2addr v2, v3

    div-int/lit8 v2, v2, 0xa

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 60
    invoke-virtual {p0}, Lcom/google/android/videos/ui/FlickeringDrawable;->invalidateSelf()V

    goto :goto_0
.end method
