.class Lcom/google/android/videos/ui/SharePanelHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "SharePanelHolder.java"


# direct methods
.method private constructor <init>(Lcom/google/android/videos/ui/SharePanelView;Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/videos/ui/SharePanelView$ShareListener;)V
    .locals 1
    .param p1, "view"    # Lcom/google/android/videos/ui/SharePanelView;
    .param p2, "url"    # Landroid/net/Uri;
    .param p3, "account"    # Ljava/lang/String;
    .param p4, "shareListener"    # Lcom/google/android/videos/ui/SharePanelView$ShareListener;

    .prologue
    .line 12
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-direct {p0, v0}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 13
    invoke-virtual {p1, p4}, Lcom/google/android/videos/ui/SharePanelView;->setShareListener(Lcom/google/android/videos/ui/SharePanelView$ShareListener;)V

    .line 14
    invoke-virtual {p1, p2, p3}, Lcom/google/android/videos/ui/SharePanelView;->setUrl(Landroid/net/Uri;Ljava/lang/String;)V

    .line 15
    return-void
.end method

.method static createSharePanelHolder(Lcom/google/android/videos/ui/SharePanelView;Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/videos/ui/SharePanelView$ShareListener;)Lcom/google/android/videos/ui/SharePanelHolder;
    .locals 1
    .param p0, "view"    # Lcom/google/android/videos/ui/SharePanelView;
    .param p1, "url"    # Landroid/net/Uri;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "shareListener"    # Lcom/google/android/videos/ui/SharePanelView$ShareListener;

    .prologue
    .line 19
    new-instance v0, Lcom/google/android/videos/ui/SharePanelHolder;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/videos/ui/SharePanelHolder;-><init>(Lcom/google/android/videos/ui/SharePanelView;Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/videos/ui/SharePanelView$ShareListener;)V

    return-object v0
.end method
