.class public Lcom/google/android/videos/store/V1Subtitle;
.super Ljava/lang/Object;
.source "V1Subtitle.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/store/V1Subtitle$Line;
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x34fd265c9966bc5cL


# instance fields
.field private final lines:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/videos/store/V1Subtitle$Line;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/store/V1Subtitle;->lines:Ljava/util/ArrayList;

    .line 23
    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 26
    new-instance v0, Lcom/google/android/videos/subtitles/Subtitles$Builder;

    invoke-direct {v0}, Lcom/google/android/videos/subtitles/Subtitles$Builder;-><init>()V

    .line 27
    .local v0, "builder":Lcom/google/android/videos/subtitles/Subtitles$Builder;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/videos/store/V1Subtitle;->lines:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v6, v2, :cond_0

    .line 28
    iget-object v2, p0, Lcom/google/android/videos/store/V1Subtitle;->lines:Ljava/util/ArrayList;

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/videos/store/V1Subtitle$Line;

    .line 29
    .local v7, "line":Lcom/google/android/videos/store/V1Subtitle$Line;
    iget-object v2, v7, Lcom/google/android/videos/store/V1Subtitle$Line;->text:Ljava/lang/String;

    iget v3, v7, Lcom/google/android/videos/store/V1Subtitle$Line;->startTimeMillis:I

    iget v4, v7, Lcom/google/android/videos/store/V1Subtitle$Line;->endTimeMillis:I

    move v5, v1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/videos/subtitles/Subtitles$Builder;->addLineToWindow(ILjava/lang/String;IIZ)Lcom/google/android/videos/subtitles/Subtitles$Builder;

    .line 27
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 32
    .end local v7    # "line":Lcom/google/android/videos/store/V1Subtitle$Line;
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/videos/subtitles/Subtitles$Builder;->build()Lcom/google/android/videos/subtitles/Subtitles;

    move-result-object v1

    return-object v1
.end method
