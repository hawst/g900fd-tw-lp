.class public Lcom/google/android/videos/activity/TrailerLauncherActivity;
.super Landroid/app/Activity;
.source "TrailerLauncherActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 32
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 33
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v12

    .line 34
    .local v12, "videosGlobals":Lcom/google/android/videos/VideosGlobals;
    invoke-virtual {v12}, Lcom/google/android/videos/VideosGlobals;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v9

    .line 35
    .local v9, "signInManager":Lcom/google/android/videos/accounts/SignInManager;
    invoke-virtual {p0}, Lcom/google/android/videos/activity/TrailerLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    .line 36
    .local v8, "intent":Landroid/content/Intent;
    invoke-virtual {v8}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v10

    .line 37
    .local v10, "uri":Landroid/net/Uri;
    if-nez v10, :cond_3

    move-object v11, v3

    .line 38
    .local v11, "uriPathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    if-eqz v11, :cond_2

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 39
    invoke-interface {v11, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 40
    .local v7, "firstSegment":Ljava/lang/String;
    const-string v0, "trailers"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "watch"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 42
    :cond_0
    const-string v0, "v"

    invoke-virtual {v10, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 43
    .local v2, "videoId":Ljava/lang/String;
    const-string v0, "authAccount"

    invoke-virtual {v8, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 44
    .local v1, "account":Ljava/lang/String;
    invoke-virtual {v9}, Lcom/google/android/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 47
    invoke-virtual {v9, v1}, Lcom/google/android/videos/accounts/SignInManager;->setSignedInAccount(Ljava/lang/String;)V

    .line 49
    :cond_1
    invoke-virtual {v12}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/Config;->panoEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v0, p0

    move-object v4, v3

    invoke-static/range {v0 .. v6}, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)Landroid/content/Intent;

    move-result-object v13

    .line 52
    .local v13, "watchIntent":Landroid/content/Intent;
    :goto_1
    invoke-virtual {p0, v13}, Lcom/google/android/videos/activity/TrailerLauncherActivity;->startActivity(Landroid/content/Intent;)V

    .line 56
    .end local v1    # "account":Ljava/lang/String;
    .end local v2    # "videoId":Ljava/lang/String;
    .end local v7    # "firstSegment":Ljava/lang/String;
    .end local v13    # "watchIntent":Landroid/content/Intent;
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/videos/activity/TrailerLauncherActivity;->finish()V

    .line 57
    return-void

    .line 37
    .end local v11    # "uriPathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_3
    invoke-virtual {v10}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v11

    goto :goto_0

    .restart local v1    # "account":Ljava/lang/String;
    .restart local v2    # "videoId":Ljava/lang/String;
    .restart local v7    # "firstSegment":Ljava/lang/String;
    .restart local v11    # "uriPathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_4
    move-object v0, p0

    move-object v4, v3

    .line 49
    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/activity/WatchActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v13

    goto :goto_1
.end method
