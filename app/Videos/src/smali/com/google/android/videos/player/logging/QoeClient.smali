.class public Lcom/google/android/videos/player/logging/QoeClient;
.super Ljava/lang/Object;
.source "QoeClient.java"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Lcom/google/android/videos/player/logging/LoggingClient;


# instance fields
.field private accumulatedBytesDownloaded:J

.field private accumulatedMsDownloading:I

.field private final appVersion:Ljava/lang/String;

.field private final baseUri:Ljava/lang/String;

.field private bitrateEstimate:J

.field private final deviceType:I

.field private final experimentId:Ljava/lang/String;

.field private firstPing:Z

.field private final handler:Landroid/os/Handler;

.field private idleIsError:Z

.field private itag:I

.field private lastBandwidthFlushSessionTimeMs:I

.field private final packageName:Ljava/lang/String;

.field private final parameterValueBuilder:Ljava/lang/StringBuilder;

.field private pendingVpsSessionTimeMs:I

.field private pendingVpsState:Ljava/lang/String;

.field private final pingSender:Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;

.field private playWhenReady:Z

.field private playbackState:I

.field private playerType:I

.field private qoeState:Ljava/lang/String;

.field private seeking:Z

.field private sessionNonce:Ljava/lang/String;

.field private sessionTimeProvider:Lcom/google/android/videos/player/logging/SessionTimeProvider;

.field private sessionUriBuilder:Lcom/google/android/videos/utils/UriBuilder;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .param p1, "pingSender"    # Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;
    .param p2, "baseUri"    # Landroid/net/Uri;
    .param p3, "appVersion"    # Ljava/lang/String;
    .param p4, "packageName"    # Ljava/lang/String;
    .param p5, "deviceType"    # I
    .param p6, "experimentId"    # Ljava/lang/String;

    .prologue
    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;

    iput-object v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->pingSender:Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;

    .line 133
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->baseUri:Ljava/lang/String;

    .line 134
    iput-object p3, p0, Lcom/google/android/videos/player/logging/QoeClient;->appVersion:Ljava/lang/String;

    .line 135
    iput-object p4, p0, Lcom/google/android/videos/player/logging/QoeClient;->packageName:Ljava/lang/String;

    .line 136
    iput p5, p0, Lcom/google/android/videos/player/logging/QoeClient;->deviceType:I

    .line 137
    iput-object p6, p0, Lcom/google/android/videos/player/logging/QoeClient;->experimentId:Ljava/lang/String;

    .line 138
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->parameterValueBuilder:Ljava/lang/StringBuilder;

    .line 139
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->handler:Landroid/os/Handler;

    .line 140
    return-void
.end method

.method private appendQoeState(ILjava/lang/String;)V
    .locals 4
    .param p1, "sessionTimeMs"    # I
    .param p2, "vpsState"    # Ljava/lang/String;

    .prologue
    .line 395
    iget-object v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->parameterValueBuilder:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 396
    iget-object v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->parameterValueBuilder:Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->getTimeString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x3a

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 398
    iget-object v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->sessionUriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    const-string v1, "vps"

    iget-object v2, p0, Lcom/google/android/videos/player/logging/QoeClient;->parameterValueBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/videos/utils/UriBuilder;->appendToQueryParameter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 400
    return-void
.end method

.method private maybeFlushRawBandwidthData(IZ)V
    .locals 8
    .param p1, "sessionTimeMs"    # I
    .param p2, "force"    # Z

    .prologue
    const-wide/16 v6, 0x0

    const/16 v5, 0x3a

    const/4 v4, 0x0

    .line 403
    iget-wide v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->accumulatedBytesDownloaded:J

    cmp-long v0, v0, v6

    if-nez v0, :cond_1

    .line 421
    :cond_0
    :goto_0
    return-void

    .line 407
    :cond_1
    if-nez p2, :cond_2

    iget v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->lastBandwidthFlushSessionTimeMs:I

    sub-int v0, p1, v0

    int-to-long v0, v0

    const-wide/16 v2, 0x7530

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 411
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->parameterValueBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 412
    iget-object v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->parameterValueBuilder:Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->getTimeString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/videos/player/logging/QoeClient;->accumulatedBytesDownloaded:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/videos/player/logging/QoeClient;->accumulatedMsDownloading:I

    invoke-static {v1}, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->getTimeString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 415
    iget-object v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->sessionUriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    const-string v1, "bwm"

    iget-object v2, p0, Lcom/google/android/videos/player/logging/QoeClient;->parameterValueBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/videos/utils/UriBuilder;->appendToQueryParameter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 417
    iput p1, p0, Lcom/google/android/videos/player/logging/QoeClient;->lastBandwidthFlushSessionTimeMs:I

    .line 418
    iput-wide v6, p0, Lcom/google/android/videos/player/logging/QoeClient;->accumulatedBytesDownloaded:J

    .line 419
    iput v4, p0, Lcom/google/android/videos/player/logging/QoeClient;->accumulatedMsDownloading:I

    .line 420
    invoke-direct {p0}, Lcom/google/android/videos/player/logging/QoeClient;->maybeSendPing()V

    goto :goto_0
.end method

.method private maybeSendPing()V
    .locals 2

    .prologue
    .line 424
    iget-object v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->sessionUriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    invoke-virtual {v0}, Lcom/google/android/videos/utils/UriBuilder;->length()I

    move-result v0

    const/16 v1, 0x708

    if-le v0, v1, :cond_0

    .line 425
    invoke-direct {p0}, Lcom/google/android/videos/player/logging/QoeClient;->sendPing()V

    .line 427
    :cond_0
    return-void
.end method

.method private onQoeState(ILjava/lang/String;Z)V
    .locals 7
    .param p1, "sessionTimeMs"    # I
    .param p2, "qoeState"    # Ljava/lang/String;
    .param p3, "isKeepAlive"    # Z

    .prologue
    const/4 v6, 0x0

    .line 367
    iget-object v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->qoeState:Ljava/lang/String;

    if-ne v0, p2, :cond_0

    if-nez p3, :cond_0

    .line 392
    :goto_0
    return-void

    .line 370
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->qoeState:Ljava/lang/String;

    const-string v1, "S"

    if-ne v0, v1, :cond_2

    const-string v0, "PA"

    if-eq p2, v0, :cond_1

    const-string v0, "PL"

    if-ne p2, v0, :cond_2

    .line 374
    :cond_1
    iput-object p2, p0, Lcom/google/android/videos/player/logging/QoeClient;->pendingVpsState:Ljava/lang/String;

    .line 375
    iput p1, p0, Lcom/google/android/videos/player/logging/QoeClient;->pendingVpsSessionTimeMs:I

    .line 387
    :goto_1
    iput-object p2, p0, Lcom/google/android/videos/player/logging/QoeClient;->qoeState:Ljava/lang/String;

    .line 390
    iget-object v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 391
    iget-object v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->handler:Landroid/os/Handler;

    const-wide/32 v2, 0x927c0

    invoke-virtual {v0, v6, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 377
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->pendingVpsState:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 378
    int-to-long v0, p1

    iget v2, p0, Lcom/google/android/videos/player/logging/QoeClient;->pendingVpsSessionTimeMs:I

    int-to-long v2, v2

    const-wide/16 v4, 0x1f4

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    .line 380
    iget v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->pendingVpsSessionTimeMs:I

    iget-object v1, p0, Lcom/google/android/videos/player/logging/QoeClient;->pendingVpsState:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/player/logging/QoeClient;->appendQoeState(ILjava/lang/String;)V

    .line 382
    :cond_3
    iput v6, p0, Lcom/google/android/videos/player/logging/QoeClient;->pendingVpsSessionTimeMs:I

    .line 383
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->pendingVpsState:Ljava/lang/String;

    .line 385
    :cond_4
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/player/logging/QoeClient;->appendQoeState(ILjava/lang/String;)V

    goto :goto_1
.end method

.method private onUserSeekingOrStateChanged(I)V
    .locals 4
    .param p1, "sessionTimeMs"    # I

    .prologue
    const/4 v3, 0x0

    .line 192
    iget-object v1, p0, Lcom/google/android/videos/player/logging/QoeClient;->qoeState:Ljava/lang/String;

    const-string v2, "ER"

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/videos/player/logging/QoeClient;->playbackState:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 194
    iput-boolean v3, p0, Lcom/google/android/videos/player/logging/QoeClient;->idleIsError:Z

    .line 197
    :cond_0
    iget v1, p0, Lcom/google/android/videos/player/logging/QoeClient;->playbackState:I

    packed-switch v1, :pswitch_data_0

    .line 213
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected playback state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/videos/player/logging/QoeClient;->playbackState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 217
    :goto_0
    return-void

    .line 199
    :pswitch_0
    iget-boolean v1, p0, Lcom/google/android/videos/player/logging/QoeClient;->idleIsError:Z

    if-eqz v1, :cond_1

    const-string v0, "ER"

    .line 216
    .local v0, "qoeState":Ljava/lang/String;
    :goto_1
    invoke-direct {p0, p1, v0, v3}, Lcom/google/android/videos/player/logging/QoeClient;->onQoeState(ILjava/lang/String;Z)V

    goto :goto_0

    .line 199
    .end local v0    # "qoeState":Ljava/lang/String;
    :cond_1
    const-string v0, "N"

    goto :goto_1

    .line 203
    :pswitch_1
    iget-boolean v1, p0, Lcom/google/android/videos/player/logging/QoeClient;->playWhenReady:Z

    if-eqz v1, :cond_2

    const-string v0, "B"

    .line 204
    .restart local v0    # "qoeState":Ljava/lang/String;
    :goto_2
    goto :goto_1

    .line 203
    .end local v0    # "qoeState":Ljava/lang/String;
    :cond_2
    const-string v0, "PB"

    goto :goto_2

    .line 206
    :pswitch_2
    iget-boolean v1, p0, Lcom/google/android/videos/player/logging/QoeClient;->playWhenReady:Z

    if-eqz v1, :cond_3

    const-string v0, "PL"

    .line 207
    .restart local v0    # "qoeState":Ljava/lang/String;
    :goto_3
    goto :goto_1

    .line 206
    .end local v0    # "qoeState":Ljava/lang/String;
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/videos/player/logging/QoeClient;->seeking:Z

    if-eqz v1, :cond_4

    const-string v0, "S"

    goto :goto_3

    :cond_4
    const-string v0, "PA"

    goto :goto_3

    .line 209
    :pswitch_3
    const-string v0, "EN"

    .line 210
    .restart local v0    # "qoeState":Ljava/lang/String;
    goto :goto_1

    .line 197
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private sendPing()V
    .locals 6

    .prologue
    .line 430
    iget-boolean v1, p0, Lcom/google/android/videos/player/logging/QoeClient;->firstPing:Z

    if-eqz v1, :cond_1

    .line 432
    iget-object v1, p0, Lcom/google/android/videos/player/logging/QoeClient;->sessionUriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    iget v2, p0, Lcom/google/android/videos/player/logging/QoeClient;->deviceType:I

    iget v3, p0, Lcom/google/android/videos/player/logging/QoeClient;->playerType:I

    iget-object v4, p0, Lcom/google/android/videos/player/logging/QoeClient;->appVersion:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/videos/player/logging/QoeClient;->packageName:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->setClientParameters(Lcom/google/android/videos/utils/UriBuilder;IILjava/lang/String;Ljava/lang/String;)V

    .line 434
    iget-object v1, p0, Lcom/google/android/videos/player/logging/QoeClient;->experimentId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 435
    iget-object v1, p0, Lcom/google/android/videos/player/logging/QoeClient;->sessionUriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    const-string v2, "fexp"

    iget-object v3, p0, Lcom/google/android/videos/player/logging/QoeClient;->experimentId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 437
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/videos/player/logging/QoeClient;->firstPing:Z

    .line 439
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/player/logging/QoeClient;->sessionUriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    invoke-virtual {v1}, Lcom/google/android/videos/utils/UriBuilder;->build()Ljava/lang/String;

    move-result-object v0

    .line 441
    .local v0, "pingUri":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/videos/player/logging/QoeClient;->sessionUriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    invoke-static {v1}, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->clearClientParameters(Lcom/google/android/videos/utils/UriBuilder;)V

    .line 442
    iget-object v1, p0, Lcom/google/android/videos/player/logging/QoeClient;->sessionUriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    const-string v2, "vfs"

    invoke-virtual {v1, v2}, Lcom/google/android/videos/utils/UriBuilder;->deleteQueryParameters(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v1

    const-string v2, "vps"

    invoke-virtual {v1, v2}, Lcom/google/android/videos/utils/UriBuilder;->deleteQueryParameters(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v1

    const-string v2, "df"

    invoke-virtual {v1, v2}, Lcom/google/android/videos/utils/UriBuilder;->deleteQueryParameters(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v1

    const-string v2, "error"

    invoke-virtual {v1, v2}, Lcom/google/android/videos/utils/UriBuilder;->deleteQueryParameters(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v1

    const-string v2, "error_info"

    invoke-virtual {v1, v2}, Lcom/google/android/videos/utils/UriBuilder;->deleteQueryParameters(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v1

    const-string v2, "bwe"

    invoke-virtual {v1, v2}, Lcom/google/android/videos/utils/UriBuilder;->deleteQueryParameters(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v1

    const-string v2, "bwm"

    invoke-virtual {v1, v2}, Lcom/google/android/videos/utils/UriBuilder;->deleteQueryParameters(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v1

    const-string v2, "fexp"

    invoke-virtual {v1, v2}, Lcom/google/android/videos/utils/UriBuilder;->deleteQueryParameters(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 451
    iget-object v1, p0, Lcom/google/android/videos/player/logging/QoeClient;->pingSender:Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;

    iget-object v2, p0, Lcom/google/android/videos/player/logging/QoeClient;->sessionNonce:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;->createQoePing(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->sendPing(Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;)V

    .line 452
    return-void
.end method

.method private setYouTubeConnectionType(I)V
    .locals 3
    .param p1, "youtubeConnectionType"    # I

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->sessionUriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    const-string v1, "conn"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 285
    return-void
.end method

.method private static toQoeErrorCode(ILjava/lang/Exception;)I
    .locals 2
    .param p0, "errorType"    # I
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    const/16 v1, 0x13e

    const/16 v0, 0x6b

    .line 472
    packed-switch p0, :pswitch_data_0

    .line 508
    :goto_0
    :pswitch_0
    return v0

    .line 477
    :pswitch_1
    const/16 v0, 0x13d

    goto :goto_0

    .line 479
    :pswitch_2
    const/16 v0, 0x12d

    goto :goto_0

    .line 481
    :pswitch_3
    const/16 v0, 0x13b

    goto :goto_0

    .line 483
    :pswitch_4
    const/16 v0, 0x13f

    goto :goto_0

    :pswitch_5
    move v0, v1

    .line 486
    goto :goto_0

    :pswitch_6
    move v0, v1

    .line 488
    goto :goto_0

    .line 492
    :pswitch_7
    const/16 v0, 0x78

    invoke-static {p1, v0}, Lcom/google/android/videos/player/logging/QoeClient;->toQoeErrorCode(Ljava/lang/Exception;I)I

    move-result v0

    goto :goto_0

    .line 494
    :pswitch_8
    const/16 v0, 0x64

    goto :goto_0

    .line 496
    :pswitch_9
    const/16 v0, 0x13a

    goto :goto_0

    .line 498
    :pswitch_a
    const/16 v0, 0x139

    goto :goto_0

    .line 500
    :pswitch_b
    invoke-static {p1, v0}, Lcom/google/android/videos/player/logging/QoeClient;->toQoeErrorCode(Ljava/lang/Exception;I)I

    move-result v0

    goto :goto_0

    .line 502
    :pswitch_c
    const/16 v0, 0x140

    invoke-static {p1, v0}, Lcom/google/android/videos/player/logging/QoeClient;->toQoeErrorCode(Ljava/lang/Exception;I)I

    move-result v0

    goto :goto_0

    .line 504
    :pswitch_d
    const/16 v0, 0x13c

    goto :goto_0

    .line 506
    :pswitch_e
    const/16 v0, 0x67

    goto :goto_0

    .line 472
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_0
        :pswitch_7
        :pswitch_7
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method private static toQoeErrorCode(Ljava/lang/Exception;I)I
    .locals 1
    .param p0, "exception"    # Ljava/lang/Exception;
    .param p1, "defaultErrorCode"    # I

    .prologue
    .line 513
    instance-of v0, p0, Landroid/accounts/AuthenticatorException;

    if-eqz v0, :cond_1

    .line 514
    const/16 p1, 0x69

    .line 521
    .end local p1    # "defaultErrorCode":I
    :cond_0
    :goto_0
    return p1

    .line 515
    .restart local p1    # "defaultErrorCode":I
    :cond_1
    instance-of v0, p0, Ljava/net/SocketTimeoutException;

    if-nez v0, :cond_2

    instance-of v0, p0, Lorg/apache/http/client/HttpResponseException;

    if-eqz v0, :cond_3

    .line 517
    :cond_2
    const/16 p1, 0x66

    goto :goto_0

    .line 518
    :cond_3
    instance-of v0, p0, Ljava/io/IOException;

    if-eqz v0, :cond_0

    .line 519
    const/16 p1, 0x78

    goto :goto_0
.end method

.method private static toQoeTrigger(I)Ljava/lang/String;
    .locals 1
    .param p0, "trigger"    # I

    .prologue
    .line 457
    packed-switch p0, :pswitch_data_0

    .line 467
    const-string v0, ""

    :goto_0
    return-object v0

    .line 459
    :pswitch_0
    const-string v0, "i"

    goto :goto_0

    .line 461
    :pswitch_1
    const-string v0, "m"

    goto :goto_0

    .line 463
    :pswitch_2
    const-string v0, "a"

    goto :goto_0

    .line 465
    :pswitch_3
    const-string v0, "r"

    goto :goto_0

    .line 457
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public endSession(IILcom/google/android/videos/player/logging/DerivedStats;)V
    .locals 2
    .param p1, "sessionTimeMs"    # I
    .param p2, "mediaTimeMs"    # I
    .param p3, "derivedStats"    # Lcom/google/android/videos/player/logging/DerivedStats;

    .prologue
    const/4 v1, 0x0

    .line 350
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/player/logging/QoeClient;->maybeFlushRawBandwidthData(IZ)V

    .line 351
    const-string v0, "EN"

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/videos/player/logging/QoeClient;->onQoeState(ILjava/lang/String;Z)V

    .line 352
    iget-object v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 353
    invoke-direct {p0}, Lcom/google/android/videos/player/logging/QoeClient;->sendPing()V

    .line 354
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v0, 0x1

    .line 358
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 363
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 360
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/videos/player/logging/QoeClient;->sessionTimeProvider:Lcom/google/android/videos/player/logging/SessionTimeProvider;

    invoke-interface {v1}, Lcom/google/android/videos/player/logging/SessionTimeProvider;->getSessionTimeMs()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/videos/player/logging/QoeClient;->qoeState:Ljava/lang/String;

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/videos/player/logging/QoeClient;->onQoeState(ILjava/lang/String;Z)V

    goto :goto_0

    .line 358
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onBandwidthSample(IIJJ)V
    .locals 3
    .param p1, "sessionTimeMs"    # I
    .param p2, "elapsedMs"    # I
    .param p3, "bytes"    # J
    .param p5, "bitrateEstimate"    # J

    .prologue
    .line 271
    iget v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->accumulatedMsDownloading:I

    add-int/2addr v0, p2

    iput v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->accumulatedMsDownloading:I

    .line 272
    iget-wide v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->accumulatedBytesDownloaded:J

    add-long/2addr v0, p3

    iput-wide v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->accumulatedBytesDownloaded:J

    .line 273
    iput-wide p5, p0, Lcom/google/android/videos/player/logging/QoeClient;->bitrateEstimate:J

    .line 274
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/player/logging/QoeClient;->maybeFlushRawBandwidthData(IZ)V

    .line 275
    return-void
.end method

.method public onDroppedFrames(II)V
    .locals 4
    .param p1, "sessionTimeMs"    # I
    .param p2, "count"    # I

    .prologue
    .line 289
    iget-object v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->parameterValueBuilder:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 290
    iget-object v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->parameterValueBuilder:Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->getTimeString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x3a

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 292
    iget-object v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->sessionUriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    const-string v1, "df"

    iget-object v2, p0, Lcom/google/android/videos/player/logging/QoeClient;->parameterValueBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/videos/utils/UriBuilder;->appendToQueryParameter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 294
    invoke-direct {p0}, Lcom/google/android/videos/player/logging/QoeClient;->maybeSendPing()V

    .line 295
    return-void
.end method

.method public onError(IIIILjava/lang/Exception;)V
    .locals 6
    .param p1, "sessionTimeMs"    # I
    .param p2, "mediaTimeMs"    # I
    .param p3, "errorType"    # I
    .param p4, "errorCode"    # I
    .param p5, "exception"    # Ljava/lang/Exception;

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x3a

    .line 227
    invoke-static {p3, p5}, Lcom/google/android/videos/player/logging/QoeClient;->toQoeErrorCode(ILjava/lang/Exception;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 230
    .local v0, "qoeErrorCodeString":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/videos/player/logging/QoeClient;->parameterValueBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 231
    iget-object v1, p0, Lcom/google/android/videos/player/logging/QoeClient;->parameterValueBuilder:Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->getTimeString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->getTimeString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 234
    iget-object v1, p0, Lcom/google/android/videos/player/logging/QoeClient;->sessionUriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    const-string v2, "error"

    iget-object v3, p0, Lcom/google/android/videos/player/logging/QoeClient;->parameterValueBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 236
    iget-object v1, p0, Lcom/google/android/videos/player/logging/QoeClient;->parameterValueBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 237
    iget-object v1, p0, Lcom/google/android/videos/player/logging/QoeClient;->parameterValueBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 239
    if-eqz p5, :cond_0

    .line 240
    iget-object v1, p0, Lcom/google/android/videos/player/logging/QoeClient;->parameterValueBuilder:Ljava/lang/StringBuilder;

    const/16 v2, 0x2e

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 242
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/player/logging/QoeClient;->sessionUriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    const-string v2, "error_info"

    iget-object v3, p0, Lcom/google/android/videos/player/logging/QoeClient;->parameterValueBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 244
    invoke-direct {p0}, Lcom/google/android/videos/player/logging/QoeClient;->sendPing()V

    .line 245
    return-void
.end method

.method public onFailed(II)V
    .locals 1
    .param p1, "sessionTimeMs"    # I
    .param p2, "mediaTimeMs"    # I

    .prologue
    .line 249
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->idleIsError:Z

    .line 250
    invoke-direct {p0}, Lcom/google/android/videos/player/logging/QoeClient;->maybeSendPing()V

    .line 251
    return-void
.end method

.method public onFormatEnabled(III)V
    .locals 9
    .param p1, "sessionTimeMs"    # I
    .param p2, "itag"    # I
    .param p3, "trigger"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v7, -0x1

    const/16 v6, 0x3a

    .line 300
    iget-object v2, p0, Lcom/google/android/videos/player/logging/QoeClient;->parameterValueBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 301
    iget-object v2, p0, Lcom/google/android/videos/player/logging/QoeClient;->parameterValueBuilder:Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->getTimeString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 305
    iget v2, p0, Lcom/google/android/videos/player/logging/QoeClient;->itag:I

    if-eq v2, v7, :cond_0

    .line 306
    iget-object v2, p0, Lcom/google/android/videos/player/logging/QoeClient;->parameterValueBuilder:Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/google/android/videos/player/logging/QoeClient;->itag:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 308
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/player/logging/QoeClient;->parameterValueBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p3}, Lcom/google/android/videos/player/logging/QoeClient;->toQoeTrigger(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 309
    iget-object v2, p0, Lcom/google/android/videos/player/logging/QoeClient;->sessionUriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    const-string v3, "vfs"

    iget-object v4, p0, Lcom/google/android/videos/player/logging/QoeClient;->parameterValueBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/videos/utils/UriBuilder;->appendToQueryParameter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 313
    iget-wide v2, p0, Lcom/google/android/videos/player/logging/QoeClient;->bitrateEstimate:J

    const-wide/16 v4, 0x8

    div-long v0, v2, v4

    .line 314
    .local v0, "byterateEstimate":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 315
    iget-object v2, p0, Lcom/google/android/videos/player/logging/QoeClient;->parameterValueBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 316
    iget-object v2, p0, Lcom/google/android/videos/player/logging/QoeClient;->parameterValueBuilder:Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->getTimeString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    long-to-float v3, v0

    invoke-static {v3}, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->getFractionString(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 318
    iget-object v2, p0, Lcom/google/android/videos/player/logging/QoeClient;->sessionUriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    const-string v3, "bwe"

    iget-object v4, p0, Lcom/google/android/videos/player/logging/QoeClient;->parameterValueBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/videos/utils/UriBuilder;->appendToQueryParameter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 323
    :cond_1
    iput p2, p0, Lcom/google/android/videos/player/logging/QoeClient;->itag:I

    .line 324
    if-ne p2, v7, :cond_2

    .line 325
    iget-object v2, p0, Lcom/google/android/videos/player/logging/QoeClient;->sessionUriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    const-string v3, "fmt"

    invoke-virtual {v2, v3}, Lcom/google/android/videos/utils/UriBuilder;->deleteQueryParameters(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 330
    :goto_0
    invoke-direct {p0}, Lcom/google/android/videos/player/logging/QoeClient;->maybeSendPing()V

    .line 331
    return-void

    .line 327
    :cond_2
    iget-object v2, p0, Lcom/google/android/videos/player/logging/QoeClient;->sessionUriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    const-string v3, "fmt"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    goto :goto_0
.end method

.method public onFormatSelected(III)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "itag"    # I
    .param p3, "trigger"    # I

    .prologue
    .line 222
    return-void
.end method

.method public onHttpDataSourceOpened(IJ)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "elapsedMs"    # J

    .prologue
    .line 280
    return-void
.end method

.method public onLoadingChanged(IZ)V
    .locals 1
    .param p1, "sessionTimeMs"    # I
    .param p2, "loading"    # Z

    .prologue
    .line 255
    if-nez p2, :cond_0

    .line 258
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/player/logging/QoeClient;->maybeFlushRawBandwidthData(IZ)V

    .line 260
    :cond_0
    return-void
.end method

.method public onNetworkType(III)V
    .locals 1
    .param p1, "sessionTimeMs"    # I
    .param p2, "type"    # I
    .param p3, "subType"    # I

    .prologue
    .line 264
    invoke-static {p2, p3}, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->toStatsConnectionType(II)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/logging/QoeClient;->setYouTubeConnectionType(I)V

    .line 265
    invoke-direct {p0}, Lcom/google/android/videos/player/logging/QoeClient;->maybeSendPing()V

    .line 266
    return-void
.end method

.method public onStateChanged(IIZI)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "mediaTimeMs"    # I
    .param p3, "playWhenReady"    # Z
    .param p4, "playbackState"    # I

    .prologue
    .line 185
    iput p4, p0, Lcom/google/android/videos/player/logging/QoeClient;->playbackState:I

    .line 186
    iput-boolean p3, p0, Lcom/google/android/videos/player/logging/QoeClient;->playWhenReady:Z

    .line 187
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/logging/QoeClient;->onUserSeekingOrStateChanged(I)V

    .line 188
    invoke-direct {p0}, Lcom/google/android/videos/player/logging/QoeClient;->maybeSendPing()V

    .line 189
    return-void
.end method

.method public onSubtitleEnabled(ILcom/google/android/videos/subtitles/SubtitleTrack;)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "track"    # Lcom/google/android/videos/subtitles/SubtitleTrack;

    .prologue
    .line 341
    return-void
.end method

.method public onSubtitleError(ILcom/google/android/videos/subtitles/SubtitleTrack;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "track"    # Lcom/google/android/videos/subtitles/SubtitleTrack;
    .param p3, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 346
    return-void
.end method

.method public onSubtitleSelected(ILcom/google/android/videos/subtitles/SubtitleTrack;)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "track"    # Lcom/google/android/videos/subtitles/SubtitleTrack;

    .prologue
    .line 336
    return-void
.end method

.method public onUserSeekingChanged(IIZZ)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "mediaTimeMs"    # I
    .param p3, "seeking"    # Z
    .param p4, "isFineScrubbing"    # Z

    .prologue
    .line 177
    iput-boolean p3, p0, Lcom/google/android/videos/player/logging/QoeClient;->seeking:Z

    .line 178
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/logging/QoeClient;->onUserSeekingOrStateChanged(I)V

    .line 179
    invoke-direct {p0}, Lcom/google/android/videos/player/logging/QoeClient;->maybeSendPing()V

    .line 180
    return-void
.end method

.method public startSession(Lcom/google/android/videos/player/logging/SessionTimeProvider;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 6
    .param p1, "sessionTimeProvider"    # Lcom/google/android/videos/player/logging/SessionTimeProvider;
    .param p2, "sessionNonce"    # Ljava/lang/String;
    .param p3, "playerType"    # I
    .param p4, "account"    # Ljava/lang/String;
    .param p5, "videoId"    # Ljava/lang/String;
    .param p6, "showId"    # Ljava/lang/String;
    .param p7, "seasonId"    # Ljava/lang/String;
    .param p8, "isTrailer"    # Z
    .param p9, "isOffline"    # Z

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 151
    iput-object p1, p0, Lcom/google/android/videos/player/logging/QoeClient;->sessionTimeProvider:Lcom/google/android/videos/player/logging/SessionTimeProvider;

    .line 152
    iput-object p2, p0, Lcom/google/android/videos/player/logging/QoeClient;->sessionNonce:Ljava/lang/String;

    .line 153
    iput p3, p0, Lcom/google/android/videos/player/logging/QoeClient;->playerType:I

    .line 154
    iget-object v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->baseUri:Ljava/lang/String;

    const-string v1, "qoe"

    invoke-static {v0, v1, p2, p5}, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->getStatsUriBuilder(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->sessionUriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    .line 156
    iget-object v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->sessionUriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    const-string v1, "event"

    const-string v2, "streamingstats"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 157
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->itag:I

    .line 158
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->bitrateEstimate:J

    .line 159
    iput v3, p0, Lcom/google/android/videos/player/logging/QoeClient;->lastBandwidthFlushSessionTimeMs:I

    .line 160
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/videos/player/logging/QoeClient;->accumulatedBytesDownloaded:J

    .line 161
    iput v3, p0, Lcom/google/android/videos/player/logging/QoeClient;->accumulatedMsDownloading:I

    .line 162
    iput v4, p0, Lcom/google/android/videos/player/logging/QoeClient;->playbackState:I

    .line 163
    iput-boolean v3, p0, Lcom/google/android/videos/player/logging/QoeClient;->playWhenReady:Z

    .line 164
    iput-boolean v3, p0, Lcom/google/android/videos/player/logging/QoeClient;->seeking:Z

    .line 165
    iput-boolean v3, p0, Lcom/google/android/videos/player/logging/QoeClient;->idleIsError:Z

    .line 166
    iput v3, p0, Lcom/google/android/videos/player/logging/QoeClient;->pendingVpsSessionTimeMs:I

    .line 167
    iput-object v5, p0, Lcom/google/android/videos/player/logging/QoeClient;->pendingVpsState:Ljava/lang/String;

    .line 168
    iput-object v5, p0, Lcom/google/android/videos/player/logging/QoeClient;->qoeState:Ljava/lang/String;

    .line 169
    iput-boolean v4, p0, Lcom/google/android/videos/player/logging/QoeClient;->firstPing:Z

    .line 170
    invoke-direct {p0, v3}, Lcom/google/android/videos/player/logging/QoeClient;->setYouTubeConnectionType(I)V

    .line 171
    const-string v0, "N"

    invoke-direct {p0, v3, v0, v3}, Lcom/google/android/videos/player/logging/QoeClient;->onQoeState(ILjava/lang/String;Z)V

    .line 172
    return-void
.end method

.method public supportsOfflinePlaybacks()Z
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x0

    return v0
.end method
