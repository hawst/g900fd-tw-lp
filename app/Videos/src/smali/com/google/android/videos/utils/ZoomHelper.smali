.class public Lcom/google/android/videos/utils/ZoomHelper;
.super Ljava/lang/Object;
.source "ZoomHelper.java"

# interfaces
.implements Lcom/google/android/videos/player/PlayerSurface$OnDisplayParametersChangedListener;


# instance fields
.field private final activity:Landroid/app/Activity;

.field private canZoom:Z

.field private isZoomed:Z

.field private final playerSurface:Lcom/google/android/videos/player/PlayerSurface;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/videos/player/PlayerSurface;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "playerSurface"    # Lcom/google/android/videos/player/PlayerSurface;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/google/android/videos/utils/ZoomHelper;->activity:Landroid/app/Activity;

    .line 31
    iput-object p2, p0, Lcom/google/android/videos/utils/ZoomHelper;->playerSurface:Lcom/google/android/videos/player/PlayerSurface;

    .line 32
    invoke-interface {p2, p0}, Lcom/google/android/videos/player/PlayerSurface;->setOnDisplayParametersChangedListener(Lcom/google/android/videos/player/PlayerSurface$OnDisplayParametersChangedListener;)V

    .line 33
    invoke-direct {p0}, Lcom/google/android/videos/utils/ZoomHelper;->shouldAllowZoom()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/videos/utils/ZoomHelper;->canZoom:Z

    .line 34
    return-void
.end method

.method private onDisplayParametersChanged()V
    .locals 2

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/google/android/videos/utils/ZoomHelper;->shouldAllowZoom()Z

    move-result v0

    .line 77
    .local v0, "canZoom":Z
    iget-boolean v1, p0, Lcom/google/android/videos/utils/ZoomHelper;->canZoom:Z

    if-eq v1, v0, :cond_0

    .line 78
    iput-boolean v0, p0, Lcom/google/android/videos/utils/ZoomHelper;->canZoom:Z

    .line 79
    iget-object v1, p0, Lcom/google/android/videos/utils/ZoomHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 81
    :cond_0
    return-void
.end method

.method private setIsZoomed(Z)V
    .locals 2
    .param p1, "shouldZoom"    # Z

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/google/android/videos/utils/ZoomHelper;->isZoomed:Z

    if-eq p1, v0, :cond_0

    .line 58
    iput-boolean p1, p0, Lcom/google/android/videos/utils/ZoomHelper;->isZoomed:Z

    .line 59
    iget-object v0, p0, Lcom/google/android/videos/utils/ZoomHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 60
    iget-object v1, p0, Lcom/google/android/videos/utils/ZoomHelper;->playerSurface:Lcom/google/android/videos/player/PlayerSurface;

    iget-boolean v0, p0, Lcom/google/android/videos/utils/ZoomHelper;->isZoomed:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x64

    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/videos/player/PlayerSurface;->setZoom(I)V

    .line 62
    :cond_0
    return-void

    .line 60
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldAllowZoom()Z
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/videos/utils/ZoomHelper;->playerSurface:Lcom/google/android/videos/player/PlayerSurface;

    invoke-interface {v0}, Lcom/google/android/videos/player/PlayerSurface;->zoomSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/utils/ZoomHelper;->playerSurface:Lcom/google/android/videos/player/PlayerSurface;

    invoke-interface {v0}, Lcom/google/android/videos/player/PlayerSurface;->getVerticalLetterboxFraction()F

    move-result v0

    const v1, 0x3f59999a    # 0.85f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;Z)V
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;
    .param p3, "forceHide"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 37
    if-eqz p3, :cond_0

    .line 43
    :goto_0
    return-void

    .line 40
    :cond_0
    const v0, 0x7f140006

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 41
    const v3, 0x7f0f0228

    iget-boolean v0, p0, Lcom/google/android/videos/utils/ZoomHelper;->canZoom:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/videos/utils/ZoomHelper;->isZoomed:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {p1, v3, v0}, Lcom/google/android/videos/utils/Util;->setMenuItemEnabled(Landroid/view/Menu;IZ)V

    .line 42
    const v0, 0x7f0f0229

    iget-boolean v3, p0, Lcom/google/android/videos/utils/ZoomHelper;->canZoom:Z

    if-eqz v3, :cond_2

    iget-boolean v3, p0, Lcom/google/android/videos/utils/ZoomHelper;->isZoomed:Z

    if-eqz v3, :cond_2

    :goto_2
    invoke-static {p1, v0, v1}, Lcom/google/android/videos/utils/Util;->setMenuItemEnabled(Landroid/view/Menu;IZ)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 41
    goto :goto_1

    :cond_2
    move v1, v2

    .line 42
    goto :goto_2
.end method

.method public onLetterboxChanged(FF)V
    .locals 0
    .param p1, "horizontalLetterboxFraction"    # F
    .param p2, "verticalLetterboxFraction"    # F

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/google/android/videos/utils/ZoomHelper;->onDisplayParametersChanged()V

    .line 68
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 46
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x7f0f0228

    if-ne v2, v3, :cond_0

    .line 47
    invoke-direct {p0, v0}, Lcom/google/android/videos/utils/ZoomHelper;->setIsZoomed(Z)V

    .line 53
    :goto_0
    return v0

    .line 49
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x7f0f0229

    if-ne v2, v3, :cond_1

    .line 50
    invoke-direct {p0, v1}, Lcom/google/android/videos/utils/ZoomHelper;->setIsZoomed(Z)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 53
    goto :goto_0
.end method

.method public onZoomSupportedChanged(Z)V
    .locals 0
    .param p1, "zoomSupported"    # Z

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/google/android/videos/utils/ZoomHelper;->onDisplayParametersChanged()V

    .line 73
    return-void
.end method
