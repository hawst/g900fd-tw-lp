.class final Lcom/google/android/videos/logging/DefaultEventLogger$Values;
.super Ljava/lang/Object;
.source "DefaultEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/logging/DefaultEventLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Values"
.end annotation


# direct methods
.method public static apiActionToProto(I)I
    .locals 1
    .param p0, "cardType"    # I

    .prologue
    .line 1167
    packed-switch p0, :pswitch_data_0

    .line 1177
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1169
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 1171
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 1173
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 1175
    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 1167
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static cardTypeToProto(I)I
    .locals 1
    .param p0, "cardType"    # I

    .prologue
    .line 1152
    packed-switch p0, :pswitch_data_0

    .line 1162
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1154
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 1156
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 1158
    :pswitch_2
    const/4 v0, 0x4

    goto :goto_0

    .line 1160
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 1152
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public static integerSetToArray(Ljava/util/HashSet;)[I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;)[I"
        }
    .end annotation

    .prologue
    .line 1182
    .local p0, "ints":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    invoke-virtual {p0}, Ljava/util/HashSet;->size()I

    move-result v5

    new-array v0, v5, [I

    .line 1183
    .local v0, "array":[I
    const/4 v3, 0x0

    .line 1184
    .local v3, "index":I
    invoke-virtual {p0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 1185
    .local v1, "i":Ljava/lang/Integer;
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "index":I
    .local v4, "index":I
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    aput v5, v0, v3

    move v3, v4

    .line 1186
    .end local v4    # "index":I
    .restart local v3    # "index":I
    goto :goto_0

    .line 1187
    .end local v1    # "i":Ljava/lang/Integer;
    :cond_0
    return-object v0
.end method

.method public static networkTypeToProto(I)I
    .locals 1
    .param p0, "networkType"    # I

    .prologue
    .line 1089
    packed-switch p0, :pswitch_data_0

    .line 1111
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1091
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 1093
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 1095
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 1097
    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 1099
    :pswitch_4
    const/4 v0, 0x5

    goto :goto_0

    .line 1101
    :pswitch_5
    const/4 v0, 0x6

    goto :goto_0

    .line 1103
    :pswitch_6
    const/4 v0, 0x7

    goto :goto_0

    .line 1105
    :pswitch_7
    const/16 v0, 0x8

    goto :goto_0

    .line 1107
    :pswitch_8
    const/16 v0, 0x9

    goto :goto_0

    .line 1109
    :pswitch_9
    const/16 v0, 0xa

    goto :goto_0

    .line 1089
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static objectToClassName(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "object"    # Ljava/lang/Object;

    .prologue
    .line 1191
    if-nez p0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static playerTypeToProto(I)I
    .locals 2
    .param p0, "playerType"    # I

    .prologue
    .line 1075
    packed-switch p0, :pswitch_data_0

    .line 1083
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown player type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 1084
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1077
    :pswitch_0
    const/4 v0, 0x2

    goto :goto_0

    .line 1079
    :pswitch_1
    const/4 v0, 0x3

    goto :goto_0

    .line 1081
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    .line 1075
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static premiumStatusToProto(I)I
    .locals 1
    .param p0, "premiumStatus"    # I

    .prologue
    .line 1116
    packed-switch p0, :pswitch_data_0

    .line 1126
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1118
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 1120
    :pswitch_1
    const/4 v0, 0x5

    goto :goto_0

    .line 1122
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 1124
    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 1116
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method public static throwableToLocation(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 6
    .param p0, "throwable"    # Ljava/lang/Throwable;

    .prologue
    const/16 v5, 0x2e

    const/4 v4, 0x0

    .line 1195
    if-nez p0, :cond_0

    .line 1196
    const-string v2, ""

    .line 1203
    :goto_0
    return-object v2

    .line 1198
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    .line 1199
    .local v1, "stackTrace":[Ljava/lang/StackTraceElement;
    if-eqz v1, :cond_1

    array-length v2, v1

    if-nez v2, :cond_2

    .line 1200
    :cond_1
    const-string v2, ""

    goto :goto_0

    .line 1202
    :cond_2
    aget-object v2, v1, v4

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v0

    .line 1203
    .local v0, "className":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v1, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3a

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v1, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static verticalToPage(I)I
    .locals 2
    .param p0, "vertical"    # I

    .prologue
    .line 1131
    sparse-switch p0, :sswitch_data_0

    .line 1146
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown vertical: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 1147
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1133
    :sswitch_0
    const/4 v0, 0x3

    goto :goto_0

    .line 1135
    :sswitch_1
    const/4 v0, 0x4

    goto :goto_0

    .line 1137
    :sswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 1142
    :sswitch_3
    const/16 v0, 0xd

    goto :goto_0

    .line 1144
    :sswitch_4
    const/16 v0, 0xf

    goto :goto_0

    .line 1131
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x4 -> :sswitch_2
        0x8 -> :sswitch_3
        0x10 -> :sswitch_4
    .end sparse-switch
.end method
