.class public interface abstract Lcom/google/android/videos/tagging/CardsViewAnimationHelper$Listener;
.super Ljava/lang/Object;
.source "CardsViewAnimationHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/CardsViewAnimationHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onCardListCollapseProgress(F)V
.end method

.method public abstract onCardListCollapsed(I)V
.end method

.method public abstract onCardListExpanded(I)V
.end method
