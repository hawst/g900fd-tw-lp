.class public Lcom/google/android/videos/streams/LegacyStreamSelection;
.super Ljava/lang/Object;
.source "LegacyStreamSelection.java"


# instance fields
.field public final audioInfos:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

.field public final hi:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;"
        }
    .end annotation
.end field

.field public final isHiHd:Z

.field public final isOffline:Z

.field public final lo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;"
        }
    .end annotation
.end field

.field public final supportsQualityToggle:Z


# direct methods
.method public constructor <init>(Lcom/google/android/videos/streams/MediaStream;)V
    .locals 1
    .param p1, "stream"    # Lcom/google/android/videos/streams/MediaStream;

    .prologue
    .line 77
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/streams/LegacyStreamSelection;-><init>(Ljava/util/List;)V

    .line 78
    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 73
    .local p1, "streams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    invoke-direct {p0, p1, p1}, Lcom/google/android/videos/streams/LegacyStreamSelection;-><init>(Ljava/util/List;Ljava/util/List;)V

    .line 74
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "hi":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    .local p2, "lo":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    if-nez p1, :cond_4

    .line 31
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    .end local p1    # "hi":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    check-cast p1, Ljava/util/List;

    .line 35
    .restart local p1    # "hi":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_5

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v9

    if-ne v6, v9, :cond_5

    move v6, v7

    :goto_1
    invoke-static {v6}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 37
    iput-object p1, p0, Lcom/google/android/videos/streams/LegacyStreamSelection;->hi:Ljava/util/List;

    .line 38
    iput-object p2, p0, Lcom/google/android/videos/streams/LegacyStreamSelection;->lo:Ljava/util/List;

    .line 39
    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/streams/MediaStream;

    .line 40
    .local v0, "firstHiStream":Lcom/google/android/videos/streams/MediaStream;
    iget-boolean v6, v0, Lcom/google/android/videos/streams/MediaStream;->isOffline:Z

    iput-boolean v6, p0, Lcom/google/android/videos/streams/LegacyStreamSelection;->isOffline:Z

    .line 41
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    if-ne v6, v7, :cond_6

    iget-object v6, v0, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-object v6, v6, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    if-nez v6, :cond_6

    .line 42
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/google/android/videos/streams/LegacyStreamSelection;->audioInfos:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    .line 47
    :goto_2
    const/4 v3, 0x0

    .line 48
    .local v3, "isHiHd":Z
    const/4 v5, 0x0

    .line 49
    .local v5, "supportsQualityToggle":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    if-ge v2, v6, :cond_9

    .line 50
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/streams/MediaStream;

    .line 51
    .local v1, "hiStream":Lcom/google/android/videos/streams/MediaStream;
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/videos/streams/MediaStream;

    .line 52
    .local v4, "loStream":Lcom/google/android/videos/streams/MediaStream;
    iget-boolean v6, p0, Lcom/google/android/videos/streams/LegacyStreamSelection;->isOffline:Z

    iget-boolean v9, v1, Lcom/google/android/videos/streams/MediaStream;->isOffline:Z

    if-ne v6, v9, :cond_7

    move v6, v7

    :goto_4
    invoke-static {v6}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 53
    iget-boolean v6, p0, Lcom/google/android/videos/streams/LegacyStreamSelection;->isOffline:Z

    iget-boolean v9, v4, Lcom/google/android/videos/streams/MediaStream;->isOffline:Z

    if-ne v6, v9, :cond_8

    move v6, v7

    :goto_5
    invoke-static {v6}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 54
    iget-object v6, v1, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-object v6, v6, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    iget-object v9, v4, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-object v9, v9, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    invoke-static {v6, v9, v7}, Lcom/google/android/videos/utils/AudioInfoUtil;->areEqual(Lcom/google/wireless/android/video/magma/proto/AudioInfo;Lcom/google/wireless/android/video/magma/proto/AudioInfo;Z)Z

    move-result v6

    invoke-static {v6}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 56
    iget-object v6, p0, Lcom/google/android/videos/streams/LegacyStreamSelection;->audioInfos:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    if-eqz v6, :cond_1

    .line 57
    iget-object v6, p0, Lcom/google/android/videos/streams/LegacyStreamSelection;->audioInfos:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    iget-object v9, v1, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-object v9, v9, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    aput-object v9, v6, v2

    .line 59
    :cond_1
    iget-object v6, v1, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget v6, v6, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    iget-object v9, v4, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget v9, v9, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    if-eq v6, v9, :cond_2

    .line 61
    const/4 v5, 0x1

    .line 63
    :cond_2
    iget-object v6, v1, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    invoke-virtual {v6}, Lcom/google/android/videos/streams/ItagInfo;->getQuality()I

    move-result v6

    if-ne v6, v7, :cond_3

    .line 65
    const/4 v3, 0x1

    .line 49
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 32
    .end local v0    # "firstHiStream":Lcom/google/android/videos/streams/MediaStream;
    .end local v1    # "hiStream":Lcom/google/android/videos/streams/MediaStream;
    .end local v2    # "i":I
    .end local v3    # "isHiHd":Z
    .end local v4    # "loStream":Lcom/google/android/videos/streams/MediaStream;
    .end local v5    # "supportsQualityToggle":Z
    :cond_4
    if-nez p2, :cond_0

    .line 33
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    .end local p2    # "lo":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    check-cast p2, Ljava/util/List;

    .restart local p2    # "lo":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    goto/16 :goto_0

    :cond_5
    move v6, v8

    .line 35
    goto/16 :goto_1

    .line 44
    .restart local v0    # "firstHiStream":Lcom/google/android/videos/streams/MediaStream;
    :cond_6
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    new-array v6, v6, [Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    iput-object v6, p0, Lcom/google/android/videos/streams/LegacyStreamSelection;->audioInfos:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    goto :goto_2

    .restart local v1    # "hiStream":Lcom/google/android/videos/streams/MediaStream;
    .restart local v2    # "i":I
    .restart local v3    # "isHiHd":Z
    .restart local v4    # "loStream":Lcom/google/android/videos/streams/MediaStream;
    .restart local v5    # "supportsQualityToggle":Z
    :cond_7
    move v6, v8

    .line 52
    goto :goto_4

    :cond_8
    move v6, v8

    .line 53
    goto :goto_5

    .line 68
    .end local v1    # "hiStream":Lcom/google/android/videos/streams/MediaStream;
    .end local v4    # "loStream":Lcom/google/android/videos/streams/MediaStream;
    :cond_9
    iput-boolean v5, p0, Lcom/google/android/videos/streams/LegacyStreamSelection;->supportsQualityToggle:Z

    .line 69
    iput-boolean v3, p0, Lcom/google/android/videos/streams/LegacyStreamSelection;->isHiHd:Z

    .line 70
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 82
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[hi[0]="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/streams/LegacyStreamSelection;->hi:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "lo[0]="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/streams/LegacyStreamSelection;->lo:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "supportsQualityToggle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/videos/streams/LegacyStreamSelection;->supportsQualityToggle:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
