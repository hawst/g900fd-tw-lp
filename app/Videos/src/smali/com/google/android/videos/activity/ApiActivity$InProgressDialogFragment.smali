.class public Lcom/google/android/videos/activity/ApiActivity$InProgressDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "ApiActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/activity/ApiActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "InProgressDialogFragment"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 356
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static dismiss(Landroid/support/v4/app/FragmentActivity;)V
    .locals 4
    .param p0, "activity"    # Landroid/support/v4/app/FragmentActivity;

    .prologue
    .line 366
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "InProgressDialogFragment"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 367
    .local v1, "fragment":Landroid/support/v4/app/Fragment;
    instance-of v2, v1, Landroid/support/v4/app/DialogFragment;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 368
    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 369
    .local v0, "dialogFragment":Landroid/support/v4/app/DialogFragment;
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    .line 371
    .end local v0    # "dialogFragment":Landroid/support/v4/app/DialogFragment;
    :cond_0
    return-void
.end method

.method public static showInstance(Lcom/google/android/videos/activity/ApiActivity;)V
    .locals 3
    .param p0, "activity"    # Lcom/google/android/videos/activity/ApiActivity;

    .prologue
    .line 361
    new-instance v0, Lcom/google/android/videos/activity/ApiActivity$InProgressDialogFragment;

    invoke-direct {v0}, Lcom/google/android/videos/activity/ApiActivity$InProgressDialogFragment;-><init>()V

    .line 362
    .local v0, "instance":Lcom/google/android/videos/activity/ApiActivity$InProgressDialogFragment;
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ApiActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "InProgressDialogFragment"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/activity/ApiActivity$InProgressDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 363
    return-void
.end method


# virtual methods
.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 385
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 386
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ApiActivity$InProgressDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/activity/ApiActivity;

    .line 387
    .local v0, "activity":Lcom/google/android/videos/activity/ApiActivity;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "User canceled convertion of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/google/android/videos/activity/ApiActivity;->eidrId:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/videos/activity/ApiActivity;->access$000(Lcom/google/android/videos/activity/ApiActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 388
    # getter for: Lcom/google/android/videos/activity/ApiActivity;->eidrIdConverterHelper:Lcom/google/android/videos/ui/EidrIdConverterHelper;
    invoke-static {v0}, Lcom/google/android/videos/activity/ApiActivity;->access$100(Lcom/google/android/videos/activity/ApiActivity;)Lcom/google/android/videos/ui/EidrIdConverterHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/videos/ui/EidrIdConverterHelper;->cancel()V

    .line 389
    const/4 v1, 0x0

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    # invokes: Lcom/google/android/videos/activity/ApiActivity;->finishWithFailure(ILjava/lang/Throwable;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/videos/activity/ApiActivity;->access$200(Lcom/google/android/videos/activity/ApiActivity;ILjava/lang/Throwable;)V

    .line 390
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 375
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ApiActivity$InProgressDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 376
    .local v0, "activity":Landroid/app/Activity;
    new-instance v1, Landroid/app/Dialog;

    invoke-direct {v1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 377
    .local v1, "dialog":Landroid/app/Dialog;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 378
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const v3, 0x7f0a00c7

    invoke-virtual {v2, v3}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 379
    new-instance v2, Landroid/widget/ProgressBar;

    invoke-virtual {v1}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 380
    return-object v1
.end method
