.class public Lcom/google/android/videos/remote/VideoMediaRouteDialogFactory;
.super Landroid/support/v7/app/MediaRouteDialogFactory;
.source "VideoMediaRouteDialogFactory.java"


# static fields
.field private static final factory:Lcom/google/android/videos/remote/VideoMediaRouteDialogFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    new-instance v0, Lcom/google/android/videos/remote/VideoMediaRouteDialogFactory;

    invoke-direct {v0}, Lcom/google/android/videos/remote/VideoMediaRouteDialogFactory;-><init>()V

    sput-object v0, Lcom/google/android/videos/remote/VideoMediaRouteDialogFactory;->factory:Lcom/google/android/videos/remote/VideoMediaRouteDialogFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/support/v7/app/MediaRouteDialogFactory;-><init>()V

    return-void
.end method

.method public static getDefault()Lcom/google/android/videos/remote/VideoMediaRouteDialogFactory;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/google/android/videos/remote/VideoMediaRouteDialogFactory;->factory:Lcom/google/android/videos/remote/VideoMediaRouteDialogFactory;

    return-object v0
.end method


# virtual methods
.method public onCreateControllerDialogFragment()Landroid/support/v7/app/MediaRouteControllerDialogFragment;
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialogFragment;

    invoke-direct {v0}, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialogFragment;-><init>()V

    return-object v0
.end method
