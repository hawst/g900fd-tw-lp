.class Lcom/google/android/videos/ui/StorageHeaderFlow;
.super Lcom/google/android/videos/flow/SingleViewFlow;
.source "StorageHeaderFlow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/StorageHeaderFlow$StorageHeaderViewHolder;
    }
.end annotation


# instance fields
.field private downloadedBytes:J

.field private requiredBytes:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    const v0, 0x7f04006b

    invoke-direct {p0, v0}, Lcom/google/android/videos/flow/SingleViewFlow;-><init>(I)V

    .line 20
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/ui/StorageHeaderFlow;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/videos/ui/StorageHeaderFlow;

    .prologue
    .line 17
    iget-wide v0, p0, Lcom/google/android/videos/ui/StorageHeaderFlow;->downloadedBytes:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/ui/StorageHeaderFlow;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/videos/ui/StorageHeaderFlow;

    .prologue
    .line 17
    iget-wide v0, p0, Lcom/google/android/videos/ui/StorageHeaderFlow;->requiredBytes:J

    return-wide v0
.end method


# virtual methods
.method public createViewHolder(ILandroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 4
    .param p1, "viewType"    # I
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 24
    new-instance v0, Lcom/google/android/videos/ui/StorageHeaderFlow$StorageHeaderViewHolder;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04006b

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/videos/ui/StorageHeaderFlow$StorageHeaderViewHolder;-><init>(Lcom/google/android/videos/ui/StorageHeaderFlow;Landroid/view/View;)V

    return-object v0
.end method

.method public updateVideosStorage(JJ)V
    .locals 3
    .param p1, "requiredBytes"    # J
    .param p3, "downloadedBytes"    # J

    .prologue
    .line 33
    iget-wide v0, p0, Lcom/google/android/videos/ui/StorageHeaderFlow;->requiredBytes:J

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/videos/ui/StorageHeaderFlow;->downloadedBytes:J

    cmp-long v0, p3, v0

    if-eqz v0, :cond_1

    .line 34
    :cond_0
    iput-wide p1, p0, Lcom/google/android/videos/ui/StorageHeaderFlow;->requiredBytes:J

    .line 35
    iput-wide p3, p0, Lcom/google/android/videos/ui/StorageHeaderFlow;->downloadedBytes:J

    .line 36
    invoke-virtual {p0}, Lcom/google/android/videos/ui/StorageHeaderFlow;->notifyItemChanged()V

    .line 38
    :cond_1
    return-void
.end method
