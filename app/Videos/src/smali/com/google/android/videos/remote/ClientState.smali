.class public Lcom/google/android/videos/remote/ClientState;
.super Ljava/lang/Object;
.source "ClientState.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/remote/ClientState$1;,
        Lcom/google/android/videos/remote/ClientState$Builder;
    }
.end annotation


# instance fields
.field public final obfuscatedAccount:Ljava/lang/String;

.field public final seasonId:Ljava/lang/String;

.field public final showId:Ljava/lang/String;

.field public final videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/net/Uri;)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const/4 v1, 0x1

    .line 47
    .local v1, "version":I
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 48
    .local v0, "path":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 49
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 52
    :cond_0
    const-string v2, "video_id"

    invoke-virtual {p1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/remote/ClientState;->videoId:Ljava/lang/String;

    .line 53
    const-string v2, "season_id"

    invoke-virtual {p1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/remote/ClientState;->seasonId:Ljava/lang/String;

    .line 54
    const-string v2, "account"

    invoke-virtual {p1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/remote/ClientState;->obfuscatedAccount:Ljava/lang/String;

    .line 56
    const/4 v2, 0x1

    if-le v1, v2, :cond_1

    const-string v2, "show_id"

    invoke-virtual {p1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_0
    iput-object v2, p0, Lcom/google/android/videos/remote/ClientState;->showId:Ljava/lang/String;

    .line 57
    return-void

    .line 56
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "seasonId"    # Ljava/lang/String;
    .param p3, "showId"    # Ljava/lang/String;
    .param p4, "obfuscatedAccount"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/google/android/videos/remote/ClientState;->videoId:Ljava/lang/String;

    .line 39
    iput-object p2, p0, Lcom/google/android/videos/remote/ClientState;->seasonId:Ljava/lang/String;

    .line 40
    iput-object p3, p0, Lcom/google/android/videos/remote/ClientState;->showId:Ljava/lang/String;

    .line 41
    iput-object p4, p0, Lcom/google/android/videos/remote/ClientState;->obfuscatedAccount:Ljava/lang/String;

    .line 42
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/remote/ClientState$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Ljava/lang/String;
    .param p4, "x3"    # Ljava/lang/String;
    .param p5, "x4"    # Lcom/google/android/videos/remote/ClientState$1;

    .prologue
    .line 18
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/videos/remote/ClientState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-static {p0}, Lcom/google/android/videos/remote/ClientState;->obfuscate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static obfuscate(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "account"    # Ljava/lang/String;

    .prologue
    .line 97
    :try_start_0
    const-string v2, "SHA-1"

    invoke-static {v2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    .line 98
    .local v0, "digest":Ljava/security/MessageDigest;
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v2

    const/16 v3, 0xb

    invoke-static {v2, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 103
    .end local v0    # "digest":Ljava/security/MessageDigest;
    :goto_0
    return-object v2

    .line 100
    :catch_0
    move-exception v1

    .line 102
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    const-string v2, "Error while trying to get SHA-1 digest algorithm"

    invoke-static {v2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 103
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public accountMatches(Ljava/lang/String;)Z
    .locals 2
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 60
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/remote/ClientState;->obfuscatedAccount:Ljava/lang/String;

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/remote/ClientState;->obfuscatedAccount:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/videos/remote/ClientState;->obfuscate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMovie()Z
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/videos/remote/ClientState;->seasonId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ClientState ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/videos/remote/ClientState;->toUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toUri()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 73
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    .line 74
    .local v0, "builder":Landroid/net/Uri$Builder;
    const-string v1, "2"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 75
    iget-object v1, p0, Lcom/google/android/videos/remote/ClientState;->videoId:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 76
    const-string v1, "video_id"

    iget-object v2, p0, Lcom/google/android/videos/remote/ClientState;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 78
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/remote/ClientState;->seasonId:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 79
    const-string v1, "season_id"

    iget-object v2, p0, Lcom/google/android/videos/remote/ClientState;->seasonId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 81
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/remote/ClientState;->showId:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 82
    const-string v1, "show_id"

    iget-object v2, p0, Lcom/google/android/videos/remote/ClientState;->showId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 84
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/remote/ClientState;->obfuscatedAccount:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 85
    const-string v1, "account"

    iget-object v2, p0, Lcom/google/android/videos/remote/ClientState;->obfuscatedAccount:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 87
    :cond_3
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method
