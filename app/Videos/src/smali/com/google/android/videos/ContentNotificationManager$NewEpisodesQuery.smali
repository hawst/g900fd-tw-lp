.class interface abstract Lcom/google/android/videos/ContentNotificationManager$NewEpisodesQuery;
.super Ljava/lang/Object;
.source "ContentNotificationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ContentNotificationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "NewEpisodesQuery"
.end annotation


# static fields
.field public static final PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 229
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "account"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "video_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "shows_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "shows_title"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "season_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "shows_poster_uri"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "shows_banner_uri"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/videos/ContentNotificationManager$NewEpisodesQuery;->PROJECTION:[Ljava/lang/String;

    return-void
.end method
