.class Lcom/google/android/videos/tagging/SuggestionGridLayout$SwipeCallback;
.super Ljava/lang/Object;
.source "SuggestionGridLayout.java"

# interfaces
.implements Lcom/google/android/videos/tagging/SwipeHelper$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/SuggestionGridLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SwipeCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/tagging/SuggestionGridLayout;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/tagging/SuggestionGridLayout;)V
    .locals 0

    .prologue
    .line 490
    iput-object p1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/tagging/SuggestionGridLayout;Lcom/google/android/videos/tagging/SuggestionGridLayout$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/tagging/SuggestionGridLayout;
    .param p2, "x1"    # Lcom/google/android/videos/tagging/SuggestionGridLayout$1;

    .prologue
    .line 490
    invoke-direct {p0, p1}, Lcom/google/android/videos/tagging/SuggestionGridLayout$SwipeCallback;-><init>(Lcom/google/android/videos/tagging/SuggestionGridLayout;)V

    return-void
.end method


# virtual methods
.method public canChildBeDismissed(Landroid/view/View;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 532
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-nez v1, :cond_0

    .line 533
    const/4 v1, 0x0

    .line 537
    :goto_0
    return v1

    .line 536
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    # invokes: Lcom/google/android/videos/tagging/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;
    invoke-static {v1, p1}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->access$300(Lcom/google/android/videos/tagging/SuggestionGridLayout;Landroid/view/View;)Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;

    move-result-object v0

    .line 537
    .local v0, "item":Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;
    invoke-interface {v0}, Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;->getGridLayoutParams()Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->canDismiss:Z

    goto :goto_0
.end method

.method dragEnd(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 598
    iget-object v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    # invokes: Lcom/google/android/videos/tagging/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;
    invoke-static {v1, p1}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->access$300(Lcom/google/android/videos/tagging/SuggestionGridLayout;Landroid/view/View;)Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;

    move-result-object v0

    .line 599
    .local v0, "gi":Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;
    if-eqz v0, :cond_0

    .line 600
    iget-object v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    # getter for: Lcom/google/android/videos/tagging/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->access$400(Lcom/google/android/videos/tagging/SuggestionGridLayout;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0}, Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;->getViews()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 602
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    # setter for: Lcom/google/android/videos/tagging/SuggestionGridLayout;->mIsDragging:Z
    invoke-static {v1, v3}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->access$102(Lcom/google/android/videos/tagging/SuggestionGridLayout;Z)Z

    .line 603
    const/4 v1, 0x0

    invoke-virtual {p1, v3, v1}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 604
    iget-object v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v1}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->invalidate()V

    .line 605
    return-void
.end method

.method public getChildAtPosition(Landroid/view/MotionEvent;)Landroid/view/View;
    .locals 9
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x0

    const/high16 v8, 0x3f000000    # 0.5f

    .line 497
    iget-object v7, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    # getter for: Lcom/google/android/videos/tagging/SuggestionGridLayout;->mIsDragging:Z
    invoke-static {v7}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->access$100(Lcom/google/android/videos/tagging/SuggestionGridLayout;)Z

    move-result v7

    if-eqz v7, :cond_0

    move-object v3, v6

    .line 526
    :goto_0
    return-object v3

    .line 501
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    add-float/2addr v7, v8

    float-to-int v4, v7

    .line 502
    .local v4, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    add-float/2addr v7, v8

    float-to-int v5, v7

    .line 504
    .local v5, "y":I
    iget-object v7, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v7}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getChildCount()I

    move-result v0

    .line 505
    .local v0, "count":I
    add-int/lit8 v1, v0, -0x1

    .local v1, "i":I
    :goto_1
    if-ltz v1, :cond_3

    .line 506
    iget-object v7, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v7, v1}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 507
    .local v3, "v":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v7

    if-eqz v7, :cond_2

    .line 505
    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 510
    :cond_2
    iget-object v7, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    # getter for: Lcom/google/android/videos/tagging/SuggestionGridLayout;->mHitRect:Landroid/graphics/Rect;
    invoke-static {v7}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->access$200(Lcom/google/android/videos/tagging/SuggestionGridLayout;)Landroid/graphics/Rect;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 511
    iget-object v7, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    # getter for: Lcom/google/android/videos/tagging/SuggestionGridLayout;->mHitRect:Landroid/graphics/Rect;
    invoke-static {v7}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->access$200(Lcom/google/android/videos/tagging/SuggestionGridLayout;)Landroid/graphics/Rect;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 512
    iget-object v7, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    # invokes: Lcom/google/android/videos/tagging/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;
    invoke-static {v7, v3}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->access$300(Lcom/google/android/videos/tagging/SuggestionGridLayout;Landroid/view/View;)Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;

    move-result-object v2

    .line 513
    .local v2, "item":Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;
    if-eqz v2, :cond_1

    .line 517
    invoke-interface {v2}, Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;->getGridLayoutParams()Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;

    move-result-object v7

    iget-boolean v7, v7, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->canDrag:Z

    if-eqz v7, :cond_1

    .line 521
    instance-of v7, v2, Lcom/google/android/videos/tagging/SuggestionGridLayout$SimpleGridItem;

    if-eqz v7, :cond_1

    goto :goto_0

    .end local v2    # "item":Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;
    .end local v3    # "v":Landroid/view/View;
    :cond_3
    move-object v3, v6

    .line 526
    goto :goto_0
.end method

.method public onBeginDrag(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    .line 542
    iget-object v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v1}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 544
    iget-object v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    # invokes: Lcom/google/android/videos/tagging/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;
    invoke-static {v1, p1}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->access$300(Lcom/google/android/videos/tagging/SuggestionGridLayout;Landroid/view/View;)Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;

    move-result-object v0

    .line 547
    .local v0, "gridItem":Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;
    if-eqz v0, :cond_0

    .line 548
    iget-object v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    # getter for: Lcom/google/android/videos/tagging/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->access$400(Lcom/google/android/videos/tagging/SuggestionGridLayout;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0}, Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;->getViews()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 550
    :cond_0
    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 552
    iget-object v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    # setter for: Lcom/google/android/videos/tagging/SuggestionGridLayout;->mIsDragging:Z
    invoke-static {v1, v3}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->access$102(Lcom/google/android/videos/tagging/SuggestionGridLayout;Z)Z

    .line 553
    iget-object v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v1}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->invalidate()V

    .line 554
    return-void
.end method

.method public onChildDismissed(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 559
    const/4 v0, 0x0

    .line 561
    .local v0, "gridItem":Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;
    iget-object v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    # invokes: Lcom/google/android/videos/tagging/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;
    invoke-static {v1, p1}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->access$300(Lcom/google/android/videos/tagging/SuggestionGridLayout;Landroid/view/View;)Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;

    move-result-object v0

    .line 564
    if-eqz v0, :cond_1

    .line 565
    iget-object v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    # getter for: Lcom/google/android/videos/tagging/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->access$400(Lcom/google/android/videos/tagging/SuggestionGridLayout;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0}, Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;->getViews()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 566
    invoke-interface {v0}, Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;->getGridLayoutParams()Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->removeOnDismiss:Z

    if-eqz v1, :cond_0

    .line 567
    invoke-interface {v0, p1}, Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;->removeView(Landroid/view/View;)V

    .line 568
    iget-object v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    # invokes: Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V
    invoke-static {v1, p1}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->access$501(Lcom/google/android/videos/tagging/SuggestionGridLayout;Landroid/view/View;)V

    .line 572
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    # getter for: Lcom/google/android/videos/tagging/SuggestionGridLayout;->mOnDismissListener:Lcom/google/android/videos/tagging/SuggestionGridLayout$OnDismissListener;
    invoke-static {v1}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->access$600(Lcom/google/android/videos/tagging/SuggestionGridLayout;)Lcom/google/android/videos/tagging/SuggestionGridLayout$OnDismissListener;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 573
    iget-object v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    # getter for: Lcom/google/android/videos/tagging/SuggestionGridLayout;->mOnDismissListener:Lcom/google/android/videos/tagging/SuggestionGridLayout$OnDismissListener;
    invoke-static {v1}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->access$600(Lcom/google/android/videos/tagging/SuggestionGridLayout;)Lcom/google/android/videos/tagging/SuggestionGridLayout$OnDismissListener;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/google/android/videos/tagging/SuggestionGridLayout$OnDismissListener;->onViewDismissed(Landroid/view/View;)V

    .line 577
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/videos/tagging/SuggestionGridLayout;->mIsDragging:Z
    invoke-static {v1, v2}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->access$102(Lcom/google/android/videos/tagging/SuggestionGridLayout;Z)Z

    .line 581
    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;->getViews()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 582
    iget-object v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    # getter for: Lcom/google/android/videos/tagging/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->access$700(Lcom/google/android/videos/tagging/SuggestionGridLayout;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 585
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v1}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->invalidate()V

    .line 586
    return-void
.end method

.method public onDragCancelled(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 590
    return-void
.end method

.method public onSnapBackCompleted(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 594
    invoke-virtual {p0, p1}, Lcom/google/android/videos/tagging/SuggestionGridLayout$SwipeCallback;->dragEnd(Landroid/view/View;)V

    .line 595
    return-void
.end method
