.class Lcom/google/android/videos/ui/RestrictionsHelper$1;
.super Ljava/lang/Object;
.source "RestrictionsHelper.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/ui/RestrictionsHelper;->syncUserConfiguration(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/ui/RestrictionsHelper;


# direct methods
.method constructor <init>(Lcom/google/android/videos/ui/RestrictionsHelper;)V
    .locals 0

    .prologue
    .line 256
    iput-object p1, p0, Lcom/google/android/videos/ui/RestrictionsHelper$1;->this$0:Lcom/google/android/videos/ui/RestrictionsHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 256
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/RestrictionsHelper$1;->onError(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public onError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/android/videos/ui/RestrictionsHelper$1;->this$0:Lcom/google/android/videos/ui/RestrictionsHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/ui/RestrictionsHelper;->onError(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 267
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 256
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/lang/Void;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/RestrictionsHelper$1;->onResponse(Ljava/lang/String;Ljava/lang/Void;)V

    return-void
.end method

.method public onResponse(Ljava/lang/String;Ljava/lang/Void;)V
    .locals 3
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "response"    # Ljava/lang/Void;

    .prologue
    .line 259
    iget-object v1, p0, Lcom/google/android/videos/ui/RestrictionsHelper$1;->this$0:Lcom/google/android/videos/ui/RestrictionsHelper;

    # getter for: Lcom/google/android/videos/ui/RestrictionsHelper;->activity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/google/android/videos/ui/RestrictionsHelper;->access$000(Lcom/google/android/videos/ui/RestrictionsHelper;)Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/ui/RestrictionsHelper$1;->this$0:Lcom/google/android/videos/ui/RestrictionsHelper;

    invoke-static {v1, v2}, Lcom/google/android/videos/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/ActivityCallback;

    move-result-object v0

    .line 261
    .local v0, "callback":Lcom/google/android/videos/async/ActivityCallback;, "Lcom/google/android/videos/async/ActivityCallback<Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/UserConfiguration;>;"
    iget-object v1, p0, Lcom/google/android/videos/ui/RestrictionsHelper$1;->this$0:Lcom/google/android/videos/ui/RestrictionsHelper;

    # getter for: Lcom/google/android/videos/ui/RestrictionsHelper;->videosGlobals:Lcom/google/android/videos/VideosGlobals;
    invoke-static {v1}, Lcom/google/android/videos/ui/RestrictionsHelper;->access$100(Lcom/google/android/videos/ui/RestrictionsHelper;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/google/android/videos/store/ConfigurationStore;->requestUserConfiguration(Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V

    .line 262
    return-void
.end method
