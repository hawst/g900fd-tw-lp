.class public Lcom/google/android/videos/cast/v2/CastV2RemoteControl;
.super Lcom/google/android/videos/remote/RemoteControl;
.source "CastV2RemoteControl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastConnectionFailedListener;,
        Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastConnectionCallbacks;,
        Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastListener;,
        Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;
    }
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private final apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

.field private final castDevice:Lcom/google/android/gms/cast/CastDevice;

.field private final castMediaRouter:Lcom/google/android/videos/cast/CastMediaRouter;

.field private final connectionCallbacks:Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastConnectionCallbacks;

.field private final connectionFailedListener:Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastConnectionFailedListener;

.field private flingInProgress:Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;

.field private final mpdUrlGetRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/MpdUrlGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;",
            ">;"
        }
    .end annotation
.end field

.field private pinned:Z

.field private final playMoviesChannel:Lcom/google/android/videos/cast/v2/PlayMoviesChannel;

.field private final preferences:Landroid/content/SharedPreferences;

.field private final receiverAppId:Ljava/lang/String;

.field private final recommendationsRequestFactory:Lcom/google/android/videos/api/RecommendationsRequest$Factory;

.field private final recommendationsRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/RecommendationsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final remoteMediaPlayerChannel:Lcom/google/android/gms/cast/RemoteMediaPlayer;

.field private final routeInfo:Landroid/support/v7/media/MediaRouter$RouteInfo;

.field private selectedSubtitleTrack:Lcom/google/android/videos/subtitles/SubtitleTrack;

.field private state:I

.field private subtitleTracks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;"
        }
    .end annotation
.end field

.field private subtitleVideoId:Ljava/lang/String;

.field private videoIdToFling:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/support/v7/media/MediaRouter$RouteInfo;Lcom/google/android/videos/cast/CastMediaRouter;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/api/RecommendationsRequest$Factory;Lcom/google/android/videos/async/Requester;Landroid/content/SharedPreferences;Z)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "receiverAppId"    # Ljava/lang/String;
    .param p3, "routeInfo"    # Landroid/support/v7/media/MediaRouter$RouteInfo;
    .param p4, "castMediaRouter"    # Lcom/google/android/videos/cast/CastMediaRouter;
    .param p7, "recommendationsRequestFactory"    # Lcom/google/android/videos/api/RecommendationsRequest$Factory;
    .param p9, "preferences"    # Landroid/content/SharedPreferences;
    .param p10, "verboseLogging"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Landroid/support/v7/media/MediaRouter$RouteInfo;",
            "Lcom/google/android/videos/cast/CastMediaRouter;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/MpdUrlGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;",
            ">;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/RecommendationsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;",
            ">;",
            "Lcom/google/android/videos/api/RecommendationsRequest$Factory;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/RobotTokenRequest;",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/SharedPreferences;",
            "Z)V"
        }
    .end annotation

    .prologue
    .local p5, "mpdUrlGetRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/MpdUrlGetRequest;Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;>;"
    .local p6, "recommendationsRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/RecommendationsRequest;Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;>;"
    .local p8, "robotTokenRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/RobotTokenRequest;Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 134
    invoke-direct {p0, p1}, Lcom/google/android/videos/remote/RemoteControl;-><init>(Landroid/content/Context;)V

    .line 135
    iput-object p3, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->routeInfo:Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 136
    invoke-virtual {p3}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/cast/CastDevice;->getFromBundle(Landroid/os/Bundle;)Lcom/google/android/gms/cast/CastDevice;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->castDevice:Lcom/google/android/gms/cast/CastDevice;

    .line 137
    iput-object p4, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->castMediaRouter:Lcom/google/android/videos/cast/CastMediaRouter;

    .line 138
    iput-object p5, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->mpdUrlGetRequester:Lcom/google/android/videos/async/Requester;

    .line 139
    iput-object p6, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->recommendationsRequester:Lcom/google/android/videos/async/Requester;

    .line 140
    iput-object p7, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->recommendationsRequestFactory:Lcom/google/android/videos/api/RecommendationsRequest$Factory;

    .line 141
    iput-object p9, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->preferences:Landroid/content/SharedPreferences;

    .line 142
    iput-object p2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->receiverAppId:Ljava/lang/String;

    .line 143
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->state:I

    .line 145
    new-instance v1, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastConnectionCallbacks;

    invoke-direct {v1, p0, v3}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastConnectionCallbacks;-><init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;)V

    iput-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->connectionCallbacks:Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastConnectionCallbacks;

    .line 146
    new-instance v1, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastConnectionFailedListener;

    invoke-direct {v1, p0, v3}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastConnectionFailedListener;-><init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;)V

    iput-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->connectionFailedListener:Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastConnectionFailedListener;

    .line 148
    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->castDevice:Lcom/google/android/gms/cast/CastDevice;

    new-instance v2, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastListener;

    invoke-direct {v2, p0, v3}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastListener;-><init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;)V

    invoke-static {v1, v2}, Lcom/google/android/gms/cast/Cast$CastOptions;->builder(Lcom/google/android/gms/cast/CastDevice;Lcom/google/android/gms/cast/Cast$Listener;)Lcom/google/android/gms/cast/Cast$CastOptions$Builder;

    move-result-object v1

    invoke-virtual {v1, p10}, Lcom/google/android/gms/cast/Cast$CastOptions$Builder;->setVerboseLoggingEnabled(Z)Lcom/google/android/gms/cast/Cast$CastOptions$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/cast/Cast$CastOptions$Builder;->build()Lcom/google/android/gms/cast/Cast$CastOptions;

    move-result-object v0

    .line 151
    .local v0, "apiOptions":Lcom/google/android/gms/cast/Cast$CastOptions;
    new-instance v1, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    invoke-virtual {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;-><init>(Landroid/content/Context;)V

    sget-object v2, Lcom/google/android/gms/cast/Cast;->API:Lcom/google/android/gms/common/api/Api;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addApi(Lcom/google/android/gms/common/api/Api;Lcom/google/android/gms/common/api/Api$ApiOptions$HasOptions;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->connectionCallbacks:Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastConnectionCallbacks;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addConnectionCallbacks(Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->connectionFailedListener:Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastConnectionFailedListener;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addOnConnectionFailedListener(Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->build()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 156
    new-instance v1, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;

    invoke-direct {v1, p0, p10, p8}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;-><init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;ZLcom/google/android/videos/async/Requester;)V

    iput-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->playMoviesChannel:Lcom/google/android/videos/cast/v2/PlayMoviesChannel;

    .line 236
    new-instance v1, Lcom/google/android/gms/cast/RemoteMediaPlayer;

    invoke-direct {v1}, Lcom/google/android/gms/cast/RemoteMediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->remoteMediaPlayerChannel:Lcom/google/android/gms/cast/RemoteMediaPlayer;

    .line 238
    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->remoteMediaPlayerChannel:Lcom/google/android/gms/cast/RemoteMediaPlayer;

    new-instance v2, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$2;

    invoke-direct {v2, p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$2;-><init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/cast/RemoteMediaPlayer;->setOnStatusUpdatedListener(Lcom/google/android/gms/cast/RemoteMediaPlayer$OnStatusUpdatedListener;)V

    .line 249
    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->remoteMediaPlayerChannel:Lcom/google/android/gms/cast/RemoteMediaPlayer;

    new-instance v2, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$3;

    invoke-direct {v2, p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$3;-><init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/cast/RemoteMediaPlayer;->setOnMetadataUpdatedListener(Lcom/google/android/gms/cast/RemoteMediaPlayer$OnMetadataUpdatedListener;)V

    .line 257
    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->updateData()V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Ljava/util/List;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;
    .param p1, "x1"    # Ljava/util/List;
    .param p2, "x2"    # I

    .prologue
    .line 64
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->notifyAudioTracksChanged(Ljava/util/List;I)V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->onConnected()V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->unfreezePlayerTime()V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Ljava/lang/String;Lcom/google/android/gms/common/api/Status;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/google/android/gms/common/api/Status;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->onSendCommandFailure(Ljava/lang/String;Lcom/google/android/gms/common/api/Status;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Landroid/support/v7/media/MediaRouter$RouteInfo;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->routeInfo:Landroid/support/v7/media/MediaRouter$RouteInfo;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->onWaitForStatus(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->clearSessionData()V

    return-void
.end method

.method static synthetic access$1800(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;
    .param p1, "x1"    # Z

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->disconnect(Z)V

    return-void
.end method

.method static synthetic access$1900(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->receiverAppId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->isPlaying(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2100(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;[Lcom/google/wireless/android/video/magma/proto/AssetResource;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;
    .param p1, "x1"    # [Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->sendRecommendations([Lcom/google/wireless/android/video/magma/proto/AssetResource;)V

    return-void
.end method

.method static synthetic access$2200(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->flingInProgress:Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;)Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;
    .param p1, "x1"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->flingInProgress:Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;

    return-object p1
.end method

.method static synthetic access$2300(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Lcom/google/android/videos/async/Requester;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->mpdUrlGetRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Lcom/google/android/gms/cast/RemoteMediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->remoteMediaPlayerChannel:Lcom/google/android/gms/cast/RemoteMediaPlayer;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Ljava/lang/String;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->seekTo(Ljava/lang/String;II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2900(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Ljava/lang/String;Lcom/google/android/videos/remote/RemoteVideoInfo;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/google/android/videos/remote/RemoteVideoInfo;
    .param p3, "x3"    # Z

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->requestRecommendations(Ljava/lang/String;Lcom/google/android/videos/remote/RemoteVideoInfo;Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    .prologue
    .line 64
    iget v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->state:I

    return v0
.end method

.method static synthetic access$3000(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;
    .param p1, "x1"    # I

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->setState(I)V

    return-void
.end method

.method static synthetic access$3100(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->isConnectionClosed()Z

    move-result v0

    return v0
.end method

.method static synthetic access$3200(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->onGmsConnected()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->videoIdToFling:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->account:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->pinned:Z

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Lcom/google/android/gms/common/api/GoogleApiClient;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    return-object v0
.end method

.method static synthetic access$802(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->subtitleTracks:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$902(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Lcom/google/android/videos/subtitles/SubtitleTrack;)Lcom/google/android/videos/subtitles/SubtitleTrack;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;
    .param p1, "x1"    # Lcom/google/android/videos/subtitles/SubtitleTrack;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->selectedSubtitleTrack:Lcom/google/android/videos/subtitles/SubtitleTrack;

    return-object p1
.end method

.method private checkCommandStatus(Ljava/lang/String;Lcom/google/android/gms/common/api/PendingResult;)V
    .locals 1
    .param p1, "command"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/gms/common/api/Result;",
            ">(",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/api/PendingResult",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 840
    .local p2, "pendingResult":Lcom/google/android/gms/common/api/PendingResult;, "Lcom/google/android/gms/common/api/PendingResult<TT;>;"
    new-instance v0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$8;

    invoke-direct {v0, p0, p1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$8;-><init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Ljava/lang/String;)V

    invoke-interface {p2, v0}, Lcom/google/android/gms/common/api/PendingResult;->setResultCallback(Lcom/google/android/gms/common/api/ResultCallback;)V

    .line 848
    return-void
.end method

.method private clearSessionData()V
    .locals 2

    .prologue
    .line 754
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "castv2_route_id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "castv2_session_id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 758
    const-string v0, "sessionRestore removed data: "

    invoke-static {v0}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 759
    return-void
.end method

.method private decodeMediaStatus(Lorg/json/JSONObject;)Lcom/google/android/videos/remote/RemoteVideoInfo;
    .locals 13
    .param p1, "contentInfo"    # Lorg/json/JSONObject;

    .prologue
    const/4 v10, 0x0

    .line 789
    if-nez p1, :cond_0

    move-object v0, v10

    .line 809
    :goto_0
    return-object v0

    .line 795
    :cond_0
    :try_start_0
    const-string v11, "video_info"

    invoke-virtual {p1, v11}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v9

    .line 796
    .local v9, "jsonVideoInfo":Lorg/json/JSONObject;
    const-string v11, "video_title"

    invoke-virtual {v9, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 797
    .local v2, "videoTitle":Ljava/lang/String;
    const-string v11, "show_title"

    invoke-virtual {v9, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 798
    .local v3, "showTitle":Ljava/lang/String;
    const-string v11, "video_id"

    invoke-virtual {v9, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 799
    .local v1, "videoId":Ljava/lang/String;
    const-string v11, "is_trailer"

    const/4 v12, 0x0

    invoke-virtual {v9, v11, v12}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 800
    .local v4, "isTrailer":Z
    const-string v11, "duration"

    invoke-virtual {v9, v11}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 801
    .local v5, "durationMillis":I
    const-string v11, "opaque"

    invoke-virtual {v9, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 802
    .local v6, "opaqueString":Ljava/lang/String;
    const-string v11, "preferred_language"

    invoke-virtual {v9, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 803
    .local v7, "preferredLanguage":Ljava/lang/String;
    new-instance v0, Lcom/google/android/videos/remote/RemoteVideoInfo;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/remote/RemoteVideoInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .local v0, "videoInfo":Lcom/google/android/videos/remote/RemoteVideoInfo;
    goto :goto_0

    .line 805
    .end local v0    # "videoInfo":Lcom/google/android/videos/remote/RemoteVideoInfo;
    .end local v1    # "videoId":Ljava/lang/String;
    .end local v2    # "videoTitle":Ljava/lang/String;
    .end local v3    # "showTitle":Ljava/lang/String;
    .end local v4    # "isTrailer":Z
    .end local v5    # "durationMillis":I
    .end local v6    # "opaqueString":Ljava/lang/String;
    .end local v7    # "preferredLanguage":Ljava/lang/String;
    .end local v9    # "jsonVideoInfo":Lorg/json/JSONObject;
    :catch_0
    move-exception v8

    .local v8, "e":Lorg/json/JSONException;
    move-object v0, v10

    .line 806
    goto :goto_0
.end method

.method private disconnect(Z)V
    .locals 3
    .param p1, "stopRemoteApplication"    # Z

    .prologue
    .line 691
    invoke-direct {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->isConnectionClosed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 727
    :goto_0
    return-void

    .line 695
    :cond_0
    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->setState(I)V

    .line 696
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Disconnecting stopRemoteApplication = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p1, :cond_2

    const-string v1, "true"

    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 697
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->flingInProgress:Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;

    .line 699
    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v1}, Lcom/google/android/gms/common/api/GoogleApiClient;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 700
    if-eqz p1, :cond_3

    .line 702
    sget-object v1, Lcom/google/android/gms/cast/Cast;->CastApi:Lcom/google/android/gms/cast/Cast$CastApi;

    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v1, v2}, Lcom/google/android/gms/cast/Cast$CastApi;->stopApplication(Lcom/google/android/gms/common/api/GoogleApiClient;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v0

    .line 703
    .local v0, "pendingResult":Lcom/google/android/gms/common/api/PendingResult;, "Lcom/google/android/gms/common/api/PendingResult<Lcom/google/android/gms/common/api/Status;>;"
    new-instance v1, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$7;

    invoke-direct {v1, p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$7;-><init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/PendingResult;->setResultCallback(Lcom/google/android/gms/common/api/ResultCallback;)V

    .line 716
    invoke-direct {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->removeCastChannelCallbacks()V

    .line 726
    .end local v0    # "pendingResult":Lcom/google/android/gms/common/api/PendingResult;, "Lcom/google/android/gms/common/api/PendingResult<Lcom/google/android/gms/common/api/Status;>;"
    :cond_1
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->onDisconnected()V

    goto :goto_0

    .line 696
    :cond_2
    const-string v1, "false"

    goto :goto_1

    .line 719
    :cond_3
    sget-object v1, Lcom/google/android/gms/cast/Cast;->CastApi:Lcom/google/android/gms/cast/Cast$CastApi;

    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v1, v2}, Lcom/google/android/gms/cast/Cast$CastApi;->leaveApplication(Lcom/google/android/gms/common/api/GoogleApiClient;)Lcom/google/android/gms/common/api/PendingResult;

    .line 720
    invoke-direct {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->removeCastChannelCallbacks()V

    .line 721
    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v1}, Lcom/google/android/gms/common/api/GoogleApiClient;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 722
    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v1}, Lcom/google/android/gms/common/api/GoogleApiClient;->disconnect()V

    goto :goto_2
.end method

.method private encodeContentInfo(Lcom/google/android/videos/remote/RemoteVideoInfo;)Lorg/json/JSONObject;
    .locals 5
    .param p1, "videoInfo"    # Lcom/google/android/videos/remote/RemoteVideoInfo;

    .prologue
    .line 766
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 768
    .local v0, "contentInfo":Lorg/json/JSONObject;
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 769
    .local v2, "jsonVideoInfo":Lorg/json/JSONObject;
    const-string v3, "video_title"

    iget-object v4, p1, Lcom/google/android/videos/remote/RemoteVideoInfo;->videoTitle:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 770
    const-string v3, "show_title"

    iget-object v4, p1, Lcom/google/android/videos/remote/RemoteVideoInfo;->showTitle:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 771
    const-string v3, "video_id"

    iget-object v4, p1, Lcom/google/android/videos/remote/RemoteVideoInfo;->videoId:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 772
    const-string v3, "is_trailer"

    iget-boolean v4, p1, Lcom/google/android/videos/remote/RemoteVideoInfo;->isTrailer:Z

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 773
    const-string v3, "duration"

    iget v4, p1, Lcom/google/android/videos/remote/RemoteVideoInfo;->durationMillis:I

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 774
    const-string v3, "opaque"

    iget-object v4, p1, Lcom/google/android/videos/remote/RemoteVideoInfo;->opaqueString:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 775
    const-string v3, "preferred_language"

    iget-object v4, p1, Lcom/google/android/videos/remote/RemoteVideoInfo;->preferredLanguage:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 776
    const-string v3, "video_info"

    invoke-virtual {v0, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 780
    .end local v2    # "jsonVideoInfo":Lorg/json/JSONObject;
    :goto_0
    return-object v0

    .line 777
    :catch_0
    move-exception v1

    .line 778
    .local v1, "e":Lorg/json/JSONException;
    const-string v3, "Unexpected JSON exception:"

    invoke-static {v3, v1}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static getStateName(I)Ljava/lang/String;
    .locals 1
    .param p0, "state"    # I

    .prologue
    .line 536
    packed-switch p0, :pswitch_data_0

    .line 550
    const-string v0, "Unknown"

    :goto_0
    return-object v0

    .line 538
    :pswitch_0
    const-string v0, "Constructed"

    goto :goto_0

    .line 540
    :pswitch_1
    const-string v0, "Connecting"

    goto :goto_0

    .line 542
    :pswitch_2
    const-string v0, "WaitingForStatus"

    goto :goto_0

    .line 544
    :pswitch_3
    const-string v0, "Connected"

    goto :goto_0

    .line 546
    :pswitch_4
    const-string v0, "Disconnecting"

    goto :goto_0

    .line 548
    :pswitch_5
    const-string v0, "Disconnected"

    goto :goto_0

    .line 536
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private isConnectionClosed()Z
    .locals 2

    .prologue
    .line 523
    iget v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->state:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->state:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPlaying(Ljava/lang/String;)Z
    .locals 3
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 872
    invoke-virtual {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->getVideoInfo()Lcom/google/android/videos/remote/RemoteVideoInfo;

    move-result-object v0

    .line 873
    .local v0, "videoInfo":Lcom/google/android/videos/remote/RemoteVideoInfo;
    iget v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->state:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/videos/remote/RemoteVideoInfo;->videoId:Ljava/lang/String;

    invoke-static {v1, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isReadyToCall()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 421
    iget v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->state:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    .line 422
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "not safe to call while in state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->state:I

    invoke-static {v2}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->getStateName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 431
    :cond_0
    :goto_0
    return v0

    .line 425
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v1}, Lcom/google/android/gms/common/api/GoogleApiClient;->isConnected()Z

    move-result v1

    if-nez v1, :cond_2

    .line 428
    const-string v1, "GoogleApiClient is not connected, reconnecting should be in progress"

    invoke-static {v1}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 431
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->remoteMediaPlayerChannel:Lcom/google/android/gms/cast/RemoteMediaPlayer;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/RemoteMediaPlayer;->getMediaStatus()Lcom/google/android/gms/cast/MediaStatus;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private onConnected()V
    .locals 2

    .prologue
    .line 647
    iget v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->state:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 648
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onConnected not in connecting state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->state:I

    invoke-static {v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->getStateName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 658
    :cond_0
    :goto_0
    return-void

    .line 652
    :cond_1
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->setState(I)V

    .line 654
    invoke-virtual {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->clearError()V

    .line 655
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->flingInProgress:Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;

    if-eqz v0, :cond_0

    .line 656
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->flingInProgress:Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;

    invoke-virtual {v0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->onRetry()V

    goto :goto_0
.end method

.method private onConnectionError(I)V
    .locals 2
    .param p1, "error"    # I

    .prologue
    .line 675
    iget v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->state:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->state:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 676
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onConnectionError("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") called while in state "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->state:I

    invoke-static {v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->getStateName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 681
    :goto_0
    return-void

    .line 680
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->onError(I)V

    goto :goto_0
.end method

.method private onGmsConnected()V
    .locals 5

    .prologue
    .line 558
    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->preferences:Landroid/content/SharedPreferences;

    const-string v3, "castv2_session_id"

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 559
    .local v1, "sessionId":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sessionRestore sessionId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 560
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 562
    sget-object v2, Lcom/google/android/gms/cast/Cast;->CastApi:Lcom/google/android/gms/cast/Cast$CastApi;

    iget-object v3, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    iget-object v4, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->receiverAppId:Ljava/lang/String;

    invoke-interface {v2, v3, v4, v1}, Lcom/google/android/gms/cast/Cast$CastApi;->joinApplication(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v0

    .line 564
    .local v0, "pendingResult":Lcom/google/android/gms/common/api/PendingResult;, "Lcom/google/android/gms/common/api/PendingResult<Lcom/google/android/gms/cast/Cast$ApplicationConnectionResult;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sessionRestore join app "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->receiverAppId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v3}, Lcom/google/android/gms/common/api/GoogleApiClient;->isConnected()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 566
    new-instance v2, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$5;

    invoke-direct {v2, p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$5;-><init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)V

    invoke-interface {v0, v2}, Lcom/google/android/gms/common/api/PendingResult;->setResultCallback(Lcom/google/android/gms/common/api/ResultCallback;)V

    .line 608
    :goto_0
    return-void

    .line 584
    .end local v0    # "pendingResult":Lcom/google/android/gms/common/api/PendingResult;, "Lcom/google/android/gms/common/api/PendingResult<Lcom/google/android/gms/cast/Cast$ApplicationConnectionResult;>;"
    :cond_0
    sget-object v2, Lcom/google/android/gms/cast/Cast;->CastApi:Lcom/google/android/gms/cast/Cast$CastApi;

    iget-object v3, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    iget-object v4, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->receiverAppId:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Lcom/google/android/gms/cast/Cast$CastApi;->launchApplication(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v0

    .line 586
    .restart local v0    # "pendingResult":Lcom/google/android/gms/common/api/PendingResult;, "Lcom/google/android/gms/common/api/PendingResult<Lcom/google/android/gms/cast/Cast$ApplicationConnectionResult;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "start receiver app "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->receiverAppId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v3}, Lcom/google/android/gms/common/api/GoogleApiClient;->isConnected()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 587
    new-instance v2, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$6;

    invoke-direct {v2, p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$6;-><init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)V

    invoke-interface {v0, v2}, Lcom/google/android/gms/common/api/PendingResult;->setResultCallback(Lcom/google/android/gms/common/api/ResultCallback;)V

    goto :goto_0
.end method

.method private onSendCommandFailure(Ljava/lang/String;Lcom/google/android/gms/common/api/Status;)V
    .locals 2
    .param p1, "command"    # Ljava/lang/String;
    .param p2, "status"    # Lcom/google/android/gms/common/api/Status;

    .prologue
    .line 851
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Command: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " status "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " while in state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->state:I

    invoke-static {v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->getStateName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 854
    return-void
.end method

.method private onWaitForStatus(Ljava/lang/String;)V
    .locals 5
    .param p1, "sessionId"    # Ljava/lang/String;

    .prologue
    .line 616
    iget v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->state:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 617
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onConnected not in connecting state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->state:I

    invoke-static {v2}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->getStateName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 639
    :goto_0
    return-void

    .line 622
    :cond_0
    :try_start_0
    sget-object v1, Lcom/google/android/gms/cast/Cast;->CastApi:Lcom/google/android/gms/cast/Cast$CastApi;

    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    iget-object v3, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->remoteMediaPlayerChannel:Lcom/google/android/gms/cast/RemoteMediaPlayer;

    invoke-virtual {v3}, Lcom/google/android/gms/cast/RemoteMediaPlayer;->getNamespace()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->remoteMediaPlayerChannel:Lcom/google/android/gms/cast/RemoteMediaPlayer;

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/gms/cast/Cast$CastApi;->setMessageReceivedCallbacks(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;Lcom/google/android/gms/cast/Cast$MessageReceivedCallback;)V

    .line 624
    sget-object v1, Lcom/google/android/gms/cast/Cast;->CastApi:Lcom/google/android/gms/cast/Cast$CastApi;

    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-static {}, Lcom/google/android/videos/cast/v2/PlayMoviesChannel;->getNamespace()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->playMoviesChannel:Lcom/google/android/videos/cast/v2/PlayMoviesChannel;

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/gms/cast/Cast$CastApi;->setMessageReceivedCallbacks(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;Lcom/google/android/gms/cast/Cast$MessageReceivedCallback;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 632
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->setState(I)V

    .line 633
    invoke-direct {p0, p1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->saveSessionData(Ljava/lang/String;)V

    .line 635
    invoke-virtual {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->unfreezePlayerTime()V

    .line 637
    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->castMediaRouter:Lcom/google/android/videos/cast/CastMediaRouter;

    invoke-interface {v1}, Lcom/google/android/videos/cast/CastMediaRouter;->getUserDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->sendUserDisplayName(Ljava/lang/String;)V

    .line 638
    const-string v1, "requestStatus"

    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->remoteMediaPlayerChannel:Lcom/google/android/gms/cast/RemoteMediaPlayer;

    iget-object v3, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/cast/RemoteMediaPlayer;->requestStatus(Lcom/google/android/gms/common/api/GoogleApiClient;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->checkCommandStatus(Ljava/lang/String;Lcom/google/android/gms/common/api/PendingResult;)V

    goto :goto_0

    .line 626
    :catch_0
    move-exception v0

    .line 627
    .local v0, "e":Ljava/io/IOException;
    const/16 v1, -0x3e8

    invoke-direct {p0, v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->onConnectionError(I)V

    .line 628
    const-string v1, "Exception while registering callbacks"

    invoke-static {v1, v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private removeCastChannelCallbacks()V
    .locals 3

    .prologue
    .line 730
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 741
    :goto_0
    return-void

    .line 735
    :cond_0
    :try_start_0
    sget-object v0, Lcom/google/android/gms/cast/Cast;->CastApi:Lcom/google/android/gms/cast/Cast$CastApi;

    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-static {}, Lcom/google/android/videos/cast/v2/PlayMoviesChannel;->getNamespace()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/cast/Cast$CastApi;->removeMessageReceivedCallbacks(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;)V

    .line 736
    sget-object v0, Lcom/google/android/gms/cast/Cast;->CastApi:Lcom/google/android/gms/cast/Cast$CastApi;

    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->remoteMediaPlayerChannel:Lcom/google/android/gms/cast/RemoteMediaPlayer;

    invoke-virtual {v2}, Lcom/google/android/gms/cast/RemoteMediaPlayer;->getNamespace()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/cast/Cast$CastApi;->removeMessageReceivedCallbacks(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 738
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private requestRecommendations(Ljava/lang/String;Lcom/google/android/videos/remote/RemoteVideoInfo;Z)V
    .locals 5
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoInfo"    # Lcom/google/android/videos/remote/RemoteVideoInfo;
    .param p3, "isEpisode"    # Z

    .prologue
    const/4 v4, 0x6

    const/4 v3, 0x0

    .line 880
    iget-object v1, p2, Lcom/google/android/videos/remote/RemoteVideoInfo;->videoId:Ljava/lang/String;

    .line 881
    .local v1, "videoId":Ljava/lang/String;
    if-eqz p3, :cond_0

    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->recommendationsRequestFactory:Lcom/google/android/videos/api/RecommendationsRequest$Factory;

    invoke-interface {v2, p1, v4, v3}, Lcom/google/android/videos/api/RecommendationsRequest$Factory;->createForShows(Ljava/lang/String;II)Lcom/google/android/videos/api/RecommendationsRequest;

    move-result-object v0

    .line 886
    .local v0, "request":Lcom/google/android/videos/api/RecommendationsRequest;
    :goto_0
    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->recommendationsRequester:Lcom/google/android/videos/async/Requester;

    iget-object v3, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->handler:Landroid/os/Handler;

    new-instance v4, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$9;

    invoke-direct {v4, p0, v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$9;-><init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Ljava/lang/String;)V

    invoke-static {v3, v4}, Lcom/google/android/videos/async/HandlerCallback;->create(Landroid/os/Handler;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/HandlerCallback;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 901
    return-void

    .line 881
    .end local v0    # "request":Lcom/google/android/videos/api/RecommendationsRequest;
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->recommendationsRequestFactory:Lcom/google/android/videos/api/RecommendationsRequest$Factory;

    invoke-interface {v2, p1, v1, v4, v3}, Lcom/google/android/videos/api/RecommendationsRequest$Factory;->createForRelatedMovies(Ljava/lang/String;Ljava/lang/String;II)Lcom/google/android/videos/api/RecommendationsRequest;

    move-result-object v0

    goto :goto_0
.end method

.method private requestSubtitleTracks(Ljava/lang/String;)V
    .locals 3
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 435
    iput-object p1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->subtitleVideoId:Ljava/lang/String;

    .line 436
    iput-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->subtitleTracks:Ljava/util/List;

    .line 437
    iput-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->selectedSubtitleTrack:Lcom/google/android/videos/subtitles/SubtitleTrack;

    .line 438
    const-string v0, "listSubtitles"

    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->playMoviesChannel:Lcom/google/android/videos/cast/v2/PlayMoviesChannel;

    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v1, v2}, Lcom/google/android/videos/cast/v2/PlayMoviesChannel;->requestSubtitleTracks(Lcom/google/android/gms/common/api/GoogleApiClient;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->checkCommandStatus(Ljava/lang/String;Lcom/google/android/gms/common/api/PendingResult;)V

    .line 439
    return-void
.end method

.method private saveSessionData(Ljava/lang/String;)V
    .locals 3
    .param p1, "sessionId"    # Ljava/lang/String;

    .prologue
    .line 745
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sessionRestore saved data: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->routeInfo:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 746
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "castv2_route_id"

    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->routeInfo:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "castv2_session_id"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 750
    return-void
.end method

.method private seekTo(Ljava/lang/String;II)Z
    .locals 6
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "milliseconds"    # I
    .param p3, "resumeState"    # I

    .prologue
    .line 347
    invoke-direct {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->isReadyToCall()Z

    move-result v1

    if-nez v1, :cond_0

    .line 348
    const/4 v1, 0x0

    .line 372
    :goto_0
    return v1

    .line 351
    :cond_0
    if-gez p2, :cond_1

    .line 352
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Tried seeking to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms. Seeking to 0 instead"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 353
    const/4 p2, 0x0

    .line 356
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->remoteMediaPlayerChannel:Lcom/google/android/gms/cast/RemoteMediaPlayer;

    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    int-to-long v4, p2

    invoke-virtual {v1, v2, v4, v5, p3}, Lcom/google/android/gms/cast/RemoteMediaPlayer;->seek(Lcom/google/android/gms/common/api/GoogleApiClient;JI)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v0

    .line 358
    .local v0, "pendingResult":Lcom/google/android/gms/common/api/PendingResult;, "Lcom/google/android/gms/common/api/PendingResult<Lcom/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult;>;"
    new-instance v1, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$4;

    invoke-direct {v1, p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$4;-><init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/PendingResult;->setResultCallback(Lcom/google/android/gms/common/api/ResultCallback;)V

    .line 371
    invoke-virtual {p0, p2}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->freezePlayerTime(I)V

    .line 372
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private sendRecommendations([Lcom/google/wireless/android/video/magma/proto/AssetResource;)V
    .locals 7
    .param p1, "recommendations"    # [Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    .line 825
    invoke-static {}, Lcom/google/android/videos/utils/Preconditions;->checkMainThread()V

    .line 826
    array-length v2, p1

    .line 827
    .local v2, "recommendationCount":I
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 828
    .local v3, "recommendedVideoIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 829
    aget-object v1, p1, v0

    .line 830
    .local v1, "recommendation":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    iget-object v4, v1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v4, :cond_0

    iget-object v4, v1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v4, v4, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 831
    iget-object v4, v1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v4, v4, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 828
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 834
    .end local v1    # "recommendation":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :cond_1
    const-string v4, "sendSuggestedVideo"

    iget-object v5, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->playMoviesChannel:Lcom/google/android/videos/cast/v2/PlayMoviesChannel;

    iget-object v6, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v5, v6, v3}, Lcom/google/android/videos/cast/v2/PlayMoviesChannel;->sendRecommendations(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/util/List;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->checkCommandStatus(Ljava/lang/String;Lcom/google/android/gms/common/api/PendingResult;)V

    .line 836
    return-void
.end method

.method private sendUserDisplayName(Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 816
    invoke-static {}, Lcom/google/android/videos/utils/Preconditions;->checkMainThread()V

    .line 817
    const-string v0, "sendUserDisplayName"

    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->playMoviesChannel:Lcom/google/android/videos/cast/v2/PlayMoviesChannel;

    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v1, v2, p1}, Lcom/google/android/videos/cast/v2/PlayMoviesChannel;->sendIdentity(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->checkCommandStatus(Ljava/lang/String;Lcom/google/android/gms/common/api/PendingResult;)V

    .line 818
    return-void
.end method

.method private setState(I)V
    .locals 2
    .param p1, "newState"    # I

    .prologue
    .line 527
    iget v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->state:I

    if-ne p1, v0, :cond_0

    .line 533
    :goto_0
    return-void

    .line 531
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->state:I

    invoke-static {v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->getStateName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " -> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->getStateName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 532
    iput p1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->state:I

    goto :goto_0
.end method

.method private updateData()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 394
    invoke-direct {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->isConnectionClosed()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 415
    :goto_0
    return-void

    .line 398
    :cond_0
    iget-object v5, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->remoteMediaPlayerChannel:Lcom/google/android/gms/cast/RemoteMediaPlayer;

    invoke-virtual {v5}, Lcom/google/android/gms/cast/RemoteMediaPlayer;->getMediaInfo()Lcom/google/android/gms/cast/MediaInfo;

    move-result-object v0

    .line 399
    .local v0, "mediaInfo":Lcom/google/android/gms/cast/MediaInfo;
    if-eqz v0, :cond_2

    .line 400
    invoke-virtual {v0}, Lcom/google/android/gms/cast/MediaInfo;->getCustomData()Lorg/json/JSONObject;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->decodeMediaStatus(Lorg/json/JSONObject;)Lcom/google/android/videos/remote/RemoteVideoInfo;

    move-result-object v4

    .line 401
    .local v4, "videoInfo":Lcom/google/android/videos/remote/RemoteVideoInfo;
    invoke-virtual {p0, v4}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->updateCachedVideoInfo(Lcom/google/android/videos/remote/RemoteVideoInfo;)V

    .line 406
    .end local v4    # "videoInfo":Lcom/google/android/videos/remote/RemoteVideoInfo;
    :goto_1
    iget-object v5, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->remoteMediaPlayerChannel:Lcom/google/android/gms/cast/RemoteMediaPlayer;

    invoke-virtual {v5}, Lcom/google/android/gms/cast/RemoteMediaPlayer;->getMediaStatus()Lcom/google/android/gms/cast/MediaStatus;

    move-result-object v1

    .line 407
    .local v1, "mediaStatus":Lcom/google/android/gms/cast/MediaStatus;
    if-eqz v1, :cond_3

    .line 408
    invoke-virtual {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->getVideoInfo()Lcom/google/android/videos/remote/RemoteVideoInfo;

    move-result-object v4

    .line 409
    .restart local v4    # "videoInfo":Lcom/google/android/videos/remote/RemoteVideoInfo;
    if-eqz v4, :cond_1

    iget-object v3, v4, Lcom/google/android/videos/remote/RemoteVideoInfo;->videoId:Ljava/lang/String;

    .line 410
    .local v3, "videoId":Ljava/lang/String;
    :cond_1
    invoke-direct {p0, v3}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->updateSubtitles(Ljava/lang/String;)Lcom/google/android/videos/subtitles/SubtitleTrack;

    move-result-object v2

    .line 411
    .local v2, "selectedSubtitleTrack":Lcom/google/android/videos/subtitles/SubtitleTrack;
    invoke-virtual {v1}, Lcom/google/android/gms/cast/MediaStatus;->getPlayerState()I

    move-result v5

    invoke-direct {p0, v3, v2, v5}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->updatePlayerState(Ljava/lang/String;Lcom/google/android/videos/subtitles/SubtitleTrack;I)V

    goto :goto_0

    .line 403
    .end local v1    # "mediaStatus":Lcom/google/android/gms/cast/MediaStatus;
    .end local v2    # "selectedSubtitleTrack":Lcom/google/android/videos/subtitles/SubtitleTrack;
    .end local v3    # "videoId":Ljava/lang/String;
    .end local v4    # "videoInfo":Lcom/google/android/videos/remote/RemoteVideoInfo;
    :cond_2
    invoke-virtual {p0, v3}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->updateCachedVideoInfo(Lcom/google/android/videos/remote/RemoteVideoInfo;)V

    goto :goto_1

    .line 413
    .restart local v1    # "mediaStatus":Lcom/google/android/gms/cast/MediaStatus;
    :cond_3
    const/4 v5, 0x0

    invoke-direct {p0, v3, v3, v5}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->updatePlayerState(Ljava/lang/String;Lcom/google/android/videos/subtitles/SubtitleTrack;I)V

    goto :goto_0
.end method

.method private updatePlayerState(Ljava/lang/String;Lcom/google/android/videos/subtitles/SubtitleTrack;I)V
    .locals 12
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "selectedSubtitleTrack"    # Lcom/google/android/videos/subtitles/SubtitleTrack;
    .param p3, "remotePlayerState"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x0

    const/4 v2, -0x1

    .line 477
    new-instance v0, Lcom/google/android/videos/remote/PlayerState;

    move v3, v2

    move v4, v2

    move-object v5, v1

    move-object v6, v1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/remote/PlayerState;-><init>(Ljava/lang/String;IIILcom/google/android/videos/subtitles/SubtitleTrack;Ljava/lang/String;Z)V

    .line 480
    .local v0, "playerState":Lcom/google/android/videos/remote/PlayerState;
    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->remoteMediaPlayerChannel:Lcom/google/android/gms/cast/RemoteMediaPlayer;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/RemoteMediaPlayer;->getApproximateStreamPosition()J

    move-result-wide v2

    long-to-int v10, v2

    .line 481
    .local v10, "positionMillis":I
    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->remoteMediaPlayerChannel:Lcom/google/android/gms/cast/RemoteMediaPlayer;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/RemoteMediaPlayer;->getStreamDuration()J

    move-result-wide v2

    long-to-int v9, v2

    .line 484
    .local v9, "durationMillis":I
    packed-switch p3, :pswitch_data_0

    .line 507
    const/4 v1, 0x4

    iput v1, v0, Lcom/google/android/videos/remote/PlayerState;->state:I

    .line 510
    :goto_0
    iput-object p1, v0, Lcom/google/android/videos/remote/PlayerState;->videoId:Ljava/lang/String;

    .line 513
    iput v10, v0, Lcom/google/android/videos/remote/PlayerState;->time:I

    .line 514
    iput-object p2, v0, Lcom/google/android/videos/remote/PlayerState;->subtitleTrack:Lcom/google/android/videos/subtitles/SubtitleTrack;

    .line 515
    invoke-virtual {p0, v0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->updateCachedPlayerState(Lcom/google/android/videos/remote/PlayerState;)V

    .line 516
    return-void

    .line 488
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->getVideoInfo()Lcom/google/android/videos/remote/RemoteVideoInfo;

    move-result-object v11

    .line 489
    .local v11, "videoInfo":Lcom/google/android/videos/remote/RemoteVideoInfo;
    if-eqz v11, :cond_0

    iget v8, v11, Lcom/google/android/videos/remote/RemoteVideoInfo;->durationMillis:I

    .line 490
    .local v8, "duration":I
    :goto_1
    invoke-static {v10, v8, v7}, Lcom/google/android/videos/utils/Util;->isAtEndOfMovie(III)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 491
    const/4 v1, 0x5

    iput v1, v0, Lcom/google/android/videos/remote/PlayerState;->state:I

    goto :goto_0

    .end local v8    # "duration":I
    :cond_0
    move v8, v9

    .line 489
    goto :goto_1

    .line 493
    .restart local v8    # "duration":I
    :cond_1
    iput v7, v0, Lcom/google/android/videos/remote/PlayerState;->state:I

    .line 494
    const/4 p1, 0x0

    .line 496
    goto :goto_0

    .line 498
    .end local v8    # "duration":I
    .end local v11    # "videoInfo":Lcom/google/android/videos/remote/RemoteVideoInfo;
    :pswitch_1
    const/4 v1, 0x1

    iput v1, v0, Lcom/google/android/videos/remote/PlayerState;->state:I

    goto :goto_0

    .line 501
    :pswitch_2
    const/4 v1, 0x2

    iput v1, v0, Lcom/google/android/videos/remote/PlayerState;->state:I

    goto :goto_0

    .line 504
    :pswitch_3
    const/4 v1, 0x3

    iput v1, v0, Lcom/google/android/videos/remote/PlayerState;->state:I

    goto :goto_0

    .line 484
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method private updateSubtitles(Ljava/lang/String;)Lcom/google/android/videos/subtitles/SubtitleTrack;
    .locals 3
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 448
    if-nez p1, :cond_0

    .line 449
    invoke-virtual {p0, v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->updateCachedSubtitleTracks(Lcom/google/android/videos/remote/SubtitleTrackList;)V

    .line 468
    :goto_0
    return-object v1

    .line 453
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->subtitleVideoId:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 455
    invoke-direct {p0, p1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->requestSubtitleTracks(Ljava/lang/String;)V

    .line 457
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->subtitleTracks:Ljava/util/List;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->subtitleTracks:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 458
    :cond_2
    invoke-virtual {p0, v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->updateCachedSubtitleTracks(Lcom/google/android/videos/remote/SubtitleTrackList;)V

    goto :goto_0

    .line 462
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->getSubtitleTrackList()Lcom/google/android/videos/remote/SubtitleTrackList;

    move-result-object v0

    .line 463
    .local v0, "trackList":Lcom/google/android/videos/remote/SubtitleTrackList;
    if-eqz v0, :cond_4

    iget-object v1, v0, Lcom/google/android/videos/remote/SubtitleTrackList;->tracks:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->subtitleTracks:Ljava/util/List;

    if-eq v1, v2, :cond_5

    .line 464
    :cond_4
    new-instance v0, Lcom/google/android/videos/remote/SubtitleTrackList;

    .end local v0    # "trackList":Lcom/google/android/videos/remote/SubtitleTrackList;
    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->subtitleTracks:Ljava/util/List;

    invoke-direct {v0, p1, v1}, Lcom/google/android/videos/remote/SubtitleTrackList;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 465
    .restart local v0    # "trackList":Lcom/google/android/videos/remote/SubtitleTrackList;
    invoke-virtual {p0, v0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->updateCachedSubtitleTracks(Lcom/google/android/videos/remote/SubtitleTrackList;)V

    .line 468
    :cond_5
    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->selectedSubtitleTrack:Lcom/google/android/videos/subtitles/SubtitleTrack;

    goto :goto_0
.end method


# virtual methods
.method protected calculateExtrapolatedPlayerTime()I
    .locals 4

    .prologue
    .line 858
    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->remoteMediaPlayerChannel:Lcom/google/android/gms/cast/RemoteMediaPlayer;

    invoke-virtual {v2}, Lcom/google/android/gms/cast/RemoteMediaPlayer;->getApproximateStreamPosition()J

    move-result-wide v2

    long-to-int v1, v2

    .line 859
    .local v1, "rampTime":I
    if-eqz v1, :cond_0

    .line 864
    .end local v1    # "rampTime":I
    :goto_0
    return v1

    .line 863
    .restart local v1    # "rampTime":I
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->getPlayerState()Lcom/google/android/videos/remote/PlayerState;

    move-result-object v0

    .line 864
    .local v0, "playerState":Lcom/google/android/videos/remote/PlayerState;
    if-eqz v0, :cond_1

    iget v2, v0, Lcom/google/android/videos/remote/PlayerState;->time:I

    :goto_1
    move v1, v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public fling(Ljava/lang/String;Lcom/google/android/videos/remote/RemoteVideoInfo;IZZZ)I
    .locals 7
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoInfo"    # Lcom/google/android/videos/remote/RemoteVideoInfo;
    .param p3, "resumeTimeMillis"    # I
    .param p4, "isTrailer"    # Z
    .param p5, "isEpisode"    # Z
    .param p6, "pinned"    # Z

    .prologue
    .line 302
    iget-object v0, p2, Lcom/google/android/videos/remote/RemoteVideoInfo;->videoId:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->videoIdToFling:Ljava/lang/String;

    .line 304
    iput-object p1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->account:Ljava/lang/String;

    .line 305
    iput-boolean p6, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->pinned:Z

    .line 306
    new-instance v0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;

    invoke-direct {p0, p2}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->encodeContentInfo(Lcom/google/android/videos/remote/RemoteVideoInfo;)Lorg/json/JSONObject;

    move-result-object v6

    move-object v1, p0

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;-><init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Lcom/google/android/videos/remote/RemoteVideoInfo;IZZLorg/json/JSONObject;)V

    iput-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->flingInProgress:Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;

    .line 308
    iget v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->state:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 309
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->flingInProgress:Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;

    invoke-virtual {v0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->invoke()I

    move-result v0

    .line 311
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRouteInfo()Landroid/support/v7/media/MediaRouter$RouteInfo;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->routeInfo:Landroid/support/v7/media/MediaRouter$RouteInfo;

    return-object v0
.end method

.method public getScreenName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->castDevice:Lcom/google/android/gms/cast/CastDevice;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->castDevice:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/CastDevice;->getFriendlyName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDisconnected()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 666
    iget v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->state:I

    if-eq v0, v1, :cond_0

    .line 667
    const-string v0, "NO direct call to onDisconnected, call disconnect() instead"

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 669
    :cond_0
    iget v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->state:I

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    .line 670
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->setState(I)V

    .line 671
    invoke-super {p0}, Lcom/google/android/videos/remote/RemoteControl;->onDisconnected()V

    .line 672
    return-void

    .line 669
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public pause(Ljava/lang/String;)Z
    .locals 3
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 325
    invoke-direct {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->isReadyToCall()Z

    move-result v0

    if-nez v0, :cond_0

    .line 326
    const/4 v0, 0x0

    .line 329
    :goto_0
    return v0

    .line 328
    :cond_0
    const-string v0, "pause"

    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->remoteMediaPlayerChannel:Lcom/google/android/gms/cast/RemoteMediaPlayer;

    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/cast/RemoteMediaPlayer;->pause(Lcom/google/android/gms/common/api/GoogleApiClient;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->checkCommandStatus(Ljava/lang/String;Lcom/google/android/gms/common/api/PendingResult;)V

    .line 329
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public play(Ljava/lang/String;)Z
    .locals 3
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 316
    invoke-direct {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->isReadyToCall()Z

    move-result v0

    if-nez v0, :cond_0

    .line 317
    const/4 v0, 0x0

    .line 320
    :goto_0
    return v0

    .line 319
    :cond_0
    const-string v0, "play"

    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->remoteMediaPlayerChannel:Lcom/google/android/gms/cast/RemoteMediaPlayer;

    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/cast/RemoteMediaPlayer;->play(Lcom/google/android/gms/common/api/GoogleApiClient;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->checkCommandStatus(Ljava/lang/String;Lcom/google/android/gms/common/api/PendingResult;)V

    .line 320
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public prepareForFling()V
    .locals 2

    .prologue
    .line 286
    invoke-virtual {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->getError()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 295
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Preparing RemoteControl for fling while in error state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->getErrorMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 297
    :goto_0
    :sswitch_0
    return-void

    .line 292
    :sswitch_1
    invoke-virtual {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->clearError()V

    goto :goto_0

    .line 286
    :sswitch_data_0
    .sparse-switch
        -0x3eb -> :sswitch_1
        -0x3ea -> :sswitch_1
        0x0 -> :sswitch_0
    .end sparse-switch
.end method

.method public requestAudioTracks()Z
    .locals 3

    .prologue
    .line 339
    invoke-direct {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->isReadyToCall()Z

    move-result v0

    if-nez v0, :cond_0

    .line 340
    const/4 v0, 0x0

    .line 343
    :goto_0
    return v0

    .line 342
    :cond_0
    const-string v0, "listAudioTracks"

    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->playMoviesChannel:Lcom/google/android/videos/cast/v2/PlayMoviesChannel;

    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v1, v2}, Lcom/google/android/videos/cast/v2/PlayMoviesChannel;->requestAudioTracks(Lcom/google/android/gms/common/api/GoogleApiClient;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->checkCommandStatus(Ljava/lang/String;Lcom/google/android/gms/common/api/PendingResult;)V

    .line 343
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public seekTo(Ljava/lang/String;I)Z
    .locals 1
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "milliseconds"    # I

    .prologue
    .line 334
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->seekTo(Ljava/lang/String;II)Z

    move-result v0

    return v0
.end method

.method public setAudioTrack(Lcom/google/wireless/android/video/magma/proto/AudioInfo;)Z
    .locals 3
    .param p1, "track"    # Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    .prologue
    .line 386
    invoke-direct {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->isConnectionClosed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 387
    const/4 v0, 0x0

    .line 390
    :goto_0
    return v0

    .line 389
    :cond_0
    const-string v0, "setAudioTrack"

    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->playMoviesChannel:Lcom/google/android/videos/cast/v2/PlayMoviesChannel;

    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v1, v2, p1}, Lcom/google/android/videos/cast/v2/PlayMoviesChannel;->selectAudioTrack(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/wireless/android/video/magma/proto/AudioInfo;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->checkCommandStatus(Ljava/lang/String;Lcom/google/android/gms/common/api/PendingResult;)V

    .line 390
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setSubtitles(Lcom/google/android/videos/subtitles/SubtitleTrack;)Z
    .locals 3
    .param p1, "track"    # Lcom/google/android/videos/subtitles/SubtitleTrack;

    .prologue
    .line 377
    invoke-direct {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->isConnectionClosed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 378
    const/4 v0, 0x0

    .line 381
    :goto_0
    return v0

    .line 380
    :cond_0
    const-string v0, "setSubtitles"

    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->playMoviesChannel:Lcom/google/android/videos/cast/v2/PlayMoviesChannel;

    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v1, v2, p1}, Lcom/google/android/videos/cast/v2/PlayMoviesChannel;->selectSubtitleTrack(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/videos/subtitles/SubtitleTrack;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->checkCommandStatus(Ljava/lang/String;Lcom/google/android/gms/common/api/PendingResult;)V

    .line 381
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public startConnect()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 268
    iget v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->state:I

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    .line 270
    invoke-direct {p0, v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->setState(I)V

    .line 271
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->connect()V

    .line 272
    return-void

    .line 268
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 281
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->disconnect(Z)V

    .line 282
    return-void
.end method
