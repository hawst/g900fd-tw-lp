.class Lcom/google/android/videos/onboard/OnboardTutorialPageInfo$2;
.super Ljava/lang/Object;
.source "OnboardTutorialPageInfo.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;->getEndButtonInfo(Lcom/google/android/play/onboard/OnboardHostControl;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;

.field final synthetic val$hostControl:Lcom/google/android/play/onboard/OnboardHostControl;


# direct methods
.method constructor <init>(Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;Lcom/google/android/play/onboard/OnboardHostControl;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo$2;->this$0:Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;

    iput-object p2, p0, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo$2;->val$hostControl:Lcom/google/android/play/onboard/OnboardHostControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo$2;->this$0:Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;

    # getter for: Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;->pageIndex:I
    invoke-static {v0}, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;->access$000(Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo$2;->this$0:Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;

    # getter for: Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;->pageCount:I
    invoke-static {v1}, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;->access$100(Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    .line 79
    iget-object v0, p0, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo$2;->val$hostControl:Lcom/google/android/play/onboard/OnboardHostControl;

    invoke-interface {v0}, Lcom/google/android/play/onboard/OnboardHostControl;->finishOnboardFlow()V

    .line 83
    :goto_0
    return-void

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo$2;->val$hostControl:Lcom/google/android/play/onboard/OnboardHostControl;

    invoke-interface {v0}, Lcom/google/android/play/onboard/OnboardHostControl;->goToNextPage()V

    goto :goto_0
.end method
