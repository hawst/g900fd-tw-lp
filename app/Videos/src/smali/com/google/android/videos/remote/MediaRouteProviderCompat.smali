.class public Lcom/google/android/videos/remote/MediaRouteProviderCompat;
.super Ljava/lang/Object;
.source "MediaRouteProviderCompat.java"


# direct methods
.method public static create(Landroid/support/v7/app/ActionBarActivity;Landroid/support/v7/media/MediaRouteSelector;)Lcom/google/android/videos/remote/MediaRouteProvider;
    .locals 1
    .param p0, "activity"    # Landroid/support/v7/app/ActionBarActivity;
    .param p1, "supportedRouteTypes"    # Landroid/support/v7/media/MediaRouteSelector;

    .prologue
    .line 20
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/utils/Util;->isTv(Landroid/content/pm/PackageManager;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 22
    :try_start_0
    new-instance v0, Lcom/google/android/videos/remote/MediaRouteProviderV7;

    invoke-direct {v0, p0, p1}, Lcom/google/android/videos/remote/MediaRouteProviderV7;-><init>(Landroid/support/v7/app/ActionBarActivity;Landroid/support/v7/media/MediaRouteSelector;)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 27
    :goto_0
    return-object v0

    .line 23
    :catch_0
    move-exception v0

    .line 27
    :cond_0
    new-instance v0, Lcom/google/android/videos/remote/MediaRouteProviderDummy;

    invoke-direct {v0}, Lcom/google/android/videos/remote/MediaRouteProviderDummy;-><init>()V

    goto :goto_0
.end method
