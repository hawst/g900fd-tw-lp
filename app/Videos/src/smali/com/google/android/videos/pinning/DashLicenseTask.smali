.class abstract Lcom/google/android/videos/pinning/DashLicenseTask;
.super Lcom/google/android/videos/pinning/Task;
.source "DashLicenseTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/pinning/DashLicenseTask$1;,
        Lcom/google/android/videos/pinning/DashLicenseTask$Release;,
        Lcom/google/android/videos/pinning/DashLicenseTask$Refresh;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/pinning/Task",
        "<",
        "Lcom/google/android/videos/pinning/DownloadKey;",
        ">;"
    }
.end annotation


# instance fields
.field protected final database:Lcom/google/android/videos/store/Database;

.field private final isRelease:Z

.field protected final itagInfoStore:Lcom/google/android/videos/store/ItagInfoStore;

.field private final videosGlobals:Lcom/google/android/videos/VideosGlobals;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/VideosGlobals;ILcom/google/android/videos/pinning/DownloadKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/videos/pinning/Task$Listener;Lcom/google/android/videos/logging/EventLogger;Z)V
    .locals 7
    .param p1, "videosGlobals"    # Lcom/google/android/videos/VideosGlobals;
    .param p2, "taskType"    # I
    .param p3, "key"    # Lcom/google/android/videos/pinning/DownloadKey;
    .param p4, "wakeLock"    # Landroid/os/PowerManager$WakeLock;
    .param p5, "wifiLock"    # Landroid/net/wifi/WifiManager$WifiLock;
    .param p6, "listener"    # Lcom/google/android/videos/pinning/Task$Listener;
    .param p7, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;
    .param p8, "isRelease"    # Z

    .prologue
    .line 36
    move-object v0, p0

    move v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/pinning/Task;-><init>(ILcom/google/android/videos/pinning/Task$Key;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/videos/pinning/Task$Listener;Lcom/google/android/videos/logging/EventLogger;)V

    .line 37
    iput-object p1, p0, Lcom/google/android/videos/pinning/DashLicenseTask;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    .line 38
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pinning/DashLicenseTask;->database:Lcom/google/android/videos/store/Database;

    .line 39
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getItagInfoStore()Lcom/google/android/videos/store/ItagInfoStore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pinning/DashLicenseTask;->itagInfoStore:Lcom/google/android/videos/store/ItagInfoStore;

    .line 40
    iput-boolean p8, p0, Lcom/google/android/videos/pinning/DashLicenseTask;->isRelease:Z

    .line 41
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/VideosGlobals;ILcom/google/android/videos/pinning/DownloadKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/videos/pinning/Task$Listener;Lcom/google/android/videos/logging/EventLogger;ZLcom/google/android/videos/pinning/DashLicenseTask$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/VideosGlobals;
    .param p2, "x1"    # I
    .param p3, "x2"    # Lcom/google/android/videos/pinning/DownloadKey;
    .param p4, "x3"    # Landroid/os/PowerManager$WakeLock;
    .param p5, "x4"    # Landroid/net/wifi/WifiManager$WifiLock;
    .param p6, "x5"    # Lcom/google/android/videos/pinning/Task$Listener;
    .param p7, "x6"    # Lcom/google/android/videos/logging/EventLogger;
    .param p8, "x7"    # Z
    .param p9, "x8"    # Lcom/google/android/videos/pinning/DashLicenseTask$1;

    .prologue
    .line 25
    invoke-direct/range {p0 .. p8}, Lcom/google/android/videos/pinning/DashLicenseTask;-><init>(Lcom/google/android/videos/VideosGlobals;ILcom/google/android/videos/pinning/DownloadKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/videos/pinning/Task$Listener;Lcom/google/android/videos/logging/EventLogger;Z)V

    return-void
.end method


# virtual methods
.method protected execute()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/Task$TaskException;
        }
    .end annotation

    .prologue
    .line 45
    iget-object v1, p0, Lcom/google/android/videos/pinning/DashLicenseTask;->database:Lcom/google/android/videos/store/Database;

    iget-object v0, p0, Lcom/google/android/videos/pinning/DashLicenseTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/videos/pinning/DownloadKey;

    invoke-static {v1, v0}, Lcom/google/android/videos/pinning/PinningDbHelper;->getDownloadDetails(Lcom/google/android/videos/store/Database;Lcom/google/android/videos/pinning/DownloadKey;)Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;

    move-result-object v6

    .line 46
    .local v6, "details":Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;
    if-eqz v6, :cond_0

    iget-object v0, v6, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->cencPsshData:[B

    if-eqz v0, :cond_0

    iget-object v0, v6, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->cencKeySetId:[B

    if-nez v0, :cond_1

    .line 47
    :cond_0
    new-instance v0, Lcom/google/android/videos/pinning/Task$TaskException;

    const-string v1, "Invalid license data"

    invoke-direct {v0, v1}, Lcom/google/android/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v6, v0}, Lcom/google/android/videos/pinning/DashLicenseTask;->onRequestImpossible(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/lang/Throwable;)V

    .line 104
    :goto_0
    return-void

    .line 53
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/pinning/DashLicenseTask;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    iget-object v1, p0, Lcom/google/android/videos/pinning/DashLicenseTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v1, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v1, v1, Lcom/google/android/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/pinning/DashLicenseTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v2, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v2, v2, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    iget-object v3, v6, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->cencKeySetId:[B

    iget v4, v6, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->cencSecurityLevel:I

    iget-boolean v5, p0, Lcom/google/android/videos/pinning/DashLicenseTask;->isRelease:Z

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->getOfflineTaskInstance(Lcom/google/android/videos/VideosGlobals;Ljava/lang/String;Ljava/lang/String;[BIZ)Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;
    :try_end_0
    .catch Landroid/media/UnsupportedSchemeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    .line 61
    .local v9, "mediaDrmWrapper":Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;
    invoke-virtual {v9}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->getSecurityLevel()I

    move-result v13

    .line 62
    .local v13, "sessionSecurityLevel":I
    iget v0, v6, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->cencSecurityLevel:I

    if-lez v0, :cond_2

    iget v0, v6, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->cencSecurityLevel:I

    if-eq v0, v13, :cond_2

    .line 65
    new-instance v0, Lcom/google/android/videos/pinning/Task$TaskException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Security level mismatch: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v6, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->cencSecurityLevel:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v9}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->getSecurityLevel()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v6, v0}, Lcom/google/android/videos/pinning/DashLicenseTask;->onRequestImpossible(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 55
    .end local v9    # "mediaDrmWrapper":Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;
    .end local v13    # "sessionSecurityLevel":I
    :catch_0
    move-exception v7

    .line 57
    .local v7, "e":Landroid/media/UnsupportedSchemeException;
    invoke-virtual {p0, v6, v7}, Lcom/google/android/videos/pinning/DashLicenseTask;->onRequestImpossible(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 70
    .end local v7    # "e":Landroid/media/UnsupportedSchemeException;
    .restart local v9    # "mediaDrmWrapper":Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;
    .restart local v13    # "sessionSecurityLevel":I
    :cond_2
    const/4 v14, 0x0

    .line 73
    .local v14, "shouldRetry":Z
    :try_start_1
    iget-object v0, v6, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->activation:Lcom/google/android/videos/drm/Activation;

    invoke-virtual {v9, v0}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->setActivation(Lcom/google/android/videos/drm/Activation;)V

    .line 76
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v10

    .line 77
    .local v10, "openCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/Object;[B>;"
    sget-object v0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->WIDEVINE_UUID:Ljava/util/UUID;

    iget-object v1, v6, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->cencPsshData:[B

    invoke-static {v0, v1}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    const-string v1, "video/"

    const/4 v2, 0x0

    invoke-virtual {v9, v0, v1, v2, v10}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->open(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 80
    invoke-virtual {v10}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;

    .line 84
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v11

    .line 85
    .local v11, "restoreCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/Object;[B>;"
    const/4 v0, 0x0

    invoke-virtual {v9, v0, v11}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->restoreLicense(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 86
    invoke-virtual {v11}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;

    .line 87
    const/4 v14, 0x1

    .line 90
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v8

    .line 91
    .local v8, "licenseCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/Object;[B>;"
    const/4 v0, 0x0

    invoke-virtual {v9, v0, v8}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->requestLicense(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 92
    invoke-virtual {v8}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, [B

    .line 94
    .local v12, "result":[B
    invoke-virtual {p0, v6, v12}, Lcom/google/android/videos/pinning/DashLicenseTask;->onRequestCompleted(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;[B)V
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 102
    invoke-virtual {v9}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->close()V

    goto/16 :goto_0

    .line 95
    .end local v8    # "licenseCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/Object;[B>;"
    .end local v10    # "openCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/Object;[B>;"
    .end local v11    # "restoreCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/Object;[B>;"
    .end local v12    # "result":[B
    :catch_1
    move-exception v7

    .line 96
    .local v7, "e":Ljava/util/concurrent/ExecutionException;
    if-eqz v14, :cond_3

    .line 97
    :try_start_2
    invoke-virtual {v7}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {p0, v6, v0}, Lcom/google/android/videos/pinning/DashLicenseTask;->onRequestError(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 102
    :goto_1
    invoke-virtual {v9}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->close()V

    goto/16 :goto_0

    .line 99
    :cond_3
    :try_start_3
    new-instance v0, Lcom/google/android/videos/pinning/Task$TaskException;

    const-string v1, "Failed to restore keys"

    invoke-direct {v0, v1, v7}, Lcom/google/android/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {p0, v6, v0}, Lcom/google/android/videos/pinning/DashLicenseTask;->onRequestImpossible(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 102
    .end local v7    # "e":Ljava/util/concurrent/ExecutionException;
    :catchall_0
    move-exception v0

    invoke-virtual {v9}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->close()V

    throw v0
.end method

.method protected abstract onRequestCompleted(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;[B)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/Task$TaskException;
        }
    .end annotation
.end method

.method protected abstract onRequestError(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/lang/Throwable;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/Task$TaskException;
        }
    .end annotation
.end method

.method protected abstract onRequestImpossible(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/lang/Throwable;)V
.end method
