.class Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1$2;
.super Ljava/lang/Object;
.source "CastV2RemoteControl.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;->onKeyRequested(JLjava/lang/String;[Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/api/RobotTokenRequest;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;

.field final synthetic val$cmdId:J

.field final synthetic val$retryAction:Lcom/google/android/videos/utils/RetryAction;


# direct methods
.method constructor <init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;JLcom/google/android/videos/utils/RetryAction;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1$2;->this$1:Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;

    iput-wide p2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1$2;->val$cmdId:J

    iput-object p4, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1$2;->val$retryAction:Lcom/google/android/videos/utils/RetryAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/api/RobotTokenRequest;Ljava/lang/Exception;)V
    .locals 3
    .param p1, "request"    # Lcom/google/android/videos/api/RobotTokenRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 209
    const-string v0, "Failed to aquire RobotToken"

    invoke-static {v0, p2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 210
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1$2;->this$1:Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;

    iget-object v0, v0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    const/16 v1, -0x3eb

    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1$2;->val$retryAction:Lcom/google/android/videos/utils/RetryAction;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->onError(ILcom/google/android/videos/utils/RetryAction;)V

    .line 211
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 191
    check-cast p1, Lcom/google/android/videos/api/RobotTokenRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1$2;->onError(Lcom/google/android/videos/api/RobotTokenRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/api/RobotTokenRequest;Ljava/lang/String;)V
    .locals 7
    .param p1, "request"    # Lcom/google/android/videos/api/RobotTokenRequest;
    .param p2, "response"    # Ljava/lang/String;

    .prologue
    .line 194
    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1$2;->this$1:Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;

    iget-object v1, v1, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # getter for: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->state:I
    invoke-static {v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$300(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    .line 205
    :goto_0
    return-void

    .line 200
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1$2;->this$1:Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;

    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1$2;->this$1:Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;

    iget-object v2, v2, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # getter for: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;
    invoke-static {v2}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$700(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1$2;->val$cmdId:J

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p2, v3, v6

    invoke-virtual {v1, v2, v4, v5, v3}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;->sendKeyResponse(Lcom/google/android/gms/common/api/GoogleApiClient;J[Ljava/lang/String;)Lcom/google/android/gms/common/api/PendingResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 201
    :catch_0
    move-exception v0

    .line 202
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "Failed to send key tokens to cast device"

    invoke-static {v1, v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 203
    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1$2;->this$1:Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;

    iget-object v1, v1, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    const/16 v2, -0x3eb

    iget-object v3, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1$2;->val$retryAction:Lcom/google/android/videos/utils/RetryAction;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->onError(ILcom/google/android/videos/utils/RetryAction;)V

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 191
    check-cast p1, Lcom/google/android/videos/api/RobotTokenRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/lang/String;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1$2;->onResponse(Lcom/google/android/videos/api/RobotTokenRequest;Ljava/lang/String;)V

    return-void
.end method
