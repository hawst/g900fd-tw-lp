.class public Lcom/google/android/videos/ui/ArtworkMonitor;
.super Lcom/google/android/videos/store/Database$BaseListener;
.source "ArtworkMonitor.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/ArtworkMonitor$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/store/Database$BaseListener;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Ljava/lang/String;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final callback:Lcom/google/android/videos/async/HandlerCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/HandlerCallback",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final database:Lcom/google/android/videos/store/Database;

.field private id:Ljava/lang/String;

.field private imageType:I

.field private final listener:Lcom/google/android/videos/ui/ArtworkMonitor$Listener;

.field private final posterStore:Lcom/google/android/videos/store/PosterStore;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/store/PosterStore;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/ui/ArtworkMonitor$Listener;)V
    .locals 1
    .param p1, "posterStore"    # Lcom/google/android/videos/store/PosterStore;
    .param p2, "database"    # Lcom/google/android/videos/store/Database;
    .param p3, "listener"    # Lcom/google/android/videos/ui/ArtworkMonitor$Listener;

    .prologue
    .line 49
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/videos/ui/ArtworkMonitor;-><init>(Lcom/google/android/videos/store/PosterStore;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/ui/ArtworkMonitor$Listener;Landroid/os/Looper;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Lcom/google/android/videos/store/PosterStore;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/ui/ArtworkMonitor$Listener;Landroid/os/Looper;)V
    .locals 1
    .param p1, "posterStore"    # Lcom/google/android/videos/store/PosterStore;
    .param p2, "database"    # Lcom/google/android/videos/store/Database;
    .param p3, "listener"    # Lcom/google/android/videos/ui/ArtworkMonitor$Listener;
    .param p4, "looper"    # Landroid/os/Looper;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/android/videos/store/Database$BaseListener;-><init>()V

    .line 58
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/PosterStore;

    iput-object v0, p0, Lcom/google/android/videos/ui/ArtworkMonitor;->posterStore:Lcom/google/android/videos/store/PosterStore;

    .line 59
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/Database;

    iput-object v0, p0, Lcom/google/android/videos/ui/ArtworkMonitor;->database:Lcom/google/android/videos/store/Database;

    .line 60
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/ArtworkMonitor$Listener;

    iput-object v0, p0, Lcom/google/android/videos/ui/ArtworkMonitor;->listener:Lcom/google/android/videos/ui/ArtworkMonitor$Listener;

    .line 61
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-static {v0, p0}, Lcom/google/android/videos/async/HandlerCallback;->create(Landroid/os/Handler;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/HandlerCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/ArtworkMonitor;->callback:Lcom/google/android/videos/async/HandlerCallback;

    .line 62
    return-void
.end method

.method private update()V
    .locals 4

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/videos/ui/ArtworkMonitor;->id:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/google/android/videos/ui/ArtworkMonitor;->posterStore:Lcom/google/android/videos/store/PosterStore;

    iget v1, p0, Lcom/google/android/videos/ui/ArtworkMonitor;->imageType:I

    iget-object v2, p0, Lcom/google/android/videos/ui/ArtworkMonitor;->id:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/ui/ArtworkMonitor;->callback:Lcom/google/android/videos/async/HandlerCallback;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/videos/store/PosterStore;->getImage(ILjava/lang/String;Lcom/google/android/videos/async/Callback;)V

    .line 98
    :goto_0
    return-void

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/ArtworkMonitor;->callback:Lcom/google/android/videos/async/HandlerCallback;

    iget-object v1, p0, Lcom/google/android/videos/ui/ArtworkMonitor;->id:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/async/HandlerCallback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 24
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/ArtworkMonitor;->onError(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public onError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "request"    # Ljava/lang/String;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 139
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/videos/ui/ArtworkMonitor;->onResponse(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 140
    return-void
.end method

.method public onPosterUpdated(Ljava/lang/String;)V
    .locals 1
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 103
    iget v0, p0, Lcom/google/android/videos/ui/ArtworkMonitor;->imageType:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/ArtworkMonitor;->id:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    invoke-direct {p0}, Lcom/google/android/videos/ui/ArtworkMonitor;->update()V

    .line 106
    :cond_0
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 24
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Landroid/graphics/Bitmap;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/ArtworkMonitor;->onResponse(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public onResponse(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "request"    # Ljava/lang/String;
    .param p2, "response"    # Landroid/graphics/Bitmap;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/videos/ui/ArtworkMonitor;->id:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/google/android/videos/ui/ArtworkMonitor;->listener:Lcom/google/android/videos/ui/ArtworkMonitor$Listener;

    invoke-interface {v0, p0, p2}, Lcom/google/android/videos/ui/ArtworkMonitor$Listener;->onImageChanged(Lcom/google/android/videos/ui/ArtworkMonitor;Landroid/graphics/Bitmap;)V

    .line 135
    :cond_0
    return-void
.end method

.method public onScreenshotUpdated(Ljava/lang/String;)V
    .locals 2
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 117
    iget v0, p0, Lcom/google/android/videos/ui/ArtworkMonitor;->imageType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/ArtworkMonitor;->id:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    invoke-direct {p0}, Lcom/google/android/videos/ui/ArtworkMonitor;->update()V

    .line 120
    :cond_0
    return-void
.end method

.method public onShowBannerUpdated(Ljava/lang/String;)V
    .locals 2
    .param p1, "showId"    # Ljava/lang/String;

    .prologue
    .line 124
    iget v0, p0, Lcom/google/android/videos/ui/ArtworkMonitor;->imageType:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/ArtworkMonitor;->id:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    invoke-direct {p0}, Lcom/google/android/videos/ui/ArtworkMonitor;->update()V

    .line 127
    :cond_0
    return-void
.end method

.method public onShowPosterUpdated(Ljava/lang/String;)V
    .locals 2
    .param p1, "showId"    # Ljava/lang/String;

    .prologue
    .line 110
    iget v0, p0, Lcom/google/android/videos/ui/ArtworkMonitor;->imageType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/ArtworkMonitor;->id:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    invoke-direct {p0}, Lcom/google/android/videos/ui/ArtworkMonitor;->update()V

    .line 113
    :cond_0
    return-void
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 69
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/videos/ui/ArtworkMonitor;->setMonitored(ILjava/lang/String;)V

    .line 70
    return-void
.end method

.method public setMonitored(ILjava/lang/String;)V
    .locals 2
    .param p1, "imageType"    # I
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    .line 77
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/videos/ui/ArtworkMonitor;->id:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eq v0, v1, :cond_0

    .line 78
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 79
    iget-object v0, p0, Lcom/google/android/videos/ui/ArtworkMonitor;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/store/Database;->removeListener(Lcom/google/android/videos/store/Database$Listener;)Z

    .line 85
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/android/videos/ui/ArtworkMonitor;->imageType:I

    if-ne v0, p1, :cond_1

    iget-object v0, p0, Lcom/google/android/videos/ui/ArtworkMonitor;->id:Ljava/lang/String;

    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 86
    :cond_1
    iput p1, p0, Lcom/google/android/videos/ui/ArtworkMonitor;->imageType:I

    .line 87
    iput-object p2, p0, Lcom/google/android/videos/ui/ArtworkMonitor;->id:Ljava/lang/String;

    .line 88
    invoke-direct {p0}, Lcom/google/android/videos/ui/ArtworkMonitor;->update()V

    .line 90
    :cond_2
    return-void

    .line 81
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/ui/ArtworkMonitor;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/store/Database;->addListener(Lcom/google/android/videos/store/Database$Listener;)V

    goto :goto_0
.end method
