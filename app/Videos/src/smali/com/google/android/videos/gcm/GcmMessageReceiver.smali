.class public Lcom/google/android/videos/gcm/GcmMessageReceiver;
.super Landroid/content/BroadcastReceiver;
.source "GcmMessageReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static createDismissMessageBundle(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/os/Bundle;
    .locals 3
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "showId"    # Ljava/lang/String;
    .param p2, "episodeIds"    # [Ljava/lang/String;

    .prologue
    .line 82
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 83
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "type"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const-string v1, "account"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const-string v1, "show"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    const-string v1, "episodes"

    invoke-static {p2}, Lcom/google/android/videos/gcm/GcmMessageReceiver;->encodeIds([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    return-object v0
.end method

.method public static createDismissMessageBundle(Ljava/lang/String;[Ljava/lang/String;)Landroid/os/Bundle;
    .locals 3
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "videoIds"    # [Ljava/lang/String;

    .prologue
    .line 91
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 92
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "type"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    const-string v1, "account"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    const-string v1, "videos"

    invoke-static {p1}, Lcom/google/android/videos/gcm/GcmMessageReceiver;->encodeIds([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    return-object v0
.end method

.method private static decodeIds(Ljava/lang/String;)[Ljava/lang/String;
    .locals 1
    .param p0, "episodeIds"    # Ljava/lang/String;

    .prologue
    .line 227
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 228
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    .line 230
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ","

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static encodeIds([Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "episodeIds"    # [Ljava/lang/String;

    .prologue
    .line 218
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 219
    .local v4, "stringBuilder":Ljava/lang/StringBuilder;
    move-object v0, p0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 220
    .local v1, "episodeId":Ljava/lang/String;
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    const/16 v5, 0x2c

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 219
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 223
    .end local v1    # "episodeId":Ljava/lang/String;
    :cond_0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method private static handleDismissRequest(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    .line 205
    const-string v3, "show"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 206
    .local v1, "showId":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 207
    const-string v3, "episodes"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/videos/gcm/GcmMessageReceiver;->decodeIds(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 208
    .local v0, "episodeIds":[Ljava/lang/String;
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/videos/VideosGlobals;->getContentNotificationManager()Lcom/google/android/videos/ContentNotificationManager;

    move-result-object v3

    invoke-virtual {v3, p1, v1, v0, v4}, Lcom/google/android/videos/ContentNotificationManager;->dismissContentNotification(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Z)V

    .line 215
    .end local v0    # "episodeIds":[Ljava/lang/String;
    :goto_0
    return-void

    .line 211
    :cond_0
    const-string v3, "videos"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/videos/gcm/GcmMessageReceiver;->decodeIds(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 212
    .local v2, "videoIds":[Ljava/lang/String;
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/videos/VideosGlobals;->getContentNotificationManager()Lcom/google/android/videos/ContentNotificationManager;

    move-result-object v3

    invoke-virtual {v3, p1, v2, v4}, Lcom/google/android/videos/ContentNotificationManager;->dismissContentNotification(Ljava/lang/String;[Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private static handlePurchaseRecommendedRequest(Landroid/content/Context;Landroid/accounts/Account;Landroid/content/Intent;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 142
    const-string v3, "idtype"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 143
    .local v2, "type":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 144
    const-string v3, "Malformed intent, expecting idtype"

    invoke-static {v3}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 160
    :goto_0
    return-void

    .line 148
    :cond_0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    .line 149
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unsupported type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 152
    :cond_1
    const-string v3, "tv"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 153
    .local v0, "seasonId":Ljava/lang/String;
    const-string v3, "show"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 154
    .local v1, "showId":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 155
    :cond_2
    const-string v3, "Missing show or season"

    invoke-static {v3}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 158
    :cond_3
    iget-object v3, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p0, v3, v1, v0}, Lcom/google/android/videos/NewSeasonNotificationService;->createNotifyIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method private static handleSyncRequest(Landroid/accounts/Account;Landroid/content/Intent;Z)V
    .locals 3
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "syncWishlist"    # Z

    .prologue
    const/4 v2, 0x0

    .line 163
    const-string v1, "idtype"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 164
    .local v0, "type":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 165
    const-string v1, "Malformed intent, expecting idtype"

    invoke-static {v1}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 202
    :goto_0
    return-void

    .line 169
    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 200
    invoke-static {p0, v2}, Lcom/google/android/videos/store/SyncService;->startSync(Landroid/accounts/Account;Z)V

    goto :goto_0

    .line 171
    :pswitch_0
    const-string v1, "video"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 172
    const-string v1, "video"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v2, p2}, Lcom/google/android/videos/store/SyncService;->startVideoSync(Landroid/accounts/Account;Ljava/lang/String;ZZ)V

    goto :goto_0

    .line 175
    :cond_1
    const-string v1, "Malformed intent, expecting video"

    invoke-static {v1}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 179
    :pswitch_1
    const-string v1, "tv"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 180
    const-string v1, "tv"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v2, p2}, Lcom/google/android/videos/store/SyncService;->startVideoSync(Landroid/accounts/Account;Ljava/lang/String;ZZ)V

    goto :goto_0

    .line 183
    :cond_2
    const-string v1, "Malformed intent, expecting tv"

    invoke-static {v1}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 187
    :pswitch_2
    const-string v1, "tv"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 188
    const-string v1, "tv"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v2, p2}, Lcom/google/android/videos/store/SyncService;->startSeasonSync(Landroid/accounts/Account;Ljava/lang/String;ZZ)V

    goto :goto_0

    .line 191
    :cond_3
    const-string v1, "Malformed intent, expecting tv"

    invoke-static {v1}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 169
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 100
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Received GCM message "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 102
    const-string v3, "type"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 103
    .local v2, "messageType":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 104
    const-string v3, "Missing message type"

    invoke-static {v3}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 138
    :goto_0
    return-void

    .line 108
    :cond_0
    const-string v3, "account"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 109
    .local v1, "accountName":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 110
    const-string v3, "Missing account"

    invoke-static {v3}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 113
    :cond_1
    invoke-static {p1}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/videos/VideosGlobals;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 115
    .local v0, "account":Landroid/accounts/Account;
    if-nez v0, :cond_2

    .line 116
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "No matching account for name "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v1}, Lcom/google/android/videos/utils/Hashing;->sha1(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 120
    :cond_2
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 136
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown message type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 122
    :pswitch_0
    const/4 v3, 0x0

    invoke-static {v0, p2, v3}, Lcom/google/android/videos/gcm/GcmMessageReceiver;->handleSyncRequest(Landroid/accounts/Account;Landroid/content/Intent;Z)V

    goto :goto_0

    .line 127
    :pswitch_1
    const/4 v3, 0x1

    invoke-static {v0, p2, v3}, Lcom/google/android/videos/gcm/GcmMessageReceiver;->handleSyncRequest(Landroid/accounts/Account;Landroid/content/Intent;Z)V

    goto :goto_0

    .line 130
    :pswitch_2
    invoke-static {p1, v0, p2}, Lcom/google/android/videos/gcm/GcmMessageReceiver;->handlePurchaseRecommendedRequest(Landroid/content/Context;Landroid/accounts/Account;Landroid/content/Intent;)V

    goto :goto_0

    .line 133
    :pswitch_3
    invoke-static {p1, v1, p2}, Lcom/google/android/videos/gcm/GcmMessageReceiver;->handleDismissRequest(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;)V

    goto :goto_0

    .line 120
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
