.class public Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;
.super Ljava/lang/Object;
.source "WidevineDrmSessionManager.java"

# interfaces
.implements Landroid/media/MediaDrm$OnEventListener;
.implements Landroid/os/Handler$Callback;
.implements Lcom/google/android/exoplayer/ExoPlayer$ExoPlayerComponent;
.implements Lcom/google/android/exoplayer/drm/DrmSessionManager;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$ProvisioningCallback;,
        Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$LicenseCallback;,
        Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$OpenCallback;,
        Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$EventListener;
    }
.end annotation


# instance fields
.field private final account:Ljava/lang/String;

.field private final eventHandler:Landroid/os/Handler;

.field private final eventListener:Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$EventListener;

.field private final isStreaming:Z

.field private lastException:Ljava/lang/Exception;

.field private final licenseCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/lang/Object;",
            "[B>;"
        }
    .end annotation
.end field

.field private mediaCrypto:Landroid/media/MediaCrypto;

.field private final openCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/lang/Object;",
            "[B>;"
        }
    .end annotation
.end field

.field private openCount:I

.field private final playbackHandler:Landroid/os/Handler;

.field private final provisioningCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/lang/Object;",
            "[B>;"
        }
    .end annotation
.end field

.field private state:I

.field private final videoId:Ljava/lang/String;

.field private final videosGlobals:Lcom/google/android/videos/VideosGlobals;

.field private final widevineMediaDrmWrapper:Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/VideosGlobals;Ljava/lang/String;Ljava/lang/String;[BLandroid/os/Looper;Landroid/os/Handler;Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$EventListener;ILcom/google/android/videos/player/logging/PlaybackPreparationLogger;)V
    .locals 7
    .param p1, "videosGlobals"    # Lcom/google/android/videos/VideosGlobals;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "videoId"    # Ljava/lang/String;
    .param p4, "keySetId"    # [B
    .param p5, "playbackLooper"    # Landroid/os/Looper;
    .param p6, "eventHandler"    # Landroid/os/Handler;
    .param p7, "eventListener"    # Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$EventListener;
    .param p8, "forcedSecurityLevel"    # I
    .param p9, "preparationLogger"    # Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/media/UnsupportedSchemeException;
        }
    .end annotation

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move v4, p8

    move-object v5, p0

    move-object/from16 v6, p9

    .line 78
    invoke-static/range {v0 .. v6}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->getPlaybackInstance(Lcom/google/android/videos/VideosGlobals;Ljava/lang/String;Ljava/lang/String;[BILandroid/media/MediaDrm$OnEventListener;Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;)Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->widevineMediaDrmWrapper:Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;

    .line 80
    iput-object p1, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    .line 81
    iput-object p6, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->eventHandler:Landroid/os/Handler;

    .line 82
    iput-object p7, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->eventListener:Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$EventListener;

    .line 83
    iput-object p2, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->account:Ljava/lang/String;

    .line 84
    iput-object p3, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->videoId:Ljava/lang/String;

    .line 85
    if-nez p4, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->isStreaming:Z

    .line 87
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p5, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->playbackHandler:Landroid/os/Handler;

    .line 88
    iget-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->playbackHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$LicenseCallback;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$LicenseCallback;-><init>(Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$1;)V

    invoke-static {v0, v1}, Lcom/google/android/videos/async/HandlerCallback;->create(Landroid/os/Handler;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/HandlerCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->licenseCallback:Lcom/google/android/videos/async/Callback;

    .line 89
    iget-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->playbackHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$ProvisioningCallback;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$ProvisioningCallback;-><init>(Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$1;)V

    invoke-static {v0, v1}, Lcom/google/android/videos/async/HandlerCallback;->create(Landroid/os/Handler;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/HandlerCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->provisioningCallback:Lcom/google/android/videos/async/Callback;

    .line 90
    iget-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->playbackHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$OpenCallback;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$OpenCallback;-><init>(Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$1;)V

    invoke-static {v0, v1}, Lcom/google/android/videos/async/HandlerCallback;->create(Landroid/os/Handler;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/HandlerCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->openCallback:Lcom/google/android/videos/async/Callback;

    .line 92
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->state:I

    .line 93
    return-void

    .line 85
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$1000(Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->onProvisioned()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;)Lcom/google/android/videos/VideosGlobals;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->account:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->videoId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;)Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$EventListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->eventListener:Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$EventListener;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->onOpened()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;Ljava/lang/Exception;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;
    .param p1, "x1"    # Ljava/lang/Exception;
    .param p2, "x2"    # Z

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->onError(Ljava/lang/Exception;Z)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->onLicenseReceived()V

    return-void
.end method

.method private notifyError(Ljava/lang/Exception;)V
    .locals 2
    .param p1, "e"    # Ljava/lang/Exception;

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->eventHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->eventListener:Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$EventListener;

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->eventHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$2;-><init>(Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;Ljava/lang/Exception;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 227
    :cond_0
    return-void
.end method

.method private onError(Ljava/lang/Exception;Z)V
    .locals 2
    .param p1, "e"    # Ljava/lang/Exception;
    .param p2, "failImmediately"    # Z

    .prologue
    .line 211
    iput-object p1, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->lastException:Ljava/lang/Exception;

    .line 212
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->notifyError(Ljava/lang/Exception;)V

    .line 213
    if-nez p2, :cond_0

    iget v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->state:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    .line 214
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->state:I

    .line 216
    :cond_1
    return-void
.end method

.method private onLicenseReceived()V
    .locals 1

    .prologue
    .line 184
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->state:I

    .line 185
    return-void
.end method

.method private onOpened()V
    .locals 4

    .prologue
    .line 159
    :try_start_0
    new-instance v1, Landroid/media/MediaCrypto;

    sget-object v2, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->WIDEVINE_UUID:Ljava/util/UUID;

    iget-object v3, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->widevineMediaDrmWrapper:Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;

    invoke-virtual {v3}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->getSessionId()[B

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/media/MediaCrypto;-><init>(Ljava/util/UUID;[B)V

    iput-object v1, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->mediaCrypto:Landroid/media/MediaCrypto;

    .line 161
    const/4 v1, 0x3

    iput v1, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->state:I

    .line 162
    iget-object v1, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->widevineMediaDrmWrapper:Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;

    const-string v2, "LoadLicenceAfterOpening"

    iget-object v3, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->licenseCallback:Lcom/google/android/videos/async/Callback;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->loadPlaybackLicense(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    :try_end_0
    .catch Landroid/media/MediaCryptoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    :goto_0
    return-void

    .line 163
    :catch_0
    move-exception v0

    .line 164
    .local v0, "e":Landroid/media/MediaCryptoException;
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->onError(Ljava/lang/Exception;Z)V

    goto :goto_0
.end method

.method private onProvisioned()V
    .locals 3

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->widevineMediaDrmWrapper:Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->licenseCallback:Lcom/google/android/videos/async/Callback;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->loadPlaybackLicense(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 181
    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 170
    iget v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->openCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->openCount:I

    if-eqz v0, :cond_0

    .line 176
    :goto_0
    return-void

    .line 173
    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->state:I

    .line 174
    iget-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->widevineMediaDrmWrapper:Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;

    invoke-virtual {v0}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->close()V

    .line 175
    iget-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->playbackHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public getError()Ljava/lang/Exception;
    .locals 1

    .prologue
    .line 124
    iget v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->state:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->lastException:Ljava/lang/Exception;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMediaCrypto()Landroid/media/MediaCrypto;
    .locals 2

    .prologue
    .line 106
    iget v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->state:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->state:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 107
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->mediaCrypto:Landroid/media/MediaCrypto;

    return-object v0
.end method

.method public getSecurityLevel()I
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->widevineMediaDrmWrapper:Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;

    invoke-virtual {v0}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->getSecurityLevel()I

    move-result v0

    return v0
.end method

.method public getState()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->state:I

    return v0
.end method

.method public handleMessage(ILjava/lang/Object;)V
    .locals 4
    .param p1, "messageType"    # I
    .param p2, "message"    # Ljava/lang/Object;

    .prologue
    .line 129
    if-nez p1, :cond_1

    move-object v1, p2

    .line 131
    check-cast v1, Landroid/util/Pair;

    .line 132
    .local v1, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/util/Map<Ljava/util/UUID;[B>;Ljava/lang/String;>;"
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/util/Map;

    .line 133
    .local v2, "psshData":Ljava/util/Map;, "Ljava/util/Map<Ljava/util/UUID;[B>;"
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 134
    .local v0, "mimeType":Ljava/lang/String;
    invoke-virtual {p0, v2, v0}, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->open(Ljava/util/Map;Ljava/lang/String;)V

    .line 138
    .end local v0    # "mimeType":Ljava/lang/String;
    .end local v1    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/util/Map<Ljava/util/UUID;[B>;Ljava/lang/String;>;"
    .end local v2    # "psshData":Ljava/util/Map;, "Ljava/util/Map<Ljava/util/UUID;[B>;"
    :cond_0
    :goto_0
    return-void

    .line 135
    :cond_1
    const/4 v3, 0x1

    if-ne p1, v3, :cond_0

    .line 136
    invoke-virtual {p0}, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->close()V

    goto :goto_0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x3

    .line 194
    iget v1, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->state:I

    if-eq v1, v3, :cond_1

    iget v1, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->state:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    .line 207
    :cond_0
    :goto_0
    return v4

    .line 197
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    .line 198
    .local v0, "event":I
    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 199
    iget-object v1, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->widevineMediaDrmWrapper:Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;

    const-string v2, "EventKeyRequired"

    iget-object v3, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->licenseCallback:Lcom/google/android/videos/async/Callback;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->loadPlaybackLicense(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    goto :goto_0

    .line 200
    :cond_2
    if-ne v0, v3, :cond_3

    .line 201
    iput v3, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->state:I

    .line 202
    iget-object v1, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->widevineMediaDrmWrapper:Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;

    const-string v2, "EventKeyExpired"

    iget-object v3, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->licenseCallback:Lcom/google/android/videos/async/Callback;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->loadPlaybackLicense(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    goto :goto_0

    .line 203
    :cond_3
    if-ne v0, v4, :cond_0

    .line 204
    iput v3, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->state:I

    .line 205
    iget-object v1, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->widevineMediaDrmWrapper:Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;

    const-string v2, "EventProvisionRequired"

    iget-object v3, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->provisioningCallback:Lcom/google/android/videos/async/Callback;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->requestProvisioning(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    goto :goto_0
.end method

.method public onEvent(Landroid/media/MediaDrm;[BII[B)V
    .locals 1
    .param p1, "md"    # Landroid/media/MediaDrm;
    .param p2, "sessionId"    # [B
    .param p3, "event"    # I
    .param p4, "extra"    # I
    .param p5, "data"    # [B

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->playbackHandler:Landroid/os/Handler;

    invoke-virtual {v0, p3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 190
    return-void
.end method

.method public open(Ljava/util/Map;Ljava/lang/String;)V
    .locals 3
    .param p2, "mimeType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/util/UUID;",
            "[B>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 142
    .local p1, "psshData":Ljava/util/Map;, "Ljava/util/Map<Ljava/util/UUID;[B>;"
    iget v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->openCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->openCount:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 143
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->state:I

    .line 144
    iget-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->widevineMediaDrmWrapper:Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;

    const-string v1, "OpenSession"

    iget-object v2, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->openCallback:Lcom/google/android/videos/async/Callback;

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->open(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 146
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->isStreaming:Z

    if-eqz v0, :cond_1

    .line 147
    iget-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getLocalExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$1;-><init>(Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;Ljava/util/Map;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 155
    :cond_1
    return-void
.end method

.method public requiresSecureDecoderComponent(Ljava/lang/String;)Z
    .locals 3
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x3

    .line 114
    iget v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->state:I

    if-eq v0, v2, :cond_0

    iget v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->state:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 115
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 118
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->getSecurityLevel()I

    move-result v0

    if-eq v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->mediaCrypto:Landroid/media/MediaCrypto;

    invoke-virtual {v0, p1}, Landroid/media/MediaCrypto;->requiresSecureDecoderComponent(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
