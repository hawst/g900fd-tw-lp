.class Lcom/google/android/videos/pinning/LastPlaybackSaver$1;
.super Ljava/lang/Object;
.source "LastPlaybackSaver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/pinning/LastPlaybackSaver;->persistLastPlayback(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pinning/LastPlaybackSaver;

.field final synthetic val$finalSeasonId:Ljava/lang/String;

.field final synthetic val$finalShowId:Ljava/lang/String;

.field final synthetic val$finalVideoId:Ljava/lang/String;

.field final synthetic val$values:Landroid/content/ContentValues;


# direct methods
.method constructor <init>(Lcom/google/android/videos/pinning/LastPlaybackSaver;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver$1;->this$0:Lcom/google/android/videos/pinning/LastPlaybackSaver;

    iput-object p2, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver$1;->val$values:Landroid/content/ContentValues;

    iput-object p3, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver$1;->val$finalVideoId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver$1;->val$finalSeasonId:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver$1;->val$finalShowId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const/16 v11, 0xc

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 84
    iget-object v2, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver$1;->this$0:Lcom/google/android/videos/pinning/LastPlaybackSaver;

    # getter for: Lcom/google/android/videos/pinning/LastPlaybackSaver;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v2}, Lcom/google/android/videos/pinning/LastPlaybackSaver;->access$000(Lcom/google/android/videos/pinning/LastPlaybackSaver;)Lcom/google/android/videos/store/Database;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 85
    .local v1, "transaction":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v0, 0x0

    .line 87
    .local v0, "success":Z
    :try_start_0
    const-string v2, "purchased_assets"

    iget-object v3, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver$1;->val$values:Landroid/content/ContentValues;

    const-string v4, "account = ? AND asset_type IN (6,20) AND asset_id = ?"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver$1;->this$0:Lcom/google/android/videos/pinning/LastPlaybackSaver;

    # getter for: Lcom/google/android/videos/pinning/LastPlaybackSaver;->account:Ljava/lang/String;
    invoke-static {v7}, Lcom/google/android/videos/pinning/LastPlaybackSaver;->access$100(Lcom/google/android/videos/pinning/LastPlaybackSaver;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver$1;->val$finalVideoId:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 90
    iget-object v2, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver$1;->this$0:Lcom/google/android/videos/pinning/LastPlaybackSaver;

    # getter for: Lcom/google/android/videos/pinning/LastPlaybackSaver;->account:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/videos/pinning/LastPlaybackSaver;->access$100(Lcom/google/android/videos/pinning/LastPlaybackSaver;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver$1;->val$finalVideoId:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/google/android/videos/store/UserAssetsUtil;->refreshVideoRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    iget-object v2, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver$1;->val$finalSeasonId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver$1;->val$finalShowId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 92
    iget-object v2, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver$1;->this$0:Lcom/google/android/videos/pinning/LastPlaybackSaver;

    # getter for: Lcom/google/android/videos/pinning/LastPlaybackSaver;->account:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/videos/pinning/LastPlaybackSaver;->access$100(Lcom/google/android/videos/pinning/LastPlaybackSaver;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver$1;->val$finalSeasonId:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/google/android/videos/store/UserAssetsUtil;->refreshSeasonRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    iget-object v2, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver$1;->this$0:Lcom/google/android/videos/pinning/LastPlaybackSaver;

    # getter for: Lcom/google/android/videos/pinning/LastPlaybackSaver;->account:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/videos/pinning/LastPlaybackSaver;->access$100(Lcom/google/android/videos/pinning/LastPlaybackSaver;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver$1;->val$finalShowId:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/google/android/videos/store/UserAssetsUtil;->refreshShowRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    :cond_0
    const/4 v0, 0x1

    .line 97
    iget-object v2, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver$1;->this$0:Lcom/google/android/videos/pinning/LastPlaybackSaver;

    # getter for: Lcom/google/android/videos/pinning/LastPlaybackSaver;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v2}, Lcom/google/android/videos/pinning/LastPlaybackSaver;->access$000(Lcom/google/android/videos/pinning/LastPlaybackSaver;)Lcom/google/android/videos/store/Database;

    move-result-object v2

    new-array v3, v10, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver$1;->this$0:Lcom/google/android/videos/pinning/LastPlaybackSaver;

    # getter for: Lcom/google/android/videos/pinning/LastPlaybackSaver;->account:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/videos/pinning/LastPlaybackSaver;->access$100(Lcom/google/android/videos/pinning/LastPlaybackSaver;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    iget-object v4, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver$1;->val$finalVideoId:Ljava/lang/String;

    aput-object v4, v3, v9

    invoke-virtual {v2, v1, v0, v11, v3}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 100
    iget-object v2, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver$1;->this$0:Lcom/google/android/videos/pinning/LastPlaybackSaver;

    # getter for: Lcom/google/android/videos/pinning/LastPlaybackSaver;->context:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/videos/pinning/LastPlaybackSaver;->access$200(Lcom/google/android/videos/pinning/LastPlaybackSaver;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver$1;->this$0:Lcom/google/android/videos/pinning/LastPlaybackSaver;

    # getter for: Lcom/google/android/videos/pinning/LastPlaybackSaver;->context:Landroid/content/Context;
    invoke-static {v3}, Lcom/google/android/videos/pinning/LastPlaybackSaver;->access$200(Lcom/google/android/videos/pinning/LastPlaybackSaver;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/videos/pinning/TransferService;->createIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 102
    return-void

    .line 97
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver$1;->this$0:Lcom/google/android/videos/pinning/LastPlaybackSaver;

    # getter for: Lcom/google/android/videos/pinning/LastPlaybackSaver;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v3}, Lcom/google/android/videos/pinning/LastPlaybackSaver;->access$000(Lcom/google/android/videos/pinning/LastPlaybackSaver;)Lcom/google/android/videos/store/Database;

    move-result-object v3

    new-array v4, v10, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver$1;->this$0:Lcom/google/android/videos/pinning/LastPlaybackSaver;

    # getter for: Lcom/google/android/videos/pinning/LastPlaybackSaver;->account:Ljava/lang/String;
    invoke-static {v5}, Lcom/google/android/videos/pinning/LastPlaybackSaver;->access$100(Lcom/google/android/videos/pinning/LastPlaybackSaver;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    iget-object v5, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver$1;->val$finalVideoId:Ljava/lang/String;

    aput-object v5, v4, v9

    invoke-virtual {v3, v1, v0, v11, v4}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 100
    iget-object v3, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver$1;->this$0:Lcom/google/android/videos/pinning/LastPlaybackSaver;

    # getter for: Lcom/google/android/videos/pinning/LastPlaybackSaver;->context:Landroid/content/Context;
    invoke-static {v3}, Lcom/google/android/videos/pinning/LastPlaybackSaver;->access$200(Lcom/google/android/videos/pinning/LastPlaybackSaver;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver$1;->this$0:Lcom/google/android/videos/pinning/LastPlaybackSaver;

    # getter for: Lcom/google/android/videos/pinning/LastPlaybackSaver;->context:Landroid/content/Context;
    invoke-static {v4}, Lcom/google/android/videos/pinning/LastPlaybackSaver;->access$200(Lcom/google/android/videos/pinning/LastPlaybackSaver;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/videos/pinning/TransferService;->createIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    throw v2
.end method
