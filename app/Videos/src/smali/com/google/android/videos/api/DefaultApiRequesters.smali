.class public Lcom/google/android/videos/api/DefaultApiRequesters;
.super Ljava/lang/Object;
.source "DefaultApiRequesters.java"

# interfaces
.implements Lcom/google/android/videos/api/ApiRequesters;


# instance fields
.field private final assetsCachingRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final assetsRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final assetsSyncRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final bytesAsyncRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "[B>;"
        }
    .end annotation
.end field

.field private final categoryListRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/CategoryListRequest;",
            "Lcom/google/wireless/android/video/magma/proto/CategoryListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final cencLicenseAsyncRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/CencLicenseRequest;",
            "[B>;"
        }
    .end annotation
.end field

.field private final conditionalEntitySyncRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/ConditionalHttpRequest",
            "<",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;",
            "Lcom/google/android/videos/async/ConditionalHttpResponse",
            "<",
            "Lorg/apache/http/HttpEntity;",
            ">;>;"
        }
    .end annotation
.end field

.field private final entitySyncRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "Lorg/apache/http/HttpEntity;",
            ">;"
        }
    .end annotation
.end field

.field private final gcmCreateNotificationKeySyncRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/GcmCreateUserNotificationKeyRequest;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final gcmRegisterSyncRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/GcmRegisterRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final mpdUrlGetRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/MpdUrlGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final promotionsRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/PromotionsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final recommendationsRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/RecommendationsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final redeemPromotionRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/RedeemPromotionRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final reviewsRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetReviewListRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final robotTokenAsyncRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/RobotTokenRequest;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final streamRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/MpdGetRequest;",
            "Lcom/google/android/videos/streams/Streams;",
            ">;"
        }
    .end annotation
.end field

.field private final unlinkAccountRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/LinkedAccountRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final updateAccountLinkingRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/LinkedAccountRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final updateAssetVisibilitySyncRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/UpdateAssetVisibilityRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final updateWishlistSyncRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/UpdateWishlistRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final userConfigGetRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/UserConfigGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final userConfigGetSyncRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/UserConfigGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final userLibraryRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/UserLibraryRequest;",
            "Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final userLibrarySyncRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/UserLibraryRequest;",
            "Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final videoCollectionGetRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/VideoCollectionGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoCollectionGetResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final videoCollectionListRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/VideoCollectionListRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoCollectionListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final videoFormatsAsyncRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/EmptyRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/wireless/android/video/magma/proto/VideoFormat;",
            ">;>;"
        }
    .end annotation
.end field

.field private final videoGetRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/VideoGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
            ">;"
        }
    .end annotation
.end field

.field private final videoUpdateRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/VideoUpdateRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
            ">;"
        }
    .end annotation
.end field

.field private final wishlistSyncRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/GetWishlistRequest;",
            "Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/videos/accounts/AccountManagerWrapper;Ljava/util/concurrent/Executor;Lcom/google/android/videos/utils/ByteArrayPool;Ljava/lang/String;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/Config;Lcom/google/android/videos/store/ItagInfoStore;)V
    .locals 39
    .param p1, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .param p2, "executor"    # Ljava/util/concurrent/Executor;
    .param p3, "byteArrayPool"    # Lcom/google/android/videos/utils/ByteArrayPool;
    .param p4, "persistentCachePath"    # Ljava/lang/String;
    .param p5, "httpClient"    # Lorg/apache/http/client/HttpClient;
    .param p6, "config"    # Lcom/google/android/videos/Config;
    .param p7, "itagInfoStore"    # Lcom/google/android/videos/store/ItagInfoStore;

    .prologue
    .line 113
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 114
    invoke-static/range {p1 .. p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    invoke-static/range {p2 .. p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    invoke-static/range {p4 .. p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 117
    invoke-static/range {p5 .. p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    invoke-interface/range {p6 .. p6}, Lcom/google/android/videos/Config;->baseApiUri()Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/net/Uri;

    .line 120
    .local v15, "baseApiUri":Landroid/net/Uri;
    sget-object v4, Lcom/google/android/videos/converter/HttpResponseConverter;->IDENTITY:Lcom/google/android/videos/converter/HttpResponseConverter;

    move-object/from16 v0, p5

    invoke-static {v0, v4}, Lcom/google/android/videos/async/HttpRequester;->create(Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/async/HttpRequester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->entitySyncRequester:Lcom/google/android/videos/async/Requester;

    .line 122
    invoke-static {}, Lcom/google/android/videos/async/ConditionalHttpRequest;->createConverter()Lcom/google/android/videos/converter/RequestConverter;

    move-result-object v4

    sget-object v5, Lcom/google/android/videos/converter/HttpResponseConverter;->IDENTITY:Lcom/google/android/videos/converter/HttpResponseConverter;

    invoke-static {v5}, Lcom/google/android/videos/async/ConditionalHttpResponse;->createConverter(Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/converter/HttpResponseConverter;

    move-result-object v5

    move-object/from16 v0, p5

    invoke-static {v0, v4, v5}, Lcom/google/android/videos/async/HttpRequester;->create(Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/RequestConverter;Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/async/HttpRequester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->conditionalEntitySyncRequester:Lcom/google/android/videos/async/Requester;

    .line 126
    sget-object v4, Lcom/google/android/videos/converter/HttpResponseConverter;->BYTE_ARRAY:Lcom/google/android/videos/converter/HttpResponseConverter;

    move-object/from16 v0, p5

    invoke-static {v0, v4}, Lcom/google/android/videos/async/HttpRequester;->create(Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/async/HttpRequester;

    move-result-object v26

    .line 128
    .local v26, "bytesSyncRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lorg/apache/http/client/methods/HttpUriRequest;[B>;"
    move-object/from16 v0, p2

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lcom/google/android/videos/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/AsyncRequester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->bytesAsyncRequester:Lcom/google/android/videos/async/Requester;

    .line 130
    new-instance v4, Lcom/google/android/videos/api/UserConfigGetConverter;

    move-object/from16 v0, p3

    invoke-direct {v4, v15, v0}, Lcom/google/android/videos/api/UserConfigGetConverter;-><init>(Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArrayPool;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p5

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/videos/api/DefaultApiRequesters;->createApiRequester(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/async/Requester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->userConfigGetSyncRequester:Lcom/google/android/videos/async/Requester;

    .line 132
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->userConfigGetSyncRequester:Lcom/google/android/videos/async/Requester;

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Lcom/google/android/videos/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/AsyncRequester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->userConfigGetRequester:Lcom/google/android/videos/async/Requester;

    .line 134
    new-instance v8, Lcom/google/android/videos/api/CategoryListConverter;

    move-object/from16 v0, p3

    invoke-direct {v8, v15, v0}, Lcom/google/android/videos/api/CategoryListConverter;-><init>(Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArrayPool;)V

    const-string v10, ".cl"

    const-class v11, Lcom/google/wireless/android/video/magma/proto/CategoryListResponse;

    const-wide/32 v12, 0x240c8400

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p5

    move-object/from16 v9, p4

    invoke-direct/range {v4 .. v13}, Lcom/google/android/videos/api/DefaultApiRequesters;->createFileCachingRequester(Lcom/google/android/videos/accounts/AccountManagerWrapper;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/HttpResponseConverter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;J)Lcom/google/android/videos/async/Requester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->categoryListRequester:Lcom/google/android/videos/async/Requester;

    .line 144
    new-instance v4, Lcom/google/android/videos/api/VideoGetConverter;

    move-object/from16 v0, p3

    invoke-direct {v4, v15, v0}, Lcom/google/android/videos/api/VideoGetConverter;-><init>(Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArrayPool;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p5

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/videos/api/DefaultApiRequesters;->createApiRequester(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/async/Requester;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Lcom/google/android/videos/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/AsyncRequester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->videoGetRequester:Lcom/google/android/videos/async/Requester;

    .line 147
    new-instance v4, Lcom/google/android/videos/api/VideoUpdateConverter;

    move-object/from16 v0, p3

    invoke-direct {v4, v15, v0}, Lcom/google/android/videos/api/VideoUpdateConverter;-><init>(Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArrayPool;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p5

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/videos/api/DefaultApiRequesters;->createApiRequester(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/async/Requester;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Lcom/google/android/videos/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/AsyncRequester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->videoUpdateRequester:Lcom/google/android/videos/async/Requester;

    .line 150
    new-instance v4, Lcom/google/android/videos/api/GcmRegisterConverter;

    invoke-interface/range {p6 .. p6}, Lcom/google/android/videos/Config;->gservicesId()J

    move-result-wide v6

    invoke-direct {v4, v15, v6, v7}, Lcom/google/android/videos/api/GcmRegisterConverter;-><init>(Landroid/net/Uri;J)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p5

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/videos/api/DefaultApiRequesters;->createVoidResponseApiRequester(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/RequestConverter;)Lcom/google/android/videos/async/Requester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->gcmRegisterSyncRequester:Lcom/google/android/videos/async/Requester;

    .line 153
    new-instance v29, Lcom/google/android/videos/api/GcmCreateNotificationKeyConverter;

    invoke-direct/range {v29 .. v29}, Lcom/google/android/videos/api/GcmCreateNotificationKeyConverter;-><init>()V

    .line 155
    .local v29, "gcmCreateNotificationKeyConverter":Lcom/google/android/videos/api/GcmCreateNotificationKeyConverter;
    move-object/from16 v0, p5

    move-object/from16 v1, v29

    move-object/from16 v2, v29

    invoke-static {v0, v1, v2}, Lcom/google/android/videos/async/HttpRequester;->create(Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/RequestConverter;Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/async/HttpRequester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->gcmCreateNotificationKeySyncRequester:Lcom/google/android/videos/async/Requester;

    .line 158
    new-instance v4, Lcom/google/android/videos/api/MpdUrlGetConverter;

    invoke-interface/range {p6 .. p6}, Lcom/google/android/videos/Config;->multiAudioEnabled()Z

    move-result v5

    move-object/from16 v0, p3

    invoke-direct {v4, v15, v0, v5}, Lcom/google/android/videos/api/MpdUrlGetConverter;-><init>(Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArrayPool;Z)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p5

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/videos/api/DefaultApiRequesters;->createApiRequester(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/async/Requester;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Lcom/google/android/videos/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/AsyncRequester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->mpdUrlGetRequester:Lcom/google/android/videos/async/Requester;

    .line 161
    new-instance v4, Lcom/google/android/videos/api/GetWishlistConverter;

    move-object/from16 v0, p3

    invoke-direct {v4, v15, v0}, Lcom/google/android/videos/api/GetWishlistConverter;-><init>(Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArrayPool;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p5

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/videos/api/DefaultApiRequesters;->createApiRequester(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/async/Requester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->wishlistSyncRequester:Lcom/google/android/videos/async/Requester;

    .line 164
    new-instance v4, Lcom/google/android/videos/api/UpdateWishlistConverter;

    invoke-direct {v4, v15}, Lcom/google/android/videos/api/UpdateWishlistConverter;-><init>(Landroid/net/Uri;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p5

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/videos/api/DefaultApiRequesters;->createVoidResponseApiRequester(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/RequestConverter;)Lcom/google/android/videos/async/Requester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->updateWishlistSyncRequester:Lcom/google/android/videos/async/Requester;

    .line 166
    new-instance v4, Lcom/google/android/videos/api/UpdateAssetVisibilityConverter;

    invoke-direct {v4, v15}, Lcom/google/android/videos/api/UpdateAssetVisibilityConverter;-><init>(Landroid/net/Uri;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p5

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/videos/api/DefaultApiRequesters;->createVoidResponseApiRequester(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/RequestConverter;)Lcom/google/android/videos/async/Requester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->updateAssetVisibilitySyncRequester:Lcom/google/android/videos/async/Requester;

    .line 169
    new-instance v4, Lcom/google/android/videos/api/AssetListConverter;

    move-object/from16 v0, p3

    invoke-direct {v4, v15, v0}, Lcom/google/android/videos/api/AssetListConverter;-><init>(Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArrayPool;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p5

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/videos/api/DefaultApiRequesters;->createApiRequester(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/async/Requester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->assetsSyncRequester:Lcom/google/android/videos/async/Requester;

    .line 172
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->assetsSyncRequester:Lcom/google/android/videos/async/Requester;

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Lcom/google/android/videos/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/AsyncRequester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->assetsRequester:Lcom/google/android/videos/async/Requester;

    .line 174
    new-instance v8, Lcom/google/android/videos/api/AssetListConverter;

    move-object/from16 v0, p3

    invoke-direct {v8, v15, v0}, Lcom/google/android/videos/api/AssetListConverter;-><init>(Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArrayPool;)V

    const-string v10, ".al"

    const-class v11, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    const-wide/32 v12, 0xa4cb80

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p5

    move-object/from16 v9, p4

    invoke-direct/range {v4 .. v13}, Lcom/google/android/videos/api/DefaultApiRequesters;->createFileCachingRequester(Lcom/google/android/videos/accounts/AccountManagerWrapper;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/HttpResponseConverter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;J)Lcom/google/android/videos/async/Requester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->assetsCachingRequester:Lcom/google/android/videos/async/Requester;

    .line 184
    new-instance v8, Lcom/google/android/videos/api/AssetReviewListConverter;

    move-object/from16 v0, p3

    invoke-direct {v8, v15, v0}, Lcom/google/android/videos/api/AssetReviewListConverter;-><init>(Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArrayPool;)V

    const-string v10, ".vr"

    const-class v11, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;

    const-wide/32 v12, 0x5265c00

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p5

    move-object/from16 v9, p4

    invoke-direct/range {v4 .. v13}, Lcom/google/android/videos/api/DefaultApiRequesters;->createFileCachingRequester(Lcom/google/android/videos/accounts/AccountManagerWrapper;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/HttpResponseConverter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;J)Lcom/google/android/videos/async/Requester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->reviewsRequester:Lcom/google/android/videos/async/Requester;

    .line 194
    new-instance v4, Lcom/google/android/videos/api/UserLibraryListConverter;

    move-object/from16 v0, p3

    invoke-direct {v4, v15, v0}, Lcom/google/android/videos/api/UserLibraryListConverter;-><init>(Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArrayPool;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p5

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/videos/api/DefaultApiRequesters;->createApiRequester(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/async/Requester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->userLibrarySyncRequester:Lcom/google/android/videos/async/Requester;

    .line 197
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->userLibrarySyncRequester:Lcom/google/android/videos/async/Requester;

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Lcom/google/android/videos/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/AsyncRequester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->userLibraryRequester:Lcom/google/android/videos/async/Requester;

    .line 199
    new-instance v8, Lcom/google/android/videos/api/RecommendationsConverter;

    move-object/from16 v0, p3

    invoke-direct {v8, v15, v0}, Lcom/google/android/videos/api/RecommendationsConverter;-><init>(Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArrayPool;)V

    .line 201
    .local v8, "recommendationsConverter":Lcom/google/android/videos/api/RecommendationsConverter;
    const-string v10, ".rec"

    const-class v11, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;

    const-wide/32 v12, 0x5265c00

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p5

    move-object/from16 v9, p4

    invoke-direct/range {v4 .. v13}, Lcom/google/android/videos/api/DefaultApiRequesters;->createFileCachingRequester(Lcom/google/android/videos/accounts/AccountManagerWrapper;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/HttpResponseConverter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;J)Lcom/google/android/videos/async/Requester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->recommendationsRequester:Lcom/google/android/videos/async/Requester;

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-object/from16 v11, p2

    move-object/from16 v12, p3

    move-object/from16 v13, p4

    move-object/from16 v14, p5

    .line 212
    invoke-direct/range {v9 .. v15}, Lcom/google/android/videos/api/DefaultApiRequesters;->createCachingPromotionsRequester(Lcom/google/android/videos/accounts/AccountManagerWrapper;Ljava/util/concurrent/Executor;Lcom/google/android/videos/utils/ByteArrayPool;Ljava/lang/String;Lorg/apache/http/client/HttpClient;Landroid/net/Uri;)Lcom/google/android/videos/async/Requester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->promotionsRequester:Lcom/google/android/videos/async/Requester;

    .line 221
    new-instance v4, Lcom/google/android/videos/api/RedeemPromotionConverter;

    invoke-direct {v4, v15}, Lcom/google/android/videos/api/RedeemPromotionConverter;-><init>(Landroid/net/Uri;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p5

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/videos/api/DefaultApiRequesters;->createVoidResponseApiRequester(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/RequestConverter;)Lcom/google/android/videos/async/Requester;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Lcom/google/android/videos/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/AsyncRequester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->redeemPromotionRequester:Lcom/google/android/videos/async/Requester;

    .line 224
    new-instance v37, Lcom/google/android/videos/api/VideoFormatsGetConverter;

    move-object/from16 v0, v37

    move-object/from16 v1, p3

    invoke-direct {v0, v15, v1}, Lcom/google/android/videos/api/VideoFormatsGetConverter;-><init>(Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArrayPool;)V

    .line 226
    .local v37, "videoFormatsGetConverter":Lcom/google/android/videos/api/VideoFormatsGetConverter;
    move-object/from16 v0, p5

    move-object/from16 v1, v37

    invoke-static {v0, v1}, Lcom/google/android/videos/async/HttpRequester;->create(Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/async/HttpRequester;

    move-result-object v36

    .line 228
    .local v36, "videoFormatHttpRequester":Lcom/google/android/videos/async/HttpRequester;, "Lcom/google/android/videos/async/HttpRequester<Lorg/apache/http/client/methods/HttpUriRequest;Ljava/util/Map<Ljava/lang/Integer;Lcom/google/wireless/android/video/magma/proto/VideoFormat;>;>;"
    invoke-static/range {v36 .. v37}, Lcom/google/android/videos/async/ConvertingRequester;->create(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/converter/RequestConverter;)Lcom/google/android/videos/async/Requester;

    move-result-object v38

    .line 230
    .local v38, "videoFormatsSyncRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/async/EmptyRequest;Ljava/util/Map<Ljava/lang/Integer;Lcom/google/wireless/android/video/magma/proto/VideoFormat;>;>;"
    move-object/from16 v0, p2

    move-object/from16 v1, v38

    invoke-static {v0, v1}, Lcom/google/android/videos/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/AsyncRequester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->videoFormatsAsyncRequester:Lcom/google/android/videos/async/Requester;

    .line 232
    new-instance v4, Lcom/google/android/videos/api/MpdGetConverter;

    invoke-interface/range {p6 .. p6}, Lcom/google/android/videos/Config;->multiAudioEnabled()Z

    move-result v5

    move-object/from16 v0, p3

    invoke-direct {v4, v15, v0, v5}, Lcom/google/android/videos/api/MpdGetConverter;-><init>(Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArrayPool;Z)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p5

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/videos/api/DefaultApiRequesters;->createApiRequester(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/async/Requester;

    move-result-object v33

    .line 235
    .local v33, "streamsNonCachingRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;>;"
    const/16 v4, 0x14

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/videos/api/DefaultApiRequesters;->newInMemoryCache(I)Lcom/google/android/videos/cache/InMemoryLruCache;

    move-result-object v32

    .line 237
    .local v32, "streamsMemoryCache":Lcom/google/android/videos/cache/InMemoryLruCache;, "Lcom/google/android/videos/cache/InMemoryLruCache<Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/android/videos/async/Timestamped<Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;>;>;"
    new-instance v4, Lcom/google/android/videos/api/ItagStreamRequester;

    const-wide/32 v6, 0x6ddd00

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    move-object/from16 v2, v33

    invoke-direct {v0, v1, v2, v6, v7}, Lcom/google/android/videos/api/DefaultApiRequesters;->newCachingRequester(Lcom/google/android/videos/cache/Cache;Lcom/google/android/videos/async/Requester;J)Lcom/google/android/videos/async/Requester;

    move-result-object v5

    move-object/from16 v0, p7

    move-object/from16 v1, p6

    invoke-direct {v4, v0, v5, v1}, Lcom/google/android/videos/api/ItagStreamRequester;-><init>(Lcom/google/android/videos/store/ItagInfoStore;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/Config;)V

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Lcom/google/android/videos/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/AsyncRequester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->streamRequester:Lcom/google/android/videos/async/Requester;

    .line 242
    new-instance v4, Lcom/google/android/videos/api/RobotTokenResponseConverter;

    invoke-direct {v4}, Lcom/google/android/videos/api/RobotTokenResponseConverter;-><init>()V

    move-object/from16 v0, p5

    invoke-static {v0, v4}, Lcom/google/android/videos/async/HttpRequester;->create(Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/async/HttpRequester;

    move-result-object v30

    .line 244
    .local v30, "robotTokenHttpRequester":Lcom/google/android/videos/async/HttpRequester;, "Lcom/google/android/videos/async/HttpRequester<Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;>;"
    new-instance v31, Lcom/google/android/videos/api/RobotTokenAuthenticatingRequester;

    invoke-interface/range {p6 .. p6}, Lcom/google/android/videos/Config;->atHomeRobotTokenRequestUri()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    move-object/from16 v0, v31

    move-object/from16 v1, p1

    move-object/from16 v2, v30

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/videos/api/RobotTokenAuthenticatingRequester;-><init>(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/async/Requester;Landroid/net/Uri;)V

    .line 247
    .local v31, "robotTokenSyncRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/RobotTokenRequest;Ljava/lang/String;>;"
    move-object/from16 v0, p2

    move-object/from16 v1, v31

    invoke-static {v0, v1}, Lcom/google/android/videos/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/AsyncRequester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->robotTokenAsyncRequester:Lcom/google/android/videos/async/Requester;

    .line 249
    new-instance v4, Lcom/google/android/videos/api/CencLicenseResponseConverter;

    invoke-direct {v4}, Lcom/google/android/videos/api/CencLicenseResponseConverter;-><init>()V

    move-object/from16 v0, p5

    invoke-static {v0, v4}, Lcom/google/android/videos/async/HttpRequester;->create(Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/async/HttpRequester;

    move-result-object v27

    .line 251
    .local v27, "cencLicenseHttpRequester":Lcom/google/android/videos/async/HttpRequester;, "Lcom/google/android/videos/async/HttpRequester<Lorg/apache/http/client/methods/HttpUriRequest;[B>;"
    new-instance v28, Lcom/google/android/videos/api/CencLicenseAuthenticatingRequester;

    move-object/from16 v0, v28

    move-object/from16 v1, p1

    move-object/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lcom/google/android/videos/api/CencLicenseAuthenticatingRequester;-><init>(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/async/Requester;)V

    .line 253
    .local v28, "cencLicenseSyncRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/CencLicenseRequest;[B>;"
    move-object/from16 v0, p2

    move-object/from16 v1, v28

    invoke-static {v0, v1}, Lcom/google/android/videos/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/AsyncRequester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->cencLicenseAsyncRequester:Lcom/google/android/videos/async/Requester;

    .line 255
    new-instance v20, Lcom/google/android/videos/api/VideoCollectionListConverter;

    move-object/from16 v0, v20

    move-object/from16 v1, p3

    invoke-direct {v0, v15, v1}, Lcom/google/android/videos/api/VideoCollectionListConverter;-><init>(Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArrayPool;)V

    const-string v22, ".vc"

    const-class v23, Lcom/google/wireless/android/video/magma/proto/VideoCollectionListResponse;

    const-wide/32 v24, 0x5265c00

    move-object/from16 v16, p0

    move-object/from16 v17, p1

    move-object/from16 v18, p2

    move-object/from16 v19, p5

    move-object/from16 v21, p4

    invoke-direct/range {v16 .. v25}, Lcom/google/android/videos/api/DefaultApiRequesters;->createFileCachingRequester(Lcom/google/android/videos/accounts/AccountManagerWrapper;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/HttpResponseConverter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;J)Lcom/google/android/videos/async/Requester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->videoCollectionListRequester:Lcom/google/android/videos/async/Requester;

    .line 265
    new-instance v20, Lcom/google/android/videos/api/VideoCollectionGetConverter;

    move-object/from16 v0, v20

    move-object/from16 v1, p3

    invoke-direct {v0, v15, v1}, Lcom/google/android/videos/api/VideoCollectionGetConverter;-><init>(Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArrayPool;)V

    const-string v22, ".vc"

    const-class v23, Lcom/google/wireless/android/video/magma/proto/VideoCollectionGetResponse;

    const-wide/32 v24, 0x5265c00

    move-object/from16 v16, p0

    move-object/from16 v17, p1

    move-object/from16 v18, p2

    move-object/from16 v19, p5

    move-object/from16 v21, p4

    invoke-direct/range {v16 .. v25}, Lcom/google/android/videos/api/DefaultApiRequesters;->createFileCachingRequester(Lcom/google/android/videos/accounts/AccountManagerWrapper;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/HttpResponseConverter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;J)Lcom/google/android/videos/async/Requester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->videoCollectionGetRequester:Lcom/google/android/videos/async/Requester;

    .line 275
    new-instance v4, Lcom/google/android/videos/api/UnlinkAccountConverter;

    invoke-direct {v4, v15}, Lcom/google/android/videos/api/UnlinkAccountConverter;-><init>(Landroid/net/Uri;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p5

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/videos/api/DefaultApiRequesters;->createApiRequester(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/async/Requester;

    move-result-object v34

    .line 277
    .local v34, "unlinkAccountSyncRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/LinkedAccountRequest;Ljava/lang/Void;>;"
    move-object/from16 v0, p2

    move-object/from16 v1, v34

    invoke-static {v0, v1}, Lcom/google/android/videos/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/AsyncRequester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->unlinkAccountRequester:Lcom/google/android/videos/async/Requester;

    .line 279
    new-instance v4, Lcom/google/android/videos/api/UpdateAccountLinkingConverter;

    invoke-direct {v4, v15}, Lcom/google/android/videos/api/UpdateAccountLinkingConverter;-><init>(Landroid/net/Uri;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p5

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/videos/api/DefaultApiRequesters;->createApiRequester(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/async/Requester;

    move-result-object v35

    .line 282
    .local v35, "updateAccountLinkingSyncRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/LinkedAccountRequest;Ljava/lang/Void;>;"
    move-object/from16 v0, p2

    move-object/from16 v1, v35

    invoke-static {v0, v1}, Lcom/google/android/videos/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/AsyncRequester;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/api/DefaultApiRequesters;->updateAccountLinkingRequester:Lcom/google/android/videos/async/Requester;

    .line 283
    return-void
.end method

.method private createApiRequester(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/async/Requester;
    .locals 2
    .param p1, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .param p2, "httpClient"    # Lorg/apache/http/client/HttpClient;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Lcom/google/android/videos/async/Request;",
            "E:",
            "Ljava/lang/Object;",
            "C:",
            "Lcom/google/android/videos/converter/HttpResponseConverter",
            "<TE;>;:",
            "Lcom/google/android/videos/converter/RequestConverter",
            "<TR;",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;>(",
            "Lcom/google/android/videos/accounts/AccountManagerWrapper;",
            "Lorg/apache/http/client/HttpClient;",
            "TC;)",
            "Lcom/google/android/videos/async/Requester",
            "<TR;TE;>;"
        }
    .end annotation

    .prologue
    .line 501
    .local p3, "converter":Lcom/google/android/videos/converter/HttpResponseConverter;, "TC;"
    invoke-static {p2, p3}, Lcom/google/android/videos/async/HttpRequester;->create(Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/async/HttpRequester;

    move-result-object v0

    .line 502
    .local v0, "httpRequester":Lcom/google/android/videos/async/HttpRequester;, "Lcom/google/android/videos/async/HttpRequester<Lorg/apache/http/client/methods/HttpUriRequest;TE;>;"
    check-cast p3, Lcom/google/android/videos/converter/RequestConverter;

    .end local p3    # "converter":Lcom/google/android/videos/converter/HttpResponseConverter;, "TC;"
    invoke-static {p1, p3, v0}, Lcom/google/android/videos/async/HttpAuthenticatingRequester;->create(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/converter/RequestConverter;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/HttpAuthenticatingRequester;

    move-result-object v1

    return-object v1
.end method

.method private createCachingPromotionsRequester(Lcom/google/android/videos/accounts/AccountManagerWrapper;Ljava/util/concurrent/Executor;Lcom/google/android/videos/utils/ByteArrayPool;Ljava/lang/String;Lorg/apache/http/client/HttpClient;Landroid/net/Uri;)Lcom/google/android/videos/async/Requester;
    .locals 6
    .param p1, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .param p2, "executor"    # Ljava/util/concurrent/Executor;
    .param p3, "byteArrayPool"    # Lcom/google/android/videos/utils/ByteArrayPool;
    .param p4, "persistentCachePath"    # Ljava/lang/String;
    .param p5, "httpClient"    # Lorg/apache/http/client/HttpClient;
    .param p6, "baseApiUri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/accounts/AccountManagerWrapper;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/videos/utils/ByteArrayPool;",
            "Ljava/lang/String;",
            "Lorg/apache/http/client/HttpClient;",
            "Landroid/net/Uri;",
            ")",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/PromotionsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 477
    const-class v4, Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;

    invoke-static {v4}, Lcom/google/android/videos/cache/ProtoConverter;->create(Ljava/lang/Class;)Lcom/google/android/videos/cache/ProtoConverter;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/videos/cache/TimestampedConverter;->create(Lcom/google/android/videos/cache/Converter;)Lcom/google/android/videos/cache/TimestampedConverter;

    move-result-object v0

    .line 480
    .local v0, "converter":Lcom/google/android/videos/cache/Converter;, "Lcom/google/android/videos/cache/Converter<Lcom/google/android/videos/async/Timestamped<Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;>;>;"
    new-instance v4, Lcom/google/android/videos/cache/PersistentCache;

    const-string v5, ".promo"

    invoke-direct {v4, v0, p4, v5}, Lcom/google/android/videos/cache/PersistentCache;-><init>(Lcom/google/android/videos/cache/Converter;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Lcom/google/android/videos/cache/PersistentCache;->init(Ljava/util/concurrent/Executor;)Lcom/google/android/videos/cache/PersistentCache;

    move-result-object v1

    .line 484
    .local v1, "promotionsPersistentCache":Lcom/google/android/videos/cache/PersistentCache;, "Lcom/google/android/videos/cache/PersistentCache<Lcom/google/android/videos/api/PromotionsRequest;Lcom/google/android/videos/async/Timestamped<Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;>;>;"
    new-instance v4, Lcom/google/android/videos/api/PromotionsConverter;

    invoke-direct {v4, p6, p3}, Lcom/google/android/videos/api/PromotionsConverter;-><init>(Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArrayPool;)V

    invoke-direct {p0, p1, p5, v4}, Lcom/google/android/videos/api/DefaultApiRequesters;->createApiRequester(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/async/Requester;

    move-result-object v3

    .line 487
    .local v3, "syncPromotionsRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/PromotionsRequest;Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;>;"
    const-wide/32 v4, 0x5265c00

    invoke-direct {p0, v1, v3, v4, v5}, Lcom/google/android/videos/api/DefaultApiRequesters;->newCachingRequester(Lcom/google/android/videos/cache/Cache;Lcom/google/android/videos/async/Requester;J)Lcom/google/android/videos/async/Requester;

    move-result-object v2

    .line 492
    .local v2, "syncCachingRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/PromotionsRequest;Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;>;"
    invoke-static {p2, v2}, Lcom/google/android/videos/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/AsyncRequester;

    move-result-object v4

    return-object v4
.end method

.method private createFileCachingRequester(Lcom/google/android/videos/accounts/AccountManagerWrapper;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/HttpResponseConverter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;J)Lcom/google/android/videos/async/Requester;
    .locals 6
    .param p1, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .param p2, "executor"    # Ljava/util/concurrent/Executor;
    .param p3, "httpClient"    # Lorg/apache/http/client/HttpClient;
    .param p5, "persistentCachePath"    # Ljava/lang/String;
    .param p6, "cacheFileExtension"    # Ljava/lang/String;
    .param p8, "timeToLive"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Lcom/google/android/videos/async/Request;",
            "E:",
            "Lcom/google/protobuf/nano/MessageNano;",
            "C:",
            "Lcom/google/android/videos/converter/HttpResponseConverter",
            "<TE;>;:",
            "Lcom/google/android/videos/converter/RequestConverter",
            "<TR;",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;>(",
            "Lcom/google/android/videos/accounts/AccountManagerWrapper;",
            "Ljava/util/concurrent/Executor;",
            "Lorg/apache/http/client/HttpClient;",
            "TC;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TE;>;J)",
            "Lcom/google/android/videos/async/Requester",
            "<TR;TE;>;"
        }
    .end annotation

    .prologue
    .line 457
    .local p4, "converter":Lcom/google/android/videos/converter/HttpResponseConverter;, "TC;"
    .local p7, "responseClass":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    invoke-static {p7}, Lcom/google/android/videos/cache/ProtoConverter;->create(Ljava/lang/Class;)Lcom/google/android/videos/cache/ProtoConverter;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/videos/cache/TimestampedConverter;->create(Lcom/google/android/videos/cache/Converter;)Lcom/google/android/videos/cache/TimestampedConverter;

    move-result-object v3

    .line 459
    .local v3, "timpestampedConverter":Lcom/google/android/videos/cache/Converter;, "Lcom/google/android/videos/cache/Converter<Lcom/google/android/videos/async/Timestamped<TE;>;>;"
    new-instance v4, Lcom/google/android/videos/cache/PersistentCache;

    invoke-direct {v4, v3, p5, p6}, Lcom/google/android/videos/cache/PersistentCache;-><init>(Lcom/google/android/videos/cache/Converter;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Lcom/google/android/videos/cache/PersistentCache;->init(Ljava/util/concurrent/Executor;)Lcom/google/android/videos/cache/PersistentCache;

    move-result-object v0

    .line 463
    .local v0, "persistentCache":Lcom/google/android/videos/cache/PersistentCache;, "Lcom/google/android/videos/cache/PersistentCache<TR;Lcom/google/android/videos/async/Timestamped<TE;>;>;"
    invoke-direct {p0, p1, p3, p4}, Lcom/google/android/videos/api/DefaultApiRequesters;->createApiRequester(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/async/Requester;

    move-result-object v2

    .line 465
    .local v2, "syncRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TR;TE;>;"
    invoke-direct {p0, v0, v2, p8, p9}, Lcom/google/android/videos/api/DefaultApiRequesters;->newCachingRequester(Lcom/google/android/videos/cache/Cache;Lcom/google/android/videos/async/Requester;J)Lcom/google/android/videos/async/Requester;

    move-result-object v1

    .line 467
    .local v1, "syncCachingRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TR;TE;>;"
    invoke-static {p2, v1}, Lcom/google/android/videos/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/AsyncRequester;

    move-result-object v4

    return-object v4
.end method

.method private createVoidResponseApiRequester(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/RequestConverter;)Lcom/google/android/videos/async/Requester;
    .locals 2
    .param p1, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .param p2, "httpClient"    # Lorg/apache/http/client/HttpClient;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Lcom/google/android/videos/async/Request;",
            ">(",
            "Lcom/google/android/videos/accounts/AccountManagerWrapper;",
            "Lorg/apache/http/client/HttpClient;",
            "Lcom/google/android/videos/converter/RequestConverter",
            "<TR;",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;)",
            "Lcom/google/android/videos/async/Requester",
            "<TR;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 512
    .local p3, "requestConverter":Lcom/google/android/videos/converter/RequestConverter;, "Lcom/google/android/videos/converter/RequestConverter<TR;Lorg/apache/http/client/methods/HttpUriRequest;>;"
    sget-object v1, Lcom/google/android/videos/converter/HttpResponseConverter;->VOID:Lcom/google/android/videos/converter/HttpResponseConverter;

    invoke-static {p2, v1}, Lcom/google/android/videos/async/HttpRequester;->create(Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/async/HttpRequester;

    move-result-object v0

    .line 514
    .local v0, "httpRequester":Lcom/google/android/videos/async/HttpRequester;, "Lcom/google/android/videos/async/HttpRequester<Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/Void;>;"
    invoke-static {p1, p3, v0}, Lcom/google/android/videos/async/HttpAuthenticatingRequester;->create(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/converter/RequestConverter;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/HttpAuthenticatingRequester;

    move-result-object v1

    return-object v1
.end method

.method private newCachingRequester(Lcom/google/android/videos/cache/Cache;Lcom/google/android/videos/async/Requester;J)Lcom/google/android/videos/async/Requester;
    .locals 1
    .param p3, "timeToLive"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/videos/cache/Cache",
            "<TR;",
            "Lcom/google/android/videos/async/Timestamped",
            "<TE;>;>;",
            "Lcom/google/android/videos/async/Requester",
            "<TR;TE;>;J)",
            "Lcom/google/android/videos/async/Requester",
            "<TR;TE;>;"
        }
    .end annotation

    .prologue
    .line 522
    .local p1, "cache":Lcom/google/android/videos/cache/Cache;, "Lcom/google/android/videos/cache/Cache<TR;Lcom/google/android/videos/async/Timestamped<TE;>;>;"
    .local p2, "target":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TR;TE;>;"
    const/4 v0, 0x0

    invoke-static {p1, p2, p3, p4, v0}, Lcom/google/android/videos/async/TimestampedCachingRequester;->create(Lcom/google/android/videos/cache/Cache;Lcom/google/android/videos/async/Requester;JZ)Lcom/google/android/videos/async/TimestampedCachingRequester;

    move-result-object v0

    return-object v0
.end method

.method private newInMemoryCache(I)Lcom/google/android/videos/cache/InMemoryLruCache;
    .locals 1
    .param p1, "size"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(I)",
            "Lcom/google/android/videos/cache/InMemoryLruCache",
            "<TK;TE;>;"
        }
    .end annotation

    .prologue
    .line 526
    new-instance v0, Lcom/google/android/videos/cache/InMemoryLruCache;

    invoke-direct {v0, p1}, Lcom/google/android/videos/cache/InMemoryLruCache;-><init>(I)V

    return-object v0
.end method


# virtual methods
.method public getAssetsCachingRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 354
    iget-object v0, p0, Lcom/google/android/videos/api/DefaultApiRequesters;->assetsCachingRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getAssetsRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 349
    iget-object v0, p0, Lcom/google/android/videos/api/DefaultApiRequesters;->assetsRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getAssetsSyncRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 344
    iget-object v0, p0, Lcom/google/android/videos/api/DefaultApiRequesters;->assetsSyncRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getBytesRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "[B>;"
        }
    .end annotation

    .prologue
    .line 415
    iget-object v0, p0, Lcom/google/android/videos/api/DefaultApiRequesters;->bytesAsyncRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getCategoryListRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/CategoryListRequest;",
            "Lcom/google/wireless/android/video/magma/proto/CategoryListResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 374
    iget-object v0, p0, Lcom/google/android/videos/api/DefaultApiRequesters;->categoryListRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getCencLicenseRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/CencLicenseRequest;",
            "[B>;"
        }
    .end annotation

    .prologue
    .line 420
    iget-object v0, p0, Lcom/google/android/videos/api/DefaultApiRequesters;->cencLicenseAsyncRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getConditionalEntitySyncRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/ConditionalHttpRequest",
            "<",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;",
            "Lcom/google/android/videos/async/ConditionalHttpResponse",
            "<",
            "Lorg/apache/http/HttpEntity;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 410
    iget-object v0, p0, Lcom/google/android/videos/api/DefaultApiRequesters;->conditionalEntitySyncRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getGcmCreateNotificationKeySyncRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/GcmCreateUserNotificationKeyRequest;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 303
    iget-object v0, p0, Lcom/google/android/videos/api/DefaultApiRequesters;->gcmCreateNotificationKeySyncRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getGcmRegisterSyncRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/GcmRegisterRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/android/videos/api/DefaultApiRequesters;->gcmRegisterSyncRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getMpdUrlGetRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/MpdUrlGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/android/videos/api/DefaultApiRequesters;->mpdUrlGetRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getPromotionsRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/PromotionsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 319
    iget-object v0, p0, Lcom/google/android/videos/api/DefaultApiRequesters;->promotionsRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getRecommendationsRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/RecommendationsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 314
    iget-object v0, p0, Lcom/google/android/videos/api/DefaultApiRequesters;->recommendationsRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getRedeemPromotionRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/RedeemPromotionRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 324
    iget-object v0, p0, Lcom/google/android/videos/api/DefaultApiRequesters;->redeemPromotionRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getReviewsRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetReviewListRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 359
    iget-object v0, p0, Lcom/google/android/videos/api/DefaultApiRequesters;->reviewsRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getRobotTokenRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/RobotTokenRequest;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 399
    iget-object v0, p0, Lcom/google/android/videos/api/DefaultApiRequesters;->robotTokenAsyncRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getStreamsRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/MpdGetRequest;",
            "Lcom/google/android/videos/streams/Streams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 379
    iget-object v0, p0, Lcom/google/android/videos/api/DefaultApiRequesters;->streamRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getUnlinkAccountRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/LinkedAccountRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 437
    iget-object v0, p0, Lcom/google/android/videos/api/DefaultApiRequesters;->unlinkAccountRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getUpdateAccountLinkingRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/LinkedAccountRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 442
    iget-object v0, p0, Lcom/google/android/videos/api/DefaultApiRequesters;->updateAccountLinkingRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getUpdateWishlistSyncRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/UpdateWishlistRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/android/videos/api/DefaultApiRequesters;->updateWishlistSyncRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getUserConfigGetRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/UserConfigGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 369
    iget-object v0, p0, Lcom/google/android/videos/api/DefaultApiRequesters;->userConfigGetRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getUserConfigGetSyncRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/UserConfigGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 364
    iget-object v0, p0, Lcom/google/android/videos/api/DefaultApiRequesters;->userConfigGetSyncRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getUserLibrarySyncRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/UserLibraryRequest;",
            "Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 384
    iget-object v0, p0, Lcom/google/android/videos/api/DefaultApiRequesters;->userLibrarySyncRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getVideoCollectionGetRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/VideoCollectionGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoCollectionGetResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 432
    iget-object v0, p0, Lcom/google/android/videos/api/DefaultApiRequesters;->videoCollectionGetRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getVideoCollectionListRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/VideoCollectionListRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoCollectionListResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 426
    iget-object v0, p0, Lcom/google/android/videos/api/DefaultApiRequesters;->videoCollectionListRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getVideoFormatsRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/EmptyRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/wireless/android/video/magma/proto/VideoFormat;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 394
    iget-object v0, p0, Lcom/google/android/videos/api/DefaultApiRequesters;->videoFormatsAsyncRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getVideoGetRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/VideoGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/android/videos/api/DefaultApiRequesters;->videoGetRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getVideoUpdateRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/VideoUpdateRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 292
    iget-object v0, p0, Lcom/google/android/videos/api/DefaultApiRequesters;->videoUpdateRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getWishlistSyncRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/GetWishlistRequest;",
            "Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 329
    iget-object v0, p0, Lcom/google/android/videos/api/DefaultApiRequesters;->wishlistSyncRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method
