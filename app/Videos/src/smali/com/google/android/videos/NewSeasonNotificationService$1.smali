.class Lcom/google/android/videos/NewSeasonNotificationService$1;
.super Ljava/lang/Object;
.source "NewSeasonNotificationService.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/NewSeasonNotificationService;->showNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/AssetResource;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Ljava/lang/String;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/NewSeasonNotificationService;

.field final synthetic val$builder:Landroid/support/v4/app/NotificationCompat$Builder;

.field final synthetic val$manager:Landroid/app/NotificationManager;

.field final synthetic val$showId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/videos/NewSeasonNotificationService;Landroid/support/v4/app/NotificationCompat$Builder;Landroid/app/NotificationManager;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lcom/google/android/videos/NewSeasonNotificationService$1;->this$0:Lcom/google/android/videos/NewSeasonNotificationService;

    iput-object p2, p0, Lcom/google/android/videos/NewSeasonNotificationService$1;->val$builder:Landroid/support/v4/app/NotificationCompat$Builder;

    iput-object p3, p0, Lcom/google/android/videos/NewSeasonNotificationService$1;->val$manager:Landroid/app/NotificationManager;

    iput-object p4, p0, Lcom/google/android/videos/NewSeasonNotificationService$1;->val$showId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 116
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/NewSeasonNotificationService$1;->onError(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public onError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 4
    .param p1, "request"    # Ljava/lang/String;
    .param p2, "err"    # Ljava/lang/Exception;

    .prologue
    .line 127
    const-string v0, "Could not get poster for show with id %s."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/videos/NewSeasonNotificationService$1;->val$showId:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 128
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 116
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Landroid/graphics/Bitmap;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/NewSeasonNotificationService$1;->onResponse(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public onResponse(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 5
    .param p1, "request"    # Ljava/lang/String;
    .param p2, "response"    # Landroid/graphics/Bitmap;

    .prologue
    .line 119
    iget-object v1, p0, Lcom/google/android/videos/NewSeasonNotificationService$1;->this$0:Lcom/google/android/videos/NewSeasonNotificationService;

    invoke-static {v1, p2}, Lcom/google/android/videos/NotificationUtil;->scaleBitmapForNotification(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 121
    .local v0, "icon":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/google/android/videos/NewSeasonNotificationService$1;->val$builder:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 122
    iget-object v1, p0, Lcom/google/android/videos/NewSeasonNotificationService$1;->val$manager:Landroid/app/NotificationManager;

    iget-object v2, p0, Lcom/google/android/videos/NewSeasonNotificationService$1;->val$showId:Ljava/lang/String;

    const v3, 0x7f0f003d

    iget-object v4, p0, Lcom/google/android/videos/NewSeasonNotificationService$1;->val$builder:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v4}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 123
    return-void
.end method
