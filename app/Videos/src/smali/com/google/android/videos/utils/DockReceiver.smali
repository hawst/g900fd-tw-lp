.class public final Lcom/google/android/videos/utils/DockReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DockReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/utils/DockReceiver$DockListener;
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private dockState:I

.field private final intentFilter:Landroid/content/IntentFilter;

.field private final listener:Lcom/google/android/videos/utils/DockReceiver$DockListener;

.field private registered:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/videos/utils/DockReceiver$DockListener;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/google/android/videos/utils/DockReceiver$DockListener;

    .prologue
    const/4 v2, 0x0

    .line 38
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 30
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.DOCK_EVENT"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/videos/utils/DockReceiver;->intentFilter:Landroid/content/IntentFilter;

    .line 35
    iput-boolean v2, p0, Lcom/google/android/videos/utils/DockReceiver;->registered:Z

    .line 39
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/videos/utils/DockReceiver;->context:Landroid/content/Context;

    .line 40
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/utils/DockReceiver$DockListener;

    iput-object v0, p0, Lcom/google/android/videos/utils/DockReceiver;->listener:Lcom/google/android/videos/utils/DockReceiver$DockListener;

    .line 41
    iput v2, p0, Lcom/google/android/videos/utils/DockReceiver;->dockState:I

    .line 42
    return-void
.end method


# virtual methods
.method public getDockState()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/google/android/videos/utils/DockReceiver;->dockState:I

    return v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/google/android/videos/utils/DockReceiver;->isInitialStickyBroadcast()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    :cond_0
    const-string v0, "android.intent.action.DOCK_EVENT"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 51
    const-string v0, "android.intent.extra.DOCK_STATE"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/utils/DockReceiver;->dockState:I

    .line 52
    iget-object v0, p0, Lcom/google/android/videos/utils/DockReceiver;->listener:Lcom/google/android/videos/utils/DockReceiver$DockListener;

    iget v1, p0, Lcom/google/android/videos/utils/DockReceiver;->dockState:I

    invoke-interface {v0, v1}, Lcom/google/android/videos/utils/DockReceiver$DockListener;->onDockState(I)V

    .line 56
    :goto_0
    return-void

    .line 54
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "unexpected intent: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public register()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 59
    iget-boolean v2, p0, Lcom/google/android/videos/utils/DockReceiver;->registered:Z

    if-nez v2, :cond_1

    .line 60
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/videos/utils/DockReceiver;->registered:Z

    .line 61
    iget-object v2, p0, Lcom/google/android/videos/utils/DockReceiver;->context:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/videos/utils/DockReceiver;->intentFilter:Landroid/content/IntentFilter;

    invoke-virtual {v2, p0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    .line 62
    .local v0, "sticky":Landroid/content/Intent;
    if-eqz v0, :cond_0

    const-string v2, "android.intent.extra.DOCK_STATE"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/videos/utils/DockReceiver;->dockState:I

    :cond_0
    iput v1, p0, Lcom/google/android/videos/utils/DockReceiver;->dockState:I

    .line 64
    iget-object v1, p0, Lcom/google/android/videos/utils/DockReceiver;->listener:Lcom/google/android/videos/utils/DockReceiver$DockListener;

    iget v2, p0, Lcom/google/android/videos/utils/DockReceiver;->dockState:I

    invoke-interface {v1, v2}, Lcom/google/android/videos/utils/DockReceiver$DockListener;->onDockState(I)V

    .line 66
    .end local v0    # "sticky":Landroid/content/Intent;
    :cond_1
    return-void
.end method

.method public unregister()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 69
    iget-boolean v0, p0, Lcom/google/android/videos/utils/DockReceiver;->registered:Z

    if-eqz v0, :cond_0

    .line 70
    iput-boolean v1, p0, Lcom/google/android/videos/utils/DockReceiver;->registered:Z

    .line 71
    iget-object v0, p0, Lcom/google/android/videos/utils/DockReceiver;->context:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 72
    iput v1, p0, Lcom/google/android/videos/utils/DockReceiver;->dockState:I

    .line 74
    :cond_0
    return-void
.end method
