.class public final Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;
.super Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;
.source "OfflineMediaPlayer.java"

# interfaces
.implements Lcom/google/android/videos/player/DownloadProgressNotifier$Listener;


# instance fields
.field private downloadProgressNotifier:Lcom/google/android/videos/player/DownloadProgressNotifier;

.field private volatile duration:I

.field private final networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

.field private volatile percentDownloaded:I

.field private final totalFileSize:J

.field private volatile waitingForMoreData:Z


# direct methods
.method public constructor <init>(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;Lcom/google/android/videos/utils/NetworkStatus;J)V
    .locals 1
    .param p1, "player"    # Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
    .param p2, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;
    .param p3, "totalFileSize"    # J

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;-><init>(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;)V

    .line 48
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/utils/NetworkStatus;

    iput-object v0, p0, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 49
    iput-wide p3, p0, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->totalFileSize:J

    .line 50
    return-void
.end method

.method public static allowSeekTo(III)Z
    .locals 1
    .param p0, "seekToMillis"    # I
    .param p1, "durationMillis"    # I
    .param p2, "percentDownloaded"    # I

    .prologue
    .line 108
    invoke-static {p1, p2}, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->maxSeekPositionMillis(II)I

    move-result v0

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getSeekToPercent(II)I
    .locals 2
    .param p0, "durationMillis"    # I
    .param p1, "percentDownloaded"    # I

    .prologue
    const/4 v0, 0x0

    .line 115
    if-nez p0, :cond_0

    .line 118
    :goto_0
    return v0

    :cond_0
    invoke-static {p0, p1}, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->maxSeekPositionMillis(II)I

    move-result v1

    mul-int/lit8 v1, v1, 0x64

    div-int/2addr v1, p0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method

.method private static maxSeekPositionMillis(II)I
    .locals 2
    .param p0, "durationMillis"    # I
    .param p1, "percentBuffered"    # I

    .prologue
    .line 128
    const/16 v1, 0x64

    if-lt p1, v1, :cond_0

    .line 132
    .end local p0    # "durationMillis":I
    :goto_0
    return p0

    .line 131
    .restart local p0    # "durationMillis":I
    :cond_0
    mul-int v1, p0, p1

    div-int/lit8 v0, v1, 0x64

    .line 132
    .local v0, "approxDurationDownloaded":I
    const v1, 0x1c138

    sub-int p0, v0, v1

    goto :goto_0
.end method

.method private declared-synchronized stopScheduler()V
    .locals 1

    .prologue
    .line 73
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->downloadProgressNotifier:Lcom/google/android/videos/player/DownloadProgressNotifier;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->downloadProgressNotifier:Lcom/google/android/videos/player/DownloadProgressNotifier;

    invoke-virtual {v0}, Lcom/google/android/videos/player/DownloadProgressNotifier;->stop()V

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->downloadProgressNotifier:Lcom/google/android/videos/player/DownloadProgressNotifier;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    :cond_0
    monitor-exit p0

    return-void

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private updateBufferedPercent()V
    .locals 2

    .prologue
    .line 153
    iget v0, p0, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->duration:I

    iget v1, p0, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->percentDownloaded:I

    invoke-static {v0, v1}, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->getSeekToPercent(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->notifyBufferingUpdate(I)V

    .line 154
    return-void
.end method

.method private updateCurrentPosition()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 158
    invoke-virtual {p0}, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->getCurrentPosition()I

    move-result v1

    .line 159
    .local v1, "position":I
    iget v2, p0, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->duration:I

    iget v3, p0, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->percentDownloaded:I

    mul-int/2addr v2, v3

    div-int/lit8 v0, v2, 0x64

    .line 161
    .local v0, "approxDurationDownloaded":I
    iget-boolean v2, p0, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->waitingForMoreData:Z

    if-nez v2, :cond_1

    .line 162
    iget v2, p0, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->percentDownloaded:I

    const/16 v3, 0x64

    if-ge v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x1d4c0

    sub-int v2, v0, v2

    if-le v1, v2, :cond_0

    .line 164
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Pausing at "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->percentDownloaded:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "% to buffer more data"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 165
    invoke-super {p0}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->pause()V

    .line 166
    iput-boolean v5, p0, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->waitingForMoreData:Z

    .line 167
    const/16 v2, 0x2bd

    invoke-virtual {p0, v2, v4}, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->notifyInfo(II)Z

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 169
    :cond_1
    const v2, 0x2bf20

    sub-int v2, v0, v2

    if-ge v1, v2, :cond_2

    .line 170
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Buffer full, resuming at "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->percentDownloaded:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 171
    iput-boolean v4, p0, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->waitingForMoreData:Z

    .line 172
    const/16 v2, 0x2be

    invoke-virtual {p0, v2, v4}, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->notifyInfo(II)Z

    .line 173
    invoke-virtual {p0}, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->start()V

    goto :goto_0

    .line 174
    :cond_2
    iget-object v2, p0, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v2}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v2

    if-nez v2, :cond_0

    .line 176
    const/16 v2, -0x3e81

    invoke-virtual {p0, v5, v2}, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->notifyError(II)Z

    goto :goto_0
.end method


# virtual methods
.method public onBufferingUpdate(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;I)V
    .locals 0
    .param p1, "player"    # Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
    .param p2, "percent"    # I

    .prologue
    .line 82
    return-void
.end method

.method public onDownloadProgress(JJI)V
    .locals 0
    .param p1, "currentFileSize"    # J
    .param p3, "totalFileSize"    # J
    .param p5, "percentDownloaded"    # I

    .prologue
    .line 147
    iput p5, p0, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->percentDownloaded:I

    .line 148
    invoke-direct {p0}, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->updateBufferedPercent()V

    .line 149
    invoke-direct {p0}, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->updateCurrentPosition()V

    .line 150
    return-void
.end method

.method public onPrepared(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;)V
    .locals 1
    .param p1, "player"    # Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->getDuration()I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->duration:I

    .line 87
    monitor-enter p0

    .line 88
    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->downloadProgressNotifier:Lcom/google/android/videos/player/DownloadProgressNotifier;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->downloadProgressNotifier:Lcom/google/android/videos/player/DownloadProgressNotifier;

    invoke-virtual {v0}, Lcom/google/android/videos/player/DownloadProgressNotifier;->start()V

    .line 91
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    invoke-super {p0, p1}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->onPrepared(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;)V

    .line 93
    return-void

    .line 91
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public pause()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 138
    invoke-super {p0}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->pause()V

    .line 139
    iget-boolean v0, p0, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->waitingForMoreData:Z

    if-eqz v0, :cond_0

    .line 140
    iput-boolean v1, p0, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->waitingForMoreData:Z

    .line 141
    const/16 v0, 0x2be

    invoke-virtual {p0, v0, v1}, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->notifyInfo(II)Z

    .line 143
    :cond_0
    return-void
.end method

.method public prepareAsync()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->updateBufferedPercent()V

    .line 63
    invoke-super {p0}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->prepareAsync()V

    .line 64
    return-void
.end method

.method public release()V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->stopScheduler()V

    .line 69
    invoke-super {p0}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->release()V

    .line 70
    return-void
.end method

.method public seekTo(I)V
    .locals 2
    .param p1, "msec"    # I

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->getDuration()I

    move-result v0

    iget v1, p0, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->percentDownloaded:I

    invoke-static {p1, v0, v1}, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->allowSeekTo(III)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    invoke-super {p0, p1}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->seekTo(I)V

    .line 102
    :goto_0
    return-void

    .line 100
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->notifySeekComplete()V

    goto :goto_0
.end method

.method public setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    .local p3, "headers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Lcom/google/android/videos/player/DownloadProgressNotifier;

    iget-wide v2, p0, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->totalFileSize:J

    invoke-direct {v0, p2, v2, v3, p0}, Lcom/google/android/videos/player/DownloadProgressNotifier;-><init>(Landroid/net/Uri;JLcom/google/android/videos/player/DownloadProgressNotifier$Listener;)V

    iput-object v0, p0, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;->downloadProgressNotifier:Lcom/google/android/videos/player/DownloadProgressNotifier;

    .line 57
    const/4 v0, 0x0

    invoke-super {p0, p1, p2, v0}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    .line 58
    return-void
.end method
