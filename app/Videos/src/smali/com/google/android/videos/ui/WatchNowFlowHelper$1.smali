.class Lcom/google/android/videos/ui/WatchNowFlowHelper$1;
.super Lcom/google/android/videos/flow/SingleViewFlow;
.source "WatchNowFlowHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/ui/WatchNowFlowHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/utils/DownloadedOnlyManager;Lcom/google/android/videos/welcome/WelcomeFlow;Lcom/google/android/videos/adapter/WatchNowDataSource;Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;Landroid/view/View$OnClickListener;Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView$Binder;Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView$Binder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/ui/WatchNowFlowHelper;

.field final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/google/android/videos/ui/WatchNowFlowHelper;ILandroid/app/Activity;)V
    .locals 0
    .param p2, "x0"    # I

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/videos/ui/WatchNowFlowHelper$1;->this$0:Lcom/google/android/videos/ui/WatchNowFlowHelper;

    iput-object p3, p0, Lcom/google/android/videos/ui/WatchNowFlowHelper$1;->val$activity:Landroid/app/Activity;

    invoke-direct {p0, p2}, Lcom/google/android/videos/flow/SingleViewFlow;-><init>(I)V

    return-void
.end method


# virtual methods
.method public createViewHolder(ILandroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 3
    .param p1, "viewType"    # I
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 72
    invoke-super {p0, p1, p2}, Lcom/google/android/videos/flow/SingleViewFlow;->createViewHolder(ILandroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v1

    .line 73
    .local v1, "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    iget-object v2, v1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    .line 74
    .local v0, "lp":Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowFlowHelper$1;->val$activity:Landroid/app/Activity;

    invoke-static {v2}, Lcom/google/android/videos/ui/WatchNowFlowHelper;->getHeaderHeight(Landroid/content/Context;)I

    move-result v2

    invoke-static {v2}, Lcom/google/android/play/utils/Compound;->intLengthToCompound(I)I

    move-result v2

    iput v2, v0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->heightCompound:I

    .line 75
    return-object v1
.end method
