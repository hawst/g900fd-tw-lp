.class public final Lcom/google/android/videos/tagging/Cards$WebSearchOnClickListener;
.super Ljava/lang/Object;
.source "Cards.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/Cards;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1c
    name = "WebSearchOnClickListener"
.end annotation


# instance fields
.field private final activity:Landroid/app/Activity;

.field private final eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private final eventSource:I

.field private final query:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/videos/logging/EventLogger;I)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "phrase"    # Ljava/lang/String;
    .param p3, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;
    .param p4, "eventSource"    # I

    .prologue
    .line 267
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 268
    iput-object p1, p0, Lcom/google/android/videos/tagging/Cards$WebSearchOnClickListener;->activity:Landroid/app/Activity;

    .line 269
    invoke-static {p2}, Lcom/google/android/videos/tagging/Cards;->toQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/tagging/Cards$WebSearchOnClickListener;->query:Ljava/lang/String;

    .line 270
    iput-object p3, p0, Lcom/google/android/videos/tagging/Cards$WebSearchOnClickListener;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 271
    iput p4, p0, Lcom/google/android/videos/tagging/Cards$WebSearchOnClickListener;->eventSource:I

    .line 272
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 276
    iget-object v1, p0, Lcom/google/android/videos/tagging/Cards$WebSearchOnClickListener;->activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/videos/tagging/Cards$WebSearchOnClickListener;->query:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/videos/utils/BrowserUtil;->startWebSearch(Landroid/app/Activity;Ljava/lang/String;)I

    move-result v0

    .line 277
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/android/videos/tagging/Cards$WebSearchOnClickListener;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget v2, p0, Lcom/google/android/videos/tagging/Cards$WebSearchOnClickListener;->eventSource:I

    invoke-interface {v1, v2, v0}, Lcom/google/android/videos/logging/EventLogger;->onWebSearch(II)V

    .line 278
    return-void
.end method
