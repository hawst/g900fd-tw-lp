.class public final Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;
.super Lcom/google/android/videos/adapter/EpisodesDataSource;
.source "EpisodesDataSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/adapter/EpisodesDataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EpisodesSubRangeDataSource"
.end annotation


# instance fields
.field private final mainDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;


# direct methods
.method private constructor <init>(Landroid/database/Cursor;Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;)V
    .locals 1
    .param p1, "subCursor"    # Landroid/database/Cursor;
    .param p2, "mainDataSource"    # Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    .prologue
    .line 292
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/videos/adapter/EpisodesDataSource;-><init>(Lcom/google/android/videos/adapter/EpisodesDataSource$1;)V

    .line 293
    invoke-super {p0, p1}, Lcom/google/android/videos/adapter/EpisodesDataSource;->changeCursor(Landroid/database/Cursor;)V

    .line 294
    iput-object p2, p0, Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;->mainDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    .line 296
    new-instance v0, Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource$1;

    invoke-direct {v0, p0}, Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource$1;-><init>(Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;)V

    invoke-virtual {p2, v0}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->registerObserver(Ljava/lang/Object;)V

    .line 306
    return-void
.end method

.method synthetic constructor <init>(Landroid/database/Cursor;Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;Lcom/google/android/videos/adapter/EpisodesDataSource$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/database/Cursor;
    .param p2, "x1"    # Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;
    .param p3, "x2"    # Lcom/google/android/videos/adapter/EpisodesDataSource$1;

    .prologue
    .line 287
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;-><init>(Landroid/database/Cursor;Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;)V

    return-void
.end method


# virtual methods
.method public changeCursor(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "newCursor"    # Landroid/database/Cursor;

    .prologue
    .line 310
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public isNetworkConnected()Z
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;->mainDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-virtual {v0}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->isNetworkConnected()Z

    move-result v0

    return v0
.end method

.method public setNetworkConnected(Z)V
    .locals 1
    .param p1, "networkConnected"    # Z

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;->mainDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->setNetworkConnected(Z)V

    .line 316
    return-void
.end method
