.class interface abstract Lcom/google/android/videos/pinning/TransfersExecutor$TransfersQuery;
.super Ljava/lang/Object;
.source "TransfersExecutor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pinning/TransfersExecutor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "TransfersQuery"
.end annotation


# static fields
.field public static final PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1016
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "account"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "asset_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "pinning_status"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "license_release_pending"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "external_storage_index"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "license_cenc_key_set_id IS NOT NULL"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/videos/pinning/TransfersExecutor$TransfersQuery;->PROJECTION:[Ljava/lang/String;

    return-void
.end method
