.class Lcom/google/android/videos/pinning/StreamingPsshPrefetchTask;
.super Lcom/google/android/videos/pinning/Task;
.source "StreamingPsshPrefetchTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/pinning/Task",
        "<",
        "Lcom/google/android/videos/pinning/DownloadKey;",
        ">;"
    }
.end annotation


# static fields
.field private static final DETAILS_PROJECTION:[Ljava/lang/String;


# instance fields
.field private final database:Lcom/google/android/videos/store/Database;

.field private final globals:Lcom/google/android/videos/VideosGlobals;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 45
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "asset_type = 20"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "duration_seconds * 1000"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/videos/pinning/StreamingPsshPrefetchTask;->DETAILS_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/videos/VideosGlobals;Lcom/google/android/videos/pinning/DownloadKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/videos/pinning/Task$Listener;Lcom/google/android/videos/logging/EventLogger;)V
    .locals 7
    .param p1, "globals"    # Lcom/google/android/videos/VideosGlobals;
    .param p2, "key"    # Lcom/google/android/videos/pinning/DownloadKey;
    .param p3, "wakeLock"    # Landroid/os/PowerManager$WakeLock;
    .param p4, "wifiLock"    # Landroid/net/wifi/WifiManager$WifiLock;
    .param p5, "listener"    # Lcom/google/android/videos/pinning/Task$Listener;
    .param p6, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;

    .prologue
    .line 59
    const/4 v1, 0x5

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/pinning/Task;-><init>(ILcom/google/android/videos/pinning/Task$Key;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/videos/pinning/Task$Listener;Lcom/google/android/videos/logging/EventLogger;)V

    .line 60
    iput-object p1, p0, Lcom/google/android/videos/pinning/StreamingPsshPrefetchTask;->globals:Lcom/google/android/videos/VideosGlobals;

    .line 61
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pinning/StreamingPsshPrefetchTask;->database:Lcom/google/android/videos/store/Database;

    .line 62
    return-void
.end method

.method private createExtractor(ZJ)Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    .locals 6
    .param p1, "isEpisode"    # Z
    .param p2, "durationMs"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/Task$TaskException;
        }
    .end annotation

    .prologue
    .line 91
    invoke-direct {p0, p1}, Lcom/google/android/videos/pinning/StreamingPsshPrefetchTask;->getAudioStream(Z)Lcom/google/android/videos/streams/MediaStream;

    move-result-object v3

    .line 92
    .local v3, "stream":Lcom/google/android/videos/streams/MediaStream;
    iget-object v4, p0, Lcom/google/android/videos/pinning/StreamingPsshPrefetchTask;->globals:Lcom/google/android/videos/VideosGlobals;

    invoke-static {v4}, Lcom/google/android/videos/player/exo/ExoPlayerUtil;->buildDownloadHttpDataSource(Lcom/google/android/videos/VideosGlobals;)Lcom/google/android/exoplayer/upstream/DataSource;

    move-result-object v0

    .line 93
    .local v0, "dataSource":Lcom/google/android/exoplayer/upstream/DataSource;
    const-string v5, "audio/"

    iget-object v4, p0, Lcom/google/android/videos/pinning/StreamingPsshPrefetchTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v4, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v4, v4, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-static {v3, v5, v4, p2, p3}, Lcom/google/android/videos/player/exo/ExoPlayerUtil;->toRepresentation(Lcom/google/android/videos/streams/MediaStream;Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    move-result-object v2

    .line 96
    .local v2, "representation":Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    :try_start_0
    invoke-static {v0, v2, v3}, Lcom/google/android/videos/player/exo/ExoPlayerUtil;->prepareExtractor(Lcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;Lcom/google/android/videos/streams/MediaStream;)Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    return-object v4

    .line 97
    :catch_0
    move-exception v1

    .line 98
    .local v1, "e":Ljava/lang/Exception;
    new-instance v4, Lcom/google/android/videos/pinning/Task$TaskException;

    const-string v5, "Could not create audio extractor"

    invoke-direct {v4, v5, v1}, Lcom/google/android/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method

.method private getAudioStream(Z)Lcom/google/android/videos/streams/MediaStream;
    .locals 8
    .param p1, "isEpisode"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/Task$TaskException;
        }
    .end annotation

    .prologue
    .line 103
    invoke-direct {p0, p1}, Lcom/google/android/videos/pinning/StreamingPsshPrefetchTask;->getStreams(Z)Lcom/google/android/videos/streams/Streams;

    move-result-object v4

    .line 104
    .local v4, "streams":Lcom/google/android/videos/streams/Streams;
    iget-object v6, p0, Lcom/google/android/videos/pinning/StreamingPsshPrefetchTask;->globals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v6}, Lcom/google/android/videos/VideosGlobals;->getDashStreamsSelector()Lcom/google/android/videos/streams/DashStreamsSelector;

    move-result-object v5

    .line 107
    .local v5, "streamsSelector":Lcom/google/android/videos/streams/DashStreamsSelector;
    :try_start_0
    iget-object v6, v4, Lcom/google/android/videos/streams/Streams;->mediaStreams:Ljava/util/List;

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Lcom/google/android/videos/streams/DashStreamsSelector;->getOnlineAudioStreams(Ljava/util/List;Z)Ljava/util/List;
    :try_end_0
    .catch Lcom/google/android/videos/streams/MissingStreamException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 111
    .local v2, "audioStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    iget-object v6, p0, Lcom/google/android/videos/pinning/StreamingPsshPrefetchTask;->globals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v6}, Lcom/google/android/videos/VideosGlobals;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/videos/pinning/StreamingPsshPrefetchTask;->globals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v7}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v7

    invoke-static {v2, v6, v7}, Lcom/google/android/videos/utils/AudioInfoUtil;->getPreferredStreamIndex(Ljava/util/List;Landroid/content/Context;Landroid/content/SharedPreferences;)I

    move-result v1

    .line 113
    .local v1, "audioStreamIndex":I
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/streams/MediaStream;

    .line 114
    .local v0, "audioStream":Lcom/google/android/videos/streams/MediaStream;
    if-nez v0, :cond_0

    .line 115
    new-instance v6, Lcom/google/android/videos/pinning/Task$TaskException;

    const-string v7, "No audio stream found"

    invoke-direct {v6, v7}, Lcom/google/android/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 108
    .end local v0    # "audioStream":Lcom/google/android/videos/streams/MediaStream;
    .end local v1    # "audioStreamIndex":I
    .end local v2    # "audioStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    :catch_0
    move-exception v3

    .line 109
    .local v3, "e":Lcom/google/android/videos/streams/MissingStreamException;
    new-instance v6, Lcom/google/android/videos/pinning/Task$TaskException;

    const-string v7, "Could not get audio streams"

    invoke-direct {v6, v7, v3}, Lcom/google/android/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6

    .line 117
    .end local v3    # "e":Lcom/google/android/videos/streams/MissingStreamException;
    .restart local v0    # "audioStream":Lcom/google/android/videos/streams/MediaStream;
    .restart local v1    # "audioStreamIndex":I
    .restart local v2    # "audioStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    :cond_0
    return-object v0
.end method

.method private getStreams(Z)Lcom/google/android/videos/streams/Streams;
    .locals 11
    .param p1, "isEpisode"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/Task$TaskException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 121
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v10

    .line 122
    .local v10, "streamsCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/android/videos/streams/Streams;>;"
    new-instance v0, Lcom/google/android/videos/api/MpdGetRequest;

    iget-object v1, p0, Lcom/google/android/videos/pinning/StreamingPsshPrefetchTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v1, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v1, v1, Lcom/google/android/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/pinning/StreamingPsshPrefetchTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v2, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v2, v2, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/pinning/StreamingPsshPrefetchTask;->globals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v3}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/videos/Config;->useSslForDownloads()Z

    move-result v6

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    move v3, p1

    move v5, v4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/api/MpdGetRequest;-><init>(Ljava/lang/String;Ljava/lang/String;ZZZZLjava/util/Locale;)V

    .line 124
    .local v0, "request":Lcom/google/android/videos/api/MpdGetRequest;
    iget-object v1, p0, Lcom/google/android/videos/pinning/StreamingPsshPrefetchTask;->globals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/videos/api/ApiRequesters;->getStreamsRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v1

    invoke-interface {v1, v0, v10}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 126
    :try_start_0
    invoke-virtual {v10}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/videos/streams/Streams;

    .line 127
    .local v9, "streams":Lcom/google/android/videos/streams/Streams;
    iget-object v1, v9, Lcom/google/android/videos/streams/Streams;->mediaStreams:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 128
    new-instance v1, Lcom/google/android/videos/pinning/Task$TaskException;

    const-string v2, "empty list of permitted streams"

    invoke-direct {v1, v2}, Lcom/google/android/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    .end local v9    # "streams":Lcom/google/android/videos/streams/Streams;
    :catch_0
    move-exception v8

    .line 132
    .local v8, "e":Ljava/util/concurrent/ExecutionException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "failed to get streams for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/videos/pinning/StreamingPsshPrefetchTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v1, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v1, v1, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 133
    new-instance v1, Lcom/google/android/videos/pinning/Task$TaskException;

    const-string v2, "could not fetch permitted streams"

    invoke-direct {v1, v2, v8}, Lcom/google/android/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 130
    .end local v8    # "e":Ljava/util/concurrent/ExecutionException;
    .restart local v9    # "streams":Lcom/google/android/videos/streams/Streams;
    :cond_0
    return-object v9
.end method


# virtual methods
.method protected execute()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/Task$TaskException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 68
    iget-object v1, p0, Lcom/google/android/videos/pinning/StreamingPsshPrefetchTask;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v1}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 69
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "purchased_assets, videos ON asset_type IN (6,20) AND asset_id = video_id"

    sget-object v2, Lcom/google/android/videos/pinning/StreamingPsshPrefetchTask;->DETAILS_PROJECTION:[Ljava/lang/String;

    const-string v3, "account = ? AND asset_type IN (6,20) AND asset_id = ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v5, p0, Lcom/google/android/videos/pinning/StreamingPsshPrefetchTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v5, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v5, v5, Lcom/google/android/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    aput-object v5, v4, v6

    const/4 v6, 0x1

    iget-object v5, p0, Lcom/google/android/videos/pinning/StreamingPsshPrefetchTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v5, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v5, v5, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    aput-object v5, v4, v6

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 73
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_0

    .line 74
    new-instance v1, Lcom/google/android/videos/pinning/Task$TaskException;

    const-string v2, "Could not obtain video details"

    invoke-direct {v1, v2}, Lcom/google/android/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 79
    :catchall_0
    move-exception v1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v1

    .line 76
    :cond_0
    const/4 v1, 0x0

    :try_start_1
    invoke-static {v8, v1}, Lcom/google/android/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v12

    .line 77
    .local v12, "isEpisode":Z
    const/4 v1, 0x1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v10

    .line 79
    .local v10, "durationMs":J
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 82
    invoke-direct {p0, v12, v10, v11}, Lcom/google/android/videos/pinning/StreamingPsshPrefetchTask;->createExtractor(ZJ)Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;

    move-result-object v9

    .line 83
    .local v9, "extractor":Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    invoke-virtual {v9}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->getPsshInfo()Ljava/util/Map;

    move-result-object v1

    sget-object v2, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->WIDEVINE_UUID:Ljava/util/UUID;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, [B

    .line 84
    .local v14, "psshData":[B
    invoke-virtual {v9}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->getFormat()Lcom/google/android/exoplayer/MediaFormat;

    move-result-object v1

    iget-object v13, v1, Lcom/google/android/exoplayer/MediaFormat;->mimeType:Ljava/lang/String;

    .line 85
    .local v13, "mimeType":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/videos/pinning/StreamingPsshPrefetchTask;->database:Lcom/google/android/videos/store/Database;

    iget-object v1, p0, Lcom/google/android/videos/pinning/StreamingPsshPrefetchTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v1, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v3, v1, Lcom/google/android/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/videos/pinning/StreamingPsshPrefetchTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v1, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v1, v1, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-static {v2, v3, v1, v14, v13}, Lcom/google/android/videos/pinning/PinningDbHelper;->persistStreamingPsshData(Lcom/google/android/videos/store/Database;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;)Z

    .line 87
    return-void
.end method

.method protected onCompleted()V
    .locals 0

    .prologue
    .line 140
    return-void
.end method

.method protected onError(Ljava/lang/Throwable;ZZ)V
    .locals 5
    .param p1, "cause"    # Ljava/lang/Throwable;
    .param p2, "maxRetries"    # Z
    .param p3, "fatal"    # Z

    .prologue
    .line 144
    if-nez p2, :cond_0

    if-eqz p3, :cond_1

    .line 146
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/pinning/StreamingPsshPrefetchTask;->database:Lcom/google/android/videos/store/Database;

    iget-object v0, p0, Lcom/google/android/videos/pinning/StreamingPsshPrefetchTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v2, v0, Lcom/google/android/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/videos/pinning/StreamingPsshPrefetchTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v0, v0, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "none"

    invoke-static {v1, v2, v0, v3, v4}, Lcom/google/android/videos/pinning/PinningDbHelper;->persistStreamingPsshData(Lcom/google/android/videos/store/Database;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;)Z

    .line 148
    :cond_1
    return-void
.end method
