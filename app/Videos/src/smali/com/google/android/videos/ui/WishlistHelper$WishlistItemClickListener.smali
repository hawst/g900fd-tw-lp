.class final Lcom/google/android/videos/ui/WishlistHelper$WishlistItemClickListener;
.super Ljava/lang/Object;
.source "WishlistHelper.java"

# interfaces
.implements Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/WishlistHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "WishlistItemClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/ui/WishlistHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/ui/WishlistHelper;)V
    .locals 0

    .prologue
    .line 288
    iput-object p1, p0, Lcom/google/android/videos/ui/WishlistHelper$WishlistItemClickListener;->this$0:Lcom/google/android/videos/ui/WishlistHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/ui/WishlistHelper;Lcom/google/android/videos/ui/WishlistHelper$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/ui/WishlistHelper;
    .param p2, "x1"    # Lcom/google/android/videos/ui/WishlistHelper$1;

    .prologue
    .line 288
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/WishlistHelper$WishlistItemClickListener;-><init>(Lcom/google/android/videos/ui/WishlistHelper;)V

    return-void
.end method


# virtual methods
.method public onItemClick(Lcom/google/android/videos/adapter/DataSource;ILandroid/view/View;)V
    .locals 14
    .param p2, "position"    # I
    .param p3, "view"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/adapter/DataSource",
            "<*>;I",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 293
    .local p1, "dataSource":Lcom/google/android/videos/adapter/DataSource;, "Lcom/google/android/videos/adapter/DataSource<*>;"
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper$WishlistItemClickListener;->this$0:Lcom/google/android/videos/ui/WishlistHelper;

    # getter for: Lcom/google/android/videos/ui/WishlistHelper;->wishlistMoviesDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;
    invoke-static {v0}, Lcom/google/android/videos/ui/WishlistHelper;->access$100(Lcom/google/android/videos/ui/WishlistHelper;)Lcom/google/android/videos/adapter/WishlistDataSource;

    move-result-object v0

    if-ne p1, v0, :cond_0

    const/4 v12, 0x1

    .line 294
    .local v12, "handleMovieWishlist":Z
    :goto_0
    invoke-interface/range {p1 .. p2}, Lcom/google/android/videos/adapter/DataSource;->getItem(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/database/Cursor;

    .line 295
    .local v11, "cursor":Landroid/database/Cursor;
    if-eqz v12, :cond_1

    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper$WishlistItemClickListener;->this$0:Lcom/google/android/videos/ui/WishlistHelper;

    # getter for: Lcom/google/android/videos/ui/WishlistHelper;->wishlistMoviesDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;
    invoke-static {v0}, Lcom/google/android/videos/ui/WishlistHelper;->access$100(Lcom/google/android/videos/ui/WishlistHelper;)Lcom/google/android/videos/adapter/WishlistDataSource;

    move-result-object v0

    invoke-virtual {v0, v11}, Lcom/google/android/videos/adapter/WishlistDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    .line 297
    .local v2, "itemId":Ljava/lang/String;
    :goto_1
    invoke-virtual/range {p3 .. p3}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0f00d2

    if-ne v0, v1, :cond_4

    .line 298
    if-eqz v12, :cond_3

    .line 299
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper$WishlistItemClickListener;->this$0:Lcom/google/android/videos/ui/WishlistHelper;

    # getter for: Lcom/google/android/videos/ui/WishlistHelper;->unpurchasedMoviesHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;
    invoke-static {v0}, Lcom/google/android/videos/ui/WishlistHelper;->access$300(Lcom/google/android/videos/ui/WishlistHelper;)Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->getAssetResource(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v13

    .line 300
    .local v13, "movieAssetResource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    if-nez v13, :cond_2

    const/4 v4, 0x0

    .line 301
    .local v4, "offers":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    :goto_2
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper$WishlistItemClickListener;->this$0:Lcom/google/android/videos/ui/WishlistHelper;

    # getter for: Lcom/google/android/videos/ui/WishlistHelper;->suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;
    invoke-static {v0}, Lcom/google/android/videos/ui/WishlistHelper;->access$400(Lcom/google/android/videos/ui/WishlistHelper;)Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    move-result-object v0

    const/4 v3, 0x6

    iget-object v1, p0, Lcom/google/android/videos/ui/WishlistHelper$WishlistItemClickListener;->this$0:Lcom/google/android/videos/ui/WishlistHelper;

    # getter for: Lcom/google/android/videos/ui/WishlistHelper;->wishlistMoviesDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;
    invoke-static {v1}, Lcom/google/android/videos/ui/WishlistHelper;->access$100(Lcom/google/android/videos/ui/WishlistHelper;)Lcom/google/android/videos/adapter/WishlistDataSource;

    move-result-object v5

    move-object/from16 v1, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->showWishlistMenu(Landroid/view/View;Ljava/lang/String;I[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;Lcom/google/android/videos/adapter/WishlistDataSource;)V

    .line 317
    .end local v4    # "offers":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    .end local v13    # "movieAssetResource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :goto_3
    return-void

    .line 293
    .end local v2    # "itemId":Ljava/lang/String;
    .end local v11    # "cursor":Landroid/database/Cursor;
    .end local v12    # "handleMovieWishlist":Z
    :cond_0
    const/4 v12, 0x0

    goto :goto_0

    .line 295
    .restart local v11    # "cursor":Landroid/database/Cursor;
    .restart local v12    # "handleMovieWishlist":Z
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper$WishlistItemClickListener;->this$0:Lcom/google/android/videos/ui/WishlistHelper;

    # getter for: Lcom/google/android/videos/ui/WishlistHelper;->wishlistShowsDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;
    invoke-static {v0}, Lcom/google/android/videos/ui/WishlistHelper;->access$200(Lcom/google/android/videos/ui/WishlistHelper;)Lcom/google/android/videos/adapter/WishlistDataSource;

    move-result-object v0

    invoke-virtual {v0, v11}, Lcom/google/android/videos/adapter/WishlistDataSource;->getShowId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 300
    .restart local v2    # "itemId":Ljava/lang/String;
    .restart local v13    # "movieAssetResource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :cond_2
    iget-object v4, v13, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    goto :goto_2

    .line 304
    .end local v13    # "movieAssetResource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper$WishlistItemClickListener;->this$0:Lcom/google/android/videos/ui/WishlistHelper;

    # getter for: Lcom/google/android/videos/ui/WishlistHelper;->suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;
    invoke-static {v0}, Lcom/google/android/videos/ui/WishlistHelper;->access$400(Lcom/google/android/videos/ui/WishlistHelper;)Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    move-result-object v5

    const/16 v8, 0x12

    const/4 v9, 0x0

    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper$WishlistItemClickListener;->this$0:Lcom/google/android/videos/ui/WishlistHelper;

    # getter for: Lcom/google/android/videos/ui/WishlistHelper;->wishlistShowsDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;
    invoke-static {v0}, Lcom/google/android/videos/ui/WishlistHelper;->access$200(Lcom/google/android/videos/ui/WishlistHelper;)Lcom/google/android/videos/adapter/WishlistDataSource;

    move-result-object v10

    move-object/from16 v6, p3

    move-object v7, v2

    invoke-virtual/range {v5 .. v10}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->showWishlistMenu(Landroid/view/View;Ljava/lang/String;I[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;Lcom/google/android/videos/adapter/WishlistDataSource;)V

    goto :goto_3

    .line 308
    :cond_4
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper$WishlistItemClickListener;->this$0:Lcom/google/android/videos/ui/WishlistHelper;

    # getter for: Lcom/google/android/videos/ui/WishlistHelper;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;
    invoke-static {v0}, Lcom/google/android/videos/ui/WishlistHelper;->access$500(Lcom/google/android/videos/ui/WishlistHelper;)Lcom/google/android/videos/logging/UiEventLoggingHelper;

    move-result-object v0

    invoke-static/range {p3 .. p3}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->findUiElementNode(Landroid/view/View;)Lcom/google/android/videos/logging/UiElementNode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->sendClickEvent(Lcom/google/android/videos/logging/UiElementNode;)V

    .line 309
    if-eqz v12, :cond_5

    .line 310
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper$WishlistItemClickListener;->this$0:Lcom/google/android/videos/ui/WishlistHelper;

    # getter for: Lcom/google/android/videos/ui/WishlistHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;
    invoke-static {v0}, Lcom/google/android/videos/ui/WishlistHelper;->access$600(Lcom/google/android/videos/ui/WishlistHelper;)Lcom/google/android/videos/activity/HomeActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/ui/WishlistHelper$WishlistItemClickListener;->this$0:Lcom/google/android/videos/ui/WishlistHelper;

    # getter for: Lcom/google/android/videos/ui/WishlistHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;
    invoke-static {v1}, Lcom/google/android/videos/ui/WishlistHelper;->access$700(Lcom/google/android/videos/ui/WishlistHelper;)Lcom/google/android/videos/ui/SyncHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v1

    const/16 v3, 0x13

    invoke-static {v0, v2, v1, v3}, Lcom/google/android/videos/utils/PlayStoreUtil;->viewMovieDetails(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_3

    .line 313
    :cond_5
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper$WishlistItemClickListener;->this$0:Lcom/google/android/videos/ui/WishlistHelper;

    # getter for: Lcom/google/android/videos/ui/WishlistHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;
    invoke-static {v0}, Lcom/google/android/videos/ui/WishlistHelper;->access$600(Lcom/google/android/videos/ui/WishlistHelper;)Lcom/google/android/videos/activity/HomeActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/ui/WishlistHelper$WishlistItemClickListener;->this$0:Lcom/google/android/videos/ui/WishlistHelper;

    # getter for: Lcom/google/android/videos/ui/WishlistHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;
    invoke-static {v1}, Lcom/google/android/videos/ui/WishlistHelper;->access$700(Lcom/google/android/videos/ui/WishlistHelper;)Lcom/google/android/videos/ui/SyncHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v1

    const/16 v3, 0x13

    invoke-static {v0, v2, v1, v3}, Lcom/google/android/videos/utils/PlayStoreUtil;->viewShowDetails(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_3
.end method
