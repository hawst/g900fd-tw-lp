.class public final Lcom/google/android/videos/pano/binders/GenreBinder;
.super Ljava/lang/Object;
.source "GenreBinder.java"

# interfaces
.implements Lcom/google/android/repolib/common/Binder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Binder",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/VideoCategory;",
        "Lcom/google/android/videos/pano/ui/LibraryCardView;",
        ">;"
    }
.end annotation


# instance fields
.field private final bitmapRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/videos/async/Requester;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p1, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/async/ControllableRequest<Landroid/net/Uri;>;Landroid/graphics/Bitmap;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/android/videos/pano/binders/GenreBinder;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    .line 32
    return-void
.end method


# virtual methods
.method public bind(Lcom/google/wireless/android/video/magma/proto/VideoCategory;Lcom/google/android/videos/pano/ui/LibraryCardView;)V
    .locals 8
    .param p1, "category"    # Lcom/google/wireless/android/video/magma/proto/VideoCategory;
    .param p2, "view"    # Lcom/google/android/videos/pano/ui/LibraryCardView;

    .prologue
    const/4 v1, 0x1

    .line 36
    invoke-virtual {p2}, Lcom/google/android/videos/pano/ui/LibraryCardView;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 37
    .local v5, "context":Landroid/content/Context;
    iget-object v0, p1, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->name:Ljava/lang/String;

    invoke-virtual {p2, v0}, Lcom/google/android/videos/pano/ui/LibraryCardView;->setText(Ljava/lang/CharSequence;)V

    .line 38
    const v0, 0x7f02017e

    invoke-virtual {p2, v0}, Lcom/google/android/videos/pano/ui/LibraryCardView;->setImageResource(I)V

    .line 40
    iget-object v0, p1, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    const v2, 0x7f0e01b6

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/pano/ui/PanoHelper;->getImageUri([Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;IIFFLandroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 43
    .local v6, "imageUri":Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 44
    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/videos/pano/binders/GenreBinder;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    invoke-virtual {p2, v0, v2}, Lcom/google/android/videos/pano/ui/LibraryCardView;->setImageUri(Landroid/net/Uri;Lcom/google/android/videos/async/Requester;)V

    .line 46
    :cond_0
    iget v0, p1, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->type:I

    if-ne v0, v1, :cond_1

    iget-object v0, p1, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->id:Ljava/lang/String;

    invoke-static {v5, v0, v1}, Lcom/google/android/videos/pano/activity/VideoGridActivity;->createMovieGenreIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    .line 49
    .local v7, "intent":Landroid/content/Intent;
    :goto_0
    new-instance v0, Lcom/google/android/videos/pano/binders/GenreBinder$1;

    invoke-direct {v0, p0, v5, v7}, Lcom/google/android/videos/pano/binders/GenreBinder$1;-><init>(Lcom/google/android/videos/pano/binders/GenreBinder;Landroid/content/Context;Landroid/content/Intent;)V

    invoke-virtual {p2, v0}, Lcom/google/android/videos/pano/ui/LibraryCardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    return-void

    .line 46
    .end local v7    # "intent":Landroid/content/Intent;
    :cond_1
    iget-object v0, p1, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->id:Ljava/lang/String;

    invoke-static {v5, v0, v1}, Lcom/google/android/videos/pano/activity/VideoGridActivity;->createShowGenreIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    goto :goto_0
.end method

.method public bridge synthetic bind(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 26
    check-cast p1, Lcom/google/wireless/android/video/magma/proto/VideoCategory;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/videos/pano/ui/LibraryCardView;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pano/binders/GenreBinder;->bind(Lcom/google/wireless/android/video/magma/proto/VideoCategory;Lcom/google/android/videos/pano/ui/LibraryCardView;)V

    return-void
.end method
