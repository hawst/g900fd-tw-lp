.class abstract Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;
.super Ljava/lang/Object;
.source "KnowledgeEntityBitmapView.java"

# interfaces
.implements Landroid/support/v4/view/ViewPropertyAnimatorListener;
.implements Lcom/google/android/videos/ui/BitmapLoader$BitmapView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$1;,
        Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$SongBitmapView;,
        Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$ActorBitmapView;,
        Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$ThumbnailBitmapView;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/support/v4/view/ViewPropertyAnimatorListener;",
        "Lcom/google/android/videos/ui/BitmapLoader$BitmapView;"
    }
.end annotation


# instance fields
.field protected final avatarCropTransformation:Lcom/google/android/play/image/AvatarCropTransformation;

.field protected final entity:Lcom/google/android/videos/tagging/KnowledgeEntity;

.field protected final imageDimension:I

.field private final requestTime:J

.field private thumbnailTag:Ljava/lang/Object;

.field protected final view:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/play/image/AvatarCropTransformation;Lcom/google/android/videos/tagging/KnowledgeEntity;Landroid/view/View;I)V
    .locals 2
    .param p1, "avatarCropTransformation"    # Lcom/google/android/play/image/AvatarCropTransformation;
    .param p2, "entity"    # Lcom/google/android/videos/tagging/KnowledgeEntity;
    .param p4, "imageDimension"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/play/image/AvatarCropTransformation;",
            "Lcom/google/android/videos/tagging/KnowledgeEntity;",
            "TT;I)V"
        }
    .end annotation

    .prologue
    .line 132
    .local p0, "this":Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;, "Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView<TT;>;"
    .local p3, "view":Landroid/view/View;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    iput-object p2, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;->entity:Lcom/google/android/videos/tagging/KnowledgeEntity;

    .line 134
    iput-object p3, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;->view:Landroid/view/View;

    .line 135
    iput-object p1, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;->avatarCropTransformation:Lcom/google/android/play/image/AvatarCropTransformation;

    .line 136
    iput p4, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;->imageDimension:I

    .line 137
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;->requestTime:J

    .line 138
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/play/image/AvatarCropTransformation;Lcom/google/android/videos/tagging/KnowledgeEntity;Landroid/view/View;ILcom/google/android/videos/tagging/KnowledgeEntityBitmapView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/play/image/AvatarCropTransformation;
    .param p2, "x1"    # Lcom/google/android/videos/tagging/KnowledgeEntity;
    .param p3, "x2"    # Landroid/view/View;
    .param p4, "x3"    # I
    .param p5, "x4"    # Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$1;

    .prologue
    .line 27
    .local p0, "this":Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;, "Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView<TT;>;"
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;-><init>(Lcom/google/android/play/image/AvatarCropTransformation;Lcom/google/android/videos/tagging/KnowledgeEntity;Landroid/view/View;I)V

    return-void
.end method


# virtual methods
.method public getThumbnailTag()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 142
    .local p0, "this":Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;, "Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView<TT;>;"
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;->thumbnailTag:Ljava/lang/Object;

    return-object v0
.end method

.method public onAnimationCancel(Landroid/view/View;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 167
    .local p0, "this":Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;, "Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView<TT;>;"
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;->view:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 168
    return-void
.end method

.method public onAnimationEnd(Landroid/view/View;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 172
    .local p0, "this":Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;, "Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView<TT;>;"
    return-void
.end method

.method public onAnimationStart(Landroid/view/View;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 176
    .local p0, "this":Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;, "Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView<TT;>;"
    return-void
.end method

.method abstract postProcess(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
.end method

.method public setThumbnail(Landroid/graphics/Bitmap;Z)V
    .locals 4
    .param p1, "thumbnail"    # Landroid/graphics/Bitmap;
    .param p2, "missingThumbnail"    # Z

    .prologue
    .line 152
    .local p0, "this":Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;, "Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView<TT;>;"
    if-nez p2, :cond_0

    .line 153
    invoke-virtual {p0, p1}, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;->postProcess(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 155
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;->updateImage(Landroid/graphics/Bitmap;)V

    .line 156
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;->view:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 157
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;->requestTime:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 158
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;->view:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 159
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;->view:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/ViewCompat;->animate(Landroid/view/View;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setDuration(J)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->alpha(F)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setListener(Landroid/support/v4/view/ViewPropertyAnimatorListener;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    .line 162
    :cond_1
    return-void
.end method

.method public bridge synthetic setThumbnail(Ljava/lang/Object;Z)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Z

    .prologue
    .line 27
    .local p0, "this":Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;, "Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView<TT;>;"
    check-cast p1, Landroid/graphics/Bitmap;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;->setThumbnail(Landroid/graphics/Bitmap;Z)V

    return-void
.end method

.method public setThumbnailTag(Ljava/lang/Object;)V
    .locals 0
    .param p1, "tag"    # Ljava/lang/Object;

    .prologue
    .line 147
    .local p0, "this":Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;, "Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView<TT;>;"
    iput-object p1, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;->thumbnailTag:Ljava/lang/Object;

    .line 148
    return-void
.end method

.method abstract updateImage(Landroid/graphics/Bitmap;)V
.end method
