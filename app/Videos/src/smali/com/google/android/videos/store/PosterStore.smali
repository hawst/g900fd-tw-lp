.class public Lcom/google/android/videos/store/PosterStore;
.super Ljava/lang/Object;
.source "PosterStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/store/PosterStore$ImageRequester;,
        Lcom/google/android/videos/store/PosterStore$NoStoredPosterException;
    }
.end annotation


# instance fields
.field private final bitmapCache:Lcom/google/android/videos/bitmap/BitmapLruCache;

.field private final byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;

.field private final cpuExecutor:Ljava/util/concurrent/Executor;

.field private final imageRequesters:[Lcom/google/android/videos/store/PosterStore$ImageRequester;

.field private final localStoreExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/videos/store/FileStore;Lcom/google/android/videos/store/FileStore;Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;Lcom/google/android/videos/store/FileStore;Lcom/google/android/videos/store/FileStore;Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;Lcom/google/android/videos/bitmap/BitmapLruCache;Lcom/google/android/videos/utils/ByteArrayPool;)V
    .locals 4
    .param p1, "localStoreExecutor"    # Ljava/util/concurrent/Executor;
    .param p2, "cpuExecutor"    # Ljava/util/concurrent/Executor;
    .param p3, "videoPosterFileStore"    # Lcom/google/android/videos/store/FileStore;
    .param p4, "showPosterFileStore"    # Lcom/google/android/videos/store/FileStore;
    .param p5, "posterConverter"    # Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;
    .param p6, "screenshotFileStore"    # Lcom/google/android/videos/store/FileStore;
    .param p7, "showBannerFileStore"    # Lcom/google/android/videos/store/FileStore;
    .param p8, "widescreenImageConverter"    # Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;
    .param p9, "bitmapCache"    # Lcom/google/android/videos/bitmap/BitmapLruCache;
    .param p10, "byteArrayPool"    # Lcom/google/android/videos/utils/ByteArrayPool;

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    invoke-static {p6}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    invoke-static {p8}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/videos/store/PosterStore;->localStoreExecutor:Ljava/util/concurrent/Executor;

    .line 70
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/videos/store/PosterStore;->cpuExecutor:Ljava/util/concurrent/Executor;

    .line 71
    invoke-static {p10}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/utils/ByteArrayPool;

    iput-object v0, p0, Lcom/google/android/videos/store/PosterStore;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;

    .line 72
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/videos/store/PosterStore$ImageRequester;

    iput-object v0, p0, Lcom/google/android/videos/store/PosterStore;->imageRequesters:[Lcom/google/android/videos/store/PosterStore$ImageRequester;

    .line 73
    iget-object v0, p0, Lcom/google/android/videos/store/PosterStore;->imageRequesters:[Lcom/google/android/videos/store/PosterStore$ImageRequester;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/videos/store/PosterStore$ImageRequester;

    const-string v3, ".p"

    invoke-direct {v2, p0, p3, p5, v3}, Lcom/google/android/videos/store/PosterStore$ImageRequester;-><init>(Lcom/google/android/videos/store/PosterStore;Lcom/google/android/videos/store/FileStore;Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 75
    iget-object v0, p0, Lcom/google/android/videos/store/PosterStore;->imageRequesters:[Lcom/google/android/videos/store/PosterStore$ImageRequester;

    const/4 v1, 0x1

    new-instance v2, Lcom/google/android/videos/store/PosterStore$ImageRequester;

    const-string v3, ".sp"

    invoke-direct {v2, p0, p4, p5, v3}, Lcom/google/android/videos/store/PosterStore$ImageRequester;-><init>(Lcom/google/android/videos/store/PosterStore;Lcom/google/android/videos/store/FileStore;Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 77
    iget-object v0, p0, Lcom/google/android/videos/store/PosterStore;->imageRequesters:[Lcom/google/android/videos/store/PosterStore$ImageRequester;

    const/4 v1, 0x2

    new-instance v2, Lcom/google/android/videos/store/PosterStore$ImageRequester;

    const-string v3, ".ss"

    invoke-direct {v2, p0, p6, p8, v3}, Lcom/google/android/videos/store/PosterStore$ImageRequester;-><init>(Lcom/google/android/videos/store/PosterStore;Lcom/google/android/videos/store/FileStore;Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 79
    iget-object v0, p0, Lcom/google/android/videos/store/PosterStore;->imageRequesters:[Lcom/google/android/videos/store/PosterStore$ImageRequester;

    const/4 v1, 0x3

    new-instance v2, Lcom/google/android/videos/store/PosterStore$ImageRequester;

    const-string v3, ".sb"

    invoke-direct {v2, p0, p7, p8, v3}, Lcom/google/android/videos/store/PosterStore$ImageRequester;-><init>(Lcom/google/android/videos/store/PosterStore;Lcom/google/android/videos/store/FileStore;Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 82
    invoke-static {p9}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/bitmap/BitmapLruCache;

    iput-object v0, p0, Lcom/google/android/videos/store/PosterStore;->bitmapCache:Lcom/google/android/videos/bitmap/BitmapLruCache;

    .line 83
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/store/PosterStore;)Lcom/google/android/videos/utils/ByteArrayPool;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/PosterStore;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/videos/store/PosterStore;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/store/PosterStore;)Lcom/google/android/videos/bitmap/BitmapLruCache;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/PosterStore;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/videos/store/PosterStore;->bitmapCache:Lcom/google/android/videos/bitmap/BitmapLruCache;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/store/PosterStore;)Ljava/util/concurrent/Executor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/PosterStore;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/videos/store/PosterStore;->cpuExecutor:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/store/PosterStore;Ljava/lang/String;Lcom/google/android/videos/store/FileStore;Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/store/PosterStore;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/google/android/videos/store/FileStore;
    .param p3, "x3"    # Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;
    .param p4, "x4"    # Ljava/lang/String;
    .param p5, "x5"    # Lcom/google/android/videos/async/Callback;

    .prologue
    .line 26
    invoke-direct/range {p0 .. p5}, Lcom/google/android/videos/store/PosterStore;->getImage(Ljava/lang/String;Lcom/google/android/videos/store/FileStore;Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V

    return-void
.end method

.method private getBitmapBytes(Ljava/lang/String;Lcom/google/android/videos/store/FileStore;Lcom/google/android/videos/async/Callback;)V
    .locals 2
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "fileStore"    # Lcom/google/android/videos/store/FileStore;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/store/FileStore;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 91
    .local p3, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Ljava/lang/String;Lcom/google/android/videos/utils/ByteArray;>;"
    iget-object v0, p0, Lcom/google/android/videos/store/PosterStore;->localStoreExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/videos/store/PosterStore$1;

    invoke-direct {v1, p0, p2, p1, p3}, Lcom/google/android/videos/store/PosterStore$1;-><init>(Lcom/google/android/videos/store/PosterStore;Lcom/google/android/videos/store/FileStore;Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 106
    return-void
.end method

.method private getImage(Ljava/lang/String;Lcom/google/android/videos/store/FileStore;Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V
    .locals 9
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "fileStore"    # Lcom/google/android/videos/store/FileStore;
    .param p3, "bitmapConverter"    # Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;
    .param p4, "bitmapCacheKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/store/FileStore;",
            "Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 119
    .local p5, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 120
    iget-object v0, p0, Lcom/google/android/videos/store/PosterStore;->bitmapCache:Lcom/google/android/videos/bitmap/BitmapLruCache;

    invoke-virtual {v0, p4}, Lcom/google/android/videos/bitmap/BitmapLruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/Bitmap;

    .line 121
    .local v7, "cached":Landroid/graphics/Bitmap;
    if-eqz v7, :cond_0

    .line 122
    invoke-interface {p5, p1, v7}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 155
    :goto_0
    return-void

    .line 126
    :cond_0
    iget-object v8, p0, Lcom/google/android/videos/store/PosterStore;->localStoreExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/google/android/videos/store/PosterStore$2;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v4, p5

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/store/PosterStore$2;-><init>(Lcom/google/android/videos/store/PosterStore;Lcom/google/android/videos/store/FileStore;Ljava/lang/String;Lcom/google/android/videos/async/Callback;Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;Ljava/lang/String;)V

    invoke-interface {v8, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method


# virtual methods
.method public getBytes(ILjava/lang/String;Lcom/google/android/videos/async/Callback;)V
    .locals 1
    .param p1, "imageType"    # I
    .param p2, "videoId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 86
    .local p3, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Ljava/lang/String;Lcom/google/android/videos/utils/ByteArray;>;"
    iget-object v0, p0, Lcom/google/android/videos/store/PosterStore;->imageRequesters:[Lcom/google/android/videos/store/PosterStore$ImageRequester;

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/google/android/videos/store/PosterStore$ImageRequester;->fileStore:Lcom/google/android/videos/store/FileStore;

    invoke-direct {p0, p2, v0, p3}, Lcom/google/android/videos/store/PosterStore;->getBitmapBytes(Ljava/lang/String;Lcom/google/android/videos/store/FileStore;Lcom/google/android/videos/async/Callback;)V

    .line 87
    return-void
.end method

.method public getImage(ILjava/lang/String;Lcom/google/android/videos/async/Callback;)V
    .locals 1
    .param p1, "imageType"    # I
    .param p2, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 113
    .local p3, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    iget-object v0, p0, Lcom/google/android/videos/store/PosterStore;->imageRequesters:[Lcom/google/android/videos/store/PosterStore$ImageRequester;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2, p3}, Lcom/google/android/videos/store/PosterStore$ImageRequester;->request(Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V

    .line 114
    return-void
.end method

.method public getRequester(I)Lcom/google/android/videos/async/Requester;
    .locals 1
    .param p1, "imageType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/videos/store/PosterStore;->imageRequesters:[Lcom/google/android/videos/store/PosterStore$ImageRequester;

    aget-object v0, v0, p1

    return-object v0
.end method
