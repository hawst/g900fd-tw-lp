.class public final Lcom/google/android/videos/pano/repositories/DataSourceRepository;
.super Ljava/lang/Object;
.source "DataSourceRepository.java"

# interfaces
.implements Lcom/google/android/repolib/common/Indexer;
.implements Lcom/google/android/repolib/observers/UpdatablesChanged;
.implements Lcom/google/android/repolib/repositories/Repository;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Indexer",
        "<TT;>;",
        "Lcom/google/android/repolib/observers/UpdatablesChanged;",
        "Lcom/google/android/repolib/repositories/Repository",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final dataSource:Lcom/google/android/videos/pano/datasource/BaseDataSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/pano/datasource/BaseDataSource",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/pano/datasource/BaseDataSource;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/pano/datasource/BaseDataSource",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p0, "this":Lcom/google/android/videos/pano/repositories/DataSourceRepository;, "Lcom/google/android/videos/pano/repositories/DataSourceRepository<TT;>;"
    .local p1, "dataSource":Lcom/google/android/videos/pano/datasource/BaseDataSource;, "Lcom/google/android/videos/pano/datasource/BaseDataSource<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/pano/datasource/BaseDataSource;

    iput-object v0, p0, Lcom/google/android/videos/pano/repositories/DataSourceRepository;->dataSource:Lcom/google/android/videos/pano/datasource/BaseDataSource;

    .line 37
    invoke-static {p0}, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->asyncUpdateDispatcher(Lcom/google/android/repolib/observers/UpdatablesChanged;)Lcom/google/android/repolib/observers/UpdateDispatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pano/repositories/DataSourceRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    .line 38
    return-void
.end method

.method public static dataSourceRepository(Lcom/google/android/videos/pano/datasource/BaseDataSource;)Lcom/google/android/repolib/repositories/Repository;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/videos/pano/datasource/BaseDataSource",
            "<TT;>;)",
            "Lcom/google/android/repolib/repositories/Repository",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 32
    .local p0, "dataSource":Lcom/google/android/videos/pano/datasource/BaseDataSource;, "Lcom/google/android/videos/pano/datasource/BaseDataSource<TT;>;"
    new-instance v0, Lcom/google/android/videos/pano/repositories/DataSourceRepository;

    invoke-direct {v0, p0}, Lcom/google/android/videos/pano/repositories/DataSourceRepository;-><init>(Lcom/google/android/videos/pano/datasource/BaseDataSource;)V

    return-object v0
.end method


# virtual methods
.method public addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 53
    .local p0, "this":Lcom/google/android/videos/pano/repositories/DataSourceRepository;, "Lcom/google/android/videos/pano/repositories/DataSourceRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DataSourceRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 54
    return-void
.end method

.method public firstUpdatableAdded()V
    .locals 2

    .prologue
    .line 42
    .local p0, "this":Lcom/google/android/videos/pano/repositories/DataSourceRepository;, "Lcom/google/android/videos/pano/repositories/DataSourceRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DataSourceRepository;->dataSource:Lcom/google/android/videos/pano/datasource/BaseDataSource;

    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/DataSourceRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/pano/datasource/BaseDataSource;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 43
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DataSourceRepository;->dataSource:Lcom/google/android/videos/pano/datasource/BaseDataSource;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/pano/datasource/BaseDataSource;->scheduleUpdate(Z)V

    .line 44
    return-void
.end method

.method public get(I)Ljava/lang/Object;
    .locals 1
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 74
    .local p0, "this":Lcom/google/android/videos/pano/repositories/DataSourceRepository;, "Lcom/google/android/videos/pano/repositories/DataSourceRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DataSourceRepository;->dataSource:Lcom/google/android/videos/pano/datasource/BaseDataSource;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/pano/datasource/BaseDataSource;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public indexer()Lcom/google/android/repolib/common/Indexer;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/repolib/common/Indexer",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 85
    .local p0, "this":Lcom/google/android/videos/pano/repositories/DataSourceRepository;, "Lcom/google/android/videos/pano/repositories/DataSourceRepository<TT;>;"
    return-object p0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 63
    .local p0, "this":Lcom/google/android/videos/pano/repositories/DataSourceRepository;, "Lcom/google/android/videos/pano/repositories/DataSourceRepository<TT;>;"
    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/DataSourceRepository;->dataSource:Lcom/google/android/videos/pano/datasource/BaseDataSource;

    invoke-virtual {v1}, Lcom/google/android/videos/pano/datasource/BaseDataSource;->getAll()[Ljava/lang/Object;

    move-result-object v0

    .line 64
    .local v0, "data":[Ljava/lang/Object;, "[TT;"
    if-eqz v0, :cond_0

    .line 65
    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/DataSourceRepository;->dataSource:Lcom/google/android/videos/pano/datasource/BaseDataSource;

    invoke-virtual {v1}, Lcom/google/android/videos/pano/datasource/BaseDataSource;->getAll()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 67
    :goto_0
    return-object v1

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    goto :goto_0
.end method

.method public lastUpdatableRemoved()V
    .locals 2

    .prologue
    .line 48
    .local p0, "this":Lcom/google/android/videos/pano/repositories/DataSourceRepository;, "Lcom/google/android/videos/pano/repositories/DataSourceRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DataSourceRepository;->dataSource:Lcom/google/android/videos/pano/datasource/BaseDataSource;

    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/DataSourceRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/pano/datasource/BaseDataSource;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 49
    return-void
.end method

.method public removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 58
    .local p0, "this":Lcom/google/android/videos/pano/repositories/DataSourceRepository;, "Lcom/google/android/videos/pano/repositories/DataSourceRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DataSourceRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 59
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 79
    .local p0, "this":Lcom/google/android/videos/pano/repositories/DataSourceRepository;, "Lcom/google/android/videos/pano/repositories/DataSourceRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DataSourceRepository;->dataSource:Lcom/google/android/videos/pano/datasource/BaseDataSource;

    invoke-virtual {v0}, Lcom/google/android/videos/pano/datasource/BaseDataSource;->getCount()I

    move-result v0

    return v0
.end method
