.class Lcom/google/android/videos/store/SyncService$SyncAdapter;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "SyncService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/SyncService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/store/SyncService;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/store/SyncService;)V
    .locals 1

    .prologue
    .line 226
    iput-object p1, p0, Lcom/google/android/videos/store/SyncService$SyncAdapter;->this$0:Lcom/google/android/videos/store/SyncService;

    .line 227
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    .line 228
    return-void
.end method

.method private cancelSyncTask()V
    .locals 2

    .prologue
    .line 296
    # getter for: Lcom/google/android/videos/store/SyncService;->syncServiceLock:Ljava/lang/Object;
    invoke-static {}, Lcom/google/android/videos/store/SyncService;->access$200()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 297
    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/store/SyncService$SyncAdapter;->this$0:Lcom/google/android/videos/store/SyncService;

    # getter for: Lcom/google/android/videos/store/SyncService;->syncTaskControl:Lcom/google/android/videos/async/TaskControl;
    invoke-static {v0}, Lcom/google/android/videos/store/SyncService;->access$300(Lcom/google/android/videos/store/SyncService;)Lcom/google/android/videos/async/TaskControl;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/async/TaskControl;->cancel(Lcom/google/android/videos/async/TaskControl;)V

    .line 298
    monitor-exit v1

    .line 299
    return-void

    .line 298
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 14
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "authority"    # Ljava/lang/String;
    .param p4, "provider"    # Landroid/content/ContentProviderClient;
    .param p5, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    .line 234
    if-eqz p2, :cond_0

    const-string v8, "initialize"

    const/4 v9, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/google/android/videos/store/SyncService$SyncAdapter;->this$0:Lcom/google/android/videos/store/SyncService;

    # getter for: Lcom/google/android/videos/store/SyncService;->preferences:Landroid/content/SharedPreferences;
    invoke-static {v8}, Lcom/google/android/videos/store/SyncService;->access$000(Lcom/google/android/videos/store/SyncService;)Landroid/content/SharedPreferences;

    move-result-object v8

    # invokes: Lcom/google/android/videos/store/SyncService;->enableSyncIfAccountUninitialized(Landroid/accounts/Account;Landroid/content/SharedPreferences;)Z
    invoke-static {p1, v8}, Lcom/google/android/videos/store/SyncService;->access$100(Landroid/accounts/Account;Landroid/content/SharedPreferences;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 281
    :goto_0
    return-void

    .line 242
    :cond_0
    # getter for: Lcom/google/android/videos/store/SyncService;->syncServiceLock:Ljava/lang/Object;
    invoke-static {}, Lcom/google/android/videos/store/SyncService;->access$200()Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9

    .line 243
    :try_start_0
    iget-object v8, p0, Lcom/google/android/videos/store/SyncService$SyncAdapter;->this$0:Lcom/google/android/videos/store/SyncService;

    new-instance v10, Lcom/google/android/videos/async/TaskControl;

    invoke-direct {v10}, Lcom/google/android/videos/async/TaskControl;-><init>()V

    # setter for: Lcom/google/android/videos/store/SyncService;->syncTaskControl:Lcom/google/android/videos/async/TaskControl;
    invoke-static {v8, v10}, Lcom/google/android/videos/store/SyncService;->access$302(Lcom/google/android/videos/store/SyncService;Lcom/google/android/videos/async/TaskControl;)Lcom/google/android/videos/async/TaskControl;

    .line 244
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 246
    # invokes: Lcom/google/android/videos/store/SyncService;->getLastSyncWasSuccessKey(Landroid/accounts/Account;)Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/videos/store/SyncService;->access$400(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v4

    .line 247
    .local v4, "lastSyncWasSuccessKey":Ljava/lang/String;
    const-string v8, "ignore_backoff"

    const/4 v9, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    if-nez v8, :cond_1

    const-string v8, "force"

    const/4 v9, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    if-nez v8, :cond_1

    const-string v8, "initialize"

    const/4 v9, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    if-nez v8, :cond_1

    const/4 v5, 0x1

    .line 251
    .local v5, "skipIfLastSyncWasSuccess":Z
    :goto_1
    if-eqz v5, :cond_2

    iget-object v8, p0, Lcom/google/android/videos/store/SyncService$SyncAdapter;->this$0:Lcom/google/android/videos/store/SyncService;

    # getter for: Lcom/google/android/videos/store/SyncService;->preferences:Landroid/content/SharedPreferences;
    invoke-static {v8}, Lcom/google/android/videos/store/SyncService;->access$000(Lcom/google/android/videos/store/SyncService;)Landroid/content/SharedPreferences;

    move-result-object v8

    const/4 v9, 0x0

    invoke-interface {v8, v4, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 252
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Skipping sync for "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v9}, Lcom/google/android/videos/utils/Hashing;->sha1(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 255
    const-wide/16 v8, 0x1f4

    :try_start_1
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 256
    :catch_0
    move-exception v8

    goto :goto_0

    .line 244
    .end local v4    # "lastSyncWasSuccessKey":Ljava/lang/String;
    .end local v5    # "skipIfLastSyncWasSuccess":Z
    :catchall_0
    move-exception v8

    :try_start_2
    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v8

    .line 247
    .restart local v4    # "lastSyncWasSuccessKey":Ljava/lang/String;
    :cond_1
    const/4 v5, 0x0

    goto :goto_1

    .line 262
    .restart local v5    # "skipIfLastSyncWasSuccess":Z
    :cond_2
    const/4 v6, 0x0

    .line 263
    .local v6, "success":Z
    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 264
    .local v2, "accountName":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/videos/store/SyncService$SyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v7

    .line 266
    .local v7, "videosGlobals":Lcom/google/android/videos/VideosGlobals;
    invoke-virtual {v7}, Lcom/google/android/videos/VideosGlobals;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v8

    invoke-virtual {v8, v2}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->blockingAuthenticate(Ljava/lang/String;)Z

    move-result v3

    .line 268
    .local v3, "authenticated":Z
    if-nez v3, :cond_3

    .line 269
    move-object/from16 v0, p5

    iget-object v8, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v10, v8, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v12, 0x1

    add-long/2addr v10, v12

    iput-wide v10, v8, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 280
    :goto_2
    iget-object v8, p0, Lcom/google/android/videos/store/SyncService$SyncAdapter;->this$0:Lcom/google/android/videos/store/SyncService;

    # getter for: Lcom/google/android/videos/store/SyncService;->preferences:Landroid/content/SharedPreferences;
    invoke-static {v8}, Lcom/google/android/videos/store/SyncService;->access$000(Lcom/google/android/videos/store/SyncService;)Landroid/content/SharedPreferences;

    move-result-object v8

    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    invoke-interface {v8, v4, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    .line 270
    :cond_3
    const-string v8, "video"

    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 271
    iget-object v8, p0, Lcom/google/android/videos/store/SyncService$SyncAdapter;->this$0:Lcom/google/android/videos/store/SyncService;

    const-string v9, "video"

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "wishlist"

    const/4 v11, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v10, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    move-object/from16 v0, p5

    # invokes: Lcom/google/android/videos/store/SyncService;->syncVideoPurchase(Ljava/lang/String;Ljava/lang/String;Landroid/content/SyncResult;Z)Z
    invoke-static {v8, v2, v9, v0, v10}, Lcom/google/android/videos/store/SyncService;->access$500(Lcom/google/android/videos/store/SyncService;Ljava/lang/String;Ljava/lang/String;Landroid/content/SyncResult;Z)Z

    move-result v6

    goto :goto_2

    .line 273
    :cond_4
    const-string v8, "season"

    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 274
    iget-object v8, p0, Lcom/google/android/videos/store/SyncService$SyncAdapter;->this$0:Lcom/google/android/videos/store/SyncService;

    const-string v9, "season"

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "wishlist"

    const/4 v11, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v10, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    move-object/from16 v0, p5

    # invokes: Lcom/google/android/videos/store/SyncService;->syncSeasonPurchase(Ljava/lang/String;Ljava/lang/String;Landroid/content/SyncResult;Z)Z
    invoke-static {v8, v2, v9, v0, v10}, Lcom/google/android/videos/store/SyncService;->access$600(Lcom/google/android/videos/store/SyncService;Ljava/lang/String;Ljava/lang/String;Landroid/content/SyncResult;Z)Z

    move-result v6

    goto :goto_2

    .line 277
    :cond_5
    iget-object v8, p0, Lcom/google/android/videos/store/SyncService$SyncAdapter;->this$0:Lcom/google/android/videos/store/SyncService;

    move-object/from16 v0, p5

    # invokes: Lcom/google/android/videos/store/SyncService;->syncAllPurchasesAndWishlist(Ljava/lang/String;Landroid/content/SyncResult;)Z
    invoke-static {v8, v2, v0}, Lcom/google/android/videos/store/SyncService;->access$700(Lcom/google/android/videos/store/SyncService;Ljava/lang/String;Landroid/content/SyncResult;)Z

    move-result v6

    goto :goto_2
.end method

.method public onSyncCanceled()V
    .locals 0

    .prologue
    .line 285
    invoke-super {p0}, Landroid/content/AbstractThreadedSyncAdapter;->onSyncCanceled()V

    .line 286
    invoke-direct {p0}, Lcom/google/android/videos/store/SyncService$SyncAdapter;->cancelSyncTask()V

    .line 287
    return-void
.end method

.method public onSyncCanceled(Ljava/lang/Thread;)V
    .locals 0
    .param p1, "thread"    # Ljava/lang/Thread;

    .prologue
    .line 291
    invoke-super {p0, p1}, Landroid/content/AbstractThreadedSyncAdapter;->onSyncCanceled(Ljava/lang/Thread;)V

    .line 292
    invoke-direct {p0}, Lcom/google/android/videos/store/SyncService$SyncAdapter;->cancelSyncTask()V

    .line 293
    return-void
.end method
