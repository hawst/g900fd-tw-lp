.class public interface abstract Lcom/google/android/videos/player/overlay/ControllerOverlay;
.super Ljava/lang/Object;
.source "ControllerOverlay.java"

# interfaces
.implements Lcom/google/android/videos/player/PlayerView$PlayerOverlay;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;
    }
.end annotation


# virtual methods
.method public abstract clearAudioTracks()V
.end method

.method public abstract clearSubtitles()V
.end method

.method public abstract hideControls(Z)V
.end method

.method public abstract init(Z)V
.end method

.method public abstract onActivityResult(IILandroid/content/Intent;)Z
.end method

.method public abstract onBackPressed()Z
.end method

.method public abstract onKeyDown(ILandroid/view/KeyEvent;)Z
.end method

.method public abstract onKeyUp(ILandroid/view/KeyEvent;)Z
.end method

.method public abstract onOrientationChanged()V
.end method

.method public abstract reset()V
.end method

.method public abstract setAudioTracks(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AudioInfo;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setErrorState(Ljava/lang/String;Lcom/google/android/videos/utils/RetryAction;)V
.end method

.method public abstract setHasKnowledge(Z)V
.end method

.method public abstract setHasSpinner(Z)V
.end method

.method public abstract setHideable(Z)V
.end method

.method public abstract setHq(Z)V
.end method

.method public abstract setHqIsHd(Z)V
.end method

.method public abstract setListener(Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;)V
.end method

.method public abstract setSelectedAudioTrackIndex(I)V
.end method

.method public abstract setSelectedSubtitleTrack(Lcom/google/android/videos/subtitles/SubtitleTrack;)V
.end method

.method public abstract setState(ZZ)V
.end method

.method public abstract setStoryboards([Lcom/google/wireless/android/video/magma/proto/Storyboard;)V
.end method

.method public abstract setSubtitles(Ljava/util/List;Z)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;Z)V"
        }
    .end annotation
.end method

.method public abstract setSupportsQualityToggle(Z)V
.end method

.method public abstract setTimes(III)V
.end method

.method public abstract setTouchExplorationEnabled(Z)V
.end method

.method public abstract setUseScrubPad(Z)V
.end method

.method public abstract setVideoInfo(Ljava/lang/String;IILjava/lang/String;)V
.end method

.method public abstract showControls()V
.end method

.method public abstract showShortClockConfirmationDialog(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;)V
.end method

.method public abstract showingChildActivity()Z
.end method

.method public abstract stopSeek()V
.end method
