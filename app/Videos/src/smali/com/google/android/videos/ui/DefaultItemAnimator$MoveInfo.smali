.class public Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;
.super Ljava/lang/Object;
.source "DefaultItemAnimator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/DefaultItemAnimator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "MoveInfo"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public fromX:I

.field public fromY:I

.field public holder:Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public toX:I

.field public toY:I


# direct methods
.method private constructor <init>(Landroid/support/v7/widget/RecyclerView$ViewHolder;IIII)V
    .locals 0
    .param p2, "fromX"    # I
    .param p3, "fromY"    # I
    .param p4, "toX"    # I
    .param p5, "toY"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;IIII)V"
        }
    .end annotation

    .prologue
    .line 43
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;, "Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo<TT;>;"
    .local p1, "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;->holder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .line 45
    iput p2, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;->fromX:I

    .line 46
    iput p3, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;->fromY:I

    .line 47
    iput p4, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;->toX:I

    .line 48
    iput p5, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;->toY:I

    .line 49
    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v7/widget/RecyclerView$ViewHolder;IIIILcom/google/android/videos/ui/DefaultItemAnimator$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "x1"    # I
    .param p3, "x2"    # I
    .param p4, "x3"    # I
    .param p5, "x4"    # I
    .param p6, "x5"    # Lcom/google/android/videos/ui/DefaultItemAnimator$1;

    .prologue
    .line 39
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;, "Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo<TT;>;"
    invoke-direct/range {p0 .. p5}, Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;-><init>(Landroid/support/v7/widget/RecyclerView$ViewHolder;IIII)V

    return-void
.end method
