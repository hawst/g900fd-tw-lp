.class Lcom/google/android/videos/activity/MyLibraryFragment$LibraryPagerAdapter;
.super Landroid/support/v4/app/FragmentPagerAdapter;
.source "MyLibraryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/activity/MyLibraryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LibraryPagerAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/activity/MyLibraryFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/activity/MyLibraryFragment;Landroid/support/v4/app/FragmentManager;)V
    .locals 0
    .param p2, "fragmentManager"    # Landroid/support/v4/app/FragmentManager;

    .prologue
    .line 276
    iput-object p1, p0, Lcom/google/android/videos/activity/MyLibraryFragment$LibraryPagerAdapter;->this$0:Lcom/google/android/videos/activity/MyLibraryFragment;

    .line 277
    invoke-direct {p0, p2}, Landroid/support/v4/app/FragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 278
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment$LibraryPagerAdapter;->this$0:Lcom/google/android/videos/activity/MyLibraryFragment;

    # getter for: Lcom/google/android/videos/activity/MyLibraryFragment;->currentVerticals:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/videos/activity/MyLibraryFragment;->access$200(Lcom/google/android/videos/activity/MyLibraryFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment$LibraryPagerAdapter;->this$0:Lcom/google/android/videos/activity/MyLibraryFragment;

    # getter for: Lcom/google/android/videos/activity/MyLibraryFragment;->currentVerticals:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/videos/activity/MyLibraryFragment;->access$200(Lcom/google/android/videos/activity/MyLibraryFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 293
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 289
    :pswitch_0
    new-instance v0, Lcom/google/android/videos/activity/MoviesFragment;

    invoke-direct {v0}, Lcom/google/android/videos/activity/MoviesFragment;-><init>()V

    .line 291
    :goto_0
    return-object v0

    :pswitch_1
    new-instance v0, Lcom/google/android/videos/activity/ShowsFragment;

    invoke-direct {v0}, Lcom/google/android/videos/activity/ShowsFragment;-><init>()V

    goto :goto_0

    .line 287
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "pos"    # I

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment$LibraryPagerAdapter;->this$0:Lcom/google/android/videos/activity/MyLibraryFragment;

    # getter for: Lcom/google/android/videos/activity/MyLibraryFragment;->currentVerticals:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/videos/activity/MyLibraryFragment;->access$200(Lcom/google/android/videos/activity/MyLibraryFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 3
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 316
    instance-of v1, p1, Lcom/google/android/videos/activity/MoviesFragment;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 318
    .local v0, "vertical":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/activity/MyLibraryFragment$LibraryPagerAdapter;->this$0:Lcom/google/android/videos/activity/MyLibraryFragment;

    # getter for: Lcom/google/android/videos/activity/MyLibraryFragment;->currentVerticals:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/google/android/videos/activity/MyLibraryFragment;->access$200(Lcom/google/android/videos/activity/MyLibraryFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 319
    const/4 v1, -0x2

    .line 321
    :goto_1
    return v1

    .line 316
    .end local v0    # "vertical":I
    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    .line 321
    .restart local v0    # "vertical":I
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/activity/MyLibraryFragment$LibraryPagerAdapter;->this$0:Lcom/google/android/videos/activity/MyLibraryFragment;

    # getter for: Lcom/google/android/videos/activity/MyLibraryFragment;->currentVerticals:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/google/android/videos/activity/MyLibraryFragment;->access$200(Lcom/google/android/videos/activity/MyLibraryFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    goto :goto_1
.end method

.method public bridge synthetic getPageTitle(I)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 274
    invoke-virtual {p0, p1}, Lcom/google/android/videos/activity/MyLibraryFragment$LibraryPagerAdapter;->getPageTitle(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPageTitle(I)Ljava/lang/String;
    .locals 2
    .param p1, "pos"    # I

    .prologue
    .line 299
    iget-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment$LibraryPagerAdapter;->this$0:Lcom/google/android/videos/activity/MyLibraryFragment;

    # getter for: Lcom/google/android/videos/activity/MyLibraryFragment;->currentVerticals:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/videos/activity/MyLibraryFragment;->access$200(Lcom/google/android/videos/activity/MyLibraryFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 305
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 301
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment$LibraryPagerAdapter;->this$0:Lcom/google/android/videos/activity/MyLibraryFragment;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/MyLibraryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0094

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 303
    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment$LibraryPagerAdapter;->this$0:Lcom/google/android/videos/activity/MyLibraryFragment;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/MyLibraryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0095

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 299
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
