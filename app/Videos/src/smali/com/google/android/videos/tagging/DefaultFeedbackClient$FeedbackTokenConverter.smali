.class final Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackTokenConverter;
.super Lcom/google/android/videos/converter/HttpResponseConverter;
.source "DefaultFeedbackClient.java"

# interfaces
.implements Lcom/google/android/videos/converter/RequestConverter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/DefaultFeedbackClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "FeedbackTokenConverter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/converter/HttpResponseConverter",
        "<",
        "Lcom/google/android/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;",
        ">;",
        "Lcom/google/android/videos/converter/RequestConverter",
        "<",
        "Ljava/lang/String;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        ">;"
    }
.end annotation


# instance fields
.field private final tokenExtractor:Ljava/util/regex/Pattern;

.field private final tokenUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "tokenUrl"    # Ljava/lang/String;
    .param p2, "tokenExtractorRegex"    # Ljava/lang/String;

    .prologue
    .line 233
    invoke-direct {p0}, Lcom/google/android/videos/converter/HttpResponseConverter;-><init>()V

    .line 234
    iput-object p1, p0, Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackTokenConverter;->tokenUrl:Ljava/lang/String;

    .line 235
    invoke-static {p2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackTokenConverter;->tokenExtractor:Ljava/util/regex/Pattern;

    .line 236
    return-void
.end method


# virtual methods
.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 224
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackTokenConverter;->convertRequest(Ljava/lang/String;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public convertRequest(Ljava/lang/String;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 2
    .param p1, "authToken"    # Ljava/lang/String;

    .prologue
    .line 240
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackTokenConverter;->tokenUrl:Ljava/lang/String;

    .line 241
    .local v0, "url":Ljava/lang/String;
    :goto_0
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    return-object v1

    .end local v0    # "url":Ljava/lang/String;
    :cond_0
    move-object v0, p1

    .line 240
    goto :goto_0
.end method

.method public convertResponse(Lorg/apache/http/HttpResponse;)Lcom/google/android/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;
    .locals 6
    .param p1, "response"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 247
    invoke-virtual {p0, p1}, Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackTokenConverter;->checkHttpError(Lorg/apache/http/HttpResponse;)V

    .line 248
    const-string v3, "X-Auto-Login"

    invoke-interface {p1, v3}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 249
    .local v0, "autoLoginHeader":Lorg/apache/http/Header;
    if-eqz v0, :cond_0

    .line 250
    new-instance v3, Lcom/google/android/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v5}, Lcom/google/android/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    :goto_0
    return-object v3

    .line 252
    :cond_0
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    invoke-static {v3}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v2

    .line 253
    .local v2, "responseBody":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackTokenConverter;->tokenExtractor:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 254
    .local v1, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 255
    new-instance v3, Lcom/google/android/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v5, v4}, Lcom/google/android/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 257
    :cond_1
    new-instance v3, Lcom/google/android/videos/converter/ConverterException;

    const-string v4, "Couldn\'t find feedback token in response"

    invoke-direct {v3, v4}, Lcom/google/android/videos/converter/ConverterException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public bridge synthetic convertResponse(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 224
    check-cast p1, Lorg/apache/http/HttpResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackTokenConverter;->convertResponse(Lorg/apache/http/HttpResponse;)Lcom/google/android/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic convertResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 224
    invoke-virtual {p0, p1}, Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackTokenConverter;->convertResponse(Lorg/apache/http/HttpResponse;)Lcom/google/android/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;

    move-result-object v0

    return-object v0
.end method
