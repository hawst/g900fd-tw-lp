.class Lcom/google/android/videos/activity/WatchActivity$TouchAwareControllerOverlay;
.super Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;
.source "WatchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/activity/WatchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TouchAwareControllerOverlay"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/activity/WatchActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/activity/WatchActivity;Landroid/support/v4/app/FragmentActivity;Lcom/google/android/videos/player/overlay/StoryboardHelper;Lcom/google/android/videos/utils/NetworkStatus;)V
    .locals 0
    .param p2, "activity"    # Landroid/support/v4/app/FragmentActivity;
    .param p3, "storyboardHelper"    # Lcom/google/android/videos/player/overlay/StoryboardHelper;
    .param p4, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;

    .prologue
    .line 437
    iput-object p1, p0, Lcom/google/android/videos/activity/WatchActivity$TouchAwareControllerOverlay;->this$0:Lcom/google/android/videos/activity/WatchActivity;

    .line 438
    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/google/android/videos/player/overlay/StoryboardHelper;Lcom/google/android/videos/utils/NetworkStatus;)V

    .line 439
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/activity/WatchActivity;Landroid/support/v4/app/FragmentActivity;Lcom/google/android/videos/player/overlay/StoryboardHelper;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/videos/activity/WatchActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/activity/WatchActivity;
    .param p2, "x1"    # Landroid/support/v4/app/FragmentActivity;
    .param p3, "x2"    # Lcom/google/android/videos/player/overlay/StoryboardHelper;
    .param p4, "x3"    # Lcom/google/android/videos/utils/NetworkStatus;
    .param p5, "x4"    # Lcom/google/android/videos/activity/WatchActivity$1;

    .prologue
    .line 434
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/videos/activity/WatchActivity$TouchAwareControllerOverlay;-><init>(Lcom/google/android/videos/activity/WatchActivity;Landroid/support/v4/app/FragmentActivity;Lcom/google/android/videos/player/overlay/StoryboardHelper;Lcom/google/android/videos/utils/NetworkStatus;)V

    return-void
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 447
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-nez v0, :cond_0

    .line 448
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity$TouchAwareControllerOverlay;->this$0:Lcom/google/android/videos/activity/WatchActivity;

    # getter for: Lcom/google/android/videos/activity/WatchActivity;->interactiveKnowledgeOverlay:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;
    invoke-static {v0}, Lcom/google/android/videos/activity/WatchActivity;->access$100(Lcom/google/android/videos/activity/WatchActivity;)Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->getMode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 457
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 451
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity$TouchAwareControllerOverlay;->this$0:Lcom/google/android/videos/activity/WatchActivity;

    # getter for: Lcom/google/android/videos/activity/WatchActivity;->interactiveKnowledgeOverlay:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;
    invoke-static {v0}, Lcom/google/android/videos/activity/WatchActivity;->access$100(Lcom/google/android/videos/activity/WatchActivity;)Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->hasContent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 452
    const/4 v0, 0x0

    goto :goto_0

    .line 448
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
