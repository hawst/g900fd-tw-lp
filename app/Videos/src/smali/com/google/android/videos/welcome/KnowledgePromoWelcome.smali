.class public Lcom/google/android/videos/welcome/KnowledgePromoWelcome;
.super Lcom/google/android/videos/welcome/PromoWelcome;
.source "KnowledgePromoWelcome.java"


# static fields
.field private static final ACTION_RES_IDS:[I


# instance fields
.field private account:Ljava/lang/String;

.field private final activity:Landroid/app/Activity;

.field private final config:Lcom/google/android/videos/Config;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/videos/welcome/KnowledgePromoWelcome;->ACTION_RES_IDS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0201c2
        0x7f0b00f4
        0x7f020070
        0x7f0b00f9
    .end array-data
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/app/Activity;Lcom/google/android/videos/Config;Landroid/content/SharedPreferences;)V
    .locals 1
    .param p1, "verticalId"    # Ljava/lang/String;
    .param p2, "activity"    # Landroid/app/Activity;
    .param p3, "config"    # Lcom/google/android/videos/Config;
    .param p4, "preferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 30
    const-string v0, "knowledge"

    invoke-direct {p0, p1, v0, p4}, Lcom/google/android/videos/welcome/PromoWelcome;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/SharedPreferences;)V

    .line 31
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/videos/welcome/KnowledgePromoWelcome;->activity:Landroid/app/Activity;

    .line 32
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/Config;

    iput-object v0, p0, Lcom/google/android/videos/welcome/KnowledgePromoWelcome;->config:Lcom/google/android/videos/Config;

    .line 33
    return-void
.end method


# virtual methods
.method public getActionResIds()[I
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/google/android/videos/welcome/KnowledgePromoWelcome;->ACTION_RES_IDS:[I

    return-object v0
.end method

.method public getDefaultBitmapResId()I
    .locals 1

    .prologue
    .line 54
    const v0, 0x7f020099

    return v0
.end method

.method public getDetailMessage()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/videos/welcome/KnowledgePromoWelcome;->activity:Landroid/app/Activity;

    const v1, 0x7f0b00e7

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNetworkBitmapUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/videos/welcome/KnowledgePromoWelcome;->activity:Landroid/app/Activity;

    const v1, 0x7f0b00e6

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onAction(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 69
    if-nez p1, :cond_0

    .line 70
    iget-object v0, p0, Lcom/google/android/videos/welcome/KnowledgePromoWelcome;->activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/videos/welcome/KnowledgePromoWelcome;->account:Ljava/lang/String;

    const/16 v2, 0x9

    invoke-static {v0, v1, v2}, Lcom/google/android/videos/utils/PlayStoreUtil;->viewMoviesVertical(Landroid/app/Activity;Ljava/lang/String;I)V

    .line 74
    :goto_0
    return-void

    .line 72
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/welcome/KnowledgePromoWelcome;->markDismissed()V

    goto :goto_0
.end method

.method public preparePromoIfEligible(Ljava/lang/String;ZZ)Z
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "contentInVertical"    # Z
    .param p3, "downloadedOnly"    # Z

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/android/videos/welcome/KnowledgePromoWelcome;->account:Ljava/lang/String;

    .line 39
    iget-object v0, p0, Lcom/google/android/videos/welcome/KnowledgePromoWelcome;->config:Lcom/google/android/videos/Config;

    invoke-interface {v0}, Lcom/google/android/videos/Config;->knowledgeEnabled()Z

    move-result v0

    return v0
.end method
