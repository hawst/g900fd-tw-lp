.class public abstract Lcom/google/android/videos/async/TimestampedCachingRequester;
.super Ljava/lang/Object;
.source "TimestampedCachingRequester.java"

# interfaces
.implements Lcom/google/android/videos/async/Requester;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/async/TimestampedCachingRequester$DefaultTimestampedCachingRequester;,
        Lcom/google/android/videos/async/TimestampedCachingRequester$CachingCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        "K:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Requester",
        "<TR;TE;>;"
    }
.end annotation


# instance fields
.field private final cache:Lcom/google/android/videos/cache/Cache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/cache/Cache",
            "<TK;",
            "Lcom/google/android/videos/async/Timestamped",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final readOnly:Z

.field private final target:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<TR;TE;>;"
        }
    .end annotation
.end field

.field private final timeToLive:J


# direct methods
.method protected constructor <init>(Lcom/google/android/videos/cache/Cache;Lcom/google/android/videos/async/Requester;JZ)V
    .locals 1
    .param p3, "timeToLive"    # J
    .param p5, "readOnly"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/cache/Cache",
            "<TK;",
            "Lcom/google/android/videos/async/Timestamped",
            "<TE;>;>;",
            "Lcom/google/android/videos/async/Requester",
            "<TR;TE;>;JZ)V"
        }
    .end annotation

    .prologue
    .line 70
    .local p0, "this":Lcom/google/android/videos/async/TimestampedCachingRequester;, "Lcom/google/android/videos/async/TimestampedCachingRequester<TR;TK;TE;>;"
    .local p1, "cache":Lcom/google/android/videos/cache/Cache;, "Lcom/google/android/videos/cache/Cache<TK;Lcom/google/android/videos/async/Timestamped<TE;>;>;"
    .local p2, "target":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TR;TE;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p1, p0, Lcom/google/android/videos/async/TimestampedCachingRequester;->cache:Lcom/google/android/videos/cache/Cache;

    .line 72
    iput-object p2, p0, Lcom/google/android/videos/async/TimestampedCachingRequester;->target:Lcom/google/android/videos/async/Requester;

    .line 73
    iput-wide p3, p0, Lcom/google/android/videos/async/TimestampedCachingRequester;->timeToLive:J

    .line 74
    iput-boolean p5, p0, Lcom/google/android/videos/async/TimestampedCachingRequester;->readOnly:Z

    .line 75
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/async/TimestampedCachingRequester;)Lcom/google/android/videos/cache/Cache;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/async/TimestampedCachingRequester;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/videos/async/TimestampedCachingRequester;->cache:Lcom/google/android/videos/cache/Cache;

    return-object v0
.end method

.method public static create(Lcom/google/android/videos/cache/Cache;J)Lcom/google/android/videos/async/TimestampedCachingRequester;
    .locals 7
    .param p1, "timeToLive"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/videos/cache/Cache",
            "<TR;",
            "Lcom/google/android/videos/async/Timestamped",
            "<TE;>;>;J)",
            "Lcom/google/android/videos/async/TimestampedCachingRequester",
            "<TR;TR;TE;>;"
        }
    .end annotation

    .prologue
    .local p0, "cache":Lcom/google/android/videos/cache/Cache;, "Lcom/google/android/videos/cache/Cache<TR;Lcom/google/android/videos/async/Timestamped<TE;>;>;"
    const/4 v6, 0x1

    .line 60
    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const-wide v0, 0x9a7ec800L

    cmp-long v0, p1, v0

    if-gtz v0, :cond_0

    move v0, v6

    :goto_0
    const-string v1, "time to live must be >=0 and <= 2592000000"

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;)V

    .line 63
    new-instance v1, Lcom/google/android/videos/async/TimestampedCachingRequester$DefaultTimestampedCachingRequester;

    const/4 v3, 0x0

    move-object v2, p0

    move-wide v4, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/videos/async/TimestampedCachingRequester$DefaultTimestampedCachingRequester;-><init>(Lcom/google/android/videos/cache/Cache;Lcom/google/android/videos/async/Requester;JZ)V

    return-object v1

    .line 61
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static create(Lcom/google/android/videos/cache/Cache;Lcom/google/android/videos/async/Requester;JZ)Lcom/google/android/videos/async/TimestampedCachingRequester;
    .locals 8
    .param p2, "timeToLive"    # J
    .param p4, "readOnly"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/videos/cache/Cache",
            "<TR;",
            "Lcom/google/android/videos/async/Timestamped",
            "<TE;>;>;",
            "Lcom/google/android/videos/async/Requester",
            "<TR;TE;>;JZ)",
            "Lcom/google/android/videos/async/TimestampedCachingRequester",
            "<TR;TR;TE;>;"
        }
    .end annotation

    .prologue
    .line 48
    .local p0, "cache":Lcom/google/android/videos/cache/Cache;, "Lcom/google/android/videos/cache/Cache<TR;Lcom/google/android/videos/async/Timestamped<TE;>;>;"
    .local p1, "target":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TR;TE;>;"
    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-ltz v0, :cond_0

    const-wide v0, 0x9a7ec800L

    cmp-long v0, p2, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "time to live must be >=0 and <= 2592000000"

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;)V

    .line 52
    new-instance v1, Lcom/google/android/videos/async/TimestampedCachingRequester$DefaultTimestampedCachingRequester;

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/google/android/videos/async/TimestampedCachingRequester$DefaultTimestampedCachingRequester;-><init>(Lcom/google/android/videos/cache/Cache;Lcom/google/android/videos/async/Requester;JZ)V

    return-object v1

    .line 50
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;)V"
        }
    .end annotation

    .prologue
    .line 88
    .local p0, "this":Lcom/google/android/videos/async/TimestampedCachingRequester;, "Lcom/google/android/videos/async/TimestampedCachingRequester<TR;TK;TE;>;"
    .local p1, "request":Ljava/lang/Object;, "TR;"
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TR;TE;>;"
    iget-wide v4, p0, Lcom/google/android/videos/async/TimestampedCachingRequester;->timeToLive:J

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_0

    .line 89
    iget-object v1, p0, Lcom/google/android/videos/async/TimestampedCachingRequester;->cache:Lcom/google/android/videos/cache/Cache;

    invoke-virtual {p0, p1}, Lcom/google/android/videos/async/TimestampedCachingRequester;->toKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1, v4}, Lcom/google/android/videos/cache/Cache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Timestamped;

    .line 90
    .local v0, "cached":Lcom/google/android/videos/async/Timestamped;, "Lcom/google/android/videos/async/Timestamped<TE;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 91
    .local v2, "currentTime":J
    if-eqz v0, :cond_0

    iget-wide v4, v0, Lcom/google/android/videos/async/Timestamped;->timestamp:J

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    iget-wide v4, v0, Lcom/google/android/videos/async/Timestamped;->timestamp:J

    iget-wide v6, p0, Lcom/google/android/videos/async/TimestampedCachingRequester;->timeToLive:J

    add-long/2addr v4, v6

    cmp-long v1, v4, v2

    if-ltz v1, :cond_0

    .line 95
    iget-object v1, v0, Lcom/google/android/videos/async/Timestamped;->element:Ljava/lang/Object;

    invoke-interface {p2, p1, v1}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 107
    .end local v0    # "cached":Lcom/google/android/videos/async/Timestamped;, "Lcom/google/android/videos/async/Timestamped<TE;>;"
    .end local v2    # "currentTime":J
    .end local p2    # "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TR;TE;>;"
    :goto_0
    return-void

    .line 99
    .restart local p2    # "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TR;TE;>;"
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/async/TimestampedCachingRequester;->target:Lcom/google/android/videos/async/Requester;

    if-eqz v1, :cond_2

    .line 102
    iget-object v4, p0, Lcom/google/android/videos/async/TimestampedCachingRequester;->target:Lcom/google/android/videos/async/Requester;

    iget-boolean v1, p0, Lcom/google/android/videos/async/TimestampedCachingRequester;->readOnly:Z

    if-eqz v1, :cond_1

    .end local p2    # "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TR;TE;>;"
    :goto_1
    invoke-interface {v4, p1, p2}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    goto :goto_0

    .restart local p2    # "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TR;TE;>;"
    :cond_1
    new-instance v1, Lcom/google/android/videos/async/TimestampedCachingRequester$CachingCallback;

    invoke-direct {v1, p0, p2}, Lcom/google/android/videos/async/TimestampedCachingRequester$CachingCallback;-><init>(Lcom/google/android/videos/async/TimestampedCachingRequester;Lcom/google/android/videos/async/Callback;)V

    move-object p2, v1

    goto :goto_1

    .line 105
    :cond_2
    new-instance v1, Lcom/google/android/videos/async/NotFoundException;

    invoke-direct {v1}, Lcom/google/android/videos/async/NotFoundException;-><init>()V

    invoke-interface {p2, p1, v1}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method protected abstract toKey(Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)TK;"
        }
    .end annotation
.end method
