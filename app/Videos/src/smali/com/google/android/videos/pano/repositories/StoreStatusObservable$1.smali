.class Lcom/google/android/videos/pano/repositories/StoreStatusObservable$1;
.super Ljava/lang/Object;
.source "StoreStatusObservable.java"

# interfaces
.implements Lcom/google/android/repolib/observers/UpdatablesChanged;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/pano/repositories/StoreStatusObservable;-><init>(Lcom/google/android/videos/accounts/SignInManager;Landroid/content/Context;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/WishlistStore;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pano/repositories/StoreStatusObservable;

.field final synthetic val$signInManager:Lcom/google/android/videos/accounts/SignInManager;

.field final synthetic val$storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;


# direct methods
.method constructor <init>(Lcom/google/android/videos/pano/repositories/StoreStatusObservable;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/StoreStatusMonitor;)V
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/google/android/videos/pano/repositories/StoreStatusObservable$1;->this$0:Lcom/google/android/videos/pano/repositories/StoreStatusObservable;

    iput-object p2, p0, Lcom/google/android/videos/pano/repositories/StoreStatusObservable$1;->val$signInManager:Lcom/google/android/videos/accounts/SignInManager;

    iput-object p3, p0, Lcom/google/android/videos/pano/repositories/StoreStatusObservable$1;->val$storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public firstUpdatableAdded()V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/StoreStatusObservable$1;->val$signInManager:Lcom/google/android/videos/accounts/SignInManager;

    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/StoreStatusObservable$1;->this$0:Lcom/google/android/videos/pano/repositories/StoreStatusObservable;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/accounts/SignInManager;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 39
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/StoreStatusObservable$1;->this$0:Lcom/google/android/videos/pano/repositories/StoreStatusObservable;

    invoke-virtual {v0}, Lcom/google/android/videos/pano/repositories/StoreStatusObservable;->update()V

    .line 40
    return-void
.end method

.method public lastUpdatableRemoved()V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/StoreStatusObservable$1;->val$storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/StoreStatusObservable$1;->this$0:Lcom/google/android/videos/pano/repositories/StoreStatusObservable;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/store/StoreStatusMonitor;->removeListener(Lcom/google/android/videos/store/StoreStatusMonitor$Listener;)V

    .line 45
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/StoreStatusObservable$1;->val$signInManager:Lcom/google/android/videos/accounts/SignInManager;

    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/StoreStatusObservable$1;->this$0:Lcom/google/android/videos/pano/repositories/StoreStatusObservable;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/accounts/SignInManager;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 46
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/StoreStatusObservable$1;->val$storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    invoke-virtual {v0}, Lcom/google/android/videos/store/StoreStatusMonitor;->reset()V

    .line 47
    return-void
.end method
