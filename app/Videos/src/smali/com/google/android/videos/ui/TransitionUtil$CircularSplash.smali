.class public Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;
.super Landroid/transition/Transition;
.source "TransitionUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/TransitionUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CircularSplash"
.end annotation


# instance fields
.field private centerView:Landroid/view/View;

.field private centerViewBounds:Landroid/graphics/Rect;

.field private combinedBounds:Landroid/graphics/Rect;

.field private final isReveal:Z

.field private smallerRadius:I


# direct methods
.method public constructor <init>(Z)V
    .locals 0
    .param p1, "isReveal"    # Z

    .prologue
    .line 92
    invoke-direct {p0}, Landroid/transition/Transition;-><init>()V

    .line 93
    iput-boolean p1, p0, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->isReveal:Z

    .line 94
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->isReveal:Z

    return v0
.end method

.method private static expandSize(Landroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 3
    .param p0, "first"    # Landroid/graphics/Rect;
    .param p1, "second"    # Landroid/graphics/Rect;

    .prologue
    .line 183
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v0, v1, 0x2

    .line 184
    .local v0, "delta":I
    if-lez v0, :cond_0

    .line 185
    iget v1, p0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v0

    iput v1, p0, Landroid/graphics/Rect;->left:I

    .line 186
    iget v1, p0, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v0

    iput v1, p0, Landroid/graphics/Rect;->right:I

    .line 188
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v0, v1, 0x2

    .line 189
    if-lez v0, :cond_1

    .line 190
    iget v1, p0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v0

    iput v1, p0, Landroid/graphics/Rect;->top:I

    .line 191
    iget v1, p0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v0

    iput v1, p0, Landroid/graphics/Rect;->bottom:I

    .line 193
    :cond_1
    return-void
.end method

.method private getScreenBounds(Landroid/view/View;)Landroid/graphics/Rect;
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 119
    invoke-static {p1}, Lcom/google/android/play/transition/PlayTransitionUtil;->viewBounds(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    .line 120
    .local v0, "bounds":Landroid/graphics/Rect;
    const/4 v2, 0x2

    new-array v1, v2, [I

    .line 121
    .local v1, "screenTopLeft":[I
    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 122
    const/4 v2, 0x0

    aget v2, v1, v2

    const/4 v3, 0x1

    aget v3, v1, v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 123
    return-object v0
.end method


# virtual methods
.method public captureEndValues(Landroid/transition/TransitionValues;)V
    .locals 3
    .param p1, "values"    # Landroid/transition/TransitionValues;

    .prologue
    .line 114
    iget-object v0, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "dummy"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    iget-object v0, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "bounds"

    iget-object v2, p1, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    invoke-direct {p0, v2}, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->getScreenBounds(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    return-void
.end method

.method public captureStartValues(Landroid/transition/TransitionValues;)V
    .locals 4
    .param p1, "values"    # Landroid/transition/TransitionValues;

    .prologue
    .line 98
    iget-object v1, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v2, "dummy"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    iget-object v1, p1, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    invoke-direct {p0, v1}, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->getScreenBounds(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    .line 100
    .local v0, "bounds":Landroid/graphics/Rect;
    iget-object v1, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v2, "bounds"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    iget-object v1, p1, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->centerView:Landroid/view/View;

    if-ne v1, v2, :cond_0

    .line 103
    iput-object v0, p0, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->centerViewBounds:Landroid/graphics/Rect;

    .line 105
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->combinedBounds:Landroid/graphics/Rect;

    if-nez v1, :cond_1

    .line 106
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v0}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v1, p0, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->combinedBounds:Landroid/graphics/Rect;

    .line 110
    :goto_0
    return-void

    .line 108
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->combinedBounds:Landroid/graphics/Rect;

    invoke-virtual {v1, v0}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public createAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
    .locals 12
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "startValues"    # Landroid/transition/TransitionValues;
    .param p3, "endValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 129
    iget-object v9, p2, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v10, "bounds"

    invoke-interface {v9, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Rect;

    .line 133
    .local v2, "bounds":Landroid/graphics/Rect;
    iget-object v9, p3, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v10, "bounds"

    invoke-interface {v9, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Rect;

    .line 134
    .local v5, "endBounds":Landroid/graphics/Rect;
    invoke-static {v2, v5}, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->expandSize(Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 135
    iget-object v9, p0, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->combinedBounds:Landroid/graphics/Rect;

    invoke-virtual {v9, v2}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 137
    iget-object v9, p0, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->centerViewBounds:Landroid/graphics/Rect;

    if-nez v9, :cond_1

    .line 138
    iget-object v9, p0, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->centerView:Landroid/view/View;

    if-eqz v9, :cond_0

    .line 140
    const/4 v0, 0x0

    .line 179
    :goto_0
    return-object v0

    .line 142
    :cond_0
    iput-object v2, p0, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->centerViewBounds:Landroid/graphics/Rect;

    .line 145
    :cond_1
    iget-object v7, p3, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 147
    .local v7, "view":Landroid/view/View;
    iget-object v9, p0, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->combinedBounds:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v8

    .line 148
    .local v8, "w":I
    iget-object v9, p0, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->combinedBounds:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v6

    .line 149
    .local v6, "h":I
    mul-int v9, v8, v8

    mul-int v10, v6, v6

    add-int/2addr v9, v10

    int-to-double v10, v9

    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    double-to-float v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float v1, v9, v10

    .line 151
    .local v1, "bigRadius":F
    iget-object v9, p0, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->centerViewBounds:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->centerX()I

    move-result v9

    iget v10, v2, Landroid/graphics/Rect;->left:I

    sub-int v3, v9, v10

    .line 152
    .local v3, "centerX":I
    iget-object v9, p0, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->centerViewBounds:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->centerY()I

    move-result v9

    iget v10, v2, Landroid/graphics/Rect;->top:I

    sub-int v4, v9, v10

    .line 154
    .local v4, "centerY":I
    iget-boolean v9, p0, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->isReveal:Z

    if-eqz v9, :cond_2

    iget v9, p0, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->smallerRadius:I

    int-to-float v9, v9

    :goto_1
    iget-boolean v10, p0, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->isReveal:Z

    if-eqz v10, :cond_3

    .end local v1    # "bigRadius":F
    :goto_2
    invoke-static {v7, v3, v4, v9, v1}, Lcom/google/android/videos/ui/TransitionUtil;->createTransitionSafeCircularReveal(Landroid/view/View;IIFF)Landroid/animation/Animator;

    move-result-object v0

    .line 159
    .local v0, "animator":Landroid/animation/Animator;
    new-instance v9, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash$1;

    invoke-direct {v9, p0, v7}, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash$1;-><init>(Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;Landroid/view/View;)V

    invoke-virtual {v0, v9}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0

    .end local v0    # "animator":Landroid/animation/Animator;
    .restart local v1    # "bigRadius":F
    :cond_2
    move v9, v1

    .line 154
    goto :goto_1

    :cond_3
    iget v10, p0, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->smallerRadius:I

    int-to-float v1, v10

    goto :goto_2
.end method

.method public setCenter(II)V
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 88
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p1, p2, p1, p2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->centerViewBounds:Landroid/graphics/Rect;

    .line 89
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->centerViewBounds:Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v0, p0, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->combinedBounds:Landroid/graphics/Rect;

    .line 90
    return-void
.end method

.method public setCenterOn(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->centerView:Landroid/view/View;

    .line 74
    return-void
.end method

.method public setSmallerRadius(I)V
    .locals 0
    .param p1, "pixels"    # I

    .prologue
    .line 80
    iput p1, p0, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->smallerRadius:I

    .line 81
    return-void
.end method
