.class public Lcom/google/android/videos/activity/DmaReconsentFragment;
.super Landroid/support/v4/app/Fragment;
.source "DmaReconsentFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/activity/DmaReconsentFragment$InProgressDialogFragment;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/app/Fragment;",
        "Landroid/content/DialogInterface$OnClickListener;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Ljava/lang/String;",
        "Lcom/google/wireless/android/video/magma/proto/UserConfiguration;",
        ">;"
    }
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

.field private dialog:Landroid/app/AlertDialog;

.field private linkedAccountRequestCallback:Lcom/google/android/videos/async/CancelableCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/CancelableCallback",
            "<",
            "Lcom/google/android/videos/api/LinkedAccountRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private preferences:Landroid/content/SharedPreferences;

.field private videosGlobals:Lcom/google/android/videos/VideosGlobals;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 205
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/activity/DmaReconsentFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/activity/DmaReconsentFragment;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->account:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/activity/DmaReconsentFragment;)Lcom/google/android/videos/store/ConfigurationStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/activity/DmaReconsentFragment;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/activity/DmaReconsentFragment;)Lcom/google/android/videos/VideosGlobals;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/activity/DmaReconsentFragment;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/videos/activity/DmaReconsentFragment;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/activity/DmaReconsentFragment;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->dialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method public static addInstance(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;)V
    .locals 4
    .param p0, "activity"    # Landroid/support/v4/app/FragmentActivity;
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 51
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 52
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    new-instance v1, Lcom/google/android/videos/activity/DmaReconsentFragment;

    invoke-direct {v1}, Lcom/google/android/videos/activity/DmaReconsentFragment;-><init>()V

    .line 55
    .local v1, "instance":Landroid/support/v4/app/Fragment;
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 56
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    const-string v3, "DmaReconsentFragment"

    invoke-virtual {v2, v1, v3}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 57
    return-void
.end method

.method public static getInstance(Landroid/support/v4/app/FragmentActivity;)Lcom/google/android/videos/activity/DmaReconsentFragment;
    .locals 2
    .param p0, "activity"    # Landroid/support/v4/app/FragmentActivity;

    .prologue
    .line 60
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 61
    .local v0, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    const-string v1, "DmaReconsentFragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/activity/DmaReconsentFragment;

    return-object v1
.end method

.method private static removeInstance(Landroid/support/v4/app/FragmentActivity;)V
    .locals 2
    .param p0, "activity"    # Landroid/support/v4/app/FragmentActivity;

    .prologue
    .line 65
    invoke-static {p0}, Lcom/google/android/videos/activity/DmaReconsentFragment;->getInstance(Landroid/support/v4/app/FragmentActivity;)Lcom/google/android/videos/activity/DmaReconsentFragment;

    move-result-object v0

    .line 66
    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    if-eqz v0, :cond_0

    .line 67
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 70
    :cond_0
    return-void
.end method


# virtual methods
.method public onCancel()V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->linkedAccountRequestCallback:Lcom/google/android/videos/async/CancelableCallback;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->linkedAccountRequestCallback:Lcom/google/android/videos/async/CancelableCallback;

    invoke-virtual {v0}, Lcom/google/android/videos/async/CancelableCallback;->cancel()V

    .line 106
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->linkedAccountRequestCallback:Lcom/google/android/videos/async/CancelableCallback;

    .line 108
    :cond_0
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v5, -0x2

    const/4 v3, 0x1

    .line 155
    const/4 v4, -0x1

    if-eq p2, v4, :cond_0

    if-eq p2, v5, :cond_0

    .line 200
    :goto_0
    return-void

    .line 159
    :cond_0
    if-ne p2, v5, :cond_1

    move v2, v3

    .line 160
    .local v2, "unlinkAccounts":Z
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/videos/activity/DmaReconsentFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 161
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    new-instance v4, Lcom/google/android/videos/activity/DmaReconsentFragment$1;

    invoke-direct {v4, p0, v0, v2}, Lcom/google/android/videos/activity/DmaReconsentFragment$1;-><init>(Lcom/google/android/videos/activity/DmaReconsentFragment;Landroid/support/v4/app/FragmentActivity;Z)V

    invoke-static {v4}, Lcom/google/android/videos/async/CancelableCallback;->create(Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/CancelableCallback;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->linkedAccountRequestCallback:Lcom/google/android/videos/async/CancelableCallback;

    .line 188
    invoke-static {v0}, Lcom/google/android/videos/activity/DmaReconsentFragment$InProgressDialogFragment;->showInstance(Landroid/support/v4/app/FragmentActivity;)V

    .line 189
    iget-object v4, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->linkedAccountRequestCallback:Lcom/google/android/videos/async/CancelableCallback;

    invoke-static {v0, v4}, Lcom/google/android/videos/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/ActivityCallback;

    move-result-object v1

    .line 191
    .local v1, "activityCallback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/api/LinkedAccountRequest;Ljava/lang/Void;>;"
    if-eqz v2, :cond_2

    .line 192
    iget-object v4, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/videos/api/ApiRequesters;->getUnlinkAccountRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v4

    new-instance v5, Lcom/google/android/videos/api/LinkedAccountRequest;

    iget-object v6, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->account:Ljava/lang/String;

    invoke-direct {v5, v6, v3}, Lcom/google/android/videos/api/LinkedAccountRequest;-><init>(Ljava/lang/String;I)V

    invoke-interface {v4, v5, v1}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    goto :goto_0

    .line 159
    .end local v0    # "activity":Landroid/support/v4/app/FragmentActivity;
    .end local v1    # "activityCallback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/api/LinkedAccountRequest;Ljava/lang/Void;>;"
    .end local v2    # "unlinkAccounts":Z
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 196
    .restart local v0    # "activity":Landroid/support/v4/app/FragmentActivity;
    .restart local v1    # "activityCallback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/api/LinkedAccountRequest;Ljava/lang/Void;>;"
    .restart local v2    # "unlinkAccounts":Z
    :cond_2
    iget-object v4, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/videos/api/ApiRequesters;->getUpdateAccountLinkingRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v4

    new-instance v5, Lcom/google/android/videos/api/LinkedAccountRequest;

    iget-object v6, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->account:Ljava/lang/String;

    invoke-direct {v5, v6, v3}, Lcom/google/android/videos/api/LinkedAccountRequest;-><init>(Ljava/lang/String;I)V

    invoke-interface {v4, v5, v1}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 74
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 76
    invoke-virtual {p0}, Lcom/google/android/videos/activity/DmaReconsentFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "authAccount"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->account:Ljava/lang/String;

    .line 77
    invoke-virtual {p0}, Lcom/google/android/videos/activity/DmaReconsentFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 79
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    invoke-static {v0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    .line 80
    iget-object v2, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    .line 81
    iget-object v2, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->preferences:Landroid/content/SharedPreferences;

    .line 83
    invoke-static {v0, p0}, Lcom/google/android/videos/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/ActivityCallback;

    move-result-object v1

    .line 84
    .local v1, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/UserConfiguration;>;"
    iget-object v2, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    iget-object v3, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->account:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Lcom/google/android/videos/store/ConfigurationStore;->requestUserConfiguration(Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V

    .line 85
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 37
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/activity/DmaReconsentFragment;->onError(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public onError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "request"    # Ljava/lang/String;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 148
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 89
    iget-object v1, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->dialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 90
    iget-object v1, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 91
    iput-object v2, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->dialog:Landroid/app/AlertDialog;

    .line 93
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->linkedAccountRequestCallback:Lcom/google/android/videos/async/CancelableCallback;

    if-eqz v1, :cond_1

    .line 94
    iget-object v1, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->linkedAccountRequestCallback:Lcom/google/android/videos/async/CancelableCallback;

    invoke-virtual {v1}, Lcom/google/android/videos/async/CancelableCallback;->cancel()V

    .line 95
    iput-object v2, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->linkedAccountRequestCallback:Lcom/google/android/videos/async/CancelableCallback;

    .line 97
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/videos/activity/DmaReconsentFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 98
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    invoke-static {v0}, Lcom/google/android/videos/activity/DmaReconsentFragment$InProgressDialogFragment;->dismiss(Landroid/support/v4/app/FragmentActivity;)V

    .line 99
    invoke-static {v0}, Lcom/google/android/videos/activity/DmaReconsentFragment;->removeInstance(Landroid/support/v4/app/FragmentActivity;)V

    .line 100
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 101
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 37
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/activity/DmaReconsentFragment;->onResponse(Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/UserConfiguration;)V

    return-void
.end method

.method public onResponse(Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/UserConfiguration;)V
    .locals 12
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "response"    # Lcom/google/wireless/android/video/magma/proto/UserConfiguration;

    .prologue
    .line 114
    iget-object v6, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->account:Ljava/lang/String;

    invoke-static {v6, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 143
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    const/4 v1, 0x0

    .line 119
    .local v1, "dmaLink":Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;
    if-eqz p2, :cond_2

    iget-object v6, p2, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->accountLink:[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    if-nez v6, :cond_4

    :cond_2
    const/4 v3, 0x0

    .line 121
    .local v3, "linkCount":I
    :goto_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    if-ge v2, v3, :cond_3

    .line 122
    iget-object v6, p2, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->accountLink:[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    aget-object v0, v6, v2

    .line 123
    .local v0, "accountLink":Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;
    iget-boolean v6, v0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->linked:Z

    if-eqz v6, :cond_5

    iget-object v6, v0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->partner:Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;

    if-eqz v6, :cond_5

    iget-object v6, v0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->partner:Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;

    iget v6, v6, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;->id:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_5

    .line 125
    move-object v1, v0

    .line 130
    .end local v0    # "accountLink":Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 131
    .local v4, "timestamp":J
    if-eqz v1, :cond_0

    iget-wide v6, v1, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->linkReconsentTimestampSec:J

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    cmp-long v6, v6, v4

    if-gez v6, :cond_0

    iget-wide v6, v1, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->consentDialogFrequencySec:J

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    iget-object v8, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->preferences:Landroid/content/SharedPreferences;

    const-string v9, "reconsent_dialog_shown_timestamp"

    const-wide/16 v10, 0x0

    invoke-interface {v8, v9, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    sub-long v8, v4, v8

    cmp-long v6, v6, v8

    if-gez v6, :cond_0

    .line 134
    iget-object v6, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "reconsent_dialog_shown_timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-interface {v6, v7, v8, v9}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 137
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/videos/activity/DmaReconsentFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v7, 0x7f0b0232

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object p1, v8, v9

    invoke-virtual {p0, v7, v8}, Lcom/google/android/videos/activity/DmaReconsentFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f0b01e7

    invoke-virtual {v6, v7, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f0b01e8

    invoke-virtual {v6, v7, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/videos/activity/DmaReconsentFragment;->dialog:Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 119
    .end local v2    # "i":I
    .end local v3    # "linkCount":I
    .end local v4    # "timestamp":J
    :cond_4
    iget-object v6, p2, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->accountLink:[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    array-length v3, v6

    goto/16 :goto_1

    .line 121
    .restart local v0    # "accountLink":Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;
    .restart local v2    # "i":I
    .restart local v3    # "linkCount":I
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2
.end method
