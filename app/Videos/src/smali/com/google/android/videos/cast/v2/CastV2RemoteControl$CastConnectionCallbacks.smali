.class Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastConnectionCallbacks;
.super Ljava/lang/Object;
.source "CastV2RemoteControl.java"

# interfaces
.implements Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/cast/v2/CastV2RemoteControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CastConnectionCallbacks"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)V
    .locals 0

    .prologue
    .line 1050
    iput-object p1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastConnectionCallbacks;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;
    .param p2, "x1"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;

    .prologue
    .line 1050
    invoke-direct {p0, p1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastConnectionCallbacks;-><init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)V

    return-void
.end method


# virtual methods
.method public onConnected(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "connectionHint"    # Landroid/os/Bundle;

    .prologue
    .line 1064
    const-string v0, "onConnected"

    invoke-static {v0}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 1065
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastConnectionCallbacks;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # invokes: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->isConnectionClosed()Z
    invoke-static {v0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$3100(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1071
    :goto_0
    return-void

    .line 1070
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastConnectionCallbacks;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # invokes: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->onGmsConnected()V
    invoke-static {v0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$3200(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)V

    goto :goto_0
.end method

.method public onConnectionSuspended(I)V
    .locals 3
    .param p1, "cause"    # I

    .prologue
    const/4 v2, 0x1

    .line 1053
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onConnectionSuspended: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 1054
    if-ne p1, v2, :cond_1

    .line 1055
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastConnectionCallbacks;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->disconnect(Z)V
    invoke-static {v0, v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$1800(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Z)V

    .line 1060
    :cond_0
    :goto_0
    return-void

    .line 1056
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastConnectionCallbacks;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # getter for: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->state:I
    invoke-static {v0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$300(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 1058
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastConnectionCallbacks;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # invokes: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->setState(I)V
    invoke-static {v0, v2}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$3000(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;I)V

    goto :goto_0
.end method
