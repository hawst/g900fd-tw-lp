.class final Lcom/google/android/videos/ui/DownloadItemView$2;
.super Lcom/google/android/videos/ui/DownloadItemView$Binder;
.source "DownloadItemView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/ui/DownloadItemView;->newEpisodesBinder(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/logging/UiElementNode;)Lcom/google/android/videos/ui/DownloadItemView$Binder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/ui/DownloadItemView$Binder",
        "<",
        "Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/logging/UiElementNode;)V
    .locals 0
    .param p2, "x1"    # Lcom/google/android/videos/logging/UiElementNode;

    .prologue
    .line 221
    .local p1, "x0":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/ui/DownloadItemView$Binder;-><init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/logging/UiElementNode;)V

    return-void
.end method


# virtual methods
.method protected onBind(Lcom/google/android/videos/ui/DownloadItemView;Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;I)V
    .locals 1
    .param p1, "view"    # Lcom/google/android/videos/ui/DownloadItemView;
    .param p2, "dataSource"    # Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;
    .param p3, "index"    # I

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/videos/ui/DownloadItemView$2;->posterRequester:Lcom/google/android/videos/async/Requester;

    # invokes: Lcom/google/android/videos/ui/DownloadItemView;->bindEpisode(Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;ILcom/google/android/videos/async/Requester;)V
    invoke-static {p1, p2, p3, v0}, Lcom/google/android/videos/ui/DownloadItemView;->access$100(Lcom/google/android/videos/ui/DownloadItemView;Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;ILcom/google/android/videos/async/Requester;)V

    .line 225
    return-void
.end method

.method protected bridge synthetic onBind(Lcom/google/android/videos/ui/playnext/ClusterItemView;Lcom/google/android/videos/adapter/DataSource;I)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/ui/playnext/ClusterItemView;
    .param p2, "x1"    # Lcom/google/android/videos/adapter/DataSource;
    .param p3, "x2"    # I

    .prologue
    .line 221
    check-cast p1, Lcom/google/android/videos/ui/DownloadItemView;

    .end local p1    # "x0":Lcom/google/android/videos/ui/playnext/ClusterItemView;
    check-cast p2, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    .end local p2    # "x1":Lcom/google/android/videos/adapter/DataSource;
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/videos/ui/DownloadItemView$2;->onBind(Lcom/google/android/videos/ui/DownloadItemView;Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;I)V

    return-void
.end method
