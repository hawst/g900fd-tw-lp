.class Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnableV16;
.super Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;
.source "PurchaseStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/PurchaseStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GetPurchasesRunnableV16"
.end annotation


# direct methods
.method public constructor <init>(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/videos/async/Callback;ZZLjava/lang/String;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "request"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .param p4, "contentRestrictionsEnabled"    # Z
    .param p5, "allowUnratedContent"    # Z
    .param p6, "inAllowedRatingIdsSql"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
            "Landroid/database/Cursor;",
            ">;ZZ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 343
    .local p3, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;>;"
    invoke-direct/range {p0 .. p6}, Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;-><init>(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/videos/async/Callback;ZZLjava/lang/String;)V

    .line 345
    return-void
.end method


# virtual methods
.method protected rawQuery(Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/videos/store/CancellationSignalHolder;)Landroid/database/Cursor;
    .locals 2
    .param p1, "sql"    # Ljava/lang/String;
    .param p2, "whereArgs"    # [Ljava/lang/String;
    .param p3, "holder"    # Lcom/google/android/videos/store/CancellationSignalHolder;

    .prologue
    .line 361
    iget-object v1, p0, Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnableV16;->db:Landroid/database/sqlite/SQLiteDatabase;

    if-nez p3, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, p1, p2, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p3, Lcom/google/android/videos/store/CancellationSignalHolder;->cancellationSignal:Landroid/os/CancellationSignal;

    goto :goto_0
.end method

.method public run()V
    .locals 4

    .prologue
    .line 351
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnableV16;->getCursor()Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/os/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 356
    .local v0, "cursor":Landroid/database/Cursor;
    iget-object v2, p0, Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnableV16;->callback:Lcom/google/android/videos/async/Callback;

    iget-object v3, p0, Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnableV16;->request:Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    invoke-interface {v2, v3, v0}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 357
    .end local v0    # "cursor":Landroid/database/Cursor;
    :goto_0
    return-void

    .line 352
    :catch_0
    move-exception v1

    .line 353
    .local v1, "e":Landroid/os/OperationCanceledException;
    iget-object v2, p0, Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnableV16;->callback:Lcom/google/android/videos/async/Callback;

    iget-object v3, p0, Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnableV16;->request:Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    invoke-interface {v2, v3, v1}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method
