.class final Lcom/google/android/videos/pano/nowtifications/NowtificationDispatcher;
.super Ljava/lang/Object;
.source "NowtificationDispatcher.java"


# instance fields
.field private final notificationManager:Landroid/app/NotificationManager;

.field private final sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/videos/pano/nowtifications/NowtificationDispatcher;->notificationManager:Landroid/app/NotificationManager;

    .line 31
    const-string v0, "nowtification-entity"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pano/nowtifications/NowtificationDispatcher;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 32
    return-void
.end method


# virtual methods
.method public dispatch(Lcom/google/android/repolib/repositories/Repository;)V
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/repolib/repositories/Repository",
            "<",
            "Lcom/google/android/videos/pano/nowtifications/Nowtification;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p1, "nowtifications":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<Lcom/google/android/videos/pano/nowtifications/Nowtification;>;"
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/videos/pano/nowtifications/NowtificationDispatcher;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v11, "nowtification-entity"

    const/4 v12, 0x0

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v9

    .line 38
    .local v9, "previousEntityIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v9, :cond_2

    .line 39
    invoke-interface/range {p1 .. p1}, Lcom/google/android/repolib/repositories/Repository;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/videos/pano/nowtifications/Nowtification;

    .line 40
    .local v7, "nowtification":Lcom/google/android/videos/pano/nowtifications/Nowtification;
    invoke-virtual {v7}, Lcom/google/android/videos/pano/nowtifications/Nowtification;->getEntityId()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 42
    .end local v7    # "nowtification":Lcom/google/android/videos/pano/nowtifications/Nowtification;
    :cond_0
    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 43
    .local v3, "entityId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/videos/pano/nowtifications/NowtificationDispatcher;->notificationManager:Landroid/app/NotificationManager;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "nowtification-prefix-"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    goto :goto_1

    .line 45
    .end local v3    # "entityId":Ljava/lang/String;
    :cond_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/videos/pano/nowtifications/NowtificationDispatcher;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v10

    const-string v11, "nowtification-entity"

    invoke-interface {v10, v11}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v10

    invoke-interface {v10}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 49
    .end local v5    # "i$":Ljava/util/Iterator;
    :goto_2
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 50
    .local v2, "currentEntityIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface/range {p1 .. p1}, Lcom/google/android/repolib/repositories/Repository;->indexer()Lcom/google/android/repolib/common/Indexer;

    move-result-object v6

    .line 51
    .local v6, "indexer":Lcom/google/android/repolib/common/Indexer;, "Lcom/google/android/repolib/common/Indexer<Lcom/google/android/videos/pano/nowtifications/Nowtification;>;"
    invoke-interface {v6}, Lcom/google/android/repolib/common/Indexer;->size()I

    move-result v8

    .line 52
    .local v8, "nowtificationCount":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_3
    if-ge v4, v8, :cond_3

    .line 53
    invoke-interface {v6, v4}, Lcom/google/android/repolib/common/Indexer;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/videos/pano/nowtifications/Nowtification;

    .line 54
    .restart local v7    # "nowtification":Lcom/google/android/videos/pano/nowtifications/Nowtification;
    invoke-virtual {v7}, Lcom/google/android/videos/pano/nowtifications/Nowtification;->getEntityId()Ljava/lang/String;

    move-result-object v3

    .line 55
    .restart local v3    # "entityId":Ljava/lang/String;
    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 56
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/videos/pano/nowtifications/NowtificationDispatcher;->notificationManager:Landroid/app/NotificationManager;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "nowtification-prefix-"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v7}, Lcom/google/android/videos/pano/nowtifications/Nowtification;->getNotification()Landroid/app/Notification$Builder;

    move-result-object v13

    const/4 v14, -0x2

    rsub-int/lit8 v15, v4, 0x2

    invoke-static {v14, v15}, Ljava/lang/Math;->max(II)I

    move-result v14

    invoke-virtual {v13, v14}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    move-result-object v13

    sub-int v14, v8, v4

    int-to-double v14, v14

    int-to-double v0, v8

    move-wide/from16 v16, v0

    const-wide/high16 v18, 0x3ff0000000000000L    # 1.0

    add-double v16, v16, v18

    div-double v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/app/Notification$Builder;->setSortKey(Ljava/lang/String;)Landroid/app/Notification$Builder;

    move-result-object v13

    invoke-virtual {v13}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v13

    invoke-virtual {v10, v11, v12, v13}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 52
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 47
    .end local v2    # "currentEntityIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v3    # "entityId":Ljava/lang/String;
    .end local v4    # "i":I
    .end local v6    # "indexer":Lcom/google/android/repolib/common/Indexer;, "Lcom/google/android/repolib/common/Indexer<Lcom/google/android/videos/pano/nowtifications/Nowtification;>;"
    .end local v7    # "nowtification":Lcom/google/android/videos/pano/nowtifications/Nowtification;
    .end local v8    # "nowtificationCount":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/videos/pano/nowtifications/NowtificationDispatcher;->notificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v10}, Landroid/app/NotificationManager;->cancelAll()V

    goto :goto_2

    .line 63
    .restart local v2    # "currentEntityIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v4    # "i":I
    .restart local v6    # "indexer":Lcom/google/android/repolib/common/Indexer;, "Lcom/google/android/repolib/common/Indexer<Lcom/google/android/videos/pano/nowtifications/Nowtification;>;"
    .restart local v8    # "nowtificationCount":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/videos/pano/nowtifications/NowtificationDispatcher;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v10

    const-string v11, "nowtification-entity"

    invoke-interface {v10, v11, v2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v10

    invoke-interface {v10}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 64
    return-void
.end method
