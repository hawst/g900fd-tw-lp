.class Lcom/google/android/videos/ui/DefaultItemAnimator$1;
.super Lcom/google/android/videos/ui/DefaultItemAnimator$VpaListenerAdapter;
.source "DefaultItemAnimator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/ui/DefaultItemAnimator;->runRemoveAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/ui/DefaultItemAnimator;

.field final synthetic val$animation:Landroid/support/v4/view/ViewPropertyAnimatorCompat;

.field final synthetic val$holder:Landroid/support/v7/widget/RecyclerView$ViewHolder;


# direct methods
.method constructor <init>(Lcom/google/android/videos/ui/DefaultItemAnimator;Landroid/support/v7/widget/RecyclerView$ViewHolder;Landroid/support/v4/view/ViewPropertyAnimatorCompat;)V
    .locals 0

    .prologue
    .line 143
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator$1;, "Lcom/google/android/videos/ui/DefaultItemAnimator.1;"
    iput-object p1, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$1;->this$0:Lcom/google/android/videos/ui/DefaultItemAnimator;

    iput-object p2, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$1;->val$holder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    iput-object p3, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$1;->val$animation:Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    invoke-direct {p0}, Lcom/google/android/videos/ui/DefaultItemAnimator$VpaListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 151
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator$1;, "Lcom/google/android/videos/ui/DefaultItemAnimator.1;"
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$1;->val$animation:Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setListener(Landroid/support/v4/view/ViewPropertyAnimatorListener;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    .line 152
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p1, v0}, Landroid/support/v4/view/ViewCompat;->setAlpha(Landroid/view/View;F)V

    .line 153
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$1;->this$0:Lcom/google/android/videos/ui/DefaultItemAnimator;

    iget-object v1, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$1;->val$holder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/DefaultItemAnimator;->dispatchRemoveFinished(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 154
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$1;->this$0:Lcom/google/android/videos/ui/DefaultItemAnimator;

    # getter for: Lcom/google/android/videos/ui/DefaultItemAnimator;->mRemoveAnimations:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/videos/ui/DefaultItemAnimator;->access$000(Lcom/google/android/videos/ui/DefaultItemAnimator;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$1;->val$holder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 155
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$1;->this$0:Lcom/google/android/videos/ui/DefaultItemAnimator;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/DefaultItemAnimator;->dispatchFinishedWhenDone()V

    .line 156
    return-void
.end method

.method public onAnimationStart(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 146
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator$1;, "Lcom/google/android/videos/ui/DefaultItemAnimator.1;"
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$1;->this$0:Lcom/google/android/videos/ui/DefaultItemAnimator;

    iget-object v1, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$1;->val$holder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/DefaultItemAnimator;->dispatchRemoveStarting(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 147
    return-void
.end method
