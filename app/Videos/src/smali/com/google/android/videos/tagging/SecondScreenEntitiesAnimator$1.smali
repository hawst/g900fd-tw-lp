.class Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$1;
.super Lcom/google/android/videos/ui/DefaultItemAnimator$VpaListenerAdapter;
.source "SecondScreenEntitiesAnimator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->runRemoveAnimation(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;

.field final synthetic val$animation:Landroid/support/v4/view/ViewPropertyAnimatorCompat;

.field final synthetic val$holder:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;


# direct methods
.method constructor <init>(Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;Landroid/support/v4/view/ViewPropertyAnimatorCompat;)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$1;->this$0:Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;

    iput-object p2, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$1;->val$holder:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    iput-object p3, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$1;->val$animation:Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    invoke-direct {p0}, Lcom/google/android/videos/ui/DefaultItemAnimator$VpaListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$1;->val$animation:Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setListener(Landroid/support/v4/view/ViewPropertyAnimatorListener;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    .line 37
    iget-object v0, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$1;->this$0:Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;

    iget-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$1;->val$holder:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    # invokes: Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->onRemoveAnimationEnd(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    invoke-static {v0, v1}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->access$000(Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 38
    return-void
.end method

.method public onAnimationStart(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$1;->this$0:Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;

    iget-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$1;->val$holder:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->dispatchRemoveStarting(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 32
    return-void
.end method
