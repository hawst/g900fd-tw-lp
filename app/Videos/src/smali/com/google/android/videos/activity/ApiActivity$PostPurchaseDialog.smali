.class public Lcom/google/android/videos/activity/ApiActivity$PostPurchaseDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "ApiActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/activity/ApiActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PostPurchaseDialog"
.end annotation


# instance fields
.field private watchOnPlay:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 473
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static showInstance(Lcom/google/android/videos/activity/ApiActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "activity"    # Lcom/google/android/videos/activity/ApiActivity;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "referer"    # Ljava/lang/String;
    .param p4, "callingPackage"    # Ljava/lang/String;

    .prologue
    .line 480
    # getter for: Lcom/google/android/videos/activity/ApiActivity;->eventLogger:Lcom/google/android/videos/logging/EventLogger;
    invoke-static {p0}, Lcom/google/android/videos/activity/ApiActivity;->access$400(Lcom/google/android/videos/activity/ApiActivity;)Lcom/google/android/videos/logging/EventLogger;

    move-result-object v2

    invoke-interface {v2, p2, p3}, Lcom/google/android/videos/logging/EventLogger;->onPostPurchaseDialogOpened(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 483
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    const-string v2, "video_id"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    const-string v2, "CALLING_PACKAGE"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    new-instance v1, Lcom/google/android/videos/activity/ApiActivity$PostPurchaseDialog;

    invoke-direct {v1}, Lcom/google/android/videos/activity/ApiActivity$PostPurchaseDialog;-><init>()V

    .line 488
    .local v1, "instance":Lcom/google/android/videos/activity/ApiActivity$PostPurchaseDialog;
    invoke-virtual {v1, v0}, Lcom/google/android/videos/activity/ApiActivity$PostPurchaseDialog;->setArguments(Landroid/os/Bundle;)V

    .line 489
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ApiActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "PostPurchaseDialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/activity/ApiActivity$PostPurchaseDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 490
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 548
    iget-object v4, p0, Lcom/google/android/videos/activity/ApiActivity$PostPurchaseDialog;->watchOnPlay:Landroid/widget/TextView;

    if-ne p1, v4, :cond_0

    .line 549
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ApiActivity$PostPurchaseDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    .line 550
    .local v6, "args":Landroid/os/Bundle;
    const-string v4, "authAccount"

    invoke-virtual {v6, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 551
    .local v1, "account":Ljava/lang/String;
    const-string v4, "video_id"

    invoke-virtual {v6, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 552
    .local v2, "videoId":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ApiActivity$PostPurchaseDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 553
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    const/4 v5, 0x0

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/activity/WatchActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 556
    .end local v0    # "activity":Landroid/support/v4/app/FragmentActivity;
    .end local v1    # "account":Ljava/lang/String;
    .end local v2    # "videoId":Ljava/lang/String;
    .end local v6    # "args":Landroid/os/Bundle;
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ApiActivity$PostPurchaseDialog;->dismiss()V

    .line 557
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 496
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 497
    .local v0, "dialog":Landroid/app/Dialog;
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/Window;->requestFeature(I)Z

    .line 498
    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 17
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 505
    const v12, 0x7f0400ad

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v12, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 507
    .local v7, "content":Landroid/view/View;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/activity/ApiActivity$PostPurchaseDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 508
    .local v11, "resources":Landroid/content/res/Resources;
    const v12, 0x7f0e01ea

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v12

    float-to-int v9, v12

    .line 510
    .local v9, "iconSize":I
    const v12, 0x7f0f01e0

    invoke-virtual {v7, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/android/videos/activity/ApiActivity$PostPurchaseDialog;->watchOnPlay:Landroid/widget/TextView;

    .line 511
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/activity/ApiActivity$PostPurchaseDialog;->watchOnPlay:Landroid/widget/TextView;

    const v13, 0x7f0b0087

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    const v16, 0x7f0b007f

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/ApiActivity$PostPurchaseDialog;->getString(I)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Lcom/google/android/videos/activity/ApiActivity$PostPurchaseDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 513
    const/high16 v12, 0x7f030000

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 514
    .local v2, "applicationIcon":Landroid/graphics/drawable/Drawable;
    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v2, v12, v13, v9, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 515
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/activity/ApiActivity$PostPurchaseDialog;->watchOnPlay:Landroid/widget/TextView;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual {v12, v2, v13, v14, v15}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 516
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/activity/ApiActivity$PostPurchaseDialog;->watchOnPlay:Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-virtual {v12, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 518
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/activity/ApiActivity$PostPurchaseDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    .line 519
    .local v4, "args":Landroid/os/Bundle;
    const-string v12, "CALLING_PACKAGE"

    invoke-virtual {v4, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 520
    .local v6, "callingPackage":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/activity/ApiActivity$PostPurchaseDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v12

    invoke-virtual {v12}, Landroid/support/v4/app/FragmentActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    .line 521
    .local v10, "packageManager":Landroid/content/pm/PackageManager;
    const v12, 0x7f0f01e1

    invoke-virtual {v7, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 523
    .local v5, "backToPartner":Landroid/widget/TextView;
    const/4 v12, 0x0

    :try_start_0
    invoke-virtual {v10, v6, v12}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    .line 524
    .local v3, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    const v12, 0x7f0b0088

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-virtual {v10, v3}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v15

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13}, Lcom/google/android/videos/activity/ApiActivity$PostPurchaseDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 526
    invoke-virtual {v10, v3}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 527
    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v2, v12, v13, v9, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 528
    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v5, v2, v12, v13, v14}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 529
    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 530
    const/4 v12, 0x0

    invoke-virtual {v5, v12}, Landroid/widget/TextView;->setVisibility(I)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 534
    .end local v3    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    :goto_0
    return-object v7

    .line 531
    :catch_0
    move-exception v8

    .line 532
    .local v8, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/16 v12, 0x8

    invoke-virtual {v5, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 539
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 540
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ApiActivity$PostPurchaseDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/activity/ApiActivity;

    .line 541
    .local v0, "activity":Lcom/google/android/videos/activity/ApiActivity;
    # invokes: Lcom/google/android/videos/activity/ApiActivity;->finishWithSuccess()V
    invoke-static {v0}, Lcom/google/android/videos/activity/ApiActivity;->access$500(Lcom/google/android/videos/activity/ApiActivity;)V

    .line 542
    return-void
.end method
