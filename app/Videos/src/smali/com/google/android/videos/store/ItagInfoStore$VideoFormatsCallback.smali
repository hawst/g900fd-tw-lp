.class Lcom/google/android/videos/store/ItagInfoStore$VideoFormatsCallback;
.super Ljava/lang/Object;
.source "ItagInfoStore.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/ItagInfoStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VideoFormatsCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/async/EmptyRequest;",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/Integer;",
        "Lcom/google/wireless/android/video/magma/proto/VideoFormat;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final originalCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final originalRequestId:I

.field final synthetic this$0:Lcom/google/android/videos/store/ItagInfoStore;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/store/ItagInfoStore;ILcom/google/android/videos/async/Callback;)V
    .locals 1
    .param p2, "requestId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 112
    .local p3, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Ljava/lang/Integer;Ljava/lang/Void;>;"
    iput-object p1, p0, Lcom/google/android/videos/store/ItagInfoStore$VideoFormatsCallback;->this$0:Lcom/google/android/videos/store/ItagInfoStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    iput p2, p0, Lcom/google/android/videos/store/ItagInfoStore$VideoFormatsCallback;->originalRequestId:I

    .line 114
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Callback;

    iput-object v0, p0, Lcom/google/android/videos/store/ItagInfoStore$VideoFormatsCallback;->originalCallback:Lcom/google/android/videos/async/Callback;

    .line 115
    return-void
.end method

.method private convertDrmType(I)I
    .locals 2
    .param p1, "drmType"    # I

    .prologue
    .line 148
    packed-switch p1, :pswitch_data_0

    .line 154
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown DRM type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 155
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 149
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 150
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 151
    :pswitch_3
    const/4 v0, 0x1

    goto :goto_0

    .line 152
    :pswitch_4
    const/4 v0, 0x2

    goto :goto_0

    .line 148
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/async/EmptyRequest;Ljava/lang/Exception;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/async/EmptyRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/videos/store/ItagInfoStore$VideoFormatsCallback;->originalCallback:Lcom/google/android/videos/async/Callback;

    iget v1, p0, Lcom/google/android/videos/store/ItagInfoStore$VideoFormatsCallback;->originalRequestId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 145
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 106
    check-cast p1, Lcom/google/android/videos/async/EmptyRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/store/ItagInfoStore$VideoFormatsCallback;->onError(Lcom/google/android/videos/async/EmptyRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/async/EmptyRequest;Ljava/util/Map;)V
    .locals 12
    .param p1, "request"    # Lcom/google/android/videos/async/EmptyRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/EmptyRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/wireless/android/video/magma/proto/VideoFormat;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "response":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/google/wireless/android/video/magma/proto/VideoFormat;>;"
    const/4 v11, 0x0

    .line 119
    iget-object v1, p0, Lcom/google/android/videos/store/ItagInfoStore$VideoFormatsCallback;->this$0:Lcom/google/android/videos/store/ItagInfoStore;

    # getter for: Lcom/google/android/videos/store/ItagInfoStore;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v1}, Lcom/google/android/videos/store/ItagInfoStore;->access$000(Lcom/google/android/videos/store/ItagInfoStore;)Lcom/google/android/videos/store/Database;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    .line 120
    .local v7, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/wireless/android/video/magma/proto/VideoFormat;

    .line 122
    .local v8, "format":Lcom/google/wireless/android/video/magma/proto/VideoFormat;
    new-instance v0, Lcom/google/android/videos/streams/ItagInfo;

    iget v1, v8, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->width:I

    iget v2, v8, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->height:I

    iget v3, v8, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->audioChannels:I

    iget-boolean v4, v8, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->dash:Z

    iget v5, v8, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->drmType:I

    invoke-direct {p0, v5}, Lcom/google/android/videos/store/ItagInfoStore$VideoFormatsCallback;->convertDrmType(I)I

    move-result v5

    iget-boolean v6, v8, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->multi:Z

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/streams/ItagInfo;-><init>(IIIZIZ)V

    .line 125
    .local v0, "itagInfo":Lcom/google/android/videos/streams/ItagInfo;
    iget-object v1, p0, Lcom/google/android/videos/store/ItagInfoStore$VideoFormatsCallback;->this$0:Lcom/google/android/videos/store/ItagInfoStore;

    # getter for: Lcom/google/android/videos/store/ItagInfoStore;->itagsData:Ljava/util/concurrent/ConcurrentMap;
    invoke-static {v1}, Lcom/google/android/videos/store/ItagInfoStore;->access$100(Lcom/google/android/videos/store/ItagInfoStore;)Ljava/util/concurrent/ConcurrentMap;

    move-result-object v1

    iget v2, v8, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->itag:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 129
    .local v10, "values":Landroid/content/ContentValues;
    const-string v1, "itag"

    iget v2, v8, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->itag:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v10, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 130
    const-string v1, "width"

    iget v2, v0, Lcom/google/android/videos/streams/ItagInfo;->width:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v10, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 131
    const-string v1, "height"

    iget v2, v0, Lcom/google/android/videos/streams/ItagInfo;->height:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v10, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 132
    const-string v1, "audio_channels"

    iget v2, v0, Lcom/google/android/videos/streams/ItagInfo;->audioChannels:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v10, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 133
    const-string v1, "drm_type"

    iget v2, v0, Lcom/google/android/videos/streams/ItagInfo;->drmType:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v10, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 134
    const-string v1, "is_dash"

    iget-boolean v2, v0, Lcom/google/android/videos/streams/ItagInfo;->isDash:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v10, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 135
    const-string v1, "is_multi"

    iget-boolean v2, v0, Lcom/google/android/videos/streams/ItagInfo;->isMulti:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v10, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 136
    const-string v1, "video_formats"

    const/4 v2, 0x5

    invoke-virtual {v7, v1, v11, v10, v2}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    goto/16 :goto_0

    .line 139
    .end local v0    # "itagInfo":Lcom/google/android/videos/streams/ItagInfo;
    .end local v8    # "format":Lcom/google/wireless/android/video/magma/proto/VideoFormat;
    .end local v10    # "values":Landroid/content/ContentValues;
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/store/ItagInfoStore$VideoFormatsCallback;->originalCallback:Lcom/google/android/videos/async/Callback;

    iget v2, p0, Lcom/google/android/videos/store/ItagInfoStore$VideoFormatsCallback;->originalRequestId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v11}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 140
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 106
    check-cast p1, Lcom/google/android/videos/async/EmptyRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/util/Map;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/store/ItagInfoStore$VideoFormatsCallback;->onResponse(Lcom/google/android/videos/async/EmptyRequest;Ljava/util/Map;)V

    return-void
.end method
