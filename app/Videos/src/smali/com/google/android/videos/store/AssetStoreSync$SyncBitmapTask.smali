.class Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;
.super Lcom/google/android/videos/store/SyncTaskManager$SyncTask;
.source "AssetStoreSync.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/AssetStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncBitmapTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/store/SyncTaskManager$SyncTask;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/async/ConditionalHttpRequest",
        "<",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        ">;",
        "Lcom/google/android/videos/async/ConditionalHttpResponse",
        "<",
        "Lorg/apache/http/HttpEntity;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final bitmapItemIdColumn:Ljava/lang/String;

.field private final bitmapTable:Ljava/lang/String;

.field private final bitmapUri:Landroid/net/Uri;

.field private final databaseEvent:I

.field private final fileStore:Lcom/google/android/videos/store/FileStore;

.field private final itemId:Ljava/lang/String;

.field private final metadataBitmapSyncedColumn:Ljava/lang/String;

.field private final metadataItemIdColumn:Ljava/lang/String;

.field private final metadataTable:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/videos/store/AssetStoreSync;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/videos/store/FileStore;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p2, "priority"    # I
    .param p4, "itemId"    # Ljava/lang/String;
    .param p5, "bitmapUri"    # Landroid/net/Uri;
    .param p6, "fileStore"    # Lcom/google/android/videos/store/FileStore;
    .param p7, "metadataTable"    # Ljava/lang/String;
    .param p8, "metadataBitmapSyncedColumn"    # Ljava/lang/String;
    .param p9, "metadataItemIdColumn"    # Ljava/lang/String;
    .param p10, "bitmapTable"    # Ljava/lang/String;
    .param p11, "bitmapItemIdColumn"    # Ljava/lang/String;
    .param p12, "databaseEvent"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/store/SyncTaskManager$SharedStates",
            "<*>;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Lcom/google/android/videos/store/FileStore;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 1426
    .local p3, "states":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<*>;"
    iput-object p1, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    .line 1427
    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->syncTaskManager:Lcom/google/android/videos/store/SyncTaskManager;
    invoke-static {p1}, Lcom/google/android/videos/store/AssetStoreSync;->access$200(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/SyncTaskManager;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;-><init>(Lcom/google/android/videos/store/SyncTaskManager;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V

    .line 1428
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->itemId:Ljava/lang/String;

    .line 1429
    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->bitmapUri:Landroid/net/Uri;

    .line 1430
    invoke-static {p6}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/FileStore;

    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->fileStore:Lcom/google/android/videos/store/FileStore;

    .line 1431
    invoke-static {p7}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->metadataTable:Ljava/lang/String;

    .line 1432
    invoke-static {p8}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->metadataBitmapSyncedColumn:Ljava/lang/String;

    .line 1433
    invoke-static {p9}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->metadataItemIdColumn:Ljava/lang/String;

    .line 1434
    invoke-static {p10}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->bitmapTable:Ljava/lang/String;

    .line 1435
    invoke-static {p11}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->bitmapItemIdColumn:Ljava/lang/String;

    .line 1436
    iput p12, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->databaseEvent:I

    .line 1437
    return-void
.end method


# virtual methods
.method protected doSync()V
    .locals 12

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1442
    iget-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;
    invoke-static {v0}, Lcom/google/android/videos/store/AssetStoreSync;->access$1700(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/utils/NetworkStatus;->isChargeableNetwork()Z

    move-result v0

    if-nez v0, :cond_2

    move v11, v4

    .line 1443
    .local v11, "shouldSync":Z
    :goto_0
    iget-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v0}, Lcom/google/android/videos/store/AssetStoreSync;->access$900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->bitmapTable:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->bitmapItemIdColumn:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/videos/store/AssetStoreSync$StoredImageQuery;->projection(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->bitmapItemIdColumn:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, " = ?"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->itemId:Ljava/lang/String;

    aput-object v7, v4, v6

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1447
    .local v8, "cursor":Landroid/database/Cursor;
    const/4 v10, 0x0

    .line 1448
    .local v10, "lastModified":Ljava/lang/String;
    const/4 v9, 0x0

    .line 1450
    .local v9, "eTag":Ljava/lang/String;
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_3

    .line 1452
    const/4 v11, 0x1

    .line 1460
    :cond_0
    :goto_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1462
    if-eqz v11, :cond_1

    .line 1463
    iget-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->conditionalHttpEntityRequester:Lcom/google/android/videos/async/Requester;
    invoke-static {v0}, Lcom/google/android/videos/store/AssetStoreSync;->access$1800(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/async/Requester;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->bitmapUri:Landroid/net/Uri;

    invoke-static {v1}, Lcom/google/android/videos/async/HttpUriRequestFactory;->createGet(Landroid/net/Uri;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v1

    invoke-static {v1, v9, v10}, Lcom/google/android/videos/async/ConditionalHttpRequest;->create(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/async/ConditionalHttpRequest;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 1466
    :cond_1
    return-void

    .end local v8    # "cursor":Landroid/database/Cursor;
    .end local v9    # "eTag":Ljava/lang/String;
    .end local v10    # "lastModified":Ljava/lang/String;
    .end local v11    # "shouldSync":Z
    :cond_2
    move v11, v6

    .line 1442
    goto :goto_0

    .line 1453
    .restart local v8    # "cursor":Landroid/database/Cursor;
    .restart local v9    # "eTag":Ljava/lang/String;
    .restart local v10    # "lastModified":Ljava/lang/String;
    .restart local v11    # "shouldSync":Z
    :cond_3
    if-eqz v11, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->bitmapUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1456
    const/4 v0, 0x2

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 1457
    const/4 v0, 0x3

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v10

    goto :goto_1

    .line 1460
    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public onError(Lcom/google/android/videos/async/ConditionalHttpRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p2, "e"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/ConditionalHttpRequest",
            "<",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1543
    .local p1, "request":Lcom/google/android/videos/async/ConditionalHttpRequest;, "Lcom/google/android/videos/async/ConditionalHttpRequest<Lorg/apache/http/client/methods/HttpUriRequest;>;"
    const/16 v0, 0xa

    invoke-virtual {p0, v0, p2}, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->onSyncError(ILjava/lang/Exception;)V

    .line 1544
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 1410
    check-cast p1, Lcom/google/android/videos/async/ConditionalHttpRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->onError(Lcom/google/android/videos/async/ConditionalHttpRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/async/ConditionalHttpRequest;Lcom/google/android/videos/async/ConditionalHttpResponse;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/ConditionalHttpRequest",
            "<",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;",
            "Lcom/google/android/videos/async/ConditionalHttpResponse",
            "<",
            "Lorg/apache/http/HttpEntity;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1471
    .local p1, "request":Lcom/google/android/videos/async/ConditionalHttpRequest;, "Lcom/google/android/videos/async/ConditionalHttpRequest<Lorg/apache/http/client/methods/HttpUriRequest;>;"
    .local p2, "conditionalEntity":Lcom/google/android/videos/async/ConditionalHttpResponse;, "Lcom/google/android/videos/async/ConditionalHttpResponse<Lorg/apache/http/HttpEntity;>;"
    move-object/from16 v0, p2

    iget-boolean v9, v0, Lcom/google/android/videos/async/ConditionalHttpResponse;->isNotModified:Z

    if-eqz v9, :cond_3

    .line 1472
    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/google/android/videos/async/ConditionalHttpRequest;->eTag:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/google/android/videos/async/ConditionalHttpRequest;->lastModified:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1474
    const/16 v9, 0xa

    new-instance v10, Lorg/apache/http/client/HttpResponseException;

    const/16 v11, 0x130

    const-string v12, "Unexpected HTTP 304 response"

    invoke-direct {v10, v11, v12}, Lorg/apache/http/client/HttpResponseException;-><init>(ILjava/lang/String;)V

    invoke-virtual {p0, v9, v10}, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->onSyncError(ILjava/lang/Exception;)V

    .line 1539
    :goto_0
    return-void

    .line 1479
    :cond_0
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 1480
    .local v8, "values":Landroid/content/ContentValues;
    iget-object v9, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->metadataBitmapSyncedColumn:Ljava/lang/String;

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1481
    iget-object v9, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v9}, Lcom/google/android/videos/store/AssetStoreSync;->access$900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    .line 1482
    .local v7, "transaction":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v6, 0x0

    .line 1483
    .local v6, "success":Z
    const/4 v5, 0x0

    .line 1485
    .local v5, "rowsAffected":I
    :try_start_0
    iget-object v9, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->metadataTable:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->metadataItemIdColumn:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " = ?"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/String;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->itemId:Ljava/lang/String;

    aput-object v13, v11, v12

    invoke-virtual {v7, v9, v8, v10, v11}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    .line 1487
    const/4 v6, 0x1

    .line 1489
    iget-object v9, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v9}, Lcom/google/android/videos/store/AssetStoreSync;->access$900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v10

    if-nez v5, :cond_1

    const/4 v9, 0x0

    :goto_1
    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->itemId:Ljava/lang/String;

    aput-object v13, v11, v12

    invoke-virtual {v10, v7, v6, v9, v11}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget v9, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->databaseEvent:I

    goto :goto_1

    :catchall_0
    move-exception v9

    move-object v10, v9

    iget-object v9, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v9}, Lcom/google/android/videos/store/AssetStoreSync;->access$900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v11

    if-nez v5, :cond_2

    const/4 v9, 0x0

    :goto_2
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    iget-object v14, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->itemId:Ljava/lang/String;

    aput-object v14, v12, v13

    invoke-virtual {v11, v7, v6, v9, v12}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v10

    :cond_2
    iget v9, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->databaseEvent:I

    goto :goto_2

    .line 1496
    .end local v5    # "rowsAffected":I
    .end local v6    # "success":Z
    .end local v7    # "transaction":Landroid/database/sqlite/SQLiteDatabase;
    .end local v8    # "values":Landroid/content/ContentValues;
    :cond_3
    :try_start_1
    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/google/android/videos/async/ConditionalHttpResponse;->targetResponse:Ljava/lang/Object;

    check-cast v9, Lorg/apache/http/HttpEntity;

    iget-object v10, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;
    invoke-static {v10}, Lcom/google/android/videos/store/AssetStoreSync;->access$1900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/utils/ByteArrayPool;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/videos/utils/EntityUtils;->toByteArrayObj(Lorg/apache/http/HttpEntity;Lcom/google/android/videos/utils/ByteArrayPool;)Lcom/google/android/videos/utils/ByteArray;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v4

    .line 1503
    .local v4, "response":Lcom/google/android/videos/utils/ByteArray;
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1504
    .local v2, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v9, 0x1

    iput-boolean v9, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1505
    iget-object v9, v4, Lcom/google/android/videos/utils/ByteArray;->data:[B

    const/4 v10, 0x0

    invoke-virtual {v4}, Lcom/google/android/videos/utils/ByteArray;->limit()I

    move-result v11

    invoke-static {v9, v10, v11, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 1506
    iget v9, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-gez v9, :cond_4

    .line 1507
    new-instance v9, Ljava/lang/IllegalStateException;

    const-string v10, "Cannot decode bitmap"

    invoke-direct {v9, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {p0, v0, v9}, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->onError(Lcom/google/android/videos/async/ConditionalHttpRequest;Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 1497
    .end local v2    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v4    # "response":Lcom/google/android/videos/utils/ByteArray;
    :catch_0
    move-exception v1

    .line 1498
    .local v1, "e":Ljava/io/IOException;
    const/16 v9, 0xa

    invoke-virtual {p0, v9, v1}, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->onSyncError(ILjava/lang/Exception;)V

    goto/16 :goto_0

    .line 1511
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v2    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v4    # "response":Lcom/google/android/videos/utils/ByteArray;
    :cond_4
    iget-object v9, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v9}, Lcom/google/android/videos/store/AssetStoreSync;->access$900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    .line 1512
    .restart local v7    # "transaction":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v6, 0x0

    .line 1513
    .restart local v6    # "success":Z
    const/4 v5, 0x0

    .line 1515
    .restart local v5    # "rowsAffected":I
    :try_start_2
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 1516
    .restart local v8    # "values":Landroid/content/ContentValues;
    iget-object v9, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->metadataBitmapSyncedColumn:Ljava/lang/String;

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1517
    iget-object v9, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->metadataTable:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->metadataItemIdColumn:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " = ?"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/String;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->itemId:Ljava/lang/String;

    aput-object v13, v11, v12

    invoke-virtual {v7, v9, v8, v10, v11}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    .line 1519
    if-lez v5, :cond_6

    const/4 v3, 0x1

    .line 1520
    .local v3, "posterStillNeeded":Z
    :goto_3
    if-eqz v3, :cond_5

    .line 1521
    new-instance v8, Landroid/content/ContentValues;

    .end local v8    # "values":Landroid/content/ContentValues;
    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 1522
    .restart local v8    # "values":Landroid/content/ContentValues;
    iget-object v9, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->bitmapItemIdColumn:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->itemId:Ljava/lang/String;

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1523
    const-string v9, "image_uri"

    iget-object v10, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->bitmapUri:Landroid/net/Uri;

    invoke-virtual {v10}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1524
    const-string v9, "image_etag"

    move-object/from16 v0, p2

    iget-object v10, v0, Lcom/google/android/videos/async/ConditionalHttpResponse;->eTag:Ljava/lang/String;

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1525
    const-string v9, "image_last_modified"

    move-object/from16 v0, p2

    iget-object v10, v0, Lcom/google/android/videos/async/ConditionalHttpResponse;->lastModified:Ljava/lang/String;

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1526
    iget-object v9, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->bitmapTable:Ljava/lang/String;

    const/4 v10, 0x0

    const/4 v11, 0x5

    invoke-virtual {v7, v9, v10, v8, v11}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 1528
    iget-object v9, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->fileStore:Lcom/google/android/videos/store/FileStore;

    iget-object v10, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->itemId:Ljava/lang/String;

    invoke-virtual {v9, v10, v4}, Lcom/google/android/videos/store/FileStore;->putBytes(Ljava/lang/String;Lcom/google/android/videos/utils/ByteArray;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1530
    :cond_5
    const/4 v6, 0x1

    .line 1534
    iget-object v9, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v9}, Lcom/google/android/videos/store/AssetStoreSync;->access$900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v10

    if-nez v5, :cond_7

    const/4 v9, 0x0

    :goto_4
    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->itemId:Ljava/lang/String;

    aput-object v13, v11, v12

    invoke-virtual {v10, v7, v6, v9, v11}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 1536
    iget-object v9, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;
    invoke-static {v9}, Lcom/google/android/videos/store/AssetStoreSync;->access$1900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/utils/ByteArrayPool;

    move-result-object v9

    invoke-virtual {v9, v4}, Lcom/google/android/videos/utils/ByteArrayPool;->returnBuf(Lcom/google/android/videos/utils/ByteArray;)V

    goto/16 :goto_0

    .line 1519
    .end local v3    # "posterStillNeeded":Z
    :cond_6
    const/4 v3, 0x0

    goto :goto_3

    .line 1534
    .restart local v3    # "posterStillNeeded":Z
    :cond_7
    iget v9, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->databaseEvent:I

    goto :goto_4

    .line 1531
    .end local v3    # "posterStillNeeded":Z
    .end local v8    # "values":Landroid/content/ContentValues;
    :catch_1
    move-exception v1

    .line 1532
    .restart local v1    # "e":Ljava/io/IOException;
    const/16 v9, 0xa

    :try_start_3
    invoke-virtual {p0, v9, v1}, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->onSyncError(ILjava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1534
    iget-object v9, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v9}, Lcom/google/android/videos/store/AssetStoreSync;->access$900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v10

    if-nez v5, :cond_8

    const/4 v9, 0x0

    :goto_5
    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->itemId:Ljava/lang/String;

    aput-object v13, v11, v12

    invoke-virtual {v10, v7, v6, v9, v11}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 1536
    iget-object v9, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;
    invoke-static {v9}, Lcom/google/android/videos/store/AssetStoreSync;->access$1900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/utils/ByteArrayPool;

    move-result-object v9

    invoke-virtual {v9, v4}, Lcom/google/android/videos/utils/ByteArrayPool;->returnBuf(Lcom/google/android/videos/utils/ByteArray;)V

    goto/16 :goto_0

    .line 1534
    :cond_8
    iget v9, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->databaseEvent:I

    goto :goto_5

    .end local v1    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v9

    move-object v10, v9

    iget-object v9, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v9}, Lcom/google/android/videos/store/AssetStoreSync;->access$900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v11

    if-nez v5, :cond_9

    const/4 v9, 0x0

    :goto_6
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    iget-object v14, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->itemId:Ljava/lang/String;

    aput-object v14, v12, v13

    invoke-virtual {v11, v7, v6, v9, v12}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 1536
    iget-object v9, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;
    invoke-static {v9}, Lcom/google/android/videos/store/AssetStoreSync;->access$1900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/utils/ByteArrayPool;

    move-result-object v9

    invoke-virtual {v9, v4}, Lcom/google/android/videos/utils/ByteArrayPool;->returnBuf(Lcom/google/android/videos/utils/ByteArray;)V

    throw v10

    .line 1534
    :cond_9
    iget v9, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->databaseEvent:I

    goto :goto_6
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 1410
    check-cast p1, Lcom/google/android/videos/async/ConditionalHttpRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/videos/async/ConditionalHttpResponse;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->onResponse(Lcom/google/android/videos/async/ConditionalHttpRequest;Lcom/google/android/videos/async/ConditionalHttpResponse;)V

    return-void
.end method
