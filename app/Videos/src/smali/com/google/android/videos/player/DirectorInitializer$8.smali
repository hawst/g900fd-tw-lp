.class Lcom/google/android/videos/player/DirectorInitializer$8;
.super Ljava/lang/Object;
.source "DirectorInitializer.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/player/DirectorInitializer;->initStoredPurchaseCallback()Lcom/google/android/videos/async/Callback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/player/DirectorInitializer;


# direct methods
.method constructor <init>(Lcom/google/android/videos/player/DirectorInitializer;)V
    .locals 0

    .prologue
    .line 827
    iput-object p1, p0, Lcom/google/android/videos/player/DirectorInitializer$8;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Ljava/lang/Exception;)V
    .locals 3
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 835
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer$8;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;
    invoke-static {v0}, Lcom/google/android/videos/player/DirectorInitializer;->access$200(Lcom/google/android/videos/player/DirectorInitializer;)Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    move-result-object v0

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->recordTaskEnd(IZ)V

    .line 836
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer$8;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # invokes: Lcom/google/android/videos/player/DirectorInitializer;->onNoOfflineStream()V
    invoke-static {v0}, Lcom/google/android/videos/player/DirectorInitializer;->access$3500(Lcom/google/android/videos/player/DirectorInitializer;)V

    .line 837
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 827
    check-cast p1, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/player/DirectorInitializer$8;->onError(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 830
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer$8;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;
    invoke-static {v0}, Lcom/google/android/videos/player/DirectorInitializer;->access$200(Lcom/google/android/videos/player/DirectorInitializer;)Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->recordTaskEnd(I)V

    .line 831
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer$8;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # invokes: Lcom/google/android/videos/player/DirectorInitializer;->onStoredPurchaseCursor(Landroid/database/Cursor;)V
    invoke-static {v0, p2}, Lcom/google/android/videos/player/DirectorInitializer;->access$3400(Lcom/google/android/videos/player/DirectorInitializer;Landroid/database/Cursor;)V

    .line 832
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 827
    check-cast p1, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/player/DirectorInitializer$8;->onResponse(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V

    return-void
.end method
