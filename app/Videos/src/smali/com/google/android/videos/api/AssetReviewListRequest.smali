.class public Lcom/google/android/videos/api/AssetReviewListRequest;
.super Lcom/google/android/videos/async/Request;
.source "AssetReviewListRequest.java"


# instance fields
.field public final criticReviews:Z

.field public final id:Ljava/lang/String;

.field public final maxResults:I

.field public final page:I

.field public final userCountry:Ljava/lang/String;


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 35
    if-ne p0, p1, :cond_1

    .line 40
    :cond_0
    :goto_0
    return v1

    .line 36
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 38
    check-cast v0, Lcom/google/android/videos/api/AssetReviewListRequest;

    .line 40
    .local v0, "that":Lcom/google/android/videos/api/AssetReviewListRequest;
    iget-object v3, p0, Lcom/google/android/videos/api/AssetReviewListRequest;->account:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/api/AssetReviewListRequest;->account:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/videos/api/AssetReviewListRequest;->id:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/api/AssetReviewListRequest;->id:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/videos/api/AssetReviewListRequest;->userCountry:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/api/AssetReviewListRequest;->userCountry:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-boolean v3, p0, Lcom/google/android/videos/api/AssetReviewListRequest;->criticReviews:Z

    iget-boolean v4, v0, Lcom/google/android/videos/api/AssetReviewListRequest;->criticReviews:Z

    if-ne v3, v4, :cond_4

    iget v3, p0, Lcom/google/android/videos/api/AssetReviewListRequest;->page:I

    iget v4, v0, Lcom/google/android/videos/api/AssetReviewListRequest;->page:I

    if-ne v3, v4, :cond_4

    iget v3, p0, Lcom/google/android/videos/api/AssetReviewListRequest;->maxResults:I

    iget v4, v0, Lcom/google/android/videos/api/AssetReviewListRequest;->maxResults:I

    if-eq v3, v4, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 50
    iget-object v2, p0, Lcom/google/android/videos/api/AssetReviewListRequest;->account:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/videos/api/AssetReviewListRequest;->account:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 51
    .local v0, "result":I
    :goto_0
    mul-int/lit8 v3, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/api/AssetReviewListRequest;->id:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/videos/api/AssetReviewListRequest;->id:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 52
    mul-int/lit8 v3, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/api/AssetReviewListRequest;->userCountry:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/videos/api/AssetReviewListRequest;->userCountry:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 53
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v3, p0, Lcom/google/android/videos/api/AssetReviewListRequest;->criticReviews:Z

    if-eqz v3, :cond_0

    const/4 v1, 0x1

    :cond_0
    add-int v0, v2, v1

    .line 54
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/videos/api/AssetReviewListRequest;->page:I

    add-int v0, v1, v2

    .line 55
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/videos/api/AssetReviewListRequest;->maxResults:I

    add-int v0, v1, v2

    .line 56
    return v0

    .end local v0    # "result":I
    :cond_1
    move v0, v1

    .line 50
    goto :goto_0

    .restart local v0    # "result":I
    :cond_2
    move v2, v1

    .line 51
    goto :goto_1

    :cond_3
    move v2, v1

    .line 52
    goto :goto_2
.end method
