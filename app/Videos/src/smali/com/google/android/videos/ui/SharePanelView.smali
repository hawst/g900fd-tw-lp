.class public Lcom/google/android/videos/ui/SharePanelView;
.super Landroid/widget/LinearLayout;
.source "SharePanelView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/SharePanelView$ShareListener;
    }
.end annotation


# instance fields
.field private plusOneButton:Lcom/google/android/gms/plus/PlusOneButtonWithPopup;

.field private shareButton:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/ui/SharePanelView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    invoke-static {p0}, Lcom/google/android/videos/utils/ViewUtil;->removeOutlineProvider(Landroid/view/View;)V

    .line 28
    return-void
.end method

.method public static getMainLayout()I
    .locals 1

    .prologue
    .line 36
    const v0, 0x7f04006f

    return v0
.end method

.method public static inflate(Landroid/view/ViewGroup;)Lcom/google/android/videos/ui/SharePanelView;
    .locals 3
    .param p0, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 31
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04006f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/SharePanelView;

    return-object v0
.end method

.method private setSafeShareListener(Lcom/google/android/videos/ui/SharePanelView$ShareListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/videos/ui/SharePanelView$ShareListener;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/videos/ui/SharePanelView;->shareButton:Landroid/view/View;

    new-instance v1, Lcom/google/android/videos/ui/SharePanelView$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/videos/ui/SharePanelView$1;-><init>(Lcom/google/android/videos/ui/SharePanelView;Lcom/google/android/videos/ui/SharePanelView$ShareListener;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 41
    const v0, 0x7f0f017a

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/SharePanelView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/videos/ui/SharePanelView;->shareButton:Landroid/view/View;

    .line 42
    const v0, 0x7f0f017b

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/SharePanelView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/PlusOneButtonWithPopup;

    iput-object v0, p0, Lcom/google/android/videos/ui/SharePanelView;->plusOneButton:Lcom/google/android/gms/plus/PlusOneButtonWithPopup;

    .line 43
    return-void
.end method

.method public setShareListener(Lcom/google/android/videos/ui/SharePanelView$ShareListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/videos/ui/SharePanelView$ShareListener;

    .prologue
    .line 50
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/SharePanelView$ShareListener;

    invoke-direct {p0, v0}, Lcom/google/android/videos/ui/SharePanelView;->setSafeShareListener(Lcom/google/android/videos/ui/SharePanelView$ShareListener;)V

    .line 51
    return-void
.end method

.method public setUrl(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "account"    # Ljava/lang/String;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/videos/ui/SharePanelView;->plusOneButton:Lcom/google/android/gms/plus/PlusOneButtonWithPopup;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/plus/PlusOneButtonWithPopup;->initialize(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    return-void
.end method
