.class public Lcom/google/android/videos/remote/MediaRouteProviderV7;
.super Ljava/lang/Object;
.source "MediaRouteProviderV7.java"

# interfaces
.implements Lcom/google/android/videos/remote/MediaRouteProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/remote/MediaRouteProviderV7$1;,
        Lcom/google/android/videos/remote/MediaRouteProviderV7$MediaRouteCallback;
    }
.end annotation


# instance fields
.field private forceHidden:Z

.field private mediaRouteMenuItem:Landroid/view/MenuItem;

.field private final mediaRouter:Landroid/support/v7/media/MediaRouter;

.field private final routeCallback:Landroid/support/v7/media/MediaRouter$Callback;

.field private final supportedRouteTypes:Landroid/support/v7/media/MediaRouteSelector;


# direct methods
.method public constructor <init>(Landroid/support/v7/app/ActionBarActivity;Landroid/support/v7/media/MediaRouteSelector;)V
    .locals 2
    .param p1, "activity"    # Landroid/support/v7/app/ActionBarActivity;
    .param p2, "supportedRouteTypes"    # Landroid/support/v7/media/MediaRouteSelector;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {p1}, Landroid/support/v7/media/MediaRouter;->getInstance(Landroid/content/Context;)Landroid/support/v7/media/MediaRouter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/remote/MediaRouteProviderV7;->mediaRouter:Landroid/support/v7/media/MediaRouter;

    .line 33
    iput-object p2, p0, Lcom/google/android/videos/remote/MediaRouteProviderV7;->supportedRouteTypes:Landroid/support/v7/media/MediaRouteSelector;

    .line 34
    new-instance v0, Lcom/google/android/videos/remote/MediaRouteProviderV7$MediaRouteCallback;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/videos/remote/MediaRouteProviderV7$MediaRouteCallback;-><init>(Lcom/google/android/videos/remote/MediaRouteProviderV7$1;)V

    iput-object v0, p0, Lcom/google/android/videos/remote/MediaRouteProviderV7;->routeCallback:Landroid/support/v7/media/MediaRouter$Callback;

    .line 35
    return-void
.end method


# virtual methods
.method public final getMediaRouter()Landroid/support/v7/media/MediaRouter;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteProviderV7;->mediaRouter:Landroid/support/v7/media/MediaRouter;

    return-object v0
.end method

.method public final getSupportedRouteTypes()Landroid/support/v7/media/MediaRouteSelector;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteProviderV7;->supportedRouteTypes:Landroid/support/v7/media/MediaRouteSelector;

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 68
    const v1, 0x7f140001

    invoke-virtual {p2, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 69
    const v1, 0x7f0f0222

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/remote/MediaRouteProviderV7;->mediaRouteMenuItem:Landroid/view/MenuItem;

    .line 70
    iget-object v1, p0, Lcom/google/android/videos/remote/MediaRouteProviderV7;->mediaRouteMenuItem:Landroid/view/MenuItem;

    invoke-static {v1}, Landroid/support/v4/view/MenuItemCompat;->getActionProvider(Landroid/view/MenuItem;)Landroid/support/v4/view/ActionProvider;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/MediaRouteActionProvider;

    .line 72
    .local v0, "mediaRouteActionProvider":Landroid/support/v7/app/MediaRouteActionProvider;
    invoke-virtual {p0}, Lcom/google/android/videos/remote/MediaRouteProviderV7;->getSupportedRouteTypes()Landroid/support/v7/media/MediaRouteSelector;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/MediaRouteActionProvider;->setRouteSelector(Landroid/support/v7/media/MediaRouteSelector;)V

    .line 73
    invoke-static {}, Lcom/google/android/videos/remote/VideoMediaRouteDialogFactory;->getDefault()Lcom/google/android/videos/remote/VideoMediaRouteDialogFactory;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/MediaRouteActionProvider;->setDialogFactory(Landroid/support/v7/app/MediaRouteDialogFactory;)V

    .line 74
    return-void
.end method

.method public onStart()V
    .locals 4

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteProviderV7;->mediaRouter:Landroid/support/v7/media/MediaRouter;

    iget-object v1, p0, Lcom/google/android/videos/remote/MediaRouteProviderV7;->supportedRouteTypes:Landroid/support/v7/media/MediaRouteSelector;

    iget-object v2, p0, Lcom/google/android/videos/remote/MediaRouteProviderV7;->routeCallback:Landroid/support/v7/media/MediaRouter$Callback;

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v7/media/MediaRouter;->addCallback(Landroid/support/v7/media/MediaRouteSelector;Landroid/support/v7/media/MediaRouter$Callback;I)V

    .line 51
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/videos/remote/MediaRouteProviderV7;->mediaRouter:Landroid/support/v7/media/MediaRouter;

    iget-object v1, p0, Lcom/google/android/videos/remote/MediaRouteProviderV7;->routeCallback:Landroid/support/v7/media/MediaRouter$Callback;

    invoke-virtual {v0, v1}, Landroid/support/v7/media/MediaRouter;->removeCallback(Landroid/support/v7/media/MediaRouter$Callback;)V

    .line 56
    return-void
.end method

.method public setForceHidden(Z)V
    .locals 2
    .param p1, "forceHidden"    # Z

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/google/android/videos/remote/MediaRouteProviderV7;->forceHidden:Z

    if-eq v0, p1, :cond_0

    .line 61
    iput-boolean p1, p0, Lcom/google/android/videos/remote/MediaRouteProviderV7;->forceHidden:Z

    .line 62
    iget-object v1, p0, Lcom/google/android/videos/remote/MediaRouteProviderV7;->mediaRouteMenuItem:Landroid/view/MenuItem;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lcom/google/android/videos/utils/Util;->setMenuItemEnabled(Landroid/view/MenuItem;Z)V

    .line 64
    :cond_0
    return-void

    .line 62
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showPicker()V
    .locals 2

    .prologue
    .line 78
    iget-object v1, p0, Lcom/google/android/videos/remote/MediaRouteProviderV7;->mediaRouteMenuItem:Landroid/view/MenuItem;

    invoke-static {v1}, Landroid/support/v4/view/MenuItemCompat;->getActionProvider(Landroid/view/MenuItem;)Landroid/support/v4/view/ActionProvider;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/MediaRouteActionProvider;

    .line 80
    .local v0, "mediaRouteActionProvider":Landroid/support/v7/app/MediaRouteActionProvider;
    invoke-virtual {v0}, Landroid/support/v7/app/MediaRouteActionProvider;->onPerformDefaultAction()Z

    .line 81
    return-void
.end method
