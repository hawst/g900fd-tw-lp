.class public Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;
.super Ljava/lang/Object;
.source "BaseDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/activity/BaseDetailsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PurchaseInfo"
.end annotation


# instance fields
.field public final eventSource:I

.field public final filteringType:I

.field public final seasonId:Ljava/lang/String;

.field public final showId:Ljava/lang/String;

.field public final videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 0
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "seasonId"    # Ljava/lang/String;
    .param p3, "showId"    # Ljava/lang/String;
    .param p4, "filteringType"    # I
    .param p5, "eventSource"    # I

    .prologue
    .line 551
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 552
    iput-object p1, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;->videoId:Ljava/lang/String;

    .line 553
    iput-object p2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;->seasonId:Ljava/lang/String;

    .line 554
    iput-object p3, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;->showId:Ljava/lang/String;

    .line 555
    iput p4, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;->filteringType:I

    .line 556
    iput p5, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;->eventSource:I

    .line 557
    return-void
.end method
