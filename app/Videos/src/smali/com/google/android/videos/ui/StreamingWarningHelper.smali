.class public Lcom/google/android/videos/ui/StreamingWarningHelper;
.super Landroid/content/BroadcastReceiver;
.source "StreamingWarningHelper.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/StreamingWarningHelper$Listener;
    }
.end annotation


# instance fields
.field private final activity:Landroid/app/Activity;

.field private final dialog:Landroid/app/Dialog;

.field private final listener:Lcom/google/android/videos/ui/StreamingWarningHelper$Listener;

.field private final networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

.field private playing:Z

.field private final preferences:Landroid/content/SharedPreferences;

.field private registered:Z

.field private warningAccepted:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/content/SharedPreferences;Lcom/google/android/videos/ui/StreamingWarningHelper$Listener;Lcom/google/android/videos/utils/NetworkStatus;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "preferences"    # Landroid/content/SharedPreferences;
    .param p3, "listener"    # Lcom/google/android/videos/ui/StreamingWarningHelper$Listener;
    .param p4, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 50
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    iput-object v1, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->activity:Landroid/app/Activity;

    .line 51
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/SharedPreferences;

    iput-object v1, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->preferences:Landroid/content/SharedPreferences;

    .line 52
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/ui/StreamingWarningHelper$Listener;

    iput-object v1, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->listener:Lcom/google/android/videos/ui/StreamingWarningHelper$Listener;

    .line 53
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/utils/NetworkStatus;

    iput-object v1, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 55
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0b018a

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 58
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0b013b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 59
    const v1, 0x7f0b013c

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 60
    const v1, 0x1040013

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 61
    const v1, 0x7f0b0189

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 62
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->dialog:Landroid/app/Dialog;

    .line 63
    return-void
.end method

.method private isDialogNeeded()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 88
    iget-object v1, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v1}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v1}, Lcom/google/android/videos/utils/NetworkStatus;->isMobileNetwork()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->warningAccepted:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->preferences:Landroid/content/SharedPreferences;

    const-string v2, "warning_streaming_bandwidth"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showDialog()V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 96
    :cond_0
    return-void
.end method


# virtual methods
.method public isShowingDialog()Z
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    return v0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 126
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->playing:Z

    .line 127
    iget-object v0, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->listener:Lcom/google/android/videos/ui/StreamingWarningHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/ui/StreamingWarningHelper$Listener;->onStreamingDeclined()V

    .line 128
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v4, 0x1

    .line 104
    packed-switch p2, :pswitch_data_0

    .line 122
    :goto_0
    return-void

    .line 106
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "warning_streaming_bandwidth"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 107
    iput-boolean v4, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->playing:Z

    .line 108
    iput-boolean v4, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->warningAccepted:Z

    .line 109
    iget-object v1, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->listener:Lcom/google/android/videos/ui/StreamingWarningHelper$Listener;

    invoke-interface {v1, v4}, Lcom/google/android/videos/ui/StreamingWarningHelper$Listener;->onStreamingAccepted(Z)V

    goto :goto_0

    .line 112
    :pswitch_1
    iput-boolean v4, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->playing:Z

    .line 113
    iput-boolean v4, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->warningAccepted:Z

    .line 114
    iget-object v1, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->listener:Lcom/google/android/videos/ui/StreamingWarningHelper$Listener;

    invoke-interface {v1, v4}, Lcom/google/android/videos/ui/StreamingWarningHelper$Listener;->onStreamingAccepted(Z)V

    goto :goto_0

    .line 117
    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.WIFI_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 118
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 119
    iget-object v1, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 104
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public onPlaybackStarted()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 77
    invoke-direct {p0}, Lcom/google/android/videos/ui/StreamingWarningHelper;->isDialogNeeded()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->playing:Z

    .line 78
    iget-boolean v0, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->playing:Z

    if-eqz v0, :cond_1

    .line 79
    iget-object v0, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->listener:Lcom/google/android/videos/ui/StreamingWarningHelper$Listener;

    invoke-interface {v0, v2}, Lcom/google/android/videos/ui/StreamingWarningHelper$Listener;->onStreamingAccepted(Z)V

    .line 83
    :goto_1
    return v2

    :cond_0
    move v0, v2

    .line 77
    goto :goto_0

    .line 82
    :cond_1
    invoke-direct {p0}, Lcom/google/android/videos/ui/StreamingWarningHelper;->showDialog()V

    move v2, v1

    .line 83
    goto :goto_1
.end method

.method public onPlaybackStopped()V
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->playing:Z

    .line 132
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    .line 147
    iget-boolean v0, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->registered:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/videos/ui/StreamingWarningHelper;->isInitialStickyBroadcast()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 166
    :cond_0
    :goto_0
    return-void

    .line 153
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->warningAccepted:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0}, Lcom/google/android/videos/utils/NetworkStatus;->isMobileNetwork()Z

    move-result v0

    if-nez v0, :cond_2

    .line 154
    iput-boolean v1, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->warningAccepted:Z

    .line 157
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->playing:Z

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/videos/ui/StreamingWarningHelper;->isDialogNeeded()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 158
    iput-boolean v1, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->playing:Z

    .line 159
    iget-object v0, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->listener:Lcom/google/android/videos/ui/StreamingWarningHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/ui/StreamingWarningHelper$Listener;->onWifiDisconnected()V

    .line 160
    invoke-direct {p0}, Lcom/google/android/videos/ui/StreamingWarningHelper;->showDialog()V

    goto :goto_0

    .line 161
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/videos/ui/StreamingWarningHelper;->isDialogNeeded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 163
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->playing:Z

    .line 164
    iget-object v0, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->listener:Lcom/google/android/videos/ui/StreamingWarningHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/ui/StreamingWarningHelper$Listener;->onWifiConnected()V

    goto :goto_0
.end method

.method public register()V
    .locals 3

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->registered:Z

    if-nez v0, :cond_0

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->registered:Z

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->warningAccepted:Z

    .line 69
    iget-object v0, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->activity:Landroid/app/Activity;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0, v1}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 71
    :cond_0
    return-void
.end method

.method public unregister()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 135
    iget-boolean v0, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->registered:Z

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v0, p0}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 137
    iput-boolean v1, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->registered:Z

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 140
    iget-object v0, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 142
    :cond_1
    iput-boolean v1, p0, Lcom/google/android/videos/ui/StreamingWarningHelper;->playing:Z

    .line 143
    return-void
.end method
