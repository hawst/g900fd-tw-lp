.class public Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;
.super Ljava/lang/Object;
.source "KnowledgeFeedbackReport.java"

# interfaces
.implements Lcom/google/android/videos/tagging/FeedbackClient$FeedbackReport;


# instance fields
.field public final account:Ljava/lang/String;

.field public final incorrectlyTaggedKnowledgeEntities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;",
            ">;"
        }
    .end annotation
.end field

.field public final knowledgePositionMillis:I

.field private final productId:I

.field public final tagStream:Lcom/google/android/videos/tagging/TagStream;

.field private final typeId:I


# direct methods
.method public constructor <init>(IILjava/lang/String;Lcom/google/android/videos/tagging/TagStream;ILjava/util/List;)V
    .locals 1
    .param p1, "productId"    # I
    .param p2, "typeId"    # I
    .param p3, "account"    # Ljava/lang/String;
    .param p4, "tagStream"    # Lcom/google/android/videos/tagging/TagStream;
    .param p5, "knowledgePositionMillis"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/tagging/TagStream;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    .local p6, "incorrectlyTaggedKnowledgeEntities":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput p1, p0, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->productId:I

    .line 30
    iput p2, p0, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->typeId:I

    .line 31
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->account:Ljava/lang/String;

    .line 32
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/tagging/TagStream;

    iput-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->tagStream:Lcom/google/android/videos/tagging/TagStream;

    .line 33
    iput p5, p0, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->knowledgePositionMillis:I

    .line 34
    invoke-static {p6}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lcom/google/android/videos/utils/CollectionUtil;->immutableCopyOf(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->incorrectlyTaggedKnowledgeEntities:Ljava/util/List;

    .line 36
    return-void
.end method

.method private appendError(Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;Ljava/lang/StringBuilder;)V
    .locals 1
    .param p1, "error"    # Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;
    .param p2, "errorsBuilder"    # Ljava/lang/StringBuilder;

    .prologue
    .line 88
    iget-object v0, p1, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;->knowledgeEntity:Lcom/google/android/videos/tagging/KnowledgeEntity;

    iget-object v0, v0, Lcom/google/android/videos/tagging/KnowledgeEntity;->name:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    const-string v0, " (sid="

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    iget v0, p1, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;->splitId:I

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 91
    const-string v0, ")"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    return-void
.end method

.method private newProductSpecificData(Ljava/lang/String;Ljava/lang/String;)Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 81
    new-instance v0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    invoke-direct {v0}, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;-><init>()V

    .line 82
    .local v0, "data":Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;
    iput-object p1, v0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->key:Ljava/lang/String;

    .line 83
    iput-object p2, v0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->value:Ljava/lang/String;

    .line 84
    return-object v0
.end method


# virtual methods
.method public buildMessage()Lcom/google/protobuf/nano/MessageNano;
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 45
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 46
    .local v2, "errorsBuilder":Ljava/lang/StringBuilder;
    iget-object v6, p0, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->incorrectlyTaggedKnowledgeEntities:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 47
    iget-object v6, p0, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->incorrectlyTaggedKnowledgeEntities:Ljava/util/List;

    invoke-interface {v6, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;

    invoke-direct {p0, v6, v2}, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->appendError(Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;Ljava/lang/StringBuilder;)V

    .line 48
    const/4 v4, 0x1

    .local v4, "i":I
    :goto_0
    iget-object v6, p0, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->incorrectlyTaggedKnowledgeEntities:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v4, v6, :cond_0

    .line 49
    const-string v6, "; "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    iget-object v6, p0, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->incorrectlyTaggedKnowledgeEntities:Ljava/util/List;

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;

    invoke-direct {p0, v6, v2}, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->appendError(Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;Ljava/lang/StringBuilder;)V

    .line 48
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 53
    .end local v4    # "i":I
    :cond_0
    iget v6, p0, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->knowledgePositionMillis:I

    invoke-static {v6, v10}, Lcom/google/android/videos/utils/TimeUtil;->getDurationString(IZ)Ljava/lang/String;

    move-result-object v5

    .line 54
    .local v5, "timeString":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "TF "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->tagStream:Lcom/google/android/videos/tagging/TagStream;

    iget-object v7, v7, Lcom/google/android/videos/tagging/TagStream;->videoTitle:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " @"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " f="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->tagStream:Lcom/google/android/videos/tagging/TagStream;

    iget-object v7, v7, Lcom/google/android/videos/tagging/TagStream;->videoId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->tagStream:Lcom/google/android/videos/tagging/TagStream;

    iget-object v7, v7, Lcom/google/android/videos/tagging/TagStream;->contentLanguage:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " itag="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->tagStream:Lcom/google/android/videos/tagging/TagStream;

    iget v7, v7, Lcom/google/android/videos/tagging/TagStream;->videoItag:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ts="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->tagStream:Lcom/google/android/videos/tagging/TagStream;

    iget-object v7, v7, Lcom/google/android/videos/tagging/TagStream;->videoTimestamp:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 58
    .local v1, "description":Ljava/lang/String;
    new-instance v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;

    invoke-direct {v0}, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;-><init>()V

    .line 59
    .local v0, "commonData":Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;
    iput-object v1, v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->description:Ljava/lang/String;

    .line 60
    iput-object v1, v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->descriptionTranslated:Ljava/lang/String;

    .line 61
    const/16 v6, 0x9

    new-array v6, v6, [Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    const-string v7, "videoId"

    iget-object v8, p0, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->tagStream:Lcom/google/android/videos/tagging/TagStream;

    iget-object v8, v8, Lcom/google/android/videos/tagging/TagStream;->videoId:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->newProductSpecificData(Ljava/lang/String;Ljava/lang/String;)Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    move-result-object v7

    aput-object v7, v6, v9

    const-string v7, "videoTitle"

    iget-object v8, p0, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->tagStream:Lcom/google/android/videos/tagging/TagStream;

    iget-object v8, v8, Lcom/google/android/videos/tagging/TagStream;->videoTitle:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->newProductSpecificData(Ljava/lang/String;Ljava/lang/String;)Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    move-result-object v7

    aput-object v7, v6, v10

    const/4 v7, 0x2

    const-string v8, "contentLanguage"

    iget-object v9, p0, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->tagStream:Lcom/google/android/videos/tagging/TagStream;

    iget-object v9, v9, Lcom/google/android/videos/tagging/TagStream;->contentLanguage:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v8, v9}, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->newProductSpecificData(Ljava/lang/String;Ljava/lang/String;)Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    const-string v8, "lastModified"

    iget-object v9, p0, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->tagStream:Lcom/google/android/videos/tagging/TagStream;

    iget-object v9, v9, Lcom/google/android/videos/tagging/TagStream;->lastModified:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v8, v9}, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->newProductSpecificData(Ljava/lang/String;Ljava/lang/String;)Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x4

    const-string v8, "videoItag"

    iget-object v9, p0, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->tagStream:Lcom/google/android/videos/tagging/TagStream;

    iget v9, v9, Lcom/google/android/videos/tagging/TagStream;->videoItag:I

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v8, v9}, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->newProductSpecificData(Ljava/lang/String;Ljava/lang/String;)Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x5

    const-string v8, "videoTimestamp"

    iget-object v9, p0, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->tagStream:Lcom/google/android/videos/tagging/TagStream;

    iget-object v9, v9, Lcom/google/android/videos/tagging/TagStream;->videoTimestamp:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v8, v9}, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->newProductSpecificData(Ljava/lang/String;Ljava/lang/String;)Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x6

    const-string v8, "knowledgePositionMillis"

    iget v9, p0, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->knowledgePositionMillis:I

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v8, v9}, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->newProductSpecificData(Ljava/lang/String;Ljava/lang/String;)Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x7

    const-string v8, "knowledgePositionString"

    invoke-direct {p0, v8, v5}, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->newProductSpecificData(Ljava/lang/String;Ljava/lang/String;)Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0x8

    const-string v8, "errors"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v8, v9}, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->newProductSpecificData(Ljava/lang/String;Ljava/lang/String;)Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    move-result-object v8

    aput-object v8, v6, v7

    iput-object v6, v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificData:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    .line 72
    new-instance v3, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;

    invoke-direct {v3}, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;-><init>()V

    .line 73
    .local v3, "extensionSubmit":Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;
    iget v6, p0, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->productId:I

    iput v6, v3, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->productId:I

    .line 74
    iget v6, p0, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->typeId:I

    iput v6, v3, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->typeId:I

    .line 75
    new-instance v6, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$WebData;

    invoke-direct {v6}, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$WebData;-><init>()V

    iput-object v6, v3, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->webData:Lcom/google/protos/userfeedback/UserFeedbackWebProtos$WebData;

    .line 76
    iput-object v0, v3, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->commonData:Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;

    .line 77
    return-object v3
.end method

.method public getAccount()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;->account:Ljava/lang/String;

    return-object v0
.end method
