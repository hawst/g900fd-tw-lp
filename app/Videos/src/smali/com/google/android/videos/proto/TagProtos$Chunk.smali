.class public final Lcom/google/android/videos/proto/TagProtos$Chunk;
.super Lcom/google/protobuf/nano/MessageNano;
.source "TagProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/proto/TagProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Chunk"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/videos/proto/TagProtos$Chunk;


# instance fields
.field public byteOffset:I

.field public localId:[I

.field public start:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 241
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 242
    invoke-virtual {p0}, Lcom/google/android/videos/proto/TagProtos$Chunk;->clear()Lcom/google/android/videos/proto/TagProtos$Chunk;

    .line 243
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/videos/proto/TagProtos$Chunk;
    .locals 2

    .prologue
    .line 221
    sget-object v0, Lcom/google/android/videos/proto/TagProtos$Chunk;->_emptyArray:[Lcom/google/android/videos/proto/TagProtos$Chunk;

    if-nez v0, :cond_1

    .line 222
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 224
    :try_start_0
    sget-object v0, Lcom/google/android/videos/proto/TagProtos$Chunk;->_emptyArray:[Lcom/google/android/videos/proto/TagProtos$Chunk;

    if-nez v0, :cond_0

    .line 225
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/videos/proto/TagProtos$Chunk;

    sput-object v0, Lcom/google/android/videos/proto/TagProtos$Chunk;->_emptyArray:[Lcom/google/android/videos/proto/TagProtos$Chunk;

    .line 227
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 229
    :cond_1
    sget-object v0, Lcom/google/android/videos/proto/TagProtos$Chunk;->_emptyArray:[Lcom/google/android/videos/proto/TagProtos$Chunk;

    return-object v0

    .line 227
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/videos/proto/TagProtos$Chunk;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 246
    iput v0, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->start:I

    .line 247
    iput v0, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->byteOffset:I

    .line 248
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->localId:[I

    .line 249
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->cachedSize:I

    .line 250
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 312
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v3

    .line 313
    .local v3, "size":I
    iget v4, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->start:I

    if-eqz v4, :cond_0

    .line 314
    const/4 v4, 0x1

    iget v5, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->start:I

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    .line 317
    :cond_0
    iget v4, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->byteOffset:I

    if-eqz v4, :cond_1

    .line 318
    const/4 v4, 0x2

    iget v5, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->byteOffset:I

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    .line 321
    :cond_1
    iget-object v4, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->localId:[I

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->localId:[I

    array-length v4, v4

    if-lez v4, :cond_3

    .line 322
    const/4 v0, 0x0

    .line 323
    .local v0, "dataSize":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->localId:[I

    array-length v4, v4

    if-ge v2, v4, :cond_2

    .line 324
    iget-object v4, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->localId:[I

    aget v1, v4, v2

    .line 325
    .local v1, "element":I
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v4

    add-int/2addr v0, v4

    .line 323
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 328
    .end local v1    # "element":I
    :cond_2
    add-int/2addr v3, v0

    .line 329
    add-int/lit8 v3, v3, 0x1

    .line 330
    invoke-static {v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeRawVarint32Size(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 333
    .end local v0    # "dataSize":I
    .end local v2    # "i":I
    :cond_3
    return v3
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 255
    if-ne p1, p0, :cond_1

    .line 272
    :cond_0
    :goto_0
    return v1

    .line 258
    :cond_1
    instance-of v3, p1, Lcom/google/android/videos/proto/TagProtos$Chunk;

    if-nez v3, :cond_2

    move v1, v2

    .line 259
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 261
    check-cast v0, Lcom/google/android/videos/proto/TagProtos$Chunk;

    .line 262
    .local v0, "other":Lcom/google/android/videos/proto/TagProtos$Chunk;
    iget v3, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->start:I

    iget v4, v0, Lcom/google/android/videos/proto/TagProtos$Chunk;->start:I

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 263
    goto :goto_0

    .line 265
    :cond_3
    iget v3, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->byteOffset:I

    iget v4, v0, Lcom/google/android/videos/proto/TagProtos$Chunk;->byteOffset:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 266
    goto :goto_0

    .line 268
    :cond_4
    iget-object v3, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->localId:[I

    iget-object v4, v0, Lcom/google/android/videos/proto/TagProtos$Chunk;->localId:[I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([I[I)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 270
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 277
    const/16 v0, 0x11

    .line 278
    .local v0, "result":I
    iget v1, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->start:I

    add-int/lit16 v0, v1, 0x20f

    .line 279
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->byteOffset:I

    add-int v0, v1, v2

    .line 280
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->localId:[I

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([I)I

    move-result v2

    add-int v0, v1, v2

    .line 282
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/TagProtos$Chunk;
    .locals 9
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 341
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v6

    .line 342
    .local v6, "tag":I
    sparse-switch v6, :sswitch_data_0

    .line 346
    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v8

    if-nez v8, :cond_0

    .line 347
    :sswitch_0
    return-object p0

    .line 352
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v8

    iput v8, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->start:I

    goto :goto_0

    .line 356
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v8

    iput v8, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->byteOffset:I

    goto :goto_0

    .line 360
    :sswitch_3
    const/16 v8, 0x18

    invoke-static {p1, v8}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 362
    .local v0, "arrayLength":I
    iget-object v8, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->localId:[I

    if-nez v8, :cond_2

    move v1, v7

    .line 363
    .local v1, "i":I
    :goto_1
    add-int v8, v1, v0

    new-array v4, v8, [I

    .line 364
    .local v4, "newArray":[I
    if-eqz v1, :cond_1

    .line 365
    iget-object v8, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->localId:[I

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 367
    :cond_1
    :goto_2
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_3

    .line 368
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v8

    aput v8, v4, v1

    .line 369
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 367
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 362
    .end local v1    # "i":I
    .end local v4    # "newArray":[I
    :cond_2
    iget-object v8, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->localId:[I

    array-length v1, v8

    goto :goto_1

    .line 372
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[I
    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v8

    aput v8, v4, v1

    .line 373
    iput-object v4, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->localId:[I

    goto :goto_0

    .line 377
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[I
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v2

    .line 378
    .local v2, "length":I
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v3

    .line 380
    .local v3, "limit":I
    const/4 v0, 0x0

    .line 381
    .restart local v0    # "arrayLength":I
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getPosition()I

    move-result v5

    .line 382
    .local v5, "startPos":I
    :goto_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v8

    if-lez v8, :cond_4

    .line 383
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    .line 384
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 386
    :cond_4
    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->rewindToPosition(I)V

    .line 387
    iget-object v8, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->localId:[I

    if-nez v8, :cond_6

    move v1, v7

    .line 388
    .restart local v1    # "i":I
    :goto_4
    add-int v8, v1, v0

    new-array v4, v8, [I

    .line 389
    .restart local v4    # "newArray":[I
    if-eqz v1, :cond_5

    .line 390
    iget-object v8, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->localId:[I

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 392
    :cond_5
    :goto_5
    array-length v8, v4

    if-ge v1, v8, :cond_7

    .line 393
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v8

    aput v8, v4, v1

    .line 392
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 387
    .end local v1    # "i":I
    .end local v4    # "newArray":[I
    :cond_6
    iget-object v8, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->localId:[I

    array-length v1, v8

    goto :goto_4

    .line 395
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[I
    :cond_7
    iput-object v4, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->localId:[I

    .line 396
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 342
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 215
    invoke-virtual {p0, p1}, Lcom/google/android/videos/proto/TagProtos$Chunk;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/TagProtos$Chunk;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 288
    iget v3, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->start:I

    if-eqz v3, :cond_0

    .line 289
    const/4 v3, 0x1

    iget v4, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->start:I

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 291
    :cond_0
    iget v3, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->byteOffset:I

    if-eqz v3, :cond_1

    .line 292
    const/4 v3, 0x2

    iget v4, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->byteOffset:I

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 294
    :cond_1
    iget-object v3, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->localId:[I

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->localId:[I

    array-length v3, v3

    if-lez v3, :cond_3

    .line 295
    const/4 v0, 0x0

    .line 296
    .local v0, "dataSize":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->localId:[I

    array-length v3, v3

    if-ge v2, v3, :cond_2

    .line 297
    iget-object v3, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->localId:[I

    aget v1, v3, v2

    .line 298
    .local v1, "element":I
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v3

    add-int/2addr v0, v3

    .line 296
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 301
    .end local v1    # "element":I
    :cond_2
    const/16 v3, 0x1a

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeRawVarint32(I)V

    .line 302
    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeRawVarint32(I)V

    .line 303
    const/4 v2, 0x0

    :goto_1
    iget-object v3, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->localId:[I

    array-length v3, v3

    if-ge v2, v3, :cond_3

    .line 304
    iget-object v3, p0, Lcom/google/android/videos/proto/TagProtos$Chunk;->localId:[I

    aget v3, v3, v2

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32NoTag(I)V

    .line 303
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 307
    .end local v0    # "dataSize":I
    .end local v2    # "i":I
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 308
    return-void
.end method
