.class public final Lcom/google/android/videos/ui/PinHelper;
.super Ljava/lang/Object;
.source "PinHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/PinHelper$SettingAdapter;,
        Lcom/google/android/videos/ui/PinHelper$DownloadErrorDialogFragment;,
        Lcom/google/android/videos/ui/PinHelper$UnpinConfirmDialogFragment;,
        Lcom/google/android/videos/ui/PinHelper$SingleDownloadSettingDialogFragment;,
        Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;,
        Lcom/google/android/videos/ui/PinHelper$PinListener;
    }
.end annotation


# static fields
.field private static final PURCHASE_QUERY_COLUMNS:[Ljava/lang/String;


# instance fields
.field private final applicationContext:Landroid/content/Context;

.field private final itagInfoStore:Lcom/google/android/videos/store/ItagInfoStore;

.field private final networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

.field private final preferences:Landroid/content/SharedPreferences;

.field private final purchaseStore:Lcom/google/android/videos/store/PurchaseStore;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 74
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "format_type"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "audio_track_languages"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/videos/ui/PinHelper;->PURCHASE_QUERY_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/SharedPreferences;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/videos/store/ItagInfoStore;Lcom/google/android/videos/store/PurchaseStore;Landroid/content/Context;)V
    .locals 1
    .param p1, "preferences"    # Landroid/content/SharedPreferences;
    .param p2, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;
    .param p3, "itagInfoStore"    # Lcom/google/android/videos/store/ItagInfoStore;
    .param p4, "purchaseStore"    # Lcom/google/android/videos/store/PurchaseStore;
    .param p5, "applicationContext"    # Landroid/content/Context;

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/videos/ui/PinHelper;->preferences:Landroid/content/SharedPreferences;

    .line 88
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/utils/NetworkStatus;

    iput-object v0, p0, Lcom/google/android/videos/ui/PinHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 89
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/ItagInfoStore;

    iput-object v0, p0, Lcom/google/android/videos/ui/PinHelper;->itagInfoStore:Lcom/google/android/videos/store/ItagInfoStore;

    .line 90
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/PurchaseStore;

    iput-object v0, p0, Lcom/google/android/videos/ui/PinHelper;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    .line 91
    iput-object p5, p0, Lcom/google/android/videos/ui/PinHelper;->applicationContext:Landroid/content/Context;

    .line 92
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/ui/PinHelper;Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/ui/PinHelper;
    .param p1, "x1"    # Landroid/support/v4/app/FragmentActivity;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Z
    .param p5, "x5"    # Z

    .prologue
    .line 62
    invoke-direct/range {p0 .. p5}, Lcom/google/android/videos/ui/PinHelper;->pinVideo(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-void
.end method

.method static synthetic access$100(Landroid/app/Activity;)V
    .locals 0
    .param p0, "x0"    # Landroid/app/Activity;

    .prologue
    .line 62
    invoke-static {p0}, Lcom/google/android/videos/ui/PinHelper;->onPinFailed(Landroid/app/Activity;)V

    return-void
.end method

.method static synthetic access$200(Landroid/app/Activity;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Landroid/app/Activity;
    .param p1, "x1"    # Landroid/content/SharedPreferences;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 62
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/videos/ui/PinHelper;->requestPin(Landroid/app/Activity;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Landroid/app/Activity;)V
    .locals 0
    .param p0, "x0"    # Landroid/app/Activity;

    .prologue
    .line 62
    invoke-static {p0}, Lcom/google/android/videos/ui/PinHelper;->onPinCanceled(Landroid/app/Activity;)V

    return-void
.end method

.method static synthetic access$400(Landroid/content/Context;)Lcom/google/android/videos/logging/EventLogger;
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;

    .prologue
    .line 62
    invoke-static {p0}, Lcom/google/android/videos/ui/PinHelper;->getEventLogger(Landroid/content/Context;)Lcom/google/android/videos/logging/EventLogger;

    move-result-object v0

    return-object v0
.end method

.method private static getEventLogger(Landroid/content/Context;)Lcom/google/android/videos/logging/EventLogger;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 198
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v0

    return-object v0
.end method

.method private getPurchaseCallback(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/async/Callback;
    .locals 1
    .param p1, "activity"    # Landroid/support/v4/app/FragmentActivity;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "videoId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/FragmentActivity;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 265
    new-instance v0, Lcom/google/android/videos/ui/PinHelper$1;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/videos/ui/PinHelper$1;-><init>(Lcom/google/android/videos/ui/PinHelper;Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private static onPinCanceled(Landroid/app/Activity;)V
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 258
    instance-of v0, p0, Lcom/google/android/videos/ui/PinHelper$PinListener;

    if-eqz v0, :cond_0

    .line 259
    check-cast p0, Lcom/google/android/videos/ui/PinHelper$PinListener;

    .end local p0    # "activity":Landroid/app/Activity;
    invoke-interface {p0}, Lcom/google/android/videos/ui/PinHelper$PinListener;->onPinCanceled()V

    .line 261
    :cond_0
    return-void
.end method

.method private static onPinFailed(Landroid/app/Activity;)V
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 252
    instance-of v0, p0, Lcom/google/android/videos/ui/PinHelper$PinListener;

    if-eqz v0, :cond_0

    .line 253
    check-cast p0, Lcom/google/android/videos/ui/PinHelper$PinListener;

    .end local p0    # "activity":Landroid/app/Activity;
    invoke-interface {p0}, Lcom/google/android/videos/ui/PinHelper$PinListener;->onPinFailed()V

    .line 255
    :cond_0
    return-void
.end method

.method private pinVideo(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 12
    .param p1, "activity"    # Landroid/support/v4/app/FragmentActivity;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "videoId"    # Ljava/lang/String;
    .param p4, "hdPurchased"    # Z
    .param p5, "multipleAudioLanguages"    # Z

    .prologue
    .line 116
    iget-object v1, p0, Lcom/google/android/videos/ui/PinHelper;->preferences:Landroid/content/SharedPreferences;

    const-string v2, "download_setting_shown_flags"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    .line 117
    .local v8, "alreadyShownFlags":I
    const/4 v11, 0x0

    .line 119
    .local v11, "shouldShowFlags":I
    iget-object v1, p0, Lcom/google/android/videos/ui/PinHelper;->itagInfoStore:Lcom/google/android/videos/store/ItagInfoStore;

    invoke-virtual {v1}, Lcom/google/android/videos/store/ItagInfoStore;->getAllowedDownloadQualities()Ljava/util/Set;

    move-result-object v7

    .line 120
    .local v7, "allowedDownloadQualities":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    if-eqz p4, :cond_3

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v7, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v10, 0x1

    .line 121
    .local v10, "hdAvailable":Z
    :goto_0
    const/4 v9, 0x0

    .line 122
    .local v9, "flagCount":I
    if-eqz v10, :cond_0

    and-int/lit8 v1, v8, 0x2

    if-nez v1, :cond_0

    .line 123
    add-int/lit8 v9, v9, 0x1

    .line 124
    or-int/lit8 v11, v11, 0x2

    .line 126
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/ui/PinHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v1}, Lcom/google/android/videos/utils/NetworkStatus;->isMobileNetworkCapable()Z

    move-result v1

    if-eqz v1, :cond_1

    and-int/lit8 v1, v8, 0x1

    if-nez v1, :cond_1

    .line 127
    add-int/lit8 v9, v9, 0x1

    .line 128
    or-int/lit8 v11, v11, 0x1

    .line 130
    :cond_1
    if-eqz p5, :cond_2

    and-int/lit8 v1, v8, 0x4

    if-nez v1, :cond_2

    .line 131
    add-int/lit8 v9, v9, 0x1

    .line 132
    or-int/lit8 v11, v11, 0x4

    .line 135
    :cond_2
    if-nez v9, :cond_4

    .line 136
    iget-object v1, p0, Lcom/google/android/videos/ui/PinHelper;->preferences:Landroid/content/SharedPreferences;

    invoke-static {p1, v1, p2, p3}, Lcom/google/android/videos/ui/PinHelper;->requestPin(Landroid/app/Activity;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    :goto_1
    return-void

    .line 120
    .end local v9    # "flagCount":I
    .end local v10    # "hdAvailable":Z
    :cond_3
    const/4 v10, 0x0

    goto :goto_0

    .line 140
    .restart local v9    # "flagCount":I
    .restart local v10    # "hdAvailable":Z
    :cond_4
    const/4 v1, 0x1

    if-le v9, v1, :cond_5

    .line 141
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    move/from16 v0, p5

    invoke-static {v1, p2, p3, v10, v0}, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->showInstance(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;ZZ)V

    goto :goto_1

    .line 147
    :cond_5
    packed-switch v11, :pswitch_data_0

    :pswitch_0
    goto :goto_1

    .line 153
    :pswitch_1
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/ui/PinHelper;->preferences:Landroid/content/SharedPreferences;

    const/4 v6, 0x1

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v1 .. v6}, Lcom/google/android/videos/ui/PinHelper$SingleDownloadSettingDialogFragment;->showNetworkPreferenceInstance(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1

    .line 149
    :pswitch_2
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/ui/PinHelper;->preferences:Landroid/content/SharedPreferences;

    const/4 v6, 0x1

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v1 .. v6}, Lcom/google/android/videos/ui/PinHelper$SingleDownloadSettingDialogFragment;->showQualityPreferenceInstance(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1

    .line 157
    :pswitch_3
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/ui/PinHelper;->preferences:Landroid/content/SharedPreferences;

    const/4 v6, 0x1

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v1 .. v6}, Lcom/google/android/videos/ui/PinHelper$SingleDownloadSettingDialogFragment;->showAudioPreferenceInstance(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1

    .line 147
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private static requestPin(Landroid/app/Activity;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "preferences"    # Landroid/content/SharedPreferences;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "videoId"    # Ljava/lang/String;

    .prologue
    .line 204
    invoke-static {p0, p1}, Lcom/google/android/videos/utils/SettingsUtil;->getDownloadQualityItag(Landroid/content/Context;Landroid/content/SharedPreferences;)I

    move-result v2

    .line 205
    .local v2, "quality":I
    invoke-static {p1}, Lcom/google/android/videos/utils/SettingsUtil;->getPreferredStorageIndex(Landroid/content/SharedPreferences;)I

    move-result v3

    .line 207
    .local v3, "storage":I
    invoke-static {p0, p2, p3, v2, v3}, Lcom/google/android/videos/pinning/PinService;->requestPin(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;II)V

    .line 208
    instance-of v4, p0, Lcom/google/android/videos/ui/PinHelper$PinListener;

    if-eqz v4, :cond_0

    move-object v4, p0

    .line 209
    check-cast v4, Lcom/google/android/videos/ui/PinHelper$PinListener;

    invoke-interface {v4}, Lcom/google/android/videos/ui/PinHelper$PinListener;->onPinRequested()V

    .line 212
    :cond_0
    invoke-static {p0}, Lcom/google/android/videos/pinning/NetworkPendChecker;->pendDueToWifi(Landroid/content/Context;)Z

    move-result v1

    .line 213
    .local v1, "pendDueToWifi":Z
    const/4 v4, 0x0

    invoke-static {p0, v4}, Lcom/google/android/videos/pinning/NetworkPendChecker;->pendDueToConnection(Landroid/content/Context;Lcom/google/android/videos/pinning/InternetConnectionChecker;)Z

    move-result v0

    .line 214
    .local v0, "pendDueToNetwork":Z
    if-nez v1, :cond_1

    if-eqz v0, :cond_2

    .line 215
    :cond_1
    invoke-static {v0, v1, p0, p2, p3}, Lcom/google/android/videos/ui/PinHelper;->showWillPendDialog(ZZLandroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    :cond_2
    return-void
.end method

.method public static showErrorDialog(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Long;Ljava/lang/Integer;Z)V
    .locals 8
    .param p0, "activity"    # Landroid/support/v4/app/FragmentActivity;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "pinningFailedReason"    # I
    .param p4, "downloadSize"    # Ljava/lang/Long;
    .param p5, "drmErrorCode"    # Ljava/lang/Integer;
    .param p6, "isRental"    # Z

    .prologue
    .line 193
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    move v7, p6

    invoke-static/range {v0 .. v7}, Lcom/google/android/videos/ui/PinHelper$DownloadErrorDialogFragment;->showInstance(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Long;Ljava/lang/Integer;Z)V

    .line 195
    return-void
.end method

.method private static showWillPendDialog(ZZLandroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 13
    .param p0, "pendDueToNetwork"    # Z
    .param p1, "pendDueToWifi"    # Z
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "account"    # Ljava/lang/String;
    .param p4, "videoId"    # Ljava/lang/String;

    .prologue
    .line 221
    invoke-static {p2}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v11

    .line 222
    .local v11, "globals":Lcom/google/android/videos/VideosGlobals;
    invoke-virtual {v11}, Lcom/google/android/videos/VideosGlobals;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 223
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "purchased_assets, videos ON asset_type IN (6,20) AND asset_id = video_id"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "title"

    aput-object v4, v2, v3

    const-string v3, "account = ? AND asset_id = ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p3, v4, v5

    const/4 v5, 0x1

    aput-object p4, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 229
    .local v10, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    .line 247
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 249
    :goto_0
    return-void

    .line 232
    :cond_0
    const/4 v1, 0x0

    :try_start_1
    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 234
    .local v12, "title":Ljava/lang/String;
    new-instance v9, Landroid/app/AlertDialog$Builder;

    invoke-direct {v9, p2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 237
    .local v9, "builder":Landroid/app/AlertDialog$Builder;
    if-eqz p1, :cond_1

    .line 238
    const v1, 0x7f0b0183

    invoke-virtual {v9, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 239
    const v1, 0x7f0b0182

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v12, v2, v3

    invoke-virtual {p2, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 244
    :goto_1
    const v1, 0x104000a

    const/4 v2, 0x0

    invoke-virtual {v9, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 245
    invoke-virtual {v9}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 247
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 241
    :cond_1
    const v1, 0x7f0b0185

    :try_start_2
    invoke-virtual {v9, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 242
    const v1, 0x7f0b0184

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v12, v2, v3

    invoke-virtual {p2, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 247
    .end local v9    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v12    # "title":Ljava/lang/String;
    :catchall_0
    move-exception v1

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v1
.end method


# virtual methods
.method public onUnpinClicked(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Integer;)V
    .locals 8
    .param p1, "activity"    # Landroid/support/v4/app/FragmentActivity;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "videoId"    # Ljava/lang/String;
    .param p4, "title"    # Ljava/lang/String;
    .param p5, "pinningStatus"    # I
    .param p6, "pinningStatusReason"    # Ljava/lang/Integer;

    .prologue
    .line 176
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v6, p5

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/videos/ui/PinHelper;->onUnpinClicked(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Integer;)V

    .line 177
    return-void
.end method

.method public onUnpinClicked(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Integer;)V
    .locals 8
    .param p1, "activity"    # Landroid/support/v4/app/FragmentActivity;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "videoId"    # Ljava/lang/String;
    .param p4, "title"    # Ljava/lang/String;
    .param p5, "showTitle"    # Ljava/lang/String;
    .param p6, "pinningStatus"    # I
    .param p7, "pinningStatusReason"    # Ljava/lang/Integer;

    .prologue
    .line 181
    if-eqz p7, :cond_0

    invoke-virtual {p7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 183
    .local v7, "reason":I
    :goto_0
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    move-object v0, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-static/range {v0 .. v7}, Lcom/google/android/videos/ui/PinHelper$UnpinConfirmDialogFragment;->showInstance(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 185
    return-void

    .line 181
    .end local v7    # "reason":I
    :cond_0
    const/4 v7, 0x0

    goto :goto_0
.end method

.method public pinVideo(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "activity"    # Landroid/support/v4/app/FragmentActivity;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "videoId"    # Ljava/lang/String;

    .prologue
    .line 103
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 104
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 107
    sget-object v2, Lcom/google/android/videos/ui/PinHelper;->PURCHASE_QUERY_COLUMNS:[Ljava/lang/String;

    invoke-static {p2, v2, p3}, Lcom/google/android/videos/store/PurchaseRequests;->createPurchaseRequestForUser(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v1

    .line 109
    .local v1, "request":Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/ui/PinHelper;->getPurchaseCallback(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/async/Callback;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/google/android/videos/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/ActivityCallback;

    move-result-object v0

    .line 111
    .local v0, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;>;"
    iget-object v2, p0, Lcom/google/android/videos/ui/PinHelper;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/videos/async/Callback;)V

    .line 112
    return-void
.end method

.method public unpinVideo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/videos/ui/PinHelper;->applicationContext:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/google/android/videos/pinning/PinService;->requestUnpin(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    return-void
.end method
