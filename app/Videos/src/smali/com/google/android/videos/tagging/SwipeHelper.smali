.class public Lcom/google/android/videos/tagging/SwipeHelper;
.super Ljava/lang/Object;
.source "SwipeHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/tagging/SwipeHelper$Callback;
    }
.end annotation


# static fields
.field private static final sLinearInterpolator:Landroid/view/animation/LinearInterpolator;


# instance fields
.field private final mCallback:Lcom/google/android/videos/tagging/SwipeHelper$Callback;

.field private mCanCurrViewBeDimissed:Z

.field private mCurrView:Landroid/view/View;

.field private mDensityScale:F

.field private mDragging:Z

.field private mInitialTouchPos:F

.field private mMinAlpha:F

.field private mPagingTouchSlop:F

.field private final mSwipeDirection:I

.field private final mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/videos/tagging/SwipeHelper;->sLinearInterpolator:Landroid/view/animation/LinearInterpolator;

    return-void
.end method

.method public constructor <init>(ILcom/google/android/videos/tagging/SwipeHelper$Callback;FF)V
    .locals 1
    .param p1, "swipeDirection"    # I
    .param p2, "callback"    # Lcom/google/android/videos/tagging/SwipeHelper$Callback;
    .param p3, "densityScale"    # F
    .param p4, "pagingTouchSlop"    # F

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mMinAlpha:F

    .line 62
    iput-object p2, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mCallback:Lcom/google/android/videos/tagging/SwipeHelper$Callback;

    .line 63
    iput p1, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mSwipeDirection:I

    .line 64
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 65
    iput p3, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mDensityScale:F

    .line 66
    iput p4, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mPagingTouchSlop:F

    .line 67
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/tagging/SwipeHelper;)Lcom/google/android/videos/tagging/SwipeHelper$Callback;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/SwipeHelper;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mCallback:Lcom/google/android/videos/tagging/SwipeHelper$Callback;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/tagging/SwipeHelper;Landroid/view/View;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/SwipeHelper;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/google/android/videos/tagging/SwipeHelper;->getAlphaForOffset(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method private createTranslationAnimation(Landroid/view/View;F)Landroid/animation/ObjectAnimator;
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "newPos"    # F

    .prologue
    .line 102
    iget v1, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mSwipeDirection:I

    if-nez v1, :cond_0

    const-string v1, "translationX"

    :goto_0
    const/4 v2, 0x1

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput p2, v2, v3

    invoke-static {p1, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 104
    .local v0, "anim":Landroid/animation/ObjectAnimator;
    return-object v0

    .line 102
    .end local v0    # "anim":Landroid/animation/ObjectAnimator;
    :cond_0
    const-string v1, "translationY"

    goto :goto_0
.end method

.method private getAlphaForOffset(Landroid/view/View;)F
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const v5, 0x3e19999a    # 0.15f

    .line 128
    invoke-direct {p0, p1}, Lcom/google/android/videos/tagging/SwipeHelper;->getSize(Landroid/view/View;)F

    move-result v3

    .line 129
    .local v3, "viewSize":F
    const v4, 0x3f266666    # 0.65f

    mul-float v0, v4, v3

    .line 130
    .local v0, "fadeSize":F
    const/high16 v2, 0x3f800000    # 1.0f

    .line 131
    .local v2, "result":F
    invoke-direct {p0, p1}, Lcom/google/android/videos/tagging/SwipeHelper;->getTranslation(Landroid/view/View;)F

    move-result v1

    .line 132
    .local v1, "pos":F
    mul-float v4, v3, v5

    cmpl-float v4, v1, v4

    if-ltz v4, :cond_1

    .line 133
    mul-float v4, v3, v5

    sub-float v4, v1, v4

    div-float/2addr v4, v0

    sub-float v2, v6, v4

    .line 137
    :cond_0
    :goto_0
    invoke-static {v2, v6}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 138
    const/4 v4, 0x0

    invoke-static {v2, v4}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 139
    iget v4, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mMinAlpha:F

    invoke-static {v4, v2}, Ljava/lang/Math;->max(FF)F

    move-result v4

    return v4

    .line 134
    :cond_1
    const v4, 0x3f59999a    # 0.85f

    mul-float/2addr v4, v3

    cmpg-float v4, v1, v4

    if-gez v4, :cond_0

    .line 135
    mul-float v4, v3, v5

    add-float/2addr v4, v1

    div-float/2addr v4, v0

    add-float v2, v6, v4

    goto :goto_0
.end method

.method private getPerpendicularVelocity(Landroid/view/VelocityTracker;)F
    .locals 1
    .param p1, "vt"    # Landroid/view/VelocityTracker;

    .prologue
    .line 108
    iget v0, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    goto :goto_0
.end method

.method private getPos(Landroid/view/MotionEvent;)F
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 90
    iget v0, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    goto :goto_0
.end method

.method private getSize(Landroid/view/View;)F
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 120
    iget v0, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    goto :goto_0
.end method

.method private getTranslation(Landroid/view/View;)F
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 94
    iget v0, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    move-result v0

    goto :goto_0
.end method

.method private getVelocity(Landroid/view/VelocityTracker;)F
    .locals 1
    .param p1, "vt"    # Landroid/view/VelocityTracker;

    .prologue
    .line 98
    iget v0, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    goto :goto_0
.end method

.method private setTranslation(Landroid/view/View;F)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "translate"    # F

    .prologue
    .line 112
    iget v0, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    .line 113
    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationX(F)V

    .line 117
    :goto_0
    return-void

    .line 115
    :cond_0
    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_0
.end method


# virtual methods
.method public cancelOngoingDrag()V
    .locals 2

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mDragging:Z

    if-eqz v0, :cond_1

    .line 79
    iget-object v0, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mCallback:Lcom/google/android/videos/tagging/SwipeHelper$Callback;

    iget-object v1, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v0, v1}, Lcom/google/android/videos/tagging/SwipeHelper$Callback;->onDragCancelled(Landroid/view/View;)V

    .line 81
    iget-object v0, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/tagging/SwipeHelper;->setTranslation(Landroid/view/View;F)V

    .line 82
    iget-object v0, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mCallback:Lcom/google/android/videos/tagging/SwipeHelper$Callback;

    iget-object v1, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v0, v1}, Lcom/google/android/videos/tagging/SwipeHelper$Callback;->onSnapBackCompleted(Landroid/view/View;)V

    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    .line 85
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mDragging:Z

    .line 87
    :cond_1
    return-void
.end method

.method public dismissChild(Landroid/view/View;F)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;
    .param p2, "velocity"    # F

    .prologue
    const/4 v6, 0x0

    .line 182
    iget-object v4, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mCallback:Lcom/google/android/videos/tagging/SwipeHelper$Callback;

    invoke-interface {v4, p1}, Lcom/google/android/videos/tagging/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v1

    .line 184
    .local v1, "canAnimViewBeDismissed":Z
    cmpg-float v4, p2, v6

    if-ltz v4, :cond_1

    cmpl-float v4, p2, v6

    if-nez v4, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/videos/tagging/SwipeHelper;->getTranslation(Landroid/view/View;)F

    move-result v4

    cmpg-float v4, v4, v6

    if-ltz v4, :cond_1

    :cond_0
    cmpl-float v4, p2, v6

    if-nez v4, :cond_2

    invoke-direct {p0, p1}, Lcom/google/android/videos/tagging/SwipeHelper;->getTranslation(Landroid/view/View;)F

    move-result v4

    cmpl-float v4, v4, v6

    if-nez v4, :cond_2

    iget v4, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mSwipeDirection:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_2

    .line 187
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/videos/tagging/SwipeHelper;->getSize(Landroid/view/View;)F

    move-result v4

    neg-float v3, v4

    .line 191
    .local v3, "newPos":F
    :goto_0
    const/16 v2, 0x96

    .line 192
    .local v2, "duration":I
    cmpl-float v4, p2, v6

    if-eqz v4, :cond_3

    .line 193
    invoke-direct {p0, p1}, Lcom/google/android/videos/tagging/SwipeHelper;->getTranslation(Landroid/view/View;)F

    move-result v4

    sub-float v4, v3, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const/high16 v5, 0x447a0000    # 1000.0f

    mul-float/2addr v4, v5

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v5

    div-float/2addr v4, v5

    float-to-int v4, v4

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 199
    :goto_1
    invoke-direct {p0, p1, v3}, Lcom/google/android/videos/tagging/SwipeHelper;->createTranslationAnimation(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 200
    .local v0, "anim":Landroid/animation/ValueAnimator;
    sget-object v4, Lcom/google/android/videos/tagging/SwipeHelper;->sLinearInterpolator:Landroid/view/animation/LinearInterpolator;

    invoke-virtual {v0, v4}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 201
    int-to-long v4, v2

    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 202
    new-instance v4, Lcom/google/android/videos/tagging/SwipeHelper$1;

    invoke-direct {v4, p0, p1, v1}, Lcom/google/android/videos/tagging/SwipeHelper$1;-><init>(Lcom/google/android/videos/tagging/SwipeHelper;Landroid/view/View;Z)V

    invoke-virtual {v0, v4}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 213
    new-instance v4, Lcom/google/android/videos/tagging/SwipeHelper$2;

    invoke-direct {v4, p0, v1, p1}, Lcom/google/android/videos/tagging/SwipeHelper$2;-><init>(Lcom/google/android/videos/tagging/SwipeHelper;ZLandroid/view/View;)V

    invoke-virtual {v0, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 221
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 222
    return-void

    .line 189
    .end local v0    # "anim":Landroid/animation/ValueAnimator;
    .end local v2    # "duration":I
    .end local v3    # "newPos":F
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/videos/tagging/SwipeHelper;->getSize(Landroid/view/View;)F

    move-result v3

    .restart local v3    # "newPos":F
    goto :goto_0

    .line 196
    .restart local v2    # "duration":I
    :cond_3
    const/16 v2, 0x4b

    goto :goto_1
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x0

    .line 143
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 145
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 174
    :cond_0
    :goto_0
    iget-boolean v3, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mDragging:Z

    return v3

    .line 147
    :pswitch_0
    iput-boolean v3, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mDragging:Z

    .line 148
    iget-object v3, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mCallback:Lcom/google/android/videos/tagging/SwipeHelper$Callback;

    invoke-interface {v3, p1}, Lcom/google/android/videos/tagging/SwipeHelper$Callback;->getChildAtPosition(Landroid/view/MotionEvent;)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    .line 149
    iget-object v3, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v3}, Landroid/view/VelocityTracker;->clear()V

    .line 150
    iget-object v3, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    if-eqz v3, :cond_0

    .line 151
    iget-object v3, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mCallback:Lcom/google/android/videos/tagging/SwipeHelper$Callback;

    iget-object v4, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v3, v4}, Lcom/google/android/videos/tagging/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mCanCurrViewBeDimissed:Z

    .line 152
    iget-object v3, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v3, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 153
    invoke-direct {p0, p1}, Lcom/google/android/videos/tagging/SwipeHelper;->getPos(Landroid/view/MotionEvent;)F

    move-result v3

    iput v3, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mInitialTouchPos:F

    goto :goto_0

    .line 157
    :pswitch_1
    iget-object v3, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    if-eqz v3, :cond_0

    .line 158
    iget-object v3, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v3, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 159
    invoke-direct {p0, p1}, Lcom/google/android/videos/tagging/SwipeHelper;->getPos(Landroid/view/MotionEvent;)F

    move-result v2

    .line 160
    .local v2, "pos":F
    iget v3, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mInitialTouchPos:F

    sub-float v1, v2, v3

    .line 161
    .local v1, "delta":F
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v4, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mPagingTouchSlop:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    .line 162
    iget-object v3, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mCallback:Lcom/google/android/videos/tagging/SwipeHelper$Callback;

    iget-object v4, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v3, v4}, Lcom/google/android/videos/tagging/SwipeHelper$Callback;->onBeginDrag(Landroid/view/View;)V

    .line 163
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mDragging:Z

    .line 164
    invoke-direct {p0, p1}, Lcom/google/android/videos/tagging/SwipeHelper;->getPos(Landroid/view/MotionEvent;)F

    move-result v3

    iget-object v4, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-direct {p0, v4}, Lcom/google/android/videos/tagging/SwipeHelper;->getTranslation(Landroid/view/View;)F

    move-result v4

    sub-float/2addr v3, v4

    iput v3, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mInitialTouchPos:F

    goto :goto_0

    .line 170
    .end local v1    # "delta":F
    .end local v2    # "pos":F
    :pswitch_2
    iput-boolean v3, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mDragging:Z

    .line 171
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    goto :goto_0

    .line 145
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 20
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 250
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/videos/tagging/SwipeHelper;->mDragging:Z

    if-nez v14, :cond_1

    .line 251
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/videos/tagging/SwipeHelper;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v14

    if-nez v14, :cond_1

    .line 252
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    if-eqz v14, :cond_0

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/videos/tagging/SwipeHelper;->mCanCurrViewBeDimissed:Z

    if-eqz v14, :cond_0

    const/4 v14, 0x1

    .line 311
    :goto_0
    return v14

    .line 252
    :cond_0
    const/4 v14, 0x0

    goto :goto_0

    .line 256
    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/tagging/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 257
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 258
    .local v2, "action":I
    packed-switch v2, :pswitch_data_0

    .line 311
    :cond_2
    :goto_1
    const/4 v14, 0x1

    goto :goto_0

    .line 261
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    if-eqz v14, :cond_2

    .line 262
    invoke-direct/range {p0 .. p1}, Lcom/google/android/videos/tagging/SwipeHelper;->getPos(Landroid/view/MotionEvent;)F

    move-result v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/videos/tagging/SwipeHelper;->mInitialTouchPos:F

    sub-float v6, v14, v15

    .line 265
    .local v6, "delta":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/tagging/SwipeHelper;->mCallback:Lcom/google/android/videos/tagging/SwipeHelper$Callback;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v14, v15}, Lcom/google/android/videos/tagging/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v14

    if-nez v14, :cond_3

    .line 266
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/videos/tagging/SwipeHelper;->getSize(Landroid/view/View;)F

    move-result v12

    .line 267
    .local v12, "size":F
    const v14, 0x3e19999a    # 0.15f

    mul-float v9, v14, v12

    .line 268
    .local v9, "maxScrollDistance":F
    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v14

    cmpl-float v14, v14, v12

    if-ltz v14, :cond_5

    .line 269
    const/4 v14, 0x0

    cmpl-float v14, v6, v14

    if-lez v14, :cond_4

    move v6, v9

    .line 274
    .end local v9    # "maxScrollDistance":F
    .end local v12    # "size":F
    :cond_3
    :goto_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v6}, Lcom/google/android/videos/tagging/SwipeHelper;->setTranslation(Landroid/view/View;F)V

    .line 275
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/videos/tagging/SwipeHelper;->mCanCurrViewBeDimissed:Z

    if-eqz v14, :cond_2

    .line 276
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/videos/tagging/SwipeHelper;->getAlphaForOffset(Landroid/view/View;)F

    move-result v3

    .line 277
    .local v3, "alpha":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-virtual {v14, v3}, Landroid/view/View;->setAlpha(F)V

    goto :goto_1

    .line 269
    .end local v3    # "alpha":F
    .restart local v9    # "maxScrollDistance":F
    .restart local v12    # "size":F
    :cond_4
    neg-float v6, v9

    goto :goto_2

    .line 271
    :cond_5
    div-float v14, v6, v12

    float-to-double v14, v14

    const-wide v16, 0x3ff921fb54442d18L    # 1.5707963267948966

    mul-double v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    double-to-float v14, v14

    mul-float v6, v9, v14

    goto :goto_2

    .line 283
    .end local v6    # "delta":F
    .end local v9    # "maxScrollDistance":F
    .end local v12    # "size":F
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    if-eqz v14, :cond_2

    .line 284
    const/high16 v14, 0x44fa0000    # 2000.0f

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/videos/tagging/SwipeHelper;->mDensityScale:F

    mul-float v10, v14, v15

    .line 285
    .local v10, "maxVelocity":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/tagging/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/16 v15, 0x3e8

    invoke-virtual {v14, v15, v10}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 286
    const/high16 v14, 0x42c80000    # 100.0f

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/videos/tagging/SwipeHelper;->mDensityScale:F

    mul-float v8, v14, v15

    .line 287
    .local v8, "escapeVelocity":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/tagging/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/videos/tagging/SwipeHelper;->getVelocity(Landroid/view/VelocityTracker;)F

    move-result v13

    .line 288
    .local v13, "velocity":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/tagging/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/videos/tagging/SwipeHelper;->getPerpendicularVelocity(Landroid/view/VelocityTracker;)F

    move-result v11

    .line 291
    .local v11, "perpendicularVelocity":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/videos/tagging/SwipeHelper;->getTranslation(Landroid/view/View;)F

    move-result v14

    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v14

    float-to-double v14, v14

    const-wide v16, 0x3fe3333333333333L    # 0.6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/google/android/videos/tagging/SwipeHelper;->getSize(Landroid/view/View;)F

    move-result v18

    move/from16 v0, v18

    float-to-double v0, v0

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    cmpl-double v14, v14, v16

    if-lez v14, :cond_7

    const/4 v4, 0x1

    .line 293
    .local v4, "childSwipedFarEnough":Z
    :goto_3
    invoke-static {v13}, Ljava/lang/Math;->abs(F)F

    move-result v14

    cmpl-float v14, v14, v8

    if-lez v14, :cond_a

    invoke-static {v13}, Ljava/lang/Math;->abs(F)F

    move-result v14

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v15

    cmpl-float v14, v14, v15

    if-lez v14, :cond_a

    const/4 v14, 0x0

    cmpl-float v14, v13, v14

    if-lez v14, :cond_8

    const/4 v14, 0x1

    :goto_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/google/android/videos/tagging/SwipeHelper;->getTranslation(Landroid/view/View;)F

    move-result v15

    const/16 v16, 0x0

    cmpl-float v15, v15, v16

    if-lez v15, :cond_9

    const/4 v15, 0x1

    :goto_5
    if-ne v14, v15, :cond_a

    const/4 v5, 0x1

    .line 297
    .local v5, "childSwipedFastEnough":Z
    :goto_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/tagging/SwipeHelper;->mCallback:Lcom/google/android/videos/tagging/SwipeHelper$Callback;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v14, v15}, Lcom/google/android/videos/tagging/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v14

    if-eqz v14, :cond_b

    if-nez v5, :cond_6

    if-eqz v4, :cond_b

    :cond_6
    const/4 v7, 0x1

    .line 300
    .local v7, "dismissChild":Z
    :goto_7
    if-eqz v7, :cond_d

    .line 302
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    if-eqz v5, :cond_c

    .end local v13    # "velocity":F
    :goto_8
    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v13}, Lcom/google/android/videos/tagging/SwipeHelper;->dismissChild(Landroid/view/View;F)V

    goto/16 :goto_1

    .line 291
    .end local v4    # "childSwipedFarEnough":Z
    .end local v5    # "childSwipedFastEnough":Z
    .end local v7    # "dismissChild":Z
    .restart local v13    # "velocity":F
    :cond_7
    const/4 v4, 0x0

    goto :goto_3

    .line 293
    .restart local v4    # "childSwipedFarEnough":Z
    :cond_8
    const/4 v14, 0x0

    goto :goto_4

    :cond_9
    const/4 v15, 0x0

    goto :goto_5

    :cond_a
    const/4 v5, 0x0

    goto :goto_6

    .line 297
    .restart local v5    # "childSwipedFastEnough":Z
    :cond_b
    const/4 v7, 0x0

    goto :goto_7

    .line 302
    .restart local v7    # "dismissChild":Z
    :cond_c
    const/4 v13, 0x0

    goto :goto_8

    .line 305
    :cond_d
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/tagging/SwipeHelper;->mCallback:Lcom/google/android/videos/tagging/SwipeHelper$Callback;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v14, v15}, Lcom/google/android/videos/tagging/SwipeHelper$Callback;->onDragCancelled(Landroid/view/View;)V

    .line 306
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v13}, Lcom/google/android/videos/tagging/SwipeHelper;->snapChild(Landroid/view/View;F)V

    goto/16 :goto_1

    .line 258
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public snapChild(Landroid/view/View;F)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "velocity"    # F

    .prologue
    .line 225
    iget-object v3, p0, Lcom/google/android/videos/tagging/SwipeHelper;->mCallback:Lcom/google/android/videos/tagging/SwipeHelper$Callback;

    invoke-interface {v3, p1}, Lcom/google/android/videos/tagging/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v1

    .line 226
    .local v1, "canAnimViewBeDismissed":Z
    const/4 v3, 0x0

    invoke-direct {p0, p1, v3}, Lcom/google/android/videos/tagging/SwipeHelper;->createTranslationAnimation(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 227
    .local v0, "anim":Landroid/animation/ValueAnimator;
    const/16 v2, 0x96

    .line 228
    .local v2, "duration":I
    int-to-long v4, v2

    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 229
    new-instance v3, Lcom/google/android/videos/tagging/SwipeHelper$3;

    invoke-direct {v3, p0, v1, p1}, Lcom/google/android/videos/tagging/SwipeHelper$3;-><init>(Lcom/google/android/videos/tagging/SwipeHelper;ZLandroid/view/View;)V

    invoke-virtual {v0, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 237
    new-instance v3, Lcom/google/android/videos/tagging/SwipeHelper$4;

    invoke-direct {v3, p0, v1, p1}, Lcom/google/android/videos/tagging/SwipeHelper$4;-><init>(Lcom/google/android/videos/tagging/SwipeHelper;ZLandroid/view/View;)V

    invoke-virtual {v0, v3}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 246
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 247
    return-void
.end method
