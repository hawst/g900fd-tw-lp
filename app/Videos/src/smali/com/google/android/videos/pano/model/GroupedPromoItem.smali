.class public final Lcom/google/android/videos/pano/model/GroupedPromoItem;
.super Ljava/lang/Object;
.source "GroupedPromoItem.java"

# interfaces
.implements Lcom/google/android/repolib/common/Entity;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/pano/model/GroupedPromoItem$1;,
        Lcom/google/android/videos/pano/model/GroupedPromoItem$GroupedPromoItemFactory;
    }
.end annotation


# instance fields
.field private final group:Ljava/lang/String;

.field private final videoItem:Lcom/google/android/videos/pano/model/PromoItem;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/pano/model/PromoItem;Ljava/lang/String;)V
    .locals 1
    .param p1, "promoItem"    # Lcom/google/android/videos/pano/model/PromoItem;
    .param p2, "group"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/pano/model/PromoItem;

    iput-object v0, p0, Lcom/google/android/videos/pano/model/GroupedPromoItem;->videoItem:Lcom/google/android/videos/pano/model/PromoItem;

    .line 21
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/videos/pano/model/GroupedPromoItem;->group:Ljava/lang/String;

    .line 22
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/pano/model/PromoItem;Ljava/lang/String;Lcom/google/android/videos/pano/model/GroupedPromoItem$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/pano/model/PromoItem;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Lcom/google/android/videos/pano/model/GroupedPromoItem$1;

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/pano/model/GroupedPromoItem;-><init>(Lcom/google/android/videos/pano/model/PromoItem;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getEntityId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/videos/pano/model/GroupedPromoItem;->videoItem:Lcom/google/android/videos/pano/model/PromoItem;

    invoke-virtual {v0}, Lcom/google/android/videos/pano/model/PromoItem;->getEntityId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGroup()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/videos/pano/model/GroupedPromoItem;->group:Ljava/lang/String;

    return-object v0
.end method

.method public getPromoItem()Lcom/google/android/videos/pano/model/PromoItem;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/videos/pano/model/GroupedPromoItem;->videoItem:Lcom/google/android/videos/pano/model/PromoItem;

    return-object v0
.end method
