.class public abstract Lcom/google/android/videos/ui/StatusHelper;
.super Ljava/lang/Object;
.source "StatusHelper.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/StatusHelper$1;,
        Lcom/google/android/videos/ui/StatusHelper$ViewStatusHelper;,
        Lcom/google/android/videos/ui/StatusHelper$OnRetryListener;
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private errorIcon:Landroid/view/View;

.field private messageView:Landroid/widget/TextView;

.field private progress:Landroid/view/View;

.field private retryButton:Landroid/view/View;

.field private final retryListener:Lcom/google/android/videos/ui/StatusHelper$OnRetryListener;

.field protected statusView:Landroid/view/View;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/videos/ui/StatusHelper$OnRetryListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "retryListener"    # Lcom/google/android/videos/ui/StatusHelper$OnRetryListener;

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/StatusHelper$OnRetryListener;

    iput-object v0, p0, Lcom/google/android/videos/ui/StatusHelper;->retryListener:Lcom/google/android/videos/ui/StatusHelper$OnRetryListener;

    .line 116
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/videos/ui/StatusHelper;->context:Landroid/content/Context;

    .line 117
    return-void
.end method

.method public static createFromParent(Landroid/content/Context;Landroid/view/View;Lcom/google/android/videos/ui/StatusHelper$OnRetryListener;)Lcom/google/android/videos/ui/StatusHelper;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "view"    # Landroid/view/View;
    .param p2, "retryListener"    # Lcom/google/android/videos/ui/StatusHelper$OnRetryListener;

    .prologue
    .line 49
    new-instance v0, Lcom/google/android/videos/ui/StatusHelper$ViewStatusHelper;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/android/videos/ui/StatusHelper$ViewStatusHelper;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/google/android/videos/ui/StatusHelper$OnRetryListener;Lcom/google/android/videos/ui/StatusHelper$1;)V

    return-object v0
.end method


# virtual methods
.method protected abstract ensureHidden()V
.end method

.method protected abstract ensureVisible()V
.end method

.method public hide()V
    .locals 0

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/google/android/videos/ui/StatusHelper;->ensureHidden()V

    .line 70
    return-void
.end method

.method public init()V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/videos/ui/StatusHelper;->statusView:Landroid/view/View;

    const v1, 0x7f0f0152

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/StatusHelper;->messageView:Landroid/widget/TextView;

    .line 57
    iget-object v0, p0, Lcom/google/android/videos/ui/StatusHelper;->messageView:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 58
    iget-object v0, p0, Lcom/google/android/videos/ui/StatusHelper;->statusView:Landroid/view/View;

    const v1, 0x7f0f020a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/StatusHelper;->progress:Landroid/view/View;

    .line 59
    iget-object v0, p0, Lcom/google/android/videos/ui/StatusHelper;->statusView:Landroid/view/View;

    const v1, 0x7f0f0209

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/StatusHelper;->retryButton:Landroid/view/View;

    .line 60
    iget-object v0, p0, Lcom/google/android/videos/ui/StatusHelper;->statusView:Landroid/view/View;

    const v1, 0x7f0f0208

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/StatusHelper;->errorIcon:Landroid/view/View;

    .line 61
    iget-object v0, p0, Lcom/google/android/videos/ui/StatusHelper;->retryButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    invoke-virtual {p0}, Lcom/google/android/videos/ui/StatusHelper;->ensureHidden()V

    .line 63
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/videos/ui/StatusHelper;->retryListener:Lcom/google/android/videos/ui/StatusHelper$OnRetryListener;

    invoke-interface {v0}, Lcom/google/android/videos/ui/StatusHelper$OnRetryListener;->onRetry()V

    .line 112
    return-void
.end method

.method public setErrorMessage(IZ)V
    .locals 1
    .param p1, "messageResource"    # I
    .param p2, "canRetry"    # Z

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/videos/ui/StatusHelper;->context:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/videos/ui/StatusHelper;->setErrorMessage(Ljava/lang/CharSequence;Z)V

    .line 106
    return-void
.end method

.method public setErrorMessage(Ljava/lang/CharSequence;Z)V
    .locals 4
    .param p1, "message"    # Ljava/lang/CharSequence;
    .param p2, "canRetry"    # Z

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 90
    iget-object v0, p0, Lcom/google/android/videos/ui/StatusHelper;->errorIcon:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 91
    iget-object v3, p0, Lcom/google/android/videos/ui/StatusHelper;->retryButton:Landroid/view/View;

    if-eqz p2, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 92
    iget-object v0, p0, Lcom/google/android/videos/ui/StatusHelper;->messageView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 93
    iget-object v0, p0, Lcom/google/android/videos/ui/StatusHelper;->progress:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 94
    iget-object v0, p0, Lcom/google/android/videos/ui/StatusHelper;->messageView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    invoke-virtual {p0}, Lcom/google/android/videos/ui/StatusHelper;->ensureVisible()V

    .line 96
    return-void

    :cond_0
    move v0, v2

    .line 91
    goto :goto_0
.end method

.method public setLoading()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 76
    iget-object v0, p0, Lcom/google/android/videos/ui/StatusHelper;->errorIcon:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 77
    iget-object v0, p0, Lcom/google/android/videos/ui/StatusHelper;->retryButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 78
    iget-object v0, p0, Lcom/google/android/videos/ui/StatusHelper;->messageView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 79
    iget-object v0, p0, Lcom/google/android/videos/ui/StatusHelper;->progress:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 80
    invoke-virtual {p0}, Lcom/google/android/videos/ui/StatusHelper;->ensureVisible()V

    .line 81
    return-void
.end method
