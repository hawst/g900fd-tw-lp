.class public abstract Lcom/google/android/videos/remote/TransportControl;
.super Ljava/lang/Object;
.source "TransportControl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/remote/TransportControl$Listener;
    }
.end annotation


# instance fields
.field private final listeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/remote/TransportControl$Listener;",
            ">;"
        }
    .end annotation
.end field

.field protected final remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;


# direct methods
.method protected constructor <init>(Lcom/google/android/videos/remote/RemoteTracker;)V
    .locals 1
    .param p1, "remoteTracker"    # Lcom/google/android/videos/remote/RemoteTracker;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/remote/TransportControl;->listeners:Ljava/util/List;

    .line 36
    iput-object p1, p0, Lcom/google/android/videos/remote/TransportControl;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    .line 37
    return-void
.end method


# virtual methods
.method public addListener(Lcom/google/android/videos/remote/TransportControl$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/videos/remote/TransportControl$Listener;

    .prologue
    .line 43
    invoke-static {}, Lcom/google/android/videos/utils/Preconditions;->checkMainThread()V

    .line 44
    iget-object v0, p0, Lcom/google/android/videos/remote/TransportControl;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    return-void
.end method

.method protected final getListeners()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/remote/TransportControl$Listener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/videos/remote/TransportControl;->listeners:Ljava/util/List;

    return-object v0
.end method

.method public onErrorChanged()V
    .locals 0

    .prologue
    .line 63
    return-void
.end method

.method public onPlayerStateChanged()V
    .locals 0

    .prologue
    .line 64
    return-void
.end method

.method public onPosterBitmapChanged()V
    .locals 0

    .prologue
    .line 66
    return-void
.end method

.method public onScreenNameChanged()V
    .locals 0

    .prologue
    .line 62
    return-void
.end method

.method public onVideoInfoChanged()V
    .locals 0

    .prologue
    .line 65
    return-void
.end method

.method public removeListener(Lcom/google/android/videos/remote/TransportControl$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/videos/remote/TransportControl$Listener;

    .prologue
    .line 51
    invoke-static {}, Lcom/google/android/videos/utils/Preconditions;->checkMainThread()V

    .line 52
    iget-object v0, p0, Lcom/google/android/videos/remote/TransportControl;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 53
    return-void
.end method
