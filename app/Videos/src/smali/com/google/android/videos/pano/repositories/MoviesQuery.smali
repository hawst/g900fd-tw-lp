.class interface abstract Lcom/google/android/videos/pano/repositories/MoviesQuery;
.super Ljava/lang/Object;
.source "MoviesQuery.java"


# static fields
.field public static final PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 18
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "video_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "poster_uri"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "merged_expiration_timestamp"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "release_year"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "duration_seconds"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "screenshot_uri"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "resume_timestamp"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/videos/pano/repositories/MoviesQuery;->PROJECTION:[Ljava/lang/String;

    return-void
.end method
