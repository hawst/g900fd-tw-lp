.class public Lcom/google/android/videos/ui/BannerTextHelper;
.super Ljava/lang/Object;
.source "BannerTextHelper.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/repolib/observers/Updatable;
.implements Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;


# instance fields
.field private final downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

.field private final networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

.field private final playHeaderListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

.field private turningOffDownloadedOnly:Z

.field private updateBannerTextRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/videos/utils/DownloadedOnlyManager;)V
    .locals 0
    .param p1, "playHeaderListLayout"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .param p2, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;
    .param p3, "downloadedOnlyManager"    # Lcom/google/android/videos/utils/DownloadedOnlyManager;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    iput-object p1, p0, Lcom/google/android/videos/ui/BannerTextHelper;->playHeaderListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 35
    iput-object p2, p0, Lcom/google/android/videos/ui/BannerTextHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 36
    iput-object p3, p0, Lcom/google/android/videos/ui/BannerTextHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    .line 37
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/ui/BannerTextHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/BannerTextHelper;

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/google/android/videos/ui/BannerTextHelper;->turningOffDownloadedOnly:Z

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/videos/ui/BannerTextHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/ui/BannerTextHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 19
    iput-boolean p1, p0, Lcom/google/android/videos/ui/BannerTextHelper;->turningOffDownloadedOnly:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/videos/ui/BannerTextHelper;)Lcom/google/android/videos/utils/DownloadedOnlyManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/BannerTextHelper;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/videos/ui/BannerTextHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    return-object v0
.end method

.method private getBannerText(I)Ljava/lang/String;
    .locals 3
    .param p1, "resId"    # I

    .prologue
    .line 121
    iget-object v1, p0, Lcom/google/android/videos/ui/BannerTextHelper;->playHeaderListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 122
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private updateBannerText(Z)V
    .locals 2
    .param p1, "animate"    # Z

    .prologue
    .line 107
    const/4 v0, 0x0

    .line 108
    .local v0, "bannerText":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/videos/ui/BannerTextHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/videos/ui/BannerTextHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-virtual {v1}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->isDownloadedOnly()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 109
    iget-boolean v1, p0, Lcom/google/android/videos/ui/BannerTextHelper;->turningOffDownloadedOnly:Z

    if-eqz v1, :cond_1

    .line 110
    const v1, 0x7f0b009f

    invoke-direct {p0, v1}, Lcom/google/android/videos/ui/BannerTextHelper;->getBannerText(I)Ljava/lang/String;

    move-result-object v0

    .line 117
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/ui/BannerTextHelper;->playHeaderListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v1, v0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setBannerText(Ljava/lang/CharSequence;Z)V

    .line 118
    return-void

    .line 112
    :cond_1
    const v1, 0x7f0b009e

    invoke-direct {p0, v1}, Lcom/google/android/videos/ui/BannerTextHelper;->getBannerText(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 114
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/ui/BannerTextHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/ui/BannerTextHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v1}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 115
    const v1, 0x7f0b00a0

    invoke-direct {p0, v1}, Lcom/google/android/videos/ui/BannerTextHelper;->getBannerText(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public disable()V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/videos/ui/BannerTextHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/google/android/videos/ui/BannerTextHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0, p0}, Lcom/google/android/videos/utils/NetworkStatus;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/BannerTextHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    if-eqz v0, :cond_1

    .line 61
    iget-object v0, p0, Lcom/google/android/videos/ui/BannerTextHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->unregisterObserver(Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;)V

    .line 63
    :cond_1
    return-void
.end method

.method public enable()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/videos/ui/BannerTextHelper;->updateBannerText(Z)V

    .line 44
    iget-object v0, p0, Lcom/google/android/videos/ui/BannerTextHelper;->playHeaderListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setBannerOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    iget-object v0, p0, Lcom/google/android/videos/ui/BannerTextHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/google/android/videos/ui/BannerTextHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0, p0}, Lcom/google/android/videos/utils/NetworkStatus;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/BannerTextHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    if-eqz v0, :cond_1

    .line 49
    iget-object v0, p0, Lcom/google/android/videos/ui/BannerTextHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->registerObserver(Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;)V

    .line 51
    :cond_1
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/videos/ui/BannerTextHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/BannerTextHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-virtual {v0}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->isDownloadedOnly()Z

    move-result v0

    if-nez v0, :cond_1

    .line 94
    :cond_0
    :goto_0
    return-void

    .line 73
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/videos/ui/BannerTextHelper;->turningOffDownloadedOnly:Z

    if-nez v0, :cond_0

    .line 76
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/ui/BannerTextHelper;->turningOffDownloadedOnly:Z

    .line 77
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/videos/ui/BannerTextHelper;->updateBannerText(Z)V

    .line 79
    iget-object v0, p0, Lcom/google/android/videos/ui/BannerTextHelper;->updateBannerTextRunnable:Ljava/lang/Runnable;

    if-nez v0, :cond_2

    .line 80
    new-instance v0, Lcom/google/android/videos/ui/BannerTextHelper$1;

    invoke-direct {v0, p0}, Lcom/google/android/videos/ui/BannerTextHelper$1;-><init>(Lcom/google/android/videos/ui/BannerTextHelper;)V

    iput-object v0, p0, Lcom/google/android/videos/ui/BannerTextHelper;->updateBannerTextRunnable:Ljava/lang/Runnable;

    .line 93
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/ui/BannerTextHelper;->playHeaderListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v1, p0, Lcom/google/android/videos/ui/BannerTextHelper;->updateBannerTextRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public onDownloadedOnlyChanged(Z)V
    .locals 1
    .param p1, "downloadedOnly"    # Z

    .prologue
    .line 98
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/videos/ui/BannerTextHelper;->updateBannerText(Z)V

    .line 99
    return-void
.end method

.method public update()V
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/videos/ui/BannerTextHelper;->updateBannerText(Z)V

    .line 104
    return-void
.end method
