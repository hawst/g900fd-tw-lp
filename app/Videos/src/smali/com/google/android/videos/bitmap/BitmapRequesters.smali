.class public interface abstract Lcom/google/android/videos/bitmap/BitmapRequesters;
.super Ljava/lang/Object;
.source "BitmapRequesters.java"


# virtual methods
.method public abstract getBitmapBytesRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getBitmapRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getControllableBitmapRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getPosterArtRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getSyncBitmapBytesMemoryCacheRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getSyncBitmapBytesRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getSyncBitmapRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end method
