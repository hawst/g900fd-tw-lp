.class Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository$ResourcePredicate;
.super Ljava/lang/Object;
.source "DynamicVideoCollectionLeanbackRepository.java"

# interfaces
.implements Lcom/google/android/repolib/common/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ResourcePredicate"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Predicate",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository$1;

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository$ResourcePredicate;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;)Z
    .locals 1
    .param p1, "resource"    # Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    .prologue
    .line 98
    iget-object v0, p1, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->collectionId:Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->collectionId:Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;->id:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->title:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->child:[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 95
    check-cast p1, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository$ResourcePredicate;->apply(Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;)Z

    move-result v0

    return v0
.end method
