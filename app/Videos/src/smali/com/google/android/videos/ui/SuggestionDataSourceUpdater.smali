.class public abstract Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;
.super Ljava/lang/Object;
.source "SuggestionDataSourceUpdater.java"

# interfaces
.implements Lcom/google/android/videos/store/StoreStatusMonitor$Listener;
.implements Lcom/google/android/videos/ui/VideoCollectionHelper$Listener;


# instance fields
.field private account:Ljava/lang/String;

.field private final configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

.field private final maxVideosPerCluster:I

.field private final storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

.field private suggestions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/store/StoreStatusMonitor;I)V
    .locals 1
    .param p1, "configurationStore"    # Lcom/google/android/videos/store/ConfigurationStore;
    .param p2, "storeStatusMonitor"    # Lcom/google/android/videos/store/StoreStatusMonitor;
    .param p3, "maxVideosPerCluster"    # I

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/ConfigurationStore;

    iput-object v0, p0, Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    .line 48
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/StoreStatusMonitor;

    iput-object v0, p0, Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    .line 49
    iput p3, p0, Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;->maxVideosPerCluster:I

    .line 50
    return-void
.end method

.method private filterSuggestions()V
    .locals 14

    .prologue
    .line 81
    iget-object v12, p0, Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;->suggestions:Ljava/util/List;

    if-eqz v12, :cond_0

    iget-object v12, p0, Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;->suggestions:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->isEmpty()Z

    move-result v12

    if-nez v12, :cond_0

    iget-object v12, p0, Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;->account:Ljava/lang/String;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 84
    :cond_1
    iget-object v12, p0, Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    iget-object v13, p0, Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;->account:Ljava/lang/String;

    invoke-virtual {v12, v13}, Lcom/google/android/videos/store/ConfigurationStore;->moviesVerticalEnabled(Ljava/lang/String;)Z

    move-result v8

    .line 85
    .local v8, "hasMovie":Z
    iget-object v12, p0, Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    iget-object v13, p0, Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;->account:Ljava/lang/String;

    invoke-virtual {v12, v13}, Lcom/google/android/videos/store/ConfigurationStore;->showsVerticalEnabled(Ljava/lang/String;)Z

    move-result v9

    .line 86
    .local v9, "hasShow":Z
    if-nez v8, :cond_2

    if-eqz v9, :cond_0

    .line 89
    :cond_2
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 90
    .local v4, "filteredMoviesIdSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 91
    .local v6, "filteredShowsIdSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 92
    .local v3, "filteredMovies":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 93
    .local v5, "filteredShows":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 94
    .local v7, "filteredVideos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 95
    .local v1, "assetResources":Ljava/util/List;, "Ljava/util/List<Lcom/google/wireless/android/video/magma/proto/AssetResource;>;"
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    iget-object v12, p0, Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;->suggestions:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    if-ge v10, v12, :cond_6

    .line 96
    iget-object v12, p0, Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;->suggestions:Ljava/util/List;

    invoke-interface {v12, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    invoke-static {v12, v1}, Lcom/google/android/videos/pano/ui/VideoCollectionUtil;->flattenCollectionResource(Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;Ljava/util/List;)V

    .line 98
    const/4 v2, 0x0

    .line 99
    .local v2, "countInCluster":I
    const/4 v11, 0x0

    .local v11, "j":I
    :goto_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v12

    if-ge v11, v12, :cond_5

    iget v12, p0, Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;->maxVideosPerCluster:I

    if-ge v2, v12, :cond_5

    .line 100
    invoke-interface {v1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 101
    .local v0, "assetResource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    if-eqz v8, :cond_3

    .line 102
    const/4 v12, 0x6

    invoke-direct {p0, v0, v12, v4, v3}, Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;->maybeAddAssetResource(Lcom/google/wireless/android/video/magma/proto/AssetResource;ILjava/util/Set;Ljava/util/ArrayList;)I

    move-result v12

    add-int/2addr v2, v12

    .line 105
    :cond_3
    if-eqz v9, :cond_4

    .line 106
    const/16 v12, 0x12

    invoke-direct {p0, v0, v12, v6, v5}, Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;->maybeAddAssetResource(Lcom/google/wireless/android/video/magma/proto/AssetResource;ILjava/util/Set;Ljava/util/ArrayList;)I

    move-result v12

    add-int/2addr v2, v12

    .line 99
    :cond_4
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 110
    .end local v0    # "assetResource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :cond_5
    invoke-direct {p0, v3, v7}, Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;->maybeMoveItems(Ljava/util/List;Ljava/util/List;)V

    .line 111
    invoke-direct {p0, v5, v7}, Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;->maybeMoveItems(Ljava/util/List;Ljava/util/List;)V

    .line 112
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 95
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 114
    .end local v2    # "countInCluster":I
    .end local v11    # "j":I
    :cond_6
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v12

    new-array v12, v12, [Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;

    invoke-virtual {v7, v12}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v12

    check-cast v12, [Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;

    invoke-virtual {p0, v12}, Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;->onFilteredSuggestions([Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;)V

    goto/16 :goto_0
.end method

.method private maybeAddAssetResource(Lcom/google/wireless/android/video/magma/proto/AssetResource;ILjava/util/Set;Ljava/util/ArrayList;)I
    .locals 6
    .param p1, "assetResource"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p2, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            "I",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;",
            ">;)I"
        }
    .end annotation

    .prologue
    .local p3, "filteredIdSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p4, "filtered":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;>;"
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 140
    iget-object v5, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget v1, v5, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    .line 141
    .local v1, "resourceType":I
    if-eq v1, p2, :cond_1

    .line 159
    :cond_0
    :goto_0
    return v3

    .line 144
    :cond_1
    iget-object v5, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    invoke-static {v3, v5}, Lcom/google/android/videos/utils/OfferUtil;->getCheapestOffer(Z[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;)Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    move-result-object v5

    if-nez v5, :cond_2

    iget-object v5, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    invoke-static {v4, v5}, Lcom/google/android/videos/utils/OfferUtil;->getCheapestOffer(Z[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;)Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    move-result-object v5

    if-nez v5, :cond_0

    .line 149
    :cond_2
    iget-object v5, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v0, v5, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    .line 150
    .local v0, "resourceId":Ljava/lang/String;
    invoke-interface {p3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 153
    iget-object v5, p0, Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    invoke-virtual {v5, v0, v1}, Lcom/google/android/videos/store/StoreStatusMonitor;->getStatus(Ljava/lang/String;I)I

    move-result v2

    .line 154
    .local v2, "status":I
    invoke-static {v2}, Lcom/google/android/videos/store/StoreStatusMonitor;->isPurchased(I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 157
    new-instance v3, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;

    invoke-static {v2}, Lcom/google/android/videos/store/StoreStatusMonitor;->isWishlisted(I)Z

    move-result v5

    invoke-direct {v3, p1, v5}, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;-><init>(Lcom/google/wireless/android/video/magma/proto/AssetResource;Z)V

    invoke-virtual {p4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v3, v4

    .line 159
    goto :goto_0
.end method

.method private maybeMoveItems(Ljava/util/List;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 122
    .local p1, "fromList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;>;"
    .local p2, "toList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 123
    .local v0, "size":I
    const/4 v2, 0x1

    if-le v0, v2, :cond_0

    .line 124
    const/4 v2, 0x0

    rem-int/lit8 v3, v0, 0x2

    if-nez v3, :cond_1

    .end local v0    # "size":I
    :goto_0
    invoke-interface {p1, v2, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    .line 125
    .local v1, "subList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;>;"
    invoke-interface {p2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 126
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 128
    .end local v1    # "subList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;>;"
    :cond_0
    return-void

    .line 124
    .restart local v0    # "size":I
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public onAccountUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;->account:Ljava/lang/String;

    .line 77
    invoke-virtual {p0}, Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;->reset()V

    .line 78
    return-void
.end method

.method public onCollectionsAvailable([Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;)V
    .locals 1
    .param p1, "collectionResources"    # [Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    .prologue
    .line 61
    const v0, 0x7fffffff

    invoke-static {p1, v0}, Lcom/google/android/videos/pano/ui/VideoCollectionUtil;->filterValidResource([Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;->suggestions:Ljava/util/List;

    .line 62
    invoke-direct {p0}, Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;->filterSuggestions()V

    .line 63
    return-void
.end method

.method public onCollectionsError(Ljava/lang/String;)V
    .locals 0
    .param p1, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 68
    return-void
.end method

.method protected abstract onFilteredSuggestions([Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;)V
.end method

.method public onStoreStatusChanged(Lcom/google/android/videos/store/StoreStatusMonitor;)V
    .locals 0
    .param p1, "sender"    # Lcom/google/android/videos/store/StoreStatusMonitor;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;->filterSuggestions()V

    .line 73
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;->suggestions:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;->suggestions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;->suggestions:Ljava/util/List;

    .line 57
    :cond_0
    return-void
.end method
