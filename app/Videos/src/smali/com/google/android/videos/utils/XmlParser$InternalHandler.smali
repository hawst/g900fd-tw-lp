.class final Lcom/google/android/videos/utils/XmlParser$InternalHandler;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "XmlParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/utils/XmlParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "InternalHandler"
.end annotation


# instance fields
.field private final attrsStack:Lcom/google/android/videos/utils/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/utils/Stack",
            "<",
            "Lorg/xml/sax/Attributes;",
            ">;"
        }
    .end annotation
.end field

.field private final charsStack:Lcom/google/android/videos/utils/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/utils/Stack",
            "<",
            "Ljava/lang/StringBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private final path:Lcom/google/android/videos/utils/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/utils/Stack",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public result:Ljava/lang/Object;

.field private final rules:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/utils/XmlParser$Rule;",
            ">;"
        }
    .end annotation
.end field

.field private final stack:Lcom/google/android/videos/utils/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/utils/Stack",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/utils/XmlParser$Rule;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 180
    .local p1, "rules":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/videos/utils/XmlParser$Rule;>;"
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 181
    iput-object p1, p0, Lcom/google/android/videos/utils/XmlParser$InternalHandler;->rules:Ljava/util/Map;

    .line 182
    new-instance v0, Lcom/google/android/videos/utils/Stack;

    invoke-direct {v0}, Lcom/google/android/videos/utils/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/utils/XmlParser$InternalHandler;->path:Lcom/google/android/videos/utils/Stack;

    .line 184
    iget-object v0, p0, Lcom/google/android/videos/utils/XmlParser$InternalHandler;->path:Lcom/google/android/videos/utils/Stack;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/Stack;->offer(Ljava/lang/Object;)Z

    .line 185
    new-instance v0, Lcom/google/android/videos/utils/Stack;

    invoke-direct {v0}, Lcom/google/android/videos/utils/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/utils/XmlParser$InternalHandler;->attrsStack:Lcom/google/android/videos/utils/Stack;

    .line 186
    new-instance v0, Lcom/google/android/videos/utils/Stack;

    invoke-direct {v0}, Lcom/google/android/videos/utils/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/utils/XmlParser$InternalHandler;->charsStack:Lcom/google/android/videos/utils/Stack;

    .line 187
    new-instance v0, Lcom/google/android/videos/utils/Stack;

    invoke-direct {v0}, Lcom/google/android/videos/utils/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/utils/XmlParser$InternalHandler;->stack:Lcom/google/android/videos/utils/Stack;

    .line 188
    return-void
.end method


# virtual methods
.method public final characters([CII)V
    .locals 2
    .param p1, "data"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I

    .prologue
    .line 217
    iget-object v1, p0, Lcom/google/android/videos/utils/XmlParser$InternalHandler;->path:Lcom/google/android/videos/utils/Stack;

    invoke-virtual {v1}, Lcom/google/android/videos/utils/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 218
    .local v0, "currentPath":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/videos/utils/XmlParser$InternalHandler;->rules:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 219
    iget-object v1, p0, Lcom/google/android/videos/utils/XmlParser$InternalHandler;->charsStack:Lcom/google/android/videos/utils/Stack;

    invoke-virtual {v1}, Lcom/google/android/videos/utils/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1, p2, p3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 221
    :cond_0
    return-void
.end method

.method public final endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "tag"    # Ljava/lang/String;

    .prologue
    .line 204
    iget-object v5, p0, Lcom/google/android/videos/utils/XmlParser$InternalHandler;->path:Lcom/google/android/videos/utils/Stack;

    invoke-virtual {v5}, Lcom/google/android/videos/utils/Stack;->poll()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 205
    .local v2, "currentPath":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/videos/utils/XmlParser$InternalHandler;->rules:Ljava/util/Map;

    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/videos/utils/XmlParser$Rule;

    .line 206
    .local v4, "rule":Lcom/google/android/videos/utils/XmlParser$Rule;
    if-eqz v4, :cond_0

    .line 207
    iget-object v5, p0, Lcom/google/android/videos/utils/XmlParser$InternalHandler;->attrsStack:Lcom/google/android/videos/utils/Stack;

    invoke-virtual {v5}, Lcom/google/android/videos/utils/Stack;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/xml/sax/Attributes;

    .line 208
    .local v0, "attrs":Lorg/xml/sax/Attributes;
    iget-object v5, p0, Lcom/google/android/videos/utils/XmlParser$InternalHandler;->charsStack:Lcom/google/android/videos/utils/Stack;

    invoke-virtual {v5}, Lcom/google/android/videos/utils/Stack;->poll()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 209
    .local v1, "chars":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/videos/utils/XmlParser$InternalHandler;->stack:Lcom/google/android/videos/utils/Stack;

    invoke-virtual {v5}, Lcom/google/android/videos/utils/Stack;->peek()Ljava/lang/Object;

    move-result-object v3

    .line 210
    .local v3, "resultCandidate":Ljava/lang/Object;
    if-eqz v3, :cond_1

    .end local v3    # "resultCandidate":Ljava/lang/Object;
    :goto_0
    iput-object v3, p0, Lcom/google/android/videos/utils/XmlParser$InternalHandler;->result:Ljava/lang/Object;

    .line 211
    iget-object v5, p0, Lcom/google/android/videos/utils/XmlParser$InternalHandler;->stack:Lcom/google/android/videos/utils/Stack;

    invoke-virtual {v4, v5, v0, v1}, Lcom/google/android/videos/utils/XmlParser$Rule;->end(Lcom/google/android/videos/utils/Stack;Lorg/xml/sax/Attributes;Ljava/lang/String;)V

    .line 213
    .end local v0    # "attrs":Lorg/xml/sax/Attributes;
    .end local v1    # "chars":Ljava/lang/String;
    :cond_0
    return-void

    .line 210
    .restart local v0    # "attrs":Lorg/xml/sax/Attributes;
    .restart local v1    # "chars":Ljava/lang/String;
    .restart local v3    # "resultCandidate":Ljava/lang/Object;
    :cond_1
    iget-object v3, p0, Lcom/google/android/videos/utils/XmlParser$InternalHandler;->result:Ljava/lang/Object;

    goto :goto_0
.end method

.method public final startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "tag"    # Ljava/lang/String;
    .param p4, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 192
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/videos/utils/XmlParser$InternalHandler;->path:Lcom/google/android/videos/utils/Stack;

    invoke-virtual {v2}, Lcom/google/android/videos/utils/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 193
    .local v0, "currentPath":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/videos/utils/XmlParser$InternalHandler;->path:Lcom/google/android/videos/utils/Stack;

    invoke-virtual {v2, v0}, Lcom/google/android/videos/utils/Stack;->offer(Ljava/lang/Object;)Z

    .line 194
    iget-object v2, p0, Lcom/google/android/videos/utils/XmlParser$InternalHandler;->rules:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/utils/XmlParser$Rule;

    .line 195
    .local v1, "rule":Lcom/google/android/videos/utils/XmlParser$Rule;
    if-eqz v1, :cond_0

    .line 196
    iget-object v3, p0, Lcom/google/android/videos/utils/XmlParser$InternalHandler;->attrsStack:Lcom/google/android/videos/utils/Stack;

    if-eqz p4, :cond_1

    new-instance v2, Lorg/xml/sax/helpers/AttributesImpl;

    invoke-direct {v2, p4}, Lorg/xml/sax/helpers/AttributesImpl;-><init>(Lorg/xml/sax/Attributes;)V

    :goto_0
    invoke-virtual {v3, v2}, Lcom/google/android/videos/utils/Stack;->offer(Ljava/lang/Object;)Z

    .line 197
    iget-object v2, p0, Lcom/google/android/videos/utils/XmlParser$InternalHandler;->charsStack:Lcom/google/android/videos/utils/Stack;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Lcom/google/android/videos/utils/Stack;->offer(Ljava/lang/Object;)Z

    .line 198
    iget-object v2, p0, Lcom/google/android/videos/utils/XmlParser$InternalHandler;->stack:Lcom/google/android/videos/utils/Stack;

    invoke-virtual {v1, v2, p4}, Lcom/google/android/videos/utils/XmlParser$Rule;->start(Lcom/google/android/videos/utils/Stack;Lorg/xml/sax/Attributes;)V

    .line 200
    :cond_0
    return-void

    .line 196
    :cond_1
    # getter for: Lcom/google/android/videos/utils/XmlParser;->EMPTY_ATTRS:Lorg/xml/sax/Attributes;
    invoke-static {}, Lcom/google/android/videos/utils/XmlParser;->access$100()Lorg/xml/sax/Attributes;

    move-result-object v2

    goto :goto_0
.end method
