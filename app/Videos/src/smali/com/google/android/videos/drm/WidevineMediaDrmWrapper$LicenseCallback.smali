.class Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$LicenseCallback;
.super Ljava/lang/Object;
.source "WidevineMediaDrmWrapper.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LicenseCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/api/CencLicenseRequest;",
        "[B>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;)V
    .locals 0

    .prologue
    .line 546
    iput-object p1, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$LicenseCallback;->this$0:Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;
    .param p2, "x1"    # Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$1;

    .prologue
    .line 546
    invoke-direct {p0, p1}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$LicenseCallback;-><init>(Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;)V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/api/CencLicenseRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/api/CencLicenseRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 555
    iget-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$LicenseCallback;->this$0:Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->onLicenseError(Lcom/google/android/videos/api/CencLicenseRequest;Ljava/lang/Exception;)V

    .line 556
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 546
    check-cast p1, Lcom/google/android/videos/api/CencLicenseRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$LicenseCallback;->onError(Lcom/google/android/videos/api/CencLicenseRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/api/CencLicenseRequest;[B)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/api/CencLicenseRequest;
    .param p2, "response"    # [B

    .prologue
    .line 550
    iget-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$LicenseCallback;->this$0:Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->onLicenseResponse(Lcom/google/android/videos/api/CencLicenseRequest;[B)V

    .line 551
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 546
    check-cast p1, Lcom/google/android/videos/api/CencLicenseRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, [B

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$LicenseCallback;->onResponse(Lcom/google/android/videos/api/CencLicenseRequest;[B)V

    return-void
.end method
