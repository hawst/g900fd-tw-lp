.class public final Lcom/google/android/videos/proto/FilmProtos$Song;
.super Lcom/google/protobuf/nano/MessageNano;
.source "FilmProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/proto/FilmProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Song"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/videos/proto/FilmProtos$Song;


# instance fields
.field public albumArt:Lcom/google/android/videos/proto/FilmProtos$Image;

.field public appearance:[B

.field public googlePlayUrl:Ljava/lang/String;

.field public isrc:Ljava/lang/String;

.field public localId:I

.field public mid:Ljava/lang/String;

.field public performer:Ljava/lang/String;

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1988
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1989
    invoke-virtual {p0}, Lcom/google/android/videos/proto/FilmProtos$Song;->clear()Lcom/google/android/videos/proto/FilmProtos$Song;

    .line 1990
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/videos/proto/FilmProtos$Song;
    .locals 2

    .prologue
    .line 1953
    sget-object v0, Lcom/google/android/videos/proto/FilmProtos$Song;->_emptyArray:[Lcom/google/android/videos/proto/FilmProtos$Song;

    if-nez v0, :cond_1

    .line 1954
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 1956
    :try_start_0
    sget-object v0, Lcom/google/android/videos/proto/FilmProtos$Song;->_emptyArray:[Lcom/google/android/videos/proto/FilmProtos$Song;

    if-nez v0, :cond_0

    .line 1957
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/videos/proto/FilmProtos$Song;

    sput-object v0, Lcom/google/android/videos/proto/FilmProtos$Song;->_emptyArray:[Lcom/google/android/videos/proto/FilmProtos$Song;

    .line 1959
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1961
    :cond_1
    sget-object v0, Lcom/google/android/videos/proto/FilmProtos$Song;->_emptyArray:[Lcom/google/android/videos/proto/FilmProtos$Song;

    return-object v0

    .line 1959
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/videos/proto/FilmProtos$Song;
    .locals 1

    .prologue
    .line 1993
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->mid:Ljava/lang/String;

    .line 1994
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->title:Ljava/lang/String;

    .line 1995
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->performer:Ljava/lang/String;

    .line 1996
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->isrc:Ljava/lang/String;

    .line 1997
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->googlePlayUrl:Ljava/lang/String;

    .line 1998
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->localId:I

    .line 1999
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->albumArt:Lcom/google/android/videos/proto/FilmProtos$Image;

    .line 2000
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->appearance:[B

    .line 2001
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->cachedSize:I

    .line 2002
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 2119
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2120
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->mid:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2121
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->mid:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2124
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->title:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2125
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->title:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2128
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->performer:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2129
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->performer:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2132
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->isrc:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2133
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->isrc:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2136
    :cond_3
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->googlePlayUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2137
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->googlePlayUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2140
    :cond_4
    iget v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->localId:I

    if-eqz v1, :cond_5

    .line 2141
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->localId:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2144
    :cond_5
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->albumArt:Lcom/google/android/videos/proto/FilmProtos$Image;

    if-eqz v1, :cond_6

    .line 2145
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->albumArt:Lcom/google/android/videos/proto/FilmProtos$Image;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2148
    :cond_6
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->appearance:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_7

    .line 2149
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->appearance:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 2152
    :cond_7
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2007
    if-ne p1, p0, :cond_1

    .line 2064
    :cond_0
    :goto_0
    return v1

    .line 2010
    :cond_1
    instance-of v3, p1, Lcom/google/android/videos/proto/FilmProtos$Song;

    if-nez v3, :cond_2

    move v1, v2

    .line 2011
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 2013
    check-cast v0, Lcom/google/android/videos/proto/FilmProtos$Song;

    .line 2014
    .local v0, "other":Lcom/google/android/videos/proto/FilmProtos$Song;
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->mid:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 2015
    iget-object v3, v0, Lcom/google/android/videos/proto/FilmProtos$Song;->mid:Ljava/lang/String;

    if-eqz v3, :cond_4

    move v1, v2

    .line 2016
    goto :goto_0

    .line 2018
    :cond_3
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->mid:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Song;->mid:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 2019
    goto :goto_0

    .line 2021
    :cond_4
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->title:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 2022
    iget-object v3, v0, Lcom/google/android/videos/proto/FilmProtos$Song;->title:Ljava/lang/String;

    if-eqz v3, :cond_6

    move v1, v2

    .line 2023
    goto :goto_0

    .line 2025
    :cond_5
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->title:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Song;->title:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    .line 2026
    goto :goto_0

    .line 2028
    :cond_6
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->performer:Ljava/lang/String;

    if-nez v3, :cond_7

    .line 2029
    iget-object v3, v0, Lcom/google/android/videos/proto/FilmProtos$Song;->performer:Ljava/lang/String;

    if-eqz v3, :cond_8

    move v1, v2

    .line 2030
    goto :goto_0

    .line 2032
    :cond_7
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->performer:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Song;->performer:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    move v1, v2

    .line 2033
    goto :goto_0

    .line 2035
    :cond_8
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->isrc:Ljava/lang/String;

    if-nez v3, :cond_9

    .line 2036
    iget-object v3, v0, Lcom/google/android/videos/proto/FilmProtos$Song;->isrc:Ljava/lang/String;

    if-eqz v3, :cond_a

    move v1, v2

    .line 2037
    goto :goto_0

    .line 2039
    :cond_9
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->isrc:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Song;->isrc:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    move v1, v2

    .line 2040
    goto :goto_0

    .line 2042
    :cond_a
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->googlePlayUrl:Ljava/lang/String;

    if-nez v3, :cond_b

    .line 2043
    iget-object v3, v0, Lcom/google/android/videos/proto/FilmProtos$Song;->googlePlayUrl:Ljava/lang/String;

    if-eqz v3, :cond_c

    move v1, v2

    .line 2044
    goto :goto_0

    .line 2046
    :cond_b
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->googlePlayUrl:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Song;->googlePlayUrl:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    move v1, v2

    .line 2047
    goto :goto_0

    .line 2049
    :cond_c
    iget v3, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->localId:I

    iget v4, v0, Lcom/google/android/videos/proto/FilmProtos$Song;->localId:I

    if-eq v3, v4, :cond_d

    move v1, v2

    .line 2050
    goto :goto_0

    .line 2052
    :cond_d
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->albumArt:Lcom/google/android/videos/proto/FilmProtos$Image;

    if-nez v3, :cond_e

    .line 2053
    iget-object v3, v0, Lcom/google/android/videos/proto/FilmProtos$Song;->albumArt:Lcom/google/android/videos/proto/FilmProtos$Image;

    if-eqz v3, :cond_f

    move v1, v2

    .line 2054
    goto/16 :goto_0

    .line 2057
    :cond_e
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->albumArt:Lcom/google/android/videos/proto/FilmProtos$Image;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Song;->albumArt:Lcom/google/android/videos/proto/FilmProtos$Image;

    invoke-virtual {v3, v4}, Lcom/google/android/videos/proto/FilmProtos$Image;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    move v1, v2

    .line 2058
    goto/16 :goto_0

    .line 2061
    :cond_f
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->appearance:[B

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Song;->appearance:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 2062
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2069
    const/16 v0, 0x11

    .line 2070
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->mid:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 2072
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->title:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 2074
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->performer:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    add-int v0, v3, v1

    .line 2076
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->isrc:Ljava/lang/String;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    add-int v0, v3, v1

    .line 2078
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->googlePlayUrl:Ljava/lang/String;

    if-nez v1, :cond_4

    move v1, v2

    :goto_4
    add-int v0, v3, v1

    .line 2080
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->localId:I

    add-int v0, v1, v3

    .line 2081
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->albumArt:Lcom/google/android/videos/proto/FilmProtos$Image;

    if-nez v3, :cond_5

    :goto_5
    add-int v0, v1, v2

    .line 2083
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->appearance:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int v0, v1, v2

    .line 2084
    return v0

    .line 2070
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->mid:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 2072
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->title:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 2074
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->performer:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    .line 2076
    :cond_3
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->isrc:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    .line 2078
    :cond_4
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->googlePlayUrl:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    .line 2081
    :cond_5
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->albumArt:Lcom/google/android/videos/proto/FilmProtos$Image;

    invoke-virtual {v2}, Lcom/google/android/videos/proto/FilmProtos$Image;->hashCode()I

    move-result v2

    goto :goto_5
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/FilmProtos$Song;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2160
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2161
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2165
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2166
    :sswitch_0
    return-object p0

    .line 2171
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->mid:Ljava/lang/String;

    goto :goto_0

    .line 2175
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->title:Ljava/lang/String;

    goto :goto_0

    .line 2179
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->performer:Ljava/lang/String;

    goto :goto_0

    .line 2183
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->isrc:Ljava/lang/String;

    goto :goto_0

    .line 2187
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->googlePlayUrl:Ljava/lang/String;

    goto :goto_0

    .line 2191
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->localId:I

    goto :goto_0

    .line 2195
    :sswitch_7
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->albumArt:Lcom/google/android/videos/proto/FilmProtos$Image;

    if-nez v1, :cond_1

    .line 2196
    new-instance v1, Lcom/google/android/videos/proto/FilmProtos$Image;

    invoke-direct {v1}, Lcom/google/android/videos/proto/FilmProtos$Image;-><init>()V

    iput-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->albumArt:Lcom/google/android/videos/proto/FilmProtos$Image;

    .line 2198
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->albumArt:Lcom/google/android/videos/proto/FilmProtos$Image;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2202
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->appearance:[B

    goto :goto_0

    .line 2161
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1947
    invoke-virtual {p0, p1}, Lcom/google/android/videos/proto/FilmProtos$Song;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/FilmProtos$Song;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2090
    iget-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->mid:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2091
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->mid:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2093
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->title:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2094
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->title:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2096
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->performer:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2097
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->performer:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2099
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->isrc:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2100
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->isrc:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2102
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->googlePlayUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2103
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->googlePlayUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2105
    :cond_4
    iget v0, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->localId:I

    if-eqz v0, :cond_5

    .line 2106
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->localId:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2108
    :cond_5
    iget-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->albumArt:Lcom/google/android/videos/proto/FilmProtos$Image;

    if-eqz v0, :cond_6

    .line 2109
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->albumArt:Lcom/google/android/videos/proto/FilmProtos$Image;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2111
    :cond_6
    iget-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->appearance:[B

    sget-object v1, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2112
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Song;->appearance:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 2114
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2115
    return-void
.end method
