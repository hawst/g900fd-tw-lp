.class Lcom/google/android/videos/pinning/PinningTask;
.super Lcom/google/android/videos/pinning/Task;
.source "PinningTask.java"

# interfaces
.implements Lcom/google/android/videos/pinning/Downloader$ProgressListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/pinning/PinningTask$PinningException;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/pinning/Task",
        "<",
        "Lcom/google/android/videos/pinning/DownloadKey;",
        ">;",
        "Lcom/google/android/videos/pinning/Downloader$ProgressListener;"
    }
.end annotation


# instance fields
.field private final database:Lcom/google/android/videos/store/Database;

.field private final debug:Z

.field private downloader:Lcom/google/android/videos/pinning/Downloader;

.field private final rootFilesDir:Ljava/io/File;

.field private final videosGlobals:Lcom/google/android/videos/VideosGlobals;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/VideosGlobals;Lcom/google/android/videos/pinning/DownloadKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/videos/pinning/Task$Listener;Ljava/io/File;Z)V
    .locals 7
    .param p1, "videosGlobals"    # Lcom/google/android/videos/VideosGlobals;
    .param p2, "key"    # Lcom/google/android/videos/pinning/DownloadKey;
    .param p3, "wakeLock"    # Landroid/os/PowerManager$WakeLock;
    .param p4, "wifiLock"    # Landroid/net/wifi/WifiManager$WifiLock;
    .param p5, "listener"    # Lcom/google/android/videos/pinning/Task$Listener;
    .param p6, "rootFilesDir"    # Ljava/io/File;
    .param p7, "debug"    # Z

    .prologue
    .line 88
    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v6

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/pinning/Task;-><init>(ILcom/google/android/videos/pinning/Task$Key;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/videos/pinning/Task$Listener;Lcom/google/android/videos/logging/EventLogger;)V

    .line 89
    iput-object p1, p0, Lcom/google/android/videos/pinning/PinningTask;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    .line 90
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pinning/PinningTask;->database:Lcom/google/android/videos/store/Database;

    .line 91
    invoke-static {p6}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, Lcom/google/android/videos/pinning/PinningTask;->rootFilesDir:Ljava/io/File;

    .line 92
    iput-boolean p7, p0, Lcom/google/android/videos/pinning/PinningTask;->debug:Z

    .line 93
    return-void
.end method

.method private mergePlaybackPosition(Lcom/google/wireless/android/video/magma/proto/VideoResource;)V
    .locals 14
    .param p1, "videoResource"    # Lcom/google/wireless/android/video/magma/proto/VideoResource;

    .prologue
    const/16 v7, 0xc

    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v8, 0x0

    .line 215
    iget-object v6, p1, Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    if-nez v6, :cond_0

    .line 238
    :goto_0
    return-void

    .line 218
    :cond_0
    iget-object v0, p1, Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    .line 219
    .local v0, "playback":Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;
    const/4 v6, 0x3

    new-array v4, v6, [Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/videos/pinning/PinningTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v6, v6, Lcom/google/android/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    aput-object v6, v4, v8

    iget-object v6, p0, Lcom/google/android/videos/pinning/PinningTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v6, v6, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    aput-object v6, v4, v12

    iget-wide v10, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->stopTimestampMsec:J

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v13

    .line 221
    .local v4, "updateArgs":[Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/videos/pinning/PinningTask;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v6}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 222
    .local v3, "transaction":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v2, 0x0

    .line 223
    .local v2, "success":Z
    const/4 v1, 0x0

    .line 225
    .local v1, "rowsUpdated":I
    :try_start_0
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 226
    .local v5, "values":Landroid/content/ContentValues;
    const-string v6, "last_playback_start_timestamp"

    iget-wide v10, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->startTimestampMsec:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 227
    const-string v6, "last_watched_timestamp"

    iget-wide v10, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->stopTimestampMsec:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 228
    const-string v6, "resume_timestamp"

    iget-wide v10, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->positionMsec:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 229
    const-string v6, "last_playback_is_dirty"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 230
    const-string v6, "purchased_assets"

    const-string v9, "account = ? AND asset_type IN (6,20) AND asset_id = ? AND ? > last_watched_timestamp"

    invoke-virtual {v3, v6, v5, v9, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 232
    const/4 v2, 0x1

    .line 234
    iget-object v9, p0, Lcom/google/android/videos/pinning/PinningTask;->database:Lcom/google/android/videos/store/Database;

    if-lez v1, :cond_1

    :goto_1
    new-array v10, v13, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/videos/pinning/PinningTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v6, v6, Lcom/google/android/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    aput-object v6, v10, v8

    iget-object v6, p0, Lcom/google/android/videos/pinning/PinningTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v6, v6, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    aput-object v6, v10, v12

    invoke-virtual {v9, v3, v2, v7, v10}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    move v7, v8

    goto :goto_1

    .end local v5    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v6

    move-object v9, v6

    iget-object v10, p0, Lcom/google/android/videos/pinning/PinningTask;->database:Lcom/google/android/videos/store/Database;

    if-lez v1, :cond_2

    :goto_2
    new-array v11, v13, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/videos/pinning/PinningTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v6, v6, Lcom/google/android/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    aput-object v6, v11, v8

    iget-object v6, p0, Lcom/google/android/videos/pinning/PinningTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v6, v6, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    aput-object v6, v11, v12

    invoke-virtual {v10, v3, v2, v7, v11}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v9

    :cond_2
    move v7, v8

    goto :goto_2
.end method

.method private refreshPlaybackPosition()V
    .locals 6

    .prologue
    .line 195
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v0

    .line 196
    .local v0, "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/api/VideoGetRequest;Lcom/google/wireless/android/video/magma/proto/VideoResource;>;"
    new-instance v2, Lcom/google/android/videos/api/VideoGetRequest;

    iget-object v4, p0, Lcom/google/android/videos/pinning/PinningTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v4, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v5, v4, Lcom/google/android/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/videos/pinning/PinningTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v4, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v4, v4, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-direct {v2, v5, v4}, Lcom/google/android/videos/api/VideoGetRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    .local v2, "request":Lcom/google/android/videos/api/VideoGetRequest;
    iget-object v4, p0, Lcom/google/android/videos/pinning/PinningTask;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/videos/api/ApiRequesters;->getVideoGetRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v4

    invoke-interface {v4, v2, v0}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 199
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/wireless/android/video/magma/proto/VideoResource;

    .line 200
    .local v3, "videoResource":Lcom/google/wireless/android/video/magma/proto/VideoResource;
    invoke-direct {p0, v3}, Lcom/google/android/videos/pinning/PinningTask;->mergePlaybackPosition(Lcom/google/wireless/android/video/magma/proto/VideoResource;)V
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 204
    .end local v3    # "videoResource":Lcom/google/wireless/android/video/magma/proto/VideoResource;
    :goto_0
    return-void

    .line 201
    :catch_0
    move-exception v1

    .line 202
    .local v1, "e":Ljava/util/concurrent/ExecutionException;
    const-string v4, "Unable to fetch updated last playback"

    invoke-static {v4, v1}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private syncPurchases()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/PinningTask$PinningException;
        }
    .end annotation

    .prologue
    .line 169
    iget-object v4, p0, Lcom/google/android/videos/pinning/PinningTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v4, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v5, v4, Lcom/google/android/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/videos/pinning/PinningTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v4, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v4, v4, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-static {v5, v4}, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->createForId(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    move-result-object v3

    .line 171
    .local v3, "purchaseSyncRequest":Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v2

    .line 172
    .local v2, "purchaseSyncCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Ljava/lang/Void;>;"
    iget-object v4, p0, Lcom/google/android/videos/pinning/PinningTask;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStoreSync()Lcom/google/android/videos/store/PurchaseStoreSync;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/videos/pinning/PinningTask;->getTaskStatus()Lcom/google/android/videos/async/TaskStatus;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/google/android/videos/async/ControllableRequest;->create(Ljava/lang/Object;Lcom/google/android/videos/async/TaskStatus;)Lcom/google/android/videos/async/ControllableRequest;

    move-result-object v5

    invoke-virtual {v4, v5, v2}, Lcom/google/android/videos/store/PurchaseStoreSync;->syncPurchasesForVideo(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V

    .line 176
    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 188
    :goto_0
    return-void

    .line 179
    :catch_0
    move-exception v0

    .line 180
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "failed to sync purchases for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v4, p0, Lcom/google/android/videos/pinning/PinningTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v4, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v4, v4, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 181
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    .line 182
    .local v1, "exception":Ljava/lang/Throwable;
    instance-of v4, v1, Lcom/google/android/videos/store/SyncTaskManager$SyncException;

    if-eqz v4, :cond_0

    .line 183
    invoke-virtual {v1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    .line 185
    :cond_0
    new-instance v4, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    const-string v5, "could not fetch purchases"

    const/4 v6, 0x0

    const/16 v7, 0xd

    invoke-direct {v4, v5, v1, v6, v7}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;ZI)V

    throw v4

    .line 177
    .end local v0    # "e":Ljava/util/concurrent/ExecutionException;
    .end local v1    # "exception":Ljava/lang/Throwable;
    :catch_1
    move-exception v4

    goto :goto_0
.end method


# virtual methods
.method public execute()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/PinningTask$PinningException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    .line 97
    iget-object v1, p0, Lcom/google/android/videos/pinning/PinningTask;->database:Lcom/google/android/videos/store/Database;

    iget-object v0, p0, Lcom/google/android/videos/pinning/PinningTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/videos/pinning/DownloadKey;

    invoke-static {v1, v0}, Lcom/google/android/videos/pinning/PinningDbHelper;->getDownloadDetails(Lcom/google/android/videos/store/Database;Lcom/google/android/videos/pinning/DownloadKey;)Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;

    move-result-object v4

    .line 98
    .local v4, "details":Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;
    if-nez v4, :cond_1

    .line 128
    :cond_0
    :goto_0
    return-void

    .line 104
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/pinning/PinningTask;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/videos/pinning/PinningTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v0, v0, Lcom/google/android/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->accountExists(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 105
    new-instance v0, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    const-string v1, "account does not exist"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v9, v2}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    throw v0

    .line 109
    :cond_2
    invoke-direct {p0}, Lcom/google/android/videos/pinning/PinningTask;->syncPurchases()V

    .line 110
    invoke-virtual {p0}, Lcom/google/android/videos/pinning/PinningTask;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 114
    invoke-direct {p0}, Lcom/google/android/videos/pinning/PinningTask;->refreshPlaybackPosition()V

    .line 115
    invoke-virtual {p0}, Lcom/google/android/videos/pinning/PinningTask;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 119
    invoke-virtual {p0}, Lcom/google/android/videos/pinning/PinningTask;->getTaskStatus()Lcom/google/android/videos/async/TaskStatus;

    move-result-object v6

    .line 120
    .local v6, "taskStatus":Lcom/google/android/videos/async/TaskStatus;
    iget-object v0, p0, Lcom/google/android/videos/pinning/PinningTask;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v8

    .line 122
    .local v8, "config":Lcom/google/android/videos/Config;
    iget v0, v4, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->licenseType:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    invoke-interface {v8}, Lcom/google/android/videos/Config;->useDashForDownloads()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 124
    .local v9, "useDash":Z
    :cond_3
    :goto_1
    if-eqz v9, :cond_5

    new-instance v0, Lcom/google/android/videos/pinning/DashDownloader;

    iget-object v1, p0, Lcom/google/android/videos/pinning/PinningTask;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    iget-object v3, p0, Lcom/google/android/videos/pinning/PinningTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v3, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v5, p0, Lcom/google/android/videos/pinning/PinningTask;->rootFilesDir:Ljava/io/File;

    iget-boolean v7, p0, Lcom/google/android/videos/pinning/PinningTask;->debug:Z

    move-object v2, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/pinning/DashDownloader;-><init>(Lcom/google/android/videos/VideosGlobals;Lcom/google/android/videos/pinning/Downloader$ProgressListener;Lcom/google/android/videos/pinning/DownloadKey;Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/io/File;Lcom/google/android/videos/async/TaskStatus;Z)V

    :goto_2
    iput-object v0, p0, Lcom/google/android/videos/pinning/PinningTask;->downloader:Lcom/google/android/videos/pinning/Downloader;

    .line 127
    iget-object v0, p0, Lcom/google/android/videos/pinning/PinningTask;->downloader:Lcom/google/android/videos/pinning/Downloader;

    invoke-virtual {v0}, Lcom/google/android/videos/pinning/Downloader;->download()V

    goto :goto_0

    .line 122
    .end local v9    # "useDash":Z
    :cond_4
    const/4 v9, 0x0

    goto :goto_1

    .line 124
    .restart local v9    # "useDash":Z
    :cond_5
    new-instance v0, Lcom/google/android/videos/pinning/LegacyDownloader;

    iget-object v1, p0, Lcom/google/android/videos/pinning/PinningTask;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    iget-object v3, p0, Lcom/google/android/videos/pinning/PinningTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v3, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v5, p0, Lcom/google/android/videos/pinning/PinningTask;->rootFilesDir:Ljava/io/File;

    iget-boolean v7, p0, Lcom/google/android/videos/pinning/PinningTask;->debug:Z

    move-object v2, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/pinning/LegacyDownloader;-><init>(Lcom/google/android/videos/VideosGlobals;Lcom/google/android/videos/pinning/Downloader$ProgressListener;Lcom/google/android/videos/pinning/DownloadKey;Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/io/File;Lcom/google/android/videos/async/TaskStatus;Z)V

    goto :goto_2
.end method

.method protected onCompleted()V
    .locals 2

    .prologue
    .line 132
    iget-object v1, p0, Lcom/google/android/videos/pinning/PinningTask;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget-object v0, p0, Lcom/google/android/videos/pinning/PinningTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v0, v0, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/EventLogger;->onPinningCompleted(Ljava/lang/String;)V

    .line 133
    return-void
.end method

.method public onDownloadProgress()V
    .locals 0

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/google/android/videos/pinning/PinningTask;->notifyProgress()V

    .line 166
    return-void
.end method

.method protected onError(Ljava/lang/Throwable;ZZ)V
    .locals 10
    .param p1, "exception"    # Ljava/lang/Throwable;
    .param p2, "exceededMaxRetries"    # Z
    .param p3, "fatal"    # Z

    .prologue
    .line 137
    const/16 v7, 0x12

    .line 138
    .local v7, "failedReason":I
    const/4 v6, -0x1

    .line 139
    .local v6, "drmErrorCode":I
    move-object v3, p1

    .line 140
    .local v3, "cause":Ljava/lang/Throwable;
    instance-of v0, p1, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    if-eqz v0, :cond_0

    move-object v8, p1

    .line 141
    check-cast v8, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    .line 142
    .local v8, "pinningException":Lcom/google/android/videos/pinning/PinningTask$PinningException;
    iget v7, v8, Lcom/google/android/videos/pinning/PinningTask$PinningException;->failedReason:I

    .line 143
    iget-object v0, v8, Lcom/google/android/videos/pinning/PinningTask$PinningException;->drmErrorCode:Ljava/lang/Integer;

    if-nez v0, :cond_4

    const/4 v6, -0x1

    .line 145
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    .line 147
    .end local v8    # "pinningException":Lcom/google/android/videos/pinning/PinningTask$PinningException;
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/pinning/PinningTask;->downloader:Lcom/google/android/videos/pinning/Downloader;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/videos/pinning/PinningTask;->downloader:Lcom/google/android/videos/pinning/Downloader;

    invoke-virtual {v0}, Lcom/google/android/videos/pinning/Downloader;->getMediaStream()Lcom/google/android/videos/streams/MediaStream;

    move-result-object v0

    if-nez v0, :cond_5

    :cond_1
    const/4 v2, 0x0

    .line 150
    .local v2, "itag":Ljava/lang/Integer;
    :goto_1
    iget-object v0, p0, Lcom/google/android/videos/pinning/PinningTask;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget-object v1, p0, Lcom/google/android/videos/pinning/PinningTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v1, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v1, v1, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    move v4, p3

    move v5, p2

    invoke-interface/range {v0 .. v7}, Lcom/google/android/videos/logging/EventLogger;->onPinningError(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Throwable;ZZII)V

    .line 152
    if-nez p3, :cond_2

    if-eqz p2, :cond_3

    .line 153
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "transfer fatal fail "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/pinning/PinningTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 154
    invoke-static {}, Lcom/google/android/videos/pinning/PinningDbHelper;->getClearDownloadContentValues()Landroid/content/ContentValues;

    move-result-object v9

    .line 155
    .local v9, "values":Landroid/content/ContentValues;
    const-string v0, "pinning_status"

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 156
    const-string v0, "pinning_status_reason"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 157
    const-string v0, "pinning_drm_error_code"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 158
    const-string v0, "pinned"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 159
    iget-object v1, p0, Lcom/google/android/videos/pinning/PinningTask;->database:Lcom/google/android/videos/store/Database;

    iget-object v0, p0, Lcom/google/android/videos/pinning/PinningTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/videos/pinning/DownloadKey;

    invoke-static {v1, v0, v9}, Lcom/google/android/videos/pinning/PinningDbHelper;->updatePinningStateForVideo(Lcom/google/android/videos/store/Database;Lcom/google/android/videos/pinning/DownloadKey;Landroid/content/ContentValues;)V

    .line 161
    .end local v9    # "values":Landroid/content/ContentValues;
    :cond_3
    return-void

    .line 143
    .end local v2    # "itag":Ljava/lang/Integer;
    .restart local v8    # "pinningException":Lcom/google/android/videos/pinning/PinningTask$PinningException;
    :cond_4
    iget-object v0, v8, Lcom/google/android/videos/pinning/PinningTask$PinningException;->drmErrorCode:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    goto/16 :goto_0

    .line 147
    .end local v8    # "pinningException":Lcom/google/android/videos/pinning/PinningTask$PinningException;
    :cond_5
    iget-object v0, p0, Lcom/google/android/videos/pinning/PinningTask;->downloader:Lcom/google/android/videos/pinning/Downloader;

    invoke-virtual {v0}, Lcom/google/android/videos/pinning/Downloader;->getMediaStream()Lcom/google/android/videos/streams/MediaStream;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget v0, v0, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_1
.end method
