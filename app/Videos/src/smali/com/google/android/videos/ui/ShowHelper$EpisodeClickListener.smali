.class final Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;
.super Ljava/lang/Object;
.source "ShowHelper.java"

# interfaces
.implements Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/ShowHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "EpisodeClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/ui/ShowHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/ui/ShowHelper;)V
    .locals 0

    .prologue
    .line 643
    iput-object p1, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/ui/ShowHelper;Lcom/google/android/videos/ui/ShowHelper$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/ui/ShowHelper;
    .param p2, "x1"    # Lcom/google/android/videos/ui/ShowHelper$1;

    .prologue
    .line 643
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;-><init>(Lcom/google/android/videos/ui/ShowHelper;)V

    return-void
.end method

.method private onPinClicked(Ljava/lang/String;Landroid/database/Cursor;)V
    .locals 8
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 688
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;
    invoke-static {v0}, Lcom/google/android/videos/ui/ShowHelper;->access$1500(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->isPinned(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 689
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->pinHelper:Lcom/google/android/videos/ui/PinHelper;
    invoke-static {v0}, Lcom/google/android/videos/ui/ShowHelper;->access$1900(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/ui/PinHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->activity:Lcom/google/android/videos/activity/ShowActivity;
    invoke-static {v1}, Lcom/google/android/videos/ui/ShowHelper;->access$1200(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/activity/ShowActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->account:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/videos/ui/ShowHelper;->access$1400(Lcom/google/android/videos/ui/ShowHelper;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;
    invoke-static {v3}, Lcom/google/android/videos/ui/ShowHelper;->access$1500(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    move-result-object v3

    invoke-virtual {v3, p2}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->getTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    iget-object v3, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;
    invoke-static {v3}, Lcom/google/android/videos/ui/ShowHelper;->access$1500(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    move-result-object v3

    invoke-virtual {v3, p2}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->getShowTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v5

    iget-object v3, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;
    invoke-static {v3}, Lcom/google/android/videos/ui/ShowHelper;->access$1500(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    move-result-object v3

    invoke-virtual {v3, p2}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->getPinningStatus(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v3, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;
    invoke-static {v3}, Lcom/google/android/videos/ui/ShowHelper;->access$1500(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    move-result-object v3

    invoke-virtual {v3, p2}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->getPinningStatusReason(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v7

    move-object v3, p1

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/videos/ui/PinHelper;->onUnpinClicked(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Integer;)V

    .line 701
    :goto_0
    return-void

    .line 694
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;
    invoke-static {v0}, Lcom/google/android/videos/ui/ShowHelper;->access$1500(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->getPinningStatus(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 696
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # invokes: Lcom/google/android/videos/ui/ShowHelper;->showPinningErrorDialog(Landroid/database/Cursor;)V
    invoke-static {v0, p2}, Lcom/google/android/videos/ui/ShowHelper;->access$2000(Lcom/google/android/videos/ui/ShowHelper;Landroid/database/Cursor;)V

    goto :goto_0

    .line 698
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->pinHelper:Lcom/google/android/videos/ui/PinHelper;
    invoke-static {v0}, Lcom/google/android/videos/ui/ShowHelper;->access$1900(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/ui/PinHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->activity:Lcom/google/android/videos/activity/ShowActivity;
    invoke-static {v1}, Lcom/google/android/videos/ui/ShowHelper;->access$1200(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/activity/ShowActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->account:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/videos/ui/ShowHelper;->access$1400(Lcom/google/android/videos/ui/ShowHelper;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, Lcom/google/android/videos/ui/PinHelper;->pinVideo(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 699
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->eventLogger:Lcom/google/android/videos/logging/EventLogger;
    invoke-static {v0}, Lcom/google/android/videos/ui/ShowHelper;->access$2100(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/logging/EventLogger;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/videos/logging/EventLogger;->onPinClick(Z)V

    goto :goto_0
.end method


# virtual methods
.method public onItemClick(Lcom/google/android/videos/adapter/DataSource;ILandroid/view/View;)V
    .locals 14
    .param p2, "position"    # I
    .param p3, "view"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/adapter/DataSource",
            "<*>;I",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 647
    .local p1, "dataSource":Lcom/google/android/videos/adapter/DataSource;, "Lcom/google/android/videos/adapter/DataSource<*>;"
    invoke-interface/range {p1 .. p2}, Lcom/google/android/videos/adapter/DataSource;->getItem(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/database/Cursor;

    .line 648
    .local v10, "cursor":Landroid/database/Cursor;
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;
    invoke-static {v0}, Lcom/google/android/videos/ui/ShowHelper;->access$1500(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    move-result-object v0

    invoke-virtual {v0, v10}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    .line 649
    .local v2, "videoId":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;
    invoke-static {v0}, Lcom/google/android/videos/ui/ShowHelper;->access$1500(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    move-result-object v0

    invoke-virtual {v0, v10}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->getSeasonId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    .line 651
    .local v3, "seasonId":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/view/View;->getId()I

    move-result v13

    .line 652
    .local v13, "viewId":I
    packed-switch v13, :pswitch_data_0

    .line 673
    const v0, 0x7f0f00d6

    if-eq v13, v0, :cond_0

    .line 674
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;
    invoke-static {v0}, Lcom/google/android/videos/ui/ShowHelper;->access$1800(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/logging/UiEventLoggingHelper;

    move-result-object v0

    invoke-static/range {p3 .. p3}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->findUiElementNode(Landroid/view/View;)Lcom/google/android/videos/logging/UiElementNode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->sendClickEvent(Lcom/google/android/videos/logging/UiElementNode;)V

    .line 676
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;
    invoke-static {v0}, Lcom/google/android/videos/ui/ShowHelper;->access$1500(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    move-result-object v0

    invoke-virtual {v0, v10}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->isPurchased(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 677
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->activity:Lcom/google/android/videos/activity/ShowActivity;
    invoke-static {v0}, Lcom/google/android/videos/ui/ShowHelper;->access$1200(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/activity/ShowActivity;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->activity:Lcom/google/android/videos/activity/ShowActivity;
    invoke-static {v0}, Lcom/google/android/videos/ui/ShowHelper;->access$1200(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/activity/ShowActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->account:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/videos/ui/ShowHelper;->access$1400(Lcom/google/android/videos/ui/ShowHelper;)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->showId:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/videos/ui/ShowHelper;->access$1300(Lcom/google/android/videos/ui/ShowHelper;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/activity/WatchActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/google/android/videos/activity/ShowActivity;->startActivity(Landroid/content/Intent;)V

    .line 685
    :goto_0
    return-void

    .line 654
    :pswitch_0
    invoke-direct {p0, v2, v10}, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->onPinClicked(Ljava/lang/String;Landroid/database/Cursor;)V

    goto :goto_0

    .line 658
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->unpurchasedEpisodesHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;
    invoke-static {v0}, Lcom/google/android/videos/ui/ShowHelper;->access$1600(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->getAssetResource(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v11

    .line 660
    .local v11, "episodeResource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    if-eqz v11, :cond_1

    iget-object v0, v11, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    array-length v0, v0

    if-eqz v0, :cond_1

    const/4 v12, 0x1

    .line 661
    .local v12, "hasOffers":Z
    :goto_1
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;
    invoke-static {v0}, Lcom/google/android/videos/ui/ShowHelper;->access$1700(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->showId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/videos/ui/ShowHelper;->access$1300(Lcom/google/android/videos/ui/ShowHelper;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1, v12}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->startFlowForEpisode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 660
    .end local v12    # "hasOffers":Z
    :cond_1
    const/4 v12, 0x0

    goto :goto_1

    .line 665
    .end local v11    # "episodeResource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->activity:Lcom/google/android/videos/activity/ShowActivity;
    invoke-static {v0}, Lcom/google/android/videos/ui/ShowHelper;->access$1200(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/activity/ShowActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;
    invoke-static {v1}, Lcom/google/android/videos/ui/ShowHelper;->access$1500(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    move-result-object v1

    invoke-virtual {v1, v10}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->getEpisodeNumber(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;
    invoke-static {v4}, Lcom/google/android/videos/ui/ShowHelper;->access$1500(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    move-result-object v4

    invoke-virtual {v4, v10}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->getTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;
    invoke-static {v5}, Lcom/google/android/videos/ui/ShowHelper;->access$1500(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    move-result-object v5

    invoke-virtual {v5, v10}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->getDescription(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v1, v4, v5}, Lcom/google/android/videos/activity/ShowActivity$EpisodeSynopsisDialogFragment;->showInstance(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 680
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->activity:Lcom/google/android/videos/activity/ShowActivity;
    invoke-static {v0}, Lcom/google/android/videos/ui/ShowHelper;->access$1200(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/activity/ShowActivity;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->showId:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/videos/ui/ShowHelper;->access$1300(Lcom/google/android/videos/ui/ShowHelper;)Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->account:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/videos/ui/ShowHelper;->access$1400(Lcom/google/android/videos/ui/ShowHelper;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x4

    move-object v6, v3

    move-object v7, v2

    invoke-static/range {v4 .. v9}, Lcom/google/android/videos/utils/PlayStoreUtil;->viewEpisodeDetails(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 652
    nop

    :pswitch_data_0
    .packed-switch 0x7f0f00d0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
