.class public Lcom/google/android/videos/onboard/VideosOnboardHostFragment;
.super Lcom/google/android/play/onboard/OnboardHostFragment;
.source "VideosOnboardHostFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;-><init>()V

    return-void
.end method

.method private showsVerticalEnabled()Z
    .locals 3

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/google/android/videos/onboard/VideosOnboardHostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v1

    .line 76
    .local v1, "videosGlobals":Lcom/google/android/videos/VideosGlobals;
    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v0

    .line 77
    .local v0, "account":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/videos/store/ConfigurationStore;->showsVerticalEnabled(Ljava/lang/String;)Z

    move-result v2

    return v2
.end method


# virtual methods
.method public finishOnboardFlow()V
    .locals 3

    .prologue
    .line 68
    invoke-super {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;->finishOnboardFlow()V

    .line 69
    invoke-virtual {p0}, Lcom/google/android/videos/onboard/VideosOnboardHostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "onboard_tutorial_shown"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 71
    invoke-virtual {p0}, Lcom/google/android/videos/onboard/VideosOnboardHostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/onboard/OnboardHostActivity;

    invoke-virtual {v0}, Lcom/google/android/videos/onboard/OnboardHostActivity;->onFinishedOnboardFlow()V

    .line 72
    return-void
.end method

.method public getAppColor()I
    .locals 2

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/google/android/videos/onboard/VideosOnboardHostFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0079

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method public getPageList()Lcom/google/android/libraries/bind/data/DataList;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x3

    .line 33
    invoke-direct {p0}, Lcom/google/android/videos/onboard/VideosOnboardHostFragment;->showsVerticalEnabled()Z

    move-result v1

    .line 34
    .local v1, "showsLaunched":Z
    invoke-static {}, Lcom/google/android/play/utils/collections/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 35
    .local v0, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    new-instance v2, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    invoke-virtual {p0}, Lcom/google/android/videos/onboard/VideosOnboardHostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3, v6, v5}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;-><init>(Landroid/content/Context;II)V

    invoke-virtual {p0}, Lcom/google/android/videos/onboard/VideosOnboardHostFragment;->getAppColor()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setBackgroundColor(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v3

    if-eqz v1, :cond_0

    const v2, 0x7f0b00fe

    :goto_0
    invoke-virtual {v3, v2}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setTitleText(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v3

    if-eqz v1, :cond_1

    const v2, 0x7f0b00ff

    :goto_1
    invoke-virtual {v3, v2}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setBodyText(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v2

    const v3, 0x7f0201fa

    invoke-virtual {v2, v3}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setIconDrawableId(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v2

    new-instance v3, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;

    invoke-virtual {p0}, Lcom/google/android/videos/onboard/VideosOnboardHostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v3, v4, v6, v5}, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;-><init>(Landroid/content/Context;II)V

    invoke-virtual {v2, v3}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setPageInfo(Lcom/google/android/play/onboard/OnboardPageInfo;)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->build()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    new-instance v2, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    invoke-virtual {p0}, Lcom/google/android/videos/onboard/VideosOnboardHostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3, v7, v5}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;-><init>(Landroid/content/Context;II)V

    invoke-virtual {p0}, Lcom/google/android/videos/onboard/VideosOnboardHostFragment;->getAppColor()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setBackgroundColor(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v2

    const v3, 0x7f0b0102

    invoke-virtual {v2, v3}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setTitleText(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v2

    const v3, 0x7f0b0103

    invoke-virtual {v2, v3}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setBodyText(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v2

    const v3, 0x7f0201fb

    invoke-virtual {v2, v3}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setIconDrawableId(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v2

    new-instance v3, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;

    invoke-virtual {p0}, Lcom/google/android/videos/onboard/VideosOnboardHostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v3, v4, v7, v5}, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;-><init>(Landroid/content/Context;II)V

    invoke-virtual {v2, v3}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setPageInfo(Lcom/google/android/play/onboard/OnboardPageInfo;)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->build()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    new-instance v2, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    invoke-virtual {p0}, Lcom/google/android/videos/onboard/VideosOnboardHostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3, v8, v5}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;-><init>(Landroid/content/Context;II)V

    invoke-virtual {p0}, Lcom/google/android/videos/onboard/VideosOnboardHostFragment;->getAppColor()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setBackgroundColor(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v2

    const v3, 0x7f0b0104

    invoke-virtual {v2, v3}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setTitleText(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v2

    const v3, 0x7f0b0105

    invoke-virtual {v2, v3}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setBodyText(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v2

    const v3, 0x7f0201fc

    invoke-virtual {v2, v3}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setIconDrawableId(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v2

    new-instance v3, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;

    invoke-virtual {p0}, Lcom/google/android/videos/onboard/VideosOnboardHostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v3, v4, v8, v5}, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;-><init>(Landroid/content/Context;II)V

    invoke-virtual {v2, v3}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setPageInfo(Lcom/google/android/play/onboard/OnboardPageInfo;)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->build()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    new-instance v2, Lcom/google/android/libraries/bind/data/DataList;

    sget v3, Lcom/google/android/play/onboard/OnboardPage;->DK_PAGE_ID:I

    invoke-direct {v2, v3, v0}, Lcom/google/android/libraries/bind/data/DataList;-><init>(ILjava/util/List;)V

    return-object v2

    .line 35
    :cond_0
    const v2, 0x7f0b0100

    goto/16 :goto_0

    :cond_1
    const v2, 0x7f0b0101

    goto/16 :goto_1
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 26
    invoke-super {p0, p1, p2}, Lcom/google/android/play/onboard/OnboardHostFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 27
    invoke-virtual {p0}, Lcom/google/android/videos/onboard/VideosOnboardHostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04007b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/onboard/VideosOnboardHostFragment;->setBackgroundView(Landroid/view/View;)V

    .line 29
    return-void
.end method
