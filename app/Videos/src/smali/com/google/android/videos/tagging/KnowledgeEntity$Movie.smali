.class public final Lcom/google/android/videos/tagging/KnowledgeEntity$Movie;
.super Lcom/google/android/videos/tagging/KnowledgeEntity$Film;
.source "KnowledgeEntity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/KnowledgeEntity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Movie"
.end annotation


# instance fields
.field public final runningTime:I

.field public final videoId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "releaseDate"    # Ljava/lang/String;
    .param p3, "runningTime"    # I
    .param p4, "googlePlayUrl"    # Ljava/lang/String;
    .param p5, "image"    # Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
    .param p6, "videoId"    # Ljava/lang/String;

    .prologue
    .line 202
    invoke-direct {p0, p1, p2, p4, p5}, Lcom/google/android/videos/tagging/KnowledgeEntity$Film;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/tagging/KnowledgeEntity$Image;)V

    .line 203
    iput p3, p0, Lcom/google/android/videos/tagging/KnowledgeEntity$Movie;->runningTime:I

    .line 204
    iput-object p6, p0, Lcom/google/android/videos/tagging/KnowledgeEntity$Movie;->videoId:Ljava/lang/String;

    .line 205
    return-void
.end method
