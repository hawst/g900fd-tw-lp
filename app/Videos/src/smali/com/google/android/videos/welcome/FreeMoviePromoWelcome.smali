.class public Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;
.super Lcom/google/android/videos/welcome/PromoWelcome;
.source "FreeMoviePromoWelcome.java"

# interfaces
.implements Lcom/google/android/videos/store/StoreStatusMonitor$Listener;
.implements Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$Listener;
.implements Lcom/google/android/videos/welcome/FreeMoviePromoRequester$Listener;


# static fields
.field private static final ACTION_RES_IDS:[I


# instance fields
.field private account:Ljava/lang/String;

.field private final config:Lcom/google/android/videos/Config;

.field private final context:Landroid/content/Context;

.field private final errorHelper:Lcom/google/android/videos/utils/ErrorHelper;

.field private isEligible:Z

.field private listeningToStore:Z

.field private final ownsPurchaseStoreMonitor:Z

.field private promotionAsset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

.field private promotionResource:Lcom/google/wireless/android/video/magma/proto/PromotionResource;

.field private final purchaseStoreMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

.field private final redeemer:Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;

.field private final requester:Lcom/google/android/videos/welcome/FreeMoviePromoRequester;

.field private final showWithContent:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->ACTION_RES_IDS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f020072
        0x7f0b00dd
        0x7f0200d7
        0x7f0b00de
    .end array-data
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/videos/Config;Landroid/content/SharedPreferences;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/utils/ErrorHelper;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/PurchaseStoreSync;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Z)V
    .locals 17
    .param p1, "verticalId"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "config"    # Lcom/google/android/videos/Config;
    .param p4, "preferences"    # Landroid/content/SharedPreferences;
    .param p5, "configStore"    # Lcom/google/android/videos/store/ConfigurationStore;
    .param p6, "errorHelper"    # Lcom/google/android/videos/utils/ErrorHelper;
    .param p7, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;
    .param p8, "database"    # Lcom/google/android/videos/store/Database;
    .param p9, "purchaseStore"    # Lcom/google/android/videos/store/PurchaseStore;
    .param p10, "purchaseStoreSync"    # Lcom/google/android/videos/store/PurchaseStoreSync;
    .param p13, "showWithContent"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            "Lcom/google/android/videos/Config;",
            "Landroid/content/SharedPreferences;",
            "Lcom/google/android/videos/store/ConfigurationStore;",
            "Lcom/google/android/videos/utils/ErrorHelper;",
            "Lcom/google/android/videos/logging/EventLogger;",
            "Lcom/google/android/videos/store/Database;",
            "Lcom/google/android/videos/store/PurchaseStore;",
            "Lcom/google/android/videos/store/PurchaseStoreSync;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/PromotionsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;",
            ">;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/RedeemPromotionRequest;",
            "Ljava/lang/Void;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 73
    .local p11, "promotionsRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/PromotionsRequest;Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;>;"
    .local p12, "redeemPromotionRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/RedeemPromotionRequest;Ljava/lang/Void;>;"
    new-instance v15, Lcom/google/android/videos/store/StoreStatusMonitor;

    const/4 v3, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, p8

    move-object/from16 v2, p9

    invoke-direct {v15, v0, v1, v2, v3}, Lcom/google/android/videos/store/StoreStatusMonitor;-><init>(Landroid/content/Context;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/WishlistStore;)V

    const/16 v16, 0x1

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move/from16 v14, p13

    invoke-direct/range {v3 .. v16}, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/videos/Config;Landroid/content/SharedPreferences;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/utils/ErrorHelper;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/store/PurchaseStoreSync;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;ZLcom/google/android/videos/store/StoreStatusMonitor;Z)V

    .line 87
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/videos/Config;Landroid/content/SharedPreferences;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/utils/ErrorHelper;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/store/PurchaseStoreSync;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;ZLcom/google/android/videos/store/StoreStatusMonitor;Z)V
    .locals 12
    .param p1, "verticalId"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "config"    # Lcom/google/android/videos/Config;
    .param p4, "preferences"    # Landroid/content/SharedPreferences;
    .param p5, "configStore"    # Lcom/google/android/videos/store/ConfigurationStore;
    .param p6, "errorHelper"    # Lcom/google/android/videos/utils/ErrorHelper;
    .param p7, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;
    .param p8, "purchaseStoreSync"    # Lcom/google/android/videos/store/PurchaseStoreSync;
    .param p11, "showWithContent"    # Z
    .param p12, "purchaseStoreMonitor"    # Lcom/google/android/videos/store/StoreStatusMonitor;
    .param p13, "ownsPurchaseStoreMonitor"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            "Lcom/google/android/videos/Config;",
            "Landroid/content/SharedPreferences;",
            "Lcom/google/android/videos/store/ConfigurationStore;",
            "Lcom/google/android/videos/utils/ErrorHelper;",
            "Lcom/google/android/videos/logging/EventLogger;",
            "Lcom/google/android/videos/store/PurchaseStoreSync;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/PromotionsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;",
            ">;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/RedeemPromotionRequest;",
            "Ljava/lang/Void;",
            ">;Z",
            "Lcom/google/android/videos/store/StoreStatusMonitor;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 129
    .local p9, "promotionsRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/PromotionsRequest;Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;>;"
    .local p10, "redeemPromotionRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/RedeemPromotionRequest;Ljava/lang/Void;>;"
    const-string v1, "freemovie"

    move-object/from16 v0, p4

    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/videos/welcome/PromoWelcome;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/SharedPreferences;)V

    .line 130
    new-instance v5, Landroid/os/Handler;

    invoke-virtual {p2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v5, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 131
    .local v5, "handler":Landroid/os/Handler;
    new-instance v1, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;

    move-object/from16 v2, p7

    move-object/from16 v3, p8

    move-object/from16 v4, p10

    move-object v6, p0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;-><init>(Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/store/PurchaseStoreSync;Lcom/google/android/videos/async/Requester;Landroid/os/Handler;Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$Listener;)V

    iput-object v1, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->redeemer:Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;

    .line 133
    new-instance v6, Lcom/google/android/videos/welcome/FreeMoviePromoRequester;

    move-object/from16 v7, p9

    move-object/from16 v8, p5

    move-object/from16 v9, p12

    move-object v10, p0

    move-object v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/google/android/videos/welcome/FreeMoviePromoRequester;-><init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/store/StoreStatusMonitor;Lcom/google/android/videos/welcome/FreeMoviePromoRequester$Listener;Landroid/os/Handler;)V

    iput-object v6, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->requester:Lcom/google/android/videos/welcome/FreeMoviePromoRequester;

    .line 135
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->context:Landroid/content/Context;

    .line 136
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/Config;

    iput-object v1, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->config:Lcom/google/android/videos/Config;

    .line 137
    invoke-static/range {p6 .. p6}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/utils/ErrorHelper;

    iput-object v1, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->errorHelper:Lcom/google/android/videos/utils/ErrorHelper;

    .line 138
    move/from16 v0, p11

    iput-boolean v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->showWithContent:Z

    .line 139
    invoke-static/range {p12 .. p12}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/store/StoreStatusMonitor;

    iput-object v1, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->purchaseStoreMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    .line 140
    move/from16 v0, p13

    iput-boolean v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->ownsPurchaseStoreMonitor:Z

    .line 141
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/videos/Config;Landroid/content/SharedPreferences;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/utils/ErrorHelper;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/store/StoreStatusMonitor;Lcom/google/android/videos/store/PurchaseStoreSync;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Z)V
    .locals 14
    .param p1, "verticalId"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "config"    # Lcom/google/android/videos/Config;
    .param p4, "preferences"    # Landroid/content/SharedPreferences;
    .param p5, "configStore"    # Lcom/google/android/videos/store/ConfigurationStore;
    .param p6, "errorHelper"    # Lcom/google/android/videos/utils/ErrorHelper;
    .param p7, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;
    .param p8, "purchaseStoreMonitor"    # Lcom/google/android/videos/store/StoreStatusMonitor;
    .param p9, "purchaseStoreSync"    # Lcom/google/android/videos/store/PurchaseStoreSync;
    .param p12, "showWithContent"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            "Lcom/google/android/videos/Config;",
            "Landroid/content/SharedPreferences;",
            "Lcom/google/android/videos/store/ConfigurationStore;",
            "Lcom/google/android/videos/utils/ErrorHelper;",
            "Lcom/google/android/videos/logging/EventLogger;",
            "Lcom/google/android/videos/store/StoreStatusMonitor;",
            "Lcom/google/android/videos/store/PurchaseStoreSync;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/PromotionsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;",
            ">;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/RedeemPromotionRequest;",
            "Ljava/lang/Void;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 102
    .local p10, "promotionsRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/PromotionsRequest;Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;>;"
    .local p11, "redeemPromotionRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/RedeemPromotionRequest;Ljava/lang/Void;>;"
    const/4 v13, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p9

    move-object/from16 v9, p10

    move-object/from16 v10, p11

    move/from16 v11, p12

    move-object/from16 v12, p8

    invoke-direct/range {v0 .. v13}, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/videos/Config;Landroid/content/SharedPreferences;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/utils/ErrorHelper;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/store/PurchaseStoreSync;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;ZLcom/google/android/videos/store/StoreStatusMonitor;Z)V

    .line 116
    return-void
.end method

.method private maybeStartListeningToStore()V
    .locals 2

    .prologue
    .line 183
    iget-boolean v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->listeningToStore:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->account:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 191
    :cond_0
    :goto_0
    return-void

    .line 186
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->ownsPurchaseStoreMonitor:Z

    if-eqz v0, :cond_2

    .line 187
    iget-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->purchaseStoreMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    iget-object v1, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->account:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/store/StoreStatusMonitor;->init(Ljava/lang/String;)V

    .line 189
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->purchaseStoreMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/store/StoreStatusMonitor;->addListener(Lcom/google/android/videos/store/StoreStatusMonitor$Listener;)V

    .line 190
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->listeningToStore:Z

    goto :goto_0
.end method

.method private maybeStopListeningToStore()V
    .locals 1

    .prologue
    .line 194
    iget-boolean v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->listeningToStore:Z

    if-nez v0, :cond_0

    .line 203
    :goto_0
    return-void

    .line 197
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->ownsPurchaseStoreMonitor:Z

    if-eqz v0, :cond_1

    .line 198
    iget-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->purchaseStoreMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    invoke-virtual {v0}, Lcom/google/android/videos/store/StoreStatusMonitor;->reset()V

    .line 202
    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->listeningToStore:Z

    goto :goto_0

    .line 200
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->purchaseStoreMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/store/StoreStatusMonitor;->removeListener(Lcom/google/android/videos/store/StoreStatusMonitor$Listener;)V

    goto :goto_1
.end method


# virtual methods
.method public getActionResIds()[I
    .locals 1

    .prologue
    .line 242
    sget-object v0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->ACTION_RES_IDS:[I

    return-object v0
.end method

.method public getDefaultBitmapResId()I
    .locals 1

    .prologue
    .line 228
    const/4 v0, 0x0

    return v0
.end method

.method public getDetailMessage()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->context:Landroid/content/Context;

    const v1, 0x7f0b00dc

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->promotionAsset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v4, v4, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v4, v4, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    return-object v0
.end method

.method public getLayoutResourceId()I
    .locals 1

    .prologue
    .line 157
    const v0, 0x7f0400d1

    return v0
.end method

.method public getNetworkBitmapUri()Landroid/net/Uri;
    .locals 7

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e017b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 234
    .local v2, "posterWidth":I
    int-to-float v0, v2

    const v1, 0x3f31a787

    div-float/2addr v0, v1

    float-to-int v3, v0

    .line 235
    .local v3, "posterHeight":I
    iget-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->promotionAsset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    const/4 v1, 0x3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/api/AssetResourceUtil;->findBestImageUrl(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;IIIFZ)Ljava/lang/String;

    move-result-object v6

    .line 237
    .local v6, "url":Ljava/lang/String;
    if-eqz v6, :cond_0

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->context:Landroid/content/Context;

    const v1, 0x7f0b00db

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->promotionAsset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v4, v4, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v4, v4, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    return-object v0
.end method

.method public onAction(I)V
    .locals 6
    .param p1, "index"    # I

    .prologue
    .line 247
    if-nez p1, :cond_1

    .line 248
    iget-boolean v1, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->isEligible:Z

    if-eqz v1, :cond_0

    .line 249
    iget-object v1, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->promotionResource:Lcom/google/wireless/android/video/magma/proto/PromotionResource;

    iget-object v1, v1, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->asset:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iget-object v0, v1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 250
    .local v0, "resourceId":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    iget-object v1, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->redeemer:Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;

    iget-object v2, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->account:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    iget-object v5, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->promotionResource:Lcom/google/wireless/android/video/magma/proto/PromotionResource;

    iget-object v5, v5, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->promotionCode:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;->redeem(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 255
    .end local v0    # "resourceId":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    :cond_0
    :goto_0
    return-void

    .line 253
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->markDismissed()V

    goto :goto_0
.end method

.method public onNoPromo()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 281
    iput-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->promotionResource:Lcom/google/wireless/android/video/magma/proto/PromotionResource;

    .line 282
    iput-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->promotionAsset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 283
    invoke-virtual {p0}, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->updateEligibility()V

    .line 284
    return-void
.end method

.method public onPromo(Lcom/google/wireless/android/video/magma/proto/PromotionResource;)V
    .locals 2
    .param p1, "promotion"    # Lcom/google/wireless/android/video/magma/proto/PromotionResource;

    .prologue
    .line 274
    iput-object p1, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->promotionResource:Lcom/google/wireless/android/video/magma/proto/PromotionResource;

    .line 275
    iget-object v0, p1, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->asset:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->promotionAsset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 276
    invoke-virtual {p0}, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->updateEligibility()V

    .line 277
    return-void
.end method

.method public onPromoRedeemError(Ljava/lang/Exception;)V
    .locals 1
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->errorHelper:Lcom/google/android/videos/utils/ErrorHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/utils/ErrorHelper;->showToast(Ljava/lang/Throwable;)V

    .line 270
    return-void
.end method

.method public onPromoRedeemed()V
    .locals 0

    .prologue
    .line 265
    return-void
.end method

.method public onPromoRequestError(Ljava/lang/Exception;)V
    .locals 1
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 288
    const-string v0, "Couldn\'t fetch promotions"

    invoke-static {v0, p1}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 289
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 145
    invoke-super {p0}, Lcom/google/android/videos/welcome/PromoWelcome;->onStart()V

    .line 146
    invoke-direct {p0}, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->maybeStartListeningToStore()V

    .line 147
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 151
    invoke-direct {p0}, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->maybeStopListeningToStore()V

    .line 152
    invoke-super {p0}, Lcom/google/android/videos/welcome/PromoWelcome;->onStop()V

    .line 153
    return-void
.end method

.method public onStoreStatusChanged(Lcom/google/android/videos/store/StoreStatusMonitor;)V
    .locals 0
    .param p1, "sender"    # Lcom/google/android/videos/store/StoreStatusMonitor;

    .prologue
    .line 259
    invoke-virtual {p0}, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->updateEligibility()V

    .line 260
    return-void
.end method

.method public preparePromoIfEligible(Ljava/lang/String;ZZ)Z
    .locals 2
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "contentInVertical"    # Z
    .param p3, "downloadedOnly"    # Z

    .prologue
    const/4 v0, 0x0

    .line 163
    iget-object v1, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->config:Lcom/google/android/videos/Config;

    invoke-interface {v1}, Lcom/google/android/videos/Config;->freeMovieWelcomeEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez p1, :cond_1

    .line 179
    :cond_0
    :goto_0
    return v0

    .line 166
    :cond_1
    if-eqz p2, :cond_2

    iget-boolean v1, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->showWithContent:Z

    if-eqz v1, :cond_0

    .line 169
    :cond_2
    if-nez p3, :cond_0

    .line 172
    iget-object v1, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->account:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 173
    iput-object p1, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->account:Ljava/lang/String;

    .line 174
    iput-boolean v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->isEligible:Z

    .line 175
    invoke-direct {p0}, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->maybeStopListeningToStore()V

    .line 176
    invoke-direct {p0}, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->maybeStartListeningToStore()V

    .line 177
    iget-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->requester:Lcom/google/android/videos/welcome/FreeMoviePromoRequester;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/welcome/FreeMoviePromoRequester;->requestPromotion(Ljava/lang/String;)V

    .line 179
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->isEligible:Z

    goto :goto_0
.end method

.method protected updateEligibility()V
    .locals 4

    .prologue
    .line 206
    iget-object v1, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->promotionAsset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->purchaseStoreMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    iget-object v2, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->promotionAsset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v2, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v2, v2, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    const/4 v3, 0x6

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/store/StoreStatusMonitor;->getStatus(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v1}, Lcom/google/android/videos/store/StoreStatusMonitor;->isPurchased(I)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    .line 208
    .local v0, "hasPromo":Z
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->isEligible:Z

    if-eq v1, v0, :cond_0

    .line 209
    iput-boolean v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->isEligible:Z

    .line 210
    invoke-virtual {p0}, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->notifyEligibilityChanged()V

    .line 212
    :cond_0
    return-void

    .line 206
    .end local v0    # "hasPromo":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
