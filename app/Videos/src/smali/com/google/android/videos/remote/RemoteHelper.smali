.class public Lcom/google/android/videos/remote/RemoteHelper;
.super Ljava/lang/Object;
.source "RemoteHelper.java"


# instance fields
.field private final mediaRouteManager:Lcom/google/android/videos/remote/MediaRouteManager;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/remote/MediaRouteManager;)V
    .locals 1
    .param p1, "mediaRouteManager"    # Lcom/google/android/videos/remote/MediaRouteManager;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/remote/MediaRouteManager;

    iput-object v0, p0, Lcom/google/android/videos/remote/RemoteHelper;->mediaRouteManager:Lcom/google/android/videos/remote/MediaRouteManager;

    .line 28
    return-void
.end method

.method private getCurrentRoute()Landroid/support/v7/media/MediaRouter$RouteInfo;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteHelper;->mediaRouteManager:Lcom/google/android/videos/remote/MediaRouteManager;

    invoke-virtual {v0}, Lcom/google/android/videos/remote/MediaRouteManager;->getCurrentRouteInfo()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 31
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemoteHelper;->getCurrentRoute()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    .line 32
    .local v0, "routeInfo":Landroid/support/v7/media/MediaRouter$RouteInfo;
    sget v2, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v3, 0x12

    if-ge v2, v3, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->isDefault()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getVolumeHandling()I

    move-result v2

    if-ne v2, v1, :cond_1

    .line 36
    const/16 v2, 0x19

    if-ne p1, v2, :cond_0

    .line 37
    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->requestUpdateVolume(I)V

    .line 44
    :goto_0
    return v1

    .line 39
    :cond_0
    const/16 v2, 0x18

    if-ne p1, v2, :cond_1

    .line 40
    invoke-virtual {v0, v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->requestUpdateVolume(I)V

    goto :goto_0

    .line 44
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 48
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemoteHelper;->getCurrentRoute()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    .line 49
    .local v0, "routeInfo":Landroid/support/v7/media/MediaRouter$RouteInfo;
    sget v2, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v3, 0x12

    if-ge v2, v3, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->isDefault()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getVolumeHandling()I

    move-result v2

    if-ne v2, v1, :cond_0

    const/16 v2, 0x19

    if-eq p1, v2, :cond_1

    :cond_0
    const/16 v2, 0x18

    if-ne p1, v2, :cond_2

    :cond_1
    :goto_0
    return v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
