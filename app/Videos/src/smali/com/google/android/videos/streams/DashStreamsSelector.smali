.class public Lcom/google/android/videos/streams/DashStreamsSelector;
.super Ljava/lang/Object;
.source "DashStreamsSelector.java"


# instance fields
.field private final config:Lcom/google/android/videos/Config;

.field private final eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private final networkStatus:Lcom/google/android/videos/utils/NetworkStatus;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/Config;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/utils/NetworkStatus;)V
    .locals 1
    .param p1, "config"    # Lcom/google/android/videos/Config;
    .param p2, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;
    .param p3, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/Config;

    iput-object v0, p0, Lcom/google/android/videos/streams/DashStreamsSelector;->config:Lcom/google/android/videos/Config;

    .line 32
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/utils/NetworkStatus;

    iput-object v0, p0, Lcom/google/android/videos/streams/DashStreamsSelector;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 33
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/logging/EventLogger;

    iput-object v0, p0, Lcom/google/android/videos/streams/DashStreamsSelector;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 34
    return-void
.end method

.method private getAudioStreams(Ljava/util/List;ZLjava/util/List;)Ljava/util/List;
    .locals 8
    .param p2, "surroundSound"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/streams/MissingStreamException;
        }
    .end annotation

    .prologue
    .line 161
    .local p1, "streams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    .local p3, "preferredFormats":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 162
    .local v1, "audioStreams":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/streams/MediaStream;>;"
    const/4 v2, 0x0

    .line 163
    .local v2, "haveTracksWithAudioInfo":Z
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v7

    if-ge v3, v7, :cond_5

    .line 164
    invoke-interface {p3, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 165
    .local v4, "itag":I
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    if-ge v5, v7, :cond_4

    .line 166
    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/videos/streams/MediaStream;

    .line 167
    .local v6, "stream":Lcom/google/android/videos/streams/MediaStream;
    iget-object v7, v6, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget v7, v7, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    if-ne v7, v4, :cond_2

    iget-object v7, v6, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    invoke-virtual {v7}, Lcom/google/android/videos/streams/ItagInfo;->isSurroundSound()Z

    move-result v7

    if-eqz v7, :cond_0

    if-eqz p2, :cond_2

    iget-object v7, p0, Lcom/google/android/videos/streams/DashStreamsSelector;->config:Lcom/google/android/videos/Config;

    invoke-interface {v7}, Lcom/google/android/videos/Config;->allowSurroundSoundFormats()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 169
    :cond_0
    iget-object v7, v6, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-object v0, v7, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    .line 170
    .local v0, "audioInfo":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    if-eqz v0, :cond_1

    if-nez v2, :cond_1

    .line 173
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 174
    const/4 v2, 0x1

    .line 176
    :cond_1
    if-nez v2, :cond_3

    .line 178
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 179
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 165
    .end local v0    # "audioInfo":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    :cond_2
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 181
    .restart local v0    # "audioInfo":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    :cond_3
    if-eqz v0, :cond_2

    .line 183
    const/4 v7, 0x1

    invoke-static {v1, v0, v7}, Lcom/google/android/videos/utils/AudioInfoUtil;->containsStreamWithInfo(Ljava/util/List;Lcom/google/wireless/android/video/magma/proto/AudioInfo;Z)Z

    move-result v7

    if-nez v7, :cond_2

    .line 186
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 163
    .end local v0    # "audioInfo":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    .end local v6    # "stream":Lcom/google/android/videos/streams/MediaStream;
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 192
    .end local v4    # "itag":I
    .end local v5    # "j":I
    :cond_5
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 193
    new-instance v7, Lcom/google/android/videos/streams/MissingStreamException;

    invoke-direct {v7}, Lcom/google/android/videos/streams/MissingStreamException;-><init>()V

    throw v7

    .line 196
    :cond_6
    return-object v1
.end method


# virtual methods
.method public getOfflineAudioStreams(Ljava/util/List;Z)Ljava/util/List;
    .locals 1
    .param p2, "surroundSound"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;Z)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/streams/MissingStreamException;
        }
    .end annotation

    .prologue
    .line 149
    .local p1, "streams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    iget-object v0, p0, Lcom/google/android/videos/streams/DashStreamsSelector;->config:Lcom/google/android/videos/Config;

    invoke-interface {v0}, Lcom/google/android/videos/Config;->orderedDashHqAudioFormats()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/videos/streams/DashStreamsSelector;->getAudioStreams(Ljava/util/List;ZLjava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getOfflineVideoStream(Ljava/util/List;IZ)Lcom/google/android/videos/streams/MediaStream;
    .locals 10
    .param p2, "preferredQuality"    # I
    .param p3, "filterHdContent"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;IZ)",
            "Lcom/google/android/videos/streams/MediaStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/streams/MissingStreamException;
        }
    .end annotation

    .prologue
    .line 41
    .local p1, "streams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    iget-object v9, p0, Lcom/google/android/videos/streams/DashStreamsSelector;->config:Lcom/google/android/videos/Config;

    invoke-interface {v9}, Lcom/google/android/videos/Config;->exoOfflineVideoHeightCap()I

    move-result v5

    .line 42
    .local v5, "maxVideoHeight":I
    if-eqz p3, :cond_0

    .line 43
    const/16 v9, 0x2cf

    invoke-static {v5, v9}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 46
    :cond_0
    const/4 v0, 0x0

    .line 47
    .local v0, "firstPermitted":Lcom/google/android/videos/streams/MediaStream;
    iget-object v9, p0, Lcom/google/android/videos/streams/DashStreamsSelector;->config:Lcom/google/android/videos/Config;

    invoke-interface {v9}, Lcom/google/android/videos/Config;->orderedDashDownloadFormats()Ljava/util/List;

    move-result-object v6

    .line 48
    .local v6, "preferredVideoFormats":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v8

    .line 49
    .local v8, "streamsSize":I
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v1

    .line 50
    .local v1, "formatsSize":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_4

    .line 51
    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 52
    .local v3, "itag":I
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    if-ge v4, v8, :cond_3

    .line 53
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/videos/streams/MediaStream;

    .line 54
    .local v7, "stream":Lcom/google/android/videos/streams/MediaStream;
    iget-object v9, v7, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget v9, v9, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    if-ne v9, v3, :cond_2

    iget-object v9, v7, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    iget v9, v9, Lcom/google/android/videos/streams/ItagInfo;->height:I

    if-gt v9, v5, :cond_2

    .line 55
    iget-object v9, v7, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    invoke-virtual {v9}, Lcom/google/android/videos/streams/ItagInfo;->getQuality()I

    move-result v9

    if-ne v9, p2, :cond_1

    .line 68
    .end local v3    # "itag":I
    .end local v4    # "j":I
    .end local v7    # "stream":Lcom/google/android/videos/streams/MediaStream;
    :goto_2
    return-object v7

    .line 57
    .restart local v3    # "itag":I
    .restart local v4    # "j":I
    .restart local v7    # "stream":Lcom/google/android/videos/streams/MediaStream;
    :cond_1
    if-nez v0, :cond_2

    .line 58
    move-object v0, v7

    .line 52
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 50
    .end local v7    # "stream":Lcom/google/android/videos/streams/MediaStream;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 65
    .end local v3    # "itag":I
    .end local v4    # "j":I
    :cond_4
    if-nez v0, :cond_5

    .line 66
    new-instance v9, Lcom/google/android/videos/streams/MissingStreamException;

    invoke-direct {v9}, Lcom/google/android/videos/streams/MissingStreamException;-><init>()V

    throw v9

    :cond_5
    move-object v7, v0

    .line 68
    goto :goto_2
.end method

.method public getOnlineAudioStreams(Ljava/util/List;Z)Ljava/util/List;
    .locals 2
    .param p2, "surroundSound"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;Z)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/streams/MissingStreamException;
        }
    .end annotation

    .prologue
    .line 154
    .local p1, "streams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    iget-object v1, p0, Lcom/google/android/videos/streams/DashStreamsSelector;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v1}, Lcom/google/android/videos/utils/NetworkStatus;->isFastNetwork()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/streams/DashStreamsSelector;->config:Lcom/google/android/videos/Config;

    invoke-interface {v1}, Lcom/google/android/videos/Config;->orderedDashHqAudioFormats()Ljava/util/List;

    move-result-object v0

    .line 156
    .local v0, "preferredFormats":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :goto_0
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/videos/streams/DashStreamsSelector;->getAudioStreams(Ljava/util/List;ZLjava/util/List;)Ljava/util/List;

    move-result-object v1

    return-object v1

    .line 154
    .end local v0    # "preferredFormats":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/streams/DashStreamsSelector;->config:Lcom/google/android/videos/Config;

    invoke-interface {v1}, Lcom/google/android/videos/Config;->orderedDashMqAudioFormats()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public getOnlineVideoStreams(Ljava/util/List;ZZLandroid/view/Display;)Lcom/google/android/videos/streams/DashVideoStreamSelection;
    .locals 16
    .param p2, "filterHdContent"    # Z
    .param p3, "adaptive"    # Z
    .param p4, "display"    # Landroid/view/Display;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;ZZ",
            "Landroid/view/Display;",
            ")",
            "Lcom/google/android/videos/streams/DashVideoStreamSelection;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/streams/MissingStreamException;
        }
    .end annotation

    .prologue
    .line 73
    .local p1, "streams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/streams/DashStreamsSelector;->config:Lcom/google/android/videos/Config;

    move-object/from16 v0, p4

    invoke-interface {v14, v0}, Lcom/google/android/videos/Config;->exoOnlineVideoHeightCap(Landroid/view/Display;)I

    move-result v8

    .line 74
    .local v8, "maxVideoHeight":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/streams/DashStreamsSelector;->config:Lcom/google/android/videos/Config;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/streams/DashStreamsSelector;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v15}, Lcom/google/android/videos/utils/NetworkStatus;->isFastNetwork()Z

    move-result v15

    move-object/from16 v0, p4

    invoke-interface {v14, v0, v15}, Lcom/google/android/videos/Config;->exoLqVideoHeightCap(Landroid/view/Display;Z)I

    move-result v6

    .line 75
    .local v6, "maxLqVideoHeight":I
    if-eqz p2, :cond_0

    .line 76
    const/16 v14, 0x2cf

    invoke-static {v8, v14}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 77
    const/16 v14, 0x2cf

    invoke-static {v6, v14}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 80
    :cond_0
    const/4 v7, 0x0

    .line 81
    .local v7, "maxSelectedVideoHeight":I
    const v9, 0x7fffffff

    .line 82
    .local v9, "minSelectedVideoHeight":I
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 83
    .local v13, "videoStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    const/4 v5, 0x0

    .line 85
    .local v5, "lqVideoStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v14

    if-ge v3, v14, :cond_4

    .line 86
    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/videos/streams/MediaStream;

    .line 87
    .local v10, "stream":Lcom/google/android/videos/streams/MediaStream;
    iget-object v14, v10, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    iget v2, v14, Lcom/google/android/videos/streams/ItagInfo;->height:I

    .line 88
    .local v2, "height":I
    if-lez v2, :cond_1

    if-gt v2, v8, :cond_1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/streams/DashStreamsSelector;->config:Lcom/google/android/videos/Config;

    invoke-interface {v14}, Lcom/google/android/videos/Config;->dashVideoFormats()Ljava/util/Set;

    move-result-object v14

    iget-object v15, v10, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget v15, v15, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-interface {v14, v15}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/streams/DashStreamsSelector;->config:Lcom/google/android/videos/Config;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/streams/DashStreamsSelector;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    invoke-static {v14, v15, v10}, Lcom/google/android/videos/utils/Util;->deviceSupportsStreamResolution(Lcom/google/android/videos/Config;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/streams/MediaStream;)Z

    move-result v14

    if-nez v14, :cond_3

    :cond_1
    const/4 v1, 0x1

    .line 91
    .local v1, "filterStream":Z
    :goto_1
    if-nez v1, :cond_2

    .line 92
    invoke-interface {v13, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    invoke-static {v7, v2}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 94
    invoke-static {v9, v2}, Ljava/lang/Math;->min(II)I

    move-result v9

    .line 85
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 88
    .end local v1    # "filterStream":Z
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 98
    .end local v2    # "height":I
    .end local v10    # "stream":Lcom/google/android/videos/streams/MediaStream;
    :cond_4
    if-eqz p3, :cond_7

    .line 101
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v14

    add-int/lit8 v3, v14, -0x1

    :goto_2
    if-ltz v3, :cond_d

    .line 102
    invoke-interface {v13, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/videos/streams/MediaStream;

    .line 104
    .local v12, "videoStream":Lcom/google/android/videos/streams/MediaStream;
    iget-object v14, v12, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    iget v14, v14, Lcom/google/android/videos/streams/ItagInfo;->height:I

    if-eq v14, v7, :cond_6

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/streams/DashStreamsSelector;->config:Lcom/google/android/videos/Config;

    invoke-interface {v14}, Lcom/google/android/videos/Config;->dashVideoHighEdgeFormats()Ljava/util/Set;

    move-result-object v14

    iget-object v15, v12, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget v15, v15, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-interface {v14, v15}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 106
    invoke-interface {v13, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 101
    :cond_5
    :goto_3
    add-int/lit8 v3, v3, -0x1

    goto :goto_2

    .line 107
    :cond_6
    iget-object v14, v12, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    iget v14, v14, Lcom/google/android/videos/streams/ItagInfo;->height:I

    if-eq v14, v9, :cond_5

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/streams/DashStreamsSelector;->config:Lcom/google/android/videos/Config;

    invoke-interface {v14}, Lcom/google/android/videos/Config;->dashVideoLowEdgeFormats()Ljava/util/Set;

    move-result-object v14

    iget-object v15, v12, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget v15, v15, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-interface {v14, v15}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 109
    invoke-interface {v13, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_3

    .line 115
    .end local v12    # "videoStream":Lcom/google/android/videos/streams/MediaStream;
    :cond_7
    const/4 v4, 0x0

    .line 116
    .local v4, "lqVideoHeight":I
    const/4 v3, 0x0

    :goto_4
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v14

    if-ge v3, v14, :cond_9

    .line 117
    invoke-interface {v13, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/videos/streams/MediaStream;

    iget-object v14, v14, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    iget v2, v14, Lcom/google/android/videos/streams/ItagInfo;->height:I

    .line 118
    .restart local v2    # "height":I
    if-gt v2, v6, :cond_8

    if-ge v2, v7, :cond_8

    .line 119
    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 116
    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 123
    .end local v2    # "height":I
    :cond_9
    if-eqz v4, :cond_a

    .line 124
    new-instance v5, Ljava/util/ArrayList;

    .end local v5    # "lqVideoStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 126
    .restart local v5    # "lqVideoStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    :cond_a
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v14

    add-int/lit8 v3, v14, -0x1

    :goto_5
    if-ltz v3, :cond_d

    .line 127
    invoke-interface {v13, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/videos/streams/MediaStream;

    .line 128
    .local v11, "videoRepresentation":Lcom/google/android/videos/streams/MediaStream;
    iget-object v14, v11, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    iget v14, v14, Lcom/google/android/videos/streams/ItagInfo;->height:I

    if-eq v14, v7, :cond_c

    const/4 v1, 0x1

    .line 129
    .restart local v1    # "filterStream":Z
    :goto_6
    if-eqz v1, :cond_b

    .line 130
    invoke-interface {v13, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 131
    if-eqz v4, :cond_b

    iget-object v14, v11, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    iget v14, v14, Lcom/google/android/videos/streams/ItagInfo;->height:I

    if-ne v14, v4, :cond_b

    .line 132
    invoke-interface {v5, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    :cond_b
    add-int/lit8 v3, v3, -0x1

    goto :goto_5

    .line 128
    .end local v1    # "filterStream":Z
    :cond_c
    const/4 v1, 0x0

    goto :goto_6

    .line 138
    .end local v4    # "lqVideoHeight":I
    .end local v11    # "videoRepresentation":Lcom/google/android/videos/streams/MediaStream;
    :cond_d
    invoke-interface {v13}, Ljava/util/List;->isEmpty()Z

    move-result v14

    if-eqz v14, :cond_e

    .line 139
    new-instance v14, Lcom/google/android/videos/streams/MissingStreamException;

    invoke-direct {v14}, Lcom/google/android/videos/streams/MissingStreamException;-><init>()V

    throw v14

    .line 142
    :cond_e
    new-instance v14, Lcom/google/android/videos/streams/DashVideoStreamSelection;

    invoke-direct {v14, v13, v5}, Lcom/google/android/videos/streams/DashVideoStreamSelection;-><init>(Ljava/util/List;Ljava/util/List;)V

    return-object v14
.end method
