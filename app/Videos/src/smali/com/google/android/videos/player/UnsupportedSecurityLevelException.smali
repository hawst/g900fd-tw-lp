.class public Lcom/google/android/videos/player/UnsupportedSecurityLevelException;
.super Ljava/lang/Exception;
.source "UnsupportedSecurityLevelException.java"


# instance fields
.field public final actualLevel:I

.field public final offline:Z

.field public final requestedLevel:I


# direct methods
.method public constructor <init>(IIZ)V
    .locals 2
    .param p1, "requestedLevel"    # I
    .param p2, "actualLevel"    # I
    .param p3, "offline"    # Z

    .prologue
    .line 15
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unsupported security level: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 16
    iput p1, p0, Lcom/google/android/videos/player/UnsupportedSecurityLevelException;->requestedLevel:I

    .line 17
    iput p2, p0, Lcom/google/android/videos/player/UnsupportedSecurityLevelException;->actualLevel:I

    .line 18
    iput-boolean p3, p0, Lcom/google/android/videos/player/UnsupportedSecurityLevelException;->offline:Z

    .line 19
    return-void
.end method
