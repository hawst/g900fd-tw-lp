.class Lcom/google/android/videos/store/PurchaseStoreSync$PurgePurchasesTask;
.super Lcom/google/android/videos/store/SyncTaskManager$SyncTask;
.source "PurchaseStoreSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/PurchaseStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PurgePurchasesTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/store/PurchaseStoreSync;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/store/PurchaseStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V
    .locals 1
    .param p2, "priority"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/store/SyncTaskManager$SharedStates",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 897
    .local p3, "states":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<*>;"
    iput-object p1, p0, Lcom/google/android/videos/store/PurchaseStoreSync$PurgePurchasesTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    .line 898
    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->syncTaskManager:Lcom/google/android/videos/store/SyncTaskManager;
    invoke-static {p1}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$300(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/SyncTaskManager;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;-><init>(Lcom/google/android/videos/store/SyncTaskManager;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V

    .line 899
    return-void
.end method


# virtual methods
.method protected doSync()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 903
    iget-object v2, p0, Lcom/google/android/videos/store/PurchaseStoreSync$PurgePurchasesTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v2}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$100(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 904
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, 0x0

    .line 906
    .local v1, "success":Z
    :try_start_0
    const-string v2, "purchased_assets"

    const-string v3, "purchase_status = -1 AND NOT (pinned IS NOT NULL AND pinned > 0) AND NOT (license_type IS NOT NULL AND license_type != 0)"

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 908
    iget-object v2, p0, Lcom/google/android/videos/store/PurchaseStoreSync$PurgePurchasesTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v2}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$100(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v1, v5, v3}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 910
    return-void

    .line 908
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/google/android/videos/store/PurchaseStoreSync$PurgePurchasesTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v3}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$100(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v0, v1, v5, v4}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v2
.end method
