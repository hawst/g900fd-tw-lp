.class public Lcom/google/android/videos/api/VideoCollectionGetRequest;
.super Lcom/google/android/videos/async/Request;
.source "VideoCollectionGetRequest.java"


# instance fields
.field public final categoryRestriction:Ljava/lang/String;

.field public final collectionId:Ljava/lang/String;

.field public final countryRestriction:Ljava/lang/String;

.field public final localeRestriction:Ljava/lang/String;

.field public final maxResults:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;Ljava/lang/String;I)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "collectionId"    # Ljava/lang/String;
    .param p3, "countryRestriction"    # Ljava/lang/String;
    .param p4, "localeRestriction"    # Ljava/util/Locale;
    .param p5, "categoryRestriction"    # Ljava/lang/String;
    .param p6, "maxResults"    # I

    .prologue
    .line 23
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/async/Request;-><init>(Ljava/lang/String;Z)V

    .line 24
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/api/VideoCollectionGetRequest;->collectionId:Ljava/lang/String;

    .line 25
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/api/VideoCollectionGetRequest;->countryRestriction:Ljava/lang/String;

    .line 26
    invoke-static {p4}, Lcom/google/android/videos/utils/LocaleUtils;->toString(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/api/VideoCollectionGetRequest;->localeRestriction:Ljava/lang/String;

    .line 27
    iput-object p5, p0, Lcom/google/android/videos/api/VideoCollectionGetRequest;->categoryRestriction:Ljava/lang/String;

    .line 29
    iput p6, p0, Lcom/google/android/videos/api/VideoCollectionGetRequest;->maxResults:I

    .line 30
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 34
    if-ne p0, p1, :cond_1

    .line 39
    :cond_0
    :goto_0
    return v1

    .line 35
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 37
    check-cast v0, Lcom/google/android/videos/api/VideoCollectionGetRequest;

    .line 39
    .local v0, "that":Lcom/google/android/videos/api/VideoCollectionGetRequest;
    iget v3, p0, Lcom/google/android/videos/api/VideoCollectionGetRequest;->maxResults:I

    iget v4, v0, Lcom/google/android/videos/api/VideoCollectionGetRequest;->maxResults:I

    if-ne v3, v4, :cond_4

    iget-object v3, p0, Lcom/google/android/videos/api/VideoCollectionGetRequest;->account:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/api/VideoCollectionGetRequest;->account:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/videos/api/VideoCollectionGetRequest;->collectionId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/api/VideoCollectionGetRequest;->collectionId:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/videos/api/VideoCollectionGetRequest;->countryRestriction:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/api/VideoCollectionGetRequest;->countryRestriction:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/videos/api/VideoCollectionGetRequest;->localeRestriction:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/api/VideoCollectionGetRequest;->localeRestriction:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/videos/api/VideoCollectionGetRequest;->categoryRestriction:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/api/VideoCollectionGetRequest;->categoryRestriction:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 49
    iget-object v2, p0, Lcom/google/android/videos/api/VideoCollectionGetRequest;->account:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/videos/api/VideoCollectionGetRequest;->account:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 50
    .local v0, "result":I
    :goto_0
    mul-int/lit8 v3, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/api/VideoCollectionGetRequest;->collectionId:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/videos/api/VideoCollectionGetRequest;->collectionId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 51
    mul-int/lit8 v3, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/api/VideoCollectionGetRequest;->countryRestriction:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/videos/api/VideoCollectionGetRequest;->countryRestriction:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 52
    mul-int/lit8 v3, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/api/VideoCollectionGetRequest;->localeRestriction:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/videos/api/VideoCollectionGetRequest;->localeRestriction:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 53
    mul-int/lit8 v2, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/videos/api/VideoCollectionGetRequest;->categoryRestriction:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/api/VideoCollectionGetRequest;->categoryRestriction:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 54
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/videos/api/VideoCollectionGetRequest;->maxResults:I

    add-int v0, v1, v2

    .line 55
    return v0

    .end local v0    # "result":I
    :cond_1
    move v0, v1

    .line 49
    goto :goto_0

    .restart local v0    # "result":I
    :cond_2
    move v2, v1

    .line 50
    goto :goto_1

    :cond_3
    move v2, v1

    .line 51
    goto :goto_2

    :cond_4
    move v2, v1

    .line 52
    goto :goto_3
.end method
