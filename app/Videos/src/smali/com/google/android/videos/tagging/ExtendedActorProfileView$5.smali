.class Lcom/google/android/videos/tagging/ExtendedActorProfileView$5;
.super Lcom/google/android/videos/ui/TransitionUtil$TransitionListenerAdapter;
.source "ExtendedActorProfileView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/tagging/ExtendedActorProfileView;->startHideTransitionStepTwo(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;


# direct methods
.method constructor <init>(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)V
    .locals 0

    .prologue
    .line 338
    iput-object p1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$5;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    invoke-direct {p0}, Lcom/google/android/videos/ui/TransitionUtil$TransitionListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onTransitionEnd(Landroid/transition/Transition;)V
    .locals 3
    .param p1, "transition"    # Landroid/transition/Transition;

    .prologue
    const/4 v2, 0x0

    .line 341
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$5;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->transitionProfileImageView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$000(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    .line 342
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$5;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->transitionProfileImageView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$000(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setHasTransientState(Z)V

    .line 343
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$5;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # setter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->transitionProfileImageView:Landroid/view/View;
    invoke-static {v0, v2}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$002(Lcom/google/android/videos/tagging/ExtendedActorProfileView;Landroid/view/View;)Landroid/view/View;

    .line 344
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$5;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # setter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->viewRoot:Landroid/view/ViewGroup;
    invoke-static {v0, v2}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$502(Lcom/google/android/videos/tagging/ExtendedActorProfileView;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;

    .line 345
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$5;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # setter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->entitiesViewGroup:Landroid/view/ViewGroup;
    invoke-static {v0, v2}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$602(Lcom/google/android/videos/tagging/ExtendedActorProfileView;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;

    .line 346
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$5;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # invokes: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->hideInternal()V
    invoke-static {v0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$700(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)V

    .line 347
    return-void
.end method
