.class Lcom/google/android/videos/drm/DrmManager$3;
.super Ljava/lang/Object;
.source "DrmManager.java"

# interfaces
.implements Lcom/google/android/videos/async/Requester;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/drm/DrmManager;->createNonAuthenticatingRequester(Ljava/util/concurrent/Executor;)Lcom/google/android/videos/async/Requester;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Requester",
        "<",
        "Lcom/google/android/videos/drm/DrmRequest;",
        "Lcom/google/android/videos/drm/DrmResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/drm/DrmManager;

.field final synthetic val$drmExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method constructor <init>(Lcom/google/android/videos/drm/DrmManager;Ljava/util/concurrent/Executor;)V
    .locals 0

    .prologue
    .line 290
    iput-object p1, p0, Lcom/google/android/videos/drm/DrmManager$3;->this$0:Lcom/google/android/videos/drm/DrmManager;

    iput-object p2, p0, Lcom/google/android/videos/drm/DrmManager$3;->val$drmExecutor:Ljava/util/concurrent/Executor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public request(Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/async/Callback;)V
    .locals 3
    .param p1, "request"    # Lcom/google/android/videos/drm/DrmRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/drm/DrmRequest;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/drm/DrmRequest;",
            "Lcom/google/android/videos/drm/DrmResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 293
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/drm/DrmResponse;>;"
    iget-object v0, p0, Lcom/google/android/videos/drm/DrmManager$3;->val$drmExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/videos/drm/DrmManager$DrmRequestRunnable;

    iget-object v2, p0, Lcom/google/android/videos/drm/DrmManager$3;->this$0:Lcom/google/android/videos/drm/DrmManager;

    invoke-direct {v1, v2, p1, p2}, Lcom/google/android/videos/drm/DrmManager$DrmRequestRunnable;-><init>(Lcom/google/android/videos/drm/DrmManager;Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/async/Callback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 294
    return-void
.end method

.method public bridge synthetic request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/google/android/videos/async/Callback;

    .prologue
    .line 290
    check-cast p1, Lcom/google/android/videos/drm/DrmRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/drm/DrmManager$3;->request(Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/async/Callback;)V

    return-void
.end method
