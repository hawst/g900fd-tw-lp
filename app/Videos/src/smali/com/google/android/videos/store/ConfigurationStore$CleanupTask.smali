.class Lcom/google/android/videos/store/ConfigurationStore$CleanupTask;
.super Ljava/lang/Object;
.source "ConfigurationStore.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/ConfigurationStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CleanupTask"
.end annotation


# instance fields
.field private final callback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<-",
            "Ljava/lang/Integer;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final request:I

.field final synthetic this$0:Lcom/google/android/videos/store/ConfigurationStore;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/store/ConfigurationStore;ILcom/google/android/videos/async/Callback;)V
    .locals 1
    .param p2, "request"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/async/Callback",
            "<-",
            "Ljava/lang/Integer;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 328
    .local p3, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<-Ljava/lang/Integer;Ljava/lang/Void;>;"
    iput-object p1, p0, Lcom/google/android/videos/store/ConfigurationStore$CleanupTask;->this$0:Lcom/google/android/videos/store/ConfigurationStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 329
    iput p2, p0, Lcom/google/android/videos/store/ConfigurationStore$CleanupTask;->request:I

    .line 330
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Callback;

    iput-object v0, p0, Lcom/google/android/videos/store/ConfigurationStore$CleanupTask;->callback:Lcom/google/android/videos/async/Callback;

    .line 331
    return-void
.end method


# virtual methods
.method public run()V
    .locals 21

    .prologue
    .line 336
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/ConfigurationStore$CleanupTask;->this$0:Lcom/google/android/videos/store/ConfigurationStore;

    # getter for: Lcom/google/android/videos/store/ConfigurationStore;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v2}, Lcom/google/android/videos/store/ConfigurationStore;->access$100(Lcom/google/android/videos/store/ConfigurationStore;)Lcom/google/android/videos/store/Database;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v20

    .line 337
    .local v20, "transaction":Landroid/database/sqlite/SQLiteDatabase;
    const/16 v19, 0x0

    .line 338
    .local v19, "success":Z
    const/4 v14, 0x0

    .line 341
    .local v14, "exception":Landroid/database/sqlite/SQLiteException;
    const/4 v2, 0x0

    :try_start_0
    const-string v3, "user_configuration"

    # getter for: Lcom/google/android/videos/store/ConfigurationStore;->ACCOUNT_COLUMN:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/videos/store/ConfigurationStore;->access$700()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-string v6, "config_account"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQueryString(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 343
    .local v18, "sql":Ljava/lang/String;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 344
    .local v11, "accounts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v2, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v12

    .line 346
    .local v12, "cursor":Landroid/database/Cursor;
    :goto_0
    :try_start_1
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 347
    const/4 v2, 0x0

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v11, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 350
    :catchall_0
    move-exception v2

    :try_start_2
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    throw v2
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 365
    .end local v11    # "accounts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "cursor":Landroid/database/Cursor;
    .end local v18    # "sql":Ljava/lang/String;
    :catch_0
    move-exception v13

    .line 366
    .local v13, "e":Landroid/database/sqlite/SQLiteException;
    move-object v14, v13

    .line 368
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/ConfigurationStore$CleanupTask;->this$0:Lcom/google/android/videos/store/ConfigurationStore;

    # getter for: Lcom/google/android/videos/store/ConfigurationStore;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v2}, Lcom/google/android/videos/store/ConfigurationStore;->access$100(Lcom/google/android/videos/store/ConfigurationStore;)Lcom/google/android/videos/store/Database;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    move-object/from16 v0, v20

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1, v3, v4}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 370
    .end local v13    # "e":Landroid/database/sqlite/SQLiteException;
    :goto_1
    if-eqz v19, :cond_3

    .line 371
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/ConfigurationStore$CleanupTask;->callback:Lcom/google/android/videos/async/Callback;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/videos/store/ConfigurationStore$CleanupTask;->request:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 375
    :goto_2
    return-void

    .line 350
    .restart local v11    # "accounts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "cursor":Landroid/database/Cursor;
    .restart local v18    # "sql":Ljava/lang/String;
    :cond_0
    :try_start_3
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 353
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/ConfigurationStore$CleanupTask;->this$0:Lcom/google/android/videos/store/ConfigurationStore;

    # getter for: Lcom/google/android/videos/store/ConfigurationStore;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;
    invoke-static {v2}, Lcom/google/android/videos/store/ConfigurationStore;->access$800(Lcom/google/android/videos/store/ConfigurationStore;)Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getAccounts()[Landroid/accounts/Account;

    move-result-object v17

    .line 354
    .local v17, "knownAccounts":[Landroid/accounts/Account;
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_3
    move-object/from16 v0, v17

    array-length v2, v0

    if-ge v15, v2, :cond_1

    .line 355
    aget-object v2, v17, v15

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v11, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 354
    add-int/lit8 v15, v15, 0x1

    goto :goto_3

    .line 358
    :cond_1
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 359
    .local v10, "account":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/ConfigurationStore$CleanupTask;->this$0:Lcom/google/android/videos/store/ConfigurationStore;

    # getter for: Lcom/google/android/videos/store/ConfigurationStore;->userCountryCodes:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v2}, Lcom/google/android/videos/store/ConfigurationStore;->access$200(Lcom/google/android/videos/store/ConfigurationStore;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 360
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/ConfigurationStore$CleanupTask;->this$0:Lcom/google/android/videos/store/ConfigurationStore;

    # getter for: Lcom/google/android/videos/store/ConfigurationStore;->userAccountLinks:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v2}, Lcom/google/android/videos/store/ConfigurationStore;->access$300(Lcom/google/android/videos/store/ConfigurationStore;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 361
    const-string v2, "user_configuration"

    const-string v3, "config_account = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v10, v4, v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_4

    .line 368
    .end local v10    # "account":Ljava/lang/String;
    .end local v11    # "accounts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "cursor":Landroid/database/Cursor;
    .end local v15    # "i":I
    .end local v16    # "i$":Ljava/util/Iterator;
    .end local v17    # "knownAccounts":[Landroid/accounts/Account;
    .end local v18    # "sql":Ljava/lang/String;
    :catchall_1
    move-exception v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/store/ConfigurationStore$CleanupTask;->this$0:Lcom/google/android/videos/store/ConfigurationStore;

    # getter for: Lcom/google/android/videos/store/ConfigurationStore;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v3}, Lcom/google/android/videos/store/ConfigurationStore;->access$100(Lcom/google/android/videos/store/ConfigurationStore;)Lcom/google/android/videos/store/Database;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    move-object/from16 v0, v20

    move/from16 v1, v19

    invoke-virtual {v3, v0, v1, v4, v5}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v2

    .line 364
    .restart local v11    # "accounts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "cursor":Landroid/database/Cursor;
    .restart local v15    # "i":I
    .restart local v16    # "i$":Ljava/util/Iterator;
    .restart local v17    # "knownAccounts":[Landroid/accounts/Account;
    .restart local v18    # "sql":Ljava/lang/String;
    :cond_2
    const/16 v19, 0x1

    .line 368
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/ConfigurationStore$CleanupTask;->this$0:Lcom/google/android/videos/store/ConfigurationStore;

    # getter for: Lcom/google/android/videos/store/ConfigurationStore;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v2}, Lcom/google/android/videos/store/ConfigurationStore;->access$100(Lcom/google/android/videos/store/ConfigurationStore;)Lcom/google/android/videos/store/Database;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    move-object/from16 v0, v20

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1, v3, v4}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 373
    .end local v11    # "accounts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "cursor":Landroid/database/Cursor;
    .end local v15    # "i":I
    .end local v16    # "i$":Ljava/util/Iterator;
    .end local v17    # "knownAccounts":[Landroid/accounts/Account;
    .end local v18    # "sql":Ljava/lang/String;
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/ConfigurationStore$CleanupTask;->callback:Lcom/google/android/videos/async/Callback;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/videos/store/ConfigurationStore$CleanupTask;->request:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3, v14}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto/16 :goto_2
.end method
