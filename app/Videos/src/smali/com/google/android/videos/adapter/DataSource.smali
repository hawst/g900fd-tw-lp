.class public interface abstract Lcom/google/android/videos/adapter/DataSource;
.super Ljava/lang/Object;
.source "DataSource.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract getCount()I
.end method

.method public abstract getItem(I)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IndexOutOfBoundsException;
        }
    .end annotation
.end method

.method public abstract getItemIdentifier(I)Ljava/lang/Object;
.end method

.method public abstract notifyChanged()V
.end method

.method public abstract registerObserver(Landroid/database/DataSetObserver;)V
.end method

.method public abstract unregisterObserver(Landroid/database/DataSetObserver;)V
.end method
