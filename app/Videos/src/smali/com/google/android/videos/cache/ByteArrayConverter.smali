.class public Lcom/google/android/videos/cache/ByteArrayConverter;
.super Ljava/lang/Object;
.source "ByteArrayConverter.java"

# interfaces
.implements Lcom/google/android/videos/cache/Converter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/cache/Converter",
        "<",
        "Lcom/google/android/videos/utils/ByteArray;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public readElement(Ljava/io/InputStream;J)Lcom/google/android/videos/utils/ByteArray;
    .locals 2
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "length"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23
    invoke-static {p1, p2, p3}, Lcom/google/android/videos/cache/ConverterUtils;->readBytes(Ljava/io/InputStream;J)Lcom/google/android/videos/utils/ByteArray;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic readElement(Ljava/io/InputStream;J)Ljava/lang/Object;
    .locals 2
    .param p1, "x0"    # Ljava/io/InputStream;
    .param p2, "x1"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/videos/cache/ByteArrayConverter;->readElement(Ljava/io/InputStream;J)Lcom/google/android/videos/utils/ByteArray;

    move-result-object v0

    return-object v0
.end method

.method public writeElement(Lcom/google/android/videos/utils/ByteArray;Ljava/io/OutputStream;)V
    .locals 3
    .param p1, "element"    # Lcom/google/android/videos/utils/ByteArray;
    .param p2, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18
    iget-object v0, p1, Lcom/google/android/videos/utils/ByteArray;->data:[B

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/android/videos/utils/ByteArray;->limit()I

    move-result v2

    invoke-virtual {p2, v0, v1, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 19
    return-void
.end method

.method public bridge synthetic writeElement(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14
    check-cast p1, Lcom/google/android/videos/utils/ByteArray;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/cache/ByteArrayConverter;->writeElement(Lcom/google/android/videos/utils/ByteArray;Ljava/io/OutputStream;)V

    return-void
.end method
