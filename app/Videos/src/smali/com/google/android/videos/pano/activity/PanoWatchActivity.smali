.class public Lcom/google/android/videos/pano/activity/PanoWatchActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "PanoWatchActivity.java"

# interfaces
.implements Lcom/google/android/videos/player/Director$Listener;


# instance fields
.field private director:Lcom/google/android/videos/player/Director;

.field private directorInitialized:Z

.field private isTrailer:Z

.field private playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

.field private videoIsPlaying:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method

.method public static createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "seasonId"    # Ljava/lang/String;
    .param p4, "showId"    # Ljava/lang/String;
    .param p5, "isTrailer"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 51
    if-nez p5, :cond_0

    if-eqz p1, :cond_4

    :cond_0
    move v1, v3

    :goto_0
    invoke-static {v1}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 52
    if-nez p4, :cond_1

    if-nez p3, :cond_2

    :cond_1
    move v2, v3

    :cond_2
    const-string v1, "ShowId cannot be null when seasonId is not null"

    invoke-static {v2, v1}, Lcom/google/android/videos/utils/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 54
    new-instance v2, Landroid/content/Intent;

    const-class v1, Lcom/google/android/videos/pano/activity/PanoWatchActivity;

    invoke-direct {v2, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "video_id"

    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "season_id"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "show_id"

    invoke-virtual {v1, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "is_trailer"

    invoke-virtual {v1, v2, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 59
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 60
    const-string v1, "authAccount"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 62
    :cond_3
    return-object v0

    .end local v0    # "intent":Landroid/content/Intent;
    :cond_4
    move v1, v2

    .line 51
    goto :goto_0
.end method

.method public static createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "seasonId"    # Ljava/lang/String;
    .param p4, "showId"    # Ljava/lang/String;
    .param p5, "isTrailer"    # Z
    .param p6, "resumeTimeMillis"    # I

    .prologue
    .line 45
    invoke-static/range {p0 .. p5}, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "resume_time_millis"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private resetDirector()V
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->directorInitialized:Z

    .line 153
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->director:Lcom/google/android/videos/player/Director;

    invoke-virtual {v0}, Lcom/google/android/videos/player/Director;->reset()V

    .line 154
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "result"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->director:Lcom/google/android/videos/player/Director;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/videos/player/Director;->onActivityResult(IILandroid/content/Intent;)Z

    .line 197
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->director:Lcom/google/android/videos/player/Director;

    invoke-virtual {v0}, Lcom/google/android/videos/player/Director;->onBackPressed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 181
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    .line 183
    :cond_0
    return-void
.end method

.method public onCardsViewScrollChanged(I)V
    .locals 0
    .param p1, "verticalScrollOrigin"    # I

    .prologue
    .line 224
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 22
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 67
    invoke-static/range {p0 .. p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v3

    .line 69
    .local v3, "videosGlobals":Lcom/google/android/videos/VideosGlobals;
    invoke-super/range {p0 .. p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 71
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v21

    .line 72
    .local v21, "intent":Landroid/content/Intent;
    const-string v2, "authAccount"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 73
    .local v11, "account":Ljava/lang/String;
    const-string v2, "video_id"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 74
    .local v7, "videoId":Ljava/lang/String;
    const-string v2, "season_id"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 75
    .local v8, "seasonId":Ljava/lang/String;
    const-string v2, "show_id"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 76
    .local v9, "showId":Ljava/lang/String;
    const-string v2, "is_trailer"

    const/4 v4, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->isTrailer:Z

    .line 78
    if-eqz p1, :cond_2

    .line 79
    new-instance v2, Lcom/google/android/videos/player/PlaybackResumeState;

    const-string v4, "playback_resume_state"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    invoke-direct {v2, v4}, Lcom/google/android/videos/player/PlaybackResumeState;-><init>(Landroid/os/Bundle;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    .line 88
    :cond_0
    :goto_0
    new-instance v2, Lcom/google/android/videos/player/Director;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->isTrailer:Z

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v4, p0

    move-object/from16 v5, p0

    invoke-direct/range {v2 .. v13}, Lcom/google/android/videos/player/Director;-><init>(Lcom/google/android/videos/VideosGlobals;Landroid/support/v4/app/FragmentActivity;Lcom/google/android/videos/player/Director$Listener;Lcom/google/android/videos/player/PlaybackResumeState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLandroid/media/MediaRouter$RouteInfo;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->director:Lcom/google/android/videos/player/Director;

    .line 91
    const v2, 0x7f0400c7

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->setContentView(I)V

    .line 92
    const v2, 0x7f0f020f

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Lcom/google/android/videos/player/PlayerView;

    .line 93
    .local v13, "playerView":Lcom/google/android/videos/player/PlayerView;
    new-instance v15, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;-><init>(Landroid/content/Context;)V

    .line 94
    .local v15, "subtitlesOverlay":Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;
    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/videos/player/PlayerView$PlayerOverlay;

    const/4 v4, 0x0

    aput-object v15, v2, v4

    invoke-virtual {v13, v2}, Lcom/google/android/videos/player/PlayerView;->addOverlays([Lcom/google/android/videos/player/PlayerView$PlayerOverlay;)V

    .line 95
    const/16 v18, 0x0

    .line 96
    .local v18, "panoKnowledgeOverlay":Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->director:Lcom/google/android/videos/player/Director;

    invoke-virtual {v2}, Lcom/google/android/videos/player/Director;->isKnowledgeEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 98
    new-instance v18, Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;

    .end local v18    # "panoKnowledgeOverlay":Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;
    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;-><init>(Landroid/content/Context;)V

    .line 99
    .restart local v18    # "panoKnowledgeOverlay":Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;
    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/videos/player/PlayerView$PlayerOverlay;

    const/4 v4, 0x0

    aput-object v18, v2, v4

    invoke-virtual {v13, v2}, Lcom/google/android/videos/player/PlayerView;->addOverlays([Lcom/google/android/videos/player/PlayerView$PlayerOverlay;)V

    .line 101
    :cond_1
    new-instance v14, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->director:Lcom/google/android/videos/player/Director;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v14, v0, v2, v1}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/google/android/videos/player/overlay/TrackChangeListener;Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;)V

    .line 103
    .local v14, "controllerOverlay":Lcom/google/android/videos/pano/playback/PanoControllerOverlay;
    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/videos/player/PlayerView$PlayerOverlay;

    const/4 v4, 0x0

    aput-object v14, v2, v4

    invoke-virtual {v13, v2}, Lcom/google/android/videos/player/PlayerView;->addOverlays([Lcom/google/android/videos/player/PlayerView$PlayerOverlay;)V

    .line 104
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->director:Lcom/google/android/videos/player/Director;

    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-virtual {v14}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->getInControllerKnowledgeView()Lcom/google/android/videos/tagging/KnowledgeView;

    move-result-object v19

    const/16 v20, 0x0

    invoke-virtual/range {v12 .. v20}, Lcom/google/android/videos/player/Director;->setViewsAndOverlays(Lcom/google/android/videos/player/PlayerView;Lcom/google/android/videos/player/overlay/ControllerOverlay;Lcom/google/android/videos/player/overlay/SubtitlesOverlay;Landroid/view/View;Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;Lcom/google/android/videos/tagging/KnowledgeView;Lcom/google/android/videos/tagging/KnowledgeView;Lcom/google/android/videos/player/overlay/DragPromoOverlay;)V

    .line 106
    return-void

    .line 82
    .end local v13    # "playerView":Lcom/google/android/videos/player/PlayerView;
    .end local v14    # "controllerOverlay":Lcom/google/android/videos/pano/playback/PanoControllerOverlay;
    .end local v15    # "subtitlesOverlay":Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;
    .end local v18    # "panoKnowledgeOverlay":Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;
    :cond_2
    new-instance v2, Lcom/google/android/videos/player/PlaybackResumeState;

    invoke-direct {v2}, Lcom/google/android/videos/player/PlaybackResumeState;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    .line 83
    const-string v2, "resume_time_millis"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 84
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    const-string v4, "resume_time_millis"

    const/4 v5, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/google/android/videos/player/PlaybackResumeState;->setResumeTimeMillis(I)V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 187
    const v1, 0x7f0f020f

    invoke-virtual {p0, v1}, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/player/PlayerView;

    .line 188
    .local v0, "playerView":Lcom/google/android/videos/player/PlayerView;
    if-eqz v0, :cond_0

    .line 189
    invoke-virtual {v0}, Lcom/google/android/videos/player/PlayerView;->getPlayerSurface()Lcom/google/android/videos/player/PlayerSurface;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/videos/player/PlayerSurface;->release()V

    .line 191
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onDestroy()V

    .line 192
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->director:Lcom/google/android/videos/player/Director;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/player/Director;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    const/4 v0, 0x1

    .line 167
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->director:Lcom/google/android/videos/player/Director;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/player/Director;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    const/4 v0, 0x1

    .line 175
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKnowledgeEnteredFullScreen()V
    .locals 0

    .prologue
    .line 247
    return-void
.end method

.method public onKnowledgeExitedFullScreen()V
    .locals 0

    .prologue
    .line 252
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 139
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->videoIsPlaying:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->director:Lcom/google/android/videos/player/Director;

    invoke-virtual {v0}, Lcom/google/android/videos/player/Director;->showingChildActivity()Z

    move-result v0

    if-nez v0, :cond_1

    .line 140
    :cond_0
    invoke-direct {p0}, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->resetDirector()V

    .line 142
    :cond_1
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onPause()V

    .line 143
    return-void
.end method

.method public onPlaybackError()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 240
    iput-boolean v0, p0, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->videoIsPlaying:Z

    .line 241
    invoke-virtual {p0, v0}, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->requestVisibleBehind(Z)Z

    .line 242
    return-void
.end method

.method public onPlaybackPaused()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 234
    iput-boolean v0, p0, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->videoIsPlaying:Z

    .line 235
    invoke-virtual {p0, v0}, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->requestVisibleBehind(Z)Z

    .line 236
    return-void
.end method

.method public onPlaybackStarted()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 228
    iput-boolean v0, p0, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->videoIsPlaying:Z

    .line 229
    invoke-virtual {p0, v0}, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->requestVisibleBehind(Z)Z

    .line 230
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 110
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    .line 111
    iget-boolean v1, p0, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->directorInitialized:Z

    if-nez v1, :cond_0

    .line 112
    iget-object v1, p0, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->director:Lcom/google/android/videos/player/Director;

    invoke-virtual {v1, v2}, Lcom/google/android/videos/player/Director;->initPlayback(Z)V

    .line 113
    iput-boolean v2, p0, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->directorInitialized:Z

    .line 115
    :cond_0
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v0

    .line 116
    .local v0, "videosGlobals":Lcom/google/android/videos/VideosGlobals;
    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->isTrailer:Z

    if-nez v1, :cond_1

    .line 117
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->finish()V

    .line 119
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->videoIsPlaying:Z

    if-eqz v1, :cond_2

    .line 120
    invoke-virtual {p0, v2}, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->requestVisibleBehind(Z)Z

    move-result v1

    if-nez v1, :cond_2

    .line 121
    const-string v1, "requestVisibleBehind returned false"

    invoke-static {v1}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 124
    :cond_2
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 158
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 159
    const-string v0, "playback_resume_state"

    iget-object v1, p0, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    invoke-virtual {v1}, Lcom/google/android/videos/player/PlaybackResumeState;->getBundle()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 160
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 256
    const/4 v0, 0x1

    return v0
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0}, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->resetDirector()V

    .line 148
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStop()V

    .line 149
    return-void
.end method

.method public onUserInteractionEnding()V
    .locals 0

    .prologue
    .line 214
    return-void
.end method

.method public onUserInteractionExpected()V
    .locals 0

    .prologue
    .line 209
    return-void
.end method

.method public onUserInteractionNotExpected()V
    .locals 0

    .prologue
    .line 219
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->director:Lcom/google/android/videos/player/Director;

    invoke-virtual {v0}, Lcom/google/android/videos/player/Director;->onUserLeaveHint()V

    .line 129
    return-void
.end method

.method public onVideoTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 204
    return-void
.end method

.method public onVisibleBehindCanceled()V
    .locals 0

    .prologue
    .line 133
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onVisibleBehindCanceled()V

    .line 134
    invoke-direct {p0}, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->resetDirector()V

    .line 135
    return-void
.end method
