.class public Lcom/google/android/videos/settings/ColorPreference;
.super Lcom/google/android/videos/settings/GridDialogPreference;
.source "ColorPreference.java"


# instance fields
.field private defaultColor:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/settings/GridDialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    const v0, 0x7f04003f

    invoke-virtual {p0, v0}, Lcom/google/android/videos/settings/ColorPreference;->setDialogLayoutResource(I)V

    .line 30
    const v0, 0x7f04003e

    invoke-virtual {p0, v0}, Lcom/google/android/videos/settings/ColorPreference;->setGridItemLayoutResource(I)V

    .line 31
    return-void
.end method


# virtual methods
.method protected decodeStringValue(Ljava/lang/String;)I
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method protected getTitleAt(I)Ljava/lang/CharSequence;
    .locals 6
    .param p1, "index"    # I

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/google/android/videos/settings/GridDialogPreference;->getTitleAt(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 51
    .local v0, "title":Ljava/lang/CharSequence;
    if-eqz v0, :cond_0

    .line 55
    .end local v0    # "title":Ljava/lang/CharSequence;
    :goto_0
    return-object v0

    .restart local v0    # "title":Ljava/lang/CharSequence;
    :cond_0
    const-string v1, "#%06X"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/videos/settings/ColorPreference;->getValueAt(I)I

    move-result v4

    const v5, 0xffffff

    and-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected onBindGridItem(Landroid/view/View;I)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;
    .param p2, "index"    # I

    .prologue
    .line 61
    invoke-virtual {p0, p2}, Lcom/google/android/videos/settings/ColorPreference;->getValueAt(I)I

    move-result v0

    .line 62
    .local v0, "argb":I
    const/4 v5, 0x1

    if-ne v0, v5, :cond_0

    .line 63
    iget v0, p0, Lcom/google/android/videos/settings/ColorPreference;->defaultColor:I

    .line 66
    :cond_0
    const v5, 0x7f0f0119

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 67
    .local v3, "swatch":Landroid/widget/ImageView;
    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v5

    const/16 v6, 0xff

    if-ge v5, v6, :cond_2

    const v5, 0x7f0201e3

    :goto_0
    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 69
    invoke-virtual {v3}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 70
    .local v1, "foreground":Landroid/graphics/drawable/Drawable;
    instance-of v5, v1, Landroid/graphics/drawable/ColorDrawable;

    if-eqz v5, :cond_3

    .line 71
    check-cast v1, Landroid/graphics/drawable/ColorDrawable;

    .end local v1    # "foreground":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/ColorDrawable;->setColor(I)V

    .line 76
    :goto_1
    invoke-virtual {p0, p2}, Lcom/google/android/videos/settings/ColorPreference;->getTitleAt(I)Ljava/lang/CharSequence;

    move-result-object v4

    .line 77
    .local v4, "title":Ljava/lang/CharSequence;
    if-eqz v4, :cond_1

    .line 78
    const v5, 0x7f0f011a

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 79
    .local v2, "summary":Landroid/widget/TextView;
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    .end local v2    # "summary":Landroid/widget/TextView;
    :cond_1
    return-void

    .line 67
    .end local v4    # "title":Ljava/lang/CharSequence;
    :cond_2
    const/4 v5, 0x0

    goto :goto_0

    .line 73
    .restart local v1    # "foreground":Landroid/graphics/drawable/Drawable;
    :cond_3
    new-instance v5, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v5, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method public setUseTrackSettingsColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 35
    iput p1, p0, Lcom/google/android/videos/settings/ColorPreference;->defaultColor:I

    .line 36
    return-void
.end method

.method public shouldDisableDependents()Z
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/google/android/videos/settings/ColorPreference;->getValue()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0}, Lcom/google/android/videos/settings/GridDialogPreference;->shouldDisableDependents()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
