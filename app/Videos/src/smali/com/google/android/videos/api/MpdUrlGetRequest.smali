.class public Lcom/google/android/videos/api/MpdUrlGetRequest;
.super Lcom/google/android/videos/async/Request;
.source "MpdUrlGetRequest.java"


# instance fields
.field public final isEpisode:Z

.field public final locale:Ljava/lang/String;

.field public final requireYouTubeUrl:Z

.field public final videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLjava/lang/String;ZZLjava/util/Locale;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "requireAuthentication"    # Z
    .param p3, "videoId"    # Ljava/lang/String;
    .param p4, "isEpisode"    # Z
    .param p5, "requireYouTubeUrl"    # Z
    .param p6, "locale"    # Ljava/util/Locale;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/async/Request;-><init>(Ljava/lang/String;Z)V

    .line 28
    iput-object p3, p0, Lcom/google/android/videos/api/MpdUrlGetRequest;->videoId:Ljava/lang/String;

    .line 29
    iput-boolean p4, p0, Lcom/google/android/videos/api/MpdUrlGetRequest;->isEpisode:Z

    .line 30
    iput-boolean p5, p0, Lcom/google/android/videos/api/MpdUrlGetRequest;->requireYouTubeUrl:Z

    .line 31
    invoke-static {p6}, Lcom/google/android/videos/utils/LocaleUtils;->toString(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/api/MpdUrlGetRequest;->locale:Ljava/lang/String;

    .line 32
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 36
    if-ne p0, p1, :cond_1

    .line 40
    :cond_0
    :goto_0
    return v1

    .line 37
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 39
    check-cast v0, Lcom/google/android/videos/api/MpdUrlGetRequest;

    .line 40
    .local v0, "that":Lcom/google/android/videos/api/MpdUrlGetRequest;
    iget-boolean v3, p0, Lcom/google/android/videos/api/MpdUrlGetRequest;->isEpisode:Z

    iget-boolean v4, v0, Lcom/google/android/videos/api/MpdUrlGetRequest;->isEpisode:Z

    if-ne v3, v4, :cond_4

    iget-boolean v3, p0, Lcom/google/android/videos/api/MpdUrlGetRequest;->requireYouTubeUrl:Z

    iget-boolean v4, v0, Lcom/google/android/videos/api/MpdUrlGetRequest;->requireYouTubeUrl:Z

    if-ne v3, v4, :cond_4

    iget-object v3, p0, Lcom/google/android/videos/api/MpdUrlGetRequest;->videoId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/api/MpdUrlGetRequest;->videoId:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/videos/api/MpdUrlGetRequest;->account:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/api/MpdUrlGetRequest;->account:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/videos/api/MpdUrlGetRequest;->locale:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/api/MpdUrlGetRequest;->locale:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 49
    iget-object v2, p0, Lcom/google/android/videos/api/MpdUrlGetRequest;->videoId:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/videos/api/MpdUrlGetRequest;->videoId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 50
    .local v0, "result":I
    :goto_0
    mul-int/lit8 v4, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/api/MpdUrlGetRequest;->account:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/videos/api/MpdUrlGetRequest;->account:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v4, v2

    .line 51
    mul-int/lit8 v4, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/api/MpdUrlGetRequest;->locale:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/videos/api/MpdUrlGetRequest;->locale:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v4, v2

    .line 52
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v2, p0, Lcom/google/android/videos/api/MpdUrlGetRequest;->isEpisode:Z

    if-eqz v2, :cond_3

    move v2, v3

    :goto_3
    add-int v0, v4, v2

    .line 53
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v4, p0, Lcom/google/android/videos/api/MpdUrlGetRequest;->requireYouTubeUrl:Z

    if-eqz v4, :cond_4

    :goto_4
    add-int v0, v2, v3

    .line 54
    return v0

    .end local v0    # "result":I
    :cond_0
    move v0, v1

    .line 49
    goto :goto_0

    .restart local v0    # "result":I
    :cond_1
    move v2, v1

    .line 50
    goto :goto_1

    :cond_2
    move v2, v1

    .line 51
    goto :goto_2

    :cond_3
    move v2, v1

    .line 52
    goto :goto_3

    :cond_4
    move v3, v1

    .line 53
    goto :goto_4
.end method
