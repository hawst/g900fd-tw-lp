.class public final Lcom/google/android/videos/async/HttpRequester;
.super Lcom/google/android/videos/async/ConvertingRequester;
.source "HttpRequester.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/videos/async/ConvertingRequester",
        "<TR;TE;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        "Lorg/apache/http/HttpResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final httpClient:Lorg/apache/http/client/HttpClient;

.field private logRequests:Z


# direct methods
.method private constructor <init>(Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/RequestConverter;Lcom/google/android/videos/converter/HttpResponseConverter;)V
    .locals 1
    .param p1, "httpClient"    # Lorg/apache/http/client/HttpClient;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/client/HttpClient;",
            "Lcom/google/android/videos/converter/RequestConverter",
            "<TR;",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;",
            "Lcom/google/android/videos/converter/HttpResponseConverter",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 61
    .local p0, "this":Lcom/google/android/videos/async/HttpRequester;, "Lcom/google/android/videos/async/HttpRequester<TR;TE;>;"
    .local p2, "requestConverter":Lcom/google/android/videos/converter/RequestConverter;, "Lcom/google/android/videos/converter/RequestConverter<TR;Lorg/apache/http/client/methods/HttpUriRequest;>;"
    .local p3, "responseConverter":Lcom/google/android/videos/converter/HttpResponseConverter;, "Lcom/google/android/videos/converter/HttpResponseConverter<TE;>;"
    invoke-direct {p0, p2, p3}, Lcom/google/android/videos/async/ConvertingRequester;-><init>(Lcom/google/android/videos/converter/RequestConverter;Lcom/google/android/videos/converter/ResponseConverter;)V

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/async/HttpRequester;->logRequests:Z

    .line 62
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/HttpClient;

    iput-object v0, p0, Lcom/google/android/videos/async/HttpRequester;->httpClient:Lorg/apache/http/client/HttpClient;

    .line 63
    return-void
.end method

.method private consumeContentResponse(Lorg/apache/http/HttpResponse;)V
    .locals 1
    .param p1, "httpResponse"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 130
    .local p0, "this":Lcom/google/android/videos/async/HttpRequester;, "Lcom/google/android/videos/async/HttpRequester<TR;TE;>;"
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 131
    .local v0, "httpEntity":Lorg/apache/http/HttpEntity;
    if-eqz v0, :cond_0

    .line 132
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 134
    :cond_0
    return-void
.end method

.method public static create(Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/async/HttpRequester;
    .locals 2
    .param p0, "httpClient"    # Lorg/apache/http/client/HttpClient;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/http/client/HttpClient;",
            "Lcom/google/android/videos/converter/HttpResponseConverter",
            "<TE;>;)",
            "Lcom/google/android/videos/async/HttpRequester",
            "<",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "TE;>;"
        }
    .end annotation

    .prologue
    .line 48
    .local p1, "responseConverter":Lcom/google/android/videos/converter/HttpResponseConverter;, "Lcom/google/android/videos/converter/HttpResponseConverter<TE;>;"
    new-instance v0, Lcom/google/android/videos/async/HttpRequester;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1}, Lcom/google/android/videos/async/HttpRequester;-><init>(Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/RequestConverter;Lcom/google/android/videos/converter/HttpResponseConverter;)V

    return-object v0
.end method

.method public static create(Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/RequestConverter;Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/async/HttpRequester;
    .locals 1
    .param p0, "httpClient"    # Lorg/apache/http/client/HttpClient;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/http/client/HttpClient;",
            "Lcom/google/android/videos/converter/RequestConverter",
            "<TR;",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;",
            "Lcom/google/android/videos/converter/HttpResponseConverter",
            "<TE;>;)",
            "Lcom/google/android/videos/async/HttpRequester",
            "<TR;TE;>;"
        }
    .end annotation

    .prologue
    .line 55
    .local p1, "requestConverter":Lcom/google/android/videos/converter/RequestConverter;, "Lcom/google/android/videos/converter/RequestConverter<TR;Lorg/apache/http/client/methods/HttpUriRequest;>;"
    .local p2, "responseConverter":Lcom/google/android/videos/converter/HttpResponseConverter;, "Lcom/google/android/videos/converter/HttpResponseConverter<TE;>;"
    new-instance v0, Lcom/google/android/videos/async/HttpRequester;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/videos/async/HttpRequester;-><init>(Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/RequestConverter;Lcom/google/android/videos/converter/HttpResponseConverter;)V

    return-object v0
.end method


# virtual methods
.method protected bridge synthetic doRequest(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/google/android/videos/async/Callback;

    .prologue
    .line 31
    .local p0, "this":Lcom/google/android/videos/async/HttpRequester;, "Lcom/google/android/videos/async/HttpRequester<TR;TE;>;"
    check-cast p1, Lorg/apache/http/client/methods/HttpUriRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/async/HttpRequester;->doRequest(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/google/android/videos/async/Callback;)V

    return-void
.end method

.method protected doRequest(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/google/android/videos/async/Callback;)V
    .locals 4
    .param p1, "httpRequest"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "Lorg/apache/http/HttpResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 82
    .local p0, "this":Lcom/google/android/videos/async/HttpRequester;, "Lcom/google/android/videos/async/HttpRequester<TR;TE;>;"
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;>;"
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    iget-boolean v2, p0, Lcom/google/android/videos/async/HttpRequester;->logRequests:Z

    if-eqz v2, :cond_0

    .line 85
    invoke-static {p1}, Lcom/google/android/videos/async/HttpUriRequestLogger;->toLogString(Lorg/apache/http/client/methods/HttpUriRequest;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 88
    :cond_0
    const/4 v1, 0x0

    .line 90
    .local v1, "httpResponse":Lorg/apache/http/HttpResponse;
    :try_start_0
    iget-object v2, p0, Lcom/google/android/videos/async/HttpRequester;->httpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v2, p1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 91
    invoke-interface {p2, p1, v1}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 103
    if-eqz v1, :cond_1

    .line 105
    :try_start_1
    invoke-direct {p0, v1}, Lcom/google/android/videos/async/HttpRequester;->consumeContentResponse(Lorg/apache/http/HttpResponse;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 111
    :cond_1
    :goto_0
    return-void

    .line 106
    :catch_0
    move-exception v0

    .line 107
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "Error consuming content response"

    invoke-static {v2, v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 92
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 95
    .local v0, "e":Ljava/lang/IllegalStateException;
    :try_start_2
    invoke-interface {p2, p1, v0}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 103
    if-eqz v1, :cond_1

    .line 105
    :try_start_3
    invoke-direct {p0, v1}, Lcom/google/android/videos/async/HttpRequester;->consumeContentResponse(Lorg/apache/http/HttpResponse;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 106
    :catch_2
    move-exception v0

    .line 107
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "Error consuming content response"

    invoke-static {v2, v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 96
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 97
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_4
    invoke-interface {p1}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    .line 98
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 103
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v2

    if-eqz v1, :cond_2

    .line 105
    :try_start_5
    invoke-direct {p0, v1}, Lcom/google/android/videos/async/HttpRequester;->consumeContentResponse(Lorg/apache/http/HttpResponse;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6

    .line 108
    :cond_2
    :goto_1
    throw v2

    .line 99
    :catch_4
    move-exception v0

    .line 101
    .local v0, "e":Ljava/lang/Exception;
    :try_start_6
    invoke-interface {p2, p1, v0}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 103
    if-eqz v1, :cond_1

    .line 105
    :try_start_7
    invoke-direct {p0, v1}, Lcom/google/android/videos/async/HttpRequester;->consumeContentResponse(Lorg/apache/http/HttpResponse;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    goto :goto_0

    .line 106
    :catch_5
    move-exception v0

    .line 107
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "Error consuming content response"

    invoke-static {v2, v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 106
    .end local v0    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v0

    .line 107
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v3, "Error consuming content response"

    invoke-static {v3, v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method protected bridge synthetic onConvertError(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/videos/async/Callback;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;
    .param p3, "x2"    # Lcom/google/android/videos/async/Callback;
    .param p4, "x3"    # Ljava/lang/Exception;

    .prologue
    .line 31
    .local p0, "this":Lcom/google/android/videos/async/HttpRequester;, "Lcom/google/android/videos/async/HttpRequester<TR;TE;>;"
    check-cast p2, Lorg/apache/http/client/methods/HttpUriRequest;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/videos/async/HttpRequester;->onConvertError(Ljava/lang/Object;Lorg/apache/http/client/methods/HttpUriRequest;Lcom/google/android/videos/async/Callback;Ljava/lang/Exception;)V

    return-void
.end method

.method protected onConvertError(Ljava/lang/Object;Lorg/apache/http/client/methods/HttpUriRequest;Lcom/google/android/videos/async/Callback;Ljava/lang/Exception;)V
    .locals 3
    .param p2, "convertedRequest"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p4, "exception"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    .prologue
    .line 116
    .local p0, "this":Lcom/google/android/videos/async/HttpRequester;, "Lcom/google/android/videos/async/HttpRequester<TR;TE;>;"
    .local p1, "originalRequest":Ljava/lang/Object;, "TR;"
    .local p3, "originalCallback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TR;TE;>;"
    instance-of v1, p4, Lorg/apache/http/client/HttpResponseException;

    if-eqz v1, :cond_0

    move-object v0, p4

    .line 117
    check-cast v0, Lorg/apache/http/client/HttpResponseException;

    .line 118
    .local v0, "httpException":Lorg/apache/http/client/HttpResponseException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Http error: request=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p2}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 119
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Http error: status=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] msg=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 122
    .end local v0    # "httpException":Lorg/apache/http/client/HttpResponseException;
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/videos/async/ConvertingRequester;->onConvertError(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/videos/async/Callback;Ljava/lang/Exception;)V

    .line 123
    return-void
.end method
