.class public final Lcom/google/android/videos/async/ActivityCallback;
.super Lcom/google/android/videos/async/ThreadingCallback;
.source "ActivityCallback.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/videos/async/ThreadingCallback",
        "<TR;TE;>;"
    }
.end annotation


# instance fields
.field private final activity:Landroid/app/Activity;


# direct methods
.method private constructor <init>(Landroid/app/Activity;Lcom/google/android/videos/async/Callback;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;)V"
        }
    .end annotation

    .prologue
    .line 25
    .local p0, "this":Lcom/google/android/videos/async/ActivityCallback;, "Lcom/google/android/videos/async/ActivityCallback<TR;TE;>;"
    .local p2, "target":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TR;TE;>;"
    invoke-direct {p0, p2}, Lcom/google/android/videos/async/ThreadingCallback;-><init>(Lcom/google/android/videos/async/Callback;)V

    .line 26
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/videos/async/ActivityCallback;->activity:Landroid/app/Activity;

    .line 27
    return-void
.end method

.method public static create(Landroid/app/Activity;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/ActivityCallback;
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/app/Activity;",
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;)",
            "Lcom/google/android/videos/async/ActivityCallback",
            "<TR;TE;>;"
        }
    .end annotation

    .prologue
    .line 30
    .local p1, "target":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TR;TE;>;"
    new-instance v0, Lcom/google/android/videos/async/ActivityCallback;

    invoke-direct {v0, p0, p1}, Lcom/google/android/videos/async/ActivityCallback;-><init>(Landroid/app/Activity;Lcom/google/android/videos/async/Callback;)V

    return-object v0
.end method


# virtual methods
.method protected post(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 35
    .local p0, "this":Lcom/google/android/videos/async/ActivityCallback;, "Lcom/google/android/videos/async/ActivityCallback<TR;TE;>;"
    iget-object v0, p0, Lcom/google/android/videos/async/ActivityCallback;->activity:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 36
    return-void
.end method
