.class public Lcom/google/android/videos/pano/nowtifications/NowtificationsContentProvider;
.super Landroid/content/ContentProvider;
.source "NowtificationsContentProvider.java"


# static fields
.field private static final BASE_URI:Landroid/net/Uri;

.field private static final URI_MATCHER:Landroid/content/UriMatcher;


# instance fields
.field private bitmapConverter:Lcom/google/android/videos/utils/BytesToPipeConverter;

.field private videosGlobals:Lcom/google/android/videos/VideosGlobals;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 35
    const-string v0, "content://com.google.android.videos.nowtifications"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/videos/pano/nowtifications/NowtificationsContentProvider;->BASE_URI:Landroid/net/Uri;

    .line 39
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/google/android/videos/pano/nowtifications/NowtificationsContentProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    .line 40
    sget-object v0, Lcom/google/android/videos/pano/nowtifications/NowtificationsContentProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.videos.nowtifications"

    const-string v2, "bitmap/"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 41
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method public static createBitmapUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 3
    .param p0, "imageUri"    # Ljava/lang/String;

    .prologue
    .line 44
    sget-object v1, Lcom/google/android/videos/pano/nowtifications/NowtificationsContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "bitmap"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "uri"

    invoke-virtual {v1, v2, p0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 47
    .local v0, "res":Landroid/net/Uri;
    return-object v0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 72
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 62
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 67
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/google/android/videos/pano/nowtifications/NowtificationsContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pano/nowtifications/NowtificationsContentProvider;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    .line 56
    new-instance v0, Lcom/google/android/videos/utils/BytesToPipeConverter;

    iget-object v1, p0, Lcom/google/android/videos/pano/nowtifications/NowtificationsContentProvider;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getNetworkExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/videos/utils/BytesToPipeConverter;-><init>(Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lcom/google/android/videos/pano/nowtifications/NowtificationsContentProvider;->bitmapConverter:Lcom/google/android/videos/utils/BytesToPipeConverter;

    .line 57
    const/4 v0, 0x1

    return v0
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "mode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 88
    sget-object v4, Lcom/google/android/videos/pano/nowtifications/NowtificationsContentProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v4

    if-nez v4, :cond_0

    .line 89
    const-string v4, "uri"

    invoke-virtual {p1, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 90
    .local v0, "bitmapUri":Landroid/net/Uri;
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v2

    .line 91
    .local v2, "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArray;>;"
    iget-object v4, p0, Lcom/google/android/videos/pano/nowtifications/NowtificationsContentProvider;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getBitmapRequesters()Lcom/google/android/videos/bitmap/BitmapRequesters;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/videos/bitmap/BitmapRequesters;->getSyncBitmapBytesRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v4

    invoke-interface {v4, v0, v2}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 94
    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/utils/ByteArray;

    .line 95
    .local v1, "byteArray":Lcom/google/android/videos/utils/ByteArray;
    iget-object v4, p0, Lcom/google/android/videos/pano/nowtifications/NowtificationsContentProvider;->bitmapConverter:Lcom/google/android/videos/utils/BytesToPipeConverter;

    invoke-virtual {v4, v1}, Lcom/google/android/videos/utils/BytesToPipeConverter;->convertResponse(Lcom/google/android/videos/utils/ByteArray;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/videos/converter/ConverterException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    .line 102
    .end local v0    # "bitmapUri":Landroid/net/Uri;
    .end local v1    # "byteArray":Lcom/google/android/videos/utils/ByteArray;
    .end local v2    # "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArray;>;"
    :goto_0
    return-object v4

    .line 96
    .restart local v0    # "bitmapUri":Landroid/net/Uri;
    .restart local v2    # "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArray;>;"
    :catch_0
    move-exception v3

    .line 97
    .local v3, "e":Ljava/util/concurrent/ExecutionException;
    const-string v4, "Exception fetching icon bytes for "

    invoke-virtual {v3}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 102
    .end local v0    # "bitmapUri":Landroid/net/Uri;
    .end local v2    # "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArray;>;"
    .end local v3    # "e":Ljava/util/concurrent/ExecutionException;
    :cond_0
    :goto_1
    const/4 v4, 0x0

    goto :goto_0

    .line 98
    .restart local v0    # "bitmapUri":Landroid/net/Uri;
    .restart local v2    # "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArray;>;"
    :catch_1
    move-exception v3

    .line 99
    .local v3, "e":Lcom/google/android/videos/converter/ConverterException;
    const-string v4, "Exception converting icon bytes for "

    invoke-virtual {v3}, Lcom/google/android/videos/converter/ConverterException;->getCause()Ljava/lang/Throwable;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 83
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 77
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
