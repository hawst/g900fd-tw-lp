.class final Lcom/google/android/videos/utils/HttpClientFactory$CookieStoreHttpClient;
.super Lorg/apache/http/impl/client/DefaultHttpClient;
.source "HttpClientFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/utils/HttpClientFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "CookieStoreHttpClient"
.end annotation


# instance fields
.field private final cookieStore:Lorg/apache/http/client/CookieStore;


# direct methods
.method public constructor <init>(Lorg/apache/http/client/CookieStore;Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V
    .locals 0
    .param p1, "cookieStore"    # Lorg/apache/http/client/CookieStore;
    .param p2, "clientConnectionManager"    # Lorg/apache/http/conn/ClientConnectionManager;
    .param p3, "params"    # Lorg/apache/http/params/HttpParams;

    .prologue
    .line 97
    invoke-direct {p0, p2, p3}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    .line 98
    iput-object p1, p0, Lcom/google/android/videos/utils/HttpClientFactory$CookieStoreHttpClient;->cookieStore:Lorg/apache/http/client/CookieStore;

    .line 99
    return-void
.end method


# virtual methods
.method protected createCookieStore()Lorg/apache/http/client/CookieStore;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/videos/utils/HttpClientFactory$CookieStoreHttpClient;->cookieStore:Lorg/apache/http/client/CookieStore;

    return-object v0
.end method
