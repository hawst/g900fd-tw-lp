.class final Lcom/google/android/videos/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;
.super Ljava/lang/Object;
.source "DefaultControllerOverlay.java"

# interfaces
.implements Lcom/google/android/videos/player/overlay/MediaKeyHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MediaKeyHelperListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;)V
    .locals 0

    .prologue
    .line 913
    iput-object p1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;Lcom/google/android/videos/player/overlay/DefaultControllerOverlay$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;
    .param p2, "x1"    # Lcom/google/android/videos/player/overlay/DefaultControllerOverlay$1;

    .prologue
    .line 913
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;-><init>(Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;)V

    return-void
.end method


# virtual methods
.method public onCC()V
    .locals 1

    .prologue
    .line 956
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    # invokes: Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->onCcToggled()V
    invoke-static {v0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->access$500(Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;)V

    .line 957
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 949
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    # getter for: Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->state:I
    invoke-static {v0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->access$400(Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 950
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    # getter for: Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;
    invoke-static {v0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->access$200(Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;)Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;->onPause()V

    .line 952
    :cond_0
    return-void
.end method

.method public onPlay()V
    .locals 1

    .prologue
    .line 942
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    # getter for: Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->state:I
    invoke-static {v0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->access$400(Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;)I

    move-result v0

    if-nez v0, :cond_0

    .line 943
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    # getter for: Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;
    invoke-static {v0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->access$200(Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;)Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;->onPlay()V

    .line 945
    :cond_0
    return-void
.end method

.method public onScrubbingEnd(I)V
    .locals 3
    .param p1, "timeMillis"    # I

    .prologue
    .line 927
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    # getter for: Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;
    invoke-static {v0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->access$300(Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;)Lcom/google/android/videos/player/overlay/TimeBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/videos/player/overlay/TimeBar;->endScrubbingAt(I)V

    .line 928
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    # getter for: Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;
    invoke-static {v0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->access$200(Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;)Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-interface {v0, v1, p1, v2}, Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;->onSeekTo(ZIZ)V

    .line 929
    return-void
.end method

.method public onScrubbingStart()V
    .locals 1

    .prologue
    .line 917
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    # getter for: Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;
    invoke-static {v0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->access$300(Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;)Lcom/google/android/videos/player/overlay/TimeBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/player/overlay/TimeBar;->startScrubbing()V

    .line 918
    return-void
.end method

.method public onTogglePlayPause()V
    .locals 2

    .prologue
    .line 933
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    # getter for: Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->state:I
    invoke-static {v0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->access$400(Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 934
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    # getter for: Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;
    invoke-static {v0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->access$200(Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;)Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;->onPause()V

    .line 938
    :cond_0
    :goto_0
    return-void

    .line 935
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    # getter for: Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->state:I
    invoke-static {v0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->access$400(Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;)I

    move-result v0

    if-nez v0, :cond_0

    .line 936
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    # getter for: Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;
    invoke-static {v0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->access$200(Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;)Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;->onPlay()V

    goto :goto_0
.end method

.method public onUpdateScrubberTime(I)V
    .locals 1
    .param p1, "timeMillis"    # I

    .prologue
    .line 922
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    # getter for: Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;
    invoke-static {v0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->access$300(Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;)Lcom/google/android/videos/player/overlay/TimeBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/videos/player/overlay/TimeBar;->setScrubberTime(I)V

    .line 923
    return-void
.end method
