.class public final Lcom/google/android/videos/player/PlaybackResumeState;
.super Ljava/lang/Object;
.source "PlaybackResumeState.java"


# instance fields
.field private final bundle:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/PlaybackResumeState;-><init>(Landroid/os/Bundle;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    if-eqz p1, :cond_0

    .end local p1    # "bundle":Landroid/os/Bundle;
    :goto_0
    iput-object p1, p0, Lcom/google/android/videos/player/PlaybackResumeState;->bundle:Landroid/os/Bundle;

    .line 34
    return-void

    .line 33
    .restart local p1    # "bundle":Landroid/os/Bundle;
    :cond_0
    new-instance p1, Landroid/os/Bundle;

    .end local p1    # "bundle":Landroid/os/Bundle;
    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public areSubtitlesTurnedOff()Z
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/videos/player/PlaybackResumeState;->bundle:Landroid/os/Bundle;

    const-string v1, "lang"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/player/PlaybackResumeState;->bundle:Landroid/os/Bundle;

    const-string v1, "lang"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public areSubtitlesTurnedOn()Z
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/videos/player/PlaybackResumeState;->bundle:Landroid/os/Bundle;

    const-string v1, "lang"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/player/PlaybackResumeState;->bundle:Landroid/os/Bundle;

    const-string v1, "lang"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public clearPlaybackError()V
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/videos/player/PlaybackResumeState;->bundle:Landroid/os/Bundle;

    const-string v1, "error"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 114
    iget-object v0, p0, Lcom/google/android/videos/player/PlaybackResumeState;->bundle:Landroid/os/Bundle;

    const-string v1, "fatal"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 115
    return-void
.end method

.method public getBundle()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/videos/player/PlaybackResumeState;->bundle:Landroid/os/Bundle;

    return-object v0
.end method

.method public getHqState(Z)Z
    .locals 2
    .param p1, "defaultValue"    # Z

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/videos/player/PlaybackResumeState;->bundle:Landroid/os/Bundle;

    const-string v1, "hq"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getPlaybackError()Ljava/lang/String;
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/videos/player/PlaybackResumeState;->bundle:Landroid/os/Bundle;

    const-string v1, "error"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getResumeTimeMillis()I
    .locals 3

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/videos/player/PlaybackResumeState;->bundle:Landroid/os/Bundle;

    const-string v1, "resume_time"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getSelectedAudioTrackIndex(I)I
    .locals 2
    .param p1, "defaultValue"    # I

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/videos/player/PlaybackResumeState;->bundle:Landroid/os/Bundle;

    const-string v1, "audio_index"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getShortClockDialogConfirmed()Z
    .locals 3

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/videos/player/PlaybackResumeState;->bundle:Landroid/os/Bundle;

    const-string v1, "short_clock"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getSubtitlesLanguage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/videos/player/PlaybackResumeState;->bundle:Landroid/os/Bundle;

    const-string v1, "lang"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasResumeTime()Z
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/videos/player/PlaybackResumeState;->bundle:Landroid/os/Bundle;

    const-string v1, "resume_time"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public setHqState(Z)V
    .locals 2
    .param p1, "hq"    # Z

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/videos/player/PlaybackResumeState;->bundle:Landroid/os/Bundle;

    const-string v1, "hq"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 89
    return-void
.end method

.method public setPlaybackError(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "error"    # Ljava/lang/String;
    .param p2, "isFatal"    # Z

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/videos/player/PlaybackResumeState;->bundle:Landroid/os/Bundle;

    const-string v1, "error"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lcom/google/android/videos/player/PlaybackResumeState;->bundle:Landroid/os/Bundle;

    const-string v1, "fatal"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 110
    return-void
.end method

.method public setResumeTimeMillis(I)V
    .locals 2
    .param p1, "resumeTimeMillis"    # I

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/videos/player/PlaybackResumeState;->bundle:Landroid/os/Bundle;

    const-string v1, "resume_time"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 54
    return-void
.end method

.method public setSelectedAudioTrackIndex(I)V
    .locals 2
    .param p1, "trackIndex"    # I

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/videos/player/PlaybackResumeState;->bundle:Landroid/os/Bundle;

    const-string v1, "audio_index"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 77
    return-void
.end method

.method public setShortClockDialogConfirmed()V
    .locals 3

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/videos/player/PlaybackResumeState;->bundle:Landroid/os/Bundle;

    const-string v1, "short_clock"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 97
    return-void
.end method

.method public setSubtitleLanguage(Ljava/lang/String;)V
    .locals 2
    .param p1, "subtitleLanguage"    # Ljava/lang/String;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/videos/player/PlaybackResumeState;->bundle:Landroid/os/Bundle;

    const-string v1, "lang"

    if-eqz p1, :cond_0

    .end local p1    # "subtitleLanguage":Ljava/lang/String;
    :goto_0
    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    return-void

    .line 71
    .restart local p1    # "subtitleLanguage":Ljava/lang/String;
    :cond_0
    const-string p1, ""

    goto :goto_0
.end method

.method public wasLastPlaybackErrorFatal()Z
    .locals 3

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/videos/player/PlaybackResumeState;->bundle:Landroid/os/Bundle;

    const-string v1, "fatal"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
