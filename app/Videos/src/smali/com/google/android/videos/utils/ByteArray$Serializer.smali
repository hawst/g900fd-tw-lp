.class public final Lcom/google/android/videos/utils/ByteArray$Serializer;
.super Ljava/lang/Object;
.source "ByteArray.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/utils/ByteArray;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Serializer"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private data:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 78
    new-instance v0, Lcom/google/android/videos/utils/ByteArray;

    iget-object v1, p0, Lcom/google/android/videos/utils/ByteArray$Serializer;->data:[B

    invoke-direct {v0, v1}, Lcom/google/android/videos/utils/ByteArray;-><init>([B)V

    return-object v0
.end method


# virtual methods
.method public setData(Lcom/google/android/videos/utils/ByteArray;)Lcom/google/android/videos/utils/ByteArray$Serializer;
    .locals 4
    .param p1, "byteArray"    # Lcom/google/android/videos/utils/ByteArray;

    .prologue
    const/4 v3, 0x0

    .line 70
    invoke-virtual {p1}, Lcom/google/android/videos/utils/ByteArray;->limit()I

    move-result v0

    .line 71
    .local v0, "length":I
    new-array v1, v0, [B

    iput-object v1, p0, Lcom/google/android/videos/utils/ByteArray$Serializer;->data:[B

    .line 72
    iget-object v1, p1, Lcom/google/android/videos/utils/ByteArray;->data:[B

    iget-object v2, p0, Lcom/google/android/videos/utils/ByteArray$Serializer;->data:[B

    invoke-static {v1, v3, v2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 73
    return-object p0
.end method
