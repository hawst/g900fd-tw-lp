.class Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;
.super Ljava/lang/Object;
.source "WatchNowDataSource.java"

# interfaces
.implements Lcom/google/android/videos/welcome/FreeMoviePromoRequester$Listener;
.implements Lcom/google/android/videos/welcome/PromoDismisser$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/datasource/WatchNowDataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PromoProcessor"
.end annotation


# instance fields
.field private final itemsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/pano/model/VideoItem;",
            ">;"
        }
    .end annotation
.end field

.field private final promoDismisser:Lcom/google/android/videos/welcome/PromoDismisser;

.field private final promoRequester:Lcom/google/android/videos/welcome/FreeMoviePromoRequester;

.field private ready:Z

.field final synthetic this$0:Lcom/google/android/videos/pano/datasource/WatchNowDataSource;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/ConfigurationStore;Landroid/content/SharedPreferences;Lcom/google/android/videos/store/StoreStatusMonitor;Landroid/os/Handler;)V
    .locals 6
    .param p3, "configStore"    # Lcom/google/android/videos/store/ConfigurationStore;
    .param p4, "preferences"    # Landroid/content/SharedPreferences;
    .param p5, "purchaseStoreMonitor"    # Lcom/google/android/videos/store/StoreStatusMonitor;
    .param p6, "handler"    # Landroid/os/Handler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/PromotionsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;",
            ">;",
            "Lcom/google/android/videos/store/ConfigurationStore;",
            "Landroid/content/SharedPreferences;",
            "Lcom/google/android/videos/store/StoreStatusMonitor;",
            "Landroid/os/Handler;",
            ")V"
        }
    .end annotation

    .prologue
    .line 406
    .local p2, "promotionsRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/PromotionsRequest;Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;>;"
    iput-object p1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;->this$0:Lcom/google/android/videos/pano/datasource/WatchNowDataSource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 401
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;->itemsList:Ljava/util/List;

    .line 407
    new-instance v0, Lcom/google/android/videos/welcome/FreeMoviePromoRequester;

    move-object v1, p2

    move-object v2, p3

    move-object v3, p5

    move-object v4, p0

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/welcome/FreeMoviePromoRequester;-><init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/store/StoreStatusMonitor;Lcom/google/android/videos/welcome/FreeMoviePromoRequester$Listener;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;->promoRequester:Lcom/google/android/videos/welcome/FreeMoviePromoRequester;

    .line 409
    new-instance v0, Lcom/google/android/videos/welcome/PromoDismisser;

    const-string v1, "watchnowtv"

    const-string v2, "freemovie"

    invoke-direct {v0, v1, v2, p4}, Lcom/google/android/videos/welcome/PromoDismisser;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;->promoDismisser:Lcom/google/android/videos/welcome/PromoDismisser;

    .line 410
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;->promoDismisser:Lcom/google/android/videos/welcome/PromoDismisser;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/welcome/PromoDismisser;->setRegisteredListener(Lcom/google/android/videos/welcome/PromoDismisser$Listener;)V

    .line 411
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/ConfigurationStore;Landroid/content/SharedPreferences;Lcom/google/android/videos/store/StoreStatusMonitor;Landroid/os/Handler;Lcom/google/android/videos/pano/datasource/WatchNowDataSource$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/pano/datasource/WatchNowDataSource;
    .param p2, "x1"    # Lcom/google/android/videos/async/Requester;
    .param p3, "x2"    # Lcom/google/android/videos/store/ConfigurationStore;
    .param p4, "x3"    # Landroid/content/SharedPreferences;
    .param p5, "x4"    # Lcom/google/android/videos/store/StoreStatusMonitor;
    .param p6, "x5"    # Landroid/os/Handler;
    .param p7, "x6"    # Lcom/google/android/videos/pano/datasource/WatchNowDataSource$1;

    .prologue
    .line 395
    invoke-direct/range {p0 .. p6}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;-><init>(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/ConfigurationStore;Landroid/content/SharedPreferences;Lcom/google/android/videos/store/StoreStatusMonitor;Landroid/os/Handler;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;

    .prologue
    .line 395
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;->itemsList:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public isReady()Z
    .locals 1

    .prologue
    .line 414
    iget-boolean v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;->ready:Z

    return v0
.end method

.method public onNoPromo()V
    .locals 1

    .prologue
    .line 443
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;->ready:Z

    .line 444
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;->this$0:Lcom/google/android/videos/pano/datasource/WatchNowDataSource;

    # invokes: Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->mergeAndUpdate()V
    invoke-static {v0}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->access$1100(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;)V

    .line 445
    return-void
.end method

.method public onPromo(Lcom/google/wireless/android/video/magma/proto/PromotionResource;)V
    .locals 4
    .param p1, "promotion"    # Lcom/google/wireless/android/video/magma/proto/PromotionResource;

    .prologue
    .line 429
    iget-object v2, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;->promoDismisser:Lcom/google/android/videos/welcome/PromoDismisser;

    invoke-virtual {v2}, Lcom/google/android/videos/welcome/PromoDismisser;->isPromoDismissed()Z

    move-result v2

    if-nez v2, :cond_0

    .line 430
    iget-object v2, p1, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->asset:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    const/4 v3, 0x0

    aget-object v0, v2, v3

    .line 431
    .local v0, "asset":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    iget-object v1, p1, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->promotionCode:Ljava/lang/String;

    .line 432
    .local v1, "promoCode":Ljava/lang/String;
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    iget-object v2, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget v2, v2, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    const/4 v3, 0x6

    if-ne v2, v3, :cond_0

    .line 434
    iget-object v2, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;->itemsList:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;->this$0:Lcom/google/android/videos/pano/datasource/WatchNowDataSource;

    iget-object v3, v3, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->context:Landroid/content/Context;

    invoke-static {v3, p1}, Lcom/google/android/videos/pano/model/PromoItem;->newInstance(Landroid/content/Context;Lcom/google/wireless/android/video/magma/proto/PromotionResource;)Lcom/google/android/videos/pano/model/PromoItem;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 437
    .end local v0    # "asset":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .end local v1    # "promoCode":Ljava/lang/String;
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;->ready:Z

    .line 438
    iget-object v2, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;->this$0:Lcom/google/android/videos/pano/datasource/WatchNowDataSource;

    # invokes: Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->mergeAndUpdate()V
    invoke-static {v2}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->access$1100(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;)V

    .line 439
    return-void
.end method

.method public onPromoRequestError(Ljava/lang/Exception;)V
    .locals 1
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 449
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;->ready:Z

    .line 450
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;->this$0:Lcom/google/android/videos/pano/datasource/WatchNowDataSource;

    # invokes: Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->mergeAndUpdate()V
    invoke-static {v0}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->access$1100(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;)V

    .line 451
    return-void
.end method

.method public onPromoStateChanged()V
    .locals 2

    .prologue
    .line 455
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;->this$0:Lcom/google/android/videos/pano/datasource/WatchNowDataSource;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->updateInternal(Z)V

    .line 456
    return-void
.end method

.method public updatePromo(Ljava/lang/String;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 418
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;->ready:Z

    .line 419
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;->itemsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 420
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;->this$0:Lcom/google/android/videos/pano/datasource/WatchNowDataSource;

    # getter for: Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->config:Lcom/google/android/videos/Config;
    invoke-static {v0}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->access$700(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;)Lcom/google/android/videos/Config;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/Config;->suggestionsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 421
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;->promoRequester:Lcom/google/android/videos/welcome/FreeMoviePromoRequester;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/welcome/FreeMoviePromoRequester;->requestPromotion(Ljava/lang/String;)V

    .line 425
    :goto_0
    return-void

    .line 423
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;->onNoPromo()V

    goto :goto_0
.end method
