.class public interface abstract Lcom/google/android/videos/tagging/CardsView$Listener;
.super Ljava/lang/Object;
.source "CardsView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/CardsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onAllCardsDismissed()V
.end method

.method public abstract onCardDismissed(Landroid/view/View;)V
.end method

.method public abstract onCardsViewScrollChanged(I)V
.end method
