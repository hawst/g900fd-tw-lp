.class Lcom/google/android/videos/store/ConfigurationStore$UserConfigurationCallback;
.super Ljava/lang/Object;
.source "ConfigurationStore.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/ConfigurationStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UserConfigurationCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/api/UserConfigGetRequest;",
        "Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final callback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<-",
            "Ljava/lang/String;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/videos/store/ConfigurationStore;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/async/Callback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Callback",
            "<-",
            "Ljava/lang/String;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 244
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<-Ljava/lang/String;Ljava/lang/Void;>;"
    iput-object p1, p0, Lcom/google/android/videos/store/ConfigurationStore$UserConfigurationCallback;->this$0:Lcom/google/android/videos/store/ConfigurationStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 245
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Callback;

    iput-object v0, p0, Lcom/google/android/videos/store/ConfigurationStore$UserConfigurationCallback;->callback:Lcom/google/android/videos/async/Callback;

    .line 246
    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/api/UserConfigGetRequest;Ljava/lang/Exception;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/api/UserConfigGetRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/android/videos/store/ConfigurationStore$UserConfigurationCallback;->callback:Lcom/google/android/videos/async/Callback;

    iget-object v1, p1, Lcom/google/android/videos/api/UserConfigGetRequest;->account:Ljava/lang/String;

    invoke-interface {v0, v1, p2}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 286
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 239
    check-cast p1, Lcom/google/android/videos/api/UserConfigGetRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/store/ConfigurationStore$UserConfigurationCallback;->onError(Lcom/google/android/videos/api/UserConfigGetRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/api/UserConfigGetRequest;Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;)V
    .locals 23
    .param p1, "request"    # Lcom/google/android/videos/api/UserConfigGetRequest;
    .param p2, "response"    # Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;

    .prologue
    .line 250
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/store/ConfigurationStore$UserConfigurationCallback;->this$0:Lcom/google/android/videos/store/ConfigurationStore;

    move-object/from16 v17, v0

    # getter for: Lcom/google/android/videos/store/ConfigurationStore;->database:Lcom/google/android/videos/store/Database;
    invoke-static/range {v17 .. v17}, Lcom/google/android/videos/store/ConfigurationStore;->access$100(Lcom/google/android/videos/store/ConfigurationStore;)Lcom/google/android/videos/store/Database;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v15

    .line 251
    .local v15, "transaction":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v14, 0x0

    .line 252
    .local v14, "success":Z
    move-object/from16 v0, p2

    iget-object v11, v0, Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;->resource:Lcom/google/wireless/android/video/magma/proto/UserConfiguration;

    .line 253
    .local v11, "resource":Lcom/google/wireless/android/video/magma/proto/UserConfiguration;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/store/ConfigurationStore$UserConfigurationCallback;->this$0:Lcom/google/android/videos/store/ConfigurationStore;

    move-object/from16 v17, v0

    # getter for: Lcom/google/android/videos/store/ConfigurationStore;->userCountryCodes:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static/range {v17 .. v17}, Lcom/google/android/videos/store/ConfigurationStore;->access$200(Lcom/google/android/videos/store/ConfigurationStore;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v17

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/videos/api/UserConfigGetRequest;->account:Ljava/lang/String;

    move-object/from16 v18, v0

    iget-object v0, v11, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->countryCode:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v17 .. v19}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    new-instance v16, Landroid/content/ContentValues;

    invoke-direct/range {v16 .. v16}, Landroid/content/ContentValues;-><init>()V

    .line 255
    .local v16, "values":Landroid/content/ContentValues;
    const-wide/16 v12, 0x0

    .line 256
    .local v12, "partnerIds":J
    iget-object v4, v11, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->accountLink:[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    .local v4, "arr$":[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;
    array-length v8, v4

    .local v8, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_0
    if-ge v7, v8, :cond_1

    aget-object v9, v4, v7

    .line 257
    .local v9, "link":Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;
    iget-object v0, v9, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->partner:Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v10, v0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;->id:I

    .line 258
    .local v10, "partnerId":I
    iget-boolean v0, v9, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->linked:Z

    move/from16 v17, v0

    if-eqz v17, :cond_0

    # invokes: Lcom/google/android/videos/store/ConfigurationStore;->isValidPartnerId(I)Z
    invoke-static {v10}, Lcom/google/android/videos/store/ConfigurationStore;->access$400(I)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 259
    # invokes: Lcom/google/android/videos/store/ConfigurationStore;->getPartnerFlag(I)J
    invoke-static {v10}, Lcom/google/android/videos/store/ConfigurationStore;->access$500(I)J

    move-result-wide v18

    or-long v12, v12, v18

    .line 256
    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 262
    .end local v9    # "link":Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;
    .end local v10    # "partnerId":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/store/ConfigurationStore$UserConfigurationCallback;->this$0:Lcom/google/android/videos/store/ConfigurationStore;

    move-object/from16 v17, v0

    # getter for: Lcom/google/android/videos/store/ConfigurationStore;->userAccountLinks:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static/range {v17 .. v17}, Lcom/google/android/videos/store/ConfigurationStore;->access$300(Lcom/google/android/videos/store/ConfigurationStore;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v17

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/videos/api/UserConfigGetRequest;->account:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    invoke-virtual/range {v17 .. v19}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    const/4 v6, 0x0

    .line 265
    .local v6, "exception":Landroid/database/sqlite/SQLiteException;
    :try_start_0
    const-string v17, "config_account"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/videos/api/UserConfigGetRequest;->account:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v16 .. v18}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    const-string v17, "config_play_country"

    iget-object v0, v11, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->countryCode:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v16 .. v18}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    const-string v17, "config_proto"

    invoke-static {v11}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v18

    invoke-virtual/range {v16 .. v18}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 268
    const-string v17, "account_links"

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    invoke-virtual/range {v16 .. v18}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 269
    const-string v17, "user_configuration"

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v16

    invoke-virtual {v15, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 270
    const/4 v14, 0x1

    .line 274
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/store/ConfigurationStore$UserConfigurationCallback;->this$0:Lcom/google/android/videos/store/ConfigurationStore;

    move-object/from16 v17, v0

    # getter for: Lcom/google/android/videos/store/ConfigurationStore;->database:Lcom/google/android/videos/store/Database;
    invoke-static/range {v17 .. v17}, Lcom/google/android/videos/store/ConfigurationStore;->access$100(Lcom/google/android/videos/store/ConfigurationStore;)Lcom/google/android/videos/store/Database;

    move-result-object v17

    const/16 v18, 0x0

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/videos/api/UserConfigGetRequest;->account:Ljava/lang/String;

    move-object/from16 v21, v0

    aput-object v21, v19, v20

    move-object/from16 v0, v17

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v15, v14, v1, v2}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 276
    :goto_1
    if-eqz v14, :cond_2

    .line 277
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/store/ConfigurationStore$UserConfigurationCallback;->callback:Lcom/google/android/videos/async/Callback;

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/videos/api/UserConfigGetRequest;->account:Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-interface/range {v17 .. v19}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 281
    :goto_2
    return-void

    .line 271
    :catch_0
    move-exception v5

    .line 272
    .local v5, "e":Landroid/database/sqlite/SQLiteException;
    move-object v6, v5

    .line 274
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/store/ConfigurationStore$UserConfigurationCallback;->this$0:Lcom/google/android/videos/store/ConfigurationStore;

    move-object/from16 v17, v0

    # getter for: Lcom/google/android/videos/store/ConfigurationStore;->database:Lcom/google/android/videos/store/Database;
    invoke-static/range {v17 .. v17}, Lcom/google/android/videos/store/ConfigurationStore;->access$100(Lcom/google/android/videos/store/ConfigurationStore;)Lcom/google/android/videos/store/Database;

    move-result-object v17

    const/16 v18, 0x0

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/videos/api/UserConfigGetRequest;->account:Ljava/lang/String;

    move-object/from16 v21, v0

    aput-object v21, v19, v20

    move-object/from16 v0, v17

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v15, v14, v1, v2}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    goto :goto_1

    .end local v5    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/store/ConfigurationStore$UserConfigurationCallback;->this$0:Lcom/google/android/videos/store/ConfigurationStore;

    move-object/from16 v18, v0

    # getter for: Lcom/google/android/videos/store/ConfigurationStore;->database:Lcom/google/android/videos/store/Database;
    invoke-static/range {v18 .. v18}, Lcom/google/android/videos/store/ConfigurationStore;->access$100(Lcom/google/android/videos/store/ConfigurationStore;)Lcom/google/android/videos/store/Database;

    move-result-object v18

    const/16 v19, 0x0

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/videos/api/UserConfigGetRequest;->account:Ljava/lang/String;

    move-object/from16 v22, v0

    aput-object v22, v20, v21

    move-object/from16 v0, v18

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v15, v14, v1, v2}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v17

    .line 279
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/store/ConfigurationStore$UserConfigurationCallback;->callback:Lcom/google/android/videos/async/Callback;

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/videos/api/UserConfigGetRequest;->account:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-interface {v0, v1, v6}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_2
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 239
    check-cast p1, Lcom/google/android/videos/api/UserConfigGetRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/store/ConfigurationStore$UserConfigurationCallback;->onResponse(Lcom/google/android/videos/api/UserConfigGetRequest;Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;)V

    return-void
.end method
