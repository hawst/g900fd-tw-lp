.class public Lcom/google/android/videos/ui/playnext/MovieClusterItemView$Binder;
.super Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;
.source "MovieClusterItemView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/playnext/MovieClusterItemView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Binder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder",
        "<",
        "Lcom/google/android/videos/ui/playnext/MovieClusterItemView;",
        "Lcom/google/android/videos/adapter/MoviesDataSource;",
        ">;"
    }
.end annotation


# instance fields
.field private final allowDownloads:Z

.field private final posterRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/videos/async/Requester;ZLcom/google/android/videos/logging/UiElementNode;)V
    .locals 1
    .param p2, "allowDownloads"    # Z
    .param p3, "clusterUiElementNode"    # Lcom/google/android/videos/logging/UiElementNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;Z",
            "Lcom/google/android/videos/logging/UiElementNode;",
            ")V"
        }
    .end annotation

    .prologue
    .line 180
    .local p1, "posterRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    const/4 v0, 0x2

    invoke-direct {p0, p3, v0}, Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;-><init>(Lcom/google/android/videos/logging/UiElementNode;I)V

    .line 181
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView$Binder;->posterRequester:Lcom/google/android/videos/async/Requester;

    .line 182
    iput-boolean p2, p0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView$Binder;->allowDownloads:Z

    .line 183
    return-void
.end method


# virtual methods
.method protected bridge synthetic onBind(Lcom/google/android/videos/ui/playnext/ClusterItemView;Lcom/google/android/videos/adapter/DataSource;I)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/ui/playnext/ClusterItemView;
    .param p2, "x1"    # Lcom/google/android/videos/adapter/DataSource;
    .param p3, "x2"    # I

    .prologue
    .line 172
    check-cast p1, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;

    .end local p1    # "x0":Lcom/google/android/videos/ui/playnext/ClusterItemView;
    check-cast p2, Lcom/google/android/videos/adapter/MoviesDataSource;

    .end local p2    # "x1":Lcom/google/android/videos/adapter/DataSource;
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/videos/ui/playnext/MovieClusterItemView$Binder;->onBind(Lcom/google/android/videos/ui/playnext/MovieClusterItemView;Lcom/google/android/videos/adapter/MoviesDataSource;I)V

    return-void
.end method

.method protected onBind(Lcom/google/android/videos/ui/playnext/MovieClusterItemView;Lcom/google/android/videos/adapter/MoviesDataSource;I)V
    .locals 6
    .param p1, "view"    # Lcom/google/android/videos/ui/playnext/MovieClusterItemView;
    .param p2, "dataSource"    # Lcom/google/android/videos/adapter/MoviesDataSource;
    .param p3, "index"    # I

    .prologue
    .line 187
    iget-object v3, p0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView$Binder;->posterRequester:Lcom/google/android/videos/async/Requester;

    iget-boolean v4, p0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView$Binder;->allowDownloads:Z

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->bind(Lcom/google/android/videos/adapter/MoviesDataSource;ILcom/google/android/videos/async/Requester;ZLcom/google/android/videos/ui/playnext/MovieClusterItemView$Binder;)V

    .line 188
    return-void
.end method
