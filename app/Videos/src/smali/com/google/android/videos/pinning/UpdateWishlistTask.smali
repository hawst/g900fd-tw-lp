.class Lcom/google/android/videos/pinning/UpdateWishlistTask;
.super Lcom/google/android/videos/pinning/Task;
.source "UpdateWishlistTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/pinning/UpdateWishlistTask$WishlistQuery;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/pinning/Task",
        "<",
        "Lcom/google/android/videos/pinning/UpdateWishlistKey;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

.field private final database:Lcom/google/android/videos/store/Database;

.field private final updateWishlistRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/UpdateWishlistRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/videos/pinning/UpdateWishlistKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/videos/pinning/Task$Listener;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/logging/EventLogger;)V
    .locals 7
    .param p1, "key"    # Lcom/google/android/videos/pinning/UpdateWishlistKey;
    .param p2, "wakeLock"    # Landroid/os/PowerManager$WakeLock;
    .param p3, "wifiLock"    # Landroid/net/wifi/WifiManager$WifiLock;
    .param p4, "listener"    # Lcom/google/android/videos/pinning/Task$Listener;
    .param p5, "database"    # Lcom/google/android/videos/store/Database;
    .param p6, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .param p8, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/pinning/UpdateWishlistKey;",
            "Landroid/os/PowerManager$WakeLock;",
            "Landroid/net/wifi/WifiManager$WifiLock;",
            "Lcom/google/android/videos/pinning/Task$Listener;",
            "Lcom/google/android/videos/store/Database;",
            "Lcom/google/android/videos/accounts/AccountManagerWrapper;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/UpdateWishlistRequest;",
            "Ljava/lang/Void;",
            ">;",
            "Lcom/google/android/videos/logging/EventLogger;",
            ")V"
        }
    .end annotation

    .prologue
    .line 44
    .local p7, "updateWishlistRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/UpdateWishlistRequest;Ljava/lang/Void;>;"
    const/4 v1, 0x4

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p8

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/pinning/Task;-><init>(ILcom/google/android/videos/pinning/Task$Key;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/videos/pinning/Task$Listener;Lcom/google/android/videos/logging/EventLogger;)V

    .line 45
    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/Database;

    iput-object v0, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->database:Lcom/google/android/videos/store/Database;

    .line 46
    invoke-static {p6}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/accounts/AccountManagerWrapper;

    iput-object v0, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    .line 48
    invoke-static {p7}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->updateWishlistRequester:Lcom/google/android/videos/async/Requester;

    .line 49
    return-void
.end method

.method private failSilently()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 126
    const-string v1, "wishlist_account = ? AND wishlist_item_type = ? AND wishlist_item_id = ?"

    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/videos/pinning/UpdateWishlistKey;

    iget-object v0, v0, Lcom/google/android/videos/pinning/UpdateWishlistKey;->account:Ljava/lang/String;

    aput-object v0, v2, v4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/videos/pinning/UpdateWishlistKey;

    iget v0, v0, Lcom/google/android/videos/pinning/UpdateWishlistKey;->itemType:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v3, 0x2

    iget-object v0, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/videos/pinning/UpdateWishlistKey;

    iget-object v0, v0, Lcom/google/android/videos/pinning/UpdateWishlistKey;->itemId:Ljava/lang/String;

    aput-object v0, v2, v3

    invoke-direct {p0, v1, v2, v4, v4}, Lcom/google/android/videos/pinning/UpdateWishlistTask;->removeItem(Ljava/lang/String;[Ljava/lang/String;ZZ)V

    .line 128
    return-void
.end method

.method private onAddedToWishlist()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 103
    iget-object v3, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v3}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 104
    .local v1, "transaction":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v0, 0x0

    .line 106
    .local v0, "success":Z
    :try_start_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 107
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "wishlist_item_state"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 108
    const-string v4, "wishlist"

    const-string v5, "wishlist_account = ? AND wishlist_item_type = ? AND wishlist_item_id = ? AND wishlist_item_state = 2"

    const/4 v3, 0x3

    new-array v6, v3, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v3, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v3, Lcom/google/android/videos/pinning/UpdateWishlistKey;

    iget-object v3, v3, Lcom/google/android/videos/pinning/UpdateWishlistKey;->account:Ljava/lang/String;

    aput-object v3, v6, v7

    const/4 v7, 0x1

    iget-object v3, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v3, Lcom/google/android/videos/pinning/UpdateWishlistKey;

    iget v3, v3, Lcom/google/android/videos/pinning/UpdateWishlistKey;->itemType:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v7

    const/4 v7, 0x2

    iget-object v3, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v3, Lcom/google/android/videos/pinning/UpdateWishlistKey;

    iget-object v3, v3, Lcom/google/android/videos/pinning/UpdateWishlistKey;->itemId:Ljava/lang/String;

    aput-object v3, v6, v7

    invoke-virtual {v1, v4, v2, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 112
    iget-object v3, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v3, Lcom/google/android/videos/pinning/UpdateWishlistKey;

    iget-object v3, v3, Lcom/google/android/videos/pinning/UpdateWishlistKey;->account:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v1, v3, v4}, Lcom/google/android/videos/store/WishlistStoreSync;->storeSnapshotToken(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113
    const/4 v0, 0x1

    .line 115
    iget-object v3, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->database:Lcom/google/android/videos/store/Database;

    new-array v4, v8, [Ljava/lang/Object;

    invoke-virtual {v3, v1, v0, v8, v4}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 117
    return-void

    .line 115
    .end local v2    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->database:Lcom/google/android/videos/store/Database;

    new-array v5, v8, [Ljava/lang/Object;

    invoke-virtual {v4, v1, v0, v8, v5}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v3
.end method

.method private onRemovedFromWishlist()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 120
    const-string v1, "wishlist_account = ? AND wishlist_item_type = ? AND wishlist_item_id = ? AND wishlist_item_state = 3"

    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/videos/pinning/UpdateWishlistKey;

    iget-object v0, v0, Lcom/google/android/videos/pinning/UpdateWishlistKey;->account:Ljava/lang/String;

    aput-object v0, v2, v4

    iget-object v0, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/videos/pinning/UpdateWishlistKey;

    iget v0, v0, Lcom/google/android/videos/pinning/UpdateWishlistKey;->itemType:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    const/4 v3, 0x2

    iget-object v0, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/videos/pinning/UpdateWishlistKey;

    iget-object v0, v0, Lcom/google/android/videos/pinning/UpdateWishlistKey;->itemId:Ljava/lang/String;

    aput-object v0, v2, v3

    invoke-direct {p0, v1, v2, v5, v4}, Lcom/google/android/videos/pinning/UpdateWishlistTask;->removeItem(Ljava/lang/String;[Ljava/lang/String;ZZ)V

    .line 123
    return-void
.end method

.method private removeItem(Ljava/lang/String;[Ljava/lang/String;ZZ)V
    .locals 8
    .param p1, "whereClause"    # Ljava/lang/String;
    .param p2, "whereArgs"    # [Ljava/lang/String;
    .param p3, "clearSnapshotToken"    # Z
    .param p4, "notify"    # Z

    .prologue
    const/4 v3, 0x2

    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 132
    iget-object v2, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v2}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 133
    .local v1, "transaction":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v0, 0x0

    .line 135
    .local v0, "success":Z
    :try_start_0
    const-string v2, "wishlist"

    invoke-virtual {v1, v2, p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 136
    if-eqz p3, :cond_0

    .line 137
    iget-object v2, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v2, Lcom/google/android/videos/pinning/UpdateWishlistKey;

    iget-object v2, v2, Lcom/google/android/videos/pinning/UpdateWishlistKey;->account:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v1, v2, v5}, Lcom/google/android/videos/store/WishlistStoreSync;->storeSnapshotToken(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139
    :cond_0
    const/4 v0, 0x1

    .line 141
    iget-object v5, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->database:Lcom/google/android/videos/store/Database;

    if-eqz p4, :cond_1

    :goto_0
    new-array v6, v7, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v2, Lcom/google/android/videos/pinning/UpdateWishlistKey;

    iget-object v2, v2, Lcom/google/android/videos/pinning/UpdateWishlistKey;->account:Ljava/lang/String;

    aput-object v2, v6, v4

    invoke-virtual {v5, v1, v0, v3, v6}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 144
    return-void

    :cond_1
    move v3, v4

    .line 141
    goto :goto_0

    :catchall_0
    move-exception v2

    move-object v5, v2

    iget-object v6, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->database:Lcom/google/android/videos/store/Database;

    if-eqz p4, :cond_2

    :goto_1
    new-array v7, v7, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v2, Lcom/google/android/videos/pinning/UpdateWishlistKey;

    iget-object v2, v2, Lcom/google/android/videos/pinning/UpdateWishlistKey;->account:Ljava/lang/String;

    aput-object v2, v7, v4

    invoke-virtual {v6, v1, v0, v3, v7}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v5

    :cond_2
    move v3, v4

    goto :goto_1
.end method

.method private updateWishlist(Z)V
    .locals 6
    .param p1, "wishlisted"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/Task$TaskException;
        }
    .end annotation

    .prologue
    .line 80
    iget-object v4, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v4, Lcom/google/android/videos/pinning/UpdateWishlistKey;

    iget v5, v4, Lcom/google/android/videos/pinning/UpdateWishlistKey;->itemType:I

    iget-object v4, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v4, Lcom/google/android/videos/pinning/UpdateWishlistKey;

    iget-object v4, v4, Lcom/google/android/videos/pinning/UpdateWishlistKey;->itemId:Ljava/lang/String;

    invoke-static {v5, v4}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromAssetTypeAndId(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 81
    .local v0, "assetResourceId":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 82
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unexpected itemType: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v4, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v4, Lcom/google/android/videos/pinning/UpdateWishlistKey;

    iget v4, v4, Lcom/google/android/videos/pinning/UpdateWishlistKey;->itemType:I

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 83
    invoke-direct {p0}, Lcom/google/android/videos/pinning/UpdateWishlistTask;->failSilently()V

    .line 100
    :goto_0
    return-void

    .line 86
    :cond_0
    new-instance v3, Lcom/google/android/videos/api/UpdateWishlistRequest;

    iget-object v4, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v4, Lcom/google/android/videos/pinning/UpdateWishlistKey;

    iget-object v4, v4, Lcom/google/android/videos/pinning/UpdateWishlistKey;->account:Ljava/lang/String;

    invoke-direct {v3, v4, v0, p1}, Lcom/google/android/videos/api/UpdateWishlistRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 88
    .local v3, "request":Lcom/google/android/videos/api/UpdateWishlistRequest;
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v1

    .line 89
    .local v1, "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/api/UpdateWishlistRequest;Ljava/lang/Void;>;"
    iget-object v4, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->updateWishlistRequester:Lcom/google/android/videos/async/Requester;

    invoke-interface {v4, v3, v1}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 91
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;

    .line 92
    if-eqz p1, :cond_1

    .line 93
    invoke-direct {p0}, Lcom/google/android/videos/pinning/UpdateWishlistTask;->onAddedToWishlist()V
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 97
    :catch_0
    move-exception v2

    .line 98
    .local v2, "e":Ljava/util/concurrent/ExecutionException;
    new-instance v4, Lcom/google/android/videos/pinning/Task$TaskException;

    const-string v5, "request failed"

    invoke-direct {v4, v5, v2}, Lcom/google/android/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    .line 95
    .end local v2    # "e":Ljava/util/concurrent/ExecutionException;
    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/videos/pinning/UpdateWishlistTask;->onRemovedFromWishlist()V
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method


# virtual methods
.method public execute()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/Task$TaskException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 53
    iget-object v2, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    iget-object v1, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v1, Lcom/google/android/videos/pinning/UpdateWishlistKey;

    iget-object v1, v1, Lcom/google/android/videos/pinning/UpdateWishlistKey;->account:Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->accountExists(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 55
    invoke-direct {p0}, Lcom/google/android/videos/pinning/UpdateWishlistTask;->failSilently()V

    .line 77
    :goto_0
    return-void

    .line 59
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v1}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 60
    .local v0, "sqlDatabase":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "wishlist"

    sget-object v2, Lcom/google/android/videos/pinning/UpdateWishlistTask$WishlistQuery;->COLUMNS:[Ljava/lang/String;

    const-string v3, "wishlist_account = ? AND wishlist_item_type = ? AND wishlist_item_id = ?"

    new-array v4, v12, [Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/videos/pinning/UpdateWishlistKey;

    iget-object v6, v6, Lcom/google/android/videos/pinning/UpdateWishlistKey;->account:Ljava/lang/String;

    aput-object v6, v4, v7

    iget-object v6, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/videos/pinning/UpdateWishlistKey;

    iget v6, v6, Lcom/google/android/videos/pinning/UpdateWishlistKey;->itemType:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v10

    iget-object v6, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/videos/pinning/UpdateWishlistKey;

    iget-object v6, v6, Lcom/google/android/videos/pinning/UpdateWishlistKey;->itemId:Ljava/lang/String;

    aput-object v6, v4, v11

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 64
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 65
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 66
    .local v9, "itemState":I
    if-ne v9, v12, :cond_2

    .line 67
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/videos/pinning/UpdateWishlistTask;->updateWishlist(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75
    .end local v9    # "itemState":I
    :cond_1
    :goto_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 68
    .restart local v9    # "itemState":I
    :cond_2
    if-ne v9, v11, :cond_1

    .line 69
    const/4 v1, 0x1

    :try_start_1
    invoke-direct {p0, v1}, Lcom/google/android/videos/pinning/UpdateWishlistTask;->updateWishlist(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 75
    .end local v9    # "itemState":I
    :catchall_0
    move-exception v1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method protected onCompleted()V
    .locals 0

    .prologue
    .line 149
    return-void
.end method

.method protected onError(Ljava/lang/Throwable;ZZ)V
    .locals 6
    .param p1, "exception"    # Ljava/lang/Throwable;
    .param p2, "maxRetries"    # Z
    .param p3, "fatal"    # Z

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 154
    if-nez p2, :cond_0

    if-eqz p3, :cond_1

    .line 155
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "too many failures "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 156
    const-string v1, "wishlist_account = ? AND wishlist_item_type = ? AND wishlist_item_id = ?"

    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/videos/pinning/UpdateWishlistKey;

    iget-object v0, v0, Lcom/google/android/videos/pinning/UpdateWishlistKey;->account:Ljava/lang/String;

    aput-object v0, v2, v4

    iget-object v0, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/videos/pinning/UpdateWishlistKey;

    iget v0, v0, Lcom/google/android/videos/pinning/UpdateWishlistKey;->itemType:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    const/4 v3, 0x2

    iget-object v0, p0, Lcom/google/android/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/videos/pinning/UpdateWishlistKey;

    iget-object v0, v0, Lcom/google/android/videos/pinning/UpdateWishlistKey;->itemId:Ljava/lang/String;

    aput-object v0, v2, v3

    invoke-direct {p0, v1, v2, v4, v5}, Lcom/google/android/videos/pinning/UpdateWishlistTask;->removeItem(Ljava/lang/String;[Ljava/lang/String;ZZ)V

    .line 159
    :cond_1
    return-void
.end method
