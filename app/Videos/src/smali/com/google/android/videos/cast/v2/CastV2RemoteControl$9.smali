.class Lcom/google/android/videos/cast/v2/CastV2RemoteControl$9;
.super Ljava/lang/Object;
.source "CastV2RemoteControl.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->requestRecommendations(Ljava/lang/String;Lcom/google/android/videos/remote/RemoteVideoInfo;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/api/RecommendationsRequest;",
        "Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

.field final synthetic val$videoId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 887
    iput-object p1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$9;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    iput-object p2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$9;->val$videoId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/api/RecommendationsRequest;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "request"    # Lcom/google/android/videos/api/RecommendationsRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 899
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 887
    check-cast p1, Lcom/google/android/videos/api/RecommendationsRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$9;->onError(Lcom/google/android/videos/api/RecommendationsRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/api/RecommendationsRequest;Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/api/RecommendationsRequest;
    .param p2, "response"    # Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;

    .prologue
    .line 891
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$9;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$9;->val$videoId:Ljava/lang/String;

    # invokes: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->isPlaying(Ljava/lang/String;)Z
    invoke-static {v0, v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$2000(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 892
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$9;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    iget-object v1, p2, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # invokes: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->sendRecommendations([Lcom/google/wireless/android/video/magma/proto/AssetResource;)V
    invoke-static {v0, v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$2100(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;[Lcom/google/wireless/android/video/magma/proto/AssetResource;)V

    .line 894
    :cond_0
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 887
    check-cast p1, Lcom/google/android/videos/api/RecommendationsRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$9;->onResponse(Lcom/google/android/videos/api/RecommendationsRequest;Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;)V

    return-void
.end method
