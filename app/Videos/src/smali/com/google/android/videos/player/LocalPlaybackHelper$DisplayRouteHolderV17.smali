.class final Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;
.super Ljava/lang/Object;
.source "LocalPlaybackHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/LocalPlaybackHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "DisplayRouteHolderV17"
.end annotation


# instance fields
.field private final display:Landroid/view/Display;

.field private final displayRoute:Landroid/media/MediaRouter$RouteInfo;

.field final synthetic this$0:Lcom/google/android/videos/player/LocalPlaybackHelper;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/player/LocalPlaybackHelper;Landroid/media/MediaRouter$RouteInfo;)V
    .locals 1
    .param p2, "displayRoute"    # Landroid/media/MediaRouter$RouteInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/player/PlaybackHelper$DisplayNotFoundException;
        }
    .end annotation

    .prologue
    .line 1353
    iput-object p1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->this$0:Lcom/google/android/videos/player/LocalPlaybackHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1354
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    iput-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->displayRoute:Landroid/media/MediaRouter$RouteInfo;

    .line 1355
    invoke-virtual {p2}, Landroid/media/MediaRouter$RouteInfo;->getPresentationDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->display:Landroid/view/Display;

    .line 1356
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->display:Landroid/view/Display;

    if-nez v0, :cond_0

    .line 1357
    new-instance v0, Lcom/google/android/videos/player/PlaybackHelper$DisplayNotFoundException;

    invoke-direct {v0}, Lcom/google/android/videos/player/PlaybackHelper$DisplayNotFoundException;-><init>()V

    throw v0

    .line 1359
    :cond_0
    return-void
.end method

.method static synthetic access$300(Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;)Landroid/view/Display;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;

    .prologue
    .line 1347
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->display:Landroid/view/Display;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;)Landroid/media/MediaRouter$RouteInfo;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;

    .prologue
    .line 1347
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->displayRoute:Landroid/media/MediaRouter$RouteInfo;

    return-object v0
.end method


# virtual methods
.method public getDisplayFlags()I
    .locals 1

    .prologue
    .line 1371
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->display:Landroid/view/Display;

    invoke-virtual {v0}, Landroid/view/Display;->getFlags()I

    move-result v0

    return v0
.end method

.method public initLocalPlaybackViewHolder(Lcom/google/android/videos/tagging/KnowledgeView;)Lcom/google/android/videos/player/LocalPlaybackViewHolder;
    .locals 4
    .param p1, "localKnowledgeOverlay"    # Lcom/google/android/videos/tagging/KnowledgeView;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/player/PlaybackHelper$DisplayNotFoundException;
        }
    .end annotation

    .prologue
    .line 1363
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->display:Landroid/view/Display;

    invoke-virtual {v1}, Landroid/view/Display;->getName()Ljava/lang/String;

    move-result-object v0

    .line 1364
    .local v0, "displayName":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->this$0:Lcom/google/android/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/videos/player/LocalPlaybackHelper;->remoteScreenPanelHelper:Lcom/google/android/videos/ui/RemoteScreenPanelHelper;
    invoke-static {v1}, Lcom/google/android/videos/player/LocalPlaybackHelper;->access$1100(Lcom/google/android/videos/player/LocalPlaybackHelper;)Lcom/google/android/videos/ui/RemoteScreenPanelHelper;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1365
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->this$0:Lcom/google/android/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/videos/player/LocalPlaybackHelper;->remoteScreenPanelHelper:Lcom/google/android/videos/ui/RemoteScreenPanelHelper;
    invoke-static {v1}, Lcom/google/android/videos/player/LocalPlaybackHelper;->access$1100(Lcom/google/android/videos/player/LocalPlaybackHelper;)Lcom/google/android/videos/ui/RemoteScreenPanelHelper;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->this$0:Lcom/google/android/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/videos/player/LocalPlaybackHelper;->videoId:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/videos/player/LocalPlaybackHelper;->access$1200(Lcom/google/android/videos/player/LocalPlaybackHelper;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->init(Ljava/lang/String;Ljava/lang/String;)V

    .line 1367
    :cond_0
    new-instance v1, Lcom/google/android/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;

    iget-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->this$0:Lcom/google/android/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/google/android/videos/player/LocalPlaybackHelper;->access$600(Lcom/google/android/videos/player/LocalPlaybackHelper;)Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->display:Landroid/view/Display;

    invoke-direct {v1, v2, v3, p1}, Lcom/google/android/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;-><init>(Landroid/app/Activity;Landroid/view/Display;Lcom/google/android/videos/tagging/KnowledgeView;)V

    return-object v1
.end method
