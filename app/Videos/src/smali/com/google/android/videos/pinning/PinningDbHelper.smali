.class public Lcom/google/android/videos/pinning/PinningDbHelper;
.super Ljava/lang/Object;
.source "PinningDbHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetailsQuery;,
        Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;
    }
.end annotation


# direct methods
.method public static clearError(Lcom/google/android/videos/store/Database;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 13
    .param p0, "database"    # Lcom/google/android/videos/store/Database;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    const/16 v0, 0xb

    const/4 v12, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 237
    const/4 v2, 0x0

    .line 238
    .local v2, "success":Z
    const/4 v4, 0x0

    .line 239
    .local v4, "updated":I
    invoke-virtual {p0}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 241
    .local v3, "transaction":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 242
    .local v5, "values":Landroid/content/ContentValues;
    const-string v8, "pinning_status"

    invoke-virtual {v5, v8}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 243
    const-string v8, "pinning_status_reason"

    invoke-virtual {v5, v8}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 244
    const-string v8, "pinning_drm_error_code"

    invoke-virtual {v5, v8}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 245
    const-string v8, "purchased_assets"

    const-string v9, "account = ? AND asset_type IN (6,20) AND asset_id = ? AND NOT (pinned IS NOT NULL AND pinned > 0) AND pinning_status = 4"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object p1, v10, v11

    const/4 v11, 0x1

    aput-object p2, v10, v11

    invoke-virtual {v3, v8, v5, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    .line 248
    const/4 v2, 0x1

    .line 250
    if-lez v4, :cond_0

    .line 251
    .local v0, "event":I
    :goto_0
    if-nez v0, :cond_1

    .line 252
    .local v1, "eventArgs":[Ljava/lang/Object;
    :goto_1
    invoke-virtual {p0, v3, v2, v0, v1}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 254
    if-lez v4, :cond_4

    :goto_2
    return v6

    .end local v0    # "event":I
    .end local v1    # "eventArgs":[Ljava/lang/Object;
    :cond_0
    move v0, v7

    .line 250
    goto :goto_0

    .line 251
    .restart local v0    # "event":I
    :cond_1
    new-array v1, v12, [Ljava/lang/Object;

    aput-object p1, v1, v7

    aput-object p2, v1, v6

    goto :goto_1

    .line 250
    .end local v0    # "event":I
    .end local v5    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v8

    if-lez v4, :cond_2

    .line 251
    .restart local v0    # "event":I
    :goto_3
    if-nez v0, :cond_3

    .line 252
    .restart local v1    # "eventArgs":[Ljava/lang/Object;
    :goto_4
    invoke-virtual {p0, v3, v2, v0, v1}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 253
    throw v8

    .end local v0    # "event":I
    .end local v1    # "eventArgs":[Ljava/lang/Object;
    :cond_2
    move v0, v7

    .line 250
    goto :goto_3

    .line 251
    .restart local v0    # "event":I
    :cond_3
    new-array v1, v12, [Ljava/lang/Object;

    aput-object p1, v1, v7

    aput-object p2, v1, v6

    goto :goto_4

    .restart local v1    # "eventArgs":[Ljava/lang/Object;
    .restart local v5    # "values":Landroid/content/ContentValues;
    :cond_4
    move v6, v7

    .line 254
    goto :goto_2
.end method

.method public static clearHaveSubtitles(Lcom/google/android/videos/store/Database;Ljava/lang/String;)V
    .locals 8
    .param p0, "database"    # Lcom/google/android/videos/store/Database;
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 258
    const/4 v0, 0x0

    .line 259
    .local v0, "success":Z
    invoke-virtual {p0}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 261
    .local v1, "transaction":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 262
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "have_subtitles"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 263
    const-string v3, "purchased_assets"

    const-string v4, "asset_type IN (6,20) AND asset_id = ?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-virtual {v1, v3, v2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 265
    const/4 v0, 0x1

    .line 267
    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {p0, v1, v0, v7, v3}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 269
    return-void

    .line 267
    .end local v2    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v3

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {p0, v1, v0, v7, v4}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v3
.end method

.method public static getClearDownloadContentValues()Landroid/content/ContentValues;
    .locals 3

    .prologue
    .line 154
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 155
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "download_quality"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 156
    const-string v1, "external_storage_index"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 157
    const-string v1, "license_release_pending"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 158
    return-object v0
.end method

.method public static getClearedLicenseContentValues()Landroid/content/ContentValues;
    .locals 3

    .prologue
    .line 133
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 134
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "license_type"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 135
    const-string v1, "license_activation"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 136
    const-string v1, "license_force_sync"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 137
    const-string v1, "license_last_synced_timestamp"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 138
    const-string v1, "license_expiration_timestamp"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 139
    const-string v1, "license_file_path_key"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 140
    const-string v1, "license_key_id"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 141
    const-string v1, "license_asset_id"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 142
    const-string v1, "license_system_id"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 143
    const-string v1, "license_cenc_key_set_id"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 144
    const-string v1, "license_cenc_pssh_data"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 145
    const-string v1, "license_cenc_security_level"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 146
    const-string v1, "license_cenc_mimetype"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 147
    const-string v1, "download_relative_filepath"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 148
    const-string v1, "download_extra_proto"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 149
    const-string v1, "license_release_pending"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 150
    return-object v0
.end method

.method public static getDownloadDetails(Lcom/google/android/videos/store/Database;Lcom/google/android/videos/pinning/DownloadKey;)Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;
    .locals 24
    .param p0, "database"    # Lcom/google/android/videos/store/Database;
    .param p1, "key"    # Lcom/google/android/videos/pinning/DownloadKey;

    .prologue
    .line 61
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 62
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v3, "purchased_assets, videos ON asset_type IN (6,20) AND asset_id = video_id"

    sget-object v4, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetailsQuery;->PROJECTION:[Ljava/lang/String;

    const-string v5, "account = ? AND asset_type IN (6,20) AND asset_id = ?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/android/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 66
    .local v19, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    .line 67
    const/4 v3, 0x0

    .line 125
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v3

    .line 70
    :cond_0
    const/4 v3, 0x4

    const/4 v4, 0x0

    :try_start_1
    move-object/from16 v0, v19

    invoke-static {v0, v3, v4}, Lcom/google/android/videos/utils/DbUtils;->getIntOrDefault(Landroid/database/Cursor;II)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v5

    .line 74
    .local v5, "licenseType":I
    const/4 v3, 0x3

    :try_start_2
    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z
    :try_end_2
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v3

    if-eqz v3, :cond_3

    const/4 v14, 0x0

    .line 80
    .local v14, "extra":Lcom/google/android/videos/proto/DownloadExtra;
    :goto_1
    if-nez v14, :cond_1

    .line 83
    :try_start_3
    new-instance v22, Lcom/google/android/videos/proto/StreamInfo;

    invoke-direct/range {v22 .. v22}, Lcom/google/android/videos/proto/StreamInfo;-><init>()V

    .line 84
    .local v22, "streamInfo":Lcom/google/android/videos/proto/StreamInfo;
    const/4 v3, 0x1

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    move-object/from16 v0, v22

    iput-wide v6, v0, Lcom/google/android/videos/proto/StreamInfo;->sizeInBytes:J

    .line 85
    const/16 v3, 0xf

    const/4 v4, -0x1

    move-object/from16 v0, v19

    invoke-static {v0, v3, v4}, Lcom/google/android/videos/utils/DbUtils;->getIntOrDefault(Landroid/database/Cursor;II)I

    move-result v3

    move-object/from16 v0, v22

    iput v3, v0, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    .line 87
    const/16 v3, 0xe

    move-object/from16 v0, v19

    invoke-static {v0, v3}, Lcom/google/android/videos/utils/DbUtils;->getDateAsTimestamp(Landroid/database/Cursor;I)J

    move-result-wide v6

    move-object/from16 v0, v22

    iput-wide v6, v0, Lcom/google/android/videos/proto/StreamInfo;->lastModifiedTimestamp:J

    .line 89
    new-instance v14, Lcom/google/android/videos/proto/DownloadExtra;

    .end local v14    # "extra":Lcom/google/android/videos/proto/DownloadExtra;
    invoke-direct {v14}, Lcom/google/android/videos/proto/DownloadExtra;-><init>()V

    .line 90
    .restart local v14    # "extra":Lcom/google/android/videos/proto/DownloadExtra;
    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/android/videos/proto/StreamInfo;

    const/4 v4, 0x0

    aput-object v22, v3, v4

    iput-object v3, v14, Lcom/google/android/videos/proto/DownloadExtra;->streamInfos:[Lcom/google/android/videos/proto/StreamInfo;

    .line 93
    .end local v22    # "streamInfo":Lcom/google/android/videos/proto/StreamInfo;
    :cond_1
    const/4 v3, 0x5

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v15, 0x0

    .line 95
    .local v15, "activation":Lcom/google/android/videos/drm/Activation;
    :goto_2
    const/4 v3, 0x6

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_5

    const/16 v16, 0x0

    .line 97
    .local v16, "cencKeySetId":[B
    :goto_3
    const/4 v3, 0x7

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_6

    const/16 v17, 0x0

    .line 99
    .local v17, "cencPsshData":[B
    :goto_4
    if-nez v17, :cond_2

    if-eqz v14, :cond_2

    .line 101
    iget-object v0, v14, Lcom/google/android/videos/proto/DownloadExtra;->streamInfos:[Lcom/google/android/videos/proto/StreamInfo;

    move-object/from16 v23, v0

    .line 102
    .local v23, "streamInfos":[Lcom/google/android/videos/proto/StreamInfo;
    const/16 v21, 0x0

    .local v21, "i":I
    :goto_5
    move-object/from16 v0, v23

    array-length v3, v0

    move/from16 v0, v21

    if-ge v0, v3, :cond_2

    .line 103
    aget-object v3, v23, v21

    iget-object v3, v3, Lcom/google/android/videos/proto/StreamInfo;->cencPsshDataDEPRECATED:[B

    if-eqz v3, :cond_7

    .line 104
    aget-object v3, v23, v21

    iget-object v0, v3, Lcom/google/android/videos/proto/StreamInfo;->cencPsshDataDEPRECATED:[B

    move-object/from16 v17, v0

    .line 110
    .end local v21    # "i":I
    .end local v23    # "streamInfos":[Lcom/google/android/videos/proto/StreamInfo;
    :cond_2
    new-instance v3, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;

    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/16 v6, 0x9

    move-object/from16 v0, v19

    invoke-static {v0, v6}, Lcom/google/android/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v6

    const/16 v7, 0xa

    move-object/from16 v0, v19

    invoke-static {v0, v7}, Lcom/google/android/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v7

    const/4 v8, 0x2

    move-object/from16 v0, v19

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    const/16 v9, 0xb

    move-object/from16 v0, v19

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    const/16 v10, 0xc

    move-object/from16 v0, v19

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    const/16 v12, 0xd

    move-object/from16 v0, v19

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    const/16 v18, 0x8

    move-object/from16 v0, v19

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    invoke-direct/range {v3 .. v18}, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;-><init>(Ljava/lang/String;IZZIIJJLcom/google/android/videos/proto/DownloadExtra;Lcom/google/android/videos/drm/Activation;[B[BI)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 125
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 74
    .end local v14    # "extra":Lcom/google/android/videos/proto/DownloadExtra;
    .end local v15    # "activation":Lcom/google/android/videos/drm/Activation;
    .end local v16    # "cencKeySetId":[B
    .end local v17    # "cencPsshData":[B
    :cond_3
    const/4 v3, 0x3

    :try_start_4
    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/videos/proto/DownloadExtra;->parseFrom([B)Lcom/google/android/videos/proto/DownloadExtra;
    :try_end_4
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v14

    goto/16 :goto_1

    .line 76
    :catch_0
    move-exception v20

    .line 77
    .local v20, "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    :try_start_5
    const-string v3, "Failed to deserialize DownloadExtra proto"

    move-object/from16 v0, v20

    invoke-static {v3, v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 78
    new-instance v3, Ljava/lang/IllegalArgumentException;

    move-object/from16 v0, v20

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 125
    .end local v5    # "licenseType":I
    .end local v20    # "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    :catchall_0
    move-exception v3

    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    throw v3

    .line 93
    .restart local v5    # "licenseType":I
    .restart local v14    # "extra":Lcom/google/android/videos/proto/DownloadExtra;
    :cond_4
    const/4 v3, 0x5

    :try_start_6
    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/videos/drm/Activation;->fromBytes([B)Lcom/google/android/videos/drm/Activation;

    move-result-object v15

    goto/16 :goto_2

    .line 95
    .restart local v15    # "activation":Lcom/google/android/videos/drm/Activation;
    :cond_5
    const/4 v3, 0x6

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v16

    goto/16 :goto_3

    .line 97
    .restart local v16    # "cencKeySetId":[B
    :cond_6
    const/4 v3, 0x7

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v17

    goto/16 :goto_4

    .line 102
    .restart local v17    # "cencPsshData":[B
    .restart local v21    # "i":I
    .restart local v23    # "streamInfos":[Lcom/google/android/videos/proto/StreamInfo;
    :cond_7
    add-int/lit8 v21, v21, 0x1

    goto/16 :goto_5
.end method

.method public static persistStreamingPsshData(Lcom/google/android/videos/store/Database;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;)Z
    .locals 8
    .param p0, "database"    # Lcom/google/android/videos/store/Database;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "psshData"    # [B
    .param p4, "mimeType"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 273
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 274
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "streaming_pssh_data"

    invoke-virtual {v2, v3, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 275
    const-string v3, "streaming_mimetype"

    invoke-virtual {v2, v3, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    const/4 v0, 0x0

    .line 277
    .local v0, "success":Z
    invoke-virtual {p0}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 279
    .local v1, "transaction":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v3, "purchased_assets"

    const-string v4, "account = ? AND asset_type IN (6,20) AND asset_id = ?"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    aput-object p2, v5, v6

    invoke-virtual {v1, v3, v2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 281
    const/4 v0, 0x1

    .line 283
    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {p0, v1, v0, v7, v3}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 285
    return v0

    .line 283
    :catchall_0
    move-exception v3

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {p0, v1, v0, v7, v4}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v3
.end method

.method public static pin(Lcom/google/android/videos/store/Database;Ljava/lang/String;Ljava/lang/String;II)Z
    .locals 6
    .param p0, "database"    # Lcom/google/android/videos/store/Database;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "quality"    # I
    .param p4, "storage"    # I

    .prologue
    .line 178
    const/4 v3, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/pinning/PinningDbHelper;->setPinned(Lcom/google/android/videos/store/Database;Ljava/lang/String;Ljava/lang/String;ZII)Z

    move-result v0

    return v0
.end method

.method private static setPinned(Lcom/google/android/videos/store/Database;Ljava/lang/String;Ljava/lang/String;ZII)Z
    .locals 10
    .param p0, "database"    # Lcom/google/android/videos/store/Database;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "pin"    # Z
    .param p4, "quality"    # I
    .param p5, "storage"    # I

    .prologue
    .line 199
    const/4 v2, 0x0

    .line 200
    .local v2, "success":Z
    const/4 v4, 0x0

    .line 201
    .local v4, "updated":I
    invoke-virtual {p0}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 203
    .local v3, "transaction":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 204
    .local v5, "values":Landroid/content/ContentValues;
    const-string v7, "pinned"

    if-eqz p3, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    :goto_0
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 205
    const-string v7, "pinning_notification_active"

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 206
    if-eqz p3, :cond_1

    .line 207
    const-string v7, "pinning_status"

    const/4 v8, 0x5

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 208
    const-string v7, "download_quality"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 209
    const-string v7, "external_storage_index"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 214
    :goto_1
    const-string v7, "pinning_status_reason"

    invoke-virtual {v5, v7}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 215
    const-string v7, "pinning_drm_error_code"

    invoke-virtual {v5, v7}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 216
    if-eqz p3, :cond_2

    const-string v6, "account = ? AND asset_type IN (6,20) AND asset_id = ? AND NOT (pinned IS NOT NULL AND pinned > 0)"

    .line 219
    .local v6, "whereClause":Ljava/lang/String;
    :goto_2
    const-string v7, "purchased_assets"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object p1, v8, v9

    const/4 v9, 0x1

    aput-object p2, v8, v9

    invoke-virtual {v3, v7, v5, v6, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    .line 221
    const/4 v2, 0x1

    .line 223
    if-lez v4, :cond_3

    const/16 v0, 0xb

    .line 224
    .local v0, "event":I
    :goto_3
    if-nez v0, :cond_4

    const/4 v1, 0x0

    .line 225
    .local v1, "eventArgs":[Ljava/lang/Object;
    :goto_4
    invoke-virtual {p0, v3, v2, v0, v1}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 227
    if-lez v4, :cond_7

    const/4 v7, 0x1

    :goto_5
    return v7

    .line 204
    .end local v0    # "event":I
    .end local v1    # "eventArgs":[Ljava/lang/Object;
    .end local v6    # "whereClause":Ljava/lang/String;
    :cond_0
    const-wide/16 v8, 0x0

    goto :goto_0

    .line 211
    :cond_1
    :try_start_1
    const-string v7, "pinning_status"

    invoke-virtual {v5, v7}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 212
    invoke-static {}, Lcom/google/android/videos/pinning/PinningDbHelper;->getClearDownloadContentValues()Landroid/content/ContentValues;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 223
    .end local v5    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v7

    if-lez v4, :cond_5

    const/16 v0, 0xb

    .line 224
    .restart local v0    # "event":I
    :goto_6
    if-nez v0, :cond_6

    const/4 v1, 0x0

    .line 225
    .restart local v1    # "eventArgs":[Ljava/lang/Object;
    :goto_7
    invoke-virtual {p0, v3, v2, v0, v1}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 226
    throw v7

    .line 216
    .end local v0    # "event":I
    .end local v1    # "eventArgs":[Ljava/lang/Object;
    .restart local v5    # "values":Landroid/content/ContentValues;
    :cond_2
    :try_start_2
    const-string v6, "account = ? AND asset_type IN (6,20) AND asset_id = ? AND (pinned IS NOT NULL AND pinned > 0)"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 223
    .restart local v6    # "whereClause":Ljava/lang/String;
    :cond_3
    const/4 v0, 0x0

    goto :goto_3

    .line 224
    .restart local v0    # "event":I
    :cond_4
    const/4 v7, 0x2

    new-array v1, v7, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v1, v7

    const/4 v7, 0x1

    aput-object p2, v1, v7

    goto :goto_4

    .line 223
    .end local v0    # "event":I
    .end local v5    # "values":Landroid/content/ContentValues;
    .end local v6    # "whereClause":Ljava/lang/String;
    :cond_5
    const/4 v0, 0x0

    goto :goto_6

    .line 224
    .restart local v0    # "event":I
    :cond_6
    const/4 v8, 0x2

    new-array v1, v8, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p1, v1, v8

    const/4 v8, 0x1

    aput-object p2, v1, v8

    goto :goto_7

    .line 227
    .restart local v1    # "eventArgs":[Ljava/lang/Object;
    .restart local v5    # "values":Landroid/content/ContentValues;
    .restart local v6    # "whereClause":Ljava/lang/String;
    :cond_7
    const/4 v7, 0x0

    goto :goto_5
.end method

.method public static unpin(Lcom/google/android/videos/store/Database;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p0, "database"    # Lcom/google/android/videos/store/Database;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;

    .prologue
    const/4 v4, -0x1

    .line 182
    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, v4

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/pinning/PinningDbHelper;->setPinned(Lcom/google/android/videos/store/Database;Ljava/lang/String;Ljava/lang/String;ZII)Z

    move-result v0

    return v0
.end method

.method public static updatePinningStateForVideo(Lcom/google/android/videos/store/Database;Lcom/google/android/videos/pinning/DownloadKey;Landroid/content/ContentValues;)V
    .locals 11
    .param p0, "database"    # Lcom/google/android/videos/store/Database;
    .param p1, "key"    # Lcom/google/android/videos/pinning/DownloadKey;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    const/16 v10, 0xb

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 163
    const/4 v0, 0x0

    .line 164
    .local v0, "success":Z
    invoke-virtual {p0}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 166
    .local v1, "transaction":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v2, "purchased_assets"

    const-string v3, "account = ? AND asset_type IN (6,20) AND asset_id = ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p1, Lcom/google/android/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p1, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, p2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 168
    iget-object v2, p1, Lcom/google/android/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/google/android/videos/store/UserAssetsUtil;->refreshVideoRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 169
    const/4 v0, 0x1

    .line 171
    new-array v2, v9, [Ljava/lang/Object;

    iget-object v3, p1, Lcom/google/android/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    aput-object v3, v2, v7

    iget-object v3, p1, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    aput-object v3, v2, v8

    invoke-virtual {p0, v1, v0, v10, v2}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 174
    return-void

    .line 171
    :catchall_0
    move-exception v2

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p1, Lcom/google/android/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    aput-object v4, v3, v7

    iget-object v4, p1, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    aput-object v4, v3, v8

    invoke-virtual {p0, v1, v0, v10, v3}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v2
.end method
