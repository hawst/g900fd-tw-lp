.class public Lcom/google/android/videos/player/logging/VssClient;
.super Ljava/lang/Object;
.source "VssClient.java"

# interfaces
.implements Lcom/google/android/videos/player/logging/LoggingClient;


# instance fields
.field private account:Ljava/lang/String;

.field private final appVersion:Ljava/lang/String;

.field private final baseUri:Ljava/lang/String;

.field private connectionType:Ljava/lang/String;

.field private final deviceType:I

.field private isPlaying:Z

.field private openWatchSegmentStartTimeMs:I

.field private final packageName:Ljava/lang/String;

.field private final pingSender:Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;

.field private playerType:I

.field private sentSessionPlaybackUri:Z

.field private sessionNonce:Ljava/lang/String;

.field private sessionTimeWhenLastSeekEnded:I

.field private sessionWatchTimeUriBuilder:Lcom/google/android/videos/utils/UriBuilder;

.field private videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1, "pingSender"    # Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;
    .param p2, "baseUri"    # Landroid/net/Uri;
    .param p3, "appVersion"    # Ljava/lang/String;
    .param p4, "packageName"    # Ljava/lang/String;
    .param p5, "deviceType"    # I

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;

    iput-object v0, p0, Lcom/google/android/videos/player/logging/VssClient;->pingSender:Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;

    .line 65
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/logging/VssClient;->baseUri:Ljava/lang/String;

    .line 66
    iput-object p3, p0, Lcom/google/android/videos/player/logging/VssClient;->appVersion:Ljava/lang/String;

    .line 67
    iput-object p4, p0, Lcom/google/android/videos/player/logging/VssClient;->packageName:Ljava/lang/String;

    .line 68
    iput p5, p0, Lcom/google/android/videos/player/logging/VssClient;->deviceType:I

    .line 69
    return-void
.end method

.method private maybeSendWatchTimePing()V
    .locals 2

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/videos/player/logging/VssClient;->sessionWatchTimeUriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    invoke-virtual {v0}, Lcom/google/android/videos/utils/UriBuilder;->length()I

    move-result v0

    const/16 v1, 0x708

    if-le v0, v1, :cond_0

    .line 217
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/logging/VssClient;->sendWatchTimePing(Z)V

    .line 219
    :cond_0
    return-void
.end method

.method private onState(IIZ)V
    .locals 8
    .param p1, "sessionTimeMs"    # I
    .param p2, "mediaTimeMs"    # I
    .param p3, "isPlaying"    # Z

    .prologue
    .line 178
    iget-boolean v1, p0, Lcom/google/android/videos/player/logging/VssClient;->isPlaying:Z

    if-ne v1, p3, :cond_1

    .line 213
    :cond_0
    :goto_0
    return-void

    .line 181
    :cond_1
    iput-boolean p3, p0, Lcom/google/android/videos/player/logging/VssClient;->isPlaying:Z

    .line 183
    if-eqz p3, :cond_2

    iget-boolean v1, p0, Lcom/google/android/videos/player/logging/VssClient;->sentSessionPlaybackUri:Z

    if-nez v1, :cond_2

    .line 184
    iget-object v1, p0, Lcom/google/android/videos/player/logging/VssClient;->baseUri:Ljava/lang/String;

    const-string v2, "playback"

    iget-object v3, p0, Lcom/google/android/videos/player/logging/VssClient;->sessionNonce:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/videos/player/logging/VssClient;->videoId:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->getStatsUriBuilder(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v0

    .line 186
    .local v0, "sessionPlaybackUriBuilder":Lcom/google/android/videos/utils/UriBuilder;
    iget v1, p0, Lcom/google/android/videos/player/logging/VssClient;->deviceType:I

    iget v2, p0, Lcom/google/android/videos/player/logging/VssClient;->playerType:I

    iget-object v3, p0, Lcom/google/android/videos/player/logging/VssClient;->appVersion:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/videos/player/logging/VssClient;->packageName:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->setClientParameters(Lcom/google/android/videos/utils/UriBuilder;IILjava/lang/String;Ljava/lang/String;)V

    .line 188
    const-string v1, "vis"

    const-string v2, "2"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 189
    iget-object v1, p0, Lcom/google/android/videos/player/logging/VssClient;->pingSender:Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;

    iget-object v2, p0, Lcom/google/android/videos/player/logging/VssClient;->account:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/player/logging/VssClient;->sessionNonce:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/videos/utils/UriBuilder;->build()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;->createVssPing(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->sendPing(Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;)V

    .line 191
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/videos/player/logging/VssClient;->sentSessionPlaybackUri:Z

    .line 194
    .end local v0    # "sessionPlaybackUriBuilder":Lcom/google/android/videos/utils/UriBuilder;
    :cond_2
    if-eqz p3, :cond_3

    .line 195
    iput p2, p0, Lcom/google/android/videos/player/logging/VssClient;->openWatchSegmentStartTimeMs:I

    goto :goto_0

    .line 197
    :cond_3
    int-to-long v2, p1

    iget v1, p0, Lcom/google/android/videos/player/logging/VssClient;->sessionTimeWhenLastSeekEnded:I

    int-to-long v4, v1

    const-wide/16 v6, 0x1f4

    add-long/2addr v4, v6

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    .line 201
    iget-object v1, p0, Lcom/google/android/videos/player/logging/VssClient;->sessionWatchTimeUriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    const-string v2, "st"

    iget v3, p0, Lcom/google/android/videos/player/logging/VssClient;->openWatchSegmentStartTimeMs:I

    invoke-static {v3}, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->getTimeString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/videos/utils/UriBuilder;->appendToQueryParameter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 204
    iget-object v1, p0, Lcom/google/android/videos/player/logging/VssClient;->sessionWatchTimeUriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    const-string v2, "et"

    invoke-static {p2}, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->getTimeString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/videos/utils/UriBuilder;->appendToQueryParameter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 206
    iget-object v1, p0, Lcom/google/android/videos/player/logging/VssClient;->sessionWatchTimeUriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    const-string v2, "vis"

    const-string v3, "2"

    const-string v4, ","

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/videos/utils/UriBuilder;->appendToQueryParameter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 208
    iget-object v1, p0, Lcom/google/android/videos/player/logging/VssClient;->sessionWatchTimeUriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    const-string v2, "conn"

    iget-object v3, p0, Lcom/google/android/videos/player/logging/VssClient;->connectionType:Ljava/lang/String;

    const-string v4, ","

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/videos/utils/UriBuilder;->appendToQueryParameter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 210
    invoke-direct {p0}, Lcom/google/android/videos/player/logging/VssClient;->maybeSendWatchTimePing()V

    goto :goto_0
.end method

.method private sendWatchTimePing(Z)V
    .locals 4
    .param p1, "finalPing"    # Z

    .prologue
    .line 222
    iget-object v2, p0, Lcom/google/android/videos/player/logging/VssClient;->sessionWatchTimeUriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    const-string v3, "state"

    iget-boolean v1, p0, Lcom/google/android/videos/player/logging/VssClient;->isPlaying:Z

    if-eqz v1, :cond_1

    const-string v1, "playing"

    :goto_0
    invoke-virtual {v2, v3, v1}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 224
    if-eqz p1, :cond_0

    .line 225
    iget-object v1, p0, Lcom/google/android/videos/player/logging/VssClient;->sessionWatchTimeUriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    const-string v2, "final"

    const-string v3, "1"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 227
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/player/logging/VssClient;->sessionWatchTimeUriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    invoke-virtual {v1}, Lcom/google/android/videos/utils/UriBuilder;->build()Ljava/lang/String;

    move-result-object v0

    .line 230
    .local v0, "pingUri":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/videos/player/logging/VssClient;->sessionWatchTimeUriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    const-string v2, "conn"

    invoke-virtual {v1, v2}, Lcom/google/android/videos/utils/UriBuilder;->deleteQueryParameters(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v1

    const-string v2, "vis"

    invoke-virtual {v1, v2}, Lcom/google/android/videos/utils/UriBuilder;->deleteQueryParameters(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v1

    const-string v2, "et"

    invoke-virtual {v1, v2}, Lcom/google/android/videos/utils/UriBuilder;->deleteQueryParameters(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v1

    const-string v2, "st"

    invoke-virtual {v1, v2}, Lcom/google/android/videos/utils/UriBuilder;->deleteQueryParameters(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v1

    const-string v2, "final"

    invoke-virtual {v1, v2}, Lcom/google/android/videos/utils/UriBuilder;->deleteQueryParameters(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v1

    const-string v2, "state"

    invoke-virtual {v1, v2}, Lcom/google/android/videos/utils/UriBuilder;->deleteQueryParameters(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 237
    iget-object v1, p0, Lcom/google/android/videos/player/logging/VssClient;->pingSender:Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;

    iget-object v2, p0, Lcom/google/android/videos/player/logging/VssClient;->account:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/player/logging/VssClient;->sessionNonce:Ljava/lang/String;

    invoke-static {v2, v3, v0}, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;->createVssPing(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->sendPing(Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;)V

    .line 238
    return-void

    .line 222
    .end local v0    # "pingUri":Ljava/lang/String;
    :cond_1
    const-string v1, "paused"

    goto :goto_0
.end method


# virtual methods
.method public endSession(IILcom/google/android/videos/player/logging/DerivedStats;)V
    .locals 1
    .param p1, "sessionTimeMs"    # I
    .param p2, "mediaTimeMs"    # I
    .param p3, "derivedStats"    # Lcom/google/android/videos/player/logging/DerivedStats;

    .prologue
    .line 173
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/videos/player/logging/VssClient;->onState(IIZ)V

    .line 174
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/logging/VssClient;->sendWatchTimePing(Z)V

    .line 175
    return-void
.end method

.method public onBandwidthSample(IIJJ)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "elapsedMs"    # I
    .param p3, "bytes"    # J
    .param p5, "bitrateEstimate"    # J

    .prologue
    .line 122
    return-void
.end method

.method public onDroppedFrames(II)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "count"    # I

    .prologue
    .line 143
    return-void
.end method

.method public onError(IIIILjava/lang/Exception;)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "mediaTimeMs"    # I
    .param p3, "errorType"    # I
    .param p4, "errorCode"    # I
    .param p5, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 133
    return-void
.end method

.method public onFailed(II)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "mediaTimeMs"    # I

    .prologue
    .line 138
    return-void
.end method

.method public onFormatEnabled(III)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "itag"    # I
    .param p3, "trigger"    # I

    .prologue
    .line 154
    return-void
.end method

.method public onFormatSelected(III)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "itag"    # I
    .param p3, "trigger"    # I

    .prologue
    .line 148
    return-void
.end method

.method public onHttpDataSourceOpened(IJ)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "elapsedMs"    # J

    .prologue
    .line 127
    return-void
.end method

.method public onLoadingChanged(IZ)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "loading"    # Z

    .prologue
    .line 109
    return-void
.end method

.method public onNetworkType(III)V
    .locals 1
    .param p1, "sessionTimeMs"    # I
    .param p2, "type"    # I
    .param p3, "subtype"    # I

    .prologue
    .line 115
    invoke-static {p2, p3}, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->toStatsConnectionType(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/logging/VssClient;->connectionType:Ljava/lang/String;

    .line 116
    return-void
.end method

.method public onStateChanged(IIZI)V
    .locals 1
    .param p1, "sessionTimeMs"    # I
    .param p2, "mediaTimeMs"    # I
    .param p3, "playWhenReady"    # Z
    .param p4, "playbackState"    # I

    .prologue
    .line 103
    if-eqz p3, :cond_0

    const/4 v0, 0x4

    if-ne p4, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/videos/player/logging/VssClient;->onState(IIZ)V

    .line 104
    return-void

    .line 103
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSubtitleEnabled(ILcom/google/android/videos/subtitles/SubtitleTrack;)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "track"    # Lcom/google/android/videos/subtitles/SubtitleTrack;

    .prologue
    .line 169
    return-void
.end method

.method public onSubtitleError(ILcom/google/android/videos/subtitles/SubtitleTrack;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "track"    # Lcom/google/android/videos/subtitles/SubtitleTrack;
    .param p3, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 164
    return-void
.end method

.method public onSubtitleSelected(ILcom/google/android/videos/subtitles/SubtitleTrack;)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "track"    # Lcom/google/android/videos/subtitles/SubtitleTrack;

    .prologue
    .line 159
    return-void
.end method

.method public onUserSeekingChanged(IIZZ)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "mediaTimeMs"    # I
    .param p3, "seeking"    # Z
    .param p4, "isFineScrubbing"    # Z

    .prologue
    .line 95
    if-nez p3, :cond_0

    .line 96
    iput p1, p0, Lcom/google/android/videos/player/logging/VssClient;->sessionTimeWhenLastSeekEnded:I

    .line 98
    :cond_0
    return-void
.end method

.method public startSession(Lcom/google/android/videos/player/logging/SessionTimeProvider;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 4
    .param p1, "sessionTimeProvider"    # Lcom/google/android/videos/player/logging/SessionTimeProvider;
    .param p2, "sessionNonce"    # Ljava/lang/String;
    .param p3, "playerType"    # I
    .param p4, "account"    # Ljava/lang/String;
    .param p5, "videoId"    # Ljava/lang/String;
    .param p6, "showId"    # Ljava/lang/String;
    .param p7, "seasonId"    # Ljava/lang/String;
    .param p8, "isTrailer"    # Z
    .param p9, "isOffline"    # Z

    .prologue
    const/4 v0, 0x0

    .line 80
    iput-object p2, p0, Lcom/google/android/videos/player/logging/VssClient;->sessionNonce:Ljava/lang/String;

    .line 81
    iput-object p4, p0, Lcom/google/android/videos/player/logging/VssClient;->account:Ljava/lang/String;

    .line 82
    iput-object p5, p0, Lcom/google/android/videos/player/logging/VssClient;->videoId:Ljava/lang/String;

    .line 83
    iput p3, p0, Lcom/google/android/videos/player/logging/VssClient;->playerType:I

    .line 84
    iput-boolean v0, p0, Lcom/google/android/videos/player/logging/VssClient;->isPlaying:Z

    .line 85
    iput-boolean v0, p0, Lcom/google/android/videos/player/logging/VssClient;->sentSessionPlaybackUri:Z

    .line 86
    iget-object v0, p0, Lcom/google/android/videos/player/logging/VssClient;->baseUri:Ljava/lang/String;

    const-string v1, "watchtime"

    invoke-static {v0, v1, p2, p5}, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->getStatsUriBuilder(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/logging/VssClient;->sessionWatchTimeUriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    .line 88
    iget-object v0, p0, Lcom/google/android/videos/player/logging/VssClient;->sessionWatchTimeUriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    iget v1, p0, Lcom/google/android/videos/player/logging/VssClient;->deviceType:I

    iget-object v2, p0, Lcom/google/android/videos/player/logging/VssClient;->appVersion:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/player/logging/VssClient;->packageName:Ljava/lang/String;

    invoke-static {v0, v1, p3, v2, v3}, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->setClientParameters(Lcom/google/android/videos/utils/UriBuilder;IILjava/lang/String;Ljava/lang/String;)V

    .line 90
    return-void
.end method

.method public supportsOfflinePlaybacks()Z
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    return v0
.end method
