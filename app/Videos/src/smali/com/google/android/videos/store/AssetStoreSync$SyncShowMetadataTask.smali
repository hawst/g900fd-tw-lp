.class Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;
.super Lcom/google/android/videos/store/AssetStoreSync$SyncAssetMetadataTask;
.source "AssetStoreSync.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/AssetStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncShowMetadataTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/store/AssetStoreSync$SyncAssetMetadataTask;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
        ">;"
    }
.end annotation


# instance fields
.field private allEpisodes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/TreeSet",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;>;"
        }
    .end annotation
.end field

.field private childEpisodeIds:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/AssetResourceId;",
            ">;"
        }
    .end annotation
.end field

.field private childSeasonIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final includeChildren:Z

.field private final remainingEpisodeIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final remainingSeasonIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final requestedEpisodeIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final reverseOrder:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;"
        }
    .end annotation
.end field

.field private seasons:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;"
        }
    .end annotation
.end field

.field private show:Lcom/google/wireless/android/video/magma/proto/AssetResource;

.field private final showId:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/videos/store/AssetStoreSync;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;Ljava/util/List;)V
    .locals 1
    .param p2, "priority"    # I
    .param p4, "account"    # Ljava/lang/String;
    .param p5, "showId"    # Ljava/lang/String;
    .param p6, "includeChildren"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/store/SyncTaskManager$SharedStates",
            "<*>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 910
    .local p3, "states":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<*>;"
    .local p7, "seasonIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p8, "episodeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    .line 911
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/videos/store/AssetStoreSync$SyncAssetMetadataTask;-><init>(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V

    .line 912
    iput-object p5, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->showId:Ljava/lang/String;

    .line 913
    iput-boolean p6, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->includeChildren:Z

    .line 914
    iput-object p7, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->remainingSeasonIds:Ljava/util/List;

    .line 915
    iput-object p8, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->remainingEpisodeIds:Ljava/util/List;

    .line 916
    if-nez p8, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->requestedEpisodeIds:Ljava/util/List;

    .line 919
    invoke-static {p0}, Ljava/util/Collections;->reverseOrder(Ljava/util/Comparator;)Ljava/util/Comparator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->reverseOrder:Ljava/util/Comparator;

    .line 920
    return-void

    .line 916
    :cond_0
    invoke-static {p8}, Lcom/google/android/videos/utils/CollectionUtil;->immutableCopyOf(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private checkMetadataUpToDate()Z
    .locals 46

    .prologue
    .line 946
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->includeChildren:Z

    if-eqz v2, :cond_1

    new-instance v12, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v12, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 948
    .local v12, "showIdNeedingUserAssetsRefresh":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->showId:Ljava/lang/String;

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/CollectionUtil;->newArrayList(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v3

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->includeChildren:Z

    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->config:Lcom/google/android/videos/Config;
    invoke-static {v4}, Lcom/google/android/videos/store/AssetStoreSync;->access$500(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/Config;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/videos/Config;->resyncFullShowAfterMillis()J

    move-result-wide v4

    :goto_1
    const-string v6, "shows"

    const-string v7, "shows_id"

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->includeChildren:Z

    if-eqz v8, :cond_3

    const-string v8, "shows_full_sync_timestamp"

    :goto_2
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->account:Ljava/lang/String;

    const/16 v11, 0x12

    # invokes: Lcom/google/android/videos/store/AssetStoreSync;->filterUpToDateIds(Ljava/util/List;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ILjava/util/List;)Z
    invoke-static/range {v2 .. v12}, Lcom/google/android/videos/store/AssetStoreSync;->access$600(Lcom/google/android/videos/store/AssetStoreSync;Ljava/util/List;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ILjava/util/List;)Z

    move-result v42

    .line 958
    .local v42, "showMetadataUpToDate":Z
    if-eqz v42, :cond_0

    .line 959
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->priority:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->showId:Ljava/lang/String;

    # invokes: Lcom/google/android/videos/store/AssetStoreSync;->maybeSyncShowPoster(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V
    invoke-static {v2, v3, v4, v5}, Lcom/google/android/videos/store/AssetStoreSync;->access$1300(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V

    .line 960
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->priority:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->showId:Ljava/lang/String;

    # invokes: Lcom/google/android/videos/store/AssetStoreSync;->maybeSyncShowBanner(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V
    invoke-static {v2, v3, v4, v5}, Lcom/google/android/videos/store/AssetStoreSync;->access$1400(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V

    .line 963
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->includeChildren:Z

    if-nez v2, :cond_4

    .line 1034
    .end local v42    # "showMetadataUpToDate":Z
    :goto_3
    return v42

    .line 946
    .end local v12    # "showIdNeedingUserAssetsRefresh":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    const/4 v12, 0x0

    goto :goto_0

    .line 948
    .restart local v12    # "showIdNeedingUserAssetsRefresh":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->config:Lcom/google/android/videos/Config;
    invoke-static {v4}, Lcom/google/android/videos/store/AssetStoreSync;->access$500(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/Config;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/videos/Config;->resyncShowAfterMillis()J

    move-result-wide v4

    goto :goto_1

    :cond_3
    const-string v8, "shows_synced_timestamp"

    goto :goto_2

    .line 968
    .restart local v42    # "showMetadataUpToDate":Z
    :cond_4
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 969
    .local v21, "upToDateEpisodeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    .line 970
    .local v24, "upToDateEpisodeIdsNeedingUserAssetsRefresh":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->remainingEpisodeIds:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->config:Lcom/google/android/videos/Config;
    invoke-static {v2}, Lcom/google/android/videos/store/AssetStoreSync;->access$500(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/Config;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/videos/Config;->resyncVideoAfterMillis()J

    move-result-wide v16

    const-string v18, "videos"

    const-string v19, "video_id"

    const-string v20, "video_synced_timestamp"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->account:Ljava/lang/String;

    move-object/from16 v22, v0

    const/16 v23, 0x14

    # invokes: Lcom/google/android/videos/store/AssetStoreSync;->filterUpToDateIds(Ljava/util/List;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ILjava/util/List;)Z
    invoke-static/range {v14 .. v24}, Lcom/google/android/videos/store/AssetStoreSync;->access$600(Lcom/google/android/videos/store/AssetStoreSync;Ljava/util/List;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ILjava/util/List;)Z

    move-result v13

    .line 982
    .local v13, "allExpectedEpisodeMetadataUpToDate":Z
    new-instance v33, Ljava/util/ArrayList;

    invoke-direct/range {v33 .. v33}, Ljava/util/ArrayList;-><init>()V

    .line 983
    .local v33, "upToDateSeasonIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v36, Ljava/util/ArrayList;

    invoke-direct/range {v36 .. v36}, Ljava/util/ArrayList;-><init>()V

    .line 984
    .local v36, "upToDateSeasonIdsNeedingUserAssetsRefresh":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->remainingSeasonIds:Ljava/util/List;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->config:Lcom/google/android/videos/Config;
    invoke-static {v2}, Lcom/google/android/videos/store/AssetStoreSync;->access$500(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/Config;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/videos/Config;->resyncSeasonAfterMillis()J

    move-result-wide v28

    const-string v30, "seasons"

    const-string v31, "season_id"

    const-string v32, "season_synced_timestamp"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->account:Ljava/lang/String;

    move-object/from16 v34, v0

    const/16 v35, 0x13

    # invokes: Lcom/google/android/videos/store/AssetStoreSync;->filterUpToDateIds(Ljava/util/List;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ILjava/util/List;)Z
    invoke-static/range {v26 .. v36}, Lcom/google/android/videos/store/AssetStoreSync;->access$600(Lcom/google/android/videos/store/AssetStoreSync;Ljava/util/List;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ILjava/util/List;)Z

    move-result v25

    .line 995
    .local v25, "allExpectedSeasonMetadataUpToDate":Z
    if-eqz v42, :cond_b

    if-eqz v13, :cond_b

    if-eqz v25, :cond_b

    .line 999
    invoke-interface {v12}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    const/16 v43, 0x1

    .line 1000
    .local v43, "showNeedsUserAssetsRefresh":Z
    :goto_4
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    move-result v37

    .line 1001
    .local v37, "episodeCount":I
    invoke-interface/range {v36 .. v36}, Ljava/util/List;->size()I

    move-result v40

    .line 1002
    .local v40, "seasonCount":I
    if-nez v37, :cond_5

    if-nez v40, :cond_5

    if-eqz v43, :cond_9

    .line 1003
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v2}, Lcom/google/android/videos/store/AssetStoreSync;->access$900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v45

    .line 1004
    .local v45, "transaction":Landroid/database/sqlite/SQLiteDatabase;
    const/16 v44, 0x0

    .line 1006
    .local v44, "success":Z
    const/16 v39, 0x0

    .local v39, "i":I
    :goto_5
    move/from16 v0, v39

    move/from16 v1, v37

    if-ge v0, v1, :cond_7

    .line 1007
    :try_start_0
    move-object/from16 v0, v24

    move/from16 v1, v39

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v38

    check-cast v38, Ljava/lang/String;

    .line 1008
    .local v38, "episodeId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->account:Ljava/lang/String;

    move-object/from16 v0, v45

    move-object/from16 v1, v38

    invoke-static {v0, v2, v1}, Lcom/google/android/videos/store/UserAssetsUtil;->refreshVideoRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 1006
    add-int/lit8 v39, v39, 0x1

    goto :goto_5

    .line 999
    .end local v37    # "episodeCount":I
    .end local v38    # "episodeId":Ljava/lang/String;
    .end local v39    # "i":I
    .end local v40    # "seasonCount":I
    .end local v43    # "showNeedsUserAssetsRefresh":Z
    .end local v44    # "success":Z
    .end local v45    # "transaction":Landroid/database/sqlite/SQLiteDatabase;
    :cond_6
    const/16 v43, 0x0

    goto :goto_4

    .line 1010
    .restart local v37    # "episodeCount":I
    .restart local v39    # "i":I
    .restart local v40    # "seasonCount":I
    .restart local v43    # "showNeedsUserAssetsRefresh":Z
    .restart local v44    # "success":Z
    .restart local v45    # "transaction":Landroid/database/sqlite/SQLiteDatabase;
    :cond_7
    const/16 v39, 0x0

    :goto_6
    move/from16 v0, v39

    move/from16 v1, v40

    if-ge v0, v1, :cond_8

    .line 1011
    move-object/from16 v0, v36

    move/from16 v1, v39

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Ljava/lang/String;

    .line 1012
    .local v41, "seasonId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->account:Ljava/lang/String;

    move-object/from16 v0, v45

    move-object/from16 v1, v41

    invoke-static {v0, v2, v1}, Lcom/google/android/videos/store/UserAssetsUtil;->refreshSeasonRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 1010
    add-int/lit8 v39, v39, 0x1

    goto :goto_6

    .line 1014
    .end local v41    # "seasonId":Ljava/lang/String;
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->account:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->showId:Ljava/lang/String;

    move-object/from16 v0, v45

    invoke-static {v0, v2, v3}, Lcom/google/android/videos/store/UserAssetsUtil;->refreshShowRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1015
    const/16 v44, 0x1

    .line 1017
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v2}, Lcom/google/android/videos/store/AssetStoreSync;->access$900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v2

    const/4 v3, 0x6

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->account:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->showId:Ljava/lang/String;

    aput-object v6, v4, v5

    move-object/from16 v0, v45

    move/from16 v1, v44

    invoke-virtual {v2, v0, v1, v3, v4}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 1023
    .end local v39    # "i":I
    .end local v44    # "success":Z
    .end local v45    # "transaction":Landroid/database/sqlite/SQLiteDatabase;
    :cond_9
    const/16 v39, 0x0

    .restart local v39    # "i":I
    :goto_7
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, v39

    if-ge v0, v2, :cond_a

    .line 1024
    move-object/from16 v0, v21

    move/from16 v1, v39

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v38

    check-cast v38, Ljava/lang/String;

    .line 1025
    .restart local v38    # "episodeId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->priority:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    move-object/from16 v0, v38

    # invokes: Lcom/google/android/videos/store/AssetStoreSync;->maybeSyncVideoScreenshot(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V
    invoke-static {v2, v3, v4, v0}, Lcom/google/android/videos/store/AssetStoreSync;->access$800(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V

    .line 1023
    add-int/lit8 v39, v39, 0x1

    goto :goto_7

    .line 1017
    .end local v38    # "episodeId":Ljava/lang/String;
    .restart local v44    # "success":Z
    .restart local v45    # "transaction":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v3}, Lcom/google/android/videos/store/AssetStoreSync;->access$900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v3

    const/4 v4, 0x6

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->account:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->showId:Ljava/lang/String;

    aput-object v7, v5, v6

    move-object/from16 v0, v45

    move/from16 v1, v44

    invoke-virtual {v3, v0, v1, v4, v5}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v2

    .line 1028
    .end local v44    # "success":Z
    .end local v45    # "transaction":Landroid/database/sqlite/SQLiteDatabase;
    :cond_a
    const/16 v42, 0x1

    goto/16 :goto_3

    .line 1032
    .end local v37    # "episodeCount":I
    .end local v39    # "i":I
    .end local v40    # "seasonCount":I
    .end local v43    # "showNeedsUserAssetsRefresh":Z
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->remainingEpisodeIds:Ljava/util/List;

    move-object/from16 v0, v21

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1033
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->remainingSeasonIds:Ljava/util/List;

    move-object/from16 v0, v33

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1034
    const/16 v42, 0x0

    goto/16 :goto_3
.end method

.method private collectEpisodes([Lcom/google/wireless/android/video/magma/proto/AssetResource;)V
    .locals 8
    .param p1, "assets"    # [Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    .line 1164
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v6, p1

    if-ge v3, v6, :cond_6

    .line 1165
    aget-object v0, p1, v3

    .line 1166
    .local v0, "asset":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    const/16 v6, 0x14

    invoke-static {v0, v6}, Lcom/google/android/videos/store/PurchaseStoreUtil;->isAssetOfType(Lcom/google/wireless/android/video/magma/proto/AssetResource;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1164
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1169
    :cond_0
    iget-object v6, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v2, v6, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    .line 1171
    .local v2, "episodeId":Ljava/lang/String;
    :try_start_0
    invoke-static {v0}, Lcom/google/android/videos/store/PurchaseStoreUtil;->checkMetadataAsset(Lcom/google/wireless/android/video/magma/proto/AssetResource;)V
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1176
    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 1177
    .local v4, "parent":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    if-nez v4, :cond_1

    .line 1178
    iget-object v6, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->childEpisodeIds:Ljava/util/Map;

    invoke-interface {v6, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "parent":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    check-cast v4, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 1179
    .restart local v4    # "parent":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    iput-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 1181
    :cond_1
    if-eqz v4, :cond_2

    iget v6, v4, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    const/16 v7, 0x13

    if-eq v6, v7, :cond_3

    :cond_2
    const/4 v5, 0x0

    .line 1183
    .local v5, "seasonEpisodes":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Lcom/google/wireless/android/video/magma/proto/AssetResource;>;"
    :goto_2
    if-nez v5, :cond_4

    .line 1184
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Received detached episode asset "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " when requesting show "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->showId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    goto :goto_1

    .line 1172
    .end local v4    # "parent":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .end local v5    # "seasonEpisodes":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Lcom/google/wireless/android/video/magma/proto/AssetResource;>;"
    :catch_0
    move-exception v1

    .line 1173
    .local v1, "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Received invalid episode asset "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " when requesting show "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->showId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v1}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1181
    .end local v1    # "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    .restart local v4    # "parent":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    :cond_3
    iget-object v6, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->allEpisodes:Ljava/util/Map;

    iget-object v7, v4, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/TreeSet;

    move-object v5, v6

    goto :goto_2

    .line 1185
    .restart local v5    # "seasonEpisodes":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Lcom/google/wireless/android/video/magma/proto/AssetResource;>;"
    :cond_4
    invoke-virtual {v5, v0}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 1186
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Received duplicate episode asset "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " when requesting show "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->showId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1188
    :cond_5
    iget-object v6, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->remainingEpisodeIds:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1191
    .end local v0    # "asset":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .end local v2    # "episodeId":Ljava/lang/String;
    .end local v4    # "parent":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .end local v5    # "seasonEpisodes":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Lcom/google/wireless/android/video/magma/proto/AssetResource;>;"
    :cond_6
    return-void
.end method

.method private collectSeasons([Lcom/google/wireless/android/video/magma/proto/AssetResource;)V
    .locals 11
    .param p1, "assets"    # [Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    .line 1126
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v8, p1

    if-ge v3, v8, :cond_7

    .line 1127
    aget-object v0, p1, v3

    .line 1128
    .local v0, "asset":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    const/16 v8, 0x13

    invoke-static {v0, v8}, Lcom/google/android/videos/store/PurchaseStoreUtil;->isAssetOfType(Lcom/google/wireless/android/video/magma/proto/AssetResource;I)Z

    move-result v8

    if-nez v8, :cond_1

    .line 1126
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1131
    :cond_1
    iget-object v8, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v7, v8, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    .line 1133
    .local v7, "seasonId":Ljava/lang/String;
    :try_start_0
    invoke-static {v0}, Lcom/google/android/videos/store/PurchaseStoreUtil;->checkMetadataAsset(Lcom/google/wireless/android/video/magma/proto/AssetResource;)V
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1138
    iget-object v5, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 1139
    .local v5, "parent":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    if-nez v5, :cond_3

    iget-object v8, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->childSeasonIds:Ljava/util/Set;

    invoke-interface {v8, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1140
    iget-object v8, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->show:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v8, v8, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iput-object v8, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 1145
    :cond_2
    iget-object v8, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->seasons:Ljava/util/TreeSet;

    invoke-virtual {v8, v0}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1146
    new-instance v6, Ljava/util/TreeSet;

    iget-object v8, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->reverseOrder:Ljava/util/Comparator;

    invoke-direct {v6, v8}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    .line 1147
    .local v6, "seasonEpisodes":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Lcom/google/wireless/android/video/magma/proto/AssetResource;>;"
    iget-object v8, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->allEpisodes:Ljava/util/Map;

    invoke-interface {v8, v7, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1148
    iget-object v8, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->remainingSeasonIds:Ljava/util/List;

    invoke-interface {v8, v7}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1153
    .end local v6    # "seasonEpisodes":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Lcom/google/wireless/android/video/magma/proto/AssetResource;>;"
    :goto_2
    iget-object v8, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez v8, :cond_6

    const/4 v1, 0x0

    .line 1154
    .local v1, "childCount":I
    :goto_3
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_4
    if-ge v4, v1, :cond_0

    .line 1155
    iget-object v8, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->childEpisodeIds:Ljava/util/Map;

    iget-object v9, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    aget-object v9, v9, v4

    iget-object v9, v9, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    iget-object v10, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-interface {v8, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1154
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 1134
    .end local v1    # "childCount":I
    .end local v4    # "j":I
    .end local v5    # "parent":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    :catch_0
    move-exception v2

    .line 1135
    .local v2, "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Received invalid season asset "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " when requesting show "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->showId:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v2}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1141
    .end local v2    # "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    .restart local v5    # "parent":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    :cond_3
    if-eqz v5, :cond_4

    iget v8, v5, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    const/16 v9, 0x12

    if-ne v8, v9, :cond_4

    iget-object v8, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->showId:Ljava/lang/String;

    iget-object v9, v5, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 1142
    :cond_4
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Received detached season asset "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " when requesting show "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->showId:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1150
    :cond_5
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Received duplicate season asset "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " when requesting show "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->showId:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1153
    :cond_6
    iget-object v8, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    array-length v1, v8

    goto/16 :goto_3

    .line 1158
    .end local v0    # "asset":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .end local v5    # "parent":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .end local v7    # "seasonId":Ljava/lang/String;
    :cond_7
    return-void
.end method

.method private compareSequenceNumbers(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/AssetResource;)I
    .locals 11
    .param p1, "lhs"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p2, "rhs"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    const/4 v5, 0x0

    const/4 v8, -0x1

    const/4 v9, 0x1

    const/4 v7, 0x0

    .line 1377
    iget-object v10, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    if-nez v10, :cond_0

    move-object v2, v5

    .line 1378
    .local v2, "leftSequence":Ljava/lang/String;
    :goto_0
    if-nez v2, :cond_1

    move v3, v7

    .line 1379
    .local v3, "leftSequenceLength":I
    :goto_1
    iget-object v10, p2, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    if-nez v10, :cond_2

    .line 1380
    .local v5, "rightSequence":Ljava/lang/String;
    :goto_2
    if-nez v5, :cond_3

    move v6, v7

    .line 1381
    .local v6, "rightSequenceLength":I
    :goto_3
    if-nez v3, :cond_5

    .line 1382
    if-nez v6, :cond_4

    .line 1405
    :goto_4
    return v7

    .line 1377
    .end local v2    # "leftSequence":Ljava/lang/String;
    .end local v3    # "leftSequenceLength":I
    .end local v5    # "rightSequence":Ljava/lang/String;
    .end local v6    # "rightSequenceLength":I
    :cond_0
    iget-object v10, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v2, v10, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->sequenceNumber:Ljava/lang/String;

    goto :goto_0

    .line 1378
    .restart local v2    # "leftSequence":Ljava/lang/String;
    :cond_1
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    goto :goto_1

    .line 1379
    .restart local v3    # "leftSequenceLength":I
    :cond_2
    iget-object v10, p2, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v5, v10, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->sequenceNumber:Ljava/lang/String;

    goto :goto_2

    .line 1380
    .restart local v5    # "rightSequence":Ljava/lang/String;
    :cond_3
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    goto :goto_3

    .restart local v6    # "rightSequenceLength":I
    :cond_4
    move v7, v8

    .line 1382
    goto :goto_4

    .line 1383
    :cond_5
    if-nez v6, :cond_6

    move v7, v9

    .line 1384
    goto :goto_4

    .line 1388
    :cond_6
    const/4 v1, 0x0

    .line 1389
    .local v1, "leftIsDigit":Z
    const/4 v4, 0x0

    .line 1390
    .local v4, "rightIsDigit":Z
    const/4 v0, 0x0

    .line 1392
    .local v0, "i":I
    :cond_7
    if-ge v0, v3, :cond_9

    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v10

    invoke-static {v10}, Ljava/lang/Character;->isDigit(C)Z

    move-result v10

    if-eqz v10, :cond_9

    move v1, v9

    .line 1393
    :goto_5
    if-ge v0, v6, :cond_a

    invoke-virtual {v5, v0}, Ljava/lang/String;->charAt(I)C

    move-result v10

    invoke-static {v10}, Ljava/lang/Character;->isDigit(C)Z

    move-result v10

    if-eqz v10, :cond_a

    move v4, v9

    .line 1394
    :goto_6
    add-int/lit8 v0, v0, 0x1

    .line 1395
    if-eqz v1, :cond_8

    if-nez v4, :cond_7

    .line 1397
    :cond_8
    if-eqz v1, :cond_b

    move v7, v9

    .line 1398
    goto :goto_4

    :cond_9
    move v1, v7

    .line 1392
    goto :goto_5

    :cond_a
    move v4, v7

    .line 1393
    goto :goto_6

    .line 1399
    :cond_b
    if-eqz v4, :cond_c

    move v7, v8

    .line 1400
    goto :goto_4

    .line 1405
    :cond_c
    invoke-virtual {v2, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v7

    goto :goto_4
.end method

.method private processFirstResponseAssets([Lcom/google/wireless/android/video/magma/proto/AssetResource;)V
    .locals 9
    .param p1, "assets"    # [Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    .line 1080
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v5, p1

    if-ge v3, v5, :cond_2

    .line 1081
    aget-object v0, p1, v3

    .line 1082
    .local v0, "asset":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    if-eqz v0, :cond_0

    iget-object v5, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v5, :cond_0

    iget-object v5, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget v5, v5, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    const/16 v6, 0x12

    if-ne v5, v6, :cond_0

    iget-object v5, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->showId:Ljava/lang/String;

    iget-object v6, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v6, v6, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1080
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1087
    :cond_1
    :try_start_0
    invoke-static {v0}, Lcom/google/android/videos/store/PurchaseStoreUtil;->checkMetadataAsset(Lcom/google/wireless/android/video/magma/proto/AssetResource;)V

    .line 1088
    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->show:Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1095
    .end local v0    # "asset":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :cond_2
    iget-object v5, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->show:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    if-nez v5, :cond_4

    .line 1096
    const/16 v5, 0x14

    new-instance v6, Lcom/google/android/videos/store/SyncTaskManager$DataException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Did not receive a show asset when requesting show "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->showId:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/google/android/videos/store/SyncTaskManager$DataException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v5, v6}, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->onSyncError(ILjava/lang/Exception;)V

    .line 1114
    :cond_3
    :goto_2
    return-void

    .line 1090
    .restart local v0    # "asset":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :catch_0
    move-exception v2

    .line 1091
    .local v2, "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Received invalid show asset when requesting show "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->showId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v2}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1101
    .end local v0    # "asset":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .end local v2    # "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    :cond_4
    iget-boolean v5, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->includeChildren:Z

    if-eqz v5, :cond_3

    .line 1102
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    iput-object v5, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->childSeasonIds:Ljava/util/Set;

    .line 1103
    iget-object v5, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->show:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v5, v5, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez v5, :cond_5

    const/4 v1, 0x0

    .line 1104
    .local v1, "childCount":I
    :goto_3
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_4
    if-ge v4, v1, :cond_6

    .line 1105
    iget-object v5, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->childSeasonIds:Ljava/util/Set;

    iget-object v6, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->show:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v6, v6, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    aget-object v6, v6, v4

    iget-object v6, v6, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1104
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 1103
    .end local v1    # "childCount":I
    .end local v4    # "j":I
    :cond_5
    iget-object v5, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->show:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v5, v5, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    array-length v1, v5

    goto :goto_3

    .line 1108
    .restart local v1    # "childCount":I
    .restart local v4    # "j":I
    :cond_6
    new-instance v5, Ljava/util/TreeSet;

    iget-object v6, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->reverseOrder:Ljava/util/Comparator;

    invoke-direct {v5, v6}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    iput-object v5, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->seasons:Ljava/util/TreeSet;

    .line 1109
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    iput-object v5, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->childEpisodeIds:Ljava/util/Map;

    .line 1110
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    iput-object v5, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->allEpisodes:Ljava/util/Map;

    .line 1111
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->collectSeasons([Lcom/google/wireless/android/video/magma/proto/AssetResource;)V

    .line 1112
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->collectEpisodes([Lcom/google/wireless/android/video/magma/proto/AssetResource;)V

    goto :goto_2
.end method

.method private processRemainingResponseAssets([Lcom/google/wireless/android/video/magma/proto/AssetResource;)V
    .locals 0
    .param p1, "assets"    # [Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    .line 1117
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->collectSeasons([Lcom/google/wireless/android/video/magma/proto/AssetResource;)V

    .line 1118
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->collectEpisodes([Lcom/google/wireless/android/video/magma/proto/AssetResource;)V

    .line 1119
    return-void
.end method

.method private requestAssets()Z
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1038
    iget-object v6, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->showId:Ljava/lang/String;

    invoke-static {v6}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromShowId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-array v7, v5, [Ljava/lang/String;

    invoke-static {v6, v7}, Lcom/google/android/videos/utils/CollectionUtil;->newArrayList(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1039
    .local v0, "assetIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0, v0, v4, v4}, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->requestAssets(Ljava/util/List;ZZ)Z

    move-result v6

    if-nez v6, :cond_1

    move v4, v5

    .line 1066
    :cond_0
    :goto_0
    return v4

    .line 1042
    :cond_1
    iget-object v6, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->show:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    if-nez v6, :cond_2

    move v4, v5

    .line 1043
    goto :goto_0

    .line 1045
    :cond_2
    iget-boolean v6, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->includeChildren:Z

    if-eqz v6, :cond_0

    .line 1050
    iget-object v6, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->remainingSeasonIds:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v3

    .line 1051
    .local v3, "remainingSeasonCount":I
    iget-object v6, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->remainingEpisodeIds:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v2

    .line 1052
    .local v2, "remainingEpisodeCount":I
    if-nez v3, :cond_3

    if-eqz v2, :cond_0

    .line 1055
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Show "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->showId:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " full metadata missing "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " requested seasons, "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " requested episodes. Fetching these separately."

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 1058
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1060
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v3, :cond_4

    .line 1061
    iget-object v4, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->remainingSeasonIds:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromSeasonId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1060
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1063
    :cond_4
    const/4 v1, 0x0

    :goto_2
    if-ge v1, v2, :cond_5

    .line 1064
    iget-object v4, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->remainingEpisodeIds:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromEpisodeId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1063
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1066
    :cond_5
    invoke-virtual {p0, v0, v5, v5}, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->requestAssets(Ljava/util/List;ZZ)Z

    move-result v4

    goto :goto_0
.end method

.method private storeEpisode(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/wireless/android/video/magma/proto/AssetResource;IILjava/lang/String;ZJ)V
    .locals 11
    .param p1, "transaction"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "episode"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p3, "seasonSeqno"    # I
    .param p4, "episodeSeqno"    # I
    .param p5, "nextEpisodeId"    # Ljava/lang/String;
    .param p6, "nextEpisodeInSameSeason"    # Z
    .param p7, "nowTimestamp"    # J

    .prologue
    .line 1343
    iget-object v2, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->playCountry:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->assetImageUriCreator:Lcom/google/android/videos/ui/AssetImageUriCreator;
    invoke-static {v3}, Lcom/google/android/videos/store/AssetStoreSync;->access$1000(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/ui/AssetImageUriCreator;

    move-result-object v3

    move-wide/from16 v0, p7

    invoke-static {p2, v2, v0, v1, v3}, Lcom/google/android/videos/store/PurchaseStoreUtil;->buildEpisodeContentValues(Lcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/lang/String;JLcom/google/android/videos/ui/AssetImageUriCreator;)Landroid/content/ContentValues;

    move-result-object v9

    .line 1345
    .local v9, "episodeValues":Landroid/content/ContentValues;
    invoke-static {v9, p1}, Lcom/google/android/videos/store/PurchaseStoreUtil;->loadMissingDataIntoVideoContentValues(Landroid/content/ContentValues;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1346
    const-string v2, "videos"

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->VIDEOS_EQUAL_COLUMNS:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/videos/store/AssetStoreSync;->access$1100()[Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v2, v9, v3}, Lcom/google/android/videos/utils/DbUtils;->updateOrReplace(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;[Ljava/lang/String;)J

    .line 1348
    iget-object v3, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->showId:Ljava/lang/String;

    move-object v2, p2

    move v4, p3

    move v5, p4

    move-object/from16 v6, p5

    move/from16 v7, p6

    invoke-static/range {v2 .. v7}, Lcom/google/android/videos/store/PurchaseStoreUtil;->buildEpisodeAssetContentValues(Lcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/lang/String;IILjava/lang/String;Z)Landroid/content/ContentValues;

    move-result-object v8

    .line 1350
    .local v8, "assetValues":Landroid/content/ContentValues;
    const-string v2, "assets"

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->ASSETS_EQUAL_COLUMNS:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/videos/store/AssetStoreSync;->access$1200()[Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v2, v8, v3}, Lcom/google/android/videos/utils/DbUtils;->updateOrReplace(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;[Ljava/lang/String;)J

    .line 1351
    return-void
.end method

.method private storeSeason(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/wireless/android/video/magma/proto/AssetResource;IJ)V
    .locals 4
    .param p1, "transaction"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "season"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p3, "seasonSeqno"    # I
    .param p4, "nowTimestamp"    # J

    .prologue
    .line 1332
    invoke-static {p2, p4, p5}, Lcom/google/android/videos/store/PurchaseStoreUtil;->buildSeasonContentValues(Lcom/google/wireless/android/video/magma/proto/AssetResource;J)Landroid/content/ContentValues;

    move-result-object v1

    .line 1333
    .local v1, "seasonValues":Landroid/content/ContentValues;
    const-string v2, "seasons"

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->SEASONS_EQUAL_COLUMNS:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/videos/store/AssetStoreSync;->access$1600()[Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v2, v1, v3}, Lcom/google/android/videos/utils/DbUtils;->updateOrReplace(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;[Ljava/lang/String;)J

    .line 1335
    invoke-static {p2, p3}, Lcom/google/android/videos/store/PurchaseStoreUtil;->buildSeasonAssetContentValues(Lcom/google/wireless/android/video/magma/proto/AssetResource;I)Landroid/content/ContentValues;

    move-result-object v0

    .line 1337
    .local v0, "assetValues":Landroid/content/ContentValues;
    const-string v2, "assets"

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->ASSETS_EQUAL_COLUMNS:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/videos/store/AssetStoreSync;->access$1200()[Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v2, v0, v3}, Lcom/google/android/videos/utils/DbUtils;->updateOrReplace(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;[Ljava/lang/String;)J

    .line 1338
    return-void
.end method

.method private storeShow(Landroid/database/sqlite/SQLiteDatabase;J)V
    .locals 4
    .param p1, "transaction"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "nowTimestamp"    # J

    .prologue
    .line 1315
    iget-object v2, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->show:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v3, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->assetImageUriCreator:Lcom/google/android/videos/ui/AssetImageUriCreator;
    invoke-static {v3}, Lcom/google/android/videos/store/AssetStoreSync;->access$1000(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/ui/AssetImageUriCreator;

    move-result-object v3

    invoke-static {v2, p2, p3, v3}, Lcom/google/android/videos/store/PurchaseStoreUtil;->buildShowContentValues(Lcom/google/wireless/android/video/magma/proto/AssetResource;JLcom/google/android/videos/ui/AssetImageUriCreator;)Landroid/content/ContentValues;

    move-result-object v1

    .line 1317
    .local v1, "showValues":Landroid/content/ContentValues;
    iget-boolean v2, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->includeChildren:Z

    if-eqz v2, :cond_0

    .line 1321
    const-string v2, "shows_full_sync_timestamp"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1323
    :cond_0
    invoke-static {v1, p1}, Lcom/google/android/videos/store/PurchaseStoreUtil;->loadMissingDataIntoShowContentValues(Landroid/content/ContentValues;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1324
    const-string v2, "shows"

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->SHOWS_EQUAL_COLUMNS:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/videos/store/AssetStoreSync;->access$1500()[Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v2, v1, v3}, Lcom/google/android/videos/utils/DbUtils;->updateOrReplace(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;[Ljava/lang/String;)J

    .line 1326
    iget-object v2, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->show:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-static {v2}, Lcom/google/android/videos/store/PurchaseStoreUtil;->buildShowAssetContentValues(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Landroid/content/ContentValues;

    move-result-object v0

    .line 1327
    .local v0, "assetValues":Landroid/content/ContentValues;
    const-string v2, "assets"

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->ASSETS_EQUAL_COLUMNS:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/videos/store/AssetStoreSync;->access$1200()[Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v2, v0, v3}, Lcom/google/android/videos/utils/DbUtils;->updateOrReplace(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;[Ljava/lang/String;)J

    .line 1328
    return-void
.end method

.method private updateAssets()V
    .locals 37

    .prologue
    .line 1198
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 1199
    .local v6, "nowTimestamp":J
    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    .line 1200
    .local v26, "processedEpisodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v2}, Lcom/google/android/videos/store/AssetStoreSync;->access$900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 1201
    .local v3, "transaction":Landroid/database/sqlite/SQLiteDatabase;
    const/16 v29, 0x0

    .line 1212
    .local v29, "success":Z
    :try_start_0
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v6, v7}, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->storeShow(Landroid/database/sqlite/SQLiteDatabase;J)V

    .line 1213
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->includeChildren:Z

    if-eqz v2, :cond_8

    .line 1214
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->seasons:Ljava/util/TreeSet;

    invoke-virtual {v2}, Ljava/util/TreeSet;->size()I

    move-result v27

    .line 1215
    .local v27, "seasonCount":I
    const-string v35, "show_id = ?"

    .line 1216
    .local v35, "whereUnseenSeasons":Ljava/lang/String;
    const-string v34, "assets_type = 19 AND root_id = ?"

    .line 1218
    .local v34, "whereUnseenSeasonAssets":Ljava/lang/String;
    if-lez v27, :cond_0

    .line 1219
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v35

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, " AND NOT ("

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, "season_id"

    move/from16 v0, v27

    invoke-static {v9, v0}, Lcom/google/android/videos/utils/DbUtils;->buildInMultipleParamsClause(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, ")"

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    .line 1221
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, " AND NOT ("

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, "assets_id"

    move/from16 v0, v27

    invoke-static {v9, v0}, Lcom/google/android/videos/utils/DbUtils;->buildInMultipleParamsClause(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, ")"

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    .line 1226
    :cond_0
    add-int/lit8 v2, v27, 0x1

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v36, v0

    .line 1227
    .local v36, "whereUnseenSeasonsArgs":[Ljava/lang/String;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->showId:Ljava/lang/String;

    aput-object v9, v36, v2

    .line 1231
    add-int/lit8 v5, v27, -0x1

    .line 1232
    .local v5, "seasonSeqno":I
    const/16 v24, 0x0

    .line 1233
    .local v24, "nextEpisodeId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->seasons:Ljava/util/TreeSet;

    invoke-virtual {v2}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :goto_0
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .local v4, "season":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    move-object/from16 v2, p0

    .line 1235
    invoke-direct/range {v2 .. v7}, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->storeSeason(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/wireless/android/video/magma/proto/AssetResource;IJ)V

    .line 1236
    iget-object v2, v4, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v0, v2, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    move-object/from16 v28, v0

    .line 1238
    .local v28, "seasonId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    invoke-static/range {v28 .. v28}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromSeasonId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->addScheduledAsset(Ljava/lang/String;)Z

    .line 1239
    add-int/lit8 v2, v5, 0x1

    aput-object v28, v36, v2

    .line 1241
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->allEpisodes:Ljava/util/Map;

    move-object/from16 v0, v28

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/util/TreeSet;

    .line 1242
    .local v19, "episodes":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Lcom/google/wireless/android/video/magma/proto/AssetResource;>;"
    invoke-virtual/range {v19 .. v19}, Ljava/util/TreeSet;->size()I

    move-result v8

    .line 1243
    .local v8, "episodeCount":I
    const-string v32, "episode_season_id IS ?"

    .line 1244
    .local v32, "whereUnseenEpisodes":Ljava/lang/String;
    const-string v30, "assets_type = 20 AND root_id = ? AND season_seqno = ?"

    .line 1246
    .local v30, "whereUnseenEpisodeAssets":Ljava/lang/String;
    if-lez v8, :cond_1

    .line 1247
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, " AND NOT ("

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, "video_id"

    invoke-static {v9, v8}, Lcom/google/android/videos/utils/DbUtils;->buildInMultipleParamsClause(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, ")"

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    .line 1249
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v30

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, " AND NOT ("

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, "assets_id"

    invoke-static {v9, v8}, Lcom/google/android/videos/utils/DbUtils;->buildInMultipleParamsClause(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, ")"

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    .line 1255
    :cond_1
    add-int/lit8 v2, v8, 0x1

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v33, v0

    .line 1256
    .local v33, "whereUnseenEpisodesArgs":[Ljava/lang/String;
    const/4 v2, 0x0

    aput-object v28, v33, v2

    .line 1257
    add-int/lit8 v2, v8, 0x2

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v31, v0

    .line 1258
    .local v31, "whereUnseenEpisodeAssetsArgs":[Ljava/lang/String;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->showId:Ljava/lang/String;

    aput-object v9, v31, v2

    .line 1259
    const/4 v2, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v31, v2

    .line 1261
    add-int/lit8 v13, v8, -0x1

    .line 1262
    .local v13, "episodeSeqno":I
    const/16 v25, 0x0

    .line 1263
    .local v25, "nextEpisodeInSameSeason":Z
    invoke-virtual/range {v19 .. v19}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v22

    .local v22, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 1265
    .local v11, "episode":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    iget-object v2, v11, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-boolean v0, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->bonusContent:Z

    move/from16 v23, v0

    .line 1267
    .local v23, "isBonusContent":Z
    if-eqz v23, :cond_4

    const/4 v14, 0x0

    :goto_2
    if-nez v23, :cond_5

    if-eqz v25, :cond_5

    const/4 v15, 0x1

    :goto_3
    move-object/from16 v9, p0

    move-object v10, v3

    move v12, v5

    move-wide/from16 v16, v6

    invoke-direct/range {v9 .. v17}, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->storeEpisode(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/wireless/android/video/magma/proto/AssetResource;IILjava/lang/String;ZJ)V

    .line 1270
    iget-object v2, v11, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v0, v2, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    move-object/from16 v18, v0

    .line 1271
    .local v18, "episodeId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->requestedEpisodeIds:Ljava/util/List;

    move-object/from16 v0, v18

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1272
    move-object/from16 v0, v26

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1275
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    invoke-static/range {v18 .. v18}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromEpisodeId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->addScheduledAsset(Ljava/lang/String;)Z

    .line 1276
    add-int/lit8 v2, v13, 0x1

    aput-object v18, v33, v2

    .line 1277
    add-int/lit8 v2, v13, 0x2

    aput-object v18, v31, v2

    .line 1279
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->account:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-static {v3, v2, v0}, Lcom/google/android/videos/store/UserAssetsUtil;->refreshVideoRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 1280
    add-int/lit8 v13, v13, -0x1

    .line 1281
    if-nez v23, :cond_2

    .line 1282
    move-object/from16 v24, v18

    .line 1283
    const/16 v25, 0x1

    goto :goto_1

    .end local v18    # "episodeId":Ljava/lang/String;
    :cond_4
    move-object/from16 v14, v24

    .line 1267
    goto :goto_2

    :cond_5
    const/4 v15, 0x0

    goto :goto_3

    .line 1287
    .end local v11    # "episode":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .end local v23    # "isBonusContent":Z
    :cond_6
    const-string v2, "videos"

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    invoke-virtual {v3, v2, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1288
    const-string v2, "assets"

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    invoke-virtual {v3, v2, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1291
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->account:Ljava/lang/String;

    move-object/from16 v0, v28

    invoke-static {v3, v2, v0}, Lcom/google/android/videos/store/UserAssetsUtil;->refreshSeasonRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 1292
    add-int/lit8 v5, v5, -0x1

    .line 1293
    goto/16 :goto_0

    .line 1295
    .end local v4    # "season":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .end local v8    # "episodeCount":I
    .end local v13    # "episodeSeqno":I
    .end local v19    # "episodes":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Lcom/google/wireless/android/video/magma/proto/AssetResource;>;"
    .end local v22    # "i$":Ljava/util/Iterator;
    .end local v25    # "nextEpisodeInSameSeason":Z
    .end local v28    # "seasonId":Ljava/lang/String;
    .end local v30    # "whereUnseenEpisodeAssets":Ljava/lang/String;
    .end local v31    # "whereUnseenEpisodeAssetsArgs":[Ljava/lang/String;
    .end local v32    # "whereUnseenEpisodes":Ljava/lang/String;
    .end local v33    # "whereUnseenEpisodesArgs":[Ljava/lang/String;
    :cond_7
    const-string v2, "seasons"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    invoke-virtual {v3, v2, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1296
    const-string v2, "assets"

    move-object/from16 v0, v34

    move-object/from16 v1, v36

    invoke-virtual {v3, v2, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1298
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->account:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->showId:Ljava/lang/String;

    invoke-static {v3, v2, v9}, Lcom/google/android/videos/store/UserAssetsUtil;->refreshShowRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1300
    .end local v5    # "seasonSeqno":I
    .end local v24    # "nextEpisodeId":Ljava/lang/String;
    .end local v27    # "seasonCount":I
    .end local v34    # "whereUnseenSeasonAssets":Ljava/lang/String;
    .end local v35    # "whereUnseenSeasons":Ljava/lang/String;
    .end local v36    # "whereUnseenSeasonsArgs":[Ljava/lang/String;
    :cond_8
    const/16 v29, 0x1

    .line 1302
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v2}, Lcom/google/android/videos/store/AssetStoreSync;->access$900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v2

    const/4 v9, 0x5

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->showId:Ljava/lang/String;

    aput-object v14, v10, v12

    move/from16 v0, v29

    invoke-virtual {v2, v3, v0, v9, v10}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 1307
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->priority:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->showId:Ljava/lang/String;

    # invokes: Lcom/google/android/videos/store/AssetStoreSync;->maybeSyncShowPoster(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V
    invoke-static {v2, v9, v10, v12}, Lcom/google/android/videos/store/AssetStoreSync;->access$1300(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V

    .line 1308
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->priority:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->showId:Ljava/lang/String;

    # invokes: Lcom/google/android/videos/store/AssetStoreSync;->maybeSyncShowBanner(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V
    invoke-static {v2, v9, v10, v12}, Lcom/google/android/videos/store/AssetStoreSync;->access$1400(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V

    .line 1309
    const/16 v20, 0x0

    .local v20, "i":I
    :goto_4
    invoke-interface/range {v26 .. v26}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, v20

    if-ge v0, v2, :cond_9

    .line 1310
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    move-object/from16 v0, p0

    iget v10, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->priority:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    move-object/from16 v0, v26

    move/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    # invokes: Lcom/google/android/videos/store/AssetStoreSync;->maybeSyncVideoScreenshot(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V
    invoke-static {v9, v10, v12, v2}, Lcom/google/android/videos/store/AssetStoreSync;->access$800(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V

    .line 1309
    add-int/lit8 v20, v20, 0x1

    goto :goto_4

    .line 1302
    .end local v20    # "i":I
    :catchall_0
    move-exception v2

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v9}, Lcom/google/android/videos/store/AssetStoreSync;->access$900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v9

    const/4 v10, 0x5

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->showId:Ljava/lang/String;

    aput-object v15, v12, v14

    move/from16 v0, v29

    invoke-virtual {v9, v3, v0, v10, v12}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v2

    .line 1312
    .restart local v20    # "i":I
    :cond_9
    return-void
.end method


# virtual methods
.method public compare(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/AssetResource;)I
    .locals 3
    .param p1, "lhs"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p2, "rhs"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    .line 1359
    if-ne p1, p2, :cond_1

    .line 1360
    const/4 v0, 0x0

    .line 1373
    :cond_0
    :goto_0
    return v0

    .line 1361
    :cond_1
    if-nez p1, :cond_2

    .line 1362
    const/4 v0, -0x1

    goto :goto_0

    .line 1363
    :cond_2
    if-nez p2, :cond_3

    .line 1364
    const/4 v0, 0x1

    goto :goto_0

    .line 1367
    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->compareSequenceNumbers(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/AssetResource;)I

    move-result v0

    .line 1368
    .local v0, "result":I
    if-nez v0, :cond_0

    .line 1373
    iget-object v1, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v1, v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    iget-object v2, p2, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v2, v2, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 870
    check-cast p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->compare(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/AssetResource;)I

    move-result v0

    return v0
.end method

.method protected doSync()V
    .locals 4

    .prologue
    .line 924
    invoke-direct {p0}, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->checkMetadataUpToDate()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 936
    :cond_0
    :goto_0
    return-void

    .line 927
    :cond_1
    invoke-direct {p0}, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->requestAssets()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 930
    invoke-direct {p0}, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->updateAssets()V

    .line 931
    iget-boolean v0, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->includeChildren:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->remainingSeasonIds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->remainingEpisodeIds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 932
    :cond_2
    const/16 v0, 0x14

    new-instance v1, Lcom/google/android/videos/store/SyncTaskManager$DataException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not download metadata for these season/episode assets when requesting show "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->showId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->remainingSeasonIds:Ljava/util/List;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->remainingEpisodeIds:Ljava/util/List;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/videos/store/SyncTaskManager$DataException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->onSyncError(ILjava/lang/Exception;)V

    goto :goto_0
.end method

.method public onResponse(Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/api/AssetsRequest;
    .param p2, "response"    # Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    .prologue
    .line 1071
    iget-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->show:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    if-nez v0, :cond_0

    .line 1072
    iget-object v0, p2, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-direct {p0, v0}, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->processFirstResponseAssets([Lcom/google/wireless/android/video/magma/proto/AssetResource;)V

    .line 1076
    :goto_0
    return-void

    .line 1074
    :cond_0
    iget-object v0, p2, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-direct {p0, v0}, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->processRemainingResponseAssets([Lcom/google/wireless/android/video/magma/proto/AssetResource;)V

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 870
    check-cast p1, Lcom/google/android/videos/api/AssetsRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->onResponse(Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;)V

    return-void
.end method
