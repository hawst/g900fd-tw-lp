.class public Lcom/google/android/videos/settings/PresetPreference;
.super Lcom/google/android/videos/settings/GridDialogPreference;
.source "PresetPreference.java"


# instance fields
.field private final assetManager:Landroid/content/res/AssetManager;

.field private final preferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/settings/GridDialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    const v0, 0x7f04003f

    invoke-virtual {p0, v0}, Lcom/google/android/videos/settings/PresetPreference;->setDialogLayoutResource(I)V

    .line 33
    const v0, 0x7f040040

    invoke-virtual {p0, v0}, Lcom/google/android/videos/settings/PresetPreference;->setGridItemLayoutResource(I)V

    .line 34
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/settings/PresetPreference;->assetManager:Landroid/content/res/AssetManager;

    .line 35
    invoke-static {p1}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/settings/PresetPreference;->preferences:Landroid/content/SharedPreferences;

    .line 36
    return-void
.end method


# virtual methods
.method protected decodeStringValue(Ljava/lang/String;)I
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-static {p1}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method protected onBindGridItem(Landroid/view/View;I)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;
    .param p2, "index"    # I

    .prologue
    .line 50
    const v4, 0x7f0f011c

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/text/SubtitleView;

    .line 51
    .local v0, "previewText":Lcom/google/android/exoplayer/text/SubtitleView;
    invoke-virtual {p0, p2}, Lcom/google/android/videos/settings/PresetPreference;->getValueAt(I)I

    move-result v4

    iget-object v5, p0, Lcom/google/android/videos/settings/PresetPreference;->preferences:Landroid/content/SharedPreferences;

    iget-object v6, p0, Lcom/google/android/videos/settings/PresetPreference;->assetManager:Landroid/content/res/AssetManager;

    invoke-static {v4, v5, v6}, Lcom/google/android/videos/subtitles/CaptionStyleUtil;->createCaptionStyleFromPreferences(ILandroid/content/SharedPreferences;Landroid/content/res/AssetManager;)Lcom/google/android/exoplayer/text/CaptionStyleCompat;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/exoplayer/text/SubtitleView;->setStyle(Lcom/google/android/exoplayer/text/CaptionStyleCompat;)V

    .line 53
    invoke-virtual {p0}, Lcom/google/android/videos/settings/PresetPreference;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v1, v4, Landroid/util/DisplayMetrics;->density:F

    .line 54
    .local v1, "scale":F
    const/high16 v4, 0x42000000    # 32.0f

    mul-float/2addr v4, v1

    invoke-virtual {v0, v4}, Lcom/google/android/exoplayer/text/SubtitleView;->setTextSize(F)V

    .line 56
    invoke-virtual {p0, p2}, Lcom/google/android/videos/settings/PresetPreference;->getTitleAt(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 57
    .local v3, "title":Ljava/lang/CharSequence;
    if-eqz v3, :cond_0

    .line 58
    const v4, 0x7f0f011a

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 59
    .local v2, "summary":Landroid/widget/TextView;
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    .end local v2    # "summary":Landroid/widget/TextView;
    :cond_0
    return-void
.end method

.method public shouldDisableDependents()Z
    .locals 2

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/google/android/videos/settings/PresetPreference;->getValue()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-super {p0}, Lcom/google/android/videos/settings/GridDialogPreference;->shouldDisableDependents()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
