.class Lcom/google/android/videos/tagging/ExtendedSongProfileView$5;
.super Lcom/google/android/videos/ui/TransitionUtil$TransitionListenerAdapter;
.source "ExtendedSongProfileView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/tagging/ExtendedSongProfileView;->startHideTransitionStepTwo(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/tagging/ExtendedSongProfileView;


# direct methods
.method constructor <init>(Lcom/google/android/videos/tagging/ExtendedSongProfileView;)V
    .locals 0

    .prologue
    .line 326
    iput-object p1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView$5;->this$0:Lcom/google/android/videos/tagging/ExtendedSongProfileView;

    invoke-direct {p0}, Lcom/google/android/videos/ui/TransitionUtil$TransitionListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onTransitionEnd(Landroid/transition/Transition;)V
    .locals 3
    .param p1, "transition"    # Landroid/transition/Transition;

    .prologue
    const/4 v2, 0x0

    .line 329
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView$5;->this$0:Lcom/google/android/videos/tagging/ExtendedSongProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedSongProfileView;->transitionProfileImageView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->access$200(Lcom/google/android/videos/tagging/ExtendedSongProfileView;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    .line 330
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView$5;->this$0:Lcom/google/android/videos/tagging/ExtendedSongProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedSongProfileView;->transitionProfileImageFrame:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->access$000(Lcom/google/android/videos/tagging/ExtendedSongProfileView;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setTransitionName(Ljava/lang/String;)V

    .line 331
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView$5;->this$0:Lcom/google/android/videos/tagging/ExtendedSongProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedSongProfileView;->transitionProfileImageFrame:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->access$000(Lcom/google/android/videos/tagging/ExtendedSongProfileView;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 332
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView$5;->this$0:Lcom/google/android/videos/tagging/ExtendedSongProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedSongProfileView;->transitionProfileImageFrame:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->access$000(Lcom/google/android/videos/tagging/ExtendedSongProfileView;)Landroid/view/ViewGroup;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setHasTransientState(Z)V

    .line 333
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView$5;->this$0:Lcom/google/android/videos/tagging/ExtendedSongProfileView;

    # setter for: Lcom/google/android/videos/tagging/ExtendedSongProfileView;->transitionProfileImageView:Landroid/view/View;
    invoke-static {v0, v2}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->access$202(Lcom/google/android/videos/tagging/ExtendedSongProfileView;Landroid/view/View;)Landroid/view/View;

    .line 334
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView$5;->this$0:Lcom/google/android/videos/tagging/ExtendedSongProfileView;

    # setter for: Lcom/google/android/videos/tagging/ExtendedSongProfileView;->transitionProfileImageFrame:Landroid/view/ViewGroup;
    invoke-static {v0, v2}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->access$002(Lcom/google/android/videos/tagging/ExtendedSongProfileView;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;

    .line 335
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView$5;->this$0:Lcom/google/android/videos/tagging/ExtendedSongProfileView;

    # setter for: Lcom/google/android/videos/tagging/ExtendedSongProfileView;->viewRoot:Landroid/view/ViewGroup;
    invoke-static {v0, v2}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->access$302(Lcom/google/android/videos/tagging/ExtendedSongProfileView;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;

    .line 336
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView$5;->this$0:Lcom/google/android/videos/tagging/ExtendedSongProfileView;

    # setter for: Lcom/google/android/videos/tagging/ExtendedSongProfileView;->entitiesViewGroup:Landroid/view/ViewGroup;
    invoke-static {v0, v2}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->access$402(Lcom/google/android/videos/tagging/ExtendedSongProfileView;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;

    .line 337
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView$5;->this$0:Lcom/google/android/videos/tagging/ExtendedSongProfileView;

    # invokes: Lcom/google/android/videos/tagging/ExtendedSongProfileView;->hideInternal()V
    invoke-static {v0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->access$500(Lcom/google/android/videos/tagging/ExtendedSongProfileView;)V

    .line 338
    return-void
.end method
