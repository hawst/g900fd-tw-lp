.class public Lcom/google/android/videos/store/VideoDownloadStatus;
.super Ljava/lang/Object;
.source "VideoDownloadStatus.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J


# instance fields
.field public final bytesDownloaded:Ljava/lang/Long;

.field public final downloadSize:Ljava/lang/Long;

.field public final drmErrorCode:Ljava/lang/Integer;

.field public final haveLicense:Z

.field public final pinned:Z

.field public final pinningStatus:Ljava/lang/Integer;

.field public final pinningStatusReason:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(ZZLjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;)V
    .locals 0
    .param p1, "pinned"    # Z
    .param p2, "haveLicense"    # Z
    .param p3, "pinningStatus"    # Ljava/lang/Integer;
    .param p4, "pinningStatusReason"    # Ljava/lang/Integer;
    .param p5, "drmErrorCode"    # Ljava/lang/Integer;
    .param p6, "downloadSize"    # Ljava/lang/Long;
    .param p7, "bytesDownloaded"    # Ljava/lang/Long;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-boolean p1, p0, Lcom/google/android/videos/store/VideoDownloadStatus;->pinned:Z

    .line 32
    iput-boolean p2, p0, Lcom/google/android/videos/store/VideoDownloadStatus;->haveLicense:Z

    .line 33
    iput-object p3, p0, Lcom/google/android/videos/store/VideoDownloadStatus;->pinningStatus:Ljava/lang/Integer;

    .line 34
    iput-object p4, p0, Lcom/google/android/videos/store/VideoDownloadStatus;->pinningStatusReason:Ljava/lang/Integer;

    .line 35
    iput-object p5, p0, Lcom/google/android/videos/store/VideoDownloadStatus;->drmErrorCode:Ljava/lang/Integer;

    .line 36
    iput-object p6, p0, Lcom/google/android/videos/store/VideoDownloadStatus;->downloadSize:Ljava/lang/Long;

    .line 37
    iput-object p7, p0, Lcom/google/android/videos/store/VideoDownloadStatus;->bytesDownloaded:Ljava/lang/Long;

    .line 38
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 42
    if-ne p0, p1, :cond_1

    .line 49
    :cond_0
    :goto_0
    return v1

    .line 45
    :cond_1
    instance-of v3, p1, Lcom/google/android/videos/store/VideoDownloadStatus;

    if-nez v3, :cond_2

    move v1, v2

    .line 46
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 48
    check-cast v0, Lcom/google/android/videos/store/VideoDownloadStatus;

    .line 49
    .local v0, "that":Lcom/google/android/videos/store/VideoDownloadStatus;
    iget-boolean v3, p0, Lcom/google/android/videos/store/VideoDownloadStatus;->pinned:Z

    iget-boolean v4, v0, Lcom/google/android/videos/store/VideoDownloadStatus;->pinned:Z

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/google/android/videos/store/VideoDownloadStatus;->haveLicense:Z

    iget-boolean v4, v0, Lcom/google/android/videos/store/VideoDownloadStatus;->haveLicense:Z

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/android/videos/store/VideoDownloadStatus;->pinningStatus:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/google/android/videos/store/VideoDownloadStatus;->pinningStatus:Ljava/lang/Integer;

    invoke-static {v3, v4}, Lcom/google/android/exoplayer/util/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/videos/store/VideoDownloadStatus;->pinningStatusReason:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/google/android/videos/store/VideoDownloadStatus;->pinningStatusReason:Ljava/lang/Integer;

    invoke-static {v3, v4}, Lcom/google/android/exoplayer/util/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/videos/store/VideoDownloadStatus;->drmErrorCode:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/google/android/videos/store/VideoDownloadStatus;->drmErrorCode:Ljava/lang/Integer;

    invoke-static {v3, v4}, Lcom/google/android/exoplayer/util/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/videos/store/VideoDownloadStatus;->downloadSize:Ljava/lang/Long;

    iget-object v4, v0, Lcom/google/android/videos/store/VideoDownloadStatus;->downloadSize:Ljava/lang/Long;

    invoke-static {v3, v4}, Lcom/google/android/exoplayer/util/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/videos/store/VideoDownloadStatus;->bytesDownloaded:Ljava/lang/Long;

    iget-object v4, v0, Lcom/google/android/videos/store/VideoDownloadStatus;->bytesDownloaded:Ljava/lang/Long;

    invoke-static {v3, v4}, Lcom/google/android/exoplayer/util/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 60
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 61
    .local v0, "result":I
    mul-int/lit8 v4, v0, 0x25

    iget-boolean v1, p0, Lcom/google/android/videos/store/VideoDownloadStatus;->pinned:Z

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v4, v1

    .line 62
    mul-int/lit8 v1, v0, 0x25

    iget-boolean v4, p0, Lcom/google/android/videos/store/VideoDownloadStatus;->haveLicense:Z

    if-eqz v4, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 63
    mul-int/lit8 v2, v0, 0x25

    iget-object v1, p0, Lcom/google/android/videos/store/VideoDownloadStatus;->pinningStatus:Ljava/lang/Integer;

    if-nez v1, :cond_2

    move v1, v3

    :goto_2
    add-int v0, v2, v1

    .line 64
    mul-int/lit8 v2, v0, 0x25

    iget-object v1, p0, Lcom/google/android/videos/store/VideoDownloadStatus;->pinningStatusReason:Ljava/lang/Integer;

    if-nez v1, :cond_3

    move v1, v3

    :goto_3
    add-int v0, v2, v1

    .line 65
    mul-int/lit8 v2, v0, 0x25

    iget-object v1, p0, Lcom/google/android/videos/store/VideoDownloadStatus;->drmErrorCode:Ljava/lang/Integer;

    if-nez v1, :cond_4

    move v1, v3

    :goto_4
    add-int v0, v2, v1

    .line 66
    mul-int/lit8 v2, v0, 0x25

    iget-object v1, p0, Lcom/google/android/videos/store/VideoDownloadStatus;->downloadSize:Ljava/lang/Long;

    if-nez v1, :cond_5

    move v1, v3

    :goto_5
    add-int v0, v2, v1

    .line 67
    mul-int/lit8 v1, v0, 0x25

    iget-object v2, p0, Lcom/google/android/videos/store/VideoDownloadStatus;->bytesDownloaded:Ljava/lang/Long;

    if-nez v2, :cond_6

    :goto_6
    add-int v0, v1, v3

    .line 68
    return v0

    :cond_0
    move v1, v3

    .line 61
    goto :goto_0

    :cond_1
    move v2, v3

    .line 62
    goto :goto_1

    .line 63
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/store/VideoDownloadStatus;->pinningStatus:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_2

    .line 64
    :cond_3
    iget-object v1, p0, Lcom/google/android/videos/store/VideoDownloadStatus;->pinningStatusReason:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_3

    .line 65
    :cond_4
    iget-object v1, p0, Lcom/google/android/videos/store/VideoDownloadStatus;->drmErrorCode:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_4

    .line 66
    :cond_5
    iget-object v1, p0, Lcom/google/android/videos/store/VideoDownloadStatus;->downloadSize:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_5

    .line 67
    :cond_6
    iget-object v2, p0, Lcom/google/android/videos/store/VideoDownloadStatus;->bytesDownloaded:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v3

    goto :goto_6
.end method
