.class public interface abstract Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;
.super Ljava/lang/Object;
.source "ControllerOverlay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/overlay/ControllerOverlay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onDialogCanceled(I)V
.end method

.method public abstract onDialogConfirmed(I)V
.end method

.method public abstract onHQ()V
.end method

.method public abstract onHidden()V
.end method

.method public abstract onHidePending()V
.end method

.method public abstract onPause()V
.end method

.method public abstract onPlay()V
.end method

.method public abstract onScrubbingCanceled()V
.end method

.method public abstract onScrubbingStart(Z)V
.end method

.method public abstract onSeekTo(ZIZ)V
.end method

.method public abstract onShown()V
.end method
