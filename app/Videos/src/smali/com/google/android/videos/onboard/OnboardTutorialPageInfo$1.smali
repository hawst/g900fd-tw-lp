.class Lcom/google/android/videos/onboard/OnboardTutorialPageInfo$1;
.super Ljava/lang/Object;
.source "OnboardTutorialPageInfo.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;->getStartButtonInfo(Lcom/google/android/play/onboard/OnboardHostControl;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;

.field final synthetic val$hostControl:Lcom/google/android/play/onboard/OnboardHostControl;


# direct methods
.method constructor <init>(Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;Lcom/google/android/play/onboard/OnboardHostControl;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo$1;->this$0:Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;

    iput-object p2, p0, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo$1;->val$hostControl:Lcom/google/android/play/onboard/OnboardHostControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo$1;->this$0:Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;

    # getter for: Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;->pageIndex:I
    invoke-static {v0}, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;->access$000(Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;)I

    move-result v0

    if-nez v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo$1;->val$hostControl:Lcom/google/android/play/onboard/OnboardHostControl;

    invoke-interface {v0}, Lcom/google/android/play/onboard/OnboardHostControl;->finishOnboardFlow()V

    .line 63
    :goto_0
    return-void

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo$1;->val$hostControl:Lcom/google/android/play/onboard/OnboardHostControl;

    invoke-interface {v0}, Lcom/google/android/play/onboard/OnboardHostControl;->goToPreviousPage()V

    goto :goto_0
.end method
