.class public final Lcom/google/android/videos/tagging/KnowledgeViewHelper;
.super Ljava/lang/Object;
.source "KnowledgeViewHelper.java"


# instance fields
.field private currentChunkData:Lcom/google/android/videos/tagging/ChunkData;

.field private currentChunkInfo:Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

.field private knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

.field private knowledgeState:I

.field private loadingChunkInfo:Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

.field private minTagArea:F

.field private playerTimeSupplier:Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;

.field private showKnowledgeAtMillis:I

.field private showRecentActorsWithinMillis:I

.field private final taggedKnowledgeEntities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;",
            ">;"
        }
    .end annotation
.end field

.field private taggingKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

.field private taglessKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

.field private videoDisplayHeight:I

.field private videoDisplayWidth:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taggedKnowledgeEntities:Ljava/util/List;

    .line 46
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/tagging/KnowledgeViewHelper;)Lcom/google/android/videos/tagging/TagStream$ChunkInfo;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->loadingChunkInfo:Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/videos/tagging/KnowledgeViewHelper;Lcom/google/android/videos/tagging/TagStream$ChunkInfo;)Lcom/google/android/videos/tagging/TagStream$ChunkInfo;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/tagging/KnowledgeViewHelper;
    .param p1, "x1"    # Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->loadingChunkInfo:Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

    return-object p1
.end method

.method static synthetic access$102(Lcom/google/android/videos/tagging/KnowledgeViewHelper;Lcom/google/android/videos/tagging/TagStream$ChunkInfo;)Lcom/google/android/videos/tagging/TagStream$ChunkInfo;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/tagging/KnowledgeViewHelper;
    .param p1, "x1"    # Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->currentChunkInfo:Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

    return-object p1
.end method

.method static synthetic access$202(Lcom/google/android/videos/tagging/KnowledgeViewHelper;Lcom/google/android/videos/tagging/ChunkData;)Lcom/google/android/videos/tagging/ChunkData;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/tagging/KnowledgeViewHelper;
    .param p1, "x1"    # Lcom/google/android/videos/tagging/ChunkData;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->currentChunkData:Lcom/google/android/videos/tagging/ChunkData;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/videos/tagging/KnowledgeViewHelper;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    .prologue
    .line 21
    iget v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->showKnowledgeAtMillis:I

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/videos/tagging/KnowledgeViewHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->showTaggedKnowledge()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/videos/tagging/KnowledgeViewHelper;)Lcom/google/android/videos/tagging/KnowledgeView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taglessKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/videos/tagging/KnowledgeViewHelper;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taggedKnowledgeEntities:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/videos/tagging/KnowledgeViewHelper;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    .prologue
    .line 21
    iget v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->showRecentActorsWithinMillis:I

    return v0
.end method

.method private loadChunkData(I)Z
    .locals 6
    .param p1, "timeMillis"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 158
    iget-object v4, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->currentChunkInfo:Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->currentChunkInfo:Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

    iget v4, v4, Lcom/google/android/videos/tagging/TagStream$ChunkInfo;->startMillis:I

    if-gt v4, p1, :cond_0

    iget-object v4, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->currentChunkInfo:Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

    iget v4, v4, Lcom/google/android/videos/tagging/TagStream$ChunkInfo;->endMillis:I

    if-ge p1, v4, :cond_0

    .line 160
    iput-object v5, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->loadingChunkInfo:Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

    .line 201
    :goto_0
    return v2

    .line 163
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

    .line 164
    .local v1, "currentKnowledgeBundle":Lcom/google/android/videos/tagging/KnowledgeBundle;
    iget-object v4, v1, Lcom/google/android/videos/tagging/KnowledgeBundle;->tagStream:Lcom/google/android/videos/tagging/TagStream;

    invoke-virtual {v4, p1}, Lcom/google/android/videos/tagging/TagStream;->getChunkAt(I)Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

    move-result-object v0

    .line 165
    .local v0, "chunkInfo":Lcom/google/android/videos/tagging/TagStream$ChunkInfo;
    if-nez v0, :cond_1

    .line 166
    iput-object v5, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->currentChunkInfo:Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

    .line 167
    iput-object v5, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->currentChunkData:Lcom/google/android/videos/tagging/ChunkData;

    .line 168
    iput-object v5, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->loadingChunkInfo:Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

    goto :goto_0

    .line 170
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->loadingChunkInfo:Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

    if-ne v0, v2, :cond_2

    move v2, v3

    .line 171
    goto :goto_0

    .line 173
    :cond_2
    iput-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->loadingChunkInfo:Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

    .line 174
    new-instance v2, Lcom/google/android/videos/tagging/KnowledgeViewHelper$1;

    invoke-direct {v2, p0, v1, v0}, Lcom/google/android/videos/tagging/KnowledgeViewHelper$1;-><init>(Lcom/google/android/videos/tagging/KnowledgeViewHelper;Lcom/google/android/videos/tagging/KnowledgeBundle;Lcom/google/android/videos/tagging/TagStream$ChunkInfo;)V

    new-array v4, v3, [Ljava/lang/Void;

    invoke-virtual {v2, v4}, Lcom/google/android/videos/tagging/KnowledgeViewHelper$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move v2, v3

    .line 201
    goto :goto_0
.end method

.method private showTaggedKnowledge()V
    .locals 15

    .prologue
    const/4 v0, 0x1

    .line 205
    iget-object v1, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taggingKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->videoDisplayWidth:I

    if-lez v1, :cond_0

    iget v1, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->videoDisplayHeight:I

    if-lez v1, :cond_0

    move v10, v0

    .line 207
    .local v10, "showTags":Z
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taggedKnowledgeEntities:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 208
    iget-object v1, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->currentChunkData:Lcom/google/android/videos/tagging/ChunkData;

    if-eqz v1, :cond_4

    .line 209
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 212
    .local v6, "activeTags":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/Tag;>;"
    iget-object v1, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

    iget-object v1, v1, Lcom/google/android/videos/tagging/KnowledgeBundle;->tagStream:Lcom/google/android/videos/tagging/TagStream;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/tagging/TagStream;->toMillis(I)I

    move-result v11

    .line 213
    .local v11, "skippedMillis":I
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->currentChunkData:Lcom/google/android/videos/tagging/ChunkData;

    iget v1, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->showKnowledgeAtMillis:I

    invoke-virtual {v0, v1, v6, v11}, Lcom/google/android/videos/tagging/ChunkData;->getActiveTags(ILjava/util/List;I)V

    .line 216
    if-eqz v10, :cond_1

    iget v14, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->videoDisplayWidth:I

    .line 217
    .local v14, "width":I
    :goto_1
    if-eqz v10, :cond_2

    iget v7, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->videoDisplayHeight:I

    .line 218
    .local v7, "height":I
    :goto_2
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_3
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-ge v8, v0, :cond_3

    .line 219
    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/videos/tagging/Tag;

    .line 220
    .local v12, "tag":Lcom/google/android/videos/tagging/Tag;
    iget v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->showKnowledgeAtMillis:I

    int-to-float v1, v14

    int-to-float v2, v7

    iget v3, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->minTagArea:F

    invoke-virtual {v12, v0, v1, v2, v3}, Lcom/google/android/videos/tagging/Tag;->getTagShape(IFFF)Lcom/google/android/videos/tagging/Tag$TagShape;

    move-result-object v13

    .line 221
    .local v13, "tagShape":Lcom/google/android/videos/tagging/Tag$TagShape;
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

    iget-object v0, v0, Lcom/google/android/videos/tagging/KnowledgeBundle;->tagStream:Lcom/google/android/videos/tagging/TagStream;

    iget v1, v12, Lcom/google/android/videos/tagging/Tag;->splitId:I

    invoke-virtual {v0, v1}, Lcom/google/android/videos/tagging/TagStream;->getKnowledgeEntityBySplitId(I)Lcom/google/android/videos/tagging/KnowledgeEntity;

    move-result-object v9

    .line 223
    .local v9, "knowledgeEntity":Lcom/google/android/videos/tagging/KnowledgeEntity;
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taggedKnowledgeEntities:Ljava/util/List;

    new-instance v1, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;

    iget v2, v12, Lcom/google/android/videos/tagging/Tag;->splitId:I

    invoke-direct {v1, v9, v2, v13}, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;-><init>(Lcom/google/android/videos/tagging/KnowledgeEntity;ILcom/google/android/videos/tagging/Tag$TagShape;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 205
    .end local v6    # "activeTags":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/Tag;>;"
    .end local v7    # "height":I
    .end local v8    # "i":I
    .end local v9    # "knowledgeEntity":Lcom/google/android/videos/tagging/KnowledgeEntity;
    .end local v10    # "showTags":Z
    .end local v11    # "skippedMillis":I
    .end local v12    # "tag":Lcom/google/android/videos/tagging/Tag;
    .end local v13    # "tagShape":Lcom/google/android/videos/tagging/Tag$TagShape;
    .end local v14    # "width":I
    :cond_0
    const/4 v10, 0x0

    goto :goto_0

    .line 216
    .restart local v6    # "activeTags":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/Tag;>;"
    .restart local v10    # "showTags":Z
    .restart local v11    # "skippedMillis":I
    :cond_1
    const/16 v14, 0x780

    goto :goto_1

    .line 217
    .restart local v14    # "width":I
    :cond_2
    const/16 v7, 0x320

    goto :goto_2

    .line 226
    .restart local v7    # "height":I
    .restart local v8    # "i":I
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taggedKnowledgeEntities:Ljava/util/List;

    invoke-static {v0}, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;->sortByTypeAndHorizontalPosition(Ljava/util/List;)V

    .line 228
    .end local v6    # "activeTags":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/Tag;>;"
    .end local v7    # "height":I
    .end local v8    # "i":I
    .end local v11    # "skippedMillis":I
    .end local v14    # "width":I
    :cond_4
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taggingKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    if-eqz v0, :cond_5

    .line 229
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taggingKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    iget v1, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->showKnowledgeAtMillis:I

    iget v2, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->videoDisplayWidth:I

    iget v3, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->videoDisplayHeight:I

    iget-object v4, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taggedKnowledgeEntities:Ljava/util/List;

    iget v5, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->showRecentActorsWithinMillis:I

    invoke-interface/range {v0 .. v5}, Lcom/google/android/videos/tagging/KnowledgeView;->showPausedKnowledge(IIILjava/util/List;I)V

    .line 233
    :cond_5
    return-void
.end method

.method private stopKnowledge()V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taggingKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taggingKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    invoke-interface {v0}, Lcom/google/android/videos/tagging/KnowledgeView;->hideKnowledge()V

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taglessKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    if-eqz v0, :cond_1

    .line 119
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taglessKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    invoke-interface {v0}, Lcom/google/android/videos/tagging/KnowledgeView;->hideKnowledge()V

    .line 121
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taggedKnowledgeEntities:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 122
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->showKnowledgeAtMillis:I

    .line 123
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->knowledgeState:I

    .line 124
    return-void
.end method


# virtual methods
.method public init(FLcom/google/android/videos/tagging/KnowledgeView;Lcom/google/android/videos/tagging/KnowledgeView;Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;)V
    .locals 0
    .param p1, "minTagArea"    # F
    .param p2, "taggingKnowledgeView"    # Lcom/google/android/videos/tagging/KnowledgeView;
    .param p3, "taglessKnowledgeView"    # Lcom/google/android/videos/tagging/KnowledgeView;
    .param p4, "playerTimeSupplier"    # Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->reset()V

    .line 60
    iput p1, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->minTagArea:F

    .line 61
    iput-object p2, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taggingKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    .line 62
    iput-object p3, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taglessKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    .line 63
    iput-object p4, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->playerTimeSupplier:Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;

    .line 64
    return-void
.end method

.method public onOrientationChanged(Lcom/google/android/videos/tagging/KnowledgeView;)V
    .locals 1
    .param p1, "localKnowledgeView"    # Lcom/google/android/videos/tagging/KnowledgeView;

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/videos/tagging/KnowledgeView;->hasContent()Z

    move-result v0

    if-nez v0, :cond_1

    .line 246
    :cond_0
    :goto_0
    return-void

    .line 240
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taggingKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    if-ne p1, v0, :cond_0

    .line 244
    invoke-interface {p1}, Lcom/google/android/videos/tagging/KnowledgeView;->hideKnowledge()V

    goto :goto_0
.end method

.method public onPlayerPause(IIII)V
    .locals 6
    .param p1, "timeMillis"    # I
    .param p2, "showRecentActorsWithinMillis"    # I
    .param p3, "videoDisplayWidth"    # I
    .param p4, "videoDisplayHeight"    # I

    .prologue
    const/4 v2, 0x0

    .line 132
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taggingKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taglessKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    if-nez v0, :cond_1

    .line 152
    :cond_0
    :goto_0
    return-void

    .line 136
    :cond_1
    iput p1, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->showKnowledgeAtMillis:I

    .line 137
    iput p2, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->showRecentActorsWithinMillis:I

    .line 138
    iput p3, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->videoDisplayWidth:I

    .line 139
    iput p4, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->videoDisplayHeight:I

    .line 140
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taggingKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    if-eqz v0, :cond_2

    .line 141
    invoke-direct {p0, p1}, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->loadChunkData(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 142
    invoke-direct {p0}, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->showTaggedKnowledge()V

    .line 147
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taglessKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    if-eqz v0, :cond_3

    .line 148
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taglessKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    iget v1, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->showKnowledgeAtMillis:I

    iget-object v4, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taggedKnowledgeEntities:Ljava/util/List;

    move v3, v2

    move v5, p2

    invoke-interface/range {v0 .. v5}, Lcom/google/android/videos/tagging/KnowledgeView;->showPausedKnowledge(IIILjava/util/List;I)V

    .line 151
    :cond_3
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->knowledgeState:I

    goto :goto_0
.end method

.method public onPlayerPlay()V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taggingKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taggingKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    invoke-interface {v0}, Lcom/google/android/videos/tagging/KnowledgeView;->showPlayingKnowledge()V

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taglessKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    if-eqz v0, :cond_1

    .line 99
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taglessKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    invoke-interface {v0}, Lcom/google/android/videos/tagging/KnowledgeView;->showPlayingKnowledge()V

    .line 101
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taggedKnowledgeEntities:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 102
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->showKnowledgeAtMillis:I

    .line 103
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->knowledgeState:I

    .line 104
    return-void
.end method

.method public onPlayerStartScrubbing()V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->stopKnowledge()V

    .line 108
    return-void
.end method

.method public onPlayerStop()V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->stopKnowledge()V

    .line 112
    return-void
.end method

.method public reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 67
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taggingKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taggingKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    invoke-interface {v0}, Lcom/google/android/videos/tagging/KnowledgeView;->reset()V

    .line 70
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taglessKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    if-eqz v0, :cond_1

    .line 71
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taglessKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    invoke-interface {v0}, Lcom/google/android/videos/tagging/KnowledgeView;->reset()V

    .line 73
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->knowledgeState:I

    .line 74
    iput-object v1, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taggingKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    .line 75
    iput-object v1, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taglessKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    .line 76
    iput-object v1, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

    .line 77
    iput-object v1, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->currentChunkInfo:Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

    .line 78
    iput-object v1, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->currentChunkData:Lcom/google/android/videos/tagging/ChunkData;

    .line 79
    iput-object v1, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->loadingChunkInfo:Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

    .line 80
    return-void
.end method

.method public setKnowledgeBundle(Lcom/google/android/videos/tagging/KnowledgeBundle;)V
    .locals 3
    .param p1, "knowledgeBundle"    # Lcom/google/android/videos/tagging/KnowledgeBundle;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

    .line 84
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taggingKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taggingKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    iget-object v1, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->playerTimeSupplier:Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;

    const/4 v2, 0x1

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/videos/tagging/KnowledgeView;->initKnowledge(Lcom/google/android/videos/tagging/KnowledgeBundle;Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;I)V

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taglessKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    if-eqz v0, :cond_1

    .line 89
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taglessKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    iget-object v1, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->playerTimeSupplier:Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;

    const/4 v2, 0x2

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/videos/tagging/KnowledgeView;->initKnowledge(Lcom/google/android/videos/tagging/KnowledgeBundle;Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;I)V

    .line 92
    :cond_1
    return-void
.end method
