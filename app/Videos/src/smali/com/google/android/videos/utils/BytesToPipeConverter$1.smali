.class Lcom/google/android/videos/utils/BytesToPipeConverter$1;
.super Ljava/lang/Object;
.source "BytesToPipeConverter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/utils/BytesToPipeConverter;->writeDataInBackground(Ljava/io/OutputStream;Lcom/google/android/videos/utils/ByteArray;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/utils/BytesToPipeConverter;

.field final synthetic val$byteArray:Lcom/google/android/videos/utils/ByteArray;

.field final synthetic val$out:Ljava/io/OutputStream;


# direct methods
.method constructor <init>(Lcom/google/android/videos/utils/BytesToPipeConverter;Ljava/io/OutputStream;Lcom/google/android/videos/utils/ByteArray;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/google/android/videos/utils/BytesToPipeConverter$1;->this$0:Lcom/google/android/videos/utils/BytesToPipeConverter;

    iput-object p2, p0, Lcom/google/android/videos/utils/BytesToPipeConverter$1;->val$out:Ljava/io/OutputStream;

    iput-object p3, p0, Lcom/google/android/videos/utils/BytesToPipeConverter$1;->val$byteArray:Lcom/google/android/videos/utils/ByteArray;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 47
    :try_start_0
    iget-object v1, p0, Lcom/google/android/videos/utils/BytesToPipeConverter$1;->val$out:Ljava/io/OutputStream;

    iget-object v2, p0, Lcom/google/android/videos/utils/BytesToPipeConverter$1;->val$byteArray:Lcom/google/android/videos/utils/ByteArray;

    iget-object v2, v2, Lcom/google/android/videos/utils/ByteArray;->data:[B

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/videos/utils/BytesToPipeConverter$1;->val$byteArray:Lcom/google/android/videos/utils/ByteArray;

    invoke-virtual {v4}, Lcom/google/android/videos/utils/ByteArray;->limit()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52
    :try_start_1
    iget-object v1, p0, Lcom/google/android/videos/utils/BytesToPipeConverter$1;->val$out:Ljava/io/OutputStream;

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 57
    :goto_0
    return-void

    .line 53
    :catch_0
    move-exception v0

    .line 54
    .local v0, "ioe":Ljava/io/IOException;
    const-string v1, "Exception closing pipe"

    invoke-static {v1, v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 48
    .end local v0    # "ioe":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 49
    .restart local v0    # "ioe":Ljava/io/IOException;
    :try_start_2
    const-string v1, "Exception writing to pipe"

    invoke-static {v1, v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 52
    :try_start_3
    iget-object v1, p0, Lcom/google/android/videos/utils/BytesToPipeConverter$1;->val$out:Ljava/io/OutputStream;

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 53
    :catch_2
    move-exception v0

    .line 54
    const-string v1, "Exception closing pipe"

    invoke-static {v1, v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 51
    .end local v0    # "ioe":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    .line 52
    :try_start_4
    iget-object v2, p0, Lcom/google/android/videos/utils/BytesToPipeConverter$1;->val$out:Ljava/io/OutputStream;

    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 55
    :goto_1
    throw v1

    .line 53
    :catch_3
    move-exception v0

    .line 54
    .restart local v0    # "ioe":Ljava/io/IOException;
    const-string v2, "Exception closing pipe"

    invoke-static {v2, v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
