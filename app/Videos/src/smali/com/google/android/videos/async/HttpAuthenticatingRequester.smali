.class public final Lcom/google/android/videos/async/HttpAuthenticatingRequester;
.super Lcom/google/android/videos/async/AuthenticatingRequester;
.source "HttpAuthenticatingRequester.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Lcom/google/android/videos/async/Request;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/videos/async/AuthenticatingRequester",
        "<TR;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final requestConverter:Lcom/google/android/videos/converter/RequestConverter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/converter/RequestConverter",
            "<TR;",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;"
        }
    .end annotation
.end field

.field private final targetRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "TE;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/converter/RequestConverter;Lcom/google/android/videos/async/Requester;)V
    .locals 0
    .param p1, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/accounts/AccountManagerWrapper;",
            "Lcom/google/android/videos/converter/RequestConverter",
            "<TR;",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "TE;>;)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p0, "this":Lcom/google/android/videos/async/HttpAuthenticatingRequester;, "Lcom/google/android/videos/async/HttpAuthenticatingRequester<TR;TE;>;"
    .local p2, "requestConverter":Lcom/google/android/videos/converter/RequestConverter;, "Lcom/google/android/videos/converter/RequestConverter<TR;Lorg/apache/http/client/methods/HttpUriRequest;>;"
    .local p3, "targetRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lorg/apache/http/client/methods/HttpUriRequest;TE;>;"
    invoke-direct {p0, p1}, Lcom/google/android/videos/async/AuthenticatingRequester;-><init>(Lcom/google/android/videos/accounts/AccountManagerWrapper;)V

    .line 31
    iput-object p2, p0, Lcom/google/android/videos/async/HttpAuthenticatingRequester;->requestConverter:Lcom/google/android/videos/converter/RequestConverter;

    .line 32
    iput-object p3, p0, Lcom/google/android/videos/async/HttpAuthenticatingRequester;->targetRequester:Lcom/google/android/videos/async/Requester;

    .line 33
    return-void
.end method

.method public static create(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/converter/RequestConverter;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/HttpAuthenticatingRequester;
    .locals 1
    .param p0, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Lcom/google/android/videos/async/Request;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/videos/accounts/AccountManagerWrapper;",
            "Lcom/google/android/videos/converter/RequestConverter",
            "<TR;",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "TE;>;)",
            "Lcom/google/android/videos/async/HttpAuthenticatingRequester",
            "<TR;TE;>;"
        }
    .end annotation

    .prologue
    .line 22
    .local p1, "requestConverter":Lcom/google/android/videos/converter/RequestConverter;, "Lcom/google/android/videos/converter/RequestConverter<TR;Lorg/apache/http/client/methods/HttpUriRequest;>;"
    .local p2, "target":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lorg/apache/http/client/methods/HttpUriRequest;TE;>;"
    new-instance v0, Lcom/google/android/videos/async/HttpAuthenticatingRequester;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/videos/async/HttpAuthenticatingRequester;-><init>(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/converter/RequestConverter;Lcom/google/android/videos/async/Requester;)V

    return-object v0
.end method


# virtual methods
.method protected makeAuthenticatedRequest(Lcom/google/android/videos/async/Request;Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V
    .locals 4
    .param p2, "authToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "TE;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 38
    .local p0, "this":Lcom/google/android/videos/async/HttpAuthenticatingRequester;, "Lcom/google/android/videos/async/HttpAuthenticatingRequester<TR;TE;>;"
    .local p1, "request":Lcom/google/android/videos/async/Request;, "TR;"
    .local p3, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lorg/apache/http/client/methods/HttpUriRequest;TE;>;"
    iget-object v1, p0, Lcom/google/android/videos/async/HttpAuthenticatingRequester;->requestConverter:Lcom/google/android/videos/converter/RequestConverter;

    invoke-interface {v1, p1}, Lcom/google/android/videos/converter/RequestConverter;->convertRequest(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/methods/HttpUriRequest;

    .line 39
    .local v0, "httpUriRequest":Lorg/apache/http/client/methods/HttpUriRequest;
    if-eqz p2, :cond_0

    .line 40
    const-string v1, "Authorization"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Bearer "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/async/HttpAuthenticatingRequester;->targetRequester:Lcom/google/android/videos/async/Requester;

    invoke-interface {v1, v0, p3}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 43
    return-void
.end method
