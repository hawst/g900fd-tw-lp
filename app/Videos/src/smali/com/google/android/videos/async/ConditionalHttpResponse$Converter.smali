.class final Lcom/google/android/videos/async/ConditionalHttpResponse$Converter;
.super Lcom/google/android/videos/converter/HttpResponseConverter;
.source "ConditionalHttpResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/async/ConditionalHttpResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Converter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/videos/converter/HttpResponseConverter",
        "<",
        "Lcom/google/android/videos/async/ConditionalHttpResponse",
        "<TE;>;>;"
    }
.end annotation


# instance fields
.field private final targetConverter:Lcom/google/android/videos/converter/HttpResponseConverter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/converter/HttpResponseConverter",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/videos/converter/HttpResponseConverter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/converter/HttpResponseConverter",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 66
    .local p0, "this":Lcom/google/android/videos/async/ConditionalHttpResponse$Converter;, "Lcom/google/android/videos/async/ConditionalHttpResponse$Converter<TE;>;"
    .local p1, "targetConverter":Lcom/google/android/videos/converter/HttpResponseConverter;, "Lcom/google/android/videos/converter/HttpResponseConverter<TE;>;"
    invoke-direct {p0}, Lcom/google/android/videos/converter/HttpResponseConverter;-><init>()V

    .line 67
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/converter/HttpResponseConverter;

    iput-object v0, p0, Lcom/google/android/videos/async/ConditionalHttpResponse$Converter;->targetConverter:Lcom/google/android/videos/converter/HttpResponseConverter;

    .line 68
    return-void
.end method


# virtual methods
.method public convertResponse(Lorg/apache/http/HttpResponse;)Lcom/google/android/videos/async/ConditionalHttpResponse;
    .locals 7
    .param p1, "response"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/HttpResponse;",
            ")",
            "Lcom/google/android/videos/async/ConditionalHttpResponse",
            "<TE;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/videos/async/ConditionalHttpResponse$Converter;, "Lcom/google/android/videos/async/ConditionalHttpResponse$Converter<TE;>;"
    const/4 v4, 0x0

    .line 74
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v5

    const/16 v6, 0x130

    if-ne v5, v6, :cond_0

    .line 75
    # getter for: Lcom/google/android/videos/async/ConditionalHttpResponse;->NOT_MODIFIED:Lcom/google/android/videos/async/ConditionalHttpResponse;
    invoke-static {}, Lcom/google/android/videos/async/ConditionalHttpResponse;->access$000()Lcom/google/android/videos/async/ConditionalHttpResponse;

    move-result-object v4

    .line 82
    :goto_0
    return-object v4

    .line 77
    :cond_0
    iget-object v5, p0, Lcom/google/android/videos/async/ConditionalHttpResponse$Converter;->targetConverter:Lcom/google/android/videos/converter/HttpResponseConverter;

    invoke-virtual {v5, p1}, Lcom/google/android/videos/converter/HttpResponseConverter;->convertResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;

    move-result-object v3

    .line 78
    .local v3, "targetResponse":Ljava/lang/Object;, "TE;"
    const-string v5, "ETag"

    invoke-interface {p1, v5}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v1

    .line 79
    .local v1, "header":Lorg/apache/http/Header;
    if-nez v1, :cond_1

    move-object v0, v4

    .line 80
    .local v0, "eTag":Ljava/lang/String;
    :goto_1
    const-string v5, "Last-Modified"

    invoke-interface {p1, v5}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v1

    .line 81
    if-nez v1, :cond_2

    move-object v2, v4

    .line 82
    .local v2, "lastModified":Ljava/lang/String;
    :goto_2
    invoke-static {v3, v0, v2}, Lcom/google/android/videos/async/ConditionalHttpResponse;->create(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/async/ConditionalHttpResponse;

    move-result-object v4

    goto :goto_0

    .line 79
    .end local v0    # "eTag":Ljava/lang/String;
    .end local v2    # "lastModified":Ljava/lang/String;
    :cond_1
    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 81
    .restart local v0    # "eTag":Ljava/lang/String;
    :cond_2
    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v2

    goto :goto_2
.end method

.method public bridge synthetic convertResponse(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    .local p0, "this":Lcom/google/android/videos/async/ConditionalHttpResponse$Converter;, "Lcom/google/android/videos/async/ConditionalHttpResponse$Converter<TE;>;"
    check-cast p1, Lorg/apache/http/HttpResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/async/ConditionalHttpResponse$Converter;->convertResponse(Lorg/apache/http/HttpResponse;)Lcom/google/android/videos/async/ConditionalHttpResponse;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic convertResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    .local p0, "this":Lcom/google/android/videos/async/ConditionalHttpResponse$Converter;, "Lcom/google/android/videos/async/ConditionalHttpResponse$Converter<TE;>;"
    invoke-virtual {p0, p1}, Lcom/google/android/videos/async/ConditionalHttpResponse$Converter;->convertResponse(Lorg/apache/http/HttpResponse;)Lcom/google/android/videos/async/ConditionalHttpResponse;

    move-result-object v0

    return-object v0
.end method
