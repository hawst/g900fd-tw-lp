.class public Lcom/google/android/videos/activity/HomeActivity;
.super Lcom/google/android/videos/activity/DrawerActivity;
.source "HomeActivity.java"

# interfaces
.implements Lcom/google/android/videos/ui/StatusHelper$OnRetryListener;
.implements Lcom/google/android/videos/ui/SyncHelper$Listener;
.implements Lcom/google/android/videos/ui/VerticalsHelper$OnContentChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;
    }
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private activeVertical:I

.field private activityRunning:Z

.field private errorMessage:Ljava/lang/CharSequence;

.field private eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private forceCloseDrawer:Z

.field private fragmentPageAdapter:Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;

.field private handler:Landroid/os/Handler;

.field private isDrawerInitializing:Z

.field private isTransitioning:Z

.field private mediaRouteProvider:Lcom/google/android/videos/remote/MediaRouteProvider;

.field private objectsPreparingForTransition:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private pendingVertical:I

.field private preferences:Landroid/content/SharedPreferences;

.field private previousVertical:I

.field private remoteHelper:Lcom/google/android/videos/remote/RemoteHelper;

.field private signInManager:Lcom/google/android/videos/accounts/SignInManager;

.field private startedFromLauncher:Z

.field private statusHelper:Lcom/google/android/videos/ui/StatusHelper;

.field private storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

.field private suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

.field private syncHelper:Lcom/google/android/videos/ui/SyncHelper;

.field private verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;

.field private videosGlobals:Lcom/google/android/videos/VideosGlobals;

.field private wishlistSectionToSelect:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/android/videos/activity/DrawerActivity;-><init>()V

    .line 119
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/activity/HomeActivity;->wishlistSectionToSelect:I

    .line 609
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/activity/HomeActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/activity/HomeActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/google/android/videos/activity/HomeActivity;->setIsTransitioningV21(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/videos/activity/HomeActivity;)Lcom/google/android/videos/remote/MediaRouteProvider;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/activity/HomeActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->mediaRouteProvider:Lcom/google/android/videos/remote/MediaRouteProvider;

    return-object v0
.end method

.method private addSearchExpandListener(Landroid/view/MenuItem;)V
    .locals 1
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 472
    new-instance v0, Lcom/google/android/videos/activity/HomeActivity$4;

    invoke-direct {v0, p0}, Lcom/google/android/videos/activity/HomeActivity$4;-><init>(Lcom/google/android/videos/activity/HomeActivity;)V

    invoke-static {p1, v0}, Landroid/support/v4/view/MenuItemCompat;->setOnActionExpandListener(Landroid/view/MenuItem;Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;)Landroid/view/MenuItem;

    .line 484
    return-void
.end method

.method public static createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "vertical"    # Ljava/lang/String;

    .prologue
    .line 87
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/videos/activity/HomeActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "vertical"    # Ljava/lang/String;
    .param p3, "forceCloseDrawer"    # Z

    .prologue
    .line 72
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/videos/activity/HomeActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v2, 0x14000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "android.intent.action.MAIN"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "android.intent.category.LAUNCHER"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 76
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 77
    const-string v1, "authAccount"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 79
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 80
    const-string v1, "selected_vertical"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 82
    :cond_1
    const-string v1, "force_close_drawer"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 83
    return-object v0
.end method

.method private doResume()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 283
    invoke-virtual {p0}, Lcom/google/android/videos/activity/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 284
    .local v1, "intent":Landroid/content/Intent;
    iget v3, p0, Lcom/google/android/videos/activity/HomeActivity;->activeVertical:I

    .line 285
    .local v3, "requestedVertical":I
    const-string v5, "selected_vertical"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 286
    const-string v5, "selected_vertical"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 287
    .local v4, "verticalName":Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/videos/ui/VerticalsHelper;->parseExternalVerticalName(Ljava/lang/String;)I

    move-result v3

    .line 290
    .end local v4    # "verticalName":Ljava/lang/String;
    :cond_0
    const-string v5, "force_close_drawer"

    invoke-virtual {v1, v5, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/videos/activity/HomeActivity;->forceCloseDrawer:Z

    .line 292
    iget-object v5, p0, Lcom/google/android/videos/activity/HomeActivity;->errorMessage:Ljava/lang/CharSequence;

    if-nez v5, :cond_3

    .line 293
    const-string v5, "authAccount"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 294
    .local v2, "intentAccount":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/videos/activity/HomeActivity;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v5}, Lcom/google/android/videos/ui/SyncHelper;->getState()I

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/videos/activity/HomeActivity;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v5}, Lcom/google/android/videos/ui/SyncHelper;->getState()I

    move-result v5

    const/16 v6, 0x8

    if-eq v5, v6, :cond_2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/google/android/videos/activity/HomeActivity;->account:Ljava/lang/String;

    invoke-static {v5, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_1
    iget-object v5, p0, Lcom/google/android/videos/activity/HomeActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v5}, Lcom/google/android/videos/VideosGlobals;->refreshContentRestrictions()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 298
    :cond_2
    iput v3, p0, Lcom/google/android/videos/activity/HomeActivity;->pendingVertical:I

    .line 299
    invoke-direct {p0, v2}, Lcom/google/android/videos/activity/HomeActivity;->init(Ljava/lang/String;)V

    .line 310
    .end local v2    # "intentAccount":Ljava/lang/String;
    :cond_3
    :goto_0
    iget-boolean v5, p0, Lcom/google/android/videos/activity/HomeActivity;->startedFromLauncher:Z

    const-string v6, "started_from_launcher"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    or-int/2addr v5, v6

    iput-boolean v5, p0, Lcom/google/android/videos/activity/HomeActivity;->startedFromLauncher:Z

    .line 313
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 314
    .local v0, "clearedIntent":Landroid/content/Intent;
    const-string v5, "authAccount"

    invoke-virtual {v0, v5}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 315
    const-string v5, "selected_vertical"

    invoke-virtual {v0, v5}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 316
    const-string v5, "force_close_drawer"

    invoke-virtual {v0, v5}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 317
    const-string v5, "started_from_launcher"

    invoke-virtual {v0, v5}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 318
    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/HomeActivity;->setIntent(Landroid/content/Intent;)V

    .line 319
    return-void

    .line 303
    .end local v0    # "clearedIntent":Landroid/content/Intent;
    .restart local v2    # "intentAccount":Ljava/lang/String;
    :cond_4
    iget v5, p0, Lcom/google/android/videos/activity/HomeActivity;->activeVertical:I

    if-eq v5, v3, :cond_3

    if-eqz v3, :cond_3

    .line 305
    iget-object v5, p0, Lcom/google/android/videos/activity/HomeActivity;->verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;

    iget-object v6, p0, Lcom/google/android/videos/activity/HomeActivity;->account:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/google/android/videos/ui/VerticalsHelper;->enabledVerticalsForUser(Ljava/lang/String;)I

    move-result v5

    invoke-direct {p0, v5, v3}, Lcom/google/android/videos/activity/HomeActivity;->initVerticals(II)V

    goto :goto_0
.end method

.method private init(Ljava/lang/String;)V
    .locals 2
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->fragmentPageAdapter:Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->setVerticals(I)V

    .line 335
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/ui/SyncHelper;->init(Ljava/lang/String;)V

    .line 336
    invoke-virtual {p0}, Lcom/google/android/videos/activity/HomeActivity;->supportInvalidateOptionsMenu()V

    .line 337
    return-void
.end method

.method private initVerticals(II)V
    .locals 3
    .param p1, "verticals"    # I
    .param p2, "requestedVertical"    # I

    .prologue
    .line 340
    if-nez p1, :cond_0

    .line 341
    const v0, 0x7f0b00fd

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->errorMessage:Ljava/lang/CharSequence;

    .line 342
    invoke-direct {p0}, Lcom/google/android/videos/activity/HomeActivity;->showErrorMessage()V

    .line 357
    :goto_0
    return-void

    .line 346
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->errorMessage:Ljava/lang/CharSequence;

    .line 347
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->statusHelper:Lcom/google/android/videos/ui/StatusHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/StatusHelper;->hide()V

    .line 349
    and-int v0, p1, p2

    if-nez v0, :cond_1

    .line 350
    const/4 p2, 0x4

    .line 352
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->fragmentPageAdapter:Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->setVerticals(I)V

    .line 354
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/activity/HomeActivity;->isDrawerInitializing:Z

    .line 355
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->videosDrawerHelper:Lcom/google/android/videos/ui/VideosDrawerHelper;

    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity;->account:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/android/videos/activity/HomeActivity;->forceCloseDrawer:Z

    invoke-virtual {v0, v1, p1, p2, v2}, Lcom/google/android/videos/ui/VideosDrawerHelper;->init(Ljava/lang/String;IIZ)Z

    .line 356
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/activity/HomeActivity;->isDrawerInitializing:Z

    goto :goto_0
.end method

.method private maybeAddTransitionEndListenerV21()V
    .locals 2

    .prologue
    .line 184
    invoke-virtual {p0}, Lcom/google/android/videos/activity/HomeActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getSharedElementReenterTransition()Landroid/transition/Transition;

    move-result-object v0

    .line 185
    .local v0, "transition":Landroid/transition/Transition;
    if-eqz v0, :cond_0

    .line 186
    new-instance v1, Lcom/google/android/videos/activity/HomeActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/videos/activity/HomeActivity$1;-><init>(Lcom/google/android/videos/activity/HomeActivity;)V

    invoke-virtual {v0, v1}, Landroid/transition/Transition;->addListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/Transition;

    .line 193
    :cond_0
    return-void
.end method

.method private onAccount(Ljava/lang/String;)V
    .locals 2
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 385
    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity;->account:Ljava/lang/String;

    invoke-static {v1, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 397
    :cond_0
    :goto_0
    return-void

    .line 388
    :cond_1
    iput-object p1, p0, Lcom/google/android/videos/activity/HomeActivity;->account:Ljava/lang/String;

    .line 389
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 390
    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    invoke-virtual {v1, p1}, Lcom/google/android/videos/store/StoreStatusMonitor;->init(Ljava/lang/String;)V

    .line 391
    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity;->verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;

    invoke-virtual {v1, p1}, Lcom/google/android/videos/ui/VerticalsHelper;->enabledVerticalsForUser(Ljava/lang/String;)I

    move-result v0

    .line 392
    .local v0, "enabledVerticals":I
    iget v1, p0, Lcom/google/android/videos/activity/HomeActivity;->pendingVertical:I

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/google/android/videos/activity/HomeActivity;->pendingVertical:I

    :goto_1
    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/activity/HomeActivity;->initVerticals(II)V

    .line 394
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/videos/activity/HomeActivity;->pendingVertical:I

    .line 395
    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity;->suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    invoke-virtual {v1, p1}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->init(Ljava/lang/String;)V

    goto :goto_0

    .line 392
    :cond_2
    iget v1, p0, Lcom/google/android/videos/activity/HomeActivity;->activeVertical:I

    goto :goto_1
.end method

.method private setIsTransitioningV21(Z)V
    .locals 2
    .param p1, "isTransitioning"    # Z

    .prologue
    .line 326
    iput-boolean p1, p0, Lcom/google/android/videos/activity/HomeActivity;->isTransitioning:Z

    .line 327
    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity;->fragmentPageAdapter:Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;

    invoke-virtual {v1}, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->getCurrentHomeFragment()Lcom/google/android/videos/activity/HomeFragment;

    move-result-object v0

    .line 328
    .local v0, "currentHomeFragment":Lcom/google/android/videos/activity/HomeFragment;
    if-eqz v0, :cond_0

    .line 329
    invoke-virtual {v0, p1}, Lcom/google/android/videos/activity/HomeFragment;->onTransitioningChanged(Z)V

    .line 331
    :cond_0
    return-void
.end method

.method private showErrorMessage()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 270
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->statusHelper:Lcom/google/android/videos/ui/StatusHelper;

    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity;->errorMessage:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/ui/StatusHelper;->setErrorMessage(Ljava/lang/CharSequence;Z)V

    .line 271
    iput v2, p0, Lcom/google/android/videos/activity/HomeActivity;->activeVertical:I

    .line 272
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->videosDrawerHelper:Lcom/google/android/videos/ui/VideosDrawerHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/VideosDrawerHelper;->initEmpty()Z

    .line 273
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->fragmentPageAdapter:Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;

    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity;->videosDrawerHelper:Lcom/google/android/videos/ui/VideosDrawerHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/ui/VideosDrawerHelper;->getPageView()Landroid/view/ViewGroup;

    move-result-object v1

    # invokes: Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->detachCurrentFragment(Landroid/view/ViewGroup;)V
    invoke-static {v0, v1}, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->access$100(Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;Landroid/view/ViewGroup;)V

    .line 274
    return-void
.end method

.method private startPostponedEnterTransitionDelayedV21()V
    .locals 2

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/videos/activity/HomeActivity$2;

    invoke-direct {v1, p0}, Lcom/google/android/videos/activity/HomeActivity$2;-><init>(Lcom/google/android/videos/activity/HomeActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 237
    return-void
.end method


# virtual methods
.method public getCurrentPlayHeaderListLayout()Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1

    .prologue
    .line 606
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->fragmentPageAdapter:Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->getCurrentHomeFragment()Lcom/google/android/videos/activity/HomeFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/activity/HomeFragment;->getPlayHeaderListLayout()Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v0

    return-object v0
.end method

.method public getMediaRouteProvider()Lcom/google/android/videos/remote/MediaRouteProvider;
    .locals 1

    .prologue
    .line 447
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->mediaRouteProvider:Lcom/google/android/videos/remote/MediaRouteProvider;

    return-object v0
.end method

.method public getStoreStatusMonitor()Lcom/google/android/videos/store/StoreStatusMonitor;
    .locals 1

    .prologue
    .line 439
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    return-object v0
.end method

.method public getSuggestionsOverflowMenuHelper()Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;
    .locals 1

    .prologue
    .line 451
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    return-object v0
.end method

.method public getSyncHelper()Lcom/google/android/videos/ui/SyncHelper;
    .locals 1

    .prologue
    .line 443
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    return-object v0
.end method

.method public getVerticalsHelper()Lcom/google/android/videos/ui/VerticalsHelper;
    .locals 1

    .prologue
    .line 591
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;

    return-object v0
.end method

.method public hasDrawer()Z
    .locals 1

    .prologue
    .line 516
    const/4 v0, 0x1

    return v0
.end method

.method public isDrawerOpen()Z
    .locals 1

    .prologue
    .line 520
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->videosDrawerHelper:Lcom/google/android/videos/ui/VideosDrawerHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/VideosDrawerHelper;->isDrawerOpen()Z

    move-result v0

    return v0
.end method

.method public isTransitioning()Z
    .locals 1

    .prologue
    .line 322
    iget-boolean v0, p0, Lcom/google/android/videos/activity/HomeActivity;->isTransitioning:Z

    return v0
.end method

.method public markAsPreparingForTransitionV21(Ljava/lang/Object;)V
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->objectsPreparingForTransition:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 217
    return-void
.end method

.method public markAsReadyForTransitionV21(Ljava/lang/Object;)V
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->objectsPreparingForTransition:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->objectsPreparingForTransition:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    invoke-direct {p0}, Lcom/google/android/videos/activity/HomeActivity;->startPostponedEnterTransitionDelayedV21()V

    .line 227
    :cond_0
    return-void
.end method

.method public onActivityReenter(ILandroid/content/Intent;)V
    .locals 4
    .param p1, "resultCode"    # I
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x1

    .line 243
    iget-boolean v0, p0, Lcom/google/android/videos/activity/HomeActivity;->activityRunning:Z

    if-eqz v0, :cond_0

    .line 247
    invoke-direct {p0, v1}, Lcom/google/android/videos/activity/HomeActivity;->setIsTransitioningV21(Z)V

    .line 267
    :goto_0
    return-void

    .line 250
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/activity/HomeActivity;->postponeEnterTransition()V

    .line 251
    invoke-direct {p0, v1}, Lcom/google/android/videos/activity/HomeActivity;->setIsTransitioningV21(Z)V

    .line 252
    invoke-direct {p0}, Lcom/google/android/videos/activity/HomeActivity;->doResume()V

    .line 261
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/videos/activity/HomeActivity$3;

    invoke-direct {v1, p0}, Lcom/google/android/videos/activity/HomeActivity$3;-><init>(Lcom/google/android/videos/activity/HomeActivity;)V

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 496
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->onActivityResult(IILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 503
    :cond_0
    :goto_0
    return-void

    .line 499
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/videos/ui/SyncHelper;->onActivityResult(IILandroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 502
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/videos/activity/DrawerActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 596
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->videosDrawerHelper:Lcom/google/android/videos/ui/VideosDrawerHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/VideosDrawerHelper;->onBackPressed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 603
    :goto_0
    return-void

    .line 598
    :cond_0
    iget v0, p0, Lcom/google/android/videos/activity/HomeActivity;->previousVertical:I

    if-eqz v0, :cond_1

    .line 599
    iget v0, p0, Lcom/google/android/videos/activity/HomeActivity;->previousVertical:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/videos/activity/HomeActivity;->selectVertical(II)V

    goto :goto_0

    .line 601
    :cond_1
    invoke-super {p0}, Lcom/google/android/videos/activity/DrawerActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 17
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 128
    invoke-super/range {p0 .. p1}, Lcom/google/android/videos/activity/DrawerActivity;->onCreate(Landroid/os/Bundle;)V

    .line 130
    invoke-static/range {p0 .. p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/HomeActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    .line 131
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/HomeActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/HomeActivity;->preferences:Landroid/content/SharedPreferences;

    .line 132
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/HomeActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/HomeActivity;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 133
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/HomeActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStoreSync()Lcom/google/android/videos/store/PurchaseStoreSync;

    move-result-object v7

    .line 134
    .local v7, "purchaseStoreSync":Lcom/google/android/videos/store/PurchaseStoreSync;
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/HomeActivity;->handler:Landroid/os/Handler;

    .line 136
    const v2, 0x7f040041

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/videos/activity/HomeActivity;->setContentView(I)V

    .line 137
    const v2, 0x7f0f011d

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/videos/activity/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Lcom/google/android/play/drawer/PlayDrawerLayout;

    .line 139
    .local v10, "playDrawerLayout":Lcom/google/android/play/drawer/PlayDrawerLayout;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/HomeActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getMediaRouteManager()Lcom/google/android/videos/remote/MediaRouteManager;

    move-result-object v16

    .line 140
    .local v16, "mediaRouteManager":Lcom/google/android/videos/remote/MediaRouteManager;
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/videos/remote/MediaRouteManager;->getRouteSelector()Landroid/support/v7/media/MediaRouteSelector;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/videos/remote/MediaRouteProviderCompat;->create(Landroid/support/v7/app/ActionBarActivity;Landroid/support/v7/media/MediaRouteSelector;)Lcom/google/android/videos/remote/MediaRouteProvider;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/HomeActivity;->mediaRouteProvider:Lcom/google/android/videos/remote/MediaRouteProvider;

    .line 142
    new-instance v2, Lcom/google/android/videos/remote/RemoteHelper;

    move-object/from16 v0, v16

    invoke-direct {v2, v0}, Lcom/google/android/videos/remote/RemoteHelper;-><init>(Lcom/google/android/videos/remote/MediaRouteManager;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/HomeActivity;->remoteHelper:Lcom/google/android/videos/remote/RemoteHelper;

    .line 143
    const v2, 0x7f0f00d8

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/videos/activity/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-static {v0, v2, v1}, Lcom/google/android/videos/ui/StatusHelper;->createFromParent(Landroid/content/Context;Landroid/view/View;Lcom/google/android/videos/ui/StatusHelper$OnRetryListener;)Lcom/google/android/videos/ui/StatusHelper;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/HomeActivity;->statusHelper:Lcom/google/android/videos/ui/StatusHelper;

    .line 144
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/HomeActivity;->statusHelper:Lcom/google/android/videos/ui/StatusHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/StatusHelper;->init()V

    .line 146
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/HomeActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/HomeActivity;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    .line 147
    new-instance v2, Lcom/google/android/videos/ui/SyncHelper;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/activity/HomeActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v3}, Lcom/google/android/videos/VideosGlobals;->getGcmRegistrationManager()Lcom/google/android/videos/gcm/GcmRegistrationManager;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/activity/HomeActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v3}, Lcom/google/android/videos/VideosGlobals;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/activity/HomeActivity;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/activity/HomeActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v3}, Lcom/google/android/videos/VideosGlobals;->getWishlistStoreSync()Lcom/google/android/videos/store/WishlistStoreSync;

    move-result-object v8

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/videos/ui/SyncHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/gcm/GcmRegistrationManager;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/PurchaseStoreSync;Lcom/google/android/videos/store/WishlistStoreSync;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/HomeActivity;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    .line 154
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/HomeActivity;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/videos/ui/SyncHelper;->addListener(Lcom/google/android/videos/ui/SyncHelper$Listener;)V

    .line 156
    new-instance v2, Lcom/google/android/videos/store/StoreStatusMonitor;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/activity/HomeActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v3}, Lcom/google/android/videos/VideosGlobals;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/activity/HomeActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStore()Lcom/google/android/videos/store/PurchaseStore;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/activity/HomeActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v5}, Lcom/google/android/videos/VideosGlobals;->getWishlistStore()Lcom/google/android/videos/store/WishlistStore;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3, v4, v5}, Lcom/google/android/videos/store/StoreStatusMonitor;-><init>(Landroid/content/Context;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/WishlistStore;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/HomeActivity;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    .line 158
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/HomeActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getUiEventLoggingHelper()Lcom/google/android/videos/logging/UiEventLoggingHelper;

    move-result-object v8

    .line 159
    .local v8, "uiEventLoggingHelper":Lcom/google/android/videos/logging/UiEventLoggingHelper;
    new-instance v3, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/activity/HomeActivity;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/activity/HomeActivity;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    move-object/from16 v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/store/StoreStatusMonitor;Lcom/google/android/videos/store/PurchaseStoreSync;Lcom/google/android/videos/logging/UiEventLoggingHelper;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/activity/HomeActivity;->suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    .line 161
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/HomeActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getVerticalsHelper()Lcom/google/android/videos/ui/VerticalsHelper;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/HomeActivity;->verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;

    .line 163
    new-instance v2, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/activity/HomeActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;-><init>(Lcom/google/android/videos/activity/HomeActivity;Landroid/support/v4/app/FragmentManager;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/HomeActivity;->fragmentPageAdapter:Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;

    .line 164
    new-instance v9, Lcom/google/android/videos/ui/VideosDrawerHelper;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/activity/HomeActivity;->preferences:Landroid/content/SharedPreferences;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/HomeActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/activity/HomeActivity;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    const/4 v15, 0x0

    move-object/from16 v11, p0

    invoke-direct/range {v9 .. v15}, Lcom/google/android/videos/ui/VideosDrawerHelper;-><init>(Lcom/google/android/play/drawer/PlayDrawerLayout;Lcom/google/android/videos/activity/DrawerActivity;Landroid/content/SharedPreferences;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/logging/EventLogger;Z)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/android/videos/activity/HomeActivity;->videosDrawerHelper:Lcom/google/android/videos/ui/VideosDrawerHelper;

    .line 166
    if-eqz p1, :cond_0

    .line 167
    const-string v2, "vertical"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/videos/activity/HomeActivity;->activeVertical:I

    .line 168
    const-string v2, "back_to_previous_vertical"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/videos/activity/HomeActivity;->previousVertical:I

    .line 171
    :cond_0
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/videos/activity/HomeActivity;->pendingVertical:I

    .line 173
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/HomeActivity;->objectsPreparingForTransition:Ljava/util/HashSet;

    .line 174
    sget v2, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_1

    .line 175
    invoke-direct/range {p0 .. p0}, Lcom/google/android/videos/activity/HomeActivity;->maybeAddTransitionEndListenerV21()V

    .line 177
    const v2, 0x7f0a00ea

    invoke-virtual {v10, v2}, Lcom/google/android/play/drawer/PlayDrawerLayout;->setStatusBarBackground(I)V

    .line 179
    :cond_1
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 461
    invoke-virtual {p0}, Lcom/google/android/videos/activity/HomeActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 462
    .local v0, "inflater":Landroid/view/MenuInflater;
    invoke-static {p1, v0, p0}, Lcom/google/android/videos/VideosApplication;->createSearchMenu(Landroid/view/Menu;Landroid/view/MenuInflater;Landroid/app/Activity;)Landroid/view/MenuItem;

    move-result-object v1

    .line 463
    .local v1, "search":Landroid/view/MenuItem;
    if-eqz v1, :cond_0

    .line 464
    invoke-direct {p0, v1}, Lcom/google/android/videos/activity/HomeActivity;->addSearchExpandListener(Landroid/view/MenuItem;)V

    .line 466
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/activity/HomeActivity;->mediaRouteProvider:Lcom/google/android/videos/remote/MediaRouteProvider;

    invoke-interface {v2, p1, v0}, Lcom/google/android/videos/remote/MediaRouteProvider;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 467
    iget-object v2, p0, Lcom/google/android/videos/activity/HomeActivity;->videosDrawerHelper:Lcom/google/android/videos/ui/VideosDrawerHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/VideosDrawerHelper;->syncState()V

    .line 468
    invoke-super {p0, p1}, Lcom/google/android/videos/activity/DrawerActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v2

    return v2
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 424
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->handler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 425
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->handler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 427
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->videosDrawerHelper:Lcom/google/android/videos/ui/VideosDrawerHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/VideosDrawerHelper;->finish()V

    .line 428
    invoke-super {p0}, Lcom/google/android/videos/activity/DrawerActivity;->onDestroy()V

    .line 429
    return-void
.end method

.method public onEnabledVerticalsChanged(Ljava/lang/String;I)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "verticals"    # I

    .prologue
    .line 534
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->account:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 535
    iget v0, p0, Lcom/google/android/videos/activity/HomeActivity;->activeVertical:I

    invoke-direct {p0, p2, v0}, Lcom/google/android/videos/activity/HomeActivity;->initVerticals(II)V

    .line 537
    :cond_0
    return-void
.end method

.method public onHasContentChanged(Ljava/lang/String;Z)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "hasContent"    # Z

    .prologue
    .line 530
    return-void
.end method

.method public onHelpSelected()V
    .locals 1

    .prologue
    .line 456
    iget v0, p0, Lcom/google/android/videos/activity/HomeActivity;->activeVertical:I

    invoke-static {v0}, Lcom/google/android/videos/ui/VerticalsHelper;->getHelpContext(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/videos/ui/HelpHelper;->startContextualHelp(Landroid/app/Activity;Ljava/lang/String;)V

    .line 457
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 507
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->remoteHelper:Lcom/google/android/videos/remote/RemoteHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/remote/RemoteHelper;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Lcom/google/android/videos/activity/DrawerActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 512
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->remoteHelper:Lcom/google/android/videos/remote/RemoteHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/remote/RemoteHelper;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Lcom/google/android/videos/activity/DrawerActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onLibraryVerticalSelected(I)V
    .locals 2
    .param p1, "contentVertical"    # I

    .prologue
    .line 582
    const/16 v1, 0x10

    invoke-virtual {p0, v1}, Lcom/google/android/videos/activity/HomeActivity;->onVerticalSelected(I)V

    .line 583
    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity;->fragmentPageAdapter:Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;

    invoke-virtual {v1}, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->getCurrentHomeFragment()Lcom/google/android/videos/activity/HomeFragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/activity/MyLibraryFragment;

    .line 585
    .local v0, "libraryFragment":Lcom/google/android/videos/activity/MyLibraryFragment;
    if-eqz p1, :cond_0

    .line 586
    invoke-virtual {v0, p1}, Lcom/google/android/videos/activity/MyLibraryFragment;->selectVertical(I)V

    .line 588
    :cond_0
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 197
    invoke-super {p0, p1}, Lcom/google/android/videos/activity/DrawerActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 198
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/SyncHelper;->clearPendingResults()V

    .line 199
    invoke-virtual {p0, p1}, Lcom/google/android/videos/activity/HomeActivity;->setIntent(Landroid/content/Intent;)V

    .line 200
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v1, 0x1

    .line 488
    invoke-super {p0, p1}, Lcom/google/android/videos/activity/DrawerActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 489
    iget-object v2, p0, Lcom/google/android/videos/activity/HomeActivity;->errorMessage:Ljava/lang/CharSequence;

    if-nez v2, :cond_0

    move v0, v1

    .line 490
    .local v0, "hasError":Z
    :goto_0
    const v2, 0x7f0f0224

    invoke-static {p1, v2, v0}, Lcom/google/android/videos/utils/Util;->setMenuItemEnabled(Landroid/view/Menu;IZ)V

    .line 491
    return v1

    .line 489
    .end local v0    # "hasError":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 278
    invoke-super {p0}, Lcom/google/android/videos/activity/DrawerActivity;->onResume()V

    .line 279
    invoke-direct {p0}, Lcom/google/android/videos/activity/HomeActivity;->doResume()V

    .line 280
    return-void
.end method

.method public onRetry()V
    .locals 1

    .prologue
    .line 361
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/videos/activity/HomeActivity;->init(Ljava/lang/String;)V

    .line 362
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 433
    invoke-super {p0, p1}, Lcom/google/android/videos/activity/DrawerActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 434
    const-string v0, "vertical"

    iget v1, p0, Lcom/google/android/videos/activity/HomeActivity;->activeVertical:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 435
    const-string v0, "back_to_previous_vertical"

    iget v1, p0, Lcom/google/android/videos/activity/HomeActivity;->previousVertical:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 436
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 204
    invoke-super {p0}, Lcom/google/android/videos/activity/DrawerActivity;->onStart()V

    .line 205
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getPremiumErrorMessage()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->errorMessage:Ljava/lang/CharSequence;

    .line 206
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->errorMessage:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    .line 207
    invoke-direct {p0}, Lcom/google/android/videos/activity/HomeActivity;->showErrorMessage()V

    .line 212
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/activity/HomeActivity;->activityRunning:Z

    .line 213
    return-void

    .line 209
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/VerticalsHelper;->addOnContentChangedListener(Lcom/google/android/videos/ui/VerticalsHelper$OnContentChangedListener;)V

    .line 210
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->mediaRouteProvider:Lcom/google/android/videos/remote/MediaRouteProvider;

    invoke-interface {v0}, Lcom/google/android/videos/remote/MediaRouteProvider;->onStart()V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 401
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/videos/activity/HomeActivity;->activityRunning:Z

    .line 402
    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity;->objectsPreparingForTransition:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    .line 403
    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity;->fragmentPageAdapter:Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;

    invoke-virtual {v1}, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->getCurrentHomeFragment()Lcom/google/android/videos/activity/HomeFragment;

    move-result-object v0

    .line 404
    .local v0, "currentHomeFragment":Lcom/google/android/videos/activity/HomeFragment;
    if-eqz v0, :cond_0

    .line 405
    invoke-virtual {v0}, Lcom/google/android/videos/activity/HomeFragment;->ensureHelperStopped()V

    .line 407
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getPremiumErrorMessage()Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_1

    .line 408
    invoke-super {p0}, Lcom/google/android/videos/activity/DrawerActivity;->resetDrawer()V

    .line 409
    invoke-virtual {p0}, Lcom/google/android/videos/activity/HomeActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 410
    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/ui/SyncHelper;->reset()V

    .line 414
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    invoke-virtual {v1}, Lcom/google/android/videos/store/StoreStatusMonitor;->reset()V

    .line 415
    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity;->suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->reset()V

    .line 416
    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity;->verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;

    invoke-virtual {v1, p0}, Lcom/google/android/videos/ui/VerticalsHelper;->removeOnContentChangedListener(Lcom/google/android/videos/ui/VerticalsHelper$OnContentChangedListener;)V

    .line 417
    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity;->mediaRouteProvider:Lcom/google/android/videos/remote/MediaRouteProvider;

    invoke-interface {v1}, Lcom/google/android/videos/remote/MediaRouteProvider;->onStop()V

    .line 419
    :cond_1
    invoke-super {p0}, Lcom/google/android/videos/activity/DrawerActivity;->onStop()V

    .line 420
    return-void

    .line 412
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/ui/SyncHelper;->markAsStale()V

    goto :goto_0
.end method

.method public onSyncStateChanged(I)V
    .locals 3
    .param p1, "syncState"    # I

    .prologue
    .line 366
    const/16 v1, 0x8

    if-ne p1, v1, :cond_1

    const/4 v0, 0x0

    .line 367
    .local v0, "account":Ljava/lang/String;
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Sync state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/videos/utils/Hashing;->sha1(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 369
    const/4 v1, 0x5

    if-ne p1, v1, :cond_2

    .line 370
    invoke-virtual {p0}, Lcom/google/android/videos/activity/HomeActivity;->finish()V

    .line 382
    :cond_0
    :goto_1
    return-void

    .line 366
    .end local v0    # "account":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 374
    .restart local v0    # "account":Ljava/lang/String;
    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/videos/activity/HomeActivity;->onAccount(Ljava/lang/String;)V

    .line 376
    iget-boolean v1, p0, Lcom/google/android/videos/activity/HomeActivity;->startedFromLauncher:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    if-ne p1, v1, :cond_0

    .line 377
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/videos/activity/HomeActivity;->startedFromLauncher:Z

    .line 378
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 379
    invoke-static {p0, v0}, Lcom/google/android/videos/activity/DmaReconsentFragment;->addInstance(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onVerticalSelected(I)V
    .locals 3
    .param p1, "selectedVertical"    # I

    .prologue
    .line 556
    iget v1, p0, Lcom/google/android/videos/activity/HomeActivity;->activeVertical:I

    if-ne p1, v1, :cond_1

    .line 574
    :cond_0
    :goto_0
    return-void

    .line 560
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity;->fragmentPageAdapter:Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;

    invoke-virtual {v1, p1}, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->getVerticalPosition(I)I

    move-result v0

    .line 563
    .local v0, "verticalPosition":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 567
    iput p1, p0, Lcom/google/android/videos/activity/HomeActivity;->activeVertical:I

    .line 568
    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    invoke-interface {v1, p1}, Lcom/google/android/videos/logging/EventLogger;->onVerticalOpened(I)V

    .line 570
    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity;->fragmentPageAdapter:Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;

    iget-object v2, p0, Lcom/google/android/videos/activity/HomeActivity;->videosDrawerHelper:Lcom/google/android/videos/ui/VideosDrawerHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/VideosDrawerHelper;->getPageView()Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->changeItem(Landroid/view/ViewGroup;I)V

    .line 571
    iget-boolean v1, p0, Lcom/google/android/videos/activity/HomeActivity;->isDrawerInitializing:Z

    if-nez v1, :cond_0

    .line 572
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/videos/activity/HomeActivity;->previousVertical:I

    goto :goto_0
.end method

.method public selectVertical(II)V
    .locals 1
    .param p1, "verticalToSelect"    # I
    .param p2, "previousVertical"    # I

    .prologue
    .line 550
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->videosDrawerHelper:Lcom/google/android/videos/ui/VideosDrawerHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/ui/VideosDrawerHelper;->selectVertical(I)Z

    .line 552
    iput p2, p0, Lcom/google/android/videos/activity/HomeActivity;->previousVertical:I

    .line 553
    return-void
.end method

.method public takeWishlistSectionToSelect()I
    .locals 2

    .prologue
    .line 544
    iget v0, p0, Lcom/google/android/videos/activity/HomeActivity;->wishlistSectionToSelect:I

    .line 545
    .local v0, "result":I
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/videos/activity/HomeActivity;->wishlistSectionToSelect:I

    .line 546
    return v0
.end method

.method public toggleDrawer()V
    .locals 1

    .prologue
    .line 524
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity;->videosDrawerHelper:Lcom/google/android/videos/ui/VideosDrawerHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/VideosDrawerHelper;->toggleDrawer()V

    .line 525
    return-void
.end method
