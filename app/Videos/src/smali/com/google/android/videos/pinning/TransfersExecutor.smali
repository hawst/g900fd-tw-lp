.class final Lcom/google/android/videos/pinning/TransfersExecutor;
.super Ljava/lang/Object;
.source "TransfersExecutor.java"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Lcom/google/android/videos/pinning/InternetConnectionChecker$Listener;
.implements Lcom/google/android/videos/pinning/Task$Listener;
.implements Lcom/google/android/videos/player/PlaybackStatusNotifier$Listener;
.implements Lcom/google/android/videos/utils/MediaMountedReceiver$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/pinning/TransfersExecutor$1;,
        Lcom/google/android/videos/pinning/TransfersExecutor$WishlistQuery;,
        Lcom/google/android/videos/pinning/TransfersExecutor$TransfersQuery;,
        Lcom/google/android/videos/pinning/TransfersExecutor$PreferencesListener;,
        Lcom/google/android/videos/pinning/TransfersExecutor$Listener;
    }
.end annotation


# instance fields
.field private activeTransfers:Z

.field private final backedOff:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/videos/pinning/Task$Key;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final backgroundHandler:Landroid/os/Handler;

.field private final backgroundThread:Landroid/os/HandlerThread;

.field private final config:Lcom/google/android/videos/Config;

.field private final connectivityManager:Landroid/net/ConnectivityManager;

.field private final context:Landroid/content/Context;

.field private final database:Lcom/google/android/videos/store/Database;

.field private final eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private final executorWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

.field private final failureCounts:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/videos/pinning/Task$Key;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private idle:Z

.field private final internetConnectionChecker:Lcom/google/android/videos/pinning/InternetConnectionChecker;

.field private final licenseRefreshTaskRetryInterval:Lcom/google/android/videos/pinning/RetryInterval;

.field private final licenseReleaseTaskRetryInterval:Lcom/google/android/videos/pinning/RetryInterval;

.field private final listener:Lcom/google/android/videos/pinning/TransfersExecutor$Listener;

.field private final maxConcurrentLicenseTasks:I

.field private final maxConcurrentOrBackedOffPinningTasks:I

.field private final maxConcurrentUserdataUpdateTasks:I

.field private final maxPinningTaskRetries:I

.field private final maxUpdateUserdataTaskRetries:I

.field private final mediaMountedReceiver:Lcom/google/android/videos/utils/MediaMountedReceiver;

.field private final pinningTaskRetryInterval:Lcom/google/android/videos/pinning/RetryInterval;

.field private postedTickets:I

.field private final preferencesListener:Lcom/google/android/videos/pinning/TransfersExecutor$PreferencesListener;

.field private processedTickets:I

.field private final refreshLicensesOlderThanMillis:J

.field private final streamingStatusNotifier:Lcom/google/android/videos/player/PlaybackStatusNotifier;

.field private final tasks:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/videos/pinning/Task$Key;",
            "Lcom/google/android/videos/pinning/Task",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final tasksWakeLock:Landroid/os/PowerManager$WakeLock;

.field private final tasksWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

.field private final ticketLock:Ljava/lang/Object;

.field private final updateUserdataTaskRetryInterval:Lcom/google/android/videos/pinning/RetryInterval;

.field private final videosGlobals:Lcom/google/android/videos/VideosGlobals;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/VideosGlobals;Landroid/content/Context;Lcom/google/android/videos/pinning/TransfersExecutor$Listener;)V
    .locals 11
    .param p1, "videosGlobals"    # Lcom/google/android/videos/VideosGlobals;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "listener"    # Lcom/google/android/videos/pinning/TransfersExecutor$Listener;

    .prologue
    const/4 v10, 0x1

    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    iput-object p1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    .line 146
    iput-object p2, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->context:Landroid/content/Context;

    .line 147
    iput-object p3, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->listener:Lcom/google/android/videos/pinning/TransfersExecutor$Listener;

    .line 148
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getPlaybackStatusNotifier()Lcom/google/android/videos/player/PlaybackStatusNotifier;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->streamingStatusNotifier:Lcom/google/android/videos/player/PlaybackStatusNotifier;

    .line 149
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 150
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->database:Lcom/google/android/videos/store/Database;

    .line 151
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->config:Lcom/google/android/videos/Config;

    .line 152
    new-instance v0, Lcom/google/android/videos/pinning/RetryInterval;

    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->config:Lcom/google/android/videos/Config;

    invoke-interface {v1}, Lcom/google/android/videos/Config;->minLicenseRefreshTaskRetryDelayMillis()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->config:Lcom/google/android/videos/Config;

    invoke-interface {v2}, Lcom/google/android/videos/Config;->maxLicenseRefreshTaskRetryDelayMillis()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/videos/pinning/RetryInterval;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->licenseRefreshTaskRetryInterval:Lcom/google/android/videos/pinning/RetryInterval;

    .line 155
    new-instance v0, Lcom/google/android/videos/pinning/RetryInterval;

    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->config:Lcom/google/android/videos/Config;

    invoke-interface {v1}, Lcom/google/android/videos/Config;->minLicenseReleaseTaskRetryDelayMillis()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->config:Lcom/google/android/videos/Config;

    invoke-interface {v2}, Lcom/google/android/videos/Config;->maxLicenseReleaseTaskRetryDelayMillis()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/videos/pinning/RetryInterval;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->licenseReleaseTaskRetryInterval:Lcom/google/android/videos/pinning/RetryInterval;

    .line 158
    new-instance v0, Lcom/google/android/videos/pinning/RetryInterval;

    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->config:Lcom/google/android/videos/Config;

    invoke-interface {v1}, Lcom/google/android/videos/Config;->minPinningTaskRetryDelayMillis()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->config:Lcom/google/android/videos/Config;

    invoke-interface {v2}, Lcom/google/android/videos/Config;->maxPinningTaskRetryDelayMillis()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/videos/pinning/RetryInterval;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->pinningTaskRetryInterval:Lcom/google/android/videos/pinning/RetryInterval;

    .line 160
    new-instance v0, Lcom/google/android/videos/pinning/RetryInterval;

    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->config:Lcom/google/android/videos/Config;

    invoke-interface {v1}, Lcom/google/android/videos/Config;->minUpdateUserdataTaskRetryDelayMillis()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->config:Lcom/google/android/videos/Config;

    invoke-interface {v2}, Lcom/google/android/videos/Config;->maxUpdateUserdataTaskRetryDelayMillis()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/videos/pinning/RetryInterval;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->updateUserdataTaskRetryInterval:Lcom/google/android/videos/pinning/RetryInterval;

    .line 163
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->config:Lcom/google/android/videos/Config;

    invoke-interface {v0}, Lcom/google/android/videos/Config;->refreshLicensesOlderThanMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->refreshLicensesOlderThanMillis:J

    .line 164
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->config:Lcom/google/android/videos/Config;

    invoke-interface {v0}, Lcom/google/android/videos/Config;->maxConcurrentLicenseTasks()I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->maxConcurrentLicenseTasks:I

    .line 165
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->config:Lcom/google/android/videos/Config;

    invoke-interface {v0}, Lcom/google/android/videos/Config;->maxConcurrentOrBackedOffPinningTasks()I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->maxConcurrentOrBackedOffPinningTasks:I

    .line 166
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->config:Lcom/google/android/videos/Config;

    invoke-interface {v0}, Lcom/google/android/videos/Config;->maxConcurrentUpdateUserdataTasks()I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->maxConcurrentUserdataUpdateTasks:I

    .line 167
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->config:Lcom/google/android/videos/Config;

    invoke-interface {v0}, Lcom/google/android/videos/Config;->maxPinningTaskRetries()I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->maxPinningTaskRetries:I

    .line 168
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->config:Lcom/google/android/videos/Config;

    invoke-interface {v0}, Lcom/google/android/videos/Config;->maxUpdateUserdataTaskRetries()I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->maxUpdateUserdataTaskRetries:I

    .line 169
    iput-boolean v10, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->idle:Z

    .line 171
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->ticketLock:Ljava/lang/Object;

    .line 172
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    .line 173
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->failureCounts:Ljava/util/Map;

    .line 174
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    .line 176
    const-string v0, "connectivity"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->connectivityManager:Landroid/net/ConnectivityManager;

    .line 178
    new-instance v0, Lcom/google/android/videos/pinning/InternetConnectionChecker;

    iget-object v2, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->config:Lcom/google/android/videos/Config;

    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getNetworkExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v5

    move-object v1, p2

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/pinning/InternetConnectionChecker;-><init>(Landroid/content/Context;Lcom/google/android/videos/Config;Ljava/util/concurrent/Executor;Lcom/google/android/videos/pinning/InternetConnectionChecker$Listener;Lcom/google/android/videos/utils/NetworkStatus;)V

    iput-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->internetConnectionChecker:Lcom/google/android/videos/pinning/InternetConnectionChecker;

    .line 181
    new-instance v0, Lcom/google/android/videos/pinning/TransfersExecutor$PreferencesListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/videos/pinning/TransfersExecutor$PreferencesListener;-><init>(Lcom/google/android/videos/pinning/TransfersExecutor;Lcom/google/android/videos/pinning/TransfersExecutor$1;)V

    iput-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->preferencesListener:Lcom/google/android/videos/pinning/TransfersExecutor$PreferencesListener;

    .line 182
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->preferencesListener:Lcom/google/android/videos/pinning/TransfersExecutor$PreferencesListener;

    invoke-virtual {v0}, Lcom/google/android/videos/pinning/TransfersExecutor$PreferencesListener;->register()V

    .line 183
    new-instance v0, Lcom/google/android/videos/utils/MediaMountedReceiver;

    invoke-direct {v0, p2, p0}, Lcom/google/android/videos/utils/MediaMountedReceiver;-><init>(Landroid/content/Context;Lcom/google/android/videos/utils/MediaMountedReceiver$Listener;)V

    iput-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->mediaMountedReceiver:Lcom/google/android/videos/utils/MediaMountedReceiver;

    .line 184
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->mediaMountedReceiver:Lcom/google/android/videos/utils/MediaMountedReceiver;

    invoke-virtual {v0}, Lcom/google/android/videos/utils/MediaMountedReceiver;->register()V

    .line 185
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->streamingStatusNotifier:Lcom/google/android/videos/player/PlaybackStatusNotifier;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/player/PlaybackStatusNotifier;->addListener(Lcom/google/android/videos/player/PlaybackStatusNotifier$Listener;)V

    .line 187
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    .line 188
    .local v6, "className":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_tasks"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 189
    .local v8, "tasksLockName":Ljava/lang/String;
    const-string v0, "power"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/os/PowerManager;

    .line 190
    .local v7, "powerManager":Landroid/os/PowerManager;
    invoke-virtual {v7, v10, v8}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasksWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 191
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasksWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, v10}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 193
    const-string v0, "wifi"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/net/wifi/WifiManager;

    .line 194
    .local v9, "wifiManager":Landroid/net/wifi/WifiManager;
    const/4 v0, 0x3

    invoke-virtual {v9, v0, v8}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasksWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    .line 195
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasksWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0, v10}, Landroid/net/wifi/WifiManager$WifiLock;->setReferenceCounted(Z)V

    .line 197
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_executor"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/net/wifi/WifiManager;->createWifiLock(Ljava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->executorWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    .line 198
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->executorWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager$WifiLock;->setReferenceCounted(Z)V

    .line 200
    new-instance v0, Landroid/os/HandlerThread;

    invoke-direct {v0, v6}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->backgroundThread:Landroid/os/HandlerThread;

    .line 201
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->backgroundThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 202
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->backgroundThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->backgroundHandler:Landroid/os/Handler;

    .line 203
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/videos/pinning/TransfersExecutor;)Lcom/google/android/videos/VideosGlobals;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pinning/TransfersExecutor;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/pinning/TransfersExecutor;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pinning/TransfersExecutor;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/pinning/TransfersExecutor;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/pinning/TransfersExecutor;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/google/android/videos/pinning/TransfersExecutor;->pingUnlessIdle()V

    return-void
.end method

.method private cancelBackedOffTask(Lcom/google/android/videos/pinning/Task$Key;)I
    .locals 3
    .param p1, "key"    # Lcom/google/android/videos/pinning/Task$Key;

    .prologue
    .line 796
    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 797
    .local v0, "taskType":I
    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->backgroundHandler:Landroid/os/Handler;

    const/4 v2, 0x5

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 798
    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->failureCounts:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 799
    return v0
.end method

.method private cancelRunningTask(Lcom/google/android/videos/pinning/Task$Key;)I
    .locals 2
    .param p1, "key"    # Lcom/google/android/videos/pinning/Task$Key;

    .prologue
    .line 789
    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/pinning/Task;

    .line 790
    .local v0, "task":Lcom/google/android/videos/pinning/Task;, "Lcom/google/android/videos/pinning/Task<*>;"
    invoke-virtual {v0}, Lcom/google/android/videos/pinning/Task;->cancel()V

    .line 791
    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->failureCounts:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 792
    iget v1, v0, Lcom/google/android/videos/pinning/Task;->taskType:I

    return v1
.end method

.method private cancelTaskNotOfType(Lcom/google/android/videos/pinning/Task$Key;I)I
    .locals 1
    .param p1, "key"    # Lcom/google/android/videos/pinning/Task$Key;
    .param p2, "taskType"    # I

    .prologue
    .line 779
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/pinning/Task;

    iget v0, v0, Lcom/google/android/videos/pinning/Task;->taskType:I

    if-eq v0, p2, :cond_0

    .line 780
    invoke-direct {p0, p1}, Lcom/google/android/videos/pinning/TransfersExecutor;->cancelRunningTask(Lcom/google/android/videos/pinning/Task$Key;)I

    move-result v0

    .line 785
    :goto_0
    return v0

    .line 782
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p2, :cond_1

    .line 783
    invoke-direct {p0, p1}, Lcom/google/android/videos/pinning/TransfersExecutor;->cancelBackedOffTask(Lcom/google/android/videos/pinning/Task$Key;)I

    move-result v0

    goto :goto_0

    .line 785
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private cancelTaskOfAnyType(Lcom/google/android/videos/pinning/Task$Key;)I
    .locals 1
    .param p1, "key"    # Lcom/google/android/videos/pinning/Task$Key;

    .prologue
    .line 752
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 753
    invoke-direct {p0, p1}, Lcom/google/android/videos/pinning/TransfersExecutor;->cancelRunningTask(Lcom/google/android/videos/pinning/Task$Key;)I

    move-result v0

    .line 757
    :goto_0
    return v0

    .line 754
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 755
    invoke-direct {p0, p1}, Lcom/google/android/videos/pinning/TransfersExecutor;->cancelBackedOffTask(Lcom/google/android/videos/pinning/Task$Key;)I

    move-result v0

    goto :goto_0

    .line 757
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private cancelTaskOfType(Lcom/google/android/videos/pinning/Task$Key;I)I
    .locals 1
    .param p1, "key"    # Lcom/google/android/videos/pinning/Task$Key;
    .param p2, "taskType"    # I

    .prologue
    .line 765
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/pinning/Task;

    iget v0, v0, Lcom/google/android/videos/pinning/Task;->taskType:I

    if-ne v0, p2, :cond_0

    .line 766
    invoke-direct {p0, p1}, Lcom/google/android/videos/pinning/TransfersExecutor;->cancelRunningTask(Lcom/google/android/videos/pinning/Task$Key;)I

    move-result v0

    .line 771
    :goto_0
    return v0

    .line 768
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p2, :cond_1

    .line 769
    invoke-direct {p0, p1}, Lcom/google/android/videos/pinning/TransfersExecutor;->cancelBackedOffTask(Lcom/google/android/videos/pinning/Task$Key;)I

    move-result v0

    goto :goto_0

    .line 771
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private deleteUnusedFiles(Ljava/io/File;ILjava/util/Map;Ljava/util/Map;)V
    .locals 11
    .param p1, "rootFilesDir"    # Ljava/io/File;
    .param p2, "storage"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "I",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/videos/pinning/DownloadKey;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 900
    .local p3, "pinnedKeys":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/videos/pinning/DownloadKey;Ljava/lang/Integer;>;"
    .local p4, "pinnedVideoIds":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Set<Ljava/lang/Integer;>;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 901
    .local v1, "allFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    invoke-static {p1, v1}, Lcom/google/android/videos/utils/OfflineUtil;->recursivelyListFiles(Ljava/io/File;Ljava/util/Collection;)V

    .line 902
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    .line 903
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    .line 904
    .local v6, "path":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v5

    .line 905
    .local v5, "parent":Ljava/lang/String;
    const-string v9, "subtitles"

    invoke-virtual {v5, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 907
    invoke-static {v6}, Lcom/google/android/videos/utils/OfflineUtil;->getVideoIdFromMetadataDownloadPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 908
    .local v8, "videoId":Ljava/lang/String;
    invoke-interface {p4, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 909
    iget-object v9, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->database:Lcom/google/android/videos/store/Database;

    invoke-static {v9, v8}, Lcom/google/android/videos/pinning/PinningDbHelper;->clearHaveSubtitles(Lcom/google/android/videos/store/Database;Ljava/lang/String;)V

    .line 910
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 911
    :cond_1
    invoke-interface {p4, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Set;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 913
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 915
    .end local v8    # "videoId":Ljava/lang/String;
    :cond_2
    const-string v9, "knowledge"

    invoke-virtual {v5, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 917
    const-string v9, "1"

    invoke-direct {p0, v2, p2, p4, v9}, Lcom/google/android/videos/pinning/TransfersExecutor;->maybeDeleteMetadataFile(Ljava/io/File;ILjava/util/Map;Ljava/lang/String;)V

    goto :goto_0

    .line 918
    :cond_3
    const-string v9, "storyboard"

    invoke-virtual {v5, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 920
    const-string v9, "1"

    invoke-direct {p0, v2, p2, p4, v9}, Lcom/google/android/videos/pinning/TransfersExecutor;->maybeDeleteMetadataFile(Ljava/io/File;ILjava/util/Map;Ljava/lang/String;)V

    goto :goto_0

    .line 922
    :cond_4
    invoke-static {v6}, Lcom/google/android/videos/utils/OfflineUtil;->getVideoIdFromMediaDownloadPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 923
    .restart local v8    # "videoId":Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/videos/utils/OfflineUtil;->getUserFromMediaDownloadPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 924
    .local v0, "account":Ljava/lang/String;
    if-nez v0, :cond_5

    .line 926
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 928
    :cond_5
    new-instance v4, Lcom/google/android/videos/pinning/DownloadKey;

    invoke-direct {v4, v0, v8}, Lcom/google/android/videos/pinning/DownloadKey;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 929
    .local v4, "key":Lcom/google/android/videos/pinning/DownloadKey;
    invoke-interface {p3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_6

    .line 930
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 931
    .local v7, "values":Landroid/content/ContentValues;
    const-string v9, "download_bytes_downloaded"

    invoke-virtual {v7, v9}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 932
    iget-object v9, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->database:Lcom/google/android/videos/store/Database;

    invoke-static {v9, v4, v7}, Lcom/google/android/videos/pinning/PinningDbHelper;->updatePinningStateForVideo(Lcom/google/android/videos/store/Database;Lcom/google/android/videos/pinning/DownloadKey;Landroid/content/ContentValues;)V

    .line 933
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto/16 :goto_0

    .line 934
    .end local v7    # "values":Landroid/content/ContentValues;
    :cond_6
    invoke-interface {p3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    if-eq p2, v9, :cond_0

    .line 936
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto/16 :goto_0

    .line 941
    .end local v0    # "account":Ljava/lang/String;
    .end local v2    # "file":Ljava/io/File;
    .end local v4    # "key":Lcom/google/android/videos/pinning/DownloadKey;
    .end local v5    # "parent":Ljava/lang/String;
    .end local v6    # "path":Ljava/lang/String;
    .end local v8    # "videoId":Ljava/lang/String;
    :cond_7
    return-void
.end method

.method private getTransfersCursor(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p1, "whereClause"    # Ljava/lang/String;
    .param p2, "whereArgs"    # [Ljava/lang/String;
    .param p3, "orderBy"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 661
    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v1}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 662
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "purchased_assets"

    sget-object v2, Lcom/google/android/videos/pinning/TransfersExecutor$TransfersQuery;->PROJECTION:[Ljava/lang/String;

    move-object v3, p1

    move-object v4, p2

    move-object v6, v5

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method private getWishlistCursor(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p1, "whereClause"    # Ljava/lang/String;
    .param p2, "whereArgs"    # [Ljava/lang/String;
    .param p3, "orderBy"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 679
    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v1}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 680
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "wishlist"

    sget-object v2, Lcom/google/android/videos/pinning/TransfersExecutor$WishlistQuery;->PROJECTION:[Ljava/lang/String;

    move-object v3, p1

    move-object v4, p2

    move-object v6, v5

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method private handleTaskCompleted(Lcom/google/android/videos/pinning/Task;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/pinning/Task",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 803
    .local p1, "task":Lcom/google/android/videos/pinning/Task;, "Lcom/google/android/videos/pinning/Task<*>;"
    iget-object v0, p1, Lcom/google/android/videos/pinning/Task;->key:Lcom/google/android/videos/pinning/Task$Key;

    .line 804
    .local v0, "key":Lcom/google/android/videos/pinning/Task$Key;
    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 805
    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->failureCounts:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 806
    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 807
    iget v1, p1, Lcom/google/android/videos/pinning/Task;->taskType:I

    if-nez v1, :cond_0

    .line 808
    check-cast v0, Lcom/google/android/videos/pinning/DownloadKey;

    .end local v0    # "key":Lcom/google/android/videos/pinning/Task$Key;
    const/4 v1, 0x3

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/videos/pinning/TransfersExecutor;->persistDownloadStatus(Lcom/google/android/videos/pinning/DownloadKey;IILjava/lang/Integer;)V

    .line 811
    :cond_0
    invoke-direct {p0}, Lcom/google/android/videos/pinning/TransfersExecutor;->ping()V

    .line 812
    return-void
.end method

.method private handleTaskError(Lcom/google/android/videos/pinning/Task;Lcom/google/android/videos/pinning/Task$TaskException;)V
    .locals 13
    .param p2, "exception"    # Lcom/google/android/videos/pinning/Task$TaskException;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/pinning/Task",
            "<*>;",
            "Lcom/google/android/videos/pinning/Task$TaskException;",
            ")V"
        }
    .end annotation

    .prologue
    .line 815
    .local p1, "task":Lcom/google/android/videos/pinning/Task;, "Lcom/google/android/videos/pinning/Task<*>;"
    iget-object v1, p1, Lcom/google/android/videos/pinning/Task;->key:Lcom/google/android/videos/pinning/Task$Key;

    .line 816
    .local v1, "key":Lcom/google/android/videos/pinning/Task$Key;
    iget-object v8, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    invoke-interface {v8, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 817
    iget-object v8, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->failureCounts:Ljava/util/Map;

    invoke-interface {v8, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    .line 818
    .local v7, "value":Ljava/lang/Integer;
    if-nez v7, :cond_1

    const/4 v5, 0x0

    .line 819
    .local v5, "strikes":I
    :goto_0
    iget v6, p1, Lcom/google/android/videos/pinning/Task;->taskType:I

    .line 828
    .local v6, "type":I
    iget-object v8, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->internetConnectionChecker:Lcom/google/android/videos/pinning/InternetConnectionChecker;

    invoke-virtual {v8}, Lcom/google/android/videos/pinning/InternetConnectionChecker;->onError()V

    .line 829
    iget-object v8, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->internetConnectionChecker:Lcom/google/android/videos/pinning/InternetConnectionChecker;

    invoke-virtual {v8}, Lcom/google/android/videos/pinning/InternetConnectionChecker;->haveInternetConnection()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 830
    add-int/lit8 v5, v5, 0x1

    .line 836
    :cond_0
    packed-switch v6, :pswitch_data_0

    .line 863
    new-instance v8, Ljava/lang/RuntimeException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unknown task type "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 818
    .end local v5    # "strikes":I
    .end local v6    # "type":I
    :cond_1
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v5

    goto :goto_0

    .line 838
    .restart local v5    # "strikes":I
    .restart local v6    # "type":I
    :pswitch_0
    instance-of v8, p2, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    if-eqz v8, :cond_4

    move-object v8, p2

    check-cast v8, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    move-object v3, v8

    .line 840
    .local v3, "pinningException":Lcom/google/android/videos/pinning/PinningTask$PinningException;
    :goto_1
    iget v8, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->maxPinningTaskRetries:I

    if-le v5, v8, :cond_5

    const/4 v2, 0x1

    .line 841
    .local v2, "maxRetries":Z
    :goto_2
    iget-object v4, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->pinningTaskRetryInterval:Lcom/google/android/videos/pinning/RetryInterval;

    .line 843
    .local v4, "retryInterval":Lcom/google/android/videos/pinning/RetryInterval;
    if-eqz v3, :cond_2

    iget-boolean v8, v3, Lcom/google/android/videos/pinning/PinningTask$PinningException;->fatal:Z

    if-eqz v8, :cond_6

    :cond_2
    const/4 v0, 0x1

    .line 866
    .end local v3    # "pinningException":Lcom/google/android/videos/pinning/PinningTask$PinningException;
    .local v0, "fatal":Z
    :goto_3
    invoke-virtual {p1, p2, v2, v0}, Lcom/google/android/videos/pinning/Task;->onError(Ljava/lang/Throwable;ZZ)V

    .line 867
    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "task error (strikes: %d, fatal: %b, maxRetries: %b, key: %s, type: %d)"

    const/4 v10, 0x5

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x3

    aput-object v1, v10, v11

    const/4 v11, 0x4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, p2}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 870
    if-nez v2, :cond_3

    if-eqz v0, :cond_8

    .line 871
    :cond_3
    iget-object v8, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->failureCounts:Ljava/util/Map;

    invoke-interface {v8, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 872
    iget-object v8, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    invoke-interface {v8, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 878
    .end local v5    # "strikes":I
    :goto_4
    invoke-direct {p0}, Lcom/google/android/videos/pinning/TransfersExecutor;->ping()V

    .line 879
    return-void

    .line 838
    .end local v0    # "fatal":Z
    .end local v2    # "maxRetries":Z
    .end local v4    # "retryInterval":Lcom/google/android/videos/pinning/RetryInterval;
    .restart local v5    # "strikes":I
    :cond_4
    const/4 v3, 0x0

    goto :goto_1

    .line 840
    .restart local v3    # "pinningException":Lcom/google/android/videos/pinning/PinningTask$PinningException;
    :cond_5
    const/4 v2, 0x0

    goto :goto_2

    .line 843
    .restart local v2    # "maxRetries":Z
    .restart local v4    # "retryInterval":Lcom/google/android/videos/pinning/RetryInterval;
    :cond_6
    const/4 v0, 0x0

    goto :goto_3

    .line 848
    .end local v2    # "maxRetries":Z
    .end local v3    # "pinningException":Lcom/google/android/videos/pinning/PinningTask$PinningException;
    .end local v4    # "retryInterval":Lcom/google/android/videos/pinning/RetryInterval;
    :pswitch_1
    iget v8, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->maxUpdateUserdataTaskRetries:I

    if-le v5, v8, :cond_7

    const/4 v2, 0x1

    .line 849
    .restart local v2    # "maxRetries":Z
    :goto_5
    iget-object v4, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->updateUserdataTaskRetryInterval:Lcom/google/android/videos/pinning/RetryInterval;

    .line 850
    .restart local v4    # "retryInterval":Lcom/google/android/videos/pinning/RetryInterval;
    const/4 v0, 0x0

    .line 851
    .restart local v0    # "fatal":Z
    goto :goto_3

    .line 848
    .end local v0    # "fatal":Z
    .end local v2    # "maxRetries":Z
    .end local v4    # "retryInterval":Lcom/google/android/videos/pinning/RetryInterval;
    :cond_7
    const/4 v2, 0x0

    goto :goto_5

    .line 853
    :pswitch_2
    const/4 v2, 0x0

    .line 854
    .restart local v2    # "maxRetries":Z
    iget-object v4, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->licenseRefreshTaskRetryInterval:Lcom/google/android/videos/pinning/RetryInterval;

    .line 855
    .restart local v4    # "retryInterval":Lcom/google/android/videos/pinning/RetryInterval;
    const/4 v0, 0x0

    .line 856
    .restart local v0    # "fatal":Z
    goto :goto_3

    .line 858
    .end local v0    # "fatal":Z
    .end local v2    # "maxRetries":Z
    .end local v4    # "retryInterval":Lcom/google/android/videos/pinning/RetryInterval;
    :pswitch_3
    const/4 v2, 0x0

    .line 859
    .restart local v2    # "maxRetries":Z
    iget-object v4, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->licenseReleaseTaskRetryInterval:Lcom/google/android/videos/pinning/RetryInterval;

    .line 860
    .restart local v4    # "retryInterval":Lcom/google/android/videos/pinning/RetryInterval;
    const/4 v0, 0x0

    .line 861
    .restart local v0    # "fatal":Z
    goto :goto_3

    .line 874
    :cond_8
    iget-object v8, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->failureCounts:Ljava/util/Map;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v1, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 875
    iget-object v8, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v1, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 876
    const/4 v8, 0x5

    if-nez v5, :cond_9

    const/4 v5, 0x1

    .end local v5    # "strikes":I
    :cond_9
    invoke-virtual {v4, v5}, Lcom/google/android/videos/pinning/RetryInterval;->getLength(I)J

    move-result-wide v10

    invoke-direct {p0, v8, v1, v10, v11}, Lcom/google/android/videos/pinning/TransfersExecutor;->sendMessageDelayed(ILjava/lang/Object;J)I

    goto :goto_4

    .line 836
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private isCurrent(Lcom/google/android/videos/pinning/Task;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/pinning/Task",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 344
    .local p1, "task":Lcom/google/android/videos/pinning/Task;, "Lcom/google/android/videos/pinning/Task<*>;"
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/videos/pinning/Task;->key:Lcom/google/android/videos/pinning/Task$Key;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private maybeDeleteMetadataFile(Ljava/io/File;ILjava/util/Map;Ljava/lang/String;)V
    .locals 3
    .param p1, "file"    # Ljava/io/File;
    .param p2, "storage"    # I
    .param p4, "expectedFileVersion"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 945
    .local p3, "pinnedVideoIds":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Set<Ljava/lang/Integer;>;>;"
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/utils/OfflineUtil;->getVideoIdFromMetadataDownloadPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 946
    .local v0, "videoId":Ljava/lang/String;
    invoke-interface {p3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p4}, Lcom/google/android/videos/utils/OfflineUtil;->isFileOfVersion(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 949
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 951
    :cond_1
    return-void
.end method

.method private ping()V
    .locals 42

    .prologue
    .line 355
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 356
    .local v4, "activeKeys":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/google/android/videos/pinning/Task$Key;>;"
    new-instance v15, Ljava/util/HashSet;

    invoke-direct {v15}, Ljava/util/HashSet;-><init>()V

    .line 357
    .local v15, "keysToCancel":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/google/android/videos/pinning/Task$Key;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    move-object/from16 v37, v0

    invoke-interface/range {v37 .. v37}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v37

    move-object/from16 v0, v37

    invoke-virtual {v15, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 358
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    move-object/from16 v37, v0

    invoke-interface/range {v37 .. v37}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v37

    move-object/from16 v0, v37

    invoke-virtual {v15, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 360
    const/16 v19, 0x0

    .line 361
    .local v19, "networkPendingFlags":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->context:Landroid/content/Context;

    move-object/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->internetConnectionChecker:Lcom/google/android/videos/pinning/InternetConnectionChecker;

    move-object/from16 v38, v0

    invoke-static/range {v37 .. v38}, Lcom/google/android/videos/pinning/NetworkPendChecker;->pendDueToConnection(Landroid/content/Context;Lcom/google/android/videos/pinning/InternetConnectionChecker;)Z

    move-result v37

    if-eqz v37, :cond_0

    .line 362
    or-int/lit8 v19, v19, 0x2

    .line 365
    :cond_0
    const/16 v37, 0x1e

    move/from16 v0, v37

    new-array v0, v0, [I

    move-object/from16 v23, v0

    .line 368
    .local v23, "pingValues":[I
    const/16 v30, 0x0

    .line 369
    .local v30, "runningLicenseTasks":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    move-object/from16 v37, v0

    invoke-interface/range {v37 .. v37}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v37

    invoke-interface/range {v37 .. v37}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v37

    if-eqz v37, :cond_3

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/google/android/videos/pinning/Task;

    .line 370
    .local v34, "task":Lcom/google/android/videos/pinning/Task;, "Lcom/google/android/videos/pinning/Task<*>;"
    move-object/from16 v0, v34

    iget v0, v0, Lcom/google/android/videos/pinning/Task;->taskType:I

    move/from16 v35, v0

    .line 371
    .local v35, "type":I
    const/16 v37, 0x2

    move/from16 v0, v35

    move/from16 v1, v37

    if-eq v0, v1, :cond_2

    const/16 v37, 0x1

    move/from16 v0, v35

    move/from16 v1, v37

    if-ne v0, v1, :cond_1

    .line 372
    :cond_2
    add-int/lit8 v30, v30, 0x1

    goto :goto_0

    .line 377
    .end local v34    # "task":Lcom/google/android/videos/pinning/Task;, "Lcom/google/android/videos/pinning/Task<*>;"
    .end local v35    # "type":I
    :cond_3
    const/4 v5, 0x0

    .line 378
    .local v5, "activeLicenseTransfers":Z
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    .line 379
    .local v20, "now":J
    const-string v37, "license_release_pending OR ((pinned IS NOT NULL AND pinned > 0) AND (license_type IS NOT NULL AND license_type != 0) AND (license_last_synced_timestamp NOT BETWEEN ? AND ? OR license_force_sync))"

    const/16 v38, 0x2

    move/from16 v0, v38

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v38, v0

    const/16 v39, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->refreshLicensesOlderThanMillis:J

    move-wide/from16 v40, v0

    sub-long v40, v20, v40

    invoke-static/range {v40 .. v41}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v40

    aput-object v40, v38, v39

    const/16 v39, 0x1

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v40

    aput-object v40, v38, v39

    const-string v39, "pinned"

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    move-object/from16 v2, v38

    move-object/from16 v3, v39

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/videos/pinning/TransfersExecutor;->getTransfersCursor(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 384
    .local v10, "cursor":Landroid/database/Cursor;
    :cond_4
    :goto_1
    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v37

    if-eqz v37, :cond_d

    .line 385
    const/16 v37, 0x5

    move/from16 v0, v37

    invoke-static {v10, v0}, Lcom/google/android/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v13

    .line 386
    .local v13, "isDash":Z
    const/16 v37, 0x3

    move/from16 v0, v37

    invoke-static {v10, v0}, Lcom/google/android/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v37

    if-nez v37, :cond_6

    const/16 v17, 0x1

    .line 388
    .local v17, "needsLicenseRefresh":Z
    :goto_2
    if-eqz v17, :cond_7

    .line 389
    const/16 v37, 0x8

    aget v38, v23, v37

    add-int/lit8 v38, v38, 0x1

    aput v38, v23, v37

    .line 394
    :goto_3
    const/4 v5, 0x1

    .line 395
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/google/android/videos/pinning/TransfersExecutor;->readDownloadKey(Landroid/database/Cursor;)Lcom/google/android/videos/pinning/DownloadKey;

    move-result-object v14

    .line 397
    .local v14, "key":Lcom/google/android/videos/pinning/DownloadKey;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->streamingStatusNotifier:Lcom/google/android/videos/player/PlaybackStatusNotifier;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Lcom/google/android/videos/player/PlaybackStatusNotifier;->isSessionInProgress()Z

    move-result v37

    if-eqz v37, :cond_8

    .line 401
    const/16 v37, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-direct {v0, v14, v1}, Lcom/google/android/videos/pinning/TransfersExecutor;->cancelTaskOfType(Lcom/google/android/videos/pinning/Task$Key;I)I

    move-result v9

    .line 402
    .local v9, "canceledTaskType":I
    const/16 v37, -0x1

    move/from16 v0, v37

    if-ne v9, v0, :cond_5

    .line 403
    const/16 v37, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-direct {v0, v14, v1}, Lcom/google/android/videos/pinning/TransfersExecutor;->cancelTaskOfType(Lcom/google/android/videos/pinning/Task$Key;I)I

    move-result v9

    .line 405
    :cond_5
    add-int/lit8 v37, v9, 0xc

    aget v38, v23, v37

    add-int/lit8 v38, v38, 0x1

    aput v38, v23, v37

    .line 406
    const/16 v37, -0x1

    move/from16 v0, v37

    if-eq v9, v0, :cond_4

    .line 407
    add-int/lit8 v30, v30, -0x1

    goto :goto_1

    .line 386
    .end local v9    # "canceledTaskType":I
    .end local v14    # "key":Lcom/google/android/videos/pinning/DownloadKey;
    .end local v17    # "needsLicenseRefresh":Z
    :cond_6
    const/16 v17, 0x0

    goto :goto_2

    .line 391
    .restart local v17    # "needsLicenseRefresh":Z
    :cond_7
    const/16 v37, 0x7

    aget v38, v23, v37

    add-int/lit8 v38, v38, 0x1

    aput v38, v23, v37
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 429
    .end local v13    # "isDash":Z
    .end local v17    # "needsLicenseRefresh":Z
    :catchall_0
    move-exception v37

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v37

    .line 409
    .restart local v13    # "isDash":Z
    .restart local v14    # "key":Lcom/google/android/videos/pinning/DownloadKey;
    .restart local v17    # "needsLicenseRefresh":Z
    :cond_8
    :try_start_1
    invoke-virtual {v4, v14}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_4

    .line 410
    if-eqz v17, :cond_b

    const/16 v37, 0x2

    :goto_4
    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-direct {v0, v14, v1}, Lcom/google/android/videos/pinning/TransfersExecutor;->cancelTaskNotOfType(Lcom/google/android/videos/pinning/Task$Key;I)I

    move-result v9

    .line 412
    .restart local v9    # "canceledTaskType":I
    add-int/lit8 v37, v9, 0xc

    aget v38, v23, v37

    add-int/lit8 v38, v38, 0x1

    aput v38, v23, v37

    .line 413
    const/16 v37, 0x2

    move/from16 v0, v37

    if-eq v9, v0, :cond_9

    const/16 v37, 0x1

    move/from16 v0, v37

    if-ne v9, v0, :cond_a

    .line 415
    :cond_9
    add-int/lit8 v30, v30, -0x1

    .line 417
    :cond_a
    if-nez v19, :cond_4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->maxConcurrentLicenseTasks:I

    move/from16 v37, v0

    move/from16 v0, v30

    move/from16 v1, v37

    if-ge v0, v1, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    invoke-interface {v0, v14}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v37

    if-nez v37, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    invoke-interface {v0, v14}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v37

    if-nez v37, :cond_4

    .line 419
    if-eqz v17, :cond_c

    .line 420
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v13}, Lcom/google/android/videos/pinning/TransfersExecutor;->startRefreshLicenseTask(Lcom/google/android/videos/pinning/DownloadKey;Z)V

    .line 424
    :goto_5
    add-int/lit8 v30, v30, 0x1

    goto/16 :goto_1

    .line 410
    .end local v9    # "canceledTaskType":I
    :cond_b
    const/16 v37, 0x1

    goto :goto_4

    .line 422
    .restart local v9    # "canceledTaskType":I
    :cond_c
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v13}, Lcom/google/android/videos/pinning/TransfersExecutor;->startReleaseLicenseTask(Lcom/google/android/videos/pinning/DownloadKey;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_5

    .line 429
    .end local v9    # "canceledTaskType":I
    .end local v13    # "isDash":Z
    .end local v14    # "key":Lcom/google/android/videos/pinning/DownloadKey;
    .end local v17    # "needsLicenseRefresh":Z
    :cond_d
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 433
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->context:Landroid/content/Context;

    move-object/from16 v37, v0

    invoke-static/range {v37 .. v37}, Lcom/google/android/videos/utils/OfflineUtil;->getSupportedRootFilesDir(Landroid/content/Context;)[Ljava/io/File;

    move-result-object v29

    .line 436
    .local v29, "rootFilesDirs":[Ljava/io/File;
    move/from16 v27, v19

    .line 437
    .local v27, "pinningPendingFlags":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->context:Landroid/content/Context;

    move-object/from16 v37, v0

    invoke-static/range {v37 .. v37}, Lcom/google/android/videos/pinning/NetworkPendChecker;->pendDueToWifi(Landroid/content/Context;)Z

    move-result v37

    if-eqz v37, :cond_12

    const/16 v37, 0x8

    :goto_6
    or-int v27, v27, v37

    .line 439
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->streamingStatusNotifier:Lcom/google/android/videos/player/PlaybackStatusNotifier;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Lcom/google/android/videos/player/PlaybackStatusNotifier;->isStreamingInProgress()Z

    move-result v37

    if-eqz v37, :cond_13

    const/16 v37, 0x20

    :goto_7
    or-int v27, v27, v37

    .line 442
    new-instance v24, Ljava/util/HashMap;

    invoke-direct/range {v24 .. v24}, Ljava/util/HashMap;-><init>()V

    .line 443
    .local v24, "pinnedKeys":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/videos/pinning/DownloadKey;Ljava/lang/Integer;>;"
    new-instance v26, Ljava/util/HashMap;

    invoke-direct/range {v26 .. v26}, Ljava/util/HashMap;-><init>()V

    .line 444
    .local v26, "pinnedVideoIds":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Set<Ljava/lang/Integer;>;>;"
    const/16 v31, 0x0

    .line 445
    .local v31, "runningOrBackedOffPinningTasks":I
    const/4 v7, 0x0

    .line 447
    .local v7, "activePinningTransfers":Z
    const-string v37, "(pinned IS NOT NULL AND pinned > 0)"

    const/16 v38, 0x0

    const-string v39, "pinning_status!=2,pinned"

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    move-object/from16 v2, v38

    move-object/from16 v3, v39

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/videos/pinning/TransfersExecutor;->getTransfersCursor(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 449
    :cond_e
    :goto_8
    :try_start_2
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v37

    if-eqz v37, :cond_1b

    .line 450
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/google/android/videos/pinning/TransfersExecutor;->readDownloadKey(Landroid/database/Cursor;)Lcom/google/android/videos/pinning/DownloadKey;

    move-result-object v14

    .line 451
    .restart local v14    # "key":Lcom/google/android/videos/pinning/DownloadKey;
    const/16 v28, 0x0

    .line 452
    .local v28, "rootFilesDir":Ljava/io/File;
    const/16 v37, 0x4

    move/from16 v0, v37

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v33

    .line 453
    .local v33, "storageIndex":I
    if-ltz v33, :cond_f

    move-object/from16 v0, v29

    array-length v0, v0

    move/from16 v37, v0

    move/from16 v0, v33

    move/from16 v1, v37

    if-lt v0, v1, :cond_14

    .line 454
    :cond_f
    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    const-string v38, "Storage index corrupt (out of bounds): "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v37

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-static/range {v37 .. v37}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 458
    :goto_9
    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v37

    move-object/from16 v0, v24

    move-object/from16 v1, v37

    invoke-interface {v0, v14, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 459
    iget-object v0, v14, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    move-object/from16 v37, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v37

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v37

    if-nez v37, :cond_10

    .line 460
    iget-object v0, v14, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    move-object/from16 v37, v0

    new-instance v38, Ljava/util/HashSet;

    invoke-direct/range {v38 .. v38}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v0, v26

    move-object/from16 v1, v37

    move-object/from16 v2, v38

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 462
    :cond_10
    iget-object v0, v14, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    move-object/from16 v37, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v37

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v37

    check-cast v37, Ljava/util/Set;

    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v38

    invoke-interface/range {v37 .. v38}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 463
    const/16 v37, 0x2

    move/from16 v0, v37

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v25

    .line 464
    .local v25, "pinnedStatus":I
    const/16 v37, 0x4

    move/from16 v0, v25

    move/from16 v1, v37

    if-eq v0, v1, :cond_15

    const/16 v37, 0x3

    move/from16 v0, v25

    move/from16 v1, v37

    if-eq v0, v1, :cond_15

    const/16 v18, 0x1

    .line 466
    .local v18, "needsPinningTransfer":Z
    :goto_a
    if-eqz v18, :cond_e

    .line 467
    const/16 v37, 0x6

    aget v38, v23, v37

    add-int/lit8 v38, v38, 0x1

    aput v38, v23, v37

    .line 469
    const/16 v22, 0x0

    .line 470
    .local v22, "pendingFlags":I
    if-nez v28, :cond_16

    const/16 v37, 0x4

    :goto_b
    or-int v22, v22, v37

    .line 472
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->maxConcurrentOrBackedOffPinningTasks:I

    move/from16 v37, v0

    move/from16 v0, v31

    move/from16 v1, v37

    if-lt v0, v1, :cond_17

    const/16 v37, 0x10

    :goto_c
    or-int v22, v22, v37

    .line 474
    if-eqz v22, :cond_18

    const/4 v6, 0x1

    .line 476
    .local v6, "activePinning":Z
    :goto_d
    or-int v22, v22, v27

    .line 477
    invoke-virtual {v4, v14}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_1a

    .line 478
    const/16 v37, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-direct {v0, v14, v1}, Lcom/google/android/videos/pinning/TransfersExecutor;->cancelTaskNotOfType(Lcom/google/android/videos/pinning/Task$Key;I)I

    move-result v9

    .line 479
    .restart local v9    # "canceledTaskType":I
    add-int/lit8 v37, v9, 0xc

    aget v38, v23, v37

    add-int/lit8 v38, v38, 0x1

    aput v38, v23, v37

    .line 480
    or-int/2addr v7, v6

    .line 481
    if-nez v22, :cond_19

    .line 482
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    invoke-interface {v0, v14}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v37

    if-nez v37, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    invoke-interface {v0, v14}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v37

    if-nez v37, :cond_11

    .line 483
    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v14, v1}, Lcom/google/android/videos/pinning/TransfersExecutor;->startPinningTask(Lcom/google/android/videos/pinning/DownloadKey;Ljava/io/File;)V

    .line 485
    :cond_11
    add-int/lit8 v31, v31, 0x1

    goto/16 :goto_8

    .line 437
    .end local v6    # "activePinning":Z
    .end local v7    # "activePinningTransfers":Z
    .end local v9    # "canceledTaskType":I
    .end local v14    # "key":Lcom/google/android/videos/pinning/DownloadKey;
    .end local v18    # "needsPinningTransfer":Z
    .end local v22    # "pendingFlags":I
    .end local v24    # "pinnedKeys":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/videos/pinning/DownloadKey;Ljava/lang/Integer;>;"
    .end local v25    # "pinnedStatus":I
    .end local v26    # "pinnedVideoIds":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Set<Ljava/lang/Integer;>;>;"
    .end local v28    # "rootFilesDir":Ljava/io/File;
    .end local v31    # "runningOrBackedOffPinningTasks":I
    .end local v33    # "storageIndex":I
    :cond_12
    const/16 v37, 0x0

    goto/16 :goto_6

    .line 439
    :cond_13
    const/16 v37, 0x0

    goto/16 :goto_7

    .line 456
    .restart local v7    # "activePinningTransfers":Z
    .restart local v14    # "key":Lcom/google/android/videos/pinning/DownloadKey;
    .restart local v24    # "pinnedKeys":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/videos/pinning/DownloadKey;Ljava/lang/Integer;>;"
    .restart local v26    # "pinnedVideoIds":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Set<Ljava/lang/Integer;>;>;"
    .restart local v28    # "rootFilesDir":Ljava/io/File;
    .restart local v31    # "runningOrBackedOffPinningTasks":I
    .restart local v33    # "storageIndex":I
    :cond_14
    aget-object v28, v29, v33

    goto/16 :goto_9

    .line 464
    .restart local v25    # "pinnedStatus":I
    :cond_15
    const/16 v18, 0x0

    goto :goto_a

    .line 470
    .restart local v18    # "needsPinningTransfer":Z
    .restart local v22    # "pendingFlags":I
    :cond_16
    const/16 v37, 0x0

    goto :goto_b

    .line 472
    :cond_17
    const/16 v37, 0x0

    goto :goto_c

    .line 474
    :cond_18
    const/4 v6, 0x0

    goto :goto_d

    .line 487
    .restart local v6    # "activePinning":Z
    .restart local v9    # "canceledTaskType":I
    :cond_19
    const/16 v37, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-direct {v0, v14, v1}, Lcom/google/android/videos/pinning/TransfersExecutor;->cancelTaskOfType(Lcom/google/android/videos/pinning/Task$Key;I)I

    move-result v9

    .line 488
    add-int/lit8 v37, v9, 0xc

    aget v38, v23, v37

    add-int/lit8 v38, v38, 0x1

    aput v38, v23, v37

    .line 489
    const/16 v37, 0x1

    const/16 v38, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v37

    move/from16 v2, v22

    move-object/from16 v3, v38

    invoke-virtual {v0, v14, v1, v2, v3}, Lcom/google/android/videos/pinning/TransfersExecutor;->persistDownloadStatus(Lcom/google/android/videos/pinning/DownloadKey;IILjava/lang/Integer;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto/16 :goto_8

    .line 506
    .end local v6    # "activePinning":Z
    .end local v9    # "canceledTaskType":I
    .end local v14    # "key":Lcom/google/android/videos/pinning/DownloadKey;
    .end local v18    # "needsPinningTransfer":Z
    .end local v22    # "pendingFlags":I
    .end local v25    # "pinnedStatus":I
    .end local v28    # "rootFilesDir":Ljava/io/File;
    .end local v33    # "storageIndex":I
    :catchall_1
    move-exception v37

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v37

    .line 498
    .restart local v6    # "activePinning":Z
    .restart local v14    # "key":Lcom/google/android/videos/pinning/DownloadKey;
    .restart local v18    # "needsPinningTransfer":Z
    .restart local v22    # "pendingFlags":I
    .restart local v25    # "pinnedStatus":I
    .restart local v28    # "rootFilesDir":Ljava/io/File;
    .restart local v33    # "storageIndex":I
    :cond_1a
    if-eqz v22, :cond_e

    .line 499
    const/16 v37, 0x1

    const/16 v38, 0x0

    :try_start_3
    move-object/from16 v0, p0

    move/from16 v1, v37

    move/from16 v2, v22

    move-object/from16 v3, v38

    invoke-virtual {v0, v14, v1, v2, v3}, Lcom/google/android/videos/pinning/TransfersExecutor;->persistDownloadStatus(Lcom/google/android/videos/pinning/DownloadKey;IILjava/lang/Integer;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto/16 :goto_8

    .line 506
    .end local v6    # "activePinning":Z
    .end local v14    # "key":Lcom/google/android/videos/pinning/DownloadKey;
    .end local v18    # "needsPinningTransfer":Z
    .end local v22    # "pendingFlags":I
    .end local v25    # "pinnedStatus":I
    .end local v28    # "rootFilesDir":Ljava/io/File;
    .end local v33    # "storageIndex":I
    :cond_1b
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 509
    const/16 v32, 0x0

    .line 510
    .local v32, "runningUserdataUpdateTasks":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    move-object/from16 v37, v0

    invoke-interface/range {v37 .. v37}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v37

    invoke-interface/range {v37 .. v37}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_1c
    :goto_e
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v37

    if-eqz v37, :cond_1e

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/google/android/videos/pinning/Task;

    .line 511
    .restart local v34    # "task":Lcom/google/android/videos/pinning/Task;, "Lcom/google/android/videos/pinning/Task<*>;"
    move-object/from16 v0, v34

    iget v0, v0, Lcom/google/android/videos/pinning/Task;->taskType:I

    move/from16 v37, v0

    const/16 v38, 0x3

    move/from16 v0, v37

    move/from16 v1, v38

    if-eq v0, v1, :cond_1d

    move-object/from16 v0, v34

    iget v0, v0, Lcom/google/android/videos/pinning/Task;->taskType:I

    move/from16 v37, v0

    const/16 v38, 0x4

    move/from16 v0, v37

    move/from16 v1, v38

    if-ne v0, v1, :cond_1c

    .line 513
    :cond_1d
    add-int/lit8 v32, v32, 0x1

    goto :goto_e

    .line 518
    .end local v34    # "task":Lcom/google/android/videos/pinning/Task;, "Lcom/google/android/videos/pinning/Task<*>;"
    :cond_1e
    const-string v37, "last_playback_is_dirty"

    const/16 v38, 0x0

    const/16 v39, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    move-object/from16 v2, v38

    move-object/from16 v3, v39

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/videos/pinning/TransfersExecutor;->getTransfersCursor(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 520
    :cond_1f
    :goto_f
    :try_start_4
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v37

    if-eqz v37, :cond_20

    .line 521
    const/16 v37, 0x9

    aget v38, v23, v37

    add-int/lit8 v38, v38, 0x1

    aput v38, v23, v37

    .line 522
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/google/android/videos/pinning/TransfersExecutor;->readUpdateLastPlaybackKey(Landroid/database/Cursor;)Lcom/google/android/videos/pinning/UpdateLastPlaybackKey;

    move-result-object v14

    .line 523
    .local v14, "key":Lcom/google/android/videos/pinning/UpdateLastPlaybackKey;
    invoke-virtual {v4, v14}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_1f

    if-nez v19, :cond_1f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->streamingStatusNotifier:Lcom/google/android/videos/player/PlaybackStatusNotifier;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Lcom/google/android/videos/player/PlaybackStatusNotifier;->isSessionInProgress()Z

    move-result v37

    if-nez v37, :cond_1f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->maxConcurrentUserdataUpdateTasks:I

    move/from16 v37, v0

    move/from16 v0, v32

    move/from16 v1, v37

    if-ge v0, v1, :cond_1f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    invoke-interface {v0, v14}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v37

    if-nez v37, :cond_1f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    invoke-interface {v0, v14}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v37

    if-nez v37, :cond_1f

    .line 529
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/videos/pinning/TransfersExecutor;->startUpdateLastPlaybackTask(Lcom/google/android/videos/pinning/UpdateLastPlaybackKey;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 530
    add-int/lit8 v32, v32, 0x1

    goto :goto_f

    .line 534
    .end local v14    # "key":Lcom/google/android/videos/pinning/UpdateLastPlaybackKey;
    :cond_20
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 538
    const-string v37, "wishlist_item_state != 1"

    const/16 v38, 0x0

    const/16 v39, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    move-object/from16 v2, v38

    move-object/from16 v3, v39

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/videos/pinning/TransfersExecutor;->getWishlistCursor(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 540
    :cond_21
    :goto_10
    :try_start_5
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v37

    if-eqz v37, :cond_22

    .line 541
    const/16 v37, 0xa

    aget v38, v23, v37

    add-int/lit8 v38, v38, 0x1

    aput v38, v23, v37

    .line 542
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/google/android/videos/pinning/TransfersExecutor;->readUpdateWishlistKey(Landroid/database/Cursor;)Lcom/google/android/videos/pinning/UpdateWishlistKey;

    move-result-object v14

    .line 543
    .local v14, "key":Lcom/google/android/videos/pinning/UpdateWishlistKey;
    invoke-virtual {v4, v14}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_21

    if-nez v19, :cond_21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->streamingStatusNotifier:Lcom/google/android/videos/player/PlaybackStatusNotifier;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Lcom/google/android/videos/player/PlaybackStatusNotifier;->isSessionInProgress()Z

    move-result v37

    if-nez v37, :cond_21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->maxConcurrentUserdataUpdateTasks:I

    move/from16 v37, v0

    move/from16 v0, v32

    move/from16 v1, v37

    if-ge v0, v1, :cond_21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    invoke-interface {v0, v14}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v37

    if-nez v37, :cond_21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    invoke-interface {v0, v14}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v37

    if-nez v37, :cond_21

    .line 549
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/videos/pinning/TransfersExecutor;->startUpdateWishlistTask(Lcom/google/android/videos/pinning/UpdateWishlistKey;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 550
    add-int/lit8 v32, v32, 0x1

    goto :goto_10

    .line 534
    .end local v14    # "key":Lcom/google/android/videos/pinning/UpdateWishlistKey;
    :catchall_2
    move-exception v37

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v37

    .line 554
    :cond_22
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 557
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->config:Lcom/google/android/videos/Config;

    move-object/from16 v37, v0

    invoke-interface/range {v37 .. v37}, Lcom/google/android/videos/Config;->useDashForStreaming()Z

    move-result v37

    if-eqz v37, :cond_25

    .line 559
    const-string v37, "streaming_pssh_data IS NULL AND streaming_mimetype IS NULL AND asset_type IN (6,20)"

    const/16 v38, 0x0

    const/16 v39, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    move-object/from16 v2, v38

    move-object/from16 v3, v39

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/videos/pinning/TransfersExecutor;->getTransfersCursor(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 561
    :cond_23
    :goto_11
    :try_start_6
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v37

    if-eqz v37, :cond_24

    .line 562
    const/16 v37, 0xb

    aget v38, v23, v37

    add-int/lit8 v38, v38, 0x1

    aput v38, v23, v37

    .line 563
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/google/android/videos/pinning/TransfersExecutor;->readDownloadKey(Landroid/database/Cursor;)Lcom/google/android/videos/pinning/DownloadKey;

    move-result-object v14

    .line 564
    .local v14, "key":Lcom/google/android/videos/pinning/DownloadKey;
    invoke-virtual {v4, v14}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_23

    if-nez v19, :cond_23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->streamingStatusNotifier:Lcom/google/android/videos/player/PlaybackStatusNotifier;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Lcom/google/android/videos/player/PlaybackStatusNotifier;->isSessionInProgress()Z

    move-result v37

    if-nez v37, :cond_23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->maxConcurrentUserdataUpdateTasks:I

    move/from16 v37, v0

    move/from16 v0, v32

    move/from16 v1, v37

    if-ge v0, v1, :cond_23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    invoke-interface {v0, v14}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v37

    if-nez v37, :cond_23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    invoke-interface {v0, v14}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v37

    if-nez v37, :cond_23

    .line 570
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/videos/pinning/TransfersExecutor;->startStreamingPsshPrefetchTask(Lcom/google/android/videos/pinning/DownloadKey;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    .line 571
    add-int/lit8 v32, v32, 0x1

    goto :goto_11

    .line 554
    .end local v14    # "key":Lcom/google/android/videos/pinning/DownloadKey;
    :catchall_3
    move-exception v37

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v37

    .line 575
    :cond_24
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 580
    :cond_25
    invoke-virtual {v15, v4}, Ljava/util/HashSet;->removeAll(Ljava/util/Collection;)Z

    .line 581
    invoke-virtual {v15}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_12
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v37

    if-eqz v37, :cond_26

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/videos/pinning/Task$Key;

    .line 582
    .local v14, "key":Lcom/google/android/videos/pinning/Task$Key;
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/videos/pinning/TransfersExecutor;->cancelTaskOfAnyType(Lcom/google/android/videos/pinning/Task$Key;)I

    move-result v9

    .line 583
    .restart local v9    # "canceledTaskType":I
    add-int/lit8 v37, v9, 0xc

    aget v38, v23, v37

    add-int/lit8 v38, v38, 0x1

    aput v38, v23, v37

    goto :goto_12

    .line 575
    .end local v9    # "canceledTaskType":I
    .end local v14    # "key":Lcom/google/android/videos/pinning/Task$Key;
    :catchall_4
    move-exception v37

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v37

    .line 586
    :cond_26
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/google/android/videos/pinning/TransfersExecutor;->updateRunningTaskCounts([I)V

    .line 587
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/google/android/videos/pinning/TransfersExecutor;->updateBackedOffTaskCounts([I)V

    .line 589
    const/16 v16, 0x0

    .line 590
    .local v16, "logPing":Z
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_13
    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v37, v0

    move/from16 v0, v37

    if-ge v11, v0, :cond_27

    .line 591
    aget v37, v23, v11

    if-eqz v37, :cond_2a

    .line 592
    const/16 v16, 0x1

    .line 596
    :cond_27
    if-eqz v16, :cond_28

    .line 597
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    move-object/from16 v1, v23

    move/from16 v2, v27

    invoke-interface {v0, v1, v2}, Lcom/google/android/videos/logging/EventLogger;->onTransfersPing([II)V

    .line 604
    :cond_28
    const/4 v8, 0x0

    .line 605
    .local v8, "anyStorageMounted":Z
    const/4 v11, 0x0

    :goto_14
    move-object/from16 v0, v29

    array-length v0, v0

    move/from16 v37, v0

    move/from16 v0, v37

    if-ge v11, v0, :cond_2b

    .line 606
    aget-object v37, v29, v11

    if-eqz v37, :cond_29

    .line 607
    aget-object v37, v29, v11

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    move-object/from16 v2, v24

    move-object/from16 v3, v26

    invoke-direct {v0, v1, v11, v2, v3}, Lcom/google/android/videos/pinning/TransfersExecutor;->deleteUnusedFiles(Ljava/io/File;ILjava/util/Map;Ljava/util/Map;)V

    .line 608
    const/4 v8, 0x1

    .line 605
    :cond_29
    add-int/lit8 v11, v11, 0x1

    goto :goto_14

    .line 590
    .end local v8    # "anyStorageMounted":Z
    :cond_2a
    add-int/lit8 v11, v11, 0x1

    goto :goto_13

    .line 611
    .restart local v8    # "anyStorageMounted":Z
    :cond_2b
    if-eqz v8, :cond_2e

    .line 612
    const-string v37, "NOT (pinned IS NOT NULL AND pinned > 0) AND (have_subtitles OR download_bytes_downloaded IS NOT NULL)"

    const/16 v38, 0x0

    const/16 v39, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    move-object/from16 v2, v38

    move-object/from16 v3, v39

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/videos/pinning/TransfersExecutor;->getTransfersCursor(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 614
    :cond_2c
    :goto_15
    :try_start_7
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v37

    if-eqz v37, :cond_2d

    .line 615
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/google/android/videos/pinning/TransfersExecutor;->readDownloadKey(Landroid/database/Cursor;)Lcom/google/android/videos/pinning/DownloadKey;

    move-result-object v14

    .line 616
    .local v14, "key":Lcom/google/android/videos/pinning/DownloadKey;
    move-object/from16 v0, v24

    invoke-interface {v0, v14}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v37

    if-nez v37, :cond_2c

    .line 617
    new-instance v36, Landroid/content/ContentValues;

    invoke-direct/range {v36 .. v36}, Landroid/content/ContentValues;-><init>()V

    .line 618
    .local v36, "values":Landroid/content/ContentValues;
    const-string v37, "download_bytes_downloaded"

    invoke-virtual/range {v36 .. v37}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 619
    const-string v37, "have_subtitles"

    const/16 v38, 0x0

    invoke-static/range {v38 .. v38}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v38

    invoke-virtual/range {v36 .. v38}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 620
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->database:Lcom/google/android/videos/store/Database;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    move-object/from16 v1, v36

    invoke-static {v0, v14, v1}, Lcom/google/android/videos/pinning/PinningDbHelper;->updatePinningStateForVideo(Lcom/google/android/videos/store/Database;Lcom/google/android/videos/pinning/DownloadKey;Landroid/content/ContentValues;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_5

    goto :goto_15

    .line 624
    .end local v14    # "key":Lcom/google/android/videos/pinning/DownloadKey;
    .end local v36    # "values":Landroid/content/ContentValues;
    :catchall_5
    move-exception v37

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v37

    :cond_2d
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 628
    :cond_2e
    invoke-virtual {v4}, Ljava/util/HashSet;->isEmpty()Z

    move-result v37

    if-nez v37, :cond_31

    const/16 v37, 0x1

    :goto_16
    move/from16 v0, v37

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/videos/pinning/TransfersExecutor;->activeTransfers:Z

    .line 629
    if-nez v5, :cond_2f

    if-eqz v7, :cond_32

    .line 630
    :cond_2f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->executorWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v37

    if-nez v37, :cond_30

    .line 633
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->executorWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    .line 638
    :cond_30
    :goto_17
    return-void

    .line 628
    :cond_31
    const/16 v37, 0x0

    goto :goto_16

    .line 635
    :cond_32
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->executorWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v37

    if-eqz v37, :cond_30

    .line 636
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pinning/TransfersExecutor;->executorWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto :goto_17
.end method

.method private pingUnlessIdle()V
    .locals 2

    .prologue
    .line 258
    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->ticketLock:Ljava/lang/Object;

    monitor-enter v1

    .line 259
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->idle:Z

    if-nez v0, :cond_0

    .line 260
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/videos/pinning/TransfersExecutor;->sendMessage(I)I

    .line 262
    :cond_0
    monitor-exit v1

    .line 263
    return-void

    .line 262
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private readDownloadKey(Landroid/database/Cursor;)Lcom/google/android/videos/pinning/DownloadKey;
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 667
    const/4 v2, 0x0

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 668
    .local v0, "account":Ljava/lang/String;
    const/4 v2, 0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 669
    .local v1, "videoId":Ljava/lang/String;
    new-instance v2, Lcom/google/android/videos/pinning/DownloadKey;

    invoke-direct {v2, v0, v1}, Lcom/google/android/videos/pinning/DownloadKey;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method private readUpdateLastPlaybackKey(Landroid/database/Cursor;)Lcom/google/android/videos/pinning/UpdateLastPlaybackKey;
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 673
    const/4 v2, 0x0

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 674
    .local v0, "account":Ljava/lang/String;
    const/4 v2, 0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 675
    .local v1, "videoId":Ljava/lang/String;
    new-instance v2, Lcom/google/android/videos/pinning/UpdateLastPlaybackKey;

    invoke-direct {v2, v0, v1}, Lcom/google/android/videos/pinning/UpdateLastPlaybackKey;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method private readUpdateWishlistKey(Landroid/database/Cursor;)Lcom/google/android/videos/pinning/UpdateWishlistKey;
    .locals 4
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 685
    const/4 v3, 0x0

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 686
    .local v0, "account":Ljava/lang/String;
    const/4 v3, 0x1

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 687
    .local v2, "itemType":I
    const/4 v3, 0x2

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 688
    .local v1, "itemId":Ljava/lang/String;
    new-instance v3, Lcom/google/android/videos/pinning/UpdateWishlistKey;

    invoke-direct {v3, v0, v2, v1}, Lcom/google/android/videos/pinning/UpdateWishlistKey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    return-object v3
.end method

.method private sendMessage(I)I
    .locals 2
    .param p1, "what"    # I

    .prologue
    .line 234
    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->ticketLock:Ljava/lang/Object;

    monitor-enter v1

    .line 235
    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->backgroundHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 236
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->idle:Z

    .line 237
    iget v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->postedTickets:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->postedTickets:I

    monitor-exit v1

    return v0

    .line 238
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private sendMessage(ILjava/lang/Object;)I
    .locals 2
    .param p1, "what"    # I
    .param p2, "obj"    # Ljava/lang/Object;

    .prologue
    .line 242
    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->ticketLock:Ljava/lang/Object;

    monitor-enter v1

    .line 243
    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->backgroundHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 244
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->idle:Z

    .line 245
    iget v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->postedTickets:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->postedTickets:I

    monitor-exit v1

    return v0

    .line 246
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private sendMessageDelayed(ILjava/lang/Object;J)I
    .locals 3
    .param p1, "what"    # I
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "delayMillis"    # J

    .prologue
    .line 250
    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->ticketLock:Ljava/lang/Object;

    monitor-enter v1

    .line 251
    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->backgroundHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->backgroundHandler:Landroid/os/Handler;

    invoke-virtual {v2, p1, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v0, v2, p3, p4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 252
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->idle:Z

    .line 253
    iget v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->postedTickets:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->postedTickets:I

    monitor-exit v1

    return v0

    .line 254
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private startPinningTask(Lcom/google/android/videos/pinning/DownloadKey;Ljava/io/File;)V
    .locals 8
    .param p1, "key"    # Lcom/google/android/videos/pinning/DownloadKey;
    .param p2, "rootFilesDir"    # Ljava/io/File;

    .prologue
    .line 692
    const/4 v0, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/google/android/videos/pinning/TransfersExecutor;->persistDownloadStatus(Lcom/google/android/videos/pinning/DownloadKey;IILjava/lang/Integer;)V

    .line 694
    new-instance v0, Lcom/google/android/videos/pinning/PinningTask;

    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    iget-object v3, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasksWakeLock:Landroid/os/PowerManager$WakeLock;

    iget-object v4, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasksWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    const/4 v7, 0x1

    move-object v2, p1

    move-object v5, p0

    move-object v6, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/pinning/PinningTask;-><init>(Lcom/google/android/videos/VideosGlobals;Lcom/google/android/videos/pinning/DownloadKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/videos/pinning/Task$Listener;Ljava/io/File;Z)V

    invoke-direct {p0, v0}, Lcom/google/android/videos/pinning/TransfersExecutor;->startTask(Lcom/google/android/videos/pinning/Task;)V

    .line 696
    return-void
.end method

.method private startRefreshLicenseTask(Lcom/google/android/videos/pinning/DownloadKey;Z)V
    .locals 11
    .param p1, "key"    # Lcom/google/android/videos/pinning/DownloadKey;
    .param p2, "isDash"    # Z

    .prologue
    .line 709
    if-eqz p2, :cond_0

    new-instance v0, Lcom/google/android/videos/pinning/DashLicenseTask$Refresh;

    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    iget-object v3, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasksWakeLock:Landroid/os/PowerManager$WakeLock;

    iget-object v4, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasksWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    iget-object v6, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    move-object v2, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/pinning/DashLicenseTask$Refresh;-><init>(Lcom/google/android/videos/VideosGlobals;Lcom/google/android/videos/pinning/DownloadKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/videos/pinning/Task$Listener;Lcom/google/android/videos/logging/EventLogger;)V

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/videos/pinning/TransfersExecutor;->startTask(Lcom/google/android/videos/pinning/Task;)V

    .line 716
    return-void

    .line 709
    :cond_0
    new-instance v0, Lcom/google/android/videos/pinning/LegacyLicenseTask$Refresh;

    iget-object v2, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasksWakeLock:Landroid/os/PowerManager$WakeLock;

    iget-object v3, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasksWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    iget-object v5, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->database:Lcom/google/android/videos/store/Database;

    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getDrmManager()Lcom/google/android/videos/drm/DrmManager;

    move-result-object v6

    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v7

    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->legacyDownloadsHaveAppLevelDrm()Z

    move-result v8

    iget-object v9, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getItagInfoStore()Lcom/google/android/videos/store/ItagInfoStore;

    move-result-object v10

    move-object v1, p1

    move-object v4, p0

    invoke-direct/range {v0 .. v10}, Lcom/google/android/videos/pinning/LegacyLicenseTask$Refresh;-><init>(Lcom/google/android/videos/pinning/DownloadKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/videos/pinning/Task$Listener;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/drm/DrmManager;Lcom/google/android/videos/accounts/AccountManagerWrapper;ZLcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/store/ItagInfoStore;)V

    goto :goto_0
.end method

.method private startReleaseLicenseTask(Lcom/google/android/videos/pinning/DownloadKey;Z)V
    .locals 11
    .param p1, "key"    # Lcom/google/android/videos/pinning/DownloadKey;
    .param p2, "isDash"    # Z

    .prologue
    .line 699
    if-eqz p2, :cond_0

    new-instance v0, Lcom/google/android/videos/pinning/DashLicenseTask$Release;

    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    iget-object v3, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasksWakeLock:Landroid/os/PowerManager$WakeLock;

    iget-object v4, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasksWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    iget-object v6, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    move-object v2, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/pinning/DashLicenseTask$Release;-><init>(Lcom/google/android/videos/VideosGlobals;Lcom/google/android/videos/pinning/DownloadKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/videos/pinning/Task$Listener;Lcom/google/android/videos/logging/EventLogger;)V

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/videos/pinning/TransfersExecutor;->startTask(Lcom/google/android/videos/pinning/Task;)V

    .line 706
    return-void

    .line 699
    :cond_0
    new-instance v0, Lcom/google/android/videos/pinning/LegacyLicenseTask$Release;

    iget-object v2, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasksWakeLock:Landroid/os/PowerManager$WakeLock;

    iget-object v3, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasksWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    iget-object v5, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->database:Lcom/google/android/videos/store/Database;

    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getDrmManager()Lcom/google/android/videos/drm/DrmManager;

    move-result-object v6

    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v7

    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->legacyDownloadsHaveAppLevelDrm()Z

    move-result v8

    iget-object v9, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getItagInfoStore()Lcom/google/android/videos/store/ItagInfoStore;

    move-result-object v10

    move-object v1, p1

    move-object v4, p0

    invoke-direct/range {v0 .. v10}, Lcom/google/android/videos/pinning/LegacyLicenseTask$Release;-><init>(Lcom/google/android/videos/pinning/DownloadKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/videos/pinning/Task$Listener;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/drm/DrmManager;Lcom/google/android/videos/accounts/AccountManagerWrapper;ZLcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/store/ItagInfoStore;)V

    goto :goto_0
.end method

.method private startStreamingPsshPrefetchTask(Lcom/google/android/videos/pinning/DownloadKey;)V
    .locals 7
    .param p1, "key"    # Lcom/google/android/videos/pinning/DownloadKey;

    .prologue
    .line 732
    new-instance v0, Lcom/google/android/videos/pinning/StreamingPsshPrefetchTask;

    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    iget-object v3, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasksWakeLock:Landroid/os/PowerManager$WakeLock;

    iget-object v4, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasksWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    iget-object v6, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    move-object v2, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/pinning/StreamingPsshPrefetchTask;-><init>(Lcom/google/android/videos/VideosGlobals;Lcom/google/android/videos/pinning/DownloadKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/videos/pinning/Task$Listener;Lcom/google/android/videos/logging/EventLogger;)V

    invoke-direct {p0, v0}, Lcom/google/android/videos/pinning/TransfersExecutor;->startTask(Lcom/google/android/videos/pinning/Task;)V

    .line 734
    return-void
.end method

.method private startTask(Lcom/google/android/videos/pinning/Task;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/pinning/Task",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 737
    .local p1, "task":Lcom/google/android/videos/pinning/Task;, "Lcom/google/android/videos/pinning/Task<*>;"
    iget-object v0, p1, Lcom/google/android/videos/pinning/Task;->key:Lcom/google/android/videos/pinning/Task$Key;

    .line 738
    .local v0, "key":Lcom/google/android/videos/pinning/Task$Key;
    iget-object v2, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 739
    iget-object v2, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    invoke-interface {v2, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 740
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Task:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 741
    .local v1, "threadName":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 742
    const/16 v2, 0x3a

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 743
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 744
    new-instance v2, Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p1, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 745
    return-void

    .line 738
    .end local v1    # "threadName":Ljava/lang/StringBuilder;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private startUpdateLastPlaybackTask(Lcom/google/android/videos/pinning/UpdateLastPlaybackKey;)V
    .locals 10
    .param p1, "key"    # Lcom/google/android/videos/pinning/UpdateLastPlaybackKey;

    .prologue
    .line 719
    new-instance v0, Lcom/google/android/videos/pinning/UpdateLastPlaybackTask;

    iget-object v2, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasksWakeLock:Landroid/os/PowerManager$WakeLock;

    iget-object v3, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasksWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    iget-object v5, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->database:Lcom/google/android/videos/store/Database;

    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v6

    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/videos/api/ApiRequesters;->getVideoUpdateRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->connectivityManager:Landroid/net/ConnectivityManager;

    iget-object v9, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    move-object v1, p1

    move-object v4, p0

    invoke-direct/range {v0 .. v9}, Lcom/google/android/videos/pinning/UpdateLastPlaybackTask;-><init>(Lcom/google/android/videos/pinning/UpdateLastPlaybackKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/videos/pinning/Task$Listener;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/async/Requester;Landroid/net/ConnectivityManager;Lcom/google/android/videos/logging/EventLogger;)V

    invoke-direct {p0, v0}, Lcom/google/android/videos/pinning/TransfersExecutor;->startTask(Lcom/google/android/videos/pinning/Task;)V

    .line 723
    return-void
.end method

.method private startUpdateWishlistTask(Lcom/google/android/videos/pinning/UpdateWishlistKey;)V
    .locals 9
    .param p1, "key"    # Lcom/google/android/videos/pinning/UpdateWishlistKey;

    .prologue
    .line 726
    new-instance v0, Lcom/google/android/videos/pinning/UpdateWishlistTask;

    iget-object v2, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasksWakeLock:Landroid/os/PowerManager$WakeLock;

    iget-object v3, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasksWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    iget-object v5, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->database:Lcom/google/android/videos/store/Database;

    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v6

    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/videos/api/ApiRequesters;->getUpdateWishlistSyncRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    move-object v1, p1

    move-object v4, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/videos/pinning/UpdateWishlistTask;-><init>(Lcom/google/android/videos/pinning/UpdateWishlistKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/videos/pinning/Task$Listener;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/logging/EventLogger;)V

    invoke-direct {p0, v0}, Lcom/google/android/videos/pinning/TransfersExecutor;->startTask(Lcom/google/android/videos/pinning/Task;)V

    .line 729
    return-void
.end method

.method private updateBackedOffTaskCounts([I)V
    .locals 6
    .param p1, "pingValues"    # [I

    .prologue
    .line 641
    iget-object v3, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/pinning/Task$Key;

    .line 642
    .local v1, "key":Lcom/google/android/videos/pinning/Task$Key;
    iget-object v3, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 643
    .local v2, "taskType":I
    add-int/lit8 v3, v2, 0x18

    aget v4, p1, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, p1, v3

    .line 644
    iget-object v3, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->failureCounts:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 645
    add-int/lit8 v4, v2, 0x12

    aget v5, p1, v4

    iget-object v3, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->failureCounts:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/2addr v3, v5

    aput v3, p1, v4

    goto :goto_0

    .line 648
    .end local v1    # "key":Lcom/google/android/videos/pinning/Task$Key;
    .end local v2    # "taskType":I
    :cond_1
    return-void
.end method

.method private updateRunningTaskCounts([I)V
    .locals 6
    .param p1, "pingValues"    # [I

    .prologue
    .line 651
    iget-object v3, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/pinning/Task$Key;

    .line 652
    .local v1, "key":Lcom/google/android/videos/pinning/Task$Key;
    iget-object v3, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/pinning/Task;

    .line 653
    .local v2, "task":Lcom/google/android/videos/pinning/Task;, "Lcom/google/android/videos/pinning/Task<*>;"
    iget v3, v2, Lcom/google/android/videos/pinning/Task;->taskType:I

    add-int/lit8 v3, v3, 0x0

    aget v4, p1, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, p1, v3

    .line 654
    iget-object v3, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->failureCounts:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 655
    iget v3, v2, Lcom/google/android/videos/pinning/Task;->taskType:I

    add-int/lit8 v4, v3, 0x12

    aget v5, p1, v4

    iget-object v3, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->failureCounts:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/2addr v3, v5

    aput v3, p1, v4

    goto :goto_0

    .line 658
    .end local v1    # "key":Lcom/google/android/videos/pinning/Task$Key;
    .end local v2    # "task":Lcom/google/android/videos/pinning/Task;, "Lcom/google/android/videos/pinning/Task<*>;"
    :cond_1
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 12
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v11, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 267
    iget v6, p1, Landroid/os/Message;->what:I

    packed-switch v6, :pswitch_data_0

    .line 315
    :cond_0
    :goto_0
    iget-object v7, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->ticketLock:Ljava/lang/Object;

    monitor-enter v7

    .line 316
    :try_start_0
    iget v6, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->processedTickets:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->processedTickets:I

    iget v10, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->postedTickets:I

    if-ne v6, v10, :cond_2

    iget-boolean v6, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->activeTransfers:Z

    if-nez v6, :cond_2

    move v6, v8

    :goto_1
    iput-boolean v6, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->idle:Z

    .line 317
    iget-boolean v2, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->idle:Z

    .line 318
    .local v2, "idleAfterCurrentTicket":Z
    iget v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->processedTickets:I

    .line 319
    .local v0, "currentProcessedTickets":I
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 323
    if-eqz v2, :cond_1

    .line 324
    const-string v6, "(license_type IS NOT NULL AND license_type != 0)"

    invoke-direct {p0, v6, v11, v11}, Lcom/google/android/videos/pinning/TransfersExecutor;->getTransfersCursor(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 326
    .local v1, "cursor":Landroid/database/Cursor;
    :try_start_1
    iget-object v7, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->listener:Lcom/google/android/videos/pinning/TransfersExecutor$Listener;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-lez v6, :cond_3

    move v6, v8

    :goto_2
    invoke-interface {v7, v0, v6}, Lcom/google/android/videos/pinning/TransfersExecutor$Listener;->onIdle(IZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 328
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 332
    .end local v1    # "cursor":Landroid/database/Cursor;
    :cond_1
    return v8

    .line 270
    .end local v0    # "currentProcessedTickets":I
    .end local v2    # "idleAfterCurrentTicket":Z
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/videos/pinning/TransfersExecutor;->ping()V

    goto :goto_0

    .line 275
    :pswitch_1
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Lcom/google/android/videos/pinning/Task;

    .line 276
    .local v5, "task":Lcom/google/android/videos/pinning/Task;, "Lcom/google/android/videos/pinning/Task<*>;"
    invoke-direct {p0, v5}, Lcom/google/android/videos/pinning/TransfersExecutor;->isCurrent(Lcom/google/android/videos/pinning/Task;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 278
    iget-object v6, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->failureCounts:Ljava/util/Map;

    iget-object v7, v5, Lcom/google/android/videos/pinning/Task;->key:Lcom/google/android/videos/pinning/Task$Key;

    invoke-interface {v6, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    iget-object v6, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->internetConnectionChecker:Lcom/google/android/videos/pinning/InternetConnectionChecker;

    invoke-virtual {v6}, Lcom/google/android/videos/pinning/InternetConnectionChecker;->onSuccess()V

    goto :goto_0

    .line 285
    .end local v5    # "task":Lcom/google/android/videos/pinning/Task;, "Lcom/google/android/videos/pinning/Task<*>;"
    :pswitch_2
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Lcom/google/android/videos/pinning/Task;

    .line 286
    .restart local v5    # "task":Lcom/google/android/videos/pinning/Task;, "Lcom/google/android/videos/pinning/Task<*>;"
    invoke-direct {p0, v5}, Lcom/google/android/videos/pinning/TransfersExecutor;->isCurrent(Lcom/google/android/videos/pinning/Task;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 287
    iget-object v6, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->internetConnectionChecker:Lcom/google/android/videos/pinning/InternetConnectionChecker;

    invoke-virtual {v6}, Lcom/google/android/videos/pinning/InternetConnectionChecker;->onSuccess()V

    .line 288
    invoke-direct {p0, v5}, Lcom/google/android/videos/pinning/TransfersExecutor;->handleTaskCompleted(Lcom/google/android/videos/pinning/Task;)V

    goto :goto_0

    .line 295
    .end local v5    # "task":Lcom/google/android/videos/pinning/Task;, "Lcom/google/android/videos/pinning/Task<*>;"
    :pswitch_3
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Landroid/util/Pair;

    .line 296
    .local v4, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/videos/pinning/Task<*>;Lcom/google/android/videos/pinning/Task$TaskException;>;"
    iget-object v6, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v6, Lcom/google/android/videos/pinning/Task;

    invoke-direct {p0, v6}, Lcom/google/android/videos/pinning/TransfersExecutor;->isCurrent(Lcom/google/android/videos/pinning/Task;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 297
    iget-object v6, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v6, Lcom/google/android/videos/pinning/Task;

    iget-object v7, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v7, Lcom/google/android/videos/pinning/Task$TaskException;

    invoke-direct {p0, v6, v7}, Lcom/google/android/videos/pinning/TransfersExecutor;->handleTaskError(Lcom/google/android/videos/pinning/Task;Lcom/google/android/videos/pinning/Task$TaskException;)V

    goto :goto_0

    .line 303
    .end local v4    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/videos/pinning/Task<*>;Lcom/google/android/videos/pinning/Task$TaskException;>;"
    :pswitch_4
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Lcom/google/android/videos/pinning/Task$Key;

    .line 304
    .local v3, "key":Lcom/google/android/videos/pinning/Task$Key;
    iget-object v6, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    invoke-interface {v6, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 305
    invoke-direct {p0}, Lcom/google/android/videos/pinning/TransfersExecutor;->ping()V

    goto/16 :goto_0

    .end local v3    # "key":Lcom/google/android/videos/pinning/Task$Key;
    :cond_2
    move v6, v9

    .line 316
    goto :goto_1

    .line 319
    :catchall_0
    move-exception v6

    :try_start_2
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v6

    .restart local v0    # "currentProcessedTickets":I
    .restart local v1    # "cursor":Landroid/database/Cursor;
    .restart local v2    # "idleAfterCurrentTicket":Z
    :cond_3
    move v6, v9

    .line 326
    goto :goto_2

    .line 328
    :catchall_1
    move-exception v6

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v6

    .line 267
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onCompleted(Lcom/google/android/videos/pinning/Task;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/pinning/Task",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 962
    .local p1, "task":Lcom/google/android/videos/pinning/Task;, "Lcom/google/android/videos/pinning/Task<*>;"
    const/4 v0, 0x3

    invoke-direct {p0, v0, p1}, Lcom/google/android/videos/pinning/TransfersExecutor;->sendMessage(ILjava/lang/Object;)I

    .line 963
    return-void
.end method

.method public onConnectivityChanged()V
    .locals 0

    .prologue
    .line 337
    invoke-direct {p0}, Lcom/google/android/videos/pinning/TransfersExecutor;->pingUnlessIdle()V

    .line 338
    return-void
.end method

.method public onError(Lcom/google/android/videos/pinning/Task;Lcom/google/android/videos/pinning/Task$TaskException;)V
    .locals 2
    .param p2, "exception"    # Lcom/google/android/videos/pinning/Task$TaskException;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/pinning/Task",
            "<*>;",
            "Lcom/google/android/videos/pinning/Task$TaskException;",
            ")V"
        }
    .end annotation

    .prologue
    .line 967
    .local p1, "task":Lcom/google/android/videos/pinning/Task;, "Lcom/google/android/videos/pinning/Task<*>;"
    const/4 v0, 0x4

    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/pinning/TransfersExecutor;->sendMessage(ILjava/lang/Object;)I

    .line 968
    return-void
.end method

.method public onMediaMountedChanged()V
    .locals 0

    .prologue
    .line 972
    invoke-direct {p0}, Lcom/google/android/videos/pinning/TransfersExecutor;->pingUnlessIdle()V

    .line 973
    return-void
.end method

.method public onPlaybackSessionStatusChanged(Z)V
    .locals 0
    .param p1, "sessionInProgress"    # Z

    .prologue
    .line 977
    invoke-direct {p0}, Lcom/google/android/videos/pinning/TransfersExecutor;->pingUnlessIdle()V

    .line 978
    return-void
.end method

.method public onProgress(Lcom/google/android/videos/pinning/Task;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/pinning/Task",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 957
    .local p1, "task":Lcom/google/android/videos/pinning/Task;, "Lcom/google/android/videos/pinning/Task<*>;"
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lcom/google/android/videos/pinning/TransfersExecutor;->sendMessage(ILjava/lang/Object;)I

    .line 958
    return-void
.end method

.method public onStreamingStatusChanged(Z)V
    .locals 0
    .param p1, "streamingInProgress"    # Z

    .prologue
    .line 982
    invoke-direct {p0}, Lcom/google/android/videos/pinning/TransfersExecutor;->pingUnlessIdle()V

    .line 983
    return-void
.end method

.method public persistDownloadStatus(Lcom/google/android/videos/pinning/DownloadKey;IILjava/lang/Integer;)V
    .locals 3
    .param p1, "key"    # Lcom/google/android/videos/pinning/DownloadKey;
    .param p2, "status"    # I
    .param p3, "statusReason"    # I
    .param p4, "drmErrorCode"    # Ljava/lang/Integer;

    .prologue
    .line 883
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 884
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "pinning_status"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 885
    const-string v1, "pinning_status_reason"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 886
    const-string v1, "pinning_drm_error_code"

    invoke-virtual {v0, v1, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 887
    const/4 v1, 0x2

    if-ne p2, v1, :cond_0

    .line 888
    const-string v1, "pinning_notification_active"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 890
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->database:Lcom/google/android/videos/store/Database;

    invoke-static {v1, p1, v0}, Lcom/google/android/videos/pinning/PinningDbHelper;->updatePinningStateForVideo(Lcom/google/android/videos/store/Database;Lcom/google/android/videos/pinning/DownloadKey;Landroid/content/ContentValues;)V

    .line 891
    return-void
.end method

.method public quit()V
    .locals 5

    .prologue
    .line 217
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->executorWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 218
    const-string v1, "wifiLock held in quit"

    invoke-static {v1}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 219
    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->executorWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto :goto_0

    .line 221
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->preferencesListener:Lcom/google/android/videos/pinning/TransfersExecutor$PreferencesListener;

    invoke-virtual {v1}, Lcom/google/android/videos/pinning/TransfersExecutor$PreferencesListener;->unregister()V

    .line 222
    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->mediaMountedReceiver:Lcom/google/android/videos/utils/MediaMountedReceiver;

    invoke-virtual {v1}, Lcom/google/android/videos/utils/MediaMountedReceiver;->unregister()V

    .line 223
    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->streamingStatusNotifier:Lcom/google/android/videos/player/PlaybackStatusNotifier;

    invoke-virtual {v1, p0}, Lcom/google/android/videos/player/PlaybackStatusNotifier;->removeListener(Lcom/google/android/videos/player/PlaybackStatusNotifier$Listener;)V

    .line 224
    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->internetConnectionChecker:Lcom/google/android/videos/pinning/InternetConnectionChecker;

    invoke-virtual {v1}, Lcom/google/android/videos/pinning/InternetConnectionChecker;->quit()V

    .line 226
    iget-object v2, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->ticketLock:Ljava/lang/Object;

    monitor-enter v2

    .line 227
    :try_start_0
    iget v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->postedTickets:I

    iget v3, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->processedTickets:I

    sub-int v0, v1, v3

    .line 228
    .local v0, "pending":I
    if-nez v0, :cond_1

    const/4 v1, 0x1

    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pendingMessages = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/videos/utils/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 229
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 230
    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor;->backgroundThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->quit()Z

    .line 231
    return-void

    .line 228
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 229
    .end local v0    # "pending":I
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public requestPing()I
    .locals 1

    .prologue
    .line 206
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/videos/pinning/TransfersExecutor;->sendMessage(I)I

    move-result v0

    return v0
.end method
