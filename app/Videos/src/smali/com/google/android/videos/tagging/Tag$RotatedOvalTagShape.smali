.class Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;
.super Ljava/lang/Object;
.source "Tag.java"

# interfaces
.implements Lcom/google/android/videos/tagging/Tag$TagShape;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/Tag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RotatedOvalTagShape"
.end annotation


# instance fields
.field public angle:F

.field public centerX:F

.field public centerY:F

.field public ovalA:F

.field public ovalB:F


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 262
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/tagging/Tag$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/tagging/Tag$1;

    .prologue
    .line 262
    invoke-direct {p0}, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;-><init>()V

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 274
    iget v1, p0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->angle:F

    const/high16 v2, -0x3fc00000    # -3.0f

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->angle:F

    const/high16 v2, 0x40400000    # 3.0f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    :cond_0
    const/4 v0, 0x1

    .line 275
    .local v0, "shouldRotate":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 276
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 277
    iget v1, p0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->angle:F

    neg-float v1, v1

    iget v2, p0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->centerX:F

    iget v3, p0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->centerY:F

    invoke-virtual {p1, v1, v2, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 279
    :cond_1
    new-instance v1, Landroid/graphics/RectF;

    iget v2, p0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->centerX:F

    iget v3, p0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->ovalA:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->centerY:F

    iget v4, p0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->ovalB:F

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->centerX:F

    iget v5, p0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->ovalA:F

    add-float/2addr v4, v5

    iget v5, p0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->centerY:F

    iget v6, p0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->ovalB:F

    add-float/2addr v5, v6

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {p1, v1, p2}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 281
    if-eqz v0, :cond_2

    .line 282
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 284
    :cond_2
    return-void

    .line 274
    .end local v0    # "shouldRotate":Z
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBoundingBox(Landroid/graphics/RectF;)V
    .locals 3
    .param p1, "box"    # Landroid/graphics/RectF;

    .prologue
    .line 313
    iget v1, p0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->ovalA:F

    iget v2, p0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->ovalB:F

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 314
    .local v0, "maxRadius":F
    iget v1, p0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->centerX:F

    sub-float/2addr v1, v0

    iput v1, p1, Landroid/graphics/RectF;->left:F

    .line 315
    iget v1, p0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->centerY:F

    sub-float/2addr v1, v0

    iput v1, p1, Landroid/graphics/RectF;->top:F

    .line 316
    iget v1, p0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->centerX:F

    add-float/2addr v1, v0

    iput v1, p1, Landroid/graphics/RectF;->right:F

    .line 317
    iget v1, p0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->centerY:F

    add-float/2addr v1, v0

    iput v1, p1, Landroid/graphics/RectF;->bottom:F

    .line 318
    return-void
.end method

.method public hitTest(FFF)Z
    .locals 24
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "tolerance"    # F

    .prologue
    .line 288
    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->centerX:F

    sub-float v3, p1, v15

    .line 289
    .local v3, "dx":F
    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->centerY:F

    sub-float v10, p2, v15

    .line 290
    .local v10, "dy":F
    mul-float v15, v3, v3

    mul-float v18, v10, v10

    add-float v15, v15, v18

    float-to-double v0, v15

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v18

    move/from16 v0, p3

    float-to-double v0, v0

    move-wide/from16 v20, v0

    sub-double v8, v18, v20

    .line 292
    .local v8, "distance":D
    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->ovalA:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->ovalB:F

    move/from16 v18, v0

    cmpl-float v15, v15, v18

    if-ltz v15, :cond_0

    const/4 v2, 0x1

    .line 293
    .local v2, "aIsGreater":Z
    :goto_0
    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->ovalA:F

    :goto_1
    float-to-double v0, v15

    move-wide/from16 v18, v0

    cmpl-double v15, v8, v18

    if-lez v15, :cond_2

    .line 294
    const/4 v15, 0x0

    .line 308
    :goto_2
    return v15

    .line 292
    .end local v2    # "aIsGreater":Z
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 293
    .restart local v2    # "aIsGreater":Z
    :cond_1
    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->ovalB:F

    goto :goto_1

    .line 295
    :cond_2
    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->ovalB:F

    :goto_3
    float-to-double v0, v15

    move-wide/from16 v18, v0

    cmpg-double v15, v8, v18

    if-gtz v15, :cond_4

    .line 296
    const/4 v15, 0x1

    goto :goto_2

    .line 295
    :cond_3
    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->ovalA:F

    goto :goto_3

    .line 301
    :cond_4
    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->centerX:F

    sub-float v11, p1, v15

    .line 302
    .local v11, "relX":F
    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->centerY:F

    sub-float v14, v15, p2

    .line 303
    .local v14, "relY":F
    float-to-double v0, v14

    move-wide/from16 v18, v0

    float-to-double v0, v11

    move-wide/from16 v20, v0

    invoke-static/range {v18 .. v21}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v18

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->angle:F

    float-to-double v0, v15

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v20

    sub-double v16, v18, v20

    .line 305
    .local v16, "theta":D
    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->ovalA:F

    float-to-double v0, v15

    move-wide/from16 v18, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sin(D)D

    move-result-wide v20

    mul-double v4, v18, v20

    .line 306
    .local v4, "aSinTheta":D
    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->ovalB:F

    float-to-double v0, v15

    move-wide/from16 v18, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->cos(D)D

    move-result-wide v20

    mul-double v6, v18, v20

    .line 307
    .local v6, "bCosTheta":D
    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->ovalA:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;->ovalB:F

    move/from16 v18, v0

    mul-float v15, v15, v18

    float-to-double v0, v15

    move-wide/from16 v18, v0

    mul-double v20, v4, v4

    mul-double v22, v6, v6

    add-double v20, v20, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v20

    div-double v12, v18, v20

    .line 308
    .local v12, "radius":D
    cmpg-double v15, v8, v12

    if-gtz v15, :cond_5

    const/4 v15, 0x1

    goto :goto_2

    :cond_5
    const/4 v15, 0x0

    goto :goto_2
.end method
