.class public abstract Lcom/google/android/videos/welcome/PromoWelcome;
.super Lcom/google/android/videos/welcome/Welcome;
.source "PromoWelcome.java"

# interfaces
.implements Lcom/google/android/videos/welcome/PromoDismisser$Listener;


# instance fields
.field private final promoDismisser:Lcom/google/android/videos/welcome/PromoDismisser;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/SharedPreferences;)V
    .locals 1
    .param p1, "verticalId"    # Ljava/lang/String;
    .param p2, "promoId"    # Ljava/lang/String;
    .param p3, "preferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 17
    invoke-direct {p0, p2}, Lcom/google/android/videos/welcome/Welcome;-><init>(Ljava/lang/String;)V

    .line 18
    new-instance v0, Lcom/google/android/videos/welcome/PromoDismisser;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/videos/welcome/PromoDismisser;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lcom/google/android/videos/welcome/PromoWelcome;->promoDismisser:Lcom/google/android/videos/welcome/PromoDismisser;

    .line 19
    return-void
.end method


# virtual methods
.method public final isUserDismissable()Z
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x1

    return v0
.end method

.method protected final markDismissed()V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/videos/welcome/PromoWelcome;->promoDismisser:Lcom/google/android/videos/welcome/PromoDismisser;

    invoke-virtual {v0}, Lcom/google/android/videos/welcome/PromoDismisser;->dismissPromo()V

    .line 66
    return-void
.end method

.method public onPromoStateChanged()V
    .locals 0

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/google/android/videos/welcome/PromoWelcome;->notifyEligibilityChanged()V

    .line 48
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/videos/welcome/PromoWelcome;->promoDismisser:Lcom/google/android/videos/welcome/PromoDismisser;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/welcome/PromoDismisser;->setRegisteredListener(Lcom/google/android/videos/welcome/PromoDismisser$Listener;)V

    .line 36
    invoke-super {p0}, Lcom/google/android/videos/welcome/Welcome;->onStart()V

    .line 37
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 41
    invoke-super {p0}, Lcom/google/android/videos/welcome/Welcome;->onStop()V

    .line 42
    iget-object v0, p0, Lcom/google/android/videos/welcome/PromoWelcome;->promoDismisser:Lcom/google/android/videos/welcome/PromoDismisser;

    invoke-virtual {v0}, Lcom/google/android/videos/welcome/PromoDismisser;->clearRegisteredListener()V

    .line 43
    return-void
.end method

.method public final prepareIfEligible(Ljava/lang/String;ZZ)Z
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "contentInVertical"    # Z
    .param p3, "downloadedOnly"    # Z

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/videos/welcome/PromoWelcome;->promoDismisser:Lcom/google/android/videos/welcome/PromoDismisser;

    invoke-virtual {v0}, Lcom/google/android/videos/welcome/PromoDismisser;->wasPromoRecentlyDismissedInVertical()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/welcome/PromoWelcome;->promoDismisser:Lcom/google/android/videos/welcome/PromoDismisser;

    invoke-virtual {v0}, Lcom/google/android/videos/welcome/PromoDismisser;->isPromoDismissed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/videos/welcome/PromoWelcome;->preparePromoIfEligible(Ljava/lang/String;ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract preparePromoIfEligible(Ljava/lang/String;ZZ)Z
.end method
