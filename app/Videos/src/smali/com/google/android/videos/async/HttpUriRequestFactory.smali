.class public Lcom/google/android/videos/async/HttpUriRequestFactory;
.super Ljava/lang/Object;
.source "HttpUriRequestFactory.java"


# direct methods
.method public static createDelete(Ljava/lang/String;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 1
    .param p0, "uri"    # Ljava/lang/String;

    .prologue
    .line 58
    new-instance v0, Lorg/apache/http/client/methods/HttpDelete;

    invoke-direct {v0, p0}, Lorg/apache/http/client/methods/HttpDelete;-><init>(Ljava/lang/String;)V

    .line 59
    .local v0, "request":Lorg/apache/http/client/methods/HttpUriRequest;
    invoke-static {v0}, Lcom/google/android/videos/async/HttpUriRequestFactory;->setDefaultHeaders(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    .line 60
    return-object v0
.end method

.method public static createGet(Landroid/net/Uri;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 1
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 44
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/async/HttpUriRequestFactory;->createGet(Ljava/lang/String;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public static createGet(Ljava/lang/String;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 1
    .param p0, "uri"    # Ljava/lang/String;

    .prologue
    .line 48
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0, p0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 49
    .local v0, "request":Lorg/apache/http/client/methods/HttpUriRequest;
    invoke-static {v0}, Lcom/google/android/videos/async/HttpUriRequestFactory;->setDefaultHeaders(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    .line 50
    return-object v0
.end method

.method public static createPost(Landroid/net/Uri;[B)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 1
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "content"    # [B

    .prologue
    .line 22
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/videos/async/HttpUriRequestFactory;->createPost(Ljava/lang/String;[B)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public static createPost(Ljava/lang/String;[B)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 1
    .param p0, "uri"    # Ljava/lang/String;
    .param p1, "content"    # [B

    .prologue
    .line 26
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v0, p0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 27
    .local v0, "request":Lorg/apache/http/client/methods/HttpUriRequest;
    invoke-static {v0}, Lcom/google/android/videos/async/HttpUriRequestFactory;->setDefaultHeaders(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    .line 28
    invoke-static {v0, p1}, Lcom/google/android/videos/async/HttpUriRequestFactory;->setContent(Lorg/apache/http/client/methods/HttpUriRequest;[B)Lorg/apache/http/client/methods/HttpUriRequest;

    .line 29
    return-object v0
.end method

.method public static createPut(Ljava/lang/String;[B)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 1
    .param p0, "uri"    # Ljava/lang/String;
    .param p1, "content"    # [B

    .prologue
    .line 37
    new-instance v0, Lorg/apache/http/client/methods/HttpPut;

    invoke-direct {v0, p0}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    .line 38
    .local v0, "request":Lorg/apache/http/client/methods/HttpUriRequest;
    invoke-static {v0}, Lcom/google/android/videos/async/HttpUriRequestFactory;->setDefaultHeaders(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    .line 39
    invoke-static {v0, p1}, Lcom/google/android/videos/async/HttpUriRequestFactory;->setContent(Lorg/apache/http/client/methods/HttpUriRequest;[B)Lorg/apache/http/client/methods/HttpUriRequest;

    .line 40
    return-object v0
.end method

.method private static setContent(Lorg/apache/http/client/methods/HttpUriRequest;[B)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 2
    .param p0, "httpRequest"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p1, "content"    # [B

    .prologue
    .line 69
    if-eqz p1, :cond_0

    .line 70
    new-instance v0, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-direct {v0, p1}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    .line 71
    .local v0, "entity":Lorg/apache/http/entity/ByteArrayEntity;
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/apache/http/entity/ByteArrayEntity;->setContentType(Ljava/lang/String;)V

    move-object v1, p0

    .line 74
    check-cast v1, Lorg/apache/http/client/methods/HttpEntityEnclosingRequestBase;

    invoke-virtual {v1, v0}, Lorg/apache/http/client/methods/HttpEntityEnclosingRequestBase;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 76
    .end local v0    # "entity":Lorg/apache/http/entity/ByteArrayEntity;
    :cond_0
    return-object p0
.end method

.method private static setDefaultHeaders(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 2
    .param p0, "httpRequest"    # Lorg/apache/http/client/methods/HttpUriRequest;

    .prologue
    .line 64
    const-string v0, "Accept-Encoding"

    const-string v1, "gzip"

    invoke-interface {p0, v0, v1}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    return-object p0
.end method
