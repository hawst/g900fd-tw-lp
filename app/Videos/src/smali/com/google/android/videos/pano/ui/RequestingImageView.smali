.class public final Lcom/google/android/videos/pano/ui/RequestingImageView;
.super Landroid/widget/ImageView;
.source "RequestingImageView.java"


# instance fields
.field private final handler:Landroid/os/Handler;

.field private taskStatus:Lcom/google/android/videos/async/TaskControl;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/RequestingImageView;->handler:Landroid/os/Handler;

    .line 24
    return-void
.end method


# virtual methods
.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/RequestingImageView;->taskStatus:Lcom/google/android/videos/async/TaskControl;

    invoke-static {v0}, Lcom/google/android/videos/async/TaskControl;->cancel(Lcom/google/android/videos/async/TaskControl;)V

    .line 29
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 30
    return-void
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/RequestingImageView;->taskStatus:Lcom/google/android/videos/async/TaskControl;

    invoke-static {v0}, Lcom/google/android/videos/async/TaskControl;->cancel(Lcom/google/android/videos/async/TaskControl;)V

    .line 35
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 36
    return-void
.end method

.method public setImageResource(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/RequestingImageView;->taskStatus:Lcom/google/android/videos/async/TaskControl;

    invoke-static {v0}, Lcom/google/android/videos/async/TaskControl;->cancel(Lcom/google/android/videos/async/TaskControl;)V

    .line 41
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 42
    return-void
.end method

.method public setImageUri(Landroid/net/Uri;Lcom/google/android/videos/async/Requester;)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    .local p2, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/async/ControllableRequest<Landroid/net/Uri;>;Landroid/graphics/Bitmap;>;"
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/RequestingImageView;->taskStatus:Lcom/google/android/videos/async/TaskControl;

    invoke-static {v0}, Lcom/google/android/videos/async/TaskControl;->cancel(Lcom/google/android/videos/async/TaskControl;)V

    .line 47
    new-instance v0, Lcom/google/android/videos/async/TaskControl;

    invoke-direct {v0}, Lcom/google/android/videos/async/TaskControl;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/RequestingImageView;->taskStatus:Lcom/google/android/videos/async/TaskControl;

    .line 48
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/RequestingImageView;->taskStatus:Lcom/google/android/videos/async/TaskControl;

    invoke-static {p1, v0}, Lcom/google/android/videos/async/ControllableRequest;->create(Ljava/lang/Object;Lcom/google/android/videos/async/TaskStatus;)Lcom/google/android/videos/async/ControllableRequest;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/pano/ui/RequestingImageView;->handler:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/videos/pano/ui/RequestingImageView$1;

    invoke-direct {v2, p0}, Lcom/google/android/videos/pano/ui/RequestingImageView$1;-><init>(Lcom/google/android/videos/pano/ui/RequestingImageView;)V

    invoke-static {v1, v2}, Lcom/google/android/videos/async/HandlerCallback;->create(Landroid/os/Handler;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/HandlerCallback;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 59
    return-void
.end method
