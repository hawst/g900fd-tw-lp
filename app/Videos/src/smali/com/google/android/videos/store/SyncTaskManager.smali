.class public Lcom/google/android/videos/store/SyncTaskManager;
.super Ljava/lang/Object;
.source "SyncTaskManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/store/SyncTaskManager$SharedStates;,
        Lcom/google/android/videos/store/SyncTaskManager$SyncTask;,
        Lcom/google/android/videos/store/SyncTaskManager$DataException;,
        Lcom/google/android/videos/store/SyncTaskManager$SyncException;
    }
.end annotation


# instance fields
.field private final executor:Ljava/util/concurrent/Executor;

.field private final nextTicket:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public constructor <init>()V
    .locals 9

    .prologue
    const/4 v2, 0x4

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const-wide/16 v4, 0x1e

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>()V

    new-instance v8, Lcom/google/android/videos/utils/PriorityThreadFactory;

    const-string v0, "sync"

    const/16 v3, 0xa

    invoke-direct {v8, v0, v3}, Lcom/google/android/videos/utils/PriorityThreadFactory;-><init>(Ljava/lang/String;I)V

    move v3, v2

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    iput-object v1, p0, Lcom/google/android/videos/store/SyncTaskManager;->executor:Ljava/util/concurrent/Executor;

    .line 82
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/store/SyncTaskManager;->nextTicket:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 83
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/store/SyncTaskManager;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/SyncTaskManager;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/videos/store/SyncTaskManager;->nextTicket:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/store/SyncTaskManager;)Ljava/util/concurrent/Executor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/SyncTaskManager;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/videos/store/SyncTaskManager;->executor:Ljava/util/concurrent/Executor;

    return-object v0
.end method
