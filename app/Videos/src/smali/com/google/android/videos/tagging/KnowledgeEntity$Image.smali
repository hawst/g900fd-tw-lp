.class public final Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
.super Ljava/lang/Object;
.source "KnowledgeEntity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/KnowledgeEntity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Image"
.end annotation


# instance fields
.field private final cropRegions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[I>;"
        }
    .end annotation
.end field

.field public final localImageId:Ljava/lang/String;

.field public final originalAspectRatio:F

.field public final source:Ljava/lang/String;

.field public final url:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;FLjava/util/Collection;)V
    .locals 1
    .param p1, "localImageId"    # Ljava/lang/String;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "source"    # Ljava/lang/String;
    .param p4, "originalAspectRatio"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "F",
            "Ljava/util/Collection",
            "<[I>;)V"
        }
    .end annotation

    .prologue
    .line 113
    .local p5, "cropRegions":Ljava/util/Collection;, "Ljava/util/Collection<[I>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    iput-object p1, p0, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;->localImageId:Ljava/lang/String;

    .line 115
    iput-object p2, p0, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;->url:Ljava/lang/String;

    .line 116
    iput-object p3, p0, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;->source:Ljava/lang/String;

    .line 117
    iput p4, p0, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;->originalAspectRatio:F

    .line 118
    invoke-static {p5}, Lcom/google/android/videos/utils/CollectionUtil;->immutableCopyOf(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;->cropRegions:Ljava/util/List;

    .line 119
    return-void
.end method


# virtual methods
.method public getCropRegion(ILandroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 5
    .param p1, "index"    # I
    .param p2, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 148
    iget-object v1, p0, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;->cropRegions:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    .line 149
    .local v0, "data":[I
    const/4 v1, 0x0

    aget v1, v0, v1

    const/4 v2, 0x1

    aget v2, v0, v2

    const/4 v3, 0x2

    aget v3, v0, v3

    const/4 v4, 0x3

    aget v4, v0, v4

    invoke-virtual {p2, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 150
    return-object p2
.end method

.method public getCropRegion(ILandroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 5
    .param p1, "index"    # I
    .param p2, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 159
    iget-object v1, p0, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;->cropRegions:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    .line 160
    .local v0, "data":[I
    const/4 v1, 0x0

    aget v1, v0, v1

    int-to-float v1, v1

    const/4 v2, 0x1

    aget v2, v0, v2

    int-to-float v2, v2

    const/4 v3, 0x2

    aget v3, v0, v3

    int-to-float v3, v3

    const/4 v4, 0x3

    aget v4, v0, v4

    int-to-float v4, v4

    invoke-virtual {p2, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 161
    return-object p2
.end method

.method public getSquareCropRegionIndex()I
    .locals 6

    .prologue
    const/4 v2, -0x1

    .line 126
    iget-object v3, p0, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;->cropRegions:Ljava/util/List;

    if-nez v3, :cond_1

    move v1, v2

    .line 135
    :cond_0
    :goto_0
    return v1

    .line 129
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;->cropRegions:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 130
    iget-object v3, p0, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;->cropRegions:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    .line 131
    .local v0, "data":[I
    const/4 v3, 0x2

    aget v3, v0, v3

    const/4 v4, 0x0

    aget v4, v0, v4

    sub-int/2addr v3, v4

    const/4 v4, 0x3

    aget v4, v0, v4

    const/4 v5, 0x1

    aget v5, v0, v5

    sub-int/2addr v4, v5

    if-eq v3, v4, :cond_0

    .line 129
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .end local v0    # "data":[I
    :cond_2
    move v1, v2

    .line 135
    goto :goto_0
.end method
