.class Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder$1;
.super Ljava/lang/Object;
.source "SecondScreenEntitiesAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;-><init>(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

.field final synthetic val$this$0:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;)V
    .locals 0

    .prologue
    .line 239
    iput-object p1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder$1;->this$1:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    iput-object p2, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder$1;->val$this$0:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder$1;->this$1:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    iget-object v0, v0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->this$0:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;

    # getter for: Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->onTagClickListener:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$OnEntityClickListener;
    invoke-static {v0}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->access$000(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;)Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$OnEntityClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder$1;->this$1:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    iget-object v0, v0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->this$0:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;

    # getter for: Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->onTagClickListener:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$OnEntityClickListener;
    invoke-static {v0}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->access$000(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;)Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$OnEntityClickListener;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/tagging/KnowledgeEntity;

    iget-object v2, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder$1;->this$1:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    iget-object v2, v2, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->imageFrame:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder$1;->this$1:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    iget-object v3, v3, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->imageView:Landroid/view/View;

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$OnEntityClickListener;->onEntityClick(Lcom/google/android/videos/tagging/KnowledgeEntity;Landroid/view/ViewGroup;Landroid/view/View;)V

    .line 246
    :cond_0
    return-void
.end method
