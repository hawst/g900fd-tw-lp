.class Lcom/google/android/videos/store/PosterStore$2$1;
.super Ljava/lang/Object;
.source "PosterStore.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/store/PosterStore$2;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/videos/store/PosterStore$2;

.field final synthetic val$byteArray:Lcom/google/android/videos/utils/ByteArray;


# direct methods
.method constructor <init>(Lcom/google/android/videos/store/PosterStore$2;Lcom/google/android/videos/utils/ByteArray;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/google/android/videos/store/PosterStore$2$1;->this$1:Lcom/google/android/videos/store/PosterStore$2;

    iput-object p2, p0, Lcom/google/android/videos/store/PosterStore$2$1;->val$byteArray:Lcom/google/android/videos/utils/ByteArray;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 139
    :try_start_0
    iget-object v2, p0, Lcom/google/android/videos/store/PosterStore$2$1;->this$1:Lcom/google/android/videos/store/PosterStore$2;

    iget-object v2, v2, Lcom/google/android/videos/store/PosterStore$2;->val$bitmapConverter:Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;

    iget-object v3, p0, Lcom/google/android/videos/store/PosterStore$2$1;->val$byteArray:Lcom/google/android/videos/utils/ByteArray;

    invoke-virtual {v2, v3}, Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;->convertResponse(Lcom/google/android/videos/utils/ByteArray;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 140
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/google/android/videos/store/PosterStore$2$1;->this$1:Lcom/google/android/videos/store/PosterStore$2;

    iget-object v2, v2, Lcom/google/android/videos/store/PosterStore$2;->this$0:Lcom/google/android/videos/store/PosterStore;

    # getter for: Lcom/google/android/videos/store/PosterStore;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;
    invoke-static {v2}, Lcom/google/android/videos/store/PosterStore;->access$000(Lcom/google/android/videos/store/PosterStore;)Lcom/google/android/videos/utils/ByteArrayPool;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/store/PosterStore$2$1;->val$byteArray:Lcom/google/android/videos/utils/ByteArray;

    invoke-virtual {v2, v3}, Lcom/google/android/videos/utils/ByteArrayPool;->returnBuf(Lcom/google/android/videos/utils/ByteArray;)V

    .line 141
    iget-object v2, p0, Lcom/google/android/videos/store/PosterStore$2$1;->this$1:Lcom/google/android/videos/store/PosterStore$2;

    iget-object v2, v2, Lcom/google/android/videos/store/PosterStore$2;->this$0:Lcom/google/android/videos/store/PosterStore;

    # getter for: Lcom/google/android/videos/store/PosterStore;->bitmapCache:Lcom/google/android/videos/bitmap/BitmapLruCache;
    invoke-static {v2}, Lcom/google/android/videos/store/PosterStore;->access$100(Lcom/google/android/videos/store/PosterStore;)Lcom/google/android/videos/bitmap/BitmapLruCache;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/store/PosterStore$2$1;->this$1:Lcom/google/android/videos/store/PosterStore$2;

    iget-object v3, v3, Lcom/google/android/videos/store/PosterStore$2;->val$bitmapCacheKey:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lcom/google/android/videos/bitmap/BitmapLruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    iget-object v2, p0, Lcom/google/android/videos/store/PosterStore$2$1;->this$1:Lcom/google/android/videos/store/PosterStore$2;

    iget-object v2, v2, Lcom/google/android/videos/store/PosterStore$2;->val$callback:Lcom/google/android/videos/async/Callback;

    iget-object v3, p0, Lcom/google/android/videos/store/PosterStore$2$1;->this$1:Lcom/google/android/videos/store/PosterStore$2;

    iget-object v3, v3, Lcom/google/android/videos/store/PosterStore$2;->val$fileName:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/google/android/videos/converter/ConverterException; {:try_start_0 .. :try_end_0} :catch_0

    .line 148
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-void

    .line 143
    :catch_0
    move-exception v1

    .line 146
    .local v1, "e":Lcom/google/android/videos/converter/ConverterException;
    iget-object v2, p0, Lcom/google/android/videos/store/PosterStore$2$1;->this$1:Lcom/google/android/videos/store/PosterStore$2;

    iget-object v2, v2, Lcom/google/android/videos/store/PosterStore$2;->val$callback:Lcom/google/android/videos/async/Callback;

    iget-object v3, p0, Lcom/google/android/videos/store/PosterStore$2$1;->this$1:Lcom/google/android/videos/store/PosterStore$2;

    iget-object v3, v3, Lcom/google/android/videos/store/PosterStore$2;->val$fileName:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method
