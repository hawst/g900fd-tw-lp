.class Lcom/google/android/videos/player/CaptionPreferences$V21;
.super Lcom/google/android/videos/player/CaptionPreferences;
.source "CaptionPreferences.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/CaptionPreferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "V21"
.end annotation


# instance fields
.field private final captioningManager:Landroid/view/accessibility/CaptioningManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 130
    invoke-direct {p0}, Lcom/google/android/videos/player/CaptionPreferences;-><init>()V

    .line 131
    const-string v0, "captioning"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/CaptioningManager;

    iput-object v0, p0, Lcom/google/android/videos/player/CaptionPreferences$V21;->captioningManager:Landroid/view/accessibility/CaptioningManager;

    .line 132
    return-void
.end method


# virtual methods
.method public getCaptionStyle()Lcom/google/android/exoplayer/text/CaptionStyleCompat;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/videos/player/CaptionPreferences$V21;->captioningManager:Landroid/view/accessibility/CaptioningManager;

    invoke-virtual {v0}, Landroid/view/accessibility/CaptioningManager;->getUserStyle()Landroid/view/accessibility/CaptioningManager$CaptionStyle;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/exoplayer/text/CaptionStyleCompat;->createFromCaptionStyle(Landroid/view/accessibility/CaptioningManager$CaptionStyle;)Lcom/google/android/exoplayer/text/CaptionStyleCompat;

    move-result-object v0

    return-object v0
.end method

.method public getFontScale()F
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/videos/player/CaptionPreferences$V21;->captioningManager:Landroid/view/accessibility/CaptioningManager;

    invoke-virtual {v0}, Landroid/view/accessibility/CaptioningManager;->getFontScale()F

    move-result v0

    return v0
.end method

.method public getLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/videos/player/CaptionPreferences$V21;->captioningManager:Landroid/view/accessibility/CaptioningManager;

    invoke-virtual {v0}, Landroid/view/accessibility/CaptioningManager;->getLocale()Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/videos/player/CaptionPreferences$V21;->captioningManager:Landroid/view/accessibility/CaptioningManager;

    invoke-virtual {v0}, Landroid/view/accessibility/CaptioningManager;->isEnabled()Z

    move-result v0

    return v0
.end method
