.class public Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;
.super Landroid/app/Service;
.source "NotificationTransportControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/remote/NotificationTransportControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ListenerService"
.end annotation


# static fields
.field private static final ACTION_PREFIX:Ljava/lang/String;

.field private static final ACTION_VERBS:[Ljava/lang/String;


# instance fields
.field private notificationTransportControl:Lcom/google/android/videos/remote/NotificationTransportControl;

.field private pendingIntents:[Landroid/app/PendingIntent;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 205
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->ACTION_PREFIX:Ljava/lang/String;

    .line 221
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "PLAY"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "PAUSE"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "SEEK"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "MUTE"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "UNMUTE"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "DISCONNECT"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "DISMISS"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "SELECT"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->ACTION_VERBS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 203
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public getPendingIntent(I)Landroid/app/PendingIntent;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->pendingIntents:[Landroid/app/PendingIntent;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 234
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 6

    .prologue
    .line 239
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 241
    const-string v3, "Remote notification service started"

    invoke-static {v3}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 243
    sget-object v3, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->ACTION_VERBS:[Ljava/lang/String;

    array-length v0, v3

    .line 244
    .local v0, "actionCount":I
    new-array v3, v0, [Landroid/app/PendingIntent;

    iput-object v3, p0, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->pendingIntents:[Landroid/app/PendingIntent;

    .line 245
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 246
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 247
    .local v2, "intent":Landroid/content/Intent;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->ACTION_PREFIX:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->ACTION_VERBS:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 248
    iget-object v3, p0, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->pendingIntents:[Landroid/app/PendingIntent;

    const/4 v4, 0x0

    const/high16 v5, 0x8000000

    invoke-static {p0, v4, v2, v5}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    aput-object v4, v3, v1

    .line 245
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 252
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_0
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/videos/VideosGlobals;->getRemoteTracker()Lcom/google/android/videos/remote/RemoteTracker;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/videos/remote/RemoteTracker;->getNotificationTransportControl()Lcom/google/android/videos/remote/NotificationTransportControl;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->notificationTransportControl:Lcom/google/android/videos/remote/NotificationTransportControl;

    .line 254
    iget-object v3, p0, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->notificationTransportControl:Lcom/google/android/videos/remote/NotificationTransportControl;

    invoke-virtual {v3, p0}, Lcom/google/android/videos/remote/NotificationTransportControl;->attachService(Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;)V

    .line 255
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 307
    const-string v0, "Remote notification service stopped"

    invoke-static {v0}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 308
    iget-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->notificationTransportControl:Lcom/google/android/videos/remote/NotificationTransportControl;

    invoke-virtual {v0}, Lcom/google/android/videos/remote/NotificationTransportControl;->detachService()V

    .line 309
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 310
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    .line 259
    if-eqz p1, :cond_7

    .line 260
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 261
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_7

    sget-object v5, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->ACTION_PREFIX:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 262
    iget-object v5, p0, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->notificationTransportControl:Lcom/google/android/videos/remote/NotificationTransportControl;

    invoke-virtual {v5}, Lcom/google/android/videos/remote/NotificationTransportControl;->getListeners()Ljava/util/List;

    move-result-object v3

    .line 265
    .local v3, "listeners":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/remote/TransportControl$Listener;>;"
    sget-object v5, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->ACTION_PREFIX:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 266
    sget-object v5, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->ACTION_VERBS:[Ljava/lang/String;

    aget-object v5, v5, v7

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 267
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/remote/TransportControl$Listener;

    .line 268
    .local v2, "listener":Lcom/google/android/videos/remote/TransportControl$Listener;
    invoke-interface {v2}, Lcom/google/android/videos/remote/TransportControl$Listener;->onPlay()V

    goto :goto_0

    .line 270
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "listener":Lcom/google/android/videos/remote/TransportControl$Listener;
    :cond_0
    sget-object v5, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->ACTION_VERBS:[Ljava/lang/String;

    const/4 v6, 0x1

    aget-object v5, v5, v6

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 271
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/remote/TransportControl$Listener;

    .line 272
    .restart local v2    # "listener":Lcom/google/android/videos/remote/TransportControl$Listener;
    invoke-interface {v2}, Lcom/google/android/videos/remote/TransportControl$Listener;->onPause()V

    goto :goto_1

    .line 274
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "listener":Lcom/google/android/videos/remote/TransportControl$Listener;
    :cond_1
    sget-object v5, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->ACTION_VERBS:[Ljava/lang/String;

    aget-object v5, v5, v8

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 275
    const-string v5, "SEEK_POSITION"

    invoke-virtual {p1, v5, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 276
    .local v4, "seekPosition":I
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/remote/TransportControl$Listener;

    .line 277
    .restart local v2    # "listener":Lcom/google/android/videos/remote/TransportControl$Listener;
    invoke-interface {v2, v4}, Lcom/google/android/videos/remote/TransportControl$Listener;->onSeek(I)V

    goto :goto_2

    .line 279
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "listener":Lcom/google/android/videos/remote/TransportControl$Listener;
    .end local v4    # "seekPosition":I
    :cond_2
    sget-object v5, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->ACTION_VERBS:[Ljava/lang/String;

    const/4 v6, 0x3

    aget-object v5, v5, v6

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 280
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/remote/TransportControl$Listener;

    .line 281
    .restart local v2    # "listener":Lcom/google/android/videos/remote/TransportControl$Listener;
    invoke-interface {v2}, Lcom/google/android/videos/remote/TransportControl$Listener;->onMute()V

    goto :goto_3

    .line 283
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "listener":Lcom/google/android/videos/remote/TransportControl$Listener;
    :cond_3
    sget-object v5, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->ACTION_VERBS:[Ljava/lang/String;

    const/4 v6, 0x4

    aget-object v5, v5, v6

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 284
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/remote/TransportControl$Listener;

    .line 285
    .restart local v2    # "listener":Lcom/google/android/videos/remote/TransportControl$Listener;
    invoke-interface {v2}, Lcom/google/android/videos/remote/TransportControl$Listener;->onUnmute()V

    goto :goto_4

    .line 287
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "listener":Lcom/google/android/videos/remote/TransportControl$Listener;
    :cond_4
    sget-object v5, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->ACTION_VERBS:[Ljava/lang/String;

    const/4 v6, 0x5

    aget-object v5, v5, v6

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 288
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/remote/TransportControl$Listener;

    .line 289
    .restart local v2    # "listener":Lcom/google/android/videos/remote/TransportControl$Listener;
    invoke-interface {v2}, Lcom/google/android/videos/remote/TransportControl$Listener;->onDisconnect()V

    goto :goto_5

    .line 291
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "listener":Lcom/google/android/videos/remote/TransportControl$Listener;
    :cond_5
    sget-object v5, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->ACTION_VERBS:[Ljava/lang/String;

    const/4 v6, 0x6

    aget-object v5, v5, v6

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 292
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/remote/TransportControl$Listener;

    .line 293
    .restart local v2    # "listener":Lcom/google/android/videos/remote/TransportControl$Listener;
    invoke-interface {v2}, Lcom/google/android/videos/remote/TransportControl$Listener;->onDismiss()V

    goto :goto_6

    .line 295
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "listener":Lcom/google/android/videos/remote/TransportControl$Listener;
    :cond_6
    sget-object v5, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->ACTION_VERBS:[Ljava/lang/String;

    const/4 v6, 0x7

    aget-object v5, v5, v6

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 296
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/remote/TransportControl$Listener;

    .line 297
    .restart local v2    # "listener":Lcom/google/android/videos/remote/TransportControl$Listener;
    invoke-interface {v2}, Lcom/google/android/videos/remote/TransportControl$Listener;->onSelect()V

    goto :goto_7

    .line 302
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "listener":Lcom/google/android/videos/remote/TransportControl$Listener;
    .end local v3    # "listeners":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/remote/TransportControl$Listener;>;"
    :cond_7
    return v8
.end method
