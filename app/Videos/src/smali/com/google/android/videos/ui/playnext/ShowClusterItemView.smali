.class public Lcom/google/android/videos/ui/playnext/ShowClusterItemView;
.super Lcom/google/android/videos/ui/playnext/CardItemView;
.source "ShowClusterItemView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/playnext/ShowClusterItemView$Binder;
    }
.end annotation


# instance fields
.field private showId:Ljava/lang/String;

.field private thumbnailBitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

.field private titleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/playnext/CardItemView;-><init>(Landroid/content/Context;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/ui/playnext/CardItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/ui/playnext/CardItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    return-void
.end method


# virtual methods
.method bind(Landroid/database/Cursor;Lcom/google/android/videos/async/Requester;ZLcom/google/android/videos/ui/playnext/ShowClusterItemView$Binder;)V
    .locals 7
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p3, "isNetworkConnected"    # Z
    .param p4, "binder"    # Lcom/google/android/videos/ui/playnext/ShowClusterItemView$Binder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;Z",
            "Lcom/google/android/videos/ui/playnext/ShowClusterItemView$Binder;",
            ")V"
        }
    .end annotation

    .prologue
    .line 50
    .local p2, "posterRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    invoke-static {p1}, Lcom/google/android/videos/adapter/ShowsDataSource;->getShowId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/ShowClusterItemView;->showId:Ljava/lang/String;

    .line 51
    const v1, 0x7f0f00c9

    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/ShowClusterItemView;->thumbnailBitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

    const/4 v4, 0x0

    const-class v5, Ljava/lang/String;

    move-object v0, p0

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/videos/ui/playnext/ShowClusterItemView;->registerBitmapView(ILcom/google/android/videos/ui/BitmapLoader$BitmapView;Lcom/google/android/videos/async/Requester;Landroid/graphics/Bitmap;Ljava/lang/Class;)V

    .line 54
    invoke-static {p1}, Lcom/google/android/videos/adapter/ShowsDataSource;->getShowTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v6

    .line 55
    .local v6, "title":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/ShowClusterItemView;->titleView:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    if-nez p3, :cond_0

    invoke-static {p1}, Lcom/google/android/videos/adapter/ShowsDataSource;->isAnyEpisodePinned(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/ShowClusterItemView;->setDimmedStyle(Z)V

    .line 59
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/ShowClusterItemView;->thumbnailView:Landroid/widget/ImageView;

    const v1, 0x7f0b0077

    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/ShowClusterItemView;->showId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/videos/ui/TransitionUtil;->encodeAndSetTransitionName(Landroid/view/View;ILjava/lang/String;)V

    .line 61
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/ShowClusterItemView;->showId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->assetInfoFromShowId(Ljava/lang/String;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v0

    invoke-virtual {p4, p0, v0}, Lcom/google/android/videos/ui/playnext/ShowClusterItemView$Binder;->childImpression(Lcom/google/android/videos/ui/playnext/ClusterItemView;Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;)V

    .line 62
    return-void

    .line 57
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected collectClickableViews([Landroid/view/View;)V
    .locals 1
    .param p1, "clickableViews"    # [Landroid/view/View;

    .prologue
    .line 74
    const/4 v0, 0x0

    aput-object p0, p1, v0

    .line 75
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 42
    invoke-super {p0}, Lcom/google/android/videos/ui/playnext/CardItemView;->onFinishInflate()V

    .line 43
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/ShowClusterItemView;->thumbnailView:Landroid/widget/ImageView;

    const v1, 0x7f0a00d7

    invoke-static {v0, v1}, Lcom/google/android/videos/ui/BitmapLoader;->createDefaultBitmapView(Landroid/widget/ImageView;I)Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/ShowClusterItemView;->thumbnailBitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

    .line 45
    const v0, 0x7f0f00a8

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/ShowClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/ShowClusterItemView;->titleView:Landroid/widget/TextView;

    .line 46
    return-void
.end method

.method protected onGenerateBitmapViewRequest(ILjava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .param p1, "id"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Q:",
            "Ljava/lang/Object;",
            ">(I",
            "Ljava/lang/Class",
            "<TQ;>;)TQ;"
        }
    .end annotation

    .prologue
    .line 66
    .local p2, "requestType":Ljava/lang/Class;, "Ljava/lang/Class<TQ;>;"
    const v0, 0x7f0f00c9

    if-ne p1, v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/ShowClusterItemView;->showId:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 69
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/videos/ui/playnext/CardItemView;->onGenerateBitmapViewRequest(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method
