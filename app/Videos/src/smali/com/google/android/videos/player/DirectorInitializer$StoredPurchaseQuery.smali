.class interface abstract Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;
.super Ljava/lang/Object;
.source "DirectorInitializer.java"

# interfaces
.implements Lcom/google/android/videos/player/DirectorInitializer$PurchaseQuery;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/DirectorInitializer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "StoredPurchaseQuery"
.end annotation


# static fields
.field public static final COLUMNS:[Ljava/lang/String;

.field public static final DOWNLOAD_BYTES_DOWNLOADED:I

.field public static final DOWNLOAD_EXTRA:I

.field public static final DOWNLOAD_LAST_MODIFIED:I

.field public static final DOWNLOAD_RELATIVE_FILEPATH:I

.field public static final DOWNLOAD_SIZE:I

.field public static final IS_DOWNLOAD_COMPLETED:I

.field public static final IS_PINNED:I

.field public static final LAST_WATCHED_TIMESTAMP:I

.field public static final LICENSE_CENC_KEY_SET_ID:I

.field public static final LICENSE_CENC_SECURITY_LEVEL:I

.field public static final LICENSE_LEGACY_ASSET_ID:I

.field public static final LICENSE_LEGACY_KEY_ID:I

.field public static final LICENSE_LEGACY_SYSTEM_ID:I

.field public static final LICENSE_LEGACY_VIDEO_FORMAT:I

.field public static final LICENSE_TYPE:I

.field public static final RESUME_TIMESTAMP:I

.field public static final STORAGE_INDEX:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 998
    sget-object v0, Lcom/google/android/videos/player/DirectorInitializer$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    const/16 v1, 0x11

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "pinning_download_size"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "download_relative_filepath"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "download_bytes_downloaded"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "download_extra_proto"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "license_type"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "license_key_id"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "license_asset_id"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "license_system_id"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "license_cenc_key_set_id"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "license_cenc_security_level"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "resume_timestamp"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "last_watched_timestamp"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "(pinned IS NOT NULL AND pinned > 0)"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "external_storage_index"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "pinning_status = 3"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "download_last_modified"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "license_video_format"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/DbUtils;->extend([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->COLUMNS:[Ljava/lang/String;

    .line 1018
    sget-object v0, Lcom/google/android/videos/player/DirectorInitializer$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x0

    sput v0, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->DOWNLOAD_SIZE:I

    .line 1019
    sget-object v0, Lcom/google/android/videos/player/DirectorInitializer$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->DOWNLOAD_RELATIVE_FILEPATH:I

    .line 1020
    sget-object v0, Lcom/google/android/videos/player/DirectorInitializer$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x2

    sput v0, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->DOWNLOAD_BYTES_DOWNLOADED:I

    .line 1021
    sget-object v0, Lcom/google/android/videos/player/DirectorInitializer$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x3

    sput v0, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->DOWNLOAD_EXTRA:I

    .line 1022
    sget-object v0, Lcom/google/android/videos/player/DirectorInitializer$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x4

    sput v0, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->LICENSE_TYPE:I

    .line 1023
    sget-object v0, Lcom/google/android/videos/player/DirectorInitializer$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x5

    sput v0, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->LICENSE_LEGACY_KEY_ID:I

    .line 1024
    sget-object v0, Lcom/google/android/videos/player/DirectorInitializer$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x6

    sput v0, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->LICENSE_LEGACY_ASSET_ID:I

    .line 1025
    sget-object v0, Lcom/google/android/videos/player/DirectorInitializer$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x7

    sput v0, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->LICENSE_LEGACY_SYSTEM_ID:I

    .line 1026
    sget-object v0, Lcom/google/android/videos/player/DirectorInitializer$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x8

    sput v0, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->LICENSE_CENC_KEY_SET_ID:I

    .line 1027
    sget-object v0, Lcom/google/android/videos/player/DirectorInitializer$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x9

    sput v0, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->LICENSE_CENC_SECURITY_LEVEL:I

    .line 1028
    sget-object v0, Lcom/google/android/videos/player/DirectorInitializer$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0xa

    sput v0, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->RESUME_TIMESTAMP:I

    .line 1029
    sget-object v0, Lcom/google/android/videos/player/DirectorInitializer$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0xb

    sput v0, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->LAST_WATCHED_TIMESTAMP:I

    .line 1030
    sget-object v0, Lcom/google/android/videos/player/DirectorInitializer$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0xc

    sput v0, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->IS_PINNED:I

    .line 1031
    sget-object v0, Lcom/google/android/videos/player/DirectorInitializer$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0xd

    sput v0, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->STORAGE_INDEX:I

    .line 1032
    sget-object v0, Lcom/google/android/videos/player/DirectorInitializer$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0xe

    sput v0, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->IS_DOWNLOAD_COMPLETED:I

    .line 1033
    sget-object v0, Lcom/google/android/videos/player/DirectorInitializer$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0xf

    sput v0, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->DOWNLOAD_LAST_MODIFIED:I

    .line 1034
    sget-object v0, Lcom/google/android/videos/player/DirectorInitializer$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x10

    sput v0, Lcom/google/android/videos/player/DirectorInitializer$StoredPurchaseQuery;->LICENSE_LEGACY_VIDEO_FORMAT:I

    return-void
.end method
