.class public Lcom/google/android/videos/utils/SettingsUtil;
.super Ljava/lang/Object;
.source "SettingsUtil.java"


# direct methods
.method public static getAllAudioPreferenceTexts(Landroid/content/Context;)[Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x1

    .line 88
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 89
    .local v1, "resources":Landroid/content/res/Resources;
    const v2, 0x7f120005

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 90
    .local v0, "preferenceTitles":[Ljava/lang/String;
    aget-object v2, v0, v6

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/videos/utils/Util;->getLanguageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    .line 92
    return-object v0
.end method

.method public static getAudioPreferenceText(Landroid/content/Context;Landroid/content/SharedPreferences;)Ljava/lang/String;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "preferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 72
    invoke-static {p0, p1}, Lcom/google/android/videos/utils/SettingsUtil;->getAudioPreferenceValue(Landroid/content/Context;Landroid/content/SharedPreferences;)Ljava/lang/String;

    move-result-object v3

    .line 73
    .local v3, "userPreference":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f120006

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 75
    .local v2, "preferenceValues":[Ljava/lang/String;
    invoke-static {p0}, Lcom/google/android/videos/utils/SettingsUtil;->getAllAudioPreferenceTexts(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v1

    .line 76
    .local v1, "preferenceTitles":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v2

    if-ge v0, v4, :cond_1

    .line 77
    aget-object v4, v2, v0

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 78
    aget-object v4, v1, v0

    .line 84
    :goto_1
    return-object v4

    .line 76
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 82
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid user preference: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 83
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "audio_language"

    invoke-interface {v4, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 84
    const/4 v4, 0x0

    aget-object v4, v1, v4

    goto :goto_1
.end method

.method public static getAudioPreferenceValue(Landroid/content/Context;Landroid/content/SharedPreferences;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "preferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 96
    const-string v1, "audio_language"

    const/4 v2, 0x0

    invoke-interface {p1, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 97
    .local v0, "preference":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 98
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0238

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 100
    :cond_0
    return-object v0
.end method

.method public static getDownloadNetworkPreferenceText(Landroid/content/Context;Landroid/content/SharedPreferences;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "preferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 34
    invoke-static {p0, p1}, Lcom/google/android/videos/utils/SettingsUtil;->getDownloadNetworkPreferenceValue(Landroid/content/Context;Landroid/content/SharedPreferences;)Ljava/lang/String;

    move-result-object v0

    .line 35
    .local v0, "preference":Ljava/lang/String;
    const v1, 0x7f0b021e

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0b0127

    :goto_0
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_0
    const v1, 0x7f0b0126

    goto :goto_0
.end method

.method public static getDownloadNetworkPreferenceValue(Landroid/content/Context;Landroid/content/SharedPreferences;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "preferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 41
    const-string v1, "download_policy"

    const/4 v2, 0x0

    invoke-interface {p1, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 42
    .local v0, "preference":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 43
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b021e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 45
    :cond_0
    return-object v0
.end method

.method public static getDownloadOnWiFiOnly(Landroid/content/Context;Landroid/content/SharedPreferences;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "preferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 28
    invoke-static {p0, p1}, Lcom/google/android/videos/utils/SettingsUtil;->getDownloadNetworkPreferenceValue(Landroid/content/Context;Landroid/content/SharedPreferences;)Ljava/lang/String;

    move-result-object v0

    .line 29
    .local v0, "preference":Ljava/lang/String;
    const v1, 0x7f0b021e

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public static getDownloadQualityItag(Landroid/content/Context;Landroid/content/SharedPreferences;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "preferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 49
    invoke-static {p0, p1}, Lcom/google/android/videos/utils/SettingsUtil;->getDownloadQualityPreferenceValue(Landroid/content/Context;Landroid/content/SharedPreferences;)Ljava/lang/String;

    move-result-object v0

    .line 50
    .local v0, "preference":Ljava/lang/String;
    const v1, 0x7f0b0221

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getDownloadQualityPreferenceText(Landroid/content/Context;Landroid/content/SharedPreferences;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "preferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 56
    invoke-static {p0, p1}, Lcom/google/android/videos/utils/SettingsUtil;->getDownloadQualityPreferenceValue(Landroid/content/Context;Landroid/content/SharedPreferences;)Ljava/lang/String;

    move-result-object v0

    .line 57
    .local v0, "preference":Ljava/lang/String;
    const v1, 0x7f0b0221

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0b0208

    :goto_0
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_0
    const v1, 0x7f0b0209

    goto :goto_0
.end method

.method public static getDownloadQualityPreferenceValue(Landroid/content/Context;Landroid/content/SharedPreferences;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "preferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 63
    const-string v1, "download_quality"

    const/4 v2, 0x0

    invoke-interface {p1, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 64
    .local v0, "preference":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 65
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0221

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 67
    :cond_0
    return-object v0
.end method

.method public static getPreferredStorageIndex(Landroid/content/SharedPreferences;)I
    .locals 3
    .param p0, "preferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 104
    const-string v1, "download_storage"

    const/4 v2, 0x0

    invoke-interface {p0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 105
    .local v0, "locationPreference":Ljava/lang/String;
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Util;->parseInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method
