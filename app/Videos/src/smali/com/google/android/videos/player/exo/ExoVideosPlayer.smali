.class public Lcom/google/android/videos/player/exo/ExoVideosPlayer;
.super Landroid/os/Handler;
.source "ExoVideosPlayer.java"

# interfaces
.implements Lcom/google/android/exoplayer/DefaultLoadControl$EventListener;
.implements Lcom/google/android/exoplayer/ExoPlayer$Listener;
.implements Lcom/google/android/exoplayer/MediaCodecAudioTrackRenderer$EventListener;
.implements Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer$EventListener;
.implements Lcom/google/android/exoplayer/chunk/ChunkSampleSource$EventListener;
.implements Lcom/google/android/exoplayer/upstream/DefaultBandwidthMeter$EventListener;
.implements Lcom/google/android/videos/player/PlayerSurface$Listener;
.implements Lcom/google/android/videos/player/VideosPlayer;
.implements Lcom/google/android/videos/player/exo/VideosHttpDataSource$VideosHttpDataSourceListener;
.implements Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$EventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;
    }
.end annotation


# instance fields
.field private final activity:Landroid/app/Activity;

.field private final adaptive:Z

.field private final adaptiveDisableHdOnMobileNetwork:Z

.field private audioRenderer:Lcom/google/android/exoplayer/TrackRenderer;

.field private audioRepresentations:[Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

.field private audioTrackInfos:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

.field private final config:Lcom/google/android/videos/Config;

.field private final display:Landroid/view/Display;

.field private final displaySupportsProtectedBuffers:Z

.field private drmSessionManager:Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;

.field private enableTrickMode:Z

.field private final enableVirtualizer:Z

.field private final exoCacheProvider:Lcom/google/android/videos/pinning/ExoCacheProvider;

.field private hq:Z

.field private hqToggleSupported:Z

.field private isHqHd:Z

.field private lastRetrySystemTimeMs:J

.field private final listener:Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;

.field private offlineVideoChunkSource:Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;

.field private onlineAudioChunkSource:Lcom/google/android/exoplayer/chunk/MultiTrackChunkSource;

.field private onlineVideoChunkSource:Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;

.field private openedDrmSessionManager:Z

.field private final playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

.field private final player:Lcom/google/android/exoplayer/ExoPlayer;

.field private playerSurface:Lcom/google/android/videos/player/PlayerSurface;

.field private final preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

.field private prepared:Z

.field private final qualityDropper:Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;

.field private realInitialTrigger:I

.field private retries:I

.field private selectedAudioTrackIndex:I

.field private selectedItag:I

.field private stablePausedState:Z

.field private final streamSelector:Lcom/google/android/videos/streams/DashStreamsSelector;

.field private final streamsRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/MpdGetRequest;",
            "Lcom/google/android/videos/streams/Streams;",
            ">;"
        }
    .end annotation
.end field

.field private subtitleRenderer:Lcom/google/android/exoplayer/TrackRenderer;

.field private subtitles:Lcom/google/android/videos/subtitles/Subtitles;

.field private subtitlesOverlay:Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

.field private final surroundSound:Z

.field private videoInfo:Lcom/google/android/videos/player/VideoInfo;

.field private videoRenderer:Lcom/google/android/videos/player/exo/VideoRenderer;

.field private final videosGlobals:Lcom/google/android/videos/VideosGlobals;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/VideosGlobals;Landroid/app/Activity;Landroid/view/Display;ZZZZZLcom/google/android/videos/streams/DashStreamsSelector;Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;Lcom/google/android/videos/player/PlaybackResumeState;Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;)V
    .locals 3
    .param p1, "videosGlobals"    # Lcom/google/android/videos/VideosGlobals;
    .param p2, "activity"    # Landroid/app/Activity;
    .param p3, "display"    # Landroid/view/Display;
    .param p4, "surroundSound"    # Z
    .param p5, "enableVirtualizer"    # Z
    .param p6, "adaptive"    # Z
    .param p7, "adaptiveDisableHdOnMobileNetwork"    # Z
    .param p8, "displaySupportsProtectedBuffers"    # Z
    .param p9, "streamSelector"    # Lcom/google/android/videos/streams/DashStreamsSelector;
    .param p10, "listener"    # Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;
    .param p11, "playbackResumeState"    # Lcom/google/android/videos/player/PlaybackResumeState;
    .param p12, "preparationLogger"    # Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    .prologue
    .line 201
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 202
    iput-object p1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    .line 203
    iput-object p2, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->activity:Landroid/app/Activity;

    .line 204
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->config:Lcom/google/android/videos/Config;

    .line 205
    iput-boolean p4, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->surroundSound:Z

    .line 206
    iput-boolean p5, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->enableVirtualizer:Z

    .line 207
    iput-boolean p6, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->adaptive:Z

    .line 208
    iput-boolean p7, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->adaptiveDisableHdOnMobileNetwork:Z

    .line 209
    iput-object p3, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->display:Landroid/view/Display;

    .line 210
    iput-boolean p8, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->displaySupportsProtectedBuffers:Z

    .line 211
    iput-object p10, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->listener:Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;

    .line 212
    iput-object p11, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    .line 213
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getExoCacheProvider()Lcom/google/android/videos/pinning/ExoCacheProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->exoCacheProvider:Lcom/google/android/videos/pinning/ExoCacheProvider;

    .line 214
    iput-object p9, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->streamSelector:Lcom/google/android/videos/streams/DashStreamsSelector;

    .line 215
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/api/ApiRequesters;->getStreamsRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->streamsRequester:Lcom/google/android/videos/async/Requester;

    .line 216
    iput-object p12, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    .line 217
    if-eqz p6, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->qualityDropper:Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;

    .line 218
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->config:Lcom/google/android/videos/Config;

    invoke-interface {v1}, Lcom/google/android/videos/Config;->exoMinBufferMs()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->config:Lcom/google/android/videos/Config;

    invoke-interface {v2}, Lcom/google/android/videos/Config;->exoMinRebufferMs()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/google/android/exoplayer/ExoPlayer$Factory;->newInstance(III)Lcom/google/android/exoplayer/ExoPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->player:Lcom/google/android/exoplayer/ExoPlayer;

    .line 220
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->player:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v0, p0}, Lcom/google/android/exoplayer/ExoPlayer;->addListener(Lcom/google/android/exoplayer/ExoPlayer$Listener;)V

    .line 221
    return-void

    .line 217
    :cond_0
    new-instance v0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;

    iget-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->config:Lcom/google/android/videos/Config;

    invoke-direct {v0, p0, v1}, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;-><init>(Lcom/google/android/videos/player/VideosPlayer;Lcom/google/android/videos/Config;)V

    goto :goto_0
.end method

.method private buildOfflineSources(Lcom/google/android/exoplayer/LoadControl;)Landroid/util/Pair;
    .locals 28
    .param p1, "loadControl"    # Lcom/google/android/exoplayer/LoadControl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer/LoadControl;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/exoplayer/SampleSource;",
            "Lcom/google/android/exoplayer/SampleSource;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/streams/MissingStreamException;
        }
    .end annotation

    .prologue
    .line 837
    const/16 v26, 0x0

    .line 838
    .local v26, "videoRepresentation":Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    const/16 v18, 0x0

    .line 839
    .local v18, "audioRepresentation":Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    const/16 v22, 0x0

    .local v22, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-object v8, v8, Lcom/google/android/videos/player/VideoInfo;->offlineStreams:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    move/from16 v0, v22

    if-ge v0, v8, :cond_1

    .line 840
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-object v8, v8, Lcom/google/android/videos/player/VideoInfo;->offlineStreams:Ljava/util/List;

    move/from16 v0, v22

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/google/android/videos/streams/MediaStream;

    .line 841
    .local v23, "stream":Lcom/google/android/videos/streams/MediaStream;
    move-object/from16 v0, v23

    iget-object v8, v0, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    iget v8, v8, Lcom/google/android/videos/streams/ItagInfo;->width:I

    move-object/from16 v0, v23

    iget-object v9, v0, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    iget v9, v9, Lcom/google/android/videos/streams/ItagInfo;->height:I

    mul-int/2addr v8, v9

    if-lez v8, :cond_0

    .line 842
    const-string v8, "video/"

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-object v9, v9, Lcom/google/android/videos/player/VideoInfo;->videoId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget v10, v10, Lcom/google/android/videos/player/VideoInfo;->durationMs:I

    int-to-long v10, v10

    move-object/from16 v0, v23

    invoke-static {v0, v8, v9, v10, v11}, Lcom/google/android/videos/player/exo/ExoPlayerUtil;->toRepresentation(Lcom/google/android/videos/streams/MediaStream;Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    move-result-object v26

    .line 839
    :goto_1
    add-int/lit8 v22, v22, 0x1

    goto :goto_0

    .line 845
    :cond_0
    const-string v8, "audio/"

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-object v9, v9, Lcom/google/android/videos/player/VideoInfo;->videoId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget v10, v10, Lcom/google/android/videos/player/VideoInfo;->durationMs:I

    int-to-long v10, v10

    move-object/from16 v0, v23

    invoke-static {v0, v8, v9, v10, v11}, Lcom/google/android/videos/player/exo/ExoPlayerUtil;->toRepresentation(Lcom/google/android/videos/streams/MediaStream;Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    move-result-object v18

    goto :goto_1

    .line 850
    .end local v23    # "stream":Lcom/google/android/videos/streams/MediaStream;
    :cond_1
    if-eqz v26, :cond_2

    if-nez v18, :cond_3

    .line 851
    :cond_2
    new-instance v8, Lcom/google/android/videos/streams/MissingStreamException;

    invoke-direct {v8}, Lcom/google/android/videos/streams/MissingStreamException;-><init>()V

    throw v8

    .line 854
    :cond_3
    new-instance v4, Lcom/google/android/videos/player/exo/SyncVideoStreamRequester;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-object v5, v8, Lcom/google/android/videos/player/VideoInfo;->account:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-object v6, v8, Lcom/google/android/videos/player/VideoInfo;->videoId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-boolean v7, v8, Lcom/google/android/videos/player/VideoInfo;->isEpisode:Z

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->streamsRequester:Lcom/google/android/videos/async/Requester;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->config:Lcom/google/android/videos/Config;

    invoke-interface {v9}, Lcom/google/android/videos/Config;->useSslForStreaming()Z

    move-result v9

    invoke-direct/range {v4 .. v9}, Lcom/google/android/videos/player/exo/SyncVideoStreamRequester;-><init>(Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/videos/async/Requester;Z)V

    .line 859
    .local v4, "syncStreamRequester":Lcom/google/android/videos/player/exo/SyncVideoStreamRequester;
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v26

    iget-object v8, v0, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->uri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v8

    move-object/from16 v0, v21

    invoke-direct {v0, v8}, Ljava/io/File;-><init>(Ljava/net/URI;)V

    .line 860
    .local v21, "cacheDir":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->exoCacheProvider:Lcom/google/android/videos/pinning/ExoCacheProvider;

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Lcom/google/android/videos/pinning/ExoCacheProvider;->acquireDownloadCache(Ljava/io/File;)Lcom/google/android/exoplayer/upstream/cache/Cache;

    move-result-object v20

    .line 862
    .local v20, "cache":Lcom/google/android/exoplayer/upstream/cache/Cache;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-static {v8, v9, v0, v1}, Lcom/google/android/videos/player/exo/ExoPlayerUtil;->buildPlaybackHttpDataSource(Lcom/google/android/videos/VideosGlobals;Lcom/google/android/exoplayer/upstream/DefaultBandwidthMeter;Lcom/google/android/videos/player/exo/VideosHttpDataSource$VideosHttpDataSourceListener;Landroid/os/Handler;)Lcom/google/android/exoplayer/upstream/DataSource;

    move-result-object v25

    .line 864
    .local v25, "videoHttpDataSource":Lcom/google/android/exoplayer/upstream/DataSource;
    new-instance v27, Lcom/google/android/videos/player/exo/UriRewriterDataSource;

    move-object/from16 v0, v26

    iget-object v8, v0, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->format:Lcom/google/android/exoplayer/chunk/Format;

    iget-object v8, v8, Lcom/google/android/exoplayer/chunk/Format;->id:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, v27

    move-object/from16 v1, v25

    invoke-direct {v0, v1, v4, v8}, Lcom/google/android/videos/player/exo/UriRewriterDataSource;-><init>(Lcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/videos/player/exo/SyncVideoStreamRequester;I)V

    .line 866
    .local v27, "videoUpstream":Lcom/google/android/exoplayer/upstream/DataSource;
    new-instance v24, Lcom/google/android/exoplayer/upstream/cache/CacheDataSource;

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, v20

    move-object/from16 v2, v27

    invoke-direct {v0, v1, v2, v8, v9}, Lcom/google/android/exoplayer/upstream/cache/CacheDataSource;-><init>(Lcom/google/android/exoplayer/upstream/cache/Cache;Lcom/google/android/exoplayer/upstream/DataSource;ZZ)V

    .line 867
    .local v24, "videoDataSource":Lcom/google/android/exoplayer/upstream/DataSource;
    new-instance v8, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    move-object/from16 v2, v20

    invoke-direct {v8, v0, v1, v2}, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;-><init>(Lcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;Lcom/google/android/exoplayer/upstream/cache/Cache;)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->offlineVideoChunkSource:Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;

    .line 869
    new-instance v5, Lcom/google/android/exoplayer/chunk/ChunkSampleSource;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->offlineVideoChunkSource:Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->config:Lcom/google/android/videos/Config;

    invoke-interface {v8}, Lcom/google/android/videos/Config;->exoBufferChunkSize()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->config:Lcom/google/android/videos/Config;

    invoke-interface {v9}, Lcom/google/android/videos/Config;->exoBufferChunkCount()I

    move-result v9

    mul-int/2addr v8, v9

    const/4 v9, 0x1

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->config:Lcom/google/android/videos/Config;

    invoke-interface {v10}, Lcom/google/android/videos/Config;->exoMinLoadableRetryCount()I

    move-result v13

    move-object/from16 v7, p1

    move-object/from16 v10, p0

    move-object/from16 v11, p0

    invoke-direct/range {v5 .. v13}, Lcom/google/android/exoplayer/chunk/ChunkSampleSource;-><init>(Lcom/google/android/exoplayer/chunk/ChunkSource;Lcom/google/android/exoplayer/LoadControl;IZLandroid/os/Handler;Lcom/google/android/exoplayer/chunk/ChunkSampleSource$EventListener;II)V

    .line 873
    .local v5, "videoSampleSource":Lcom/google/android/exoplayer/SampleSource;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-static {v8, v9, v0, v1}, Lcom/google/android/videos/player/exo/ExoPlayerUtil;->buildPlaybackHttpDataSource(Lcom/google/android/videos/VideosGlobals;Lcom/google/android/exoplayer/upstream/DefaultBandwidthMeter;Lcom/google/android/videos/player/exo/VideosHttpDataSource$VideosHttpDataSourceListener;Landroid/os/Handler;)Lcom/google/android/exoplayer/upstream/DataSource;

    move-result-object v17

    .line 875
    .local v17, "audioHttpDataSource":Lcom/google/android/exoplayer/upstream/DataSource;
    new-instance v19, Lcom/google/android/videos/player/exo/UriRewriterDataSource;

    move-object/from16 v0, v18

    iget-object v8, v0, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->format:Lcom/google/android/exoplayer/chunk/Format;

    iget-object v8, v8, Lcom/google/android/exoplayer/chunk/Format;->id:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v4, v8}, Lcom/google/android/videos/player/exo/UriRewriterDataSource;-><init>(Lcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/videos/player/exo/SyncVideoStreamRequester;I)V

    .line 877
    .local v19, "audioUpstream":Lcom/google/android/exoplayer/upstream/DataSource;
    new-instance v15, Lcom/google/android/exoplayer/upstream/cache/CacheDataSource;

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-direct {v15, v0, v1, v8, v9}, Lcom/google/android/exoplayer/upstream/cache/CacheDataSource;-><init>(Lcom/google/android/exoplayer/upstream/cache/Cache;Lcom/google/android/exoplayer/upstream/DataSource;ZZ)V

    .line 878
    .local v15, "audioDataSource":Lcom/google/android/exoplayer/upstream/cache/CacheDataSource;
    new-instance v16, Lcom/google/android/exoplayer/chunk/FormatEvaluator$FixedEvaluator;

    invoke-direct/range {v16 .. v16}, Lcom/google/android/exoplayer/chunk/FormatEvaluator$FixedEvaluator;-><init>()V

    .line 879
    .local v16, "audioEvaluator":Lcom/google/android/exoplayer/chunk/FormatEvaluator$FixedEvaluator;
    new-instance v7, Lcom/google/android/exoplayer/dash/DashChunkSource;

    const/4 v8, 0x1

    new-array v8, v8, [Lcom/google/android/exoplayer/dash/mpd/Representation;

    const/4 v9, 0x0

    aput-object v18, v8, v9

    move-object/from16 v0, v16

    invoke-direct {v7, v15, v0, v8}, Lcom/google/android/exoplayer/dash/DashChunkSource;-><init>(Lcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/exoplayer/chunk/FormatEvaluator;[Lcom/google/android/exoplayer/dash/mpd/Representation;)V

    .line 881
    .local v7, "audioChunkSource":Lcom/google/android/exoplayer/chunk/ChunkSource;
    new-instance v6, Lcom/google/android/exoplayer/chunk/ChunkSampleSource;

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v13, 0x1

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->config:Lcom/google/android/videos/Config;

    invoke-interface {v8}, Lcom/google/android/videos/Config;->exoMinLoadableRetryCount()I

    move-result v14

    move-object/from16 v8, p1

    move-object/from16 v11, p0

    move-object/from16 v12, p0

    invoke-direct/range {v6 .. v14}, Lcom/google/android/exoplayer/chunk/ChunkSampleSource;-><init>(Lcom/google/android/exoplayer/chunk/ChunkSource;Lcom/google/android/exoplayer/LoadControl;IZLandroid/os/Handler;Lcom/google/android/exoplayer/chunk/ChunkSampleSource$EventListener;II)V

    .line 884
    .local v6, "audioSampleSource":Lcom/google/android/exoplayer/SampleSource;
    invoke-static {v5, v6}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v8

    return-object v8
.end method

.method private buildOnlineSources(ZLcom/google/android/exoplayer/LoadControl;)Landroid/util/Pair;
    .locals 29
    .param p1, "filterHdContent"    # Z
    .param p2, "loadControl"    # Lcom/google/android/exoplayer/LoadControl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/google/android/exoplayer/LoadControl;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/exoplayer/SampleSource;",
            "Lcom/google/android/exoplayer/SampleSource;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/streams/MissingStreamException;
        }
    .end annotation

    .prologue
    .line 754
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->streamSelector:Lcom/google/android/videos/streams/DashStreamsSelector;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-object v4, v4, Lcom/google/android/videos/player/VideoInfo;->onlineStreams:Ljava/util/List;

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->adaptive:Z

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->display:Landroid/view/Display;

    move/from16 v0, p1

    invoke-virtual {v2, v4, v0, v5, v7}, Lcom/google/android/videos/streams/DashStreamsSelector;->getOnlineVideoStreams(Ljava/util/List;ZZLandroid/view/Display;)Lcom/google/android/videos/streams/DashVideoStreamSelection;

    move-result-object v27

    .line 756
    .local v27, "videoStreamSelection":Lcom/google/android/videos/streams/DashVideoStreamSelection;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->hq:Z

    if-eqz v2, :cond_1

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/google/android/videos/streams/DashVideoStreamSelection;->hi:Ljava/util/List;

    move-object/from16 v28, v0

    .line 757
    .local v28, "videoStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    :goto_0
    move-object/from16 v0, v27

    iget-boolean v2, v0, Lcom/google/android/videos/streams/DashVideoStreamSelection;->supportsQualityToggle:Z

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->hqToggleSupported:Z

    .line 758
    move-object/from16 v0, v27

    iget-boolean v2, v0, Lcom/google/android/videos/streams/DashVideoStreamSelection;->isHiHd:Z

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->isHqHd:Z

    .line 759
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->qualityDropper:Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;

    if-eqz v2, :cond_0

    .line 760
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->qualityDropper:Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->hqToggleSupported:Z

    invoke-virtual {v2, v4}, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->onStreamsSelected(Z)V

    .line 763
    :cond_0
    invoke-interface/range {v28 .. v28}, Ljava/util/List;->size()I

    move-result v2

    new-array v6, v2, [Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    .line 765
    .local v6, "videoRepresentations":[Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    const/16 v25, 0x0

    .local v25, "i":I
    :goto_1
    invoke-interface/range {v28 .. v28}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, v25

    if-ge v0, v2, :cond_2

    .line 766
    move-object/from16 v0, v28

    move/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/streams/MediaStream;

    const-string v4, "video/"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-object v5, v5, Lcom/google/android/videos/player/VideoInfo;->videoId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget v7, v7, Lcom/google/android/videos/player/VideoInfo;->durationMs:I

    int-to-long v12, v7

    invoke-static {v2, v4, v5, v12, v13}, Lcom/google/android/videos/player/exo/ExoPlayerUtil;->toRepresentation(Lcom/google/android/videos/streams/MediaStream;Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    move-result-object v2

    aput-object v2, v6, v25

    .line 765
    add-int/lit8 v25, v25, 0x1

    goto :goto_1

    .line 756
    .end local v6    # "videoRepresentations":[Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    .end local v25    # "i":I
    .end local v28    # "videoStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    :cond_1
    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/google/android/videos/streams/DashVideoStreamSelection;->lo:Ljava/util/List;

    move-object/from16 v28, v0

    goto :goto_0

    .line 770
    .restart local v6    # "videoRepresentations":[Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    .restart local v25    # "i":I
    .restart local v28    # "videoStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->streamSelector:Lcom/google/android/videos/streams/DashStreamsSelector;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-object v4, v4, Lcom/google/android/videos/player/VideoInfo;->onlineStreams:Ljava/util/List;

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->surroundSound:Z

    invoke-virtual {v2, v4, v5}, Lcom/google/android/videos/streams/DashStreamsSelector;->getOnlineAudioStreams(Ljava/util/List;Z)Ljava/util/List;

    move-result-object v24

    .line 772
    .local v24, "audioStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    const/4 v4, -0x1

    invoke-virtual {v2, v4}, Lcom/google/android/videos/player/PlaybackResumeState;->getSelectedAudioTrackIndex(I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->selectedAudioTrackIndex:I

    .line 773
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->selectedAudioTrackIndex:I

    const/4 v4, -0x1

    if-eq v2, v4, :cond_3

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->selectedAudioTrackIndex:I

    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    move-result v4

    if-lt v2, v4, :cond_4

    .line 774
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->activity:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-static {v0, v2, v4}, Lcom/google/android/videos/utils/AudioInfoUtil;->getPreferredStreamIndex(Ljava/util/List;Landroid/content/Context;Landroid/content/SharedPreferences;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->selectedAudioTrackIndex:I

    .line 778
    :cond_4
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->audioRepresentations:[Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    .line 779
    const/4 v2, 0x0

    move-object/from16 v0, v24

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/streams/MediaStream;

    iget-object v2, v2, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-object v2, v2, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    if-nez v2, :cond_6

    const/4 v2, 0x0

    :goto_2
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->audioTrackInfos:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    .line 781
    const/16 v25, 0x0

    :goto_3
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, v25

    if-ge v0, v2, :cond_7

    .line 782
    invoke-interface/range {v24 .. v25}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/google/android/videos/streams/MediaStream;

    .line 783
    .local v23, "audioStream":Lcom/google/android/videos/streams/MediaStream;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->audioRepresentations:[Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    const-string v4, "audio/"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-object v5, v5, Lcom/google/android/videos/player/VideoInfo;->videoId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget v7, v7, Lcom/google/android/videos/player/VideoInfo;->durationMs:I

    int-to-long v12, v7

    move-object/from16 v0, v23

    invoke-static {v0, v4, v5, v12, v13}, Lcom/google/android/videos/player/exo/ExoPlayerUtil;->toRepresentation(Lcom/google/android/videos/streams/MediaStream;Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    move-result-object v4

    aput-object v4, v2, v25

    .line 785
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->audioTrackInfos:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    if-eqz v2, :cond_5

    .line 786
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->audioTrackInfos:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    move-object/from16 v0, v23

    iget-object v4, v0, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-object v4, v4, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    aput-object v4, v2, v25

    .line 781
    :cond_5
    add-int/lit8 v25, v25, 0x1

    goto :goto_3

    .line 779
    .end local v23    # "audioStream":Lcom/google/android/videos/streams/MediaStream;
    :cond_6
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    goto :goto_2

    .line 790
    :cond_7
    new-instance v8, Lcom/google/android/exoplayer/upstream/DefaultBandwidthMeter;

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-direct {v8, v0, v1}, Lcom/google/android/exoplayer/upstream/DefaultBandwidthMeter;-><init>(Landroid/os/Handler;Lcom/google/android/exoplayer/upstream/DefaultBandwidthMeter$EventListener;)V

    .line 791
    .local v8, "bandwidthMeter":Lcom/google/android/exoplayer/upstream/DefaultBandwidthMeter;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-static {v2, v8, v0, v1}, Lcom/google/android/videos/player/exo/ExoPlayerUtil;->buildPlaybackHttpDataSource(Lcom/google/android/videos/VideosGlobals;Lcom/google/android/exoplayer/upstream/DefaultBandwidthMeter;Lcom/google/android/videos/player/exo/VideosHttpDataSource$VideosHttpDataSourceListener;Landroid/os/Handler;)Lcom/google/android/exoplayer/upstream/DataSource;

    move-result-object v3

    .line 793
    .local v3, "videoDataSource":Lcom/google/android/exoplayer/upstream/DataSource;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/videos/Config;->exoAbrAlgorithm()I

    move-result v19

    .line 794
    .local v19, "abrAlgorithm":I
    packed-switch v19, :pswitch_data_0

    .line 807
    new-instance v2, Lcom/google/android/videos/player/exo/adaptive/DefaultVideoChunkSource;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->activity:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v5}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v5

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->adaptive:Z

    if-eqz v7, :cond_9

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->adaptiveDisableHdOnMobileNetwork:Z

    if-eqz v7, :cond_9

    const/4 v7, 0x1

    :goto_4
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v11}, Lcom/google/android/videos/VideosGlobals;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v9

    invoke-direct/range {v2 .. v9}, Lcom/google/android/videos/player/exo/adaptive/DefaultVideoChunkSource;-><init>(Lcom/google/android/exoplayer/upstream/DataSource;Landroid/content/Context;Lcom/google/android/videos/Config;[Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;ZLcom/google/android/exoplayer/upstream/BandwidthMeter;Lcom/google/android/videos/utils/NetworkStatus;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->onlineVideoChunkSource:Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;

    .line 815
    :goto_5
    new-instance v9, Lcom/google/android/exoplayer/chunk/ChunkSampleSource;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->onlineVideoChunkSource:Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->config:Lcom/google/android/videos/Config;

    invoke-interface {v2}, Lcom/google/android/videos/Config;->exoBufferChunkSize()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->config:Lcom/google/android/videos/Config;

    invoke-interface {v4}, Lcom/google/android/videos/Config;->exoBufferChunkCount()I

    move-result v4

    mul-int v12, v2, v4

    const/4 v13, 0x1

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->config:Lcom/google/android/videos/Config;

    invoke-interface {v2}, Lcom/google/android/videos/Config;->exoMinLoadableRetryCount()I

    move-result v17

    move-object/from16 v11, p2

    move-object/from16 v14, p0

    move-object/from16 v15, p0

    invoke-direct/range {v9 .. v17}, Lcom/google/android/exoplayer/chunk/ChunkSampleSource;-><init>(Lcom/google/android/exoplayer/chunk/ChunkSource;Lcom/google/android/exoplayer/LoadControl;IZLandroid/os/Handler;Lcom/google/android/exoplayer/chunk/ChunkSampleSource$EventListener;II)V

    .line 820
    .local v9, "videoSampleSource":Lcom/google/android/exoplayer/SampleSource;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-static {v2, v8, v0, v1}, Lcom/google/android/videos/player/exo/ExoPlayerUtil;->buildPlaybackHttpDataSource(Lcom/google/android/videos/VideosGlobals;Lcom/google/android/exoplayer/upstream/DefaultBandwidthMeter;Lcom/google/android/videos/player/exo/VideosHttpDataSource$VideosHttpDataSourceListener;Landroid/os/Handler;)Lcom/google/android/exoplayer/upstream/DataSource;

    move-result-object v21

    .line 822
    .local v21, "audioDataSource":Lcom/google/android/exoplayer/upstream/DataSource;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->audioRepresentations:[Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    array-length v2, v2

    new-array v0, v2, [Lcom/google/android/exoplayer/chunk/ChunkSource;

    move-object/from16 v20, v0

    .line 823
    .local v20, "audioChunkSources":[Lcom/google/android/exoplayer/chunk/ChunkSource;
    new-instance v22, Lcom/google/android/exoplayer/chunk/FormatEvaluator$FixedEvaluator;

    invoke-direct/range {v22 .. v22}, Lcom/google/android/exoplayer/chunk/FormatEvaluator$FixedEvaluator;-><init>()V

    .line 824
    .local v22, "audioEvaluator":Lcom/google/android/exoplayer/chunk/FormatEvaluator$FixedEvaluator;
    const/16 v25, 0x0

    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->audioRepresentations:[Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    array-length v2, v2

    move/from16 v0, v25

    if-ge v0, v2, :cond_a

    .line 825
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->audioRepresentations:[Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    aget-object v26, v2, v25

    .line 826
    .local v26, "representation":Lcom/google/android/exoplayer/dash/mpd/Representation;
    new-instance v2, Lcom/google/android/exoplayer/dash/DashChunkSource;

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/google/android/exoplayer/dash/mpd/Representation;

    const/4 v5, 0x0

    aput-object v26, v4, v5

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-direct {v2, v0, v1, v4}, Lcom/google/android/exoplayer/dash/DashChunkSource;-><init>(Lcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/exoplayer/chunk/FormatEvaluator;[Lcom/google/android/exoplayer/dash/mpd/Representation;)V

    aput-object v2, v20, v25

    .line 824
    add-int/lit8 v25, v25, 0x1

    goto :goto_6

    .line 796
    .end local v9    # "videoSampleSource":Lcom/google/android/exoplayer/SampleSource;
    .end local v20    # "audioChunkSources":[Lcom/google/android/exoplayer/chunk/ChunkSource;
    .end local v21    # "audioDataSource":Lcom/google/android/exoplayer/upstream/DataSource;
    .end local v22    # "audioEvaluator":Lcom/google/android/exoplayer/chunk/FormatEvaluator$FixedEvaluator;
    .end local v26    # "representation":Lcom/google/android/exoplayer/dash/mpd/Representation;
    :pswitch_0
    new-instance v2, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->activity:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v5}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v5

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->adaptive:Z

    if-eqz v7, :cond_8

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->adaptiveDisableHdOnMobileNetwork:Z

    if-eqz v7, :cond_8

    const/4 v7, 0x1

    :goto_7
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v11}, Lcom/google/android/videos/VideosGlobals;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v9

    invoke-direct/range {v2 .. v9}, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;-><init>(Lcom/google/android/exoplayer/upstream/DataSource;Landroid/content/Context;Lcom/google/android/videos/Config;[Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;ZLcom/google/android/exoplayer/upstream/BandwidthMeter;Lcom/google/android/videos/utils/NetworkStatus;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->onlineVideoChunkSource:Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;

    goto/16 :goto_5

    :cond_8
    const/4 v7, 0x0

    goto :goto_7

    .line 802
    :pswitch_1
    new-instance v9, Lcom/google/android/videos/player/exo/adaptive/DebugVideoChunkSource;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->activity:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v14

    move-object v10, v3

    move-object v13, v8

    move-object v15, v6

    invoke-direct/range {v9 .. v15}, Lcom/google/android/videos/player/exo/adaptive/DebugVideoChunkSource;-><init>(Lcom/google/android/exoplayer/upstream/DataSource;Landroid/content/Context;Lcom/google/android/videos/Config;Lcom/google/android/exoplayer/upstream/BandwidthMeter;Lcom/google/android/videos/utils/NetworkStatus;[Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->onlineVideoChunkSource:Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;

    goto/16 :goto_5

    .line 807
    :cond_9
    const/4 v7, 0x0

    goto/16 :goto_4

    .line 828
    .restart local v9    # "videoSampleSource":Lcom/google/android/exoplayer/SampleSource;
    .restart local v20    # "audioChunkSources":[Lcom/google/android/exoplayer/chunk/ChunkSource;
    .restart local v21    # "audioDataSource":Lcom/google/android/exoplayer/upstream/DataSource;
    .restart local v22    # "audioEvaluator":Lcom/google/android/exoplayer/chunk/FormatEvaluator$FixedEvaluator;
    :cond_a
    new-instance v2, Lcom/google/android/exoplayer/chunk/MultiTrackChunkSource;

    move-object/from16 v0, v20

    invoke-direct {v2, v0}, Lcom/google/android/exoplayer/chunk/MultiTrackChunkSource;-><init>([Lcom/google/android/exoplayer/chunk/ChunkSource;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->onlineAudioChunkSource:Lcom/google/android/exoplayer/chunk/MultiTrackChunkSource;

    .line 829
    new-instance v10, Lcom/google/android/exoplayer/chunk/ChunkSampleSource;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->onlineAudioChunkSource:Lcom/google/android/exoplayer/chunk/MultiTrackChunkSource;

    const/4 v13, 0x0

    const/4 v14, 0x1

    const/16 v17, 0x1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->config:Lcom/google/android/videos/Config;

    invoke-interface {v2}, Lcom/google/android/videos/Config;->exoMinLoadableRetryCount()I

    move-result v18

    move-object/from16 v12, p2

    move-object/from16 v15, p0

    move-object/from16 v16, p0

    invoke-direct/range {v10 .. v18}, Lcom/google/android/exoplayer/chunk/ChunkSampleSource;-><init>(Lcom/google/android/exoplayer/chunk/ChunkSource;Lcom/google/android/exoplayer/LoadControl;IZLandroid/os/Handler;Lcom/google/android/exoplayer/chunk/ChunkSampleSource$EventListener;II)V

    .line 832
    .local v10, "audioSampleSource":Lcom/google/android/exoplayer/SampleSource;
    invoke-static {v9, v10}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    return-object v2

    .line 794
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private buildRenderers()[Lcom/google/android/exoplayer/TrackRenderer;
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/streams/MissingStreamException;,
            Landroid/media/UnsupportedSchemeException;,
            Lcom/google/android/videos/player/UnsupportedSecurityLevelException;,
            Lcom/google/android/videos/player/ProtectedBufferException;
        }
    .end annotation

    .prologue
    .line 657
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->recordTaskStart(I)V

    .line 659
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->config:Lcom/google/android/videos/Config;

    invoke-interface {v4}, Lcom/google/android/videos/Config;->exoUseBlockBufferPool()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 661
    :try_start_0
    new-instance v3, Lcom/google/android/videos/player/exo/BlockBufferPool;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->config:Lcom/google/android/videos/Config;

    invoke-interface {v4}, Lcom/google/android/videos/Config;->exoBufferChunkSize()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->config:Lcom/google/android/videos/Config;

    invoke-interface {v5}, Lcom/google/android/videos/Config;->exoBufferChunkCount()I

    move-result v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/videos/player/exo/BlockBufferPool;-><init>(II)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 670
    .local v3, "allocator":Lcom/google/android/exoplayer/upstream/Allocator;
    :goto_0
    new-instance v2, Lcom/google/android/exoplayer/DefaultLoadControl;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->config:Lcom/google/android/videos/Config;

    invoke-interface {v4}, Lcom/google/android/videos/Config;->exoLowWatermarkMs()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->config:Lcom/google/android/videos/Config;

    invoke-interface {v4}, Lcom/google/android/videos/Config;->exoHighWatermarkMs()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->config:Lcom/google/android/videos/Config;

    invoke-interface {v4}, Lcom/google/android/videos/Config;->exoLowPoolLoad()F

    move-result v8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->config:Lcom/google/android/videos/Config;

    invoke-interface {v4}, Lcom/google/android/videos/Config;->exoHighPoolLoad()F

    move-result v9

    move-object/from16 v4, p0

    move-object/from16 v5, p0

    invoke-direct/range {v2 .. v9}, Lcom/google/android/exoplayer/DefaultLoadControl;-><init>(Lcom/google/android/exoplayer/upstream/Allocator;Landroid/os/Handler;Lcom/google/android/exoplayer/DefaultLoadControl$EventListener;IIFF)V

    .line 674
    .local v2, "loadControl":Lcom/google/android/exoplayer/LoadControl;
    const/16 v18, 0x0

    .line 675
    .local v18, "filterHdContent":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-boolean v4, v4, Lcom/google/android/videos/player/VideoInfo;->isEncrypted:Z

    if-eqz v4, :cond_1

    .line 676
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->drmSessionManager:Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;

    if-nez v4, :cond_0

    .line 677
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-object v5, v4, Lcom/google/android/videos/player/VideoInfo;->account:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-object v6, v4, Lcom/google/android/videos/player/VideoInfo;->videoId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-boolean v7, v4, Lcom/google/android/videos/player/VideoInfo;->isOffline:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget v8, v4, Lcom/google/android/videos/player/VideoInfo;->cencSecurityLevel:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-object v9, v4, Lcom/google/android/videos/player/VideoInfo;->cencKeySetId:[B

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v9}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->maybeCreateDrmSessionManager(Ljava/lang/String;Ljava/lang/String;ZI[B)V

    .line 680
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->drmSessionManager:Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;

    invoke-virtual {v4}, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->getSecurityLevel()I

    move-result v20

    .line 683
    .local v20, "sessionSecurityLevel":I
    const/4 v4, 0x1

    move/from16 v0, v20

    if-eq v0, v4, :cond_4

    const/16 v18, 0x1

    .line 686
    .end local v20    # "sessionSecurityLevel":I
    :cond_1
    :goto_1
    const/4 v9, 0x0

    .line 687
    .local v9, "frameReleaseHelper":Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer$FrameReleaseTimeHelper;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->config:Lcom/google/android/videos/Config;

    invoke-interface {v4}, Lcom/google/android/videos/Config;->exoSmoothFrameRelease()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 688
    new-instance v9, Lcom/google/android/exoplayer/SmoothFrameReleaseTimeHelper;

    .end local v9    # "frameReleaseHelper":Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer$FrameReleaseTimeHelper;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->activity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Display;->getRefreshRate()F

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->config:Lcom/google/android/videos/Config;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->display:Landroid/view/Display;

    invoke-interface {v5, v6}, Lcom/google/android/videos/Config;->exoVsyncFrameRelease(Landroid/view/Display;)Z

    move-result v5

    invoke-direct {v9, v4, v5}, Lcom/google/android/exoplayer/SmoothFrameReleaseTimeHelper;-><init>(FZ)V

    .line 693
    .restart local v9    # "frameReleaseHelper":Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer$FrameReleaseTimeHelper;
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-boolean v4, v4, Lcom/google/android/videos/player/VideoInfo;->isOffline:Z

    if-eqz v4, :cond_5

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->buildOfflineSources(Lcom/google/android/exoplayer/LoadControl;)Landroid/util/Pair;

    move-result-object v21

    .line 696
    .local v21, "sources":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/exoplayer/SampleSource;Lcom/google/android/exoplayer/SampleSource;>;"
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->config:Lcom/google/android/videos/Config;

    invoke-interface {v4}, Lcom/google/android/videos/Config;->exoPlayClearSamplesWithoutKeys()Z

    move-result v7

    .line 697
    .local v7, "playClearSamplesWithoutKeys":Z
    const/4 v4, 0x3

    new-array v0, v4, [Lcom/google/android/exoplayer/TrackRenderer;

    move-object/from16 v19, v0

    .line 698
    .local v19, "renderers":[Lcom/google/android/exoplayer/TrackRenderer;
    new-instance v4, Lcom/google/android/videos/player/exo/VideoRenderer;

    move-object/from16 v0, v21

    iget-object v5, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Lcom/google/android/exoplayer/SampleSource;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->drmSessionManager:Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;

    const/4 v11, 0x0

    const-wide/16 v12, 0x0

    const/4 v14, 0x2

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->adaptive:Z

    move-object/from16 v8, p0

    move-object/from16 v10, p0

    invoke-direct/range {v4 .. v15}, Lcom/google/android/videos/player/exo/VideoRenderer;-><init>(Lcom/google/android/exoplayer/SampleSource;Lcom/google/android/exoplayer/drm/DrmSessionManager;ZLandroid/os/Handler;Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer$FrameReleaseTimeHelper;Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer$EventListener;IJIZ)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videoRenderer:Lcom/google/android/videos/player/exo/VideoRenderer;

    .line 701
    new-instance v10, Lcom/google/android/videos/player/exo/AudioRenderer;

    move-object/from16 v0, v21

    iget-object v11, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v11, Lcom/google/android/exoplayer/SampleSource;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->drmSessionManager:Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->config:Lcom/google/android/videos/Config;

    invoke-interface {v4}, Lcom/google/android/videos/Config;->exoAudioTrackMinBufferMultiplicationFactor()F

    move-result v14

    move v13, v7

    move-object/from16 v15, p0

    move-object/from16 v16, p0

    invoke-direct/range {v10 .. v16}, Lcom/google/android/videos/player/exo/AudioRenderer;-><init>(Lcom/google/android/exoplayer/SampleSource;Lcom/google/android/exoplayer/drm/DrmSessionManager;ZFLandroid/os/Handler;Lcom/google/android/exoplayer/MediaCodecAudioTrackRenderer$EventListener;)V

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->audioRenderer:Lcom/google/android/exoplayer/TrackRenderer;

    .line 704
    new-instance v4, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->activity:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->subtitlesOverlay:Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    invoke-direct {v4, v5, v6}, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;-><init>(Landroid/os/Looper;Lcom/google/android/videos/player/overlay/SubtitlesOverlay;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->subtitleRenderer:Lcom/google/android/exoplayer/TrackRenderer;

    .line 705
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videoRenderer:Lcom/google/android/videos/player/exo/VideoRenderer;

    aput-object v5, v19, v4

    .line 706
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->audioRenderer:Lcom/google/android/exoplayer/TrackRenderer;

    aput-object v5, v19, v4

    .line 707
    const/4 v4, 0x2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->subtitleRenderer:Lcom/google/android/exoplayer/TrackRenderer;

    aput-object v5, v19, v4

    .line 709
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->recordTaskEnd(I)V

    .line 710
    return-object v19

    .line 662
    .end local v2    # "loadControl":Lcom/google/android/exoplayer/LoadControl;
    .end local v3    # "allocator":Lcom/google/android/exoplayer/upstream/Allocator;
    .end local v7    # "playClearSamplesWithoutKeys":Z
    .end local v9    # "frameReleaseHelper":Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer$FrameReleaseTimeHelper;
    .end local v18    # "filterHdContent":Z
    .end local v19    # "renderers":[Lcom/google/android/exoplayer/TrackRenderer;
    .end local v21    # "sources":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/exoplayer/SampleSource;Lcom/google/android/exoplayer/SampleSource;>;"
    :catch_0
    move-exception v17

    .line 663
    .local v17, "e":Ljava/lang/OutOfMemoryError;
    const-string v4, "Failed to allocate block buffer. Falling back to chunked mode"

    invoke-static {v4}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 664
    new-instance v3, Lcom/google/android/exoplayer/upstream/BufferPool;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->config:Lcom/google/android/videos/Config;

    invoke-interface {v4}, Lcom/google/android/videos/Config;->exoBufferChunkSize()I

    move-result v4

    invoke-direct {v3, v4}, Lcom/google/android/exoplayer/upstream/BufferPool;-><init>(I)V

    .line 665
    .restart local v3    # "allocator":Lcom/google/android/exoplayer/upstream/Allocator;
    goto/16 :goto_0

    .line 667
    .end local v3    # "allocator":Lcom/google/android/exoplayer/upstream/Allocator;
    .end local v17    # "e":Ljava/lang/OutOfMemoryError;
    :cond_3
    new-instance v3, Lcom/google/android/exoplayer/upstream/BufferPool;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->config:Lcom/google/android/videos/Config;

    invoke-interface {v4}, Lcom/google/android/videos/Config;->exoBufferChunkSize()I

    move-result v4

    invoke-direct {v3, v4}, Lcom/google/android/exoplayer/upstream/BufferPool;-><init>(I)V

    .restart local v3    # "allocator":Lcom/google/android/exoplayer/upstream/Allocator;
    goto/16 :goto_0

    .line 683
    .restart local v2    # "loadControl":Lcom/google/android/exoplayer/LoadControl;
    .restart local v18    # "filterHdContent":Z
    .restart local v20    # "sessionSecurityLevel":I
    :cond_4
    const/16 v18, 0x0

    goto/16 :goto_1

    .line 693
    .end local v20    # "sessionSecurityLevel":I
    .restart local v9    # "frameReleaseHelper":Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer$FrameReleaseTimeHelper;
    :cond_5
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1, v2}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->buildOnlineSources(ZLcom/google/android/exoplayer/LoadControl;)Landroid/util/Pair;

    move-result-object v21

    goto/16 :goto_2
.end method

.method private closeDrm()V
    .locals 4

    .prologue
    .line 945
    iget-boolean v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->openedDrmSessionManager:Z

    if-eqz v0, :cond_0

    .line 946
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->openedDrmSessionManager:Z

    .line 947
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->player:Lcom/google/android/exoplayer/ExoPlayer;

    iget-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->drmSessionManager:Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/exoplayer/ExoPlayer;->sendMessage(Lcom/google/android/exoplayer/ExoPlayer$ExoPlayerComponent;ILjava/lang/Object;)V

    .line 949
    :cond_0
    return-void
.end method

.method private maybeCreateDrmSessionManager(Ljava/lang/String;Ljava/lang/String;ZI[B)V
    .locals 11
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "isOffline"    # Z
    .param p4, "cencSecurityLevel"    # I
    .param p5, "cencKeySetId"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/player/ProtectedBufferException;,
            Landroid/media/UnsupportedSchemeException;,
            Lcom/google/android/videos/player/UnsupportedSecurityLevelException;
        }
    .end annotation

    .prologue
    .line 716
    iget-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->drmSessionManager:Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;

    if-eqz v1, :cond_0

    .line 749
    :goto_0
    return-void

    .line 721
    :cond_0
    if-eqz p3, :cond_1

    move v8, p4

    .line 724
    .local v8, "forcedSecurityLevel":I
    :goto_1
    iget-boolean v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->displaySupportsProtectedBuffers:Z

    if-nez v1, :cond_3

    .line 725
    if-lez v8, :cond_2

    const/4 v1, 0x3

    if-eq v8, v1, :cond_2

    .line 728
    new-instance v1, Lcom/google/android/videos/player/ProtectedBufferException;

    invoke-direct {v1}, Lcom/google/android/videos/player/ProtectedBufferException;-><init>()V

    throw v1

    .line 721
    .end local v8    # "forcedSecurityLevel":I
    :cond_1
    const/4 v8, 0x0

    goto :goto_1

    .line 730
    .restart local v8    # "forcedSecurityLevel":I
    :cond_2
    const/4 v8, 0x3

    .line 734
    :cond_3
    new-instance v0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;

    iget-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    iget-object v2, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->player:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v2}, Lcom/google/android/exoplayer/ExoPlayer;->getPlaybackLooper()Landroid/os/Looper;

    move-result-object v5

    iget-object v9, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p5

    move-object v6, p0

    move-object v7, p0

    invoke-direct/range {v0 .. v9}, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;-><init>(Lcom/google/android/videos/VideosGlobals;Ljava/lang/String;Ljava/lang/String;[BLandroid/os/Looper;Landroid/os/Handler;Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$EventListener;ILcom/google/android/videos/player/logging/PlaybackPreparationLogger;)V

    .line 738
    .local v0, "drmSessionManager":Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;
    invoke-virtual {v0}, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->getSecurityLevel()I

    move-result v10

    .line 739
    .local v10, "sessionSecurityLevel":I
    if-lez v8, :cond_4

    if-eq v10, v8, :cond_4

    .line 743
    new-instance v1, Lcom/google/android/videos/player/UnsupportedSecurityLevelException;

    iget-object v2, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-boolean v2, v2, Lcom/google/android/videos/player/VideoInfo;->isOffline:Z

    invoke-direct {v1, v10, v8, v2}, Lcom/google/android/videos/player/UnsupportedSecurityLevelException;-><init>(IIZ)V

    throw v1

    .line 748
    :cond_4
    iput-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->drmSessionManager:Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;

    goto :goto_0
.end method

.method private prepareInternal(I)V
    .locals 6
    .param p1, "realInitialTrigger"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 256
    iput p1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->realInitialTrigger:I

    .line 257
    iput-boolean v3, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->prepared:Z

    .line 258
    iput-object v2, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->onlineAudioChunkSource:Lcom/google/android/exoplayer/chunk/MultiTrackChunkSource;

    .line 259
    iput-object v2, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->onlineVideoChunkSource:Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;

    .line 260
    iput-object v2, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->offlineVideoChunkSource:Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;

    .line 261
    iput-object v2, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->audioTrackInfos:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    .line 262
    iput-object v2, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->audioRepresentations:[Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    .line 267
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->buildRenderers()[Lcom/google/android/exoplayer/TrackRenderer;
    :try_end_0
    .catch Landroid/media/UnsupportedSchemeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/videos/player/UnsupportedSecurityLevelException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/videos/player/ProtectedBufferException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/videos/streams/MissingStreamException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_4

    move-result-object v1

    .line 284
    .local v1, "renderers":[Lcom/google/android/exoplayer/TrackRenderer;
    iget-boolean v2, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->hqToggleSupported:Z

    if-eqz v2, :cond_0

    move v2, v3

    :goto_0
    iget-boolean v5, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->isHqHd:Z

    if-eqz v5, :cond_1

    :goto_1
    invoke-virtual {p0, v4, v2, v3}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->obtainMessage(III)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    .line 285
    invoke-direct {p0}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->pushEnableVirtualizer()V

    .line 286
    invoke-direct {p0}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->pushEnableTrickMode()V

    .line 287
    invoke-direct {p0}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->pushSubtitles()V

    .line 288
    invoke-direct {p0}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->pushAudioTrackSelection()V

    .line 289
    invoke-direct {p0}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->pushSurface()V

    .line 290
    iget-object v2, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->player:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v2, v1}, Lcom/google/android/exoplayer/ExoPlayer;->prepare([Lcom/google/android/exoplayer/TrackRenderer;)V

    .line 291
    .end local v1    # "renderers":[Lcom/google/android/exoplayer/TrackRenderer;
    :goto_2
    return-void

    .line 268
    :catch_0
    move-exception v0

    .line 269
    .local v0, "e":Landroid/media/UnsupportedSchemeException;
    invoke-virtual {p0, v3, v0}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto :goto_2

    .line 271
    .end local v0    # "e":Landroid/media/UnsupportedSchemeException;
    :catch_1
    move-exception v0

    .line 272
    .local v0, "e":Lcom/google/android/videos/player/UnsupportedSecurityLevelException;
    invoke-virtual {p0, v3, v0}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto :goto_2

    .line 274
    .end local v0    # "e":Lcom/google/android/videos/player/UnsupportedSecurityLevelException;
    :catch_2
    move-exception v0

    .line 275
    .local v0, "e":Lcom/google/android/videos/player/ProtectedBufferException;
    invoke-virtual {p0, v3, v0}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto :goto_2

    .line 277
    .end local v0    # "e":Lcom/google/android/videos/player/ProtectedBufferException;
    :catch_3
    move-exception v0

    .line 278
    .local v0, "e":Lcom/google/android/videos/streams/MissingStreamException;
    invoke-virtual {p0, v3, v0}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto :goto_2

    .line 280
    .end local v0    # "e":Lcom/google/android/videos/streams/MissingStreamException;
    :catch_4
    move-exception v0

    .line 281
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {p0, v3, v0}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto :goto_2

    .end local v0    # "e":Ljava/lang/RuntimeException;
    .restart local v1    # "renderers":[Lcom/google/android/exoplayer/TrackRenderer;
    :cond_0
    move v2, v4

    .line 284
    goto :goto_0

    :cond_1
    move v3, v4

    goto :goto_1
.end method

.method private pushAudioTrackSelection()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 925
    iget-boolean v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->prepared:Z

    if-eqz v1, :cond_1

    .line 928
    iget-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->onlineVideoChunkSource:Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;

    if-eqz v1, :cond_0

    .line 929
    iget-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->player:Lcom/google/android/exoplayer/ExoPlayer;

    iget-object v2, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->onlineVideoChunkSource:Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;

    iget-object v3, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->audioRepresentations:[Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    iget v4, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->selectedAudioTrackIndex:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->format:Lcom/google/android/exoplayer/chunk/Format;

    iget v3, v3, Lcom/google/android/exoplayer/chunk/Format;->bitrate:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v5, v3}, Lcom/google/android/exoplayer/ExoPlayer;->sendMessage(Lcom/google/android/exoplayer/ExoPlayer$ExoPlayerComponent;ILjava/lang/Object;)V

    .line 932
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->onlineAudioChunkSource:Lcom/google/android/exoplayer/chunk/MultiTrackChunkSource;

    if-eqz v1, :cond_1

    .line 933
    iget-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->player:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v1}, Lcom/google/android/exoplayer/ExoPlayer;->getPlayWhenReady()Z

    move-result v0

    .line 934
    .local v0, "playWhenReady":Z
    iget-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->player:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v1, v6}, Lcom/google/android/exoplayer/ExoPlayer;->setPlayWhenReady(Z)V

    .line 935
    iget-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->player:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v1, v5, v6}, Lcom/google/android/exoplayer/ExoPlayer;->setRendererEnabled(IZ)V

    .line 936
    iget-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->player:Lcom/google/android/exoplayer/ExoPlayer;

    iget-object v2, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->onlineAudioChunkSource:Lcom/google/android/exoplayer/chunk/MultiTrackChunkSource;

    iget v3, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->selectedAudioTrackIndex:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v5, v3}, Lcom/google/android/exoplayer/ExoPlayer;->sendMessage(Lcom/google/android/exoplayer/ExoPlayer$ExoPlayerComponent;ILjava/lang/Object;)V

    .line 938
    iget-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->player:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v1, v5, v5}, Lcom/google/android/exoplayer/ExoPlayer;->setRendererEnabled(IZ)V

    .line 939
    iget-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->player:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v1, v0}, Lcom/google/android/exoplayer/ExoPlayer;->setPlayWhenReady(Z)V

    .line 942
    .end local v0    # "playWhenReady":Z
    :cond_1
    return-void
.end method

.method private pushEnableTrickMode()V
    .locals 4

    .prologue
    .line 912
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->onlineVideoChunkSource:Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;

    if-eqz v0, :cond_0

    .line 913
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->player:Lcom/google/android/exoplayer/ExoPlayer;

    iget-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->onlineVideoChunkSource:Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;

    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->enableTrickMode:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/exoplayer/ExoPlayer;->sendMessage(Lcom/google/android/exoplayer/ExoPlayer$ExoPlayerComponent;ILjava/lang/Object;)V

    .line 916
    :cond_0
    return-void
.end method

.method private pushEnableVirtualizer()V
    .locals 4

    .prologue
    .line 906
    iget-boolean v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->prepared:Z

    if-eqz v0, :cond_0

    .line 907
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->player:Lcom/google/android/exoplayer/ExoPlayer;

    iget-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->audioRenderer:Lcom/google/android/exoplayer/TrackRenderer;

    const/4 v2, 0x2

    iget-boolean v3, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->enableVirtualizer:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/exoplayer/ExoPlayer;->sendMessage(Lcom/google/android/exoplayer/ExoPlayer$ExoPlayerComponent;ILjava/lang/Object;)V

    .line 909
    :cond_0
    return-void
.end method

.method private pushSubtitles()V
    .locals 4

    .prologue
    .line 919
    iget-boolean v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->prepared:Z

    if-eqz v0, :cond_0

    .line 920
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->player:Lcom/google/android/exoplayer/ExoPlayer;

    iget-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->subtitleRenderer:Lcom/google/android/exoplayer/TrackRenderer;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->subtitles:Lcom/google/android/videos/subtitles/Subtitles;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/exoplayer/ExoPlayer;->sendMessage(Lcom/google/android/exoplayer/ExoPlayer$ExoPlayerComponent;ILjava/lang/Object;)V

    .line 922
    :cond_0
    return-void
.end method

.method private pushSurface()V
    .locals 4

    .prologue
    .line 899
    iget-boolean v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->prepared:Z

    if-eqz v0, :cond_0

    .line 900
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->player:Lcom/google/android/exoplayer/ExoPlayer;

    iget-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videoRenderer:Lcom/google/android/videos/player/exo/VideoRenderer;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->playerSurface:Lcom/google/android/videos/player/PlayerSurface;

    invoke-interface {v3}, Lcom/google/android/videos/player/PlayerSurface;->getSurfaceHolder()Landroid/view/SurfaceHolder;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/exoplayer/ExoPlayer;->sendMessage(Lcom/google/android/exoplayer/ExoPlayer$ExoPlayerComponent;ILjava/lang/Object;)V

    .line 903
    :cond_0
    return-void
.end method

.method private setHq(ZZ)V
    .locals 2
    .param p1, "hq"    # Z
    .param p2, "wasAutomaticDrop"    # Z

    .prologue
    .line 425
    iget-boolean v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->hq:Z

    if-ne v0, p1, :cond_1

    .line 441
    :cond_0
    :goto_0
    return-void

    .line 428
    :cond_1
    iput-boolean p1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->hq:Z

    .line 429
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->listener:Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;

    iget-boolean v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->isHqHd:Z

    invoke-interface {v0, p0, v1, p1, p2}, Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;->onExoHqToggled(Lcom/google/android/videos/player/exo/ExoVideosPlayer;ZZZ)V

    .line 430
    iget-boolean v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->adaptive:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->prepared:Z

    if-eqz v0, :cond_0

    .line 434
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->prepared:Z

    .line 435
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->playerSurface:Lcom/google/android/videos/player/PlayerSurface;

    if-eqz v0, :cond_2

    .line 436
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->playerSurface:Lcom/google/android/videos/player/PlayerSurface;

    invoke-interface {v0}, Lcom/google/android/videos/player/PlayerSurface;->closeShutter()V

    .line 438
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->player:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v0}, Lcom/google/android/exoplayer/ExoPlayer;->stop()V

    .line 439
    if-eqz p2, :cond_3

    const/4 v0, 0x2

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->prepareInternal(I)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private updateStablePausedState()V
    .locals 3

    .prologue
    .line 888
    invoke-virtual {p0}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->getPlaybackState()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->getPlayWhenReady()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->player:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v1}, Lcom/google/android/exoplayer/ExoPlayer;->isPlayWhenReadyCommitted()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 890
    .local v0, "stablePausedState":Z
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->stablePausedState:Z

    if-eq v1, v0, :cond_0

    .line 891
    iput-boolean v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->stablePausedState:Z

    .line 892
    if-eqz v0, :cond_0

    .line 893
    iget-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->listener:Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;

    iget-object v2, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videoRenderer:Lcom/google/android/videos/player/exo/VideoRenderer;

    invoke-virtual {v2}, Lcom/google/android/videos/player/exo/VideoRenderer;->getLastFrameTimestampMs()I

    move-result v2

    invoke-interface {v1, p0, v2}, Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;->onExoPausedFrameTimestamp(Lcom/google/android/videos/player/exo/ExoVideosPlayer;I)V

    .line 896
    :cond_0
    return-void

    .line 888
    .end local v0    # "stablePausedState":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getAudioTracks()[Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->audioTrackInfos:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    return-object v0
.end method

.method public getBufferedPercentage()I
    .locals 6

    .prologue
    .line 321
    iget-object v2, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->player:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v2}, Lcom/google/android/exoplayer/ExoPlayer;->getBufferedPercentage()I

    move-result v0

    .line 322
    .local v0, "bufferedPercentage":I
    iget-object v2, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->offlineVideoChunkSource:Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;

    if-eqz v2, :cond_0

    .line 323
    iget-object v2, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->offlineVideoChunkSource:Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;

    invoke-virtual {v2}, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->getCachedPosition()I

    move-result v1

    .line 324
    .local v1, "cachedPosition":I
    const/4 v2, -0x2

    if-ne v1, v2, :cond_1

    .line 325
    const/16 v0, 0x64

    .line 331
    .end local v1    # "cachedPosition":I
    :cond_0
    :goto_0
    return v0

    .line 326
    .restart local v1    # "cachedPosition":I
    :cond_1
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 327
    mul-int/lit8 v2, v1, 0x64

    int-to-long v2, v2

    iget-object v4, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->player:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v4}, Lcom/google/android/exoplayer/ExoPlayer;->getDuration()J

    move-result-wide v4

    div-long/2addr v2, v4

    long-to-int v2, v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method

.method public getCurrentPosition()I
    .locals 2

    .prologue
    .line 316
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->player:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v0}, Lcom/google/android/exoplayer/ExoPlayer;->getCurrentPosition()J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public getHq()Z
    .locals 1

    .prologue
    .line 416
    iget-boolean v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->hq:Z

    return v0
.end method

.method public getPlayWhenReady()Z
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->player:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v0}, Lcom/google/android/exoplayer/ExoPlayer;->getPlayWhenReady()Z

    move-result v0

    return v0
.end method

.method public getPlaybackState()I
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->player:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v0}, Lcom/google/android/exoplayer/ExoPlayer;->getPlaybackState()I

    move-result v0

    return v0
.end method

.method public getSelectedAudioTrack()I
    .locals 1

    .prologue
    .line 400
    iget v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->selectedAudioTrackIndex:I

    return v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 373
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 384
    :goto_0
    return-void

    .line 375
    :pswitch_0
    iget-object v3, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->listener:Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;

    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    iget v4, p1, Landroid/os/Message;->arg2:I

    if-eqz v4, :cond_1

    :goto_2
    invoke-interface {v3, p0, v0, v1}, Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;->onExoInitialized(Lcom/google/android/videos/player/exo/ExoVideosPlayer;ZZ)V

    goto :goto_0

    :cond_0
    move v0, v2

    goto :goto_1

    :cond_1
    move v1, v2

    goto :goto_2

    .line 378
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->listener:Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Exception;

    invoke-interface {v1, p0, v0}, Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;->onExoInitializationError(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Ljava/lang/Exception;)V

    goto :goto_0

    .line 381
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->listener:Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v0, v1}, Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;->onExoReleased(I)V

    goto :goto_0

    .line 373
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onAudioTrackInitializationError(Lcom/google/android/exoplayer/audio/AudioTrack$InitializationException;)V
    .locals 1
    .param p1, "e"    # Lcom/google/android/exoplayer/audio/AudioTrack$InitializationException;

    .prologue
    .line 618
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->listener:Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;

    invoke-interface {v0, p0, p1}, Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;->onExoInternalAudioTrackInitializationError(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Lcom/google/android/exoplayer/audio/AudioTrack$InitializationException;)V

    .line 619
    return-void
.end method

.method public onBandwidthSample(IJJ)V
    .locals 8
    .param p1, "elapsedMs"    # I
    .param p2, "bytes"    # J
    .param p4, "bitrateEstimate"    # J

    .prologue
    .line 525
    iget-boolean v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->prepared:Z

    if-eqz v0, :cond_0

    .line 526
    iget-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->listener:Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;

    move-object v2, p0

    move v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-interface/range {v1 .. v7}, Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;->onExoInternalBandwidthSample(Lcom/google/android/videos/player/exo/ExoVideosPlayer;IJJ)V

    .line 531
    :cond_0
    return-void
.end method

.method public onConsumptionError(ILjava/io/IOException;)V
    .locals 1
    .param p1, "sourceId"    # I
    .param p2, "e"    # Ljava/io/IOException;

    .prologue
    .line 578
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->listener:Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;

    invoke-interface {v0, p0, p2}, Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;->onExoInternalConsumptionError(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Ljava/io/IOException;)V

    .line 579
    return-void
.end method

.method public onCryptoError(Landroid/media/MediaCodec$CryptoException;)V
    .locals 2
    .param p1, "e"    # Landroid/media/MediaCodec$CryptoException;

    .prologue
    .line 611
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->listener:Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;

    invoke-virtual {p1}, Landroid/media/MediaCodec$CryptoException;->getErrorCode()I

    move-result v1

    invoke-interface {v0, p0, p1, v1}, Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;->onExoInternalCryptoError(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Ljava/lang/Exception;I)V

    .line 612
    return-void
.end method

.method public onDecoderInitializationError(Lcom/google/android/exoplayer/MediaCodecTrackRenderer$DecoderInitializationException;)V
    .locals 1
    .param p1, "e"    # Lcom/google/android/exoplayer/MediaCodecTrackRenderer$DecoderInitializationException;

    .prologue
    .line 606
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->listener:Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;

    invoke-interface {v0, p0, p1}, Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;->onExoInternalDecoderInitializationError(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Lcom/google/android/exoplayer/MediaCodecTrackRenderer$DecoderInitializationException;)V

    .line 607
    return-void
.end method

.method public onDownstreamDiscarded(IIIJ)V
    .locals 0
    .param p1, "sourceId"    # I
    .param p2, "mediaStartTimeMs"    # I
    .param p3, "mediaEndTimeMs"    # I
    .param p4, "totalBytes"    # J

    .prologue
    .line 593
    return-void
.end method

.method public onDownstreamFormatChanged(ILjava/lang/String;II)V
    .locals 2
    .param p1, "sourceId"    # I
    .param p2, "formatId"    # Ljava/lang/String;
    .param p3, "trigger"    # I
    .param p4, "mediaTimeMs"    # I

    .prologue
    .line 584
    if-nez p1, :cond_0

    .line 585
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->listener:Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, p0, v1, p3}, Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;->onExoInternalFormatEnabled(Lcom/google/android/videos/player/exo/ExoVideosPlayer;II)V

    .line 587
    :cond_0
    return-void
.end method

.method public onDrawnToSurface(Landroid/view/Surface;)V
    .locals 1
    .param p1, "surface"    # Landroid/view/Surface;

    .prologue
    .line 625
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->playerSurface:Lcom/google/android/videos/player/PlayerSurface;

    invoke-interface {v0}, Lcom/google/android/videos/player/PlayerSurface;->getSurfaceHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 626
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->playerSurface:Lcom/google/android/videos/player/PlayerSurface;

    invoke-interface {v0}, Lcom/google/android/videos/player/PlayerSurface;->openShutter()V

    .line 628
    :cond_0
    return-void
.end method

.method public onDroppedFrames(IJ)V
    .locals 1
    .param p1, "count"    # I
    .param p2, "elapsedMs"    # J

    .prologue
    .line 638
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->listener:Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;

    invoke-interface {v0, p0, p1}, Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;->onExoDroppedFrames(Lcom/google/android/videos/player/exo/ExoVideosPlayer;I)V

    .line 639
    return-void
.end method

.method public onLoadCanceled(IJ)V
    .locals 0
    .param p1, "sourceId"    # I
    .param p2, "bytesLoaded"    # J

    .prologue
    .line 563
    return-void
.end method

.method public onLoadCompleted(IJ)V
    .locals 0
    .param p1, "sourceId"    # I
    .param p2, "bytesLoaded"    # J

    .prologue
    .line 558
    return-void
.end method

.method public onLoadStarted(ILjava/lang/String;IZIIJ)V
    .locals 2
    .param p1, "sourceId"    # I
    .param p2, "formatId"    # Ljava/lang/String;
    .param p3, "trigger"    # I
    .param p4, "isInitialization"    # Z
    .param p5, "mediaStartTimeMs"    # I
    .param p6, "mediaEndTimeMs"    # I
    .param p7, "totalBytes"    # J

    .prologue
    .line 545
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 546
    .local v0, "itag":I
    if-nez p1, :cond_1

    iget v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->selectedItag:I

    if-eq v0, v1, :cond_1

    .line 547
    if-nez p3, :cond_0

    .line 548
    iget p3, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->realInitialTrigger:I

    .line 550
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->listener:Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;

    invoke-interface {v1, p0, v0, p3}, Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;->onExoInternalFormatSelected(Lcom/google/android/videos/player/exo/ExoVideosPlayer;II)V

    .line 551
    iput v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->selectedItag:I

    .line 553
    :cond_1
    return-void
.end method

.method public onLoadingChanged(Z)V
    .locals 1
    .param p1, "loading"    # Z

    .prologue
    .line 537
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->listener:Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;

    invoke-interface {v0, p0, p1}, Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;->onExoInternalLoadingChanged(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Z)V

    .line 538
    return-void
.end method

.method public onOpened(J)V
    .locals 1
    .param p1, "elapsedTimeMillis"    # J

    .prologue
    .line 645
    iget-boolean v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->prepared:Z

    if-eqz v0, :cond_0

    .line 646
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->listener:Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;

    invoke-interface {v0, p0, p1, p2}, Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;->onExoHttpDataSourceOpened(Lcom/google/android/videos/player/exo/ExoVideosPlayer;J)V

    .line 651
    :cond_0
    return-void
.end method

.method public onPlayWhenReadyCommitted()V
    .locals 0

    .prologue
    .line 477
    invoke-direct {p0}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->updateStablePausedState()V

    .line 478
    return-void
.end method

.method public onPlayerError(Lcom/google/android/exoplayer/ExoPlaybackException;)V
    .locals 12
    .param p1, "error"    # Lcom/google/android/exoplayer/ExoPlaybackException;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 483
    iget-boolean v2, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->openedDrmSessionManager:Z

    .line 484
    .local v2, "hadDrmSessionOpen":Z
    invoke-direct {p0}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->closeDrm()V

    .line 486
    invoke-virtual {p1}, Lcom/google/android/exoplayer/ExoPlaybackException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 487
    .local v0, "cause":Ljava/lang/Throwable;
    if-eqz v0, :cond_5

    .line 488
    const/4 v4, 0x0

    .line 489
    .local v4, "retry":Z
    instance-of v7, v0, Ljava/lang/RuntimeException;

    if-eqz v7, :cond_3

    .line 491
    iget-object v7, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->listener:Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;

    check-cast v0, Ljava/lang/RuntimeException;

    .end local v0    # "cause":Ljava/lang/Throwable;
    invoke-interface {v7, p0, v0}, Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;->onExoInternalRuntimeError(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Ljava/lang/RuntimeException;)V

    .line 493
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    iget-wide v10, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->lastRetrySystemTimeMs:J

    sub-long/2addr v8, v10

    const-wide/32 v10, 0x493e0

    cmp-long v7, v8, v10

    if-lez v7, :cond_1

    move v3, v5

    .line 495
    .local v3, "resetRetryCount":Z
    :goto_0
    if-eqz v3, :cond_2

    :goto_1
    iput v5, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->retries:I

    .line 496
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->lastRetrySystemTimeMs:J

    .line 497
    iget v5, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->retries:I

    const/4 v7, 0x3

    if-gt v5, v7, :cond_0

    .line 498
    const/4 v4, 0x1

    .line 508
    .end local v3    # "resetRetryCount":Z
    :cond_0
    :goto_2
    if-eqz v4, :cond_5

    .line 509
    iput-boolean v6, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->prepared:Z

    .line 510
    iget-object v5, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->playerSurface:Lcom/google/android/videos/player/PlayerSurface;

    invoke-interface {v5}, Lcom/google/android/videos/player/PlayerSurface;->closeShutter()V

    .line 511
    const/4 v5, 0x2

    invoke-direct {p0, v5}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->prepareInternal(I)V

    .line 519
    .end local v4    # "retry":Z
    :goto_3
    return-void

    .restart local v4    # "retry":Z
    :cond_1
    move v3, v6

    .line 493
    goto :goto_0

    .line 495
    .restart local v3    # "resetRetryCount":Z
    :cond_2
    iget v5, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->retries:I

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 500
    .end local v3    # "resetRetryCount":Z
    .restart local v0    # "cause":Ljava/lang/Throwable;
    :cond_3
    if-eqz v2, :cond_0

    .line 501
    instance-of v5, v0, Lcom/google/android/videos/api/CencLicenseException;

    if-eqz v5, :cond_4

    move-object v1, v0

    .line 502
    check-cast v1, Lcom/google/android/videos/api/CencLicenseException;

    .line 503
    .local v1, "cencLicenseException":Lcom/google/android/videos/api/CencLicenseException;
    iget-object v5, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->config:Lcom/google/android/videos/Config;

    invoke-interface {v5}, Lcom/google/android/videos/Config;->retryCencDrmErrorCodes()Ljava/util/List;

    move-result-object v5

    iget v7, v1, Lcom/google/android/videos/api/CencLicenseException;->statusCode:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    .line 504
    goto :goto_2

    .line 505
    .end local v1    # "cencLicenseException":Lcom/google/android/videos/api/CencLicenseException;
    :cond_4
    instance-of v4, v0, Landroid/media/MediaCodec$CryptoException;

    goto :goto_2

    .line 517
    .end local v0    # "cause":Ljava/lang/Throwable;
    .end local v4    # "retry":Z
    :cond_5
    iput v6, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->retries:I

    .line 518
    iget-object v5, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->listener:Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;

    invoke-interface {v5, p0, p1}, Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;->onExoPlayerError(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Lcom/google/android/exoplayer/ExoPlaybackException;)V

    goto :goto_3
.end method

.method public onPlayerStateChanged(ZI)V
    .locals 2
    .param p1, "playWhenReady"    # Z
    .param p2, "playbackState"    # I

    .prologue
    const/4 v1, 0x0

    .line 467
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->listener:Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;

    invoke-interface {v0, p0, p1, p2}, Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;->onExoPlayerStateChanged(Lcom/google/android/videos/player/exo/ExoVideosPlayer;ZI)V

    .line 468
    invoke-direct {p0}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->updateStablePausedState()V

    .line 469
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->qualityDropper:Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->qualityDropper:Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;

    invoke-virtual {v0, p2, v1, p1}, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->onStateChanged(IZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 471
    const/4 v0, 0x1

    invoke-direct {p0, v1, v0}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->setHq(ZZ)V

    .line 473
    :cond_0
    return-void
.end method

.method public onUpstreamDiscarded(IIIJ)V
    .locals 0
    .param p1, "sourceId"    # I
    .param p2, "mediaStartTimeMs"    # I
    .param p3, "mediaEndTimeMs"    # I
    .param p4, "totalBytes"    # J

    .prologue
    .line 569
    return-void
.end method

.method public onUpstreamError(ILjava/io/IOException;)V
    .locals 1
    .param p1, "sourceId"    # I
    .param p2, "e"    # Ljava/io/IOException;

    .prologue
    .line 573
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->listener:Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;

    invoke-interface {v0, p0, p2}, Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;->onExoInternalUpstreamError(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Ljava/io/IOException;)V

    .line 574
    return-void
.end method

.method public onVideoSizeChanged(IIF)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "pixelWidthHeightRatio"    # F

    .prologue
    .line 633
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->playerSurface:Lcom/google/android/videos/player/PlayerSurface;

    invoke-interface {v0, p1, p2}, Lcom/google/android/videos/player/PlayerSurface;->setVideoSize(II)V

    .line 634
    return-void
.end method

.method public onWidevineDrmError(Ljava/lang/Exception;)V
    .locals 1
    .param p1, "e"    # Ljava/lang/Exception;

    .prologue
    .line 599
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->listener:Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;

    invoke-interface {v0, p0, p1}, Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;->onExoInternalWidevineDrmError(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Ljava/lang/Exception;)V

    .line 600
    return-void
.end method

.method public openDrm(Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;ZI[B)V
    .locals 6
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "psshData"    # [B
    .param p4, "mimeType"    # Ljava/lang/String;
    .param p5, "isOffline"    # Z
    .param p6, "cencSecurityLevel"    # I
    .param p7, "cencKeySetId"    # [B

    .prologue
    .line 234
    iget-boolean v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->openedDrmSessionManager:Z

    if-eqz v0, :cond_0

    .line 246
    :goto_0
    return-void

    :cond_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p5

    move v4, p6

    move-object v5, p7

    .line 238
    :try_start_0
    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->maybeCreateDrmSessionManager(Ljava/lang/String;Ljava/lang/String;ZI[B)V

    .line 239
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->openedDrmSessionManager:Z

    .line 240
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->player:Lcom/google/android/exoplayer/ExoPlayer;

    iget-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->drmSessionManager:Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->WIDEVINE_UUID:Ljava/util/UUID;

    invoke-static {v3, p3}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v3

    invoke-static {v3, p4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/exoplayer/ExoPlayer;->sendMessage(Lcom/google/android/exoplayer/ExoPlayer$ExoPlayerComponent;ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 243
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public prepare(Lcom/google/android/videos/player/VideoInfo;)V
    .locals 1
    .param p1, "videoInfo"    # Lcom/google/android/videos/player/VideoInfo;

    .prologue
    .line 250
    iput-object p1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    .line 251
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->selectedItag:I

    .line 252
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->prepareInternal(I)V

    .line 253
    return-void
.end method

.method public release()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 346
    iput-boolean v4, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->prepared:Z

    .line 347
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->playerSurface:Lcom/google/android/videos/player/PlayerSurface;

    if-eqz v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->playerSurface:Lcom/google/android/videos/player/PlayerSurface;

    invoke-interface {v0}, Lcom/google/android/videos/player/PlayerSurface;->closeShutter()V

    .line 350
    :cond_0
    invoke-direct {p0}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->closeDrm()V

    .line 351
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->player:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v0}, Lcom/google/android/exoplayer/ExoPlayer;->release()V

    .line 352
    iput-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videoRenderer:Lcom/google/android/videos/player/exo/VideoRenderer;

    .line 353
    iput-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->audioRenderer:Lcom/google/android/exoplayer/TrackRenderer;

    .line 354
    iput-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->subtitleRenderer:Lcom/google/android/exoplayer/TrackRenderer;

    .line 355
    iput-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->onlineAudioChunkSource:Lcom/google/android/exoplayer/chunk/MultiTrackChunkSource;

    .line 356
    iput-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->onlineVideoChunkSource:Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;

    .line 357
    iput-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->offlineVideoChunkSource:Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;

    .line 358
    iput-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->audioTrackInfos:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    .line 359
    iput-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->audioRepresentations:[Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    .line 362
    invoke-virtual {p0, v1}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 368
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->player:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v1}, Lcom/google/android/exoplayer/ExoPlayer;->getCurrentPosition()J

    move-result-wide v2

    long-to-int v1, v2

    invoke-virtual {p0, v0, v1, v4}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 369
    return-void
.end method

.method public seekTo(I)V
    .locals 4
    .param p1, "timeMillis"    # I

    .prologue
    .line 336
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->player:Lcom/google/android/exoplayer/ExoPlayer;

    int-to-long v2, p1

    invoke-interface {v0, v2, v3}, Lcom/google/android/exoplayer/ExoPlayer;->seekTo(J)V

    .line 337
    return-void
.end method

.method public seekTo(IZ)V
    .locals 0
    .param p1, "timeMillis"    # I
    .param p2, "ensureAccurateTime"    # Z

    .prologue
    .line 341
    invoke-virtual {p0, p1}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->seekTo(I)V

    .line 342
    return-void
.end method

.method public setHq(Z)V
    .locals 1
    .param p1, "hq"    # Z

    .prologue
    .line 421
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->setHq(ZZ)V

    .line 422
    return-void
.end method

.method public setPlayWhenReady(Z)V
    .locals 1
    .param p1, "playWhenReady"    # Z

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->player:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer/ExoPlayer;->setPlayWhenReady(Z)V

    .line 301
    invoke-direct {p0}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->updateStablePausedState()V

    .line 302
    return-void
.end method

.method public setSelectedAudioTrack(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 394
    iput p1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->selectedAudioTrackIndex:I

    .line 395
    invoke-direct {p0}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->pushAudioTrackSelection()V

    .line 396
    return-void
.end method

.method public setSubtitles(Lcom/google/android/videos/subtitles/Subtitles;)V
    .locals 0
    .param p1, "subtitles"    # Lcom/google/android/videos/subtitles/Subtitles;

    .prologue
    .line 388
    iput-object p1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->subtitles:Lcom/google/android/videos/subtitles/Subtitles;

    .line 389
    invoke-direct {p0}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->pushSubtitles()V

    .line 390
    return-void
.end method

.method public setTrickPlayEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 410
    iput-boolean p1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->enableTrickMode:Z

    .line 411
    invoke-direct {p0}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->pushEnableTrickMode()V

    .line 412
    return-void
.end method

.method public setViews(Lcom/google/android/videos/player/PlayerSurface;Lcom/google/android/videos/player/overlay/SubtitlesOverlay;)V
    .locals 1
    .param p1, "playerSurface"    # Lcom/google/android/videos/player/PlayerSurface;
    .param p2, "subtitlesOverlay"    # Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    .prologue
    .line 225
    iput-object p1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->playerSurface:Lcom/google/android/videos/player/PlayerSurface;

    .line 226
    iput-object p2, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->subtitlesOverlay:Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    .line 228
    invoke-interface {p1, p0}, Lcom/google/android/videos/player/PlayerSurface;->setListener(Lcom/google/android/videos/player/PlayerSurface$Listener;)V

    .line 229
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lcom/google/android/videos/player/PlayerSurface;->setZoomSupported(Z)V

    .line 230
    return-void
.end method

.method public surfaceChanged()V
    .locals 0

    .prologue
    .line 453
    return-void
.end method

.method public surfaceCreated()V
    .locals 0

    .prologue
    .line 447
    invoke-direct {p0}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->pushSurface()V

    .line 448
    return-void
.end method

.method public surfaceDestroyed()V
    .locals 4

    .prologue
    .line 457
    iget-boolean v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->prepared:Z

    if-eqz v0, :cond_0

    .line 458
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->playerSurface:Lcom/google/android/videos/player/PlayerSurface;

    invoke-interface {v0}, Lcom/google/android/videos/player/PlayerSurface;->closeShutter()V

    .line 459
    iget-object v0, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->player:Lcom/google/android/exoplayer/ExoPlayer;

    iget-object v1, p0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->videoRenderer:Lcom/google/android/videos/player/exo/VideoRenderer;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/exoplayer/ExoPlayer;->blockingSendMessage(Lcom/google/android/exoplayer/ExoPlayer$ExoPlayerComponent;ILjava/lang/Object;)V

    .line 461
    :cond_0
    return-void
.end method
