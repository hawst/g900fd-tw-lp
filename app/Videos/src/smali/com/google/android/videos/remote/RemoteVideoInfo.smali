.class public Lcom/google/android/videos/remote/RemoteVideoInfo;
.super Ljava/lang/Object;
.source "RemoteVideoInfo.java"


# instance fields
.field public final durationMillis:I

.field public final isTrailer:Z

.field public final opaqueString:Ljava/lang/String;

.field public final preferredLanguage:Ljava/lang/String;

.field public final showTitle:Ljava/lang/String;

.field public final videoId:Ljava/lang/String;

.field public final videoTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "videoTitle"    # Ljava/lang/String;
    .param p3, "showTitle"    # Ljava/lang/String;
    .param p4, "isTrailer"    # Z
    .param p5, "durationMillis"    # I
    .param p6, "opaqueString"    # Ljava/lang/String;
    .param p7, "preferredLanguage"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/google/android/videos/remote/RemoteVideoInfo;->videoId:Ljava/lang/String;

    .line 27
    iput-object p2, p0, Lcom/google/android/videos/remote/RemoteVideoInfo;->videoTitle:Ljava/lang/String;

    .line 28
    iput-object p3, p0, Lcom/google/android/videos/remote/RemoteVideoInfo;->showTitle:Ljava/lang/String;

    .line 29
    iput-boolean p4, p0, Lcom/google/android/videos/remote/RemoteVideoInfo;->isTrailer:Z

    .line 30
    iput p5, p0, Lcom/google/android/videos/remote/RemoteVideoInfo;->durationMillis:I

    .line 31
    iput-object p6, p0, Lcom/google/android/videos/remote/RemoteVideoInfo;->opaqueString:Ljava/lang/String;

    .line 32
    iput-object p7, p0, Lcom/google/android/videos/remote/RemoteVideoInfo;->preferredLanguage:Ljava/lang/String;

    .line 33
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 37
    if-ne p1, p0, :cond_1

    .line 45
    :cond_0
    :goto_0
    return v1

    .line 40
    :cond_1
    instance-of v3, p1, Lcom/google/android/videos/remote/RemoteVideoInfo;

    if-nez v3, :cond_2

    move v1, v2

    .line 41
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 44
    check-cast v0, Lcom/google/android/videos/remote/RemoteVideoInfo;

    .line 45
    .local v0, "other":Lcom/google/android/videos/remote/RemoteVideoInfo;
    iget v3, p0, Lcom/google/android/videos/remote/RemoteVideoInfo;->durationMillis:I

    iget v4, v0, Lcom/google/android/videos/remote/RemoteVideoInfo;->durationMillis:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/android/videos/remote/RemoteVideoInfo;->videoId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/remote/RemoteVideoInfo;->videoId:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/videos/remote/RemoteVideoInfo;->videoTitle:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/remote/RemoteVideoInfo;->videoTitle:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/videos/remote/RemoteVideoInfo;->showTitle:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/remote/RemoteVideoInfo;->showTitle:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/videos/remote/RemoteVideoInfo;->opaqueString:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/remote/RemoteVideoInfo;->opaqueString:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/videos/remote/RemoteVideoInfo;->preferredLanguage:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/remote/RemoteVideoInfo;->preferredLanguage:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 55
    iget v0, p0, Lcom/google/android/videos/remote/RemoteVideoInfo;->durationMillis:I

    .line 56
    .local v0, "result":I
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteVideoInfo;->videoId:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 57
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteVideoInfo;->videoTitle:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 58
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteVideoInfo;->showTitle:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    add-int v0, v3, v1

    .line 59
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteVideoInfo;->opaqueString:Ljava/lang/String;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    add-int v0, v3, v1

    .line 60
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/videos/remote/RemoteVideoInfo;->preferredLanguage:Ljava/lang/String;

    if-nez v3, :cond_4

    :goto_4
    add-int v0, v1, v2

    .line 61
    return v0

    .line 56
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteVideoInfo;->videoId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 57
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteVideoInfo;->videoTitle:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 58
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteVideoInfo;->showTitle:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    .line 59
    :cond_3
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteVideoInfo;->opaqueString:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    .line 60
    :cond_4
    iget-object v2, p0, Lcom/google/android/videos/remote/RemoteVideoInfo;->preferredLanguage:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_4
.end method
