.class public Lcom/google/android/videos/async/FallbackRequester;
.super Ljava/lang/Object;
.source "FallbackRequester.java"

# interfaces
.implements Lcom/google/android/videos/async/Requester;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/async/FallbackRequester$FallbackCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Requester",
        "<TR;TE;>;"
    }
.end annotation


# static fields
.field private static final ALWAYS_TRUE_PREDICATE:Lcom/google/android/videos/utils/Predicate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/utils/Predicate",
            "<",
            "Ljava/lang/Exception;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final firstRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<TR;TE;>;"
        }
    .end annotation
.end field

.field private final mustFallback:Lcom/google/android/videos/utils/Predicate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/utils/Predicate",
            "<",
            "Ljava/lang/Exception;",
            ">;"
        }
    .end annotation
.end field

.field private final secondRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<TR;TE;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/videos/async/FallbackRequester$1;

    invoke-direct {v0}, Lcom/google/android/videos/async/FallbackRequester$1;-><init>()V

    sput-object v0, Lcom/google/android/videos/async/FallbackRequester;->ALWAYS_TRUE_PREDICATE:Lcom/google/android/videos/utils/Predicate;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/utils/Predicate;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<TR;TE;>;",
            "Lcom/google/android/videos/async/Requester",
            "<TR;TE;>;",
            "Lcom/google/android/videos/utils/Predicate",
            "<",
            "Ljava/lang/Exception;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p0, "this":Lcom/google/android/videos/async/FallbackRequester;, "Lcom/google/android/videos/async/FallbackRequester<TR;TE;>;"
    .local p1, "firstRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TR;TE;>;"
    .local p2, "secondRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TR;TE;>;"
    .local p3, "mustFallback":Lcom/google/android/videos/utils/Predicate;, "Lcom/google/android/videos/utils/Predicate<Ljava/lang/Exception;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/async/FallbackRequester;->firstRequester:Lcom/google/android/videos/async/Requester;

    .line 38
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/async/FallbackRequester;->secondRequester:Lcom/google/android/videos/async/Requester;

    .line 39
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/utils/Predicate;

    iput-object v0, p0, Lcom/google/android/videos/async/FallbackRequester;->mustFallback:Lcom/google/android/videos/utils/Predicate;

    .line 40
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/async/FallbackRequester;)Lcom/google/android/videos/utils/Predicate;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/async/FallbackRequester;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/videos/async/FallbackRequester;->mustFallback:Lcom/google/android/videos/utils/Predicate;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/async/FallbackRequester;)Lcom/google/android/videos/async/Requester;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/async/FallbackRequester;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/videos/async/FallbackRequester;->secondRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public static create(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/FallbackRequester;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/videos/async/Requester",
            "<TR;TE;>;",
            "Lcom/google/android/videos/async/Requester",
            "<TR;TE;>;)",
            "Lcom/google/android/videos/async/FallbackRequester",
            "<TR;TE;>;"
        }
    .end annotation

    .prologue
    .line 44
    .local p0, "firstRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TR;TE;>;"
    .local p1, "secondRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TR;TE;>;"
    new-instance v0, Lcom/google/android/videos/async/FallbackRequester;

    sget-object v1, Lcom/google/android/videos/async/FallbackRequester;->ALWAYS_TRUE_PREDICATE:Lcom/google/android/videos/utils/Predicate;

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/videos/async/FallbackRequester;-><init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/utils/Predicate;)V

    return-object v0
.end method


# virtual methods
.method public request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;)V"
        }
    .end annotation

    .prologue
    .line 49
    .local p0, "this":Lcom/google/android/videos/async/FallbackRequester;, "Lcom/google/android/videos/async/FallbackRequester<TR;TE;>;"
    .local p1, "request":Ljava/lang/Object;, "TR;"
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TR;TE;>;"
    iget-object v0, p0, Lcom/google/android/videos/async/FallbackRequester;->firstRequester:Lcom/google/android/videos/async/Requester;

    new-instance v1, Lcom/google/android/videos/async/FallbackRequester$FallbackCallback;

    invoke-direct {v1, p0, p2}, Lcom/google/android/videos/async/FallbackRequester$FallbackCallback;-><init>(Lcom/google/android/videos/async/FallbackRequester;Lcom/google/android/videos/async/Callback;)V

    invoke-interface {v0, p1, v1}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 50
    return-void
.end method
