.class Lcom/google/android/videos/store/WishlistStore$LoadWishlistTask;
.super Ljava/lang/Object;
.source "WishlistStore.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/WishlistStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadWishlistTask"
.end annotation


# instance fields
.field private final callback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/store/WishlistStore$WishlistRequest;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final request:Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

.field final synthetic this$0:Lcom/google/android/videos/store/WishlistStore;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/store/WishlistStore;Lcom/google/android/videos/store/WishlistStore$WishlistRequest;Lcom/google/android/videos/async/Callback;)V
    .locals 1
    .param p2, "request"    # Lcom/google/android/videos/store/WishlistStore$WishlistRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/store/WishlistStore$WishlistRequest;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/store/WishlistStore$WishlistRequest;",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 162
    .local p3, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/store/WishlistStore$WishlistRequest;Landroid/database/Cursor;>;"
    iput-object p1, p0, Lcom/google/android/videos/store/WishlistStore$LoadWishlistTask;->this$0:Lcom/google/android/videos/store/WishlistStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    iput-object v0, p0, Lcom/google/android/videos/store/WishlistStore$LoadWishlistTask;->request:Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    .line 164
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Callback;

    iput-object v0, p0, Lcom/google/android/videos/store/WishlistStore$LoadWishlistTask;->callback:Lcom/google/android/videos/async/Callback;

    .line 165
    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 169
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/videos/store/WishlistStore$LoadWishlistTask;->request:Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    # getter for: Lcom/google/android/videos/store/WishlistStore$WishlistRequest;->tables:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;->access$000(Lcom/google/android/videos/store/WishlistStore$WishlistRequest;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/store/WishlistStore$LoadWishlistTask;->request:Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    # getter for: Lcom/google/android/videos/store/WishlistStore$WishlistRequest;->columns:[Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;->access$100(Lcom/google/android/videos/store/WishlistStore$WishlistRequest;)[Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/store/WishlistStore$LoadWishlistTask;->request:Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    # getter for: Lcom/google/android/videos/store/WishlistStore$WishlistRequest;->whereClause:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;->access$200(Lcom/google/android/videos/store/WishlistStore$WishlistRequest;)Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/videos/store/WishlistStore$LoadWishlistTask;->request:Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    # getter for: Lcom/google/android/videos/store/WishlistStore$WishlistRequest;->orderByColumn:Ljava/lang/String;
    invoke-static {v5}, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;->access$300(Lcom/google/android/videos/store/WishlistStore$WishlistRequest;)Ljava/lang/String;

    move-result-object v6

    move-object v5, v4

    move-object v7, v4

    invoke-static/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQueryString(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 171
    .local v9, "sql":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/videos/store/WishlistStore$LoadWishlistTask;->this$0:Lcom/google/android/videos/store/WishlistStore;

    # getter for: Lcom/google/android/videos/store/WishlistStore;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v0}, Lcom/google/android/videos/store/WishlistStore;->access$500(Lcom/google/android/videos/store/WishlistStore;)Lcom/google/android/videos/store/Database;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/store/WishlistStore$LoadWishlistTask;->request:Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    # getter for: Lcom/google/android/videos/store/WishlistStore$WishlistRequest;->whereArgs:[Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;->access$400(Lcom/google/android/videos/store/WishlistStore$WishlistRequest;)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v9, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 172
    .local v8, "cursor":Landroid/database/Cursor;
    iget-object v0, p0, Lcom/google/android/videos/store/WishlistStore$LoadWishlistTask;->callback:Lcom/google/android/videos/async/Callback;

    iget-object v1, p0, Lcom/google/android/videos/store/WishlistStore$LoadWishlistTask;->request:Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    invoke-static {v8}, Lcom/google/android/videos/utils/DbUtils;->copyAndClose(Landroid/database/Cursor;)Lcom/google/android/videos/store/MemoryCursor;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 173
    return-void
.end method
