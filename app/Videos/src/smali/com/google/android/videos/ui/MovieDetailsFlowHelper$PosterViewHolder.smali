.class final Lcom/google/android/videos/ui/MovieDetailsFlowHelper$PosterViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "MovieDetailsFlowHelper.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/videos/flow/Bindable;
.implements Lcom/google/android/videos/flow/SelfRenderedListener;
.implements Lcom/google/android/videos/ui/BitmapLoader$BitmapView;
.implements Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/MovieDetailsFlowHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PosterViewHolder"
.end annotation


# instance fields
.field private final selector:Landroid/graphics/drawable/Drawable;

.field final synthetic this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

.field private thumbnailTag:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;Landroid/view/View;)V
    .locals 5
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 366
    iput-object p1, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$PosterViewHolder;->this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    .line 367
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 368
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [I

    const v3, 0x7f010051

    aput v3, v2, v4

    invoke-virtual {v1, v2}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 370
    .local v0, "attrs":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$PosterViewHolder;->selector:Landroid/graphics/drawable/Drawable;

    .line 371
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 372
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 373
    sget v1, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    .line 374
    # getter for: Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->activity:Lcom/google/android/videos/activity/MovieDetailsActivity;
    invoke-static {p1}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->access$100(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Lcom/google/android/videos/activity/MovieDetailsActivity;

    move-result-object v1

    const v2, 0x7f0b0077

    # getter for: Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->videoId:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->access$200(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/videos/ui/TransitionUtil;->encodeTransitionName(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    .line 377
    :cond_0
    return-void
.end method


# virtual methods
.method public bind()V
    .locals 2

    .prologue
    .line 381
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$PosterViewHolder;->this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    # getter for: Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->posterStorePosterRequester:Lcom/google/android/videos/async/Requester;
    invoke-static {v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->access$300(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Lcom/google/android/videos/async/Requester;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$PosterViewHolder;->this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    # getter for: Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->videoId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->access$200(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1, p0}, Lcom/google/android/videos/ui/BitmapLoader;->setBitmapAsync(Lcom/google/android/videos/ui/BitmapLoader$BitmapView;Lcom/google/android/videos/async/Requester;Ljava/lang/Object;Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;)V

    .line 382
    return-void
.end method

.method public getThumbnailTag()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$PosterViewHolder;->thumbnailTag:Ljava/lang/Object;

    return-object v0
.end method

.method public onBitmapRequestCompleted(Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 408
    .local p1, "bitmapView":Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;, "Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView<*>;"
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$PosterViewHolder;->this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    # getter for: Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->activity:Lcom/google/android/videos/activity/MovieDetailsActivity;
    invoke-static {v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->access$100(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Lcom/google/android/videos/activity/MovieDetailsActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->onPosterLoaded()V

    .line 409
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 413
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$PosterViewHolder;->this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    # getter for: Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->infoPanelItemClickListener:Lcom/google/android/videos/ui/MovieDetailsFlowHelper$InfoPanelItemClickListener;
    invoke-static {v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->access$400(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Lcom/google/android/videos/ui/MovieDetailsFlowHelper$InfoPanelItemClickListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$InfoPanelItemClickListener;->onPlayClick()V

    .line 414
    return-void
.end method

.method public onViewRendered()V
    .locals 1

    .prologue
    .line 386
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$PosterViewHolder;->this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    # getter for: Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->activity:Lcom/google/android/videos/activity/MovieDetailsActivity;
    invoke-static {v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->access$100(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Lcom/google/android/videos/activity/MovieDetailsActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->onPosterRendered()V

    .line 387
    return-void
.end method

.method public setThumbnail(Landroid/graphics/Bitmap;Z)V
    .locals 5
    .param p1, "thumbnail"    # Landroid/graphics/Bitmap;
    .param p2, "missingThumbnail"    # Z

    .prologue
    .line 401
    iget-object v1, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$PosterViewHolder;->itemView:Landroid/view/View;

    check-cast v1, Landroid/widget/ImageView;

    .line 402
    .local v1, "thumbnailView":Landroid/widget/ImageView;
    const/4 v2, 0x2

    new-array v0, v2, [Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v3, v0, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$PosterViewHolder;->selector:Landroid/graphics/drawable/Drawable;

    aput-object v3, v0, v2

    .line 403
    .local v0, "layers":[Landroid/graphics/drawable/Drawable;
    new-instance v2, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v2, v0}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 404
    return-void
.end method

.method public bridge synthetic setThumbnail(Ljava/lang/Object;Z)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Z

    .prologue
    .line 358
    check-cast p1, Landroid/graphics/Bitmap;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$PosterViewHolder;->setThumbnail(Landroid/graphics/Bitmap;Z)V

    return-void
.end method

.method public setThumbnailTag(Ljava/lang/Object;)V
    .locals 0
    .param p1, "tag"    # Ljava/lang/Object;

    .prologue
    .line 396
    iput-object p1, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$PosterViewHolder;->thumbnailTag:Ljava/lang/Object;

    .line 397
    return-void
.end method
