.class public final Lcom/google/android/videos/utils/EntityUtils;
.super Ljava/lang/Object;
.source "EntityUtils.java"


# direct methods
.method private static getByteArray(ILcom/google/android/videos/utils/ByteArrayPool;)Lcom/google/android/videos/utils/ByteArray;
    .locals 1
    .param p0, "minCapacity"    # I
    .param p1, "pool"    # Lcom/google/android/videos/utils/ByteArrayPool;

    .prologue
    .line 123
    if-eqz p1, :cond_0

    invoke-virtual {p1, p0}, Lcom/google/android/videos/utils/ByteArrayPool;->getBuf(I)Lcom/google/android/videos/utils/ByteArray;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/videos/utils/ByteArray;

    invoke-direct {v0, p0}, Lcom/google/android/videos/utils/ByteArray;-><init>(I)V

    goto :goto_0
.end method

.method public static mergeFrom(Lcom/google/protobuf/nano/MessageNano;Lorg/apache/http/HttpEntity;Lcom/google/android/videos/utils/ByteArrayPool;)Lcom/google/protobuf/nano/MessageNano;
    .locals 5
    .param p1, "entity"    # Lorg/apache/http/HttpEntity;
    .param p2, "pool"    # Lcom/google/android/videos/utils/ByteArrayPool;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/protobuf/nano/MessageNano;",
            ">(TT;",
            "Lorg/apache/http/HttpEntity;",
            "Lcom/google/android/videos/utils/ByteArrayPool;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 37
    .local p0, "messageNano":Lcom/google/protobuf/nano/MessageNano;, "TT;"
    invoke-static {p1, p2}, Lcom/google/android/videos/utils/EntityUtils;->toByteArrayObj(Lorg/apache/http/HttpEntity;Lcom/google/android/videos/utils/ByteArrayPool;)Lcom/google/android/videos/utils/ByteArray;

    move-result-object v0

    .line 39
    .local v0, "byteBuffer":Lcom/google/android/videos/utils/ByteArray;
    :try_start_0
    iget-object v2, v0, Lcom/google/android/videos/utils/ByteArray;->data:[B

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/google/android/videos/utils/ByteArray;->limit()I

    move-result v4

    invoke-static {p0, v2, v3, v4}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[BII)Lcom/google/protobuf/nano/MessageNano;
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    if-eqz p2, :cond_0

    .line 44
    invoke-virtual {p2, v0}, Lcom/google/android/videos/utils/ByteArrayPool;->returnBuf(Lcom/google/android/videos/utils/ByteArray;)V

    .line 46
    :cond_0
    return-object p0

    .line 40
    :catch_0
    move-exception v1

    .line 41
    .local v1, "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    new-instance v2, Lcom/google/android/videos/converter/ConverterException;

    invoke-direct {v2, v1}, Lcom/google/android/videos/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public static toByteArray(Lorg/apache/http/HttpEntity;)[B
    .locals 5
    .param p0, "entity"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 59
    invoke-static {p0}, Lcom/google/android/videos/utils/EntityUtils;->toByteArrayObj(Lorg/apache/http/HttpEntity;)Lcom/google/android/videos/utils/ByteArray;

    move-result-object v0

    .line 60
    .local v0, "byteArray":Lcom/google/android/videos/utils/ByteArray;
    invoke-virtual {v0}, Lcom/google/android/videos/utils/ByteArray;->limit()I

    move-result v1

    .line 61
    .local v1, "limit":I
    iget v3, v0, Lcom/google/android/videos/utils/ByteArray;->capacity:I

    if-ne v1, v3, :cond_0

    .line 62
    iget-object v2, v0, Lcom/google/android/videos/utils/ByteArray;->data:[B

    .line 67
    :goto_0
    return-object v2

    .line 65
    :cond_0
    new-array v2, v1, [B

    .line 66
    .local v2, "newbuffer":[B
    iget-object v3, v0, Lcom/google/android/videos/utils/ByteArray;->data:[B

    invoke-static {v3, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public static toByteArrayObj(Lorg/apache/http/HttpEntity;)Lcom/google/android/videos/utils/ByteArray;
    .locals 1
    .param p0, "entity"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/android/videos/utils/EntityUtils;->toByteArrayObj(Lorg/apache/http/HttpEntity;Lcom/google/android/videos/utils/ByteArrayPool;)Lcom/google/android/videos/utils/ByteArray;

    move-result-object v0

    return-object v0
.end method

.method public static toByteArrayObj(Lorg/apache/http/HttpEntity;Lcom/google/android/videos/utils/ByteArrayPool;)Lcom/google/android/videos/utils/ByteArray;
    .locals 11
    .param p0, "entity"    # Lorg/apache/http/HttpEntity;
    .param p1, "pool"    # Lcom/google/android/videos/utils/ByteArrayPool;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    if-nez p0, :cond_0

    .line 89
    new-instance v6, Ljava/io/IOException;

    const-string v7, "entity is null"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 91
    :cond_0
    invoke-interface {p0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v2

    .line 92
    .local v2, "inputStream":Ljava/io/InputStream;
    if-nez v2, :cond_1

    .line 93
    new-instance v6, Ljava/io/IOException;

    const-string v7, "entity content is null"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 96
    :cond_1
    :try_start_0
    invoke-interface {p0}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v6

    long-to-int v1, v6

    .line 97
    .local v1, "contentLength":I
    if-gez v1, :cond_4

    const/16 v6, 0x1000

    :goto_0
    invoke-static {v6, p1}, Lcom/google/android/videos/utils/EntityUtils;->getByteArray(ILcom/google/android/videos/utils/ByteArrayPool;)Lcom/google/android/videos/utils/ByteArray;

    move-result-object v0

    .line 98
    .local v0, "buffer":Lcom/google/android/videos/utils/ByteArray;
    const/4 v5, 0x0

    .line 100
    .local v5, "totalRead":I
    :cond_2
    :goto_1
    iget-object v6, v0, Lcom/google/android/videos/utils/ByteArray;->data:[B

    iget v7, v0, Lcom/google/android/videos/utils/ByteArray;->capacity:I

    sub-int/2addr v7, v5

    invoke-virtual {v2, v6, v5, v7}, Ljava/io/InputStream;->read([BII)I

    move-result v4

    .local v4, "read":I
    const/4 v6, -0x1

    if-eq v4, v6, :cond_3

    .line 101
    add-int/2addr v5, v4

    .line 102
    iget v6, v0, Lcom/google/android/videos/utils/ByteArray;->capacity:I

    if-ne v5, v6, :cond_2

    .line 103
    if-ne v5, v1, :cond_5

    .line 115
    :cond_3
    invoke-virtual {v0, v5}, Lcom/google/android/videos/utils/ByteArray;->setLimit(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 118
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    return-object v0

    .end local v0    # "buffer":Lcom/google/android/videos/utils/ByteArray;
    .end local v4    # "read":I
    .end local v5    # "totalRead":I
    :cond_4
    move v6, v1

    .line 97
    goto :goto_0

    .line 106
    .restart local v0    # "buffer":Lcom/google/android/videos/utils/ByteArray;
    .restart local v4    # "read":I
    .restart local v5    # "totalRead":I
    :cond_5
    :try_start_1
    iget v6, v0, Lcom/google/android/videos/utils/ByteArray;->capacity:I

    shl-int/lit8 v6, v6, 0x1

    invoke-static {v6, p1}, Lcom/google/android/videos/utils/EntityUtils;->getByteArray(ILcom/google/android/videos/utils/ByteArrayPool;)Lcom/google/android/videos/utils/ByteArray;

    move-result-object v3

    .line 107
    .local v3, "newBuffer":Lcom/google/android/videos/utils/ByteArray;
    iget-object v6, v0, Lcom/google/android/videos/utils/ByteArray;->data:[B

    const/4 v7, 0x0

    iget-object v8, v3, Lcom/google/android/videos/utils/ByteArray;->data:[B

    const/4 v9, 0x0

    iget v10, v0, Lcom/google/android/videos/utils/ByteArray;->capacity:I

    invoke-static {v6, v7, v8, v9, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 108
    if-eqz p1, :cond_6

    .line 109
    invoke-virtual {p1, v0}, Lcom/google/android/videos/utils/ByteArrayPool;->returnBuf(Lcom/google/android/videos/utils/ByteArray;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 111
    :cond_6
    move-object v0, v3

    .line 112
    goto :goto_1

    .line 118
    .end local v0    # "buffer":Lcom/google/android/videos/utils/ByteArray;
    .end local v1    # "contentLength":I
    .end local v3    # "newBuffer":Lcom/google/android/videos/utils/ByteArray;
    .end local v4    # "read":I
    .end local v5    # "totalRead":I
    :catchall_0
    move-exception v6

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    throw v6
.end method
