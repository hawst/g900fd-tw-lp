.class final Lcom/google/android/videos/api/VideoFormatsGetConverter;
.super Lcom/google/android/videos/converter/HttpResponseConverter;
.source "VideoFormatsGetConverter.java"

# interfaces
.implements Lcom/google/android/videos/converter/RequestConverter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/converter/HttpResponseConverter",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/Integer;",
        "Lcom/google/wireless/android/video/magma/proto/VideoFormat;",
        ">;>;",
        "Lcom/google/android/videos/converter/RequestConverter",
        "<",
        "Lcom/google/android/videos/async/EmptyRequest;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        ">;"
    }
.end annotation


# instance fields
.field private final baseUri:Ljava/lang/String;

.field private final byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArrayPool;)V
    .locals 3
    .param p1, "baseUri"    # Landroid/net/Uri;
    .param p2, "byteArrayPool"    # Lcom/google/android/videos/utils/ByteArrayPool;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/videos/converter/HttpResponseConverter;-><init>()V

    .line 39
    new-instance v0, Lcom/google/android/videos/utils/UriBuilder;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "config"

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;->addSegment(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v0

    const-string v1, "fs"

    const-string v2, "f"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/utils/UriBuilder;->build()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/api/VideoFormatsGetConverter;->baseUri:Ljava/lang/String;

    .line 43
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/utils/ByteArrayPool;

    iput-object v0, p0, Lcom/google/android/videos/api/VideoFormatsGetConverter;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;

    .line 44
    return-void
.end method


# virtual methods
.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 27
    check-cast p1, Lcom/google/android/videos/async/EmptyRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/api/VideoFormatsGetConverter;->convertRequest(Lcom/google/android/videos/async/EmptyRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public convertRequest(Lcom/google/android/videos/async/EmptyRequest;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/async/EmptyRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/videos/api/VideoFormatsGetConverter;->baseUri:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/videos/async/HttpUriRequestFactory;->createGet(Ljava/lang/String;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic convertResponseEntity(Lorg/apache/http/HttpEntity;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lcom/google/android/videos/api/VideoFormatsGetConverter;->convertResponseEntity(Lorg/apache/http/HttpEntity;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public convertResponseEntity(Lorg/apache/http/HttpEntity;)Ljava/util/Map;
    .locals 7
    .param p1, "entity"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/HttpEntity;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/wireless/android/video/magma/proto/VideoFormat;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 54
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;-><init>()V

    iget-object v6, p0, Lcom/google/android/videos/api/VideoFormatsGetConverter;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;

    invoke-static {v5, p1, v6}, Lcom/google/android/videos/utils/EntityUtils;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;Lorg/apache/http/HttpEntity;Lcom/google/android/videos/utils/ByteArrayPool;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;

    .line 56
    .local v0, "configGetResponse":Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 57
    .local v2, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/google/wireless/android/video/magma/proto/VideoFormat;>;"
    iget-object v5, v0, Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;->resource:Lcom/google/wireless/android/video/magma/proto/UserConfiguration;

    if-eqz v5, :cond_0

    .line 58
    iget-object v5, v0, Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;->resource:Lcom/google/wireless/android/video/magma/proto/UserConfiguration;

    iget-object v4, v5, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->videoFormat:[Lcom/google/wireless/android/video/magma/proto/VideoFormat;

    .line 59
    .local v4, "videoFormats":[Lcom/google/wireless/android/video/magma/proto/VideoFormat;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v5, v4

    if-ge v1, v5, :cond_0

    .line 60
    aget-object v3, v4, v1

    .line 61
    .local v3, "videoFormat":Lcom/google/wireless/android/video/magma/proto/VideoFormat;
    iget v5, v3, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->itag:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v2, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 64
    .end local v1    # "i":I
    .end local v3    # "videoFormat":Lcom/google/wireless/android/video/magma/proto/VideoFormat;
    .end local v4    # "videoFormats":[Lcom/google/wireless/android/video/magma/proto/VideoFormat;
    :cond_0
    return-object v2
.end method
