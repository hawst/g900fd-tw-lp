.class public Lcom/google/android/videos/store/WishlistService;
.super Landroid/app/IntentService;
.source "WishlistService.java"


# instance fields
.field private database:Lcom/google/android/videos/store/Database;

.field private eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private wishlistStoreSync:Lcom/google/android/videos/store/WishlistStoreSync;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 76
    const-class v0, Lcom/google/android/videos/store/WishlistService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 77
    return-void
.end method

.method public static requestSetWishlisted(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZILandroid/view/View;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "itemId"    # Ljava/lang/String;
    .param p3, "itemType"    # I
    .param p4, "isAdd"    # Z
    .param p5, "eventSource"    # I
    .param p6, "accessibilityView"    # Landroid/view/View;

    .prologue
    .line 56
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 57
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 58
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/videos/store/WishlistService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 59
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.google.android.videos.SET_WISHLISTED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 60
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 61
    const-string v1, "item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 62
    const-string v1, "item_type"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 63
    const-string v1, "is_add"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 64
    const-string v1, "event_source"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 65
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 67
    invoke-static {p0}, Lcom/google/android/videos/utils/AccessibilityUtils;->isAccessibilityEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 68
    if-eqz p4, :cond_1

    const v1, 0x7f0b011c

    :goto_0
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, p6}, Lcom/google/android/videos/utils/AccessibilityUtils;->sendAccessibilityEventWithText(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;)V

    .line 73
    :cond_0
    return-void

    .line 68
    :cond_1
    const v1, 0x7f0b011d

    goto :goto_0
.end method

.method private setWishlisted(Ljava/lang/String;Ljava/lang/String;IZI)V
    .locals 10
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "itemId"    # Ljava/lang/String;
    .param p3, "itemType"    # I
    .param p4, "isAdd"    # Z
    .param p5, "eventSource"    # I

    .prologue
    .line 111
    iget-object v5, p0, Lcom/google/android/videos/store/WishlistService;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    invoke-interface {v5, p2, p3, p4, p5}, Lcom/google/android/videos/logging/EventLogger;->onWishlistAction(Ljava/lang/String;IZI)V

    .line 112
    const/4 v1, 0x0

    .line 113
    .local v1, "success":Z
    const/4 v3, 0x0

    .line 114
    .local v3, "updated":Z
    iget-object v5, p0, Lcom/google/android/videos/store/WishlistService;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v5}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 116
    .local v2, "transaction":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 117
    .local v4, "values":Landroid/content/ContentValues;
    const-string v5, "wishlist_account"

    invoke-virtual {v4, v5, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    const-string v5, "wishlist_item_type"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 119
    const-string v5, "wishlist_item_id"

    invoke-virtual {v4, v5, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    if-eqz p4, :cond_4

    .line 122
    const-string v5, "wishlist_item_state"

    const/4 v6, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 123
    const-string v5, "wishlist"

    const-string v6, "wishlist_account = ? AND wishlist_item_id = ? AND wishlist_item_type = ? AND wishlist_item_state == 3"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    const/4 v8, 0x1

    aput-object p2, v7, v8

    const/4 v8, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v2, v5, v4, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 125
    .local v0, "rowsUpdated":I
    if-lez v0, :cond_3

    const/4 v3, 0x1

    .line 126
    :goto_0
    if-nez v3, :cond_0

    .line 128
    :try_start_1
    const-string v5, "wishlist_item_order"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    neg-long v6, v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 129
    const-string v5, "wishlist"

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 130
    const/4 v3, 0x1

    .line 142
    :cond_0
    :goto_1
    const/4 v1, 0x1

    .line 144
    iget-object v6, p0, Lcom/google/android/videos/store/WishlistService;->database:Lcom/google/android/videos/store/Database;

    if-eqz v3, :cond_6

    const/4 v5, 0x2

    :goto_2
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    invoke-virtual {v6, v2, v1, v5, v7}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 146
    if-eqz v3, :cond_1

    .line 147
    invoke-static {p0}, Lcom/google/android/videos/pinning/TransferService;->createIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/videos/store/WishlistService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 150
    :cond_1
    if-eqz p4, :cond_2

    if-eqz v3, :cond_2

    .line 151
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/store/WishlistService;->syncWishlistItem(Ljava/lang/String;Ljava/lang/String;I)V

    .line 153
    :cond_2
    return-void

    .line 125
    :cond_3
    const/4 v3, 0x0

    goto :goto_0

    .line 136
    .end local v0    # "rowsUpdated":I
    :cond_4
    :try_start_2
    const-string v5, "wishlist_item_state"

    const/4 v6, 0x3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 137
    const-string v5, "wishlist"

    const-string v6, "wishlist_account = ? AND wishlist_item_id = ? AND wishlist_item_type = ? AND wishlist_item_state != 3"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    const/4 v8, 0x1

    aput-object p2, v7, v8

    const/4 v8, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v2, v5, v4, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    .line 139
    .restart local v0    # "rowsUpdated":I
    if-lez v0, :cond_5

    const/4 v3, 0x1

    :goto_3
    goto :goto_1

    :cond_5
    const/4 v3, 0x0

    goto :goto_3

    .line 144
    :cond_6
    const/4 v5, 0x0

    goto :goto_2

    .end local v0    # "rowsUpdated":I
    .end local v4    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v5

    move-object v6, v5

    iget-object v7, p0, Lcom/google/android/videos/store/WishlistService;->database:Lcom/google/android/videos/store/Database;

    if-eqz v3, :cond_8

    const/4 v5, 0x2

    :goto_4
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object p1, v8, v9

    invoke-virtual {v7, v2, v1, v5, v8}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 146
    if-eqz v3, :cond_7

    .line 147
    invoke-static {p0}, Lcom/google/android/videos/pinning/TransferService;->createIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/videos/store/WishlistService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_7
    throw v6

    .line 144
    :cond_8
    const/4 v5, 0x0

    goto :goto_4

    .line 131
    .restart local v0    # "rowsUpdated":I
    .restart local v4    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v5

    goto :goto_1
.end method

.method private syncWishlistItem(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "itemId"    # Ljava/lang/String;
    .param p3, "itemType"    # I

    .prologue
    const/4 v2, 0x0

    .line 156
    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 157
    .local v0, "wishlistAssetSyncRequest":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v1, 0x6

    if-ne p3, v1, :cond_1

    .line 158
    iget-object v1, p0, Lcom/google/android/videos/store/WishlistService;->wishlistStoreSync:Lcom/google/android/videos/store/WishlistStoreSync;

    invoke-static {v0, v2}, Lcom/google/android/videos/async/ControllableRequest;->create(Ljava/lang/Object;Lcom/google/android/videos/async/TaskStatus;)Lcom/google/android/videos/async/ControllableRequest;

    move-result-object v2

    invoke-static {}, Lcom/google/android/videos/async/IgnoreCallback;->get()Lcom/google/android/videos/async/NewCallback;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/store/WishlistStoreSync;->syncAssetForWishlistMovies(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V

    .line 166
    :cond_0
    :goto_0
    return-void

    .line 161
    :cond_1
    const/16 v1, 0x12

    if-ne p3, v1, :cond_0

    .line 162
    iget-object v1, p0, Lcom/google/android/videos/store/WishlistService;->wishlistStoreSync:Lcom/google/android/videos/store/WishlistStoreSync;

    invoke-static {v0, v2}, Lcom/google/android/videos/async/ControllableRequest;->create(Ljava/lang/Object;Lcom/google/android/videos/async/TaskStatus;)Lcom/google/android/videos/async/ControllableRequest;

    move-result-object v2

    invoke-static {}, Lcom/google/android/videos/async/IgnoreCallback;->get()Lcom/google/android/videos/async/NewCallback;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/store/WishlistStoreSync;->syncAssetForWishlistShows(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 81
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 86
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 87
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v0

    .line 88
    .local v0, "videosGlobals":Lcom/google/android/videos/VideosGlobals;
    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/store/WishlistService;->database:Lcom/google/android/videos/store/Database;

    .line 89
    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getWishlistStoreSync()Lcom/google/android/videos/store/WishlistStoreSync;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/store/WishlistService;->wishlistStoreSync:Lcom/google/android/videos/store/WishlistStoreSync;

    .line 90
    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/store/WishlistService;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 91
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x0

    .line 95
    if-eqz p1, :cond_0

    .line 96
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    .line 97
    .local v6, "action":Ljava/lang/String;
    const-string v0, "com.google.android.videos.SET_WISHLISTED"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 99
    .local v1, "account":Ljava/lang/String;
    const-string v0, "item_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 100
    .local v2, "itemId":Ljava/lang/String;
    const-string v0, "item_type"

    const/4 v7, -0x1

    invoke-virtual {p1, v0, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 101
    .local v3, "itemType":I
    const-string v0, "is_add"

    invoke-virtual {p1, v0, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 102
    .local v4, "isAdd":Z
    const-string v0, "event_source"

    invoke-virtual {p1, v0, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .local v5, "eventSource":I
    move-object v0, p0

    .line 104
    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/store/WishlistService;->setWishlisted(Ljava/lang/String;Ljava/lang/String;IZI)V

    .line 107
    .end local v1    # "account":Ljava/lang/String;
    .end local v2    # "itemId":Ljava/lang/String;
    .end local v3    # "itemType":I
    .end local v4    # "isAdd":Z
    .end local v5    # "eventSource":I
    .end local v6    # "action":Ljava/lang/String;
    :cond_0
    return-void
.end method
