.class public abstract Lcom/google/android/videos/tagging/KnowledgeEntity;
.super Ljava/lang/Object;
.source "KnowledgeEntity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/tagging/KnowledgeEntity$TvShow;,
        Lcom/google/android/videos/tagging/KnowledgeEntity$Movie;,
        Lcom/google/android/videos/tagging/KnowledgeEntity$Film;,
        Lcom/google/android/videos/tagging/KnowledgeEntity$Image;,
        Lcom/google/android/videos/tagging/KnowledgeEntity$Song;,
        Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    }
.end annotation


# instance fields
.field public final appearances:[I

.field public final image:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

.field public final localId:I

.field public final name:Ljava/lang/String;

.field public final splitIds:[I


# direct methods
.method constructor <init>(ILjava/lang/String;Lcom/google/android/videos/tagging/KnowledgeEntity$Image;[I)V
    .locals 2
    .param p1, "localId"    # I
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "image"    # Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
    .param p4, "appearances"    # [I

    .prologue
    .line 277
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 278
    iput p1, p0, Lcom/google/android/videos/tagging/KnowledgeEntity;->localId:I

    .line 279
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput p1, v0, v1

    iput-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeEntity;->splitIds:[I

    .line 280
    iput-object p2, p0, Lcom/google/android/videos/tagging/KnowledgeEntity;->name:Ljava/lang/String;

    .line 281
    iput-object p3, p0, Lcom/google/android/videos/tagging/KnowledgeEntity;->image:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    .line 282
    iput-object p4, p0, Lcom/google/android/videos/tagging/KnowledgeEntity;->appearances:[I

    .line 283
    return-void
.end method

.method constructor <init>(I[ILjava/lang/String;Lcom/google/android/videos/tagging/KnowledgeEntity$Image;[I)V
    .locals 0
    .param p1, "localId"    # I
    .param p2, "splitIds"    # [I
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "image"    # Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
    .param p5, "appearances"    # [I

    .prologue
    .line 266
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 267
    iput p1, p0, Lcom/google/android/videos/tagging/KnowledgeEntity;->localId:I

    .line 268
    iput-object p2, p0, Lcom/google/android/videos/tagging/KnowledgeEntity;->splitIds:[I

    .line 269
    iput-object p3, p0, Lcom/google/android/videos/tagging/KnowledgeEntity;->name:Ljava/lang/String;

    .line 270
    iput-object p4, p0, Lcom/google/android/videos/tagging/KnowledgeEntity;->image:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    .line 271
    iput-object p5, p0, Lcom/google/android/videos/tagging/KnowledgeEntity;->appearances:[I

    .line 272
    return-void
.end method


# virtual methods
.method public appearsAt(I)Z
    .locals 1
    .param p1, "timeMillis"    # I

    .prologue
    .line 313
    invoke-virtual {p0, p1}, Lcom/google/android/videos/tagging/KnowledgeEntity;->lastAppearance(I)I

    move-result v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasAppearanceData()Z
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeEntity;->appearances:[I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public lastAppearance(I)I
    .locals 4
    .param p1, "upToMillis"    # I

    .prologue
    const/4 v1, -0x1

    .line 294
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/KnowledgeEntity;->hasAppearanceData()Z

    move-result v2

    if-nez v2, :cond_1

    .line 308
    :cond_0
    :goto_0
    return v1

    .line 297
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/tagging/KnowledgeEntity;->appearances:[I

    invoke-static {v2, p1}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v0

    .line 298
    .local v0, "index":I
    if-ltz v0, :cond_2

    move v1, p1

    .line 300
    goto :goto_0

    .line 302
    :cond_2
    xor-int/lit8 v0, v0, -0x1

    .line 303
    and-int/lit8 v2, v0, 0x1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    move v1, p1

    .line 305
    goto :goto_0

    .line 308
    :cond_3
    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/tagging/KnowledgeEntity;->appearances:[I

    add-int/lit8 v2, v0, -0x1

    aget v1, v1, v2

    goto :goto_0
.end method
