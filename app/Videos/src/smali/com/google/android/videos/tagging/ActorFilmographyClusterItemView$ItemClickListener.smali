.class final Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$ItemClickListener;
.super Ljava/lang/Object;
.source "ActorFilmographyClusterItemView.java"

# interfaces
.implements Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ItemClickListener"
.end annotation


# instance fields
.field private final account:Ljava/lang/String;

.field private final activity:Landroid/app/Activity;

.field private final eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private final storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/videos/store/StoreStatusMonitor;Lcom/google/android/videos/logging/EventLogger;Ljava/lang/String;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "storeStatusMonitor"    # Lcom/google/android/videos/store/StoreStatusMonitor;
    .param p3, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;
    .param p4, "account"    # Ljava/lang/String;

    .prologue
    .line 203
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 204
    iput-object p1, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$ItemClickListener;->activity:Landroid/app/Activity;

    .line 205
    iput-object p3, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$ItemClickListener;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 206
    iput-object p2, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$ItemClickListener;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    .line 207
    iput-object p4, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$ItemClickListener;->account:Ljava/lang/String;

    .line 208
    return-void
.end method

.method private onMovieItemClick(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V
    .locals 8
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "videoTitle"    # Ljava/lang/String;
    .param p3, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x6

    const/4 v5, 0x2

    .line 225
    invoke-virtual {p3}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0f00ca

    if-ne v0, v1, :cond_2

    .line 226
    iget-object v0, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$ItemClickListener;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    invoke-virtual {v0, p1, v3}, Lcom/google/android/videos/store/StoreStatusMonitor;->getStatus(Ljava/lang/String;I)I

    move-result v7

    .line 227
    .local v7, "status":I
    invoke-static {v7}, Lcom/google/android/videos/store/StoreStatusMonitor;->isPurchased(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 239
    .end local v7    # "status":I
    :goto_0
    return-void

    .line 231
    .restart local v7    # "status":I
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$ItemClickListener;->activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$ItemClickListener;->account:Ljava/lang/String;

    invoke-static {v7}, Lcom/google/android/videos/store/StoreStatusMonitor;->isWishlisted(I)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v4, 0x1

    :goto_1
    move-object v2, p1

    move-object v6, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/videos/store/WishlistService;->requestSetWishlisted(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZILandroid/view/View;)V

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    .line 233
    .end local v7    # "status":I
    :cond_2
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 234
    iget-object v0, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$ItemClickListener;->activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$ItemClickListener;->account:Ljava/lang/String;

    invoke-static {v0, p1, v1, v5}, Lcom/google/android/videos/utils/PlayStoreUtil;->viewMovieDetails(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 236
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$ItemClickListener;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget-object v1, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$ItemClickListener;->activity:Landroid/app/Activity;

    invoke-static {p2}, Lcom/google/android/videos/tagging/Cards;->toQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/videos/utils/BrowserUtil;->startWebSearch(Landroid/app/Activity;Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v5, v1}, Lcom/google/android/videos/logging/EventLogger;->onWebSearch(II)V

    goto :goto_0
.end method

.method private onShowItemClick(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V
    .locals 8
    .param p1, "showId"    # Ljava/lang/String;
    .param p2, "showTitle"    # Ljava/lang/String;
    .param p3, "view"    # Landroid/view/View;

    .prologue
    const/16 v3, 0x12

    const/4 v5, 0x2

    .line 242
    invoke-virtual {p3}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0f00ca

    if-ne v0, v1, :cond_2

    .line 243
    iget-object v0, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$ItemClickListener;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    invoke-virtual {v0, p1, v3}, Lcom/google/android/videos/store/StoreStatusMonitor;->getStatus(Ljava/lang/String;I)I

    move-result v7

    .line 244
    .local v7, "status":I
    invoke-static {v7}, Lcom/google/android/videos/store/StoreStatusMonitor;->isPurchased(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256
    .end local v7    # "status":I
    :goto_0
    return-void

    .line 248
    .restart local v7    # "status":I
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$ItemClickListener;->activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$ItemClickListener;->account:Ljava/lang/String;

    invoke-static {v7}, Lcom/google/android/videos/store/StoreStatusMonitor;->isWishlisted(I)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v4, 0x1

    :goto_1
    move-object v2, p1

    move-object v6, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/videos/store/WishlistService;->requestSetWishlisted(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZILandroid/view/View;)V

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    .line 250
    .end local v7    # "status":I
    :cond_2
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 251
    iget-object v0, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$ItemClickListener;->activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$ItemClickListener;->account:Ljava/lang/String;

    invoke-static {v0, p1, v1, v5}, Lcom/google/android/videos/utils/PlayStoreUtil;->viewShowDetails(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 253
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$ItemClickListener;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget-object v1, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$ItemClickListener;->activity:Landroid/app/Activity;

    invoke-static {p2}, Lcom/google/android/videos/tagging/Cards;->toQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/videos/utils/BrowserUtil;->startWebSearch(Landroid/app/Activity;Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v5, v1}, Lcom/google/android/videos/logging/EventLogger;->onWebSearch(II)V

    goto :goto_0
.end method


# virtual methods
.method public onItemClick(Lcom/google/android/videos/adapter/DataSource;ILandroid/view/View;)V
    .locals 6
    .param p2, "position"    # I
    .param p3, "view"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/adapter/DataSource",
            "<*>;I",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 213
    .local p1, "dataSource":Lcom/google/android/videos/adapter/DataSource;, "Lcom/google/android/videos/adapter/DataSource<*>;"
    move-object v0, p1

    check-cast v0, Lcom/google/android/videos/adapter/ArrayDataSource;

    .line 214
    .local v0, "filmDataSource":Lcom/google/android/videos/adapter/ArrayDataSource;, "Lcom/google/android/videos/adapter/ArrayDataSource<Lcom/google/android/videos/tagging/KnowledgeEntity$Film;>;"
    invoke-virtual {v0, p2}, Lcom/google/android/videos/adapter/ArrayDataSource;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/tagging/KnowledgeEntity$Film;

    .line 215
    .local v1, "item":Lcom/google/android/videos/tagging/KnowledgeEntity$Film;
    instance-of v4, v1, Lcom/google/android/videos/tagging/KnowledgeEntity$Movie;

    if-eqz v4, :cond_1

    move-object v2, v1

    .line 216
    check-cast v2, Lcom/google/android/videos/tagging/KnowledgeEntity$Movie;

    .line 217
    .local v2, "movie":Lcom/google/android/videos/tagging/KnowledgeEntity$Movie;
    iget-object v4, v2, Lcom/google/android/videos/tagging/KnowledgeEntity$Movie;->videoId:Ljava/lang/String;

    iget-object v5, v2, Lcom/google/android/videos/tagging/KnowledgeEntity$Movie;->title:Ljava/lang/String;

    invoke-direct {p0, v4, v5, p3}, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$ItemClickListener;->onMovieItemClick(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V

    .line 222
    .end local v2    # "movie":Lcom/google/android/videos/tagging/KnowledgeEntity$Movie;
    :cond_0
    :goto_0
    return-void

    .line 218
    :cond_1
    instance-of v4, v1, Lcom/google/android/videos/tagging/KnowledgeEntity$TvShow;

    if-eqz v4, :cond_0

    move-object v3, v1

    .line 219
    check-cast v3, Lcom/google/android/videos/tagging/KnowledgeEntity$TvShow;

    .line 220
    .local v3, "tvShow":Lcom/google/android/videos/tagging/KnowledgeEntity$TvShow;
    iget-object v4, v3, Lcom/google/android/videos/tagging/KnowledgeEntity$TvShow;->showId:Ljava/lang/String;

    iget-object v5, v3, Lcom/google/android/videos/tagging/KnowledgeEntity$TvShow;->title:Ljava/lang/String;

    invoke-direct {p0, v4, v5, p3}, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$ItemClickListener;->onShowItemClick(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V

    goto :goto_0
.end method
