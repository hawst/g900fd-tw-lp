.class public final Lcom/google/android/videos/subtitles/Subtitles;
.super Ljava/lang/Object;
.source "Subtitles.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/subtitles/Subtitles$1;,
        Lcom/google/android/videos/subtitles/Subtitles$Builder;
    }
.end annotation


# instance fields
.field private transient eventTimes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final windows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleWindow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleWindow;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p1, "windows":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/subtitles/SubtitleWindow;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/subtitles/Subtitles;->windows:Ljava/util/List;

    .line 32
    invoke-direct {p0}, Lcom/google/android/videos/subtitles/Subtitles;->initEventTimes()V

    .line 33
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/List;Lcom/google/android/videos/subtitles/Subtitles$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/util/List;
    .param p2, "x1"    # Lcom/google/android/videos/subtitles/Subtitles$1;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/google/android/videos/subtitles/Subtitles;-><init>(Ljava/util/List;)V

    return-void
.end method

.method private initEventTimes()V
    .locals 4

    .prologue
    .line 36
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    .line 37
    .local v0, "eventTimes":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/videos/subtitles/Subtitles;->windows:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 38
    iget-object v3, p0, Lcom/google/android/videos/subtitles/Subtitles;->windows:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/subtitles/SubtitleWindow;

    .line 39
    .local v2, "window":Lcom/google/android/videos/subtitles/SubtitleWindow;
    iget-object v3, v2, Lcom/google/android/videos/subtitles/SubtitleWindow;->textTimeline:Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline;

    iget-object v3, v3, Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline;->startTimes:Ljava/util/List;

    invoke-virtual {v0, v3}, Ljava/util/TreeSet;->addAll(Ljava/util/Collection;)Z

    .line 40
    iget-object v3, v2, Lcom/google/android/videos/subtitles/SubtitleWindow;->textTimeline:Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline;

    iget-object v3, v3, Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline;->endTimes:Ljava/util/List;

    invoke-virtual {v0, v3}, Ljava/util/TreeSet;->addAll(Ljava/util/Collection;)Z

    .line 41
    iget-object v3, v2, Lcom/google/android/videos/subtitles/SubtitleWindow;->settingsTimeline:Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline;

    iget-object v3, v3, Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline;->startTimes:Ljava/util/List;

    invoke-virtual {v0, v3}, Ljava/util/TreeSet;->addAll(Ljava/util/Collection;)Z

    .line 37
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 43
    .end local v2    # "window":Lcom/google/android/videos/subtitles/SubtitleWindow;
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/subtitles/Subtitles;->eventTimes:Ljava/util/List;

    .line 44
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 0
    .param p1, "in"    # Ljava/io/ObjectInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 71
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 72
    invoke-direct {p0}, Lcom/google/android/videos/subtitles/Subtitles;->initEventTimes()V

    .line 73
    return-void
.end method


# virtual methods
.method public getEventTimes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/videos/subtitles/Subtitles;->eventTimes:Ljava/util/List;

    return-object v0
.end method

.method public getSubtitleWindowSnapshotsAt(I)Ljava/util/List;
    .locals 3
    .param p1, "timeMillis"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 48
    .local v1, "windowSnapshots":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/videos/subtitles/Subtitles;->windows:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 49
    iget-object v2, p0, Lcom/google/android/videos/subtitles/Subtitles;->windows:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/subtitles/SubtitleWindow;

    invoke-virtual {v2, p1}, Lcom/google/android/videos/subtitles/SubtitleWindow;->getSubtitleWindowSnapshotAt(I)Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 51
    :cond_0
    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 61
    .local v0, "builder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/videos/subtitles/Subtitles;->windows:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 62
    const-string v2, "["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, p0, Lcom/google/android/videos/subtitles/Subtitles;->windows:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/subtitles/SubtitleWindow;

    invoke-virtual {v2}, Lcom/google/android/videos/subtitles/SubtitleWindow;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 67
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
