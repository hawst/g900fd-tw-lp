.class public abstract Lcom/google/android/videos/store/AbstractFileStore;
.super Ljava/lang/Object;
.source "AbstractFileStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final converter:Lcom/google/android/videos/cache/Converter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/cache/Converter",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/videos/cache/Converter;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/videos/cache/Converter",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 49
    .local p0, "this":Lcom/google/android/videos/store/AbstractFileStore;, "Lcom/google/android/videos/store/AbstractFileStore<TK;TE;>;"
    .local p2, "converter":Lcom/google/android/videos/cache/Converter;, "Lcom/google/android/videos/cache/Converter<TE;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/videos/store/AbstractFileStore;->context:Landroid/content/Context;

    .line 51
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/cache/Converter;

    iput-object v0, p0, Lcom/google/android/videos/store/AbstractFileStore;->converter:Lcom/google/android/videos/cache/Converter;

    .line 52
    return-void
.end method

.method private closeIfOpen(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "s"    # Ljava/io/InputStream;

    .prologue
    .line 195
    .local p0, "this":Lcom/google/android/videos/store/AbstractFileStore;, "Lcom/google/android/videos/store/AbstractFileStore<TK;TE;>;"
    if-nez p1, :cond_0

    .line 203
    :goto_0
    return-void

    .line 199
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 200
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private closeIfOpen(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "s"    # Ljava/io/OutputStream;

    .prologue
    .line 184
    .local p0, "this":Lcom/google/android/videos/store/AbstractFileStore;, "Lcom/google/android/videos/store/AbstractFileStore<TK;TE;>;"
    if-nez p1, :cond_0

    .line 192
    :goto_0
    return-void

    .line 188
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 189
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private generateFilepath(Ljava/lang/Object;I)Ljava/io/File;
    .locals 2
    .param p2, "storage"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I)",
            "Ljava/io/File;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;
        }
    .end annotation

    .prologue
    .line 60
    .local p0, "this":Lcom/google/android/videos/store/AbstractFileStore;, "Lcom/google/android/videos/store/AbstractFileStore<TK;TE;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    :try_start_0
    iget-object v1, p0, Lcom/google/android/videos/store/AbstractFileStore;->context:Landroid/content/Context;

    invoke-static {v1, p2}, Lcom/google/android/videos/utils/OfflineUtil;->getRootFilesDir(Landroid/content/Context;I)Ljava/io/File;

    move-result-object v1

    invoke-virtual {p0, v1, p1}, Lcom/google/android/videos/store/AbstractFileStore;->generateFilepath(Ljava/io/File;Ljava/lang/Object;)Ljava/io/File;
    :try_end_0
    .catch Lcom/google/android/videos/utils/MediaNotMountedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 61
    :catch_0
    move-exception v0

    .line 62
    .local v0, "e":Lcom/google/android/videos/utils/MediaNotMountedException;
    new-instance v1, Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;

    invoke-direct {v1, v0}, Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;-><init>(Ljava/lang/Exception;)V

    throw v1
.end method

.method private syncAndCloseIfOpen(Ljava/io/FileOutputStream;)V
    .locals 1
    .param p1, "s"    # Ljava/io/FileOutputStream;

    .prologue
    .line 169
    .local p0, "this":Lcom/google/android/videos/store/AbstractFileStore;, "Lcom/google/android/videos/store/AbstractFileStore<TK;TE;>;"
    if-nez p1, :cond_0

    .line 181
    :goto_0
    return-void

    .line 173
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/FileDescriptor;->sync()V
    :try_end_0
    .catch Ljava/io/SyncFailedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 179
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/AbstractFileStore;->closeIfOpen(Ljava/io/OutputStream;)V

    goto :goto_0

    .line 174
    :catch_0
    move-exception v0

    .line 179
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/AbstractFileStore;->closeIfOpen(Ljava/io/OutputStream;)V

    goto :goto_0

    .line 176
    :catch_1
    move-exception v0

    .line 179
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/AbstractFileStore;->closeIfOpen(Ljava/io/OutputStream;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-direct {p0, p1}, Lcom/google/android/videos/store/AbstractFileStore;->closeIfOpen(Ljava/io/OutputStream;)V

    throw v0
.end method


# virtual methods
.method protected abstract generateFilepath(Ljava/io/File;Ljava/lang/Object;)Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "TK;)",
            "Ljava/io/File;"
        }
    .end annotation
.end method

.method public get(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 8
    .param p2, "storage"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I)TE;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;
        }
    .end annotation

    .prologue
    .line 70
    .local p0, "this":Lcom/google/android/videos/store/AbstractFileStore;, "Lcom/google/android/videos/store/AbstractFileStore<TK;TE;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/store/AbstractFileStore;->generateFilepath(Ljava/lang/Object;I)Ljava/io/File;

    move-result-object v2

    .line 72
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    .line 73
    const/4 v1, 0x0

    .line 88
    :goto_0
    return-object v1

    .line 75
    :cond_0
    const/4 v3, 0x0

    .line 76
    .local v3, "fis":Ljava/io/FileInputStream;
    const/4 v1, 0x0

    .line 78
    .local v1, "element":Ljava/lang/Object;, "TE;"
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 79
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .local v4, "fis":Ljava/io/FileInputStream;
    :try_start_1
    iget-object v5, p0, Lcom/google/android/videos/store/AbstractFileStore;->converter:Lcom/google/android/videos/cache/Converter;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-interface {v5, v4, v6, v7}, Lcom/google/android/videos/cache/Converter;->readElement(Ljava/io/InputStream;J)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    .line 86
    invoke-direct {p0, v4}, Lcom/google/android/videos/store/AbstractFileStore;->closeIfOpen(Ljava/io/InputStream;)V

    goto :goto_0

    .line 80
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    :catch_0
    move-exception v0

    .line 82
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error opening cache file (maybe removed). [file="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 83
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 84
    new-instance v5, Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;

    invoke-direct {v5, v0}, Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;-><init>(Ljava/lang/Exception;)V

    throw v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 86
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    :goto_2
    invoke-direct {p0, v3}, Lcom/google/android/videos/store/AbstractFileStore;->closeIfOpen(Ljava/io/InputStream;)V

    throw v5

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v5

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_2

    .line 80
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catch_1
    move-exception v0

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_1
.end method

.method public getLastModified(Ljava/lang/Object;I)J
    .locals 2
    .param p2, "storage"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I)J"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;
        }
    .end annotation

    .prologue
    .line 97
    .local p0, "this":Lcom/google/android/videos/store/AbstractFileStore;, "Lcom/google/android/videos/store/AbstractFileStore<TK;TE;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/store/AbstractFileStore;->generateFilepath(Ljava/lang/Object;I)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    return-wide v0
.end method

.method public put(Ljava/lang/Object;ILjava/lang/Object;)V
    .locals 9
    .param p2, "storage"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;ITE;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;
        }
    .end annotation

    .prologue
    .line 112
    .local p0, "this":Lcom/google/android/videos/store/AbstractFileStore;, "Lcom/google/android/videos/store/AbstractFileStore<TK;TE;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    .local p3, "element":Ljava/lang/Object;, "TE;"
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/store/AbstractFileStore;->generateFilepath(Ljava/lang/Object;I)Ljava/io/File;

    move-result-object v1

    .line 115
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v4

    .line 116
    .local v4, "parentDirectory":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_0

    .line 117
    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    move-result v5

    .line 118
    .local v5, "success":Z
    if-nez v5, :cond_0

    .line 119
    new-instance v6, Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "unable to create parent directory: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 123
    .end local v5    # "success":Z
    :cond_0
    const/4 v2, 0x0

    .line 125
    .local v2, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .local v3, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    iget-object v6, p0, Lcom/google/android/videos/store/AbstractFileStore;->converter:Lcom/google/android/videos/cache/Converter;

    invoke-interface {v6, p3, v3}, Lcom/google/android/videos/cache/Converter;->writeElement(Ljava/lang/Object;Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 136
    invoke-direct {p0, v3}, Lcom/google/android/videos/store/AbstractFileStore;->syncAndCloseIfOpen(Ljava/io/FileOutputStream;)V

    .line 138
    return-void

    .line 127
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v0

    .line 129
    .local v0, "e":Ljava/io/FileNotFoundException;
    :goto_0
    :try_start_2
    const-string v6, "Error creating file."

    invoke-static {v6, v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 130
    new-instance v6, Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;

    invoke-direct {v6, v0}, Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;-><init>(Ljava/lang/Exception;)V

    throw v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 136
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catchall_0
    move-exception v6

    :goto_1
    invoke-direct {p0, v2}, Lcom/google/android/videos/store/AbstractFileStore;->syncAndCloseIfOpen(Ljava/io/FileOutputStream;)V

    throw v6

    .line 131
    :catch_1
    move-exception v0

    .line 133
    .local v0, "e":Ljava/io/IOException;
    :goto_2
    :try_start_3
    const-string v6, "Error creating file."

    invoke-static {v6, v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 134
    new-instance v6, Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;

    invoke-direct {v6, v0}, Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;-><init>(Ljava/lang/Exception;)V

    throw v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 136
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v6

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    goto :goto_1

    .line 131
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v0

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 127
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v0

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    goto :goto_0
.end method

.method public remove(Ljava/lang/Object;I)V
    .locals 1
    .param p2, "storage"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;
        }
    .end annotation

    .prologue
    .line 141
    .local p0, "this":Lcom/google/android/videos/store/AbstractFileStore;, "Lcom/google/android/videos/store/AbstractFileStore<TK;TE;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/store/AbstractFileStore;->generateFilepath(Ljava/lang/Object;I)Ljava/io/File;

    move-result-object v0

    .line 143
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 144
    return-void
.end method

.method public removeByPrefix(Ljava/lang/Object;I)V
    .locals 4
    .param p2, "storage"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;
        }
    .end annotation

    .prologue
    .line 154
    .local p0, "this":Lcom/google/android/videos/store/AbstractFileStore;, "Lcom/google/android/videos/store/AbstractFileStore<TK;TE;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/store/AbstractFileStore;->generateFilepath(Ljava/lang/Object;I)Ljava/io/File;

    move-result-object v0

    .line 155
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    .line 156
    .local v2, "prefix":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    .line 157
    .local v1, "parent":Ljava/io/File;
    if-nez v1, :cond_0

    .line 166
    :goto_0
    return-void

    .line 160
    :cond_0
    new-instance v3, Lcom/google/android/videos/store/AbstractFileStore$1;

    invoke-direct {v3, p0, v2}, Lcom/google/android/videos/store/AbstractFileStore$1;-><init>(Lcom/google/android/videos/store/AbstractFileStore;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/videos/utils/OfflineUtil;->recursivelyDelete([Ljava/io/File;)V

    goto :goto_0
.end method

.method public touch(Ljava/lang/Object;I)V
    .locals 4
    .param p2, "storage"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;
        }
    .end annotation

    .prologue
    .line 107
    .local p0, "this":Lcom/google/android/videos/store/AbstractFileStore;, "Lcom/google/android/videos/store/AbstractFileStore<TK;TE;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/store/AbstractFileStore;->generateFilepath(Ljava/lang/Object;I)Ljava/io/File;

    move-result-object v0

    .line 108
    .local v0, "file":Ljava/io/File;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/io/File;->setLastModified(J)Z

    .line 109
    return-void
.end method
