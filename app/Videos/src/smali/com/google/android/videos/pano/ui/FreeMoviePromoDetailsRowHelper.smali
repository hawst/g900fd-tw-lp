.class public final Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper;
.super Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;
.source "FreeMoviePromoDetailsRowHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper$RedeemListener;
    }
.end annotation


# instance fields
.field private final activity:Landroid/app/Activity;

.field private final errorHelper:Lcom/google/android/videos/utils/ErrorHelper;

.field private final eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private final promoCode:Ljava/lang/String;

.field private final purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

.field private final redeemPromotionRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/RedeemPromotionRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$OnPurchaseActionListener;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/api/RecommendationsRequest$Factory;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/WishlistStore;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/PurchaseStoreSync;Ljava/lang/String;Lcom/google/android/videos/utils/ErrorHelper;Lcom/google/android/videos/logging/EventLogger;Landroid/content/SharedPreferences;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "itemId"    # Ljava/lang/String;
    .param p4, "onPurchaseActionListener"    # Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$OnPurchaseActionListener;
    .param p6, "relatedRequestFactory"    # Lcom/google/android/videos/api/RecommendationsRequest$Factory;
    .param p10, "purchaseStore"    # Lcom/google/android/videos/store/PurchaseStore;
    .param p11, "wishlistStore"    # Lcom/google/android/videos/store/WishlistStore;
    .param p12, "configurationStore"    # Lcom/google/android/videos/store/ConfigurationStore;
    .param p15, "purchaseStoreSync"    # Lcom/google/android/videos/store/PurchaseStoreSync;
    .param p16, "promoCode"    # Ljava/lang/String;
    .param p17, "errorHelper"    # Lcom/google/android/videos/utils/ErrorHelper;
    .param p18, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;
    .param p19, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$OnPurchaseActionListener;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;",
            "Lcom/google/android/videos/api/RecommendationsRequest$Factory;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/RecommendationsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;",
            ">;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetReviewListRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;",
            ">;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/CategoryListRequest;",
            "Lcom/google/wireless/android/video/magma/proto/CategoryListResponse;",
            ">;",
            "Lcom/google/android/videos/store/PurchaseStore;",
            "Lcom/google/android/videos/store/WishlistStore;",
            "Lcom/google/android/videos/store/ConfigurationStore;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/RedeemPromotionRequest;",
            "Ljava/lang/Void;",
            ">;",
            "Lcom/google/android/videos/store/PurchaseStoreSync;",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/utils/ErrorHelper;",
            "Lcom/google/android/videos/logging/EventLogger;",
            "Landroid/content/SharedPreferences;",
            ")V"
        }
    .end annotation

    .prologue
    .line 69
    .local p5, "assetsCachingRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;>;"
    .local p7, "relatedRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/RecommendationsRequest;Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;>;"
    .local p8, "reviewsRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/AssetReviewListRequest;Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;>;"
    .local p9, "categoryListRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/CategoryListRequest;Lcom/google/wireless/android/video/magma/proto/CategoryListResponse;>;"
    .local p13, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/async/ControllableRequest<Landroid/net/Uri;>;Landroid/graphics/Bitmap;>;"
    .local p14, "redeemPromotionRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/RedeemPromotionRequest;Ljava/lang/Void;>;"
    invoke-direct/range {p0 .. p13}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$OnPurchaseActionListener;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/api/RecommendationsRequest$Factory;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/WishlistStore;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/async/Requester;)V

    .line 72
    iput-object p1, p0, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper;->activity:Landroid/app/Activity;

    .line 73
    iput-object p14, p0, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper;->redeemPromotionRequester:Lcom/google/android/videos/async/Requester;

    .line 74
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper;->purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

    .line 75
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper;->errorHelper:Lcom/google/android/videos/utils/ErrorHelper;

    .line 76
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper;->promoCode:Ljava/lang/String;

    .line 77
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 78
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 79
    return-void
.end method


# virtual methods
.method public addDetailsSectionActions()V
    .locals 13

    .prologue
    .line 91
    iget-object v1, p0, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 92
    .local v12, "resources":Landroid/content/res/Resources;
    invoke-virtual {p0}, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper;->getAsset()Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v10

    .line 93
    .local v10, "asset":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    if-nez v10, :cond_0

    .line 106
    :goto_0
    return-void

    .line 97
    :cond_0
    new-instance v0, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;

    iget-object v1, p0, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget-object v2, p0, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper;->purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

    iget-object v3, p0, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper;->redeemPromotionRequester:Lcom/google/android/videos/async/Requester;

    new-instance v4, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v5, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper$RedeemListener;

    iget-object v6, p0, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper;->itemId:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper;->activity:Landroid/app/Activity;

    iget-object v8, p0, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper;->errorHelper:Lcom/google/android/videos/utils/ErrorHelper;

    invoke-direct {v5, v6, v7, v8}, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper$RedeemListener;-><init>(Ljava/lang/String;Landroid/app/Activity;Lcom/google/android/videos/utils/ErrorHelper;)V

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;-><init>(Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/store/PurchaseStoreSync;Lcom/google/android/videos/async/Requester;Landroid/os/Handler;Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$Listener;)V

    .line 100
    .local v0, "redeemer":Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;
    new-instance v11, Lcom/google/android/videos/welcome/PromoDismisser;

    const-string v1, "watchnowtv"

    const-string v2, "freemovie"

    iget-object v3, p0, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-direct {v11, v1, v2, v3}, Lcom/google/android/videos/welcome/PromoDismisser;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/SharedPreferences;)V

    .line 102
    .local v11, "promoDismisser":Lcom/google/android/videos/welcome/PromoDismisser;
    new-instance v1, Lcom/google/android/videos/pano/ui/RedeemPromoClickableAction;

    const-wide/16 v2, 0x0

    iget-object v4, p0, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper;->account:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper;->itemId:Ljava/lang/String;

    const/4 v6, 0x6

    iget-object v7, p0, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper;->promoCode:Ljava/lang/String;

    const v8, 0x7f0b00dd

    invoke-virtual {v12, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    move-object v8, v0

    invoke-direct/range {v1 .. v9}, Lcom/google/android/videos/pano/ui/RedeemPromoClickableAction;-><init>(JLjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper;->addAction(Landroid/support/v17/leanback/widget/Action;)V

    .line 104
    new-instance v1, Lcom/google/android/videos/pano/ui/DismissPromoClickableAction;

    const-wide/16 v2, 0x1

    const v4, 0x7f0b00de

    invoke-virtual {v12, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v11, v4}, Lcom/google/android/videos/pano/ui/DismissPromoClickableAction;-><init>(JLcom/google/android/videos/welcome/PromoDismisser;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper;->addAction(Landroid/support/v17/leanback/widget/Action;)V

    goto :goto_0
.end method

.method protected getAssetId(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "itemId"    # Ljava/lang/String;

    .prologue
    .line 123
    invoke-static {p1}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromMovieId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getRelatedRequest()Lcom/google/android/videos/api/RecommendationsRequest;
    .locals 5

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper;->relatedRequestFactory:Lcom/google/android/videos/api/RecommendationsRequest$Factory;

    iget-object v1, p0, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper;->itemId:Ljava/lang/String;

    const/16 v3, 0xf

    const/16 v4, 0x1b

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/videos/api/RecommendationsRequest$Factory;->createForRelatedMovies(Ljava/lang/String;Ljava/lang/String;II)Lcom/google/android/videos/api/RecommendationsRequest;

    move-result-object v0

    return-object v0
.end method

.method protected getWishlistRequest()Lcom/google/android/videos/store/WishlistStore$WishlistRequest;
    .locals 4

    .prologue
    .line 118
    new-instance v0, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    iget-object v1, p0, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper;->account:Ljava/lang/String;

    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper;->itemId:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    return-object v0
.end method

.method public onPurchasesUpdated()V
    .locals 0

    .prologue
    .line 87
    return-void
.end method

.method public onWatchTimestampsUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 83
    return-void
.end method
