.class Lcom/google/android/videos/store/SubtitlesClient$3;
.super Ljava/lang/Object;
.source "SubtitlesClient.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/store/SubtitlesClient;->requestOfflineSubtitleTracks(Ljava/lang/String;ILcom/google/android/videos/async/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/store/SubtitlesClient;

.field final synthetic val$callback:Lcom/google/android/videos/async/Callback;

.field final synthetic val$storage:I

.field final synthetic val$videoId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/videos/store/SubtitlesClient;Ljava/lang/String;ILcom/google/android/videos/async/Callback;)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/google/android/videos/store/SubtitlesClient$3;->this$0:Lcom/google/android/videos/store/SubtitlesClient;

    iput-object p2, p0, Lcom/google/android/videos/store/SubtitlesClient$3;->val$videoId:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/videos/store/SubtitlesClient$3;->val$storage:I

    iput-object p4, p0, Lcom/google/android/videos/store/SubtitlesClient$3;->val$callback:Lcom/google/android/videos/async/Callback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 107
    :try_start_0
    iget-object v5, p0, Lcom/google/android/videos/store/SubtitlesClient$3;->this$0:Lcom/google/android/videos/store/SubtitlesClient;

    # getter for: Lcom/google/android/videos/store/SubtitlesClient;->subtitleTracksStore:Lcom/google/android/videos/store/AbstractFileStore;
    invoke-static {v5}, Lcom/google/android/videos/store/SubtitlesClient;->access$000(Lcom/google/android/videos/store/SubtitlesClient;)Lcom/google/android/videos/store/AbstractFileStore;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/videos/store/SubtitlesClient$3;->val$videoId:Ljava/lang/String;

    iget v7, p0, Lcom/google/android/videos/store/SubtitlesClient$3;->val$storage:I

    invoke-virtual {v5, v6, v7}, Lcom/google/android/videos/store/AbstractFileStore;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 108
    .local v3, "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/subtitles/SubtitleTrack;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 109
    .local v4, "validTracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/subtitles/SubtitleTrack;>;"
    if-eqz v3, :cond_1

    .line 110
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_1

    .line 111
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/subtitles/SubtitleTrack;

    .line 113
    .local v2, "track":Lcom/google/android/videos/subtitles/SubtitleTrack;
    iget-object v5, v2, Lcom/google/android/videos/subtitles/SubtitleTrack;->videoId:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 114
    iget-object v5, v2, Lcom/google/android/videos/subtitles/SubtitleTrack;->languageCode:Ljava/lang/String;

    iget-object v6, v2, Lcom/google/android/videos/subtitles/SubtitleTrack;->trackName:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/videos/store/SubtitlesClient$3;->val$videoId:Ljava/lang/String;

    iget v8, v2, Lcom/google/android/videos/subtitles/SubtitleTrack;->format:I

    iget-boolean v9, v2, Lcom/google/android/videos/subtitles/SubtitleTrack;->isForced:Z

    invoke-static {v5, v6, v7, v8, v9}, Lcom/google/android/videos/subtitles/SubtitleTrack;->create(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)Lcom/google/android/videos/subtitles/SubtitleTrack;

    move-result-object v2

    .line 117
    :cond_0
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 120
    .end local v1    # "i":I
    .end local v2    # "track":Lcom/google/android/videos/subtitles/SubtitleTrack;
    :cond_1
    iget-object v5, p0, Lcom/google/android/videos/store/SubtitlesClient$3;->val$callback:Lcom/google/android/videos/async/Callback;

    iget-object v6, p0, Lcom/google/android/videos/store/SubtitlesClient$3;->val$videoId:Ljava/lang/String;

    invoke-interface {v5, v6, v4}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    .end local v3    # "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/subtitles/SubtitleTrack;>;"
    .end local v4    # "validTracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/subtitles/SubtitleTrack;>;"
    :goto_1
    return-void

    .line 121
    :catch_0
    move-exception v0

    .line 122
    .local v0, "e":Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;
    iget-object v5, p0, Lcom/google/android/videos/store/SubtitlesClient$3;->val$callback:Lcom/google/android/videos/async/Callback;

    iget-object v6, p0, Lcom/google/android/videos/store/SubtitlesClient$3;->val$videoId:Ljava/lang/String;

    invoke-interface {v5, v6, v0}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_1
.end method
