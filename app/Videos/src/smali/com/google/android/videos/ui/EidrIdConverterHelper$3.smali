.class Lcom/google/android/videos/ui/EidrIdConverterHelper$3;
.super Ljava/lang/Object;
.source "EidrIdConverterHelper.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/ui/EidrIdConverterHelper;->convertThroughAssetApi()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/api/AssetsRequest;",
        "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/ui/EidrIdConverterHelper;


# direct methods
.method constructor <init>(Lcom/google/android/videos/ui/EidrIdConverterHelper;)V
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper$3;->this$0:Lcom/google/android/videos/ui/EidrIdConverterHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/api/AssetsRequest;Ljava/lang/Exception;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/api/AssetsRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 179
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unable to convert "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper$3;->this$0:Lcom/google/android/videos/ui/EidrIdConverterHelper;

    # getter for: Lcom/google/android/videos/ui/EidrIdConverterHelper;->eidrId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/videos/ui/EidrIdConverterHelper;->access$400(Lcom/google/android/videos/ui/EidrIdConverterHelper;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 180
    iget-object v0, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper$3;->this$0:Lcom/google/android/videos/ui/EidrIdConverterHelper;

    # invokes: Lcom/google/android/videos/ui/EidrIdConverterHelper;->onFailed()V
    invoke-static {v0}, Lcom/google/android/videos/ui/EidrIdConverterHelper;->access$300(Lcom/google/android/videos/ui/EidrIdConverterHelper;)V

    .line 181
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 163
    check-cast p1, Lcom/google/android/videos/api/AssetsRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/EidrIdConverterHelper$3;->onError(Lcom/google/android/videos/api/AssetsRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;)V
    .locals 6
    .param p1, "request"    # Lcom/google/android/videos/api/AssetsRequest;
    .param p2, "response"    # Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    .prologue
    .line 167
    iget-object v0, p2, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .local v0, "arr$":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 168
    .local v1, "assetResource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    iget-object v4, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper$3;->this$0:Lcom/google/android/videos/ui/EidrIdConverterHelper;

    # getter for: Lcom/google/android/videos/ui/EidrIdConverterHelper;->eidrId:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/videos/ui/EidrIdConverterHelper;->access$400(Lcom/google/android/videos/ui/EidrIdConverterHelper;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, v1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v5, v5, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->titleEidrId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 169
    iget-object v4, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper$3;->this$0:Lcom/google/android/videos/ui/EidrIdConverterHelper;

    iget-object v5, v1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v5, v5, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    # invokes: Lcom/google/android/videos/ui/EidrIdConverterHelper;->onEidrIdConverted(Ljava/lang/String;)V
    invoke-static {v4, v5}, Lcom/google/android/videos/ui/EidrIdConverterHelper;->access$000(Lcom/google/android/videos/ui/EidrIdConverterHelper;Ljava/lang/String;)V

    .line 175
    .end local v1    # "assetResource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :goto_1
    return-void

    .line 167
    .restart local v1    # "assetResource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 173
    .end local v1    # "assetResource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unable to convert "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper$3;->this$0:Lcom/google/android/videos/ui/EidrIdConverterHelper;

    # getter for: Lcom/google/android/videos/ui/EidrIdConverterHelper;->eidrId:Ljava/lang/String;
    invoke-static {v5}, Lcom/google/android/videos/ui/EidrIdConverterHelper;->access$400(Lcom/google/android/videos/ui/EidrIdConverterHelper;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 174
    iget-object v4, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper$3;->this$0:Lcom/google/android/videos/ui/EidrIdConverterHelper;

    # invokes: Lcom/google/android/videos/ui/EidrIdConverterHelper;->onFailed()V
    invoke-static {v4}, Lcom/google/android/videos/ui/EidrIdConverterHelper;->access$300(Lcom/google/android/videos/ui/EidrIdConverterHelper;)V

    goto :goto_1
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 163
    check-cast p1, Lcom/google/android/videos/api/AssetsRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/EidrIdConverterHelper$3;->onResponse(Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;)V

    return-void
.end method
