.class public final Lcom/google/android/videos/proto/FilmProtos$Rectangle;
.super Lcom/google/protobuf/nano/MessageNano;
.source "FilmProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/proto/FilmProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Rectangle"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/videos/proto/FilmProtos$Rectangle;


# instance fields
.field public bottom:I

.field public left:I

.field public right:I

.field public top:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1588
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1589
    invoke-virtual {p0}, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->clear()Lcom/google/android/videos/proto/FilmProtos$Rectangle;

    .line 1590
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/videos/proto/FilmProtos$Rectangle;
    .locals 2

    .prologue
    .line 1565
    sget-object v0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->_emptyArray:[Lcom/google/android/videos/proto/FilmProtos$Rectangle;

    if-nez v0, :cond_1

    .line 1566
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 1568
    :try_start_0
    sget-object v0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->_emptyArray:[Lcom/google/android/videos/proto/FilmProtos$Rectangle;

    if-nez v0, :cond_0

    .line 1569
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/videos/proto/FilmProtos$Rectangle;

    sput-object v0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->_emptyArray:[Lcom/google/android/videos/proto/FilmProtos$Rectangle;

    .line 1571
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1573
    :cond_1
    sget-object v0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->_emptyArray:[Lcom/google/android/videos/proto/FilmProtos$Rectangle;

    return-object v0

    .line 1571
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/videos/proto/FilmProtos$Rectangle;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1593
    iput v0, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->left:I

    .line 1594
    iput v0, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->right:I

    .line 1595
    iput v0, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->top:I

    .line 1596
    iput v0, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->bottom:I

    .line 1597
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->cachedSize:I

    .line 1598
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1655
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1656
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->left:I

    if-eqz v1, :cond_0

    .line 1657
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->left:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1660
    :cond_0
    iget v1, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->right:I

    if-eqz v1, :cond_1

    .line 1661
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->right:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1664
    :cond_1
    iget v1, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->top:I

    if-eqz v1, :cond_2

    .line 1665
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->top:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1668
    :cond_2
    iget v1, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->bottom:I

    if-eqz v1, :cond_3

    .line 1669
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->bottom:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1672
    :cond_3
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1603
    if-ne p1, p0, :cond_1

    .line 1622
    :cond_0
    :goto_0
    return v1

    .line 1606
    :cond_1
    instance-of v3, p1, Lcom/google/android/videos/proto/FilmProtos$Rectangle;

    if-nez v3, :cond_2

    move v1, v2

    .line 1607
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 1609
    check-cast v0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;

    .line 1610
    .local v0, "other":Lcom/google/android/videos/proto/FilmProtos$Rectangle;
    iget v3, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->left:I

    iget v4, v0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->left:I

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 1611
    goto :goto_0

    .line 1613
    :cond_3
    iget v3, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->right:I

    iget v4, v0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->right:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 1614
    goto :goto_0

    .line 1616
    :cond_4
    iget v3, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->top:I

    iget v4, v0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->top:I

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 1617
    goto :goto_0

    .line 1619
    :cond_5
    iget v3, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->bottom:I

    iget v4, v0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->bottom:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 1620
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 1627
    const/16 v0, 0x11

    .line 1628
    .local v0, "result":I
    iget v1, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->left:I

    add-int/lit16 v0, v1, 0x20f

    .line 1629
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->right:I

    add-int v0, v1, v2

    .line 1630
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->top:I

    add-int v0, v1, v2

    .line 1631
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->bottom:I

    add-int v0, v1, v2

    .line 1632
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/FilmProtos$Rectangle;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1680
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1681
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1685
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1686
    :sswitch_0
    return-object p0

    .line 1691
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->left:I

    goto :goto_0

    .line 1695
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->right:I

    goto :goto_0

    .line 1699
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->top:I

    goto :goto_0

    .line 1703
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->bottom:I

    goto :goto_0

    .line 1681
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1559
    invoke-virtual {p0, p1}, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/FilmProtos$Rectangle;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1638
    iget v0, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->left:I

    if-eqz v0, :cond_0

    .line 1639
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->left:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1641
    :cond_0
    iget v0, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->right:I

    if-eqz v0, :cond_1

    .line 1642
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->right:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1644
    :cond_1
    iget v0, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->top:I

    if-eqz v0, :cond_2

    .line 1645
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->top:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1647
    :cond_2
    iget v0, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->bottom:I

    if-eqz v0, :cond_3

    .line 1648
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->bottom:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1650
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1651
    return-void
.end method
