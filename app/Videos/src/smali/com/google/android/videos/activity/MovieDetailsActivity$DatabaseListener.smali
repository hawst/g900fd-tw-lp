.class Lcom/google/android/videos/activity/MovieDetailsActivity$DatabaseListener;
.super Lcom/google/android/videos/store/Database$BaseListener;
.source "MovieDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/activity/MovieDetailsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DatabaseListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/activity/MovieDetailsActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/activity/MovieDetailsActivity;)V
    .locals 0

    .prologue
    .line 1026
    iput-object p1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity$DatabaseListener;->this$0:Lcom/google/android/videos/activity/MovieDetailsActivity;

    invoke-direct {p0}, Lcom/google/android/videos/store/Database$BaseListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/activity/MovieDetailsActivity;Lcom/google/android/videos/activity/MovieDetailsActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/activity/MovieDetailsActivity;
    .param p2, "x1"    # Lcom/google/android/videos/activity/MovieDetailsActivity$1;

    .prologue
    .line 1026
    invoke-direct {p0, p1}, Lcom/google/android/videos/activity/MovieDetailsActivity$DatabaseListener;-><init>(Lcom/google/android/videos/activity/MovieDetailsActivity;)V

    return-void
.end method


# virtual methods
.method public onMovieMetadataUpdated(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1030
    .local p1, "movieIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity$DatabaseListener;->this$0:Lcom/google/android/videos/activity/MovieDetailsActivity;

    # invokes: Lcom/google/android/videos/activity/MovieDetailsActivity;->onMovieMetadataUpdated(Ljava/util/List;)V
    invoke-static {v0, p1}, Lcom/google/android/videos/activity/MovieDetailsActivity;->access$1000(Lcom/google/android/videos/activity/MovieDetailsActivity;Ljava/util/List;)V

    .line 1031
    return-void
.end method

.method public onPinningStateChanged(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;

    .prologue
    .line 1040
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity$DatabaseListener;->this$0:Lcom/google/android/videos/activity/MovieDetailsActivity;

    # invokes: Lcom/google/android/videos/activity/MovieDetailsActivity;->onPinningStateChanged(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, p1, p2}, Lcom/google/android/videos/activity/MovieDetailsActivity;->access$1200(Lcom/google/android/videos/activity/MovieDetailsActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 1041
    return-void
.end method

.method public onPosterUpdated(Ljava/lang/String;)V
    .locals 1
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 1035
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity$DatabaseListener;->this$0:Lcom/google/android/videos/activity/MovieDetailsActivity;

    # invokes: Lcom/google/android/videos/activity/MovieDetailsActivity;->onPosterUpdated(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/google/android/videos/activity/MovieDetailsActivity;->access$1100(Lcom/google/android/videos/activity/MovieDetailsActivity;Ljava/lang/String;)V

    .line 1036
    return-void
.end method

.method public onScreenshotUpdated(Ljava/lang/String;)V
    .locals 1
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 1045
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity$DatabaseListener;->this$0:Lcom/google/android/videos/activity/MovieDetailsActivity;

    # invokes: Lcom/google/android/videos/activity/MovieDetailsActivity;->onScreenshotUpdated(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/google/android/videos/activity/MovieDetailsActivity;->access$1300(Lcom/google/android/videos/activity/MovieDetailsActivity;Ljava/lang/String;)V

    .line 1046
    return-void
.end method
