.class public Lcom/google/android/videos/pano/activity/PanoDialogActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "PanoDialogActivity.java"

# interfaces
.implements Lcom/google/android/recline/app/DialogFragment$Action$Listener;
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/app/FragmentActivity;",
        "Lcom/google/android/recline/app/DialogFragment$Action$Listener;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Landroid/net/Uri;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private canShowDialog:Z

.field private pendingShowDialog:Z

.field private poster:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method

.method public static createIntent(Landroid/content/Context;ILjava/lang/CharSequence;Ljava/lang/CharSequence;IILjava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dialogId"    # I
    .param p2, "title"    # Ljava/lang/CharSequence;
    .param p3, "message"    # Ljava/lang/CharSequence;
    .param p4, "positiveResourceId"    # I
    .param p5, "negativeResourceId"    # I
    .param p6, "posterUri"    # Ljava/lang/String;

    .prologue
    .line 41
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/videos/pano/activity/PanoDialogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "EXTRA_DIALOG_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_TITLE"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_MESSAGE"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_POSTER_URI"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_POSITIVE_STRING_ID"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_NEGATIVE_STRING_ID"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private maybeShowDialog()V
    .locals 9

    .prologue
    .line 82
    iget-boolean v7, p0, Lcom/google/android/videos/pano/activity/PanoDialogActivity;->canShowDialog:Z

    if-nez v7, :cond_0

    .line 83
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/google/android/videos/pano/activity/PanoDialogActivity;->pendingShowDialog:Z

    .line 106
    :goto_0
    return-void

    .line 86
    :cond_0
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/google/android/videos/pano/activity/PanoDialogActivity;->pendingShowDialog:Z

    .line 87
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/PanoDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 88
    .local v2, "intent":Landroid/content/Intent;
    const-string v7, "EXTRA_TITLE"

    invoke-virtual {v2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 89
    .local v6, "title":Ljava/lang/String;
    const-string v7, "EXTRA_MESSAGE"

    invoke-virtual {v2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 91
    .local v3, "message":Ljava/lang/String;
    new-instance v0, Ljava/util/ArrayList;

    const/4 v7, 0x2

    invoke-direct {v0, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 92
    .local v0, "actions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/recline/app/DialogFragment$Action;>;"
    const-string v7, "EXTRA_POSITIVE_STRING_ID"

    const v8, 0x1040013

    invoke-virtual {v2, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 93
    .local v5, "positiveStringId":I
    const-string v7, "EXTRA_NEGATIVE_STRING_ID"

    const v8, 0x1040009

    invoke-virtual {v2, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 94
    .local v4, "negativeStringId":I
    new-instance v7, Lcom/google/android/recline/app/DialogFragment$Action$Builder;

    invoke-direct {v7}, Lcom/google/android/recline/app/DialogFragment$Action$Builder;-><init>()V

    invoke-virtual {p0, v5}, Lcom/google/android/videos/pano/activity/PanoDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->title(Ljava/lang/String;)Lcom/google/android/recline/app/DialogFragment$Action$Builder;

    move-result-object v7

    const-string v8, "yes"

    invoke-virtual {v7, v8}, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->key(Ljava/lang/String;)Lcom/google/android/recline/app/DialogFragment$Action$Builder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->build()Lcom/google/android/recline/app/DialogFragment$Action;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    new-instance v7, Lcom/google/android/recline/app/DialogFragment$Action$Builder;

    invoke-direct {v7}, Lcom/google/android/recline/app/DialogFragment$Action$Builder;-><init>()V

    invoke-virtual {p0, v4}, Lcom/google/android/videos/pano/activity/PanoDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->title(Ljava/lang/String;)Lcom/google/android/recline/app/DialogFragment$Action$Builder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->build()Lcom/google/android/recline/app/DialogFragment$Action;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    new-instance v7, Lcom/google/android/recline/app/DialogFragment$Builder;

    invoke-direct {v7}, Lcom/google/android/recline/app/DialogFragment$Builder;-><init>()V

    invoke-virtual {v7, v6}, Lcom/google/android/recline/app/DialogFragment$Builder;->title(Ljava/lang/String;)Lcom/google/android/recline/app/DialogFragment$Builder;

    move-result-object v7

    invoke-virtual {v7, v3}, Lcom/google/android/recline/app/DialogFragment$Builder;->description(Ljava/lang/String;)Lcom/google/android/recline/app/DialogFragment$Builder;

    move-result-object v7

    const v8, 0x7f0b0080

    invoke-virtual {p0, v8}, Lcom/google/android/videos/pano/activity/PanoDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/recline/app/DialogFragment$Builder;->breadcrumb(Ljava/lang/String;)Lcom/google/android/recline/app/DialogFragment$Builder;

    move-result-object v7

    invoke-virtual {v7, v0}, Lcom/google/android/recline/app/DialogFragment$Builder;->actions(Ljava/util/ArrayList;)Lcom/google/android/recline/app/DialogFragment$Builder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/videos/pano/activity/PanoDialogActivity;->poster:Landroid/graphics/Bitmap;

    invoke-virtual {v7, v8}, Lcom/google/android/recline/app/DialogFragment$Builder;->iconBitmap(Landroid/graphics/Bitmap;)Lcom/google/android/recline/app/DialogFragment$Builder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/recline/app/DialogFragment$Builder;->build()Lcom/google/android/recline/app/DialogFragment;

    move-result-object v1

    .line 105
    .local v1, "dialogFragment":Lcom/google/android/recline/app/DialogFragment;
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/PanoDialogActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    invoke-static {v7, v1}, Lcom/google/android/recline/app/DialogFragment;->add(Landroid/app/FragmentManager;Lcom/google/android/recline/app/DialogFragment;)V

    goto/16 :goto_0
.end method

.method private requestPoster(Ljava/lang/String;)V
    .locals 4
    .param p1, "posterUri"    # Ljava/lang/String;

    .prologue
    .line 115
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v1

    .line 116
    .local v1, "videosGlobals":Lcom/google/android/videos/VideosGlobals;
    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getBitmapRequesters()Lcom/google/android/videos/bitmap/BitmapRequesters;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/videos/bitmap/BitmapRequesters;->getBitmapRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v0

    .line 119
    .local v0, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Landroid/graphics/Bitmap;>;"
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {p0, p0}, Lcom/google/android/videos/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/ActivityCallback;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 120
    return-void
.end method


# virtual methods
.method public onActionClicked(Lcom/google/android/recline/app/DialogFragment$Action;)V
    .locals 2
    .param p1, "action"    # Lcom/google/android/recline/app/DialogFragment$Action;

    .prologue
    .line 110
    const-string v0, "yes"

    invoke-virtual {p1}, Lcom/google/android/recline/app/DialogFragment$Action;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/PanoDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/videos/pano/activity/PanoDialogActivity;->setResult(ILandroid/content/Intent;)V

    .line 111
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/PanoDialogActivity;->finish()V

    .line 112
    return-void

    .line 110
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 52
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/PanoDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 56
    .local v0, "intent":Landroid/content/Intent;
    const/4 v2, 0x0

    invoke-virtual {p0, v2, v0}, Lcom/google/android/videos/pano/activity/PanoDialogActivity;->setResult(ILandroid/content/Intent;)V

    .line 58
    const-string v2, "EXTRA_POSTER_URI"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 59
    .local v1, "posterUri":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 60
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/videos/pano/activity/PanoDialogActivity;->pendingShowDialog:Z

    .line 64
    :goto_0
    return-void

    .line 62
    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/videos/pano/activity/PanoDialogActivity;->requestPoster(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onError(Landroid/net/Uri;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "request"    # Landroid/net/Uri;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/google/android/videos/pano/activity/PanoDialogActivity;->maybeShowDialog()V

    .line 133
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 21
    check-cast p1, Landroid/net/Uri;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pano/activity/PanoDialogActivity;->onError(Landroid/net/Uri;Ljava/lang/Exception;)V

    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/pano/activity/PanoDialogActivity;->canShowDialog:Z

    .line 69
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onPause()V

    .line 70
    return-void
.end method

.method public onResponse(Landroid/net/Uri;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "request"    # Landroid/net/Uri;
    .param p2, "response"    # Landroid/graphics/Bitmap;

    .prologue
    .line 126
    iput-object p2, p0, Lcom/google/android/videos/pano/activity/PanoDialogActivity;->poster:Landroid/graphics/Bitmap;

    .line 127
    invoke-direct {p0}, Lcom/google/android/videos/pano/activity/PanoDialogActivity;->maybeShowDialog()V

    .line 128
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 21
    check-cast p1, Landroid/net/Uri;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Landroid/graphics/Bitmap;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pano/activity/PanoDialogActivity;->onResponse(Landroid/net/Uri;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method protected onResumeFragments()V
    .locals 1

    .prologue
    .line 74
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResumeFragments()V

    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/pano/activity/PanoDialogActivity;->canShowDialog:Z

    .line 76
    iget-boolean v0, p0, Lcom/google/android/videos/pano/activity/PanoDialogActivity;->pendingShowDialog:Z

    if-eqz v0, :cond_0

    .line 77
    invoke-direct {p0}, Lcom/google/android/videos/pano/activity/PanoDialogActivity;->maybeShowDialog()V

    .line 79
    :cond_0
    return-void
.end method
