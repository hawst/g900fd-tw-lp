.class public final Lcom/google/android/videos/pano/repositories/GenresRepository;
.super Ljava/lang/Object;
.source "GenresRepository.java"

# interfaces
.implements Lcom/google/android/repolib/observers/Updatable;
.implements Lcom/google/android/repolib/observers/UpdatablesChanged;
.implements Lcom/google/android/repolib/repositories/Repository;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/observers/Updatable;",
        "Lcom/google/android/repolib/observers/UpdatablesChanged;",
        "Lcom/google/android/repolib/repositories/Repository",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/VideoCategory;",
        ">;"
    }
.end annotation


# instance fields
.field private final callback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/api/CategoryListRequest;",
            "Lcom/google/wireless/android/video/magma/proto/CategoryListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final categoryListRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/CategoryListRequest;",
            "Lcom/google/wireless/android/video/magma/proto/CategoryListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

.field private final networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

.field private final repository:Lcom/google/android/repolib/repositories/ArrayRepository;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/repositories/ArrayRepository",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/VideoCategory;",
            ">;"
        }
    .end annotation
.end field

.field private final signInManager:Lcom/google/android/videos/accounts/SignInManager;

.field private final updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/utils/NetworkStatus;)V
    .locals 1
    .param p2, "configurationStore"    # Lcom/google/android/videos/store/ConfigurationStore;
    .param p3, "signInManager"    # Lcom/google/android/videos/accounts/SignInManager;
    .param p4, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/CategoryListRequest;",
            "Lcom/google/wireless/android/video/magma/proto/CategoryListResponse;",
            ">;",
            "Lcom/google/android/videos/store/ConfigurationStore;",
            "Lcom/google/android/videos/accounts/SignInManager;",
            "Lcom/google/android/videos/utils/NetworkStatus;",
            ")V"
        }
    .end annotation

    .prologue
    .line 55
    .local p1, "categoryListRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/CategoryListRequest;Lcom/google/wireless/android/video/magma/proto/CategoryListResponse;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {}, Lcom/google/android/repolib/repositories/ArrayRepository;->arrayRepository()Lcom/google/android/repolib/repositories/ArrayRepository;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pano/repositories/GenresRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    .line 38
    new-instance v0, Lcom/google/android/videos/pano/repositories/GenresRepository$1;

    invoke-direct {v0, p0}, Lcom/google/android/videos/pano/repositories/GenresRepository$1;-><init>(Lcom/google/android/videos/pano/repositories/GenresRepository;)V

    iput-object v0, p0, Lcom/google/android/videos/pano/repositories/GenresRepository;->callback:Lcom/google/android/videos/async/Callback;

    .line 56
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/pano/repositories/GenresRepository;->categoryListRequester:Lcom/google/android/videos/async/Requester;

    .line 57
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/ConfigurationStore;

    iput-object v0, p0, Lcom/google/android/videos/pano/repositories/GenresRepository;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    .line 58
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/accounts/SignInManager;

    iput-object v0, p0, Lcom/google/android/videos/pano/repositories/GenresRepository;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    .line 59
    iput-object p4, p0, Lcom/google/android/videos/pano/repositories/GenresRepository;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 60
    invoke-static {p0}, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->asyncUpdateDispatcher(Lcom/google/android/repolib/observers/UpdatablesChanged;)Lcom/google/android/repolib/observers/UpdateDispatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pano/repositories/GenresRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    .line 61
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/pano/repositories/GenresRepository;)Lcom/google/android/repolib/repositories/ArrayRepository;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/repositories/GenresRepository;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/GenresRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    return-object v0
.end method

.method public static genresRepository(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/utils/NetworkStatus;)Lcom/google/android/repolib/repositories/Repository;
    .locals 1
    .param p1, "configurationStore"    # Lcom/google/android/videos/store/ConfigurationStore;
    .param p2, "signInManager"    # Lcom/google/android/videos/accounts/SignInManager;
    .param p3, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/CategoryListRequest;",
            "Lcom/google/wireless/android/video/magma/proto/CategoryListResponse;",
            ">;",
            "Lcom/google/android/videos/store/ConfigurationStore;",
            "Lcom/google/android/videos/accounts/SignInManager;",
            "Lcom/google/android/videos/utils/NetworkStatus;",
            ")",
            "Lcom/google/android/repolib/repositories/Repository",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/VideoCategory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    .local p0, "categoryListRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/CategoryListRequest;Lcom/google/wireless/android/video/magma/proto/CategoryListResponse;>;"
    new-instance v0, Lcom/google/android/videos/pano/repositories/GenresRepository;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/videos/pano/repositories/GenresRepository;-><init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/utils/NetworkStatus;)V

    return-object v0
.end method


# virtual methods
.method public addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/GenresRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 89
    return-void
.end method

.method public firstUpdatableAdded()V
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/GenresRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/GenresRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/repolib/repositories/ArrayRepository;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 74
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/GenresRepository;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0, p0}, Lcom/google/android/videos/utils/NetworkStatus;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 75
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/GenresRepository;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/accounts/SignInManager;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 76
    invoke-virtual {p0}, Lcom/google/android/videos/pano/repositories/GenresRepository;->update()V

    .line 77
    return-void
.end method

.method public indexer()Lcom/google/android/repolib/common/Indexer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/repolib/common/Indexer",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/VideoCategory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/GenresRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    invoke-virtual {v0}, Lcom/google/android/repolib/repositories/ArrayRepository;->indexer()Lcom/google/android/repolib/common/Indexer;

    move-result-object v0

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/VideoCategory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/GenresRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    invoke-virtual {v0}, Lcom/google/android/repolib/repositories/ArrayRepository;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public lastUpdatableRemoved()V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/GenresRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/GenresRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/repolib/repositories/ArrayRepository;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 82
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/GenresRepository;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0, p0}, Lcom/google/android/videos/utils/NetworkStatus;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 83
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/GenresRepository;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/accounts/SignInManager;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 84
    return-void
.end method

.method public removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/GenresRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 94
    return-void
.end method

.method public update()V
    .locals 5

    .prologue
    .line 98
    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/GenresRepository;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    invoke-virtual {v1}, Lcom/google/android/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v0

    .line 99
    .local v0, "account":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 100
    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/GenresRepository;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v1}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 101
    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/GenresRepository;->categoryListRequester:Lcom/google/android/videos/async/Requester;

    new-instance v2, Lcom/google/android/videos/api/CategoryListRequest;

    iget-object v3, p0, Lcom/google/android/videos/pano/repositories/GenresRepository;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    invoke-virtual {v3, v0}, Lcom/google/android/videos/store/ConfigurationStore;->getPlayCountry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-direct {v2, v0, v3, v4}, Lcom/google/android/videos/api/CategoryListRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;)V

    iget-object v3, p0, Lcom/google/android/videos/pano/repositories/GenresRepository;->callback:Lcom/google/android/videos/async/Callback;

    invoke-interface {v1, v2, v3}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 107
    :cond_0
    :goto_0
    return-void

    .line 105
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/GenresRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    invoke-virtual {v1}, Lcom/google/android/repolib/repositories/ArrayRepository;->clear()V

    goto :goto_0
.end method
