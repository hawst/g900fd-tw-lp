.class abstract Lcom/google/android/videos/activity/WatchActivityCompat;
.super Ljava/lang/Object;
.source "WatchActivityCompat.java"

# interfaces
.implements Lcom/google/android/videos/player/Director$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/activity/WatchActivityCompat$V11;,
        Lcom/google/android/videos/activity/WatchActivityCompat$V17;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 198
    return-void
.end method

.method public static create(Lcom/google/android/videos/activity/WatchActivity;)Lcom/google/android/videos/activity/WatchActivityCompat;
    .locals 2
    .param p0, "activity"    # Lcom/google/android/videos/activity/WatchActivity;

    .prologue
    .line 38
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/Config;->forceMirrorMode()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/videos/activity/WatchActivityCompat$V17;

    invoke-direct {v0, p0}, Lcom/google/android/videos/activity/WatchActivityCompat$V17;-><init>(Lcom/google/android/videos/activity/WatchActivity;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;

    invoke-direct {v0, p0}, Lcom/google/android/videos/activity/WatchActivityCompat$V11;-><init>(Lcom/google/android/videos/activity/WatchActivity;)V

    goto :goto_0
.end method


# virtual methods
.method public getPresentationDisplayRoute()Landroid/media/MediaRouter$RouteInfo;
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    return-object v0
.end method

.method public initPlayback(Lcom/google/android/videos/player/Director;ZZ)V
    .locals 1
    .param p1, "director"    # Lcom/google/android/videos/player/Director;
    .param p2, "firstResume"    # Z
    .param p3, "toggled"    # Z

    .prologue
    .line 43
    if-eqz p2, :cond_0

    if-nez p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/android/videos/player/Director;->initPlayback(Z)V

    .line 44
    return-void

    .line 43
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract onCreate()V
.end method

.method public abstract onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
.end method

.method public abstract onDestroy()V
.end method

.method public abstract onOptionsItemSelected(Landroid/view/MenuItem;)Z
.end method

.method public onPlaybackError()V
    .locals 0

    .prologue
    .line 67
    return-void
.end method

.method public abstract onRemoteControlChanged()V
.end method

.method public abstract onStart()V
.end method

.method public abstract onStop()V
.end method
