.class public Lcom/google/android/videos/ui/RestrictionsHelper$Receiver;
.super Landroid/content/BroadcastReceiver;
.source "RestrictionsHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/RestrictionsHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Receiver"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 315
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 319
    const-string v4, "android.intent.extra.restrictions_bundle"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 321
    .local v0, "currentRestrictions":Landroid/os/Bundle;
    invoke-static {p1}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/videos/Config;->panoEnabled()Z

    move-result v1

    .line 323
    .local v1, "pano":Z
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 324
    .local v3, "setLimitsIntent":Landroid/content/Intent;
    if-eqz v1, :cond_1

    const-class v4, Lcom/google/android/videos/pano/activity/RestrictionsActivity;

    :goto_0
    invoke-virtual {v3, p1, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 327
    if-eqz v0, :cond_0

    .line 329
    const-string v4, "android.intent.extra.restrictions_bundle"

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 332
    :cond_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 333
    .local v2, "result":Landroid/os/Bundle;
    const-string v4, "android.intent.extra.restrictions_intent"

    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 334
    const/4 v4, -0x1

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5, v2}, Lcom/google/android/videos/ui/RestrictionsHelper$Receiver;->setResult(ILjava/lang/String;Landroid/os/Bundle;)V

    .line 335
    return-void

    .line 324
    .end local v2    # "result":Landroid/os/Bundle;
    :cond_1
    const-class v4, Lcom/google/android/videos/activity/RestrictionsActivity;

    goto :goto_0
.end method
