.class public final Lcom/google/android/videos/utils/Util;
.super Ljava/lang/Object;
.source "Util.java"


# static fields
.field public static final SDK_INT:I

.field private static final languagesCodeToName:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static maxH264DecodableFrameSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 56
    const-string v0, "L"

    sget-object v1, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x15

    :goto_0
    sput v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    .line 66
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/videos/utils/Util;->languagesCodeToName:Ljava/util/Map;

    .line 69
    const/4 v0, -0x1

    sput v0, Lcom/google/android/videos/utils/Util;->maxH264DecodableFrameSize:I

    return-void

    .line 56
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    goto :goto_0
.end method

.method public static areEqual(Landroid/content/Intent;Landroid/content/Intent;)Z
    .locals 1
    .param p0, "o1"    # Landroid/content/Intent;
    .param p1, "o2"    # Landroid/content/Intent;

    .prologue
    .line 177
    if-eqz p0, :cond_0

    .line 178
    invoke-virtual {p0, p1}, Landroid/content/Intent;->filterEquals(Landroid/content/Intent;)Z

    move-result v0

    .line 180
    :goto_0
    return v0

    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p0, "o1"    # Ljava/lang/Object;
    .param p1, "o2"    # Ljava/lang/Object;

    .prologue
    .line 169
    if-eqz p0, :cond_0

    .line 170
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 172
    :goto_0
    return v0

    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static areProtosEqual(Lcom/google/protobuf/nano/MessageNano;Lcom/google/protobuf/nano/MessageNano;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/protobuf/nano/MessageNano;",
            ">(TT;TT;)Z"
        }
    .end annotation

    .prologue
    .line 185
    .local p0, "p1":Lcom/google/protobuf/nano/MessageNano;, "TT;"
    .local p1, "p2":Lcom/google/protobuf/nano/MessageNano;, "TT;"
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 186
    invoke-static {p0}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v0

    invoke-static {p1}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    .line 188
    :goto_0
    return v0

    :cond_0
    if-nez p0, :cond_1

    if-nez p1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static buildListString(Landroid/content/res/Resources;ZLjava/util/List;)Ljava/lang/String;
    .locals 6
    .param p0, "resources"    # Landroid/content/res/Resources;
    .param p1, "heterogeneous"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 310
    .local p2, "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 311
    :cond_0
    const/4 v3, 0x0

    .line 326
    :cond_1
    return-object v3

    .line 313
    :cond_2
    if-eqz p1, :cond_3

    const v2, 0x7f0b013a

    .line 314
    .local v2, "joinerResId":I
    :goto_0
    const/4 v3, 0x0

    .line 315
    .local v3, "result":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 316
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 317
    .local v1, "item":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 315
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 313
    .end local v0    # "i":I
    .end local v1    # "item":Ljava/lang/String;
    .end local v2    # "joinerResId":I
    .end local v3    # "result":Ljava/lang/String;
    :cond_3
    const v2, 0x7f0b0139

    goto :goto_0

    .line 320
    .restart local v0    # "i":I
    .restart local v1    # "item":Ljava/lang/String;
    .restart local v2    # "joinerResId":I
    .restart local v3    # "result":Ljava/lang/String;
    :cond_4
    if-nez v3, :cond_5

    .line 321
    move-object v3, v1

    goto :goto_2

    .line 323
    :cond_5
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    const/4 v5, 0x1

    aput-object v1, v4, v5

    invoke-virtual {p0, v2, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2
.end method

.method public static varargs buildListString(Landroid/content/res/Resources;Z[Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "resources"    # Landroid/content/res/Resources;
    .param p1, "heterogeneous"    # Z
    .param p2, "items"    # [Ljava/lang/String;

    .prologue
    .line 341
    if-eqz p2, :cond_0

    array-length v0, p2

    if-nez v0, :cond_1

    .line 342
    :cond_0
    const/4 v0, 0x0

    .line 344
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/google/android/videos/utils/Util;->buildListString(Landroid/content/res/Resources;ZLjava/util/List;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static buildUserAgent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "applicationVersion"    # Ljava/lang/String;
    .param p2, "experimentId"    # Ljava/lang/String;

    .prologue
    .line 254
    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 260
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    const/16 v3, 0x2f

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 262
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    const-string v3, "(Linux; U; Android "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    const-string v3, "; "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 271
    .local v2, "model":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 272
    const-string v3, "; "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    :cond_0
    sget-object v1, Landroid/os/Build;->ID:Ljava/lang/String;

    .line 277
    .local v1, "id":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 278
    const-string v3, " Build/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 282
    :cond_1
    const/16 v3, 0x29

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 284
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 285
    const-string v3, " Experiment/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 286
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 289
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static canResolveIntent(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 348
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 349
    .local v0, "pm":Landroid/content/pm/PackageManager;
    const/high16 v2, 0x10000

    invoke-virtual {v0, p1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 351
    .local v1, "resolveInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static closeQuietly(Ljava/io/Closeable;)V
    .locals 3
    .param p0, "c"    # Ljava/io/Closeable;

    .prologue
    .line 578
    if-nez p0, :cond_0

    .line 586
    :goto_0
    return-void

    .line 582
    :cond_0
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 583
    :catch_0
    move-exception v0

    .line 584
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to close "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static closeQuietly(Ljava/nio/channels/FileChannel;Z)V
    .locals 2
    .param p0, "c"    # Ljava/nio/channels/FileChannel;
    .param p1, "force"    # Z

    .prologue
    .line 561
    if-nez p0, :cond_0

    .line 575
    :goto_0
    return-void

    .line 565
    :cond_0
    if-eqz p1, :cond_1

    .line 566
    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0, v1}, Ljava/nio/channels/FileChannel;->force(Z)V
    :try_end_0
    .catch Ljava/io/SyncFailedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 573
    :cond_1
    invoke-static {p0}, Lcom/google/android/videos/utils/Util;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    .line 568
    :catch_0
    move-exception v0

    .line 569
    .local v0, "e":Ljava/io/SyncFailedException;
    :try_start_1
    const-string v1, "Failed to force FileChannel"

    invoke-static {v1, v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 573
    invoke-static {p0}, Lcom/google/android/videos/utils/Util;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    .line 570
    .end local v0    # "e":Ljava/io/SyncFailedException;
    :catch_1
    move-exception v0

    .line 571
    .local v0, "e":Ljava/io/IOException;
    :try_start_2
    const-string v1, "Failed to force FileChannel"

    invoke-static {v1, v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 573
    invoke-static {p0}, Lcom/google/android/videos/utils/Util;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    invoke-static {p0}, Lcom/google/android/videos/utils/Util;->closeQuietly(Ljava/io/Closeable;)V

    throw v1
.end method

.method public static createToast(Landroid/content/Context;II)Landroid/widget/Toast;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resId"    # I
    .param p2, "duration"    # I

    .prologue
    .line 96
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    return-object v0
.end method

.method public static createToast(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "duration"    # I

    .prologue
    .line 100
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    return-object v0
.end method

.method public static deviceSupportsStreamResolution(Lcom/google/android/videos/Config;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/streams/MediaStream;)Z
    .locals 3
    .param p0, "config"    # Lcom/google/android/videos/Config;
    .param p1, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;
    .param p2, "stream"    # Lcom/google/android/videos/streams/MediaStream;

    .prologue
    const/4 v0, 0x1

    .line 437
    invoke-static {}, Lcom/google/android/videos/utils/Util;->initMaxH264DecodableFrameSize()V

    .line 440
    iget-object v1, p2, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-object v1, v1, Lcom/google/android/videos/proto/StreamInfo;->codec:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p2, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-object v1, v1, Lcom/google/android/videos/proto/StreamInfo;->codec:Ljava/lang/String;

    const-string v2, "avc"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p2, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    iget v1, v1, Lcom/google/android/videos/streams/ItagInfo;->width:I

    iget-object v2, p2, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    iget v2, v2, Lcom/google/android/videos/streams/ItagInfo;->height:I

    mul-int/2addr v1, v2

    sget v2, Lcom/google/android/videos/utils/Util;->maxH264DecodableFrameSize:I

    if-le v1, v2, :cond_0

    .line 442
    sget v1, Lcom/google/android/videos/utils/Util;->maxH264DecodableFrameSize:I

    invoke-interface {p0}, Lcom/google/android/videos/Config;->deviceCapabilitiesFilterEnabled()Z

    move-result v2

    invoke-interface {p1, p2, v1, v2}, Lcom/google/android/videos/logging/EventLogger;->onDeviceCapabilitiesStreamFilter(Lcom/google/android/videos/streams/MediaStream;IZ)V

    .line 444
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Filtered out H264 stream because the device is not capable of decoding it. Maximum H264 resolution: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/google/android/videos/utils/Util;->maxH264DecodableFrameSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". The stream: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 447
    invoke-interface {p0}, Lcom/google/android/videos/Config;->deviceCapabilitiesFilterEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 449
    :cond_0
    :goto_0
    return v0

    .line 447
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;TT;)TT;"
        }
    .end annotation

    .prologue
    .line 615
    .local p0, "first":Ljava/lang/Object;, "TT;"
    .local p1, "second":Ljava/lang/Object;, "TT;"
    if-eqz p0, :cond_0

    .end local p0    # "first":Ljava/lang/Object;, "TT;"
    :goto_0
    return-object p0

    .restart local p0    # "first":Ljava/lang/Object;, "TT;"
    :cond_0
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    goto :goto_0
.end method

.method public static getAvailableSize(Ljava/lang/String;)J
    .locals 4
    .param p0, "dirPath"    # Ljava/lang/String;

    .prologue
    .line 589
    new-instance v0, Landroid/os/StatFs;

    invoke-direct {v0, p0}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 590
    .local v0, "statFs":Landroid/os/StatFs;
    sget v1, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v2, 0x12

    if-ge v1, v2, :cond_0

    invoke-static {v0}, Lcom/google/android/videos/utils/Util;->getAvailableSizeV8(Landroid/os/StatFs;)J

    move-result-wide v2

    :goto_0
    return-wide v2

    :cond_0
    invoke-static {v0}, Lcom/google/android/videos/utils/Util;->getAvailableSizeV18(Landroid/os/StatFs;)J

    move-result-wide v2

    goto :goto_0
.end method

.method private static getAvailableSizeV18(Landroid/os/StatFs;)J
    .locals 4
    .param p0, "statFs"    # Landroid/os/StatFs;

    .prologue
    .line 603
    invoke-virtual {p0}, Landroid/os/StatFs;->getAvailableBlocksLong()J

    move-result-wide v0

    invoke-virtual {p0}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v2

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method private static getAvailableSizeV8(Landroid/os/StatFs;)J
    .locals 4
    .param p0, "statFs"    # Landroid/os/StatFs;

    .prologue
    .line 608
    invoke-virtual {p0}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v2

    int-to-long v2, v2

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public static getBestStoryboard(Landroid/content/Context;[Lcom/google/wireless/android/video/magma/proto/Storyboard;)Lcom/google/wireless/android/video/magma/proto/Storyboard;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "storyboard"    # [Lcom/google/wireless/android/video/magma/proto/Storyboard;

    .prologue
    .line 509
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e01d9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 511
    .local v0, "thumbnailHeight":I
    invoke-static {p1, v0}, Lcom/google/android/videos/utils/Util;->getBestStoryboard([Lcom/google/wireless/android/video/magma/proto/Storyboard;I)Lcom/google/wireless/android/video/magma/proto/Storyboard;

    move-result-object v1

    return-object v1
.end method

.method public static getBestStoryboard([Lcom/google/wireless/android/video/magma/proto/Storyboard;I)Lcom/google/wireless/android/video/magma/proto/Storyboard;
    .locals 7
    .param p0, "candidates"    # [Lcom/google/wireless/android/video/magma/proto/Storyboard;
    .param p1, "thumbnailHeight"    # I

    .prologue
    .line 518
    if-eqz p0, :cond_0

    array-length v5, p0

    if-nez v5, :cond_2

    .line 519
    :cond_0
    const/4 v0, 0x0

    .line 532
    :cond_1
    return-object v0

    .line 521
    :cond_2
    const/4 v5, 0x0

    aget-object v0, p0, v5

    .line 522
    .local v0, "best":Lcom/google/wireless/android/video/magma/proto/Storyboard;
    iget v5, v0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameHeight:I

    sub-int v5, p1, v5

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 523
    .local v1, "bestDiff":I
    const/4 v4, 0x1

    .local v4, "i":I
    :goto_0
    array-length v5, p0

    if-ge v4, v5, :cond_1

    .line 524
    aget-object v2, p0, v4

    .line 525
    .local v2, "candidate":Lcom/google/wireless/android/video/magma/proto/Storyboard;
    iget v5, v2, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameHeight:I

    sub-int v5, p1, v5

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 526
    .local v3, "candidateDiff":I
    if-lt v3, v1, :cond_3

    if-ne v3, v1, :cond_4

    iget v5, v2, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameHeight:I

    iget v6, v0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameHeight:I

    if-le v5, v6, :cond_4

    .line 528
    :cond_3
    move-object v0, v2

    .line 529
    move v1, v3

    .line 523
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public static getLanguageMatchScore(Ljava/lang/String;Ljava/lang/String;)I
    .locals 6
    .param p0, "first"    # Ljava/lang/String;
    .param p1, "second"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 401
    if-nez p0, :cond_0

    new-array v0, v5, [Ljava/lang/String;

    .line 402
    .local v0, "firstComponents":[Ljava/lang/String;
    :goto_0
    if-nez p1, :cond_1

    new-array v3, v5, [Ljava/lang/String;

    .line 403
    .local v3, "secondComponents":[Ljava/lang/String;
    :goto_1
    const/4 v2, 0x0

    .line 405
    .local v2, "score":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    array-length v4, v0

    if-ge v1, v4, :cond_2

    array-length v4, v3

    if-ge v1, v4, :cond_2

    .line 406
    aget-object v4, v0, v1

    aget-object v5, v3, v1

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 407
    add-int/lit8 v2, v2, 0x1

    .line 405
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 401
    .end local v0    # "firstComponents":[Ljava/lang/String;
    .end local v1    # "i":I
    .end local v2    # "score":I
    .end local v3    # "secondComponents":[Ljava/lang/String;
    :cond_0
    const-string v4, "[-_]"

    invoke-virtual {p0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 402
    .restart local v0    # "firstComponents":[Ljava/lang/String;
    :cond_1
    const-string v4, "[-_]"

    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 412
    .restart local v1    # "i":I
    .restart local v2    # "score":I
    .restart local v3    # "secondComponents":[Ljava/lang/String;
    :cond_2
    array-length v4, v0

    array-length v5, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    if-ne v2, v4, :cond_3

    .line 413
    const v2, 0x7fffffff

    .line 415
    :cond_3
    return v2
.end method

.method public static getLanguageName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "languageCode"    # Ljava/lang/String;

    .prologue
    .line 369
    sget-object v2, Lcom/google/android/videos/utils/Util;->languagesCodeToName:Ljava/util/Map;

    invoke-interface {v2, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 370
    .local v0, "languageName":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 371
    invoke-static {p0}, Lcom/google/android/videos/utils/Util;->getLocaleFor(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v1

    .line 372
    .local v1, "locale":Ljava/util/Locale;
    invoke-virtual {v1, v1}, Ljava/util/Locale;->getDisplayName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/videos/utils/Util;->toTitleCase(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 373
    sget-object v2, Lcom/google/android/videos/utils/Util;->languagesCodeToName:Ljava/util/Map;

    invoke-interface {v2, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375
    .end local v1    # "locale":Ljava/util/Locale;
    :cond_0
    return-object v0
.end method

.method public static getLocaleFor(Ljava/lang/String;)Ljava/util/Locale;
    .locals 5
    .param p0, "languageCode"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 379
    const-string v1, "[_-]"

    const/4 v2, 0x3

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    .line 380
    .local v0, "parts":[Ljava/lang/String;
    array-length v1, v0

    packed-switch v1, :pswitch_data_0

    .line 388
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 382
    :pswitch_0
    new-instance v1, Ljava/util/Locale;

    aget-object v2, v0, v3

    invoke-direct {v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    .line 386
    :goto_0
    return-object v1

    .line 384
    :pswitch_1
    new-instance v1, Ljava/util/Locale;

    aget-object v2, v0, v3

    aget-object v3, v0, v4

    invoke-direct {v1, v2, v3}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 386
    :pswitch_2
    new-instance v1, Ljava/util/Locale;

    aget-object v2, v0, v3

    aget-object v3, v0, v4

    const/4 v4, 0x2

    aget-object v4, v0, v4

    invoke-direct {v1, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 380
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static getTextForLocale(Landroid/content/Context;Ljava/util/Locale;I)Ljava/lang/CharSequence;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "locale"    # Ljava/util/Locale;
    .param p2, "resourceId"    # I

    .prologue
    .line 497
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 498
    invoke-virtual {p0, p2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 502
    :goto_0
    return-object v1

    .line 500
    :cond_0
    new-instance v0, Landroid/content/res/Configuration;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    .line 501
    .local v0, "config":Landroid/content/res/Configuration;
    invoke-virtual {v0, p1}, Landroid/content/res/Configuration;->setLocale(Ljava/util/Locale;)V

    .line 502
    invoke-virtual {p0, v0}, Landroid/content/Context;->createConfigurationContext(Landroid/content/res/Configuration;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0
.end method

.method public static handlesMenuKeyEvent(ILandroid/app/Activity;)Z
    .locals 3
    .param p0, "keyCode"    # I
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 627
    const/16 v1, 0x52

    if-ne p0, v1, :cond_1

    const-string v1, "LGE"

    sget-object v2, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 628
    .local v0, "shouldHandle":Z
    :goto_0
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 629
    invoke-virtual {p1}, Landroid/app/Activity;->openOptionsMenu()V

    .line 631
    :cond_0
    return v0

    .line 627
    .end local v0    # "shouldHandle":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static hashCode(Landroid/content/Intent;)I
    .locals 1
    .param p0, "obj"    # Landroid/content/Intent;

    .prologue
    .line 598
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Intent;->filterHashCode()I

    move-result v0

    goto :goto_0
.end method

.method public static hashCode(Ljava/lang/Object;)I
    .locals 1
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 594
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method private static declared-synchronized initMaxH264DecodableFrameSize()V
    .locals 4

    .prologue
    .line 453
    const-class v2, Lcom/google/android/videos/utils/Util;

    monitor-enter v2

    :try_start_0
    sget v1, Lcom/google/android/videos/utils/Util;->maxH264DecodableFrameSize:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v3, -0x1

    if-eq v1, v3, :cond_1

    .line 470
    :cond_0
    :goto_0
    monitor-exit v2

    return-void

    .line 458
    :cond_1
    const v1, 0x7fffffff

    :try_start_1
    sput v1, Lcom/google/android/videos/utils/Util;->maxH264DecodableFrameSize:I

    .line 459
    sget v1, Lcom/google/android/videos/utils/Util;->SDK_INT:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/16 v3, 0x13

    if-lt v1, v3, :cond_0

    .line 461
    :try_start_2
    invoke-static {}, Lcom/google/android/exoplayer/MediaCodecUtil;->maxH264DecodableFrameSize()I

    move-result v1

    sput v1, Lcom/google/android/videos/utils/Util;->maxH264DecodableFrameSize:I
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 462
    :catch_0
    move-exception v0

    .line 464
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_3
    const-string v1, "Failed to calculate maximum frame size"

    invoke-static {v1, v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 453
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 465
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 467
    .local v0, "e":Ljava/lang/IllegalStateException;
    :try_start_4
    const-string v1, "Failed to calculate maximum frame size"

    invoke-static {v1, v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public static isAtEndOfMovie(III)Z
    .locals 4
    .param p0, "positionMillis"    # I
    .param p1, "movieLengthMillis"    # I
    .param p2, "endCreditStartSeconds"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 482
    if-gtz p1, :cond_1

    move v0, v1

    .line 489
    :cond_0
    :goto_0
    return v0

    .line 485
    :cond_1
    if-lez p2, :cond_2

    .line 486
    mul-int/lit16 v2, p2, 0x3e8

    if-ge p0, v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 489
    :cond_2
    sub-int v2, p1, p0

    const/16 v3, 0x7d0

    if-lt v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static isGoogleTv(Landroid/content/pm/PackageManager;)Z
    .locals 1
    .param p0, "packageManager"    # Landroid/content/pm/PackageManager;

    .prologue
    .line 74
    const-string v0, "com.google.android.tv"

    invoke-virtual {p0, v0}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isTv(Landroid/content/pm/PackageManager;)Z
    .locals 1
    .param p0, "packageManager"    # Landroid/content/pm/PackageManager;

    .prologue
    .line 78
    invoke-static {p0}, Lcom/google/android/videos/utils/Util;->isGoogleTv(Landroid/content/pm/PackageManager;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "android.hardware.type.television"

    invoke-virtual {p0, v0}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFloat(Ljava/lang/String;F)F
    .locals 2
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "defaultValue"    # F

    .prologue
    .line 127
    if-eqz p0, :cond_0

    :try_start_0
    invoke-static {p0}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    .line 129
    .end local p1    # "defaultValue":F
    :cond_0
    :goto_0
    return p1

    .line 128
    .restart local p1    # "defaultValue":F
    :catch_0
    move-exception v0

    .line 129
    .local v0, "e":Ljava/lang/NumberFormatException;
    goto :goto_0
.end method

.method public static parseInt(Ljava/lang/String;I)I
    .locals 2
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "defaultValue"    # I

    .prologue
    .line 116
    if-eqz p0, :cond_0

    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    .line 118
    .end local p1    # "defaultValue":I
    :cond_0
    :goto_0
    return p1

    .line 117
    .restart local p1    # "defaultValue":I
    :catch_0
    move-exception v0

    .line 118
    .local v0, "e":Ljava/lang/NumberFormatException;
    goto :goto_0
.end method

.method public static protoHashCode(Lcom/google/protobuf/nano/MessageNano;)I
    .locals 1
    .param p0, "proto"    # Lcom/google/protobuf/nano/MessageNano;

    .prologue
    .line 193
    if-nez p0, :cond_0

    .line 194
    const/4 v0, 0x0

    .line 196
    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    goto :goto_0
.end method

.method public static setMenuItemEnabled(Landroid/view/Menu;IZ)V
    .locals 1
    .param p0, "menu"    # Landroid/view/Menu;
    .param p1, "itemId"    # I
    .param p2, "enabled"    # Z

    .prologue
    .line 355
    invoke-interface {p0, p1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/videos/utils/Util;->setMenuItemEnabled(Landroid/view/MenuItem;Z)V

    .line 356
    return-void
.end method

.method public static setMenuItemEnabled(Landroid/view/MenuItem;Z)V
    .locals 0
    .param p0, "menuItem"    # Landroid/view/MenuItem;
    .param p1, "enabled"    # Z

    .prologue
    .line 359
    if-eqz p0, :cond_0

    .line 360
    invoke-interface {p0, p1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 361
    invoke-interface {p0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 363
    :cond_0
    return-void
.end method

.method public static showToast(Landroid/content/Context;II)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resId"    # I
    .param p2, "duration"    # I

    .prologue
    .line 104
    invoke-static {p0, p1, p2}, Lcom/google/android/videos/utils/Util;->createToast(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 105
    return-void
.end method

.method public static showToast(Landroid/content/Context;Ljava/lang/CharSequence;I)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "duration"    # I

    .prologue
    .line 108
    invoke-static {p0, p1, p2}, Lcom/google/android/videos/utils/Util;->createToast(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 109
    return-void
.end method

.method public static splitIntegersToList(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 7
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "delimiter"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 157
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 158
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 165
    :cond_0
    return-object v4

    .line 160
    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 161
    .local v5, "split":[Ljava/lang/String;
    new-instance v4, Ljava/util/ArrayList;

    array-length v6, v5

    invoke-direct {v4, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 162
    .local v4, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    move-object v0, v5

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v2, v0, v1

    .line 163
    .local v2, "integer":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static splitIntegersToSet(Ljava/lang/String;Ljava/lang/String;)Ljava/util/HashSet;
    .locals 7
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "delimiter"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 145
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 146
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 153
    :cond_0
    return-object v4

    .line 148
    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 149
    .local v5, "split":[Ljava/lang/String;
    new-instance v4, Ljava/util/HashSet;

    array-length v6, v5

    invoke-direct {v4, v6}, Ljava/util/HashSet;-><init>(I)V

    .line 150
    .local v4, "set":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    move-object v0, v5

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v2, v0, v1

    .line 151
    .local v2, "integer":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 150
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static terminateInputStream(Ljava/net/HttpURLConnection;)V
    .locals 5
    .param p0, "connection"    # Ljava/net/HttpURLConnection;

    .prologue
    .line 545
    :try_start_0
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 546
    .local v0, "input":Ljava/io/InputStream;
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.android.okhttp.internal.http.HttpTransport$FixedLengthInputStream"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 548
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "unexpectedEndOfInput"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 550
    .local v1, "unexpectedEndOfInput":Ljava/lang/reflect/Method;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 551
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 558
    .end local v0    # "input":Ljava/io/InputStream;
    .end local v1    # "unexpectedEndOfInput":Ljava/lang/reflect/Method;
    :cond_0
    :goto_0
    return-void

    .line 555
    :catch_0
    move-exception v2

    goto :goto_0

    .line 553
    :catch_1
    move-exception v2

    goto :goto_0
.end method

.method public static toLowerInvariant(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 203
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static toTitleCase(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/String;
    .locals 5
    .param p0, "input"    # Ljava/lang/String;
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    const/4 v4, 0x0

    .line 419
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 425
    .end local p0    # "input":Ljava/lang/String;
    :goto_0
    return-object p0

    .line 422
    .restart local p0    # "input":Ljava/lang/String;
    :cond_0
    const/4 v3, 0x1

    invoke-virtual {p0, v4, v3}, Ljava/lang/String;->offsetByCodePoints(II)I

    move-result v1

    .line 423
    .local v1, "indexOfSecondCodePoint":I
    invoke-virtual {p0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 424
    .local v0, "firstChar":Ljava/lang/String;
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 425
    .local v2, "remainder":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static toUpperInvariant(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 210
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static wipeDatabase(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 13
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 218
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "type"

    aput-object v0, v2, v1

    const-string v0, "name"

    aput-object v0, v2, v4

    .line 219
    .local v2, "columns":[Ljava/lang/String;
    const-string v1, "sqlite_master"

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 221
    .local v8, "cursor":Landroid/database/Cursor;
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 222
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 223
    .local v12, "type":Ljava/lang/String;
    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 224
    .local v10, "name":Ljava/lang/String;
    const-string v0, "sqlite_sequence"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 226
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DROP "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " IF EXISTS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v11

    .line 228
    .local v11, "sql":Ljava/lang/String;
    :try_start_1
    invoke-virtual {p0, v11}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 229
    :catch_0
    move-exception v9

    .line 230
    .local v9, "e":Landroid/database/SQLException;
    :try_start_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error executing "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v9}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 235
    .end local v9    # "e":Landroid/database/SQLException;
    .end local v10    # "name":Ljava/lang/String;
    .end local v11    # "sql":Ljava/lang/String;
    .end local v12    # "type":Ljava/lang/String;
    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 237
    return-void
.end method
