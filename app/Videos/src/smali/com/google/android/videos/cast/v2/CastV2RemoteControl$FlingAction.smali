.class Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;
.super Ljava/lang/Object;
.source "CastV2RemoteControl.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;
.implements Lcom/google/android/videos/utils/RetryAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/cast/v2/CastV2RemoteControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FlingAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/api/MpdUrlGetRequest;",
        "Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;",
        ">;",
        "Lcom/google/android/videos/utils/RetryAction;"
    }
.end annotation


# instance fields
.field private final contentInfo:Lorg/json/JSONObject;

.field private final isEpisode:Z

.field private final isTrailer:Z

.field private final resumeTimeMillis:I

.field final synthetic this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

.field private final videoInfo:Lcom/google/android/videos/remote/RemoteVideoInfo;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Lcom/google/android/videos/remote/RemoteVideoInfo;IZZLorg/json/JSONObject;)V
    .locals 0
    .param p2, "videoInfo"    # Lcom/google/android/videos/remote/RemoteVideoInfo;
    .param p3, "resumeTimeMillis"    # I
    .param p4, "isTrailer"    # Z
    .param p5, "isEpisode"    # Z
    .param p6, "contentInfo"    # Lorg/json/JSONObject;

    .prologue
    .line 911
    iput-object p1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 912
    iput-object p2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->videoInfo:Lcom/google/android/videos/remote/RemoteVideoInfo;

    .line 913
    iput p3, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->resumeTimeMillis:I

    .line 914
    iput-boolean p4, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->isTrailer:Z

    .line 915
    iput-boolean p5, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->isEpisode:Z

    .line 916
    iput-object p6, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->contentInfo:Lorg/json/JSONObject;

    .line 917
    return-void
.end method

.method static synthetic access$2600(Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;

    .prologue
    .line 903
    invoke-direct {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->onCompleted()V

    return-void
.end method

.method static synthetic access$2700(Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;

    .prologue
    .line 903
    invoke-direct {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->onCancelled()V

    return-void
.end method

.method private onCancelled()V
    .locals 0

    .prologue
    .line 1011
    return-void
.end method

.method private onCompleted()V
    .locals 4

    .prologue
    .line 997
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # getter for: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->flingInProgress:Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;
    invoke-static {v0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$2200(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;

    move-result-object v0

    if-eq v0, p0, :cond_0

    .line 1007
    :goto_0
    return-void

    .line 1000
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->videoInfo:Lcom/google/android/videos/remote/RemoteVideoInfo;

    iget-object v1, v1, Lcom/google/android/videos/remote/RemoteVideoInfo;->videoId:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->resumeTimeMillis:I

    const/4 v3, 0x1

    # invokes: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->seekTo(Ljava/lang/String;II)Z
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$2800(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Ljava/lang/String;II)Z

    .line 1003
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # getter for: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->account:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$500(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1004
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # getter for: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->account:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$500(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->videoInfo:Lcom/google/android/videos/remote/RemoteVideoInfo;

    iget-boolean v3, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->isEpisode:Z

    # invokes: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->requestRecommendations(Ljava/lang/String;Lcom/google/android/videos/remote/RemoteVideoInfo;Z)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$2900(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Ljava/lang/String;Lcom/google/android/videos/remote/RemoteVideoInfo;Z)V

    .line 1006
    :cond_1
    invoke-direct {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->onFlingCompleted()V

    goto :goto_0
.end method

.method private onFlingCompleted()V
    .locals 2

    .prologue
    .line 1020
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    invoke-virtual {v0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->getError()I

    move-result v0

    const/16 v1, -0x3ea

    if-ne v0, v1, :cond_0

    .line 1021
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    invoke-virtual {v0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->clearError()V

    .line 1023
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->flingInProgress:Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;
    invoke-static {v0, v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$2202(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;)Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;

    .line 1024
    return-void
.end method


# virtual methods
.method public invoke()I
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 920
    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # setter for: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->flingInProgress:Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;
    invoke-static {v1, p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$2202(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;)Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;

    .line 923
    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->videoInfo:Lcom/google/android/videos/remote/RemoteVideoInfo;

    iget-object v2, v2, Lcom/google/android/videos/remote/RemoteVideoInfo;->videoId:Ljava/lang/String;

    # invokes: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->isPlaying(Ljava/lang/String;)Z
    invoke-static {v1, v2}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$2000(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 924
    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    invoke-virtual {v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->getPlayerState()Lcom/google/android/videos/remote/PlayerState;

    move-result-object v1

    iget v1, v1, Lcom/google/android/videos/remote/PlayerState;->state:I

    packed-switch v1, :pswitch_data_0

    .line 936
    :cond_0
    const-string v1, "Requesting mpd url"

    invoke-static {v1}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 937
    new-instance v0, Lcom/google/android/videos/api/MpdUrlGetRequest;

    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # getter for: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->account:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$500(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->isTrailer:Z

    if-nez v2, :cond_1

    move v2, v5

    :goto_0
    iget-object v3, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->videoInfo:Lcom/google/android/videos/remote/RemoteVideoInfo;

    iget-object v3, v3, Lcom/google/android/videos/remote/RemoteVideoInfo;->videoId:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->isEpisode:Z

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/api/MpdUrlGetRequest;-><init>(Ljava/lang/String;ZLjava/lang/String;ZZLjava/util/Locale;)V

    .line 939
    .local v0, "request":Lcom/google/android/videos/api/MpdUrlGetRequest;
    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # getter for: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->mpdUrlGetRequester:Lcom/google/android/videos/async/Requester;
    invoke-static {v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$2400(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Lcom/google/android/videos/async/Requester;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # getter for: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$2300(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Landroid/os/Handler;

    move-result-object v2

    invoke-static {v2, p0}, Lcom/google/android/videos/async/HandlerCallback;->create(Landroid/os/Handler;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/HandlerCallback;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 940
    .end local v0    # "request":Lcom/google/android/videos/api/MpdUrlGetRequest;
    :goto_1
    return v7

    .line 929
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->onFlingCompleted()V

    .line 930
    const/4 v7, -0x1

    goto :goto_1

    :cond_1
    move v2, v7

    .line 937
    goto :goto_0

    .line 924
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onError(Lcom/google/android/videos/api/MpdUrlGetRequest;Ljava/lang/Exception;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/api/MpdUrlGetRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 988
    const-string v0, "Failed to get mpd url"

    invoke-static {v0, p2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 989
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # getter for: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->flingInProgress:Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;
    invoke-static {v0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$2200(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;

    move-result-object v0

    if-eq v0, p0, :cond_0

    .line 994
    :goto_0
    return-void

    .line 993
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    const/16 v1, -0x3ea

    invoke-virtual {v0, v1, p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->onError(ILcom/google/android/videos/utils/RetryAction;)V

    goto :goto_0
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 903
    check-cast p1, Lcom/google/android/videos/api/MpdUrlGetRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->onError(Lcom/google/android/videos/api/MpdUrlGetRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/api/MpdUrlGetRequest;Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;)V
    .locals 6
    .param p1, "request"    # Lcom/google/android/videos/api/MpdUrlGetRequest;
    .param p2, "response"    # Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;

    .prologue
    const/4 v5, 0x1

    .line 951
    const-string v3, "Mpd url fetched"

    invoke-static {v3}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 952
    iget-object v3, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # getter for: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->flingInProgress:Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;
    invoke-static {v3}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$2200(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;

    move-result-object v3

    if-eq v3, p0, :cond_1

    .line 984
    :cond_0
    :goto_0
    return-void

    .line 956
    :cond_1
    iget-object v3, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # getter for: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->state:I
    invoke-static {v3}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$300(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    .line 963
    iget-object v0, p2, Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;->resource:Ljava/lang/String;

    .line 965
    .local v0, "contentId":Ljava/lang/String;
    new-instance v3, Lcom/google/android/gms/cast/MediaInfo$Builder;

    invoke-direct {v3, v0}, Lcom/google/android/gms/cast/MediaInfo$Builder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Lcom/google/android/gms/cast/MediaInfo$Builder;->setStreamType(I)Lcom/google/android/gms/cast/MediaInfo$Builder;

    move-result-object v3

    const-string v4, "video/mp4"

    invoke-virtual {v3, v4}, Lcom/google/android/gms/cast/MediaInfo$Builder;->setContentType(Ljava/lang/String;)Lcom/google/android/gms/cast/MediaInfo$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->contentInfo:Lorg/json/JSONObject;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/cast/MediaInfo$Builder;->setCustomData(Lorg/json/JSONObject;)Lcom/google/android/gms/cast/MediaInfo$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/cast/MediaInfo$Builder;->build()Lcom/google/android/gms/cast/MediaInfo;

    move-result-object v1

    .line 972
    .local v1, "mediaInformation":Lcom/google/android/gms/cast/MediaInfo;
    iget-object v3, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # getter for: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->remoteMediaPlayerChannel:Lcom/google/android/gms/cast/RemoteMediaPlayer;
    invoke-static {v3}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$2500(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Lcom/google/android/gms/cast/RemoteMediaPlayer;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # getter for: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;
    invoke-static {v4}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$700(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v4

    invoke-virtual {v3, v4, v1, v5}, Lcom/google/android/gms/cast/RemoteMediaPlayer;->load(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/gms/cast/MediaInfo;Z)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v2

    .line 974
    .local v2, "pendingResult":Lcom/google/android/gms/common/api/PendingResult;, "Lcom/google/android/gms/common/api/PendingResult<Lcom/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult;>;"
    new-instance v3, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction$1;

    invoke-direct {v3, p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction$1;-><init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;)V

    invoke-interface {v2, v3}, Lcom/google/android/gms/common/api/PendingResult;->setResultCallback(Lcom/google/android/gms/common/api/ResultCallback;)V

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 903
    check-cast p1, Lcom/google/android/videos/api/MpdUrlGetRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->onResponse(Lcom/google/android/videos/api/MpdUrlGetRequest;Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;)V

    return-void
.end method

.method public onRetry()V
    .locals 0

    .prologue
    .line 1016
    invoke-virtual {p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$FlingAction;->invoke()I

    .line 1017
    return-void
.end method
