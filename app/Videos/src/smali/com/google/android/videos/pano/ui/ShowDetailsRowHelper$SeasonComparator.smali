.class Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$SeasonComparator;
.super Ljava/lang/Object;
.source "ShowDetailsRowHelper.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SeasonComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 742
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$1;

    .prologue
    .line 742
    invoke-direct {p0}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$SeasonComparator;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/AssetResource;)I
    .locals 5
    .param p1, "lhs"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p2, "rhs"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    .line 746
    iget-object v3, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v1, v3, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->sequenceNumber:Ljava/lang/String;

    .line 747
    .local v1, "lhsSequenceNumber":Ljava/lang/String;
    iget-object v3, p2, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v2, v3, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->sequenceNumber:Ljava/lang/String;

    .line 748
    .local v2, "rhsSequenceNumber":Ljava/lang/String;
    # invokes: Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeComparator;->parse(Ljava/lang/String;)I
    invoke-static {v1}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeComparator;->access$600(Ljava/lang/String;)I

    move-result v3

    # invokes: Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeComparator;->parse(Ljava/lang/String;)I
    invoke-static {v2}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeComparator;->access$600(Ljava/lang/String;)I

    move-result v4

    sub-int v0, v3, v4

    .line 750
    .local v0, "diff":I
    if-eqz v0, :cond_0

    .end local v0    # "diff":I
    :goto_0
    return v0

    .restart local v0    # "diff":I
    :cond_0
    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 742
    check-cast p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$SeasonComparator;->compare(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/AssetResource;)I

    move-result v0

    return v0
.end method
