.class Lcom/google/android/videos/pano/activity/RestrictionsActivity$1;
.super Ljava/lang/Object;
.source "RestrictionsActivity.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/pano/activity/RestrictionsActivity;->updateActions()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pano/activity/RestrictionsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/videos/pano/activity/RestrictionsActivity;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity$1;->this$0:Lcom/google/android/videos/pano/activity/RestrictionsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 137
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/lang/String;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pano/activity/RestrictionsActivity$1;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public compare(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2
    .param p1, "key1"    # Ljava/lang/String;
    .param p2, "key2"    # Ljava/lang/String;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity$1;->this$0:Lcom/google/android/videos/pano/activity/RestrictionsActivity;

    # getter for: Lcom/google/android/videos/pano/activity/RestrictionsActivity;->preferences:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->access$100(Lcom/google/android/videos/pano/activity/RestrictionsActivity;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/Preference;

    invoke-virtual {v0}, Landroid/preference/Preference;->getOrder()I

    move-result v1

    iget-object v0, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity$1;->this$0:Lcom/google/android/videos/pano/activity/RestrictionsActivity;

    # getter for: Lcom/google/android/videos/pano/activity/RestrictionsActivity;->preferences:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->access$100(Lcom/google/android/videos/pano/activity/RestrictionsActivity;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/Preference;

    invoke-virtual {v0}, Landroid/preference/Preference;->getOrder()I

    move-result v0

    sub-int v0, v1, v0

    return v0
.end method
