.class public abstract Lcom/google/android/videos/adapter/WishlistDataSource$Query;
.super Ljava/lang/Object;
.source "WishlistDataSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/adapter/WishlistDataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Query"
.end annotation


# static fields
.field private static final PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 67
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "wishlist_item_type"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "video_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "shows_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "shows_title"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "wishlist_account"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/videos/adapter/WishlistDataSource$Query;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public static createRequestForMovies(Ljava/lang/String;)Lcom/google/android/videos/store/WishlistStore$WishlistRequest;
    .locals 7
    .param p0, "account"    # Ljava/lang/String;

    .prologue
    .line 77
    new-instance v0, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    const-string v1, "wishlist LEFT JOIN shows ON wishlist_item_type = 18 AND wishlist_item_id = shows_id LEFT JOIN videos ON wishlist_item_type = 6 AND wishlist_item_id = video_id"

    sget-object v2, Lcom/google/android/videos/adapter/WishlistDataSource$Query;->PROJECTION:[Ljava/lang/String;

    const-string v3, "wishlist_account = ? AND wishlist_item_state != 3 AND video_id IS NOT NULL"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const-string v5, "wishlist_item_order"

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;-><init>(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static createRequestForShows(Ljava/lang/String;)Lcom/google/android/videos/store/WishlistStore$WishlistRequest;
    .locals 7
    .param p0, "account"    # Ljava/lang/String;

    .prologue
    .line 82
    new-instance v0, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    const-string v1, "wishlist LEFT JOIN shows ON wishlist_item_type = 18 AND wishlist_item_id = shows_id LEFT JOIN videos ON wishlist_item_type = 6 AND wishlist_item_id = video_id"

    sget-object v2, Lcom/google/android/videos/adapter/WishlistDataSource$Query;->PROJECTION:[Ljava/lang/String;

    const-string v3, "wishlist_account = ? AND wishlist_item_state != 3 AND shows_id IS NOT NULL"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const-string v5, "wishlist_item_order"

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;-><init>(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
