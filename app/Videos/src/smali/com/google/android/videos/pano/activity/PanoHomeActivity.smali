.class public final Lcom/google/android/videos/pano/activity/PanoHomeActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "PanoHomeActivity.java"

# interfaces
.implements Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/repolib/observers/Updatable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/pano/activity/PanoHomeActivity$NonEmptyListRowPredicate;,
        Lcom/google/android/videos/pano/activity/PanoHomeActivity$VideoCategoryTypePredicate;
    }
.end annotation


# instance fields
.field private backgroundHelper:Lcom/google/android/videos/pano/ui/BackgroundHelper;

.field private networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

.field private objectAdapter:Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/leanback/RepositoryObjectAdapter",
            "<",
            "Landroid/support/v17/leanback/widget/ListRow;",
            ">;"
        }
    .end annotation
.end field

.field private repository:Lcom/google/android/repolib/repositories/Repository;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/repositories/Repository",
            "<",
            "Landroid/support/v17/leanback/widget/ListRow;",
            ">;"
        }
    .end annotation
.end field

.field private syncHelper:Lcom/google/android/videos/ui/SyncHelper;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 102
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 364
    return-void
.end method

.method public static createIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 109
    new-instance v0, Landroid/content/ComponentName;

    const-class v1, Lcom/google/android/videos/pano/activity/PanoHomeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {v0}, Landroid/content/Intent;->makeRestartActivityTask(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/videos/ui/SyncHelper;->onActivityResult(IILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 316
    :goto_0
    return-void

    .line 315
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 340
    invoke-static {p0}, Lcom/google/android/videos/utils/PlayStoreUtil;->startPanoSearch(Landroid/app/Activity;)V

    .line 341
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 43
    .param p1, "savedInstance"    # Landroid/os/Bundle;

    .prologue
    .line 120
    invoke-super/range {p0 .. p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 121
    invoke-static/range {p0 .. p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v42

    .line 122
    .local v42, "videosGlobals":Lcom/google/android/videos/VideosGlobals;
    invoke-virtual/range {v42 .. v42}, Lcom/google/android/videos/VideosGlobals;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v6

    .line 123
    .local v6, "database":Lcom/google/android/videos/store/Database;
    invoke-virtual/range {v42 .. v42}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStore()Lcom/google/android/videos/store/PurchaseStore;

    move-result-object v7

    .line 124
    .local v7, "purchaseStore":Lcom/google/android/videos/store/PurchaseStore;
    invoke-virtual/range {v42 .. v42}, Lcom/google/android/videos/VideosGlobals;->getWishlistStore()Lcom/google/android/videos/store/WishlistStore;

    move-result-object v8

    .line 125
    .local v8, "wishlistStore":Lcom/google/android/videos/store/WishlistStore;
    invoke-virtual/range {v42 .. v42}, Lcom/google/android/videos/VideosGlobals;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v4

    .line 126
    .local v4, "signInManager":Lcom/google/android/videos/accounts/SignInManager;
    invoke-virtual/range {v42 .. v42}, Lcom/google/android/videos/VideosGlobals;->getLocaleObservable()Lcom/google/android/repolib/observers/Observable;

    move-result-object v16

    .line 127
    .local v16, "localeObservable":Lcom/google/android/repolib/observers/Observable;
    new-instance v3, Lcom/google/android/videos/pano/repositories/StoreStatusObservable;

    move-object/from16 v5, p0

    invoke-direct/range {v3 .. v8}, Lcom/google/android/videos/pano/repositories/StoreStatusObservable;-><init>(Lcom/google/android/videos/accounts/SignInManager;Landroid/content/Context;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/WishlistStore;)V

    .line 129
    .local v3, "storeStatusObservable":Lcom/google/android/videos/pano/repositories/StoreStatusObservable;
    invoke-virtual/range {v42 .. v42}, Lcom/google/android/videos/VideosGlobals;->getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v20

    .line 130
    .local v20, "configurationStore":Lcom/google/android/videos/store/ConfigurationStore;
    invoke-virtual/range {v42 .. v42}, Lcom/google/android/videos/VideosGlobals;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v27

    .line 131
    .local v27, "apiRequesters":Lcom/google/android/videos/api/ApiRequesters;
    invoke-virtual/range {v42 .. v42}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/videos/Config;->suggestionsEnabled()Z

    move-result v38

    .line 132
    .local v38, "suggestionsEnabled":Z
    invoke-virtual/range {v42 .. v42}, Lcom/google/android/videos/VideosGlobals;->getDataSources()Lcom/google/android/videos/pano/datasource/DataSources;

    move-result-object v31

    .line 133
    .local v31, "dataSources":Lcom/google/android/videos/pano/datasource/DataSources;
    invoke-interface/range {v27 .. v27}, Lcom/google/android/videos/api/ApiRequesters;->getCategoryListRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v30

    .line 136
    .local v30, "categoryListRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/CategoryListRequest;Lcom/google/wireless/android/video/magma/proto/CategoryListResponse;>;"
    invoke-interface/range {v27 .. v27}, Lcom/google/android/videos/api/ApiRequesters;->getVideoCollectionListRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v41

    .line 138
    .local v41, "videoCollectionListRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/VideoCollectionListRequest;Lcom/google/wireless/android/video/magma/proto/VideoCollectionListResponse;>;"
    invoke-virtual/range {v42 .. v42}, Lcom/google/android/videos/VideosGlobals;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 139
    new-instance v9, Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual/range {v42 .. v42}, Lcom/google/android/videos/VideosGlobals;->getGcmRegistrationManager()Lcom/google/android/videos/gcm/GcmRegistrationManager;

    move-result-object v11

    invoke-virtual/range {v42 .. v42}, Lcom/google/android/videos/VideosGlobals;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v12

    invoke-virtual/range {v42 .. v42}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStoreSync()Lcom/google/android/videos/store/PurchaseStoreSync;

    move-result-object v14

    invoke-virtual/range {v42 .. v42}, Lcom/google/android/videos/VideosGlobals;->getWishlistStoreSync()Lcom/google/android/videos/store/WishlistStoreSync;

    move-result-object v15

    move-object/from16 v10, p0

    move-object v13, v4

    invoke-direct/range {v9 .. v15}, Lcom/google/android/videos/ui/SyncHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/gcm/GcmRegistrationManager;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/PurchaseStoreSync;Lcom/google/android/videos/store/WishlistStoreSync;)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    .line 143
    invoke-virtual/range {v42 .. v42}, Lcom/google/android/videos/VideosGlobals;->getBitmapRequesters()Lcom/google/android/videos/bitmap/BitmapRequesters;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/videos/bitmap/BitmapRequesters;->getControllableBitmapRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v28

    .line 145
    .local v28, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/async/ControllableRequest<Landroid/net/Uri;>;Landroid/graphics/Bitmap;>;"
    new-instance v34, Lcom/google/android/videos/pano/binders/MovieBinder;

    move-object/from16 v0, v34

    move-object/from16 v1, v28

    move-object/from16 v2, p0

    invoke-direct {v0, v1, v2}, Lcom/google/android/videos/pano/binders/MovieBinder;-><init>(Lcom/google/android/videos/async/Requester;Landroid/app/Activity;)V

    .line 146
    .local v34, "movieBinder":Lcom/google/android/videos/pano/binders/MovieBinder;
    new-instance v26, Lcom/google/android/repolib/leanback/ObjectAdapterFactory;

    invoke-direct/range {v26 .. v26}, Lcom/google/android/repolib/leanback/ObjectAdapterFactory;-><init>()V

    .line 147
    .local v26, "adapterFactory":Lcom/google/android/repolib/leanback/ObjectAdapterFactory;, "Lcom/google/android/repolib/leanback/ObjectAdapterFactory<Ljava/lang/Object;>;"
    const-class v5, Lcom/google/android/videos/pano/model/ShowItem;

    const v10, 0x7f040082

    new-instance v11, Lcom/google/android/videos/pano/binders/ShowBinder;

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    invoke-direct {v11, v0, v1}, Lcom/google/android/videos/pano/binders/ShowBinder;-><init>(Lcom/google/android/videos/async/Requester;Landroid/app/Activity;)V

    invoke-static {v10, v11}, Lcom/google/android/repolib/ui/ViewBinder;->viewBinder(ILcom/google/android/repolib/common/Binder;)Lcom/google/android/repolib/ui/ViewBinder;

    move-result-object v10

    move-object/from16 v0, v26

    invoke-virtual {v0, v5, v10}, Lcom/google/android/repolib/leanback/ObjectAdapterFactory;->bind(Ljava/lang/Class;Lcom/google/android/repolib/ui/ViewBinder;)V

    .line 149
    const-class v5, Lcom/google/android/videos/pano/model/MovieItem;

    const v10, 0x7f04007f

    move-object/from16 v0, v34

    invoke-static {v10, v0}, Lcom/google/android/repolib/ui/ViewBinder;->viewBinder(ILcom/google/android/repolib/common/Binder;)Lcom/google/android/repolib/ui/ViewBinder;

    move-result-object v10

    move-object/from16 v0, v26

    invoke-virtual {v0, v5, v10}, Lcom/google/android/repolib/leanback/ObjectAdapterFactory;->bind(Ljava/lang/Class;Lcom/google/android/repolib/ui/ViewBinder;)V

    .line 151
    const-class v5, Lcom/google/android/videos/pano/model/PromoItem;

    const v10, 0x7f04007f

    move-object/from16 v0, v34

    invoke-static {v10, v0}, Lcom/google/android/repolib/ui/ViewBinder;->viewBinder(ILcom/google/android/repolib/common/Binder;)Lcom/google/android/repolib/ui/ViewBinder;

    move-result-object v10

    move-object/from16 v0, v26

    invoke-virtual {v0, v5, v10}, Lcom/google/android/repolib/leanback/ObjectAdapterFactory;->bind(Ljava/lang/Class;Lcom/google/android/repolib/ui/ViewBinder;)V

    .line 153
    const-class v5, Lcom/google/android/videos/pano/model/LibraryItem;

    const v10, 0x7f04007e

    new-instance v11, Lcom/google/android/videos/pano/activity/PanoHomeActivity$1;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Lcom/google/android/videos/pano/activity/PanoHomeActivity$1;-><init>(Lcom/google/android/videos/pano/activity/PanoHomeActivity;)V

    invoke-static {v11}, Lcom/google/android/videos/pano/binders/LibraryItemViewBinder;->libraryItemViewBinder(Lcom/google/android/repolib/common/Action;)Lcom/google/android/repolib/common/Binder;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/google/android/repolib/ui/ViewBinder;->viewBinder(ILcom/google/android/repolib/common/Binder;)Lcom/google/android/repolib/ui/ViewBinder;

    move-result-object v10

    move-object/from16 v0, v26

    invoke-virtual {v0, v5, v10}, Lcom/google/android/repolib/leanback/ObjectAdapterFactory;->bind(Ljava/lang/Class;Lcom/google/android/repolib/ui/ViewBinder;)V

    .line 162
    const-class v5, Lcom/google/android/videos/pano/model/UriItem;

    const v10, 0x7f04007e

    new-instance v11, Lcom/google/android/videos/pano/activity/PanoHomeActivity$2;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Lcom/google/android/videos/pano/activity/PanoHomeActivity$2;-><init>(Lcom/google/android/videos/pano/activity/PanoHomeActivity;)V

    invoke-static {v11}, Lcom/google/android/videos/pano/binders/UriItemViewBinder;->uriItemViewBinder(Lcom/google/android/repolib/common/Action;)Lcom/google/android/repolib/common/Binder;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/google/android/repolib/ui/ViewBinder;->viewBinder(ILcom/google/android/repolib/common/Binder;)Lcom/google/android/repolib/ui/ViewBinder;

    move-result-object v10

    move-object/from16 v0, v26

    invoke-virtual {v0, v5, v10}, Lcom/google/android/repolib/leanback/ObjectAdapterFactory;->bind(Ljava/lang/Class;Lcom/google/android/repolib/ui/ViewBinder;)V

    .line 178
    const-class v5, Lcom/google/wireless/android/video/magma/proto/VideoCategory;

    const v10, 0x7f04007e

    new-instance v11, Lcom/google/android/videos/pano/binders/GenreBinder;

    move-object/from16 v0, v28

    invoke-direct {v11, v0}, Lcom/google/android/videos/pano/binders/GenreBinder;-><init>(Lcom/google/android/videos/async/Requester;)V

    invoke-static {v10, v11}, Lcom/google/android/repolib/ui/ViewBinder;->viewBinder(ILcom/google/android/repolib/common/Binder;)Lcom/google/android/repolib/ui/ViewBinder;

    move-result-object v10

    move-object/from16 v0, v26

    invoke-virtual {v0, v5, v10}, Lcom/google/android/repolib/leanback/ObjectAdapterFactory;->bind(Ljava/lang/Class;Lcom/google/android/repolib/ui/ViewBinder;)V

    .line 182
    invoke-virtual/range {v42 .. v42}, Lcom/google/android/videos/VideosGlobals;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/videos/api/ApiRequesters;->getVideoCollectionGetRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v25

    .line 185
    .local v25, "videoCollectionGetRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/VideoCollectionGetRequest;Lcom/google/wireless/android/video/magma/proto/VideoCollectionGetResponse;>;"
    new-instance v9, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;

    const/16 v10, 0x14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    move-object v11, v6

    move-object v12, v4

    move-object/from16 v13, v20

    move-object/from16 v14, v41

    move-object/from16 v17, v3

    invoke-direct/range {v9 .. v17}, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;-><init>(ILcom/google/android/videos/store/Database;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/repolib/observers/Observable;Lcom/google/android/videos/pano/repositories/StoreStatusObservable;)V

    .line 189
    .local v9, "videoCollectionsRepository":Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    move-object/from16 v0, v30

    move-object/from16 v1, v20

    invoke-static {v0, v1, v4, v5}, Lcom/google/android/videos/pano/repositories/GenresRepository;->genresRepository(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/utils/NetworkStatus;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v32

    .line 192
    .local v32, "genresRepository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<Lcom/google/wireless/android/video/magma/proto/VideoCategory;>;"
    const/16 v17, 0x32

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    move-object/from16 v23, v0

    move-object/from16 v18, p0

    move-object/from16 v19, v4

    move-object/from16 v21, v6

    move-object/from16 v22, v7

    move-object/from16 v24, v16

    invoke-static/range {v17 .. v25}, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->createTopMoviesRepository(ILandroid/content/Context;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/repolib/observers/Observable;Lcom/google/android/videos/async/Requester;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v39

    .line 195
    .local v39, "topMoviesRepository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<Lcom/google/android/videos/pano/model/VideoItem;>;"
    const/16 v17, 0x32

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    move-object/from16 v23, v0

    move-object/from16 v18, p0

    move-object/from16 v19, v4

    move-object/from16 v21, v6

    move-object/from16 v22, v7

    move-object/from16 v24, v16

    invoke-static/range {v17 .. v25}, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->createTopShowsRepository(ILandroid/content/Context;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/repolib/observers/Observable;Lcom/google/android/videos/async/Requester;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v40

    .line 199
    .local v40, "topShowsRepository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<Lcom/google/android/videos/pano/model/VideoItem;>;"
    new-instance v33, Ljava/util/ArrayList;

    invoke-direct/range {v33 .. v33}, Ljava/util/ArrayList;-><init>()V

    .line 200
    .local v33, "libraryItems":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/videos/pano/model/LibraryItem;>;"
    new-instance v5, Lcom/google/android/videos/pano/model/LibraryItem;

    const v10, 0x7f0200d2

    const v11, 0x7f0b0094

    invoke-static/range {p0 .. p0}, Lcom/google/android/videos/pano/activity/VideoGridActivity;->createMyMoviesIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v12

    invoke-direct {v5, v10, v11, v12}, Lcom/google/android/videos/pano/model/LibraryItem;-><init>(IILandroid/content/Intent;)V

    move-object/from16 v0, v33

    invoke-interface {v0, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 202
    new-instance v5, Lcom/google/android/videos/pano/model/LibraryItem;

    const v10, 0x7f0200d3

    const v11, 0x7f0b0095

    invoke-static/range {p0 .. p0}, Lcom/google/android/videos/pano/activity/VideoGridActivity;->createMyShowsIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v12

    invoke-direct {v5, v10, v11, v12}, Lcom/google/android/videos/pano/model/LibraryItem;-><init>(IILandroid/content/Intent;)V

    move-object/from16 v0, v33

    invoke-interface {v0, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 205
    if-eqz v38, :cond_0

    .line 206
    new-instance v5, Lcom/google/android/videos/pano/model/LibraryItem;

    const v10, 0x7f0200d4

    const v11, 0x7f0b0097

    invoke-static/range {p0 .. p0}, Lcom/google/android/videos/pano/activity/VideoGridActivity;->createWishlistIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v12

    invoke-direct {v5, v10, v11, v12}, Lcom/google/android/videos/pano/model/LibraryItem;-><init>(IILandroid/content/Intent;)V

    move-object/from16 v0, v33

    invoke-interface {v0, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 210
    :cond_0
    const/4 v5, 0x2

    new-array v5, v5, [Lcom/google/android/videos/pano/model/UriItem;

    const/4 v10, 0x0

    new-instance v11, Lcom/google/android/videos/pano/model/UriItem;

    const v12, 0x7f0200bb

    const v13, 0x7f0b009c

    const-string v14, "https://g.co/PlayTVHelp"

    const v15, 0x7f0b009c

    invoke-direct {v11, v12, v13, v14, v15}, Lcom/google/android/videos/pano/model/UriItem;-><init>(IILjava/lang/String;I)V

    aput-object v11, v5, v10

    const/4 v10, 0x1

    new-instance v11, Lcom/google/android/videos/pano/model/UriItem;

    const v12, 0x7f0200e2

    const v13, 0x7f0b022d

    const-string v14, "file:///android_asset/license.html"

    const v15, 0x7f0b022e

    invoke-direct {v11, v12, v13, v14, v15}, Lcom/google/android/videos/pano/model/UriItem;-><init>(IILjava/lang/String;I)V

    aput-object v11, v5, v10

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v36

    .line 216
    .local v36, "settingsItems":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/videos/pano/model/UriItem;>;"
    if-eqz v38, :cond_2

    const/4 v5, 0x2

    new-array v5, v5, [Lcom/google/android/repolib/leanback/RepositoryListRow;

    const/4 v10, 0x0

    const v11, 0x7f0b00a2

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, v39

    invoke-static {v11, v0}, Lcom/google/android/repolib/leanback/RepositoryListRow;->repositoryListRow(Ljava/lang/String;Lcom/google/android/repolib/repositories/Repository;)Lcom/google/android/repolib/leanback/RepositoryListRow;

    move-result-object v11

    aput-object v11, v5, v10

    const/4 v10, 0x1

    const v11, 0x7f0b00a3

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, v40

    invoke-static {v11, v0}, Lcom/google/android/repolib/leanback/RepositoryListRow;->repositoryListRow(Ljava/lang/String;Lcom/google/android/repolib/repositories/Repository;)Lcom/google/android/repolib/leanback/RepositoryListRow;

    move-result-object v11

    aput-object v11, v5, v10

    move-object/from16 v0, v26

    invoke-static {v0, v5}, Lcom/google/android/repolib/leanback/ListRowRepository;->listRowRepository(Lcom/google/android/repolib/leanback/ObjectAdapterFactory;[Lcom/google/android/repolib/leanback/RepositoryListRow;)Lcom/google/android/repolib/leanback/ListRowRepository;

    move-result-object v5

    new-instance v10, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository;

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v10, v0, v1, v3, v9}, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionLeanbackRepository;-><init>(Landroid/content/Context;Lcom/google/android/repolib/leanback/ObjectAdapterFactory;Lcom/google/android/videos/pano/repositories/StoreStatusObservable;Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;)V

    const/4 v11, 0x1

    new-array v11, v11, [Lcom/google/android/repolib/repositories/Repository;

    const/4 v12, 0x0

    const/4 v13, 0x2

    new-array v13, v13, [Lcom/google/android/repolib/leanback/RepositoryListRow;

    const/4 v14, 0x0

    const v15, 0x7f0b0098

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v15

    new-instance v17, Lcom/google/android/videos/pano/activity/PanoHomeActivity$VideoCategoryTypePredicate;

    const/16 v18, 0x1

    const/16 v19, 0x0

    invoke-direct/range {v17 .. v19}, Lcom/google/android/videos/pano/activity/PanoHomeActivity$VideoCategoryTypePredicate;-><init>(ILcom/google/android/videos/pano/activity/PanoHomeActivity$1;)V

    move-object/from16 v0, v32

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/google/android/repolib/repositories/FilteringRepository;->filteringRepository(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/common/Predicate;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-static {v15, v0}, Lcom/google/android/repolib/leanback/RepositoryListRow;->repositoryListRow(Ljava/lang/String;Lcom/google/android/repolib/repositories/Repository;)Lcom/google/android/repolib/leanback/RepositoryListRow;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const v15, 0x7f0b0099

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v15

    new-instance v17, Lcom/google/android/videos/pano/activity/PanoHomeActivity$VideoCategoryTypePredicate;

    const/16 v18, 0x2

    const/16 v19, 0x0

    invoke-direct/range {v17 .. v19}, Lcom/google/android/videos/pano/activity/PanoHomeActivity$VideoCategoryTypePredicate;-><init>(ILcom/google/android/videos/pano/activity/PanoHomeActivity$1;)V

    move-object/from16 v0, v32

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/google/android/repolib/repositories/FilteringRepository;->filteringRepository(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/common/Predicate;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-static {v15, v0}, Lcom/google/android/repolib/leanback/RepositoryListRow;->repositoryListRow(Ljava/lang/String;Lcom/google/android/repolib/repositories/Repository;)Lcom/google/android/repolib/leanback/RepositoryListRow;

    move-result-object v15

    aput-object v15, v13, v14

    move-object/from16 v0, v26

    invoke-static {v0, v13}, Lcom/google/android/repolib/leanback/ListRowRepository;->listRowRepository(Lcom/google/android/repolib/leanback/ObjectAdapterFactory;[Lcom/google/android/repolib/leanback/RepositoryListRow;)Lcom/google/android/repolib/leanback/ListRowRepository;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v5, v10, v11}, Lcom/google/android/repolib/repositories/ConcatenatingRepository;->concatenatingRepository(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/repositories/Repository;[Lcom/google/android/repolib/repositories/Repository;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v37

    .line 233
    .local v37, "storeRows":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<Landroid/support/v17/leanback/widget/ListRow;>;"
    :goto_0
    const/4 v5, 0x2

    new-array v5, v5, [Lcom/google/android/repolib/leanback/RepositoryListRow;

    const/4 v10, 0x0

    const v11, 0x7f0b0096

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, v31

    iget-object v12, v0, Lcom/google/android/videos/pano/datasource/DataSources;->watchNowDataSource:Lcom/google/android/videos/pano/datasource/BaseDataSource;

    invoke-static {v12}, Lcom/google/android/videos/pano/repositories/DataSourceRepository;->dataSourceRepository(Lcom/google/android/videos/pano/datasource/BaseDataSource;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/google/android/repolib/leanback/RepositoryListRow;->repositoryListRow(Ljava/lang/String;Lcom/google/android/repolib/repositories/Repository;)Lcom/google/android/repolib/leanback/RepositoryListRow;

    move-result-object v11

    aput-object v11, v5, v10

    const/4 v10, 0x1

    const v11, 0x7f0b009a

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static/range {v33 .. v33}, Lcom/google/android/repolib/repositories/ArrayRepository;->arrayRepository(Ljava/util/Collection;)Lcom/google/android/repolib/repositories/ArrayRepository;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/google/android/repolib/leanback/RepositoryListRow;->repositoryListRow(Ljava/lang/String;Lcom/google/android/repolib/repositories/Repository;)Lcom/google/android/repolib/leanback/RepositoryListRow;

    move-result-object v11

    aput-object v11, v5, v10

    move-object/from16 v0, v26

    invoke-static {v0, v5}, Lcom/google/android/repolib/leanback/ListRowRepository;->listRowRepository(Lcom/google/android/repolib/leanback/ObjectAdapterFactory;[Lcom/google/android/repolib/leanback/RepositoryListRow;)Lcom/google/android/repolib/leanback/ListRowRepository;

    move-result-object v5

    const/4 v10, 0x1

    new-array v10, v10, [Lcom/google/android/repolib/repositories/Repository;

    const/4 v11, 0x0

    const/4 v12, 0x1

    new-array v12, v12, [Lcom/google/android/repolib/leanback/RepositoryListRow;

    const/4 v13, 0x0

    const v14, 0x7f0b009b

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-static/range {v36 .. v36}, Lcom/google/android/repolib/repositories/ArrayRepository;->arrayRepository(Ljava/util/Collection;)Lcom/google/android/repolib/repositories/ArrayRepository;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/google/android/repolib/leanback/RepositoryListRow;->repositoryListRow(Ljava/lang/String;Lcom/google/android/repolib/repositories/Repository;)Lcom/google/android/repolib/leanback/RepositoryListRow;

    move-result-object v14

    aput-object v14, v12, v13

    move-object/from16 v0, v26

    invoke-static {v0, v12}, Lcom/google/android/repolib/leanback/ListRowRepository;->listRowRepository(Lcom/google/android/repolib/leanback/ObjectAdapterFactory;[Lcom/google/android/repolib/leanback/RepositoryListRow;)Lcom/google/android/repolib/leanback/ListRowRepository;

    move-result-object v12

    aput-object v12, v10, v11

    move-object/from16 v0, v37

    invoke-static {v5, v0, v10}, Lcom/google/android/repolib/repositories/ConcatenatingRepository;->concatenatingRepository(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/repositories/Repository;[Lcom/google/android/repolib/repositories/Repository;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v5

    new-instance v10, Lcom/google/android/videos/pano/activity/PanoHomeActivity$NonEmptyListRowPredicate;

    const/4 v11, 0x0

    invoke-direct {v10, v11}, Lcom/google/android/videos/pano/activity/PanoHomeActivity$NonEmptyListRowPredicate;-><init>(Lcom/google/android/videos/pano/activity/PanoHomeActivity$1;)V

    invoke-static {v5, v10}, Lcom/google/android/repolib/repositories/FilteringRepository;->filteringRepository(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/common/Predicate;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v5

    const/4 v10, 0x1

    new-array v10, v10, [Lcom/google/android/repolib/leanback/RepositoryListRow;

    const/4 v11, 0x0

    const v12, 0x7f0b00cd

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    new-instance v13, Lcom/google/android/videos/pano/model/LibraryItem;

    const v14, 0x7f0200ea

    const v15, 0x7f0b00cd

    new-instance v17, Landroid/content/Intent;

    const-string v18, "android.settings.WIFI_SETTINGS"

    invoke-direct/range {v17 .. v18}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-direct {v13, v14, v15, v0}, Lcom/google/android/videos/pano/model/LibraryItem;-><init>(IILandroid/content/Intent;)V

    invoke-static {v13}, Lcom/google/android/repolib/repositories/ItemRepository;->itemRepository(Ljava/lang/Object;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/google/android/repolib/leanback/RepositoryListRow;->repositoryListRow(Ljava/lang/String;Lcom/google/android/repolib/repositories/Repository;)Lcom/google/android/repolib/leanback/RepositoryListRow;

    move-result-object v12

    aput-object v12, v10, v11

    move-object/from16 v0, v26

    invoke-static {v0, v10}, Lcom/google/android/repolib/leanback/ListRowRepository;->listRowRepository(Lcom/google/android/repolib/leanback/ObjectAdapterFactory;[Lcom/google/android/repolib/leanback/RepositoryListRow;)Lcom/google/android/repolib/leanback/ListRowRepository;

    move-result-object v10

    new-instance v11, Lcom/google/android/videos/utils/NetworkCondition;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-direct {v11, v12}, Lcom/google/android/videos/utils/NetworkCondition;-><init>(Lcom/google/android/videos/utils/NetworkStatus;)V

    invoke-static {v5, v10, v11}, Lcom/google/android/repolib/repositories/ConditionalRepository;->conditionalRepository(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/common/Condition;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->repository:Lcom/google/android/repolib/repositories/Repository;

    .line 253
    new-instance v5, Lcom/google/android/videos/pano/ui/ListRowObjectAdapterFactory;

    invoke-direct {v5}, Lcom/google/android/videos/pano/ui/ListRowObjectAdapterFactory;-><init>()V

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->repository:Lcom/google/android/repolib/repositories/Repository;

    invoke-virtual {v5, v10}, Lcom/google/android/videos/pano/ui/ListRowObjectAdapterFactory;->createFrom(Lcom/google/android/repolib/repositories/Repository;)Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->objectAdapter:Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;

    .line 254
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->objectAdapter:Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;

    const/4 v10, 0x1

    invoke-virtual {v5, v10}, Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;->setHasStableIds(Z)V

    .line 256
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    const-string v10, "browse_fragment"

    invoke-virtual {v5, v10}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v29

    check-cast v29, Landroid/support/v17/leanback/app/BrowseSupportFragment;

    .line 258
    .local v29, "browseFragment":Landroid/support/v17/leanback/app/BrowseSupportFragment;
    if-nez v29, :cond_1

    .line 259
    new-instance v29, Landroid/support/v17/leanback/app/BrowseSupportFragment;

    .end local v29    # "browseFragment":Landroid/support/v17/leanback/app/BrowseSupportFragment;
    invoke-direct/range {v29 .. v29}, Landroid/support/v17/leanback/app/BrowseSupportFragment;-><init>()V

    .line 260
    .restart local v29    # "browseFragment":Landroid/support/v17/leanback/app/BrowseSupportFragment;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v5

    const v10, 0x1020002

    const-string v11, "browse_fragment"

    move-object/from16 v0, v29

    invoke-virtual {v5, v10, v0, v11}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 265
    :cond_1
    new-instance v5, Lcom/google/android/videos/pano/ui/BackgroundHelper;

    invoke-virtual/range {v42 .. v42}, Lcom/google/android/videos/VideosGlobals;->getBitmapRequesters()Lcom/google/android/videos/bitmap/BitmapRequesters;

    move-result-object v10

    invoke-interface {v10}, Lcom/google/android/videos/bitmap/BitmapRequesters;->getControllableBitmapRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v10

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v10}, Lcom/google/android/videos/pano/ui/BackgroundHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/async/Requester;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->backgroundHelper:Lcom/google/android/videos/pano/ui/BackgroundHelper;

    .line 268
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v35

    .line 269
    .local v35, "resources":Landroid/content/res/Resources;
    const v5, 0x7f0a0079

    move-object/from16 v0, v35

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->setBrandColor(I)V

    .line 270
    const v5, 0x7f0a00ed

    move-object/from16 v0, v35

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->setSearchAffordanceColor(I)V

    .line 272
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->objectAdapter:Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->setAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 273
    move-object/from16 v0, v29

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->setOnSearchClickedListener(Landroid/view/View$OnClickListener;)V

    .line 274
    move-object/from16 v0, v29

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->setOnItemViewSelectedListener(Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V

    .line 275
    const v5, 0x7f02006f

    move-object/from16 v0, v35

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->setBadgeDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 276
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->repository:Lcom/google/android/repolib/repositories/Repository;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->objectAdapter:Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;

    invoke-interface {v5, v10}, Lcom/google/android/repolib/repositories/Repository;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 277
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Lcom/google/android/videos/ui/SyncHelper;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 278
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    move-object/from16 v0, p0

    invoke-interface {v5, v0}, Lcom/google/android/videos/utils/NetworkStatus;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 279
    return-void

    .line 216
    .end local v29    # "browseFragment":Landroid/support/v17/leanback/app/BrowseSupportFragment;
    .end local v35    # "resources":Landroid/content/res/Resources;
    .end local v37    # "storeRows":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<Landroid/support/v17/leanback/widget/ListRow;>;"
    :cond_2
    invoke-static {}, Lcom/google/android/repolib/repositories/EmptyRepository;->emptyRepository()Lcom/google/android/repolib/repositories/Repository;

    move-result-object v37

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 303
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0, p0}, Lcom/google/android/videos/utils/NetworkStatus;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 304
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/SyncHelper;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 305
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->repository:Lcom/google/android/repolib/repositories/Repository;

    iget-object v1, p0, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->objectAdapter:Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;

    invoke-interface {v0, v1}, Lcom/google/android/repolib/repositories/Repository;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 306
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->backgroundHelper:Lcom/google/android/videos/pano/ui/BackgroundHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/pano/ui/BackgroundHelper;->release()V

    .line 307
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onDestroy()V

    .line 308
    return-void
.end method

.method public onItemSelected(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Landroid/support/v17/leanback/widget/Row;)V
    .locals 3
    .param p1, "itemViewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .param p2, "object"    # Ljava/lang/Object;
    .param p3, "rowViewHolder"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .param p4, "row"    # Landroid/support/v17/leanback/widget/Row;

    .prologue
    .line 321
    iget-object v1, p0, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->objectAdapter:Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;

    invoke-virtual {v1}, Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->objectAdapter:Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    instance-of v1, p2, Lcom/google/android/videos/pano/model/Item;

    if-eqz v1, :cond_0

    move-object v0, p2

    .line 322
    check-cast v0, Lcom/google/android/videos/pano/model/Item;

    .line 323
    .local v0, "item":Lcom/google/android/videos/pano/model/Item;
    iget-object v1, p0, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->backgroundHelper:Lcom/google/android/videos/pano/ui/BackgroundHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/pano/model/Item;->getBackgroundUri()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/videos/pano/ui/BackgroundHelper;->setBackgroundUri(Ljava/lang/String;)V

    .line 327
    .end local v0    # "item":Lcom/google/android/videos/pano/model/Item;
    :goto_0
    return-void

    .line 325
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->backgroundHelper:Lcom/google/android/videos/pano/ui/BackgroundHelper;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/videos/pano/ui/BackgroundHelper;->setBackgroundUri(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 331
    const/16 v0, 0x54

    if-ne p1, v0, :cond_0

    .line 332
    invoke-static {p0}, Lcom/google/android/videos/utils/PlayStoreUtil;->startPanoSearch(Landroid/app/Activity;)V

    .line 333
    const/4 v0, 0x1

    .line 335
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 290
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 291
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/SyncHelper;->clearPendingResults()V

    .line 292
    invoke-virtual {p0, p1}, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->setIntent(Landroid/content/Intent;)V

    .line 293
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 283
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    .line 284
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->refreshContentRestrictions()Z

    .line 285
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/SyncHelper;->init(Ljava/lang/String;)V

    .line 286
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/SyncHelper;->reset()V

    .line 298
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStop()V

    .line 299
    return-void
.end method

.method public update()V
    .locals 2

    .prologue
    .line 345
    iget-object v1, p0, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/ui/SyncHelper;->getState()I

    move-result v0

    .line 346
    .local v0, "syncState":I
    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 347
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->finish()V

    .line 349
    :cond_0
    return-void
.end method
