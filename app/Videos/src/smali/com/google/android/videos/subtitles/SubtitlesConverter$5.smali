.class final Lcom/google/android/videos/subtitles/SubtitlesConverter$5;
.super Lcom/google/android/videos/utils/XmlParser$Rule;
.source "SubtitlesConverter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/subtitles/SubtitlesConverter;->addFormat2Rules(Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 140
    invoke-direct {p0}, Lcom/google/android/videos/utils/XmlParser$Rule;-><init>()V

    return-void
.end method


# virtual methods
.method public end(Lcom/google/android/videos/utils/Stack;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 11
    .param p2, "attrs"    # Lorg/xml/sax/Attributes;
    .param p3, "chars"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/utils/Stack",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Lorg/xml/sax/Attributes;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "stack":Lcom/google/android/videos/utils/Stack;, "Lcom/google/android/videos/utils/Stack<Ljava/lang/Object;>;"
    const/4 v10, 0x2

    const/4 v5, 0x1

    const/4 v8, 0x0

    .line 143
    const-class v4, Lcom/google/android/videos/subtitles/Subtitles$Builder;

    invoke-virtual {p1, v4}, Lcom/google/android/videos/utils/Stack;->peek(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/subtitles/Subtitles$Builder;

    .line 144
    .local v0, "builder":Lcom/google/android/videos/subtitles/Subtitles$Builder;
    new-array v4, v10, [Ljava/lang/String;

    const-string v9, "w"

    aput-object v9, v4, v8

    const-string v9, "win"

    aput-object v9, v4, v5

    # invokes: Lcom/google/android/videos/subtitles/SubtitlesConverter;->getValue(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p2, v4}, Lcom/google/android/videos/subtitles/SubtitlesConverter;->access$000(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v8}, Lcom/google/android/videos/utils/Util;->parseInt(Ljava/lang/String;I)I

    move-result v1

    .line 145
    .local v1, "windowId":I
    new-array v4, v10, [Ljava/lang/String;

    const-string v9, "t"

    aput-object v9, v4, v8

    const-string v9, "start"

    aput-object v9, v4, v5

    # invokes: Lcom/google/android/videos/subtitles/SubtitlesConverter;->getValue(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p2, v4}, Lcom/google/android/videos/subtitles/SubtitlesConverter;->access$000(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 146
    .local v3, "startTimeMillis":I
    new-array v4, v10, [Ljava/lang/String;

    const-string v9, "d"

    aput-object v9, v4, v8

    const-string v9, "dur"

    aput-object v9, v4, v5

    # invokes: Lcom/google/android/videos/subtitles/SubtitlesConverter;->getValue(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p2, v4}, Lcom/google/android/videos/subtitles/SubtitlesConverter;->access$000(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    # getter for: Lcom/google/android/videos/subtitles/SubtitlesConverter;->DEFAULT_DURATION_MILLIS:I
    invoke-static {}, Lcom/google/android/videos/subtitles/SubtitlesConverter;->access$300()I

    move-result v9

    invoke-static {v4, v9}, Lcom/google/android/videos/utils/Util;->parseInt(Ljava/lang/String;I)I

    move-result v7

    .line 148
    .local v7, "durationMillis":I
    const-string v4, "\n"

    const-string v9, "<br/>"

    invoke-virtual {p3, v4, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 149
    .local v2, "text":Ljava/lang/String;
    new-array v4, v5, [Ljava/lang/String;

    const-string v9, "append"

    aput-object v9, v4, v8

    # invokes: Lcom/google/android/videos/subtitles/SubtitlesConverter;->getValue(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p2, v4}, Lcom/google/android/videos/subtitles/SubtitlesConverter;->access$000(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 151
    .local v6, "append":Ljava/lang/String;
    add-int v4, v3, v7

    if-eqz v6, :cond_0

    :goto_0
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/videos/subtitles/Subtitles$Builder;->addLineToWindow(ILjava/lang/String;IIZ)Lcom/google/android/videos/subtitles/Subtitles$Builder;

    .line 153
    return-void

    :cond_0
    move v5, v8

    .line 151
    goto :goto_0
.end method
