.class public Lcom/google/android/videos/accounts/SignInManager;
.super Ljava/lang/Object;
.source "SignInManager.java"

# interfaces
.implements Lcom/google/android/repolib/observers/Observable;


# instance fields
.field private final accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

.field private final preferences:Landroid/content/SharedPreferences;

.field private final updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/accounts/AccountManagerWrapper;Landroid/content/SharedPreferences;)V
    .locals 1
    .param p1, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .param p2, "preferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/accounts/AccountManagerWrapper;

    iput-object v0, p0, Lcom/google/android/videos/accounts/SignInManager;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    .line 31
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/videos/accounts/SignInManager;->preferences:Landroid/content/SharedPreferences;

    .line 32
    invoke-static {}, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->asyncUpdateDispatcher()Lcom/google/android/repolib/observers/UpdateDispatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/accounts/SignInManager;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    .line 33
    return-void
.end method


# virtual methods
.method public addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/videos/accounts/SignInManager;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 38
    return-void
.end method

.method public chooseFirstAccount()Ljava/lang/String;
    .locals 3

    .prologue
    .line 64
    iget-object v2, p0, Lcom/google/android/videos/accounts/SignInManager;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v2}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getAccounts()[Landroid/accounts/Account;

    move-result-object v0

    .line 65
    .local v0, "accounts":[Landroid/accounts/Account;
    array-length v2, v0

    if-lez v2, :cond_0

    const/4 v2, 0x0

    aget-object v2, v0, v2

    iget-object v1, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 66
    .local v1, "firstAccount":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0, v1}, Lcom/google/android/videos/accounts/SignInManager;->setSignedInAccount(Ljava/lang/String;)V

    .line 67
    return-object v1

    .line 65
    .end local v1    # "firstAccount":Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public clearSignedInAccount()V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/accounts/SignInManager;->setSignedInAccount(Ljava/lang/String;)V

    .line 51
    return-void
.end method

.method public clearSignedInAccountIfRemoved()V
    .locals 0

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/google/android/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    .line 47
    return-void
.end method

.method public getSignedInAccount()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 55
    iget-object v2, p0, Lcom/google/android/videos/accounts/SignInManager;->preferences:Landroid/content/SharedPreferences;

    const-string v3, "user_account"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 56
    .local v0, "account":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/videos/accounts/SignInManager;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v2, v0}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->accountExists(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 60
    .end local v0    # "account":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 59
    .restart local v0    # "account":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/accounts/SignInManager;->clearSignedInAccount()V

    move-object v0, v1

    .line 60
    goto :goto_0
.end method

.method public removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/videos/accounts/SignInManager;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 43
    return-void
.end method

.method public setSignedInAccount(Ljava/lang/String;)V
    .locals 3
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/videos/accounts/SignInManager;->preferences:Landroid/content/SharedPreferences;

    const-string v1, "user_account"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/google/android/videos/accounts/SignInManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "user_account"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 73
    iget-object v0, p0, Lcom/google/android/videos/accounts/SignInManager;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0}, Lcom/google/android/repolib/observers/UpdateDispatcher;->update()V

    .line 75
    :cond_0
    return-void
.end method
