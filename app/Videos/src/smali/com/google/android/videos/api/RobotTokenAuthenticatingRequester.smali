.class final Lcom/google/android/videos/api/RobotTokenAuthenticatingRequester;
.super Lcom/google/android/videos/async/AuthenticatingRequester;
.source "RobotTokenAuthenticatingRequester.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/async/AuthenticatingRequester",
        "<",
        "Lcom/google/android/videos/api/RobotTokenRequest;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final target:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final uri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/async/Requester;Landroid/net/Uri;)V
    .locals 1
    .param p1, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .param p3, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/accounts/AccountManagerWrapper;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "Ljava/lang/String;",
            ">;",
            "Landroid/net/Uri;",
            ")V"
        }
    .end annotation

    .prologue
    .line 25
    .local p2, "target":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lcom/google/android/videos/async/AuthenticatingRequester;-><init>(Lcom/google/android/videos/accounts/AccountManagerWrapper;)V

    .line 26
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/api/RobotTokenAuthenticatingRequester;->target:Lcom/google/android/videos/async/Requester;

    .line 27
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/videos/api/RobotTokenAuthenticatingRequester;->uri:Landroid/net/Uri;

    .line 28
    return-void
.end method


# virtual methods
.method protected makeAuthenticatedRequest(Lcom/google/android/videos/api/RobotTokenRequest;Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V
    .locals 6
    .param p1, "request"    # Lcom/google/android/videos/api/RobotTokenRequest;
    .param p2, "authToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/api/RobotTokenRequest;",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    .local p3, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 34
    .local v0, "content":Ljava/lang/StringBuilder;
    const-string v3, "videoid="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    iget-object v3, p1, Lcom/google/android/videos/api/RobotTokenRequest;->videoId:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    const-string v3, "&oauth="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    invoke-static {p2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    const-string v3, "&keyrequests="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    iget-object v3, p1, Lcom/google/android/videos/api/RobotTokenRequest;->keyRequests:[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_0
    iget-object v3, p1, Lcom/google/android/videos/api/RobotTokenRequest;->keyRequests:[Ljava/lang/String;

    array-length v3, v3

    if-ge v2, v3, :cond_0

    .line 41
    const-string v3, "|"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    iget-object v3, p1, Lcom/google/android/videos/api/RobotTokenRequest;->keyRequests:[Ljava/lang/String;

    aget-object v3, v3, v2

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 44
    :cond_0
    iget-boolean v3, p1, Lcom/google/android/videos/api/RobotTokenRequest;->pinned:Z

    if-eqz v3, :cond_1

    .line 45
    const-string v3, "&pinned=true"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    :cond_1
    iget-object v3, p0, Lcom/google/android/videos/api/RobotTokenAuthenticatingRequester;->uri:Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/videos/async/HttpUriRequestFactory;->createPost(Landroid/net/Uri;[B)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v1

    .line 49
    .local v1, "httpUriRequest":Lorg/apache/http/client/methods/HttpUriRequest;
    const-string v3, "Authorization"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Bearer "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    iget-object v3, p0, Lcom/google/android/videos/api/RobotTokenAuthenticatingRequester;->target:Lcom/google/android/videos/async/Requester;

    invoke-interface {v3, v1, p3}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 51
    return-void
.end method

.method protected bridge synthetic makeAuthenticatedRequest(Lcom/google/android/videos/async/Request;Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/async/Request;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 17
    check-cast p1, Lcom/google/android/videos/api/RobotTokenRequest;

    .end local p1    # "x0":Lcom/google/android/videos/async/Request;
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/videos/api/RobotTokenAuthenticatingRequester;->makeAuthenticatedRequest(Lcom/google/android/videos/api/RobotTokenRequest;Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V

    return-void
.end method
