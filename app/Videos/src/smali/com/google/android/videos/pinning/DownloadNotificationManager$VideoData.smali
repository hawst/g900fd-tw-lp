.class Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
.super Ljava/lang/Object;
.source "DownloadNotificationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pinning/DownloadNotificationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "VideoData"
.end annotation


# instance fields
.field private final account:Ljava/lang/String;

.field private final bytesDownloaded:J

.field private final downloadSize:J

.field private final drmErrorCode:Ljava/lang/Integer;

.field private final isRental:Z

.field private final pinningStatus:I

.field private final pinningStatusReason:I

.field private final poster:Landroid/graphics/Bitmap;

.field private final seasonId:Ljava/lang/String;

.field private final showId:Ljava/lang/String;

.field private final showTitle:Ljava/lang/String;

.field private final title:Ljava/lang/String;

.field private final videoId:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;JJIILjava/lang/Integer;Z)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "seasonId"    # Ljava/lang/String;
    .param p4, "showId"    # Ljava/lang/String;
    .param p5, "title"    # Ljava/lang/String;
    .param p6, "showTitle"    # Ljava/lang/String;
    .param p7, "poster"    # Landroid/graphics/Bitmap;
    .param p8, "bytesDownloaded"    # J
    .param p10, "downloadSize"    # J
    .param p12, "pinningStatus"    # I
    .param p13, "pinningStatusReason"    # I
    .param p14, "drmErrorCode"    # Ljava/lang/Integer;
    .param p15, "isRental"    # Z

    .prologue
    .line 728
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 729
    iput-object p1, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->account:Ljava/lang/String;

    .line 730
    iput-object p2, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->videoId:Ljava/lang/String;

    .line 731
    iput-object p3, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->seasonId:Ljava/lang/String;

    .line 732
    iput-object p4, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->showId:Ljava/lang/String;

    .line 733
    iput-object p5, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->title:Ljava/lang/String;

    .line 734
    iput-object p6, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->showTitle:Ljava/lang/String;

    .line 735
    iput-object p7, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->poster:Landroid/graphics/Bitmap;

    .line 736
    iput-wide p8, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->bytesDownloaded:J

    .line 737
    iput-wide p10, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->downloadSize:J

    .line 738
    iput p12, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->pinningStatus:I

    .line 739
    iput p13, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->pinningStatusReason:I

    .line 740
    iput-object p14, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->drmErrorCode:Ljava/lang/Integer;

    .line 741
    iput-boolean p15, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->isRental:Z

    .line 742
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;JJIILjava/lang/Integer;ZLcom/google/android/videos/pinning/DownloadNotificationManager$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Ljava/lang/String;
    .param p4, "x3"    # Ljava/lang/String;
    .param p5, "x4"    # Ljava/lang/String;
    .param p6, "x5"    # Ljava/lang/String;
    .param p7, "x6"    # Landroid/graphics/Bitmap;
    .param p8, "x7"    # J
    .param p10, "x8"    # J
    .param p12, "x9"    # I
    .param p13, "x10"    # I
    .param p14, "x11"    # Ljava/lang/Integer;
    .param p15, "x12"    # Z
    .param p16, "x13"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$1;

    .prologue
    .line 711
    invoke-direct/range {p0 .. p15}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;JJIILjava/lang/Integer;Z)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;

    .prologue
    .line 711
    iget-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->account:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;

    .prologue
    .line 711
    iget-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->seasonId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/Integer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;

    .prologue
    .line 711
    iget-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->drmErrorCode:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;

    .prologue
    .line 711
    iget-boolean v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->isRental:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;

    .prologue
    .line 711
    iget-wide v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->bytesDownloaded:J

    return-wide v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;

    .prologue
    .line 711
    iget-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->videoId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;

    .prologue
    .line 711
    iget v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->pinningStatus:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;

    .prologue
    .line 711
    iget-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->showId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;

    .prologue
    .line 711
    iget-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->title:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;

    .prologue
    .line 711
    iget-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->showTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;

    .prologue
    .line 711
    iget-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->poster:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;

    .prologue
    .line 711
    iget v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->pinningStatusReason:I

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;

    .prologue
    .line 711
    iget-wide v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->downloadSize:J

    return-wide v0
.end method
