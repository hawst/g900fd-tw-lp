.class public Lcom/google/android/videos/activity/ShowActivity;
.super Lcom/google/android/videos/activity/AssetDetailsActivity;
.source "ShowActivity.java"

# interfaces
.implements Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;
.implements Lcom/google/android/videos/ui/RemoveItemDialogFragment$OnRemovedListener;
.implements Lcom/google/android/videos/ui/StatusHelper$OnRetryListener;
.implements Lcom/google/android/videos/ui/VerticalsHelper$OnContentChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/activity/ShowActivity$EpisodeSynopsisDialogFragment;,
        Lcom/google/android/videos/activity/ShowActivity$Query;,
        Lcom/google/android/videos/activity/ShowActivity$SharedElements;
    }
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private bannerTextHelper:Lcom/google/android/videos/ui/BannerTextHelper;

.field private currentPurchaseRequest:Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

.field private downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

.field enterBackgroundSplash:Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;

.field enterBackgroundTinter:Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;

.field enterLogoCrop:Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;

.field enterPosterHeroTinter:Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;

.field private episodeId:Ljava/lang/String;

.field private episodeIdForHero:Ljava/lang/String;

.field private episodesCursorHelper:Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;

.field exitBackgroundSplash:Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;

.field exitBackgroundTinter:Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;

.field exitLogoCrop:Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;

.field exitPosterHeroTinter:Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;

.field haveHeaderColor:Z

.field private haveSeasons:Z

.field headerViewCreated:Z

.field private pendingPurchaseRequest:Z

.field posterHeroView:Landroid/widget/ImageView;

.field private posterStoreShowBannerRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private purchaseCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private purchaseSyncCallback:Lcom/google/android/videos/async/NewCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/NewCallback",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private rootUiElementNode:Lcom/google/android/videos/logging/RootUiElementNode;

.field private seasonId:Ljava/lang/String;

.field private showHelper:Lcom/google/android/videos/ui/ShowHelper;

.field private showId:Ljava/lang/String;

.field private showTitle:Ljava/lang/String;

.field private started:Z

.field private statusHelper:Lcom/google/android/videos/ui/StatusHelper;

.field private suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

.field private syncCompleted:Z

.field private syncException:Ljava/lang/Exception;

.field private volatile syncTaskControl:Lcom/google/android/videos/async/TaskControl;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 102
    invoke-direct {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;-><init>()V

    .line 968
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/activity/ShowActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/activity/ShowActivity;

    .prologue
    .line 102
    invoke-direct {p0}, Lcom/google/android/videos/activity/ShowActivity;->onSyncSuccess()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/videos/activity/ShowActivity;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/activity/ShowActivity;
    .param p1, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 102
    invoke-direct {p0, p1}, Lcom/google/android/videos/activity/ShowActivity;->onSyncError(Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/videos/activity/ShowActivity;)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/activity/ShowActivity;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->currentPurchaseRequest:Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/videos/activity/ShowActivity;Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/activity/ShowActivity;
    .param p1, "x1"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/google/android/videos/activity/ShowActivity;->currentPurchaseRequest:Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/videos/activity/ShowActivity;Landroid/database/Cursor;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/activity/ShowActivity;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 102
    invoke-direct {p0, p1}, Lcom/google/android/videos/activity/ShowActivity;->onPurchaseCursor(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/videos/activity/ShowActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/activity/ShowActivity;

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/google/android/videos/activity/ShowActivity;->pendingPurchaseRequest:Z

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/videos/activity/ShowActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/activity/ShowActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 102
    iput-boolean p1, p0, Lcom/google/android/videos/activity/ShowActivity;->pendingPurchaseRequest:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/videos/activity/ShowActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/activity/ShowActivity;

    .prologue
    .line 102
    invoke-direct {p0}, Lcom/google/android/videos/activity/ShowActivity;->refreshPurchase()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/videos/activity/ShowActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/activity/ShowActivity;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->showId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/videos/activity/ShowActivity;)Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/activity/ShowActivity;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->episodesCursorHelper:Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;

    return-object v0
.end method

.method public static createAnimationBundleV21(Landroid/app/Activity;Landroid/content/Intent;Lcom/google/android/videos/activity/ShowActivity$SharedElements;)Landroid/os/Bundle;
    .locals 5
    .param p0, "fromActivity"    # Landroid/app/Activity;
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "from"    # Lcom/google/android/videos/activity/ShowActivity$SharedElements;

    .prologue
    const/4 v4, 0x1

    .line 148
    const-string v3, "run_animation"

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 150
    const-string v3, "show_id"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 152
    .local v0, "assetId":Ljava/lang/String;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 154
    .local v2, "transitionPairs":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Landroid/view/View;Ljava/lang/String;>;>;"
    iget-object v3, p2, Lcom/google/android/videos/activity/ShowActivity$SharedElements;->poster:Landroid/view/View;

    if-eqz v3, :cond_0

    .line 155
    const-string v3, "have_poster"

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 156
    const v3, 0x7f0b0077

    invoke-static {p0, v3, v0}, Lcom/google/android/videos/ui/TransitionUtil;->encodeTransitionName(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 158
    .local v1, "transitionName":Ljava/lang/String;
    iget-object v3, p2, Lcom/google/android/videos/activity/ShowActivity$SharedElements;->poster:Landroid/view/View;

    invoke-static {v3, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 161
    .end local v1    # "transitionName":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/Window;->setSharedElementsUseOverlay(Z)V

    .line 163
    new-instance v3, Lcom/google/android/videos/activity/ShowActivity$1;

    invoke-direct {v3}, Lcom/google/android/videos/activity/ShowActivity$1;-><init>()V

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setExitSharedElementCallback(Landroid/app/SharedElementCallback;)V

    .line 178
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Landroid/util/Pair;

    invoke-interface {v2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Landroid/util/Pair;

    invoke-static {p0, v3}, Landroid/app/ActivityOptions;->makeSceneTransitionAnimation(Landroid/app/Activity;[Landroid/util/Pair;)Landroid/app/ActivityOptions;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v3

    return-object v3
.end method

.method public static createEpisodeIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;
    .param p3, "seasonId"    # Ljava/lang/String;
    .param p4, "episodeId"    # Ljava/lang/String;
    .param p5, "startDownload"    # Z
    .param p6, "pinningErrorDialog"    # Z

    .prologue
    .line 125
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/videos/activity/ShowActivity;->createSeasonIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "video_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "start_download"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "pinning_error_dialog"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static createSeasonIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;
    .param p3, "seasonId"    # Ljava/lang/String;

    .prologue
    .line 115
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 116
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 117
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/videos/activity/ShowActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "season_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "show_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "authAccount"

    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static createShowIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;

    .prologue
    .line 107
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 108
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/videos/activity/ShowActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "show_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "authAccount"

    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private getHeroInnerRadius()I
    .locals 1

    .prologue
    .line 504
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ShowActivity;->getScreenshotHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x5

    return v0
.end method

.method private initEnterSharedElementCallback()V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 410
    invoke-direct {p0}, Lcom/google/android/videos/activity/ShowActivity;->initPosterHeroView()V

    .line 411
    new-instance v6, Lcom/google/android/videos/activity/ShowActivity$4;

    invoke-direct {v6, p0}, Lcom/google/android/videos/activity/ShowActivity$4;-><init>(Lcom/google/android/videos/activity/ShowActivity;)V

    invoke-virtual {p0, v6}, Lcom/google/android/videos/activity/ShowActivity;->setEnterSharedElementCallback(Landroid/app/SharedElementCallback;)V

    .line 478
    iget-boolean v6, p0, Lcom/google/android/videos/activity/ShowActivity;->pendingEnterTransition:Z

    if-nez v6, :cond_1

    .line 481
    iget-object v6, p0, Lcom/google/android/videos/activity/ShowActivity;->episodeIdForHero:Ljava/lang/String;

    if-eqz v6, :cond_2

    move v1, v4

    .line 482
    .local v1, "heroIsScreenshot":Z
    :goto_0
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/videos/VideosGlobals;->getPosterStore()Lcom/google/android/videos/store/PosterStore;

    move-result-object v3

    .line 483
    .local v3, "posterStore":Lcom/google/android/videos/store/PosterStore;
    if-eqz v1, :cond_0

    const/4 v4, 0x2

    :cond_0
    invoke-virtual {v3, v4}, Lcom/google/android/videos/store/PosterStore;->getRequester(I)Lcom/google/android/videos/async/Requester;

    move-result-object v2

    .line 485
    .local v2, "posterRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    iget-object v4, p0, Lcom/google/android/videos/activity/ShowActivity;->posterHeroView:Landroid/widget/ImageView;

    invoke-static {v4, v5}, Lcom/google/android/videos/ui/BitmapLoader;->createDefaultBitmapView(Landroid/widget/ImageView;I)Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;

    move-result-object v0

    .line 486
    .local v0, "bitmapView":Lcom/google/android/videos/ui/BitmapLoader$BitmapView;
    if-eqz v1, :cond_3

    iget-object v4, p0, Lcom/google/android/videos/activity/ShowActivity;->episodeIdForHero:Ljava/lang/String;

    :goto_1
    invoke-static {v0, v2, v4}, Lcom/google/android/videos/ui/BitmapLoader;->setBitmapAsync(Lcom/google/android/videos/ui/BitmapLoader$BitmapView;Lcom/google/android/videos/async/Requester;Ljava/lang/Object;)V

    .line 489
    .end local v0    # "bitmapView":Lcom/google/android/videos/ui/BitmapLoader$BitmapView;
    .end local v1    # "heroIsScreenshot":Z
    .end local v2    # "posterRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    .end local v3    # "posterStore":Lcom/google/android/videos/store/PosterStore;
    :cond_1
    return-void

    :cond_2
    move v1, v5

    .line 481
    goto :goto_0

    .line 486
    .restart local v0    # "bitmapView":Lcom/google/android/videos/ui/BitmapLoader$BitmapView;
    .restart local v1    # "heroIsScreenshot":Z
    .restart local v2    # "posterRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    .restart local v3    # "posterStore":Lcom/google/android/videos/store/PosterStore;
    :cond_3
    iget-object v4, p0, Lcom/google/android/videos/activity/ShowActivity;->showId:Ljava/lang/String;

    goto :goto_1
.end method

.method private initPosterHeroView()V
    .locals 6

    .prologue
    .line 381
    iget-object v1, p0, Lcom/google/android/videos/activity/ShowActivity;->posterHeroView:Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    .line 403
    :cond_0
    :goto_0
    return-void

    .line 385
    :cond_1
    const v1, 0x7f0f00c0

    invoke-virtual {p0, v1}, Lcom/google/android/videos/activity/ShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/google/android/videos/activity/ShowActivity;->posterHeroView:Landroid/widget/ImageView;

    .line 386
    iget-object v1, p0, Lcom/google/android/videos/activity/ShowActivity;->posterHeroView:Landroid/widget/ImageView;

    const v2, 0x7f0b0077

    iget-object v3, p0, Lcom/google/android/videos/activity/ShowActivity;->showId:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/google/android/videos/ui/TransitionUtil;->encodeAndSetTransitionName(Landroid/view/View;ILjava/lang/String;)V

    .line 387
    iget-object v1, p0, Lcom/google/android/videos/activity/ShowActivity;->posterHeroView:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 389
    iget-boolean v1, p0, Lcom/google/android/videos/activity/ShowActivity;->pendingEnterTransition:Z

    if-nez v1, :cond_0

    .line 394
    iget-object v1, p0, Lcom/google/android/videos/activity/ShowActivity;->posterHeroView:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 400
    iget-object v1, p0, Lcom/google/android/videos/activity/ShowActivity;->posterHeroView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 401
    .local v0, "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ShowActivity;->getScreenshotHeight()I

    move-result v1

    int-to-double v2, v1

    const-wide v4, 0x3ff3333333333333L    # 1.2

    div-double/2addr v2, v4

    double-to-int v1, v2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto :goto_0
.end method

.method private onPurchaseCursor(Landroid/database/Cursor;)V
    .locals 4
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 905
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 906
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/activity/ShowActivity;->showTitle:Ljava/lang/String;

    .line 907
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ShowActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-static {p1, v3}, Lcom/google/android/videos/utils/DbUtils;->getStringList(Landroid/database/Cursor;I)Ljava/util/List;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/videos/utils/Util;->buildListString(Landroid/content/res/Resources;ZLjava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 909
    .local v0, "broadcasters":Ljava/lang/String;
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/videos/activity/ShowActivity;->haveSeasons:Z

    .line 910
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ShowActivity;->supportInvalidateOptionsMenu()V

    .line 911
    invoke-direct {p0}, Lcom/google/android/videos/activity/ShowActivity;->updateVisibilities()V

    .line 916
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/activity/ShowActivity;->showTitle:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/android/videos/activity/ShowActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 917
    iget-object v1, p0, Lcom/google/android/videos/activity/ShowActivity;->showHelper:Lcom/google/android/videos/ui/ShowHelper;

    iget-object v2, p0, Lcom/google/android/videos/activity/ShowActivity;->showTitle:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/videos/ui/ShowHelper;->updateInfo(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 919
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 921
    return-void

    .line 913
    .end local v0    # "broadcasters":Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    :try_start_1
    iput-object v1, p0, Lcom/google/android/videos/activity/ShowActivity;->showTitle:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 914
    const/4 v0, 0x0

    .restart local v0    # "broadcasters":Ljava/lang/String;
    goto :goto_0

    .line 919
    .end local v0    # "broadcasters":Ljava/lang/String;
    :catchall_0
    move-exception v1

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private onSyncError(Ljava/lang/Exception;)V
    .locals 1
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 897
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/activity/ShowActivity;->syncCompleted:Z

    .line 898
    iput-object p1, p0, Lcom/google/android/videos/activity/ShowActivity;->syncException:Ljava/lang/Exception;

    .line 899
    invoke-direct {p0}, Lcom/google/android/videos/activity/ShowActivity;->updateVisibilities()V

    .line 900
    return-void
.end method

.method private onSyncSuccess()V
    .locals 1

    .prologue
    .line 892
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/activity/ShowActivity;->syncCompleted:Z

    .line 893
    invoke-direct {p0}, Lcom/google/android/videos/activity/ShowActivity;->updateVisibilities()V

    .line 894
    return-void
.end method

.method private refreshPurchase()V
    .locals 6

    .prologue
    .line 882
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->currentPurchaseRequest:Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    if-eqz v0, :cond_0

    .line 883
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/activity/ShowActivity;->pendingPurchaseRequest:Z

    .line 889
    :goto_0
    return-void

    .line 885
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->account:Ljava/lang/String;

    sget-object v1, Lcom/google/android/videos/activity/ShowActivity$Query;->COLUMNS:[Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/activity/ShowActivity;->showId:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v0, v1, v2, v4, v5}, Lcom/google/android/videos/store/PurchaseRequests;->createMyShowRequest(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->currentPurchaseRequest:Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .line 887
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    iget-object v1, p0, Lcom/google/android/videos/activity/ShowActivity;->currentPurchaseRequest:Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    iget-object v2, p0, Lcom/google/android/videos/activity/ShowActivity;->purchaseCallback:Lcom/google/android/videos/async/Callback;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/videos/async/Callback;)V

    goto :goto_0
.end method

.method private startEnterTransitionIfReady()V
    .locals 2

    .prologue
    .line 493
    iget-boolean v0, p0, Lcom/google/android/videos/activity/ShowActivity;->pendingEnterTransition:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/activity/ShowActivity;->haveHeaderColor:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/activity/ShowActivity;->headerViewCreated:Z

    if-eqz v0, :cond_0

    .line 494
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->contentLayout:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lcom/google/android/videos/activity/ShowActivity$5;

    invoke-direct {v1, p0}, Lcom/google/android/videos/activity/ShowActivity$5;-><init>(Lcom/google/android/videos/activity/ShowActivity;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->post(Ljava/lang/Runnable;)Z

    .line 501
    :cond_0
    return-void
.end method

.method private startSync()V
    .locals 2

    .prologue
    .line 781
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->syncTaskControl:Lcom/google/android/videos/async/TaskControl;

    invoke-static {v0}, Lcom/google/android/videos/async/TaskControl;->cancel(Lcom/google/android/videos/async/TaskControl;)V

    .line 782
    new-instance v0, Lcom/google/android/videos/async/TaskControl;

    invoke-direct {v0}, Lcom/google/android/videos/async/TaskControl;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->syncTaskControl:Lcom/google/android/videos/async/TaskControl;

    .line 783
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/activity/ShowActivity;->syncCompleted:Z

    .line 784
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->syncException:Ljava/lang/Exception;

    .line 785
    invoke-direct {p0}, Lcom/google/android/videos/activity/ShowActivity;->updateVisibilities()V

    .line 786
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    iget-object v1, p0, Lcom/google/android/videos/activity/ShowActivity;->account:Ljava/lang/String;

    invoke-virtual {v0, v1, p0, p0}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getAuthToken(Ljava/lang/String;Landroid/app/Activity;Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;)V

    .line 787
    return-void
.end method

.method private updateVisibilities()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 924
    iget-boolean v0, p0, Lcom/google/android/videos/activity/ShowActivity;->haveSeasons:Z

    if-eqz v0, :cond_0

    .line 925
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->contentLayout:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 926
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->statusHelper:Lcom/google/android/videos/ui/StatusHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/StatusHelper;->hide()V

    .line 939
    :goto_0
    return-void

    .line 928
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->contentLayout:Landroid/support/v7/widget/RecyclerView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 929
    iget-boolean v0, p0, Lcom/google/android/videos/activity/ShowActivity;->syncCompleted:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->currentPurchaseRequest:Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    if-nez v0, :cond_2

    .line 930
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->syncException:Ljava/lang/Exception;

    if-eqz v0, :cond_1

    .line 931
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->statusHelper:Lcom/google/android/videos/ui/StatusHelper;

    iget-object v1, p0, Lcom/google/android/videos/activity/ShowActivity;->errorHelper:Lcom/google/android/videos/utils/ErrorHelper;

    iget-object v2, p0, Lcom/google/android/videos/activity/ShowActivity;->syncException:Ljava/lang/Exception;

    invoke-virtual {v1, v2}, Lcom/google/android/videos/utils/ErrorHelper;->humanize(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/google/android/videos/ui/StatusHelper;->setErrorMessage(Ljava/lang/CharSequence;Z)V

    goto :goto_0

    .line 933
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->statusHelper:Lcom/google/android/videos/ui/StatusHelper;

    const v1, 0x7f0b0146

    invoke-virtual {v0, v1, v3}, Lcom/google/android/videos/ui/StatusHelper;->setErrorMessage(IZ)V

    goto :goto_0

    .line 936
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->statusHelper:Lcom/google/android/videos/ui/StatusHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/StatusHelper;->setLoading()V

    goto :goto_0
.end method


# virtual methods
.method protected createNonSharedTransition(Z)Landroid/transition/TransitionSet;
    .locals 12
    .param p1, "isEnter"    # Z

    .prologue
    const/4 v2, 0x0

    const-wide/16 v10, 0x15e

    const-wide/16 v8, 0x0

    .line 582
    invoke-super {p0, p1}, Lcom/google/android/videos/activity/AssetDetailsActivity;->createNonSharedTransition(Z)Landroid/transition/TransitionSet;

    move-result-object v3

    .line 584
    .local v3, "set":Landroid/transition/TransitionSet;
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ShowActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "have_poster"

    invoke-virtual {v5, v6, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v2, 0x1f4

    .line 587
    .local v2, "enterDelayOffset":I
    :cond_0
    new-instance v0, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;

    invoke-direct {v0, p1}, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;-><init>(Z)V

    .line 588
    .local v0, "backgroundSplash":Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;
    iget-object v5, p0, Lcom/google/android/videos/activity/ShowActivity;->backgroundImageView:Lcom/google/android/videos/ui/FocusedImageView;

    invoke-virtual {v0, v5}, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 589
    const v5, 0x7f0b0076

    invoke-virtual {p0, v5}, Lcom/google/android/videos/activity/ShowActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->addTarget(Ljava/lang/String;)Landroid/transition/Transition;

    .line 590
    iget-object v5, p0, Lcom/google/android/videos/activity/ShowActivity;->backgroundImageView:Lcom/google/android/videos/ui/FocusedImageView;

    invoke-virtual {v0, v5}, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->setCenterOn(Landroid/view/View;)V

    .line 591
    if-eqz p1, :cond_3

    int-to-long v6, v2

    :goto_0
    invoke-virtual {v0, v6, v7}, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->setStartDelay(J)Landroid/transition/Transition;

    .line 592
    invoke-virtual {v0, v10, v11}, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->setDuration(J)Landroid/transition/Transition;

    .line 593
    invoke-direct {p0}, Lcom/google/android/videos/activity/ShowActivity;->getHeroInnerRadius()I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->setSmallerRadius(I)V

    .line 594
    invoke-virtual {v3, v0}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 596
    if-eqz p1, :cond_4

    .line 597
    iget-object v5, p0, Lcom/google/android/videos/activity/ShowActivity;->backgroundImageView:Lcom/google/android/videos/ui/FocusedImageView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/google/android/videos/ui/FocusedImageView;->setAlpha(F)V

    .line 611
    :goto_1
    new-instance v1, Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;

    invoke-direct {v1}, Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;-><init>()V

    .line 612
    .local v1, "backgroundTinter":Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;
    iget-object v5, p0, Lcom/google/android/videos/activity/ShowActivity;->backgroundImageView:Lcom/google/android/videos/ui/FocusedImageView;

    invoke-virtual {v1, v5}, Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 613
    if-eqz p1, :cond_5

    add-int/lit16 v5, v2, 0xc8

    int-to-long v6, v5

    :goto_2
    invoke-virtual {v1, v6, v7}, Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;->setStartDelay(J)Landroid/transition/Transition;

    .line 614
    invoke-virtual {v1, v10, v11}, Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;->setDuration(J)Landroid/transition/Transition;

    .line 615
    invoke-virtual {v3, v1}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 617
    if-eqz p1, :cond_6

    .line 618
    iput-object v1, p0, Lcom/google/android/videos/activity/ShowActivity;->enterBackgroundTinter:Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;

    .line 619
    iput-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->enterBackgroundSplash:Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;

    .line 625
    :goto_3
    if-nez p1, :cond_2

    .line 627
    new-instance v4, Landroid/transition/Slide;

    invoke-direct {v4}, Landroid/transition/Slide;-><init>()V

    .line 628
    .local v4, "slideCards":Landroid/transition/Transition;
    const v5, 0x7f0f00cc

    invoke-virtual {v4, v5}, Landroid/transition/Transition;->addTarget(I)Landroid/transition/Transition;

    .line 629
    if-eqz p1, :cond_1

    move-wide v8, v10

    :cond_1
    invoke-virtual {v4, v8, v9}, Landroid/transition/Transition;->setStartDelay(J)Landroid/transition/Transition;

    .line 630
    invoke-virtual {v3, v4}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 632
    new-instance v5, Lcom/google/android/videos/activity/ShowActivity$8;

    invoke-direct {v5, p0}, Lcom/google/android/videos/activity/ShowActivity$8;-><init>(Lcom/google/android/videos/activity/ShowActivity;)V

    invoke-virtual {v3, v5}, Landroid/transition/TransitionSet;->addListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/TransitionSet;

    .line 641
    .end local v4    # "slideCards":Landroid/transition/Transition;
    :cond_2
    return-object v3

    .end local v1    # "backgroundTinter":Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;
    :cond_3
    move-wide v6, v8

    .line 591
    goto :goto_0

    .line 599
    :cond_4
    new-instance v5, Lcom/google/android/videos/activity/ShowActivity$7;

    invoke-direct {v5, p0}, Lcom/google/android/videos/activity/ShowActivity$7;-><init>(Lcom/google/android/videos/activity/ShowActivity;)V

    invoke-virtual {v0, v5}, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->addListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/Transition;

    goto :goto_1

    .restart local v1    # "backgroundTinter":Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;
    :cond_5
    move-wide v6, v8

    .line 613
    goto :goto_2

    .line 621
    :cond_6
    iput-object v1, p0, Lcom/google/android/videos/activity/ShowActivity;->exitBackgroundTinter:Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;

    .line 622
    iput-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->exitBackgroundSplash:Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;

    goto :goto_3
.end method

.method protected createSharedTransition(Z)Landroid/transition/TransitionSet;
    .locals 14
    .param p1, "isEnter"    # Z

    .prologue
    const-wide/16 v12, 0x1f4

    const/4 v8, 0x0

    .line 510
    invoke-super {p0, p1}, Lcom/google/android/videos/activity/AssetDetailsActivity;->createSharedTransition(Z)Landroid/transition/TransitionSet;

    move-result-object v7

    .line 512
    .local v7, "set":Landroid/transition/TransitionSet;
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ShowActivity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    const-string v10, "have_poster"

    invoke-virtual {v9, v10, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    if-nez v9, :cond_0

    .line 576
    :goto_0
    return-object v7

    .line 516
    :cond_0
    invoke-static {p0}, Lcom/google/android/play/animation/PlayInterpolators;->fastOutSlowIn(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v0

    .line 517
    .local v0, "interpolator":Landroid/view/animation/Interpolator;
    new-instance v4, Landroid/transition/ArcMotion;

    invoke-direct {v4}, Landroid/transition/ArcMotion;-><init>()V

    .line 519
    .local v4, "motion":Landroid/transition/PathMotion;
    if-eqz p1, :cond_2

    move v5, v8

    .line 522
    .local v5, "moveStartDelay":I
    :goto_1
    invoke-direct {p0}, Lcom/google/android/videos/activity/ShowActivity;->initPosterHeroView()V

    .line 524
    new-instance v2, Landroid/transition/ChangeBounds;

    invoke-direct {v2}, Landroid/transition/ChangeBounds;-><init>()V

    .line 525
    .local v2, "logoMove":Landroid/transition/ChangeBounds;
    iget-object v9, p0, Lcom/google/android/videos/activity/ShowActivity;->posterHeroView:Landroid/widget/ImageView;

    invoke-virtual {v2, v9}, Landroid/transition/ChangeBounds;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 526
    invoke-virtual {v2, v12, v13}, Landroid/transition/ChangeBounds;->setDuration(J)Landroid/transition/Transition;

    .line 527
    invoke-virtual {v2, v0}, Landroid/transition/ChangeBounds;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/Transition;

    .line 528
    invoke-virtual {v2, v4}, Landroid/transition/ChangeBounds;->setPathMotion(Landroid/transition/PathMotion;)V

    .line 529
    int-to-long v10, v5

    invoke-virtual {v2, v10, v11}, Landroid/transition/ChangeBounds;->setStartDelay(J)Landroid/transition/Transition;

    .line 530
    new-instance v9, Lcom/google/android/videos/activity/ShowActivity$6;

    invoke-direct {v9, p0, p1}, Lcom/google/android/videos/activity/ShowActivity$6;-><init>(Lcom/google/android/videos/activity/ShowActivity;Z)V

    invoke-virtual {v2, v9}, Landroid/transition/ChangeBounds;->addListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/Transition;

    .line 545
    invoke-virtual {v7, v2}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 547
    new-instance v3, Landroid/transition/ChangeImageTransform;

    invoke-direct {v3}, Landroid/transition/ChangeImageTransform;-><init>()V

    .line 548
    .local v3, "logoTransform":Landroid/transition/ChangeImageTransform;
    iget-object v9, p0, Lcom/google/android/videos/activity/ShowActivity;->posterHeroView:Landroid/widget/ImageView;

    invoke-virtual {v3, v9}, Landroid/transition/ChangeImageTransform;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 549
    invoke-virtual {v3, v12, v13}, Landroid/transition/ChangeImageTransform;->setDuration(J)Landroid/transition/Transition;

    .line 550
    invoke-virtual {v3, v0}, Landroid/transition/ChangeImageTransform;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/Transition;

    .line 551
    int-to-long v10, v5

    invoke-virtual {v3, v10, v11}, Landroid/transition/ChangeImageTransform;->setStartDelay(J)Landroid/transition/Transition;

    .line 552
    invoke-virtual {v7, v3}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 554
    new-instance v6, Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;

    invoke-direct {v6}, Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;-><init>()V

    .line 555
    .local v6, "posterHeroTinter":Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;
    iget-object v9, p0, Lcom/google/android/videos/activity/ShowActivity;->posterHeroView:Landroid/widget/ImageView;

    invoke-virtual {v6, v9}, Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 556
    invoke-virtual {v6, v12, v13}, Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;->setDuration(J)Landroid/transition/Transition;

    .line 557
    invoke-virtual {v6, v0}, Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/Transition;

    .line 558
    int-to-long v10, v5

    invoke-virtual {v6, v10, v11}, Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;->setStartDelay(J)Landroid/transition/Transition;

    .line 559
    invoke-virtual {v7, v6}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 561
    new-instance v1, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;

    if-nez p1, :cond_3

    const/4 v9, 0x1

    :goto_2
    invoke-direct {v1, v9}, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;-><init>(Z)V

    .line 562
    .local v1, "logoCrop":Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;
    iget-object v9, p0, Lcom/google/android/videos/activity/ShowActivity;->posterHeroView:Landroid/widget/ImageView;

    invoke-virtual {v1, v9}, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 563
    const-wide/16 v10, 0x1c2

    invoke-virtual {v1, v10, v11}, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->setDuration(J)Landroid/transition/Transition;

    .line 564
    if-eqz p1, :cond_1

    const/16 v8, 0x32

    :cond_1
    add-int/2addr v8, v5

    int-to-long v8, v8

    invoke-virtual {v1, v8, v9}, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->setStartDelay(J)Landroid/transition/Transition;

    .line 565
    invoke-direct {p0}, Lcom/google/android/videos/activity/ShowActivity;->getHeroInnerRadius()I

    move-result v8

    invoke-virtual {v1, v8}, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->setSmallerRadius(I)V

    .line 566
    invoke-virtual {v7, v1}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 568
    if-eqz p1, :cond_4

    .line 569
    iput-object v6, p0, Lcom/google/android/videos/activity/ShowActivity;->enterPosterHeroTinter:Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;

    .line 570
    iput-object v1, p0, Lcom/google/android/videos/activity/ShowActivity;->enterLogoCrop:Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;

    goto/16 :goto_0

    .line 519
    .end local v1    # "logoCrop":Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;
    .end local v2    # "logoMove":Landroid/transition/ChangeBounds;
    .end local v3    # "logoTransform":Landroid/transition/ChangeImageTransform;
    .end local v5    # "moveStartDelay":I
    .end local v6    # "posterHeroTinter":Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;
    :cond_2
    const/16 v5, 0x96

    goto/16 :goto_1

    .restart local v2    # "logoMove":Landroid/transition/ChangeBounds;
    .restart local v3    # "logoTransform":Landroid/transition/ChangeImageTransform;
    .restart local v5    # "moveStartDelay":I
    .restart local v6    # "posterHeroTinter":Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;
    :cond_3
    move v9, v8

    .line 561
    goto :goto_2

    .line 572
    .restart local v1    # "logoCrop":Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;
    :cond_4
    iput-object v6, p0, Lcom/google/android/videos/activity/ShowActivity;->exitPosterHeroTinter:Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;

    .line 573
    iput-object v1, p0, Lcom/google/android/videos/activity/ShowActivity;->exitLogoCrop:Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;

    goto/16 :goto_0
.end method

.method protected getHeaderHeight()I
    .locals 1

    .prologue
    .line 368
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ShowActivity;->getScreenshotHeight()I

    move-result v0

    return v0
.end method

.method protected getLayoutId()I
    .locals 1

    .prologue
    .line 358
    const v0, 0x7f0400bf

    return v0
.end method

.method protected getScreenshotHeight()I
    .locals 1

    .prologue
    .line 363
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ShowActivity;->getStandardScreenshotHeight()I

    move-result v0

    return v0
.end method

.method public getSupportParentActivityIntent()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 871
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->account:Ljava/lang/String;

    const-string v1, "shows"

    invoke-static {p0, v0, v1}, Lcom/google/android/videos/activity/HomeActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public loadBackgroundImage()V
    .locals 6

    .prologue
    .line 373
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->posterStoreShowBannerRequester:Lcom/google/android/videos/async/Requester;

    if-nez v0, :cond_0

    .line 374
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->posterStore:Lcom/google/android/videos/store/PosterStore;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/videos/store/PosterStore;->getRequester(I)Lcom/google/android/videos/async/Requester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->posterStoreShowBannerRequester:Lcom/google/android/videos/async/Requester;

    .line 376
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/activity/ShowActivity;->posterStoreShowBannerRequester:Lcom/google/android/videos/async/Requester;

    iget-object v2, p0, Lcom/google/android/videos/activity/ShowActivity;->cpuExecutor:Ljava/util/concurrent/Executor;

    iget-object v3, p0, Lcom/google/android/videos/activity/ShowActivity;->showId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/videos/activity/ShowActivity;->missingBackgroundBitmap:Landroid/graphics/Bitmap;

    const/4 v5, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/ui/BitmapLoader;->setBitmapAsync(Lcom/google/android/videos/ui/BitmapLoader$BitmapPaletteView;Lcom/google/android/videos/async/Requester;Ljava/util/concurrent/Executor;Ljava/lang/Object;Landroid/graphics/Bitmap;Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;)V

    .line 378
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 791
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->onActivityResult(IILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 798
    :cond_0
    :goto_0
    return-void

    .line 794
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->onActivityResult(IILandroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 797
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/videos/activity/AssetDetailsActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onAuthenticated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "authToken"    # Ljava/lang/String;

    .prologue
    .line 802
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

    iget-object v1, p0, Lcom/google/android/videos/activity/ShowActivity;->syncTaskControl:Lcom/google/android/videos/async/TaskControl;

    invoke-static {p1, v1}, Lcom/google/android/videos/async/ControllableRequest;->create(Ljava/lang/Object;Lcom/google/android/videos/async/TaskStatus;)Lcom/google/android/videos/async/ControllableRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/activity/ShowActivity;->purchaseSyncCallback:Lcom/google/android/videos/async/NewCallback;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/store/PurchaseStoreSync;->syncPurchases(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V

    .line 804
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->wishlistStoreSync:Lcom/google/android/videos/store/WishlistStoreSync;

    const/4 v1, 0x0

    invoke-static {p1, v1}, Lcom/google/android/videos/async/ControllableRequest;->create(Ljava/lang/Object;Lcom/google/android/videos/async/TaskStatus;)Lcom/google/android/videos/async/ControllableRequest;

    move-result-object v1

    invoke-static {}, Lcom/google/android/videos/async/IgnoreCallback;->get()Lcom/google/android/videos/async/NewCallback;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/store/WishlistStoreSync;->syncWishlist(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V

    .line 806
    return-void
.end method

.method public onAuthenticationError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 817
    invoke-direct {p0, p2}, Lcom/google/android/videos/activity/ShowActivity;->onSyncError(Ljava/lang/Exception;)V

    .line 818
    return-void
.end method

.method public onBackgroundColorExtracted(IZ)V
    .locals 2
    .param p1, "color"    # I
    .param p2, "isFinalColor"    # Z

    .prologue
    .line 721
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->showHelper:Lcom/google/android/videos/ui/ShowHelper;

    if-nez v0, :cond_1

    .line 757
    :cond_0
    :goto_0
    return-void

    .line 726
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->showHelper:Lcom/google/android/videos/ui/ShowHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/ui/ShowHelper;->setHeaderBackgroundColor(I)V

    .line 729
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 733
    if-eqz p2, :cond_2

    .line 734
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/activity/ShowActivity;->haveHeaderColor:Z

    .line 738
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->exitPosterHeroTinter:Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;

    if-eqz v0, :cond_3

    .line 739
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->exitPosterHeroTinter:Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;->setFromColor(I)V

    .line 742
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->exitBackgroundTinter:Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;

    if-eqz v0, :cond_4

    .line 743
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->exitBackgroundTinter:Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;->setToColor(I)V

    .line 746
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/videos/activity/ShowActivity;->pendingEnterTransition:Z

    if-eqz v0, :cond_0

    .line 747
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->enterPosterHeroTinter:Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;

    if-eqz v0, :cond_5

    .line 748
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->enterPosterHeroTinter:Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;->setToColor(I)V

    .line 750
    :cond_5
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->enterBackgroundTinter:Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;

    if-eqz v0, :cond_6

    .line 751
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->enterBackgroundTinter:Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;->setFromColor(I)V

    .line 754
    :cond_6
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->backgroundImageView:Lcom/google/android/videos/ui/FocusedImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/ui/FocusedImageView;->setColorFilter(I)V

    .line 755
    invoke-direct {p0}, Lcom/google/android/videos/activity/ShowActivity;->startEnterTransitionIfReady()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 34
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 232
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/activity/ShowActivity;->getIntent()Landroid/content/Intent;

    move-result-object v33

    .line 233
    .local v33, "intent":Landroid/content/Intent;
    const-string v2, "authAccount"

    move-object/from16 v0, v33

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->account:Ljava/lang/String;

    .line 234
    const-string v2, "show_id"

    move-object/from16 v0, v33

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->showId:Ljava/lang/String;

    .line 235
    const-string v2, "start_download"

    const/4 v3, 0x0

    move-object/from16 v0, v33

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v27

    .line 236
    .local v27, "startDownload":Z
    const-string v2, "pinning_error_dialog"

    const/4 v3, 0x0

    move-object/from16 v0, v33

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v28

    .line 239
    .local v28, "pinningErrorDialog":Z
    invoke-super/range {p0 .. p1}, Lcom/google/android/videos/activity/AssetDetailsActivity;->onCreate(Landroid/os/Bundle;)V

    .line 241
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->account:Ljava/lang/String;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->showId:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 242
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "invalid arguments format: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/activity/ShowActivity;->account:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/videos/utils/Hashing;->sha1(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/activity/ShowActivity;->showId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 243
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/activity/ShowActivity;->finish()V

    .line 354
    :cond_1
    :goto_0
    return-void

    .line 247
    :cond_2
    new-instance v2, Lcom/google/android/videos/utils/DownloadedOnlyManager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/activity/ShowActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v3}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/videos/utils/DownloadedOnlyManager;-><init>(Landroid/content/SharedPreferences;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    .line 249
    new-instance v2, Lcom/google/android/videos/ui/BannerTextHelper;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/activity/ShowActivity;->playLayout:Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/activity/ShowActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/activity/ShowActivity;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-direct {v2, v3, v4, v6}, Lcom/google/android/videos/ui/BannerTextHelper;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/videos/utils/DownloadedOnlyManager;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->bannerTextHelper:Lcom/google/android/videos/ui/BannerTextHelper;

    .line 252
    const v2, 0x7f0f01fa

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/videos/activity/ShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-static {v0, v2, v1}, Lcom/google/android/videos/ui/StatusHelper;->createFromParent(Landroid/content/Context;Landroid/view/View;Lcom/google/android/videos/ui/StatusHelper$OnRetryListener;)Lcom/google/android/videos/ui/StatusHelper;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->statusHelper:Lcom/google/android/videos/ui/StatusHelper;

    .line 253
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->statusHelper:Lcom/google/android/videos/ui/StatusHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/StatusHelper;->init()V

    .line 255
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getUiEventLoggingHelper()Lcom/google/android/videos/logging/UiEventLoggingHelper;

    move-result-object v7

    .line 256
    .local v7, "uiEventLoggingHelper":Lcom/google/android/videos/logging/UiEventLoggingHelper;
    new-instance v2, Lcom/google/android/videos/logging/RootUiElementNodeImpl;

    const/4 v3, 0x7

    invoke-direct {v2, v3, v7}, Lcom/google/android/videos/logging/RootUiElementNodeImpl;-><init>(ILcom/google/android/videos/logging/UiEventLoggingHelper;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->rootUiElementNode:Lcom/google/android/videos/logging/RootUiElementNode;

    .line 259
    new-instance v5, Lcom/google/android/videos/store/StoreStatusMonitor;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/activity/ShowActivity;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/activity/ShowActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getWishlistStore()Lcom/google/android/videos/store/WishlistStore;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v2, v3, v4}, Lcom/google/android/videos/store/StoreStatusMonitor;-><init>(Landroid/content/Context;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/WishlistStore;)V

    .line 261
    .local v5, "storeStatusMonitor":Lcom/google/android/videos/store/StoreStatusMonitor;
    new-instance v2, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/activity/ShowActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v3}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/activity/ShowActivity;->purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v7}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/store/StoreStatusMonitor;Lcom/google/android/videos/store/PurchaseStoreSync;Lcom/google/android/videos/logging/UiEventLoggingHelper;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    .line 264
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v9

    .line 265
    .local v9, "config":Lcom/google/android/videos/Config;
    new-instance v10, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v10, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/activity/ShowActivity;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/videos/activity/ShowActivity;->account:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/activity/ShowActivity;->showId:Ljava/lang/String;

    invoke-interface {v9}, Lcom/google/android/videos/Config;->suggestionsEnabled()Z

    move-result v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/ShowActivity;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    move-object/from16 v16, v0

    move-object/from16 v8, p0

    invoke-static/range {v8 .. v16}, Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;->create(Landroid/app/Activity;Lcom/google/android/videos/Config;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/videos/utils/DownloadedOnlyManager;)Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->episodesCursorHelper:Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;

    .line 268
    const-string v2, "video_id"

    move-object/from16 v0, v33

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->episodeId:Ljava/lang/String;

    .line 269
    const-string v2, "season_id"

    move-object/from16 v0, v33

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->seasonId:Ljava/lang/String;

    .line 270
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->episodeId:Ljava/lang/String;

    if-nez v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->seasonId:Ljava/lang/String;

    if-nez v2, :cond_3

    if-eqz p1, :cond_3

    .line 272
    const-string v2, "video_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->episodeId:Ljava/lang/String;

    .line 273
    const-string v2, "season_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->seasonId:Ljava/lang/String;

    .line 275
    :cond_3
    if-nez p1, :cond_4

    .line 276
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->episodeId:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->episodeIdForHero:Ljava/lang/String;

    .line 280
    :goto_1
    new-instance v10, Lcom/google/android/videos/ui/ShowHelper;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/activity/ShowActivity;->contentLayout:Landroid/support/v7/widget/RecyclerView;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/activity/ShowActivity;->episodesCursorHelper:Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/ShowActivity;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/ShowActivity;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/ShowActivity;->posterStore:Lcom/google/android/videos/store/PosterStore;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getBitmapRequesters()Lcom/google/android/videos/bitmap/BitmapRequesters;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getPinHelper()Lcom/google/android/videos/ui/PinHelper;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/ShowActivity;->suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/ShowActivity;->account:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/ShowActivity;->showId:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/activity/ShowActivity;->account:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/videos/store/ConfigurationStore;->getPlayCountry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/ShowActivity;->rootUiElementNode:Lcom/google/android/videos/logging/RootUiElementNode;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v30

    move-object/from16 v11, p0

    move-object v15, v9

    move-object/from16 v31, v7

    invoke-direct/range {v10 .. v31}, Lcom/google/android/videos/ui/ShowHelper;-><init>(Lcom/google/android/videos/activity/ShowActivity;Landroid/support/v7/widget/RecyclerView;Landroid/view/View;Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;Lcom/google/android/videos/Config;Lcom/google/android/videos/utils/DownloadedOnlyManager;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/PosterStore;Lcom/google/android/videos/bitmap/BitmapRequesters;Lcom/google/android/videos/api/ApiRequesters;Lcom/google/android/videos/ui/PinHelper;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/google/android/videos/logging/UiElementNode;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/videos/logging/UiEventLoggingHelper;)V

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/google/android/videos/activity/ShowActivity;->showHelper:Lcom/google/android/videos/ui/ShowHelper;

    .line 303
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/activity/ShowActivity;->account:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/activity/ShowActivity;->showId:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v6}, Lcom/google/android/videos/store/PurchaseStoreSync;->setHiddenStateForShow(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 304
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->showHelper:Lcom/google/android/videos/ui/ShowHelper;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/videos/activity/ShowActivity;->backgroundColor:I

    invoke-virtual {v2, v3}, Lcom/google/android/videos/ui/ShowHelper;->setHeaderBackgroundColor(I)V

    .line 306
    new-instance v2, Lcom/google/android/videos/activity/ShowActivity$2;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/videos/activity/ShowActivity$2;-><init>(Lcom/google/android/videos/activity/ShowActivity;)V

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/videos/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/ActivityCallback;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->purchaseSyncCallback:Lcom/google/android/videos/async/NewCallback;

    .line 317
    new-instance v2, Lcom/google/android/videos/activity/ShowActivity$3;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/videos/activity/ShowActivity$3;-><init>(Lcom/google/android/videos/activity/ShowActivity;)V

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/videos/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/ActivityCallback;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->purchaseCallback:Lcom/google/android/videos/async/Callback;

    .line 344
    new-instance v32, Landroid/content/Intent;

    invoke-direct/range {v32 .. v33}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 345
    .local v32, "clearedIntent":Landroid/content/Intent;
    const-string v2, "video_id"

    move-object/from16 v0, v32

    invoke-virtual {v0, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 346
    const-string v2, "season_id"

    move-object/from16 v0, v32

    invoke-virtual {v0, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 347
    const-string v2, "start_download"

    move-object/from16 v0, v32

    invoke-virtual {v0, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 348
    const-string v2, "pinning_error_dialog"

    move-object/from16 v0, v32

    invoke-virtual {v0, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 349
    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/ShowActivity;->setIntent(Landroid/content/Intent;)V

    .line 351
    sget v2, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_1

    .line 352
    invoke-direct/range {p0 .. p0}, Lcom/google/android/videos/activity/ShowActivity;->initEnterSharedElementCallback()V

    goto/16 :goto_0

    .line 278
    .end local v32    # "clearedIntent":Landroid/content/Intent;
    :cond_4
    const-string v2, "episode_id_for_hero"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/ShowActivity;->episodeIdForHero:Ljava/lang/String;

    goto/16 :goto_1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 841
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->episodesCursorHelper:Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;->onDestroy()V

    .line 842
    invoke-super {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->onDestroy()V

    .line 843
    return-void
.end method

.method public onEnabledVerticalsChanged(Ljava/lang/String;I)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "verticals"    # I

    .prologue
    .line 947
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->videosDrawerHelper:Lcom/google/android/videos/ui/VideosDrawerHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->account:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 948
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->videosDrawerHelper:Lcom/google/android/videos/ui/VideosDrawerHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/ui/VideosDrawerHelper;->init(Ljava/lang/String;I)Z

    .line 950
    :cond_0
    return-void
.end method

.method protected onEnterTransitionFinish()V
    .locals 1

    .prologue
    .line 691
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->showHelper:Lcom/google/android/videos/ui/ShowHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/ShowHelper;->updateVisibilities()V

    .line 692
    return-void
.end method

.method protected onEnterTransitionStart()V
    .locals 2

    .prologue
    .line 675
    iget-boolean v0, p0, Lcom/google/android/videos/activity/ShowActivity;->activityRunning:Z

    if-eqz v0, :cond_0

    .line 679
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->contentLayout:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lcom/google/android/videos/activity/ShowActivity$9;

    invoke-direct {v1, p0}, Lcom/google/android/videos/activity/ShowActivity$9;-><init>(Lcom/google/android/videos/activity/ShowActivity;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->post(Ljava/lang/Runnable;)Z

    .line 685
    invoke-direct {p0}, Lcom/google/android/videos/activity/ShowActivity;->startSync()V

    .line 687
    :cond_0
    return-void
.end method

.method public onHasContentChanged(Ljava/lang/String;Z)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "hasContent"    # Z

    .prologue
    .line 943
    return-void
.end method

.method public onHeaderViewCreated()V
    .locals 1

    .prologue
    .line 760
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/activity/ShowActivity;->headerViewCreated:Z

    .line 761
    invoke-direct {p0}, Lcom/google/android/videos/activity/ShowActivity;->startEnterTransitionIfReady()V

    .line 762
    return-void
.end method

.method public onHelpSelected()V
    .locals 1

    .prologue
    .line 847
    const-string v0, "mobile_show_object"

    invoke-static {p0, v0}, Lcom/google/android/videos/ui/HelpHelper;->startContextualHelp(Landroid/app/Activity;Ljava/lang/String;)V

    .line 848
    return-void
.end method

.method public onNotAuthenticated(Ljava/lang/String;)V
    .locals 2
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 810
    const v0, 0x7f0b0145

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/google/android/videos/utils/Util;->showToast(Landroid/content/Context;II)V

    .line 811
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ShowActivity;->finish()V

    .line 812
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 859
    iget-object v2, p0, Lcom/google/android/videos/activity/ShowActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v0

    .line 860
    .local v0, "eventLogger":Lcom/google/android/videos/logging/EventLogger;
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x7f0f0225

    if-ne v2, v3, :cond_1

    .line 861
    iget-object v2, p0, Lcom/google/android/videos/activity/ShowActivity;->account:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/activity/ShowActivity;->showId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/videos/activity/ShowActivity;->showTitle:Ljava/lang/String;

    invoke-static {p0, v2, v3, v4, v1}, Lcom/google/android/videos/ui/RemoveItemDialogFragment;->showInstanceWithCallback(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 862
    iget-object v2, p0, Lcom/google/android/videos/activity/ShowActivity;->showId:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcom/google/android/videos/logging/EventLogger;->onRemoveItemDialogShown(Ljava/lang/String;)V

    .line 865
    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-static {p1, p0}, Lcom/google/android/videos/VideosApplication;->onCommonOptionsItemSelected(Landroid/view/MenuItem;Landroid/app/Activity;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-super {p0, p1}, Lcom/google/android/videos/activity/AssetDetailsActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 710
    invoke-super {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->onPause()V

    .line 711
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->rootUiElementNode:Lcom/google/android/videos/logging/RootUiElementNode;

    invoke-interface {v0}, Lcom/google/android/videos/logging/RootUiElementNode;->flushImpression()V

    .line 712
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 852
    invoke-super {p0, p1}, Lcom/google/android/videos/activity/AssetDetailsActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 853
    const v0, 0x7f0f0225

    iget-boolean v1, p0, Lcom/google/android/videos/activity/ShowActivity;->haveSeasons:Z

    invoke-static {p1, v0, v1}, Lcom/google/android/videos/utils/Util;->setMenuItemEnabled(Landroid/view/Menu;IZ)V

    .line 854
    const/4 v0, 0x1

    return v0
.end method

.method public onRemoved(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "itemId"    # Ljava/lang/String;
    .param p3, "itemIsShow"    # Z

    .prologue
    .line 876
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->account:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->showId:Ljava/lang/String;

    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    .line 877
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ShowActivity;->finish()V

    .line 879
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 696
    invoke-super {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->onResume()V

    .line 697
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->rootUiElementNode:Lcom/google/android/videos/logging/RootUiElementNode;

    invoke-interface {v0}, Lcom/google/android/videos/logging/RootUiElementNode;->startNewImpression()V

    .line 698
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->refreshContentRestrictions()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 699
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ShowActivity;->finish()V

    .line 706
    :cond_0
    :goto_0
    return-void

    .line 702
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/activity/ShowActivity;->showId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/activity/ShowActivity;->seasonId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/videos/logging/EventLogger;->onShowPageOpened(Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->videosDrawerHelper:Lcom/google/android/videos/ui/VideosDrawerHelper;

    if-eqz v0, :cond_0

    .line 704
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->videosDrawerHelper:Lcom/google/android/videos/ui/VideosDrawerHelper;

    iget-object v1, p0, Lcom/google/android/videos/activity/ShowActivity;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/activity/ShowActivity;->verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;

    iget-object v3, p0, Lcom/google/android/videos/activity/ShowActivity;->account:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/videos/ui/VerticalsHelper;->enabledVerticalsForUser(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/ui/VideosDrawerHelper;->init(Ljava/lang/String;I)Z

    goto :goto_0
.end method

.method public onRetry()V
    .locals 0

    .prologue
    .line 716
    invoke-direct {p0}, Lcom/google/android/videos/activity/ShowActivity;->startSync()V

    .line 717
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 766
    invoke-super {p0, p1}, Lcom/google/android/videos/activity/AssetDetailsActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 767
    iget-object v2, p0, Lcom/google/android/videos/activity/ShowActivity;->showHelper:Lcom/google/android/videos/ui/ShowHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/ShowHelper;->getCurrentEpisodeId()Ljava/lang/String;

    move-result-object v0

    .line 768
    .local v0, "currentEpisodeId":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 769
    const-string v2, "video_id"

    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/activity/ShowActivity;->showHelper:Lcom/google/android/videos/ui/ShowHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/ShowHelper;->getCurrentSeasonId()Ljava/lang/String;

    move-result-object v1

    .line 772
    .local v1, "currentSeasonId":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 773
    const-string v2, "season_id"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 775
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/activity/ShowActivity;->episodeIdForHero:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 776
    const-string v2, "episode_id_for_hero"

    iget-object v3, p0, Lcom/google/android/videos/activity/ShowActivity;->episodeIdForHero:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 778
    :cond_2
    return-void
.end method

.method protected onStart()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 646
    invoke-super {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->onStart()V

    .line 647
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/activity/ShowActivity;->account:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_0

    .line 648
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "account does not exist: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/activity/ShowActivity;->account:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/videos/utils/Hashing;->sha1(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 649
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ShowActivity;->finish()V

    .line 671
    :goto_0
    return-void

    .line 652
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    iget-object v1, p0, Lcom/google/android/videos/activity/ShowActivity;->account:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->init(Ljava/lang/String;)V

    .line 653
    invoke-direct {p0}, Lcom/google/android/videos/activity/ShowActivity;->refreshPurchase()V

    .line 654
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->seasonId:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 655
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->showHelper:Lcom/google/android/videos/ui/ShowHelper;

    iget-object v1, p0, Lcom/google/android/videos/activity/ShowActivity;->seasonId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/ShowHelper;->setSeasonIdToSelect(Ljava/lang/String;)V

    .line 656
    iput-object v2, p0, Lcom/google/android/videos/activity/ShowActivity;->seasonId:Ljava/lang/String;

    .line 658
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->episodeId:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 659
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->showHelper:Lcom/google/android/videos/ui/ShowHelper;

    iget-object v1, p0, Lcom/google/android/videos/activity/ShowActivity;->episodeId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/ShowHelper;->setEpisodeIdToSelect(Ljava/lang/String;)V

    .line 660
    iput-object v2, p0, Lcom/google/android/videos/activity/ShowActivity;->episodeId:Ljava/lang/String;

    .line 662
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->showHelper:Lcom/google/android/videos/ui/ShowHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/ShowHelper;->onStart()V

    .line 663
    iget-boolean v0, p0, Lcom/google/android/videos/activity/ShowActivity;->pendingEnterTransition:Z

    if-nez v0, :cond_3

    .line 664
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->episodesCursorHelper:Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;->onStart()V

    .line 665
    invoke-direct {p0}, Lcom/google/android/videos/activity/ShowActivity;->startSync()V

    .line 667
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/VerticalsHelper;->addOnContentChangedListener(Lcom/google/android/videos/ui/VerticalsHelper$OnContentChangedListener;)V

    .line 668
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->mediaRouteProvider:Lcom/google/android/videos/remote/MediaRouteProvider;

    invoke-interface {v0}, Lcom/google/android/videos/remote/MediaRouteProvider;->onStart()V

    .line 669
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->bannerTextHelper:Lcom/google/android/videos/ui/BannerTextHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/BannerTextHelper;->enable()V

    .line 670
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/activity/ShowActivity;->started:Z

    goto :goto_0
.end method

.method protected onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 822
    iget-boolean v0, p0, Lcom/google/android/videos/activity/ShowActivity;->started:Z

    if-eqz v0, :cond_0

    .line 823
    iput-boolean v1, p0, Lcom/google/android/videos/activity/ShowActivity;->started:Z

    .line 824
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->reset()V

    .line 825
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->currentPurchaseRequest:Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .line 826
    iput-boolean v1, p0, Lcom/google/android/videos/activity/ShowActivity;->pendingPurchaseRequest:Z

    .line 827
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->episodesCursorHelper:Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;->onStop()V

    .line 828
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->showHelper:Lcom/google/android/videos/ui/ShowHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/ShowHelper;->onStop()V

    .line 829
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/VerticalsHelper;->removeOnContentChangedListener(Lcom/google/android/videos/ui/VerticalsHelper$OnContentChangedListener;)V

    .line 830
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->syncTaskControl:Lcom/google/android/videos/async/TaskControl;

    invoke-static {v0}, Lcom/google/android/videos/async/TaskControl;->cancel(Lcom/google/android/videos/async/TaskControl;)V

    .line 831
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->mediaRouteProvider:Lcom/google/android/videos/remote/MediaRouteProvider;

    invoke-interface {v0}, Lcom/google/android/videos/remote/MediaRouteProvider;->onStop()V

    .line 832
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity;->bannerTextHelper:Lcom/google/android/videos/ui/BannerTextHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/BannerTextHelper;->disable()V

    .line 833
    invoke-super {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->resetDrawer()V

    .line 836
    :cond_0
    invoke-super {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->onStop()V

    .line 837
    return-void
.end method
