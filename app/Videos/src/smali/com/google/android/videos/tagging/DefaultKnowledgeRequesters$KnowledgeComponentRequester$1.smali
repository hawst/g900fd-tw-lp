.class Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester$1;
.super Ljava/lang/Object;
.source "DefaultKnowledgeRequesters.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->request(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;Lcom/google/android/videos/async/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<TT;TS;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;

.field final synthetic val$callback:Lcom/google/android/videos/async/Callback;

.field final synthetic val$finalStoredResponse:Ljava/lang/Object;

.field final synthetic val$hadResponse:Z

.field final synthetic val$oldResponse:Ljava/lang/Object;

.field final synthetic val$request:Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;


# direct methods
.method constructor <init>(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;ZLjava/lang/Object;Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    .locals 0

    .prologue
    .line 515
    .local p0, "this":Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester$1;, "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester.1;"
    iput-object p1, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester$1;->this$0:Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;

    iput-object p2, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester$1;->val$request:Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;

    iput-boolean p3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester$1;->val$hadResponse:Z

    iput-object p4, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester$1;->val$finalStoredResponse:Ljava/lang/Object;

    iput-object p5, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester$1;->val$oldResponse:Ljava/lang/Object;

    iput-object p6, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester$1;->val$callback:Lcom/google/android/videos/async/Callback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 6
    .param p2, "exception"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    .prologue
    .line 523
    .local p0, "this":Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester$1;, "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester.1;"
    .local p1, "unused":Ljava/lang/Object;, "TT;"
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester$1;->this$0:Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;

    iget-object v1, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester$1;->val$request:Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;

    iget-boolean v2, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester$1;->val$hadResponse:Z

    iget-object v3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester$1;->val$oldResponse:Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester$1;->val$callback:Lcom/google/android/videos/async/Callback;

    move-object v4, p2

    # invokes: Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->handleError(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;ZLjava/lang/Object;Ljava/lang/Exception;Lcom/google/android/videos/async/Callback;)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->access$400(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;ZLjava/lang/Object;Ljava/lang/Exception;Lcom/google/android/videos/async/Callback;)V

    .line 524
    return-void
.end method

.method public onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TS;)V"
        }
    .end annotation

    .prologue
    .line 518
    .local p0, "this":Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester$1;, "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester.1;"
    .local p1, "unused":Ljava/lang/Object;, "TT;"
    .local p2, "response":Ljava/lang/Object;, "TS;"
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester$1;->this$0:Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;

    iget-object v1, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester$1;->val$request:Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;

    iget-boolean v2, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester$1;->val$hadResponse:Z

    iget-object v3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester$1;->val$finalStoredResponse:Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester$1;->val$oldResponse:Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester$1;->val$callback:Lcom/google/android/videos/async/Callback;

    move-object v5, p2

    # invokes: Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->handleResponse(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    invoke-static/range {v0 .. v6}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->access$300(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 520
    return-void
.end method
