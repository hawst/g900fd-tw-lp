.class final Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot$ParcelableCreator;
.super Ljava/lang/Object;
.source "SubtitleWindowSnapshot.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ParcelableCreator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot$1;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot$ParcelableCreator;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 57
    new-instance v0, Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;-><init>(Landroid/os/Parcel;Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot$1;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot$ParcelableCreator;->createFromParcel(Landroid/os/Parcel;)Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 62
    new-array v0, p1, [Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot$ParcelableCreator;->newArray(I)[Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;

    move-result-object v0

    return-object v0
.end method
