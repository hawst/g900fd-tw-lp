.class public Lcom/google/android/videos/player/exo/adaptive/DebugVideoChunkSource;
.super Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;
.source "DebugVideoChunkSource.java"


# instance fields
.field private fromFormatIndex:I

.field private selectFromFormat:Z

.field private toFormatIndex:I


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/upstream/DataSource;Landroid/content/Context;Lcom/google/android/videos/Config;Lcom/google/android/exoplayer/upstream/BandwidthMeter;Lcom/google/android/videos/utils/NetworkStatus;[Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;)V
    .locals 8
    .param p1, "dataSource"    # Lcom/google/android/exoplayer/upstream/DataSource;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "config"    # Lcom/google/android/videos/Config;
    .param p4, "bandwidthMeter"    # Lcom/google/android/exoplayer/upstream/BandwidthMeter;
    .param p5, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;
    .param p6, "representations"    # [Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    .prologue
    .line 30
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;-><init>(Lcom/google/android/exoplayer/upstream/DataSource;Landroid/content/Context;Lcom/google/android/videos/Config;Lcom/google/android/exoplayer/upstream/BandwidthMeter;Lcom/google/android/videos/utils/NetworkStatus;Z[Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;)V

    .line 31
    return-void
.end method


# virtual methods
.method public enable()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/player/exo/adaptive/DebugVideoChunkSource;->selectFromFormat:Z

    .line 36
    iput v1, p0, Lcom/google/android/videos/player/exo/adaptive/DebugVideoChunkSource;->fromFormatIndex:I

    .line 37
    iput v1, p0, Lcom/google/android/videos/player/exo/adaptive/DebugVideoChunkSource;->toFormatIndex:I

    .line 38
    return-void
.end method

.method public evaluate(Ljava/util/List;JJ[Lcom/google/android/exoplayer/chunk/Format;JZZZZLcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;)Z
    .locals 4
    .param p2, "seekPositionUs"    # J
    .param p4, "playbackPositionUs"    # J
    .param p6, "formats"    # [Lcom/google/android/exoplayer/chunk/Format;
    .param p7, "bitrateEstimate"    # J
    .param p9, "isFastNetwork"    # Z
    .param p10, "isHdDisabled"    # Z
    .param p11, "trickPlayEnabled"    # Z
    .param p12, "exitingTrickPlay"    # Z
    .param p13, "evaluation"    # Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/google/android/exoplayer/chunk/MediaChunk;",
            ">;JJ[",
            "Lcom/google/android/exoplayer/chunk/Format;",
            "JZZZZ",
            "Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 46
    .local p1, "queue":Ljava/util/List;, "Ljava/util/List<+Lcom/google/android/exoplayer/chunk/MediaChunk;>;"
    iget-boolean v2, p0, Lcom/google/android/videos/player/exo/adaptive/DebugVideoChunkSource;->selectFromFormat:Z

    if-eqz v2, :cond_1

    .line 47
    iget v1, p0, Lcom/google/android/videos/player/exo/adaptive/DebugVideoChunkSource;->fromFormatIndex:I

    .line 58
    .local v1, "formatIndex":I
    :cond_0
    :goto_0
    iget-boolean v2, p0, Lcom/google/android/videos/player/exo/adaptive/DebugVideoChunkSource;->selectFromFormat:Z

    if-nez v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    iput-boolean v2, p0, Lcom/google/android/videos/player/exo/adaptive/DebugVideoChunkSource;->selectFromFormat:Z

    .line 59
    aget-object v2, p6, v1

    move-object/from16 v0, p13

    iput-object v2, v0, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->format:Lcom/google/android/exoplayer/chunk/Format;

    .line 60
    const/4 v2, 0x0

    return v2

    .line 49
    .end local v1    # "formatIndex":I
    :cond_1
    iget v1, p0, Lcom/google/android/videos/player/exo/adaptive/DebugVideoChunkSource;->toFormatIndex:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/videos/player/exo/adaptive/DebugVideoChunkSource;->toFormatIndex:I

    .line 50
    .restart local v1    # "formatIndex":I
    iget v2, p0, Lcom/google/android/videos/player/exo/adaptive/DebugVideoChunkSource;->toFormatIndex:I

    array-length v3, p6

    if-ne v2, v3, :cond_0

    .line 51
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/videos/player/exo/adaptive/DebugVideoChunkSource;->toFormatIndex:I

    .line 52
    iget v2, p0, Lcom/google/android/videos/player/exo/adaptive/DebugVideoChunkSource;->fromFormatIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/videos/player/exo/adaptive/DebugVideoChunkSource;->fromFormatIndex:I

    .line 53
    iget v2, p0, Lcom/google/android/videos/player/exo/adaptive/DebugVideoChunkSource;->fromFormatIndex:I

    array-length v3, p6

    if-ne v2, v3, :cond_0

    .line 54
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/videos/player/exo/adaptive/DebugVideoChunkSource;->fromFormatIndex:I

    goto :goto_0

    .line 58
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method
