.class Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper$RedeemListener;
.super Ljava/lang/Object;
.source "FreeMoviePromoDetailsRowHelper.java"

# interfaces
.implements Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RedeemListener"
.end annotation


# instance fields
.field private final activity:Landroid/app/Activity;

.field private final errorHelper:Lcom/google/android/videos/utils/ErrorHelper;

.field private final itemId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/app/Activity;Lcom/google/android/videos/utils/ErrorHelper;)V
    .locals 0
    .param p1, "itemId"    # Ljava/lang/String;
    .param p2, "activity"    # Landroid/app/Activity;
    .param p3, "errorHelper"    # Lcom/google/android/videos/utils/ErrorHelper;

    .prologue
    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    iput-object p1, p0, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper$RedeemListener;->itemId:Ljava/lang/String;

    .line 133
    iput-object p2, p0, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper$RedeemListener;->activity:Landroid/app/Activity;

    .line 134
    iput-object p3, p0, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper$RedeemListener;->errorHelper:Lcom/google/android/videos/utils/ErrorHelper;

    .line 135
    return-void
.end method


# virtual methods
.method public onPromoRedeemError(Ljava/lang/Exception;)V
    .locals 1
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper$RedeemListener;->errorHelper:Lcom/google/android/videos/utils/ErrorHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/utils/ErrorHelper;->showToast(Ljava/lang/Throwable;)V

    .line 146
    return-void
.end method

.method public onPromoRedeemed()V
    .locals 3

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper$RedeemListener;->activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper$RedeemListener;->activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper$RedeemListener;->itemId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/videos/pano/activity/MovieDetailsActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 140
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/FreeMoviePromoDetailsRowHelper$RedeemListener;->activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 141
    return-void
.end method
