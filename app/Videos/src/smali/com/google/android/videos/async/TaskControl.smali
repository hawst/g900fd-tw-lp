.class public Lcom/google/android/videos/async/TaskControl;
.super Lcom/google/android/videos/async/TaskStatus;
.source "TaskControl.java"


# instance fields
.field private volatile isCancelled:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/google/android/videos/async/TaskStatus;-><init>()V

    return-void
.end method

.method public static cancel(Lcom/google/android/videos/async/TaskControl;)V
    .locals 0
    .param p0, "control"    # Lcom/google/android/videos/async/TaskControl;

    .prologue
    .line 40
    if-eqz p0, :cond_0

    .line 41
    invoke-virtual {p0}, Lcom/google/android/videos/async/TaskControl;->cancel()V

    .line 43
    :cond_0
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/async/TaskControl;->isCancelled:Z

    .line 34
    return-void
.end method

.method public isCancelled()Z
    .locals 1

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/google/android/videos/async/TaskControl;->isCancelled:Z

    return v0
.end method
