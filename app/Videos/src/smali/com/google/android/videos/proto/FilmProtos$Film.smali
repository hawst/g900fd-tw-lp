.class public final Lcom/google/android/videos/proto/FilmProtos$Film;
.super Lcom/google/protobuf/nano/MessageNano;
.source "FilmProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/proto/FilmProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Film"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/videos/proto/FilmProtos$Film;


# instance fields
.field public actor:[Lcom/google/android/videos/proto/FilmProtos$Person;

.field public album:[Lcom/google/android/videos/proto/FilmProtos$Album;

.field public director:[Lcom/google/android/videos/proto/FilmProtos$Person;

.field public endDate:Ljava/lang/String;

.field public googlePlayUrl:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field public image:Lcom/google/android/videos/proto/FilmProtos$Image;

.field public isTv:Z

.field public mid:Ljava/lang/String;

.field public referencedFilm:[Lcom/google/android/videos/proto/FilmProtos$Film;

.field public releaseDate:Ljava/lang/String;

.field public runtimeMinutes:I

.field public song:[Lcom/google/android/videos/proto/FilmProtos$Song;

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 68
    invoke-virtual {p0}, Lcom/google/android/videos/proto/FilmProtos$Film;->clear()Lcom/google/android/videos/proto/FilmProtos$Film;

    .line 69
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/videos/proto/FilmProtos$Film;
    .locals 2

    .prologue
    .line 14
    sget-object v0, Lcom/google/android/videos/proto/FilmProtos$Film;->_emptyArray:[Lcom/google/android/videos/proto/FilmProtos$Film;

    if-nez v0, :cond_1

    .line 15
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Lcom/google/android/videos/proto/FilmProtos$Film;->_emptyArray:[Lcom/google/android/videos/proto/FilmProtos$Film;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/videos/proto/FilmProtos$Film;

    sput-object v0, Lcom/google/android/videos/proto/FilmProtos$Film;->_emptyArray:[Lcom/google/android/videos/proto/FilmProtos$Film;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :cond_1
    sget-object v0, Lcom/google/android/videos/proto/FilmProtos$Film;->_emptyArray:[Lcom/google/android/videos/proto/FilmProtos$Film;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/videos/proto/FilmProtos$Film;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 72
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->mid:Ljava/lang/String;

    .line 73
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->id:Ljava/lang/String;

    .line 74
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->title:Ljava/lang/String;

    .line 75
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->releaseDate:Ljava/lang/String;

    .line 76
    iput v1, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->runtimeMinutes:I

    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    .line 78
    invoke-static {}, Lcom/google/android/videos/proto/FilmProtos$Person;->emptyArray()[Lcom/google/android/videos/proto/FilmProtos$Person;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->actor:[Lcom/google/android/videos/proto/FilmProtos$Person;

    .line 79
    invoke-static {}, Lcom/google/android/videos/proto/FilmProtos$Person;->emptyArray()[Lcom/google/android/videos/proto/FilmProtos$Person;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->director:[Lcom/google/android/videos/proto/FilmProtos$Person;

    .line 80
    invoke-static {}, Lcom/google/android/videos/proto/FilmProtos$Song;->emptyArray()[Lcom/google/android/videos/proto/FilmProtos$Song;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->song:[Lcom/google/android/videos/proto/FilmProtos$Song;

    .line 81
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->googlePlayUrl:Ljava/lang/String;

    .line 82
    invoke-static {}, Lcom/google/android/videos/proto/FilmProtos$Film;->emptyArray()[Lcom/google/android/videos/proto/FilmProtos$Film;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->referencedFilm:[Lcom/google/android/videos/proto/FilmProtos$Film;

    .line 83
    iput-boolean v1, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->isTv:Z

    .line 84
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->endDate:Ljava/lang/String;

    .line 85
    invoke-static {}, Lcom/google/android/videos/proto/FilmProtos$Album;->emptyArray()[Lcom/google/android/videos/proto/FilmProtos$Album;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->album:[Lcom/google/android/videos/proto/FilmProtos$Album;

    .line 86
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->cachedSize:I

    .line 87
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 286
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 287
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->mid:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 288
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->mid:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 291
    :cond_0
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->id:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 292
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->id:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 295
    :cond_1
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->title:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 296
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->title:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 299
    :cond_2
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->releaseDate:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 300
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->releaseDate:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 303
    :cond_3
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    if-eqz v3, :cond_4

    .line 304
    const/4 v3, 0x5

    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 307
    :cond_4
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->actor:[Lcom/google/android/videos/proto/FilmProtos$Person;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->actor:[Lcom/google/android/videos/proto/FilmProtos$Person;

    array-length v3, v3

    if-lez v3, :cond_6

    .line 308
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->actor:[Lcom/google/android/videos/proto/FilmProtos$Person;

    array-length v3, v3

    if-ge v1, v3, :cond_6

    .line 309
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->actor:[Lcom/google/android/videos/proto/FilmProtos$Person;

    aget-object v0, v3, v1

    .line 310
    .local v0, "element":Lcom/google/android/videos/proto/FilmProtos$Person;
    if-eqz v0, :cond_5

    .line 311
    const/4 v3, 0x6

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 308
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 316
    .end local v0    # "element":Lcom/google/android/videos/proto/FilmProtos$Person;
    .end local v1    # "i":I
    :cond_6
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->director:[Lcom/google/android/videos/proto/FilmProtos$Person;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->director:[Lcom/google/android/videos/proto/FilmProtos$Person;

    array-length v3, v3

    if-lez v3, :cond_8

    .line 317
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->director:[Lcom/google/android/videos/proto/FilmProtos$Person;

    array-length v3, v3

    if-ge v1, v3, :cond_8

    .line 318
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->director:[Lcom/google/android/videos/proto/FilmProtos$Person;

    aget-object v0, v3, v1

    .line 319
    .restart local v0    # "element":Lcom/google/android/videos/proto/FilmProtos$Person;
    if-eqz v0, :cond_7

    .line 320
    const/4 v3, 0x7

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 317
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 325
    .end local v0    # "element":Lcom/google/android/videos/proto/FilmProtos$Person;
    .end local v1    # "i":I
    :cond_8
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->song:[Lcom/google/android/videos/proto/FilmProtos$Song;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->song:[Lcom/google/android/videos/proto/FilmProtos$Song;

    array-length v3, v3

    if-lez v3, :cond_a

    .line 326
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->song:[Lcom/google/android/videos/proto/FilmProtos$Song;

    array-length v3, v3

    if-ge v1, v3, :cond_a

    .line 327
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->song:[Lcom/google/android/videos/proto/FilmProtos$Song;

    aget-object v0, v3, v1

    .line 328
    .local v0, "element":Lcom/google/android/videos/proto/FilmProtos$Song;
    if-eqz v0, :cond_9

    .line 329
    const/16 v3, 0x8

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 326
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 334
    .end local v0    # "element":Lcom/google/android/videos/proto/FilmProtos$Song;
    .end local v1    # "i":I
    :cond_a
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->googlePlayUrl:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    .line 335
    const/16 v3, 0x9

    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->googlePlayUrl:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 338
    :cond_b
    iget v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->runtimeMinutes:I

    if-eqz v3, :cond_c

    .line 339
    const/16 v3, 0xa

    iget v4, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->runtimeMinutes:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 342
    :cond_c
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->referencedFilm:[Lcom/google/android/videos/proto/FilmProtos$Film;

    if-eqz v3, :cond_e

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->referencedFilm:[Lcom/google/android/videos/proto/FilmProtos$Film;

    array-length v3, v3

    if-lez v3, :cond_e

    .line 343
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->referencedFilm:[Lcom/google/android/videos/proto/FilmProtos$Film;

    array-length v3, v3

    if-ge v1, v3, :cond_e

    .line 344
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->referencedFilm:[Lcom/google/android/videos/proto/FilmProtos$Film;

    aget-object v0, v3, v1

    .line 345
    .local v0, "element":Lcom/google/android/videos/proto/FilmProtos$Film;
    if-eqz v0, :cond_d

    .line 346
    const/16 v3, 0xc

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 343
    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 351
    .end local v0    # "element":Lcom/google/android/videos/proto/FilmProtos$Film;
    .end local v1    # "i":I
    :cond_e
    iget-boolean v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->isTv:Z

    if-eqz v3, :cond_f

    .line 352
    const/16 v3, 0xd

    iget-boolean v4, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->isTv:Z

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 355
    :cond_f
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->endDate:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_10

    .line 356
    const/16 v3, 0xe

    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->endDate:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 359
    :cond_10
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->album:[Lcom/google/android/videos/proto/FilmProtos$Album;

    if-eqz v3, :cond_12

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->album:[Lcom/google/android/videos/proto/FilmProtos$Album;

    array-length v3, v3

    if-lez v3, :cond_12

    .line 360
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_4
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->album:[Lcom/google/android/videos/proto/FilmProtos$Album;

    array-length v3, v3

    if-ge v1, v3, :cond_12

    .line 361
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->album:[Lcom/google/android/videos/proto/FilmProtos$Album;

    aget-object v0, v3, v1

    .line 362
    .local v0, "element":Lcom/google/android/videos/proto/FilmProtos$Album;
    if-eqz v0, :cond_11

    .line 363
    const/16 v3, 0xf

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 360
    :cond_11
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 368
    .end local v0    # "element":Lcom/google/android/videos/proto/FilmProtos$Album;
    .end local v1    # "i":I
    :cond_12
    return v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 92
    if-ne p1, p0, :cond_1

    .line 176
    :cond_0
    :goto_0
    return v1

    .line 95
    :cond_1
    instance-of v3, p1, Lcom/google/android/videos/proto/FilmProtos$Film;

    if-nez v3, :cond_2

    move v1, v2

    .line 96
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 98
    check-cast v0, Lcom/google/android/videos/proto/FilmProtos$Film;

    .line 99
    .local v0, "other":Lcom/google/android/videos/proto/FilmProtos$Film;
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->mid:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 100
    iget-object v3, v0, Lcom/google/android/videos/proto/FilmProtos$Film;->mid:Ljava/lang/String;

    if-eqz v3, :cond_4

    move v1, v2

    .line 101
    goto :goto_0

    .line 103
    :cond_3
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->mid:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Film;->mid:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 104
    goto :goto_0

    .line 106
    :cond_4
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->id:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 107
    iget-object v3, v0, Lcom/google/android/videos/proto/FilmProtos$Film;->id:Ljava/lang/String;

    if-eqz v3, :cond_6

    move v1, v2

    .line 108
    goto :goto_0

    .line 110
    :cond_5
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->id:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Film;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    .line 111
    goto :goto_0

    .line 113
    :cond_6
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->title:Ljava/lang/String;

    if-nez v3, :cond_7

    .line 114
    iget-object v3, v0, Lcom/google/android/videos/proto/FilmProtos$Film;->title:Ljava/lang/String;

    if-eqz v3, :cond_8

    move v1, v2

    .line 115
    goto :goto_0

    .line 117
    :cond_7
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->title:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Film;->title:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    move v1, v2

    .line 118
    goto :goto_0

    .line 120
    :cond_8
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->releaseDate:Ljava/lang/String;

    if-nez v3, :cond_9

    .line 121
    iget-object v3, v0, Lcom/google/android/videos/proto/FilmProtos$Film;->releaseDate:Ljava/lang/String;

    if-eqz v3, :cond_a

    move v1, v2

    .line 122
    goto :goto_0

    .line 124
    :cond_9
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->releaseDate:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Film;->releaseDate:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    move v1, v2

    .line 125
    goto :goto_0

    .line 127
    :cond_a
    iget v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->runtimeMinutes:I

    iget v4, v0, Lcom/google/android/videos/proto/FilmProtos$Film;->runtimeMinutes:I

    if-eq v3, v4, :cond_b

    move v1, v2

    .line 128
    goto :goto_0

    .line 130
    :cond_b
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    if-nez v3, :cond_c

    .line 131
    iget-object v3, v0, Lcom/google/android/videos/proto/FilmProtos$Film;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    if-eqz v3, :cond_d

    move v1, v2

    .line 132
    goto :goto_0

    .line 135
    :cond_c
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Film;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    invoke-virtual {v3, v4}, Lcom/google/android/videos/proto/FilmProtos$Image;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_d

    move v1, v2

    .line 136
    goto :goto_0

    .line 139
    :cond_d
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->actor:[Lcom/google/android/videos/proto/FilmProtos$Person;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Film;->actor:[Lcom/google/android/videos/proto/FilmProtos$Person;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    move v1, v2

    .line 141
    goto/16 :goto_0

    .line 143
    :cond_e
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->director:[Lcom/google/android/videos/proto/FilmProtos$Person;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Film;->director:[Lcom/google/android/videos/proto/FilmProtos$Person;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    move v1, v2

    .line 145
    goto/16 :goto_0

    .line 147
    :cond_f
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->song:[Lcom/google/android/videos/proto/FilmProtos$Song;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Film;->song:[Lcom/google/android/videos/proto/FilmProtos$Song;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_10

    move v1, v2

    .line 149
    goto/16 :goto_0

    .line 151
    :cond_10
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->googlePlayUrl:Ljava/lang/String;

    if-nez v3, :cond_11

    .line 152
    iget-object v3, v0, Lcom/google/android/videos/proto/FilmProtos$Film;->googlePlayUrl:Ljava/lang/String;

    if-eqz v3, :cond_12

    move v1, v2

    .line 153
    goto/16 :goto_0

    .line 155
    :cond_11
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->googlePlayUrl:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Film;->googlePlayUrl:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_12

    move v1, v2

    .line 156
    goto/16 :goto_0

    .line 158
    :cond_12
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->referencedFilm:[Lcom/google/android/videos/proto/FilmProtos$Film;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Film;->referencedFilm:[Lcom/google/android/videos/proto/FilmProtos$Film;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_13

    move v1, v2

    .line 160
    goto/16 :goto_0

    .line 162
    :cond_13
    iget-boolean v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->isTv:Z

    iget-boolean v4, v0, Lcom/google/android/videos/proto/FilmProtos$Film;->isTv:Z

    if-eq v3, v4, :cond_14

    move v1, v2

    .line 163
    goto/16 :goto_0

    .line 165
    :cond_14
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->endDate:Ljava/lang/String;

    if-nez v3, :cond_15

    .line 166
    iget-object v3, v0, Lcom/google/android/videos/proto/FilmProtos$Film;->endDate:Ljava/lang/String;

    if-eqz v3, :cond_16

    move v1, v2

    .line 167
    goto/16 :goto_0

    .line 169
    :cond_15
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->endDate:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Film;->endDate:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_16

    move v1, v2

    .line 170
    goto/16 :goto_0

    .line 172
    :cond_16
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->album:[Lcom/google/android/videos/proto/FilmProtos$Album;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Film;->album:[Lcom/google/android/videos/proto/FilmProtos$Album;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 174
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 181
    const/16 v0, 0x11

    .line 182
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->mid:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 184
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->id:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 186
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->title:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    add-int v0, v3, v1

    .line 188
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->releaseDate:Ljava/lang/String;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    add-int v0, v3, v1

    .line 190
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->runtimeMinutes:I

    add-int v0, v1, v3

    .line 191
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    if-nez v1, :cond_4

    move v1, v2

    :goto_4
    add-int v0, v3, v1

    .line 193
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->actor:[Lcom/google/android/videos/proto/FilmProtos$Person;

    invoke-static {v3}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v3

    add-int v0, v1, v3

    .line 195
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->director:[Lcom/google/android/videos/proto/FilmProtos$Person;

    invoke-static {v3}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v3

    add-int v0, v1, v3

    .line 197
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->song:[Lcom/google/android/videos/proto/FilmProtos$Song;

    invoke-static {v3}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v3

    add-int v0, v1, v3

    .line 199
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->googlePlayUrl:Ljava/lang/String;

    if-nez v1, :cond_5

    move v1, v2

    :goto_5
    add-int v0, v3, v1

    .line 201
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->referencedFilm:[Lcom/google/android/videos/proto/FilmProtos$Film;

    invoke-static {v3}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v3

    add-int v0, v1, v3

    .line 203
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->isTv:Z

    if-eqz v1, :cond_6

    const/16 v1, 0x4cf

    :goto_6
    add-int v0, v3, v1

    .line 204
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->endDate:Ljava/lang/String;

    if-nez v3, :cond_7

    :goto_7
    add-int v0, v1, v2

    .line 206
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->album:[Lcom/google/android/videos/proto/FilmProtos$Album;

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 208
    return v0

    .line 182
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->mid:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 184
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->id:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 186
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->title:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    .line 188
    :cond_3
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->releaseDate:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    .line 191
    :cond_4
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    invoke-virtual {v1}, Lcom/google/android/videos/proto/FilmProtos$Image;->hashCode()I

    move-result v1

    goto :goto_4

    .line 199
    :cond_5
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->googlePlayUrl:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    .line 203
    :cond_6
    const/16 v1, 0x4d5

    goto :goto_6

    .line 204
    :cond_7
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->endDate:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_7
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/FilmProtos$Film;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 376
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 377
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 381
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 382
    :sswitch_0
    return-object p0

    .line 387
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->mid:Ljava/lang/String;

    goto :goto_0

    .line 391
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->id:Ljava/lang/String;

    goto :goto_0

    .line 395
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->title:Ljava/lang/String;

    goto :goto_0

    .line 399
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->releaseDate:Ljava/lang/String;

    goto :goto_0

    .line 403
    :sswitch_5
    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    if-nez v5, :cond_1

    .line 404
    new-instance v5, Lcom/google/android/videos/proto/FilmProtos$Image;

    invoke-direct {v5}, Lcom/google/android/videos/proto/FilmProtos$Image;-><init>()V

    iput-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    .line 406
    :cond_1
    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 410
    :sswitch_6
    const/16 v5, 0x32

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 412
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->actor:[Lcom/google/android/videos/proto/FilmProtos$Person;

    if-nez v5, :cond_3

    move v1, v4

    .line 413
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/videos/proto/FilmProtos$Person;

    .line 415
    .local v2, "newArray":[Lcom/google/android/videos/proto/FilmProtos$Person;
    if-eqz v1, :cond_2

    .line 416
    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->actor:[Lcom/google/android/videos/proto/FilmProtos$Person;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 418
    :cond_2
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_4

    .line 419
    new-instance v5, Lcom/google/android/videos/proto/FilmProtos$Person;

    invoke-direct {v5}, Lcom/google/android/videos/proto/FilmProtos$Person;-><init>()V

    aput-object v5, v2, v1

    .line 420
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 421
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 418
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 412
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/videos/proto/FilmProtos$Person;
    :cond_3
    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->actor:[Lcom/google/android/videos/proto/FilmProtos$Person;

    array-length v1, v5

    goto :goto_1

    .line 424
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/videos/proto/FilmProtos$Person;
    :cond_4
    new-instance v5, Lcom/google/android/videos/proto/FilmProtos$Person;

    invoke-direct {v5}, Lcom/google/android/videos/proto/FilmProtos$Person;-><init>()V

    aput-object v5, v2, v1

    .line 425
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 426
    iput-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->actor:[Lcom/google/android/videos/proto/FilmProtos$Person;

    goto :goto_0

    .line 430
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/videos/proto/FilmProtos$Person;
    :sswitch_7
    const/16 v5, 0x3a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 432
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->director:[Lcom/google/android/videos/proto/FilmProtos$Person;

    if-nez v5, :cond_6

    move v1, v4

    .line 433
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/videos/proto/FilmProtos$Person;

    .line 435
    .restart local v2    # "newArray":[Lcom/google/android/videos/proto/FilmProtos$Person;
    if-eqz v1, :cond_5

    .line 436
    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->director:[Lcom/google/android/videos/proto/FilmProtos$Person;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 438
    :cond_5
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_7

    .line 439
    new-instance v5, Lcom/google/android/videos/proto/FilmProtos$Person;

    invoke-direct {v5}, Lcom/google/android/videos/proto/FilmProtos$Person;-><init>()V

    aput-object v5, v2, v1

    .line 440
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 441
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 438
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 432
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/videos/proto/FilmProtos$Person;
    :cond_6
    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->director:[Lcom/google/android/videos/proto/FilmProtos$Person;

    array-length v1, v5

    goto :goto_3

    .line 444
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/videos/proto/FilmProtos$Person;
    :cond_7
    new-instance v5, Lcom/google/android/videos/proto/FilmProtos$Person;

    invoke-direct {v5}, Lcom/google/android/videos/proto/FilmProtos$Person;-><init>()V

    aput-object v5, v2, v1

    .line 445
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 446
    iput-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->director:[Lcom/google/android/videos/proto/FilmProtos$Person;

    goto/16 :goto_0

    .line 450
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/videos/proto/FilmProtos$Person;
    :sswitch_8
    const/16 v5, 0x42

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 452
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->song:[Lcom/google/android/videos/proto/FilmProtos$Song;

    if-nez v5, :cond_9

    move v1, v4

    .line 453
    .restart local v1    # "i":I
    :goto_5
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/videos/proto/FilmProtos$Song;

    .line 455
    .local v2, "newArray":[Lcom/google/android/videos/proto/FilmProtos$Song;
    if-eqz v1, :cond_8

    .line 456
    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->song:[Lcom/google/android/videos/proto/FilmProtos$Song;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 458
    :cond_8
    :goto_6
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_a

    .line 459
    new-instance v5, Lcom/google/android/videos/proto/FilmProtos$Song;

    invoke-direct {v5}, Lcom/google/android/videos/proto/FilmProtos$Song;-><init>()V

    aput-object v5, v2, v1

    .line 460
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 461
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 458
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 452
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/videos/proto/FilmProtos$Song;
    :cond_9
    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->song:[Lcom/google/android/videos/proto/FilmProtos$Song;

    array-length v1, v5

    goto :goto_5

    .line 464
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/videos/proto/FilmProtos$Song;
    :cond_a
    new-instance v5, Lcom/google/android/videos/proto/FilmProtos$Song;

    invoke-direct {v5}, Lcom/google/android/videos/proto/FilmProtos$Song;-><init>()V

    aput-object v5, v2, v1

    .line 465
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 466
    iput-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->song:[Lcom/google/android/videos/proto/FilmProtos$Song;

    goto/16 :goto_0

    .line 470
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/videos/proto/FilmProtos$Song;
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->googlePlayUrl:Ljava/lang/String;

    goto/16 :goto_0

    .line 474
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v5

    iput v5, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->runtimeMinutes:I

    goto/16 :goto_0

    .line 478
    :sswitch_b
    const/16 v5, 0x62

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 480
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->referencedFilm:[Lcom/google/android/videos/proto/FilmProtos$Film;

    if-nez v5, :cond_c

    move v1, v4

    .line 481
    .restart local v1    # "i":I
    :goto_7
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/videos/proto/FilmProtos$Film;

    .line 483
    .local v2, "newArray":[Lcom/google/android/videos/proto/FilmProtos$Film;
    if-eqz v1, :cond_b

    .line 484
    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->referencedFilm:[Lcom/google/android/videos/proto/FilmProtos$Film;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 486
    :cond_b
    :goto_8
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_d

    .line 487
    new-instance v5, Lcom/google/android/videos/proto/FilmProtos$Film;

    invoke-direct {v5}, Lcom/google/android/videos/proto/FilmProtos$Film;-><init>()V

    aput-object v5, v2, v1

    .line 488
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 489
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 486
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 480
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/videos/proto/FilmProtos$Film;
    :cond_c
    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->referencedFilm:[Lcom/google/android/videos/proto/FilmProtos$Film;

    array-length v1, v5

    goto :goto_7

    .line 492
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/videos/proto/FilmProtos$Film;
    :cond_d
    new-instance v5, Lcom/google/android/videos/proto/FilmProtos$Film;

    invoke-direct {v5}, Lcom/google/android/videos/proto/FilmProtos$Film;-><init>()V

    aput-object v5, v2, v1

    .line 493
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 494
    iput-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->referencedFilm:[Lcom/google/android/videos/proto/FilmProtos$Film;

    goto/16 :goto_0

    .line 498
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/videos/proto/FilmProtos$Film;
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->isTv:Z

    goto/16 :goto_0

    .line 502
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->endDate:Ljava/lang/String;

    goto/16 :goto_0

    .line 506
    :sswitch_e
    const/16 v5, 0x7a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 508
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->album:[Lcom/google/android/videos/proto/FilmProtos$Album;

    if-nez v5, :cond_f

    move v1, v4

    .line 509
    .restart local v1    # "i":I
    :goto_9
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/videos/proto/FilmProtos$Album;

    .line 511
    .local v2, "newArray":[Lcom/google/android/videos/proto/FilmProtos$Album;
    if-eqz v1, :cond_e

    .line 512
    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->album:[Lcom/google/android/videos/proto/FilmProtos$Album;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 514
    :cond_e
    :goto_a
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_10

    .line 515
    new-instance v5, Lcom/google/android/videos/proto/FilmProtos$Album;

    invoke-direct {v5}, Lcom/google/android/videos/proto/FilmProtos$Album;-><init>()V

    aput-object v5, v2, v1

    .line 516
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 517
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 514
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    .line 508
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/videos/proto/FilmProtos$Album;
    :cond_f
    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->album:[Lcom/google/android/videos/proto/FilmProtos$Album;

    array-length v1, v5

    goto :goto_9

    .line 520
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/videos/proto/FilmProtos$Album;
    :cond_10
    new-instance v5, Lcom/google/android/videos/proto/FilmProtos$Album;

    invoke-direct {v5}, Lcom/google/android/videos/proto/FilmProtos$Album;-><init>()V

    aput-object v5, v2, v1

    .line 521
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 522
    iput-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->album:[Lcom/google/android/videos/proto/FilmProtos$Album;

    goto/16 :goto_0

    .line 377
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x62 -> :sswitch_b
        0x68 -> :sswitch_c
        0x72 -> :sswitch_d
        0x7a -> :sswitch_e
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/videos/proto/FilmProtos$Film;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/FilmProtos$Film;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 214
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->mid:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 215
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->mid:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 217
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->id:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 218
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->id:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 220
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->title:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 221
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->title:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 223
    :cond_2
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->releaseDate:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 224
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->releaseDate:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 226
    :cond_3
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    if-eqz v2, :cond_4

    .line 227
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 229
    :cond_4
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->actor:[Lcom/google/android/videos/proto/FilmProtos$Person;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->actor:[Lcom/google/android/videos/proto/FilmProtos$Person;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 230
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->actor:[Lcom/google/android/videos/proto/FilmProtos$Person;

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 231
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->actor:[Lcom/google/android/videos/proto/FilmProtos$Person;

    aget-object v0, v2, v1

    .line 232
    .local v0, "element":Lcom/google/android/videos/proto/FilmProtos$Person;
    if-eqz v0, :cond_5

    .line 233
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 230
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 237
    .end local v0    # "element":Lcom/google/android/videos/proto/FilmProtos$Person;
    .end local v1    # "i":I
    :cond_6
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->director:[Lcom/google/android/videos/proto/FilmProtos$Person;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->director:[Lcom/google/android/videos/proto/FilmProtos$Person;

    array-length v2, v2

    if-lez v2, :cond_8

    .line 238
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->director:[Lcom/google/android/videos/proto/FilmProtos$Person;

    array-length v2, v2

    if-ge v1, v2, :cond_8

    .line 239
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->director:[Lcom/google/android/videos/proto/FilmProtos$Person;

    aget-object v0, v2, v1

    .line 240
    .restart local v0    # "element":Lcom/google/android/videos/proto/FilmProtos$Person;
    if-eqz v0, :cond_7

    .line 241
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 238
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 245
    .end local v0    # "element":Lcom/google/android/videos/proto/FilmProtos$Person;
    .end local v1    # "i":I
    :cond_8
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->song:[Lcom/google/android/videos/proto/FilmProtos$Song;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->song:[Lcom/google/android/videos/proto/FilmProtos$Song;

    array-length v2, v2

    if-lez v2, :cond_a

    .line 246
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->song:[Lcom/google/android/videos/proto/FilmProtos$Song;

    array-length v2, v2

    if-ge v1, v2, :cond_a

    .line 247
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->song:[Lcom/google/android/videos/proto/FilmProtos$Song;

    aget-object v0, v2, v1

    .line 248
    .local v0, "element":Lcom/google/android/videos/proto/FilmProtos$Song;
    if-eqz v0, :cond_9

    .line 249
    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 246
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 253
    .end local v0    # "element":Lcom/google/android/videos/proto/FilmProtos$Song;
    .end local v1    # "i":I
    :cond_a
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->googlePlayUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 254
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->googlePlayUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 256
    :cond_b
    iget v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->runtimeMinutes:I

    if-eqz v2, :cond_c

    .line 257
    const/16 v2, 0xa

    iget v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->runtimeMinutes:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 259
    :cond_c
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->referencedFilm:[Lcom/google/android/videos/proto/FilmProtos$Film;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->referencedFilm:[Lcom/google/android/videos/proto/FilmProtos$Film;

    array-length v2, v2

    if-lez v2, :cond_e

    .line 260
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->referencedFilm:[Lcom/google/android/videos/proto/FilmProtos$Film;

    array-length v2, v2

    if-ge v1, v2, :cond_e

    .line 261
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->referencedFilm:[Lcom/google/android/videos/proto/FilmProtos$Film;

    aget-object v0, v2, v1

    .line 262
    .local v0, "element":Lcom/google/android/videos/proto/FilmProtos$Film;
    if-eqz v0, :cond_d

    .line 263
    const/16 v2, 0xc

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 260
    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 267
    .end local v0    # "element":Lcom/google/android/videos/proto/FilmProtos$Film;
    .end local v1    # "i":I
    :cond_e
    iget-boolean v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->isTv:Z

    if-eqz v2, :cond_f

    .line 268
    const/16 v2, 0xd

    iget-boolean v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->isTv:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 270
    :cond_f
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->endDate:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    .line 271
    const/16 v2, 0xe

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->endDate:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 273
    :cond_10
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->album:[Lcom/google/android/videos/proto/FilmProtos$Album;

    if-eqz v2, :cond_12

    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->album:[Lcom/google/android/videos/proto/FilmProtos$Album;

    array-length v2, v2

    if-lez v2, :cond_12

    .line 274
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_4
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->album:[Lcom/google/android/videos/proto/FilmProtos$Album;

    array-length v2, v2

    if-ge v1, v2, :cond_12

    .line 275
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Film;->album:[Lcom/google/android/videos/proto/FilmProtos$Album;

    aget-object v0, v2, v1

    .line 276
    .local v0, "element":Lcom/google/android/videos/proto/FilmProtos$Album;
    if-eqz v0, :cond_11

    .line 277
    const/16 v2, 0xf

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 274
    :cond_11
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 281
    .end local v0    # "element":Lcom/google/android/videos/proto/FilmProtos$Album;
    .end local v1    # "i":I
    :cond_12
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 282
    return-void
.end method
