.class Lcom/google/android/videos/drm/DrmManagerV8;
.super Lcom/google/android/videos/drm/DrmManager;
.source "DrmManagerV8.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/drm/DrmManagerV8$1;,
        Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;,
        Lcom/google/android/videos/drm/DrmManagerV8$DrmListener;
    }
.end annotation


# static fields
.field private static final INITIALIZE_EVENT_TYPES:[Lcom/widevine/drmapi/android/WVEvent;

.field private static final LICENSE_EVENT_TYPES:[Lcom/widevine/drmapi/android/WVEvent;

.field private static final QUERY_EVENT_TYPES:[Lcom/widevine/drmapi/android/WVEvent;

.field private static final REGISTER_EVENT_TYPES:[Lcom/widevine/drmapi/android/WVEvent;


# instance fields
.field private final context:Landroid/content/Context;

.field private final deviceId:Ljava/lang/String;

.field private final drmListener:Lcom/google/android/videos/drm/DrmManagerV8$DrmListener;

.field private final eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private fatalInitializationError:Z

.field private frameworkDrmError:I

.field private frameworkDrmLevel:I

.field private final globalCredentials:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final gservicesId:J

.field private initializationStatus:Lcom/widevine/drmapi/android/WVStatus;

.field private final pendingResponseQueue:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final requestLock:Ljava/lang/Object;

.field private final wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 40
    new-array v0, v4, [Lcom/widevine/drmapi/android/WVEvent;

    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->Initialized:Lcom/widevine/drmapi/android/WVEvent;

    aput-object v1, v0, v2

    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->InitializeFailed:Lcom/widevine/drmapi/android/WVEvent;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/videos/drm/DrmManagerV8;->INITIALIZE_EVENT_TYPES:[Lcom/widevine/drmapi/android/WVEvent;

    .line 42
    new-array v0, v4, [Lcom/widevine/drmapi/android/WVEvent;

    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->LicenseReceived:Lcom/widevine/drmapi/android/WVEvent;

    aput-object v1, v0, v2

    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->LicenseRequestFailed:Lcom/widevine/drmapi/android/WVEvent;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/videos/drm/DrmManagerV8;->LICENSE_EVENT_TYPES:[Lcom/widevine/drmapi/android/WVEvent;

    .line 44
    new-array v0, v3, [Lcom/widevine/drmapi/android/WVEvent;

    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->Registered:Lcom/widevine/drmapi/android/WVEvent;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/videos/drm/DrmManagerV8;->REGISTER_EVENT_TYPES:[Lcom/widevine/drmapi/android/WVEvent;

    .line 45
    new-array v0, v3, [Lcom/widevine/drmapi/android/WVEvent;

    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->QueryStatus:Lcom/widevine/drmapi/android/WVEvent;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/videos/drm/DrmManagerV8;->QUERY_EVENT_TYPES:[Lcom/widevine/drmapi/android/WVEvent;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/Config;Lcom/google/android/videos/logging/EventLogger;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "networkExecutor"    # Ljava/util/concurrent/Executor;
    .param p3, "drmExecutor"    # Ljava/util/concurrent/Executor;
    .param p4, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .param p5, "config"    # Lcom/google/android/videos/Config;
    .param p6, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;

    .prologue
    .line 68
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/videos/drm/DrmManager;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/videos/accounts/AccountManagerWrapper;)V

    .line 69
    iput-object p1, p0, Lcom/google/android/videos/drm/DrmManagerV8;->context:Landroid/content/Context;

    .line 70
    iput-object p6, p0, Lcom/google/android/videos/drm/DrmManagerV8;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 71
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 72
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/google/android/videos/drm/DrmManagerV8;->requestLock:Ljava/lang/Object;

    .line 73
    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/drm/DrmManagerV8;->deviceId:Ljava/lang/String;

    .line 74
    invoke-interface {p5}, Lcom/google/android/videos/Config;->gservicesId()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/videos/drm/DrmManagerV8;->gservicesId:J

    .line 75
    new-instance v1, Lcom/widevine/drmapi/android/WVPlayback;

    invoke-direct {v1}, Lcom/widevine/drmapi/android/WVPlayback;-><init>()V

    iput-object v1, p0, Lcom/google/android/videos/drm/DrmManagerV8;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    .line 76
    new-instance v1, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v1, p0, Lcom/google/android/videos/drm/DrmManagerV8;->pendingResponseQueue:Ljava/util/concurrent/BlockingQueue;

    .line 77
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/videos/drm/DrmManagerV8;->frameworkDrmLevel:I

    .line 79
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/videos/drm/DrmManagerV8;->globalCredentials:Ljava/util/HashMap;

    .line 80
    iget-object v1, p0, Lcom/google/android/videos/drm/DrmManagerV8;->globalCredentials:Ljava/util/HashMap;

    const-string v2, "WVDRMServer"

    invoke-interface {p5}, Lcom/google/android/videos/Config;->wvClassicDrmServerUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    iget-object v1, p0, Lcom/google/android/videos/drm/DrmManagerV8;->globalCredentials:Ljava/util/HashMap;

    const-string v2, "WVPortalKey"

    invoke-interface {p5}, Lcom/google/android/videos/Config;->wvPortalName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    iget-object v1, p0, Lcom/google/android/videos/drm/DrmManagerV8;->globalCredentials:Ljava/util/HashMap;

    const-string v2, "WVDeviceIDKey"

    iget-object v3, p0, Lcom/google/android/videos/drm/DrmManagerV8;->deviceId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    iget-object v1, p0, Lcom/google/android/videos/drm/DrmManagerV8;->globalCredentials:Ljava/util/HashMap;

    const-string v2, "WVAssetRootKey"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    iget-object v1, p0, Lcom/google/android/videos/drm/DrmManagerV8;->globalCredentials:Ljava/util/HashMap;

    const-string v2, "WVAssetDBPathKey"

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    new-instance v1, Lcom/google/android/videos/drm/DrmManagerV8$DrmListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/videos/drm/DrmManagerV8$DrmListener;-><init>(Lcom/google/android/videos/drm/DrmManagerV8;Lcom/google/android/videos/drm/DrmManagerV8$1;)V

    iput-object v1, p0, Lcom/google/android/videos/drm/DrmManagerV8;->drmListener:Lcom/google/android/videos/drm/DrmManagerV8$DrmListener;

    .line 87
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/videos/drm/DrmManagerV8;Lcom/widevine/drmapi/android/WVEvent;Ljava/util/HashMap;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/drm/DrmManagerV8;
    .param p1, "x1"    # Lcom/widevine/drmapi/android/WVEvent;
    .param p2, "x2"    # Ljava/util/HashMap;

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/drm/DrmManagerV8;->logEvent(Lcom/widevine/drmapi/android/WVEvent;Ljava/util/HashMap;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/videos/drm/DrmManagerV8;)Ljava/util/concurrent/BlockingQueue;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/drm/DrmManagerV8;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/videos/drm/DrmManagerV8;->pendingResponseQueue:Ljava/util/concurrent/BlockingQueue;

    return-object v0
.end method

.method private getError(Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;)Lcom/google/android/videos/drm/DrmException;
    .locals 4
    .param p1, "response"    # Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;

    .prologue
    .line 311
    iget-object v2, p1, Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;->attributes:Ljava/util/HashMap;

    const-string v3, "WVErrorKey"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/android/videos/drm/DrmManagerV8;->parseErrorCode(Ljava/lang/String;)I

    move-result v1

    .line 312
    .local v1, "errorCode":I
    invoke-virtual {p0, v1}, Lcom/google/android/videos/drm/DrmManagerV8;->getDrmError(I)Lcom/google/android/videos/drm/DrmException$DrmError;

    move-result-object v0

    .line 313
    .local v0, "drmError":Lcom/google/android/videos/drm/DrmException$DrmError;
    new-instance v2, Lcom/google/android/videos/drm/DrmException;

    invoke-direct {v2, v0, v1}, Lcom/google/android/videos/drm/DrmException;-><init>(Lcom/google/android/videos/drm/DrmException$DrmError;I)V

    return-object v2
.end method

.method private getLicense(Ljava/lang/String;Lcom/google/android/videos/drm/DrmManager$Identifiers;Z)Lcom/google/android/videos/drm/DrmResponse;
    .locals 11
    .param p1, "assetUri"    # Ljava/lang/String;
    .param p2, "ids"    # Lcom/google/android/videos/drm/DrmManager$Identifiers;
    .param p3, "refreshed"    # Z

    .prologue
    const/4 v0, 0x0

    .line 164
    invoke-direct {p0}, Lcom/google/android/videos/drm/DrmManagerV8;->initialize()V

    .line 165
    iget-object v1, p0, Lcom/google/android/videos/drm/DrmManagerV8;->initializationStatus:Lcom/widevine/drmapi/android/WVStatus;

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/videos/drm/DrmManagerV8;->isDisabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 186
    :cond_0
    :goto_0
    return-object v0

    .line 170
    :cond_1
    iget-object v10, p0, Lcom/google/android/videos/drm/DrmManagerV8;->requestLock:Ljava/lang/Object;

    monitor-enter v10

    .line 171
    if-eqz p2, :cond_2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/videos/drm/DrmManagerV8;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    iget-wide v2, p2, Lcom/google/android/videos/drm/DrmManager$Identifiers;->systemId:J

    iget-wide v4, p2, Lcom/google/android/videos/drm/DrmManager$Identifiers;->assetId:J

    iget-wide v6, p2, Lcom/google/android/videos/drm/DrmManager$Identifiers;->keyId:J

    invoke-virtual/range {v1 .. v7}, Lcom/widevine/drmapi/android/WVPlayback;->queryAssetStatus(JJJ)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v9

    .line 174
    .local v9, "status":Lcom/widevine/drmapi/android/WVStatus;
    :goto_1
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-eq v9, v1, :cond_3

    .line 175
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Widevine queryAssetStatus failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 176
    monitor-exit v10

    goto :goto_0

    .line 180
    .end local v9    # "status":Lcom/widevine/drmapi/android/WVStatus;
    :catchall_0
    move-exception v1

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 171
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/google/android/videos/drm/DrmManagerV8;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    invoke-virtual {v1, p1}, Lcom/widevine/drmapi/android/WVPlayback;->queryAssetStatus(Ljava/lang/String;)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v9

    goto :goto_1

    .line 178
    .restart local v9    # "status":Lcom/widevine/drmapi/android/WVStatus;
    :cond_3
    if-eqz p2, :cond_4

    sget-object v1, Lcom/google/android/videos/drm/DrmManagerV8;->QUERY_EVENT_TYPES:[Lcom/widevine/drmapi/android/WVEvent;

    invoke-direct {p0, p2, v1}, Lcom/google/android/videos/drm/DrmManagerV8;->getResponseForIdentifiers(Lcom/google/android/videos/drm/DrmManager$Identifiers;[Lcom/widevine/drmapi/android/WVEvent;)Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;

    move-result-object v8

    .line 180
    .local v8, "response":Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;
    :goto_2
    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 182
    iget-object v1, v8, Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;->attributes:Ljava/util/HashMap;

    invoke-direct {p0, v1, p3}, Lcom/google/android/videos/drm/DrmManagerV8;->parseLicense(Ljava/util/HashMap;Z)Lcom/google/android/videos/drm/DrmResponse;

    move-result-object v0

    .line 183
    .local v0, "license":Lcom/google/android/videos/drm/DrmResponse;
    if-nez v0, :cond_0

    .line 184
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No license for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 178
    .end local v0    # "license":Lcom/google/android/videos/drm/DrmResponse;
    .end local v8    # "response":Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;
    :cond_4
    :try_start_2
    sget-object v1, Lcom/google/android/videos/drm/DrmManagerV8;->QUERY_EVENT_TYPES:[Lcom/widevine/drmapi/android/WVEvent;

    invoke-direct {p0, p1, v1}, Lcom/google/android/videos/drm/DrmManagerV8;->getResponseForAssetUri(Ljava/lang/String;[Lcom/widevine/drmapi/android/WVEvent;)Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v8

    goto :goto_2
.end method

.method private getRemainingTime(Ljava/util/HashMap;)J
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 334
    .local p1, "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v0, "WVPurchaseDurationRemainingKey"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-string v0, "WVLicenseDurationRemainingKey"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method private getResponse(Ljava/util/Map;[Lcom/widevine/drmapi/android/WVEvent;)Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;
    .locals 11
    .param p2, "eventTypes"    # [Lcom/widevine/drmapi/android/WVEvent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;[",
            "Lcom/widevine/drmapi/android/WVEvent;",
            ")",
            "Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;"
        }
    .end annotation

    .prologue
    .line 367
    .local p1, "expectedAttributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 368
    .local v3, "eventSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/widevine/drmapi/android/WVEvent;>;"
    invoke-static {v3, p2}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 369
    const/4 v8, 0x0

    .line 370
    .local v8, "next":Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;
    const/4 v7, 0x0

    .line 371
    .local v7, "matchedEvent":Z
    :cond_0
    :goto_0
    if-nez v7, :cond_5

    .line 373
    :try_start_0
    iget-object v9, p0, Lcom/google/android/videos/drm/DrmManagerV8;->pendingResponseQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v9}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v9

    move-object v0, v9

    check-cast v0, Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;

    move-object v8, v0

    .line 375
    const/4 v1, 0x1

    .line 376
    .local v1, "attributesMatch":Z
    if-eqz p1, :cond_3

    .line 377
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 378
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 379
    .local v6, "key":Ljava/lang/String;
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    .line 380
    .local v4, "expectedValue":Ljava/lang/Object;
    iget-object v9, v8, Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;->attributes:Ljava/util/HashMap;

    invoke-virtual {v9, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    iget-object v9, v8, Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;->attributes:Ljava/util/HashMap;

    invoke-virtual {v9, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 382
    :cond_2
    const/4 v1, 0x0

    .line 388
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v4    # "expectedValue":Ljava/lang/Object;
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "key":Ljava/lang/String;
    :cond_3
    if-eqz v1, :cond_4

    iget-object v9, v8, Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;->event:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v3, v9}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    const/4 v7, 0x1

    .line 389
    :goto_1
    if-nez v7, :cond_0

    .line 390
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Discarding unexpected event from WV library of type: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v8, Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;->event:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 392
    .end local v1    # "attributesMatch":Z
    :catch_0
    move-exception v9

    goto :goto_0

    .line 388
    .restart local v1    # "attributesMatch":Z
    :cond_4
    const/4 v7, 0x0

    goto :goto_1

    .line 396
    .end local v1    # "attributesMatch":Z
    :cond_5
    return-object v8
.end method

.method private getResponseForAssetUri(Ljava/lang/String;[Lcom/widevine/drmapi/android/WVEvent;)Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;
    .locals 2
    .param p1, "assetPath"    # Ljava/lang/String;
    .param p2, "eventTypes"    # [Lcom/widevine/drmapi/android/WVEvent;

    .prologue
    .line 348
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 349
    .local v0, "expectedAttributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v1, "WVAssetPathKey"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    invoke-direct {p0, v0, p2}, Lcom/google/android/videos/drm/DrmManagerV8;->getResponse(Ljava/util/Map;[Lcom/widevine/drmapi/android/WVEvent;)Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;

    move-result-object v1

    return-object v1
.end method

.method private getResponseForIdentifiers(Lcom/google/android/videos/drm/DrmManager$Identifiers;[Lcom/widevine/drmapi/android/WVEvent;)Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;
    .locals 4
    .param p1, "ids"    # Lcom/google/android/videos/drm/DrmManager$Identifiers;
    .param p2, "eventTypes"    # [Lcom/widevine/drmapi/android/WVEvent;

    .prologue
    .line 359
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 360
    .local v0, "expectedAttributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v1, "WVKeyIDKey"

    iget-wide v2, p1, Lcom/google/android/videos/drm/DrmManager$Identifiers;->keyId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 361
    const-string v1, "WVAssetIDKey"

    iget-wide v2, p1, Lcom/google/android/videos/drm/DrmManager$Identifiers;->assetId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 362
    const-string v1, "WVSystemIDKey"

    iget-wide v2, p1, Lcom/google/android/videos/drm/DrmManager$Identifiers;->systemId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 363
    invoke-direct {p0, v0, p2}, Lcom/google/android/videos/drm/DrmManagerV8;->getResponse(Ljava/util/Map;[Lcom/widevine/drmapi/android/WVEvent;)Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;

    move-result-object v1

    return-object v1
.end method

.method private getSuccess(Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;)Z
    .locals 3
    .param p1, "response"    # Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;

    .prologue
    .line 317
    iget-object v0, p1, Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;->attributes:Ljava/util/HashMap;

    const-string v1, "WVErrorKey"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v1, "ok"

    iget-object v0, p1, Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;->attributes:Ljava/util/HashMap;

    const-string v2, "WVErrorKey"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private initialize()V
    .locals 11

    .prologue
    const/4 v4, 0x0

    .line 90
    iget-object v5, p0, Lcom/google/android/videos/drm/DrmManagerV8;->initializationStatus:Lcom/widevine/drmapi/android/WVStatus;

    sget-object v6, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-eq v5, v6, :cond_0

    iget-boolean v5, p0, Lcom/google/android/videos/drm/DrmManagerV8;->fatalInitializationError:Z

    if-eqz v5, :cond_1

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    iget-object v6, p0, Lcom/google/android/videos/drm/DrmManagerV8;->requestLock:Ljava/lang/Object;

    monitor-enter v6

    .line 99
    :try_start_0
    iget-object v5, p0, Lcom/google/android/videos/drm/DrmManagerV8;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    iget-object v7, p0, Lcom/google/android/videos/drm/DrmManagerV8;->context:Landroid/content/Context;

    iget-object v8, p0, Lcom/google/android/videos/drm/DrmManagerV8;->globalCredentials:Ljava/util/HashMap;

    iget-object v9, p0, Lcom/google/android/videos/drm/DrmManagerV8;->drmListener:Lcom/google/android/videos/drm/DrmManagerV8$DrmListener;

    invoke-virtual {v5, v7, v8, v9}, Lcom/widevine/drmapi/android/WVPlayback;->initialize(Landroid/content/Context;Ljava/util/HashMap;Lcom/widevine/drmapi/android/WVEventListener;)Lcom/widevine/drmapi/android/WVStatus;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 107
    .local v3, "status":Lcom/widevine/drmapi/android/WVStatus;
    :try_start_1
    sget-object v5, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-eq v3, v5, :cond_2

    .line 108
    iput-object v3, p0, Lcom/google/android/videos/drm/DrmManagerV8;->initializationStatus:Lcom/widevine/drmapi/android/WVStatus;

    .line 109
    iget-object v5, p0, Lcom/google/android/videos/drm/DrmManagerV8;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget-object v7, p0, Lcom/google/android/videos/drm/DrmManagerV8;->initializationStatus:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v7}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v7

    const/4 v8, 0x0

    iget v9, p0, Lcom/google/android/videos/drm/DrmManagerV8;->frameworkDrmLevel:I

    iget v10, p0, Lcom/google/android/videos/drm/DrmManagerV8;->frameworkDrmError:I

    invoke-interface {v5, v7, v8, v9, v10}, Lcom/google/android/videos/logging/EventLogger;->onAppDrmInitFailed(IIII)V

    .line 111
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Widevine initialization failed (sync): "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v7, p0, Lcom/google/android/videos/drm/DrmManagerV8;->initializationStatus:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 112
    iget-object v5, p0, Lcom/google/android/videos/drm/DrmManagerV8;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    invoke-virtual {v5}, Lcom/widevine/drmapi/android/WVPlayback;->terminate()Lcom/widevine/drmapi/android/WVStatus;

    .line 113
    monitor-exit v6

    goto :goto_0

    .line 116
    .end local v3    # "status":Lcom/widevine/drmapi/android/WVStatus;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5

    .line 100
    :catch_0
    move-exception v1

    .line 101
    .local v1, "e":Ljava/lang/Throwable;
    :try_start_2
    iget-object v5, p0, Lcom/google/android/videos/drm/DrmManagerV8;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    const/4 v7, 0x0

    const/4 v8, 0x0

    iget v9, p0, Lcom/google/android/videos/drm/DrmManagerV8;->frameworkDrmLevel:I

    iget v10, p0, Lcom/google/android/videos/drm/DrmManagerV8;->frameworkDrmError:I

    invoke-interface {v5, v7, v8, v9, v10}, Lcom/google/android/videos/logging/EventLogger;->onAppDrmInitFailed(IIII)V

    .line 102
    const-string v5, "Can\'t load native drm library"

    invoke-static {v5, v1}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 103
    sget-object v5, Lcom/widevine/drmapi/android/WVStatus;->NotInitialized:Lcom/widevine/drmapi/android/WVStatus;

    iput-object v5, p0, Lcom/google/android/videos/drm/DrmManagerV8;->initializationStatus:Lcom/widevine/drmapi/android/WVStatus;

    .line 104
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/videos/drm/DrmManagerV8;->fatalInitializationError:Z

    .line 105
    monitor-exit v6

    goto :goto_0

    .line 115
    .end local v1    # "e":Ljava/lang/Throwable;
    .restart local v3    # "status":Lcom/widevine/drmapi/android/WVStatus;
    :cond_2
    const/4 v5, 0x0

    sget-object v7, Lcom/google/android/videos/drm/DrmManagerV8;->INITIALIZE_EVENT_TYPES:[Lcom/widevine/drmapi/android/WVEvent;

    invoke-direct {p0, v5, v7}, Lcom/google/android/videos/drm/DrmManagerV8;->getResponse(Ljava/util/Map;[Lcom/widevine/drmapi/android/WVEvent;)Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;

    move-result-object v2

    .line 116
    .local v2, "response":Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 118
    iget-object v0, v2, Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;->attributes:Ljava/util/HashMap;

    .line 119
    .local v0, "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v5, "WVStatusKey"

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/widevine/drmapi/android/WVStatus;

    iput-object v5, p0, Lcom/google/android/videos/drm/DrmManagerV8;->initializationStatus:Lcom/widevine/drmapi/android/WVStatus;

    .line 120
    iget-object v5, p0, Lcom/google/android/videos/drm/DrmManagerV8;->initializationStatus:Lcom/widevine/drmapi/android/WVStatus;

    sget-object v6, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-eq v5, v6, :cond_5

    .line 121
    const-string v5, "WVErrorKey"

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    const-string v5, "WVErrorKey"

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 123
    .local v4, "wvError":Ljava/lang/String;
    :cond_3
    iget-object v5, p0, Lcom/google/android/videos/drm/DrmManagerV8;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget-object v6, p0, Lcom/google/android/videos/drm/DrmManagerV8;->initializationStatus:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v6}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v6

    invoke-virtual {p0, v4}, Lcom/google/android/videos/drm/DrmManagerV8;->parseErrorCode(Ljava/lang/String;)I

    move-result v7

    iget v8, p0, Lcom/google/android/videos/drm/DrmManagerV8;->frameworkDrmLevel:I

    iget v9, p0, Lcom/google/android/videos/drm/DrmManagerV8;->frameworkDrmError:I

    invoke-interface {v5, v6, v7, v8, v9}, Lcom/google/android/videos/logging/EventLogger;->onAppDrmInitFailed(IIII)V

    .line 125
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Widevine initialization failed (async): "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/videos/drm/DrmManagerV8;->initializationStatus:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-eqz v4, :cond_4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_1
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 127
    iget-object v5, p0, Lcom/google/android/videos/drm/DrmManagerV8;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    invoke-virtual {v5}, Lcom/widevine/drmapi/android/WVPlayback;->terminate()Lcom/widevine/drmapi/android/WVStatus;

    goto/16 :goto_0

    .line 125
    :cond_4
    const-string v5, ""

    goto :goto_1

    .line 129
    .end local v4    # "wvError":Ljava/lang/String;
    :cond_5
    iget-object v5, p0, Lcom/google/android/videos/drm/DrmManagerV8;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget v6, p0, Lcom/google/android/videos/drm/DrmManagerV8;->frameworkDrmLevel:I

    iget v7, p0, Lcom/google/android/videos/drm/DrmManagerV8;->frameworkDrmError:I

    invoke-interface {v5, v6, v7}, Lcom/google/android/videos/logging/EventLogger;->onAppDrmInit(II)V

    goto/16 :goto_0
.end method

.method private logEvent(Lcom/widevine/drmapi/android/WVEvent;Ljava/util/HashMap;)V
    .locals 4
    .param p1, "eventType"    # Lcom/widevine/drmapi/android/WVEvent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/widevine/drmapi/android/WVEvent;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 431
    .local p2, "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v2, "===================================="

    invoke-static {v2}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 432
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onEvent: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/widevine/drmapi/android/WVEvent;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 433
    invoke-virtual {p2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 434
    .local v1, "key":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 436
    .end local v1    # "key":Ljava/lang/String;
    :cond_0
    const-string v2, "===================================="

    invoke-static {v2}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 437
    return-void
.end method

.method private parseLicense(Ljava/util/HashMap;Z)Lcom/google/android/videos/drm/DrmResponse;
    .locals 20
    .param p2, "refreshed"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;Z)",
            "Lcom/google/android/videos/drm/DrmResponse;"
        }
    .end annotation

    .prologue
    .line 190
    .local p1, "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v4, "WVStatusKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    sget-object v5, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-ne v4, v5, :cond_0

    const/4 v15, 0x1

    .line 191
    .local v15, "haveLicense":Z
    :goto_0
    if-nez v15, :cond_1

    .line 192
    const/4 v5, 0x0

    .line 205
    :goto_1
    return-object v5

    .line 190
    .end local v15    # "haveLicense":Z
    :cond_0
    const/4 v15, 0x0

    goto :goto_0

    .line 195
    .restart local v15    # "haveLicense":Z
    :cond_1
    const-string v4, "WVLicenseTypeKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v16

    .line 196
    .local v16, "mode":I
    and-int/lit8 v4, v16, 0x1

    if-eqz v4, :cond_2

    const/4 v14, 0x1

    .line 197
    .local v14, "allowsStreaming":Z
    :goto_2
    and-int/lit8 v4, v16, 0x2

    if-eqz v4, :cond_3

    const/4 v2, 0x1

    .line 199
    .local v2, "allowsOffline":Z
    :goto_3
    const-string v4, "WVPlaybackElapsedTimeKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    .line 201
    .local v18, "secondsSinceActivation":J
    new-instance v3, Lcom/google/android/videos/drm/DrmManager$Identifiers;

    const-string v4, "WVKeyIDKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-string v6, "WVAssetIDKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-string v8, "WVSystemIDKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-direct/range {v3 .. v9}, Lcom/google/android/videos/drm/DrmManager$Identifiers;-><init>(JJJ)V

    .line 205
    .local v3, "ids":Lcom/google/android/videos/drm/DrmManager$Identifiers;
    new-instance v5, Lcom/google/android/videos/drm/DrmResponse;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct/range {p0 .. p1}, Lcom/google/android/videos/drm/DrmManagerV8;->getRemainingTime(Ljava/util/HashMap;)J

    move-result-wide v8

    long-to-int v10, v8

    move-wide/from16 v0, v18

    long-to-int v11, v0

    move v8, v14

    move v9, v2

    move-object v12, v3

    move/from16 v13, p2

    invoke-direct/range {v5 .. v13}, Lcom/google/android/videos/drm/DrmResponse;-><init>(JZZIILcom/google/android/videos/drm/DrmManager$Identifiers;Z)V

    goto :goto_1

    .line 196
    .end local v2    # "allowsOffline":Z
    .end local v3    # "ids":Lcom/google/android/videos/drm/DrmManager$Identifiers;
    .end local v14    # "allowsStreaming":Z
    .end local v18    # "secondsSinceActivation":J
    :cond_2
    const/4 v14, 0x0

    goto :goto_2

    .line 197
    .restart local v14    # "allowsStreaming":Z
    :cond_3
    const/4 v2, 0x0

    goto :goto_3
.end method

.method private toErrorCode(Lcom/widevine/drmapi/android/WVStatus;)I
    .locals 1
    .param p1, "status"    # Lcom/widevine/drmapi/android/WVStatus;

    .prologue
    .line 339
    invoke-virtual {p1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v0

    add-int/lit16 v0, v0, 0x384

    return v0
.end method


# virtual methods
.method public getDrmLevel()I
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x1

    return v0
.end method

.method public getPlayableUri(Landroid/net/Uri;Z)Landroid/net/Uri;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "appLevelDrm"    # Z

    .prologue
    .line 323
    const-string v2, "appLevelDrm must be true"

    invoke-static {p2, v2}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;)V

    .line 324
    invoke-virtual {p0, p1}, Lcom/google/android/videos/drm/DrmManagerV8;->stripQueryParameters(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 325
    .local v0, "assetUri":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/videos/drm/DrmManagerV8;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    invoke-virtual {v2, v0}, Lcom/widevine/drmapi/android/WVPlayback;->play(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 330
    .local v1, "decryptedUri":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto :goto_0
.end method

.method protected requestOfflineRights(Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/async/Callback;)V
    .locals 5
    .param p1, "request"    # Lcom/google/android/videos/drm/DrmRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/drm/DrmRequest;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/drm/DrmRequest;",
            "Lcom/google/android/videos/drm/DrmResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/drm/DrmResponse;>;"
    const/4 v4, 0x0

    .line 146
    iget-object v2, p1, Lcom/google/android/videos/drm/DrmRequest;->stream:Lcom/google/android/videos/streams/MediaStream;

    iget-object v2, v2, Lcom/google/android/videos/streams/MediaStream;->uri:Landroid/net/Uri;

    invoke-virtual {p0, v2}, Lcom/google/android/videos/drm/DrmManagerV8;->stripQueryParameters(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 147
    .local v0, "assetUri":Ljava/lang/String;
    iget-object v2, p1, Lcom/google/android/videos/drm/DrmRequest;->ids:Lcom/google/android/videos/drm/DrmManager$Identifiers;

    invoke-direct {p0, v0, v2, v4}, Lcom/google/android/videos/drm/DrmManagerV8;->getLicense(Ljava/lang/String;Lcom/google/android/videos/drm/DrmManager$Identifiers;Z)Lcom/google/android/videos/drm/DrmResponse;

    move-result-object v1

    .line 148
    .local v1, "license":Lcom/google/android/videos/drm/DrmResponse;
    if-eqz v1, :cond_0

    .line 149
    iget-boolean v2, v1, Lcom/google/android/videos/drm/DrmResponse;->allowsOffline:Z

    if-nez v2, :cond_1

    .line 150
    const/4 v1, 0x0

    .line 156
    :cond_0
    iget-object v2, p1, Lcom/google/android/videos/drm/DrmRequest;->authToken:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 157
    new-instance v2, Lcom/google/android/videos/drm/DrmException;

    sget-object v3, Lcom/google/android/videos/drm/DrmException$DrmError;->NO_LICENSE:Lcom/google/android/videos/drm/DrmException$DrmError;

    invoke-direct {v2, v3, v4}, Lcom/google/android/videos/drm/DrmException;-><init>(Lcom/google/android/videos/drm/DrmException$DrmError;I)V

    invoke-interface {p2, p1, v2}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 161
    :goto_0
    return-void

    .line 151
    :cond_1
    iget-object v2, p1, Lcom/google/android/videos/drm/DrmRequest;->type:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    sget-object v3, Lcom/google/android/videos/drm/DrmRequest$RequestType;->OFFLINE:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    if-ne v2, v3, :cond_0

    .line 152
    invoke-interface {p2, p1, v1}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 159
    :cond_2
    invoke-virtual {p0, p1, v1, p2}, Lcom/google/android/videos/drm/DrmManagerV8;->requestRights(Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/drm/DrmResponse;Lcom/google/android/videos/async/Callback;)V

    goto :goto_0
.end method

.method protected requestRights(Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/drm/DrmResponse;Lcom/google/android/videos/async/Callback;)V
    .locals 20
    .param p1, "request"    # Lcom/google/android/videos/drm/DrmRequest;
    .param p2, "existingLicense"    # Lcom/google/android/videos/drm/DrmResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/drm/DrmRequest;",
            "Lcom/google/android/videos/drm/DrmResponse;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/drm/DrmRequest;",
            "Lcom/google/android/videos/drm/DrmResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 213
    .local p3, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/drm/DrmResponse;>;"
    invoke-direct/range {p0 .. p0}, Lcom/google/android/videos/drm/DrmManagerV8;->initialize()V

    .line 214
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/drm/DrmManagerV8;->isDisabled()Z

    move-result v14

    .line 215
    .local v14, "isDisabled":Z
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/drm/DrmManagerV8;->initializationStatus:Lcom/widevine/drmapi/android/WVStatus;

    sget-object v4, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-ne v3, v4, :cond_0

    if-eqz v14, :cond_2

    .line 216
    :cond_0
    if-eqz v14, :cond_1

    sget-object v11, Lcom/google/android/videos/drm/DrmException$DrmError;->ROOTED_DEVICE:Lcom/google/android/videos/drm/DrmException$DrmError;

    .line 217
    .local v11, "drmError":Lcom/google/android/videos/drm/DrmException$DrmError;
    :goto_0
    new-instance v3, Lcom/google/android/videos/drm/DrmException;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/drm/DrmManagerV8;->initializationStatus:Lcom/widevine/drmapi/android/WVStatus;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/videos/drm/DrmManagerV8;->toErrorCode(Lcom/widevine/drmapi/android/WVStatus;)I

    move-result v4

    invoke-direct {v3, v11, v4}, Lcom/google/android/videos/drm/DrmException;-><init>(Lcom/google/android/videos/drm/DrmException$DrmError;I)V

    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v3}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 308
    .end local v11    # "drmError":Lcom/google/android/videos/drm/DrmException$DrmError;
    :goto_1
    return-void

    .line 216
    :cond_1
    sget-object v11, Lcom/google/android/videos/drm/DrmException$DrmError;->UNKNOWN:Lcom/google/android/videos/drm/DrmException$DrmError;

    goto :goto_0

    .line 221
    :cond_2
    new-instance v10, Ljava/util/HashMap;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/drm/DrmManagerV8;->globalCredentials:Ljava/util/HashMap;

    invoke-direct {v10, v3}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 222
    .local v10, "credentials":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v4, "WVLicenseTypeKey"

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/videos/drm/DrmRequest;->type:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    iget-boolean v3, v3, Lcom/google/android/videos/drm/DrmRequest$RequestType;->isOffline:Z

    if-eqz v3, :cond_6

    const/4 v3, 0x3

    :goto_2
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v4, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/videos/drm/DrmRequest;->playbackId:Ljava/lang/String;

    if-eqz v3, :cond_3

    .line 225
    const-string v3, "WVStreamIDKey"

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/videos/drm/DrmRequest;->playbackId:Ljava/lang/String;

    invoke-virtual {v10, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    :cond_3
    new-instance v18, Ljava/lang/StringBuilder;

    const-string v3, "v=2"

    move-object/from16 v0, v18

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 228
    .local v18, "userData":Ljava/lang/StringBuilder;
    const-string v3, "&videoid="

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/videos/drm/DrmRequest;->videoId:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 229
    const-string v3, "&aid="

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/videos/drm/DrmManagerV8;->gservicesId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 230
    const-string v3, "&root="

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/drm/DrmManagerV8;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    invoke-virtual {v4}, Lcom/widevine/drmapi/android/WVPlayback;->isRooted()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 231
    const-string v3, "&oauth="

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/videos/drm/DrmRequest;->authToken:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 232
    if-eqz p2, :cond_4

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/drm/DrmResponse;->isActivated()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 233
    const-string v3, "&time_since_started="

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    iget v4, v0, Lcom/google/android/videos/drm/DrmResponse;->secondsSinceActivation:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 235
    :cond_4
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/videos/drm/DrmRequest;->type:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    sget-object v4, Lcom/google/android/videos/drm/DrmRequest$RequestType;->OFFLINE_UNPIN:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    if-ne v3, v4, :cond_5

    .line 236
    const-string v3, "&unpin=true"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 239
    :cond_5
    const-string v3, "WVCAUserDataKey"

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/drm/DrmManagerV8;->requestLock:Ljava/lang/Object;

    move-object/from16 v19, v0

    monitor-enter v19

    .line 242
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/drm/DrmManagerV8;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    invoke-virtual {v3, v10}, Lcom/widevine/drmapi/android/WVPlayback;->setCredentials(Ljava/util/HashMap;)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v17

    .line 243
    .local v17, "status":Lcom/widevine/drmapi/android/WVStatus;
    sget-object v3, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    move-object/from16 v0, v17

    if-eq v0, v3, :cond_7

    .line 244
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Widevine setCredentials failed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 245
    new-instance v3, Lcom/google/android/videos/drm/DrmException;

    sget-object v4, Lcom/google/android/videos/drm/DrmException$DrmError;->UNKNOWN:Lcom/google/android/videos/drm/DrmException$DrmError;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/google/android/videos/drm/DrmManagerV8;->toErrorCode(Lcom/widevine/drmapi/android/WVStatus;)I

    move-result v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/videos/drm/DrmException;-><init>(Lcom/google/android/videos/drm/DrmException$DrmError;I)V

    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v3}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 246
    monitor-exit v19

    goto/16 :goto_1

    .line 307
    .end local v17    # "status":Lcom/widevine/drmapi/android/WVStatus;
    :catchall_0
    move-exception v3

    monitor-exit v19
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 222
    .end local v18    # "userData":Ljava/lang/StringBuilder;
    :cond_6
    const/4 v3, 0x1

    goto/16 :goto_2

    .line 249
    .restart local v17    # "status":Lcom/widevine/drmapi/android/WVStatus;
    .restart local v18    # "userData":Ljava/lang/StringBuilder;
    :cond_7
    :try_start_1
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/videos/drm/DrmRequest;->stream:Lcom/google/android/videos/streams/MediaStream;

    iget-object v3, v3, Lcom/google/android/videos/streams/MediaStream;->uri:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/videos/drm/DrmManagerV8;->stripQueryParameters(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 250
    .local v2, "assetUri":Ljava/lang/String;
    const/16 v16, 0x0

    .line 251
    .local v16, "response":Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/drm/DrmManagerV8;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    invoke-virtual {v3, v2}, Lcom/widevine/drmapi/android/WVPlayback;->registerAsset(Ljava/lang/String;)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v17

    .line 252
    sget-object v3, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    move-object/from16 v0, v17

    if-eq v0, v3, :cond_8

    .line 253
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Widevine registerAsset failed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 254
    new-instance v3, Lcom/google/android/videos/drm/DrmException;

    sget-object v4, Lcom/google/android/videos/drm/DrmException$DrmError;->UNKNOWN:Lcom/google/android/videos/drm/DrmException$DrmError;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/google/android/videos/drm/DrmManagerV8;->toErrorCode(Lcom/widevine/drmapi/android/WVStatus;)I

    move-result v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/videos/drm/DrmException;-><init>(Lcom/google/android/videos/drm/DrmException$DrmError;I)V

    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v3}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 255
    monitor-exit v19

    goto/16 :goto_1

    .line 258
    :cond_8
    sget-object v3, Lcom/google/android/videos/drm/DrmManagerV8;->REGISTER_EVENT_TYPES:[Lcom/widevine/drmapi/android/WVEvent;

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/google/android/videos/drm/DrmManagerV8;->getResponseForAssetUri(Ljava/lang/String;[Lcom/widevine/drmapi/android/WVEvent;)Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;

    move-result-object v16

    .line 260
    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/google/android/videos/drm/DrmRequest;->ids:Lcom/google/android/videos/drm/DrmManager$Identifiers;

    .line 262
    .local v13, "ids":Lcom/google/android/videos/drm/DrmManager$Identifiers;
    if-eqz v13, :cond_9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/drm/DrmManagerV8;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    iget-wide v4, v13, Lcom/google/android/videos/drm/DrmManager$Identifiers;->systemId:J

    iget-wide v6, v13, Lcom/google/android/videos/drm/DrmManager$Identifiers;->assetId:J

    iget-wide v8, v13, Lcom/google/android/videos/drm/DrmManager$Identifiers;->keyId:J

    invoke-virtual/range {v3 .. v9}, Lcom/widevine/drmapi/android/WVPlayback;->requestLicense(JJJ)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v17

    .line 265
    :goto_3
    sget-object v3, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    move-object/from16 v0, v17

    if-eq v0, v3, :cond_a

    .line 266
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Widevine requestLicense failed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 267
    new-instance v3, Lcom/google/android/videos/drm/DrmException;

    sget-object v4, Lcom/google/android/videos/drm/DrmException$DrmError;->UNKNOWN:Lcom/google/android/videos/drm/DrmException$DrmError;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/google/android/videos/drm/DrmManagerV8;->toErrorCode(Lcom/widevine/drmapi/android/WVStatus;)I

    move-result v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/videos/drm/DrmException;-><init>(Lcom/google/android/videos/drm/DrmException$DrmError;I)V

    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v3}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 268
    monitor-exit v19

    goto/16 :goto_1

    .line 262
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/drm/DrmManagerV8;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    invoke-virtual {v3, v2}, Lcom/widevine/drmapi/android/WVPlayback;->requestLicense(Ljava/lang/String;)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v17

    goto :goto_3

    .line 271
    :cond_a
    if-eqz v13, :cond_b

    sget-object v3, Lcom/google/android/videos/drm/DrmManagerV8;->LICENSE_EVENT_TYPES:[Lcom/widevine/drmapi/android/WVEvent;

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v3}, Lcom/google/android/videos/drm/DrmManagerV8;->getResponseForIdentifiers(Lcom/google/android/videos/drm/DrmManager$Identifiers;[Lcom/widevine/drmapi/android/WVEvent;)Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;

    move-result-object v16

    .line 275
    :goto_4
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/videos/drm/DrmRequest;->type:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    iget-boolean v3, v3, Lcom/google/android/videos/drm/DrmRequest$RequestType;->isOffline:Z

    if-eqz v3, :cond_c

    .line 278
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/videos/drm/DrmRequest;->ids:Lcom/google/android/videos/drm/DrmManager$Identifiers;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/google/android/videos/drm/DrmManagerV8;->getSuccess(Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;)Z

    move-result v4

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/videos/drm/DrmManagerV8;->getLicense(Ljava/lang/String;Lcom/google/android/videos/drm/DrmManager$Identifiers;Z)Lcom/google/android/videos/drm/DrmResponse;

    move-result-object v15

    .line 284
    .local v15, "license":Lcom/google/android/videos/drm/DrmResponse;
    :goto_5
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/google/android/videos/drm/DrmManagerV8;->getError(Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;)Lcom/google/android/videos/drm/DrmException;

    move-result-object v12

    .line 285
    .local v12, "error":Lcom/google/android/videos/drm/DrmException;
    iget-object v3, v12, Lcom/google/android/videos/drm/DrmException;->drmError:Lcom/google/android/videos/drm/DrmException$DrmError;

    sget-object v4, Lcom/google/android/videos/drm/DrmException$DrmError;->AUTHENTICATION_FAILED:Lcom/google/android/videos/drm/DrmException$DrmError;

    if-ne v3, v4, :cond_d

    .line 287
    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v12}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 307
    :goto_6
    monitor-exit v19

    goto/16 :goto_1

    .line 271
    .end local v12    # "error":Lcom/google/android/videos/drm/DrmException;
    .end local v15    # "license":Lcom/google/android/videos/drm/DrmResponse;
    :cond_b
    sget-object v3, Lcom/google/android/videos/drm/DrmManagerV8;->LICENSE_EVENT_TYPES:[Lcom/widevine/drmapi/android/WVEvent;

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/google/android/videos/drm/DrmManagerV8;->getResponseForAssetUri(Ljava/lang/String;[Lcom/widevine/drmapi/android/WVEvent;)Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;

    move-result-object v16

    goto :goto_4

    .line 281
    :cond_c
    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;->attributes:Ljava/util/HashMap;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/google/android/videos/drm/DrmManagerV8;->getSuccess(Lcom/google/android/videos/drm/DrmManagerV8$WVResponse;)Z

    move-result v4

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/google/android/videos/drm/DrmManagerV8;->parseLicense(Ljava/util/HashMap;Z)Lcom/google/android/videos/drm/DrmResponse;

    move-result-object v15

    .restart local v15    # "license":Lcom/google/android/videos/drm/DrmResponse;
    goto :goto_5

    .line 288
    .restart local v12    # "error":Lcom/google/android/videos/drm/DrmException;
    :cond_d
    if-eqz v15, :cond_10

    .line 289
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/videos/drm/DrmRequest;->type:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    sget-object v4, Lcom/google/android/videos/drm/DrmRequest$RequestType;->OFFLINE_UNPIN:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    if-eq v3, v4, :cond_e

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/videos/drm/DrmRequest;->type:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    iget-boolean v3, v3, Lcom/google/android/videos/drm/DrmRequest$RequestType;->isOffline:Z

    if-eqz v3, :cond_f

    iget-boolean v3, v15, Lcom/google/android/videos/drm/DrmResponse;->allowsOffline:Z

    if-nez v3, :cond_f

    .line 293
    :cond_e
    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v12}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_6

    .line 295
    :cond_f
    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v15}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_6

    .line 298
    :cond_10
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/videos/drm/DrmRequest;->type:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    sget-object v4, Lcom/google/android/videos/drm/DrmRequest$RequestType;->OFFLINE_UNPIN:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    if-ne v3, v4, :cond_12

    iget-object v3, v12, Lcom/google/android/videos/drm/DrmException;->drmError:Lcom/google/android/videos/drm/DrmException$DrmError;

    sget-object v4, Lcom/google/android/videos/drm/DrmException$DrmError;->UNPIN_SUCCESSFUL:Lcom/google/android/videos/drm/DrmException$DrmError;

    if-eq v3, v4, :cond_11

    iget-object v3, v12, Lcom/google/android/videos/drm/DrmException;->drmError:Lcom/google/android/videos/drm/DrmException$DrmError;

    sget-object v4, Lcom/google/android/videos/drm/DrmException$DrmError;->NO_LICENSE:Lcom/google/android/videos/drm/DrmException$DrmError;

    if-ne v3, v4, :cond_12

    .line 302
    :cond_11
    const/4 v3, 0x0

    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v3}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_6

    .line 304
    :cond_12
    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v12}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_6
.end method

.method public setFrameworkDrmFallbackInfo(II)V
    .locals 0
    .param p1, "frameworkDrmLevel"    # I
    .param p2, "frameworkDrmError"    # I

    .prologue
    .line 134
    iput p1, p0, Lcom/google/android/videos/drm/DrmManagerV8;->frameworkDrmLevel:I

    .line 135
    iput p2, p0, Lcom/google/android/videos/drm/DrmManagerV8;->frameworkDrmError:I

    .line 136
    return-void
.end method
