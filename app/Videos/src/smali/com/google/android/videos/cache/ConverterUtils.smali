.class Lcom/google/android/videos/cache/ConverterUtils;
.super Ljava/lang/Object;
.source "ConverterUtils.java"


# direct methods
.method public static readBytes(Ljava/io/InputStream;J)Lcom/google/android/videos/utils/ByteArray;
    .locals 7
    .param p0, "in"    # Ljava/io/InputStream;
    .param p1, "length"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    const-wide/32 v4, 0x7fffffff

    cmp-long v4, p1, v4

    if-lez v4, :cond_0

    .line 46
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Byte array size too large"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 48
    :cond_0
    long-to-int v2, p1

    .line 49
    .local v2, "dataLength":I
    new-instance v0, Lcom/google/android/videos/utils/ByteArray;

    invoke-direct {v0, v2}, Lcom/google/android/videos/utils/ByteArray;-><init>(I)V

    .line 50
    .local v0, "byteArray":Lcom/google/android/videos/utils/ByteArray;
    const/4 v1, 0x0

    .line 51
    .local v1, "bytesRead":I
    :goto_0
    int-to-long v4, v1

    cmp-long v4, v4, p1

    if-gez v4, :cond_2

    .line 52
    iget-object v4, v0, Lcom/google/android/videos/utils/ByteArray;->data:[B

    sub-int v5, v2, v1

    invoke-virtual {p0, v4, v1, v5}, Ljava/io/InputStream;->read([BII)I

    move-result v3

    .line 53
    .local v3, "thisRead":I
    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    .line 54
    new-instance v4, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unexpected end of data ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 56
    :cond_1
    add-int/2addr v1, v3

    .line 57
    goto :goto_0

    .line 58
    .end local v3    # "thisRead":I
    :cond_2
    return-object v0
.end method

.method public static readInt(Ljava/io/InputStream;)I
    .locals 6
    .param p0, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 24
    .local v0, "ch1":I
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 25
    .local v1, "ch2":I
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v2

    .line 26
    .local v2, "ch3":I
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v3

    .line 27
    .local v3, "ch4":I
    or-int v4, v0, v1

    or-int/2addr v4, v2

    or-int/2addr v4, v3

    if-gez v4, :cond_0

    .line 28
    new-instance v4, Ljava/io/EOFException;

    invoke-direct {v4}, Ljava/io/EOFException;-><init>()V

    throw v4

    .line 30
    :cond_0
    shl-int/lit8 v4, v0, 0x18

    shl-int/lit8 v5, v1, 0x10

    or-int/2addr v4, v5

    shl-int/lit8 v5, v2, 0x8

    or-int/2addr v4, v5

    or-int/2addr v4, v3

    return v4
.end method

.method public static readLong(Ljava/io/InputStream;)J
    .locals 8
    .param p0, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39
    invoke-static {p0}, Lcom/google/android/videos/cache/ConverterUtils;->readInt(Ljava/io/InputStream;)I

    move-result v4

    int-to-long v0, v4

    .line 40
    .local v0, "i1":J
    invoke-static {p0}, Lcom/google/android/videos/cache/ConverterUtils;->readInt(Ljava/io/InputStream;)I

    move-result v4

    int-to-long v4, v4

    const-wide v6, 0xffffffffL

    and-long v2, v4, v6

    .line 41
    .local v2, "i2":J
    const/16 v4, 0x20

    shl-long v4, v0, v4

    or-long/2addr v4, v2

    return-wide v4
.end method

.method public static writeInt(Ljava/io/OutputStream;I)V
    .locals 1
    .param p0, "out"    # Ljava/io/OutputStream;
    .param p1, "value"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 16
    ushr-int/lit8 v0, p1, 0x18

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write(I)V

    .line 17
    ushr-int/lit8 v0, p1, 0x10

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write(I)V

    .line 18
    ushr-int/lit8 v0, p1, 0x8

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write(I)V

    .line 19
    invoke-virtual {p0, p1}, Ljava/io/OutputStream;->write(I)V

    .line 20
    return-void
.end method

.method public static writeLong(Ljava/io/OutputStream;J)V
    .locals 3
    .param p0, "out"    # Ljava/io/OutputStream;
    .param p1, "value"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 34
    const/16 v0, 0x20

    ushr-long v0, p1, v0

    long-to-int v0, v0

    invoke-static {p0, v0}, Lcom/google/android/videos/cache/ConverterUtils;->writeInt(Ljava/io/OutputStream;I)V

    .line 35
    long-to-int v0, p1

    invoke-static {p0, v0}, Lcom/google/android/videos/cache/ConverterUtils;->writeInt(Ljava/io/OutputStream;I)V

    .line 36
    return-void
.end method
