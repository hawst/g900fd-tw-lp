.class public Lcom/google/android/videos/player/exo/ExoPlayerUtil;
.super Ljava/lang/Object;
.source "ExoPlayerUtil.java"


# direct methods
.method public static buildDownloadHttpDataSource(Lcom/google/android/videos/VideosGlobals;)Lcom/google/android/exoplayer/upstream/DataSource;
    .locals 9
    .param p0, "globals"    # Lcom/google/android/videos/VideosGlobals;

    .prologue
    const/4 v2, 0x0

    .line 42
    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v8

    .line 43
    .local v8, "config":Lcom/google/android/videos/Config;
    new-instance v0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobals;->getUserAgent()Ljava/lang/String;

    move-result-object v1

    const/4 v6, -0x1

    invoke-interface {v8}, Lcom/google/android/videos/Config;->exoAlternateRedirectEnabled()Z

    move-result v7

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/player/exo/VideosHttpDataSource;-><init>(Ljava/lang/String;Lcom/google/android/exoplayer/util/Predicate;Lcom/google/android/exoplayer/upstream/TransferListener;Landroid/os/Handler;Lcom/google/android/videos/player/exo/VideosHttpDataSource$VideosHttpDataSourceListener;IZ)V

    return-object v0
.end method

.method public static buildPlaybackHttpDataSource(Lcom/google/android/videos/VideosGlobals;Lcom/google/android/exoplayer/upstream/DefaultBandwidthMeter;Lcom/google/android/videos/player/exo/VideosHttpDataSource$VideosHttpDataSourceListener;Landroid/os/Handler;)Lcom/google/android/exoplayer/upstream/DataSource;
    .locals 9
    .param p0, "globals"    # Lcom/google/android/videos/VideosGlobals;
    .param p1, "bandwidthMeter"    # Lcom/google/android/exoplayer/upstream/DefaultBandwidthMeter;
    .param p2, "listener"    # Lcom/google/android/videos/player/exo/VideosHttpDataSource$VideosHttpDataSourceListener;
    .param p3, "handler"    # Landroid/os/Handler;

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v8

    .line 51
    .local v8, "config":Lcom/google/android/videos/Config;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobals;->getUserAgent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ExoPlayerLib/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "1.0.13"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 52
    .local v1, "userAgent":Ljava/lang/String;
    new-instance v0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;

    const/4 v2, 0x0

    invoke-interface {v8}, Lcom/google/android/videos/Config;->exoLoadTimeoutMs()I

    move-result v6

    invoke-interface {v8}, Lcom/google/android/videos/Config;->exoAlternateRedirectEnabled()Z

    move-result v7

    move-object v3, p1

    move-object v4, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/player/exo/VideosHttpDataSource;-><init>(Ljava/lang/String;Lcom/google/android/exoplayer/util/Predicate;Lcom/google/android/exoplayer/upstream/TransferListener;Landroid/os/Handler;Lcom/google/android/videos/player/exo/VideosHttpDataSource$VideosHttpDataSourceListener;IZ)V

    return-object v0
.end method

.method public static prepareExtractor(Lcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;Lcom/google/android/videos/streams/MediaStream;)Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    .locals 16
    .param p0, "dataSource"    # Lcom/google/android/exoplayer/upstream/DataSource;
    .param p1, "representation"    # Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    .param p2, "stream"    # Lcom/google/android/videos/streams/MediaStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/util/concurrent/CancellationException;
        }
    .end annotation

    .prologue
    .line 72
    new-instance v12, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;

    invoke-direct {v12}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;-><init>()V

    .line 73
    .local v12, "extractor":Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    new-instance v2, Lcom/google/android/exoplayer/upstream/DataSpec;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->uri:Landroid/net/Uri;

    const-wide/16 v4, 0x0

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-wide v6, v6, Lcom/google/android/videos/proto/StreamInfo;->dashIndexEnd:J

    const-wide/16 v14, 0x1

    add-long/2addr v6, v14

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->getCacheKey()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v2 .. v8}, Lcom/google/android/exoplayer/upstream/DataSpec;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    .line 75
    .local v2, "dataSpec":Lcom/google/android/exoplayer/upstream/DataSpec;
    new-instance v9, Lcom/google/android/exoplayer/upstream/BufferPool;

    iget-wide v4, v2, Lcom/google/android/exoplayer/upstream/DataSpec;->length:J

    long-to-int v3, v4

    invoke-direct {v9, v3}, Lcom/google/android/exoplayer/upstream/BufferPool;-><init>(I)V

    .line 76
    .local v9, "allocator":Lcom/google/android/exoplayer/upstream/Allocator;
    new-instance v10, Lcom/google/android/exoplayer/upstream/DataSourceStream;

    move-object/from16 v0, p0

    invoke-direct {v10, v0, v2, v9}, Lcom/google/android/exoplayer/upstream/DataSourceStream;-><init>(Lcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/exoplayer/upstream/DataSpec;Lcom/google/android/exoplayer/upstream/Allocator;)V

    .line 79
    .local v10, "currentHeaderStream":Lcom/google/android/exoplayer/upstream/DataSourceStream;
    :try_start_0
    invoke-virtual {v10}, Lcom/google/android/exoplayer/upstream/DataSourceStream;->load()V

    .line 80
    const/4 v3, 0x0

    invoke-virtual {v12, v10, v3}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->read(Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;Lcom/google/android/exoplayer/SampleHolder;)I

    move-result v13

    .line 81
    .local v13, "result":I
    const/16 v3, 0x1a

    if-eq v13, v3, :cond_0

    .line 82
    new-instance v3, Lcom/google/android/exoplayer/ParserException;

    const-string v4, "Invalid initialization data"

    invoke-direct {v3, v4}, Lcom/google/android/exoplayer/ParserException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    .end local v13    # "result":I
    :catch_0
    move-exception v11

    .line 85
    .local v11, "e":Ljava/lang/InterruptedException;
    :try_start_1
    new-instance v3, Ljava/util/concurrent/CancellationException;

    invoke-direct {v3}, Ljava/util/concurrent/CancellationException;-><init>()V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87
    .end local v11    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v3

    invoke-virtual {v10}, Lcom/google/android/exoplayer/upstream/DataSourceStream;->close()V

    throw v3

    .restart local v13    # "result":I
    :cond_0
    invoke-virtual {v10}, Lcom/google/android/exoplayer/upstream/DataSourceStream;->close()V

    .line 89
    return-object v12
.end method

.method public static toRepresentation(Lcom/google/android/videos/streams/MediaStream;Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    .locals 31
    .param p0, "stream"    # Lcom/google/android/videos/streams/MediaStream;
    .param p1, "mimeType"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "durationMs"    # J

    .prologue
    .line 58
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    .line 59
    .local v10, "itagInfo":Lcom/google/android/videos/streams/ItagInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    move-object/from16 v30, v0

    .line 60
    .local v30, "streamInfo":Lcom/google/android/videos/proto/StreamInfo;
    move-object/from16 v0, v30

    iget-wide v4, v0, Lcom/google/android/videos/proto/StreamInfo;->sizeInBytes:J

    const-wide/16 v6, 0x1f40

    mul-long/2addr v4, v6

    div-long v4, v4, p3

    long-to-int v9, v4

    .line 61
    .local v9, "bitrate":I
    new-instance v2, Lcom/google/android/exoplayer/chunk/Format;

    move-object/from16 v0, v30

    iget v3, v0, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget v5, v10, Lcom/google/android/videos/streams/ItagInfo;->width:I

    iget v6, v10, Lcom/google/android/videos/streams/ItagInfo;->height:I

    iget v7, v10, Lcom/google/android/videos/streams/ItagInfo;->audioChannels:I

    const/4 v8, 0x0

    move-object/from16 v4, p1

    invoke-direct/range {v2 .. v9}, Lcom/google/android/exoplayer/chunk/Format;-><init>(Ljava/lang/String;Ljava/lang/String;IIIII)V

    .line 63
    .local v2, "format":Lcom/google/android/exoplayer/chunk/Format;
    const-wide/16 v11, 0x0

    move-object/from16 v0, v30

    iget-wide v0, v0, Lcom/google/android/videos/proto/StreamInfo;->lastModifiedTimestamp:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/streams/MediaStream;->uri:Landroid/net/Uri;

    move-object/from16 v19, v0

    move-object/from16 v0, v30

    iget-wide v0, v0, Lcom/google/android/videos/proto/StreamInfo;->dashInitStart:J

    move-wide/from16 v20, v0

    move-object/from16 v0, v30

    iget-wide v0, v0, Lcom/google/android/videos/proto/StreamInfo;->dashInitEnd:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v30

    iget-wide v0, v0, Lcom/google/android/videos/proto/StreamInfo;->dashIndexStart:J

    move-wide/from16 v24, v0

    move-object/from16 v0, v30

    iget-wide v0, v0, Lcom/google/android/videos/proto/StreamInfo;->dashIndexEnd:J

    move-wide/from16 v26, v0

    move-object/from16 v0, v30

    iget-wide v0, v0, Lcom/google/android/videos/proto/StreamInfo;->sizeInBytes:J

    move-wide/from16 v28, v0

    move-wide/from16 v13, p3

    move-object/from16 v15, p2

    move-object/from16 v18, v2

    invoke-static/range {v11 .. v29}, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->newInstance(JJLjava/lang/String;JLcom/google/android/exoplayer/chunk/Format;Landroid/net/Uri;JJJJJ)Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    move-result-object v3

    return-object v3
.end method
