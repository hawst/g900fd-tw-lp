.class Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$1;
.super Ljava/lang/Object;
.source "WidevineDrmSessionManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->open(Ljava/util/Map;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;

.field final synthetic val$mimeType:Ljava/lang/String;

.field final synthetic val$psshData:Ljava/util/Map;


# direct methods
.method constructor <init>(Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;Ljava/util/Map;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$1;->this$0:Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;

    iput-object p2, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$1;->val$psshData:Ljava/util/Map;

    iput-object p3, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$1;->val$mimeType:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$1;->this$0:Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;

    # getter for: Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->videosGlobals:Lcom/google/android/videos/VideosGlobals;
    invoke-static {v0}, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->access$300(Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$1;->this$0:Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;

    # getter for: Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->account:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->access$400(Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$1;->this$0:Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;

    # getter for: Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->videoId:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->access$500(Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$1;->val$psshData:Ljava/util/Map;

    sget-object v4, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->WIDEVINE_UUID:Ljava/util/UUID;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iget-object v4, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$1;->val$mimeType:Ljava/lang/String;

    invoke-static {v1, v2, v3, v0, v4}, Lcom/google/android/videos/pinning/PinningDbHelper;->persistStreamingPsshData(Lcom/google/android/videos/store/Database;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;)Z

    .line 152
    return-void
.end method
