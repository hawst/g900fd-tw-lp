.class Lcom/google/android/videos/store/StoryboardClient$2;
.super Ljava/lang/Object;
.source "StoryboardClient.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/store/StoryboardClient;->saveOfflineStoryboard(Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/Storyboard;ILcom/google/android/videos/async/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/store/StoryboardClient;

.field final synthetic val$callback:Lcom/google/android/videos/async/Callback;

.field final synthetic val$storage:I

.field final synthetic val$storyboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

.field final synthetic val$videoId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/videos/store/StoryboardClient;Ljava/lang/String;ILcom/google/wireless/android/video/magma/proto/Storyboard;Lcom/google/android/videos/async/Callback;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/google/android/videos/store/StoryboardClient$2;->this$0:Lcom/google/android/videos/store/StoryboardClient;

    iput-object p2, p0, Lcom/google/android/videos/store/StoryboardClient$2;->val$videoId:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/videos/store/StoryboardClient$2;->val$storage:I

    iput-object p4, p0, Lcom/google/android/videos/store/StoryboardClient$2;->val$storyboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

    iput-object p5, p0, Lcom/google/android/videos/store/StoryboardClient$2;->val$callback:Lcom/google/android/videos/async/Callback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 96
    :try_start_0
    iget-object v1, p0, Lcom/google/android/videos/store/StoryboardClient$2;->this$0:Lcom/google/android/videos/store/StoryboardClient;

    # getter for: Lcom/google/android/videos/store/StoryboardClient;->storyboardStore:Lcom/google/android/videos/store/AbstractFileStore;
    invoke-static {v1}, Lcom/google/android/videos/store/StoryboardClient;->access$000(Lcom/google/android/videos/store/StoryboardClient;)Lcom/google/android/videos/store/AbstractFileStore;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/store/StoryboardClient$2;->val$videoId:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/videos/store/StoryboardClient$2;->val$storage:I

    iget-object v4, p0, Lcom/google/android/videos/store/StoryboardClient$2;->val$storyboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/videos/store/AbstractFileStore;->put(Ljava/lang/Object;ILjava/lang/Object;)V

    .line 97
    iget-object v1, p0, Lcom/google/android/videos/store/StoryboardClient$2;->val$callback:Lcom/google/android/videos/async/Callback;

    iget-object v2, p0, Lcom/google/android/videos/store/StoryboardClient$2;->val$videoId:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    :goto_0
    return-void

    .line 98
    :catch_0
    move-exception v0

    .line 99
    .local v0, "e":Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;
    iget-object v1, p0, Lcom/google/android/videos/store/StoryboardClient$2;->val$callback:Lcom/google/android/videos/async/Callback;

    iget-object v2, p0, Lcom/google/android/videos/store/StoryboardClient$2;->val$videoId:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method
