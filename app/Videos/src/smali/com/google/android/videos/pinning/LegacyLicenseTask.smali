.class abstract Lcom/google/android/videos/pinning/LegacyLicenseTask;
.super Lcom/google/android/videos/pinning/Task;
.source "LegacyLicenseTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/pinning/LegacyLicenseTask$1;,
        Lcom/google/android/videos/pinning/LegacyLicenseTask$LicenseInfoQuery;,
        Lcom/google/android/videos/pinning/LegacyLicenseTask$Release;,
        Lcom/google/android/videos/pinning/LegacyLicenseTask$Refresh;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/pinning/Task",
        "<",
        "Lcom/google/android/videos/pinning/DownloadKey;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

.field protected final database:Lcom/google/android/videos/store/Database;

.field private final drmManager:Lcom/google/android/videos/drm/DrmManager;

.field private final itagInfoStore:Lcom/google/android/videos/store/ItagInfoStore;

.field private final legacyDownloadsHaveAppLevelDrm:Z


# direct methods
.method private constructor <init>(ILcom/google/android/videos/pinning/DownloadKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/videos/pinning/Task$Listener;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/drm/DrmManager;Lcom/google/android/videos/accounts/AccountManagerWrapper;ZLcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/store/ItagInfoStore;)V
    .locals 8
    .param p1, "taskType"    # I
    .param p2, "key"    # Lcom/google/android/videos/pinning/DownloadKey;
    .param p3, "wakeLock"    # Landroid/os/PowerManager$WakeLock;
    .param p4, "wifiLock"    # Landroid/net/wifi/WifiManager$WifiLock;
    .param p5, "listener"    # Lcom/google/android/videos/pinning/Task$Listener;
    .param p6, "database"    # Lcom/google/android/videos/store/Database;
    .param p7, "drmManager"    # Lcom/google/android/videos/drm/DrmManager;
    .param p8, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .param p9, "legacyDownloadsHaveAppLevelDrm"    # Z
    .param p10, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;
    .param p11, "itagInfoStore"    # Lcom/google/android/videos/store/ItagInfoStore;

    .prologue
    .line 66
    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p10

    invoke-direct/range {v1 .. v7}, Lcom/google/android/videos/pinning/Task;-><init>(ILcom/google/android/videos/pinning/Task$Key;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/videos/pinning/Task$Listener;Lcom/google/android/videos/logging/EventLogger;)V

    .line 67
    invoke-static {p7}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/drm/DrmManager;

    iput-object v1, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask;->drmManager:Lcom/google/android/videos/drm/DrmManager;

    .line 68
    invoke-static {p6}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/store/Database;

    iput-object v1, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask;->database:Lcom/google/android/videos/store/Database;

    .line 69
    invoke-static/range {p8 .. p8}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/accounts/AccountManagerWrapper;

    iput-object v1, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    .line 70
    move/from16 v0, p9

    iput-boolean v0, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask;->legacyDownloadsHaveAppLevelDrm:Z

    .line 71
    invoke-static/range {p11 .. p11}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/store/ItagInfoStore;

    iput-object v1, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask;->itagInfoStore:Lcom/google/android/videos/store/ItagInfoStore;

    .line 72
    return-void
.end method

.method synthetic constructor <init>(ILcom/google/android/videos/pinning/DownloadKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/videos/pinning/Task$Listener;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/drm/DrmManager;Lcom/google/android/videos/accounts/AccountManagerWrapper;ZLcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/store/ItagInfoStore;Lcom/google/android/videos/pinning/LegacyLicenseTask$1;)V
    .locals 0
    .param p1, "x0"    # I
    .param p2, "x1"    # Lcom/google/android/videos/pinning/DownloadKey;
    .param p3, "x2"    # Landroid/os/PowerManager$WakeLock;
    .param p4, "x3"    # Landroid/net/wifi/WifiManager$WifiLock;
    .param p5, "x4"    # Lcom/google/android/videos/pinning/Task$Listener;
    .param p6, "x5"    # Lcom/google/android/videos/store/Database;
    .param p7, "x6"    # Lcom/google/android/videos/drm/DrmManager;
    .param p8, "x7"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .param p9, "x8"    # Z
    .param p10, "x9"    # Lcom/google/android/videos/logging/EventLogger;
    .param p11, "x10"    # Lcom/google/android/videos/store/ItagInfoStore;
    .param p12, "x11"    # Lcom/google/android/videos/pinning/LegacyLicenseTask$1;

    .prologue
    .line 46
    invoke-direct/range {p0 .. p11}, Lcom/google/android/videos/pinning/LegacyLicenseTask;-><init>(ILcom/google/android/videos/pinning/DownloadKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/videos/pinning/Task$Listener;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/drm/DrmManager;Lcom/google/android/videos/accounts/AccountManagerWrapper;ZLcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/store/ItagInfoStore;)V

    return-void
.end method

.method private getStoredLicenseInfo()Landroid/util/Pair;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Ljava/io/File;",
            "Lcom/google/android/videos/drm/DrmManager$Identifiers;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v10, 0x1

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 157
    iget-object v2, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v2}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 158
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "purchased_assets"

    sget-object v2, Lcom/google/android/videos/pinning/LegacyLicenseTask$LicenseInfoQuery;->PROJECTION:[Ljava/lang/String;

    const-string v3, "asset_type IN (6,20) AND account = ? AND asset_id = ?"

    new-array v4, v4, [Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v6, v6, Lcom/google/android/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    aput-object v6, v4, v7

    iget-object v6, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v6, v6, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    aput-object v6, v4, v10

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 161
    .local v8, "cursor":Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 162
    .local v9, "filePathKey":Ljava/lang/String;
    const/4 v1, 0x0

    .line 164
    .local v1, "ids":Lcom/google/android/videos/drm/DrmManager$Identifiers;
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 165
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "missing entry in pinning table for: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v2, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v2, v2, Lcom/google/android/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v2, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v2, v2, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 179
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 181
    :goto_0
    return-object v5

    .line 168
    :cond_0
    const/4 v2, 0x4

    :try_start_1
    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-static {v8, v2}, Lcom/google/android/videos/utils/DbUtils;->isAnyNull(Landroid/database/Cursor;[I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 171
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "missing identifers in pinning table for: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v2, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v2, v2, Lcom/google/android/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v2, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v2, v2, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 179
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 175
    :cond_1
    const/4 v2, 0x3

    :try_start_2
    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 176
    new-instance v1, Lcom/google/android/videos/drm/DrmManager$Identifiers;

    .end local v1    # "ids":Lcom/google/android/videos/drm/DrmManager$Identifiers;
    const/4 v2, 0x0

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const/4 v4, 0x1

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const/4 v6, 0x2

    invoke-interface {v8, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-direct/range {v1 .. v7}, Lcom/google/android/videos/drm/DrmManager$Identifiers;-><init>(JJJ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 179
    .restart local v1    # "ids":Lcom/google/android/videos/drm/DrmManager$Identifiers;
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 181
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v5

    goto :goto_0

    .line 179
    .end local v1    # "ids":Lcom/google/android/videos/drm/DrmManager$Identifiers;
    :catchall_0
    move-exception v2

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v2

    .line 168
    nop

    :array_0
    .array-data 4
        0x0
        0x1
        0x2
        0x3
    .end array-data
.end method

.method private reconstructOfflineVideoStream(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/io/File;)Lcom/google/android/videos/streams/MediaStream;
    .locals 9
    .param p1, "details"    # Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;
    .param p2, "file"    # Ljava/io/File;

    .prologue
    const/4 v3, 0x2

    const/4 v4, 0x0

    .line 116
    iget-object v1, p1, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->extra:Lcom/google/android/videos/proto/DownloadExtra;

    iget-object v1, v1, Lcom/google/android/videos/proto/DownloadExtra;->streamInfos:[Lcom/google/android/videos/proto/StreamInfo;

    aget-object v8, v1, v4

    .line 117
    .local v8, "streamInfo":Lcom/google/android/videos/proto/StreamInfo;
    iget-object v1, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask;->itagInfoStore:Lcom/google/android/videos/store/ItagInfoStore;

    iget v2, v8, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    invoke-virtual {v1, v2}, Lcom/google/android/videos/store/ItagInfoStore;->getItagInfo(I)Lcom/google/android/videos/streams/ItagInfo;

    move-result-object v0

    .line 118
    .local v0, "itagInfo":Lcom/google/android/videos/streams/ItagInfo;
    if-nez v0, :cond_0

    .line 119
    invoke-virtual {p2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask;->legacyDownloadsHaveAppLevelDrm:Z

    invoke-static {v1, v2}, Lcom/google/android/videos/utils/OfflineUtil;->isAppLevelDrmEncrypted(Ljava/lang/String;Z)Z

    move-result v7

    .line 121
    .local v7, "appLevelDrm":Z
    if-eqz v7, :cond_1

    const/4 v5, 0x1

    .line 122
    .local v5, "drmType":I
    :goto_0
    new-instance v0, Lcom/google/android/videos/streams/ItagInfo;

    .end local v0    # "itagInfo":Lcom/google/android/videos/streams/ItagInfo;
    const/16 v1, 0x280

    const/16 v2, 0x1e0

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/streams/ItagInfo;-><init>(IIIZIZ)V

    .line 124
    .end local v5    # "drmType":I
    .end local v7    # "appLevelDrm":Z
    .restart local v0    # "itagInfo":Lcom/google/android/videos/streams/ItagInfo;
    :cond_0
    new-instance v1, Lcom/google/android/videos/streams/MediaStream;

    invoke-static {p2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v1, v2, v0, v8}, Lcom/google/android/videos/streams/MediaStream;-><init>(Landroid/net/Uri;Lcom/google/android/videos/streams/ItagInfo;Lcom/google/android/videos/proto/StreamInfo;)V

    return-object v1

    .restart local v7    # "appLevelDrm":Z
    :cond_1
    move v5, v3

    .line 121
    goto :goto_0
.end method


# virtual methods
.method protected abstract buildRequest(Ljava/lang/String;Lcom/google/android/videos/streams/MediaStream;Lcom/google/android/videos/drm/DrmManager$Identifiers;)Lcom/google/android/videos/drm/DrmRequest;
.end method

.method public execute()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/Task$TaskException;
        }
    .end annotation

    .prologue
    .line 76
    iget-object v7, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask;->database:Lcom/google/android/videos/store/Database;

    iget-object v6, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/videos/pinning/DownloadKey;

    invoke-static {v7, v6}, Lcom/google/android/videos/pinning/PinningDbHelper;->getDownloadDetails(Lcom/google/android/videos/store/Database;Lcom/google/android/videos/pinning/DownloadKey;)Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;

    move-result-object v0

    .line 77
    .local v0, "details":Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;
    if-eqz v0, :cond_0

    iget v6, v0, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->licenseType:I

    const/4 v7, 0x1

    if-eq v6, v7, :cond_2

    .line 78
    :cond_0
    new-instance v6, Lcom/google/android/videos/pinning/Task$TaskException;

    const-string v7, "Missing license details"

    invoke-direct {v6, v7}, Lcom/google/android/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v6}, Lcom/google/android/videos/pinning/LegacyLicenseTask;->onRequestImpossible(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/lang/Throwable;)V

    .line 108
    :cond_1
    :goto_0
    return-void

    .line 83
    :cond_2
    iget-object v7, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    iget-object v6, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v6, v6, Lcom/google/android/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    invoke-virtual {v7, v6}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->accountExists(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 84
    new-instance v7, Lcom/google/android/videos/pinning/Task$TaskException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Account does not exist: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v6, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v6, v6, Lcom/google/android/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v7, v6}, Lcom/google/android/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v7}, Lcom/google/android/videos/pinning/LegacyLicenseTask;->onRequestImpossible(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 89
    :cond_3
    invoke-direct {p0}, Lcom/google/android/videos/pinning/LegacyLicenseTask;->getStoredLicenseInfo()Landroid/util/Pair;

    move-result-object v4

    .line 90
    .local v4, "licenseInfo":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/io/File;Lcom/google/android/videos/drm/DrmManager$Identifiers;>;"
    if-nez v4, :cond_4

    .line 91
    new-instance v6, Lcom/google/android/videos/pinning/Task$TaskException;

    const-string v7, "Null licenseInfo"

    invoke-direct {v6, v7}, Lcom/google/android/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v6}, Lcom/google/android/videos/pinning/LegacyLicenseTask;->onRequestImpossible(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 95
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/videos/pinning/LegacyLicenseTask;->isCanceled()Z

    move-result v6

    if-nez v6, :cond_1

    .line 99
    iget-object v6, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v6, Ljava/io/File;

    invoke-direct {p0, v0, v6}, Lcom/google/android/videos/pinning/LegacyLicenseTask;->reconstructOfflineVideoStream(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/io/File;)Lcom/google/android/videos/streams/MediaStream;

    move-result-object v5

    .line 100
    .local v5, "stream":Lcom/google/android/videos/streams/MediaStream;
    iget-object v6, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v7, v6, Lcom/google/android/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    iget-object v6, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Lcom/google/android/videos/drm/DrmManager$Identifiers;

    invoke-virtual {p0, v7, v5, v6}, Lcom/google/android/videos/pinning/LegacyLicenseTask;->buildRequest(Ljava/lang/String;Lcom/google/android/videos/streams/MediaStream;Lcom/google/android/videos/drm/DrmManager$Identifiers;)Lcom/google/android/videos/drm/DrmRequest;

    move-result-object v2

    .line 101
    .local v2, "drmRequest":Lcom/google/android/videos/drm/DrmRequest;
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v1

    .line 102
    .local v1, "drmCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/drm/DrmResponse;>;"
    iget-object v6, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask;->drmManager:Lcom/google/android/videos/drm/DrmManager;

    invoke-virtual {v6, v2, v1}, Lcom/google/android/videos/drm/DrmManager;->request(Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/async/Callback;)V

    .line 104
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/videos/drm/DrmResponse;

    invoke-virtual {p0, v0, v6}, Lcom/google/android/videos/pinning/LegacyLicenseTask;->onRequestCompleted(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Lcom/google/android/videos/drm/DrmResponse;)V
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 105
    :catch_0
    move-exception v3

    .line 106
    .local v3, "e":Ljava/util/concurrent/ExecutionException;
    invoke-virtual {v3}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v6

    invoke-virtual {p0, v0, v6}, Lcom/google/android/videos/pinning/LegacyLicenseTask;->onRequestError(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected abstract onRequestCompleted(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Lcom/google/android/videos/drm/DrmResponse;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/Task$TaskException;
        }
    .end annotation
.end method

.method protected abstract onRequestError(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/lang/Throwable;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/Task$TaskException;
        }
    .end annotation
.end method

.method protected abstract onRequestImpossible(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/lang/Throwable;)V
.end method
