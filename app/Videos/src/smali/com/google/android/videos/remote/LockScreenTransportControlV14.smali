.class public Lcom/google/android/videos/remote/LockScreenTransportControlV14;
.super Lcom/google/android/videos/remote/LockScreenTransportControl;
.source "LockScreenTransportControlV14.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/remote/LockScreenTransportControlV14$1;,
        Lcom/google/android/videos/remote/LockScreenTransportControlV14$MediaButtonIntentReceiver;,
        Lcom/google/android/videos/remote/LockScreenTransportControlV14$AudioFocusListener;
    }
.end annotation


# instance fields
.field private final audioFocusListener:Lcom/google/android/videos/remote/LockScreenTransportControlV14$AudioFocusListener;

.field private final audioManager:Landroid/media/AudioManager;

.field private isPlaying:Z

.field private final mediaEventReceiver:Landroid/content/ComponentName;

.field private final mediaPendingIntent:Landroid/app/PendingIntent;

.field private remoteControlClient:Landroid/media/RemoteControlClient;


# direct methods
.method protected constructor <init>(Lcom/google/android/videos/remote/RemoteTracker;Landroid/content/Context;)V
    .locals 5
    .param p1, "remoteTracker"    # Lcom/google/android/videos/remote/RemoteTracker;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 40
    invoke-direct {p0, p1}, Lcom/google/android/videos/remote/LockScreenTransportControl;-><init>(Lcom/google/android/videos/remote/RemoteTracker;)V

    .line 42
    const-string v1, "audio"

    invoke-virtual {p2, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->audioManager:Landroid/media/AudioManager;

    .line 43
    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/google/android/videos/remote/LockScreenTransportControlV14$MediaButtonIntentReceiver;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->mediaEventReceiver:Landroid/content/ComponentName;

    .line 45
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MEDIA_BUTTON"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 46
    .local v0, "mediaButtonIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->mediaEventReceiver:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 47
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v4, v0, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->mediaPendingIntent:Landroid/app/PendingIntent;

    .line 49
    new-instance v1, Lcom/google/android/videos/remote/LockScreenTransportControlV14$AudioFocusListener;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/videos/remote/LockScreenTransportControlV14$AudioFocusListener;-><init>(Lcom/google/android/videos/remote/LockScreenTransportControlV14$1;)V

    iput-object v1, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->audioFocusListener:Lcom/google/android/videos/remote/LockScreenTransportControlV14$AudioFocusListener;

    .line 50
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/videos/remote/LockScreenTransportControlV14;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/remote/LockScreenTransportControlV14;

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->isPlaying:Z

    return v0
.end method

.method private updateBitmap(Landroid/media/RemoteControlClient$MetadataEditor;)V
    .locals 4
    .param p1, "editor"    # Landroid/media/RemoteControlClient$MetadataEditor;

    .prologue
    .line 139
    iget-object v2, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v2}, Lcom/google/android/videos/remote/RemoteTracker;->getPosterBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 140
    .local v1, "src":Landroid/graphics/Bitmap;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 141
    .local v0, "copy":Landroid/graphics/Bitmap;
    :goto_0
    const/16 v2, 0x64

    invoke-virtual {p1, v2, v0}, Landroid/media/RemoteControlClient$MetadataEditor;->putBitmap(ILandroid/graphics/Bitmap;)Landroid/media/RemoteControlClient$MetadataEditor;

    .line 142
    return-void

    .line 140
    .end local v0    # "copy":Landroid/graphics/Bitmap;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final getRemoteControlClient()Landroid/media/RemoteControlClient;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->remoteControlClient:Landroid/media/RemoteControlClient;

    return-object v0
.end method

.method protected getTransportControlFlags()I
    .locals 1

    .prologue
    .line 175
    const/16 v0, 0x28

    return v0
.end method

.method onDisconnect()V
    .locals 3

    .prologue
    .line 199
    invoke-virtual {p0}, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->getListeners()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/remote/TransportControl$Listener;

    .line 200
    .local v1, "listener":Lcom/google/android/videos/remote/TransportControl$Listener;
    invoke-interface {v1}, Lcom/google/android/videos/remote/TransportControl$Listener;->onDisconnect()V

    goto :goto_0

    .line 202
    .end local v1    # "listener":Lcom/google/android/videos/remote/TransportControl$Listener;
    :cond_0
    return-void
.end method

.method public onErrorChanged()V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v0}, Lcom/google/android/videos/remote/RemoteTracker;->hasError()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->updatePlaybackState(I)V

    .line 57
    :cond_0
    return-void
.end method

.method onPause()V
    .locals 3

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->getListeners()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/remote/TransportControl$Listener;

    .line 194
    .local v1, "listener":Lcom/google/android/videos/remote/TransportControl$Listener;
    invoke-interface {v1}, Lcom/google/android/videos/remote/TransportControl$Listener;->onPause()V

    goto :goto_0

    .line 196
    .end local v1    # "listener":Lcom/google/android/videos/remote/TransportControl$Listener;
    :cond_0
    return-void
.end method

.method onPlay()V
    .locals 3

    .prologue
    .line 187
    invoke-virtual {p0}, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->getListeners()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/remote/TransportControl$Listener;

    .line 188
    .local v1, "listener":Lcom/google/android/videos/remote/TransportControl$Listener;
    invoke-interface {v1}, Lcom/google/android/videos/remote/TransportControl$Listener;->onPlay()V

    goto :goto_0

    .line 190
    .end local v1    # "listener":Lcom/google/android/videos/remote/TransportControl$Listener;
    :cond_0
    return-void
.end method

.method public onPlayerStateChanged()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 61
    const/4 v0, 0x1

    .line 62
    .local v0, "playbackState":I
    iget-object v5, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v5}, Lcom/google/android/videos/remote/RemoteTracker;->getPlayerState()Lcom/google/android/videos/remote/PlayerState;

    move-result-object v1

    .line 63
    .local v1, "playerState":Lcom/google/android/videos/remote/PlayerState;
    const/4 v3, 0x0

    .line 64
    .local v3, "shouldBeRegistered":Z
    if-eqz v1, :cond_0

    .line 65
    iput-boolean v4, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->isPlaying:Z

    .line 66
    const/4 v3, 0x1

    .line 67
    iget v5, v1, Lcom/google/android/videos/remote/PlayerState;->state:I

    packed-switch v5, :pswitch_data_0

    .line 82
    const/4 v3, 0x0

    .line 87
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->remoteControlClient:Landroid/media/RemoteControlClient;

    if-eqz v5, :cond_2

    .line 88
    .local v2, "registered":Z
    :goto_1
    if-eq v2, v3, :cond_1

    .line 89
    if-eqz v3, :cond_3

    .line 90
    invoke-virtual {p0}, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->register()V

    .line 91
    invoke-virtual {p0}, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->onVideoInfoChanged()V

    .line 92
    invoke-virtual {p0}, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->onPosterBitmapChanged()V

    .line 98
    :cond_1
    :goto_2
    invoke-virtual {p0, v0}, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->updatePlaybackState(I)V

    .line 99
    return-void

    .line 69
    .end local v2    # "registered":Z
    :pswitch_0
    const/4 v0, 0x2

    .line 70
    goto :goto_0

    .line 72
    :pswitch_1
    const/16 v0, 0x9

    .line 73
    goto :goto_0

    .line 75
    :pswitch_2
    const/4 v0, 0x3

    .line 76
    iput-boolean v2, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->isPlaying:Z

    goto :goto_0

    .line 79
    :pswitch_3
    const/16 v0, 0x8

    .line 80
    goto :goto_0

    :cond_2
    move v2, v4

    .line 87
    goto :goto_1

    .line 94
    .restart local v2    # "registered":Z
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->unregister()V

    goto :goto_2

    .line 67
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPosterBitmapChanged()V
    .locals 3

    .prologue
    .line 127
    iget-object v1, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->remoteControlClient:Landroid/media/RemoteControlClient;

    if-nez v1, :cond_0

    .line 133
    :goto_0
    return-void

    .line 130
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->remoteControlClient:Landroid/media/RemoteControlClient;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/media/RemoteControlClient;->editMetadata(Z)Landroid/media/RemoteControlClient$MetadataEditor;

    move-result-object v0

    .line 131
    .local v0, "editor":Landroid/media/RemoteControlClient$MetadataEditor;
    invoke-direct {p0, v0}, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->updateBitmap(Landroid/media/RemoteControlClient$MetadataEditor;)V

    .line 132
    invoke-virtual {v0}, Landroid/media/RemoteControlClient$MetadataEditor;->apply()V

    goto :goto_0
.end method

.method public onVideoInfoChanged()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 103
    iget-object v6, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->remoteControlClient:Landroid/media/RemoteControlClient;

    if-nez v6, :cond_0

    .line 123
    :goto_0
    return-void

    .line 106
    :cond_0
    iget-object v6, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v6}, Lcom/google/android/videos/remote/RemoteTracker;->getVideoInfo()Lcom/google/android/videos/remote/RemoteVideoInfo;

    move-result-object v5

    .line 108
    .local v5, "videoInfo":Lcom/google/android/videos/remote/RemoteVideoInfo;
    const/4 v0, 0x0

    .line 109
    .local v0, "album":Ljava/lang/String;
    const/4 v4, 0x0

    .line 110
    .local v4, "title":Ljava/lang/String;
    const-wide/16 v2, 0x0

    .line 111
    .local v2, "duration":J
    if-eqz v5, :cond_1

    .line 112
    iget-object v0, v5, Lcom/google/android/videos/remote/RemoteVideoInfo;->showTitle:Ljava/lang/String;

    .line 113
    iget-object v4, v5, Lcom/google/android/videos/remote/RemoteVideoInfo;->videoTitle:Ljava/lang/String;

    .line 114
    iget v6, v5, Lcom/google/android/videos/remote/RemoteVideoInfo;->durationMillis:I

    int-to-long v2, v6

    .line 117
    :cond_1
    iget-object v6, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->remoteControlClient:Landroid/media/RemoteControlClient;

    invoke-virtual {v6, v7}, Landroid/media/RemoteControlClient;->editMetadata(Z)Landroid/media/RemoteControlClient$MetadataEditor;

    move-result-object v1

    .line 118
    .local v1, "editor":Landroid/media/RemoteControlClient$MetadataEditor;
    invoke-virtual {v1, v7, v0}, Landroid/media/RemoteControlClient$MetadataEditor;->putString(ILjava/lang/String;)Landroid/media/RemoteControlClient$MetadataEditor;

    .line 119
    const/4 v6, 0x7

    invoke-virtual {v1, v6, v4}, Landroid/media/RemoteControlClient$MetadataEditor;->putString(ILjava/lang/String;)Landroid/media/RemoteControlClient$MetadataEditor;

    .line 120
    const/16 v6, 0x9

    invoke-virtual {v1, v6, v2, v3}, Landroid/media/RemoteControlClient$MetadataEditor;->putLong(IJ)Landroid/media/RemoteControlClient$MetadataEditor;

    .line 121
    invoke-direct {p0, v1}, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->updateBitmap(Landroid/media/RemoteControlClient$MetadataEditor;)V

    .line 122
    invoke-virtual {v1}, Landroid/media/RemoteControlClient$MetadataEditor;->apply()V

    goto :goto_0
.end method

.method protected register()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 149
    iget-object v0, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->remoteControlClient:Landroid/media/RemoteControlClient;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 150
    new-instance v0, Landroid/media/RemoteControlClient;

    iget-object v2, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->mediaPendingIntent:Landroid/app/PendingIntent;

    invoke-direct {v0, v2}, Landroid/media/RemoteControlClient;-><init>(Landroid/app/PendingIntent;)V

    iput-object v0, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->remoteControlClient:Landroid/media/RemoteControlClient;

    .line 151
    iget-object v0, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->audioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->audioFocusListener:Lcom/google/android/videos/remote/LockScreenTransportControlV14$AudioFocusListener;

    const/high16 v3, -0x80000000

    invoke-virtual {v0, v2, v3, v1}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 153
    iget-object v0, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->audioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->mediaEventReceiver:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->registerMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    .line 154
    iget-object v0, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->audioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->remoteControlClient:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->registerRemoteControlClient(Landroid/media/RemoteControlClient;)V

    .line 155
    iget-object v0, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->remoteControlClient:Landroid/media/RemoteControlClient;

    invoke-virtual {p0}, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->getTransportControlFlags()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient;->setTransportControlFlags(I)V

    .line 156
    return-void

    .line 149
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected unregister()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 159
    iget-object v0, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->remoteControlClient:Landroid/media/RemoteControlClient;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 160
    iget-object v0, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->remoteControlClient:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient;->editMetadata(Z)Landroid/media/RemoteControlClient$MetadataEditor;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/RemoteControlClient$MetadataEditor;->apply()V

    .line 161
    iget-object v0, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->remoteControlClient:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v2}, Landroid/media/RemoteControlClient;->setTransportControlFlags(I)V

    .line 162
    iget-object v0, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->audioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->audioFocusListener:Lcom/google/android/videos/remote/LockScreenTransportControlV14$AudioFocusListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 163
    iget-object v0, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->audioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->mediaEventReceiver:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->unregisterMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    .line 164
    iget-object v0, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->audioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->remoteControlClient:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->unregisterRemoteControlClient(Landroid/media/RemoteControlClient;)V

    .line 165
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->remoteControlClient:Landroid/media/RemoteControlClient;

    .line 166
    return-void

    :cond_0
    move v0, v2

    .line 159
    goto :goto_0
.end method

.method protected updatePlaybackState(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->remoteControlClient:Landroid/media/RemoteControlClient;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->remoteControlClient:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, p1}, Landroid/media/RemoteControlClient;->setPlaybackState(I)V

    .line 172
    :cond_0
    return-void
.end method
