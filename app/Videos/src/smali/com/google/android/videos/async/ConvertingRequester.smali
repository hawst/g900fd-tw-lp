.class public Lcom/google/android/videos/async/ConvertingRequester;
.super Ljava/lang/Object;
.source "ConvertingRequester.java"

# interfaces
.implements Lcom/google/android/videos/async/Requester;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        "S:",
        "Ljava/lang/Object;",
        "F:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Requester",
        "<TR;TE;>;"
    }
.end annotation


# instance fields
.field private final executor:Ljava/util/concurrent/Executor;

.field private final requestConverter:Lcom/google/android/videos/converter/RequestConverter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/converter/RequestConverter",
            "<TR;TS;>;"
        }
    .end annotation
.end field

.field private final requester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<TS;TF;>;"
        }
    .end annotation
.end field

.field private final responseConverter:Lcom/google/android/videos/converter/ResponseConverter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/converter/ResponseConverter",
            "<TF;TE;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/converter/RequestConverter;Lcom/google/android/videos/converter/ResponseConverter;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p4, "executor"    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<TS;TF;>;",
            "Lcom/google/android/videos/converter/RequestConverter",
            "<TR;TS;>;",
            "Lcom/google/android/videos/converter/ResponseConverter",
            "<TF;TE;>;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 39
    .local p0, "this":Lcom/google/android/videos/async/ConvertingRequester;, "Lcom/google/android/videos/async/ConvertingRequester<TR;TE;TS;TF;>;"
    .local p1, "requester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TS;TF;>;"
    .local p2, "requestConverter":Lcom/google/android/videos/converter/RequestConverter;, "Lcom/google/android/videos/converter/RequestConverter<TR;TS;>;"
    .local p3, "responseConverter":Lcom/google/android/videos/converter/ResponseConverter;, "Lcom/google/android/videos/converter/ResponseConverter<TF;TE;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/async/ConvertingRequester;->requester:Lcom/google/android/videos/async/Requester;

    .line 41
    iput-object p2, p0, Lcom/google/android/videos/async/ConvertingRequester;->requestConverter:Lcom/google/android/videos/converter/RequestConverter;

    .line 42
    iput-object p3, p0, Lcom/google/android/videos/async/ConvertingRequester;->responseConverter:Lcom/google/android/videos/converter/ResponseConverter;

    .line 43
    iput-object p4, p0, Lcom/google/android/videos/async/ConvertingRequester;->executor:Ljava/util/concurrent/Executor;

    .line 44
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/videos/converter/RequestConverter;Lcom/google/android/videos/converter/ResponseConverter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/converter/RequestConverter",
            "<TR;TS;>;",
            "Lcom/google/android/videos/converter/ResponseConverter",
            "<TF;TE;>;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/videos/async/ConvertingRequester;, "Lcom/google/android/videos/async/ConvertingRequester<TR;TE;TS;TF;>;"
    .local p1, "requestConverter":Lcom/google/android/videos/converter/RequestConverter;, "Lcom/google/android/videos/converter/RequestConverter<TR;TS;>;"
    .local p2, "responseConverter":Lcom/google/android/videos/converter/ResponseConverter;, "Lcom/google/android/videos/converter/ResponseConverter<TF;TE;>;"
    const/4 v0, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object v0, p0, Lcom/google/android/videos/async/ConvertingRequester;->requester:Lcom/google/android/videos/async/Requester;

    .line 55
    iput-object p1, p0, Lcom/google/android/videos/async/ConvertingRequester;->requestConverter:Lcom/google/android/videos/converter/RequestConverter;

    .line 56
    iput-object p2, p0, Lcom/google/android/videos/async/ConvertingRequester;->responseConverter:Lcom/google/android/videos/converter/ResponseConverter;

    .line 57
    iput-object v0, p0, Lcom/google/android/videos/async/ConvertingRequester;->executor:Ljava/util/concurrent/Executor;

    .line 58
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/async/ConvertingRequester;)Lcom/google/android/videos/converter/ResponseConverter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/async/ConvertingRequester;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/videos/async/ConvertingRequester;->responseConverter:Lcom/google/android/videos/converter/ResponseConverter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/async/ConvertingRequester;)Ljava/util/concurrent/Executor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/async/ConvertingRequester;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/videos/async/ConvertingRequester;->executor:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public static create(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/converter/RequestConverter;)Lcom/google/android/videos/async/Requester;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/videos/async/Requester",
            "<TS;TE;>;",
            "Lcom/google/android/videos/converter/RequestConverter",
            "<TR;TS;>;)",
            "Lcom/google/android/videos/async/Requester",
            "<TR;TE;>;"
        }
    .end annotation

    .prologue
    .local p0, "requester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TS;TE;>;"
    .local p1, "requestConverter":Lcom/google/android/videos/converter/RequestConverter;, "Lcom/google/android/videos/converter/RequestConverter<TR;TS;>;"
    const/4 v1, 0x0

    .line 264
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    new-instance v0, Lcom/google/android/videos/async/ConvertingRequester;

    invoke-direct {v0, p0, p1, v1, v1}, Lcom/google/android/videos/async/ConvertingRequester;-><init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/converter/RequestConverter;Lcom/google/android/videos/converter/ResponseConverter;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method

.method public static create(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/converter/ResponseConverter;)Lcom/google/android/videos/async/Requester;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "F:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/videos/async/Requester",
            "<TR;TF;>;",
            "Lcom/google/android/videos/converter/ResponseConverter",
            "<TF;TE;>;)",
            "Lcom/google/android/videos/async/Requester",
            "<TR;TE;>;"
        }
    .end annotation

    .prologue
    .local p0, "requester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TR;TF;>;"
    .local p1, "responseConverter":Lcom/google/android/videos/converter/ResponseConverter;, "Lcom/google/android/videos/converter/ResponseConverter<TF;TE;>;"
    const/4 v1, 0x0

    .line 226
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    new-instance v0, Lcom/google/android/videos/async/ConvertingRequester;

    invoke-direct {v0, p0, v1, p1, v1}, Lcom/google/android/videos/async/ConvertingRequester;-><init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/converter/RequestConverter;Lcom/google/android/videos/converter/ResponseConverter;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method

.method public static create(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/converter/ResponseConverter;Ljava/util/concurrent/Executor;)Lcom/google/android/videos/async/Requester;
    .locals 2
    .param p2, "executor"    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "F:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/videos/async/Requester",
            "<TR;TF;>;",
            "Lcom/google/android/videos/converter/ResponseConverter",
            "<TF;TE;>;",
            "Ljava/util/concurrent/Executor;",
            ")",
            "Lcom/google/android/videos/async/Requester",
            "<TR;TE;>;"
        }
    .end annotation

    .prologue
    .line 246
    .local p0, "requester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TR;TF;>;"
    .local p1, "responseConverter":Lcom/google/android/videos/converter/ResponseConverter;, "Lcom/google/android/videos/converter/ResponseConverter<TF;TE;>;"
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    new-instance v0, Lcom/google/android/videos/async/ConvertingRequester;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/google/android/videos/async/ConvertingRequester;-><init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/converter/RequestConverter;Lcom/google/android/videos/converter/ResponseConverter;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method


# virtual methods
.method protected doRequest(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;",
            "Lcom/google/android/videos/async/Callback",
            "<TS;TF;>;)V"
        }
    .end annotation

    .prologue
    .line 98
    .local p0, "this":Lcom/google/android/videos/async/ConvertingRequester;, "Lcom/google/android/videos/async/ConvertingRequester<TR;TE;TS;TF;>;"
    .local p1, "convertedRequest":Ljava/lang/Object;, "TS;"
    .local p2, "convertingCallback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TS;TF;>;"
    iget-object v0, p0, Lcom/google/android/videos/async/ConvertingRequester;->requester:Lcom/google/android/videos/async/Requester;

    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    iget-object v0, p0, Lcom/google/android/videos/async/ConvertingRequester;->requester:Lcom/google/android/videos/async/Requester;

    invoke-interface {v0, p1, p2}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 100
    return-void
.end method

.method protected onConvertError(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/videos/async/Callback;Ljava/lang/Exception;)V
    .locals 0
    .param p4, "exception"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;TS;",
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    .prologue
    .line 111
    .local p0, "this":Lcom/google/android/videos/async/ConvertingRequester;, "Lcom/google/android/videos/async/ConvertingRequester<TR;TE;TS;TF;>;"
    .local p1, "originalRequest":Ljava/lang/Object;, "TR;"
    .local p2, "convertedRequest":Ljava/lang/Object;, "TS;"
    .local p3, "originalCallback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TR;TE;>;"
    invoke-interface {p3, p1, p4}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 112
    return-void
.end method

.method public request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;)V"
        }
    .end annotation

    .prologue
    .line 80
    .local p0, "this":Lcom/google/android/videos/async/ConvertingRequester;, "Lcom/google/android/videos/async/ConvertingRequester<TR;TE;TS;TF;>;"
    .local p1, "request":Ljava/lang/Object;, "TR;"
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TR;TE;>;"
    :try_start_0
    iget-object v4, p0, Lcom/google/android/videos/async/ConvertingRequester;->requestConverter:Lcom/google/android/videos/converter/RequestConverter;

    if-eqz v4, :cond_0

    .line 81
    iget-object v4, p0, Lcom/google/android/videos/async/ConvertingRequester;->requestConverter:Lcom/google/android/videos/converter/RequestConverter;

    invoke-interface {v4, p1}, Lcom/google/android/videos/converter/RequestConverter;->convertRequest(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 89
    .local v0, "convertedRequest":Ljava/lang/Object;, "TS;"
    :goto_0
    new-instance v1, Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;

    invoke-direct {v1, p0, p1, v0, p2}, Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;-><init>(Lcom/google/android/videos/async/ConvertingRequester;Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 91
    .local v1, "convertingCallback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TS;TF;>;"
    invoke-virtual {p0, v0, v1}, Lcom/google/android/videos/async/ConvertingRequester;->doRequest(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    :try_end_0
    .catch Lcom/google/android/videos/converter/ConverterException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    .end local v0    # "convertedRequest":Ljava/lang/Object;, "TS;"
    .end local v1    # "convertingCallback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TS;TF;>;"
    :goto_1
    return-void

    .line 85
    :cond_0
    move-object v3, p1

    .line 86
    .local v3, "temp":Ljava/lang/Object;, "TS;"
    move-object v0, v3

    .restart local v0    # "convertedRequest":Ljava/lang/Object;, "TS;"
    goto :goto_0

    .line 92
    .end local v0    # "convertedRequest":Ljava/lang/Object;, "TS;"
    .end local v3    # "temp":Ljava/lang/Object;, "TS;"
    :catch_0
    move-exception v2

    .line 93
    .local v2, "exception":Lcom/google/android/videos/converter/ConverterException;
    const/4 v4, 0x0

    invoke-virtual {p0, p1, v4, p2, v2}, Lcom/google/android/videos/async/ConvertingRequester;->onConvertError(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/videos/async/Callback;Ljava/lang/Exception;)V

    goto :goto_1
.end method
