.class abstract Lcom/google/android/videos/store/SyncTaskManager$SyncTask;
.super Ljava/lang/Object;
.source "SyncTaskManager.java"

# interfaces
.implements Ljava/lang/Comparable;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/SyncTaskManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "SyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/videos/store/SyncTaskManager$SyncTask;",
        ">;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field protected final priority:I

.field private volatile scheduled:Z

.field protected final states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/store/SyncTaskManager$SharedStates",
            "<*>;"
        }
    .end annotation
.end field

.field private final syncTaskManager:Lcom/google/android/videos/store/SyncTaskManager;

.field private final ticket:I


# direct methods
.method public constructor <init>(Lcom/google/android/videos/store/SyncTaskManager;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V
    .locals 1
    .param p1, "syncTaskManager"    # Lcom/google/android/videos/store/SyncTaskManager;
    .param p2, "priorityLevel"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/store/SyncTaskManager;",
            "I",
            "Lcom/google/android/videos/store/SyncTaskManager$SharedStates",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 105
    .local p3, "states":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<*>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    iput-object p1, p0, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;->syncTaskManager:Lcom/google/android/videos/store/SyncTaskManager;

    .line 107
    iput p2, p0, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;->priority:I

    .line 108
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    iput-object v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    .line 109
    # getter for: Lcom/google/android/videos/store/SyncTaskManager;->nextTicket:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {p1}, Lcom/google/android/videos/store/SyncTaskManager;->access$000(Lcom/google/android/videos/store/SyncTaskManager;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;->ticket:I

    .line 110
    return-void
.end method


# virtual methods
.method public final compareTo(Lcom/google/android/videos/store/SyncTaskManager$SyncTask;)I
    .locals 2
    .param p1, "other"    # Lcom/google/android/videos/store/SyncTaskManager$SyncTask;

    .prologue
    .line 159
    iget v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;->priority:I

    iget v1, p1, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;->priority:I

    if-eq v0, v1, :cond_0

    .line 160
    iget v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;->priority:I

    iget v1, p1, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;->priority:I

    sub-int/2addr v0, v1

    .line 162
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;->ticket:I

    iget v1, p1, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;->ticket:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 95
    check-cast p1, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;->compareTo(Lcom/google/android/videos/store/SyncTaskManager$SyncTask;)I

    move-result v0

    return v0
.end method

.method protected abstract doSync()V
.end method

.method protected final maybeReportRequiredDataLevelError(Ljava/lang/Exception;)V
    .locals 1
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 150
    instance-of v0, p1, Ljava/io/IOException;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/google/android/videos/converter/ConverterException;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/google/android/videos/store/SyncTaskManager$DataException;

    if-eqz v0, :cond_1

    .line 153
    :cond_0
    const/16 v0, 0x14

    invoke-virtual {p0, v0, p1}, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;->onSyncError(ILjava/lang/Exception;)V

    .line 155
    :cond_1
    return-void
.end method

.method protected final onSyncError(ILjava/lang/Exception;)V
    .locals 2
    .param p1, "errorLevel"    # I
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 137
    invoke-virtual {p0, p2}, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;->shouldLogAsError(Ljava/lang/Exception;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "In "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; error level "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    if-eqz v0, :cond_1

    .line 141
    iget-object v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->onError(ILjava/lang/Exception;)V

    .line 143
    :cond_1
    return-void
.end method

.method public final run()V
    .locals 2

    .prologue
    .line 122
    iget-boolean v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;->scheduled:Z

    const-string v1, "sync task run without being scheduled"

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    invoke-virtual {v0}, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    invoke-virtual {p0}, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;->doSync()V

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    invoke-virtual {v0}, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->decrement()V

    .line 127
    return-void
.end method

.method public final schedule()V
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;->scheduled:Z

    .line 114
    iget-object v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    invoke-virtual {v0}, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    invoke-virtual {v0}, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->increment()V

    .line 116
    iget-object v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;->syncTaskManager:Lcom/google/android/videos/store/SyncTaskManager;

    # getter for: Lcom/google/android/videos/store/SyncTaskManager;->executor:Ljava/util/concurrent/Executor;
    invoke-static {v0}, Lcom/google/android/videos/store/SyncTaskManager;->access$100(Lcom/google/android/videos/store/SyncTaskManager;)Ljava/util/concurrent/Executor;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 118
    :cond_0
    return-void
.end method

.method protected final shouldLogAsError(Ljava/lang/Exception;)Z
    .locals 1
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 146
    instance-of v0, p1, Lcom/google/android/videos/converter/ConverterException;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/google/android/videos/store/SyncTaskManager$DataException;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
