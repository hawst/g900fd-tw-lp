.class public Lcom/google/android/videos/ui/NoWishlistContentFlow;
.super Lcom/google/android/videos/flow/SingleViewFlow;
.source "NoWishlistContentFlow.java"


# instance fields
.field private textResId:I

.field private final topOffset:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "topOffset"    # I

    .prologue
    .line 23
    const v0, 0x7f0400d2

    invoke-direct {p0, v0}, Lcom/google/android/videos/flow/SingleViewFlow;-><init>(I)V

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/ui/NoWishlistContentFlow;->textResId:I

    .line 24
    iput p1, p0, Lcom/google/android/videos/ui/NoWishlistContentFlow;->topOffset:I

    .line 25
    return-void
.end method


# virtual methods
.method public bindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 4
    .param p1, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .prologue
    const/4 v3, 0x0

    .line 35
    move-object v0, p1

    check-cast v0, Lcom/google/android/videos/ui/NoContentViewHolder;

    .line 36
    .local v0, "noContentViewHolder":Lcom/google/android/videos/ui/NoContentViewHolder;
    iget v1, p0, Lcom/google/android/videos/ui/NoWishlistContentFlow;->textResId:I

    if-eqz v1, :cond_0

    .line 37
    iget-object v1, v0, Lcom/google/android/videos/ui/NoContentViewHolder;->textView:Landroid/widget/TextView;

    iget v2, p0, Lcom/google/android/videos/ui/NoWishlistContentFlow;->textResId:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 39
    :cond_0
    iget-object v1, v0, Lcom/google/android/videos/ui/NoContentViewHolder;->imageView:Landroid/widget/ImageView;

    const v2, 0x7f02012c

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 40
    iget-object v1, v0, Lcom/google/android/videos/ui/NoContentViewHolder;->noContentView:Landroid/view/View;

    iget v2, p0, Lcom/google/android/videos/ui/NoWishlistContentFlow;->topOffset:I

    invoke-virtual {v1, v3, v2, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 41
    return-void
.end method

.method public createViewHolder(ILandroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 4
    .param p1, "viewType"    # I
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 29
    new-instance v0, Lcom/google/android/videos/ui/NoContentViewHolder;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0400d2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/videos/ui/NoContentViewHolder;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public updateText(Z)V
    .locals 2
    .param p1, "showsVerticalEnabled"    # Z

    .prologue
    .line 44
    if-eqz p1, :cond_1

    const v0, 0x7f0b00f2

    .line 47
    .local v0, "newTextResId":I
    :goto_0
    iget v1, p0, Lcom/google/android/videos/ui/NoWishlistContentFlow;->textResId:I

    if-eq v0, v1, :cond_0

    .line 48
    iput v0, p0, Lcom/google/android/videos/ui/NoWishlistContentFlow;->textResId:I

    .line 49
    invoke-virtual {p0}, Lcom/google/android/videos/ui/NoWishlistContentFlow;->notifyItemChanged()V

    .line 51
    :cond_0
    return-void

    .line 44
    .end local v0    # "newTextResId":I
    :cond_1
    const v0, 0x7f0b00f1

    goto :goto_0
.end method
