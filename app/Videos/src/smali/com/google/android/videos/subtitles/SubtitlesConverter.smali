.class public Lcom/google/android/videos/subtitles/SubtitlesConverter;
.super Lcom/google/android/videos/converter/HttpResponseConverter;
.source "SubtitlesConverter.java"

# interfaces
.implements Lcom/google/android/videos/converter/RequestConverter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/converter/HttpResponseConverter",
        "<",
        "Lcom/google/android/videos/subtitles/Subtitles;",
        ">;",
        "Lcom/google/android/videos/converter/RequestConverter",
        "<",
        "Lcom/google/android/videos/subtitles/SubtitleTrack;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        ">;"
    }
.end annotation


# static fields
.field private static final DEFAULT_DURATION_MILLIS:I


# instance fields
.field private final parser:Lcom/google/android/videos/utils/XmlParser;

.field private final rules:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/utils/XmlParser$Rule;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const/high16 v0, 0x40a00000    # 5.0f

    invoke-static {v0}, Lcom/google/android/videos/subtitles/SubtitlesConverter;->secondsToMillis(F)I

    move-result v0

    sput v0, Lcom/google/android/videos/subtitles/SubtitlesConverter;->DEFAULT_DURATION_MILLIS:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/videos/utils/XmlParser;)V
    .locals 1
    .param p1, "parser"    # Lcom/google/android/videos/utils/XmlParser;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/videos/converter/HttpResponseConverter;-><init>()V

    .line 59
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/utils/XmlParser;

    iput-object v0, p0, Lcom/google/android/videos/subtitles/SubtitlesConverter;->parser:Lcom/google/android/videos/utils/XmlParser;

    .line 60
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/subtitles/SubtitlesConverter;->rules:Ljava/util/Map;

    .line 61
    iget-object v0, p0, Lcom/google/android/videos/subtitles/SubtitlesConverter;->rules:Ljava/util/Map;

    invoke-static {v0}, Lcom/google/android/videos/subtitles/SubtitlesConverter;->addFormat1Rules(Ljava/util/Map;)V

    .line 62
    iget-object v0, p0, Lcom/google/android/videos/subtitles/SubtitlesConverter;->rules:Ljava/util/Map;

    invoke-static {v0}, Lcom/google/android/videos/subtitles/SubtitlesConverter;->addFormat2Rules(Ljava/util/Map;)V

    .line 63
    return-void
.end method

.method static synthetic access$000(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lorg/xml/sax/Attributes;
    .param p1, "x1"    # [Ljava/lang/String;

    .prologue
    .line 46
    invoke-static {p0, p1}, Lcom/google/android/videos/subtitles/SubtitlesConverter;->getValue(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(F)I
    .locals 1
    .param p0, "x0"    # F

    .prologue
    .line 46
    invoke-static {p0}, Lcom/google/android/videos/subtitles/SubtitlesConverter;->secondsToMillis(F)I

    move-result v0

    return v0
.end method

.method static synthetic access$200(I)I
    .locals 1
    .param p0, "x0"    # I

    .prologue
    .line 46
    invoke-static {p0}, Lcom/google/android/videos/subtitles/SubtitlesConverter;->anchorPointFromRawValue(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$300()I
    .locals 1

    .prologue
    .line 46
    sget v0, Lcom/google/android/videos/subtitles/SubtitlesConverter;->DEFAULT_DURATION_MILLIS:I

    return v0
.end method

.method private static addFormat1Rules(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/utils/XmlParser$Rule;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 66
    .local p0, "rules":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/videos/utils/XmlParser$Rule;>;"
    const-string v0, "/transcript"

    new-instance v1, Lcom/google/android/videos/subtitles/SubtitlesConverter$1;

    invoke-direct {v1}, Lcom/google/android/videos/subtitles/SubtitlesConverter$1;-><init>()V

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    const-string v0, "/transcript/text"

    new-instance v1, Lcom/google/android/videos/subtitles/SubtitlesConverter$2;

    invoke-direct {v1}, Lcom/google/android/videos/subtitles/SubtitlesConverter$2;-><init>()V

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    return-void
.end method

.method private static addFormat2Rules(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/utils/XmlParser$Rule;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 91
    .local p0, "rules":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/videos/utils/XmlParser$Rule;>;"
    const-string v0, "/timedtext"

    new-instance v1, Lcom/google/android/videos/subtitles/SubtitlesConverter$3;

    invoke-direct {v1}, Lcom/google/android/videos/subtitles/SubtitlesConverter$3;-><init>()V

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    const-string v0, "/timedtext/window"

    new-instance v1, Lcom/google/android/videos/subtitles/SubtitlesConverter$4;

    invoke-direct {v1}, Lcom/google/android/videos/subtitles/SubtitlesConverter$4;-><init>()V

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    const-string v0, "/timedtext/text"

    new-instance v1, Lcom/google/android/videos/subtitles/SubtitlesConverter$5;

    invoke-direct {v1}, Lcom/google/android/videos/subtitles/SubtitlesConverter$5;-><init>()V

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    return-void
.end method

.method private static anchorPointFromRawValue(I)I
    .locals 1
    .param p0, "rawAnchorPoint"    # I

    .prologue
    const/16 v0, 0x22

    .line 158
    packed-switch p0, :pswitch_data_0

    .line 169
    :goto_0
    :pswitch_0
    return v0

    .line 159
    :pswitch_1
    const/16 v0, 0x9

    goto :goto_0

    .line 160
    :pswitch_2
    const/16 v0, 0xa

    goto :goto_0

    .line 161
    :pswitch_3
    const/16 v0, 0xc

    goto :goto_0

    .line 162
    :pswitch_4
    const/16 v0, 0x11

    goto :goto_0

    .line 163
    :pswitch_5
    const/16 v0, 0x12

    goto :goto_0

    .line 164
    :pswitch_6
    const/16 v0, 0x14

    goto :goto_0

    .line 165
    :pswitch_7
    const/16 v0, 0x21

    goto :goto_0

    .line 167
    :pswitch_8
    const/16 v0, 0x24

    goto :goto_0

    .line 158
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
    .end packed-switch
.end method

.method private static varargs getValue(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "attrs"    # Lorg/xml/sax/Attributes;
    .param p1, "names"    # [Ljava/lang/String;

    .prologue
    .line 174
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    .line 175
    aget-object v2, p1, v0

    invoke-interface {p0, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 176
    .local v1, "value":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 180
    .end local v1    # "value":Ljava/lang/String;
    :goto_1
    return-object v1

    .line 174
    .restart local v1    # "value":Ljava/lang/String;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 180
    .end local v1    # "value":Ljava/lang/String;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private static secondsToMillis(F)I
    .locals 1
    .param p0, "seconds"    # F

    .prologue
    .line 184
    const/high16 v0, 0x447a0000    # 1000.0f

    mul-float/2addr v0, p0

    float-to-int v0, v0

    return v0
.end method


# virtual methods
.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 46
    check-cast p1, Lcom/google/android/videos/subtitles/SubtitleTrack;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/subtitles/SubtitlesConverter;->convertRequest(Lcom/google/android/videos/subtitles/SubtitleTrack;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public convertRequest(Lcom/google/android/videos/subtitles/SubtitleTrack;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 2
    .param p1, "track"    # Lcom/google/android/videos/subtitles/SubtitleTrack;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 189
    iget-object v0, p1, Lcom/google/android/videos/subtitles/SubtitleTrack;->url:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 190
    new-instance v0, Lcom/google/android/videos/converter/ConverterException;

    const-string v1, "subtitle track url cannot be empty"

    invoke-direct {v0, v1}, Lcom/google/android/videos/converter/ConverterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 192
    :cond_0
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    iget-object v1, p1, Lcom/google/android/videos/subtitles/SubtitleTrack;->url:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method protected convertResponseEntity(Lorg/apache/http/HttpEntity;)Lcom/google/android/videos/subtitles/Subtitles;
    .locals 5
    .param p1, "entity"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 199
    iget-object v2, p0, Lcom/google/android/videos/subtitles/SubtitlesConverter;->parser:Lcom/google/android/videos/utils/XmlParser;

    invoke-interface {p1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/videos/subtitles/SubtitlesConverter;->rules:Ljava/util/Map;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/videos/utils/XmlParser;->parse(Ljava/io/InputStream;Ljava/util/Map;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/subtitles/Subtitles$Builder;

    .line 201
    .local v0, "builder":Lcom/google/android/videos/subtitles/Subtitles$Builder;
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/videos/subtitles/Subtitles$Builder;->build()Lcom/google/android/videos/subtitles/Subtitles;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    .line 202
    :catch_0
    move-exception v1

    .line 203
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Lcom/google/android/videos/converter/ConverterException;

    invoke-direct {v2, v1}, Lcom/google/android/videos/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method protected bridge synthetic convertResponseEntity(Lorg/apache/http/HttpEntity;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lcom/google/android/videos/subtitles/SubtitlesConverter;->convertResponseEntity(Lorg/apache/http/HttpEntity;)Lcom/google/android/videos/subtitles/Subtitles;

    move-result-object v0

    return-object v0
.end method
