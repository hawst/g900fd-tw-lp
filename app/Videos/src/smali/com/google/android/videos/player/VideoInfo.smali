.class public Lcom/google/android/videos/player/VideoInfo;
.super Ljava/lang/Object;
.source "VideoInfo.java"


# instance fields
.field public final account:Ljava/lang/String;

.field public final cencKeySetId:[B

.field public final cencSecurityLevel:I

.field public final drmIdentifiers:Lcom/google/android/videos/drm/DrmManager$Identifiers;

.field public final durationMs:I

.field public final isEncrypted:Z

.field public final isEpisode:Z

.field public final isOffline:Z

.field public final offlineStreams:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;"
        }
    .end annotation
.end field

.field public final onlineStreams:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;"
        }
    .end annotation
.end field

.field public final videoId:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;ZZZLcom/google/android/videos/drm/DrmManager$Identifiers;[BILjava/util/List;Ljava/util/List;I)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "isEpisode"    # Z
    .param p4, "isEncrypted"    # Z
    .param p5, "isOffline"    # Z
    .param p6, "offlineDrmIdentifiers"    # Lcom/google/android/videos/drm/DrmManager$Identifiers;
    .param p7, "cencKeySetId"    # [B
    .param p8, "cencSecurityLevel"    # I
    .param p11, "durationMs"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZZZ",
            "Lcom/google/android/videos/drm/DrmManager$Identifiers;",
            "[BI",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 52
    .local p9, "offlineStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    .local p10, "onlineStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/google/android/videos/player/VideoInfo;->account:Ljava/lang/String;

    .line 54
    iput-object p2, p0, Lcom/google/android/videos/player/VideoInfo;->videoId:Ljava/lang/String;

    .line 55
    iput-boolean p3, p0, Lcom/google/android/videos/player/VideoInfo;->isEpisode:Z

    .line 56
    iput-boolean p4, p0, Lcom/google/android/videos/player/VideoInfo;->isEncrypted:Z

    .line 57
    iput-boolean p5, p0, Lcom/google/android/videos/player/VideoInfo;->isOffline:Z

    .line 58
    iput-object p6, p0, Lcom/google/android/videos/player/VideoInfo;->drmIdentifiers:Lcom/google/android/videos/drm/DrmManager$Identifiers;

    .line 59
    iput-object p7, p0, Lcom/google/android/videos/player/VideoInfo;->cencKeySetId:[B

    .line 60
    iput p8, p0, Lcom/google/android/videos/player/VideoInfo;->cencSecurityLevel:I

    .line 61
    iput-object p9, p0, Lcom/google/android/videos/player/VideoInfo;->offlineStreams:Ljava/util/List;

    .line 62
    iput-object p10, p0, Lcom/google/android/videos/player/VideoInfo;->onlineStreams:Ljava/util/List;

    .line 63
    iput p11, p0, Lcom/google/android/videos/player/VideoInfo;->durationMs:I

    .line 64
    return-void
.end method

.method public static createOffline(Ljava/lang/String;Ljava/lang/String;ZZLcom/google/android/videos/drm/DrmManager$Identifiers;[BILjava/util/List;I)Lcom/google/android/videos/player/VideoInfo;
    .locals 12
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "isEpisode"    # Z
    .param p3, "isEncrypted"    # Z
    .param p4, "drmIdentifiers"    # Lcom/google/android/videos/drm/DrmManager$Identifiers;
    .param p5, "cencKeySetId"    # [B
    .param p6, "cencSecurityLevel"    # I
    .param p8, "durationMs"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZZ",
            "Lcom/google/android/videos/drm/DrmManager$Identifiers;",
            "[BI",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;I)",
            "Lcom/google/android/videos/player/VideoInfo;"
        }
    .end annotation

    .prologue
    .line 39
    .local p7, "offlineStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    new-instance v0, Lcom/google/android/videos/player/VideoInfo;

    const/4 v5, 0x1

    const/4 v10, 0x0

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move/from16 v8, p6

    move-object/from16 v9, p7

    move/from16 v11, p8

    invoke-direct/range {v0 .. v11}, Lcom/google/android/videos/player/VideoInfo;-><init>(Ljava/lang/String;Ljava/lang/String;ZZZLcom/google/android/videos/drm/DrmManager$Identifiers;[BILjava/util/List;Ljava/util/List;I)V

    return-object v0
.end method

.method public static createOnline(Ljava/lang/String;Ljava/lang/String;ZZLjava/util/List;I)Lcom/google/android/videos/player/VideoInfo;
    .locals 12
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "isEpisode"    # Z
    .param p3, "isEncrypted"    # Z
    .param p5, "durationMs"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZZ",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;I)",
            "Lcom/google/android/videos/player/VideoInfo;"
        }
    .end annotation

    .prologue
    .line 45
    .local p4, "onlineStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    new-instance v0, Lcom/google/android/videos/player/VideoInfo;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object/from16 v10, p4

    move/from16 v11, p5

    invoke-direct/range {v0 .. v11}, Lcom/google/android/videos/player/VideoInfo;-><init>(Ljava/lang/String;Ljava/lang/String;ZZZLcom/google/android/videos/drm/DrmManager$Identifiers;[BILjava/util/List;Ljava/util/List;I)V

    return-object v0
.end method
