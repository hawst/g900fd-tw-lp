.class public Lcom/google/android/videos/utils/OfferUtil;
.super Ljava/lang/Object;
.source "OfferUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    }
.end annotation


# direct methods
.method public static getCheapest([Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    .locals 8
    .param p0, "offers"    # [Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    .prologue
    .line 64
    const/4 v0, 0x0

    .line 65
    .local v0, "cheapest":Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, p0

    if-ge v1, v3, :cond_2

    .line 66
    aget-object v2, p0, v1

    .line 67
    .local v2, "offer":Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    if-eqz v0, :cond_0

    iget-wide v4, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->priceMicros:J

    iget-wide v6, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->priceMicros:J

    cmp-long v3, v4, v6

    if-gez v3, :cond_1

    .line 68
    :cond_0
    move-object v0, v2

    .line 65
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 71
    .end local v2    # "offer":Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    :cond_2
    return-object v0
.end method

.method public static getCheapestOffer(Z[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;)Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    .locals 1
    .param p0, "preorderAllowed"    # Z
    .param p1, "offers"    # [Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    .prologue
    .line 75
    const/4 v0, -0x1

    invoke-static {p0, p1, v0}, Lcom/google/android/videos/utils/OfferUtil;->getCheapestOffer(Z[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;I)Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    move-result-object v0

    return-object v0
.end method

.method public static getCheapestOffer(Z[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;I)Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    .locals 10
    .param p0, "preorderAllowed"    # Z
    .param p1, "offers"    # [Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    .param p2, "offerType"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 80
    if-nez p1, :cond_0

    .line 98
    :goto_0
    return-object v5

    .line 83
    :cond_0
    const/4 v3, 0x0

    .line 84
    .local v3, "pricesCount":I
    const/4 v0, 0x0

    .line 85
    .local v0, "cheapest":Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v6, p1

    if-ge v1, v6, :cond_5

    .line 86
    aget-object v2, p1, v1

    .line 87
    .local v2, "offer":Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    const/4 v6, -0x1

    if-eq p2, v6, :cond_2

    iget v6, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->offerType:I

    if-eq v6, p2, :cond_2

    .line 85
    :cond_1
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 90
    :cond_2
    if-nez p0, :cond_3

    iget-boolean v6, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->preorder:Z

    if-nez v6, :cond_1

    .line 93
    :cond_3
    add-int/lit8 v3, v3, 0x1

    .line 94
    if-eqz v0, :cond_4

    iget-wide v6, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->priceMicros:J

    iget-wide v8, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->priceMicros:J

    cmp-long v6, v6, v8

    if-lez v6, :cond_1

    .line 95
    :cond_4
    move-object v0, v2

    goto :goto_2

    .line 98
    .end local v2    # "offer":Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    :cond_5
    if-nez v0, :cond_6

    move-object v4, v5

    :goto_3
    move-object v5, v4

    goto :goto_0

    :cond_6
    new-instance v5, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    if-ne v3, v4, :cond_7

    :goto_4
    invoke-direct {v5, v0, v4}, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;-><init>(Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;Z)V

    move-object v4, v5

    goto :goto_3

    :cond_7
    const/4 v4, 0x0

    goto :goto_4
.end method
