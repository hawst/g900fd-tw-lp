.class public Lcom/google/android/videos/pinning/TransferService;
.super Landroid/app/Service;
.source "TransferService.java"

# interfaces
.implements Lcom/google/android/videos/pinning/DownloadNotificationManager$OngoingNotificationHandler;
.implements Lcom/google/android/videos/pinning/TransfersExecutor$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/pinning/TransferService$Receiver;,
        Lcom/google/android/videos/pinning/TransferService$AlarmReceiver;
    }
.end annotation


# instance fields
.field private downloadNotificationManager:Lcom/google/android/videos/pinning/DownloadNotificationManager;

.field private handler:Landroid/os/Handler;

.field private mostRecentStartId:I

.field private pendingTicket:I

.field private pingIntervalMillis:J

.field private transfersExecutor:Lcom/google/android/videos/pinning/TransfersExecutor;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 165
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/pinning/TransferService;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pinning/TransferService;

    .prologue
    .line 30
    iget v0, p0, Lcom/google/android/videos/pinning/TransferService;->pendingTicket:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/pinning/TransferService;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pinning/TransferService;

    .prologue
    .line 30
    iget v0, p0, Lcom/google/android/videos/pinning/TransferService;->mostRecentStartId:I

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/pinning/TransferService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/pinning/TransferService;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/videos/pinning/TransferService;->scheduleRestart()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/videos/pinning/TransferService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/pinning/TransferService;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/videos/pinning/TransferService;->cancelRestart()V

    return-void
.end method

.method private cancelRestart()V
    .locals 2

    .prologue
    .line 144
    const/4 v1, 0x0

    # invokes: Lcom/google/android/videos/pinning/TransferService$Receiver;->setEnabled(Landroid/content/Context;Z)V
    invoke-static {p0, v1}, Lcom/google/android/videos/pinning/TransferService$Receiver;->access$400(Landroid/content/Context;Z)V

    .line 145
    const-string v1, "alarm"

    invoke-virtual {p0, v1}, Lcom/google/android/videos/pinning/TransferService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 146
    .local v0, "am":Landroid/app/AlarmManager;
    invoke-static {p0}, Lcom/google/android/videos/pinning/TransferService$AlarmReceiver;->createIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 147
    return-void
.end method

.method public static createIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/videos/pinning/TransferService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method private scheduleRestart()V
    .locals 6

    .prologue
    .line 133
    const/4 v1, 0x1

    # invokes: Lcom/google/android/videos/pinning/TransferService$Receiver;->setEnabled(Landroid/content/Context;Z)V
    invoke-static {p0, v1}, Lcom/google/android/videos/pinning/TransferService$Receiver;->access$400(Landroid/content/Context;Z)V

    .line 134
    const-string v1, "alarm"

    invoke-virtual {p0, v1}, Lcom/google/android/videos/pinning/TransferService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 135
    .local v0, "am":Landroid/app/AlarmManager;
    const/4 v1, 0x3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/videos/pinning/TransferService;->pingIntervalMillis:J

    add-long/2addr v2, v4

    invoke-static {p0}, Lcom/google/android/videos/pinning/TransferService$AlarmReceiver;->createIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 138
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 88
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 51
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 52
    const-string v2, "creating transfer service"

    invoke-static {v2}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 54
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v1

    .line 55
    .local v1, "videosGlobals":Lcom/google/android/videos/VideosGlobals;
    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v0

    .line 56
    .local v0, "config":Lcom/google/android/videos/Config;
    invoke-interface {v0}, Lcom/google/android/videos/Config;->transferServicePingIntervalMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/videos/pinning/TransferService;->pingIntervalMillis:J

    .line 57
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    iput-object v2, p0, Lcom/google/android/videos/pinning/TransferService;->handler:Landroid/os/Handler;

    .line 58
    new-instance v2, Lcom/google/android/videos/pinning/TransfersExecutor;

    invoke-direct {v2, v1, p0, p0}, Lcom/google/android/videos/pinning/TransfersExecutor;-><init>(Lcom/google/android/videos/VideosGlobals;Landroid/content/Context;Lcom/google/android/videos/pinning/TransfersExecutor$Listener;)V

    iput-object v2, p0, Lcom/google/android/videos/pinning/TransferService;->transfersExecutor:Lcom/google/android/videos/pinning/TransfersExecutor;

    .line 59
    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getDownloadNotificationManager()Lcom/google/android/videos/pinning/DownloadNotificationManager;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/pinning/TransferService;->downloadNotificationManager:Lcom/google/android/videos/pinning/DownloadNotificationManager;

    .line 60
    iget-object v2, p0, Lcom/google/android/videos/pinning/TransferService;->downloadNotificationManager:Lcom/google/android/videos/pinning/DownloadNotificationManager;

    if-eqz v2, :cond_0

    .line 61
    iget-object v2, p0, Lcom/google/android/videos/pinning/TransferService;->downloadNotificationManager:Lcom/google/android/videos/pinning/DownloadNotificationManager;

    invoke-virtual {v2, p0}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->setHandler(Lcom/google/android/videos/pinning/DownloadNotificationManager$OngoingNotificationHandler;)V

    .line 63
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransferService;->transfersExecutor:Lcom/google/android/videos/pinning/TransfersExecutor;

    invoke-virtual {v0}, Lcom/google/android/videos/pinning/TransfersExecutor;->quit()V

    .line 78
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransferService;->downloadNotificationManager:Lcom/google/android/videos/pinning/DownloadNotificationManager;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransferService;->downloadNotificationManager:Lcom/google/android/videos/pinning/DownloadNotificationManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->setHandler(Lcom/google/android/videos/pinning/DownloadNotificationManager$OngoingNotificationHandler;)V

    .line 81
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 82
    return-void
.end method

.method public onIdle(IZ)V
    .locals 2
    .param p1, "ticket"    # I
    .param p2, "haveLicenses"    # Z

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransferService;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/videos/pinning/TransferService$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/videos/pinning/TransferService$1;-><init>(Lcom/google/android/videos/pinning/TransferService;IZ)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 125
    return-void
.end method

.method public onOngoingNotification(ILandroid/app/Notification;)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "notification"    # Landroid/app/Notification;

    .prologue
    .line 95
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pinning/TransferService;->startForeground(ILandroid/app/Notification;)V

    .line 96
    return-void
.end method

.method public onOngoingNotificationCancelled()V
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/videos/pinning/TransferService;->stopForeground(Z)V

    .line 101
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/google/android/videos/pinning/TransferService;->scheduleRestart()V

    .line 70
    iput p3, p0, Lcom/google/android/videos/pinning/TransferService;->mostRecentStartId:I

    .line 71
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransferService;->transfersExecutor:Lcom/google/android/videos/pinning/TransfersExecutor;

    invoke-virtual {v0}, Lcom/google/android/videos/pinning/TransfersExecutor;->requestPing()I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/pinning/TransferService;->pendingTicket:I

    .line 72
    const/4 v0, 0x1

    return v0
.end method
