.class Lcom/google/android/videos/pano/binders/MovieBinder$1;
.super Ljava/lang/Object;
.source "MovieBinder.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/pano/binders/MovieBinder;->bind(Lcom/google/android/videos/pano/model/MovieItem;Lcom/google/android/videos/pano/ui/MovieCardView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pano/binders/MovieBinder;

.field final synthetic val$item:Lcom/google/android/videos/pano/model/MovieItem;

.field final synthetic val$view:Lcom/google/android/videos/pano/ui/MovieCardView;


# direct methods
.method constructor <init>(Lcom/google/android/videos/pano/binders/MovieBinder;Lcom/google/android/videos/pano/model/MovieItem;Lcom/google/android/videos/pano/ui/MovieCardView;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/videos/pano/binders/MovieBinder$1;->this$0:Lcom/google/android/videos/pano/binders/MovieBinder;

    iput-object p2, p0, Lcom/google/android/videos/pano/binders/MovieBinder$1;->val$item:Lcom/google/android/videos/pano/model/MovieItem;

    iput-object p3, p0, Lcom/google/android/videos/pano/binders/MovieBinder$1;->val$view:Lcom/google/android/videos/pano/ui/MovieCardView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 46
    iget-object v1, p0, Lcom/google/android/videos/pano/binders/MovieBinder$1;->val$item:Lcom/google/android/videos/pano/model/MovieItem;

    iget-object v0, v1, Lcom/google/android/videos/pano/model/MovieItem;->intent:Landroid/content/Intent;

    .line 47
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 48
    invoke-static {v0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->markAsRunningAnimation(Landroid/content/Intent;)V

    .line 49
    iget-object v1, p0, Lcom/google/android/videos/pano/binders/MovieBinder$1;->this$0:Lcom/google/android/videos/pano/binders/MovieBinder;

    # getter for: Lcom/google/android/videos/pano/binders/MovieBinder;->activity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/google/android/videos/pano/binders/MovieBinder;->access$000(Lcom/google/android/videos/pano/binders/MovieBinder;)Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/pano/binders/MovieBinder$1;->this$0:Lcom/google/android/videos/pano/binders/MovieBinder;

    # getter for: Lcom/google/android/videos/pano/binders/MovieBinder;->activity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/google/android/videos/pano/binders/MovieBinder;->access$000(Lcom/google/android/videos/pano/binders/MovieBinder;)Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/pano/binders/MovieBinder$1;->val$view:Lcom/google/android/videos/pano/ui/MovieCardView;

    invoke-virtual {v3}, Lcom/google/android/videos/pano/ui/MovieCardView;->getImageView()Landroid/widget/ImageView;

    move-result-object v3

    const-string v4, "hero"

    invoke-static {v2, v3, v4}, Landroid/support/v4/app/ActivityOptionsCompat;->makeSceneTransitionAnimation(Landroid/app/Activity;Landroid/view/View;Ljava/lang/String;)Landroid/support/v4/app/ActivityOptionsCompat;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/ActivityOptionsCompat;->toBundle()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 54
    :cond_0
    return-void
.end method
