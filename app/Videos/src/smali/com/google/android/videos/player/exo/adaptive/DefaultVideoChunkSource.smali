.class public Lcom/google/android/videos/player/exo/adaptive/DefaultVideoChunkSource;
.super Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;
.source "DefaultVideoChunkSource.java"


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/upstream/DataSource;Landroid/content/Context;Lcom/google/android/videos/Config;[Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;ZLcom/google/android/exoplayer/upstream/BandwidthMeter;Lcom/google/android/videos/utils/NetworkStatus;)V
    .locals 8
    .param p1, "dataSource"    # Lcom/google/android/exoplayer/upstream/DataSource;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "config"    # Lcom/google/android/videos/Config;
    .param p4, "representations"    # [Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    .param p5, "disableHdOnMobileNetwork"    # Z
    .param p6, "bandwidthMeter"    # Lcom/google/android/exoplayer/upstream/BandwidthMeter;
    .param p7, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;

    .prologue
    .line 33
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p6

    move-object v5, p7

    move v6, p5

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;-><init>(Lcom/google/android/exoplayer/upstream/DataSource;Landroid/content/Context;Lcom/google/android/videos/Config;Lcom/google/android/exoplayer/upstream/BandwidthMeter;Lcom/google/android/videos/utils/NetworkStatus;Z[Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;)V

    .line 35
    return-void
.end method


# virtual methods
.method public evaluate(Ljava/util/List;JJ[Lcom/google/android/exoplayer/chunk/Format;JZZZZLcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;)Z
    .locals 19
    .param p2, "seekPositionUs"    # J
    .param p4, "playbackPositionUs"    # J
    .param p6, "formats"    # [Lcom/google/android/exoplayer/chunk/Format;
    .param p7, "bitrateEstimate"    # J
    .param p9, "isFastNetwork"    # Z
    .param p10, "isHdDisabled"    # Z
    .param p11, "trickPlayEnabled"    # Z
    .param p12, "exitingTrickPlay"    # Z
    .param p13, "evaluation"    # Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/google/android/exoplayer/chunk/MediaChunk;",
            ">;JJ[",
            "Lcom/google/android/exoplayer/chunk/Format;",
            "JZZZZ",
            "Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 42
    .local p1, "queue":Ljava/util/List;, "Ljava/util/List<+Lcom/google/android/exoplayer/chunk/MediaChunk;>;"
    move-object/from16 v0, p13

    iget-object v12, v0, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->format:Lcom/google/android/exoplayer/chunk/Format;

    .line 43
    .local v12, "current":Lcom/google/android/exoplayer/chunk/Format;
    if-eqz p11, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/adaptive/DefaultVideoChunkSource;->formatSelectionHelper:Lcom/google/android/videos/player/exo/adaptive/FormatSelectionHelper;

    move-object/from16 v0, p6

    move-wide/from16 v1, p7

    invoke-virtual {v4, v0, v1, v2}, Lcom/google/android/videos/player/exo/adaptive/FormatSelectionHelper;->determineTrickPlayFormat([Lcom/google/android/exoplayer/chunk/Format;J)Lcom/google/android/exoplayer/chunk/Format;

    move-result-object v16

    .line 47
    .local v16, "selected":Lcom/google/android/exoplayer/chunk/Format;
    :goto_0
    if-nez p11, :cond_0

    if-eqz p12, :cond_4

    :cond_0
    const/16 v18, 0x4

    .line 50
    .local v18, "trigger":I
    :goto_1
    if-eqz v16, :cond_5

    if-eqz v12, :cond_5

    move-object/from16 v0, v16

    iget v4, v0, Lcom/google/android/exoplayer/chunk/Format;->bitrate:I

    iget v5, v12, Lcom/google/android/exoplayer/chunk/Format;->bitrate:I

    if-le v4, v5, :cond_5

    const/4 v14, 0x1

    .line 51
    .local v14, "isHigher":Z
    :goto_2
    if-eqz v16, :cond_6

    if-eqz v12, :cond_6

    move-object/from16 v0, v16

    iget v4, v0, Lcom/google/android/exoplayer/chunk/Format;->bitrate:I

    iget v5, v12, Lcom/google/android/exoplayer/chunk/Format;->bitrate:I

    if-ge v4, v5, :cond_6

    const/4 v15, 0x1

    .line 52
    .local v15, "isLower":Z
    :goto_3
    if-eqz v14, :cond_a

    .line 53
    if-eqz p12, :cond_7

    move-object/from16 v0, p13

    iget v4, v0, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->queueSize:I

    const/4 v5, 0x1

    if-le v4, v5, :cond_7

    .line 55
    const/16 p12, 0x0

    .line 56
    iget v4, v12, Lcom/google/android/exoplayer/chunk/Format;->height:I

    const/16 v5, 0x2d0

    if-ge v4, v5, :cond_1

    iget v4, v12, Lcom/google/android/exoplayer/chunk/Format;->height:I

    move-object/from16 v0, v16

    iget v5, v0, Lcom/google/android/exoplayer/chunk/Format;->height:I

    if-ge v4, v5, :cond_1

    .line 57
    const/4 v4, 0x2

    move-object/from16 v0, p13

    iput v4, v0, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->queueSize:I

    .line 82
    :cond_1
    :goto_4
    if-eqz v12, :cond_2

    move-object/from16 v0, v16

    if-eq v0, v12, :cond_2

    .line 83
    move/from16 v0, v18

    move-object/from16 v1, p13

    iput v0, v1, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->trigger:I

    .line 85
    :cond_2
    move-object/from16 v0, v16

    move-object/from16 v1, p13

    iput-object v0, v1, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->format:Lcom/google/android/exoplayer/chunk/Format;

    .line 86
    return p12

    .line 43
    .end local v14    # "isHigher":Z
    .end local v15    # "isLower":Z
    .end local v16    # "selected":Lcom/google/android/exoplayer/chunk/Format;
    .end local v18    # "trigger":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/adaptive/DefaultVideoChunkSource;->formatSelectionHelper:Lcom/google/android/videos/player/exo/adaptive/FormatSelectionHelper;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/player/exo/adaptive/DefaultVideoChunkSource;->getHistoricalBitrateEstimate()J

    move-result-wide v8

    move-object/from16 v5, p6

    move-wide/from16 v6, p7

    move/from16 v10, p10

    move/from16 v11, p9

    invoke-virtual/range {v4 .. v11}, Lcom/google/android/videos/player/exo/adaptive/FormatSelectionHelper;->determineIdealFormat([Lcom/google/android/exoplayer/chunk/Format;JJZZ)Lcom/google/android/exoplayer/chunk/Format;

    move-result-object v16

    goto :goto_0

    .line 47
    .restart local v16    # "selected":Lcom/google/android/exoplayer/chunk/Format;
    :cond_4
    const/16 v18, 0x2

    goto :goto_1

    .line 50
    .restart local v18    # "trigger":I
    :cond_5
    const/4 v14, 0x0

    goto :goto_2

    .line 51
    .restart local v14    # "isHigher":Z
    :cond_6
    const/4 v15, 0x0

    goto :goto_3

    .line 59
    .restart local v15    # "isLower":Z
    :cond_7
    move-object/from16 v0, p13

    iget v4, v0, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->queueSize:I

    const/4 v5, 0x3

    if-ge v4, v5, :cond_8

    .line 62
    move-object/from16 v16, v12

    goto :goto_4

    .line 67
    :cond_8
    const/4 v13, 0x5

    .local v13, "i":I
    :goto_5
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v13, v4, :cond_1

    .line 68
    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/android/exoplayer/chunk/MediaChunk;

    .line 69
    .local v17, "thisChunk":Lcom/google/android/exoplayer/chunk/MediaChunk;
    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/google/android/exoplayer/chunk/MediaChunk;->format:Lcom/google/android/exoplayer/chunk/Format;

    iget v4, v4, Lcom/google/android/exoplayer/chunk/Format;->height:I

    const/16 v5, 0x2d0

    if-ge v4, v5, :cond_9

    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/google/android/exoplayer/chunk/MediaChunk;->format:Lcom/google/android/exoplayer/chunk/Format;

    iget v4, v4, Lcom/google/android/exoplayer/chunk/Format;->height:I

    move-object/from16 v0, v16

    iget v5, v0, Lcom/google/android/exoplayer/chunk/Format;->height:I

    if-ge v4, v5, :cond_9

    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/google/android/exoplayer/chunk/MediaChunk;->format:Lcom/google/android/exoplayer/chunk/Format;

    iget v4, v4, Lcom/google/android/exoplayer/chunk/Format;->bitrate:I

    move-object/from16 v0, v16

    iget v5, v0, Lcom/google/android/exoplayer/chunk/Format;->bitrate:I

    if-ge v4, v5, :cond_9

    .line 72
    move-object/from16 v0, p13

    iput v13, v0, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->queueSize:I

    goto :goto_4

    .line 67
    :cond_9
    add-int/lit8 v13, v13, 0x1

    goto :goto_5

    .line 77
    .end local v13    # "i":I
    .end local v17    # "thisChunk":Lcom/google/android/exoplayer/chunk/MediaChunk;
    :cond_a
    if-eqz v12, :cond_1

    if-eqz v15, :cond_1

    move-object/from16 v0, p13

    iget v4, v0, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->queueSize:I

    const/4 v5, 0x5

    if-lt v4, v5, :cond_1

    .line 80
    move-object/from16 v16, v12

    goto :goto_4
.end method
