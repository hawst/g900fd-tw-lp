.class Lcom/google/android/videos/ui/DefaultItemAnimator$7;
.super Ljava/lang/Object;
.source "DefaultItemAnimator.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/ui/DefaultItemAnimator;->runPendingAnimations()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/ui/DefaultItemAnimator;

.field final synthetic val$changes:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/google/android/videos/ui/DefaultItemAnimator;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 374
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator$7;, "Lcom/google/android/videos/ui/DefaultItemAnimator.7;"
    iput-object p1, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$7;->this$0:Lcom/google/android/videos/ui/DefaultItemAnimator;

    iput-object p2, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$7;->val$changes:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 377
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator$7;, "Lcom/google/android/videos/ui/DefaultItemAnimator.7;"
    iget-object v2, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$7;->val$changes:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;

    .line 378
    .local v0, "change":Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;, "Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo<TT;>;"
    iget-object v2, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$7;->this$0:Lcom/google/android/videos/ui/DefaultItemAnimator;

    # invokes: Lcom/google/android/videos/ui/DefaultItemAnimator;->animateChangeImpl(Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;)V
    invoke-static {v2, v0}, Lcom/google/android/videos/ui/DefaultItemAnimator;->access$600(Lcom/google/android/videos/ui/DefaultItemAnimator;Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;)V

    goto :goto_0

    .line 380
    .end local v0    # "change":Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;, "Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo<TT;>;"
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$7;->val$changes:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 381
    iget-object v2, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$7;->this$0:Lcom/google/android/videos/ui/DefaultItemAnimator;

    # getter for: Lcom/google/android/videos/ui/DefaultItemAnimator;->mChangesList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/google/android/videos/ui/DefaultItemAnimator;->access$700(Lcom/google/android/videos/ui/DefaultItemAnimator;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$7;->val$changes:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 382
    return-void
.end method
