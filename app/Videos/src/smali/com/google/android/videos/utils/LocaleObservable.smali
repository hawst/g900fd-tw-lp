.class public final Lcom/google/android/videos/utils/LocaleObservable;
.super Landroid/content/BroadcastReceiver;
.source "LocaleObservable.java"

# interfaces
.implements Lcom/google/android/repolib/observers/Observable;
.implements Lcom/google/android/repolib/observers/UpdatablesChanged;


# instance fields
.field private final context:Landroid/content/Context;

.field private final updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 26
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/videos/utils/LocaleObservable;->context:Landroid/content/Context;

    .line 27
    invoke-static {p0}, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->asyncUpdateDispatcher(Lcom/google/android/repolib/observers/UpdatablesChanged;)Lcom/google/android/repolib/observers/UpdateDispatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/utils/LocaleObservable;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    .line 28
    return-void
.end method

.method public static localeObservable(Landroid/content/Context;)Lcom/google/android/repolib/observers/Observable;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    new-instance v0, Lcom/google/android/videos/utils/LocaleObservable;

    invoke-direct {v0, p0}, Lcom/google/android/videos/utils/LocaleObservable;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/videos/utils/LocaleObservable;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 52
    return-void
.end method

.method public firstUpdatableAdded()V
    .locals 3

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/videos/utils/LocaleObservable;->context:Landroid/content/Context;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.LOCALE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 37
    return-void
.end method

.method public lastUpdatableRemoved()V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/videos/utils/LocaleObservable;->context:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 42
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/videos/utils/LocaleObservable;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0}, Lcom/google/android/repolib/observers/UpdateDispatcher;->update()V

    .line 47
    return-void
.end method

.method public removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/videos/utils/LocaleObservable;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 57
    return-void
.end method
