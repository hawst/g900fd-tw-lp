.class Lcom/google/android/videos/ui/StatusHelper$ViewStatusHelper;
.super Lcom/google/android/videos/ui/StatusHelper;
.source "StatusHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/StatusHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ViewStatusHelper"
.end annotation


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/view/View;Lcom/google/android/videos/ui/StatusHelper$OnRetryListener;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "statusView"    # Landroid/view/View;
    .param p3, "retryListener"    # Lcom/google/android/videos/ui/StatusHelper$OnRetryListener;

    .prologue
    .line 147
    invoke-direct {p0, p1, p3}, Lcom/google/android/videos/ui/StatusHelper;-><init>(Landroid/content/Context;Lcom/google/android/videos/ui/StatusHelper$OnRetryListener;)V

    .line 148
    iput-object p2, p0, Lcom/google/android/videos/ui/StatusHelper$ViewStatusHelper;->statusView:Landroid/view/View;

    .line 149
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Landroid/view/View;Lcom/google/android/videos/ui/StatusHelper$OnRetryListener;Lcom/google/android/videos/ui/StatusHelper$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Context;
    .param p2, "x1"    # Landroid/view/View;
    .param p3, "x2"    # Lcom/google/android/videos/ui/StatusHelper$OnRetryListener;
    .param p4, "x3"    # Lcom/google/android/videos/ui/StatusHelper$1;

    .prologue
    .line 145
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/ui/StatusHelper$ViewStatusHelper;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/google/android/videos/ui/StatusHelper$OnRetryListener;)V

    return-void
.end method


# virtual methods
.method protected ensureHidden()V
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/videos/ui/StatusHelper$ViewStatusHelper;->statusView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 159
    return-void
.end method

.method protected ensureVisible()V
    .locals 2

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/videos/ui/StatusHelper$ViewStatusHelper;->statusView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 154
    return-void
.end method

.method public init()V
    .locals 0

    .prologue
    .line 168
    invoke-super {p0}, Lcom/google/android/videos/ui/StatusHelper;->init()V

    .line 169
    invoke-virtual {p0}, Lcom/google/android/videos/ui/StatusHelper$ViewStatusHelper;->ensureHidden()V

    .line 170
    return-void
.end method
