.class public Lcom/google/android/videos/api/UpdateWishlistRequest;
.super Lcom/google/android/videos/async/Request;
.source "UpdateWishlistRequest.java"


# instance fields
.field public final assetResourceId:Ljava/lang/String;

.field public final wishlisted:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "assetResourceId"    # Ljava/lang/String;
    .param p3, "wishlisted"    # Z

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/google/android/videos/async/Request;-><init>(Ljava/lang/String;)V

    .line 19
    iput-object p2, p0, Lcom/google/android/videos/api/UpdateWishlistRequest;->assetResourceId:Ljava/lang/String;

    .line 20
    iput-boolean p3, p0, Lcom/google/android/videos/api/UpdateWishlistRequest;->wishlisted:Z

    .line 21
    return-void
.end method
