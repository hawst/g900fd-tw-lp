.class Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter$2;
.super Ljava/lang/Object;
.source "BaseDetailsActivity.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter;->onBindRowViewHolder(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter;


# direct methods
.method constructor <init>(Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter;)V
    .locals 0

    .prologue
    .line 600
    iput-object p1, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter$2;->this$1:Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I
    .param p6, "oldLeft"    # I
    .param p7, "oldTop"    # I
    .param p8, "oldRight"    # I
    .param p9, "oldBottom"    # I

    .prologue
    .line 604
    sub-int v2, p5, p3

    .line 605
    .local v2, "height":I
    :goto_0
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    const v5, 0x7f0f0167

    if-eq v4, v5, :cond_0

    .line 606
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    .end local p1    # "view":Landroid/view/View;
    check-cast p1, Landroid/view/View;

    .restart local p1    # "view":Landroid/view/View;
    goto :goto_0

    .line 608
    :cond_0
    if-eqz p1, :cond_1

    move-object v1, p1

    .line 609
    check-cast v1, Landroid/support/v17/leanback/widget/VerticalGridView;

    .line 610
    .local v1, "gridView":Landroid/support/v17/leanback/widget/VerticalGridView;
    iget-object v4, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter$2;->this$1:Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter;

    iget-object v4, v4, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    invoke-virtual {v4}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 611
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    sub-int/2addr v4, v2

    div-int/lit8 v4, v4, 0x2

    iget-object v5, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter$2;->this$1:Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter;

    iget-object v5, v5, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    invoke-virtual {v5}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e01ba

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 613
    .local v3, "topMargin":I
    invoke-virtual {v1, v3}, Landroid/support/v17/leanback/widget/VerticalGridView;->setWindowAlignmentOffset(I)V

    .line 616
    .end local v0    # "displayMetrics":Landroid/util/DisplayMetrics;
    .end local v1    # "gridView":Landroid/support/v17/leanback/widget/VerticalGridView;
    .end local v3    # "topMargin":I
    :cond_1
    iget-object v4, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter$2;->this$1:Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter;

    iget-object v4, v4, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    iget-boolean v4, v4, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->pendingEnterTransition:Z

    if-eqz v4, :cond_2

    .line 617
    iget-object v4, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter$2;->this$1:Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter;

    iget-object v4, v4, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    # getter for: Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->handler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->access$300(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)Landroid/os/Handler;

    move-result-object v4

    new-instance v5, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter$2$1;

    invoke-direct {v5, p0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter$2$1;-><init>(Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter$2;)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 624
    :cond_2
    return-void
.end method
