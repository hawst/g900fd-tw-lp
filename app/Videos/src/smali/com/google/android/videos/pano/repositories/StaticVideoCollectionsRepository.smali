.class public final Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;
.super Ljava/lang/Object;
.source "StaticVideoCollectionsRepository.java"

# interfaces
.implements Lcom/google/android/repolib/observers/Updatable;
.implements Lcom/google/android/repolib/observers/UpdatablesChanged;
.implements Lcom/google/android/repolib/repositories/Repository;
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/observers/Updatable;",
        "Lcom/google/android/repolib/observers/UpdatablesChanged;",
        "Lcom/google/android/repolib/repositories/Repository",
        "<",
        "Lcom/google/android/videos/pano/model/VideoItem;",
        ">;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/api/VideoCollectionGetRequest;",
        "Lcom/google/wireless/android/video/magma/proto/VideoCollectionGetResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final categoryId:Ljava/lang/String;

.field private final collectionGetRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/VideoCollectionGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoCollectionGetResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final collectionId:Ljava/lang/String;

.field private final configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

.field private final context:Landroid/content/Context;

.field private final localeObservable:Lcom/google/android/repolib/observers/Observable;

.field private final maxResults:I

.field private final networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

.field private final repository:Lcom/google/android/repolib/repositories/ArrayRepository;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/repositories/ArrayRepository",
            "<",
            "Lcom/google/android/videos/pano/model/VideoItem;",
            ">;"
        }
    .end annotation
.end field

.field private final signInManager:Lcom/google/android/videos/accounts/SignInManager;

.field private final storeStatusObservable:Lcom/google/android/videos/pano/repositories/StoreStatusObservable;

.field private final updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;


# direct methods
.method private constructor <init>(ILjava/lang/String;Ljava/lang/String;Landroid/content/Context;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/repolib/observers/Observable;Lcom/google/android/videos/async/Requester;)V
    .locals 7
    .param p1, "maxResults"    # I
    .param p2, "collectionId"    # Ljava/lang/String;
    .param p3, "categoryId"    # Ljava/lang/String;
    .param p4, "context"    # Landroid/content/Context;
    .param p5, "signInManager"    # Lcom/google/android/videos/accounts/SignInManager;
    .param p6, "configurationStore"    # Lcom/google/android/videos/store/ConfigurationStore;
    .param p7, "database"    # Lcom/google/android/videos/store/Database;
    .param p8, "purchaseStore"    # Lcom/google/android/videos/store/PurchaseStore;
    .param p9, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;
    .param p10, "localeObservable"    # Lcom/google/android/repolib/observers/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            "Lcom/google/android/videos/accounts/SignInManager;",
            "Lcom/google/android/videos/store/ConfigurationStore;",
            "Lcom/google/android/videos/store/Database;",
            "Lcom/google/android/videos/store/PurchaseStore;",
            "Lcom/google/android/videos/utils/NetworkStatus;",
            "Lcom/google/android/repolib/observers/Observable;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/VideoCollectionGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoCollectionGetResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 110
    .local p11, "collectionGetRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/VideoCollectionGetRequest;Lcom/google/wireless/android/video/magma/proto/VideoCollectionGetResponse;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    invoke-static {}, Lcom/google/android/repolib/repositories/ArrayRepository;->arrayRepository()Lcom/google/android/repolib/repositories/ArrayRepository;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    .line 111
    iput p1, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->maxResults:I

    .line 112
    iput-object p4, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->context:Landroid/content/Context;

    .line 113
    iput-object p2, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->collectionId:Ljava/lang/String;

    .line 114
    iput-object p3, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->categoryId:Ljava/lang/String;

    .line 115
    invoke-static {p0}, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->asyncUpdateDispatcher(Lcom/google/android/repolib/observers/UpdatablesChanged;)Lcom/google/android/repolib/observers/UpdateDispatcher;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    .line 116
    iput-object p5, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    .line 117
    iput-object p6, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    .line 118
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->collectionGetRequester:Lcom/google/android/videos/async/Requester;

    .line 119
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 120
    new-instance v1, Lcom/google/android/videos/pano/repositories/StoreStatusObservable;

    const/4 v6, 0x0

    move-object v2, p5

    move-object v3, p4

    move-object v4, p7

    move-object v5, p8

    invoke-direct/range {v1 .. v6}, Lcom/google/android/videos/pano/repositories/StoreStatusObservable;-><init>(Lcom/google/android/videos/accounts/SignInManager;Landroid/content/Context;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/WishlistStore;)V

    iput-object v1, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->storeStatusObservable:Lcom/google/android/videos/pano/repositories/StoreStatusObservable;

    .line 122
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->localeObservable:Lcom/google/android/repolib/observers/Observable;

    .line 123
    return-void
.end method

.method public static createGenreTopMoviesRepository(ILjava/lang/String;Landroid/content/Context;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/repolib/observers/Observable;Lcom/google/android/videos/async/Requester;)Lcom/google/android/repolib/repositories/Repository;
    .locals 12
    .param p0, "maxResults"    # I
    .param p1, "categoryId"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "signInManager"    # Lcom/google/android/videos/accounts/SignInManager;
    .param p4, "configurationStore"    # Lcom/google/android/videos/store/ConfigurationStore;
    .param p5, "database"    # Lcom/google/android/videos/store/Database;
    .param p6, "purchaseStore"    # Lcom/google/android/videos/store/PurchaseStore;
    .param p7, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;
    .param p8, "localeObservable"    # Lcom/google/android/repolib/observers/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            "Lcom/google/android/videos/accounts/SignInManager;",
            "Lcom/google/android/videos/store/ConfigurationStore;",
            "Lcom/google/android/videos/store/Database;",
            "Lcom/google/android/videos/store/PurchaseStore;",
            "Lcom/google/android/videos/utils/NetworkStatus;",
            "Lcom/google/android/repolib/observers/Observable;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/VideoCollectionGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoCollectionGetResponse;",
            ">;)",
            "Lcom/google/android/repolib/repositories/Repository",
            "<",
            "Lcom/google/android/videos/pano/model/VideoItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    .local p9, "collectionGetRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/VideoCollectionGetRequest;Lcom/google/wireless/android/video/magma/proto/VideoCollectionGetResponse;>;"
    new-instance v0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;

    const-string v2, "topselling_paid"

    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move v1, p0

    move-object v4, p2

    move-object v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    invoke-direct/range {v0 .. v11}, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;-><init>(ILjava/lang/String;Ljava/lang/String;Landroid/content/Context;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/repolib/observers/Observable;Lcom/google/android/videos/async/Requester;)V

    return-object v0
.end method

.method public static createGenreTopShowsRepository(ILjava/lang/String;Landroid/content/Context;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/repolib/observers/Observable;Lcom/google/android/videos/async/Requester;)Lcom/google/android/repolib/repositories/Repository;
    .locals 12
    .param p0, "maxResults"    # I
    .param p1, "categoryId"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "signInManager"    # Lcom/google/android/videos/accounts/SignInManager;
    .param p4, "configurationStore"    # Lcom/google/android/videos/store/ConfigurationStore;
    .param p5, "database"    # Lcom/google/android/videos/store/Database;
    .param p6, "purchaseStore"    # Lcom/google/android/videos/store/PurchaseStore;
    .param p7, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;
    .param p8, "localeObservable"    # Lcom/google/android/repolib/observers/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            "Lcom/google/android/videos/accounts/SignInManager;",
            "Lcom/google/android/videos/store/ConfigurationStore;",
            "Lcom/google/android/videos/store/Database;",
            "Lcom/google/android/videos/store/PurchaseStore;",
            "Lcom/google/android/videos/utils/NetworkStatus;",
            "Lcom/google/android/repolib/observers/Observable;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/VideoCollectionGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoCollectionGetResponse;",
            ">;)",
            "Lcom/google/android/repolib/repositories/Repository",
            "<",
            "Lcom/google/android/videos/pano/model/VideoItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    .local p9, "collectionGetRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/VideoCollectionGetRequest;Lcom/google/wireless/android/video/magma/proto/VideoCollectionGetResponse;>;"
    new-instance v0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;

    const-string v2, "topselling_paid_show"

    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move v1, p0

    move-object v4, p2

    move-object v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    invoke-direct/range {v0 .. v11}, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;-><init>(ILjava/lang/String;Ljava/lang/String;Landroid/content/Context;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/repolib/observers/Observable;Lcom/google/android/videos/async/Requester;)V

    return-object v0
.end method

.method public static createTopMoviesRepository(ILandroid/content/Context;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/repolib/observers/Observable;Lcom/google/android/videos/async/Requester;)Lcom/google/android/repolib/repositories/Repository;
    .locals 12
    .param p0, "maxResults"    # I
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "signInManager"    # Lcom/google/android/videos/accounts/SignInManager;
    .param p3, "configurationStore"    # Lcom/google/android/videos/store/ConfigurationStore;
    .param p4, "database"    # Lcom/google/android/videos/store/Database;
    .param p5, "purchaseStore"    # Lcom/google/android/videos/store/PurchaseStore;
    .param p6, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;
    .param p7, "localeObservable"    # Lcom/google/android/repolib/observers/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/content/Context;",
            "Lcom/google/android/videos/accounts/SignInManager;",
            "Lcom/google/android/videos/store/ConfigurationStore;",
            "Lcom/google/android/videos/store/Database;",
            "Lcom/google/android/videos/store/PurchaseStore;",
            "Lcom/google/android/videos/utils/NetworkStatus;",
            "Lcom/google/android/repolib/observers/Observable;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/VideoCollectionGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoCollectionGetResponse;",
            ">;)",
            "Lcom/google/android/repolib/repositories/Repository",
            "<",
            "Lcom/google/android/videos/pano/model/VideoItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    .local p8, "collectionGetRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/VideoCollectionGetRequest;Lcom/google/wireless/android/video/magma/proto/VideoCollectionGetResponse;>;"
    new-instance v0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;

    const-string v2, "topselling_paid"

    const/4 v3, 0x0

    move v1, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    invoke-direct/range {v0 .. v11}, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;-><init>(ILjava/lang/String;Ljava/lang/String;Landroid/content/Context;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/repolib/observers/Observable;Lcom/google/android/videos/async/Requester;)V

    return-object v0
.end method

.method public static createTopShowsRepository(ILandroid/content/Context;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/repolib/observers/Observable;Lcom/google/android/videos/async/Requester;)Lcom/google/android/repolib/repositories/Repository;
    .locals 12
    .param p0, "maxResults"    # I
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "signInManager"    # Lcom/google/android/videos/accounts/SignInManager;
    .param p3, "configurationStore"    # Lcom/google/android/videos/store/ConfigurationStore;
    .param p4, "database"    # Lcom/google/android/videos/store/Database;
    .param p5, "purchaseStore"    # Lcom/google/android/videos/store/PurchaseStore;
    .param p6, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;
    .param p7, "localeObservable"    # Lcom/google/android/repolib/observers/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/content/Context;",
            "Lcom/google/android/videos/accounts/SignInManager;",
            "Lcom/google/android/videos/store/ConfigurationStore;",
            "Lcom/google/android/videos/store/Database;",
            "Lcom/google/android/videos/store/PurchaseStore;",
            "Lcom/google/android/videos/utils/NetworkStatus;",
            "Lcom/google/android/repolib/observers/Observable;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/VideoCollectionGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoCollectionGetResponse;",
            ">;)",
            "Lcom/google/android/repolib/repositories/Repository",
            "<",
            "Lcom/google/android/videos/pano/model/VideoItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    .local p8, "collectionGetRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/VideoCollectionGetRequest;Lcom/google/wireless/android/video/magma/proto/VideoCollectionGetResponse;>;"
    new-instance v0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;

    const-string v2, "topselling_paid_show"

    const/4 v3, 0x0

    move v1, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    invoke-direct/range {v0 .. v11}, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;-><init>(ILjava/lang/String;Ljava/lang/String;Landroid/content/Context;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/repolib/observers/Observable;Lcom/google/android/videos/async/Requester;)V

    return-object v0
.end method


# virtual methods
.method public addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 147
    return-void
.end method

.method public firstUpdatableAdded()V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/repolib/repositories/ArrayRepository;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 128
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->storeStatusObservable:Lcom/google/android/videos/pano/repositories/StoreStatusObservable;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/pano/repositories/StoreStatusObservable;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 129
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/accounts/SignInManager;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 130
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0, p0}, Lcom/google/android/videos/utils/NetworkStatus;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 131
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->localeObservable:Lcom/google/android/repolib/observers/Observable;

    invoke-interface {v0, p0}, Lcom/google/android/repolib/observers/Observable;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 132
    invoke-virtual {p0}, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->update()V

    .line 133
    return-void
.end method

.method public indexer()Lcom/google/android/repolib/common/Indexer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/repolib/common/Indexer",
            "<",
            "Lcom/google/android/videos/pano/model/VideoItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    invoke-virtual {v0}, Lcom/google/android/repolib/repositories/ArrayRepository;->indexer()Lcom/google/android/repolib/common/Indexer;

    move-result-object v0

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/android/videos/pano/model/VideoItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    invoke-virtual {v0}, Lcom/google/android/repolib/repositories/ArrayRepository;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public lastUpdatableRemoved()V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->localeObservable:Lcom/google/android/repolib/observers/Observable;

    invoke-interface {v0, p0}, Lcom/google/android/repolib/observers/Observable;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 138
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/repolib/repositories/ArrayRepository;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 139
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->storeStatusObservable:Lcom/google/android/videos/pano/repositories/StoreStatusObservable;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/pano/repositories/StoreStatusObservable;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 140
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/accounts/SignInManager;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 141
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0, p0}, Lcom/google/android/videos/utils/NetworkStatus;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 142
    return-void
.end method

.method public onError(Lcom/google/android/videos/api/VideoCollectionGetRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/api/VideoCollectionGetRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    invoke-virtual {v0}, Lcom/google/android/repolib/repositories/ArrayRepository;->clear()V

    .line 189
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 36
    check-cast p1, Lcom/google/android/videos/api/VideoCollectionGetRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->onError(Lcom/google/android/videos/api/VideoCollectionGetRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/api/VideoCollectionGetRequest;Lcom/google/wireless/android/video/magma/proto/VideoCollectionGetResponse;)V
    .locals 4
    .param p1, "request"    # Lcom/google/android/videos/api/VideoCollectionGetRequest;
    .param p2, "response"    # Lcom/google/wireless/android/video/magma/proto/VideoCollectionGetResponse;

    .prologue
    .line 181
    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->context:Landroid/content/Context;

    iget-object v2, p2, Lcom/google/wireless/android/video/magma/proto/VideoCollectionGetResponse;->resource:Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    iget-object v3, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->storeStatusObservable:Lcom/google/android/videos/pano/repositories/StoreStatusObservable;

    invoke-virtual {v3}, Lcom/google/android/videos/pano/repositories/StoreStatusObservable;->getStoreStatusMonitor()Lcom/google/android/videos/store/StoreStatusMonitor;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/videos/pano/ui/VideoCollectionUtil;->getItemsFromVideoCollection(Landroid/content/Context;Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;Lcom/google/android/videos/store/StoreStatusMonitor;)[Lcom/google/android/videos/pano/model/VideoItem;

    move-result-object v0

    .line 183
    .local v0, "items":[Lcom/google/android/videos/pano/model/VideoItem;
    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/repolib/repositories/ArrayRepository;->setItems(Ljava/util/Collection;)V

    .line 184
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 36
    check-cast p1, Lcom/google/android/videos/api/VideoCollectionGetRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/wireless/android/video/magma/proto/VideoCollectionGetResponse;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->onResponse(Lcom/google/android/videos/api/VideoCollectionGetRequest;Lcom/google/wireless/android/video/magma/proto/VideoCollectionGetResponse;)V

    return-void
.end method

.method public removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 152
    return-void
.end method

.method public update()V
    .locals 8

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    invoke-virtual {v0}, Lcom/google/android/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v1

    .line 157
    .local v1, "account":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 158
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    iget-object v7, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->collectionGetRequester:Lcom/google/android/videos/async/Requester;

    new-instance v0, Lcom/google/android/videos/api/VideoCollectionGetRequest;

    iget-object v2, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->collectionId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    invoke-virtual {v3, v1}, Lcom/google/android/videos/store/ConfigurationStore;->getPlayCountry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->categoryId:Ljava/lang/String;

    iget v6, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->maxResults:I

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/api/VideoCollectionGetRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;Ljava/lang/String;I)V

    invoke-interface {v7, v0, p0}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 166
    :cond_0
    :goto_0
    return-void

    .line 164
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    invoke-virtual {v0}, Lcom/google/android/repolib/repositories/ArrayRepository;->clear()V

    goto :goto_0
.end method
