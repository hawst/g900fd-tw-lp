.class public Lcom/google/android/videos/ui/ShowHeaderLayout;
.super Landroid/widget/LinearLayout;
.source "ShowHeaderLayout.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 26
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 28
    invoke-virtual {p0}, Lcom/google/android/videos/ui/ShowHeaderLayout;->getChildCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 29
    invoke-virtual {p0}, Lcom/google/android/videos/ui/ShowHeaderLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    .line 30
    .local v0, "lp":Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/videos/ui/ShowHeaderLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/play/utils/Compound;->intLengthToCompound(I)I

    move-result v1

    iput v1, v0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetTopCompound:I

    .line 32
    .end local v0    # "lp":Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
    :cond_0
    return-void
.end method
