.class public abstract Lcom/google/android/videos/remote/LockScreenTransportControl;
.super Lcom/google/android/videos/remote/TransportControl;
.source "LockScreenTransportControl.java"


# direct methods
.method public constructor <init>(Lcom/google/android/videos/remote/RemoteTracker;)V
    .locals 0
    .param p1, "remoteTracker"    # Lcom/google/android/videos/remote/RemoteTracker;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/google/android/videos/remote/TransportControl;-><init>(Lcom/google/android/videos/remote/RemoteTracker;)V

    .line 17
    return-void
.end method

.method public static newInstanceOrNull(Lcom/google/android/videos/remote/RemoteTracker;Landroid/content/Context;)Lcom/google/android/videos/remote/LockScreenTransportControlV14;
    .locals 2
    .param p0, "remoteTracker"    # Lcom/google/android/videos/remote/RemoteTracker;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    .line 26
    new-instance v0, Lcom/google/android/videos/remote/LockScreenTransportControlV18;

    invoke-direct {v0, p0, p1}, Lcom/google/android/videos/remote/LockScreenTransportControlV18;-><init>(Lcom/google/android/videos/remote/RemoteTracker;Landroid/content/Context;)V

    .line 32
    :goto_0
    return-object v0

    .line 27
    :cond_0
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    .line 28
    new-instance v0, Lcom/google/android/videos/remote/LockScreenTransportControlV16;

    invoke-direct {v0, p0, p1}, Lcom/google/android/videos/remote/LockScreenTransportControlV16;-><init>(Lcom/google/android/videos/remote/RemoteTracker;Landroid/content/Context;)V

    goto :goto_0

    .line 32
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected getRemoteTracker()Lcom/google/android/videos/remote/RemoteTracker;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/videos/remote/LockScreenTransportControl;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    return-object v0
.end method
