.class public final Lcom/google/android/videos/proto/FilmProtos$Character;
.super Lcom/google/protobuf/nano/MessageNano;
.source "FilmProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/proto/FilmProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Character"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/videos/proto/FilmProtos$Character;


# instance fields
.field public image:Lcom/google/android/videos/proto/FilmProtos$Image;

.field public mid:Ljava/lang/String;

.field public name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1421
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1422
    invoke-virtual {p0}, Lcom/google/android/videos/proto/FilmProtos$Character;->clear()Lcom/google/android/videos/proto/FilmProtos$Character;

    .line 1423
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/videos/proto/FilmProtos$Character;
    .locals 2

    .prologue
    .line 1401
    sget-object v0, Lcom/google/android/videos/proto/FilmProtos$Character;->_emptyArray:[Lcom/google/android/videos/proto/FilmProtos$Character;

    if-nez v0, :cond_1

    .line 1402
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 1404
    :try_start_0
    sget-object v0, Lcom/google/android/videos/proto/FilmProtos$Character;->_emptyArray:[Lcom/google/android/videos/proto/FilmProtos$Character;

    if-nez v0, :cond_0

    .line 1405
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/videos/proto/FilmProtos$Character;

    sput-object v0, Lcom/google/android/videos/proto/FilmProtos$Character;->_emptyArray:[Lcom/google/android/videos/proto/FilmProtos$Character;

    .line 1407
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1409
    :cond_1
    sget-object v0, Lcom/google/android/videos/proto/FilmProtos$Character;->_emptyArray:[Lcom/google/android/videos/proto/FilmProtos$Character;

    return-object v0

    .line 1407
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/videos/proto/FilmProtos$Character;
    .locals 1

    .prologue
    .line 1426
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->mid:Ljava/lang/String;

    .line 1427
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->name:Ljava/lang/String;

    .line 1428
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    .line 1429
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->cachedSize:I

    .line 1430
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1497
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1498
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->mid:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1499
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->mid:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1502
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->name:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1503
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->name:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1506
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    if-eqz v1, :cond_2

    .line 1507
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1510
    :cond_2
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1435
    if-ne p1, p0, :cond_1

    .line 1465
    :cond_0
    :goto_0
    return v1

    .line 1438
    :cond_1
    instance-of v3, p1, Lcom/google/android/videos/proto/FilmProtos$Character;

    if-nez v3, :cond_2

    move v1, v2

    .line 1439
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 1441
    check-cast v0, Lcom/google/android/videos/proto/FilmProtos$Character;

    .line 1442
    .local v0, "other":Lcom/google/android/videos/proto/FilmProtos$Character;
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->mid:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 1443
    iget-object v3, v0, Lcom/google/android/videos/proto/FilmProtos$Character;->mid:Ljava/lang/String;

    if-eqz v3, :cond_4

    move v1, v2

    .line 1444
    goto :goto_0

    .line 1446
    :cond_3
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->mid:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Character;->mid:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 1447
    goto :goto_0

    .line 1449
    :cond_4
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->name:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 1450
    iget-object v3, v0, Lcom/google/android/videos/proto/FilmProtos$Character;->name:Ljava/lang/String;

    if-eqz v3, :cond_6

    move v1, v2

    .line 1451
    goto :goto_0

    .line 1453
    :cond_5
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->name:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Character;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    .line 1454
    goto :goto_0

    .line 1456
    :cond_6
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    if-nez v3, :cond_7

    .line 1457
    iget-object v3, v0, Lcom/google/android/videos/proto/FilmProtos$Character;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    if-eqz v3, :cond_0

    move v1, v2

    .line 1458
    goto :goto_0

    .line 1461
    :cond_7
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Character;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    invoke-virtual {v3, v4}, Lcom/google/android/videos/proto/FilmProtos$Image;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 1462
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1470
    const/16 v0, 0x11

    .line 1471
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->mid:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 1473
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->name:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 1475
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    if-nez v3, :cond_2

    :goto_2
    add-int v0, v1, v2

    .line 1477
    return v0

    .line 1471
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->mid:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 1473
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 1475
    :cond_2
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    invoke-virtual {v2}, Lcom/google/android/videos/proto/FilmProtos$Image;->hashCode()I

    move-result v2

    goto :goto_2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/FilmProtos$Character;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1518
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1519
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1523
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1524
    :sswitch_0
    return-object p0

    .line 1529
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->mid:Ljava/lang/String;

    goto :goto_0

    .line 1533
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->name:Ljava/lang/String;

    goto :goto_0

    .line 1537
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    if-nez v1, :cond_1

    .line 1538
    new-instance v1, Lcom/google/android/videos/proto/FilmProtos$Image;

    invoke-direct {v1}, Lcom/google/android/videos/proto/FilmProtos$Image;-><init>()V

    iput-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    .line 1540
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1519
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1395
    invoke-virtual {p0, p1}, Lcom/google/android/videos/proto/FilmProtos$Character;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/FilmProtos$Character;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1483
    iget-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->mid:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1484
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->mid:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1486
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1487
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->name:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1489
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    if-eqz v0, :cond_2

    .line 1490
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Character;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1492
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1493
    return-void
.end method
