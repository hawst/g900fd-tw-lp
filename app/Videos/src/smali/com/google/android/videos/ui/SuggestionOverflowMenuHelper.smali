.class public Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;
.super Ljava/lang/Object;
.source "SuggestionOverflowMenuHelper.java"

# interfaces
.implements Lcom/google/android/play/layout/PlayPopupMenu$OnPopupActionSelectedListener;


# instance fields
.field private accessibilityView:Landroid/view/View;

.field private account:Ljava/lang/String;

.field private final activity:Landroid/app/Activity;

.field private final directPurchaseFlowResultHandler:Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler;

.field private final eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private eventSource:I

.field private filteringType:I

.field private final handler:Landroid/os/Handler;

.field private idPendingActivityResult:I

.field private itemId:Ljava/lang/String;

.field private itemType:I

.field private seasonIdForViewEpisode:Ljava/lang/String;

.field private showIdForViewEpisode:Ljava/lang/String;

.field private final storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

.field private uiElementNode:Lcom/google/android/videos/logging/UiElementNode;

.field private final uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

.field private wishlistDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;

.field private wishlistIsAdd:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/store/StoreStatusMonitor;Lcom/google/android/videos/store/PurchaseStoreSync;Lcom/google/android/videos/logging/UiEventLoggingHelper;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;
    .param p3, "storeStatusMonitor"    # Lcom/google/android/videos/store/StoreStatusMonitor;
    .param p4, "purchaseStoreSync"    # Lcom/google/android/videos/store/PurchaseStoreSync;
    .param p5, "uiEventLoggingHelper"    # Lcom/google/android/videos/logging/UiEventLoggingHelper;

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->activity:Landroid/app/Activity;

    .line 68
    iput-object p2, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 69
    iput-object p3, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    .line 70
    new-instance v0, Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler;

    invoke-direct {v0, p4}, Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler;-><init>(Lcom/google/android/videos/store/PurchaseStoreSync;)V

    iput-object v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->directPurchaseFlowResultHandler:Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler;

    .line 71
    iput-object p5, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    .line 72
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->handler:Landroid/os/Handler;

    .line 73
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;Ljava/lang/String;Ljava/lang/String;IZI)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # I
    .param p4, "x4"    # Z
    .param p5, "x5"    # I

    .prologue
    .line 38
    invoke-direct/range {p0 .. p5}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->setWishlisted(Ljava/lang/String;Ljava/lang/String;IZI)V

    return-void
.end method

.method private addMenuItem(Lcom/google/android/play/layout/PlayPopupMenu;II)V
    .locals 2
    .param p1, "popup"    # Lcom/google/android/play/layout/PlayPopupMenu;
    .param p2, "id"    # I
    .param p3, "titleResId"    # I

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v0, p3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p1, p2, v0, v1, p0}, Lcom/google/android/play/layout/PlayPopupMenu;->addMenuItem(ILjava/lang/CharSequence;ZLcom/google/android/play/layout/PlayPopupMenu$OnPopupActionSelectedListener;)V

    .line 220
    return-void
.end method

.method private varargs addMenuItem(Lcom/google/android/play/layout/PlayPopupMenu;II[Ljava/lang/Object;)V
    .locals 2
    .param p1, "popup"    # Lcom/google/android/play/layout/PlayPopupMenu;
    .param p2, "id"    # I
    .param p3, "titleResId"    # I
    .param p4, "titleArgs"    # [Ljava/lang/Object;

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v0, p3, p4}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p1, p2, v0, v1, p0}, Lcom/google/android/play/layout/PlayPopupMenu;->addMenuItem(ILjava/lang/CharSequence;ZLcom/google/android/play/layout/PlayPopupMenu$OnPopupActionSelectedListener;)V

    .line 224
    return-void
.end method

.method private handleDirectPurchaseActivityLaunch(II)V
    .locals 7
    .param p1, "id"    # I
    .param p2, "result"    # I

    .prologue
    .line 334
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 335
    iput p1, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->idPendingActivityResult:I

    .line 340
    :goto_0
    return-void

    .line 337
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget v1, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->eventSource:I

    iget v3, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->filteringType:I

    iget-object v4, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->itemId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->showIdForViewEpisode:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->seasonIdForViewEpisode:Ljava/lang/String;

    move v2, p2

    invoke-interface/range {v0 .. v6}, Lcom/google/android/videos/logging/EventLogger;->onDirectPurchase(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setWishlisted(Ljava/lang/String;Ljava/lang/String;IZI)V
    .locals 7
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "itemId"    # Ljava/lang/String;
    .param p3, "itemType"    # I
    .param p4, "wishlistIsAdd"    # Z
    .param p5, "eventSource"    # I

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->activity:Landroid/app/Activity;

    iget-object v6, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->accessibilityView:Landroid/view/View;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-static/range {v0 .. v6}, Lcom/google/android/videos/store/WishlistService;->requestSetWishlisted(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZILandroid/view/View;)V

    .line 315
    return-void
.end method

.method private showMenu(Landroid/view/View;Ljava/lang/String;I[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;ILcom/google/android/videos/adapter/WishlistDataSource;)V
    .locals 12
    .param p1, "view"    # Landroid/view/View;
    .param p2, "itemId"    # Ljava/lang/String;
    .param p3, "itemType"    # I
    .param p4, "offers"    # [Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    .param p5, "eventSource"    # I
    .param p6, "wishlistDataSource"    # Lcom/google/android/videos/adapter/WishlistDataSource;

    .prologue
    .line 97
    const/16 v7, 0x12

    if-eq p3, v7, :cond_0

    const/4 v7, 0x6

    if-eq p3, v7, :cond_0

    .line 98
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Invalid asset type: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 170
    :goto_0
    return-void

    .line 101
    :cond_0
    iput-object p2, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->itemId:Ljava/lang/String;

    .line 102
    iput p3, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->itemType:I

    .line 103
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->seasonIdForViewEpisode:Ljava/lang/String;

    .line 104
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->showIdForViewEpisode:Ljava/lang/String;

    .line 105
    const/4 v7, 0x0

    iput v7, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->filteringType:I

    .line 106
    move/from16 v0, p5

    iput v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->eventSource:I

    .line 107
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->wishlistDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;

    .line 108
    iput-object p1, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->accessibilityView:Landroid/view/View;

    .line 110
    invoke-static {p1}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->findUiElementNode(Landroid/view/View;)Lcom/google/android/videos/logging/UiElementNode;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->uiElementNode:Lcom/google/android/videos/logging/UiElementNode;

    .line 111
    iget-object v7, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    const-wide/16 v8, 0x0

    const/16 v10, 0x11

    iget-object v11, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->uiElementNode:Lcom/google/android/videos/logging/UiElementNode;

    invoke-virtual {v7, v8, v9, v10, v11}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->sendPathImpression(JILcom/google/android/videos/logging/UiElementNode;)V

    .line 113
    new-instance v4, Lcom/google/android/play/layout/PlayPopupMenu;

    iget-object v7, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->activity:Landroid/app/Activity;

    invoke-direct {v4, v7, p1}, Lcom/google/android/play/layout/PlayPopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 115
    .local v4, "popup":Lcom/google/android/play/layout/PlayPopupMenu;
    iget-object v7, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    invoke-virtual {v7, p2, p3}, Lcom/google/android/videos/store/StoreStatusMonitor;->getStatus(Ljava/lang/String;I)I

    move-result v5

    .line 116
    .local v5, "status":I
    invoke-static {v5}, Lcom/google/android/videos/store/StoreStatusMonitor;->isWishlisted(I)Z

    move-result v7

    if-nez v7, :cond_5

    const/4 v7, 0x1

    :goto_1
    iput-boolean v7, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->wishlistIsAdd:Z

    .line 118
    const/4 v3, 0x0

    .line 119
    .local v3, "hasPurchaseOptions":Z
    const/4 v7, 0x6

    if-ne p3, v7, :cond_3

    iget-object v7, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->activity:Landroid/app/Activity;

    invoke-static {v7}, Lcom/google/android/videos/utils/PlayStoreUtil;->supportsDirectPurchases(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-static {v5}, Lcom/google/android/videos/store/StoreStatusMonitor;->isPurchased(I)Z

    move-result v7

    if-nez v7, :cond_3

    .line 121
    const/4 v7, 0x0

    const/4 v8, 0x2

    move-object/from16 v0, p4

    invoke-static {v7, v0, v8}, Lcom/google/android/videos/utils/OfferUtil;->getCheapestOffer(Z[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;I)Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    move-result-object v2

    .line 122
    .local v2, "cheapestOffer":Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    if-eqz v2, :cond_1

    .line 123
    const/4 v3, 0x1

    .line 125
    iget-boolean v7, v2, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->singleOffer:Z

    if-nez v7, :cond_6

    .line 126
    const v6, 0x7f0b0195

    .line 132
    .local v6, "titleResId":I
    :goto_2
    const v7, 0x7f0f0047

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, v2, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->offer:Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    iget-object v10, v10, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedAmount:Ljava/lang/String;

    aput-object v10, v8, v9

    invoke-direct {p0, v4, v7, v6, v8}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->addMenuItem(Lcom/google/android/play/layout/PlayPopupMenu;II[Ljava/lang/Object;)V

    .line 136
    .end local v6    # "titleResId":I
    :cond_1
    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object/from16 v0, p4

    invoke-static {v7, v0, v8}, Lcom/google/android/videos/utils/OfferUtil;->getCheapestOffer(Z[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;I)Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    move-result-object v2

    .line 137
    if-eqz v2, :cond_2

    .line 138
    const/4 v3, 0x1

    .line 140
    iget-boolean v7, v2, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->singleOffer:Z

    if-nez v7, :cond_8

    .line 141
    const v6, 0x7f0b0196

    .line 147
    .restart local v6    # "titleResId":I
    :goto_3
    const v7, 0x7f0f0048

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, v2, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->offer:Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    iget-object v10, v10, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedAmount:Ljava/lang/String;

    aput-object v10, v8, v9

    invoke-direct {p0, v4, v7, v6, v8}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->addMenuItem(Lcom/google/android/play/layout/PlayPopupMenu;II[Ljava/lang/Object;)V

    .line 151
    .end local v6    # "titleResId":I
    :cond_2
    if-eqz v3, :cond_a

    const/4 v2, 0x0

    .line 153
    :goto_4
    if-eqz v2, :cond_3

    .line 154
    const/4 v3, 0x1

    .line 155
    const v8, 0x7f0f0049

    iget-boolean v7, v2, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->singleOffer:Z

    if-eqz v7, :cond_b

    const v7, 0x7f0b019e

    :goto_5
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, v2, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->offer:Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    iget-object v11, v11, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedAmount:Ljava/lang/String;

    aput-object v11, v9, v10

    invoke-direct {p0, v4, v8, v7, v9}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->addMenuItem(Lcom/google/android/play/layout/PlayPopupMenu;II[Ljava/lang/Object;)V

    .line 160
    .end local v2    # "cheapestOffer":Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    :cond_3
    if-nez v3, :cond_4

    .line 162
    const/4 v7, 0x6

    if-ne p3, v7, :cond_c

    const v7, 0x7f0f004c

    :goto_6
    const v8, 0x7f0b019f

    invoke-direct {p0, v4, v7, v8}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->addMenuItem(Lcom/google/android/play/layout/PlayPopupMenu;II)V

    .line 166
    :cond_4
    const v8, 0x7f0f0046

    iget-boolean v7, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->wishlistIsAdd:Z

    if-eqz v7, :cond_d

    const v7, 0x7f0b0118

    :goto_7
    invoke-direct {p0, v4, v8, v7}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->addMenuItem(Lcom/google/android/play/layout/PlayPopupMenu;II)V

    .line 169
    invoke-virtual {v4}, Lcom/google/android/play/layout/PlayPopupMenu;->show()V

    goto/16 :goto_0

    .line 116
    .end local v3    # "hasPurchaseOptions":Z
    :cond_5
    const/4 v7, 0x0

    goto/16 :goto_1

    .line 127
    .restart local v2    # "cheapestOffer":Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    .restart local v3    # "hasPurchaseOptions":Z
    :cond_6
    invoke-virtual {v2}, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->isHd()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 128
    const v6, 0x7f0b019a

    .restart local v6    # "titleResId":I
    goto :goto_2

    .line 130
    .end local v6    # "titleResId":I
    :cond_7
    const v6, 0x7f0b019c

    .restart local v6    # "titleResId":I
    goto :goto_2

    .line 142
    .end local v6    # "titleResId":I
    :cond_8
    invoke-virtual {v2}, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->isHd()Z

    move-result v7

    if-eqz v7, :cond_9

    .line 143
    const v6, 0x7f0b019b

    .restart local v6    # "titleResId":I
    goto :goto_3

    .line 145
    .end local v6    # "titleResId":I
    :cond_9
    const v6, 0x7f0b019d

    .restart local v6    # "titleResId":I
    goto :goto_3

    .line 151
    .end local v6    # "titleResId":I
    :cond_a
    const/4 v7, 0x1

    const/4 v8, -0x1

    move-object/from16 v0, p4

    invoke-static {v7, v0, v8}, Lcom/google/android/videos/utils/OfferUtil;->getCheapestOffer(Z[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;I)Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    move-result-object v2

    goto :goto_4

    .line 155
    :cond_b
    const v7, 0x7f0b0197

    goto :goto_5

    .line 162
    .end local v2    # "cheapestOffer":Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    :cond_c
    const v7, 0x7f0f004d

    goto :goto_6

    .line 166
    :cond_d
    const v7, 0x7f0b0119

    goto :goto_7
.end method


# virtual methods
.method public init(Ljava/lang/String;)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->account:Ljava/lang/String;

    .line 77
    return-void
.end method

.method public onActionSelected(I)V
    .locals 14
    .param p1, "id"    # I

    .prologue
    const/4 v7, 0x0

    .line 228
    iput v7, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->idPendingActivityResult:I

    .line 229
    const/4 v13, 0x0

    .line 230
    .local v13, "result":I
    const/4 v12, 0x0

    .line 231
    .local v12, "leafType":I
    packed-switch p1, :pswitch_data_0

    .line 306
    :goto_0
    if-eqz v12, :cond_0

    .line 307
    iget-object v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    iget-object v1, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->uiElementNode:Lcom/google/android/videos/logging/UiElementNode;

    invoke-virtual {v0, v12, v1}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->sendClickEvent(ILcom/google/android/videos/logging/UiElementNode;)V

    .line 309
    :cond_0
    return-void

    .line 234
    :pswitch_0
    iget-boolean v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->wishlistIsAdd:Z

    if-eqz v0, :cond_1

    .line 235
    const/16 v12, 0x12

    .line 236
    iget-object v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->activity:Landroid/app/Activity;

    const v1, 0x7f0b011c

    invoke-static {v0, v1, v7}, Lcom/google/android/videos/utils/Util;->showToast(Landroid/content/Context;II)V

    .line 237
    iget-object v1, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->itemId:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->itemType:I

    iget-boolean v4, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->wishlistIsAdd:Z

    iget v5, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->eventSource:I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->setWishlisted(Ljava/lang/String;Ljava/lang/String;IZI)V

    goto :goto_0

    .line 239
    :cond_1
    const/16 v12, 0x13

    .line 240
    iget-object v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->wishlistDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;

    if-eqz v0, :cond_2

    .line 241
    iget-object v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->wishlistDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;

    iget v1, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->itemType:I

    iget-object v6, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->itemId:Ljava/lang/String;

    invoke-virtual {v0, v1, v6}, Lcom/google/android/videos/adapter/WishlistDataSource;->setRemoving(ILjava/lang/String;)V

    .line 243
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->activity:Landroid/app/Activity;

    const v1, 0x7f0b011d

    invoke-static {v0, v1, v7}, Lcom/google/android/videos/utils/Util;->showToast(Landroid/content/Context;II)V

    .line 244
    iget-object v2, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->account:Ljava/lang/String;

    .line 245
    .local v2, "finalAccount":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->itemId:Ljava/lang/String;

    .line 246
    .local v3, "finalItemId":Ljava/lang/String;
    iget v4, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->itemType:I

    .line 247
    .local v4, "finalItemType":I
    iget v5, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->eventSource:I

    .line 248
    .local v5, "finalEventSource":I
    iget-object v6, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->handler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper$1;-><init>(Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;Ljava/lang/String;Ljava/lang/String;II)V

    const-wide/16 v8, 0xc8

    invoke-virtual {v6, v0, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 258
    .end local v2    # "finalAccount":Ljava/lang/String;
    .end local v3    # "finalItemId":Ljava/lang/String;
    .end local v4    # "finalItemType":I
    .end local v5    # "finalEventSource":I
    :pswitch_1
    const/16 v12, 0x17

    .line 259
    iget-object v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->itemId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->account:Ljava/lang/String;

    iget v7, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->eventSource:I

    invoke-static {v0, v1, v6, v7}, Lcom/google/android/videos/utils/PlayStoreUtil;->viewMovieDetails(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 263
    :pswitch_2
    const/16 v12, 0x18

    .line 264
    iget-object v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->itemId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->account:Ljava/lang/String;

    iget v7, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->eventSource:I

    invoke-static {v0, v1, v6, v7}, Lcom/google/android/videos/utils/PlayStoreUtil;->viewShowDetails(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 268
    :pswitch_3
    const/16 v12, 0x19

    .line 269
    iget-object v6, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->activity:Landroid/app/Activity;

    iget-object v7, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->showIdForViewEpisode:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->seasonIdForViewEpisode:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->itemId:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->account:Ljava/lang/String;

    iget v11, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->eventSource:I

    invoke-static/range {v6 .. v11}, Lcom/google/android/videos/utils/PlayStoreUtil;->viewEpisodeDetails(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 274
    :pswitch_4
    const/16 v12, 0x14

    .line 275
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->filteringType:I

    .line 276
    iget-object v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->itemId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->account:Ljava/lang/String;

    iget v7, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->filteringType:I

    invoke-static {v0, v1, v6, v7}, Lcom/google/android/videos/utils/PlayStoreUtil;->startMovieDirectPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v13

    .line 278
    invoke-direct {p0, p1, v13}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->handleDirectPurchaseActivityLaunch(II)V

    goto/16 :goto_0

    .line 282
    :pswitch_5
    const/16 v12, 0x15

    .line 283
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->filteringType:I

    .line 284
    iget-object v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->itemId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->account:Ljava/lang/String;

    iget v7, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->filteringType:I

    invoke-static {v0, v1, v6, v7}, Lcom/google/android/videos/utils/PlayStoreUtil;->startMovieDirectPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v13

    .line 286
    invoke-direct {p0, p1, v13}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->handleDirectPurchaseActivityLaunch(II)V

    goto/16 :goto_0

    .line 291
    :pswitch_6
    const/16 v12, 0x1b

    .line 292
    iput v7, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->filteringType:I

    .line 293
    iget-object v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->itemId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->account:Ljava/lang/String;

    iget v7, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->filteringType:I

    invoke-static {v0, v1, v6, v7}, Lcom/google/android/videos/utils/PlayStoreUtil;->startMovieDirectPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v13

    .line 295
    invoke-direct {p0, p1, v13}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->handleDirectPurchaseActivityLaunch(II)V

    goto/16 :goto_0

    .line 299
    :pswitch_7
    const/16 v12, 0x16

    .line 300
    iput v7, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->filteringType:I

    .line 301
    iget-object v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->itemId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->account:Ljava/lang/String;

    iget v7, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->filteringType:I

    invoke-static {v0, v1, v6, v7}, Lcom/google/android/videos/utils/PlayStoreUtil;->startEpisodeDirectPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v13

    .line 303
    invoke-direct {p0, p1, v13}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->handleDirectPurchaseActivityLaunch(II)V

    goto/16 :goto_0

    .line 231
    nop

    :pswitch_data_0
    .packed-switch 0x7f0f0046
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onActivityResult(IILandroid/content/Intent;)Z
    .locals 8
    .param p1, "requestCode"    # I
    .param p2, "result"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x0

    const/4 v0, -0x1

    .line 318
    iget-object v1, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->directPurchaseFlowResultHandler:Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler;

    invoke-virtual {v1, p1, p2, p3}, Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler;->onActivityResult(IILandroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->idPendingActivityResult:I

    if-eqz v1, :cond_2

    .line 320
    if-ne p2, v0, :cond_1

    move v2, v0

    .line 322
    .local v2, "purchaseResult":I
    :goto_0
    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->wishlistDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->wishlistDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;

    iget v1, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->itemType:I

    iget-object v3, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->itemId:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/videos/adapter/WishlistDataSource;->setRemoving(ILjava/lang/String;)V

    .line 325
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget v1, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->eventSource:I

    iget v3, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->filteringType:I

    iget-object v4, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->itemId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->showIdForViewEpisode:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->seasonIdForViewEpisode:Ljava/lang/String;

    invoke-interface/range {v0 .. v6}, Lcom/google/android/videos/logging/EventLogger;->onDirectPurchase(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    iput v7, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->idPendingActivityResult:I

    .line 328
    const/4 v0, 0x1

    .line 330
    .end local v2    # "purchaseResult":I
    :goto_1
    return v0

    .line 320
    :cond_1
    const/16 v2, 0xe

    goto :goto_0

    :cond_2
    move v0, v7

    .line 330
    goto :goto_1
.end method

.method public reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 80
    iget-object v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 81
    iput-object v1, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->account:Ljava/lang/String;

    .line 82
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->idPendingActivityResult:I

    .line 83
    return-void
.end method

.method public showSuggestionMenu(Lcom/google/wireless/android/video/magma/proto/AssetResource;Landroid/view/View;)V
    .locals 7
    .param p1, "asset"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 91
    iget-object v0, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v2, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    iget-object v4, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->showMenu(Landroid/view/View;Ljava/lang/String;I[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;ILcom/google/android/videos/adapter/WishlistDataSource;)V

    .line 93
    return-void
.end method

.method public showWishlistMenu(Landroid/view/View;Ljava/lang/String;I[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;Lcom/google/android/videos/adapter/WishlistDataSource;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;
    .param p2, "itemId"    # Ljava/lang/String;
    .param p3, "itemType"    # I
    .param p4, "offers"    # [Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    .param p5, "wishlistDataSource"    # Lcom/google/android/videos/adapter/WishlistDataSource;

    .prologue
    .line 87
    const/16 v5, 0x13

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->showMenu(Landroid/view/View;Ljava/lang/String;I[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;ILcom/google/android/videos/adapter/WishlistDataSource;)V

    .line 88
    return-void
.end method

.method public startFlowForEpisode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "episodeId"    # Ljava/lang/String;
    .param p2, "seasonId"    # Ljava/lang/String;
    .param p3, "showId"    # Ljava/lang/String;
    .param p4, "hasOffers"    # Z

    .prologue
    .line 204
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->itemId:Ljava/lang/String;

    .line 205
    const/16 v0, 0x14

    iput v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->itemType:I

    .line 206
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->seasonIdForViewEpisode:Ljava/lang/String;

    .line 207
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->showIdForViewEpisode:Ljava/lang/String;

    .line 208
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->eventSource:I

    .line 210
    if-eqz p4, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->activity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/videos/utils/PlayStoreUtil;->supportsDirectPurchases(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    iget-object v1, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->itemId:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->itemType:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/store/StoreStatusMonitor;->getStatus(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/videos/store/StoreStatusMonitor;->isPurchased(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 212
    const v0, 0x7f0f004b

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->onActionSelected(I)V

    .line 216
    :goto_0
    return-void

    .line 214
    :cond_0
    const v0, 0x7f0f004e

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->onActionSelected(I)V

    goto :goto_0
.end method

.method public startMovieDirectPurchaseFlow(Ljava/lang/String;II)V
    .locals 3
    .param p1, "movieId"    # Ljava/lang/String;
    .param p2, "playStoreFilterType"    # I
    .param p3, "eventSource"    # I

    .prologue
    const/4 v1, 0x0

    .line 177
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->itemId:Ljava/lang/String;

    .line 178
    const/4 v0, 0x6

    iput v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->itemType:I

    .line 179
    iput-object v1, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->seasonIdForViewEpisode:Ljava/lang/String;

    .line 180
    iput-object v1, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->showIdForViewEpisode:Ljava/lang/String;

    .line 181
    iput p3, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->eventSource:I

    .line 183
    iget-object v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->activity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/videos/utils/PlayStoreUtil;->supportsDirectPurchases(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    iget-object v1, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->itemId:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->itemType:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/store/StoreStatusMonitor;->getStatus(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/videos/store/StoreStatusMonitor;->isPurchased(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 185
    packed-switch p2, :pswitch_data_0

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->itemId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->account:Ljava/lang/String;

    invoke-static {v0, v1, v2, p3}, Lcom/google/android/videos/utils/PlayStoreUtil;->viewMovieDetails(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;I)V

    .line 200
    :goto_0
    return-void

    .line 187
    :pswitch_0
    const v0, 0x7f0f0047

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->onActionSelected(I)V

    goto :goto_0

    .line 190
    :pswitch_1
    const v0, 0x7f0f0048

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->onActionSelected(I)V

    goto :goto_0

    .line 193
    :pswitch_2
    const v0, 0x7f0f004a

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->onActionSelected(I)V

    goto :goto_0

    .line 185
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
