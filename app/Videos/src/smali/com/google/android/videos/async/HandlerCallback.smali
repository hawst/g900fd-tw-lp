.class public final Lcom/google/android/videos/async/HandlerCallback;
.super Lcom/google/android/videos/async/ThreadingCallback;
.source "HandlerCallback.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/videos/async/ThreadingCallback",
        "<TR;TE;>;"
    }
.end annotation


# instance fields
.field private final handler:Landroid/os/Handler;

.field private final thread:Ljava/lang/Thread;


# direct methods
.method private constructor <init>(Landroid/os/Handler;Lcom/google/android/videos/async/Callback;)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Handler;",
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;)V"
        }
    .end annotation

    .prologue
    .line 23
    .local p0, "this":Lcom/google/android/videos/async/HandlerCallback;, "Lcom/google/android/videos/async/HandlerCallback<TR;TE;>;"
    .local p2, "target":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TR;TE;>;"
    invoke-direct {p0, p2}, Lcom/google/android/videos/async/ThreadingCallback;-><init>(Lcom/google/android/videos/async/Callback;)V

    .line 24
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/videos/async/HandlerCallback;->handler:Landroid/os/Handler;

    .line 25
    invoke-virtual {p1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/async/HandlerCallback;->thread:Ljava/lang/Thread;

    .line 26
    return-void
.end method

.method public static create(Landroid/os/Handler;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/HandlerCallback;
    .locals 1
    .param p0, "handler"    # Landroid/os/Handler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/os/Handler;",
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;)",
            "Lcom/google/android/videos/async/HandlerCallback",
            "<TR;TE;>;"
        }
    .end annotation

    .prologue
    .line 29
    .local p1, "target":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TR;TE;>;"
    new-instance v0, Lcom/google/android/videos/async/HandlerCallback;

    invoke-direct {v0, p0, p1}, Lcom/google/android/videos/async/HandlerCallback;-><init>(Landroid/os/Handler;Lcom/google/android/videos/async/Callback;)V

    return-object v0
.end method


# virtual methods
.method protected post(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 34
    .local p0, "this":Lcom/google/android/videos/async/HandlerCallback;, "Lcom/google/android/videos/async/HandlerCallback<TR;TE;>;"
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/async/HandlerCallback;->thread:Ljava/lang/Thread;

    if-eq v0, v1, :cond_0

    .line 35
    iget-object v0, p0, Lcom/google/android/videos/async/HandlerCallback;->handler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 39
    :goto_0
    return-void

    .line 37
    :cond_0
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method
