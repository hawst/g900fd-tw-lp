.class public Lcom/google/android/videos/ui/TransitionUtil$MoveTopLeft;
.super Landroid/transition/Transition;
.source "TransitionUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/TransitionUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MoveTopLeft"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 365
    invoke-direct {p0}, Landroid/transition/Transition;-><init>()V

    return-void
.end method

.method private captureValues(Landroid/transition/TransitionValues;)V
    .locals 3
    .param p1, "values"    # Landroid/transition/TransitionValues;

    .prologue
    .line 383
    iget-object v0, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "left"

    iget-object v2, p1, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 384
    iget-object v0, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "top"

    iget-object v2, p1, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 385
    iget-object v0, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "bottom"

    iget-object v2, p1, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 386
    iget-object v0, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "right"

    iget-object v2, p1, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 387
    return-void
.end method


# virtual methods
.method public captureEndValues(Landroid/transition/TransitionValues;)V
    .locals 0
    .param p1, "values"    # Landroid/transition/TransitionValues;

    .prologue
    .line 379
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/TransitionUtil$MoveTopLeft;->captureValues(Landroid/transition/TransitionValues;)V

    .line 380
    return-void
.end method

.method public captureStartValues(Landroid/transition/TransitionValues;)V
    .locals 0
    .param p1, "values"    # Landroid/transition/TransitionValues;

    .prologue
    .line 374
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/TransitionUtil$MoveTopLeft;->captureValues(Landroid/transition/TransitionValues;)V

    .line 375
    return-void
.end method

.method public createAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
    .locals 16
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "startValues"    # Landroid/transition/TransitionValues;
    .param p3, "endValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 392
    move-object/from16 v0, p2

    iget-object v11, v0, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v12, "left"

    invoke-interface {v11, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 393
    .local v6, "startLeft":I
    move-object/from16 v0, p2

    iget-object v11, v0, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v12, "top"

    invoke-interface {v11, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 394
    .local v8, "startTop":I
    move-object/from16 v0, p2

    iget-object v11, v0, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v12, "right"

    invoke-interface {v11, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 395
    .local v7, "startRight":I
    move-object/from16 v0, p2

    iget-object v11, v0, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v12, "bottom"

    invoke-interface {v11, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 397
    .local v5, "startBottom":I
    move-object/from16 v0, p3

    iget-object v11, v0, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v12, "left"

    invoke-interface {v11, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 398
    .local v2, "endLeft":I
    move-object/from16 v0, p3

    iget-object v11, v0, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v12, "top"

    invoke-interface {v11, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 399
    .local v4, "endTop":I
    move-object/from16 v0, p3

    iget-object v11, v0, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v12, "right"

    invoke-interface {v11, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 400
    .local v3, "endRight":I
    move-object/from16 v0, p3

    iget-object v11, v0, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v12, "bottom"

    invoke-interface {v11, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 402
    .local v1, "endBottom":I
    move-object/from16 v0, p2

    iget-object v10, v0, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 403
    .local v10, "view":Landroid/view/View;
    invoke-static {v7, v3}, Ljava/lang/Math;->max(II)I

    move-result v11

    invoke-virtual {v10, v11}, Landroid/view/View;->setRight(I)V

    .line 404
    invoke-static {v5, v1}, Ljava/lang/Math;->max(II)I

    move-result v11

    invoke-virtual {v10, v11}, Landroid/view/View;->setBottom(I)V

    .line 406
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/ui/TransitionUtil$MoveTopLeft;->getPathMotion()Landroid/transition/PathMotion;

    move-result-object v11

    int-to-float v12, v6

    int-to-float v13, v8

    int-to-float v14, v2

    int-to-float v15, v4

    invoke-virtual {v11, v12, v13, v14, v15}, Landroid/transition/PathMotion;->getPath(FFFF)Landroid/graphics/Path;

    move-result-object v9

    .line 407
    .local v9, "topLeftPath":Landroid/graphics/Path;
    const-string v11, "left"

    const-string v12, "top"

    invoke-static {v10, v11, v12, v9}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Path;)Landroid/animation/ObjectAnimator;

    move-result-object v11

    return-object v11
.end method
