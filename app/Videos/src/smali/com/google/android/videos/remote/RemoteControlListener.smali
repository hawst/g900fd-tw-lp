.class public interface abstract Lcom/google/android/videos/remote/RemoteControlListener;
.super Ljava/lang/Object;
.source "RemoteControlListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/remote/RemoteControlListener$BaseRemoteControlListener;
    }
.end annotation


# virtual methods
.method public abstract onAudioTracksChanged(Ljava/util/List;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AudioInfo;",
            ">;I)V"
        }
    .end annotation
.end method

.method public abstract onDisconnected()V
.end method

.method public abstract onPlayerStateChanged(Lcom/google/android/videos/remote/PlayerState;)V
.end method

.method public abstract onRemoteControlError(ILcom/google/android/videos/utils/RetryAction;)V
.end method

.method public abstract onRemoteControlErrorCleared()V
.end method

.method public abstract onSubtitleTracksChanged(Lcom/google/android/videos/remote/SubtitleTrackList;)V
.end method

.method public abstract onVideoInfoChanged(Lcom/google/android/videos/remote/RemoteVideoInfo;)V
.end method
