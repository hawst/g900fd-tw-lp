.class public Lcom/google/android/videos/api/RobotTokenRequest;
.super Lcom/google/android/videos/async/Request;
.source "RobotTokenRequest.java"


# instance fields
.field public final keyRequests:[Ljava/lang/String;

.field public final pinned:Z

.field public final videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Z)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "keyRequests"    # [Ljava/lang/String;
    .param p4, "pinned"    # Z

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/google/android/videos/async/Request;-><init>(Ljava/lang/String;)V

    .line 19
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/api/RobotTokenRequest;->videoId:Ljava/lang/String;

    .line 20
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/videos/api/RobotTokenRequest;->keyRequests:[Ljava/lang/String;

    .line 21
    iput-boolean p4, p0, Lcom/google/android/videos/api/RobotTokenRequest;->pinned:Z

    .line 22
    return-void
.end method
