.class Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;
.super Ljava/lang/Object;
.source "RemoteTracker.java"

# interfaces
.implements Lcom/google/android/videos/remote/TransportControl$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/remote/RemoteTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TransportControlListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/remote/RemoteTracker;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/remote/RemoteTracker;)V
    .locals 0

    .prologue
    .line 532
    iput-object p1, p0, Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/remote/RemoteTracker;Lcom/google/android/videos/remote/RemoteTracker$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/remote/RemoteTracker;
    .param p2, "x1"    # Lcom/google/android/videos/remote/RemoteTracker$1;

    .prologue
    .line 532
    invoke-direct {p0, p1}, Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;-><init>(Lcom/google/android/videos/remote/RemoteTracker;)V

    return-void
.end method


# virtual methods
.method public onDisconnect()V
    .locals 1

    .prologue
    .line 585
    const-string v0, "onDisconnect() called"

    invoke-static {v0}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 586
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # getter for: Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;
    invoke-static {v0}, Lcom/google/android/videos/remote/RemoteTracker;->access$600(Lcom/google/android/videos/remote/RemoteTracker;)Lcom/google/android/videos/remote/RemoteControl;

    move-result-object v0

    if-nez v0, :cond_0

    .line 587
    const-string v0, "No active remote!"

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 591
    :goto_0
    return-void

    .line 590
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # getter for: Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;
    invoke-static {v0}, Lcom/google/android/videos/remote/RemoteTracker;->access$600(Lcom/google/android/videos/remote/RemoteTracker;)Lcom/google/android/videos/remote/RemoteControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/remote/RemoteControl;->stop()V

    goto :goto_0
.end method

.method public onDismiss()V
    .locals 1

    .prologue
    .line 595
    const-string v0, "onDismiss() called"

    invoke-static {v0}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 596
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # getter for: Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;
    invoke-static {v0}, Lcom/google/android/videos/remote/RemoteTracker;->access$600(Lcom/google/android/videos/remote/RemoteTracker;)Lcom/google/android/videos/remote/RemoteControl;

    move-result-object v0

    if-nez v0, :cond_0

    .line 597
    const-string v0, "No active remote!"

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 601
    :cond_0
    return-void
.end method

.method public onMute()V
    .locals 1

    .prologue
    .line 565
    const-string v0, "onMute() called"

    invoke-static {v0}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 566
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # getter for: Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;
    invoke-static {v0}, Lcom/google/android/videos/remote/RemoteTracker;->access$600(Lcom/google/android/videos/remote/RemoteTracker;)Lcom/google/android/videos/remote/RemoteControl;

    move-result-object v0

    if-nez v0, :cond_0

    .line 567
    const-string v0, "No active remote!"

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 571
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 545
    const-string v0, "onPause() called"

    invoke-static {v0}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 546
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # getter for: Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;
    invoke-static {v0}, Lcom/google/android/videos/remote/RemoteTracker;->access$600(Lcom/google/android/videos/remote/RemoteTracker;)Lcom/google/android/videos/remote/RemoteControl;

    move-result-object v0

    if-nez v0, :cond_0

    .line 547
    const-string v0, "No active remote!"

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 551
    :goto_0
    return-void

    .line 550
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # getter for: Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;
    invoke-static {v0}, Lcom/google/android/videos/remote/RemoteTracker;->access$600(Lcom/google/android/videos/remote/RemoteTracker;)Lcom/google/android/videos/remote/RemoteControl;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # getter for: Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;
    invoke-static {v1}, Lcom/google/android/videos/remote/RemoteTracker;->access$600(Lcom/google/android/videos/remote/RemoteTracker;)Lcom/google/android/videos/remote/RemoteControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/videos/remote/RemoteControl;->getPlayerState()Lcom/google/android/videos/remote/PlayerState;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/videos/remote/PlayerState;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/remote/RemoteControl;->pause(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public onPlay()V
    .locals 2

    .prologue
    .line 535
    const-string v0, "onPlay() called"

    invoke-static {v0}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 536
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # getter for: Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;
    invoke-static {v0}, Lcom/google/android/videos/remote/RemoteTracker;->access$600(Lcom/google/android/videos/remote/RemoteTracker;)Lcom/google/android/videos/remote/RemoteControl;

    move-result-object v0

    if-nez v0, :cond_0

    .line 537
    const-string v0, "No active remote!"

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 541
    :goto_0
    return-void

    .line 540
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # getter for: Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;
    invoke-static {v0}, Lcom/google/android/videos/remote/RemoteTracker;->access$600(Lcom/google/android/videos/remote/RemoteTracker;)Lcom/google/android/videos/remote/RemoteControl;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # getter for: Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;
    invoke-static {v1}, Lcom/google/android/videos/remote/RemoteTracker;->access$600(Lcom/google/android/videos/remote/RemoteTracker;)Lcom/google/android/videos/remote/RemoteControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/videos/remote/RemoteControl;->getPlayerState()Lcom/google/android/videos/remote/PlayerState;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/videos/remote/PlayerState;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/remote/RemoteControl;->play(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public onSeek(I)V
    .locals 2
    .param p1, "milliseconds"    # I

    .prologue
    .line 555
    const-string v0, "onSeek() called"

    invoke-static {v0}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 556
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # getter for: Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;
    invoke-static {v0}, Lcom/google/android/videos/remote/RemoteTracker;->access$600(Lcom/google/android/videos/remote/RemoteTracker;)Lcom/google/android/videos/remote/RemoteControl;

    move-result-object v0

    if-nez v0, :cond_0

    .line 557
    const-string v0, "No active remote!"

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 561
    :goto_0
    return-void

    .line 560
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # getter for: Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;
    invoke-static {v0}, Lcom/google/android/videos/remote/RemoteTracker;->access$600(Lcom/google/android/videos/remote/RemoteTracker;)Lcom/google/android/videos/remote/RemoteControl;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # getter for: Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;
    invoke-static {v1}, Lcom/google/android/videos/remote/RemoteTracker;->access$600(Lcom/google/android/videos/remote/RemoteTracker;)Lcom/google/android/videos/remote/RemoteControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/videos/remote/RemoteControl;->getPlayerState()Lcom/google/android/videos/remote/PlayerState;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/videos/remote/PlayerState;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/videos/remote/RemoteControl;->seekTo(Ljava/lang/String;I)Z

    goto :goto_0
.end method

.method public onSelect()V
    .locals 6

    .prologue
    .line 605
    const-string v2, "onSelect() called"

    invoke-static {v2}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 606
    iget-object v2, p0, Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # getter for: Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;
    invoke-static {v2}, Lcom/google/android/videos/remote/RemoteTracker;->access$600(Lcom/google/android/videos/remote/RemoteTracker;)Lcom/google/android/videos/remote/RemoteControl;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # getter for: Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;
    invoke-static {v2}, Lcom/google/android/videos/remote/RemoteTracker;->access$600(Lcom/google/android/videos/remote/RemoteTracker;)Lcom/google/android/videos/remote/RemoteControl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/videos/remote/RemoteControl;->getError()I

    move-result v2

    if-nez v2, :cond_0

    .line 607
    iget-object v2, p0, Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # getter for: Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;
    invoke-static {v2}, Lcom/google/android/videos/remote/RemoteTracker;->access$600(Lcom/google/android/videos/remote/RemoteTracker;)Lcom/google/android/videos/remote/RemoteControl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/videos/remote/RemoteControl;->getVideoInfo()Lcom/google/android/videos/remote/RemoteVideoInfo;

    move-result-object v1

    .line 608
    .local v1, "videoInfo":Lcom/google/android/videos/remote/RemoteVideoInfo;
    iget-object v2, p0, Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # getter for: Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;
    invoke-static {v2}, Lcom/google/android/videos/remote/RemoteTracker;->access$600(Lcom/google/android/videos/remote/RemoteTracker;)Lcom/google/android/videos/remote/RemoteControl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/videos/remote/RemoteControl;->getPlayerState()Lcom/google/android/videos/remote/PlayerState;

    move-result-object v0

    .line 609
    .local v0, "playerState":Lcom/google/android/videos/remote/PlayerState;
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 610
    iget v2, v0, Lcom/google/android/videos/remote/PlayerState;->state:I

    packed-switch v2, :pswitch_data_0

    .line 624
    .end local v0    # "playerState":Lcom/google/android/videos/remote/PlayerState;
    .end local v1    # "videoInfo":Lcom/google/android/videos/remote/RemoteVideoInfo;
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # getter for: Lcom/google/android/videos/remote/RemoteTracker;->context:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/videos/remote/RemoteTracker;->access$500(Lcom/google/android/videos/remote/RemoteTracker;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # getter for: Lcom/google/android/videos/remote/RemoteTracker;->context:Landroid/content/Context;
    invoke-static {v3}, Lcom/google/android/videos/remote/RemoteTracker;->access$500(Lcom/google/android/videos/remote/RemoteTracker;)Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    const-string v5, "movies"

    invoke-static {v3, v4, v5}, Lcom/google/android/videos/activity/HomeActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 626
    :goto_0
    return-void

    .line 615
    .restart local v0    # "playerState":Lcom/google/android/videos/remote/PlayerState;
    .restart local v1    # "videoInfo":Lcom/google/android/videos/remote/RemoteVideoInfo;
    :pswitch_0
    iget-object v2, p0, Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # invokes: Lcom/google/android/videos/remote/RemoteTracker;->showPlaybackScreen(Lcom/google/android/videos/remote/RemoteVideoInfo;)Z
    invoke-static {v2, v1}, Lcom/google/android/videos/remote/RemoteTracker;->access$1500(Lcom/google/android/videos/remote/RemoteTracker;Lcom/google/android/videos/remote/RemoteVideoInfo;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    .line 610
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onUnmute()V
    .locals 1

    .prologue
    .line 575
    const-string v0, "onUnmute() called"

    invoke-static {v0}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 576
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # getter for: Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;
    invoke-static {v0}, Lcom/google/android/videos/remote/RemoteTracker;->access$600(Lcom/google/android/videos/remote/RemoteTracker;)Lcom/google/android/videos/remote/RemoteControl;

    move-result-object v0

    if-nez v0, :cond_0

    .line 577
    const-string v0, "No active remote!"

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 581
    :cond_0
    return-void
.end method
