.class abstract Lcom/google/android/videos/player/DirectorInitializer$DirectorAuthenticatee;
.super Ljava/lang/Object;
.source "DirectorInitializer.java"

# interfaces
.implements Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/DirectorInitializer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "DirectorAuthenticatee"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/player/DirectorInitializer;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/player/DirectorInitializer;)V
    .locals 0

    .prologue
    .line 760
    iput-object p1, p0, Lcom/google/android/videos/player/DirectorInitializer$DirectorAuthenticatee;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/player/DirectorInitializer;Lcom/google/android/videos/player/DirectorInitializer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/player/DirectorInitializer;
    .param p2, "x1"    # Lcom/google/android/videos/player/DirectorInitializer$1;

    .prologue
    .line 760
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/DirectorInitializer$DirectorAuthenticatee;-><init>(Lcom/google/android/videos/player/DirectorInitializer;)V

    return-void
.end method


# virtual methods
.method public final onAuthenticationError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 3
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 769
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer$DirectorAuthenticatee;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    const/4 v1, 0x2

    const/4 v2, 0x1

    # invokes: Lcom/google/android/videos/player/DirectorInitializer;->onInitializationError(ILjava/lang/Exception;Z)V
    invoke-static {v0, v1, p2, v2}, Lcom/google/android/videos/player/DirectorInitializer;->access$2900(Lcom/google/android/videos/player/DirectorInitializer;ILjava/lang/Exception;Z)V

    .line 770
    return-void
.end method

.method public final onNotAuthenticated(Ljava/lang/String;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 764
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer$DirectorAuthenticatee;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # invokes: Lcom/google/android/videos/player/DirectorInitializer;->onNotAuthenticated()V
    invoke-static {v0}, Lcom/google/android/videos/player/DirectorInitializer;->access$2800(Lcom/google/android/videos/player/DirectorInitializer;)V

    .line 765
    return-void
.end method
