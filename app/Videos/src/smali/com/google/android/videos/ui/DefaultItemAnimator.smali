.class public Lcom/google/android/videos/ui/DefaultItemAnimator;
.super Landroid/support/v7/widget/RecyclerView$ItemAnimator;
.source "DefaultItemAnimator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/DefaultItemAnimator$VpaListenerAdapter;,
        Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;,
        Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        ">",
        "Landroid/support/v7/widget/RecyclerView$ItemAnimator;"
    }
.end annotation


# instance fields
.field private mAddAnimations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mAdditionsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private mChangeAnimations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mChangesList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo",
            "<TT;>;>;>;"
        }
    .end annotation
.end field

.field private mMoveAnimations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mMovesList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo",
            "<TT;>;>;>;"
        }
    .end annotation
.end field

.field private mPendingAdditions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mPendingChanges:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private mPendingMoves:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private mPendingRemovals:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mRemoveAnimations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$ItemAnimator;-><init>()V

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingRemovals:Ljava/util/ArrayList;

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingAdditions:Ljava/util/ArrayList;

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingMoves:Ljava/util/ArrayList;

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingChanges:Ljava/util/ArrayList;

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mAdditionsList:Ljava/util/ArrayList;

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mMovesList:Ljava/util/ArrayList;

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mChangesList:Ljava/util/ArrayList;

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mAddAnimations:Ljava/util/ArrayList;

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mMoveAnimations:Ljava/util/ArrayList;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mRemoveAnimations:Ljava/util/ArrayList;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mChangeAnimations:Ljava/util/ArrayList;

    .line 734
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/ui/DefaultItemAnimator;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/DefaultItemAnimator;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mRemoveAnimations:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/ui/DefaultItemAnimator;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/DefaultItemAnimator;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mAddAnimations:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/ui/DefaultItemAnimator;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/DefaultItemAnimator;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mMoveAnimations:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/ui/DefaultItemAnimator;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/DefaultItemAnimator;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mChangeAnimations:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/videos/ui/DefaultItemAnimator;Landroid/support/v7/widget/RecyclerView$ViewHolder;IIII)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/ui/DefaultItemAnimator;
    .param p1, "x1"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "x2"    # I
    .param p3, "x3"    # I
    .param p4, "x4"    # I
    .param p5, "x5"    # I

    .prologue
    .line 19
    invoke-direct/range {p0 .. p5}, Lcom/google/android/videos/ui/DefaultItemAnimator;->animateMoveImpl(Landroid/support/v7/widget/RecyclerView$ViewHolder;IIII)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/videos/ui/DefaultItemAnimator;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/DefaultItemAnimator;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mMovesList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/videos/ui/DefaultItemAnimator;Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/ui/DefaultItemAnimator;
    .param p1, "x1"    # Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/DefaultItemAnimator;->animateChangeImpl(Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/videos/ui/DefaultItemAnimator;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/DefaultItemAnimator;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mChangesList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/videos/ui/DefaultItemAnimator;Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/ui/DefaultItemAnimator;
    .param p1, "x1"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/DefaultItemAnimator;->animateAddImpl(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/videos/ui/DefaultItemAnimator;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/DefaultItemAnimator;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mAdditionsList:Ljava/util/ArrayList;

    return-object v0
.end method

.method private animateAddImpl(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 441
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    .local p1, "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;, "TT;"
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mAddAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 442
    invoke-virtual {p0, p1}, Lcom/google/android/videos/ui/DefaultItemAnimator;->runAddAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 443
    return-void
.end method

.method private animateChangeImpl(Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 484
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    .local p1, "changeInfo":Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;, "Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo<TT;>;"
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mChangeAnimations:Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;->oldHolder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 485
    invoke-virtual {p0, p1}, Lcom/google/android/videos/ui/DefaultItemAnimator;->runOldViewChangeAnimation(Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;)V

    .line 486
    iget-object v0, p1, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;->newHolder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;->newHolder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 487
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mChangeAnimations:Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;->newHolder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 488
    invoke-virtual {p0, p1}, Lcom/google/android/videos/ui/DefaultItemAnimator;->runNewViewChangeAnimation(Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;)V

    .line 490
    :cond_0
    return-void
.end method

.method private animateMoveImpl(Landroid/support/v7/widget/RecyclerView$ViewHolder;IIII)V
    .locals 5
    .param p2, "fromX"    # I
    .param p3, "fromY"    # I
    .param p4, "toX"    # I
    .param p5, "toY"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;IIII)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    .local p1, "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;, "TT;"
    const/4 v4, 0x0

    .line 459
    iget-object v2, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 460
    .local v2, "view":Landroid/view/View;
    sub-int v0, p4, p2

    .line 461
    .local v0, "deltaX":I
    sub-int v1, p5, p3

    .line 462
    .local v1, "deltaY":I
    if-eqz v0, :cond_0

    .line 463
    invoke-static {v2}, Landroid/support/v4/view/ViewCompat;->animate(Landroid/view/View;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->translationX(F)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    .line 465
    :cond_0
    if-eqz v1, :cond_1

    .line 466
    invoke-static {v2}, Landroid/support/v4/view/ViewCompat;->animate(Landroid/view/View;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->translationY(F)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    .line 471
    :cond_1
    iget-object v3, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mMoveAnimations:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 472
    invoke-virtual/range {p0 .. p5}, Lcom/google/android/videos/ui/DefaultItemAnimator;->runMoveAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;IIII)V

    .line 473
    return-void
.end method

.method private animateRemoveImpl(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 428
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    .local p1, "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;, "TT;"
    invoke-virtual {p0, p1}, Lcom/google/android/videos/ui/DefaultItemAnimator;->runRemoveAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 429
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mRemoveAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 430
    return-void
.end method

.method private cancelAll(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 713
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    .local p1, "viewHolders":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 714
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/RecyclerView$ViewHolder;

    iget-object v1, v2, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 715
    .local v1, "itemView":Landroid/view/View;
    invoke-direct {p0, v1}, Lcom/google/android/videos/ui/DefaultItemAnimator;->cancelAnimationRecursive(Landroid/view/View;)V

    .line 713
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 717
    .end local v1    # "itemView":Landroid/view/View;
    :cond_0
    return-void
.end method

.method private cancelAnimationRecursive(Landroid/view/View;)V
    .locals 5
    .param p1, "root"    # Landroid/view/View;

    .prologue
    .line 720
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    instance-of v4, p1, Landroid/view/ViewGroup;

    if-eqz v4, :cond_1

    move-object v2, p1

    .line 721
    check-cast v2, Landroid/view/ViewGroup;

    .line 722
    .local v2, "parentView":Landroid/view/ViewGroup;
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 723
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_2

    .line 724
    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 725
    .local v3, "view":Landroid/view/View;
    if-eqz v3, :cond_0

    .line 726
    invoke-direct {p0, v3}, Lcom/google/android/videos/ui/DefaultItemAnimator;->cancelAnimationRecursive(Landroid/view/View;)V

    .line 723
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 730
    .end local v0    # "count":I
    .end local v1    # "i":I
    .end local v2    # "parentView":Landroid/view/ViewGroup;
    .end local v3    # "view":Landroid/view/View;
    :cond_1
    invoke-static {p1}, Landroid/support/v4/view/ViewCompat;->animate(Landroid/view/View;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->cancel()V

    .line 732
    :cond_2
    return-void
.end method

.method private endChangeAnimation(Ljava/util/List;Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo",
            "<TT;>;>;TT;)V"
        }
    .end annotation

    .prologue
    .line 493
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    .local p1, "infoList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo<TT;>;>;"
    .local p2, "item":Landroid/support/v7/widget/RecyclerView$ViewHolder;, "TT;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v1, v2, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_1

    .line 494
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;

    .line 495
    .local v0, "changeInfo":Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;, "Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo<TT;>;"
    invoke-direct {p0, v0, p2}, Lcom/google/android/videos/ui/DefaultItemAnimator;->endChangeAnimationIfNecessary(Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;Landroid/support/v7/widget/RecyclerView$ViewHolder;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 496
    iget-object v2, v0, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;->oldHolder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    if-nez v2, :cond_0

    iget-object v2, v0, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;->newHolder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    if-nez v2, :cond_0

    .line 497
    invoke-interface {p1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 493
    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 501
    .end local v0    # "changeInfo":Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;, "Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo<TT;>;"
    :cond_1
    return-void
.end method

.method private endChangeAnimationIfNecessary(Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 504
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    .local p1, "changeInfo":Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;, "Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo<TT;>;"
    iget-object v0, p1, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;->oldHolder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    if-eqz v0, :cond_0

    .line 505
    iget-object v0, p1, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;->oldHolder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/ui/DefaultItemAnimator;->endChangeAnimationIfNecessary(Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;Landroid/support/v7/widget/RecyclerView$ViewHolder;)Z

    .line 507
    :cond_0
    iget-object v0, p1, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;->newHolder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    if-eqz v0, :cond_1

    .line 508
    iget-object v0, p1, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;->newHolder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/ui/DefaultItemAnimator;->endChangeAnimationIfNecessary(Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;Landroid/support/v7/widget/RecyclerView$ViewHolder;)Z

    .line 510
    :cond_1
    return-void
.end method

.method private endChangeAnimationIfNecessary(Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;Landroid/support/v7/widget/RecyclerView$ViewHolder;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo",
            "<TT;>;TT;)Z"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    .local p1, "changeInfo":Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;, "Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo<TT;>;"
    .local p2, "item":Landroid/support/v7/widget/RecyclerView$ViewHolder;, "TT;"
    const/4 v2, 0x0

    .line 513
    const/4 v0, 0x0

    .line 514
    .local v0, "oldItem":Z
    iget-object v1, p1, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;->newHolder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    if-ne v1, p2, :cond_0

    .line 515
    iput-object v2, p1, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;->newHolder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .line 522
    :goto_0
    invoke-virtual {p0, p2}, Lcom/google/android/videos/ui/DefaultItemAnimator;->setViewStateAfterEndChangeAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 523
    invoke-virtual {p0, p2, v0}, Lcom/google/android/videos/ui/DefaultItemAnimator;->dispatchChangeFinished(Landroid/support/v7/widget/RecyclerView$ViewHolder;Z)V

    .line 524
    const/4 v1, 0x1

    :goto_1
    return v1

    .line 516
    :cond_0
    iget-object v1, p1, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;->oldHolder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    if-ne v1, p2, :cond_1

    .line 517
    iput-object v2, p1, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;->oldHolder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .line 518
    const/4 v0, 0x1

    goto :goto_0

    .line 520
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public animateAdd(Landroid/support/v7/widget/RecyclerView$ViewHolder;)Z
    .locals 1
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 434
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    invoke-virtual {p0, p1}, Lcom/google/android/videos/ui/DefaultItemAnimator;->endAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 435
    invoke-virtual {p0, p1}, Lcom/google/android/videos/ui/DefaultItemAnimator;->prepareAddAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 436
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingAdditions:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 437
    const/4 v0, 0x1

    return v0
.end method

.method public animateChange(Landroid/support/v7/widget/RecyclerView$ViewHolder;Landroid/support/v7/widget/RecyclerView$ViewHolder;IIII)Z
    .locals 9
    .param p1, "oldHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "newHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p3, "fromX"    # I
    .param p4, "fromY"    # I
    .param p5, "toX"    # I
    .param p6, "toY"    # I

    .prologue
    .line 478
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    invoke-virtual/range {p0 .. p6}, Lcom/google/android/videos/ui/DefaultItemAnimator;->prepareChangeAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;Landroid/support/v7/widget/RecyclerView$ViewHolder;IIII)V

    .line 479
    iget-object v8, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingChanges:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;

    const/4 v7, 0x0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;-><init>(Landroid/support/v7/widget/RecyclerView$ViewHolder;Landroid/support/v7/widget/RecyclerView$ViewHolder;IIIILcom/google/android/videos/ui/DefaultItemAnimator$1;)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 480
    const/4 v0, 0x1

    return v0
.end method

.method public animateMove(Landroid/support/v7/widget/RecyclerView$ViewHolder;IIII)Z
    .locals 8
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "fromX"    # I
    .param p3, "fromY"    # I
    .param p4, "toX"    # I
    .param p5, "toY"    # I

    .prologue
    .line 448
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    int-to-float v0, p2

    iget-object v1, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-static {v1}, Landroid/support/v4/view/ViewCompat;->getTranslationX(Landroid/view/View;)F

    move-result v1

    add-float/2addr v0, v1

    float-to-int p2, v0

    .line 449
    int-to-float v0, p3

    iget-object v1, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-static {v1}, Landroid/support/v4/view/ViewCompat;->getTranslationY(Landroid/view/View;)F

    move-result v1

    add-float/2addr v0, v1

    float-to-int p3, v0

    .line 450
    invoke-virtual {p0, p1}, Lcom/google/android/videos/ui/DefaultItemAnimator;->endAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 451
    invoke-virtual/range {p0 .. p5}, Lcom/google/android/videos/ui/DefaultItemAnimator;->prepareMoveAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;IIII)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 452
    iget-object v7, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingMoves:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;

    const/4 v6, 0x0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;-><init>(Landroid/support/v7/widget/RecyclerView$ViewHolder;IIIILcom/google/android/videos/ui/DefaultItemAnimator$1;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 453
    const/4 v0, 0x1

    .line 455
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public animateRemove(Landroid/support/v7/widget/RecyclerView$ViewHolder;)Z
    .locals 1
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 422
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    invoke-virtual {p0, p1}, Lcom/google/android/videos/ui/DefaultItemAnimator;->endAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 423
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingRemovals:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 424
    const/4 v0, 0x1

    return v0
.end method

.method protected dispatchFinishedWhenDone()V
    .locals 1

    .prologue
    .line 628
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    invoke-virtual {p0}, Lcom/google/android/videos/ui/DefaultItemAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 629
    invoke-virtual {p0}, Lcom/google/android/videos/ui/DefaultItemAnimator;->dispatchAnimationsFinished()V

    .line 631
    :cond_0
    return-void
.end method

.method public endAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 10
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 529
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    move-object v3, p1

    .line 530
    .local v3, "item":Landroid/support/v7/widget/RecyclerView$ViewHolder;, "TT;"
    iget-object v7, v3, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 532
    .local v7, "view":Landroid/view/View;
    invoke-static {v7}, Landroid/support/v4/view/ViewCompat;->animate(Landroid/view/View;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->cancel()V

    .line 534
    iget-object v8, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingMoves:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v2, v8, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_1

    .line 535
    iget-object v8, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingMoves:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;

    .line 536
    .local v5, "moveInfo":Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;, "Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo<TT;>;"
    iget-object v8, v5, Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;->holder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    if-ne v8, v3, :cond_0

    .line 537
    invoke-virtual {p0, v3}, Lcom/google/android/videos/ui/DefaultItemAnimator;->setViewStateAfterEndMoveAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 538
    invoke-virtual {p0, v3}, Lcom/google/android/videos/ui/DefaultItemAnimator;->dispatchMoveFinished(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 539
    iget-object v8, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingMoves:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 534
    :cond_0
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 542
    .end local v5    # "moveInfo":Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;, "Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo<TT;>;"
    :cond_1
    iget-object v8, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingChanges:Ljava/util/ArrayList;

    invoke-direct {p0, v8, v3}, Lcom/google/android/videos/ui/DefaultItemAnimator;->endChangeAnimation(Ljava/util/List;Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 543
    iget-object v8, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingRemovals:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 544
    invoke-virtual {p0, v3}, Lcom/google/android/videos/ui/DefaultItemAnimator;->setViewStateAfterEndRemoveAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 545
    invoke-virtual {p0, v3}, Lcom/google/android/videos/ui/DefaultItemAnimator;->dispatchRemoveFinished(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 547
    :cond_2
    iget-object v8, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingAdditions:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 548
    invoke-virtual {p0, v3}, Lcom/google/android/videos/ui/DefaultItemAnimator;->setViewStateAfterEndAdditionAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 549
    invoke-virtual {p0, v3}, Lcom/google/android/videos/ui/DefaultItemAnimator;->dispatchAddFinished(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 552
    :cond_3
    iget-object v8, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mChangesList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v2, v8, -0x1

    :goto_1
    if-ltz v2, :cond_5

    .line 553
    iget-object v8, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mChangesList:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 554
    .local v1, "changes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo<TT;>;>;"
    invoke-direct {p0, v1, v3}, Lcom/google/android/videos/ui/DefaultItemAnimator;->endChangeAnimation(Ljava/util/List;Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 555
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 556
    iget-object v8, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mChangesList:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 552
    :cond_4
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 559
    .end local v1    # "changes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo<TT;>;>;"
    :cond_5
    iget-object v8, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mMovesList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v2, v8, -0x1

    :goto_2
    if-ltz v2, :cond_8

    .line 560
    iget-object v8, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mMovesList:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/ArrayList;

    .line 561
    .local v6, "moves":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo<TT;>;>;"
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v4, v8, -0x1

    .local v4, "j":I
    :goto_3
    if-ltz v4, :cond_6

    .line 562
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;

    .line 563
    .restart local v5    # "moveInfo":Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;, "Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo<TT;>;"
    iget-object v8, v5, Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;->holder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    if-ne v8, v3, :cond_7

    .line 564
    invoke-virtual {p0, v3}, Lcom/google/android/videos/ui/DefaultItemAnimator;->setViewStateAfterEndMoveAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 565
    invoke-virtual {p0, v3}, Lcom/google/android/videos/ui/DefaultItemAnimator;->dispatchMoveFinished(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 566
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 567
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 568
    iget-object v8, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mMovesList:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 559
    .end local v5    # "moveInfo":Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;, "Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo<TT;>;"
    :cond_6
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    .line 561
    .restart local v5    # "moveInfo":Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;, "Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo<TT;>;"
    :cond_7
    add-int/lit8 v4, v4, -0x1

    goto :goto_3

    .line 574
    .end local v4    # "j":I
    .end local v5    # "moveInfo":Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;, "Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo<TT;>;"
    .end local v6    # "moves":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo<TT;>;>;"
    :cond_8
    iget-object v8, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mAdditionsList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v2, v8, -0x1

    :goto_4
    if-ltz v2, :cond_a

    .line 575
    iget-object v8, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mAdditionsList:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 576
    .local v0, "additions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 577
    invoke-virtual {p0, v3}, Lcom/google/android/videos/ui/DefaultItemAnimator;->setViewStateAfterEndAdditionAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 578
    invoke-virtual {p0, v3}, Lcom/google/android/videos/ui/DefaultItemAnimator;->dispatchAddFinished(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 579
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_9

    .line 580
    iget-object v8, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mAdditionsList:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 574
    :cond_9
    add-int/lit8 v2, v2, -0x1

    goto :goto_4

    .line 586
    .end local v0    # "additions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    :cond_a
    iget-object v8, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mRemoveAnimations:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 587
    new-instance v8, Ljava/lang/IllegalStateException;

    const-string v9, "after animation is cancelled, item should not be in mRemoveAnimations list"

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 591
    :cond_b
    iget-object v8, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mAddAnimations:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 592
    new-instance v8, Ljava/lang/IllegalStateException;

    const-string v9, "after animation is cancelled, item should not be in mAddAnimations list"

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 596
    :cond_c
    iget-object v8, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mChangeAnimations:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_d

    .line 597
    new-instance v8, Ljava/lang/IllegalStateException;

    const-string v9, "after animation is cancelled, item should not be in mChangeAnimations list"

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 601
    :cond_d
    iget-object v8, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mMoveAnimations:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_e

    .line 602
    new-instance v8, Ljava/lang/IllegalStateException;

    const-string v9, "after animation is cancelled, item should not be in mMoveAnimations list"

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 605
    :cond_e
    invoke-virtual {p0}, Lcom/google/android/videos/ui/DefaultItemAnimator;->dispatchFinishedWhenDone()V

    .line 606
    return-void
.end method

.method public endAnimations()V
    .locals 12

    .prologue
    .line 635
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    iget-object v11, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingMoves:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 636
    .local v2, "count":I
    add-int/lit8 v3, v2, -0x1

    .local v3, "i":I
    :goto_0
    if-ltz v3, :cond_0

    .line 637
    iget-object v11, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingMoves:Ljava/util/ArrayList;

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;

    .line 638
    .local v5, "item":Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;, "Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo<TT;>;"
    iget-object v11, v5, Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;->holder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    invoke-virtual {p0, v11}, Lcom/google/android/videos/ui/DefaultItemAnimator;->setViewStateAfterEndMoveAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 639
    iget-object v11, v5, Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;->holder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    invoke-virtual {p0, v11}, Lcom/google/android/videos/ui/DefaultItemAnimator;->dispatchMoveFinished(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 640
    iget-object v11, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingMoves:Ljava/util/ArrayList;

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 636
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 642
    .end local v5    # "item":Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;, "Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo<TT;>;"
    :cond_0
    iget-object v11, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingRemovals:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 643
    add-int/lit8 v3, v2, -0x1

    :goto_1
    if-ltz v3, :cond_1

    .line 644
    iget-object v11, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingRemovals:Ljava/util/ArrayList;

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .line 645
    .local v4, "item":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-virtual {p0, v4}, Lcom/google/android/videos/ui/DefaultItemAnimator;->dispatchRemoveFinished(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 646
    iget-object v11, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingRemovals:Ljava/util/ArrayList;

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 643
    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    .line 648
    .end local v4    # "item":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    :cond_1
    iget-object v11, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingAdditions:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 649
    add-int/lit8 v3, v2, -0x1

    :goto_2
    if-ltz v3, :cond_2

    .line 650
    iget-object v11, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingAdditions:Ljava/util/ArrayList;

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .line 651
    .local v6, "item":Landroid/support/v7/widget/RecyclerView$ViewHolder;, "TT;"
    invoke-virtual {p0, v6}, Lcom/google/android/videos/ui/DefaultItemAnimator;->setViewStateAfterEndAdditionAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 652
    invoke-virtual {p0, v6}, Lcom/google/android/videos/ui/DefaultItemAnimator;->dispatchAddFinished(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 653
    iget-object v11, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingAdditions:Ljava/util/ArrayList;

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 649
    add-int/lit8 v3, v3, -0x1

    goto :goto_2

    .line 655
    .end local v6    # "item":Landroid/support/v7/widget/RecyclerView$ViewHolder;, "TT;"
    :cond_2
    iget-object v11, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingChanges:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 656
    add-int/lit8 v3, v2, -0x1

    :goto_3
    if-ltz v3, :cond_3

    .line 657
    iget-object v11, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingChanges:Ljava/util/ArrayList;

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;

    invoke-direct {p0, v11}, Lcom/google/android/videos/ui/DefaultItemAnimator;->endChangeAnimationIfNecessary(Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;)V

    .line 656
    add-int/lit8 v3, v3, -0x1

    goto :goto_3

    .line 659
    :cond_3
    iget-object v11, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingChanges:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->clear()V

    .line 660
    invoke-virtual {p0}, Lcom/google/android/videos/ui/DefaultItemAnimator;->isRunning()Z

    move-result v11

    if-nez v11, :cond_4

    .line 710
    :goto_4
    return-void

    .line 664
    :cond_4
    iget-object v11, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mMovesList:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 665
    .local v8, "listCount":I
    add-int/lit8 v3, v8, -0x1

    :goto_5
    if-ltz v3, :cond_7

    .line 666
    iget-object v11, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mMovesList:Ljava/util/ArrayList;

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/ArrayList;

    .line 667
    .local v10, "moves":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo<TT;>;>;"
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 668
    add-int/lit8 v7, v2, -0x1

    .local v7, "j":I
    :goto_6
    if-ltz v7, :cond_6

    .line 669
    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;

    .line 670
    .local v9, "moveInfo":Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;, "Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo<TT;>;"
    iget-object v11, v9, Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;->holder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    invoke-virtual {p0, v11}, Lcom/google/android/videos/ui/DefaultItemAnimator;->setViewStateAfterEndMoveAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 671
    iget-object v11, v9, Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;->holder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    invoke-virtual {p0, v11}, Lcom/google/android/videos/ui/DefaultItemAnimator;->dispatchMoveFinished(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 672
    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 673
    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_5

    .line 674
    iget-object v11, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mMovesList:Ljava/util/ArrayList;

    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 668
    :cond_5
    add-int/lit8 v7, v7, -0x1

    goto :goto_6

    .line 665
    .end local v9    # "moveInfo":Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;, "Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo<TT;>;"
    :cond_6
    add-int/lit8 v3, v3, -0x1

    goto :goto_5

    .line 678
    .end local v7    # "j":I
    .end local v10    # "moves":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo<TT;>;>;"
    :cond_7
    iget-object v11, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mAdditionsList:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 679
    add-int/lit8 v3, v8, -0x1

    :goto_7
    if-ltz v3, :cond_a

    .line 680
    iget-object v11, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mAdditionsList:Ljava/util/ArrayList;

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 681
    .local v0, "additions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 682
    add-int/lit8 v7, v2, -0x1

    .restart local v7    # "j":I
    :goto_8
    if-ltz v7, :cond_9

    .line 683
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .line 684
    .restart local v6    # "item":Landroid/support/v7/widget/RecyclerView$ViewHolder;, "TT;"
    invoke-virtual {p0, v6}, Lcom/google/android/videos/ui/DefaultItemAnimator;->setViewStateAfterEndAdditionAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 685
    invoke-virtual {p0, v6}, Lcom/google/android/videos/ui/DefaultItemAnimator;->dispatchAddFinished(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 686
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 687
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_8

    .line 688
    iget-object v11, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mAdditionsList:Ljava/util/ArrayList;

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 682
    :cond_8
    add-int/lit8 v7, v7, -0x1

    goto :goto_8

    .line 679
    .end local v6    # "item":Landroid/support/v7/widget/RecyclerView$ViewHolder;, "TT;"
    :cond_9
    add-int/lit8 v3, v3, -0x1

    goto :goto_7

    .line 692
    .end local v0    # "additions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    .end local v7    # "j":I
    :cond_a
    iget-object v11, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mChangesList:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 693
    add-int/lit8 v3, v8, -0x1

    :goto_9
    if-ltz v3, :cond_d

    .line 694
    iget-object v11, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mChangesList:Ljava/util/ArrayList;

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 695
    .local v1, "changes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo<TT;>;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 696
    add-int/lit8 v7, v2, -0x1

    .restart local v7    # "j":I
    :goto_a
    if-ltz v7, :cond_c

    .line 697
    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;

    invoke-direct {p0, v11}, Lcom/google/android/videos/ui/DefaultItemAnimator;->endChangeAnimationIfNecessary(Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;)V

    .line 698
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_b

    .line 699
    iget-object v11, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mChangesList:Ljava/util/ArrayList;

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 696
    :cond_b
    add-int/lit8 v7, v7, -0x1

    goto :goto_a

    .line 693
    :cond_c
    add-int/lit8 v3, v3, -0x1

    goto :goto_9

    .line 704
    .end local v1    # "changes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo<TT;>;>;"
    .end local v7    # "j":I
    :cond_d
    iget-object v11, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mRemoveAnimations:Ljava/util/ArrayList;

    invoke-direct {p0, v11}, Lcom/google/android/videos/ui/DefaultItemAnimator;->cancelAll(Ljava/util/List;)V

    .line 705
    iget-object v11, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mMoveAnimations:Ljava/util/ArrayList;

    invoke-direct {p0, v11}, Lcom/google/android/videos/ui/DefaultItemAnimator;->cancelAll(Ljava/util/List;)V

    .line 706
    iget-object v11, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mAddAnimations:Ljava/util/ArrayList;

    invoke-direct {p0, v11}, Lcom/google/android/videos/ui/DefaultItemAnimator;->cancelAll(Ljava/util/List;)V

    .line 707
    iget-object v11, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mChangeAnimations:Ljava/util/ArrayList;

    invoke-direct {p0, v11}, Lcom/google/android/videos/ui/DefaultItemAnimator;->cancelAll(Ljava/util/List;)V

    .line 709
    invoke-virtual {p0}, Lcom/google/android/videos/ui/DefaultItemAnimator;->dispatchAnimationsFinished()V

    goto/16 :goto_4
.end method

.method public isRunning()Z
    .locals 1

    .prologue
    .line 610
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingAdditions:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingChanges:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingMoves:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingRemovals:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mMoveAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mRemoveAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mAddAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mChangeAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mMovesList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mAdditionsList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mChangesList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAddAnimationEnd(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 312
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    .local p1, "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;, "TT;"
    invoke-virtual {p0, p1}, Lcom/google/android/videos/ui/DefaultItemAnimator;->setViewStateAfterEndAdditionAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 313
    invoke-virtual {p0, p1}, Lcom/google/android/videos/ui/DefaultItemAnimator;->dispatchAddFinished(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 314
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mAddAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 315
    invoke-virtual {p0}, Lcom/google/android/videos/ui/DefaultItemAnimator;->dispatchFinishedWhenDone()V

    .line 316
    return-void
.end method

.method protected onChangeAnimationEnd(Landroid/support/v7/widget/RecyclerView$ViewHolder;Z)V
    .locals 1
    .param p2, "isOld"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z)V"
        }
    .end annotation

    .prologue
    .line 319
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    .local p1, "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;, "TT;"
    invoke-virtual {p0, p1}, Lcom/google/android/videos/ui/DefaultItemAnimator;->setViewStateAfterEndChangeAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 320
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/DefaultItemAnimator;->dispatchChangeFinished(Landroid/support/v7/widget/RecyclerView$ViewHolder;Z)V

    .line 321
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mChangeAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 322
    invoke-virtual {p0}, Lcom/google/android/videos/ui/DefaultItemAnimator;->dispatchFinishedWhenDone()V

    .line 323
    return-void
.end method

.method protected onRemoveAnimationEnd(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 305
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    .local p1, "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;, "TT;"
    invoke-virtual {p0, p1}, Lcom/google/android/videos/ui/DefaultItemAnimator;->setViewStateAfterEndRemoveAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 306
    invoke-virtual {p0, p1}, Lcom/google/android/videos/ui/DefaultItemAnimator;->dispatchRemoveFinished(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 307
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mRemoveAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 308
    invoke-virtual {p0}, Lcom/google/android/videos/ui/DefaultItemAnimator;->dispatchFinishedWhenDone()V

    .line 309
    return-void
.end method

.method protected prepareAddAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 87
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    .local p1, "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;, "TT;"
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->setAlpha(Landroid/view/View;F)V

    .line 88
    return-void
.end method

.method protected prepareChangeAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;Landroid/support/v7/widget/RecyclerView$ViewHolder;IIII)V
    .locals 7
    .param p3, "fromX"    # I
    .param p4, "fromY"    # I
    .param p5, "toX"    # I
    .param p6, "toY"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;IIII)V"
        }
    .end annotation

    .prologue
    .line 117
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    .local p1, "oldHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;, "TT;"
    .local p2, "newHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;, "TT;"
    iget-object v5, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-static {v5}, Landroid/support/v4/view/ViewCompat;->getTranslationX(Landroid/view/View;)F

    move-result v3

    .line 118
    .local v3, "prevTranslationX":F
    iget-object v5, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-static {v5}, Landroid/support/v4/view/ViewCompat;->getTranslationY(Landroid/view/View;)F

    move-result v4

    .line 119
    .local v4, "prevTranslationY":F
    iget-object v5, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-static {v5}, Landroid/support/v4/view/ViewCompat;->getAlpha(Landroid/view/View;)F

    move-result v2

    .line 120
    .local v2, "prevAlpha":F
    invoke-virtual {p0, p1}, Lcom/google/android/videos/ui/DefaultItemAnimator;->endAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 121
    sub-int v5, p5, p3

    int-to-float v5, v5

    sub-float/2addr v5, v3

    float-to-int v0, v5

    .line 122
    .local v0, "deltaX":I
    sub-int v5, p6, p4

    int-to-float v5, v5

    sub-float/2addr v5, v4

    float-to-int v1, v5

    .line 124
    .local v1, "deltaY":I
    iget-object v5, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-static {v5, v3}, Landroid/support/v4/view/ViewCompat;->setTranslationX(Landroid/view/View;F)V

    .line 125
    iget-object v5, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-static {v5, v4}, Landroid/support/v4/view/ViewCompat;->setTranslationY(Landroid/view/View;F)V

    .line 126
    iget-object v5, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-static {v5, v2}, Landroid/support/v4/view/ViewCompat;->setAlpha(Landroid/view/View;F)V

    .line 127
    if-eqz p2, :cond_0

    iget-object v5, p2, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    if-eqz v5, :cond_0

    .line 129
    invoke-virtual {p0, p2}, Lcom/google/android/videos/ui/DefaultItemAnimator;->endAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 130
    iget-object v5, p2, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    neg-int v6, v0

    int-to-float v6, v6

    invoke-static {v5, v6}, Landroid/support/v4/view/ViewCompat;->setTranslationX(Landroid/view/View;F)V

    .line 131
    iget-object v5, p2, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    neg-int v6, v1

    int-to-float v6, v6

    invoke-static {v5, v6}, Landroid/support/v4/view/ViewCompat;->setTranslationY(Landroid/view/View;F)V

    .line 132
    iget-object v5, p2, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const/4 v6, 0x0

    invoke-static {v5, v6}, Landroid/support/v4/view/ViewCompat;->setAlpha(Landroid/view/View;F)V

    .line 134
    :cond_0
    return-void
.end method

.method protected prepareMoveAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;IIII)Z
    .locals 4
    .param p2, "fromX"    # I
    .param p3, "fromY"    # I
    .param p4, "toX"    # I
    .param p5, "toY"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;IIII)Z"
        }
    .end annotation

    .prologue
    .line 96
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    .local p1, "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;, "TT;"
    iget-object v2, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 97
    .local v2, "view":Landroid/view/View;
    sub-int v0, p4, p2

    .line 98
    .local v0, "deltaX":I
    sub-int v1, p5, p3

    .line 99
    .local v1, "deltaY":I
    if-nez v0, :cond_0

    if-nez v1, :cond_0

    .line 100
    invoke-virtual {p0, p1}, Lcom/google/android/videos/ui/DefaultItemAnimator;->dispatchMoveFinished(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 101
    const/4 v3, 0x0

    .line 109
    :goto_0
    return v3

    .line 103
    :cond_0
    if-eqz v0, :cond_1

    .line 104
    neg-int v3, v0

    int-to-float v3, v3

    invoke-static {v2, v3}, Landroid/support/v4/view/ViewCompat;->setTranslationX(Landroid/view/View;F)V

    .line 106
    :cond_1
    if-eqz v1, :cond_2

    .line 107
    neg-int v3, v1

    int-to-float v3, v3

    invoke-static {v2, v3}, Landroid/support/v4/view/ViewCompat;->setTranslationY(Landroid/view/View;F)V

    .line 109
    :cond_2
    const/4 v3, 0x1

    goto :goto_0
.end method

.method protected runAddAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 164
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    .local p1, "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;, "TT;"
    iget-object v1, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 165
    .local v1, "view":Landroid/view/View;
    invoke-static {v1}, Landroid/support/v4/view/ViewCompat;->animate(Landroid/view/View;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    .line 166
    .local v0, "animation":Landroid/support/v4/view/ViewPropertyAnimatorCompat;
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->alpha(F)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/videos/ui/DefaultItemAnimator;->getAddDuration()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setDuration(J)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v2

    new-instance v3, Lcom/google/android/videos/ui/DefaultItemAnimator$2;

    invoke-direct {v3, p0, p1, v0}, Lcom/google/android/videos/ui/DefaultItemAnimator$2;-><init>(Lcom/google/android/videos/ui/DefaultItemAnimator;Landroid/support/v7/widget/RecyclerView$ViewHolder;Landroid/support/v4/view/ViewPropertyAnimatorCompat;)V

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setListener(Landroid/support/v4/view/ViewPropertyAnimatorListener;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->start()V

    .line 186
    return-void
.end method

.method protected runMoveAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;IIII)V
    .locals 8
    .param p2, "fromX"    # I
    .param p3, "fromY"    # I
    .param p4, "toX"    # I
    .param p5, "toY"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;IIII)V"
        }
    .end annotation

    .prologue
    .line 189
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    .local p1, "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;, "TT;"
    sub-int v3, p4, p2

    .line 190
    .local v3, "deltaX":I
    sub-int v4, p5, p3

    .line 191
    .local v4, "deltaY":I
    iget-object v6, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 192
    .local v6, "view":Landroid/view/View;
    invoke-static {v6}, Landroid/support/v4/view/ViewCompat;->animate(Landroid/view/View;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v5

    .line 193
    .local v5, "animation":Landroid/support/v4/view/ViewPropertyAnimatorCompat;
    invoke-virtual {p0}, Lcom/google/android/videos/ui/DefaultItemAnimator;->getMoveDuration()J

    move-result-wide v0

    invoke-virtual {v5, v0, v1}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setDuration(J)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v7

    new-instance v0, Lcom/google/android/videos/ui/DefaultItemAnimator$3;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/ui/DefaultItemAnimator$3;-><init>(Lcom/google/android/videos/ui/DefaultItemAnimator;Landroid/support/v7/widget/RecyclerView$ViewHolder;IILandroid/support/v4/view/ViewPropertyAnimatorCompat;)V

    invoke-virtual {v7, v0}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setListener(Landroid/support/v4/view/ViewPropertyAnimatorListener;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->start()V

    .line 217
    return-void
.end method

.method protected runNewViewChangeAnimation(Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    .local p1, "changeInfo":Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;, "Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo<TT;>;"
    const/4 v3, 0x0

    .line 251
    iget-object v2, p1, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;->newHolder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    iget-object v0, v2, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 252
    .local v0, "newView":Landroid/view/View;
    invoke-static {v0}, Landroid/support/v4/view/ViewCompat;->animate(Landroid/view/View;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v1

    .line 253
    .local v1, "newViewAnimation":Landroid/support/v4/view/ViewPropertyAnimatorCompat;
    invoke-virtual {v1, v3}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->translationX(F)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->translationY(F)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/videos/ui/DefaultItemAnimator;->getChangeDuration()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setDuration(J)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->alpha(F)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v2

    new-instance v3, Lcom/google/android/videos/ui/DefaultItemAnimator$5;

    invoke-direct {v3, p0, p1, v1, v0}, Lcom/google/android/videos/ui/DefaultItemAnimator$5;-><init>(Lcom/google/android/videos/ui/DefaultItemAnimator;Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;Landroid/support/v4/view/ViewPropertyAnimatorCompat;Landroid/view/View;)V

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setListener(Landroid/support/v4/view/ViewPropertyAnimatorListener;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->start()V

    .line 271
    return-void
.end method

.method protected runOldViewChangeAnimation(Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 223
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    .local p1, "changeInfo":Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;, "Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo<TT;>;"
    iget-object v2, p1, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;->oldHolder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    iget-object v1, v2, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 224
    .local v1, "view":Landroid/view/View;
    invoke-static {v1}, Landroid/support/v4/view/ViewCompat;->animate(Landroid/view/View;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/videos/ui/DefaultItemAnimator;->getChangeDuration()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setDuration(J)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    .line 226
    .local v0, "oldViewAnim":Landroid/support/v4/view/ViewPropertyAnimatorCompat;
    iget v2, p1, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;->toX:I

    iget v3, p1, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;->fromX:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->translationX(F)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    .line 227
    iget v2, p1, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;->toY:I

    iget v3, p1, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;->fromY:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->translationY(F)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    .line 228
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->alpha(F)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v2

    new-instance v3, Lcom/google/android/videos/ui/DefaultItemAnimator$4;

    invoke-direct {v3, p0, p1, v0}, Lcom/google/android/videos/ui/DefaultItemAnimator$4;-><init>(Lcom/google/android/videos/ui/DefaultItemAnimator;Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;Landroid/support/v4/view/ViewPropertyAnimatorCompat;)V

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setListener(Landroid/support/v4/view/ViewPropertyAnimatorListener;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->start()V

    .line 245
    return-void
.end method

.method public runPendingAnimations()V
    .locals 30

    .prologue
    .line 331
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingRemovals:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v26

    if-nez v26, :cond_1

    const/16 v20, 0x1

    .line 332
    .local v20, "removalsPending":Z
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingMoves:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v26

    if-nez v26, :cond_2

    const/16 v19, 0x1

    .line 333
    .local v19, "movesPending":Z
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingChanges:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v26

    if-nez v26, :cond_3

    const/4 v11, 0x1

    .line 334
    .local v11, "changesPending":Z
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingAdditions:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v26

    if-nez v26, :cond_4

    const/4 v6, 0x1

    .line 335
    .local v6, "additionsPending":Z
    :goto_3
    if-nez v20, :cond_5

    if-nez v19, :cond_5

    if-nez v6, :cond_5

    if-nez v11, :cond_5

    .line 418
    :cond_0
    :goto_4
    return-void

    .line 331
    .end local v6    # "additionsPending":Z
    .end local v11    # "changesPending":Z
    .end local v19    # "movesPending":Z
    .end local v20    # "removalsPending":Z
    :cond_1
    const/16 v20, 0x0

    goto :goto_0

    .line 332
    .restart local v20    # "removalsPending":Z
    :cond_2
    const/16 v19, 0x0

    goto :goto_1

    .line 333
    .restart local v19    # "movesPending":Z
    :cond_3
    const/4 v11, 0x0

    goto :goto_2

    .line 334
    .restart local v11    # "changesPending":Z
    :cond_4
    const/4 v6, 0x0

    goto :goto_3

    .line 340
    .restart local v6    # "additionsPending":Z
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingRemovals:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v26

    if-eqz v26, :cond_6

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .line 341
    .local v13, "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;, "TT;"
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/google/android/videos/ui/DefaultItemAnimator;->animateRemoveImpl(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    goto :goto_5

    .line 343
    .end local v13    # "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;, "TT;"
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingRemovals:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->clear()V

    .line 345
    if-eqz v19, :cond_7

    .line 346
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 347
    .local v18, "moves":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo<TT;>;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingMoves:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 348
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mMovesList:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 349
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingMoves:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->clear()V

    .line 350
    new-instance v15, Lcom/google/android/videos/ui/DefaultItemAnimator$6;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v15, v0, v1}, Lcom/google/android/videos/ui/DefaultItemAnimator$6;-><init>(Lcom/google/android/videos/ui/DefaultItemAnimator;Ljava/util/ArrayList;)V

    .line 361
    .local v15, "mover":Ljava/lang/Runnable;
    if-eqz v20, :cond_a

    .line 362
    const/16 v26, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;->holder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    move-object/from16 v21, v0

    .line 363
    .local v21, "view":Landroid/view/View;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/ui/DefaultItemAnimator;->getRemoveDuration()J

    move-result-wide v26

    move-object/from16 v0, v21

    move-wide/from16 v1, v26

    invoke-static {v0, v15, v1, v2}, Landroid/support/v4/view/ViewCompat;->postOnAnimationDelayed(Landroid/view/View;Ljava/lang/Runnable;J)V

    .line 369
    .end local v15    # "mover":Ljava/lang/Runnable;
    .end local v18    # "moves":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo<TT;>;>;"
    .end local v21    # "view":Landroid/view/View;
    :cond_7
    :goto_6
    if-eqz v11, :cond_8

    .line 370
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 371
    .local v10, "changes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo<TT;>;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingChanges:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 372
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mChangesList:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 373
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingChanges:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->clear()V

    .line 374
    new-instance v7, Lcom/google/android/videos/ui/DefaultItemAnimator$7;

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v10}, Lcom/google/android/videos/ui/DefaultItemAnimator$7;-><init>(Lcom/google/android/videos/ui/DefaultItemAnimator;Ljava/util/ArrayList;)V

    .line 384
    .local v7, "changer":Ljava/lang/Runnable;
    if-eqz v20, :cond_b

    .line 385
    const/16 v26, 0x0

    move/from16 v0, v26

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;

    move-object/from16 v0, v26

    iget-object v12, v0, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;->oldHolder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .line 386
    .local v12, "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    iget-object v0, v12, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    move-object/from16 v26, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/ui/DefaultItemAnimator;->getRemoveDuration()J

    move-result-wide v28

    move-object/from16 v0, v26

    move-wide/from16 v1, v28

    invoke-static {v0, v7, v1, v2}, Landroid/support/v4/view/ViewCompat;->postOnAnimationDelayed(Landroid/view/View;Ljava/lang/Runnable;J)V

    .line 392
    .end local v7    # "changer":Ljava/lang/Runnable;
    .end local v10    # "changes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo<TT;>;>;"
    .end local v12    # "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    :cond_8
    :goto_7
    if-eqz v6, :cond_0

    .line 393
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 394
    .local v5, "additions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingAdditions:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 395
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mAdditionsList:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 396
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/DefaultItemAnimator;->mPendingAdditions:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->clear()V

    .line 397
    new-instance v4, Lcom/google/android/videos/ui/DefaultItemAnimator$8;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5}, Lcom/google/android/videos/ui/DefaultItemAnimator$8;-><init>(Lcom/google/android/videos/ui/DefaultItemAnimator;Ljava/util/ArrayList;)V

    .line 407
    .local v4, "adder":Ljava/lang/Runnable;
    if-nez v20, :cond_9

    if-nez v19, :cond_9

    if-eqz v11, :cond_f

    .line 408
    :cond_9
    if-eqz v20, :cond_c

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/ui/DefaultItemAnimator;->getRemoveDuration()J

    move-result-wide v22

    .line 409
    .local v22, "removeDuration":J
    :goto_8
    if-eqz v19, :cond_d

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/ui/DefaultItemAnimator;->getMoveDuration()J

    move-result-wide v16

    .line 410
    .local v16, "moveDuration":J
    :goto_9
    if-eqz v11, :cond_e

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/ui/DefaultItemAnimator;->getChangeDuration()J

    move-result-wide v8

    .line 411
    .local v8, "changeDuration":J
    :goto_a
    move-wide/from16 v0, v16

    invoke-static {v0, v1, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v26

    add-long v24, v22, v26

    .line 412
    .local v24, "totalDelay":J
    const/16 v26, 0x0

    move/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-object/from16 v0, v26

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    move-object/from16 v21, v0

    .line 413
    .restart local v21    # "view":Landroid/view/View;
    move-object/from16 v0, v21

    move-wide/from16 v1, v24

    invoke-static {v0, v4, v1, v2}, Landroid/support/v4/view/ViewCompat;->postOnAnimationDelayed(Landroid/view/View;Ljava/lang/Runnable;J)V

    goto/16 :goto_4

    .line 365
    .end local v4    # "adder":Ljava/lang/Runnable;
    .end local v5    # "additions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    .end local v8    # "changeDuration":J
    .end local v16    # "moveDuration":J
    .end local v21    # "view":Landroid/view/View;
    .end local v22    # "removeDuration":J
    .end local v24    # "totalDelay":J
    .restart local v15    # "mover":Ljava/lang/Runnable;
    .restart local v18    # "moves":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo<TT;>;>;"
    :cond_a
    invoke-interface {v15}, Ljava/lang/Runnable;->run()V

    goto/16 :goto_6

    .line 388
    .end local v15    # "mover":Ljava/lang/Runnable;
    .end local v18    # "moves":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo<TT;>;>;"
    .restart local v7    # "changer":Ljava/lang/Runnable;
    .restart local v10    # "changes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo<TT;>;>;"
    :cond_b
    invoke-interface {v7}, Ljava/lang/Runnable;->run()V

    goto :goto_7

    .line 408
    .end local v7    # "changer":Ljava/lang/Runnable;
    .end local v10    # "changes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo<TT;>;>;"
    .restart local v4    # "adder":Ljava/lang/Runnable;
    .restart local v5    # "additions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    :cond_c
    const-wide/16 v22, 0x0

    goto :goto_8

    .line 409
    .restart local v22    # "removeDuration":J
    :cond_d
    const-wide/16 v16, 0x0

    goto :goto_9

    .line 410
    .restart local v16    # "moveDuration":J
    :cond_e
    const-wide/16 v8, 0x0

    goto :goto_a

    .line 415
    .end local v16    # "moveDuration":J
    .end local v22    # "removeDuration":J
    :cond_f
    invoke-interface {v4}, Ljava/lang/Runnable;->run()V

    goto/16 :goto_4
.end method

.method protected runRemoveAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 140
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    .local p1, "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;, "TT;"
    iget-object v1, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 141
    .local v1, "view":Landroid/view/View;
    invoke-static {v1}, Landroid/support/v4/view/ViewCompat;->animate(Landroid/view/View;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    .line 142
    .local v0, "animation":Landroid/support/v4/view/ViewPropertyAnimatorCompat;
    invoke-virtual {p0}, Lcom/google/android/videos/ui/DefaultItemAnimator;->getRemoveDuration()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setDuration(J)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->alpha(F)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v2

    new-instance v3, Lcom/google/android/videos/ui/DefaultItemAnimator$1;

    invoke-direct {v3, p0, p1, v0}, Lcom/google/android/videos/ui/DefaultItemAnimator$1;-><init>(Lcom/google/android/videos/ui/DefaultItemAnimator;Landroid/support/v7/widget/RecyclerView$ViewHolder;Landroid/support/v4/view/ViewPropertyAnimatorCompat;)V

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setListener(Landroid/support/v4/view/ViewPropertyAnimatorListener;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->start()V

    .line 158
    return-void
.end method

.method protected setViewStateAfterEndAdditionAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 284
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    .local p1, "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;, "TT;"
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->setAlpha(Landroid/view/View;F)V

    .line 285
    return-void
.end method

.method protected setViewStateAfterEndChangeAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    .local p1, "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;, "TT;"
    const/4 v2, 0x0

    .line 299
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->setAlpha(Landroid/view/View;F)V

    .line 300
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-static {v0, v2}, Landroid/support/v4/view/ViewCompat;->setTranslationX(Landroid/view/View;F)V

    .line 301
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-static {v0, v2}, Landroid/support/v4/view/ViewCompat;->setTranslationY(Landroid/view/View;F)V

    .line 302
    return-void
.end method

.method protected setViewStateAfterEndMoveAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    .local p1, "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;, "TT;"
    const/4 v1, 0x0

    .line 291
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->setTranslationY(Landroid/view/View;F)V

    .line 292
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->setTranslationX(Landroid/view/View;F)V

    .line 293
    return-void
.end method

.method protected setViewStateAfterEndRemoveAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 277
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator;, "Lcom/google/android/videos/ui/DefaultItemAnimator<TT;>;"
    .local p1, "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;, "TT;"
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->setAlpha(Landroid/view/View;F)V

    .line 278
    return-void
.end method
