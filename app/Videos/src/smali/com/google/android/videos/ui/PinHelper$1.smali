.class Lcom/google/android/videos/ui/PinHelper$1;
.super Ljava/lang/Object;
.source "PinHelper.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/ui/PinHelper;->getPurchaseCallback(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/async/Callback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/ui/PinHelper;

.field final synthetic val$account:Ljava/lang/String;

.field final synthetic val$activity:Landroid/support/v4/app/FragmentActivity;

.field final synthetic val$videoId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/videos/ui/PinHelper;Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 265
    iput-object p1, p0, Lcom/google/android/videos/ui/PinHelper$1;->this$0:Lcom/google/android/videos/ui/PinHelper;

    iput-object p2, p0, Lcom/google/android/videos/ui/PinHelper$1;->val$activity:Landroid/support/v4/app/FragmentActivity;

    iput-object p3, p0, Lcom/google/android/videos/ui/PinHelper$1;->val$account:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/videos/ui/PinHelper$1;->val$videoId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Ljava/lang/Exception;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 288
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Database request cancelled when pinning "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/ui/PinHelper$1;->val$videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 289
    iget-object v0, p0, Lcom/google/android/videos/ui/PinHelper$1;->val$activity:Landroid/support/v4/app/FragmentActivity;

    # invokes: Lcom/google/android/videos/ui/PinHelper;->onPinFailed(Landroid/app/Activity;)V
    invoke-static {v0}, Lcom/google/android/videos/ui/PinHelper;->access$100(Landroid/app/Activity;)V

    .line 290
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 265
    check-cast p1, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/PinHelper$1;->onError(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V
    .locals 6
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x1

    .line 269
    :try_start_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 270
    const/4 v1, 0x0

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    move v4, v5

    .line 272
    .local v4, "hdPurchased":Z
    :goto_0
    const/4 v1, 0x1

    invoke-static {p2, v1}, Lcom/google/android/videos/utils/DbUtils;->getStringList(Landroid/database/Cursor;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v5, :cond_1

    .line 274
    .local v5, "multipleAudioLanguages":Z
    :goto_1
    iget-object v0, p0, Lcom/google/android/videos/ui/PinHelper$1;->this$0:Lcom/google/android/videos/ui/PinHelper;

    iget-object v1, p0, Lcom/google/android/videos/ui/PinHelper$1;->val$activity:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, p0, Lcom/google/android/videos/ui/PinHelper$1;->val$account:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/ui/PinHelper$1;->val$videoId:Ljava/lang/String;

    # invokes: Lcom/google/android/videos/ui/PinHelper;->pinVideo(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;ZZ)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/ui/PinHelper;->access$000(Lcom/google/android/videos/ui/PinHelper;Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;ZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 281
    .end local v4    # "hdPurchased":Z
    .end local v5    # "multipleAudioLanguages":Z
    :goto_2
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    .line 283
    return-void

    :cond_0
    move v4, v0

    .line 270
    goto :goto_0

    .restart local v4    # "hdPurchased":Z
    :cond_1
    move v5, v0

    .line 272
    goto :goto_1

    .line 277
    .end local v4    # "hdPurchased":Z
    :cond_2
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Purchase not found when pinning "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/ui/PinHelper$1;->val$videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 278
    iget-object v0, p0, Lcom/google/android/videos/ui/PinHelper$1;->val$activity:Landroid/support/v4/app/FragmentActivity;

    # invokes: Lcom/google/android/videos/ui/PinHelper;->onPinFailed(Landroid/app/Activity;)V
    invoke-static {v0}, Lcom/google/android/videos/ui/PinHelper;->access$100(Landroid/app/Activity;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 281
    :catchall_0
    move-exception v0

    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 265
    check-cast p1, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/PinHelper$1;->onResponse(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V

    return-void
.end method
