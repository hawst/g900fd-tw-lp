.class public interface abstract Lcom/google/android/videos/api/RecommendationsRequest$Factory;
.super Ljava/lang/Object;
.source "RecommendationsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/api/RecommendationsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Factory"
.end annotation


# virtual methods
.method public abstract createForRelatedMovies(Ljava/lang/String;Ljava/lang/String;II)Lcom/google/android/videos/api/RecommendationsRequest;
.end method

.method public abstract createForRelatedShows(Ljava/lang/String;Ljava/lang/String;II)Lcom/google/android/videos/api/RecommendationsRequest;
.end method

.method public abstract createForShows(Ljava/lang/String;II)Lcom/google/android/videos/api/RecommendationsRequest;
.end method
