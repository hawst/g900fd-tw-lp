.class interface abstract Lcom/google/android/videos/store/WishlistStoreSync$SnapshotTokenQuery;
.super Ljava/lang/Object;
.source "WishlistStoreSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/WishlistStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "SnapshotTokenQuery"
.end annotation


# static fields
.field public static final EQUAL_COLUMNS:[Ljava/lang/String;

.field public static final PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 47
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "wishlist_snapshot_token"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/videos/store/WishlistStoreSync$SnapshotTokenQuery;->PROJECTION:[Ljava/lang/String;

    .line 51
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "user_account"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/videos/store/WishlistStoreSync$SnapshotTokenQuery;->EQUAL_COLUMNS:[Ljava/lang/String;

    return-void
.end method
