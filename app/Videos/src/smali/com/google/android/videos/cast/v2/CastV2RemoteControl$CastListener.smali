.class Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastListener;
.super Lcom/google/android/gms/cast/Cast$Listener;
.source "CastV2RemoteControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/cast/v2/CastV2RemoteControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CastListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)V
    .locals 0

    .prologue
    .line 1027
    iput-object p1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastListener;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    invoke-direct {p0}, Lcom/google/android/gms/cast/Cast$Listener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;
    .param p2, "x1"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;

    .prologue
    .line 1027
    invoke-direct {p0, p1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastListener;-><init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)V

    return-void
.end method


# virtual methods
.method public onApplicationDisconnected(I)V
    .locals 2
    .param p1, "errorCode"    # I

    .prologue
    .line 1030
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onApplicationDisconnected: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 1031
    const/16 v0, 0x7d5

    if-ne p1, v0, :cond_0

    .line 1033
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastListener;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # invokes: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->clearSessionData()V
    invoke-static {v0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$1700(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)V

    .line 1035
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastListener;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->disconnect(Z)V
    invoke-static {v0, v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$1800(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Z)V

    .line 1036
    return-void
.end method

.method public onApplicationStatusChanged()V
    .locals 3

    .prologue
    .line 1040
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastListener;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # getter for: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;
    invoke-static {v0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$700(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1041
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onApplicationStatusChanged "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v0, Lcom/google/android/gms/cast/Cast;->CastApi:Lcom/google/android/gms/cast/Cast$CastApi;

    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastListener;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # getter for: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;
    invoke-static {v2}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$700(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/android/gms/cast/Cast$CastApi;->getApplicationStatus(Lcom/google/android/gms/common/api/GoogleApiClient;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "empty status "

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 1047
    :goto_1
    return-void

    .line 1041
    :cond_0
    sget-object v0, Lcom/google/android/gms/cast/Cast;->CastApi:Lcom/google/android/gms/cast/Cast$CastApi;

    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastListener;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # getter for: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;
    invoke-static {v2}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$700(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/android/gms/cast/Cast$CastApi;->getApplicationStatus(Lcom/google/android/gms/common/api/GoogleApiClient;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1045
    :cond_1
    const-string v0, "onApplicationStatusChanged not connected"

    invoke-static {v0}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    goto :goto_1
.end method
