.class public Lcom/google/android/videos/logging/UiEventLoggingHelper;
.super Ljava/lang/Object;
.source "UiEventLoggingHelper.java"


# instance fields
.field private final eventProtoCache:Lcom/google/android/videos/logging/EventProtoCache;

.field private nextImpressionId:J

.field private final playLogImpressionSettleTimeMs:I

.field private final uiEventLogger:Lcom/google/android/videos/logging/UiEventLogger;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/logging/UiEventLogger;Lcom/google/android/videos/Config;)V
    .locals 1
    .param p1, "uiEventLogger"    # Lcom/google/android/videos/logging/UiEventLogger;
    .param p2, "config"    # Lcom/google/android/videos/Config;

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/logging/UiEventLogger;

    iput-object v0, p0, Lcom/google/android/videos/logging/UiEventLoggingHelper;->uiEventLogger:Lcom/google/android/videos/logging/UiEventLogger;

    .line 36
    invoke-interface {p2}, Lcom/google/android/videos/Config;->getPlayLogImpressionSettleTimeMillis()I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/logging/UiEventLoggingHelper;->playLogImpressionSettleTimeMs:I

    .line 37
    new-instance v0, Lcom/google/android/videos/logging/EventProtoCache;

    invoke-direct {v0}, Lcom/google/android/videos/logging/EventProtoCache;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/logging/UiEventLoggingHelper;->eventProtoCache:Lcom/google/android/videos/logging/EventProtoCache;

    .line 38
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/logging/UiEventLoggingHelper;JLcom/google/android/videos/logging/UiElementNode;ZLjava/lang/String;)V
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/logging/UiEventLoggingHelper;
    .param p1, "x1"    # J
    .param p3, "x2"    # Lcom/google/android/videos/logging/UiElementNode;
    .param p4, "x3"    # Z
    .param p5, "x4"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-direct/range {p0 .. p5}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->sendImpression(JLcom/google/android/videos/logging/UiElementNode;ZLjava/lang/String;)V

    return-void
.end method

.method public static assetInfoFromEpisodeId(Ljava/lang/String;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;
    .locals 2
    .param p0, "episodeId"    # Ljava/lang/String;

    .prologue
    .line 431
    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 432
    new-instance v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    invoke-direct {v0}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;-><init>()V

    .line 433
    .local v0, "assetInfo":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;
    const/4 v1, 0x2

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    .line 434
    iput-object p0, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->assetId:Ljava/lang/String;

    .line 435
    return-object v0
.end method

.method public static assetInfoFromMovieId(Ljava/lang/String;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;
    .locals 2
    .param p0, "movieId"    # Ljava/lang/String;

    .prologue
    .line 423
    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 424
    new-instance v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    invoke-direct {v0}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;-><init>()V

    .line 425
    .local v0, "assetInfo":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;
    const/4 v1, 0x1

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    .line 426
    iput-object p0, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->assetId:Ljava/lang/String;

    .line 427
    return-object v0
.end method

.method public static assetInfoFromShowId(Ljava/lang/String;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;
    .locals 2
    .param p0, "showId"    # Ljava/lang/String;

    .prologue
    .line 439
    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 440
    new-instance v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    invoke-direct {v0}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;-><init>()V

    .line 441
    .local v0, "assetInfo":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;
    const/4 v1, 0x5

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    .line 442
    iput-object p0, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->showId:Ljava/lang/String;

    .line 443
    return-object v0
.end method

.method private static debugLogClick(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;)V
    .locals 0
    .param p0, "clickEvent"    # Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;

    .prologue
    .line 298
    return-void
.end method

.method private static debugLogImpression(JLcom/google/android/videos/logging/UiElementNode;Ljava/lang/String;)V
    .locals 2
    .param p0, "impressionId"    # J
    .param p2, "rootNode"    # Lcom/google/android/videos/logging/UiElementNode;
    .param p3, "action"    # Ljava/lang/String;

    .prologue
    .line 286
    invoke-interface {p2}, Lcom/google/android/videos/logging/UiElementNode;->getUiElementWrapper()Lcom/google/android/videos/logging/UiElementWrapper;

    move-result-object v0

    .line 287
    .local v0, "rootElement":Lcom/google/android/videos/logging/UiElementWrapper;
    invoke-static {p3, v0}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->quickDebugLog(Ljava/lang/String;Lcom/google/android/videos/logging/UiElementWrapper;)V

    .line 288
    const-string v1, ""

    invoke-static {p3, p0, p1, v0, v1}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->dumpTree(Ljava/lang/String;JLcom/google/android/videos/logging/UiElementWrapper;Ljava/lang/String;)V

    .line 289
    return-void
.end method

.method private static debugLogImpression(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;Ljava/lang/String;)V
    .locals 4
    .param p0, "impression"    # Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 292
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->tree:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    invoke-static {p1, v0}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->quickDebugLog(Ljava/lang/String;Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;)V

    .line 293
    iget-wide v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->id:J

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->tree:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    const-string v3, ""

    invoke-static {p1, v0, v1, v2, v3}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->dumpTree(Ljava/lang/String;JLcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;Ljava/lang/String;)V

    .line 294
    return-void
.end method

.method private static dumpTree(Ljava/lang/String;JLcom/google/android/videos/logging/UiElementWrapper;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "id"    # J
    .param p3, "node"    # Lcom/google/android/videos/logging/UiElementWrapper;
    .param p4, "indent"    # Ljava/lang/String;

    .prologue
    .line 358
    return-void
.end method

.method private static dumpTree(Ljava/lang/String;JLcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "id"    # J
    .param p3, "node"    # Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    .param p4, "indent"    # Ljava/lang/String;

    .prologue
    .line 376
    return-void
.end method

.method public static findUiElementNode(Landroid/view/View;)Lcom/google/android/videos/logging/UiElementNode;
    .locals 2
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 199
    instance-of v1, p0, Lcom/google/android/videos/logging/UiElementNode;

    if-eqz v1, :cond_0

    .line 200
    check-cast p0, Lcom/google/android/videos/logging/UiElementNode;

    .line 209
    .end local p0    # "view":Landroid/view/View;
    :goto_0
    return-object p0

    .line 202
    .restart local p0    # "view":Landroid/view/View;
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 203
    .local v0, "parent":Landroid/view/ViewParent;
    :goto_1
    if-eqz v0, :cond_2

    .line 204
    instance-of v1, v0, Lcom/google/android/videos/logging/UiElementNode;

    if-eqz v1, :cond_1

    .line 205
    check-cast v0, Lcom/google/android/videos/logging/UiElementNode;

    .end local v0    # "parent":Landroid/view/ViewParent;
    move-object p0, v0

    goto :goto_0

    .line 207
    .restart local v0    # "parent":Landroid/view/ViewParent;
    :cond_1
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_1

    .line 209
    :cond_2
    const/4 p0, 0x0

    goto :goto_0
.end method

.method private static quickDebugLog(Ljava/lang/String;Lcom/google/android/videos/logging/UiElementWrapper;)V
    .locals 0
    .param p0, "action"    # Ljava/lang/String;
    .param p1, "impressionRootElement"    # Lcom/google/android/videos/logging/UiElementWrapper;

    .prologue
    .line 322
    return-void
.end method

.method private static quickDebugLog(Ljava/lang/String;Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;)V
    .locals 0
    .param p0, "action"    # Ljava/lang/String;
    .param p1, "impressionRootElement"    # Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    .prologue
    .line 310
    return-void
.end method

.method private sendClickEventInternal(Lcom/google/android/videos/logging/UiElementNode;Ljava/util/ArrayList;)V
    .locals 3
    .param p1, "parentNode"    # Lcom/google/android/videos/logging/UiElementNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/logging/UiElementNode;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 267
    .local p2, "path":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;>;"
    :goto_0
    if-eqz p1, :cond_0

    .line 268
    iget-object v2, p0, Lcom/google/android/videos/logging/UiEventLoggingHelper;->eventProtoCache:Lcom/google/android/videos/logging/EventProtoCache;

    invoke-virtual {v2}, Lcom/google/android/videos/logging/EventProtoCache;->obtainUiElement()Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    move-result-object v1

    .line 269
    .local v1, "parentElement":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    invoke-interface {p1}, Lcom/google/android/videos/logging/UiElementNode;->getUiElementWrapper()Lcom/google/android/videos/logging/UiElementWrapper;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/videos/logging/UiElementWrapper;->cloneTo(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;)V

    .line 270
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 271
    invoke-interface {p1}, Lcom/google/android/videos/logging/UiElementNode;->getParentNode()Lcom/google/android/videos/logging/UiElementNode;

    move-result-object p1

    .line 272
    goto :goto_0

    .line 274
    .end local v1    # "parentElement":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/logging/UiEventLoggingHelper;->eventProtoCache:Lcom/google/android/videos/logging/EventProtoCache;

    invoke-virtual {v2}, Lcom/google/android/videos/logging/EventProtoCache;->obtainClickEvent()Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;

    move-result-object v0

    .line 275
    .local v0, "clickEvent":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    iput-object v2, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;->elementPath:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    .line 276
    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    .line 277
    invoke-static {v0}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->debugLogClick(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;)V

    .line 278
    iget-object v2, p0, Lcom/google/android/videos/logging/UiEventLoggingHelper;->uiEventLogger:Lcom/google/android/videos/logging/UiEventLogger;

    invoke-interface {v2, v0}, Lcom/google/android/videos/logging/UiEventLogger;->onClickEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;)V

    .line 279
    iget-object v2, p0, Lcom/google/android/videos/logging/UiEventLoggingHelper;->eventProtoCache:Lcom/google/android/videos/logging/EventProtoCache;

    invoke-virtual {v2, v0}, Lcom/google/android/videos/logging/EventProtoCache;->recycle(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;)V

    .line 280
    return-void
.end method

.method private sendImpression(JLcom/google/android/videos/logging/UiElementNode;ZLjava/lang/String;)V
    .locals 3
    .param p1, "impressionId"    # J
    .param p3, "rootNode"    # Lcom/google/android/videos/logging/UiElementNode;
    .param p4, "dropIfEmpty"    # Z
    .param p5, "debugAction"    # Ljava/lang/String;

    .prologue
    .line 138
    if-eqz p4, :cond_0

    invoke-interface {p3}, Lcom/google/android/videos/logging/UiElementNode;->getUiElementWrapper()Lcom/google/android/videos/logging/UiElementWrapper;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/videos/logging/UiElementWrapper;->children:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 147
    :goto_0
    return-void

    .line 143
    :cond_0
    invoke-interface {p3}, Lcom/google/android/videos/logging/UiElementNode;->getUiElementWrapper()Lcom/google/android/videos/logging/UiElementWrapper;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/logging/UiEventLoggingHelper;->eventProtoCache:Lcom/google/android/videos/logging/EventProtoCache;

    invoke-virtual {v1, v2}, Lcom/google/android/videos/logging/UiElementWrapper;->deepCloneAndWipeChildren(Lcom/google/android/videos/logging/EventProtoCache;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    move-result-object v0

    .line 146
    .local v0, "impressionRootElement":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    invoke-direct {p0, p1, p2, v0, p5}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->sendImpression(JLcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private sendImpression(JLcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;Ljava/lang/String;)V
    .locals 3
    .param p1, "impressionId"    # J
    .param p3, "tree"    # Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    .param p4, "debugAction"    # Ljava/lang/String;

    .prologue
    .line 155
    iget-object v1, p0, Lcom/google/android/videos/logging/UiEventLoggingHelper;->eventProtoCache:Lcom/google/android/videos/logging/EventProtoCache;

    invoke-virtual {v1}, Lcom/google/android/videos/logging/EventProtoCache;->obtainImpressionEvent()Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;

    move-result-object v0

    .line 156
    .local v0, "impression":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;
    iput-object p3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->tree:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    .line 157
    iput-wide p1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->id:J

    .line 158
    invoke-static {v0, p4}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->debugLogImpression(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;Ljava/lang/String;)V

    .line 160
    iget-object v1, p0, Lcom/google/android/videos/logging/UiEventLoggingHelper;->uiEventLogger:Lcom/google/android/videos/logging/UiEventLogger;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/UiEventLogger;->onImpressionEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;)V

    .line 161
    iget-object v1, p0, Lcom/google/android/videos/logging/UiEventLoggingHelper;->eventProtoCache:Lcom/google/android/videos/logging/EventProtoCache;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/logging/EventProtoCache;->recycle(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;)V

    .line 162
    return-void
.end method


# virtual methods
.method public addChild(Lcom/google/android/videos/logging/UiElementNode;Lcom/google/android/videos/logging/UiElementWrapper;)Z
    .locals 2
    .param p1, "receiverNode"    # Lcom/google/android/videos/logging/UiElementNode;
    .param p2, "newChild"    # Lcom/google/android/videos/logging/UiElementWrapper;

    .prologue
    .line 98
    invoke-interface {p1}, Lcom/google/android/videos/logging/UiElementNode;->getUiElementWrapper()Lcom/google/android/videos/logging/UiElementWrapper;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/videos/logging/UiElementWrapper;->addChild(Lcom/google/android/videos/logging/UiElementWrapper;)Z

    move-result v0

    .line 104
    .local v0, "added":Z
    return v0
.end method

.method public flushImpression(Landroid/os/Handler;JLcom/google/android/videos/logging/RootUiElementNode;)V
    .locals 8
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "impressionId"    # J
    .param p4, "rootNode"    # Lcom/google/android/videos/logging/RootUiElementNode;

    .prologue
    .line 170
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 173
    const/4 v5, 0x1

    const-string v6, "Flushing"

    move-object v1, p0

    move-wide v2, p2

    move-object v4, p4

    invoke-direct/range {v1 .. v6}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->sendImpression(JLcom/google/android/videos/logging/UiElementNode;ZLjava/lang/String;)V

    .line 174
    return-void
.end method

.method public getNextImpressionId()J
    .locals 6

    .prologue
    const-wide/16 v4, 0x1

    const-wide/16 v2, 0x0

    .line 181
    iget-wide v0, p0, Lcom/google/android/videos/logging/UiEventLoggingHelper;->nextImpressionId:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 182
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v0}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/videos/logging/UiEventLoggingHelper;->nextImpressionId:J

    .line 185
    :cond_0
    iget-wide v0, p0, Lcom/google/android/videos/logging/UiEventLoggingHelper;->nextImpressionId:J

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/google/android/videos/logging/UiEventLoggingHelper;->nextImpressionId:J

    .line 186
    iget-wide v0, p0, Lcom/google/android/videos/logging/UiEventLoggingHelper;->nextImpressionId:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 187
    iput-wide v4, p0, Lcom/google/android/videos/logging/UiEventLoggingHelper;->nextImpressionId:J

    .line 189
    :cond_1
    iget-wide v0, p0, Lcom/google/android/videos/logging/UiEventLoggingHelper;->nextImpressionId:J

    return-wide v0
.end method

.method public sendClickEvent(ILcom/google/android/videos/logging/UiElementNode;)V
    .locals 3
    .param p1, "leafType"    # I
    .param p2, "parentNode"    # Lcom/google/android/videos/logging/UiElementNode;

    .prologue
    .line 244
    if-nez p2, :cond_1

    .line 262
    :cond_0
    :goto_0
    return-void

    .line 250
    :cond_1
    invoke-interface {p2}, Lcom/google/android/videos/logging/UiElementNode;->getUiElementWrapper()Lcom/google/android/videos/logging/UiElementWrapper;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 257
    iget-object v2, p0, Lcom/google/android/videos/logging/UiEventLoggingHelper;->eventProtoCache:Lcom/google/android/videos/logging/EventProtoCache;

    invoke-virtual {v2}, Lcom/google/android/videos/logging/EventProtoCache;->obtainUiElement()Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    move-result-object v0

    .line 258
    .local v0, "leafElement":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    iput p1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->type:I

    .line 259
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 260
    .local v1, "path":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;>;"
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 261
    invoke-direct {p0, p2, v1}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->sendClickEventInternal(Lcom/google/android/videos/logging/UiElementNode;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public sendClickEvent(Lcom/google/android/videos/logging/UiElementNode;)V
    .locals 2
    .param p1, "node"    # Lcom/google/android/videos/logging/UiElementNode;

    .prologue
    .line 218
    if-nez p1, :cond_1

    .line 232
    :cond_0
    :goto_0
    return-void

    .line 224
    :cond_1
    invoke-interface {p1}, Lcom/google/android/videos/logging/UiElementNode;->getUiElementWrapper()Lcom/google/android/videos/logging/UiElementWrapper;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 230
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 231
    .local v0, "path":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;>;"
    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->sendClickEventInternal(Lcom/google/android/videos/logging/UiElementNode;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public sendImpressionDelayed(Landroid/os/Handler;JLcom/google/android/videos/logging/UiElementNode;)V
    .locals 4
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "impressionId"    # J
    .param p4, "rootNode"    # Lcom/google/android/videos/logging/UiElementNode;

    .prologue
    .line 119
    const-string v0, "Collecting"

    invoke-static {p2, p3, p4, v0}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->debugLogImpression(JLcom/google/android/videos/logging/UiElementNode;Ljava/lang/String;)V

    .line 121
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 122
    new-instance v0, Lcom/google/android/videos/logging/UiEventLoggingHelper$1;

    invoke-direct {v0, p0, p2, p3, p4}, Lcom/google/android/videos/logging/UiEventLoggingHelper$1;-><init>(Lcom/google/android/videos/logging/UiEventLoggingHelper;JLcom/google/android/videos/logging/UiElementNode;)V

    iget v1, p0, Lcom/google/android/videos/logging/UiEventLoggingHelper;->playLogImpressionSettleTimeMs:I

    int-to-long v2, v1

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 128
    return-void
.end method

.method public sendPathImpression(JILcom/google/android/videos/logging/UiElementNode;)V
    .locals 5
    .param p1, "impressionId"    # J
    .param p3, "leafType"    # I
    .param p4, "parentNode"    # Lcom/google/android/videos/logging/UiElementNode;

    .prologue
    .line 64
    if-nez p4, :cond_1

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 70
    :cond_1
    invoke-interface {p4}, Lcom/google/android/videos/logging/UiElementNode;->getUiElementWrapper()Lcom/google/android/videos/logging/UiElementWrapper;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 77
    iget-object v2, p0, Lcom/google/android/videos/logging/UiEventLoggingHelper;->eventProtoCache:Lcom/google/android/videos/logging/EventProtoCache;

    invoke-virtual {v2}, Lcom/google/android/videos/logging/EventProtoCache;->obtainUiElement()Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    move-result-object v0

    .line 78
    .local v0, "currentElement":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    iput p3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->type:I

    .line 80
    :goto_1
    if-eqz p4, :cond_2

    .line 81
    iget-object v2, p0, Lcom/google/android/videos/logging/UiEventLoggingHelper;->eventProtoCache:Lcom/google/android/videos/logging/EventProtoCache;

    invoke-virtual {v2}, Lcom/google/android/videos/logging/EventProtoCache;->obtainUiElement()Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    move-result-object v1

    .line 82
    .local v1, "parentElement":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    invoke-interface {p4}, Lcom/google/android/videos/logging/UiElementNode;->getUiElementWrapper()Lcom/google/android/videos/logging/UiElementWrapper;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/videos/logging/UiElementWrapper;->cloneTo(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;)V

    .line 83
    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    iput-object v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->child:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    .line 84
    move-object v0, v1

    .line 85
    invoke-interface {p4}, Lcom/google/android/videos/logging/UiElementNode;->getParentNode()Lcom/google/android/videos/logging/UiElementNode;

    move-result-object p4

    .line 86
    goto :goto_1

    .line 87
    .end local v1    # "parentElement":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    :cond_2
    const-string v2, "Sending"

    invoke-direct {p0, p1, p2, v0, v2}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->sendImpression(JLcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;Ljava/lang/String;)V

    goto :goto_0
.end method
