.class public Lcom/google/android/videos/ui/TransitionUtil$ExpandTransition;
.super Landroid/transition/Transition;
.source "TransitionUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/TransitionUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ExpandTransition"
.end annotation


# instance fields
.field private final expand:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0
    .param p1, "expand"    # Z

    .prologue
    .line 256
    invoke-direct {p0}, Landroid/transition/Transition;-><init>()V

    .line 257
    iput-boolean p1, p0, Lcom/google/android/videos/ui/TransitionUtil$ExpandTransition;->expand:Z

    .line 258
    return-void
.end method


# virtual methods
.method public captureEndValues(Landroid/transition/TransitionValues;)V
    .locals 0
    .param p1, "values"    # Landroid/transition/TransitionValues;

    .prologue
    .line 267
    return-void
.end method

.method public captureStartValues(Landroid/transition/TransitionValues;)V
    .locals 3
    .param p1, "values"    # Landroid/transition/TransitionValues;

    .prologue
    .line 262
    iget-object v0, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "scale"

    iget-object v2, p1, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getScaleX()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    return-void
.end method

.method public createAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
    .locals 11
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "startValues"    # Landroid/transition/TransitionValues;
    .param p3, "endValues"    # Landroid/transition/TransitionValues;

    .prologue
    const/4 v6, 0x0

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 272
    iget-boolean v5, p0, Lcom/google/android/videos/ui/TransitionUtil$ExpandTransition;->expand:Z

    if-eqz v5, :cond_0

    move v4, v6

    .line 273
    .local v4, "start":F
    :goto_0
    iget-boolean v5, p0, Lcom/google/android/videos/ui/TransitionUtil$ExpandTransition;->expand:Z

    if-eqz v5, :cond_1

    const/high16 v1, 0x3f800000    # 1.0f

    .line 274
    .local v1, "end":F
    :goto_1
    iget-object v5, p3, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    const-string v6, "scaleX"

    new-array v7, v10, [F

    aput v4, v7, v8

    aput v1, v7, v9

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 275
    .local v2, "scaleXAnimator":Landroid/animation/ObjectAnimator;
    iget-object v5, p3, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    const-string v6, "scaleY"

    new-array v7, v10, [F

    aput v4, v7, v8

    aput v1, v7, v9

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 277
    .local v3, "scaleYAnimator":Landroid/animation/ObjectAnimator;
    iget-object v5, p2, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    invoke-virtual {v5, v4}, Landroid/view/View;->setScaleX(F)V

    .line 278
    iget-object v5, p2, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    invoke-virtual {v5, v4}, Landroid/view/View;->setScaleY(F)V

    .line 280
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 281
    .local v0, "animatorSet":Landroid/animation/AnimatorSet;
    new-array v5, v10, [Landroid/animation/Animator;

    aput-object v2, v5, v8

    aput-object v3, v5, v9

    invoke-virtual {v0, v5}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 282
    return-object v0

    .line 272
    .end local v0    # "animatorSet":Landroid/animation/AnimatorSet;
    .end local v1    # "end":F
    .end local v2    # "scaleXAnimator":Landroid/animation/ObjectAnimator;
    .end local v3    # "scaleYAnimator":Landroid/animation/ObjectAnimator;
    .end local v4    # "start":F
    :cond_0
    iget-object v5, p2, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v7, "scale"

    invoke-interface {v5, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v4

    goto :goto_0

    .restart local v4    # "start":F
    :cond_1
    move v1, v6

    .line 273
    goto :goto_1
.end method
