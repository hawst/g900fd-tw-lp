.class final Lcom/google/android/videos/tagging/TaggedKnowledgeEntity$TypeAndHorizontalPositionComparator;
.super Ljava/lang/Object;
.source "TaggedKnowledgeEntity.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "TypeAndHorizontalPositionComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;",
        ">;"
    }
.end annotation


# static fields
.field private static final TYPE_ORDER:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/videos/tagging/KnowledgeEntity;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private final boundingBox:Landroid/graphics/RectF;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 55
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/videos/utils/CollectionUtil;->immutableList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity$TypeAndHorizontalPositionComparator;->TYPE_ORDER:Ljava/util/List;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity$TypeAndHorizontalPositionComparator;->boundingBox:Landroid/graphics/RectF;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/tagging/TaggedKnowledgeEntity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/tagging/TaggedKnowledgeEntity$1;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity$TypeAndHorizontalPositionComparator;-><init>()V

    return-void
.end method

.method private getCenterX(Lcom/google/android/videos/tagging/Tag$TagShape;)F
    .locals 1
    .param p1, "tagShape"    # Lcom/google/android/videos/tagging/Tag$TagShape;

    .prologue
    .line 79
    if-nez p1, :cond_0

    .line 80
    const/4 v0, 0x0

    .line 83
    :goto_0
    return v0

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity$TypeAndHorizontalPositionComparator;->boundingBox:Landroid/graphics/RectF;

    invoke-interface {p1, v0}, Lcom/google/android/videos/tagging/Tag$TagShape;->getBoundingBox(Landroid/graphics/RectF;)V

    .line 83
    iget-object v0, p0, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity$TypeAndHorizontalPositionComparator;->boundingBox:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public compare(Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;)I
    .locals 7
    .param p1, "lhs"    # Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;
    .param p2, "rhs"    # Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;

    .prologue
    .line 62
    if-nez p1, :cond_1

    .line 63
    if-nez p2, :cond_0

    const/4 v5, 0x0

    .line 75
    :goto_0
    return v5

    .line 63
    :cond_0
    const/4 v5, -0x1

    goto :goto_0

    .line 64
    :cond_1
    if-nez p2, :cond_2

    .line 65
    const/4 v5, 0x1

    goto :goto_0

    .line 67
    :cond_2
    sget-object v5, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity$TypeAndHorizontalPositionComparator;->TYPE_ORDER:Ljava/util/List;

    iget-object v6, p1, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;->knowledgeEntity:Lcom/google/android/videos/tagging/KnowledgeEntity;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 68
    .local v0, "lhsTypeIndex":I
    sget-object v5, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity$TypeAndHorizontalPositionComparator;->TYPE_ORDER:Ljava/util/List;

    iget-object v6, p2, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;->knowledgeEntity:Lcom/google/android/videos/tagging/KnowledgeEntity;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v3

    .line 69
    .local v3, "rhsTypeIndex":I
    sub-int v2, v0, v3

    .line 70
    .local v2, "result":I
    if-eqz v2, :cond_3

    move v5, v2

    .line 71
    goto :goto_0

    .line 73
    :cond_3
    iget-object v5, p1, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;->tagShape:Lcom/google/android/videos/tagging/Tag$TagShape;

    invoke-direct {p0, v5}, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity$TypeAndHorizontalPositionComparator;->getCenterX(Lcom/google/android/videos/tagging/Tag$TagShape;)F

    move-result v1

    .line 74
    .local v1, "lhsX":F
    iget-object v5, p2, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;->tagShape:Lcom/google/android/videos/tagging/Tag$TagShape;

    invoke-direct {p0, v5}, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity$TypeAndHorizontalPositionComparator;->getCenterX(Lcom/google/android/videos/tagging/Tag$TagShape;)F

    move-result v4

    .line 75
    .local v4, "rhsX":F
    sub-float v5, v1, v4

    invoke-static {v5}, Ljava/lang/Math;->signum(F)F

    move-result v5

    float-to-int v5, v5

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 51
    check-cast p1, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity$TypeAndHorizontalPositionComparator;->compare(Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;)I

    move-result v0

    return v0
.end method
