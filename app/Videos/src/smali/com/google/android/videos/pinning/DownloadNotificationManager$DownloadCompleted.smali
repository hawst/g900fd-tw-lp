.class public Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;
.super Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadSingle;
.source "DownloadNotificationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pinning/DownloadNotificationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DownloadCompleted"
.end annotation


# instance fields
.field public final videoIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)V
    .locals 2
    .param p1, "videoData"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;

    .prologue
    .line 95
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadSingle;-><init>(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;Lcom/google/android/videos/pinning/DownloadNotificationManager$1;)V

    .line 96
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->videoId:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$200(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/CollectionUtil;->newArrayList(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;->videoIds:Ljava/util/List;

    .line 97
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;Lcom/google/android/videos/pinning/DownloadNotificationManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
    .param p2, "x1"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$1;

    .prologue
    .line 91
    invoke-direct {p0, p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;-><init>(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)V

    return-void
.end method
