.class public Lcom/google/android/videos/ui/BitmapLoader;
.super Ljava/lang/Object;
.source "BitmapLoader.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;,
        Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;,
        Lcom/google/android/videos/ui/BitmapLoader$BitmapPaletteView;,
        Lcom/google/android/videos/ui/BitmapLoader$BitmapView;,
        Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        "B:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<TR;TB;>;"
    }
.end annotation


# static fields
.field private static final PALETTE_GENERATOR:Lcom/google/android/videos/converter/ResponseConverter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/converter/ResponseConverter",
            "<",
            "Landroid/graphics/Bitmap;",
            "Landroid/util/Pair",
            "<",
            "Landroid/graphics/Bitmap;",
            "Landroid/support/v7/graphics/Palette;",
            ">;>;"
        }
    .end annotation
.end field

.field private static handler:Landroid/os/Handler;


# instance fields
.field private final completionCallback:Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;

.field private final view:Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView",
            "<TB;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 125
    new-instance v0, Lcom/google/android/videos/ui/BitmapLoader$1;

    invoke-direct {v0}, Lcom/google/android/videos/ui/BitmapLoader$1;-><init>()V

    sput-object v0, Lcom/google/android/videos/ui/BitmapLoader;->PALETTE_GENERATOR:Lcom/google/android/videos/converter/ResponseConverter;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;)V
    .locals 0
    .param p2, "completionCallback"    # Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView",
            "<TB;>;",
            "Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 151
    .local p0, "this":Lcom/google/android/videos/ui/BitmapLoader;, "Lcom/google/android/videos/ui/BitmapLoader<TR;TB;>;"
    .local p1, "view":Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;, "Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView<TB;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    iput-object p1, p0, Lcom/google/android/videos/ui/BitmapLoader;->view:Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;

    .line 153
    iput-object p2, p0, Lcom/google/android/videos/ui/BitmapLoader;->completionCallback:Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;

    .line 154
    return-void
.end method

.method public static cancel(Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 122
    .local p0, "view":Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;, "Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView<*>;"
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;->setThumbnailTag(Ljava/lang/Object;)V

    .line 123
    return-void
.end method

.method public static createDefaultBitmapView(Landroid/widget/ImageView;I)Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;
    .locals 1
    .param p0, "imageView"    # Landroid/widget/ImageView;
    .param p1, "noThumbnailBackgroundResource"    # I

    .prologue
    .line 206
    const/4 v0, 0x1

    invoke-static {p0, v0, p1}, Lcom/google/android/videos/ui/BitmapLoader;->createDefaultBitmapView(Landroid/widget/ImageView;ZI)Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;

    move-result-object v0

    return-object v0
.end method

.method public static createDefaultBitmapView(Landroid/widget/ImageView;ZI)Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;
    .locals 6
    .param p0, "imageView"    # Landroid/widget/ImageView;
    .param p1, "shouldSetScaleType"    # Z
    .param p2, "noThumbnailBackgroundResource"    # I

    .prologue
    const/4 v5, 0x0

    .line 196
    new-instance v0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;

    if-nez p2, :cond_0

    move-object v3, v5

    :goto_0
    const/4 v4, 0x0

    move-object v1, p0

    move v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;-><init>(Landroid/widget/ImageView;ZLandroid/graphics/drawable/Drawable;ZLcom/google/android/videos/ui/BitmapLoader$1;)V

    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    goto :goto_0
.end method

.method public static createSelectableDefaultBitmapView(Landroid/widget/ImageView;I)Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;
    .locals 6
    .param p0, "imageView"    # Landroid/widget/ImageView;
    .param p1, "noThumbnailBackgroundResource"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    .line 182
    new-instance v0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;

    if-nez p1, :cond_0

    move-object v3, v5

    :goto_0
    move-object v1, p0

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;-><init>(Landroid/widget/ImageView;ZLandroid/graphics/drawable/Drawable;ZLcom/google/android/videos/ui/BitmapLoader$1;)V

    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    goto :goto_0
.end method

.method private static getHandler()Landroid/os/Handler;
    .locals 2

    .prologue
    .line 142
    sget-object v0, Lcom/google/android/videos/ui/BitmapLoader;->handler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 143
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/google/android/videos/ui/BitmapLoader;->handler:Landroid/os/Handler;

    .line 145
    :cond_0
    sget-object v0, Lcom/google/android/videos/ui/BitmapLoader;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method public static setBitmapAsync(Lcom/google/android/videos/ui/BitmapLoader$BitmapPaletteView;Lcom/google/android/videos/async/Requester;Ljava/util/concurrent/Executor;Ljava/lang/Object;Landroid/graphics/Bitmap;Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;)V
    .locals 2
    .param p0, "view"    # Lcom/google/android/videos/ui/BitmapLoader$BitmapPaletteView;
    .param p2, "cpuExecutor"    # Ljava/util/concurrent/Executor;
    .param p4, "missingBitmap"    # Landroid/graphics/Bitmap;
    .param p5, "completionCallback"    # Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/videos/ui/BitmapLoader$BitmapPaletteView;",
            "Lcom/google/android/videos/async/Requester",
            "<TR;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Ljava/util/concurrent/Executor;",
            "TR;",
            "Landroid/graphics/Bitmap;",
            "Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 82
    .local p1, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TR;Landroid/graphics/Bitmap;>;"
    .local p3, "request":Ljava/lang/Object;, "TR;"
    sget-object v0, Lcom/google/android/videos/ui/BitmapLoader;->PALETTE_GENERATOR:Lcom/google/android/videos/converter/ResponseConverter;

    invoke-static {p1, v0, p2}, Lcom/google/android/videos/async/ConvertingRequester;->create(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/converter/ResponseConverter;Ljava/util/concurrent/Executor;)Lcom/google/android/videos/async/Requester;

    move-result-object v1

    const/4 v0, 0x0

    check-cast v0, Landroid/support/v7/graphics/Palette;

    invoke-static {p4, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-static {p0, v1, p3, v0, p5}, Lcom/google/android/videos/ui/BitmapLoader;->setBitmapAsync(Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;Lcom/google/android/videos/async/Requester;Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;)V

    .line 85
    return-void
.end method

.method public static setBitmapAsync(Lcom/google/android/videos/ui/BitmapLoader$BitmapView;Lcom/google/android/videos/async/Requester;Ljava/lang/Object;)V
    .locals 1
    .param p0, "view"    # Lcom/google/android/videos/ui/BitmapLoader$BitmapView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/videos/ui/BitmapLoader$BitmapView;",
            "Lcom/google/android/videos/async/Requester",
            "<TR;",
            "Landroid/graphics/Bitmap;",
            ">;TR;)V"
        }
    .end annotation

    .prologue
    .local p1, "requester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TR;Landroid/graphics/Bitmap;>;"
    .local p2, "request":Ljava/lang/Object;, "TR;"
    const/4 v0, 0x0

    .line 60
    invoke-static {p0, p1, p2, v0, v0}, Lcom/google/android/videos/ui/BitmapLoader;->setBitmapAsync(Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;Lcom/google/android/videos/async/Requester;Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;)V

    .line 61
    return-void
.end method

.method public static setBitmapAsync(Lcom/google/android/videos/ui/BitmapLoader$BitmapView;Lcom/google/android/videos/async/Requester;Ljava/lang/Object;Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;)V
    .locals 1
    .param p0, "view"    # Lcom/google/android/videos/ui/BitmapLoader$BitmapView;
    .param p3, "completionCallback"    # Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/videos/ui/BitmapLoader$BitmapView;",
            "Lcom/google/android/videos/async/Requester",
            "<TR;",
            "Landroid/graphics/Bitmap;",
            ">;TR;",
            "Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 68
    .local p1, "requester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TR;Landroid/graphics/Bitmap;>;"
    .local p2, "request":Ljava/lang/Object;, "TR;"
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0, p3}, Lcom/google/android/videos/ui/BitmapLoader;->setBitmapAsync(Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;Lcom/google/android/videos/async/Requester;Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;)V

    .line 69
    return-void
.end method

.method public static setBitmapAsync(Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;Lcom/google/android/videos/async/Requester;Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;)V
    .locals 2
    .param p4, "completionCallback"    # Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView",
            "<TB;>;",
            "Lcom/google/android/videos/async/Requester",
            "<TR;TB;>;TR;TB;",
            "Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "view":Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;, "Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView<TB;>;"
    .local p1, "requester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TR;TB;>;"
    .local p2, "request":Ljava/lang/Object;, "TR;"
    .local p3, "missingBitmap":Ljava/lang/Object;, "TB;"
    const/4 v1, 0x1

    .line 104
    if-eqz p2, :cond_1

    .line 105
    invoke-interface {p0}, Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;->getThumbnailTag()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    invoke-interface {p0, p2}, Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;->setThumbnailTag(Ljava/lang/Object;)V

    .line 107
    invoke-interface {p0, p3, v1}, Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;->setThumbnail(Ljava/lang/Object;Z)V

    .line 108
    invoke-static {}, Lcom/google/android/videos/ui/BitmapLoader;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/videos/ui/BitmapLoader;

    invoke-direct {v1, p0, p4}, Lcom/google/android/videos/ui/BitmapLoader;-><init>(Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;)V

    invoke-static {v0, v1}, Lcom/google/android/videos/async/HandlerCallback;->create(Landroid/os/Handler;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/HandlerCallback;

    move-result-object v0

    invoke-interface {p1, p2, v0}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 115
    :cond_0
    :goto_0
    return-void

    .line 112
    :cond_1
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;->setThumbnailTag(Ljava/lang/Object;)V

    .line 113
    invoke-interface {p0, p3, v1}, Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;->setThumbnail(Ljava/lang/Object;Z)V

    goto :goto_0
.end method


# virtual methods
.method public onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2
    .param p2, "exception"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    .prologue
    .line 168
    .local p0, "this":Lcom/google/android/videos/ui/BitmapLoader;, "Lcom/google/android/videos/ui/BitmapLoader<TR;TB;>;"
    .local p1, "request":Ljava/lang/Object;, "TR;"
    iget-object v0, p0, Lcom/google/android/videos/ui/BitmapLoader;->view:Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;

    invoke-interface {v0}, Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;->getThumbnailTag()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/google/android/videos/ui/BitmapLoader;->view:Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;->setThumbnailTag(Ljava/lang/Object;)V

    .line 170
    iget-object v0, p0, Lcom/google/android/videos/ui/BitmapLoader;->completionCallback:Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/google/android/videos/ui/BitmapLoader;->completionCallback:Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;

    iget-object v1, p0, Lcom/google/android/videos/ui/BitmapLoader;->view:Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;

    invoke-interface {v0, v1}, Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;->onBitmapRequestCompleted(Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;)V

    .line 174
    :cond_0
    return-void
.end method

.method public onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;TB;)V"
        }
    .end annotation

    .prologue
    .line 158
    .local p0, "this":Lcom/google/android/videos/ui/BitmapLoader;, "Lcom/google/android/videos/ui/BitmapLoader<TR;TB;>;"
    .local p1, "request":Ljava/lang/Object;, "TR;"
    .local p2, "response":Ljava/lang/Object;, "TB;"
    iget-object v0, p0, Lcom/google/android/videos/ui/BitmapLoader;->view:Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;

    invoke-interface {v0}, Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;->getThumbnailTag()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/google/android/videos/ui/BitmapLoader;->view:Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;

    const/4 v1, 0x0

    invoke-interface {v0, p2, v1}, Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;->setThumbnail(Ljava/lang/Object;Z)V

    .line 160
    iget-object v0, p0, Lcom/google/android/videos/ui/BitmapLoader;->completionCallback:Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/google/android/videos/ui/BitmapLoader;->completionCallback:Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;

    iget-object v1, p0, Lcom/google/android/videos/ui/BitmapLoader;->view:Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;

    invoke-interface {v0, v1}, Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;->onBitmapRequestCompleted(Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;)V

    .line 164
    :cond_0
    return-void
.end method
