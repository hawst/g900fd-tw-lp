.class public final Lcom/google/android/videos/pinning/ExoCacheProvider;
.super Ljava/lang/Object;
.source "ExoCacheProvider.java"


# instance fields
.field private final downloadCacheMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/io/File;",
            "Lcom/google/android/exoplayer/upstream/cache/Cache;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/pinning/ExoCacheProvider;->downloadCacheMap:Ljava/util/Map;

    .line 22
    return-void
.end method


# virtual methods
.method public declared-synchronized acquireDownloadCache(Ljava/io/File;)Lcom/google/android/exoplayer/upstream/cache/Cache;
    .locals 2
    .param p1, "cachePath"    # Ljava/io/File;

    .prologue
    .line 29
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/videos/pinning/ExoCacheProvider;->downloadCacheMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/upstream/cache/Cache;

    .line 30
    .local v0, "cache":Lcom/google/android/exoplayer/upstream/cache/Cache;
    if-nez v0, :cond_0

    .line 31
    new-instance v0, Lcom/google/android/exoplayer/upstream/cache/SimpleCache;

    .end local v0    # "cache":Lcom/google/android/exoplayer/upstream/cache/Cache;
    new-instance v1, Lcom/google/android/exoplayer/upstream/cache/NoOpCacheEvictor;

    invoke-direct {v1}, Lcom/google/android/exoplayer/upstream/cache/NoOpCacheEvictor;-><init>()V

    invoke-direct {v0, p1, v1}, Lcom/google/android/exoplayer/upstream/cache/SimpleCache;-><init>(Ljava/io/File;Lcom/google/android/exoplayer/upstream/cache/CacheEvictor;)V

    .line 32
    .restart local v0    # "cache":Lcom/google/android/exoplayer/upstream/cache/Cache;
    iget-object v1, p0, Lcom/google/android/videos/pinning/ExoCacheProvider;->downloadCacheMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34
    :cond_0
    monitor-exit p0

    return-object v0

    .line 29
    .end local v0    # "cache":Lcom/google/android/exoplayer/upstream/cache/Cache;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
