.class public final Lcom/google/android/videos/cache/PersistentCache;
.super Ljava/lang/Object;
.source "PersistentCache.java"

# interfaces
.implements Lcom/google/android/videos/cache/Cache;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/cache/Cache",
        "<TK;TE;>;"
    }
.end annotation


# instance fields
.field private final cachePath:Ljava/lang/String;

.field private final converter:Lcom/google/android/videos/cache/Converter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/cache/Converter",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final filenames:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final filter:Ljava/io/FilenameFilter;

.field private volatile initCalled:Z

.field private final initialized:Landroid/os/ConditionVariable;

.field private final suffix:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/cache/Converter;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p2, "cachePath"    # Ljava/lang/String;
    .param p3, "suffix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/cache/Converter",
            "<TE;>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 173
    .local p0, "this":Lcom/google/android/videos/cache/PersistentCache;, "Lcom/google/android/videos/cache/PersistentCache<TK;TE;>;"
    .local p1, "converter":Lcom/google/android/videos/cache/Converter;, "Lcom/google/android/videos/cache/Converter<TE;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 174
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    .line 176
    iput-object p2, p0, Lcom/google/android/videos/cache/PersistentCache;->cachePath:Ljava/lang/String;

    .line 177
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/videos/cache/PersistentCache;->suffix:Ljava/lang/String;

    .line 178
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/cache/Converter;

    iput-object v0, p0, Lcom/google/android/videos/cache/PersistentCache;->converter:Lcom/google/android/videos/cache/Converter;

    .line 179
    new-instance v0, Landroid/os/ConditionVariable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/videos/cache/PersistentCache;->initialized:Landroid/os/ConditionVariable;

    .line 180
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/cache/PersistentCache;->filenames:Ljava/util/concurrent/ConcurrentHashMap;

    .line 181
    new-instance v0, Lcom/google/android/videos/cache/PersistentCache$4;

    invoke-direct {v0, p0, p3}, Lcom/google/android/videos/cache/PersistentCache$4;-><init>(Lcom/google/android/videos/cache/PersistentCache;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/videos/cache/PersistentCache;->filter:Ljava/io/FilenameFilter;

    .line 187
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/cache/PersistentCache;)Ljava/io/FilenameFilter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/cache/PersistentCache;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/videos/cache/PersistentCache;->filter:Ljava/io/FilenameFilter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/cache/PersistentCache;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/cache/PersistentCache;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/videos/cache/PersistentCache;->cachePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/cache/PersistentCache;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/cache/PersistentCache;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/videos/cache/PersistentCache;->filenames:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/cache/PersistentCache;)Landroid/os/ConditionVariable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/cache/PersistentCache;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/videos/cache/PersistentCache;->initialized:Landroid/os/ConditionVariable;

    return-object v0
.end method

.method private assertInitWasStartedAndWaitForIt()V
    .locals 2

    .prologue
    .line 338
    .local p0, "this":Lcom/google/android/videos/cache/PersistentCache;, "Lcom/google/android/videos/cache/PersistentCache<TK;TE;>;"
    iget-boolean v0, p0, Lcom/google/android/videos/cache/PersistentCache;->initCalled:Z

    const-string v1, "init() must be called before calling to this method"

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 340
    iget-object v0, p0, Lcom/google/android/videos/cache/PersistentCache;->initialized:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    .line 341
    return-void
.end method

.method public static cleanup(Ljava/lang/String;J)V
    .locals 21
    .param p0, "cachePath"    # Ljava/lang/String;
    .param p1, "limit"    # J

    .prologue
    .line 82
    const-wide/16 v16, 0x0

    cmp-long v15, p1, v16

    if-lez v15, :cond_1

    const/4 v15, 0x1

    :goto_0
    const-string v16, "limit may not be <= 0"

    invoke-static/range {v15 .. v16}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;)V

    .line 83
    invoke-static/range {p0 .. p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 84
    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 85
    .local v4, "dir":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v15

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " is not a directory"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;)V

    .line 86
    new-instance v10, Lcom/google/android/videos/cache/PersistentCache$1;

    invoke-direct {v10}, Lcom/google/android/videos/cache/PersistentCache$1;-><init>()V

    .line 93
    .local v10, "filter":Ljava/io/FileFilter;
    invoke-virtual {v4, v10}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v9

    .line 94
    .local v9, "files":[Ljava/io/File;
    if-nez v9, :cond_2

    .line 142
    :cond_0
    :goto_1
    return-void

    .line 82
    .end local v4    # "dir":Ljava/io/File;
    .end local v9    # "files":[Ljava/io/File;
    .end local v10    # "filter":Ljava/io/FileFilter;
    :cond_1
    const/4 v15, 0x0

    goto :goto_0

    .line 98
    .restart local v4    # "dir":Ljava/io/File;
    .restart local v9    # "files":[Ljava/io/File;
    .restart local v10    # "filter":Ljava/io/FileFilter;
    :cond_2
    const/4 v14, 0x0

    .line 99
    .local v14, "size":I
    move-object v2, v9

    .local v2, "arr$":[Ljava/io/File;
    array-length v12, v2

    .local v12, "len$":I
    const/4 v11, 0x0

    .local v11, "i$":I
    :goto_2
    if-ge v11, v12, :cond_3

    aget-object v5, v2, v11

    .line 100
    .local v5, "file":Ljava/io/File;
    int-to-long v0, v14

    move-wide/from16 v16, v0

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v18

    add-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v14, v0

    .line 99
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 102
    .end local v5    # "file":Ljava/io/File;
    :cond_3
    int-to-long v0, v14

    move-wide/from16 v16, v0

    cmp-long v15, v16, p1

    if-gez v15, :cond_4

    .line 103
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Cache is below limit, no need to shrink: [size="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", limit="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-wide/from16 v0, p1

    invoke-virtual {v15, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "]"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    goto :goto_1

    .line 107
    :cond_4
    new-instance v8, Ljava/util/HashMap;

    array-length v15, v9

    invoke-direct {v8, v15}, Ljava/util/HashMap;-><init>(I)V

    .line 108
    .local v8, "fileTimestamps":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/io/File;Ljava/lang/Long;>;"
    move-object v2, v9

    array-length v12, v2

    const/4 v11, 0x0

    :goto_3
    if-ge v11, v12, :cond_5

    aget-object v5, v2, v11

    .line 111
    .restart local v5    # "file":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->lastModified()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v8, v5, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 113
    .end local v5    # "file":Ljava/io/File;
    :cond_5
    new-instance v15, Lcom/google/android/videos/cache/PersistentCache$2;

    invoke-direct {v15, v8}, Lcom/google/android/videos/cache/PersistentCache$2;-><init>(Ljava/util/HashMap;)V

    invoke-static {v9, v15}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 127
    move v13, v14

    .line 128
    .local v13, "newSize":I
    const/4 v3, 0x0

    .line 129
    .local v3, "deletedFilesCount":I
    move-object v2, v9

    array-length v12, v2

    const/4 v11, 0x0

    :goto_4
    if-ge v11, v12, :cond_0

    aget-object v5, v2, v11

    .line 130
    .restart local v5    # "file":Ljava/io/File;
    int-to-long v0, v13

    move-wide/from16 v16, v0

    cmp-long v15, v16, p1

    if-ltz v15, :cond_7

    .line 131
    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v6

    .line 132
    .local v6, "fileSize":J
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    move-result v15

    if-eqz v15, :cond_6

    .line 133
    int-to-long v0, v13

    move-wide/from16 v16, v0

    sub-long v16, v16, v6

    move-wide/from16 v0, v16

    long-to-int v13, v0

    .line 134
    add-int/lit8 v3, v3, 0x1

    .line 129
    :cond_6
    add-int/lit8 v11, v11, 0x1

    goto :goto_4

    .line 137
    .end local v6    # "fileSize":J
    :cond_7
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Cache shrunk: [deleted="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", newSize="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", previousSize="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", limit="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-wide/from16 v0, p1

    invoke-virtual {v15, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "]"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private closeIfOpen(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "s"    # Ljava/io/InputStream;

    .prologue
    .line 326
    .local p0, "this":Lcom/google/android/videos/cache/PersistentCache;, "Lcom/google/android/videos/cache/PersistentCache<TK;TE;>;"
    if-nez p1, :cond_0

    .line 334
    :goto_0
    return-void

    .line 330
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 331
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private closeIfOpen(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "s"    # Ljava/io/OutputStream;

    .prologue
    .line 315
    .local p0, "this":Lcom/google/android/videos/cache/PersistentCache;, "Lcom/google/android/videos/cache/PersistentCache<TK;TE;>;"
    if-nez p1, :cond_0

    .line 323
    :goto_0
    return-void

    .line 319
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 320
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private generateFilename(Ljava/lang/Object;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 311
    .local p0, "this":Lcom/google/android/videos/cache/PersistentCache;, "Lcom/google/android/videos/cache/PersistentCache<TK;TE;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/cache/PersistentCache;->suffix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".cache"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static triggerCleanup(Ljava/util/concurrent/Executor;Ljava/lang/String;J)V
    .locals 2
    .param p0, "executor"    # Ljava/util/concurrent/Executor;
    .param p1, "cachePath"    # Ljava/lang/String;
    .param p2, "limit"    # J

    .prologue
    .line 159
    new-instance v0, Lcom/google/android/videos/cache/PersistentCache$3;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/videos/cache/PersistentCache$3;-><init>(Ljava/lang/String;J)V

    invoke-interface {p0, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 165
    return-void
.end method


# virtual methods
.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TE;"
        }
    .end annotation

    .prologue
    .line 217
    .local p0, "this":Lcom/google/android/videos/cache/PersistentCache;, "Lcom/google/android/videos/cache/PersistentCache<TK;TE;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    invoke-direct {p0}, Lcom/google/android/videos/cache/PersistentCache;->assertInitWasStartedAndWaitForIt()V

    .line 219
    invoke-direct {p0, p1}, Lcom/google/android/videos/cache/PersistentCache;->generateFilename(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 220
    .local v3, "filename":Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/videos/cache/PersistentCache;->filenames:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v3}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 221
    const/4 v1, 0x0

    .line 238
    :goto_0
    return-object v1

    .line 223
    :cond_0
    new-instance v2, Ljava/io/File;

    iget-object v6, p0, Lcom/google/android/videos/cache/PersistentCache;->cachePath:Ljava/lang/String;

    invoke-direct {v2, v6, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    .local v2, "file":Ljava/io/File;
    const/4 v4, 0x0

    .line 225
    .local v4, "fis":Ljava/io/FileInputStream;
    const/4 v1, 0x0

    .line 227
    .local v1, "element":Ljava/lang/Object;, "TE;"
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 228
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .local v5, "fis":Ljava/io/FileInputStream;
    :try_start_1
    iget-object v6, p0, Lcom/google/android/videos/cache/PersistentCache;->converter:Lcom/google/android/videos/cache/Converter;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v8

    invoke-interface {v6, v5, v8, v9}, Lcom/google/android/videos/cache/Converter;->readElement(Ljava/io/InputStream;J)Ljava/lang/Object;

    move-result-object v1

    .line 230
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Ljava/io/File;->setLastModified(J)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 236
    invoke-direct {p0, v5}, Lcom/google/android/videos/cache/PersistentCache;->closeIfOpen(Ljava/io/InputStream;)V

    move-object v4, v5

    .line 237
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto :goto_0

    .line 231
    :catch_0
    move-exception v0

    .line 233
    .end local v1    # "element":Ljava/lang/Object;, "TE;"
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_2
    iget-object v6, p0, Lcom/google/android/videos/cache/PersistentCache;->filenames:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v3}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error opening cache file (maybe removed). [filename="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 236
    invoke-direct {p0, v4}, Lcom/google/android/videos/cache/PersistentCache;->closeIfOpen(Ljava/io/InputStream;)V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    :goto_2
    invoke-direct {p0, v4}, Lcom/google/android/videos/cache/PersistentCache;->closeIfOpen(Ljava/io/InputStream;)V

    throw v6

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v6

    move-object v4, v5

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto :goto_2

    .line 231
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :catch_1
    move-exception v0

    move-object v4, v5

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto :goto_1
.end method

.method public init(Ljava/util/concurrent/Executor;)Lcom/google/android/videos/cache/PersistentCache;
    .locals 1
    .param p1, "executor"    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            ")",
            "Lcom/google/android/videos/cache/PersistentCache",
            "<TK;TE;>;"
        }
    .end annotation

    .prologue
    .line 198
    .local p0, "this":Lcom/google/android/videos/cache/PersistentCache;, "Lcom/google/android/videos/cache/PersistentCache<TK;TE;>;"
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/cache/PersistentCache;->initCalled:Z

    .line 200
    new-instance v0, Lcom/google/android/videos/cache/PersistentCache$5;

    invoke-direct {v0, p0}, Lcom/google/android/videos/cache/PersistentCache$5;-><init>(Lcom/google/android/videos/cache/PersistentCache;)V

    invoke-interface {p1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 212
    return-object p0
.end method

.method public final put(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TE;)V"
        }
    .end annotation

    .prologue
    .line 243
    .local p0, "this":Lcom/google/android/videos/cache/PersistentCache;, "Lcom/google/android/videos/cache/PersistentCache<TK;TE;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    .local p2, "element":Ljava/lang/Object;, "TE;"
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    invoke-direct {p0}, Lcom/google/android/videos/cache/PersistentCache;->assertInitWasStartedAndWaitForIt()V

    .line 246
    invoke-direct {p0, p1}, Lcom/google/android/videos/cache/PersistentCache;->generateFilename(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 247
    .local v2, "filename":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    iget-object v5, p0, Lcom/google/android/videos/cache/PersistentCache;->cachePath:Ljava/lang/String;

    invoke-direct {v1, v5, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    .local v1, "file":Ljava/io/File;
    const/4 v3, 0x0

    .line 250
    .local v3, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 251
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .local v4, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    iget-object v5, p0, Lcom/google/android/videos/cache/PersistentCache;->converter:Lcom/google/android/videos/cache/Converter;

    invoke-interface {v5, p2, v4}, Lcom/google/android/videos/cache/Converter;->writeElement(Ljava/lang/Object;Ljava/io/OutputStream;)V

    .line 252
    iget-object v5, p0, Lcom/google/android/videos/cache/PersistentCache;->filenames:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v6, ""

    invoke-virtual {v5, v2, v6}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 260
    invoke-direct {p0, v4}, Lcom/google/android/videos/cache/PersistentCache;->closeIfOpen(Ljava/io/OutputStream;)V

    move-object v3, v4

    .line 262
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :goto_0
    return-void

    .line 253
    :catch_0
    move-exception v0

    .line 255
    .local v0, "e":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_2
    const-string v5, "Error creating cache file."

    invoke-static {v5, v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 260
    invoke-direct {p0, v3}, Lcom/google/android/videos/cache/PersistentCache;->closeIfOpen(Ljava/io/OutputStream;)V

    goto :goto_0

    .line 256
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 258
    .local v0, "e":Ljava/io/IOException;
    :goto_2
    :try_start_3
    const-string v5, "Error creating cache file."

    invoke-static {v5, v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 260
    invoke-direct {p0, v3}, Lcom/google/android/videos/cache/PersistentCache;->closeIfOpen(Ljava/io/OutputStream;)V

    goto :goto_0

    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    :goto_3
    invoke-direct {p0, v3}, Lcom/google/android/videos/cache/PersistentCache;->closeIfOpen(Ljava/io/OutputStream;)V

    throw v5

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v5

    move-object v3, v4

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 256
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v0

    move-object v3, v4

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 253
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v0

    move-object v3, v4

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    goto :goto_1
.end method
