.class public Lcom/google/android/videos/utils/OfflineUtil;
.super Ljava/lang/Object;
.source "OfflineUtil.java"


# direct methods
.method public static getDirPathForDashDownload(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "username"    # Ljava/lang/String;
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 135
    :try_start_0
    const-string v1, "utf-8"

    invoke-virtual {p0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    const/16 v2, 0xb

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    .line 140
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 137
    :catch_0
    move-exception v0

    .line 138
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static getFilePathForLegacyDownload(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 4
    .param p0, "username"    # Ljava/lang/String;
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "appLevelDrm"    # Z

    .prologue
    .line 121
    :try_start_0
    const-string v2, "utf-8"

    invoke-virtual {p0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    const/16 v3, 0xb

    invoke-static {v2, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    .line 126
    if-eqz p2, :cond_0

    const-string v1, "application_"

    .line 127
    .local v1, "prefix":Ljava/lang/String;
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".wvm"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 123
    .end local v1    # "prefix":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 124
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 126
    .end local v0    # "e":Ljava/io/UnsupportedEncodingException;
    :cond_0
    const-string v1, "framework_"

    goto :goto_0
.end method

.method private static getMediaNotMountedException()Lcom/google/android/videos/utils/MediaNotMountedException;
    .locals 4

    .prologue
    .line 252
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v2

    .line 255
    .local v2, "state":Ljava/lang/String;
    const-string v3, "removed"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Landroid/os/Environment;->isExternalStorageRemovable()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 256
    const/16 v0, 0xf

    .line 257
    .local v0, "errorCode":I
    const v1, 0x7f0b0165

    .line 274
    .local v1, "errorStringResourceId":I
    :goto_0
    new-instance v3, Lcom/google/android/videos/utils/MediaNotMountedException;

    invoke-direct {v3, v0, v1}, Lcom/google/android/videos/utils/MediaNotMountedException;-><init>(II)V

    return-object v3

    .line 258
    .end local v0    # "errorCode":I
    .end local v1    # "errorStringResourceId":I
    :cond_0
    const-string v3, "unmountable"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 259
    const/16 v0, 0x10

    .line 260
    .restart local v0    # "errorCode":I
    const v1, 0x7f0b0167

    .restart local v1    # "errorStringResourceId":I
    goto :goto_0

    .line 261
    .end local v0    # "errorCode":I
    .end local v1    # "errorStringResourceId":I
    :cond_1
    const-string v3, "nofs"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 262
    const/16 v0, 0x11

    .line 263
    .restart local v0    # "errorCode":I
    const v1, 0x7f0b0167

    .restart local v1    # "errorStringResourceId":I
    goto :goto_0

    .line 264
    .end local v0    # "errorCode":I
    .end local v1    # "errorStringResourceId":I
    :cond_2
    const-string v3, "unmounted"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 265
    const/16 v0, 0x12

    .line 266
    .restart local v0    # "errorCode":I
    const v1, 0x7f0b0167

    .restart local v1    # "errorStringResourceId":I
    goto :goto_0

    .line 267
    .end local v0    # "errorCode":I
    .end local v1    # "errorStringResourceId":I
    :cond_3
    const-string v3, "shared"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 268
    const/16 v0, 0x13

    .line 269
    .restart local v0    # "errorCode":I
    const v1, 0x7f0b0166

    .restart local v1    # "errorStringResourceId":I
    goto :goto_0

    .line 271
    .end local v0    # "errorCode":I
    .end local v1    # "errorStringResourceId":I
    :cond_4
    const/16 v0, 0x14

    .line 272
    .restart local v0    # "errorCode":I
    const v1, 0x7f0b0167

    .restart local v1    # "errorStringResourceId":I
    goto :goto_0
.end method

.method public static getRecursiveFileSize(Ljava/io/File;)J
    .locals 2
    .param p0, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 296
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {p0, v0}, Lcom/google/android/videos/utils/OfflineUtil;->getRecursiveFileSize(Ljava/io/File;Ljava/util/Set;)J

    move-result-wide v0

    return-wide v0
.end method

.method private static getRecursiveFileSize(Ljava/io/File;Ljava/util/Set;)J
    .locals 6
    .param p0, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 301
    .local p1, "visited":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-nez v4, :cond_1

    .line 302
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 311
    :cond_0
    return-wide v2

    .line 304
    :cond_1
    const-wide/16 v2, 0x0

    .line 305
    .local v2, "size":J
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 306
    .local v0, "files":[Ljava/io/File;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, v0

    if-ge v1, v4, :cond_0

    .line 307
    aget-object v4, v0, v1

    invoke-virtual {v4}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 308
    aget-object v4, v0, v1

    invoke-static {v4, p1}, Lcom/google/android/videos/utils/OfflineUtil;->getRecursiveFileSize(Ljava/io/File;Ljava/util/Set;)J

    move-result-wide v4

    add-long/2addr v2, v4

    .line 306
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static getRecursiveFileSizeAsync(Ljava/util/concurrent/Executor;Ljava/io/File;Lcom/google/android/videos/async/Callback;)V
    .locals 1
    .param p0, "executor"    # Ljava/util/concurrent/Executor;
    .param p1, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "Ljava/io/File;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/io/File;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 279
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Ljava/io/File;Ljava/lang/Long;>;"
    new-instance v0, Lcom/google/android/videos/utils/OfflineUtil$1;

    invoke-direct {v0, p1, p2}, Lcom/google/android/videos/utils/OfflineUtil$1;-><init>(Ljava/io/File;Lcom/google/android/videos/async/Callback;)V

    invoke-interface {p0, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 293
    return-void
.end method

.method public static getRootFilesDir(Landroid/content/Context;I)Ljava/io/File;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "storage"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/utils/MediaNotMountedException;
        }
    .end annotation

    .prologue
    .line 78
    invoke-static {p0}, Lcom/google/android/videos/utils/OfflineUtil;->getSupportedRootFilesDir(Landroid/content/Context;)[Ljava/io/File;

    move-result-object v1

    .line 79
    .local v1, "roots":[Ljava/io/File;
    array-length v2, v1

    if-le p1, v2, :cond_0

    .line 80
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid storage option: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 82
    :cond_0
    aget-object v0, v1, p1

    .line 83
    .local v0, "root":Ljava/io/File;
    if-nez v0, :cond_2

    .line 84
    if-nez p1, :cond_1

    invoke-static {}, Lcom/google/android/videos/utils/OfflineUtil;->getMediaNotMountedException()Lcom/google/android/videos/utils/MediaNotMountedException;

    move-result-object v2

    :goto_0
    throw v2

    :cond_1
    new-instance v2, Lcom/google/android/videos/utils/MediaNotMountedException;

    const/16 v3, 0x12

    const v4, 0x7f0b0167

    invoke-direct {v2, v3, v4}, Lcom/google/android/videos/utils/MediaNotMountedException;-><init>(II)V

    goto :goto_0

    .line 89
    :cond_2
    return-object v0
.end method

.method public static getSupportedRootFilesDir(Landroid/content/Context;)[Ljava/io/File;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    .line 104
    sget-object v2, Landroid/os/Environment;->DIRECTORY_MOVIES:Ljava/lang/String;

    invoke-static {p0, v2}, Landroid/support/v4/content/ContextCompat;->getExternalFilesDirs(Landroid/content/Context;Ljava/lang/String;)[Ljava/io/File;

    move-result-object v1

    .line 107
    .local v1, "roots":[Ljava/io/File;
    array-length v2, v1

    if-le v2, v3, :cond_0

    .line 108
    new-array v0, v3, [Ljava/io/File;

    .line 109
    .local v0, "result":[Ljava/io/File;
    invoke-static {v1, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 112
    .end local v0    # "result":[Ljava/io/File;
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public static getUserFromMediaDownloadPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 180
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 182
    .local v1, "file":Ljava/io/File;
    :try_start_0
    const-string v2, ".wvm"

    invoke-virtual {p0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p0

    .line 185
    :goto_0
    new-instance v2, Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {p0, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v3

    const-string v4, "utf-8"

    invoke-direct {v2, v3, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 189
    :goto_1
    return-object v2

    .line 182
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object p0

    goto :goto_0

    .line 186
    :catch_0
    move-exception v0

    .line 187
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 188
    .end local v0    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v0

    .line 189
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static getVideoIdFromMediaDownloadPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 159
    const-string v2, ".wvm"

    invoke-virtual {p0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 160
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 161
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p0

    .line 172
    .end local v1    # "file":Ljava/io/File;
    .end local p0    # "path":Ljava/lang/String;
    .local v0, "extPos":I
    :cond_0
    :goto_0
    return-object p0

    .line 165
    .end local v0    # "extPos":I
    .restart local p0    # "path":Ljava/lang/String;
    :cond_1
    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 166
    const-string v2, "application_"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 167
    const-string v2, "application_"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 171
    :cond_2
    :goto_1
    const/16 v2, 0x2e

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 172
    .restart local v0    # "extPos":I
    if-ltz v0, :cond_0

    const/4 v2, 0x0

    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 168
    .end local v0    # "extPos":I
    :cond_3
    const-string v2, "framework_"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 169
    const-string v2, "framework_"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_1
.end method

.method public static getVideoIdFromMetadataDownloadPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 148
    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 149
    const/16 v1, 0x2e

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 150
    .local v0, "extPos":I
    if-ltz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .end local p0    # "path":Ljava/lang/String;
    :cond_0
    return-object p0
.end method

.method public static isAppLevelDrmEncrypted(Ljava/lang/String;Z)Z
    .locals 1
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "legacyDownloadsHaveAppLevelDrm"    # Z

    .prologue
    .line 195
    sget-object v0, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 196
    const-string v0, "framework_"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 197
    const/4 p1, 0x0

    .line 201
    .end local p1    # "legacyDownloadsHaveAppLevelDrm":Z
    :cond_0
    :goto_0
    return p1

    .line 198
    .restart local p1    # "legacyDownloadsHaveAppLevelDrm":Z
    :cond_1
    const-string v0, "application_"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    const/4 p1, 0x1

    goto :goto_0
.end method

.method public static isFileOfVersion(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p0, "fileName"    # Ljava/lang/String;
    .param p1, "version"    # Ljava/lang/String;

    .prologue
    .line 67
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, -0x1

    .line 68
    .local v0, "extPos":I
    :goto_0
    if-ltz v0, :cond_1

    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    return v1

    .line 67
    .end local v0    # "extPos":I
    :cond_0
    const/16 v1, 0x2e

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    goto :goto_0

    .line 68
    .restart local v0    # "extPos":I
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static varargs recursivelyDelete([Ljava/io/File;)V
    .locals 3
    .param p0, "filesAndDirectories"    # [Ljava/io/File;

    .prologue
    .line 236
    if-nez p0, :cond_1

    .line 246
    :cond_0
    return-void

    .line 239
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_0

    .line 240
    aget-object v0, p0, v1

    .line 241
    .local v0, "fileOrDirectory":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 242
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/utils/OfflineUtil;->recursivelyDelete([Ljava/io/File;)V

    .line 244
    :cond_2
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 239
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static recursivelyListFiles(Ljava/io/File;Ljava/util/Collection;)V
    .locals 0
    .param p0, "rootDirectory"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/Collection",
            "<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 206
    .local p1, "results":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/io/File;>;"
    invoke-static {p0, p0, p1}, Lcom/google/android/videos/utils/OfflineUtil;->recursivelyListFilesInternal(Ljava/io/File;Ljava/io/File;Ljava/util/Collection;)V

    .line 207
    return-void
.end method

.method private static recursivelyListFilesInternal(Ljava/io/File;Ljava/io/File;Ljava/util/Collection;)V
    .locals 8
    .param p0, "originalRootDirectory"    # Ljava/io/File;
    .param p1, "rootDirectory"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/io/File;",
            "Ljava/util/Collection",
            "<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 213
    .local p2, "results":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/io/File;>;"
    const/4 v3, 0x0

    .line 215
    .local v3, "filesAndDirs":[Ljava/io/File;
    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 222
    if-nez v3, :cond_1

    .line 223
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Can\'t list files for directory "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 233
    :cond_0
    return-void

    .line 216
    :catch_0
    move-exception v1

    .line 217
    .local v1, "e":Ljava/lang/OutOfMemoryError;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "OOM_debug: Original root="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 218
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "OOM_debug: Current root="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 219
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "OOM_debug: Results size="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 220
    throw v1

    .line 226
    .end local v1    # "e":Ljava/lang/OutOfMemoryError;
    :cond_1
    move-object v0, v3

    .local v0, "arr$":[Ljava/io/File;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v2, v0, v4

    .line 227
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 228
    invoke-static {p0, v2, p2}, Lcom/google/android/videos/utils/OfflineUtil;->recursivelyListFilesInternal(Ljava/io/File;Ljava/io/File;Ljava/util/Collection;)V

    .line 226
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 230
    :cond_2
    invoke-interface {p2, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method
