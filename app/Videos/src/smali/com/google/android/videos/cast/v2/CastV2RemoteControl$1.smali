.class Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;
.super Lcom/google/android/videos/cast/v2/PlayMoviesChannel;
.source "CastV2RemoteControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/cast/v2/CastV2RemoteControl;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/support/v7/media/MediaRouter$RouteInfo;Lcom/google/android/videos/cast/CastMediaRouter;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/api/RecommendationsRequest$Factory;Lcom/google/android/videos/async/Requester;Landroid/content/SharedPreferences;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

.field final synthetic val$robotTokenRequester:Lcom/google/android/videos/async/Requester;


# direct methods
.method constructor <init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;ZLcom/google/android/videos/async/Requester;)V
    .locals 0
    .param p2, "x0"    # Z

    .prologue
    .line 156
    iput-object p1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    iput-object p3, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;->val$robotTokenRequester:Lcom/google/android/videos/async/Requester;

    invoke-direct {p0, p2}, Lcom/google/android/videos/cast/v2/PlayMoviesChannel;-><init>(Z)V

    return-void
.end method


# virtual methods
.method protected onAudioTracksChanged(Ljava/util/List;I)V
    .locals 1
    .param p2, "selectedTrackIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AudioInfo;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 232
    .local p1, "newTracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/wireless/android/video/magma/proto/AudioInfo;>;"
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # invokes: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->notifyAudioTracksChanged(Ljava/util/List;I)V
    invoke-static {v0, p1, p2}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$1100(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Ljava/util/List;I)V

    .line 233
    return-void
.end method

.method protected onKeyRequested(JLjava/lang/String;[Ljava/lang/String;)V
    .locals 11
    .param p1, "cmdId"    # J
    .param p3, "method"    # Ljava/lang/String;
    .param p4, "requests"    # [Ljava/lang/String;

    .prologue
    const/16 v9, -0x3eb

    .line 160
    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # getter for: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->state:I
    invoke-static {v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$300(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 219
    :goto_0
    return-void

    .line 164
    :cond_0
    const-string v1, "WV-token"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 165
    const-string v1, "Unknown key request method \'%s\'"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p3, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 169
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # getter for: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->videoIdToFling:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$400(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 170
    const-string v1, "Key requested for unknown video!"

    invoke-static {v1}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 174
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    invoke-virtual {v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->getError()I

    move-result v1

    if-ne v1, v9, :cond_3

    .line 176
    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    invoke-virtual {v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->clearError()V

    .line 180
    :cond_3
    new-instance v0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1$1;

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1$1;-><init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;JLjava/lang/String;[Ljava/lang/String;)V

    .line 188
    .local v0, "retryAction":Lcom/google/android/videos/utils/RetryAction;
    :try_start_0
    new-instance v8, Lcom/google/android/videos/api/RobotTokenRequest;

    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # getter for: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->account:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$500(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # getter for: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->videoIdToFling:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$400(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # getter for: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->pinned:Z
    invoke-static {v3}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$600(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Z

    move-result v3

    invoke-direct {v8, v1, v2, p4, v3}, Lcom/google/android/videos/api/RobotTokenRequest;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Z)V

    .line 190
    .local v8, "request":Lcom/google/android/videos/api/RobotTokenRequest;
    new-instance v6, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1$2;

    invoke-direct {v6, p0, p1, p2, v0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1$2;-><init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;JLcom/google/android/videos/utils/RetryAction;)V

    .line 214
    .local v6, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/api/RobotTokenRequest;Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;->val$robotTokenRequester:Lcom/google/android/videos/async/Requester;

    invoke-interface {v1, v8, v6}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 215
    .end local v6    # "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/api/RobotTokenRequest;Ljava/lang/String;>;"
    .end local v8    # "request":Lcom/google/android/videos/api/RobotTokenRequest;
    :catch_0
    move-exception v7

    .line 216
    .local v7, "e":Ljava/lang/Exception;
    const-string v1, "Exception thrown while trying to fetch RobotTokens for cast device"

    invoke-static {v1, v7}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 217
    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    invoke-virtual {v1, v9, v0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->onError(ILcom/google/android/videos/utils/RetryAction;)V

    goto :goto_0
.end method

.method protected onSubtitleTracksChanged(Ljava/util/List;Lcom/google/android/videos/subtitles/SubtitleTrack;)V
    .locals 1
    .param p2, "newSelectedTrack"    # Lcom/google/android/videos/subtitles/SubtitleTrack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ")V"
        }
    .end annotation

    .prologue
    .line 224
    .local p1, "newTracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/subtitles/SubtitleTrack;>;"
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # setter for: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->subtitleTracks:Ljava/util/List;
    invoke-static {v0, p1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$802(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Ljava/util/List;)Ljava/util/List;

    .line 225
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # setter for: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->selectedSubtitleTrack:Lcom/google/android/videos/subtitles/SubtitleTrack;
    invoke-static {v0, p2}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$902(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Lcom/google/android/videos/subtitles/SubtitleTrack;)Lcom/google/android/videos/subtitles/SubtitleTrack;

    .line 226
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # invokes: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->updateData()V
    invoke-static {v0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$1000(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)V

    .line 227
    return-void
.end method
