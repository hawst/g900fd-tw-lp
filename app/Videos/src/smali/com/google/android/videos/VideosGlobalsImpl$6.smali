.class Lcom/google/android/videos/VideosGlobalsImpl$6;
.super Lcom/google/android/videos/store/Database$BaseListener;
.source "VideosGlobalsImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/VideosGlobalsImpl;->ensureNotificationRelatedObjectsCreated()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/VideosGlobalsImpl;


# direct methods
.method constructor <init>(Lcom/google/android/videos/VideosGlobalsImpl;)V
    .locals 0

    .prologue
    .line 1180
    iput-object p1, p0, Lcom/google/android/videos/VideosGlobalsImpl$6;->this$0:Lcom/google/android/videos/VideosGlobalsImpl;

    invoke-direct {p0}, Lcom/google/android/videos/store/Database$BaseListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onPurchasesUpdated(Ljava/lang/String;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 1183
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl$6;->this$0:Lcom/google/android/videos/VideosGlobalsImpl;

    # getter for: Lcom/google/android/videos/VideosGlobalsImpl;->contentNotificationManager:Lcom/google/android/videos/ContentNotificationManager;
    invoke-static {v0}, Lcom/google/android/videos/VideosGlobalsImpl;->access$400(Lcom/google/android/videos/VideosGlobalsImpl;)Lcom/google/android/videos/ContentNotificationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/ContentNotificationManager;->checkForNewEpisodes()V

    .line 1184
    return-void
.end method

.method public onShowMetadataUpdated(Ljava/lang/String;)V
    .locals 1
    .param p1, "showId"    # Ljava/lang/String;

    .prologue
    .line 1187
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl$6;->this$0:Lcom/google/android/videos/VideosGlobalsImpl;

    # getter for: Lcom/google/android/videos/VideosGlobalsImpl;->contentNotificationManager:Lcom/google/android/videos/ContentNotificationManager;
    invoke-static {v0}, Lcom/google/android/videos/VideosGlobalsImpl;->access$400(Lcom/google/android/videos/VideosGlobalsImpl;)Lcom/google/android/videos/ContentNotificationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/ContentNotificationManager;->checkForNewEpisodes()V

    .line 1188
    return-void
.end method

.method public onShowPosterUpdated(Ljava/lang/String;)V
    .locals 1
    .param p1, "showId"    # Ljava/lang/String;

    .prologue
    .line 1191
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl$6;->this$0:Lcom/google/android/videos/VideosGlobalsImpl;

    # getter for: Lcom/google/android/videos/VideosGlobalsImpl;->contentNotificationManager:Lcom/google/android/videos/ContentNotificationManager;
    invoke-static {v0}, Lcom/google/android/videos/VideosGlobalsImpl;->access$400(Lcom/google/android/videos/VideosGlobalsImpl;)Lcom/google/android/videos/ContentNotificationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/ContentNotificationManager;->checkForNewEpisodes()V

    .line 1192
    return-void
.end method
