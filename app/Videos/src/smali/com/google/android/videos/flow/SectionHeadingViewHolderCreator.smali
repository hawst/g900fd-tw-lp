.class public Lcom/google/android/videos/flow/SectionHeadingViewHolderCreator;
.super Ljava/lang/Object;
.source "SectionHeadingViewHolderCreator.java"

# interfaces
.implements Lcom/google/android/videos/flow/ViewHolderCreator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/flow/ViewHolderCreator",
        "<",
        "Lcom/google/android/videos/ui/SectionHeadingHelper;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/google/android/videos/flow/SectionHeadingViewHolderCreator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/google/android/videos/flow/SectionHeadingViewHolderCreator;

    invoke-direct {v0}, Lcom/google/android/videos/flow/SectionHeadingViewHolderCreator;-><init>()V

    sput-object v0, Lcom/google/android/videos/flow/SectionHeadingViewHolderCreator;->INSTANCE:Lcom/google/android/videos/flow/SectionHeadingViewHolderCreator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addOrVerify(Landroid/util/SparseArray;I)V
    .locals 7
    .param p1, "viewType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/videos/flow/ViewHolderCreator",
            "<*>;>;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 45
    .local p0, "viewTypes":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/google/android/videos/flow/ViewHolderCreator<*>;>;"
    invoke-virtual {p0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/flow/ViewHolderCreator;

    .line 46
    .local v0, "existing":Lcom/google/android/videos/flow/ViewHolderCreator;, "Lcom/google/android/videos/flow/ViewHolderCreator<*>;"
    if-nez v0, :cond_1

    .line 47
    sget-object v1, Lcom/google/android/videos/flow/SectionHeadingViewHolderCreator;->INSTANCE:Lcom/google/android/videos/flow/SectionHeadingViewHolderCreator;

    invoke-virtual {p0, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 52
    :cond_0
    return-void

    .line 48
    :cond_1
    sget-object v1, Lcom/google/android/videos/flow/SectionHeadingViewHolderCreator;->INSTANCE:Lcom/google/android/videos/flow/SectionHeadingViewHolderCreator;

    if-eq v0, v1, :cond_0

    .line 49
    new-instance v1, Ljava/lang/IllegalArgumentException;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Existing ViewHolderCreator for view type 0x%1$x is not a SectionHeadingViewHolderCreator"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public bridge synthetic createViewHolder(ILandroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1
    .param p1, "x0"    # I
    .param p2, "x1"    # Landroid/view/ViewGroup;

    .prologue
    .line 18
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/flow/SectionHeadingViewHolderCreator;->createViewHolder(ILandroid/view/ViewGroup;)Lcom/google/android/videos/ui/SectionHeadingHelper;

    move-result-object v0

    return-object v0
.end method

.method public createViewHolder(ILandroid/view/ViewGroup;)Lcom/google/android/videos/ui/SectionHeadingHelper;
    .locals 4
    .param p1, "viewType"    # I
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 27
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0400be

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 29
    .local v0, "sectionHeadingView":Landroid/view/View;
    new-instance v1, Lcom/google/android/videos/ui/SectionHeadingHelper;

    invoke-direct {v1, v0}, Lcom/google/android/videos/ui/SectionHeadingHelper;-><init>(Landroid/view/View;)V

    return-object v1
.end method
