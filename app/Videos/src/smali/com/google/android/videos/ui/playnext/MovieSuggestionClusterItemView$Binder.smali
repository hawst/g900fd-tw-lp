.class public Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView$Binder;
.super Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;
.source "MovieSuggestionClusterItemView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Binder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder",
        "<",
        "Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;",
        "Lcom/google/android/videos/adapter/ArrayDataSource",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final bitmapRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/logging/UiElementNode;)V
    .locals 1
    .param p2, "clusterUiElementNode"    # Lcom/google/android/videos/logging/UiElementNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/google/android/videos/logging/UiElementNode;",
            ")V"
        }
    .end annotation

    .prologue
    .line 149
    .local p1, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Landroid/graphics/Bitmap;>;"
    const/4 v0, 0x2

    invoke-direct {p0, p2, v0}, Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;-><init>(Lcom/google/android/videos/logging/UiElementNode;I)V

    .line 150
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView$Binder;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    .line 151
    return-void
.end method


# virtual methods
.method protected bridge synthetic onBind(Lcom/google/android/videos/ui/playnext/ClusterItemView;Lcom/google/android/videos/adapter/DataSource;I)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/ui/playnext/ClusterItemView;
    .param p2, "x1"    # Lcom/google/android/videos/adapter/DataSource;
    .param p3, "x2"    # I

    .prologue
    .line 143
    check-cast p1, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;

    .end local p1    # "x0":Lcom/google/android/videos/ui/playnext/ClusterItemView;
    check-cast p2, Lcom/google/android/videos/adapter/ArrayDataSource;

    .end local p2    # "x1":Lcom/google/android/videos/adapter/DataSource;
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView$Binder;->onBind(Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;Lcom/google/android/videos/adapter/ArrayDataSource;I)V

    return-void
.end method

.method protected onBind(Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;Lcom/google/android/videos/adapter/ArrayDataSource;I)V
    .locals 3
    .param p1, "view"    # Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;
    .param p3, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;",
            "Lcom/google/android/videos/adapter/ArrayDataSource",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 156
    .local p2, "dataSource":Lcom/google/android/videos/adapter/ArrayDataSource;, "Lcom/google/android/videos/adapter/ArrayDataSource<Lcom/google/wireless/android/video/magma/proto/AssetResource;>;"
    invoke-virtual {p2, p3}, Lcom/google/android/videos/adapter/ArrayDataSource;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView$Binder;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    invoke-virtual {p2}, Lcom/google/android/videos/adapter/ArrayDataSource;->isNetworkConnected()Z

    move-result v2

    invoke-virtual {p1, v0, v1, v2, p0}, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->bind(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/android/videos/async/Requester;ZLcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView$Binder;)V

    .line 157
    return-void
.end method
