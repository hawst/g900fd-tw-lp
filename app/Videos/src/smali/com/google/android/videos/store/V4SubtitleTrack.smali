.class public final Lcom/google/android/videos/store/V4SubtitleTrack;
.super Ljava/lang/Object;
.source "V4SubtitleTrack.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x563d73082449947L


# instance fields
.field public final format:I

.field public final languageCode:Ljava/lang/String;

.field public final trackName:Ljava/lang/String;

.field public final url:Ljava/lang/String;

.field public final videoId:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object v1, p0, Lcom/google/android/videos/store/V4SubtitleTrack;->languageCode:Ljava/lang/String;

    .line 22
    iput-object v1, p0, Lcom/google/android/videos/store/V4SubtitleTrack;->trackName:Ljava/lang/String;

    .line 23
    iput-object v1, p0, Lcom/google/android/videos/store/V4SubtitleTrack;->videoId:Ljava/lang/String;

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/store/V4SubtitleTrack;->format:I

    .line 25
    iput-object v1, p0, Lcom/google/android/videos/store/V4SubtitleTrack;->url:Ljava/lang/String;

    .line 26
    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/videos/store/V4SubtitleTrack;->languageCode:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/videos/store/V4SubtitleTrack;->trackName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/store/V4SubtitleTrack;->videoId:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/videos/store/V4SubtitleTrack;->format:I

    iget-object v4, p0, Lcom/google/android/videos/store/V4SubtitleTrack;->url:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/videos/subtitles/SubtitleTrack;->createLegacy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/google/android/videos/subtitles/SubtitleTrack;

    move-result-object v0

    return-object v0
.end method
