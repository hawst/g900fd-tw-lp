.class Lcom/google/android/videos/player/LocalPlaybackHelper$MediaSessionEventCallback;
.super Landroid/support/v4/media/session/MediaSessionCompat$Callback;
.source "LocalPlaybackHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/LocalPlaybackHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MediaSessionEventCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/player/LocalPlaybackHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/player/LocalPlaybackHelper;)V
    .locals 0

    .prologue
    .line 1376
    iput-object p1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$MediaSessionEventCallback;->this$0:Lcom/google/android/videos/player/LocalPlaybackHelper;

    invoke-direct {p0}, Landroid/support/v4/media/session/MediaSessionCompat$Callback;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/player/LocalPlaybackHelper;Lcom/google/android/videos/player/LocalPlaybackHelper$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/player/LocalPlaybackHelper;
    .param p2, "x1"    # Lcom/google/android/videos/player/LocalPlaybackHelper$1;

    .prologue
    .line 1376
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/LocalPlaybackHelper$MediaSessionEventCallback;-><init>(Lcom/google/android/videos/player/LocalPlaybackHelper;)V

    return-void
.end method


# virtual methods
.method public onMediaButtonEvent(Landroid/content/Intent;)Z
    .locals 3
    .param p1, "mediaButtonEvent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    .line 1380
    const-string v2, "android.intent.extra.KEY_EVENT"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/view/KeyEvent;

    .line 1381
    .local v0, "event":Landroid/view/KeyEvent;
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    .line 1402
    :goto_0
    return v1

    .line 1385
    :cond_0
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    .line 1387
    :sswitch_0
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$MediaSessionEventCallback;->this$0:Lcom/google/android/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;
    invoke-static {v1}, Lcom/google/android/videos/player/LocalPlaybackHelper;->access$1000(Lcom/google/android/videos/player/LocalPlaybackHelper;)Lcom/google/android/videos/player/VideosPlayer;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/videos/player/VideosPlayer;->getPlayWhenReady()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1388
    invoke-virtual {p0}, Lcom/google/android/videos/player/LocalPlaybackHelper$MediaSessionEventCallback;->onPause()V

    .line 1402
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 1390
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/videos/player/LocalPlaybackHelper$MediaSessionEventCallback;->onPlay()V

    goto :goto_1

    .line 1394
    :sswitch_1
    invoke-virtual {p0}, Lcom/google/android/videos/player/LocalPlaybackHelper$MediaSessionEventCallback;->onPlay()V

    goto :goto_1

    .line 1397
    :sswitch_2
    invoke-virtual {p0}, Lcom/google/android/videos/player/LocalPlaybackHelper$MediaSessionEventCallback;->onPause()V

    goto :goto_1

    .line 1385
    nop

    :sswitch_data_0
    .sparse-switch
        0x55 -> :sswitch_0
        0x7e -> :sswitch_1
        0x7f -> :sswitch_2
    .end sparse-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 1412
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$MediaSessionEventCallback;->this$0:Lcom/google/android/videos/player/LocalPlaybackHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/player/LocalPlaybackHelper;->pause()V

    .line 1413
    return-void
.end method

.method public onPlay()V
    .locals 2

    .prologue
    .line 1407
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$MediaSessionEventCallback;->this$0:Lcom/google/android/videos/player/LocalPlaybackHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/player/LocalPlaybackHelper;->play(Z)V

    .line 1408
    return-void
.end method
