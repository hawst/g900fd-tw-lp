.class public Lcom/google/android/videos/store/WishlistStore;
.super Ljava/lang/Object;
.source "WishlistStore.java"

# interfaces
.implements Lcom/google/android/videos/async/Requester;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/store/WishlistStore$LoadWishlistTask;,
        Lcom/google/android/videos/store/WishlistStore$WishlistRequest;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Requester",
        "<",
        "Lcom/google/android/videos/store/WishlistStore$WishlistRequest;",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field static final PROJECTION:[Ljava/lang/String;


# instance fields
.field private final database:Lcom/google/android/videos/store/Database;

.field private final executor:Ljava/util/concurrent/Executor;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 127
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "wishlist_item_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "wishlist_item_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/videos/store/WishlistStore;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Lcom/google/android/videos/store/Database;)V
    .locals 1
    .param p1, "executor"    # Ljava/util/concurrent/Executor;
    .param p2, "database"    # Lcom/google/android/videos/store/Database;

    .prologue
    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/Database;

    iput-object v0, p0, Lcom/google/android/videos/store/WishlistStore;->database:Lcom/google/android/videos/store/Database;

    .line 141
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/videos/store/WishlistStore;->executor:Ljava/util/concurrent/Executor;

    .line 142
    return-void
.end method

.method static synthetic access$500(Lcom/google/android/videos/store/WishlistStore;)Lcom/google/android/videos/store/Database;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/WishlistStore;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/videos/store/WishlistStore;->database:Lcom/google/android/videos/store/Database;

    return-object v0
.end method


# virtual methods
.method public getDatabase()Lcom/google/android/videos/store/Database;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/videos/store/WishlistStore;->database:Lcom/google/android/videos/store/Database;

    return-object v0
.end method

.method public loadWishlist(Lcom/google/android/videos/store/WishlistStore$WishlistRequest;Lcom/google/android/videos/async/Callback;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/store/WishlistStore$WishlistRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/store/WishlistStore$WishlistRequest;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/store/WishlistStore$WishlistRequest;",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 150
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/store/WishlistStore$WishlistRequest;Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/google/android/videos/store/WishlistStore;->executor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/videos/store/WishlistStore$LoadWishlistTask;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/videos/store/WishlistStore$LoadWishlistTask;-><init>(Lcom/google/android/videos/store/WishlistStore;Lcom/google/android/videos/store/WishlistStore$WishlistRequest;Lcom/google/android/videos/async/Callback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 151
    return-void
.end method

.method public request(Lcom/google/android/videos/store/WishlistStore$WishlistRequest;Lcom/google/android/videos/async/Callback;)V
    .locals 0
    .param p1, "request"    # Lcom/google/android/videos/store/WishlistStore$WishlistRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/store/WishlistStore$WishlistRequest;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/store/WishlistStore$WishlistRequest;",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 146
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/store/WishlistStore$WishlistRequest;Landroid/database/Cursor;>;"
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/store/WishlistStore;->loadWishlist(Lcom/google/android/videos/store/WishlistStore$WishlistRequest;Lcom/google/android/videos/async/Callback;)V

    .line 147
    return-void
.end method

.method public bridge synthetic request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/google/android/videos/async/Callback;

    .prologue
    .line 20
    check-cast p1, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/store/WishlistStore;->request(Lcom/google/android/videos/store/WishlistStore$WishlistRequest;Lcom/google/android/videos/async/Callback;)V

    return-void
.end method
