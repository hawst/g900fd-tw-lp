.class public Lcom/google/android/videos/utils/MediaMountedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MediaMountedReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/utils/MediaMountedReceiver$Listener;
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final listener:Lcom/google/android/videos/utils/MediaMountedReceiver$Listener;

.field private registered:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/videos/utils/MediaMountedReceiver$Listener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/google/android/videos/utils/MediaMountedReceiver$Listener;

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 30
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/videos/utils/MediaMountedReceiver;->context:Landroid/content/Context;

    .line 31
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/utils/MediaMountedReceiver$Listener;

    iput-object v0, p0, Lcom/google/android/videos/utils/MediaMountedReceiver;->listener:Lcom/google/android/videos/utils/MediaMountedReceiver$Listener;

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/utils/MediaMountedReceiver;->registered:Z

    .line 33
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/videos/utils/MediaMountedReceiver;->listener:Lcom/google/android/videos/utils/MediaMountedReceiver$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/utils/MediaMountedReceiver$Listener;->onMediaMountedChanged()V

    .line 54
    return-void
.end method

.method public register()V
    .locals 2

    .prologue
    .line 36
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/videos/utils/MediaMountedReceiver;->registered:Z

    .line 37
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 38
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 39
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 40
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 41
    iget-object v1, p0, Lcom/google/android/videos/utils/MediaMountedReceiver;->context:Landroid/content/Context;

    invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 42
    return-void
.end method

.method public unregister()V
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/google/android/videos/utils/MediaMountedReceiver;->registered:Z

    if-eqz v0, :cond_0

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/utils/MediaMountedReceiver;->registered:Z

    .line 47
    iget-object v0, p0, Lcom/google/android/videos/utils/MediaMountedReceiver;->context:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 49
    :cond_0
    return-void
.end method
