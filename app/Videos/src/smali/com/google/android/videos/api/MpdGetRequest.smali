.class public Lcom/google/android/videos/api/MpdGetRequest;
.super Lcom/google/android/videos/async/Request;
.source "MpdGetRequest.java"


# instance fields
.field public final dash:Z

.field public final device:Ljava/lang/String;

.field public final isEpisode:Z

.field public final locale:Ljava/lang/String;

.field public final product:Ljava/lang/String;

.field public final productManufacturer:Ljava/lang/String;

.field public final productModel:Ljava/lang/String;

.field public final secure:Z

.field public final videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZZZZLjava/util/Locale;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "isEpisode"    # Z
    .param p4, "dash"    # Z
    .param p5, "requireAuthentication"    # Z
    .param p6, "secure"    # Z
    .param p7, "locale"    # Ljava/util/Locale;

    .prologue
    .line 29
    invoke-direct {p0, p1, p5}, Lcom/google/android/videos/async/Request;-><init>(Ljava/lang/String;Z)V

    .line 30
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/api/MpdGetRequest;->videoId:Ljava/lang/String;

    .line 31
    iput-boolean p3, p0, Lcom/google/android/videos/api/MpdGetRequest;->isEpisode:Z

    .line 32
    iput-boolean p4, p0, Lcom/google/android/videos/api/MpdGetRequest;->dash:Z

    .line 33
    invoke-static {p7}, Lcom/google/android/videos/utils/LocaleUtils;->toString(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/api/MpdGetRequest;->locale:Ljava/lang/String;

    .line 34
    iput-boolean p6, p0, Lcom/google/android/videos/api/MpdGetRequest;->secure:Z

    .line 35
    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/videos/api/MpdGetRequest;->device:Ljava/lang/String;

    .line 36
    sget-object v0, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/videos/api/MpdGetRequest;->product:Ljava/lang/String;

    .line 37
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/videos/api/MpdGetRequest;->productManufacturer:Ljava/lang/String;

    .line 38
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/videos/api/MpdGetRequest;->productModel:Ljava/lang/String;

    .line 39
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 43
    if-ne p0, p1, :cond_1

    .line 47
    :cond_0
    :goto_0
    return v1

    .line 44
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 46
    check-cast v0, Lcom/google/android/videos/api/MpdGetRequest;

    .line 47
    .local v0, "that":Lcom/google/android/videos/api/MpdGetRequest;
    iget-boolean v3, p0, Lcom/google/android/videos/api/MpdGetRequest;->isEpisode:Z

    iget-boolean v4, v0, Lcom/google/android/videos/api/MpdGetRequest;->isEpisode:Z

    if-ne v3, v4, :cond_4

    iget-boolean v3, p0, Lcom/google/android/videos/api/MpdGetRequest;->dash:Z

    iget-boolean v4, v0, Lcom/google/android/videos/api/MpdGetRequest;->dash:Z

    if-ne v3, v4, :cond_4

    iget-boolean v3, p0, Lcom/google/android/videos/api/MpdGetRequest;->secure:Z

    iget-boolean v4, v0, Lcom/google/android/videos/api/MpdGetRequest;->secure:Z

    if-ne v3, v4, :cond_4

    iget-object v3, p0, Lcom/google/android/videos/api/MpdGetRequest;->videoId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/api/MpdGetRequest;->videoId:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/videos/api/MpdGetRequest;->account:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/api/MpdGetRequest;->account:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/videos/api/MpdGetRequest;->locale:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/api/MpdGetRequest;->locale:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/videos/api/MpdGetRequest;->device:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/api/MpdGetRequest;->device:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/videos/api/MpdGetRequest;->product:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/api/MpdGetRequest;->product:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/videos/api/MpdGetRequest;->productManufacturer:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/api/MpdGetRequest;->productManufacturer:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/videos/api/MpdGetRequest;->productModel:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/api/MpdGetRequest;->productModel:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-boolean v3, p0, Lcom/google/android/videos/api/MpdGetRequest;->requireAuthentication:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iget-boolean v4, v0, Lcom/google/android/videos/api/MpdGetRequest;->requireAuthentication:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 62
    iget-object v2, p0, Lcom/google/android/videos/api/MpdGetRequest;->videoId:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/videos/api/MpdGetRequest;->videoId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 63
    .local v0, "result":I
    :goto_0
    mul-int/lit8 v4, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/api/MpdGetRequest;->account:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/videos/api/MpdGetRequest;->account:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v4, v2

    .line 64
    mul-int/lit8 v4, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/api/MpdGetRequest;->locale:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/videos/api/MpdGetRequest;->locale:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v4, v2

    .line 65
    mul-int/lit8 v4, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/api/MpdGetRequest;->device:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/videos/api/MpdGetRequest;->device:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v4, v2

    .line 66
    mul-int/lit8 v4, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/api/MpdGetRequest;->product:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/videos/api/MpdGetRequest;->product:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v4, v2

    .line 67
    mul-int/lit8 v4, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/api/MpdGetRequest;->productManufacturer:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/videos/api/MpdGetRequest;->productManufacturer:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_5
    add-int v0, v4, v2

    .line 68
    mul-int/lit8 v4, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/api/MpdGetRequest;->productModel:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/videos/api/MpdGetRequest;->productModel:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_6
    add-int v0, v4, v2

    .line 69
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v2, p0, Lcom/google/android/videos/api/MpdGetRequest;->isEpisode:Z

    if-eqz v2, :cond_7

    move v2, v3

    :goto_7
    add-int v0, v4, v2

    .line 70
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v2, p0, Lcom/google/android/videos/api/MpdGetRequest;->dash:Z

    if-eqz v2, :cond_8

    move v2, v3

    :goto_8
    add-int v0, v4, v2

    .line 71
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v2, p0, Lcom/google/android/videos/api/MpdGetRequest;->secure:Z

    if-eqz v2, :cond_9

    move v2, v3

    :goto_9
    add-int v0, v4, v2

    .line 72
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v4, p0, Lcom/google/android/videos/api/MpdGetRequest;->requireAuthentication:Z

    if-eqz v4, :cond_a

    :goto_a
    add-int v0, v2, v3

    .line 73
    return v0

    .end local v0    # "result":I
    :cond_0
    move v0, v1

    .line 62
    goto :goto_0

    .restart local v0    # "result":I
    :cond_1
    move v2, v1

    .line 63
    goto :goto_1

    :cond_2
    move v2, v1

    .line 64
    goto :goto_2

    :cond_3
    move v2, v1

    .line 65
    goto :goto_3

    :cond_4
    move v2, v1

    .line 66
    goto :goto_4

    :cond_5
    move v2, v1

    .line 67
    goto :goto_5

    :cond_6
    move v2, v1

    .line 68
    goto :goto_6

    :cond_7
    move v2, v1

    .line 69
    goto :goto_7

    :cond_8
    move v2, v1

    .line 70
    goto :goto_8

    :cond_9
    move v2, v1

    .line 71
    goto :goto_9

    :cond_a
    move v3, v1

    .line 72
    goto :goto_a
.end method
