.class public Lcom/google/android/videos/pano/playback/PanoControllerOverlay;
.super Ljava/lang/Object;
.source "PanoControllerOverlay.java"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Landroid/support/v17/leanback/widget/OnActionClickedListener;
.implements Landroid/support/v17/leanback/widget/OnItemViewClickedListener;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$Listener;
.implements Lcom/google/android/videos/player/overlay/ControllerOverlay;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/pano/playback/PanoControllerOverlay$MultiAudioAction;,
        Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackDescriptionPresenter;,
        Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;
    }
.end annotation


# static fields
.field private static final LONG_VIDEO_SPEED_UP_FACTORS:[I

.field private static final SHORT_VIDEO_SPEED_UP_FACTORS:[I


# instance fields
.field private final activity:Landroid/support/v4/app/FragmentActivity;

.field private audioTracks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AudioInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final closedCaptionAction:Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;

.field private final controlGlue:Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;

.field private currentPlaybackSpeedId:I

.field private currentTimeMillis:I

.field private final errorFragment:Landroid/support/v17/leanback/app/ErrorSupportFragment;

.field private errorState:Z

.field private final fastForwardAction:Landroid/support/v17/leanback/widget/Action;

.field private final handler:Landroid/os/Handler;

.field private isSeeking:Z

.field private final knowledgeControls:Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;

.field private final knowledgeRow:Landroid/support/v17/leanback/widget/Row;

.field private listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

.field private final loadingSpinner:Landroid/view/View;

.field private final multiAudioAction:Landroid/support/v17/leanback/widget/Action;

.field private final overlayFragment:Lcom/google/android/recline/app/PlaybackOverlayFragment;

.field private final overlayView:Landroid/view/ViewGroup;

.field private final panoKnowledgeOverlay:Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;

.field private final playPauseAction:Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;

.field private playWhenReady:Z

.field private final playbackControlsRow:Lcom/google/android/recline/widget/PlaybackControlsRow;

.field private retryAction:Lcom/google/android/videos/utils/RetryAction;

.field private final rewindAction:Landroid/support/v17/leanback/widget/Action;

.field private final rowsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

.field private final secondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

.field private selectedAudioTrackIndex:I

.field private selectedSubtitle:Lcom/google/android/videos/subtitles/SubtitleTrack;

.field private showingChildActivity:Z

.field private speedUp:I

.field private speedUpFactors:[I

.field private subtitleTracks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;"
        }
    .end annotation
.end field

.field private totalTimeMillis:I

.field private final trackChangeListener:Lcom/google/android/videos/player/overlay/TrackChangeListener;

.field private final updateProgressRunnable:Ljava/lang/Runnable;

.field private updateProgressRunnableActive:Z

.field videoTitle:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 78
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->SHORT_VIDEO_SPEED_UP_FACTORS:[I

    .line 79
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->LONG_VIDEO_SPEED_UP_FACTORS:[I

    return-void

    .line 78
    :array_0
    .array-data 4
        0x2
        0x4
        0xc
        0x30
    .end array-data

    .line 79
    :array_1
    .array-data 4
        0x2
        0x8
        0x20
        0x80
    .end array-data
.end method

.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;Lcom/google/android/videos/player/overlay/TrackChangeListener;Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;)V
    .locals 12
    .param p1, "activity"    # Landroid/support/v4/app/FragmentActivity;
    .param p2, "trackChangeListener"    # Lcom/google/android/videos/player/overlay/TrackChangeListener;
    .param p3, "panoKnowledgeOverlay"    # Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    iput v10, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->speedUp:I

    .line 119
    sget-object v7, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->LONG_VIDEO_SPEED_UP_FACTORS:[I

    iput-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->speedUpFactors:[I

    .line 124
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/support/v4/app/FragmentActivity;

    iput-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->activity:Landroid/support/v4/app/FragmentActivity;

    .line 125
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/videos/player/overlay/TrackChangeListener;

    iput-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->trackChangeListener:Lcom/google/android/videos/player/overlay/TrackChangeListener;

    .line 126
    iput-object p3, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->panoKnowledgeOverlay:Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;

    .line 129
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v7

    const v8, 0x7f040080

    invoke-virtual {v7, v8, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    .line 131
    .local v4, "overlay":Landroid/view/ViewGroup;
    iput-object v4, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->overlayView:Landroid/view/ViewGroup;

    .line 132
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    const v8, 0x7f0f019a

    invoke-virtual {v7, v8}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v7

    check-cast v7, Lcom/google/android/recline/app/PlaybackOverlayFragment;

    iput-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->overlayFragment:Lcom/google/android/recline/app/PlaybackOverlayFragment;

    .line 135
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v7

    const-string v8, "ErrorFragment"

    invoke-virtual {v7, v8}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Landroid/support/v17/leanback/app/ErrorSupportFragment;

    .line 137
    .local v2, "errorFragment":Landroid/support/v17/leanback/app/ErrorSupportFragment;
    if-nez v2, :cond_0

    .line 138
    new-instance v2, Landroid/support/v17/leanback/app/ErrorSupportFragment;

    .end local v2    # "errorFragment":Landroid/support/v17/leanback/app/ErrorSupportFragment;
    invoke-direct {v2}, Landroid/support/v17/leanback/app/ErrorSupportFragment;-><init>()V

    .line 139
    .restart local v2    # "errorFragment":Landroid/support/v17/leanback/app/ErrorSupportFragment;
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v7

    invoke-virtual {v7}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v7

    const v8, 0x1020002

    const-string v9, "ErrorFragment"

    invoke-virtual {v7, v8, v2, v9}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v7

    invoke-virtual {v7}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 143
    :cond_0
    iput-object v2, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->errorFragment:Landroid/support/v17/leanback/app/ErrorSupportFragment;

    .line 145
    invoke-virtual {v2, p0}, Landroid/support/v17/leanback/app/ErrorSupportFragment;->setButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    const v7, 0x7f0f00ef

    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->loadingSpinner:Landroid/view/View;

    .line 147
    new-instance v7, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;

    invoke-direct {v7, p1, p0}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;-><init>(Landroid/app/Activity;Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$Listener;)V

    iput-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->knowledgeControls:Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;

    .line 148
    new-instance v7, Landroid/os/Handler;

    invoke-direct {v7, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->handler:Landroid/os/Handler;

    .line 149
    iput v10, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->currentPlaybackSpeedId:I

    .line 151
    new-instance v7, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;

    invoke-direct {v7, p0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;-><init>(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)V

    iput-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->controlGlue:Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;

    .line 152
    iget-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->controlGlue:Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;

    invoke-virtual {v7, p0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;->setOnItemViewClickedListener(Landroid/support/v17/leanback/widget/OnItemViewClickedListener;)V

    .line 155
    new-instance v7, Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;

    invoke-direct {v7, p1}, Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->playPauseAction:Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;

    .line 156
    new-instance v7, Lcom/google/android/recline/widget/PlaybackControlsRow$RewindAction;

    invoke-direct {v7, p1}, Lcom/google/android/recline/widget/PlaybackControlsRow$RewindAction;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->rewindAction:Landroid/support/v17/leanback/widget/Action;

    .line 157
    new-instance v7, Lcom/google/android/recline/widget/PlaybackControlsRow$FastForwardAction;

    invoke-direct {v7, p1}, Lcom/google/android/recline/widget/PlaybackControlsRow$FastForwardAction;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->fastForwardAction:Landroid/support/v17/leanback/widget/Action;

    .line 158
    new-instance v7, Lcom/google/android/recline/widget/PlaybackControlsRow$ClosedCaptioningAction;

    invoke-direct {v7, p1}, Lcom/google/android/recline/widget/PlaybackControlsRow$ClosedCaptioningAction;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->closedCaptionAction:Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;

    .line 159
    new-instance v7, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$MultiAudioAction;

    invoke-direct {v7, p1}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$MultiAudioAction;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->multiAudioAction:Landroid/support/v17/leanback/widget/Action;

    .line 160
    new-instance v0, Landroid/support/v17/leanback/widget/ControlButtonPresenterSelector;

    invoke-direct {v0}, Landroid/support/v17/leanback/widget/ControlButtonPresenterSelector;-><init>()V

    .line 161
    .local v0, "actionPresenterSelector":Landroid/support/v17/leanback/widget/ControlButtonPresenterSelector;
    new-instance v7, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    invoke-direct {v7, v0}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/PresenterSelector;)V

    iput-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->secondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    .line 162
    new-instance v6, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    invoke-direct {v6, v0}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/PresenterSelector;)V

    .line 163
    .local v6, "primaryActionsAdapter":Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
    iget-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->rewindAction:Landroid/support/v17/leanback/widget/Action;

    invoke-virtual {v6, v7}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 164
    iget-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->playPauseAction:Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;

    invoke-virtual {v6, v7}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 165
    iget-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->fastForwardAction:Landroid/support/v17/leanback/widget/Action;

    invoke-virtual {v6, v7}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 166
    new-instance v7, Lcom/google/android/recline/widget/PlaybackControlsRow;

    iget-object v8, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->controlGlue:Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;

    invoke-direct {v7, v8}, Lcom/google/android/recline/widget/PlaybackControlsRow;-><init>(Ljava/lang/Object;)V

    iput-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->playbackControlsRow:Lcom/google/android/recline/widget/PlaybackControlsRow;

    .line 167
    iget-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->controlGlue:Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;

    iget-object v8, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->playbackControlsRow:Lcom/google/android/recline/widget/PlaybackControlsRow;

    invoke-virtual {v7, v8}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;->setControlsRow(Lcom/google/android/recline/widget/PlaybackControlsRow;)V

    .line 168
    iget-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->playbackControlsRow:Lcom/google/android/recline/widget/PlaybackControlsRow;

    iget-object v8, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->secondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    invoke-virtual {v7, v8}, Lcom/google/android/recline/widget/PlaybackControlsRow;->setSecondaryActionsAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 169
    iget-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->knowledgeControls:Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;

    invoke-virtual {v7}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->createRow()Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->knowledgeRow:Landroid/support/v17/leanback/widget/Row;

    .line 170
    new-instance v1, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;

    new-instance v7, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackDescriptionPresenter;

    invoke-direct {v7, p0, v11}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackDescriptionPresenter;-><init>(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;Lcom/google/android/videos/pano/playback/PanoControllerOverlay$1;)V

    invoke-direct {v1, v7}, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;-><init>(Landroid/support/v17/leanback/widget/Presenter;)V

    .line 172
    .local v1, "controlsPresenter":Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;
    invoke-virtual {v1, p0}, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;->setOnActionClickedListener(Landroid/support/v17/leanback/widget/OnActionClickedListener;)V

    .line 173
    iget-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->knowledgeControls:Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;

    invoke-virtual {v7}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->createPresenter()Landroid/support/v17/leanback/widget/RowPresenter;

    move-result-object v3

    .line 174
    .local v3, "knowledgePresenter":Landroid/support/v17/leanback/widget/Presenter;
    new-instance v5, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$1;

    invoke-direct {v5, p0, v1, v3}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$1;-><init>(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 186
    .local v5, "presenterSelector":Landroid/support/v17/leanback/widget/PresenterSelector;
    new-instance v7, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    invoke-direct {v7, v5}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/PresenterSelector;)V

    iput-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->rowsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    .line 187
    iget-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->rowsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    iget-object v8, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->playbackControlsRow:Lcom/google/android/recline/widget/PlaybackControlsRow;

    invoke-virtual {v7, v8}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 188
    iget-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->rowsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    iget-object v8, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->knowledgeRow:Landroid/support/v17/leanback/widget/Row;

    invoke-virtual {v7, v8}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 189
    iget-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->overlayFragment:Lcom/google/android/recline/app/PlaybackOverlayFragment;

    iget-object v8, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->rowsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    invoke-virtual {v7, v8}, Lcom/google/android/recline/app/PlaybackOverlayFragment;->setAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 191
    new-instance v7, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$2;

    invoke-direct {v7, p0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$2;-><init>(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)V

    iput-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->updateProgressRunnable:Ljava/lang/Runnable;

    .line 198
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)Lcom/google/android/recline/widget/PlaybackControlsRow;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->playbackControlsRow:Lcom/google/android/recline/widget/PlaybackControlsRow;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    .prologue
    .line 70
    iget v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->totalTimeMillis:I

    return v0
.end method

.method static synthetic access$1200(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    .prologue
    .line 70
    iget v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->speedUp:I

    return v0
.end method

.method static synthetic access$1302(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/pano/playback/PanoControllerOverlay;
    .param p1, "x1"    # I

    .prologue
    .line 70
    iput p1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->speedUp:I

    return p1
.end method

.method static synthetic access$1400(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->isSeeking:Z

    return v0
.end method

.method static synthetic access$1402(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/pano/playback/PanoControllerOverlay;
    .param p1, "x1"    # Z

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->isSeeking:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)Landroid/support/v17/leanback/widget/Row;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->knowledgeRow:Landroid/support/v17/leanback/widget/Row;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->controlGlue:Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)Landroid/support/v4/app/FragmentActivity;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->activity:Landroid/support/v4/app/FragmentActivity;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)Lcom/google/android/recline/app/PlaybackOverlayFragment;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->overlayFragment:Lcom/google/android/recline/app/PlaybackOverlayFragment;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)[I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->speedUpFactors:[I

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    .prologue
    .line 70
    iget v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->currentTimeMillis:I

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    .prologue
    .line 70
    iget v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->currentPlaybackSpeedId:I

    return v0
.end method

.method static synthetic access$902(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/pano/playback/PanoControllerOverlay;
    .param p1, "x1"    # I

    .prologue
    .line 70
    iput p1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->currentPlaybackSpeedId:I

    return p1
.end method

.method private clearError()V
    .locals 2

    .prologue
    .line 426
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->errorFragment:Landroid/support/v17/leanback/app/ErrorSupportFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/app/ErrorSupportFragment;->setMessage(Ljava/lang/CharSequence;)V

    .line 427
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->errorFragment:Landroid/support/v17/leanback/app/ErrorSupportFragment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/ErrorSupportFragment;->getView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 428
    return-void
.end method

.method private notifyActionChanged(Landroid/support/v17/leanback/widget/Action;)V
    .locals 3
    .param p1, "action"    # Landroid/support/v17/leanback/widget/Action;

    .prologue
    .line 380
    iget-object v1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->secondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    invoke-virtual {v1, p1}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 381
    .local v0, "index":I
    if-ltz v0, :cond_0

    .line 382
    iget-object v1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->secondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->notifyArrayItemRangeChanged(II)V

    .line 384
    :cond_0
    return-void
.end method

.method private onSeekBy(I)V
    .locals 3
    .param p1, "timeMillis"    # I

    .prologue
    .line 501
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->totalTimeMillis:I

    iget v2, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->currentTimeMillis:I

    add-int/2addr v2, p1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->seekTo(I)V

    .line 502
    return-void
.end method

.method private seekTo(I)V
    .locals 2
    .param p1, "timeMillis"    # I

    .prologue
    const/4 v1, 0x0

    .line 505
    iput p1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->currentTimeMillis:I

    .line 506
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->playbackControlsRow:Lcom/google/android/recline/widget/PlaybackControlsRow;

    invoke-virtual {v0, v1}, Lcom/google/android/recline/widget/PlaybackControlsRow;->setBufferedProgress(I)V

    .line 507
    invoke-direct {p0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->updatePlayState()V

    .line 509
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    invoke-interface {v0, v1, p1, v1}, Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;->onSeekTo(ZIZ)V

    .line 510
    return-void
.end method

.method private setError(Ljava/lang/String;)V
    .locals 4
    .param p1, "errorMessage"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 418
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->errorFragment:Landroid/support/v17/leanback/app/ErrorSupportFragment;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/app/ErrorSupportFragment;->setMessage(Ljava/lang/CharSequence;)V

    .line 419
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->errorFragment:Landroid/support/v17/leanback/app/ErrorSupportFragment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/ErrorSupportFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 420
    iget-object v1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->errorFragment:Landroid/support/v17/leanback/app/ErrorSupportFragment;

    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->retryAction:Lcom/google/android/videos/utils/RetryAction;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->activity:Landroid/support/v4/app/FragmentActivity;

    const v2, 0x7f0b010e

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/support/v17/leanback/app/ErrorSupportFragment;->setButtonText(Ljava/lang/String;)V

    .line 421
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->errorFragment:Landroid/support/v17/leanback/app/ErrorSupportFragment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/ErrorSupportFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 422
    invoke-virtual {p0, v3}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->hideControls(Z)V

    .line 423
    return-void

    .line 420
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setTotalTime(I)V
    .locals 3
    .param p1, "totalTimeMillis"    # I

    .prologue
    .line 514
    iget v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->totalTimeMillis:I

    if-eq p1, v0, :cond_0

    .line 515
    iput p1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->totalTimeMillis:I

    .line 516
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->playbackControlsRow:Lcom/google/android/recline/widget/PlaybackControlsRow;

    invoke-virtual {v0, p1}, Lcom/google/android/recline/widget/PlaybackControlsRow;->setTotalTime(I)V

    .line 517
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->rowsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->notifyArrayItemRangeChanged(II)V

    .line 519
    :cond_0
    return-void
.end method

.method private updateFadingEnabled()V
    .locals 2

    .prologue
    .line 263
    iget-object v1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->overlayFragment:Lcom/google/android/recline/app/PlaybackOverlayFragment;

    iget-boolean v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->playWhenReady:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->errorState:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->showingChildActivity:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/recline/app/PlaybackOverlayFragment;->setFadingEnabled(Z)V

    .line 264
    return-void

    .line 263
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updatePlayState()V
    .locals 2

    .prologue
    .line 522
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->playbackControlsRow:Lcom/google/android/recline/widget/PlaybackControlsRow;

    iget v1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->totalTimeMillis:I

    invoke-virtual {v0, v1}, Lcom/google/android/recline/widget/PlaybackControlsRow;->setTotalTime(I)V

    .line 523
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->playbackControlsRow:Lcom/google/android/recline/widget/PlaybackControlsRow;

    iget v1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->currentTimeMillis:I

    invoke-virtual {v0, v1}, Lcom/google/android/recline/widget/PlaybackControlsRow;->setCurrentTime(I)V

    .line 524
    iget v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->totalTimeMillis:I

    invoke-direct {p0, v0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->updateSpeedupRates(I)V

    .line 526
    iget-boolean v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->errorState:Z

    if-nez v0, :cond_0

    .line 527
    iget-object v1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->playPauseAction:Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;

    iget-boolean v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->playWhenReady:Z

    if-eqz v0, :cond_1

    sget v0, Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;->PAUSE:I

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;->setIndex(I)V

    .line 529
    :cond_0
    return-void

    .line 527
    :cond_1
    sget v0, Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;->PLAY:I

    goto :goto_0
.end method

.method private updateSecondaryActions()V
    .locals 2

    .prologue
    .line 370
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->secondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->clear()V

    .line 371
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->subtitleTracks:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 372
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->secondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    iget-object v1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->closedCaptionAction:Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 374
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->audioTracks:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 375
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->secondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    iget-object v1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->multiAudioAction:Landroid/support/v17/leanback/widget/Action;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 377
    :cond_1
    return-void
.end method

.method private updateSpeedupRates(I)V
    .locals 1
    .param p1, "videoDurationMillis"    # I

    .prologue
    .line 533
    const v0, 0x2a1d40

    if-ge p1, v0, :cond_0

    sget-object v0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->SHORT_VIDEO_SPEED_UP_FACTORS:[I

    :goto_0
    iput-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->speedUpFactors:[I

    .line 535
    return-void

    .line 533
    :cond_0
    sget-object v0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->LONG_VIDEO_SPEED_UP_FACTORS:[I

    goto :goto_0
.end method


# virtual methods
.method public clearAudioTracks()V
    .locals 1

    .prologue
    .line 335
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->setSelectedAudioTrackIndex(I)V

    .line 336
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->setAudioTracks(Ljava/util/List;)V

    .line 337
    return-void
.end method

.method public clearSubtitles()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 329
    invoke-virtual {p0, v1}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->setSelectedSubtitleTrack(Lcom/google/android/videos/subtitles/SubtitleTrack;)V

    .line 330
    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->setSubtitles(Ljava/util/List;Z)V

    .line 331
    return-void
.end method

.method public generateLayoutParams()Lcom/google/android/videos/player/PlayerView$LayoutParams;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 212
    new-instance v0, Lcom/google/android/videos/player/PlayerView$LayoutParams;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v2, v1}, Lcom/google/android/videos/player/PlayerView$LayoutParams;-><init>(IIZ)V

    return-object v0
.end method

.method public getInControllerKnowledgeView()Lcom/google/android/videos/tagging/KnowledgeView;
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->knowledgeControls:Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->overlayView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 270
    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_0

    .line 271
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->handler:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x15e

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 272
    iget v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->speedUp:I

    mul-int/lit16 v0, v0, 0x15e

    invoke-direct {p0, v0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->onSeekBy(I)V

    .line 274
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public hideControls(Z)V
    .locals 0
    .param p1, "persistHideAtOrientationChange"    # Z

    .prologue
    .line 482
    return-void
.end method

.method public init(Z)V
    .locals 0
    .param p1, "playWhenInitialized"    # Z

    .prologue
    .line 452
    if-nez p1, :cond_0

    .line 453
    invoke-virtual {p0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->showControls()V

    .line 455
    :cond_0
    return-void
.end method

.method public onActionClicked(Landroid/support/v17/leanback/widget/Action;)V
    .locals 9
    .param p1, "action"    # Landroid/support/v17/leanback/widget/Action;

    .prologue
    .line 220
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 221
    .local v2, "optionsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->closedCaptionAction:Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;

    if-ne p1, v7, :cond_2

    .line 222
    iget-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->subtitleTracks:Ljava/util/List;

    if-nez v7, :cond_1

    .line 252
    :cond_0
    :goto_0
    return-void

    .line 225
    :cond_1
    const v5, 0x7f0b01ec

    .line 226
    .local v5, "titleResourceId":I
    const/16 v3, 0x3e9

    .line 227
    .local v3, "requestCode":I
    iget-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->subtitleTracks:Ljava/util/List;

    iget-object v8, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->selectedSubtitle:Lcom/google/android/videos/subtitles/SubtitleTrack;

    invoke-interface {v7, v8}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    .line 228
    .local v4, "selectedIndex":I
    iget-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->subtitleTracks:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/videos/subtitles/SubtitleTrack;

    .line 229
    .local v6, "track":Lcom/google/android/videos/subtitles/SubtitleTrack;
    invoke-virtual {v6}, Lcom/google/android/videos/subtitles/SubtitleTrack;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 231
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v3    # "requestCode":I
    .end local v4    # "selectedIndex":I
    .end local v5    # "titleResourceId":I
    .end local v6    # "track":Lcom/google/android/videos/subtitles/SubtitleTrack;
    :cond_2
    iget-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->multiAudioAction:Landroid/support/v17/leanback/widget/Action;

    if-ne p1, v7, :cond_0

    .line 232
    iget-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->audioTracks:Ljava/util/List;

    if-eqz v7, :cond_0

    .line 235
    const v5, 0x7f0b01ee

    .line 236
    .restart local v5    # "titleResourceId":I
    const/16 v3, 0x3ea

    .line 237
    .restart local v3    # "requestCode":I
    iget v4, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->selectedAudioTrackIndex:I

    .line 238
    .restart local v4    # "selectedIndex":I
    iget-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->audioTracks:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    .line 240
    .local v6, "track":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    iget-object v7, v6, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->language:Ljava/lang/String;

    invoke-static {v7}, Lcom/google/android/videos/utils/Util;->getLanguageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 247
    .end local v6    # "track":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    :cond_3
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->showingChildActivity:Z

    .line 248
    invoke-direct {p0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->updateFadingEnabled()V

    .line 249
    iget-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->activity:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v7, v5, v2, v4}, Lcom/google/android/videos/pano/activity/PlaybackSelectorActivity;->createIntent(Landroid/content/Context;ILjava/util/ArrayList;I)Landroid/content/Intent;

    move-result-object v1

    .line 251
    .local v1, "intent":Landroid/content/Intent;
    iget-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->activity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v7, v1, v3}, Landroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)Z
    .locals 7
    .param p1, "requestCode"    # I
    .param p2, "result"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v6, -0x1

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 607
    iput-boolean v5, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->showingChildActivity:Z

    .line 613
    invoke-direct {p0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->updateFadingEnabled()V

    .line 615
    const/16 v3, 0x3e8

    if-ne p1, v3, :cond_3

    .line 616
    if-eqz p3, :cond_1

    const-string v3, "EXTRA_DIALOG_ID"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v3, v4

    :goto_0
    invoke-static {v3}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 617
    const-string v3, "EXTRA_DIALOG_ID"

    invoke-virtual {p3, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 618
    .local v0, "dialogId":I
    if-ne p2, v6, :cond_2

    .line 619
    iget-object v3, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    invoke-interface {v3, v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;->onDialogConfirmed(I)V

    .line 647
    .end local v0    # "dialogId":I
    :cond_0
    :goto_1
    return v4

    :cond_1
    move v3, v5

    .line 616
    goto :goto_0

    .line 621
    .restart local v0    # "dialogId":I
    :cond_2
    iget-object v3, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    invoke-interface {v3, v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;->onDialogCanceled(I)V

    goto :goto_1

    .line 626
    .end local v0    # "dialogId":I
    :cond_3
    const/16 v3, 0x3e9

    if-ne p1, v3, :cond_5

    .line 627
    invoke-static {p3}, Lcom/google/android/videos/pano/activity/PlaybackSelectorActivity;->getSelected(Landroid/content/Intent;)I

    move-result v1

    .line 628
    .local v1, "selected":I
    if-ne p2, v6, :cond_0

    if-ltz v1, :cond_0

    iget-object v3, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->subtitleTracks:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 629
    iget-object v3, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->subtitleTracks:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/subtitles/SubtitleTrack;

    .line 630
    .local v2, "track":Lcom/google/android/videos/subtitles/SubtitleTrack;
    iget-object v3, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->trackChangeListener:Lcom/google/android/videos/player/overlay/TrackChangeListener;

    invoke-virtual {v2}, Lcom/google/android/videos/subtitles/SubtitleTrack;->isDisableTrack()Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v2, 0x0

    .end local v2    # "track":Lcom/google/android/videos/subtitles/SubtitleTrack;
    :cond_4
    invoke-interface {v3, v2}, Lcom/google/android/videos/player/overlay/TrackChangeListener;->onSelectSubtitleTrack(Lcom/google/android/videos/subtitles/SubtitleTrack;)V

    goto :goto_1

    .line 635
    .end local v1    # "selected":I
    :cond_5
    const/16 v3, 0x3ea

    if-ne p1, v3, :cond_6

    .line 636
    invoke-static {p3}, Lcom/google/android/videos/pano/activity/PlaybackSelectorActivity;->getSelected(Landroid/content/Intent;)I

    move-result v1

    .line 637
    .restart local v1    # "selected":I
    if-ne p2, v6, :cond_0

    if-ltz v1, :cond_0

    iget-object v3, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->audioTracks:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 638
    iget-object v3, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->trackChangeListener:Lcom/google/android/videos/player/overlay/TrackChangeListener;

    invoke-interface {v3, v1}, Lcom/google/android/videos/player/overlay/TrackChangeListener;->onSelectAudioTrackIndex(I)V

    goto :goto_1

    .line 643
    .end local v1    # "selected":I
    :cond_6
    const/16 v3, 0x3ec

    if-eq p1, v3, :cond_0

    move v4, v5

    .line 647
    goto :goto_1
.end method

.method public onBackPressed()Z
    .locals 1

    .prologue
    .line 548
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->retryAction:Lcom/google/android/videos/utils/RetryAction;

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->retryAction:Lcom/google/android/videos/utils/RetryAction;

    invoke-interface {v0}, Lcom/google/android/videos/utils/RetryAction;->onRetry()V

    .line 260
    :cond_0
    return-void
.end method

.method public onFocusKnowledgeEntity(Lcom/google/android/videos/tagging/KnowledgeEntity;)V
    .locals 1
    .param p1, "focused"    # Lcom/google/android/videos/tagging/KnowledgeEntity;

    .prologue
    .line 294
    if-nez p1, :cond_0

    .line 295
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->panoKnowledgeOverlay:Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;->clearSpotlight()V

    .line 299
    :goto_0
    return-void

    .line 297
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->panoKnowledgeOverlay:Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;->setSpotlight(Lcom/google/android/videos/tagging/KnowledgeEntity;)V

    goto :goto_0
.end method

.method public onItemClicked(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Landroid/support/v17/leanback/widget/Row;)V
    .locals 2
    .param p1, "itemViewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .param p2, "item"    # Ljava/lang/Object;
    .param p3, "rowViewHolder"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .param p4, "row"    # Landroid/support/v17/leanback/widget/Row;

    .prologue
    .line 601
    iget-object v1, p1, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/tagging/KnowledgeEntity;

    .line 602
    .local v0, "entity":Lcom/google/android/videos/tagging/KnowledgeEntity;
    invoke-virtual {p0, v0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->onSelectKnowledgeEntity(Lcom/google/android/videos/tagging/KnowledgeEntity;)V

    .line 603
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 554
    iget-object v1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->overlayFragment:Lcom/google/android/recline/app/PlaybackOverlayFragment;

    invoke-virtual {v1}, Lcom/google/android/recline/app/PlaybackOverlayFragment;->getInputEventHandler_recline()Lcom/google/android/recline/app/PlaybackOverlayFragment$InputEventHandler;

    move-result-object v1

    invoke-interface {v1, p2}, Lcom/google/android/recline/app/PlaybackOverlayFragment$InputEventHandler;->handleInputEvent(Landroid/view/InputEvent;)Z

    move-result v0

    .line 555
    .local v0, "handled":Z
    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 563
    const/16 v0, 0x55

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onOrientationChanged()V
    .locals 0

    .prologue
    .line 569
    return-void
.end method

.method public onSelectKnowledgeEntity(Lcom/google/android/videos/tagging/KnowledgeEntity;)V
    .locals 5
    .param p1, "selected"    # Lcom/google/android/videos/tagging/KnowledgeEntity;

    .prologue
    .line 302
    instance-of v2, p1, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 303
    check-cast v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    .line 304
    .local v0, "actor":Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.ASSIST"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "query"

    iget-object v4, v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->name:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 306
    .local v1, "search":Landroid/content/Intent;
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->showingChildActivity:Z

    .line 307
    invoke-direct {p0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->updateFadingEnabled()V

    .line 308
    iget-object v2, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->activity:Landroid/support/v4/app/FragmentActivity;

    const/16 v3, 0x3ec

    invoke-virtual {v2, v1, v3}, Landroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 311
    .end local v0    # "actor":Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    .end local v1    # "search":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public reset()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 460
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->retryAction:Lcom/google/android/videos/utils/RetryAction;

    .line 461
    iput-boolean v2, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->errorState:Z

    .line 462
    iput-boolean v2, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->playWhenReady:Z

    .line 463
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->updateProgressRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 464
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->playbackControlsRow:Lcom/google/android/recline/widget/PlaybackControlsRow;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/google/android/recline/widget/PlaybackControlsRow;->setTotalTime(I)V

    .line 465
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->playPauseAction:Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;

    sget v1, Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;->PAUSE:I

    invoke-virtual {v0, v1}, Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;->setIndex(I)V

    .line 466
    invoke-virtual {p0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->stopSeek()V

    .line 467
    invoke-direct {p0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->clearError()V

    .line 468
    invoke-virtual {p0, v2}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->hideControls(Z)V

    .line 469
    invoke-direct {p0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->updateFadingEnabled()V

    .line 470
    return-void
.end method

.method public setAudioTracks(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AudioInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 360
    .local p1, "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/wireless/android/video/magma/proto/AudioInfo;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    :goto_0
    iput-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->audioTracks:Ljava/util/List;

    .line 361
    invoke-direct {p0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->updateSecondaryActions()V

    .line 362
    return-void

    .line 360
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setErrorState(Ljava/lang/String;Lcom/google/android/videos/utils/RetryAction;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "retryAction"    # Lcom/google/android/videos/utils/RetryAction;

    .prologue
    .line 410
    iput-object p2, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->retryAction:Lcom/google/android/videos/utils/RetryAction;

    .line 411
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->errorState:Z

    .line 412
    invoke-virtual {p0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->stopSeek()V

    .line 413
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->loadingSpinner:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 414
    invoke-direct {p0, p1}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->setError(Ljava/lang/String;)V

    .line 415
    return-void
.end method

.method public setHasKnowledge(Z)V
    .locals 2
    .param p1, "hasKnowledge"    # Z

    .prologue
    .line 285
    if-nez p1, :cond_0

    .line 286
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->rowsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    iget-object v1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->knowledgeRow:Landroid/support/v17/leanback/widget/Row;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->remove(Ljava/lang/Object;)Z

    .line 290
    :goto_0
    return-void

    .line 288
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->rowsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    iget-object v1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->knowledgeRow:Landroid/support/v17/leanback/widget/Row;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public setHasSpinner(Z)V
    .locals 0
    .param p1, "hasSpinner"    # Z

    .prologue
    .line 437
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 438
    return-void
.end method

.method public setHideable(Z)V
    .locals 0
    .param p1, "hideable"    # Z

    .prologue
    .line 539
    return-void
.end method

.method public setHq(Z)V
    .locals 0
    .param p1, "on"    # Z

    .prologue
    .line 443
    return-void
.end method

.method public setHqIsHd(Z)V
    .locals 0
    .param p1, "hqIsHd"    # Z

    .prologue
    .line 448
    return-void
.end method

.method public setListener(Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    .prologue
    .line 202
    iput-object p1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    .line 203
    return-void
.end method

.method public setSelectedAudioTrackIndex(I)V
    .locals 0
    .param p1, "trackIndex"    # I

    .prologue
    .line 366
    iput p1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->selectedAudioTrackIndex:I

    .line 367
    return-void
.end method

.method public setSelectedSubtitleTrack(Lcom/google/android/videos/subtitles/SubtitleTrack;)V
    .locals 2
    .param p1, "track"    # Lcom/google/android/videos/subtitles/SubtitleTrack;

    .prologue
    .line 352
    iput-object p1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->selectedSubtitle:Lcom/google/android/videos/subtitles/SubtitleTrack;

    .line 353
    iget-object v1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->closedCaptionAction:Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;

    if-eqz p1, :cond_0

    iget-boolean v0, p1, Lcom/google/android/videos/subtitles/SubtitleTrack;->isForced:Z

    if-nez v0, :cond_0

    sget v0, Lcom/google/android/recline/widget/PlaybackControlsRow$ClosedCaptioningAction;->ON:I

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;->setIndex(I)V

    .line 355
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->closedCaptionAction:Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;

    invoke-direct {p0, v0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->notifyActionChanged(Landroid/support/v17/leanback/widget/Action;)V

    .line 356
    return-void

    .line 353
    :cond_0
    sget v0, Lcom/google/android/recline/widget/PlaybackControlsRow$ClosedCaptioningAction;->OFF:I

    goto :goto_0
.end method

.method public setState(ZZ)V
    .locals 5
    .param p1, "playWhenReady"    # Z
    .param p2, "loading"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 388
    iput-boolean v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->errorState:Z

    .line 389
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->retryAction:Lcom/google/android/videos/utils/RetryAction;

    .line 390
    invoke-direct {p0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->clearError()V

    .line 391
    iget-object v1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->loadingSpinner:Landroid/view/View;

    if-eqz p2, :cond_3

    if-eqz p1, :cond_3

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 392
    iput-boolean p1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->playWhenReady:Z

    .line 393
    if-eqz p1, :cond_1

    .line 394
    iget-boolean v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->updateProgressRunnableActive:Z

    if-nez v0, :cond_0

    .line 395
    iput-boolean v4, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->updateProgressRunnableActive:Z

    .line 396
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->updateProgressRunnable:Ljava/lang/Runnable;

    iget-object v2, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->controlGlue:Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;

    invoke-virtual {v2}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;->getUpdatePeriod()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 398
    :cond_0
    iput v4, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->currentPlaybackSpeedId:I

    .line 400
    :cond_1
    invoke-direct {p0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->updatePlayState()V

    .line 401
    invoke-direct {p0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->updateFadingEnabled()V

    .line 403
    if-nez p1, :cond_2

    .line 404
    invoke-virtual {p0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->showControls()V

    .line 406
    :cond_2
    return-void

    .line 391
    :cond_3
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setStoryboards([Lcom/google/wireless/android/video/magma/proto/Storyboard;)V
    .locals 0
    .param p1, "storyboards"    # [Lcom/google/wireless/android/video/magma/proto/Storyboard;

    .prologue
    .line 325
    return-void
.end method

.method public setSubtitles(Ljava/util/List;Z)V
    .locals 2
    .param p2, "includeDisableOption"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .local p1, "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/subtitles/SubtitleTrack;>;"
    const/4 v0, 0x0

    .line 341
    if-nez p1, :cond_0

    .line 342
    iput-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->subtitleTracks:Ljava/util/List;

    .line 347
    :goto_0
    invoke-direct {p0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->updateSecondaryActions()V

    .line 348
    return-void

    .line 344
    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->activity:Landroid/support/v4/app/FragmentActivity;

    const v1, 0x7f0b01ed

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_1
    invoke-static {p1, v0}, Lcom/google/android/videos/subtitles/TrackSelectionUtil;->getSelectableTracks(Ljava/util/List;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->subtitleTracks:Ljava/util/List;

    goto :goto_0
.end method

.method public setSupportsQualityToggle(Z)V
    .locals 0
    .param p1, "supportsQualityToggle"    # Z

    .prologue
    .line 433
    return-void
.end method

.method public setTimes(III)V
    .locals 2
    .param p1, "currentTimeMillis"    # I
    .param p2, "totalTimeMillis"    # I
    .param p3, "bufferedPercent"    # I

    .prologue
    .line 491
    iget v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->speedUp:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 498
    :goto_0
    return-void

    .line 494
    :cond_0
    iput p1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->currentTimeMillis:I

    .line 495
    invoke-direct {p0, p2}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->setTotalTime(I)V

    .line 496
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->playbackControlsRow:Lcom/google/android/recline/widget/PlaybackControlsRow;

    mul-int v1, p3, p2

    div-int/lit8 v1, v1, 0x64

    invoke-virtual {v0, v1}, Lcom/google/android/recline/widget/PlaybackControlsRow;->setBufferedProgress(I)V

    .line 497
    invoke-direct {p0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->updatePlayState()V

    goto :goto_0
.end method

.method public setTouchExplorationEnabled(Z)V
    .locals 0
    .param p1, "touchExplorationEnabled"    # Z

    .prologue
    .line 544
    return-void
.end method

.method public setUseScrubPad(Z)V
    .locals 0
    .param p1, "useScrubPad"    # Z

    .prologue
    .line 487
    return-void
.end method

.method public setVideoInfo(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "durationMillis"    # I
    .param p3, "resumeTimeMillis"    # I
    .param p4, "videoTitle"    # Ljava/lang/String;

    .prologue
    .line 316
    iput-object p4, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->videoTitle:Ljava/lang/String;

    .line 317
    iput p3, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->currentTimeMillis:I

    .line 318
    invoke-direct {p0, p2}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->setTotalTime(I)V

    .line 319
    invoke-direct {p0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->updatePlayState()V

    .line 320
    return-void
.end method

.method public showControls()V
    .locals 1

    .prologue
    .line 474
    iget-boolean v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->errorState:Z

    if-nez v0, :cond_0

    .line 475
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->overlayFragment:Lcom/google/android/recline/app/PlaybackOverlayFragment;

    invoke-virtual {v0}, Lcom/google/android/recline/app/PlaybackOverlayFragment;->tickle()V

    .line 477
    :cond_0
    return-void
.end method

.method public showShortClockConfirmationDialog(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 8
    .param p1, "dialogId"    # I
    .param p2, "title"    # Ljava/lang/CharSequence;
    .param p3, "message"    # Ljava/lang/CharSequence;
    .param p4, "posterUri"    # Ljava/lang/String;

    .prologue
    .line 585
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->showingChildActivity:Z

    .line 586
    invoke-direct {p0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->updateFadingEnabled()V

    .line 587
    iget-object v7, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->activity:Landroid/support/v4/app/FragmentActivity;

    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->activity:Landroid/support/v4/app/FragmentActivity;

    const v4, 0x1040013

    const v5, 0x1040009

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v6, p4

    invoke-static/range {v0 .. v6}, Lcom/google/android/videos/pano/activity/PanoDialogActivity;->createIntent(Landroid/content/Context;ILjava/lang/CharSequence;Ljava/lang/CharSequence;IILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x3e8

    invoke-virtual {v7, v0, v1}, Landroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 591
    return-void
.end method

.method public showingChildActivity()Z
    .locals 1

    .prologue
    .line 595
    iget-boolean v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->showingChildActivity:Z

    return v0
.end method

.method public stopSeek()V
    .locals 2

    .prologue
    .line 652
    iget-boolean v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->isSeeking:Z

    if-nez v0, :cond_0

    .line 661
    :goto_0
    return-void

    .line 655
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->playPauseAction:Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;

    invoke-virtual {v0}, Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;->getIndex()I

    move-result v0

    sget v1, Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;->PLAY:I

    if-ne v0, v1, :cond_1

    .line 656
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->controlGlue:Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;->startPlayback(I)V

    .line 660
    :goto_1
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->controlGlue:Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;

    invoke-virtual {v0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;->onStateChanged()V

    goto :goto_0

    .line 658
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->controlGlue:Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;

    invoke-virtual {v0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;->pausePlayback()V

    goto :goto_1
.end method
