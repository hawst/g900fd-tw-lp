.class public final Lcom/google/android/videos/tagging/SongCards$GoToStoreOnClickListener;
.super Ljava/lang/Object;
.source "SongCards.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/SongCards;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1c
    name = "GoToStoreOnClickListener"
.end annotation


# instance fields
.field private final account:Ljava/lang/String;

.field private final activity:Landroid/app/Activity;

.field private final storeUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "storeUrl"    # Ljava/lang/String;
    .param p3, "account"    # Ljava/lang/String;

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iput-object p1, p0, Lcom/google/android/videos/tagging/SongCards$GoToStoreOnClickListener;->activity:Landroid/app/Activity;

    .line 109
    iput-object p2, p0, Lcom/google/android/videos/tagging/SongCards$GoToStoreOnClickListener;->storeUrl:Ljava/lang/String;

    .line 110
    iput-object p3, p0, Lcom/google/android/videos/tagging/SongCards$GoToStoreOnClickListener;->account:Ljava/lang/String;

    .line 111
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/videos/tagging/SongCards$GoToStoreOnClickListener;->activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/videos/tagging/SongCards$GoToStoreOnClickListener;->storeUrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/tagging/SongCards$GoToStoreOnClickListener;->account:Ljava/lang/String;

    const/4 v3, 0x3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/videos/utils/PlayStoreUtil;->startForUri(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)V

    .line 116
    return-void
.end method
