.class final Lcom/google/android/videos/cache/PersistentCache$3;
.super Ljava/lang/Object;
.source "PersistentCache.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/cache/PersistentCache;->triggerCleanup(Ljava/util/concurrent/Executor;Ljava/lang/String;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$cachePath:Ljava/lang/String;

.field final synthetic val$limit:J


# direct methods
.method constructor <init>(Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcom/google/android/videos/cache/PersistentCache$3;->val$cachePath:Ljava/lang/String;

    iput-wide p2, p0, Lcom/google/android/videos/cache/PersistentCache$3;->val$limit:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/videos/cache/PersistentCache$3;->val$cachePath:Ljava/lang/String;

    iget-wide v2, p0, Lcom/google/android/videos/cache/PersistentCache$3;->val$limit:J

    invoke-static {v0, v2, v3}, Lcom/google/android/videos/cache/PersistentCache;->cleanup(Ljava/lang/String;J)V

    .line 163
    return-void
.end method
