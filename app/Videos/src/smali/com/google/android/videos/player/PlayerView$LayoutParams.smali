.class public Lcom/google/android/videos/player/PlayerView$LayoutParams;
.super Landroid/widget/RelativeLayout$LayoutParams;
.source "PlayerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/PlayerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LayoutParams"
.end annotation


# instance fields
.field public final isInsetView:Z


# direct methods
.method public constructor <init>(IIZ)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "isInsetView"    # Z

    .prologue
    .line 233
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 234
    iput-boolean p3, p0, Lcom/google/android/videos/player/PlayerView$LayoutParams;->isInsetView:Z

    .line 235
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 226
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 227
    sget-object v1, Lcom/google/android/videos/R$styleable;->PlayerOverlaysLayout_Layout:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 228
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/videos/player/PlayerView$LayoutParams;->isInsetView:Z

    .line 229
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 230
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;Z)V
    .locals 0
    .param p1, "source"    # Landroid/view/ViewGroup$LayoutParams;
    .param p2, "isInsetView"    # Z

    .prologue
    .line 238
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 239
    iput-boolean p2, p0, Lcom/google/android/videos/player/PlayerView$LayoutParams;->isInsetView:Z

    .line 240
    return-void
.end method
