.class public Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
.super Ljava/lang/Object;
.source "PurchaseStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/PurchaseStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PurchaseRequest"
.end annotation


# instance fields
.field private final cancellationSignalHolder:Lcom/google/android/videos/store/CancellationSignalHolder;

.field private final columns:[Ljava/lang/String;

.field private final distinct:Z

.field private final groupClause:Ljava/lang/String;

.field private final limit:I

.field private final orderClause:Ljava/lang/String;

.field private final ratingIdColumn:Ljava/lang/String;

.field private final subRequestCreator:Lcom/google/android/videos/store/PurchaseStore$SubRequestCreator;

.field private final tables:Ljava/lang/String;

.field private final whereArgs:[Ljava/lang/String;

.field private final whereClause:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 12
    .param p1, "distinct"    # Z
    .param p2, "tables"    # Ljava/lang/String;
    .param p3, "columns"    # [Ljava/lang/String;
    .param p4, "ratingIdColumn"    # Ljava/lang/String;
    .param p5, "whereClause"    # Ljava/lang/String;
    .param p6, "whereArgs"    # [Ljava/lang/String;
    .param p7, "groupClause"    # Ljava/lang/String;
    .param p8, "orderClause"    # Ljava/lang/String;
    .param p9, "limit"    # I

    .prologue
    .line 67
    invoke-static/range {p4 .. p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move/from16 v9, p9

    invoke-direct/range {v0 .. v11}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/videos/store/PurchaseStore$SubRequestCreator;Lcom/google/android/videos/store/CancellationSignalHolder;)V

    .line 69
    return-void
.end method

.method public constructor <init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/videos/store/CancellationSignalHolder;)V
    .locals 12
    .param p1, "distinct"    # Z
    .param p2, "tables"    # Ljava/lang/String;
    .param p3, "columns"    # [Ljava/lang/String;
    .param p4, "ratingIdColumn"    # Ljava/lang/String;
    .param p5, "whereClause"    # Ljava/lang/String;
    .param p6, "whereArgs"    # [Ljava/lang/String;
    .param p7, "groupClause"    # Ljava/lang/String;
    .param p8, "orderClause"    # Ljava/lang/String;
    .param p9, "limit"    # I
    .param p10, "cancellationSignalHolder"    # Lcom/google/android/videos/store/CancellationSignalHolder;

    .prologue
    .line 78
    invoke-static/range {p4 .. p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v10, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move/from16 v9, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/videos/store/PurchaseStore$SubRequestCreator;Lcom/google/android/videos/store/CancellationSignalHolder;)V

    .line 80
    return-void
.end method

.method public constructor <init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/videos/store/PurchaseStore$SubRequestCreator;)V
    .locals 12
    .param p1, "distinct"    # Z
    .param p2, "tables"    # Ljava/lang/String;
    .param p3, "columns"    # [Ljava/lang/String;
    .param p4, "ratingIdColumn"    # Ljava/lang/String;
    .param p5, "whereClause"    # Ljava/lang/String;
    .param p6, "whereArgs"    # [Ljava/lang/String;
    .param p7, "groupClause"    # Ljava/lang/String;
    .param p8, "orderClause"    # Ljava/lang/String;
    .param p9, "limit"    # I
    .param p10, "subRequestCreator"    # Lcom/google/android/videos/store/PurchaseStore$SubRequestCreator;

    .prologue
    .line 104
    invoke-static/range {p10 .. p10}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/videos/store/PurchaseStore$SubRequestCreator;

    const/4 v11, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move/from16 v9, p9

    invoke-direct/range {v0 .. v11}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/videos/store/PurchaseStore$SubRequestCreator;Lcom/google/android/videos/store/CancellationSignalHolder;)V

    .line 106
    return-void
.end method

.method private constructor <init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/videos/store/PurchaseStore$SubRequestCreator;Lcom/google/android/videos/store/CancellationSignalHolder;)V
    .locals 1
    .param p1, "distinct"    # Z
    .param p2, "tables"    # Ljava/lang/String;
    .param p3, "columns"    # [Ljava/lang/String;
    .param p4, "ratingIdColumn"    # Ljava/lang/String;
    .param p5, "whereClause"    # Ljava/lang/String;
    .param p6, "whereArgs"    # [Ljava/lang/String;
    .param p7, "groupClause"    # Ljava/lang/String;
    .param p8, "orderClause"    # Ljava/lang/String;
    .param p9, "limit"    # I
    .param p10, "subRequestCreator"    # Lcom/google/android/videos/store/PurchaseStore$SubRequestCreator;
    .param p11, "cancellationSignalHolder"    # Lcom/google/android/videos/store/CancellationSignalHolder;

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    iput-boolean p1, p0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->distinct:Z

    .line 113
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->tables:Ljava/lang/String;

    .line 114
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->columns:[Ljava/lang/String;

    .line 115
    iput-object p4, p0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->ratingIdColumn:Ljava/lang/String;

    .line 116
    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->whereClause:Ljava/lang/String;

    .line 117
    iput-object p6, p0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->whereArgs:[Ljava/lang/String;

    .line 118
    iput-object p7, p0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->groupClause:Ljava/lang/String;

    .line 119
    iput-object p8, p0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->orderClause:Ljava/lang/String;

    .line 120
    iput p9, p0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->limit:I

    .line 121
    iput-object p10, p0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->subRequestCreator:Lcom/google/android/videos/store/PurchaseStore$SubRequestCreator;

    .line 122
    iput-object p11, p0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->cancellationSignalHolder:Lcom/google/android/videos/store/CancellationSignalHolder;

    .line 123
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Lcom/google/android/videos/store/PurchaseStore$SubRequestCreator;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->subRequestCreator:Lcom/google/android/videos/store/PurchaseStore$SubRequestCreator;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->distinct:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Lcom/google/android/videos/store/CancellationSignalHolder;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->cancellationSignalHolder:Lcom/google/android/videos/store/CancellationSignalHolder;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->tables:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->columns:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->whereClause:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->ratingIdColumn:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->groupClause:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->orderClause:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .prologue
    .line 35
    iget v0, p0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->limit:I

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->whereArgs:[Ljava/lang/String;

    return-object v0
.end method
