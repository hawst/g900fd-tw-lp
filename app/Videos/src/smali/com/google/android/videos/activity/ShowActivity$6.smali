.class Lcom/google/android/videos/activity/ShowActivity$6;
.super Lcom/google/android/videos/ui/TransitionUtil$TransitionListenerAdapter;
.source "ShowActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/activity/ShowActivity;->createSharedTransition(Z)Landroid/transition/TransitionSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/activity/ShowActivity;

.field final synthetic val$isEnter:Z


# direct methods
.method constructor <init>(Lcom/google/android/videos/activity/ShowActivity;Z)V
    .locals 0

    .prologue
    .line 530
    iput-object p1, p0, Lcom/google/android/videos/activity/ShowActivity$6;->this$0:Lcom/google/android/videos/activity/ShowActivity;

    iput-boolean p2, p0, Lcom/google/android/videos/activity/ShowActivity$6;->val$isEnter:Z

    invoke-direct {p0}, Lcom/google/android/videos/ui/TransitionUtil$TransitionListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onTransitionEnd(Landroid/transition/Transition;)V
    .locals 2
    .param p1, "transition"    # Landroid/transition/Transition;

    .prologue
    .line 540
    iget-boolean v0, p0, Lcom/google/android/videos/activity/ShowActivity$6;->val$isEnter:Z

    if-eqz v0, :cond_0

    .line 541
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity$6;->this$0:Lcom/google/android/videos/activity/ShowActivity;

    iget-object v0, v0, Lcom/google/android/videos/activity/ShowActivity;->backgroundView:Landroid/view/ViewGroup;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    .line 543
    :cond_0
    return-void
.end method

.method public onTransitionStart(Landroid/transition/Transition;)V
    .locals 2
    .param p1, "transition"    # Landroid/transition/Transition;

    .prologue
    .line 535
    iget-object v0, p0, Lcom/google/android/videos/activity/ShowActivity$6;->this$0:Lcom/google/android/videos/activity/ShowActivity;

    iget-object v0, v0, Lcom/google/android/videos/activity/ShowActivity;->backgroundView:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    .line 536
    return-void
.end method
