.class public Lcom/google/android/videos/tagging/TagsView;
.super Landroid/widget/FrameLayout;
.source "TagsView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/tagging/TagsView$LabelView;,
        Lcom/google/android/videos/tagging/TagsView$TagShapeView;,
        Lcom/google/android/videos/tagging/TagsView$TagViewComparator;,
        Lcom/google/android/videos/tagging/TagsView$TagView;,
        Lcom/google/android/videos/tagging/TagsView$OnTagClickListener;
    }
.end annotation


# instance fields
.field private final activatedTagColor:I

.field private activatedView:Landroid/view/View;

.field private final boundingBox:Landroid/graphics/RectF;

.field private downX:F

.field private downY:F

.field private inSpotlightMode:Z

.field private interactionEnabled:Z

.field private moved:Z

.field private onTagClickListener:Lcom/google/android/videos/tagging/TagsView$OnTagClickListener;

.field private final shadowColor:I

.field private final shadowOverflow:F

.field private final shadowRadius:F

.field private final shadowYOffset:F

.field private final tagColor:I

.field private final tagPaint:Landroid/graphics/Paint;

.field private final tagShapeAlpha:F

.field private final tagStrokeWidth:F

.field private final tagViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/TagsView$TagView;",
            ">;"
        }
    .end annotation
.end field

.field private taggedKnowledgeEntities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;",
            ">;"
        }
    .end annotation
.end field

.field private final textHeight:F

.field private final textPaint:Landroid/graphics/Paint;

.field private final textYOffset:F

.field private final touchSlop:I

.field private videoDisplayHeight:I

.field private videoDisplayWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 85
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/videos/tagging/TagsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 86
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 89
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/videos/tagging/TagsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 90
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v6, 0x1

    .line 93
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 95
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 96
    .local v0, "res":Landroid/content/res/Resources;
    const v2, 0x7f0c002a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x437f0000    # 255.0f

    div-float/2addr v2, v3

    iput v2, p0, Lcom/google/android/videos/tagging/TagsView;->tagShapeAlpha:F

    .line 97
    const v2, 0x7f0a00db

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/videos/tagging/TagsView;->tagColor:I

    .line 98
    const v2, 0x7f0a00da

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/videos/tagging/TagsView;->activatedTagColor:I

    .line 99
    const v2, 0x7f0e0198

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    iput v2, p0, Lcom/google/android/videos/tagging/TagsView;->tagStrokeWidth:F

    .line 100
    const v2, 0x7f0a00dc

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/videos/tagging/TagsView;->shadowColor:I

    .line 101
    const v2, 0x7f0e0199

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lcom/google/android/videos/tagging/TagsView;->shadowRadius:F

    .line 102
    const v2, 0x7f0e019a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lcom/google/android/videos/tagging/TagsView;->shadowYOffset:F

    .line 103
    iget v2, p0, Lcom/google/android/videos/tagging/TagsView;->shadowRadius:F

    iget v3, p0, Lcom/google/android/videos/tagging/TagsView;->shadowYOffset:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    add-float/2addr v2, v3

    iput v2, p0, Lcom/google/android/videos/tagging/TagsView;->shadowOverflow:F

    .line 104
    const v2, 0x7f0e019c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    iput v2, p0, Lcom/google/android/videos/tagging/TagsView;->textHeight:F

    .line 105
    iget v2, p0, Lcom/google/android/videos/tagging/TagsView;->textHeight:F

    const v3, 0x7f0e019d

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    add-float/2addr v2, v3

    iput v2, p0, Lcom/google/android/videos/tagging/TagsView;->textYOffset:F

    .line 106
    const v2, 0x7f0a00dd

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 107
    .local v1, "textShadowColor":I
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    iput v2, p0, Lcom/google/android/videos/tagging/TagsView;->touchSlop:I

    .line 109
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/google/android/videos/tagging/TagsView;->tagPaint:Landroid/graphics/Paint;

    .line 110
    iget-object v2, p0, Lcom/google/android/videos/tagging/TagsView;->tagPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 111
    iget-object v2, p0, Lcom/google/android/videos/tagging/TagsView;->tagPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 112
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/google/android/videos/tagging/TagsView;->textPaint:Landroid/graphics/Paint;

    .line 113
    iget-object v2, p0, Lcom/google/android/videos/tagging/TagsView;->textPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 114
    iget-object v2, p0, Lcom/google/android/videos/tagging/TagsView;->textPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 115
    iget-object v2, p0, Lcom/google/android/videos/tagging/TagsView;->textPaint:Landroid/graphics/Paint;

    iget v3, p0, Lcom/google/android/videos/tagging/TagsView;->textHeight:F

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 116
    iget-object v2, p0, Lcom/google/android/videos/tagging/TagsView;->textPaint:Landroid/graphics/Paint;

    iget v3, p0, Lcom/google/android/videos/tagging/TagsView;->shadowRadius:F

    const/4 v4, 0x0

    iget v5, p0, Lcom/google/android/videos/tagging/TagsView;->shadowYOffset:F

    invoke-virtual {v2, v3, v4, v5, v1}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 118
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    iput-object v2, p0, Lcom/google/android/videos/tagging/TagsView;->boundingBox:Landroid/graphics/RectF;

    .line 119
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    .line 120
    iput-boolean v6, p0, Lcom/google/android/videos/tagging/TagsView;->interactionEnabled:Z

    .line 121
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/tagging/TagsView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/tagging/TagsView;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/videos/tagging/TagsView;->createChildren()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/videos/tagging/TagsView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/TagsView;

    .prologue
    .line 36
    iget v0, p0, Lcom/google/android/videos/tagging/TagsView;->tagColor:I

    return v0
.end method

.method static synthetic access$1100(Lcom/google/android/videos/tagging/TagsView;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/TagsView;

    .prologue
    .line 36
    iget v0, p0, Lcom/google/android/videos/tagging/TagsView;->tagShapeAlpha:F

    return v0
.end method

.method static synthetic access$1200(Lcom/google/android/videos/tagging/TagsView;)Landroid/graphics/Paint;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/TagsView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/videos/tagging/TagsView;->textPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/videos/tagging/TagsView;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/TagsView;

    .prologue
    .line 36
    iget v0, p0, Lcom/google/android/videos/tagging/TagsView;->textYOffset:F

    return v0
.end method

.method static synthetic access$1400(Lcom/google/android/videos/tagging/TagsView;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/TagsView;

    .prologue
    .line 36
    iget v0, p0, Lcom/google/android/videos/tagging/TagsView;->textHeight:F

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/tagging/TagsView;)Landroid/graphics/RectF;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/TagsView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/videos/tagging/TagsView;->boundingBox:Landroid/graphics/RectF;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/tagging/TagsView;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/TagsView;

    .prologue
    .line 36
    iget v0, p0, Lcom/google/android/videos/tagging/TagsView;->shadowOverflow:F

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/videos/tagging/TagsView;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/TagsView;

    .prologue
    .line 36
    iget v0, p0, Lcom/google/android/videos/tagging/TagsView;->tagStrokeWidth:F

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/videos/tagging/TagsView;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/TagsView;

    .prologue
    .line 36
    iget v0, p0, Lcom/google/android/videos/tagging/TagsView;->shadowYOffset:F

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/videos/tagging/TagsView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/TagsView;

    .prologue
    .line 36
    iget v0, p0, Lcom/google/android/videos/tagging/TagsView;->shadowColor:I

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/videos/tagging/TagsView;)Landroid/graphics/Paint;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/TagsView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/videos/tagging/TagsView;->tagPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/videos/tagging/TagsView;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/TagsView;

    .prologue
    .line 36
    iget v0, p0, Lcom/google/android/videos/tagging/TagsView;->shadowRadius:F

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/videos/tagging/TagsView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/TagsView;

    .prologue
    .line 36
    iget v0, p0, Lcom/google/android/videos/tagging/TagsView;->activatedTagColor:I

    return v0
.end method

.method private clearViews()V
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/videos/tagging/TagsView;->setActivatedView(Landroid/view/View;)V

    .line 130
    iget-object v0, p0, Lcom/google/android/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 131
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/TagsView;->removeAllViews()V

    .line 132
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/tagging/TagsView;->inSpotlightMode:Z

    .line 133
    return-void
.end method

.method private createChildren()V
    .locals 21

    .prologue
    .line 165
    invoke-direct/range {p0 .. p0}, Lcom/google/android/videos/tagging/TagsView;->clearViews()V

    .line 166
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/TagsView;->taggedKnowledgeEntities:Ljava/util/List;

    move-object/from16 v17, v0

    if-eqz v17, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/TagsView;->taggedKnowledgeEntities:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->isEmpty()Z

    move-result v17

    if-nez v17, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/videos/tagging/TagsView;->videoDisplayWidth:I

    move/from16 v17, v0

    if-eqz v17, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/videos/tagging/TagsView;->videoDisplayHeight:I

    move/from16 v17, v0

    if-eqz v17, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/tagging/TagsView;->getWidth()I

    move-result v17

    if-eqz v17, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/tagging/TagsView;->getHeight()I

    move-result v17

    if-nez v17, :cond_1

    .line 252
    :cond_0
    :goto_0
    return-void

    .line 172
    :cond_1
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 176
    .local v9, "labelViews":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/TagsView$LabelView;>;"
    new-instance v5, Landroid/graphics/RectF;

    const/16 v17, 0x0

    const/16 v18, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/tagging/TagsView;->getWidth()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/tagging/TagsView;->getHeight()I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v5, v0, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 177
    .local v5, "clipBox":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/videos/tagging/TagsView;->videoDisplayWidth:I

    move/from16 v17, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/tagging/TagsView;->getWidth()I

    move-result v18

    sub-int v17, v17, v18

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    const/high16 v18, 0x40000000    # 2.0f

    div-float v17, v17, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/videos/tagging/TagsView;->videoDisplayHeight:I

    move/from16 v18, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/tagging/TagsView;->getHeight()I

    move-result v19

    sub-int v18, v18, v19

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    const/high16 v19, 0x40000000    # 2.0f

    div-float v18, v18, v19

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v5, v0, v1}, Landroid/graphics/RectF;->offset(FF)V

    .line 182
    const/4 v10, 0x1

    .line 184
    .local v10, "nextViewId":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/TagsView;->taggedKnowledgeEntities:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v17

    move/from16 v0, v17

    if-ge v6, v0, :cond_4

    .line 185
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/TagsView;->taggedKnowledgeEntities:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;

    .line 186
    .local v15, "taggedKnowledgeEntity":Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;
    iget-object v7, v15, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;->knowledgeEntity:Lcom/google/android/videos/tagging/KnowledgeEntity;

    .line 187
    .local v7, "knowledgeEntity":Lcom/google/android/videos/tagging/KnowledgeEntity;
    iget-object v0, v15, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;->tagShape:Lcom/google/android/videos/tagging/Tag$TagShape;

    move-object/from16 v17, v0

    if-nez v17, :cond_2

    .line 184
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 191
    :cond_2
    new-instance v14, Lcom/google/android/videos/tagging/TagsView$TagShapeView;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/tagging/TagsView;->getContext()Landroid/content/Context;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v14, v0, v1}, Lcom/google/android/videos/tagging/TagsView$TagShapeView;-><init>(Lcom/google/android/videos/tagging/TagsView;Landroid/content/Context;)V

    .line 192
    .local v14, "tagShapeView":Lcom/google/android/videos/tagging/TagsView$TagShapeView;
    const/4 v8, 0x0

    .line 193
    .local v8, "labelView":Lcom/google/android/videos/tagging/TagsView$LabelView;
    iget-object v0, v7, Lcom/google/android/videos/tagging/KnowledgeEntity;->name:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_3

    .line 194
    new-instance v8, Lcom/google/android/videos/tagging/TagsView$LabelView;

    .end local v8    # "labelView":Lcom/google/android/videos/tagging/TagsView$LabelView;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/tagging/TagsView;->getContext()Landroid/content/Context;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v8, v0, v1}, Lcom/google/android/videos/tagging/TagsView$LabelView;-><init>(Lcom/google/android/videos/tagging/TagsView;Landroid/content/Context;)V

    .line 195
    .restart local v8    # "labelView":Lcom/google/android/videos/tagging/TagsView$LabelView;
    invoke-virtual {v8, v15, v14, v5}, Lcom/google/android/videos/tagging/TagsView$LabelView;->init(Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;Lcom/google/android/videos/tagging/TagsView$TagShapeView;Landroid/graphics/RectF;)V

    .line 196
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lcom/google/android/videos/tagging/TagsView;->updateOverlaps(Lcom/google/android/videos/tagging/TagsView$LabelView;Ljava/util/List;)V

    .line 197
    invoke-interface {v9, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 199
    :cond_3
    invoke-virtual {v14, v15, v8, v5}, Lcom/google/android/videos/tagging/TagsView$TagShapeView;->init(Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;Lcom/google/android/videos/tagging/TagsView$LabelView;Landroid/graphics/RectF;)V

    .line 200
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 201
    add-int/lit8 v11, v10, 0x1

    .end local v10    # "nextViewId":I
    .local v11, "nextViewId":I
    invoke-virtual {v14, v10}, Lcom/google/android/videos/tagging/TagsView$TagShapeView;->setId(I)V

    .line 202
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/videos/tagging/TagsView;->interactionEnabled:Z

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/google/android/videos/tagging/TagsView$TagShapeView;->setFocusable(Z)V

    .line 203
    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Lcom/google/android/videos/tagging/TagsView$TagShapeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 204
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/videos/tagging/TagsView;->interactionEnabled:Z

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/google/android/videos/tagging/TagsView$TagShapeView;->setClickable(Z)V

    .line 205
    iget-object v0, v7, Lcom/google/android/videos/tagging/KnowledgeEntity;->name:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/google/android/videos/tagging/TagsView$TagShapeView;->setContentDescription(Ljava/lang/CharSequence;)V

    move v10, v11

    .end local v11    # "nextViewId":I
    .restart local v10    # "nextViewId":I
    goto :goto_2

    .line 211
    .end local v7    # "knowledgeEntity":Lcom/google/android/videos/tagging/KnowledgeEntity;
    .end local v8    # "labelView":Lcom/google/android/videos/tagging/TagsView$LabelView;
    .end local v14    # "tagShapeView":Lcom/google/android/videos/tagging/TagsView$TagShapeView;
    .end local v15    # "taggedKnowledgeEntity":Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    move-object/from16 v17, v0

    sget-object v18, Lcom/google/android/videos/tagging/TagsView$TagView;->ORDER_BY_AREA:Lcom/google/android/videos/tagging/TagsView$TagViewComparator;

    invoke-static/range {v17 .. v18}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 212
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v13

    .line 213
    .local v13, "tagShapeCount":I
    add-int/lit8 v6, v13, -0x1

    :goto_3
    if-ltz v6, :cond_6

    .line 214
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/videos/tagging/TagsView$TagShapeView;

    .line 215
    .restart local v14    # "tagShapeView":Lcom/google/android/videos/tagging/TagsView$TagShapeView;
    add-int/lit8 v17, v13, -0x1

    sub-int v17, v17, v6

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v14, v1}, Lcom/google/android/videos/tagging/TagsView;->addView(Landroid/view/View;I)V

    .line 216
    invoke-virtual {v14}, Lcom/google/android/videos/tagging/TagsView$TagShapeView;->getLabelView()Lcom/google/android/videos/tagging/TagsView$LabelView;

    move-result-object v8

    .line 217
    .restart local v8    # "labelView":Lcom/google/android/videos/tagging/TagsView$LabelView;
    if-eqz v8, :cond_5

    .line 218
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/google/android/videos/tagging/TagsView;->addView(Landroid/view/View;)V

    .line 219
    invoke-virtual {v8}, Lcom/google/android/videos/tagging/TagsView$LabelView;->setVisibleIfNoVisibleOverlaps()V

    .line 213
    :cond_5
    add-int/lit8 v6, v6, -0x1

    goto :goto_3

    .line 224
    .end local v8    # "labelView":Lcom/google/android/videos/tagging/TagsView$LabelView;
    .end local v14    # "tagShapeView":Lcom/google/android/videos/tagging/TagsView$TagShapeView;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    move-object/from16 v17, v0

    sget-object v18, Lcom/google/android/videos/tagging/TagsView$TagView;->ORDER_BY_CENTER_Y:Lcom/google/android/videos/tagging/TagsView$TagViewComparator;

    invoke-static/range {v17 .. v18}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 225
    const/4 v12, 0x0

    .line 226
    .local v12, "previous":Landroid/view/View;
    const/4 v6, 0x0

    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v17

    move/from16 v0, v17

    if-ge v6, v0, :cond_8

    .line 227
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/view/View;

    .line 228
    .local v16, "view":Landroid/view/View;
    if-eqz v12, :cond_7

    .line 229
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getId()I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/view/View;->setNextFocusDownId(I)V

    .line 230
    invoke-virtual {v12}, Landroid/view/View;->getId()I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Landroid/view/View;->setNextFocusUpId(I)V

    .line 232
    :cond_7
    move-object/from16 v12, v16

    .line 226
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 236
    .end local v16    # "view":Landroid/view/View;
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    move-object/from16 v17, v0

    sget-object v18, Lcom/google/android/videos/tagging/TagsView$TagView;->ORDER_BY_CENTER_X:Lcom/google/android/videos/tagging/TagsView$TagViewComparator;

    invoke-static/range {v17 .. v18}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 237
    const/4 v12, 0x0

    .line 238
    const/4 v4, 0x0

    .line 239
    .local v4, "animateOrder":I
    const/4 v6, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v17

    move/from16 v0, v17

    if-ge v6, v0, :cond_a

    .line 240
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/google/android/videos/tagging/TagsView$TagView;

    .line 241
    .local v16, "view":Lcom/google/android/videos/tagging/TagsView$TagView;
    if-eqz v12, :cond_9

    .line 242
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/videos/tagging/TagsView$TagView;->getId()I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 243
    invoke-virtual {v12}, Landroid/view/View;->getId()I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Lcom/google/android/videos/tagging/TagsView$TagView;->setNextFocusLeftId(I)V

    .line 245
    :cond_9
    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lcom/google/android/videos/tagging/TagsView$TagView;->startAppearAnimation(I)V

    .line 246
    move-object/from16 v12, v16

    .line 239
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    .line 250
    .end local v16    # "view":Lcom/google/android/videos/tagging/TagsView$TagView;
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v9}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 251
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    move-object/from16 v17, v0

    sget-object v18, Lcom/google/android/videos/tagging/TagsView$TagView;->ORDER_BY_AREA:Lcom/google/android/videos/tagging/TagsView$TagViewComparator;

    invoke-static/range {v17 .. v18}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto/16 :goto_0
.end method

.method private setActivatedView(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 407
    iget-object v0, p0, Lcom/google/android/videos/tagging/TagsView;->activatedView:Landroid/view/View;

    if-eq p1, v0, :cond_1

    .line 408
    iget-object v0, p0, Lcom/google/android/videos/tagging/TagsView;->activatedView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 409
    iget-object v0, p0, Lcom/google/android/videos/tagging/TagsView;->activatedView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setActivated(Z)V

    .line 411
    :cond_0
    iput-object p1, p0, Lcom/google/android/videos/tagging/TagsView;->activatedView:Landroid/view/View;

    .line 412
    if-eqz p1, :cond_1

    .line 413
    iget-object v0, p0, Lcom/google/android/videos/tagging/TagsView;->activatedView:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setActivated(Z)V

    .line 416
    :cond_1
    return-void
.end method

.method private updateOverlaps(Lcom/google/android/videos/tagging/TagsView$LabelView;Ljava/util/List;)V
    .locals 3
    .param p1, "newLabelView"    # Lcom/google/android/videos/tagging/TagsView$LabelView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/tagging/TagsView$LabelView;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/TagsView$LabelView;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 255
    .local p2, "existingLabelViews":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/TagsView$LabelView;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 256
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/tagging/TagsView$LabelView;

    .line 257
    .local v1, "oldLabelView":Lcom/google/android/videos/tagging/TagsView$LabelView;
    invoke-virtual {p1, v1}, Lcom/google/android/videos/tagging/TagsView$LabelView;->intersects(Lcom/google/android/videos/tagging/TagsView$LabelView;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 258
    invoke-virtual {p1, v1}, Lcom/google/android/videos/tagging/TagsView$LabelView;->addOverlappingLabelView(Lcom/google/android/videos/tagging/TagsView$LabelView;)V

    .line 259
    invoke-virtual {v1, p1}, Lcom/google/android/videos/tagging/TagsView$LabelView;->addOverlappingLabelView(Lcom/google/android/videos/tagging/TagsView$LabelView;)V

    .line 255
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 262
    .end local v1    # "oldLabelView":Lcom/google/android/videos/tagging/TagsView$LabelView;
    :cond_1
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 124
    invoke-direct {p0}, Lcom/google/android/videos/tagging/TagsView;->clearViews()V

    .line 125
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/tagging/TagsView;->taggedKnowledgeEntities:Ljava/util/List;

    .line 126
    return-void
.end method

.method public clearSpotlight()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 310
    iget-boolean v2, p0, Lcom/google/android/videos/tagging/TagsView;->inSpotlightMode:Z

    if-eqz v2, :cond_1

    .line 311
    iput-boolean v3, p0, Lcom/google/android/videos/tagging/TagsView;->inSpotlightMode:Z

    .line 312
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 313
    iget-object v2, p0, Lcom/google/android/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/tagging/TagsView$TagView;

    .line 314
    .local v1, "tagView":Lcom/google/android/videos/tagging/TagsView$TagView;
    instance-of v2, v1, Lcom/google/android/videos/tagging/TagsView$LabelView;

    if-eqz v2, :cond_0

    .line 315
    check-cast v1, Lcom/google/android/videos/tagging/TagsView$LabelView;

    .end local v1    # "tagView":Lcom/google/android/videos/tagging/TagsView$TagView;
    invoke-virtual {v1}, Lcom/google/android/videos/tagging/TagsView$LabelView;->setVisibleIfNoVisibleOverlaps()V

    .line 312
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 317
    .restart local v1    # "tagView":Lcom/google/android/videos/tagging/TagsView$TagView;
    :cond_0
    invoke-virtual {v1, v3}, Lcom/google/android/videos/tagging/TagsView$TagView;->setVisibility(I)V

    goto :goto_1

    .line 321
    .end local v0    # "i":I
    .end local v1    # "tagView":Lcom/google/android/videos/tagging/TagsView$TagView;
    :cond_1
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 328
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/TagsView;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/tagging/TagsView;->onTagClickListener:Lcom/google/android/videos/tagging/TagsView$OnTagClickListener;

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/google/android/videos/tagging/TagsView$TagView;

    if-eqz v0, :cond_0

    .line 329
    iget-object v1, p0, Lcom/google/android/videos/tagging/TagsView;->onTagClickListener:Lcom/google/android/videos/tagging/TagsView$OnTagClickListener;

    move-object v0, p1

    check-cast v0, Lcom/google/android/videos/tagging/TagsView$TagView;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/TagsView$TagView;->getTaggedKnowledgeEntity()Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/videos/tagging/TagsView$OnTagClickListener;->onTagClick(Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;)V

    .line 330
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setActivated(Z)V

    .line 332
    :cond_0
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 354
    const/4 v0, 0x1

    return v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 152
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 153
    iget-object v0, p0, Lcom/google/android/videos/tagging/TagsView;->taggedKnowledgeEntities:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 155
    new-instance v0, Lcom/google/android/videos/tagging/TagsView$1;

    invoke-direct {v0, p0}, Lcom/google/android/videos/tagging/TagsView$1;-><init>(Lcom/google/android/videos/tagging/TagsView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/TagsView;->post(Ljava/lang/Runnable;)Z

    .line 162
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 359
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 360
    .local v0, "action":I
    const/4 v11, 0x3

    if-ne v0, v11, :cond_1

    .line 361
    invoke-direct {p0, v8}, Lcom/google/android/videos/tagging/TagsView;->setActivatedView(Landroid/view/View;)V

    .line 403
    :cond_0
    :goto_0
    return v10

    .line 364
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 365
    .local v1, "eventX":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 366
    .local v2, "eventY":F
    const/4 v5, 0x0

    .line 367
    .local v5, "touchedView":Lcom/google/android/videos/tagging/TagsView$TagView;
    iget v11, p0, Lcom/google/android/videos/tagging/TagsView;->videoDisplayWidth:I

    if-eqz v11, :cond_2

    iget v11, p0, Lcom/google/android/videos/tagging/TagsView;->videoDisplayHeight:I

    if-eqz v11, :cond_2

    iget-object v11, p0, Lcom/google/android/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_2

    .line 369
    iget v11, p0, Lcom/google/android/videos/tagging/TagsView;->videoDisplayWidth:I

    invoke-virtual {p0}, Lcom/google/android/videos/tagging/TagsView;->getWidth()I

    move-result v12

    sub-int/2addr v11, v12

    div-int/lit8 v11, v11, 0x2

    int-to-float v11, v11

    add-float v6, v1, v11

    .line 370
    .local v6, "x":F
    iget v11, p0, Lcom/google/android/videos/tagging/TagsView;->videoDisplayHeight:I

    invoke-virtual {p0}, Lcom/google/android/videos/tagging/TagsView;->getHeight()I

    move-result v12

    sub-int/2addr v11, v12

    div-int/lit8 v11, v11, 0x2

    int-to-float v11, v11

    add-float v7, v2, v11

    .line 372
    .local v7, "y":F
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    iget-object v11, p0, Lcom/google/android/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    if-ge v3, v11, :cond_2

    .line 373
    iget-object v11, p0, Lcom/google/android/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    invoke-interface {v11, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/videos/tagging/TagsView$TagView;

    .line 374
    .local v4, "tagView":Lcom/google/android/videos/tagging/TagsView$TagView;
    invoke-virtual {v4}, Lcom/google/android/videos/tagging/TagsView$TagView;->getVisibility()I

    move-result v11

    if-nez v11, :cond_5

    iget v11, p0, Lcom/google/android/videos/tagging/TagsView;->tagStrokeWidth:F

    invoke-virtual {v4, v6, v7, v11}, Lcom/google/android/videos/tagging/TagsView$TagView;->hitTest(FFF)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 375
    move-object v5, v4

    .line 380
    .end local v3    # "i":I
    .end local v4    # "tagView":Lcom/google/android/videos/tagging/TagsView$TagView;
    .end local v6    # "x":F
    .end local v7    # "y":F
    :cond_2
    packed-switch v0, :pswitch_data_0

    .line 397
    iget-boolean v8, p0, Lcom/google/android/videos/tagging/TagsView;->moved:Z

    if-nez v8, :cond_4

    .line 398
    iget v8, p0, Lcom/google/android/videos/tagging/TagsView;->downX:F

    sub-float v8, v1, v8

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    iget v11, p0, Lcom/google/android/videos/tagging/TagsView;->touchSlop:I

    int-to-float v11, v11

    cmpl-float v8, v8, v11

    if-gtz v8, :cond_3

    iget v8, p0, Lcom/google/android/videos/tagging/TagsView;->downY:F

    sub-float v8, v2, v8

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    iget v11, p0, Lcom/google/android/videos/tagging/TagsView;->touchSlop:I

    int-to-float v11, v11

    cmpl-float v8, v8, v11

    if-lez v8, :cond_8

    :cond_3
    move v8, v10

    :goto_2
    iput-boolean v8, p0, Lcom/google/android/videos/tagging/TagsView;->moved:Z

    .line 400
    :cond_4
    invoke-direct {p0, v5}, Lcom/google/android/videos/tagging/TagsView;->setActivatedView(Landroid/view/View;)V

    goto :goto_0

    .line 372
    .restart local v3    # "i":I
    .restart local v4    # "tagView":Lcom/google/android/videos/tagging/TagsView$TagView;
    .restart local v6    # "x":F
    .restart local v7    # "y":F
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 382
    .end local v3    # "i":I
    .end local v4    # "tagView":Lcom/google/android/videos/tagging/TagsView$TagView;
    .end local v6    # "x":F
    .end local v7    # "y":F
    :pswitch_0
    invoke-direct {p0, v8}, Lcom/google/android/videos/tagging/TagsView;->setActivatedView(Landroid/view/View;)V

    .line 384
    iget-object v11, p0, Lcom/google/android/videos/tagging/TagsView;->onTagClickListener:Lcom/google/android/videos/tagging/TagsView$OnTagClickListener;

    if-eqz v11, :cond_0

    if-nez v5, :cond_6

    iget-boolean v11, p0, Lcom/google/android/videos/tagging/TagsView;->moved:Z

    if-nez v11, :cond_0

    .line 385
    :cond_6
    invoke-virtual {p0, v9}, Lcom/google/android/videos/tagging/TagsView;->playSoundEffect(I)V

    .line 386
    iget-object v9, p0, Lcom/google/android/videos/tagging/TagsView;->onTagClickListener:Lcom/google/android/videos/tagging/TagsView$OnTagClickListener;

    if-nez v5, :cond_7

    :goto_3
    invoke-interface {v9, v8}, Lcom/google/android/videos/tagging/TagsView$OnTagClickListener;->onTagClick(Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;)V

    goto/16 :goto_0

    :cond_7
    invoke-virtual {v5}, Lcom/google/android/videos/tagging/TagsView$TagView;->getTaggedKnowledgeEntity()Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;

    move-result-object v8

    goto :goto_3

    .line 391
    :pswitch_1
    iput v1, p0, Lcom/google/android/videos/tagging/TagsView;->downX:F

    .line 392
    iput v2, p0, Lcom/google/android/videos/tagging/TagsView;->downY:F

    .line 393
    iput-boolean v9, p0, Lcom/google/android/videos/tagging/TagsView;->moved:Z

    .line 394
    invoke-direct {p0, v5}, Lcom/google/android/videos/tagging/TagsView;->setActivatedView(Landroid/view/View;)V

    goto/16 :goto_0

    :cond_8
    move v8, v9

    .line 398
    goto :goto_2

    .line 380
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setInteractionEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 335
    iget-boolean v2, p0, Lcom/google/android/videos/tagging/TagsView;->interactionEnabled:Z

    if-eq v2, p1, :cond_2

    .line 336
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 337
    iget-object v2, p0, Lcom/google/android/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 338
    .local v0, "child":Landroid/view/View;
    instance-of v2, v0, Lcom/google/android/videos/tagging/TagsView$TagShapeView;

    if-eqz v2, :cond_0

    .line 339
    invoke-virtual {v0, p1}, Landroid/view/View;->setFocusable(Z)V

    .line 340
    invoke-virtual {v0, p1}, Landroid/view/View;->setClickable(Z)V

    .line 336
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 343
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    iput-boolean p1, p0, Lcom/google/android/videos/tagging/TagsView;->interactionEnabled:Z

    .line 345
    .end local v1    # "i":I
    :cond_2
    return-void
.end method

.method public setOnTagClickListener(Lcom/google/android/videos/tagging/TagsView$OnTagClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/videos/tagging/TagsView$OnTagClickListener;

    .prologue
    .line 348
    iput-object p1, p0, Lcom/google/android/videos/tagging/TagsView;->onTagClickListener:Lcom/google/android/videos/tagging/TagsView$OnTagClickListener;

    .line 349
    return-void
.end method

.method public setSpotlight(Lcom/google/android/videos/tagging/KnowledgeEntity;)V
    .locals 3
    .param p1, "knowledgeEntity"    # Lcom/google/android/videos/tagging/KnowledgeEntity;

    .prologue
    .line 295
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 296
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/videos/tagging/TagsView;->inSpotlightMode:Z

    .line 297
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 298
    iget-object v2, p0, Lcom/google/android/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/tagging/TagsView$TagView;

    .line 299
    .local v1, "tagView":Lcom/google/android/videos/tagging/TagsView$TagView;
    instance-of v2, v1, Lcom/google/android/videos/tagging/TagsView$TagShapeView;

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/videos/tagging/TagsView$TagView;->getTaggedKnowledgeEntity()Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;->knowledgeEntity:Lcom/google/android/videos/tagging/KnowledgeEntity;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v1, v2}, Lcom/google/android/videos/tagging/TagsView$TagView;->setVisibility(I)V

    .line 297
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 299
    :cond_0
    const/16 v2, 0x8

    goto :goto_1

    .line 304
    .end local v1    # "tagView":Lcom/google/android/videos/tagging/TagsView$TagView;
    :cond_1
    return-void
.end method

.method public setSpotlight(Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;)Z
    .locals 5
    .param p1, "taggedKnowledgeEntity"    # Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 272
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    iget-object v2, p0, Lcom/google/android/videos/tagging/TagsView;->taggedKnowledgeEntities:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 274
    iput-boolean v4, p0, Lcom/google/android/videos/tagging/TagsView;->inSpotlightMode:Z

    .line 275
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 276
    iget-object v2, p0, Lcom/google/android/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/tagging/TagsView$TagView;

    .line 277
    .local v1, "tagView":Lcom/google/android/videos/tagging/TagsView$TagView;
    instance-of v2, v1, Lcom/google/android/videos/tagging/TagsView$TagShapeView;

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/videos/tagging/TagsView$TagView;->getTaggedKnowledgeEntity()Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v3

    :goto_1
    invoke-virtual {v1, v2}, Lcom/google/android/videos/tagging/TagsView$TagView;->setVisibility(I)V

    .line 275
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 277
    :cond_0
    const/16 v2, 0x8

    goto :goto_1

    .end local v1    # "tagView":Lcom/google/android/videos/tagging/TagsView$TagView;
    :cond_1
    move v3, v4

    .line 284
    .end local v0    # "i":I
    :cond_2
    return v3
.end method

.method public show(Ljava/util/List;II)V
    .locals 1
    .param p2, "videoDisplayWidth"    # I
    .param p3, "videoDisplayHeight"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 143
    .local p1, "taggedKnowledgeEntities":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;>;"
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/videos/tagging/TagsView;->taggedKnowledgeEntities:Ljava/util/List;

    .line 145
    iput p2, p0, Lcom/google/android/videos/tagging/TagsView;->videoDisplayWidth:I

    .line 146
    iput p3, p0, Lcom/google/android/videos/tagging/TagsView;->videoDisplayHeight:I

    .line 147
    invoke-direct {p0}, Lcom/google/android/videos/tagging/TagsView;->createChildren()V

    .line 148
    return-void

    .line 143
    :cond_0
    invoke-static {p1}, Lcom/google/android/videos/utils/CollectionUtil;->immutableCopyOf(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
