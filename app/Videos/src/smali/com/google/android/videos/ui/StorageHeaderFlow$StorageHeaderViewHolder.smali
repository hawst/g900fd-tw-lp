.class Lcom/google/android/videos/ui/StorageHeaderFlow$StorageHeaderViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "StorageHeaderFlow.java"

# interfaces
.implements Lcom/google/android/videos/flow/Bindable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/StorageHeaderFlow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "StorageHeaderViewHolder"
.end annotation


# instance fields
.field private final freeSpaceText:Landroid/widget/TextView;

.field private final storageBar:Lcom/google/android/videos/ui/RatioBar;

.field final synthetic this$0:Lcom/google/android/videos/ui/StorageHeaderFlow;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/ui/StorageHeaderFlow;Landroid/view/View;)V
    .locals 1
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/google/android/videos/ui/StorageHeaderFlow$StorageHeaderViewHolder;->this$0:Lcom/google/android/videos/ui/StorageHeaderFlow;

    .line 45
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 47
    const v0, 0x7f0f0177

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/RatioBar;

    iput-object v0, p0, Lcom/google/android/videos/ui/StorageHeaderFlow$StorageHeaderViewHolder;->storageBar:Lcom/google/android/videos/ui/RatioBar;

    .line 48
    const v0, 0x7f0f0179

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/StorageHeaderFlow$StorageHeaderViewHolder;->freeSpaceText:Landroid/widget/TextView;

    .line 49
    return-void
.end method


# virtual methods
.method public bind()V
    .locals 24

    .prologue
    .line 53
    const-wide/16 v4, 0x0

    .line 54
    .local v4, "availableBytes":J
    const-wide/16 v14, 0x0

    .line 56
    .local v14, "totalBytes":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/StorageHeaderFlow$StorageHeaderViewHolder;->itemView:Landroid/view/View;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/google/android/videos/utils/OfflineUtil;->getSupportedRootFilesDir(Landroid/content/Context;)[Ljava/io/File;

    move-result-object v2

    .local v2, "arr$":[Ljava/io/File;
    array-length v9, v2

    .local v9, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    :goto_0
    if-ge v8, v9, :cond_1

    aget-object v12, v2, v8

    .line 57
    .local v12, "rootDir":Ljava/io/File;
    if-eqz v12, :cond_0

    .line 58
    invoke-virtual {v12}, Ljava/io/File;->getUsableSpace()J

    move-result-wide v16

    add-long v4, v4, v16

    .line 59
    invoke-virtual {v12}, Ljava/io/File;->getTotalSpace()J

    move-result-wide v16

    add-long v14, v14, v16

    .line 56
    :cond_0
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 64
    .end local v12    # "rootDir":Ljava/io/File;
    :cond_1
    sub-long v16, v14, v4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/StorageHeaderFlow$StorageHeaderViewHolder;->this$0:Lcom/google/android/videos/ui/StorageHeaderFlow;

    move-object/from16 v18, v0

    # getter for: Lcom/google/android/videos/ui/StorageHeaderFlow;->downloadedBytes:J
    invoke-static/range {v18 .. v18}, Lcom/google/android/videos/ui/StorageHeaderFlow;->access$000(Lcom/google/android/videos/ui/StorageHeaderFlow;)J

    move-result-wide v18

    sub-long v10, v16, v18

    .line 67
    .local v10, "nonMoviesUsedBytes":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/StorageHeaderFlow$StorageHeaderViewHolder;->this$0:Lcom/google/android/videos/ui/StorageHeaderFlow;

    move-object/from16 v16, v0

    # getter for: Lcom/google/android/videos/ui/StorageHeaderFlow;->requiredBytes:J
    invoke-static/range {v16 .. v16}, Lcom/google/android/videos/ui/StorageHeaderFlow;->access$100(Lcom/google/android/videos/ui/StorageHeaderFlow;)J

    move-result-wide v16

    sub-long v16, v14, v16

    sub-long v6, v16, v10

    .line 69
    .local v6, "eventualAvailableBytes":J
    long-to-float v13, v14

    .line 70
    .local v13, "total":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/StorageHeaderFlow$StorageHeaderViewHolder;->storageBar:Lcom/google/android/videos/ui/RatioBar;

    move-object/from16 v16, v0

    const/16 v17, 0x4

    move/from16 v0, v17

    new-array v0, v0, [F

    move-object/from16 v17, v0

    const/16 v18, 0x0

    long-to-float v0, v10

    move/from16 v19, v0

    div-float v19, v19, v13

    aput v19, v17, v18

    const/16 v18, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/StorageHeaderFlow$StorageHeaderViewHolder;->this$0:Lcom/google/android/videos/ui/StorageHeaderFlow;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/videos/ui/StorageHeaderFlow;->downloadedBytes:J
    invoke-static/range {v19 .. v19}, Lcom/google/android/videos/ui/StorageHeaderFlow;->access$000(Lcom/google/android/videos/ui/StorageHeaderFlow;)J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-float v0, v0

    move/from16 v19, v0

    div-float v19, v19, v13

    aput v19, v17, v18

    const/16 v18, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/StorageHeaderFlow$StorageHeaderViewHolder;->this$0:Lcom/google/android/videos/ui/StorageHeaderFlow;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/videos/ui/StorageHeaderFlow;->requiredBytes:J
    invoke-static/range {v19 .. v19}, Lcom/google/android/videos/ui/StorageHeaderFlow;->access$100(Lcom/google/android/videos/ui/StorageHeaderFlow;)J

    move-result-wide v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/StorageHeaderFlow$StorageHeaderViewHolder;->this$0:Lcom/google/android/videos/ui/StorageHeaderFlow;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/videos/ui/StorageHeaderFlow;->downloadedBytes:J
    invoke-static/range {v19 .. v19}, Lcom/google/android/videos/ui/StorageHeaderFlow;->access$000(Lcom/google/android/videos/ui/StorageHeaderFlow;)J

    move-result-wide v22

    sub-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-float v0, v0

    move/from16 v19, v0

    div-float v19, v19, v13

    aput v19, v17, v18

    const/16 v18, 0x3

    long-to-float v0, v6

    move/from16 v19, v0

    div-float v19, v19, v13

    aput v19, v17, v18

    invoke-virtual/range {v16 .. v17}, Lcom/google/android/videos/ui/RatioBar;->setRatios([F)V

    .line 76
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/StorageHeaderFlow$StorageHeaderViewHolder;->itemView:Landroid/view/View;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v0, v6, v7}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    .line 79
    .local v3, "freeSpaceString":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/StorageHeaderFlow$StorageHeaderViewHolder;->freeSpaceText:Landroid/widget/TextView;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/StorageHeaderFlow$StorageHeaderViewHolder;->itemView:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f0b025b

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v3, v19, v20

    invoke-virtual/range {v17 .. v19}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    return-void
.end method
