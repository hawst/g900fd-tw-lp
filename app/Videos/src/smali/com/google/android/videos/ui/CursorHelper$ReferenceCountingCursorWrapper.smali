.class Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;
.super Landroid/database/CursorWrapper;
.source "CursorHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/CursorHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ReferenceCountingCursorWrapper"
.end annotation


# instance fields
.field private referenceCount:I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 239
    invoke-direct {p0, p1}, Landroid/database/CursorWrapper;-><init>(Landroid/database/Cursor;)V

    .line 240
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;->referenceCount:I

    .line 241
    return-void
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 1

    .prologue
    .line 249
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;->referenceCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;->referenceCount:I

    if-nez v0, :cond_0

    .line 250
    invoke-super {p0}, Landroid/database/CursorWrapper;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 252
    :cond_0
    monitor-exit p0

    return-void

    .line 249
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized increment()V
    .locals 1

    .prologue
    .line 244
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;->referenceCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;->referenceCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 245
    monitor-exit p0

    return-void

    .line 244
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
