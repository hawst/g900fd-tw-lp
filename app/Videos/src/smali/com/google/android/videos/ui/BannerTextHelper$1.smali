.class Lcom/google/android/videos/ui/BannerTextHelper$1;
.super Ljava/lang/Object;
.source "BannerTextHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/ui/BannerTextHelper;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/ui/BannerTextHelper;


# direct methods
.method constructor <init>(Lcom/google/android/videos/ui/BannerTextHelper;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/google/android/videos/ui/BannerTextHelper$1;->this$0:Lcom/google/android/videos/ui/BannerTextHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 83
    iget-object v0, p0, Lcom/google/android/videos/ui/BannerTextHelper$1;->this$0:Lcom/google/android/videos/ui/BannerTextHelper;

    # getter for: Lcom/google/android/videos/ui/BannerTextHelper;->turningOffDownloadedOnly:Z
    invoke-static {v0}, Lcom/google/android/videos/ui/BannerTextHelper;->access$000(Lcom/google/android/videos/ui/BannerTextHelper;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/google/android/videos/ui/BannerTextHelper$1;->this$0:Lcom/google/android/videos/ui/BannerTextHelper;

    # setter for: Lcom/google/android/videos/ui/BannerTextHelper;->turningOffDownloadedOnly:Z
    invoke-static {v0, v1}, Lcom/google/android/videos/ui/BannerTextHelper;->access$002(Lcom/google/android/videos/ui/BannerTextHelper;Z)Z

    .line 85
    iget-object v0, p0, Lcom/google/android/videos/ui/BannerTextHelper$1;->this$0:Lcom/google/android/videos/ui/BannerTextHelper;

    # getter for: Lcom/google/android/videos/ui/BannerTextHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;
    invoke-static {v0}, Lcom/google/android/videos/ui/BannerTextHelper;->access$100(Lcom/google/android/videos/ui/BannerTextHelper;)Lcom/google/android/videos/utils/DownloadedOnlyManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->setDownloadedOnly(Z)V

    .line 87
    :cond_0
    return-void
.end method
