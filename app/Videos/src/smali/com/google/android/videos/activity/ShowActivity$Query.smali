.class public interface abstract Lcom/google/android/videos/activity/ShowActivity$Query;
.super Ljava/lang/Object;
.source "ShowActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/activity/ShowActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Query"
.end annotation


# static fields
.field public static final COLUMNS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 954
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "season_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "shows_title"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "shows_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "broadcasters"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/videos/activity/ShowActivity$Query;->COLUMNS:[Ljava/lang/String;

    return-void
.end method
