.class public Lcom/google/android/videos/api/GcmRegisterRequest;
.super Lcom/google/android/videos/async/Request;
.source "GcmRegisterRequest.java"


# instance fields
.field public final registrationId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "registrationId"    # Ljava/lang/String;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/google/android/videos/async/Request;-><init>(Ljava/lang/String;)V

    .line 14
    iput-object p2, p0, Lcom/google/android/videos/api/GcmRegisterRequest;->registrationId:Ljava/lang/String;

    .line 15
    return-void
.end method
