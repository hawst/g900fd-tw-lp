.class public Lcom/google/android/videos/ui/FocusedImageView;
.super Landroid/widget/ImageView;
.source "FocusedImageView.java"


# instance fields
.field private canReduce:Z

.field private final focusPoint:Landroid/graphics/PointF;

.field private final matrix:Landroid/graphics/Matrix;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/ui/FocusedImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/videos/ui/FocusedImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v4, 0x0

    .line 36
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    new-instance v1, Landroid/graphics/PointF;

    const/high16 v2, 0x3f000000    # 0.5f

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v1, p0, Lcom/google/android/videos/ui/FocusedImageView;->focusPoint:Landroid/graphics/PointF;

    .line 23
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    iput-object v1, p0, Lcom/google/android/videos/ui/FocusedImageView;->matrix:Landroid/graphics/Matrix;

    .line 25
    iput-boolean v4, p0, Lcom/google/android/videos/ui/FocusedImageView;->canReduce:Z

    .line 37
    sget-object v1, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-super {p0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 38
    sget-object v1, Lcom/google/android/videos/R$styleable;->FocusedImageView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 39
    .local v0, "a":Landroid/content/res/TypedArray;
    iget-object v1, p0, Lcom/google/android/videos/ui/FocusedImageView;->focusPoint:Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/google/android/videos/ui/FocusedImageView;->focusPoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    invoke-virtual {v0, v4, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, v1, Landroid/graphics/PointF;->x:F

    .line 40
    iget-object v1, p0, Lcom/google/android/videos/ui/FocusedImageView;->focusPoint:Landroid/graphics/PointF;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/videos/ui/FocusedImageView;->focusPoint:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, v1, Landroid/graphics/PointF;->y:F

    .line 41
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/videos/ui/FocusedImageView;->canReduce:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/videos/ui/FocusedImageView;->canReduce:Z

    .line 42
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 43
    return-void
.end method

.method private updateMatrix()V
    .locals 17

    .prologue
    .line 105
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/ui/FocusedImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 106
    .local v7, "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/ui/FocusedImageView;->getWidth()I

    move-result v15

    int-to-float v10, v15

    .line 107
    .local v10, "dstWidth":F
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/ui/FocusedImageView;->getHeight()I

    move-result v15

    int-to-float v8, v15

    .line 108
    .local v8, "dstHeight":F
    if-eqz v7, :cond_0

    const/4 v15, 0x0

    cmpl-float v15, v10, v15

    if-eqz v15, :cond_0

    const/4 v15, 0x0

    cmpl-float v15, v8, v15

    if-nez v15, :cond_1

    .line 144
    :cond_0
    :goto_0
    return-void

    .line 112
    :cond_1
    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v15

    int-to-float v14, v15

    .line 113
    .local v14, "srcWidth":F
    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v15

    int-to-float v12, v15

    .line 114
    .local v12, "srcHeight":F
    const/4 v15, 0x0

    cmpg-float v15, v14, v15

    if-lez v15, :cond_2

    const/4 v15, 0x0

    cmpg-float v15, v12, v15

    if-gtz v15, :cond_3

    .line 117
    :cond_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/ui/FocusedImageView;->matrix:Landroid/graphics/Matrix;

    invoke-virtual {v15}, Landroid/graphics/Matrix;->reset()V

    .line 143
    :goto_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/ui/FocusedImageView;->matrix:Landroid/graphics/Matrix;

    move-object/from16 v0, p0

    invoke-super {v0, v15}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0

    .line 120
    :cond_3
    div-float v15, v10, v14

    div-float v16, v8, v12

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->max(FF)F

    move-result v11

    .line 123
    .local v11, "scale":F
    const/high16 v15, 0x3f800000    # 1.0f

    cmpg-float v15, v11, v15

    if-gez v15, :cond_4

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/google/android/videos/ui/FocusedImageView;->canReduce:Z

    if-nez v15, :cond_4

    .line 124
    const/high16 v11, 0x3f800000    # 1.0f

    .line 128
    :cond_4
    div-float v15, v10, v11

    sub-float v15, v14, v15

    const/16 v16, 0x0

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->max(FF)F

    move-result v5

    .line 129
    .local v5, "cropX":F
    div-float v15, v8, v11

    sub-float v15, v12, v15

    const/16 v16, 0x0

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->max(FF)F

    move-result v6

    .line 132
    .local v6, "cropY":F
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/ui/FocusedImageView;->focusPoint:Landroid/graphics/PointF;

    iget v15, v15, Landroid/graphics/PointF;->x:F

    mul-float v2, v5, v15

    .line 133
    .local v2, "cropL":F
    sub-float v3, v5, v2

    .line 134
    .local v3, "cropR":F
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/ui/FocusedImageView;->focusPoint:Landroid/graphics/PointF;

    iget v15, v15, Landroid/graphics/PointF;->y:F

    mul-float v4, v6, v15

    .line 135
    .local v4, "cropT":F
    sub-float v1, v6, v4

    .line 138
    .local v1, "cropB":F
    new-instance v13, Landroid/graphics/RectF;

    sub-float v15, v14, v3

    sub-float v16, v12, v1

    move/from16 v0, v16

    invoke-direct {v13, v2, v4, v15, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 139
    .local v13, "srcRect":Landroid/graphics/RectF;
    new-instance v9, Landroid/graphics/RectF;

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-direct {v9, v15, v0, v10, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 141
    .local v9, "dstRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/ui/FocusedImageView;->matrix:Landroid/graphics/Matrix;

    sget-object v16, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    move-object/from16 v0, v16

    invoke-virtual {v15, v13, v9, v0}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    goto :goto_1
.end method


# virtual methods
.method public canReduce()Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/google/android/videos/ui/FocusedImageView;->canReduce:Z

    return v0
.end method

.method public getFocusPoint()Landroid/graphics/PointF;
    .locals 3

    .prologue
    .line 60
    new-instance v0, Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/google/android/videos/ui/FocusedImageView;->focusPoint:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/google/android/videos/ui/FocusedImageView;->focusPoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 0
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 100
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ImageView;->onSizeChanged(IIII)V

    .line 101
    invoke-direct {p0}, Lcom/google/android/videos/ui/FocusedImageView;->updateMatrix()V

    .line 102
    return-void
.end method

.method public setCanReduce(Z)V
    .locals 0
    .param p1, "canReduce"    # Z

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/google/android/videos/ui/FocusedImageView;->canReduce:Z

    .line 71
    invoke-direct {p0}, Lcom/google/android/videos/ui/FocusedImageView;->updateMatrix()V

    .line 72
    return-void
.end method

.method public setFocusPoint(Landroid/graphics/PointF;)V
    .locals 1
    .param p1, "focusPoint"    # Landroid/graphics/PointF;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/videos/ui/FocusedImageView;->focusPoint:Landroid/graphics/PointF;

    invoke-virtual {v0, p1}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 52
    invoke-direct {p0}, Lcom/google/android/videos/ui/FocusedImageView;->updateMatrix()V

    .line 53
    return-void
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 94
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 95
    invoke-direct {p0}, Lcom/google/android/videos/ui/FocusedImageView;->updateMatrix()V

    .line 96
    return-void
.end method

.method public setImageMatrix(Landroid/graphics/Matrix;)V
    .locals 0
    .param p1, "matrix"    # Landroid/graphics/Matrix;

    .prologue
    .line 90
    return-void
.end method

.method public setScaleType(Landroid/widget/ImageView$ScaleType;)V
    .locals 0
    .param p1, "scaleType"    # Landroid/widget/ImageView$ScaleType;

    .prologue
    .line 85
    return-void
.end method
