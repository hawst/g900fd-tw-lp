.class final Lcom/google/android/videos/store/StoryboardClient$7;
.super Lcom/google/android/videos/bitmap/BitmapCachingRequester;
.source "StoryboardClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/store/StoryboardClient;->createImageRequester(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/AbstractFileStore;Lcom/google/android/videos/bitmap/BitmapLruCache;Z)Lcom/google/android/videos/async/Requester;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/bitmap/BitmapCachingRequester",
        "<",
        "Landroid/util/Pair",
        "<",
        "Lcom/google/android/videos/store/StoryboardImageRequest;",
        "Ljava/lang/Integer;",
        ">;>;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/bitmap/BitmapLruCache;)V
    .locals 0
    .param p2, "x1"    # Lcom/google/android/videos/bitmap/BitmapLruCache;

    .prologue
    .line 186
    .local p1, "x0":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/util/Pair<Lcom/google/android/videos/store/StoryboardImageRequest;Ljava/lang/Integer;>;Landroid/graphics/Bitmap;>;"
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/bitmap/BitmapCachingRequester;-><init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/bitmap/BitmapLruCache;)V

    return-void
.end method


# virtual methods
.method public toCacheKey(Landroid/util/Pair;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/videos/store/StoryboardImageRequest;",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 189
    .local p1, "request":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/videos/store/StoryboardImageRequest;Ljava/lang/Integer;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/videos/store/StoryboardImageRequest;

    iget-object v0, v0, Lcom/google/android/videos/store/StoryboardImageRequest;->uniqueKey:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".gb"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toCacheKey(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 186
    check-cast p1, Landroid/util/Pair;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/store/StoryboardClient$7;->toCacheKey(Landroid/util/Pair;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
