.class public Lcom/google/android/videos/ui/ShareHelper;
.super Ljava/lang/Object;
.source "ShareHelper.java"

# interfaces
.implements Lcom/google/android/videos/flow/ViewHolderCreator;
.implements Lcom/google/android/videos/ui/SharePanelView$ShareListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/flow/ViewHolderCreator",
        "<",
        "Lcom/google/android/videos/ui/SharePanelHolder;",
        ">;",
        "Lcom/google/android/videos/ui/SharePanelView$ShareListener;"
    }
.end annotation


# instance fields
.field private final account:Ljava/lang/String;

.field private final context:Landroid/content/Context;

.field private final plusOneUrl:Landroid/net/Uri;

.field private final shareUrl:Landroid/net/Uri;

.field private title:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "shareUrl"    # Landroid/net/Uri;
    .param p3, "plusOneUrl"    # Landroid/net/Uri;
    .param p4, "account"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/videos/ui/ShareHelper;->title:Ljava/lang/String;

    .line 27
    iput-object p1, p0, Lcom/google/android/videos/ui/ShareHelper;->context:Landroid/content/Context;

    .line 28
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/videos/ui/ShareHelper;->shareUrl:Landroid/net/Uri;

    .line 29
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/videos/ui/ShareHelper;->plusOneUrl:Landroid/net/Uri;

    .line 30
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/videos/ui/ShareHelper;->account:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public static createShareHelper(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/videos/ui/ShareHelper;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "shareUrl"    # Landroid/net/Uri;
    .param p2, "plusOneUrl"    # Landroid/net/Uri;
    .param p3, "account"    # Ljava/lang/String;

    .prologue
    .line 35
    new-instance v0, Lcom/google/android/videos/ui/ShareHelper;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/videos/ui/ShareHelper;-><init>(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;)V

    return-object v0
.end method

.method private createShareTextIntent()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 62
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 63
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 64
    const-string v1, "android.intent.extra.TEXT"

    iget-object v2, p0, Lcom/google/android/videos/ui/ShareHelper;->shareUrl:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 65
    const-string v1, "android.intent.extra.SUBJECT"

    invoke-direct {p0}, Lcom/google/android/videos/ui/ShareHelper;->getSubject()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 66
    iget-object v1, p0, Lcom/google/android/videos/ui/ShareHelper;->title:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method private createViewHolderFromView(Lcom/google/android/videos/ui/SharePanelView;)Lcom/google/android/videos/ui/SharePanelHolder;
    .locals 2
    .param p1, "view"    # Lcom/google/android/videos/ui/SharePanelView;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/videos/ui/ShareHelper;->plusOneUrl:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/videos/ui/ShareHelper;->account:Ljava/lang/String;

    invoke-static {p1, v0, v1, p0}, Lcom/google/android/videos/ui/SharePanelHolder;->createSharePanelHolder(Lcom/google/android/videos/ui/SharePanelView;Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/videos/ui/SharePanelView$ShareListener;)Lcom/google/android/videos/ui/SharePanelHolder;

    move-result-object v0

    return-object v0
.end method

.method public static getMainLayout()I
    .locals 1

    .prologue
    .line 39
    invoke-static {}, Lcom/google/android/videos/ui/SharePanelView;->getMainLayout()I

    move-result v0

    return v0
.end method

.method private getSubject()Ljava/lang/String;
    .locals 5

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/videos/ui/ShareHelper;->context:Landroid/content/Context;

    const v1, 0x7f0b0256

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/videos/ui/ShareHelper;->title:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic createViewHolder(ILandroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1
    .param p1, "x0"    # I
    .param p2, "x1"    # Landroid/view/ViewGroup;

    .prologue
    .line 16
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/ShareHelper;->createViewHolder(ILandroid/view/ViewGroup;)Lcom/google/android/videos/ui/SharePanelHolder;

    move-result-object v0

    return-object v0
.end method

.method public createViewHolder(ILandroid/view/ViewGroup;)Lcom/google/android/videos/ui/SharePanelHolder;
    .locals 2
    .param p1, "viewType"    # I
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 44
    invoke-static {p2}, Lcom/google/android/videos/ui/SharePanelView;->inflate(Landroid/view/ViewGroup;)Lcom/google/android/videos/ui/SharePanelView;

    move-result-object v0

    .line 45
    .local v0, "view":Lcom/google/android/videos/ui/SharePanelView;
    invoke-direct {p0, v0}, Lcom/google/android/videos/ui/ShareHelper;->createViewHolderFromView(Lcom/google/android/videos/ui/SharePanelView;)Lcom/google/android/videos/ui/SharePanelHolder;

    move-result-object v1

    return-object v1
.end method

.method public onShareClick()V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/videos/ui/ShareHelper;->context:Landroid/content/Context;

    invoke-direct {p0}, Lcom/google/android/videos/ui/ShareHelper;->createShareTextIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 55
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 58
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/videos/ui/ShareHelper;->title:Ljava/lang/String;

    .line 59
    return-void
.end method
