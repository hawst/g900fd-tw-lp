.class Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastConnectionFailedListener;
.super Ljava/lang/Object;
.source "CastV2RemoteControl.java"

# interfaces
.implements Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/cast/v2/CastV2RemoteControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CastConnectionFailedListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)V
    .locals 0

    .prologue
    .line 1074
    iput-object p1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastConnectionFailedListener;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl;
    .param p2, "x1"    # Lcom/google/android/videos/cast/v2/CastV2RemoteControl$1;

    .prologue
    .line 1074
    invoke-direct {p0, p1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastConnectionFailedListener;-><init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)V

    return-void
.end method


# virtual methods
.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 2
    .param p1, "result"    # Lcom/google/android/gms/common/ConnectionResult;

    .prologue
    .line 1077
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cannot connect to GMS, error code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 1085
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$CastConnectionFailedListener;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->disconnect(Z)V
    invoke-static {v0, v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$1800(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Z)V

    .line 1086
    return-void
.end method
