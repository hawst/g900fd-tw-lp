.class public Lcom/google/android/videos/adapter/WatchNowDataSource;
.super Lcom/google/android/videos/adapter/AbstractDataSource;
.source "WatchNowDataSource.java"

# interfaces
.implements Lcom/google/android/videos/adapter/VideosDataSource;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/adapter/WatchNowDataSource$TransportControl;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/adapter/AbstractDataSource",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/google/android/videos/adapter/VideosDataSource;"
    }
.end annotation


# instance fields
.field private final episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

.field private final moviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

.field private final remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

.field private final transportControl:Lcom/google/android/videos/adapter/WatchNowDataSource$TransportControl;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/remote/RemoteTracker;Lcom/google/android/videos/adapter/MoviesDataSource;Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;)V
    .locals 2
    .param p1, "remoteTracker"    # Lcom/google/android/videos/remote/RemoteTracker;
    .param p2, "moviesDataSource"    # Lcom/google/android/videos/adapter/MoviesDataSource;
    .param p3, "episodesDataSource"    # Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/videos/adapter/AbstractDataSource;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    .line 31
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/adapter/MoviesDataSource;

    iput-object v1, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->moviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    .line 32
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    iput-object v1, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    .line 33
    new-instance v1, Lcom/google/android/videos/adapter/WatchNowDataSource$TransportControl;

    invoke-direct {v1, p0, p1}, Lcom/google/android/videos/adapter/WatchNowDataSource$TransportControl;-><init>(Lcom/google/android/videos/adapter/WatchNowDataSource;Lcom/google/android/videos/remote/RemoteTracker;)V

    iput-object v1, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->transportControl:Lcom/google/android/videos/adapter/WatchNowDataSource$TransportControl;

    .line 35
    new-instance v0, Lcom/google/android/videos/adapter/WatchNowDataSource$1;

    invoke-direct {v0, p0}, Lcom/google/android/videos/adapter/WatchNowDataSource$1;-><init>(Lcom/google/android/videos/adapter/WatchNowDataSource;)V

    .line 41
    .local v0, "observer":Landroid/database/DataSetObserver;
    invoke-virtual {p2, v0}, Lcom/google/android/videos/adapter/MoviesDataSource;->registerObserver(Ljava/lang/Object;)V

    .line 42
    invoke-virtual {p3, v0}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->registerObserver(Ljava/lang/Object;)V

    .line 43
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/adapter/WatchNowDataSource;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/adapter/WatchNowDataSource;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/videos/adapter/WatchNowDataSource;->onDataSourceChanged()V

    return-void
.end method

.method private getActualDataSource(Landroid/database/Cursor;)Lcom/google/android/videos/adapter/VideosDataSource;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->moviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/adapter/MoviesDataSource;->hasCursor(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->moviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    .line 92
    :goto_0
    return-object v0

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->hasCursor(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 92
    iget-object v0, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    goto :goto_0

    .line 94
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cursor does not belong to either movie or episode data source"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private onDataSourceChanged()V
    .locals 0

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/google/android/videos/adapter/WatchNowDataSource;->notifyChanged()V

    .line 86
    return-void
.end method


# virtual methods
.method public changeCursor(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "newCursor"    # Landroid/database/Cursor;

    .prologue
    .line 231
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getAccount(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 101
    invoke-direct {p0, p1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getActualDataSource(Landroid/database/Cursor;)Lcom/google/android/videos/adapter/VideosDataSource;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/videos/adapter/VideosDataSource;->getAccount(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBytesDownloaded(Landroid/database/Cursor;)Ljava/lang/Long;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 196
    invoke-direct {p0, p1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getActualDataSource(Landroid/database/Cursor;)Lcom/google/android/videos/adapter/VideosDataSource;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/videos/adapter/VideosDataSource;->getBytesDownloaded(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->moviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v0}, Lcom/google/android/videos/adapter/MoviesDataSource;->getCount()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-virtual {v1}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->getCount()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getDownloadSize(Landroid/database/Cursor;)Ljava/lang/Long;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 191
    invoke-direct {p0, p1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getActualDataSource(Landroid/database/Cursor;)Lcom/google/android/videos/adapter/VideosDataSource;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/videos/adapter/VideosDataSource;->getDownloadSize(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getDurationSeconds(Landroid/database/Cursor;)I
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 211
    invoke-direct {p0, p1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getActualDataSource(Landroid/database/Cursor;)Lcom/google/android/videos/adapter/VideosDataSource;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/videos/adapter/VideosDataSource;->getDurationSeconds(Landroid/database/Cursor;)I

    move-result v0

    return v0
.end method

.method public getEpisodeNumber(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->hasCursor(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->getEpisodeNumber(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 151
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getExpirationTimestamp(Landroid/database/Cursor;)J
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 156
    invoke-direct {p0, p1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getActualDataSource(Landroid/database/Cursor;)Lcom/google/android/videos/adapter/VideosDataSource;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/videos/adapter/VideosDataSource;->getExpirationTimestamp(Landroid/database/Cursor;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getHaveLicense(Landroid/database/Cursor;)Z
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 166
    invoke-direct {p0, p1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getActualDataSource(Landroid/database/Cursor;)Lcom/google/android/videos/adapter/VideosDataSource;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/videos/adapter/VideosDataSource;->getHaveLicense(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method

.method public getItem(I)Landroid/database/Cursor;
    .locals 3
    .param p1, "position"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IndexOutOfBoundsException;
        }
    .end annotation

    .prologue
    .line 66
    iget-object v1, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->moviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getCount()I

    move-result v0

    .line 67
    .local v0, "moviesCount":I
    if-ge p1, v0, :cond_0

    .line 68
    iget-object v1, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->moviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v1, p1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getItem(I)Landroid/database/Cursor;

    move-result-object v1

    .line 70
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    sub-int v2, p1, v0

    invoke-virtual {v1, v2}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->getItem(I)Landroid/database/Cursor;

    move-result-object v1

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IndexOutOfBoundsException;
        }
    .end annotation

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getItem(I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getItemIdentifier(I)Ljava/lang/Object;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 76
    iget-object v1, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->moviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getCount()I

    move-result v0

    .line 77
    .local v0, "moviesCount":I
    if-ge p1, v0, :cond_0

    .line 78
    iget-object v1, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->moviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v1, p1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getItemIdentifier(I)Ljava/lang/Object;

    move-result-object v1

    .line 80
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    sub-int v2, p1, v0

    invoke-virtual {v1, v2}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->getItemIdentifier(I)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method

.method public getPinningDrmErrorCode(Landroid/database/Cursor;)Ljava/lang/Integer;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 201
    invoke-direct {p0, p1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getActualDataSource(Landroid/database/Cursor;)Lcom/google/android/videos/adapter/VideosDataSource;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/videos/adapter/VideosDataSource;->getPinningDrmErrorCode(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getPinningStatus(Landroid/database/Cursor;)Ljava/lang/Integer;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 171
    invoke-direct {p0, p1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getActualDataSource(Landroid/database/Cursor;)Lcom/google/android/videos/adapter/VideosDataSource;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/videos/adapter/VideosDataSource;->getPinningStatus(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getPinningStatusReason(Landroid/database/Cursor;)Ljava/lang/Integer;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 176
    invoke-direct {p0, p1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getActualDataSource(Landroid/database/Cursor;)Lcom/google/android/videos/adapter/VideosDataSource;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/videos/adapter/VideosDataSource;->getPinningStatusReason(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getResumeTimestamp(Landroid/database/Cursor;)I
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 206
    invoke-direct {p0, p1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getActualDataSource(Landroid/database/Cursor;)Lcom/google/android/videos/adapter/VideosDataSource;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/videos/adapter/VideosDataSource;->getResumeTimestamp(Landroid/database/Cursor;)I

    move-result v0

    return v0
.end method

.method public getSeasonId(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 120
    invoke-direct {p0, p1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getActualDataSource(Landroid/database/Cursor;)Lcom/google/android/videos/adapter/VideosDataSource;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/videos/adapter/VideosDataSource;->getSeasonId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSeasonNumber(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->hasCursor(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->getSeasonNumber(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 137
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSeasonTitle(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->hasCursor(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->getSeasonTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 144
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getShowId(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 125
    invoke-direct {p0, p1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getActualDataSource(Landroid/database/Cursor;)Lcom/google/android/videos/adapter/VideosDataSource;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/videos/adapter/VideosDataSource;->getShowId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getShowTitle(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 130
    invoke-direct {p0, p1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getActualDataSource(Landroid/database/Cursor;)Lcom/google/android/videos/adapter/VideosDataSource;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/videos/adapter/VideosDataSource;->getShowTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitle(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 115
    invoke-direct {p0, p1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getActualDataSource(Landroid/database/Cursor;)Lcom/google/android/videos/adapter/VideosDataSource;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/videos/adapter/VideosDataSource;->getTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVideoDownloadStatus(Landroid/database/Cursor;)Lcom/google/android/videos/store/VideoDownloadStatus;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->moviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/adapter/MoviesDataSource;->hasCursor(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->moviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getVideoDownloadStatus(Landroid/database/Cursor;)Lcom/google/android/videos/store/VideoDownloadStatus;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getVideoId(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 106
    invoke-direct {p0, p1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getActualDataSource(Landroid/database/Cursor;)Lcom/google/android/videos/adapter/VideosDataSource;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/videos/adapter/VideosDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVideoMetadata(Landroid/database/Cursor;Lcom/google/android/videos/Config;)Lcom/google/android/videos/store/VideoMetadata;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "config"    # Lcom/google/android/videos/Config;

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->moviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/adapter/MoviesDataSource;->hasCursor(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->moviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/adapter/MoviesDataSource;->getVideoMetadata(Landroid/database/Cursor;Lcom/google/android/videos/Config;)Lcom/google/android/videos/store/VideoMetadata;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isActive(Landroid/database/Cursor;)Z
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 181
    invoke-direct {p0, p1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getActualDataSource(Landroid/database/Cursor;)Lcom/google/android/videos/adapter/VideosDataSource;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/videos/adapter/VideosDataSource;->isActive(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method

.method public isMovie(Landroid/database/Cursor;)Z
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->moviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/adapter/MoviesDataSource;->hasCursor(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method

.method public isNetworkConnected()Z
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->moviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v0}, Lcom/google/android/videos/adapter/MoviesDataSource;->isNetworkConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-virtual {v0}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->isNetworkConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNowPlaying(Landroid/database/Cursor;)Z
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 250
    iget-object v0, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v0}, Lcom/google/android/videos/remote/RemoteTracker;->isPlayingLocallyOwnedVideo()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {p0, p1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/remote/RemoteTracker;->isPlaying(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPinned(Landroid/database/Cursor;)Z
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 161
    invoke-direct {p0, p1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getActualDataSource(Landroid/database/Cursor;)Lcom/google/android/videos/adapter/VideosDataSource;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/videos/adapter/VideosDataSource;->isPinned(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method

.method public isRental(Landroid/database/Cursor;)Z
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 186
    invoke-direct {p0, p1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getActualDataSource(Landroid/database/Cursor;)Lcom/google/android/videos/adapter/VideosDataSource;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/videos/adapter/VideosDataSource;->isRental(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method

.method public registerWithRemoteTracker()V
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    iget-object v1, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->transportControl:Lcom/google/android/videos/adapter/WatchNowDataSource$TransportControl;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/remote/RemoteTracker;->registerCustomTransportControl(Lcom/google/android/videos/remote/TransportControl;)V

    .line 47
    return-void
.end method

.method public setNetworkConnected(Z)V
    .locals 1
    .param p1, "networkConnected"    # Z

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->moviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/adapter/MoviesDataSource;->setNetworkConnected(Z)V

    .line 244
    iget-object v0, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->setNetworkConnected(Z)V

    .line 245
    return-void
.end method

.method public unregisterWithRemoteTracker()V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    iget-object v1, p0, Lcom/google/android/videos/adapter/WatchNowDataSource;->transportControl:Lcom/google/android/videos/adapter/WatchNowDataSource$TransportControl;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/remote/RemoteTracker;->unregisterCustomTransportControl(Lcom/google/android/videos/remote/TransportControl;)V

    .line 51
    return-void
.end method
