.class public Lcom/google/android/videos/pano/ui/VideoCollectionUtil;
.super Ljava/lang/Object;
.source "VideoCollectionUtil.java"


# direct methods
.method public static filterValidResource([Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;I)Ljava/util/List;
    .locals 4
    .param p0, "resources"    # [Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;
    .param p1, "maxCollections"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 113
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 115
    .local v0, "collections":Ljava/util/List;, "Ljava/util/List<Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, p0

    if-ge v1, v3, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v3, p1, :cond_2

    .line 116
    aget-object v2, p0, v1

    .line 117
    .local v2, "resource":Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;
    iget-object v3, v2, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->collectionId:Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;

    if-eqz v3, :cond_0

    iget-object v3, v2, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->collectionId:Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;

    iget-object v3, v3, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;->id:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 115
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 120
    :cond_1
    aget-object v3, p0, v1

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 122
    .end local v2    # "resource":Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;
    :cond_2
    return-object v0
.end method

.method public static flattenCollectionResource(Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;Ljava/util/List;)V
    .locals 4
    .param p0, "resource"    # Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 85
    .local p1, "assetResourceList":Ljava/util/List;, "Ljava/util/List<Lcom/google/wireless/android/video/magma/proto/AssetResource;>;"
    if-nez p0, :cond_1

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->child:[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    .line 90
    .local v0, "child":[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;
    if-eqz v0, :cond_2

    array-length v2, v0

    if-lez v2, :cond_2

    .line 91
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v2, v0

    if-ge v1, v2, :cond_0

    .line 92
    aget-object v2, v0, v1

    invoke-static {v2, p1}, Lcom/google/android/videos/pano/ui/VideoCollectionUtil;->flattenCollectionResource(Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;Ljava/util/List;)V

    .line 91
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 97
    .end local v1    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v2, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v2, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v2, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget v2, v2, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    const/4 v3, 0x6

    if-eq v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v2, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget v2, v2, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    const/16 v3, 0x12

    if-ne v2, v3, :cond_0

    .line 104
    :cond_3
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static flattenVideoCollections(Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;)Ljava/util/List;
    .locals 1
    .param p0, "resource"    # Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 32
    .local v0, "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;>;"
    invoke-static {p0, v0}, Lcom/google/android/videos/pano/ui/VideoCollectionUtil;->flattenVideoCollections(Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;Ljava/util/List;)V

    .line 33
    return-object v0
.end method

.method private static flattenVideoCollections(Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;Ljava/util/List;)V
    .locals 3
    .param p0, "resource"    # Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p1, "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;>;"
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->child:[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    .line 39
    .local v0, "children":[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;
    if-eqz v0, :cond_0

    .line 40
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    .line 41
    aget-object v2, v0, v1

    invoke-static {v2, p1}, Lcom/google/android/videos/pano/ui/VideoCollectionUtil;->flattenVideoCollections(Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;Ljava/util/List;)V

    .line 40
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 44
    .end local v1    # "i":I
    :cond_0
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v2, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v2, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    if-eqz v2, :cond_1

    .line 46
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v2, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget v2, v2, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    sparse-switch v2, :sswitch_data_0

    .line 53
    :cond_1
    :goto_1
    return-void

    .line 49
    :sswitch_0
    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 46
    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0x12 -> :sswitch_0
    .end sparse-switch
.end method

.method public static getItemsFromVideoCollection(Landroid/content/Context;Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;Lcom/google/android/videos/store/StoreStatusMonitor;)[Lcom/google/android/videos/pano/model/VideoItem;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "rootResource"    # Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;
    .param p2, "storeStatusMonitor"    # Lcom/google/android/videos/store/StoreStatusMonitor;

    .prologue
    const/4 v7, 0x0

    .line 61
    invoke-static {p1}, Lcom/google/android/videos/pano/ui/VideoCollectionUtil;->flattenVideoCollections(Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;)Ljava/util/List;

    move-result-object v5

    .line 62
    .local v5, "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;>;"
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 63
    new-array v3, v7, [Lcom/google/android/videos/pano/model/VideoItem;

    .line 76
    :cond_0
    return-object v3

    .line 66
    :cond_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    new-array v3, v6, [Lcom/google/android/videos/pano/model/VideoItem;

    .line 67
    .local v3, "items":[Lcom/google/android/videos/pano/model/VideoItem;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    if-ge v1, v6, :cond_0

    .line 68
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    iget-object v0, v6, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 69
    .local v0, "asset":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    iget-object v6, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget v6, v6, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    const/4 v8, 0x6

    if-ne v6, v8, :cond_2

    const/4 v2, 0x1

    .line 70
    .local v2, "isMovie":Z
    :goto_1
    if-eqz v2, :cond_3

    iget-object v6, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v6, v6, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    iget-object v8, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget v8, v8, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    invoke-virtual {p2, v6, v8}, Lcom/google/android/videos/store/StoreStatusMonitor;->getStatus(Ljava/lang/String;I)I

    move-result v6

    invoke-static {v6}, Lcom/google/android/videos/store/StoreStatusMonitor;->isPurchased(I)Z

    move-result v4

    .line 72
    .local v4, "purchased":Z
    :goto_2
    if-eqz v2, :cond_4

    invoke-static {p0, v0, v4}, Lcom/google/android/videos/pano/model/MovieItem;->createMovieItem(Landroid/content/Context;Lcom/google/wireless/android/video/magma/proto/AssetResource;Z)Lcom/google/android/videos/pano/model/VideoItem;

    move-result-object v6

    :goto_3
    aput-object v6, v3, v1

    .line 67
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .end local v2    # "isMovie":Z
    .end local v4    # "purchased":Z
    :cond_2
    move v2, v7

    .line 69
    goto :goto_1

    .restart local v2    # "isMovie":Z
    :cond_3
    move v4, v7

    .line 70
    goto :goto_2

    .line 72
    .restart local v4    # "purchased":Z
    :cond_4
    invoke-static {p0, v0, v4}, Lcom/google/android/videos/pano/model/ShowItem;->createShowItem(Landroid/content/Context;Lcom/google/wireless/android/video/magma/proto/AssetResource;Z)Lcom/google/android/videos/pano/model/ShowItem;

    move-result-object v6

    goto :goto_3
.end method
