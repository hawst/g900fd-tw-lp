.class Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;
.super Lcom/google/android/videos/pano/datasource/WatchNowDataSource$ItemsProcessor;
.source "WatchNowDataSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/datasource/WatchNowDataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EpisodesProcessor"
.end annotation


# instance fields
.field private final episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource;

.field final synthetic this$0:Lcom/google/android/videos/pano/datasource/WatchNowDataSource;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;)V
    .locals 1

    .prologue
    .line 274
    iput-object p1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;->this$0:Lcom/google/android/videos/pano/datasource/WatchNowDataSource;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$ItemsProcessor;-><init>(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;Lcom/google/android/videos/pano/datasource/WatchNowDataSource$1;)V

    .line 276
    new-instance v0, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-direct {v0}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;Lcom/google/android/videos/pano/datasource/WatchNowDataSource$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/pano/datasource/WatchNowDataSource;
    .param p2, "x1"    # Lcom/google/android/videos/pano/datasource/WatchNowDataSource$1;

    .prologue
    .line 274
    invoke-direct {p0, p1}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;-><init>(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;)V

    return-void
.end method


# virtual methods
.method protected createCursorRequest()Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 10

    .prologue
    .line 280
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v7, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;->this$0:Lcom/google/android/videos/pano/datasource/WatchNowDataSource;

    # getter for: Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->config:Lcom/google/android/videos/Config;
    invoke-static {v7}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->access$700(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;)Lcom/google/android/videos/Config;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/android/videos/Config;->recentActiveMillis()J

    move-result-wide v8

    sub-long v2, v0, v8

    .line 281
    .local v2, "activeSinceTimestamp":J
    const/4 v5, 0x1

    .line 282
    .local v5, "allowUnpurchased":Z
    const/4 v4, 0x1

    .line 283
    .local v4, "excludeFinishedLastWatched":Z
    const/4 v6, 0x1

    .line 284
    .local v6, "maxEpisodesPerShow":I
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;->this$0:Lcom/google/android/videos/pano/datasource/WatchNowDataSource;

    invoke-virtual {v0}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->getAccount()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static/range {v0 .. v6}, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow;->createRequest(Ljava/lang/String;ZJZZI)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v0

    return-object v0
.end method

.method protected createItemFromCursor(Landroid/database/Cursor;Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;
    .locals 18
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "account"    # Ljava/lang/String;

    .prologue
    .line 290
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getShowId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    .line 294
    .local v4, "showId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/videos/adapter/EpisodesDataSource;->isPurchased(Landroid/database/Cursor;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 295
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getSeasonId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-static/range {v2 .. v7}, Lcom/google/android/videos/activity/LauncherActivity;->createWatchEpisodeDeepLinkingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v11

    .line 297
    .local v11, "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getResumeTimestamp(Landroid/database/Cursor;)I

    move-result v2

    if-lez v2, :cond_0

    const v17, 0x7f0b00ad

    .line 305
    .local v17, "subtitleId":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getShowBannerUri(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v8

    .line 306
    .local v8, "wallpaperUri":Ljava/lang/String;
    invoke-static/range {p2 .. p2}, Lcom/google/android/videos/pano/ui/PanoHelper;->getDefaultPosterHeight(Landroid/content/Context;)I

    move-result v10

    .line 307
    .local v10, "posterHeight":I
    move v9, v10

    .line 308
    .local v9, "posterWidth":I
    new-instance v3, Lcom/google/android/videos/pano/model/ShowItem;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getShowPosterUri(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v7

    const-wide v12, 0x7fffffffffffffffL

    const/4 v14, 0x0

    const/4 v15, -0x1

    const/16 v16, 0x0

    invoke-direct/range {v3 .. v16}, Lcom/google/android/videos/pano/model/ShowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/content/Intent;JIIF)V

    .line 320
    .local v3, "item":Lcom/google/android/videos/pano/model/VideoItem;
    new-instance v2, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource;

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getShowLastActivityTimestamp(Landroid/database/Cursor;)J

    move-result-wide v6

    const/4 v5, 0x0

    invoke-direct {v2, v3, v6, v7, v5}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;-><init>(Lcom/google/android/videos/pano/model/VideoItem;JLcom/google/android/videos/pano/datasource/WatchNowDataSource$1;)V

    return-object v2

    .line 297
    .end local v3    # "item":Lcom/google/android/videos/pano/model/VideoItem;
    .end local v8    # "wallpaperUri":Ljava/lang/String;
    .end local v9    # "posterWidth":I
    .end local v10    # "posterHeight":I
    .end local v17    # "subtitleId":I
    :cond_0
    const v17, 0x7f0b0123

    goto :goto_0

    .line 300
    .end local v11    # "intent":Landroid/content/Intent;
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getSeasonId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-static/range {v2 .. v7}, Lcom/google/android/videos/activity/LauncherActivity;->createEpisodeDeepLinkingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v11

    .line 302
    .restart local v11    # "intent":Landroid/content/Intent;
    const v17, 0x7f0b0123

    .restart local v17    # "subtitleId":I
    goto :goto_0
.end method
