.class public Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;
.super Lcom/google/android/videos/adapter/AbstractDataSource;
.source "WatchNowRecommendationDataSource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/adapter/AbstractDataSource",
        "<",
        "Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;",
        ">;"
    }
.end annotation


# instance fields
.field private videos:[Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/android/videos/adapter/AbstractDataSource;-><init>()V

    .line 17
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;->videos:[Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;->videos:[Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;

    array-length v0, v0

    goto :goto_0
.end method

.method public getItem(I)Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;
    .locals 1
    .param p1, "position"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IndexOutOfBoundsException;
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;->videos:[Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IndexOutOfBoundsException;
        }
    .end annotation

    .prologue
    .line 14
    invoke-virtual {p0, p1}, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;->getItem(I)Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;

    move-result-object v0

    return-object v0
.end method

.method public getItemIdentifier(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 58
    invoke-virtual {p0, p1}, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;->getItem(I)Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;

    move-result-object v0

    .line 59
    .local v0, "item":Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;
    iget-object v1, v0, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v1, v1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-static {v1}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromAssetResourceId(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public updateVideos([Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;)V
    .locals 1
    .param p1, "videos"    # [Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;->videos:[Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;

    invoke-static {p1, v0}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 64
    iput-object p1, p0, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;->videos:[Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;

    .line 65
    invoke-virtual {p0}, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;->notifyChanged()V

    .line 67
    :cond_0
    return-void
.end method
