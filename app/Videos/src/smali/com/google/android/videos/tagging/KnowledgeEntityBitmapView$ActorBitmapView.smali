.class Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$ActorBitmapView;
.super Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;
.source "KnowledgeEntityBitmapView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ActorBitmapView"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView",
        "<",
        "Landroid/widget/TextView;",
        ">;"
    }
.end annotation


# instance fields
.field private final large:Z


# direct methods
.method public constructor <init>(Lcom/google/android/play/image/AvatarCropTransformation;Lcom/google/android/videos/tagging/KnowledgeEntity$Person;Landroid/widget/TextView;IZ)V
    .locals 6
    .param p1, "avatarCropTransformation"    # Lcom/google/android/play/image/AvatarCropTransformation;
    .param p2, "entity"    # Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    .param p3, "textView"    # Landroid/widget/TextView;
    .param p4, "imageDimension"    # I
    .param p5, "large"    # Z

    .prologue
    .line 75
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;-><init>(Lcom/google/android/play/image/AvatarCropTransformation;Lcom/google/android/videos/tagging/KnowledgeEntity;Landroid/view/View;ILcom/google/android/videos/tagging/KnowledgeEntityBitmapView$1;)V

    .line 76
    iput-boolean p5, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$ActorBitmapView;->large:Z

    .line 77
    return-void
.end method


# virtual methods
.method postProcess(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "response"    # Landroid/graphics/Bitmap;

    .prologue
    .line 81
    move-object v1, p1

    .line 82
    .local v1, "croppedBitmap":Landroid/graphics/Bitmap;
    iget-object v3, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$ActorBitmapView;->entity:Lcom/google/android/videos/tagging/KnowledgeEntity;

    iget-object v3, v3, Lcom/google/android/videos/tagging/KnowledgeEntity;->image:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    invoke-virtual {v3}, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;->getSquareCropRegionIndex()I

    move-result v2

    .line 83
    .local v2, "squareCropRegionIndex":I
    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    .line 84
    iget-object v3, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$ActorBitmapView;->entity:Lcom/google/android/videos/tagging/KnowledgeEntity;

    iget-object v3, v3, Lcom/google/android/videos/tagging/KnowledgeEntity;->image:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v3, v2, v4}, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;->getCropRegion(ILandroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    .line 85
    .local v0, "cropRect":Landroid/graphics/Rect;
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget v4, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v5

    add-int/2addr v4, v5

    if-lt v3, v4, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    iget v4, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v5

    add-int/2addr v4, v5

    if-ge v3, v4, :cond_1

    .line 87
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$ActorBitmapView;->entity:Lcom/google/android/videos/tagging/KnowledgeEntity;

    iget-object v4, v4, Lcom/google/android/videos/tagging/KnowledgeEntity;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 90
    :cond_1
    iget v3, v0, Landroid/graphics/Rect;->left:I

    iget v4, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v5

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v6

    invoke-static {p1, v3, v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 93
    .end local v0    # "cropRect":Landroid/graphics/Rect;
    :cond_2
    iget-object v3, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$ActorBitmapView;->avatarCropTransformation:Lcom/google/android/play/image/AvatarCropTransformation;

    iget v4, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$ActorBitmapView;->imageDimension:I

    iget v5, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$ActorBitmapView;->imageDimension:I

    invoke-virtual {v3, v1, v4, v5}, Lcom/google/android/play/image/AvatarCropTransformation;->transform(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v3

    return-object v3
.end method

.method public bridge synthetic setThumbnail(Ljava/lang/Object;Z)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Z

    .prologue
    .line 69
    check-cast p1, Landroid/graphics/Bitmap;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-super {p0, p1, p2}, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;->setThumbnail(Landroid/graphics/Bitmap;Z)V

    return-void
.end method

.method updateImage(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "response"    # Landroid/graphics/Bitmap;

    .prologue
    .line 98
    if-eqz p1, :cond_0

    .line 99
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$ActorBitmapView;->view:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    iget-object v1, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$ActorBitmapView;->view:Landroid/view/View;

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$ActorBitmapView;->view:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {v2, v0, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-static {v1, v2}, Lcom/google/android/videos/utils/ViewUtil;->setViewBackground(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 104
    :goto_0
    return-void

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$ActorBitmapView;->view:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$ActorBitmapView;->entity:Lcom/google/android/videos/tagging/KnowledgeEntity;

    check-cast v1, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    iget-boolean v2, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$ActorBitmapView;->large:Z

    invoke-static {v0, v1, v2}, Lcom/google/android/videos/tagging/Cards;->setDefaultActorImage(Landroid/widget/TextView;Lcom/google/android/videos/tagging/KnowledgeEntity$Person;Z)V

    goto :goto_0
.end method
