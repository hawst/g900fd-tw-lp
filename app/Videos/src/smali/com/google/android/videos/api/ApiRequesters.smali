.class public interface abstract Lcom/google/android/videos/api/ApiRequesters;
.super Ljava/lang/Object;
.source "ApiRequesters.java"


# virtual methods
.method public abstract getAssetsCachingRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAssetsRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAssetsSyncRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getBytesRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "[B>;"
        }
    .end annotation
.end method

.method public abstract getCategoryListRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/CategoryListRequest;",
            "Lcom/google/wireless/android/video/magma/proto/CategoryListResponse;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getCencLicenseRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/CencLicenseRequest;",
            "[B>;"
        }
    .end annotation
.end method

.method public abstract getConditionalEntitySyncRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/ConditionalHttpRequest",
            "<",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;",
            "Lcom/google/android/videos/async/ConditionalHttpResponse",
            "<",
            "Lorg/apache/http/HttpEntity;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract getGcmCreateNotificationKeySyncRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/GcmCreateUserNotificationKeyRequest;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getGcmRegisterSyncRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/GcmRegisterRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getMpdUrlGetRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/MpdUrlGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getPromotionsRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/PromotionsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getRecommendationsRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/RecommendationsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getRedeemPromotionRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/RedeemPromotionRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getReviewsRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetReviewListRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getRobotTokenRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/RobotTokenRequest;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getStreamsRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/MpdGetRequest;",
            "Lcom/google/android/videos/streams/Streams;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getUnlinkAccountRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/LinkedAccountRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getUpdateAccountLinkingRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/LinkedAccountRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getUpdateWishlistSyncRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/UpdateWishlistRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getUserConfigGetRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/UserConfigGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getUserConfigGetSyncRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/UserConfigGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getUserLibrarySyncRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/UserLibraryRequest;",
            "Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getVideoCollectionGetRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/VideoCollectionGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoCollectionGetResponse;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getVideoCollectionListRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/VideoCollectionListRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoCollectionListResponse;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getVideoFormatsRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/EmptyRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/wireless/android/video/magma/proto/VideoFormat;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract getVideoGetRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/VideoGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getVideoUpdateRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/VideoUpdateRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getWishlistSyncRequester()Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/GetWishlistRequest;",
            "Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;",
            ">;"
        }
    .end annotation
.end method
