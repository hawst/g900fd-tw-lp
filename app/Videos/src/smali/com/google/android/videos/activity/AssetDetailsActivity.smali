.class public abstract Lcom/google/android/videos/activity/AssetDetailsActivity;
.super Lcom/google/android/videos/activity/DrawerActivity;
.source "AssetDetailsActivity.java"

# interfaces
.implements Lcom/google/android/videos/flow/ViewHolderCreator;
.implements Lcom/google/android/videos/ui/BitmapLoader$BitmapPaletteView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;,
        Lcom/google/android/videos/activity/AssetDetailsActivity$StatusBarRecolorListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/activity/DrawerActivity;",
        "Lcom/google/android/videos/flow/ViewHolderCreator",
        "<",
        "Lcom/google/android/videos/flow/BasicViewHolderCreator$BasicViewHolder;",
        ">;",
        "Lcom/google/android/videos/ui/BitmapLoader$BitmapPaletteView;"
    }
.end annotation


# static fields
.field private static final FOCUS_POINT_CENTER:Landroid/graphics/PointF;

.field private static final FOCUS_POINT_NATURAL:Landroid/graphics/PointF;


# instance fields
.field protected accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

.field private actionBarBackgroundUpdater:Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;

.field protected activityRunning:Z

.field private argbEvaluator:Landroid/animation/ArgbEvaluator;

.field protected backgroundColor:I

.field protected backgroundImageView:Lcom/google/android/videos/ui/FocusedImageView;

.field private backgroundTag:Ljava/lang/Object;

.field protected backgroundView:Landroid/view/ViewGroup;

.field protected contentLayout:Landroid/support/v7/widget/RecyclerView;

.field protected cpuExecutor:Ljava/util/concurrent/Executor;

.field private defaultBackgroundColor:I

.field private ensureTransitionStartedRunnable:Ljava/lang/Runnable;

.field protected errorHelper:Lcom/google/android/videos/utils/ErrorHelper;

.field protected mediaRouteProvider:Lcom/google/android/videos/remote/MediaRouteProvider;

.field protected missingBackgroundBitmap:Landroid/graphics/Bitmap;

.field protected pendingEnterTransition:Z

.field protected playDrawerLayout:Lcom/google/android/videos/ui/StatusBarDrawerLayout;

.field protected playLayout:Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;

.field protected posterStore:Lcom/google/android/videos/store/PosterStore;

.field protected purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

.field protected purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

.field protected remoteHelper:Lcom/google/android/videos/remote/RemoteHelper;

.field protected runningEnterTransition:Z

.field protected verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;

.field protected videosGlobals:Lcom/google/android/videos/VideosGlobals;

.field protected wishlistStoreSync:Lcom/google/android/videos/store/WishlistStoreSync;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f000000    # 0.5f

    .line 78
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v2, v2}, Landroid/graphics/PointF;-><init>(FF)V

    sput-object v0, Lcom/google/android/videos/activity/AssetDetailsActivity;->FOCUS_POINT_CENTER:Landroid/graphics/PointF;

    .line 79
    new-instance v0, Landroid/graphics/PointF;

    const v1, 0x3e4ccccd    # 0.2f

    invoke-direct {v0, v2, v1}, Landroid/graphics/PointF;-><init>(FF)V

    sput-object v0, Lcom/google/android/videos/activity/AssetDetailsActivity;->FOCUS_POINT_NATURAL:Landroid/graphics/PointF;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/google/android/videos/activity/DrawerActivity;-><init>()V

    .line 587
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/activity/AssetDetailsActivity;)Landroid/animation/ArgbEvaluator;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/activity/AssetDetailsActivity;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->argbEvaluator:Landroid/animation/ArgbEvaluator;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/activity/AssetDetailsActivity;)Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/activity/AssetDetailsActivity;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->actionBarBackgroundUpdater:Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;

    return-object v0
.end method

.method private addSearchExpandListener(Landroid/view/MenuItem;)V
    .locals 1
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 457
    new-instance v0, Lcom/google/android/videos/activity/AssetDetailsActivity$5;

    invoke-direct {v0, p0}, Lcom/google/android/videos/activity/AssetDetailsActivity$5;-><init>(Lcom/google/android/videos/activity/AssetDetailsActivity;)V

    invoke-static {p1, v0}, Landroid/support/v4/view/MenuItemCompat;->setOnActionExpandListener(Landroid/view/MenuItem;Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;)Landroid/view/MenuItem;

    .line 474
    return-void
.end method

.method private setUpActivityTransitionV21()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 235
    invoke-virtual {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0, v2}, Lcom/google/android/videos/activity/AssetDetailsActivity;->createSharedTransition(Z)Landroid/transition/TransitionSet;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSharedElementReturnTransition(Landroid/transition/Transition;)V

    .line 236
    invoke-virtual {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0, v2}, Lcom/google/android/videos/activity/AssetDetailsActivity;->createNonSharedTransition(Z)Landroid/transition/TransitionSet;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setReturnTransition(Landroid/transition/Transition;)V

    .line 237
    invoke-virtual {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Window;->setSharedElementsUseOverlay(Z)V

    .line 238
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->setResult(I)V

    .line 240
    iget-boolean v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->pendingEnterTransition:Z

    if-eqz v0, :cond_0

    .line 241
    invoke-virtual {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0, v3}, Lcom/google/android/videos/activity/AssetDetailsActivity;->createSharedTransition(Z)Landroid/transition/TransitionSet;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSharedElementEnterTransition(Landroid/transition/Transition;)V

    .line 242
    invoke-virtual {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0, v3}, Lcom/google/android/videos/activity/AssetDetailsActivity;->createNonSharedTransition(Z)Landroid/transition/TransitionSet;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setEnterTransition(Landroid/transition/Transition;)V

    .line 243
    invoke-virtual {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->postponeEnterTransition()V

    .line 248
    new-instance v0, Lcom/google/android/videos/activity/AssetDetailsActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/videos/activity/AssetDetailsActivity$2;-><init>(Lcom/google/android/videos/activity/AssetDetailsActivity;)V

    iput-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->ensureTransitionStartedRunnable:Ljava/lang/Runnable;

    .line 257
    :cond_0
    new-instance v0, Landroid/animation/ArgbEvaluator;

    invoke-direct {v0}, Landroid/animation/ArgbEvaluator;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->argbEvaluator:Landroid/animation/ArgbEvaluator;

    .line 259
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->playLayout:Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;

    new-instance v1, Lcom/google/android/videos/activity/AssetDetailsActivity$3;

    invoke-direct {v1, p0}, Lcom/google/android/videos/activity/AssetDetailsActivity$3;-><init>(Lcom/google/android/videos/activity/AssetDetailsActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;->setActionBarAlphaListener(Lcom/google/android/videos/ui/ImmersiveHeaderListLayout$ActionBarAlphaListener;)V

    .line 272
    return-void
.end method


# virtual methods
.method protected createNonSharedTransition(Z)Landroid/transition/TransitionSet;
    .locals 10
    .param p1, "isEnter"    # Z

    .prologue
    const v9, 0x7f0f01c1

    const v8, 0x7f0f015c

    const/4 v7, 0x1

    .line 349
    new-instance v3, Landroid/transition/TransitionSet;

    invoke-direct {v3}, Landroid/transition/TransitionSet;-><init>()V

    .line 351
    .local v3, "set":Landroid/transition/TransitionSet;
    invoke-virtual {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a00eb

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 352
    .local v1, "assetDetailsColor":I
    invoke-virtual {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a00ea

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 354
    .local v0, "appColor":I
    if-eqz p1, :cond_0

    new-instance v4, Lcom/google/android/videos/activity/AssetDetailsActivity$StatusBarRecolorListener;

    iget-object v5, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->playDrawerLayout:Lcom/google/android/videos/ui/StatusBarDrawerLayout;

    invoke-direct {v4, v5, v0, v1}, Lcom/google/android/videos/activity/AssetDetailsActivity$StatusBarRecolorListener;-><init>(Lcom/google/android/videos/ui/StatusBarDrawerLayout;II)V

    .line 357
    .local v4, "statusBarFade":Lcom/google/android/videos/activity/AssetDetailsActivity$StatusBarRecolorListener;
    :goto_0
    invoke-virtual {v3, v4}, Landroid/transition/TransitionSet;->addListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/TransitionSet;

    .line 359
    invoke-virtual {p0, v9}, Lcom/google/android/videos/activity/AssetDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    invoke-virtual {v5, v7}, Landroid/view/ViewGroup;->setTransitionGroup(Z)V

    .line 360
    invoke-virtual {p0, v8}, Lcom/google/android/videos/activity/AssetDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    invoke-virtual {v5, v7}, Landroid/view/ViewGroup;->setTransitionGroup(Z)V

    .line 362
    new-instance v2, Landroid/transition/Fade;

    invoke-direct {v2}, Landroid/transition/Fade;-><init>()V

    .line 363
    .local v2, "phllFade":Landroid/transition/Transition;
    invoke-virtual {v2, v8}, Landroid/transition/Transition;->addTarget(I)Landroid/transition/Transition;

    .line 364
    invoke-virtual {v2, v9}, Landroid/transition/Transition;->addTarget(I)Landroid/transition/Transition;

    .line 365
    invoke-virtual {v3, v2}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 367
    return-object v3

    .line 354
    .end local v2    # "phllFade":Landroid/transition/Transition;
    .end local v4    # "statusBarFade":Lcom/google/android/videos/activity/AssetDetailsActivity$StatusBarRecolorListener;
    :cond_0
    new-instance v4, Lcom/google/android/videos/activity/AssetDetailsActivity$StatusBarRecolorListener;

    iget-object v5, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->playDrawerLayout:Lcom/google/android/videos/ui/StatusBarDrawerLayout;

    invoke-direct {v4, v5, v0}, Lcom/google/android/videos/activity/AssetDetailsActivity$StatusBarRecolorListener;-><init>(Lcom/google/android/videos/ui/StatusBarDrawerLayout;I)V

    goto :goto_0
.end method

.method protected createSharedTransition(Z)Landroid/transition/TransitionSet;
    .locals 1
    .param p1, "isEnter"    # Z

    .prologue
    .line 338
    new-instance v0, Landroid/transition/TransitionSet;

    invoke-direct {v0}, Landroid/transition/TransitionSet;-><init>()V

    return-object v0
.end method

.method public bridge synthetic createViewHolder(ILandroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1
    .param p1, "x0"    # I
    .param p2, "x1"    # Landroid/view/ViewGroup;

    .prologue
    .line 75
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/activity/AssetDetailsActivity;->createViewHolder(ILandroid/view/ViewGroup;)Lcom/google/android/videos/flow/BasicViewHolderCreator$BasicViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public createViewHolder(ILandroid/view/ViewGroup;)Lcom/google/android/videos/flow/BasicViewHolderCreator$BasicViewHolder;
    .locals 5
    .param p1, "viewType"    # I
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v4, 0x7f040018

    const/4 v3, 0x0

    .line 315
    if-ne p1, v4, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    .line 316
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-virtual {v2, v4, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 318
    .local v1, "spacerView":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 319
    .local v0, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    instance-of v2, v0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    if-eqz v2, :cond_1

    .line 320
    check-cast v0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    .end local v0    # "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->getHeaderHeight()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/play/utils/Compound;->intLengthToCompound(I)I

    move-result v2

    iput v2, v0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->heightCompound:I

    .line 325
    :goto_1
    new-instance v2, Lcom/google/android/videos/flow/BasicViewHolderCreator$BasicViewHolder;

    invoke-direct {v2, v1}, Lcom/google/android/videos/flow/BasicViewHolderCreator$BasicViewHolder;-><init>(Landroid/view/View;)V

    return-object v2

    .end local v1    # "spacerView":Landroid/view/View;
    :cond_0
    move v2, v3

    .line 315
    goto :goto_0

    .line 323
    .restart local v0    # "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    .restart local v1    # "spacerView":Landroid/view/View;
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->getHeaderHeight()I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_1
.end method

.method protected abstract getHeaderHeight()I
.end method

.method protected abstract getLayoutId()I
.end method

.method protected abstract getScreenshotHeight()I
.end method

.method protected final getStandardScreenshotHeight()I
    .locals 5

    .prologue
    .line 302
    invoke-virtual {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 305
    .local v0, "resources":Landroid/content/res/Resources;
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v1, v3, Landroid/content/res/Configuration;->screenWidthDp:I

    .line 306
    .local v1, "screenWidthDp":I
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    int-to-float v4, v1

    mul-float/2addr v3, v4

    const/high16 v4, 0x41800000    # 16.0f

    div-float/2addr v3, v4

    const v4, 0x7f0c0028

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    int-to-float v4, v4

    mul-float v2, v3, v4

    .line 308
    .local v2, "screenshotHeight":F
    float-to-int v3, v2

    return v3
.end method

.method public final getThumbnailTag()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 493
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->backgroundTag:Ljava/lang/Object;

    return-object v0
.end method

.method public abstract loadBackgroundImage()V
.end method

.method protected onBackgroundColorExtracted(IZ)V
    .locals 0
    .param p1, "color"    # I
    .param p2, "isFinal"    # Z

    .prologue
    .line 414
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 124
    invoke-super {p0, p1}, Lcom/google/android/videos/activity/DrawerActivity;->onCreate(Landroid/os/Bundle;)V

    .line 126
    invoke-virtual {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    .line 127
    .local v7, "intent":Landroid/content/Intent;
    const-string v0, "run_animation"

    invoke-virtual {v7, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    .line 128
    .local v9, "useTransitions":Z
    if-eqz v9, :cond_1

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->pendingEnterTransition:Z

    .line 130
    invoke-virtual {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->getLayoutId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->setContentView(I)V

    .line 132
    const v0, 0x7f0f011e

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;

    iput-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->playLayout:Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;

    .line 133
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->playLayout:Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;

    new-instance v1, Lcom/google/android/videos/activity/AssetDetailsActivity$1;

    invoke-direct {v1, p0, p0, v9}, Lcom/google/android/videos/activity/AssetDetailsActivity$1;-><init>(Lcom/google/android/videos/activity/AssetDetailsActivity;Landroid/content/Context;Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;->configure(Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;)V

    .line 190
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->playLayout:Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;

    const v1, 0x7f0a0079

    invoke-static {p0, v1}, Lcom/google/android/videos/ui/DogfoodHelper;->addPawsIfNeeded(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;->setFloatingControlsBackground(Landroid/graphics/drawable/Drawable;)V

    .line 193
    new-instance v0, Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;

    invoke-direct {v0, p0}, Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;-><init>(Lcom/google/android/videos/activity/AssetDetailsActivity;)V

    iput-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->actionBarBackgroundUpdater:Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;

    .line 194
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->playLayout:Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;

    iget-object v1, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->actionBarBackgroundUpdater:Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;->setOnLayoutChangedListener(Lcom/google/android/play/headerlist/PlayHeaderListLayout$LayoutListener;)V

    .line 195
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->actionBarBackgroundUpdater:Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;->updateBackground()V

    .line 197
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    .line 198
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    .line 199
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getPosterStore()Lcom/google/android/videos/store/PosterStore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->posterStore:Lcom/google/android/videos/store/PosterStore;

    .line 200
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStoreSync()Lcom/google/android/videos/store/PurchaseStoreSync;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

    .line 201
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getWishlistStoreSync()Lcom/google/android/videos/store/WishlistStoreSync;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->wishlistStoreSync:Lcom/google/android/videos/store/WishlistStoreSync;

    .line 202
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStore()Lcom/google/android/videos/store/PurchaseStore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    .line 203
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getErrorHelper()Lcom/google/android/videos/utils/ErrorHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->errorHelper:Lcom/google/android/videos/utils/ErrorHelper;

    .line 204
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getVerticalsHelper()Lcom/google/android/videos/ui/VerticalsHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;

    .line 205
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getCpuExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->cpuExecutor:Ljava/util/concurrent/Executor;

    .line 207
    const v0, 0x7f0f011d

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/StatusBarDrawerLayout;

    iput-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->playDrawerLayout:Lcom/google/android/videos/ui/StatusBarDrawerLayout;

    .line 208
    new-instance v0, Lcom/google/android/videos/ui/VideosDrawerHelper;

    iget-object v1, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->playDrawerLayout:Lcom/google/android/videos/ui/StatusBarDrawerLayout;

    iget-object v2, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    iget-object v2, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v5

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/ui/VideosDrawerHelper;-><init>(Lcom/google/android/play/drawer/PlayDrawerLayout;Lcom/google/android/videos/activity/DrawerActivity;Landroid/content/SharedPreferences;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/logging/EventLogger;Z)V

    iput-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->videosDrawerHelper:Lcom/google/android/videos/ui/VideosDrawerHelper;

    .line 211
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->videosDrawerHelper:Lcom/google/android/videos/ui/VideosDrawerHelper;

    invoke-virtual {v0, v6}, Lcom/google/android/videos/ui/VideosDrawerHelper;->setDrawerIndicatorEnabled(Z)V

    .line 213
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getMediaRouteManager()Lcom/google/android/videos/remote/MediaRouteManager;

    move-result-object v8

    .line 214
    .local v8, "mediaRouteManager":Lcom/google/android/videos/remote/MediaRouteManager;
    invoke-virtual {v8}, Lcom/google/android/videos/remote/MediaRouteManager;->getRouteSelector()Landroid/support/v7/media/MediaRouteSelector;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/videos/remote/MediaRouteProviderCompat;->create(Landroid/support/v7/app/ActionBarActivity;Landroid/support/v7/media/MediaRouteSelector;)Lcom/google/android/videos/remote/MediaRouteProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->mediaRouteProvider:Lcom/google/android/videos/remote/MediaRouteProvider;

    .line 216
    new-instance v0, Lcom/google/android/videos/remote/RemoteHelper;

    invoke-direct {v0, v8}, Lcom/google/android/videos/remote/RemoteHelper;-><init>(Lcom/google/android/videos/remote/MediaRouteManager;)V

    iput-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->remoteHelper:Lcom/google/android/videos/remote/RemoteHelper;

    .line 218
    invoke-virtual {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00c4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->defaultBackgroundColor:I

    .line 219
    iget v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->defaultBackgroundColor:I

    iput v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->backgroundColor:I

    .line 220
    invoke-virtual {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020039

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->missingBackgroundBitmap:Landroid/graphics/Bitmap;

    .line 222
    invoke-virtual {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->loadBackgroundImage()V

    .line 224
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 225
    invoke-direct {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->setUpActivityTransitionV21()V

    .line 227
    :cond_0
    return-void

    .end local v8    # "mediaRouteManager":Lcom/google/android/videos/remote/MediaRouteManager;
    :cond_1
    move v0, v6

    .line 128
    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 444
    invoke-super {p0, p1}, Lcom/google/android/videos/activity/DrawerActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 445
    invoke-virtual {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 446
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v2, 0x7f140004

    invoke-virtual {v0, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 447
    invoke-static {p1, v0, p0}, Lcom/google/android/videos/VideosApplication;->createSearchMenu(Landroid/view/Menu;Landroid/view/MenuInflater;Landroid/app/Activity;)Landroid/view/MenuItem;

    move-result-object v1

    .line 448
    .local v1, "search":Landroid/view/MenuItem;
    if-eqz v1, :cond_0

    .line 449
    invoke-direct {p0, v1}, Lcom/google/android/videos/activity/AssetDetailsActivity;->addSearchExpandListener(Landroid/view/MenuItem;)V

    .line 451
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->mediaRouteProvider:Lcom/google/android/videos/remote/MediaRouteProvider;

    invoke-interface {v2, p1, v0}, Lcom/google/android/videos/remote/MediaRouteProvider;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 452
    iget-object v2, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->videosDrawerHelper:Lcom/google/android/videos/ui/VideosDrawerHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/VideosDrawerHelper;->syncState()V

    .line 453
    const/4 v2, 0x1

    return v2
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 438
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->videosDrawerHelper:Lcom/google/android/videos/ui/VideosDrawerHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/VideosDrawerHelper;->finish()V

    .line 439
    invoke-super {p0}, Lcom/google/android/videos/activity/DrawerActivity;->onDestroy()V

    .line 440
    return-void
.end method

.method protected onEnterTransitionFinish()V
    .locals 0

    .prologue
    .line 404
    return-void
.end method

.method protected onEnterTransitionStart()V
    .locals 0

    .prologue
    .line 400
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 478
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->remoteHelper:Lcom/google/android/videos/remote/RemoteHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/remote/RemoteHelper;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Lcom/google/android/videos/activity/DrawerActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 483
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->remoteHelper:Lcom/google/android/videos/remote/RemoteHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/remote/RemoteHelper;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Lcom/google/android/videos/activity/DrawerActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 429
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->activityRunning:Z

    .line 430
    iget-boolean v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->pendingEnterTransition:Z

    if-eqz v0, :cond_0

    .line 431
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->contentLayout:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->ensureTransitionStartedRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 433
    :cond_0
    invoke-super {p0}, Lcom/google/android/videos/activity/DrawerActivity;->onPause()V

    .line 434
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 418
    invoke-super {p0}, Lcom/google/android/videos/activity/DrawerActivity;->onResume()V

    .line 419
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->activityRunning:Z

    .line 420
    iget-boolean v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->pendingEnterTransition:Z

    if-eqz v0, :cond_0

    .line 421
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->contentLayout:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->ensureTransitionStartedRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x4b0

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v7/widget/RecyclerView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 424
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->playLayout:Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;->activateBuiltInToolbar()V

    .line 425
    return-void
.end method

.method public onSupportNavigateUp()Z
    .locals 3

    .prologue
    .line 277
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "up_is_back"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 280
    invoke-virtual {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->finishAfterTransition()V

    .line 281
    const/4 v0, 0x1

    .line 283
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/google/android/videos/activity/DrawerActivity;->onSupportNavigateUp()Z

    move-result v0

    goto :goto_0
.end method

.method public pendingEnterTransition()Z
    .locals 1

    .prologue
    .line 391
    iget-boolean v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->pendingEnterTransition:Z

    return v0
.end method

.method public runningEnterTransition()Z
    .locals 1

    .prologue
    .line 395
    iget-boolean v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->runningEnterTransition:Z

    return v0
.end method

.method public final setThumbnail(Landroid/util/Pair;Z)V
    .locals 4
    .param p2, "isMissing"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Landroid/graphics/Bitmap;",
            "Landroid/support/v7/graphics/Palette;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 511
    .local p1, "background":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/graphics/Bitmap;Landroid/support/v7/graphics/Palette;>;"
    iget-object v2, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->backgroundImageView:Lcom/google/android/videos/ui/FocusedImageView;

    if-eqz v2, :cond_0

    .line 512
    iget-object v3, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->backgroundImageView:Lcom/google/android/videos/ui/FocusedImageView;

    iget-object v2, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-virtual {v3, v2}, Lcom/google/android/videos/ui/FocusedImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 513
    iget-object v3, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->backgroundImageView:Lcom/google/android/videos/ui/FocusedImageView;

    if-eqz p2, :cond_3

    sget-object v2, Lcom/google/android/videos/activity/AssetDetailsActivity;->FOCUS_POINT_CENTER:Landroid/graphics/PointF;

    :goto_0
    invoke-virtual {v3, v2}, Lcom/google/android/videos/ui/FocusedImageView;->setFocusPoint(Landroid/graphics/PointF;)V

    .line 519
    :cond_0
    iget v2, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->defaultBackgroundColor:I

    iput v2, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->backgroundColor:I

    .line 520
    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/support/v7/graphics/Palette;

    .line 521
    .local v0, "palette":Landroid/support/v7/graphics/Palette;
    if-eqz v0, :cond_2

    .line 522
    invoke-virtual {v0}, Landroid/support/v7/graphics/Palette;->getDarkVibrantSwatch()Landroid/support/v7/graphics/Palette$Swatch;

    move-result-object v1

    .line 523
    .local v1, "swatch":Landroid/support/v7/graphics/Palette$Swatch;
    if-nez v1, :cond_1

    .line 524
    invoke-virtual {v0}, Landroid/support/v7/graphics/Palette;->getDarkMutedSwatch()Landroid/support/v7/graphics/Palette$Swatch;

    move-result-object v1

    .line 526
    :cond_1
    if-eqz v1, :cond_2

    .line 527
    invoke-virtual {v1}, Landroid/support/v7/graphics/Palette$Swatch;->getRgb()I

    move-result v2

    iput v2, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->backgroundColor:I

    .line 530
    .end local v1    # "swatch":Landroid/support/v7/graphics/Palette$Swatch;
    :cond_2
    iget v3, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->backgroundColor:I

    if-nez p2, :cond_4

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {p0, v3, v2}, Lcom/google/android/videos/activity/AssetDetailsActivity;->onBackgroundColorExtracted(IZ)V

    .line 531
    return-void

    .line 513
    .end local v0    # "palette":Landroid/support/v7/graphics/Palette;
    :cond_3
    sget-object v2, Lcom/google/android/videos/activity/AssetDetailsActivity;->FOCUS_POINT_NATURAL:Landroid/graphics/PointF;

    goto :goto_0

    .line 530
    .restart local v0    # "palette":Landroid/support/v7/graphics/Palette;
    :cond_4
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public bridge synthetic setThumbnail(Ljava/lang/Object;Z)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Z

    .prologue
    .line 75
    check-cast p1, Landroid/util/Pair;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/activity/AssetDetailsActivity;->setThumbnail(Landroid/util/Pair;Z)V

    return-void
.end method

.method public final setThumbnailTag(Ljava/lang/Object;)V
    .locals 0
    .param p1, "tag"    # Ljava/lang/Object;

    .prologue
    .line 501
    iput-object p1, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->backgroundTag:Ljava/lang/Object;

    .line 502
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 288
    iget-object v1, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->playLayout:Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;

    invoke-virtual {v1}, Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;->getActionBarView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    .line 289
    .local v0, "toolbar":Landroid/support/v7/widget/Toolbar;
    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 290
    invoke-super {p0, p1}, Lcom/google/android/videos/activity/DrawerActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 291
    return-void
.end method

.method protected startEnterTransition()V
    .locals 2

    .prologue
    .line 372
    iget-boolean v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->pendingEnterTransition:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->activityRunning:Z

    if-eqz v0, :cond_0

    .line 373
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->pendingEnterTransition:Z

    .line 374
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity;->runningEnterTransition:Z

    .line 376
    invoke-virtual {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getEnterTransition()Landroid/transition/Transition;

    move-result-object v0

    new-instance v1, Lcom/google/android/videos/activity/AssetDetailsActivity$4;

    invoke-direct {v1, p0}, Lcom/google/android/videos/activity/AssetDetailsActivity$4;-><init>(Lcom/google/android/videos/activity/AssetDetailsActivity;)V

    invoke-virtual {v0, v1}, Landroid/transition/Transition;->addListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/Transition;

    .line 385
    invoke-virtual {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->startPostponedEnterTransition()V

    .line 386
    invoke-virtual {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->onEnterTransitionStart()V

    .line 388
    :cond_0
    return-void
.end method
