.class public Lcom/google/android/videos/player/Director;
.super Ljava/lang/Object;
.source "Director.java"

# interfaces
.implements Lcom/google/android/videos/player/DirectorInitializer$Listener;
.implements Lcom/google/android/videos/player/LocalPlaybackHelper$Listener;
.implements Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;
.implements Lcom/google/android/videos/player/overlay/TrackChangeListener;
.implements Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;
.implements Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;
.implements Lcom/google/android/videos/utils/RetryAction;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/Director$Listener;
    }
.end annotation


# instance fields
.field private final account:Ljava/lang/String;

.field private final activity:Landroid/support/v4/app/FragmentActivity;

.field private cancelableStoryboardCallback:Lcom/google/android/videos/async/CancelableCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/CancelableCallback",
            "<",
            "Lcom/google/android/videos/api/MpdGetRequest;",
            "Lcom/google/android/videos/streams/Streams;",
            ">;"
        }
    .end annotation
.end field

.field private final config:Lcom/google/android/videos/Config;

.field private final configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

.field private controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

.field private currentKnowledgeRequest:Lcom/google/android/videos/tagging/KnowledgeRequest;

.field private currentKnowledgeTimeMillis:I

.field private final displayRoute:Landroid/media/MediaRouter$RouteInfo;

.field private dragPromoOverlay:Lcom/google/android/videos/player/overlay/DragPromoOverlay;

.field private final eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

.field private initializer:Lcom/google/android/videos/player/DirectorInitializer;

.field private final isRemotePlayback:Z

.field private final isTrailer:Z

.field private final isTv:Z

.field private knowledgeCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeRequest;",
            "Lcom/google/android/videos/tagging/KnowledgeBundle;",
            ">;"
        }
    .end annotation
.end field

.field private knowledgeClient:Lcom/google/android/videos/tagging/KnowledgeClient;

.field private knowledgeViewHelper:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

.field private final listener:Lcom/google/android/videos/player/Director$Listener;

.field private localKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

.field private localTaglessKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

.field private playWhenInitialized:Z

.field private playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

.field private final playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

.field private final playbackType:I

.field private playedFromMillis:I

.field private playerView:Lcom/google/android/videos/player/PlayerView;

.field private final preferences:Landroid/content/SharedPreferences;

.field private final preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

.field private remoteScreenPanelHelper:Lcom/google/android/videos/ui/RemoteScreenPanelHelper;

.field private final seasonId:Ljava/lang/String;

.field private final showId:Ljava/lang/String;

.field private showRecentActorsWithinMillis:I

.field private state:I

.field private storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

.field private final streamsRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/MpdGetRequest;",
            "Lcom/google/android/videos/streams/Streams;",
            ">;"
        }
    .end annotation
.end field

.field private subtitlesOverlay:Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

.field private final useDashStreams:Z

.field private final videoId:Ljava/lang/String;

.field private final videosGlobals:Lcom/google/android/videos/VideosGlobals;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/VideosGlobals;Landroid/support/v4/app/FragmentActivity;Lcom/google/android/videos/player/Director$Listener;Lcom/google/android/videos/player/PlaybackResumeState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLandroid/media/MediaRouter$RouteInfo;)V
    .locals 4
    .param p1, "videosGlobals"    # Lcom/google/android/videos/VideosGlobals;
    .param p2, "activity"    # Landroid/support/v4/app/FragmentActivity;
    .param p3, "listener"    # Lcom/google/android/videos/player/Director$Listener;
    .param p4, "playbackResumeState"    # Lcom/google/android/videos/player/PlaybackResumeState;
    .param p5, "videoId"    # Ljava/lang/String;
    .param p6, "seasonId"    # Ljava/lang/String;
    .param p7, "showId"    # Ljava/lang/String;
    .param p8, "isTrailer"    # Z
    .param p9, "account"    # Ljava/lang/String;
    .param p10, "isRemotePlayback"    # Z
    .param p11, "displayRoute"    # Landroid/media/MediaRouter$RouteInfo;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    new-instance v0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;-><init>(Lcom/google/android/videos/Config;)V

    iput-object v0, p0, Lcom/google/android/videos/player/Director;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    .line 156
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/VideosGlobals;

    iput-object v0, p0, Lcom/google/android/videos/player/Director;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    .line 157
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    iput-object v0, p0, Lcom/google/android/videos/player/Director;->activity:Landroid/support/v4/app/FragmentActivity;

    .line 158
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/player/Director$Listener;

    iput-object v0, p0, Lcom/google/android/videos/player/Director;->listener:Lcom/google/android/videos/player/Director$Listener;

    .line 159
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/player/PlaybackResumeState;

    iput-object v0, p0, Lcom/google/android/videos/player/Director;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    .line 160
    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/videos/player/Director;->videoId:Ljava/lang/String;

    .line 161
    if-eqz p8, :cond_2

    .end local p9    # "account":Ljava/lang/String;
    :goto_0
    iput-object p9, p0, Lcom/google/android/videos/player/Director;->account:Ljava/lang/String;

    .line 162
    if-eqz p10, :cond_3

    const/4 v0, 0x2

    :goto_1
    iput v0, p0, Lcom/google/android/videos/player/Director;->playbackType:I

    .line 164
    iput-boolean p10, p0, Lcom/google/android/videos/player/Director;->isRemotePlayback:Z

    .line 165
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/Director;->preferences:Landroid/content/SharedPreferences;

    .line 166
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/Director;->config:Lcom/google/android/videos/Config;

    .line 167
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/Director;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    .line 168
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/Director;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 169
    if-nez p7, :cond_0

    if-nez p6, :cond_5

    :cond_0
    move v0, v1

    :goto_2
    const-string v3, "ShowId cannot be null when seasonId is not null"

    invoke-static {v0, v3}, Lcom/google/android/videos/utils/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 171
    iput-object p6, p0, Lcom/google/android/videos/player/Director;->seasonId:Ljava/lang/String;

    .line 172
    iput-object p7, p0, Lcom/google/android/videos/player/Director;->showId:Ljava/lang/String;

    .line 173
    iput-boolean p8, p0, Lcom/google/android/videos/player/Director;->isTrailer:Z

    .line 174
    iput-object p11, p0, Lcom/google/android/videos/player/Director;->displayRoute:Landroid/media/MediaRouter$RouteInfo;

    .line 176
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->config:Lcom/google/android/videos/Config;

    invoke-interface {v0}, Lcom/google/android/videos/Config;->useDashForStreaming()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p10, :cond_1

    move v2, v1

    :cond_1
    iput-boolean v2, p0, Lcom/google/android/videos/player/Director;->useDashStreams:Z

    .line 177
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/api/ApiRequesters;->getStreamsRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/Director;->streamsRequester:Lcom/google/android/videos/async/Requester;

    .line 178
    invoke-virtual {p2}, Landroid/support/v4/app/FragmentActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/utils/Util;->isTv(Landroid/content/pm/PackageManager;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/videos/player/Director;->isTv:Z

    .line 180
    iput v1, p0, Lcom/google/android/videos/player/Director;->state:I

    .line 182
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/videos/player/Director;->createPlaybackHelperIfNull()V
    :try_end_0
    .catch Lcom/google/android/videos/player/PlaybackHelper$DisplayNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 186
    :goto_3
    invoke-direct {p0}, Lcom/google/android/videos/player/Director;->createInitializerIfNull()V

    .line 187
    return-void

    .line 161
    .restart local p9    # "account":Ljava/lang/String;
    :cond_2
    invoke-static {p9}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p9

    goto :goto_0

    .line 162
    .end local p9    # "account":Ljava/lang/String;
    :cond_3
    if-eqz p11, :cond_4

    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    move v0, v2

    .line 169
    goto :goto_2

    .line 183
    :catch_0
    move-exception v0

    goto :goto_3
.end method

.method static synthetic access$000(Lcom/google/android/videos/player/Director;)Lcom/google/android/videos/tagging/KnowledgeRequest;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/Director;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->currentKnowledgeRequest:Lcom/google/android/videos/tagging/KnowledgeRequest;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/player/Director;)Lcom/google/android/videos/tagging/KnowledgeViewHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/Director;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->knowledgeViewHelper:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/player/Director;)Lcom/google/android/videos/player/overlay/ControllerOverlay;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/Director;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/player/Director;[Lcom/google/wireless/android/video/magma/proto/Storyboard;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/player/Director;
    .param p1, "x1"    # [Lcom/google/wireless/android/video/magma/proto/Storyboard;

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/Director;->onStoryboards([Lcom/google/wireless/android/video/magma/proto/Storyboard;)V

    return-void
.end method

.method private createInitializerIfNull()V
    .locals 12

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->initializer:Lcom/google/android/videos/player/DirectorInitializer;

    if-eqz v0, :cond_0

    .line 364
    :goto_0
    return-void

    .line 361
    :cond_0
    new-instance v0, Lcom/google/android/videos/player/DirectorInitializer;

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    iget-object v2, p0, Lcom/google/android/videos/player/Director;->activity:Landroid/support/v4/app/FragmentActivity;

    iget-object v4, p0, Lcom/google/android/videos/player/Director;->videoId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/player/Director;->showId:Ljava/lang/String;

    if-eqz v3, :cond_1

    const/4 v5, 0x1

    :goto_1
    iget-boolean v6, p0, Lcom/google/android/videos/player/Director;->isTrailer:Z

    iget-object v7, p0, Lcom/google/android/videos/player/Director;->account:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/videos/player/Director;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    iget-boolean v9, p0, Lcom/google/android/videos/player/Director;->isRemotePlayback:Z

    iget-boolean v10, p0, Lcom/google/android/videos/player/Director;->useDashStreams:Z

    iget-object v11, p0, Lcom/google/android/videos/player/Director;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    move-object v3, p0

    invoke-direct/range {v0 .. v11}, Lcom/google/android/videos/player/DirectorInitializer;-><init>(Lcom/google/android/videos/VideosGlobals;Landroid/app/Activity;Lcom/google/android/videos/player/DirectorInitializer$Listener;Ljava/lang/String;ZZLjava/lang/String;Lcom/google/android/videos/player/PlaybackResumeState;ZZLcom/google/android/videos/player/logging/PlaybackPreparationLogger;)V

    iput-object v0, p0, Lcom/google/android/videos/player/Director;->initializer:Lcom/google/android/videos/player/DirectorInitializer;

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private createPlaybackHelperIfNull()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/player/PlaybackHelper$DisplayNotFoundException;
        }
    .end annotation

    .prologue
    .line 336
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

    if-eqz v0, :cond_1

    .line 350
    :cond_0
    :goto_0
    return-void

    .line 339
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/videos/player/Director;->isRemotePlayback:Z

    if-eqz v0, :cond_2

    .line 340
    new-instance v0, Lcom/google/android/videos/remote/RemotePlaybackHelper;

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->activity:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, p0, Lcom/google/android/videos/player/Director;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/player/Director;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v3}, Lcom/google/android/videos/VideosGlobals;->getMediaRouteManager()Lcom/google/android/videos/remote/MediaRouteManager;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/videos/player/Director;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    iget-object v5, p0, Lcom/google/android/videos/player/Director;->videoId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/videos/player/Director;->seasonId:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/videos/player/Director;->showId:Ljava/lang/String;

    iget-boolean v8, p0, Lcom/google/android/videos/player/Director;->isTrailer:Z

    iget-object v9, p0, Lcom/google/android/videos/player/Director;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v9}, Lcom/google/android/videos/VideosGlobals;->getCaptionPreferences()Lcom/google/android/videos/player/CaptionPreferences;

    move-result-object v11

    iget-object v12, p0, Lcom/google/android/videos/player/Director;->account:Ljava/lang/String;

    move-object v9, p0

    move-object v10, p0

    invoke-direct/range {v0 .. v12}, Lcom/google/android/videos/remote/RemotePlaybackHelper;-><init>(Landroid/app/Activity;Landroid/content/SharedPreferences;Lcom/google/android/videos/remote/MediaRouteManager;Lcom/google/android/videos/player/PlaybackResumeState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/videos/utils/RetryAction;Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;Lcom/google/android/videos/player/CaptionPreferences;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/videos/player/Director;->playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

    .line 347
    :goto_1
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playerView:Lcom/google/android/videos/player/PlayerView;

    if-eqz v0, :cond_0

    .line 348
    invoke-direct {p0}, Lcom/google/android/videos/player/Director;->setPlaybackHelperViews()V

    goto :goto_0

    .line 344
    :cond_2
    new-instance v0, Lcom/google/android/videos/player/LocalPlaybackHelper;

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    iget-object v2, p0, Lcom/google/android/videos/player/Director;->activity:Landroid/support/v4/app/FragmentActivity;

    iget-object v4, p0, Lcom/google/android/videos/player/Director;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    iget-object v5, p0, Lcom/google/android/videos/player/Director;->videoId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/videos/player/Director;->showId:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/videos/player/Director;->seasonId:Ljava/lang/String;

    iget-boolean v8, p0, Lcom/google/android/videos/player/Director;->isTrailer:Z

    iget-object v9, p0, Lcom/google/android/videos/player/Director;->account:Ljava/lang/String;

    iget-object v11, p0, Lcom/google/android/videos/player/Director;->displayRoute:Landroid/media/MediaRouter$RouteInfo;

    iget-object v12, p0, Lcom/google/android/videos/player/Director;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    move-object v3, p0

    move-object v10, p0

    invoke-direct/range {v0 .. v12}, Lcom/google/android/videos/player/LocalPlaybackHelper;-><init>(Lcom/google/android/videos/VideosGlobals;Landroid/app/Activity;Lcom/google/android/videos/player/LocalPlaybackHelper$Listener;Lcom/google/android/videos/player/PlaybackResumeState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/videos/utils/RetryAction;Landroid/media/MediaRouter$RouteInfo;Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;)V

    iput-object v0, p0, Lcom/google/android/videos/player/Director;->playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

    goto :goto_1
.end method

.method private initKnowledgeCallback()Lcom/google/android/videos/async/Callback;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeRequest;",
            "Lcom/google/android/videos/tagging/KnowledgeBundle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 772
    new-instance v0, Lcom/google/android/videos/player/Director$1;

    invoke-direct {v0, p0}, Lcom/google/android/videos/player/Director$1;-><init>(Lcom/google/android/videos/player/Director;)V

    return-object v0
.end method

.method private initPlaybackInternal()V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v6, 0x0

    const/4 v11, 0x0

    const/4 v5, 0x1

    .line 280
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/videos/player/Director;->createPlaybackHelperIfNull()V

    .line 281
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

    iget-boolean v1, p0, Lcom/google/android/videos/player/Director;->playWhenInitialized:Z

    invoke-interface {v0, v1}, Lcom/google/android/videos/player/PlaybackHelper;->init(Z)V
    :try_end_0
    .catch Lcom/google/android/videos/player/PlaybackHelper$DisplayNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/videos/player/PlaybackHelper$DisplayNotSecureException; {:try_start_0 .. :try_end_0} :catch_1

    .line 295
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->knowledgeViewHelper:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->knowledgeViewHelper:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    invoke-interface {v0, v1}, Lcom/google/android/videos/player/PlaybackHelper;->initKnowledgeViewHelper(Lcom/google/android/videos/tagging/KnowledgeViewHelper;)V

    .line 299
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

    iget-boolean v0, p0, Lcom/google/android/videos/player/Director;->playWhenInitialized:Z

    if-nez v0, :cond_2

    move-object v0, v11

    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/videos/player/PlaybackHelper;->maybeJoinCurrentPlayback(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 300
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    if-eqz v0, :cond_1

    .line 301
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->account:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/store/StoreStatusMonitor;->init(Ljava/lang/String;)V

    .line 303
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    invoke-virtual {v0}, Lcom/google/android/videos/player/PlaybackResumeState;->clearPlaybackError()V

    .line 304
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->videoId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->showId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    :goto_1
    invoke-direct {p0, v0, v5}, Lcom/google/android/videos/player/Director;->requestStoryboards(Ljava/lang/String;Z)V

    .line 305
    invoke-direct {p0}, Lcom/google/android/videos/player/Director;->maybeLoadDashKnowledgeBundle()V

    .line 306
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->videoId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/player/Director;->playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

    invoke-interface {v2}, Lcom/google/android/videos/player/PlaybackHelper;->getDisplayType()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/videos/logging/EventLogger;->onPlaybackInit(Ljava/lang/String;I)V

    .line 307
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/videos/player/Director;->state:I

    .line 333
    :goto_2
    return-void

    .line 282
    :catch_0
    move-exception v8

    .line 286
    .local v8, "e":Lcom/google/android/videos/player/PlaybackHelper$DisplayNotFoundException;
    goto :goto_2

    .line 287
    .end local v8    # "e":Lcom/google/android/videos/player/PlaybackHelper$DisplayNotFoundException;
    :catch_1
    move-exception v8

    .line 288
    .local v8, "e":Lcom/google/android/videos/player/PlaybackHelper$DisplayNotSecureException;
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->videoId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/player/Director;->showId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/player/Director;->seasonId:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/google/android/videos/player/Director;->isTrailer:Z

    const/16 v7, 0x8

    invoke-interface/range {v0 .. v8}, Lcom/google/android/videos/logging/EventLogger;->onPlaybackInitError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZILjava/lang/Exception;)V

    .line 290
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->activity:Landroid/support/v4/app/FragmentActivity;

    const v2, 0x7f0b0153

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v11}, Lcom/google/android/videos/player/PlaybackHelper;->setErrorState(Ljava/lang/String;Lcom/google/android/videos/utils/RetryAction;)V

    .line 291
    iput v12, p0, Lcom/google/android/videos/player/Director;->state:I

    goto :goto_2

    .line 299
    .end local v8    # "e":Lcom/google/android/videos/player/PlaybackHelper$DisplayNotSecureException;
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->videoId:Ljava/lang/String;

    goto :goto_0

    :cond_3
    move v5, v6

    .line 304
    goto :goto_1

    .line 311
    :cond_4
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    invoke-virtual {v0}, Lcom/google/android/videos/player/PlaybackResumeState;->getPlaybackError()Ljava/lang/String;

    move-result-object v9

    .line 312
    .local v9, "lastError":Ljava/lang/String;
    if-eqz v9, :cond_6

    .line 313
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    invoke-virtual {v0}, Lcom/google/android/videos/player/PlaybackResumeState;->wasLastPlaybackErrorFatal()Z

    move-result v0

    if-eqz v0, :cond_5

    move-object v10, v11

    .line 314
    .local v10, "retryAction":Lcom/google/android/videos/utils/RetryAction;
    :goto_3
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

    invoke-interface {v0, v9, v10}, Lcom/google/android/videos/player/PlaybackHelper;->setErrorState(Ljava/lang/String;Lcom/google/android/videos/utils/RetryAction;)V

    .line 315
    iput v12, p0, Lcom/google/android/videos/player/Director;->state:I

    goto :goto_2

    .end local v10    # "retryAction":Lcom/google/android/videos/utils/RetryAction;
    :cond_5
    move-object v10, p0

    .line 313
    goto :goto_3

    .line 319
    :cond_6
    iput v5, p0, Lcom/google/android/videos/player/Director;->state:I

    .line 320
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

    iget-boolean v1, p0, Lcom/google/android/videos/player/Director;->playWhenInitialized:Z

    invoke-interface {v0, v1, v5}, Lcom/google/android/videos/player/PlaybackHelper;->setState(ZZ)V

    .line 322
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    if-eqz v0, :cond_7

    .line 323
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->account:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/store/StoreStatusMonitor;->init(Ljava/lang/String;)V

    .line 328
    :cond_7
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    if-eqz v0, :cond_8

    .line 329
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/Director;->onInitialized(Lcom/google/android/videos/player/DirectorInitializer$InitializationData;)V

    goto :goto_2

    .line 331
    :cond_8
    invoke-direct {p0}, Lcom/google/android/videos/player/Director;->createInitializerIfNull()V

    goto :goto_2
.end method

.method private initStoryboardCallback()Lcom/google/android/videos/async/Callback;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/api/MpdGetRequest;",
            "Lcom/google/android/videos/streams/Streams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 972
    new-instance v0, Lcom/google/android/videos/player/Director$4;

    invoke-direct {v0, p0}, Lcom/google/android/videos/player/Director$4;-><init>(Lcom/google/android/videos/player/Director;)V

    invoke-static {v0}, Lcom/google/android/videos/async/CancelableCallback;->create(Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/CancelableCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/Director;->cancelableStoryboardCallback:Lcom/google/android/videos/async/CancelableCallback;

    .line 984
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->activity:Landroid/support/v4/app/FragmentActivity;

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->cancelableStoryboardCallback:Lcom/google/android/videos/async/CancelableCallback;

    invoke-static {v0, v1}, Lcom/google/android/videos/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/ActivityCallback;

    move-result-object v0

    return-object v0
.end method

.method private isInToddlerMode()Z
    .locals 1

    .prologue
    .line 849
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->activity:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0}, Lcom/google/android/videos/utils/LockTaskModeCompat;->isInLockTaskMode(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private maybeGetKnowledgeBundle(Lcom/google/android/videos/tagging/KnowledgeRequest;)V
    .locals 4
    .param p1, "request"    # Lcom/google/android/videos/tagging/KnowledgeRequest;

    .prologue
    .line 798
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->knowledgeViewHelper:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->preferences:Landroid/content/SharedPreferences;

    const-string v2, "enable_info_cards"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->currentKnowledgeRequest:Lcom/google/android/videos/tagging/KnowledgeRequest;

    invoke-static {p1, v1}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 822
    :cond_0
    :goto_0
    return-void

    .line 803
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->knowledgeViewHelper:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->setKnowledgeBundle(Lcom/google/android/videos/tagging/KnowledgeBundle;)V

    .line 804
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->knowledgeViewHelper:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->onPlayerPlay()V

    .line 805
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->localKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    instance-of v1, v1, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    if-eqz v1, :cond_2

    .line 808
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v1}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->getView()Landroid/view/View;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 810
    :cond_2
    iput-object p1, p0, Lcom/google/android/videos/player/Director;->currentKnowledgeRequest:Lcom/google/android/videos/tagging/KnowledgeRequest;

    .line 811
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Getting knowledge for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 815
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->activity:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, p0, Lcom/google/android/videos/player/Director;->knowledgeCallback:Lcom/google/android/videos/async/Callback;

    invoke-static {v1, v2}, Lcom/google/android/videos/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/ActivityCallback;

    move-result-object v0

    .line 817
    .local v0, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/tagging/KnowledgeRequest;Lcom/google/android/videos/tagging/KnowledgeBundle;>;"
    iget-boolean v1, p0, Lcom/google/android/videos/player/Director;->isRemotePlayback:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    iget v1, v1, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->pinnedStorage:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_3

    .line 818
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->knowledgeClient:Lcom/google/android/videos/tagging/KnowledgeClient;

    iget-object v2, p0, Lcom/google/android/videos/player/Director;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    iget v2, v2, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->pinnedStorage:I

    invoke-interface {v1, p1, v2, v0}, Lcom/google/android/videos/tagging/KnowledgeClient;->requestPinnedKnowledgeBundle(Lcom/google/android/videos/tagging/KnowledgeRequest;ILcom/google/android/videos/async/Callback;)V

    goto :goto_0

    .line 820
    :cond_3
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->knowledgeClient:Lcom/google/android/videos/tagging/KnowledgeClient;

    invoke-interface {v1, p1, v0}, Lcom/google/android/videos/tagging/KnowledgeClient;->requestKnowledgeBundle(Lcom/google/android/videos/tagging/KnowledgeRequest;Lcom/google/android/videos/async/Callback;)V

    goto :goto_0
.end method

.method private maybeLoadDashKnowledgeBundle()V
    .locals 4

    .prologue
    .line 437
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->account:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/videos/player/Director;->useDashStreams:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/player/Director;->isRemotePlayback:Z

    if-eqz v0, :cond_1

    .line 438
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->account:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->videoId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/player/Director;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    iget-object v3, p0, Lcom/google/android/videos/player/Director;->account:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/videos/store/ConfigurationStore;->getPlayCountry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/videos/tagging/KnowledgeRequest;->createForDashWithCurrentLocale(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/tagging/KnowledgeRequest;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/Director;->maybeGetKnowledgeBundle(Lcom/google/android/videos/tagging/KnowledgeRequest;)V

    .line 441
    :cond_1
    return-void
.end method

.method private onInitialized()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 485
    iget v0, p0, Lcom/google/android/videos/player/Director;->state:I

    if-ne v0, v3, :cond_0

    .line 486
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    invoke-interface {v0, v1}, Lcom/google/android/videos/player/PlaybackHelper;->load(Lcom/google/android/videos/player/DirectorInitializer$InitializationData;)V

    .line 487
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->videoId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/player/Director;->playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

    invoke-interface {v2}, Lcom/google/android/videos/player/PlaybackHelper;->getDisplayType()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/videos/logging/EventLogger;->onPlaybackInit(Ljava/lang/String;I)V

    .line 488
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/videos/player/Director;->state:I

    .line 490
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/videos/player/Director;->playWhenInitialized:Z

    if-eqz v0, :cond_1

    .line 491
    invoke-direct {p0, v3}, Lcom/google/android/videos/player/Director;->playVideo(Z)V

    .line 493
    :cond_1
    return-void
.end method

.method private onStoryboards([Lcom/google/wireless/android/video/magma/proto/Storyboard;)V
    .locals 1
    .param p1, "storyboards"    # [Lcom/google/wireless/android/video/magma/proto/Storyboard;

    .prologue
    .line 433
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0, p1}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setStoryboards([Lcom/google/wireless/android/video/magma/proto/Storyboard;)V

    .line 434
    return-void
.end method

.method private pauseVideo()V
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->listener:Lcom/google/android/videos/player/Director$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/player/Director$Listener;->onPlaybackPaused()V

    .line 502
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

    invoke-interface {v0}, Lcom/google/android/videos/player/PlaybackHelper;->pause()V

    .line 503
    return-void
.end method

.method private playVideo(Z)V
    .locals 1
    .param p1, "onInitialized"    # Z

    .prologue
    .line 496
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->listener:Lcom/google/android/videos/player/Director$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/player/Director$Listener;->onPlaybackStarted()V

    .line 497
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

    invoke-interface {v0, p1}, Lcom/google/android/videos/player/PlaybackHelper;->play(Z)V

    .line 498
    return-void
.end method

.method private requestStoryboards(Ljava/lang/String;Z)V
    .locals 8
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "isEpisode"    # Z

    .prologue
    const/4 v4, 0x0

    .line 962
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->cancelableStoryboardCallback:Lcom/google/android/videos/async/CancelableCallback;

    if-eqz v1, :cond_0

    .line 963
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->cancelableStoryboardCallback:Lcom/google/android/videos/async/CancelableCallback;

    invoke-virtual {v1}, Lcom/google/android/videos/async/CancelableCallback;->cancel()V

    .line 964
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/videos/player/Director;->cancelableStoryboardCallback:Lcom/google/android/videos/async/CancelableCallback;

    .line 966
    :cond_0
    new-instance v0, Lcom/google/android/videos/api/MpdGetRequest;

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->account:Ljava/lang/String;

    const/4 v6, 0x1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    move-object v2, p1

    move v3, p2

    move v5, v4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/api/MpdGetRequest;-><init>(Ljava/lang/String;Ljava/lang/String;ZZZZLjava/util/Locale;)V

    .line 968
    .local v0, "request":Lcom/google/android/videos/api/MpdGetRequest;
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->streamsRequester:Lcom/google/android/videos/async/Requester;

    invoke-direct {p0}, Lcom/google/android/videos/player/Director;->initStoryboardCallback()Lcom/google/android/videos/async/Callback;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 969
    return-void
.end method

.method private resetKnowledgeV11()V
    .locals 2

    .prologue
    .line 723
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->knowledgeViewHelper:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->reset()V

    .line 724
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->localKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    instance-of v0, v0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    if-eqz v0, :cond_0

    .line 727
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->getView()Landroid/view/View;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 729
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/player/Director;->currentKnowledgeRequest:Lcom/google/android/videos/tagging/KnowledgeRequest;

    .line 730
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/player/Director;->currentKnowledgeTimeMillis:I

    .line 731
    return-void
.end method

.method private setPlaybackHelperViews()V
    .locals 7

    .prologue
    .line 353
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->playerView:Lcom/google/android/videos/player/PlayerView;

    iget-object v2, p0, Lcom/google/android/videos/player/Director;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    iget-object v3, p0, Lcom/google/android/videos/player/Director;->subtitlesOverlay:Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    iget-object v4, p0, Lcom/google/android/videos/player/Director;->remoteScreenPanelHelper:Lcom/google/android/videos/ui/RemoteScreenPanelHelper;

    iget-object v5, p0, Lcom/google/android/videos/player/Director;->localKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    iget-object v6, p0, Lcom/google/android/videos/player/Director;->localTaglessKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    invoke-interface/range {v0 .. v6}, Lcom/google/android/videos/player/PlaybackHelper;->setViews(Lcom/google/android/videos/player/PlayerView;Lcom/google/android/videos/player/overlay/ControllerOverlay;Lcom/google/android/videos/player/overlay/SubtitlesOverlay;Lcom/google/android/videos/ui/RemoteScreenPanelHelper;Lcom/google/android/videos/tagging/KnowledgeView;Lcom/google/android/videos/tagging/KnowledgeView;)V

    .line 355
    return-void
.end method

.method private setStatusBarTranslucency(Z)V
    .locals 5
    .param p1, "translucent"    # Z

    .prologue
    const/high16 v4, 0x4000000

    .line 908
    sget v2, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v3, 0x13

    if-ge v2, v3, :cond_1

    .line 918
    :cond_0
    :goto_0
    return-void

    .line 911
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/player/Director;->activity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 912
    .local v1, "window":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iget v0, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 913
    .local v0, "flags":I
    if-eqz p1, :cond_2

    and-int v2, v0, v4

    if-nez v2, :cond_2

    .line 914
    invoke-virtual {v1, v4}, Landroid/view/Window;->addFlags(I)V

    goto :goto_0

    .line 915
    :cond_2
    if-nez p1, :cond_0

    and-int v2, v0, v4

    if-eqz v2, :cond_0

    .line 916
    invoke-virtual {v1, v4}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0
.end method

.method private setSystemWindowTranslucency(Z)V
    .locals 3
    .param p1, "setToTranslucent"    # Z

    .prologue
    const/high16 v2, 0xc000000

    .line 254
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    .line 264
    :goto_0
    return-void

    .line 257
    :cond_0
    if-eqz p1, :cond_1

    .line 258
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->activity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Window;->addFlags(I)V

    goto :goto_0

    .line 261
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->activity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0
.end method

.method private showActionBar()V
    .locals 1

    .prologue
    .line 274
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/Director;->onEntityExpandingStateChanged(Z)V

    .line 275
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/Director;->onEntityExpandingStateChanged(Z)V

    .line 276
    return-void
.end method

.method private showShortClockConfirmationDialog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "posterUri"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 444
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->activity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 455
    :goto_0
    return-void

    .line 447
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    invoke-virtual {v1}, Lcom/google/android/videos/player/PlaybackResumeState;->getShortClockDialogConfirmed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 448
    invoke-virtual {p0, v4}, Lcom/google/android/videos/player/Director;->onDialogConfirmed(I)V

    goto :goto_0

    .line 450
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    iget-wide v2, v1, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->shortClockMillis:J

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->activity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v2, v3, v1}, Lcom/google/android/videos/utils/TimeUtil;->getShortClockTimeString(JLandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    .line 452
    .local v0, "message":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v1, v4, p1, v0, p2}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->showShortClockConfirmationDialog(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public initPlayback(Z)V
    .locals 2
    .param p1, "playWhenInitialized"    # Z

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/google/android/videos/player/Director;->showActionBar()V

    .line 244
    iput-boolean p1, p0, Lcom/google/android/videos/player/Director;->playWhenInitialized:Z

    .line 245
    iget v1, p0, Lcom/google/android/videos/player/Director;->playbackType:I

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 246
    .local v0, "isLocal":Z
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->playerView:Lcom/google/android/videos/player/PlayerView;

    invoke-virtual {v1}, Lcom/google/android/videos/player/PlayerView;->getPlayerSurface()Lcom/google/android/videos/player/PlayerSurface;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/videos/player/PlayerSurface;->setVisible(Z)V

    .line 247
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v1, v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setHasSpinner(Z)V

    .line 248
    invoke-direct {p0, v0}, Lcom/google/android/videos/player/Director;->setSystemWindowTranslucency(Z)V

    .line 249
    invoke-direct {p0}, Lcom/google/android/videos/player/Director;->initPlaybackInternal()V

    .line 250
    return-void

    .line 245
    .end local v0    # "isLocal":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isKnowledgeEnabled()Z
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getKnowledgeClient()Lcom/google/android/videos/tagging/KnowledgeClient;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/player/Director;->isTrailer:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)Z
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "result"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 686
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->onActivityResult(IILandroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method public onBackPressed()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 734
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v1}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->onBackPressed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 737
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->localKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->localKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    invoke-interface {v1}, Lcom/google/android/videos/tagging/KnowledgeView;->onBackPressed()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCardDismissed(Lcom/google/android/videos/tagging/CardTag;)V
    .locals 2
    .param p1, "cardTag"    # Lcom/google/android/videos/tagging/CardTag;

    .prologue
    .line 933
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget v1, p1, Lcom/google/android/videos/tagging/CardTag;->cardType:I

    invoke-interface {v0, v1}, Lcom/google/android/videos/logging/EventLogger;->onInfoCardDismissed(I)V

    .line 934
    return-void
.end method

.method public onCardListCollapseProgress(F)V
    .locals 4
    .param p1, "progress"    # F

    .prologue
    .line 894
    const/4 v1, 0x0

    const/high16 v2, 0x3fc00000    # 1.5f

    mul-float/2addr v2, p1

    const/high16 v3, 0x3f000000    # 0.5f

    sub-float/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 895
    .local v0, "alpha":F
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v1}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 896
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v1}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->showControls()V

    .line 897
    return-void
.end method

.method public onCardListCollapsed(I)V
    .locals 2
    .param p1, "eventCause"    # I

    .prologue
    .line 885
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->getView()Landroid/view/View;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 886
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->showControls()V

    .line 887
    if-eqz p1, :cond_0

    .line 888
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    invoke-interface {v0, p1}, Lcom/google/android/videos/logging/EventLogger;->onInfoCardsCollapsed(I)V

    .line 890
    :cond_0
    return-void
.end method

.method public onCardListExpanded(I)V
    .locals 3
    .param p1, "eventCause"    # I

    .prologue
    const/4 v0, 0x0

    .line 875
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v1}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->getView()Landroid/view/View;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 876
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v1, v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->hideControls(Z)V

    .line 877
    if-eqz p1, :cond_1

    .line 878
    iget v1, p0, Lcom/google/android/videos/player/Director;->playedFromMillis:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 879
    .local v0, "isAfterSeek":Z
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    invoke-interface {v1, p1, v0}, Lcom/google/android/videos/logging/EventLogger;->onInfoCardsExpanded(IZ)V

    .line 881
    .end local v0    # "isAfterSeek":Z
    :cond_1
    return-void
.end method

.method public onCardsShown(Z)V
    .locals 1
    .param p1, "hasRecentActorsCard"    # Z

    .prologue
    .line 870
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    invoke-interface {v0, p1}, Lcom/google/android/videos/logging/EventLogger;->onInfoCardsShown(Z)V

    .line 871
    return-void
.end method

.method public onCardsViewScrollChanged(I)V
    .locals 1
    .param p1, "verticalScrollOrigin"    # I

    .prologue
    .line 902
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/videos/player/Director;->setStatusBarTranslucency(Z)V

    .line 903
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->listener:Lcom/google/android/videos/player/Director$Listener;

    invoke-interface {v0, p1}, Lcom/google/android/videos/player/Director$Listener;->onCardsViewScrollChanged(I)V

    .line 904
    return-void

    .line 902
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClickOutsideTags()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 922
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->localKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    invoke-interface {v0, v1}, Lcom/google/android/videos/tagging/KnowledgeView;->setContentVisible(Z)V

    .line 923
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0, v1}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->hideControls(Z)V

    .line 924
    return-void
.end method

.method public onDialogCanceled(I)V
    .locals 3
    .param p1, "dialogId"    # I

    .prologue
    .line 471
    packed-switch p1, :pswitch_data_0

    .line 476
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown dialogId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 473
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->activity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 478
    return-void

    .line 471
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onDialogConfirmed(I)V
    .locals 3
    .param p1, "dialogId"    # I

    .prologue
    .line 459
    packed-switch p1, :pswitch_data_0

    .line 465
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown dialogId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 461
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    invoke-virtual {v0}, Lcom/google/android/videos/player/PlaybackResumeState;->setShortClockDialogConfirmed()V

    .line 462
    invoke-direct {p0}, Lcom/google/android/videos/player/Director;->onInitialized()V

    .line 467
    return-void

    .line 459
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onDialogShown()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 760
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

    invoke-interface {v0, v1, v1}, Lcom/google/android/videos/player/PlaybackHelper;->setState(ZZ)V

    .line 761
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->listener:Lcom/google/android/videos/player/Director$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/player/Director$Listener;->onUserInteractionExpected()V

    .line 762
    return-void
.end method

.method public declared-synchronized onDrmData([BLjava/lang/String;ZI[B)V
    .locals 6
    .param p1, "psshData"    # [B
    .param p2, "mimeType"    # Ljava/lang/String;
    .param p3, "isOffline"    # Z
    .param p4, "cencSecurityLevel"    # I
    .param p5, "cencKeySetId"    # [B

    .prologue
    .line 389
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

    check-cast v0, Lcom/google/android/videos/player/LocalPlaybackHelper;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/videos/player/LocalPlaybackHelper;->openDrm([BLjava/lang/String;ZI[B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 391
    monitor-exit p0

    return-void

    .line 389
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onEntityExpandingStateChanged(Z)V
    .locals 2
    .param p1, "expanded"    # Z

    .prologue
    .line 938
    if-eqz p1, :cond_0

    .line 939
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/google/android/videos/player/Director$2;

    invoke-direct {v1, p0}, Lcom/google/android/videos/player/Director$2;-><init>(Lcom/google/android/videos/player/Director;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 946
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->listener:Lcom/google/android/videos/player/Director$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/player/Director$Listener;->onKnowledgeEnteredFullScreen()V

    .line 957
    :goto_0
    return-void

    .line 948
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/google/android/videos/player/Director$3;

    invoke-direct {v1, p0}, Lcom/google/android/videos/player/Director$3;-><init>(Lcom/google/android/videos/player/Director;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 955
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->listener:Lcom/google/android/videos/player/Director$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/player/Director$Listener;->onKnowledgeExitedFullScreen()V

    goto :goto_0
.end method

.method public onError(ZILjava/lang/String;Ljava/lang/Exception;)V
    .locals 10
    .param p1, "canRetry"    # Z
    .param p2, "playbackInitError"    # I
    .param p3, "errorMessage"    # Ljava/lang/String;
    .param p4, "e"    # Ljava/lang/Exception;

    .prologue
    const/4 v9, 0x0

    .line 376
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->videoId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/player/Director;->showId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/player/Director;->seasonId:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/google/android/videos/player/Director;->isTrailer:Z

    iget-boolean v5, p0, Lcom/google/android/videos/player/Director;->playWhenInitialized:Z

    move v6, p1

    move v7, p2

    move-object v8, p4

    invoke-interface/range {v0 .. v8}, Lcom/google/android/videos/logging/EventLogger;->onPlaybackInitError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZILjava/lang/Exception;)V

    .line 378
    iget-boolean v0, p0, Lcom/google/android/videos/player/Director;->playWhenInitialized:Z

    if-eqz v0, :cond_0

    .line 379
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

    if-eqz p1, :cond_1

    move-object v0, p0

    :goto_0
    invoke-interface {v1, p3, v0}, Lcom/google/android/videos/player/PlaybackHelper;->setErrorState(Ljava/lang/String;Lcom/google/android/videos/utils/RetryAction;)V

    .line 381
    :cond_0
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/videos/player/Director;->state:I

    .line 382
    iput-object v9, p0, Lcom/google/android/videos/player/Director;->initializer:Lcom/google/android/videos/player/DirectorInitializer;

    .line 383
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->listener:Lcom/google/android/videos/player/Director$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/player/Director$Listener;->onPlaybackError()V

    .line 384
    return-void

    :cond_1
    move-object v0, v9

    .line 379
    goto :goto_0
.end method

.method public onExitRemotePlayback()V
    .locals 1

    .prologue
    .line 516
    const-string v0, "Director reset from onExitRemotePlayback"

    invoke-static {v0}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 517
    invoke-virtual {p0}, Lcom/google/android/videos/player/Director;->reset()V

    .line 518
    const-string v0, "Disconnected from remote device. Closing activity."

    invoke-static {v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 519
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->activity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 520
    return-void
.end method

.method public onExpandRecentActors()V
    .locals 1

    .prologue
    .line 928
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    invoke-interface {v0}, Lcom/google/android/videos/logging/EventLogger;->onExpandRecentActors()V

    .line 929
    return-void
.end method

.method public onHQ()V
    .locals 2

    .prologue
    .line 630
    iget v0, p0, Lcom/google/android/videos/player/Director;->state:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 631
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

    invoke-interface {v0}, Lcom/google/android/videos/player/PlaybackHelper;->onHQ()V

    .line 633
    :cond_0
    return-void
.end method

.method public onHidden()V
    .locals 1

    .prologue
    .line 644
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->localKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/player/Director;->localKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    invoke-interface {v0}, Lcom/google/android/videos/tagging/KnowledgeView;->isInteracting()Z

    move-result v0

    if-nez v0, :cond_1

    .line 645
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->listener:Lcom/google/android/videos/player/Director$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/player/Director$Listener;->onUserInteractionNotExpected()V

    .line 647
    :cond_1
    return-void
.end method

.method public onHidePending()V
    .locals 1

    .prologue
    .line 637
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->localKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/player/Director;->localKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    invoke-interface {v0}, Lcom/google/android/videos/tagging/KnowledgeView;->isInteracting()Z

    move-result v0

    if-nez v0, :cond_1

    .line 638
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->listener:Lcom/google/android/videos/player/Director$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/player/Director$Listener;->onUserInteractionEnding()V

    .line 640
    :cond_1
    return-void
.end method

.method public declared-synchronized onInitialized(Lcom/google/android/videos/player/DirectorInitializer$InitializationData;)V
    .locals 5
    .param p1, "data"    # Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    .prologue
    .line 395
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/videos/player/Director;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    .line 396
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playerView:Lcom/google/android/videos/player/PlayerView;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 426
    :goto_0
    monitor-exit p0

    return-void

    .line 402
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->videoId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/player/Director;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    iget v2, v2, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->durationMillis:I

    iget-object v3, p0, Lcom/google/android/videos/player/Director;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    iget v3, v3, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->resumeTimeMillis:I

    iget-object v4, p0, Lcom/google/android/videos/player/Director;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    iget-object v4, v4, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->videoTitle:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setVideoInfo(Ljava/lang/String;IILjava/lang/String;)V

    .line 404
    invoke-direct {p0}, Lcom/google/android/videos/player/Director;->maybeLoadDashKnowledgeBundle()V

    .line 405
    iget-boolean v0, p0, Lcom/google/android/videos/player/Director;->isRemotePlayback:Z

    if-nez v0, :cond_1

    .line 408
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->listener:Lcom/google/android/videos/player/Director$Listener;

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    iget-object v1, v1, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->videoTitle:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/videos/player/Director$Listener;->onVideoTitle(Ljava/lang/String;)V

    .line 410
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    iget-boolean v0, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->shortClockNotActivated:Z

    if-eqz v0, :cond_2

    .line 414
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    iget-object v0, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->videoTitle:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    iget-object v1, v1, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->posterUri:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/player/Director;->showShortClockConfirmationDialog(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/videos/player/Director;->isRemotePlayback:Z

    if-eqz v0, :cond_4

    .line 422
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->videoId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/videos/player/Director;->showId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    invoke-direct {p0, v1, v0}, Lcom/google/android/videos/player/Director;->requestStoryboards(Ljava/lang/String;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 395
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 416
    :cond_2
    :try_start_2
    invoke-direct {p0}, Lcom/google/android/videos/player/Director;->onInitialized()V

    goto :goto_1

    .line 422
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 424
    :cond_4
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    iget-object v0, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->streams:Lcom/google/android/videos/streams/Streams;

    if-nez v0, :cond_5

    const/4 v0, 0x0

    :goto_3
    invoke-direct {p0, v0}, Lcom/google/android/videos/player/Director;->onStoryboards([Lcom/google/wireless/android/video/magma/proto/Storyboard;)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    iget-object v0, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->streams:Lcom/google/android/videos/streams/Streams;

    iget-object v0, v0, Lcom/google/android/videos/streams/Streams;->storyboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 678
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0, p1, p2}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 682
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0, p1, p2}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onLegacyStreamSelected(Lcom/google/android/videos/streams/MediaStream;)V
    .locals 4
    .param p1, "stream"    # Lcom/google/android/videos/streams/MediaStream;

    .prologue
    .line 752
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->account:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 753
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->account:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->videoId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/player/Director;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    iget-object v3, p0, Lcom/google/android/videos/player/Director;->account:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/videos/store/ConfigurationStore;->getPlayCountry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2, p1}, Lcom/google/android/videos/tagging/KnowledgeRequest;->createWithCurrentLocale(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/streams/MediaStream;)Lcom/google/android/videos/tagging/KnowledgeRequest;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/Director;->maybeGetKnowledgeBundle(Lcom/google/android/videos/tagging/KnowledgeRequest;)V

    .line 756
    :cond_0
    return-void
.end method

.method public onLocalPlaybackError()V
    .locals 1

    .prologue
    .line 766
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->listener:Lcom/google/android/videos/player/Director$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/player/Director$Listener;->onPlaybackError()V

    .line 767
    return-void
.end method

.method public onNotAuthenticated()V
    .locals 3

    .prologue
    .line 370
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->activity:Landroid/support/v4/app/FragmentActivity;

    const v1, 0x7f0b0145

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/videos/utils/Util;->showToast(Landroid/content/Context;II)V

    .line 371
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->activity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 372
    return-void
.end method

.method public onOrientationChanged()V
    .locals 2

    .prologue
    .line 741
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->onOrientationChanged()V

    .line 742
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->dragPromoOverlay:Lcom/google/android/videos/player/overlay/DragPromoOverlay;

    invoke-virtual {v0}, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->onOrientationChanged()V

    .line 743
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->knowledgeViewHelper:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/videos/player/Director;->state:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 744
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->knowledgeViewHelper:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->localKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->onOrientationChanged(Lcom/google/android/videos/tagging/KnowledgeView;)V

    .line 746
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 549
    iget v0, p0, Lcom/google/android/videos/player/Director;->state:I

    packed-switch v0, :pswitch_data_0

    .line 559
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onPause called in unexpected state "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/videos/player/Director;->state:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 561
    :goto_0
    return-void

    .line 551
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/videos/player/Director;->pauseVideo()V

    goto :goto_0

    .line 554
    :pswitch_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/player/Director;->playWhenInitialized:Z

    .line 555
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

    iget-boolean v1, p0, Lcom/google/android/videos/player/Director;->playWhenInitialized:Z

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/google/android/videos/player/PlaybackHelper;->setState(ZZ)V

    goto :goto_0

    .line 549
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onPlay()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 526
    iget v0, p0, Lcom/google/android/videos/player/Director;->state:I

    packed-switch v0, :pswitch_data_0

    .line 543
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onPlay called in unexpected state "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/videos/player/Director;->state:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 545
    :goto_0
    return-void

    .line 528
    :pswitch_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/Director;->playVideo(Z)V

    goto :goto_0

    .line 531
    :pswitch_1
    iput-boolean v2, p0, Lcom/google/android/videos/player/Director;->playWhenInitialized:Z

    .line 532
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

    iget-boolean v1, p0, Lcom/google/android/videos/player/Director;->playWhenInitialized:Z

    invoke-interface {v0, v1, v2}, Lcom/google/android/videos/player/PlaybackHelper;->setState(ZZ)V

    goto :goto_0

    .line 535
    :pswitch_2
    const-string v0, "Director reset from onPlay:STATE_ERROR"

    invoke-static {v0}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 536
    invoke-virtual {p0}, Lcom/google/android/videos/player/Director;->reset()V

    .line 537
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    invoke-virtual {v0}, Lcom/google/android/videos/player/PlaybackResumeState;->clearPlaybackError()V

    .line 538
    iput-boolean v2, p0, Lcom/google/android/videos/player/Director;->playWhenInitialized:Z

    .line 539
    invoke-direct {p0}, Lcom/google/android/videos/player/Director;->initPlaybackInternal()V

    goto :goto_0

    .line 526
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onPlayerPause(III)V
    .locals 3
    .param p1, "frameTimestampMs"    # I
    .param p2, "knowledgeDisplayWidth"    # I
    .param p3, "knowledgeDisplayHeight"    # I

    .prologue
    const/4 v1, -0x1

    .line 834
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->knowledgeViewHelper:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/videos/player/Director;->isInToddlerMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 846
    :cond_0
    :goto_0
    return-void

    .line 837
    :cond_1
    iget v0, p0, Lcom/google/android/videos/player/Director;->playedFromMillis:I

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/google/android/videos/player/Director;->playedFromMillis:I

    if-le v0, p1, :cond_3

    .line 838
    :cond_2
    iput p1, p0, Lcom/google/android/videos/player/Director;->playedFromMillis:I

    .line 840
    :cond_3
    iput p1, p0, Lcom/google/android/videos/player/Director;->currentKnowledgeTimeMillis:I

    .line 841
    iget v0, p0, Lcom/google/android/videos/player/Director;->playedFromMillis:I

    if-ne v0, v1, :cond_4

    const/4 v0, 0x0

    :goto_1
    iput v0, p0, Lcom/google/android/videos/player/Director;->showRecentActorsWithinMillis:I

    .line 844
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->knowledgeViewHelper:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    iget v1, p0, Lcom/google/android/videos/player/Director;->currentKnowledgeTimeMillis:I

    iget v2, p0, Lcom/google/android/videos/player/Director;->showRecentActorsWithinMillis:I

    invoke-virtual {v0, v1, v2, p2, p3}, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->onPlayerPause(IIII)V

    goto :goto_0

    .line 841
    :cond_4
    iget v0, p0, Lcom/google/android/videos/player/Director;->playedFromMillis:I

    sub-int v0, p1, v0

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->config:Lcom/google/android/videos/Config;

    invoke-interface {v1}, Lcom/google/android/videos/Config;->knowledgeShowRecentActorsWithinMillis()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_1
.end method

.method public onPlayerPlay()V
    .locals 1

    .prologue
    .line 826
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->knowledgeViewHelper:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    if-eqz v0, :cond_0

    .line 827
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->knowledgeViewHelper:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->onPlayerPlay()V

    .line 829
    :cond_0
    return-void
.end method

.method public onPlayerScrubbingStart()V
    .locals 1

    .prologue
    .line 861
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->knowledgeViewHelper:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    if-eqz v0, :cond_0

    .line 862
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->knowledgeViewHelper:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->onPlayerStartScrubbing()V

    .line 864
    :cond_0
    return-void
.end method

.method public onPlayerStop()V
    .locals 1

    .prologue
    .line 854
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->knowledgeViewHelper:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    if-eqz v0, :cond_0

    .line 855
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->knowledgeViewHelper:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->onPlayerStop()V

    .line 857
    :cond_0
    return-void
.end method

.method public onRemoteVideoTitleChanged(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 509
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->listener:Lcom/google/android/videos/player/Director$Listener;

    invoke-interface {v0, p1}, Lcom/google/android/videos/player/Director$Listener;->onVideoTitle(Ljava/lang/String;)V

    .line 510
    return-void
.end method

.method public onRetry()V
    .locals 2

    .prologue
    .line 565
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    invoke-virtual {v0}, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->setHasFailures()V

    .line 566
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->flushAndRelease(Lcom/google/android/videos/logging/EventLogger;)V

    .line 567
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    invoke-virtual {v0}, Lcom/google/android/videos/player/PlaybackResumeState;->clearPlaybackError()V

    .line 568
    iget v0, p0, Lcom/google/android/videos/player/Director;->state:I

    packed-switch v0, :pswitch_data_0

    .line 580
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onRetry called in unexpected state "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/videos/player/Director;->state:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 582
    :goto_0
    return-void

    .line 570
    :pswitch_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/Director;->playVideo(Z)V

    goto :goto_0

    .line 573
    :pswitch_1
    const-string v0, "Director reset from onRetry:STATE_ERROR"

    invoke-static {v0}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 574
    invoke-virtual {p0}, Lcom/google/android/videos/player/Director;->reset()V

    .line 575
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/player/Director;->playWhenInitialized:Z

    .line 576
    invoke-direct {p0}, Lcom/google/android/videos/player/Director;->initPlaybackInternal()V

    goto :goto_0

    .line 568
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onScrubbingCanceled()V
    .locals 2

    .prologue
    .line 620
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

    invoke-interface {v0}, Lcom/google/android/videos/player/PlaybackHelper;->getDisplayType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/player/Director;->isTv:Z

    if-nez v0, :cond_0

    .line 621
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->dragPromoOverlay:Lcom/google/android/videos/player/overlay/DragPromoOverlay;

    invoke-virtual {v0}, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->onScrubbingCanceled()V

    .line 623
    :cond_0
    iget v0, p0, Lcom/google/android/videos/player/Director;->state:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 624
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

    invoke-interface {v0}, Lcom/google/android/videos/player/PlaybackHelper;->onScrubbingCanceled()V

    .line 626
    :cond_1
    return-void
.end method

.method public onScrubbingStart(Z)V
    .locals 2
    .param p1, "isFineScrubbing"    # Z

    .prologue
    .line 591
    iget v0, p0, Lcom/google/android/videos/player/Director;->state:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 592
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

    invoke-interface {v0, p1}, Lcom/google/android/videos/player/PlaybackHelper;->onScrubbingStart(Z)V

    .line 594
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

    invoke-interface {v0}, Lcom/google/android/videos/player/PlaybackHelper;->getDisplayType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/player/Director;->isTv:Z

    if-nez v0, :cond_0

    .line 595
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->dragPromoOverlay:Lcom/google/android/videos/player/overlay/DragPromoOverlay;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->onScrubbingStart(Z)V

    .line 598
    :cond_0
    return-void
.end method

.method public onSeekTo(ZIZ)V
    .locals 2
    .param p1, "isFineScrubbing"    # Z
    .param p2, "time"    # I
    .param p3, "scrubbingEnded"    # Z

    .prologue
    .line 602
    iget v0, p0, Lcom/google/android/videos/player/Director;->state:I

    if-nez v0, :cond_1

    .line 616
    :cond_0
    :goto_0
    return-void

    .line 605
    :cond_1
    if-eqz p3, :cond_2

    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

    invoke-interface {v0}, Lcom/google/android/videos/player/PlaybackHelper;->getDisplayType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/google/android/videos/player/Director;->isTv:Z

    if-nez v0, :cond_2

    .line 606
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->dragPromoOverlay:Lcom/google/android/videos/player/overlay/DragPromoOverlay;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->onScrubbingEnd(Z)V

    .line 608
    :cond_2
    iget v0, p0, Lcom/google/android/videos/player/Director;->state:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 609
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/player/Director;->playedFromMillis:I

    .line 610
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/videos/player/PlaybackHelper;->onSeekTo(ZIZ)V

    goto :goto_0

    .line 611
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    if-eqz v0, :cond_0

    .line 614
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    invoke-virtual {v0, p2}, Lcom/google/android/videos/player/PlaybackResumeState;->setResumeTimeMillis(I)V

    goto :goto_0
.end method

.method public onSelectAudioTrackIndex(I)V
    .locals 2
    .param p1, "trackIndex"    # I

    .prologue
    .line 670
    iget v0, p0, Lcom/google/android/videos/player/Director;->state:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 671
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

    invoke-interface {v0, p1}, Lcom/google/android/videos/player/PlaybackHelper;->onSelectedAudioTrackIndexChanged(I)V

    .line 673
    :cond_0
    return-void
.end method

.method public onSelectSubtitleTrack(Lcom/google/android/videos/subtitles/SubtitleTrack;)V
    .locals 2
    .param p1, "track"    # Lcom/google/android/videos/subtitles/SubtitleTrack;

    .prologue
    .line 659
    iget v0, p0, Lcom/google/android/videos/player/Director;->state:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 660
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/player/Director;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    iget-boolean v0, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->haveAudioInDeviceLanguage:Z

    if-nez v0, :cond_0

    .line 661
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->preferences:Landroid/content/SharedPreferences;

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lcom/google/android/videos/subtitles/TrackSelectionUtil;->persistSelectSubtitleWhenNoAudioInDeviceLanguage(Landroid/content/SharedPreferences;Z)V

    .line 664
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

    invoke-interface {v0, p1}, Lcom/google/android/videos/player/PlaybackHelper;->onSelectedSubtitleTrackChanged(Lcom/google/android/videos/subtitles/SubtitleTrack;)V

    .line 666
    :cond_1
    return-void

    .line 661
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onShown()V
    .locals 2

    .prologue
    .line 651
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->localKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    if-eqz v0, :cond_0

    .line 652
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->localKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/videos/tagging/KnowledgeView;->setContentVisible(Z)V

    .line 654
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->listener:Lcom/google/android/videos/player/Director$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/player/Director$Listener;->onUserInteractionExpected()V

    .line 655
    return-void
.end method

.method public onUserLeaveHint()V
    .locals 1

    .prologue
    .line 429
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->stopSeek()V

    .line 430
    return-void
.end method

.method public reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 693
    iget v0, p0, Lcom/google/android/videos/player/Director;->state:I

    if-nez v0, :cond_0

    .line 719
    :goto_0
    return-void

    .line 699
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->cancelableStoryboardCallback:Lcom/google/android/videos/async/CancelableCallback;

    if-eqz v0, :cond_1

    .line 700
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->cancelableStoryboardCallback:Lcom/google/android/videos/async/CancelableCallback;

    invoke-virtual {v0}, Lcom/google/android/videos/async/CancelableCallback;->cancel()V

    .line 701
    iput-object v1, p0, Lcom/google/android/videos/player/Director;->cancelableStoryboardCallback:Lcom/google/android/videos/async/CancelableCallback;

    .line 703
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/player/Director;->state:I

    .line 704
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/player/Director;->playedFromMillis:I

    .line 705
    iput-object v1, p0, Lcom/google/android/videos/player/Director;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    .line 706
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->initializer:Lcom/google/android/videos/player/DirectorInitializer;

    if-eqz v0, :cond_2

    .line 707
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->initializer:Lcom/google/android/videos/player/DirectorInitializer;

    invoke-virtual {v0}, Lcom/google/android/videos/player/DirectorInitializer;->release()V

    .line 708
    iput-object v1, p0, Lcom/google/android/videos/player/Director;->initializer:Lcom/google/android/videos/player/DirectorInitializer;

    .line 710
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

    invoke-interface {v0}, Lcom/google/android/videos/player/PlaybackHelper;->release()V

    .line 711
    iput-object v1, p0, Lcom/google/android/videos/player/Director;->playbackHelper:Lcom/google/android/videos/player/PlaybackHelper;

    .line 712
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->knowledgeViewHelper:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    if-eqz v0, :cond_3

    .line 713
    invoke-direct {p0}, Lcom/google/android/videos/player/Director;->resetKnowledgeV11()V

    .line 715
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    if-eqz v0, :cond_4

    .line 716
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    invoke-virtual {v0}, Lcom/google/android/videos/store/StoreStatusMonitor;->reset()V

    .line 718
    :cond_4
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->flushAndRelease(Lcom/google/android/videos/logging/EventLogger;)V

    goto :goto_0
.end method

.method public setViewsAndOverlays(Lcom/google/android/videos/player/PlayerView;Lcom/google/android/videos/player/overlay/ControllerOverlay;Lcom/google/android/videos/player/overlay/SubtitlesOverlay;Landroid/view/View;Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;Lcom/google/android/videos/tagging/KnowledgeView;Lcom/google/android/videos/tagging/KnowledgeView;Lcom/google/android/videos/player/overlay/DragPromoOverlay;)V
    .locals 11
    .param p1, "playerView"    # Lcom/google/android/videos/player/PlayerView;
    .param p2, "controllerOverlay"    # Lcom/google/android/videos/player/overlay/ControllerOverlay;
    .param p3, "subtitlesOverlay"    # Lcom/google/android/videos/player/overlay/SubtitlesOverlay;
    .param p4, "remoteScreenPanel"    # Landroid/view/View;
    .param p5, "interactiveKnowledgeOverlay"    # Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;
    .param p6, "localKnowledgeView"    # Lcom/google/android/videos/tagging/KnowledgeView;
    .param p7, "localTaglessKnowledgeView"    # Lcom/google/android/videos/tagging/KnowledgeView;
    .param p8, "dragPromoOverlay"    # Lcom/google/android/videos/player/overlay/DragPromoOverlay;

    .prologue
    .line 194
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->playerView:Lcom/google/android/videos/player/PlayerView;

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 195
    iput-object p1, p0, Lcom/google/android/videos/player/Director;->playerView:Lcom/google/android/videos/player/PlayerView;

    .line 196
    iput-object p2, p0, Lcom/google/android/videos/player/Director;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    .line 197
    invoke-interface {p2, p0}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setListener(Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;)V

    .line 198
    iput-object p3, p0, Lcom/google/android/videos/player/Director;->subtitlesOverlay:Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    .line 199
    if-eqz p4, :cond_0

    .line 200
    new-instance v1, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;

    iget-object v2, p0, Lcom/google/android/videos/player/Director;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getPosterStore()Lcom/google/android/videos/store/PosterStore;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/player/Director;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v3}, Lcom/google/android/videos/VideosGlobals;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v3

    move-object/from16 v0, p5

    invoke-direct {v1, v0, v2, v3, p4}, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;-><init>(Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;Lcom/google/android/videos/store/PosterStore;Lcom/google/android/videos/store/Database;Landroid/view/View;)V

    iput-object v1, p0, Lcom/google/android/videos/player/Director;->remoteScreenPanelHelper:Lcom/google/android/videos/ui/RemoteScreenPanelHelper;

    .line 204
    :cond_0
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/videos/player/Director;->localKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    .line 205
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/videos/player/Director;->localTaglessKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    .line 207
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getKnowledgeClient()Lcom/google/android/videos/tagging/KnowledgeClient;

    move-result-object v10

    .line 208
    .local v10, "knowledgeClient":Lcom/google/android/videos/tagging/KnowledgeClient;
    if-eqz v10, :cond_1

    iget-boolean v1, p0, Lcom/google/android/videos/player/Director;->isTrailer:Z

    if-nez v1, :cond_1

    .line 209
    iput-object v10, p0, Lcom/google/android/videos/player/Director;->knowledgeClient:Lcom/google/android/videos/tagging/KnowledgeClient;

    .line 210
    invoke-direct {p0}, Lcom/google/android/videos/player/Director;->initKnowledgeCallback()Lcom/google/android/videos/async/Callback;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/player/Director;->knowledgeCallback:Lcom/google/android/videos/async/Callback;

    .line 211
    new-instance v1, Lcom/google/android/videos/store/StoreStatusMonitor;

    iget-object v2, p0, Lcom/google/android/videos/player/Director;->activity:Landroid/support/v4/app/FragmentActivity;

    iget-object v3, p0, Lcom/google/android/videos/player/Director;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v3}, Lcom/google/android/videos/VideosGlobals;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/videos/player/Director;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStore()Lcom/google/android/videos/store/PurchaseStore;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/videos/player/Director;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v5}, Lcom/google/android/videos/VideosGlobals;->getWishlistStore()Lcom/google/android/videos/store/WishlistStore;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/videos/store/StoreStatusMonitor;-><init>(Landroid/content/Context;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/WishlistStore;)V

    iput-object v1, p0, Lcom/google/android/videos/player/Director;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    .line 213
    new-instance v1, Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    invoke-direct {v1}, Lcom/google/android/videos/tagging/KnowledgeViewHelper;-><init>()V

    iput-object v1, p0, Lcom/google/android/videos/player/Director;->knowledgeViewHelper:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    .line 215
    if-eqz p5, :cond_1

    .line 216
    move-object/from16 v0, p5

    invoke-virtual {v0, p0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->setListener(Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;)V

    .line 217
    iget-object v2, p0, Lcom/google/android/videos/player/Director;->activity:Landroid/support/v4/app/FragmentActivity;

    iget-object v3, p0, Lcom/google/android/videos/player/Director;->account:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/videos/player/Director;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v5

    invoke-interface {v10}, Lcom/google/android/videos/tagging/KnowledgeClient;->getFeedbackClient()Lcom/google/android/videos/tagging/FeedbackClient;

    move-result-object v6

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->config:Lcom/google/android/videos/Config;

    invoke-interface {v1}, Lcom/google/android/videos/Config;->feedbackProductId()I

    move-result v7

    iget-object v1, p0, Lcom/google/android/videos/player/Director;->config:Lcom/google/android/videos/Config;

    invoke-interface {v1}, Lcom/google/android/videos/Config;->knowledgeFeedbackTypeId()I

    move-result v8

    iget-object v9, p0, Lcom/google/android/videos/player/Director;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    move-object/from16 v1, p5

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->init(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/videos/store/StoreStatusMonitor;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/videos/tagging/FeedbackClient;IILcom/google/android/videos/logging/EventLogger;)V

    .line 223
    :cond_1
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/videos/player/Director;->dragPromoOverlay:Lcom/google/android/videos/player/overlay/DragPromoOverlay;

    .line 225
    invoke-direct {p0}, Lcom/google/android/videos/player/Director;->setPlaybackHelperViews()V

    .line 227
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    if-eqz v1, :cond_2

    .line 230
    iget-object v1, p0, Lcom/google/android/videos/player/Director;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    invoke-virtual {p0, v1}, Lcom/google/android/videos/player/Director;->onInitialized(Lcom/google/android/videos/player/DirectorInitializer$InitializationData;)V

    .line 232
    :cond_2
    return-void

    .line 194
    .end local v10    # "knowledgeClient":Lcom/google/android/videos/tagging/KnowledgeClient;
    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method public showControls()V
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->showControls()V

    .line 240
    return-void
.end method

.method public showingChildActivity()Z
    .locals 1

    .prologue
    .line 481
    iget-object v0, p0, Lcom/google/android/videos/player/Director;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->showingChildActivity()Z

    move-result v0

    return v0
.end method
