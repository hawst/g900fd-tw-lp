.class public interface abstract Lcom/google/android/videos/player/VideosPlayer;
.super Ljava/lang/Object;
.source "VideosPlayer.java"


# virtual methods
.method public abstract getAudioTracks()[Lcom/google/wireless/android/video/magma/proto/AudioInfo;
.end method

.method public abstract getBufferedPercentage()I
.end method

.method public abstract getCurrentPosition()I
.end method

.method public abstract getHq()Z
.end method

.method public abstract getPlayWhenReady()Z
.end method

.method public abstract getPlaybackState()I
.end method

.method public abstract prepare(Lcom/google/android/videos/player/VideoInfo;)V
.end method

.method public abstract release()V
.end method

.method public abstract seekTo(I)V
.end method

.method public abstract seekTo(IZ)V
.end method

.method public abstract setHq(Z)V
.end method

.method public abstract setPlayWhenReady(Z)V
.end method

.method public abstract setSelectedAudioTrack(I)V
.end method

.method public abstract setSubtitles(Lcom/google/android/videos/subtitles/Subtitles;)V
.end method

.method public abstract setTrickPlayEnabled(Z)V
.end method

.method public abstract setViews(Lcom/google/android/videos/player/PlayerSurface;Lcom/google/android/videos/player/overlay/SubtitlesOverlay;)V
.end method
