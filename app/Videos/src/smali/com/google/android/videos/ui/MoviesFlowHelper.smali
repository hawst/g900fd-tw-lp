.class public Lcom/google/android/videos/ui/MoviesFlowHelper;
.super Landroid/database/DataSetObserver;
.source "MoviesFlowHelper.java"

# interfaces
.implements Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;


# instance fields
.field private final downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

.field private final flow:Lcom/google/android/videos/flow/Flow;

.field private final libraryDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

.field private final libraryFlow:Lcom/google/android/videos/flow/Flow;

.field private final noDownloadedContentFlow:Lcom/google/android/videos/flow/Flow;

.field private final preorderDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

.field private final preorderFlow:Lcom/google/android/videos/flow/Flow;

.field private final welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/videos/utils/DownloadedOnlyManager;Lcom/google/android/videos/welcome/WelcomeFlow;Lcom/google/android/videos/adapter/MoviesDataSource;Lcom/google/android/videos/adapter/MoviesDataSource;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;Lcom/google/android/videos/ui/playnext/MoviePreorderClusterItemView$Binder;Lcom/google/android/videos/ui/playnext/MovieClusterItemView$Binder;)V
    .locals 9
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "downloadedOnlyManager"    # Lcom/google/android/videos/utils/DownloadedOnlyManager;
    .param p3, "welcomeFlow"    # Lcom/google/android/videos/welcome/WelcomeFlow;
    .param p4, "libraryDataSource"    # Lcom/google/android/videos/adapter/MoviesDataSource;
    .param p5, "preorderDataSource"    # Lcom/google/android/videos/adapter/MoviesDataSource;
    .param p6, "preorderClickListener"    # Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;
    .param p7, "assetClickListener"    # Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;
    .param p8, "preorderBinder"    # Lcom/google/android/videos/ui/playnext/MoviePreorderClusterItemView$Binder;
    .param p9, "movieBinder"    # Lcom/google/android/videos/ui/playnext/MovieClusterItemView$Binder;

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    .line 47
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    iput-object p2, p0, Lcom/google/android/videos/ui/MoviesFlowHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    .line 49
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/welcome/WelcomeFlow;

    iput-object v0, p0, Lcom/google/android/videos/ui/MoviesFlowHelper;->welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;

    .line 50
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/adapter/MoviesDataSource;

    iput-object v0, p0, Lcom/google/android/videos/ui/MoviesFlowHelper;->libraryDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    .line 51
    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/adapter/MoviesDataSource;

    iput-object v0, p0, Lcom/google/android/videos/ui/MoviesFlowHelper;->preorderDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    .line 52
    invoke-static {p6}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    invoke-static/range {p7 .. p7}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    invoke-static/range {p8 .. p8}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    invoke-static/range {p9 .. p9}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    new-instance v0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;

    const/4 v1, 0x0

    const v3, 0x7f040028

    const/4 v6, -0x1

    move-object v2, p4

    move-object/from16 v4, p9

    move-object/from16 v5, p7

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;-><init>(Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;Lcom/google/android/videos/adapter/DataSource;ILcom/google/android/videos/ui/playnext/ClusterItemView$Binder;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;I)V

    iput-object v0, p0, Lcom/google/android/videos/ui/MoviesFlowHelper;->libraryFlow:Lcom/google/android/videos/flow/Flow;

    .line 65
    new-instance v8, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;

    new-instance v0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;

    const v2, 0x7f0b00bf

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;-><init>(Landroid/content/Context;IIILandroid/view/View$OnClickListener;ILjava/lang/Object;)V

    const v4, 0x7f040026

    const/4 v7, -0x1

    move-object v1, v8

    move-object v2, v0

    move-object v3, p5

    move-object/from16 v5, p8

    move-object v6, p6

    invoke-direct/range {v1 .. v7}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;-><init>(Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;Lcom/google/android/videos/adapter/DataSource;ILcom/google/android/videos/ui/playnext/ClusterItemView$Binder;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;I)V

    iput-object v8, p0, Lcom/google/android/videos/ui/MoviesFlowHelper;->preorderFlow:Lcom/google/android/videos/flow/Flow;

    .line 73
    new-instance v0, Lcom/google/android/videos/ui/NoDownloadedContentFlow;

    invoke-static {p1}, Lcom/google/android/videos/activity/MyLibraryFragment;->getExtendedActionBarHeight(Landroid/content/Context;)I

    move-result v1

    invoke-direct {v0, v1}, Lcom/google/android/videos/ui/NoDownloadedContentFlow;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/videos/ui/MoviesFlowHelper;->noDownloadedContentFlow:Lcom/google/android/videos/flow/Flow;

    .line 76
    invoke-direct {p0}, Lcom/google/android/videos/ui/MoviesFlowHelper;->updateVisibilities()V

    .line 78
    new-instance v0, Lcom/google/android/videos/flow/SequentialFlow;

    const/4 v1, 0x5

    new-array v1, v1, [Lcom/google/android/videos/flow/Flow;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/videos/ui/MoviesFlowHelper;->noDownloadedContentFlow:Lcom/google/android/videos/flow/Flow;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Lcom/google/android/videos/ui/PlayListSpacerFlow;

    const/4 v4, 0x0

    const/4 v5, 0x3

    invoke-direct {v3, v4, v5}, Lcom/google/android/videos/ui/PlayListSpacerFlow;-><init>(II)V

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object p3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/videos/ui/MoviesFlowHelper;->libraryFlow:Lcom/google/android/videos/flow/Flow;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/videos/ui/MoviesFlowHelper;->preorderFlow:Lcom/google/android/videos/flow/Flow;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/google/android/videos/flow/SequentialFlow;-><init>([Lcom/google/android/videos/flow/Flow;)V

    iput-object v0, p0, Lcom/google/android/videos/ui/MoviesFlowHelper;->flow:Lcom/google/android/videos/flow/Flow;

    .line 83
    return-void
.end method

.method private updateVisibilities()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 110
    iget-object v5, p0, Lcom/google/android/videos/ui/MoviesFlowHelper;->libraryDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v5}, Lcom/google/android/videos/adapter/MoviesDataSource;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    move v2, v3

    .line 111
    .local v2, "hasPurchases":Z
    :goto_0
    iget-object v5, p0, Lcom/google/android/videos/ui/MoviesFlowHelper;->preorderDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v5}, Lcom/google/android/videos/adapter/MoviesDataSource;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    move v1, v3

    .line 112
    .local v1, "hasPreorders":Z
    :goto_1
    iget-object v5, p0, Lcom/google/android/videos/ui/MoviesFlowHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-virtual {v5}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->isDownloadedOnly()Z

    move-result v0

    .line 114
    .local v0, "downloadedOnly":Z
    iget-object v6, p0, Lcom/google/android/videos/ui/MoviesFlowHelper;->welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;

    if-nez v2, :cond_0

    if-eqz v1, :cond_3

    :cond_0
    move v5, v3

    :goto_2
    invoke-virtual {v6, v5}, Lcom/google/android/videos/welcome/WelcomeFlow;->setContentInVertical(Z)V

    .line 115
    iget-object v5, p0, Lcom/google/android/videos/ui/MoviesFlowHelper;->welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;

    invoke-virtual {v5, v0}, Lcom/google/android/videos/welcome/WelcomeFlow;->setDownloadedOnly(Z)V

    .line 116
    iget-object v6, p0, Lcom/google/android/videos/ui/MoviesFlowHelper;->preorderFlow:Lcom/google/android/videos/flow/Flow;

    if-nez v0, :cond_4

    move v5, v3

    :goto_3
    invoke-virtual {v6, v5}, Lcom/google/android/videos/flow/Flow;->setVisible(Z)V

    .line 117
    iget-object v5, p0, Lcom/google/android/videos/ui/MoviesFlowHelper;->noDownloadedContentFlow:Lcom/google/android/videos/flow/Flow;

    if-eqz v0, :cond_5

    if-nez v2, :cond_5

    if-nez v1, :cond_5

    iget-object v6, p0, Lcom/google/android/videos/ui/MoviesFlowHelper;->welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;

    invoke-virtual {v6}, Lcom/google/android/videos/welcome/WelcomeFlow;->getCount()I

    move-result v6

    if-nez v6, :cond_5

    :goto_4
    invoke-virtual {v5, v3}, Lcom/google/android/videos/flow/Flow;->setVisible(Z)V

    .line 119
    return-void

    .end local v0    # "downloadedOnly":Z
    .end local v1    # "hasPreorders":Z
    .end local v2    # "hasPurchases":Z
    :cond_1
    move v2, v4

    .line 110
    goto :goto_0

    .restart local v2    # "hasPurchases":Z
    :cond_2
    move v1, v4

    .line 111
    goto :goto_1

    .restart local v0    # "downloadedOnly":Z
    .restart local v1    # "hasPreorders":Z
    :cond_3
    move v5, v4

    .line 114
    goto :goto_2

    :cond_4
    move v5, v4

    .line 116
    goto :goto_3

    :cond_5
    move v3, v4

    .line 117
    goto :goto_4
.end method


# virtual methods
.method public getFlow()Lcom/google/android/videos/flow/Flow;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesFlowHelper;->flow:Lcom/google/android/videos/flow/Flow;

    return-object v0
.end method

.method public onChanged()V
    .locals 0

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/google/android/videos/ui/MoviesFlowHelper;->updateVisibilities()V

    .line 107
    return-void
.end method

.method public onDownloadedOnlyChanged(Z)V
    .locals 0
    .param p1, "downloadedOnly"    # Z

    .prologue
    .line 123
    invoke-direct {p0}, Lcom/google/android/videos/ui/MoviesFlowHelper;->updateVisibilities()V

    .line 124
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesFlowHelper;->libraryDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/adapter/MoviesDataSource;->registerObserver(Ljava/lang/Object;)V

    .line 91
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesFlowHelper;->preorderDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/adapter/MoviesDataSource;->registerObserver(Ljava/lang/Object;)V

    .line 92
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesFlowHelper;->welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;

    invoke-virtual {v0}, Lcom/google/android/videos/welcome/WelcomeFlow;->onStart()V

    .line 93
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesFlowHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->registerObserver(Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;)V

    .line 94
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesFlowHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-virtual {v0}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->isDownloadedOnly()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/MoviesFlowHelper;->onDownloadedOnlyChanged(Z)V

    .line 95
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesFlowHelper;->libraryDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/adapter/MoviesDataSource;->unregisterObserver(Ljava/lang/Object;)V

    .line 99
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesFlowHelper;->preorderDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/adapter/MoviesDataSource;->unregisterObserver(Ljava/lang/Object;)V

    .line 100
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesFlowHelper;->welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;

    invoke-virtual {v0}, Lcom/google/android/videos/welcome/WelcomeFlow;->onStop()V

    .line 101
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesFlowHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->unregisterObserver(Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;)V

    .line 102
    return-void
.end method
