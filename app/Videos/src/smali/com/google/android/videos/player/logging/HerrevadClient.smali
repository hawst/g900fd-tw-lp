.class public Lcom/google/android/videos/player/logging/HerrevadClient;
.super Ljava/lang/Object;
.source "HerrevadClient.java"

# interfaces
.implements Lcom/google/android/videos/player/logging/LoggingClient;


# instance fields
.field private final context:Landroid/content/Context;

.field private lastHerrevadReportTimeMs:J

.field private latencyUs:I

.field private final minHerrevadReportIntervalSeconds:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "minHerrevadReportIntervalSeconds"    # I

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/google/android/videos/player/logging/HerrevadClient;->context:Landroid/content/Context;

    .line 29
    iput p2, p0, Lcom/google/android/videos/player/logging/HerrevadClient;->minHerrevadReportIntervalSeconds:I

    .line 30
    return-void
.end method


# virtual methods
.method public endSession(IILcom/google/android/videos/player/logging/DerivedStats;)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "mediaTimeMs"    # I
    .param p3, "derivedStats"    # Lcom/google/android/videos/player/logging/DerivedStats;

    .prologue
    .line 133
    return-void
.end method

.method public onBandwidthSample(IIJJ)V
    .locals 9
    .param p1, "sessionTimeMs"    # I
    .param p2, "elapsedMs"    # I
    .param p3, "bytes"    # J
    .param p5, "bitrateEstimate"    # J

    .prologue
    .line 111
    const-wide/32 v0, 0x186a0

    cmp-long v0, p3, v0

    if-ltz v0, :cond_1

    iget-wide v0, p0, Lcom/google/android/videos/player/logging/HerrevadClient;->lastHerrevadReportTimeMs:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    int-to-long v0, p1

    iget-wide v2, p0, Lcom/google/android/videos/player/logging/HerrevadClient;->lastHerrevadReportTimeMs:J

    sub-long/2addr v0, v2

    iget v2, p0, Lcom/google/android/videos/player/logging/HerrevadClient;->minHerrevadReportIntervalSeconds:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    .line 113
    :cond_0
    new-instance v5, Landroid/os/Bundle;

    const/4 v0, 0x2

    invoke-direct {v5, v0}, Landroid/os/Bundle;-><init>(I)V

    .line 114
    .local v5, "customNetStats":Landroid/os/Bundle;
    const-string v0, "transfer_time_millis"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    const-string v0, "bytes_received"

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    const-string v0, "bandwidth_estimate"

    const-wide/16 v2, 0x8

    div-long v2, p5, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const-wide/16 v0, 0x3e8

    mul-long/2addr v0, p3

    int-to-long v2, p2

    div-long v6, v0, v2

    .line 118
    .local v6, "throughput":J
    iget-object v0, p0, Lcom/google/android/videos/player/logging/HerrevadClient;->context:Landroid/content/Context;

    iget v1, p0, Lcom/google/android/videos/player/logging/HerrevadClient;->latencyUs:I

    mul-int/lit16 v1, v1, 0x3e8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/mdm/NetworkQualityUploader;->logNetworkStats(Landroid/content/Context;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Landroid/os/Bundle;)V

    .line 120
    int-to-long v0, p1

    iput-wide v0, p0, Lcom/google/android/videos/player/logging/HerrevadClient;->lastHerrevadReportTimeMs:J

    .line 122
    .end local v5    # "customNetStats":Landroid/os/Bundle;
    .end local v6    # "throughput":J
    :cond_1
    return-void
.end method

.method public onDroppedFrames(II)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "count"    # I

    .prologue
    .line 70
    return-void
.end method

.method public onError(IIIILjava/lang/Exception;)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "mediaTimeMs"    # I
    .param p3, "errorType"    # I
    .param p4, "errorCode"    # I
    .param p5, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 60
    return-void
.end method

.method public onFailed(II)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "mediaTimeMs"    # I

    .prologue
    .line 65
    return-void
.end method

.method public onFormatEnabled(III)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "itag"    # I
    .param p3, "trigger"    # I

    .prologue
    .line 80
    return-void
.end method

.method public onFormatSelected(III)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "itag"    # I
    .param p3, "trigger"    # I

    .prologue
    .line 75
    return-void
.end method

.method public onHttpDataSourceOpened(IJ)V
    .locals 2
    .param p1, "sessionTimeMs"    # I
    .param p2, "elapsedMs"    # J

    .prologue
    .line 126
    const-wide/16 v0, 0x3e8

    mul-long/2addr v0, p2

    long-to-int v0, v0

    iput v0, p0, Lcom/google/android/videos/player/logging/HerrevadClient;->latencyUs:I

    .line 127
    return-void
.end method

.method public onLoadingChanged(IZ)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "loading"    # Z

    .prologue
    .line 101
    return-void
.end method

.method public onNetworkType(III)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "type"    # I
    .param p3, "subtype"    # I

    .prologue
    .line 106
    return-void
.end method

.method public onStateChanged(IIZI)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "mediaTimeMs"    # I
    .param p3, "playWhenReady"    # Z
    .param p4, "playbackState"    # I

    .prologue
    .line 54
    return-void
.end method

.method public onSubtitleEnabled(ILcom/google/android/videos/subtitles/SubtitleTrack;)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "track"    # Lcom/google/android/videos/subtitles/SubtitleTrack;

    .prologue
    .line 96
    return-void
.end method

.method public onSubtitleError(ILcom/google/android/videos/subtitles/SubtitleTrack;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "track"    # Lcom/google/android/videos/subtitles/SubtitleTrack;
    .param p3, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 91
    return-void
.end method

.method public onSubtitleSelected(ILcom/google/android/videos/subtitles/SubtitleTrack;)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "track"    # Lcom/google/android/videos/subtitles/SubtitleTrack;

    .prologue
    .line 85
    return-void
.end method

.method public onUserSeekingChanged(IIZZ)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "mediaTimeMs"    # I
    .param p3, "seeking"    # Z
    .param p4, "isFineScrubbing"    # Z

    .prologue
    .line 48
    return-void
.end method

.method public startSession(Lcom/google/android/videos/player/logging/SessionTimeProvider;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 0
    .param p1, "sessionTimeProvider"    # Lcom/google/android/videos/player/logging/SessionTimeProvider;
    .param p2, "sessionNonce"    # Ljava/lang/String;
    .param p3, "playerType"    # I
    .param p4, "account"    # Ljava/lang/String;
    .param p5, "videoId"    # Ljava/lang/String;
    .param p6, "showId"    # Ljava/lang/String;
    .param p7, "seasonId"    # Ljava/lang/String;
    .param p8, "isTrailer"    # Z
    .param p9, "isOffline"    # Z

    .prologue
    .line 42
    return-void
.end method

.method public supportsOfflinePlaybacks()Z
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    return v0
.end method
