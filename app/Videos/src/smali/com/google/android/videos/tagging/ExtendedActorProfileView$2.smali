.class Lcom/google/android/videos/tagging/ExtendedActorProfileView$2;
.super Lcom/google/android/videos/ui/TransitionUtil$TransitionListenerAdapter;
.source "ExtendedActorProfileView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/tagging/ExtendedActorProfileView;->startShowTransition(Landroid/widget/FrameLayout;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;


# direct methods
.method constructor <init>(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)V
    .locals 0

    .prologue
    .line 249
    iput-object p1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$2;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    invoke-direct {p0}, Lcom/google/android/videos/ui/TransitionUtil$TransitionListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onTransitionEnd(Landroid/transition/Transition;)V
    .locals 2
    .param p1, "transition"    # Landroid/transition/Transition;

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$2;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->transitionProfileImageView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$000(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setHasTransientState(Z)V

    .line 253
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$2;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->profileContent:Landroid/support/v7/widget/RecyclerView;
    invoke-static {v0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$100(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Landroid/support/v7/widget/RecyclerView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$2;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actorVideosFlow:Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow;
    invoke-static {v1}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$200(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/videos/ui/FlowAnimationUtil;->animateFlowAppearing(Landroid/support/v7/widget/RecyclerView;Lcom/google/android/videos/flow/Flow;)V

    .line 254
    return-void
.end method
