.class public Lcom/google/android/videos/utils/BytesToPipeConverter;
.super Ljava/lang/Object;
.source "BytesToPipeConverter.java"

# interfaces
.implements Lcom/google/android/videos/converter/ResponseConverter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/converter/ResponseConverter",
        "<",
        "Lcom/google/android/videos/utils/ByteArray;",
        "Landroid/os/ParcelFileDescriptor;",
        ">;"
    }
.end annotation


# instance fields
.field public final writeExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1, "writeExecutor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/google/android/videos/utils/BytesToPipeConverter;->writeExecutor:Ljava/util/concurrent/Executor;

    .line 36
    return-void
.end method

.method private writeDataInBackground(Ljava/io/OutputStream;Lcom/google/android/videos/utils/ByteArray;)V
    .locals 2
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "byteArray"    # Lcom/google/android/videos/utils/ByteArray;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/videos/utils/BytesToPipeConverter;->writeExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/videos/utils/BytesToPipeConverter$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/videos/utils/BytesToPipeConverter$1;-><init>(Lcom/google/android/videos/utils/BytesToPipeConverter;Ljava/io/OutputStream;Lcom/google/android/videos/utils/ByteArray;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 59
    return-void
.end method


# virtual methods
.method public convertResponse(Lcom/google/android/videos/utils/ByteArray;)Landroid/os/ParcelFileDescriptor;
    .locals 4
    .param p1, "byteArray"    # Lcom/google/android/videos/utils/ByteArray;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 64
    :try_start_0
    invoke-static {}, Landroid/os/ParcelFileDescriptor;->createPipe()[Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    .line 65
    .local v1, "readAndWrite":[Landroid/os/ParcelFileDescriptor;
    new-instance v2, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;

    const/4 v3, 0x1

    aget-object v3, v1, v3

    invoke-direct {v2, v3}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    invoke-direct {p0, v2, p1}, Lcom/google/android/videos/utils/BytesToPipeConverter;->writeDataInBackground(Ljava/io/OutputStream;Lcom/google/android/videos/utils/ByteArray;)V

    .line 67
    const/4 v2, 0x0

    aget-object v2, v1, v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    .line 68
    .end local v1    # "readAndWrite":[Landroid/os/ParcelFileDescriptor;
    :catch_0
    move-exception v0

    .line 69
    .local v0, "ioe":Ljava/io/IOException;
    new-instance v2, Lcom/google/android/videos/converter/ConverterException;

    invoke-direct {v2, v0}, Lcom/google/android/videos/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public bridge synthetic convertResponse(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 26
    check-cast p1, Lcom/google/android/videos/utils/ByteArray;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/utils/BytesToPipeConverter;->convertResponse(Lcom/google/android/videos/utils/ByteArray;)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    return-object v0
.end method
