.class Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;
.super Ljava/lang/Object;
.source "AssetDetailsActivity.java"

# interfaces
.implements Lcom/google/android/play/headerlist/PlayHeaderListLayout$LayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/activity/AssetDetailsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActionBarBackgroundUpdater"
.end annotation


# instance fields
.field private baseBackground:Landroid/graphics/drawable/Drawable;

.field private isActionBarOpaque:Z

.field private isHeaderListFloating:Z

.field public final opaqueBackground:Landroid/graphics/drawable/Drawable;

.field public final protectionBackground:Landroid/graphics/drawable/Drawable;

.field final synthetic this$0:Lcom/google/android/videos/activity/AssetDetailsActivity;

.field public final transparentBackground:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/activity/AssetDetailsActivity;)V
    .locals 2

    .prologue
    .line 596
    iput-object p1, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;->this$0:Lcom/google/android/videos/activity/AssetDetailsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 597
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;->transparentBackground:Landroid/graphics/drawable/Drawable;

    .line 598
    invoke-virtual {p1}, Lcom/google/android/videos/activity/AssetDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020038

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;->protectionBackground:Landroid/graphics/drawable/Drawable;

    .line 599
    const v0, 0x7f0a0079

    invoke-static {p1, v0}, Lcom/google/android/videos/ui/DogfoodHelper;->addPawsIfNeeded(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;->opaqueBackground:Landroid/graphics/drawable/Drawable;

    .line 602
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;->protectionBackground:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;->baseBackground:Landroid/graphics/drawable/Drawable;

    .line 603
    return-void
.end method

.method private setBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1, "background"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 638
    iget-object v1, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;->this$0:Lcom/google/android/videos/activity/AssetDetailsActivity;

    iget-object v1, v1, Lcom/google/android/videos/activity/AssetDetailsActivity;->playLayout:Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;

    invoke-virtual {v1}, Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;->getActionBarView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    .line 639
    .local v0, "toolbar":Landroid/support/v7/widget/Toolbar;
    invoke-static {v0, p1}, Lcom/google/android/videos/utils/ViewUtil;->setViewBackground(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 640
    return-void
.end method


# virtual methods
.method public forceActionBarOpaque(Z)V
    .locals 6
    .param p1, "opaque"    # Z

    .prologue
    const/4 v5, 0x1

    .line 627
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;->baseBackground:Landroid/graphics/drawable/Drawable;

    .line 628
    .local v0, "fromDrawable":Landroid/graphics/drawable/Drawable;
    :goto_0
    if-eqz p1, :cond_1

    iget-object v2, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;->opaqueBackground:Landroid/graphics/drawable/Drawable;

    .line 629
    .local v2, "toDrawable":Landroid/graphics/drawable/Drawable;
    :goto_1
    new-instance v1, Landroid/graphics/drawable/TransitionDrawable;

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    aput-object v2, v3, v5

    invoke-direct {v1, v3}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 631
    .local v1, "newBackground":Landroid/graphics/drawable/TransitionDrawable;
    invoke-virtual {v1, v5}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    .line 632
    invoke-direct {p0, v1}, Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 633
    const/16 v3, 0x96

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 634
    iput-boolean p1, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;->isActionBarOpaque:Z

    .line 635
    return-void

    .line 627
    .end local v0    # "fromDrawable":Landroid/graphics/drawable/Drawable;
    .end local v1    # "newBackground":Landroid/graphics/drawable/TransitionDrawable;
    .end local v2    # "toDrawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;->opaqueBackground:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 628
    .restart local v0    # "fromDrawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;->baseBackground:Landroid/graphics/drawable/Drawable;

    goto :goto_1
.end method

.method public onPlayHeaderListLayoutChanged()V
    .locals 2

    .prologue
    .line 608
    iget-object v1, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;->this$0:Lcom/google/android/videos/activity/AssetDetailsActivity;

    iget-object v1, v1, Lcom/google/android/videos/activity/AssetDetailsActivity;->playLayout:Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;

    invoke-virtual {v1}, Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;->isHeaderFloating()Z

    move-result v0

    .line 609
    .local v0, "isFloating":Z
    iget-boolean v1, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;->isHeaderListFloating:Z

    if-eq v1, v0, :cond_0

    .line 610
    iput-boolean v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;->isHeaderListFloating:Z

    .line 611
    invoke-virtual {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;->updateBackground()V

    .line 613
    :cond_0
    return-void
.end method

.method public updateBackground()V
    .locals 1

    .prologue
    .line 617
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;->this$0:Lcom/google/android/videos/activity/AssetDetailsActivity;

    iget-object v0, v0, Lcom/google/android/videos/activity/AssetDetailsActivity;->playLayout:Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;->isHeaderFloating()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;->transparentBackground:Landroid/graphics/drawable/Drawable;

    :goto_0
    iput-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;->baseBackground:Landroid/graphics/drawable/Drawable;

    .line 620
    iget-boolean v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;->isActionBarOpaque:Z

    if-nez v0, :cond_0

    .line 621
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;->baseBackground:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v0}, Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 623
    :cond_0
    return-void

    .line 617
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$ActionBarBackgroundUpdater;->protectionBackground:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method
