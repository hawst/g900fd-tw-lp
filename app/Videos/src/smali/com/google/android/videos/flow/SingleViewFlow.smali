.class public Lcom/google/android/videos/flow/SingleViewFlow;
.super Lcom/google/android/videos/flow/Flow;
.source "SingleViewFlow.java"

# interfaces
.implements Lcom/google/android/videos/flow/ViewHolderCreator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/flow/Flow;",
        "Lcom/google/android/videos/flow/ViewHolderCreator",
        "<",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field public final identifier:Ljava/lang/Object;

.field public final layoutId:I

.field private final viewHolderCreator:Lcom/google/android/videos/flow/ViewHolderCreator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/flow/ViewHolderCreator",
            "<*>;"
        }
    .end annotation
.end field

.field public final viewType:I


# direct methods
.method public constructor <init>(I)V
    .locals 2
    .param p1, "layoutId"    # I

    .prologue
    .line 62
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    const/4 v1, 0x0

    invoke-direct {p0, p1, p1, v0, v1}, Lcom/google/android/videos/flow/SingleViewFlow;-><init>(IILjava/lang/Object;Lcom/google/android/videos/flow/ViewHolderCreator;)V

    .line 63
    return-void
.end method

.method public constructor <init>(IILjava/lang/Object;Lcom/google/android/videos/flow/ViewHolderCreator;)V
    .locals 1
    .param p1, "layoutId"    # I
    .param p2, "viewType"    # I
    .param p3, "identifier"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/Object;",
            "Lcom/google/android/videos/flow/ViewHolderCreator",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 78
    .local p4, "viewHolderCreator":Lcom/google/android/videos/flow/ViewHolderCreator;, "Lcom/google/android/videos/flow/ViewHolderCreator<*>;"
    invoke-direct {p0}, Lcom/google/android/videos/flow/Flow;-><init>()V

    .line 79
    if-lez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    .line 80
    iput p1, p0, Lcom/google/android/videos/flow/SingleViewFlow;->layoutId:I

    .line 81
    iput p2, p0, Lcom/google/android/videos/flow/SingleViewFlow;->viewType:I

    .line 82
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/flow/SingleViewFlow;->identifier:Ljava/lang/Object;

    .line 83
    if-nez p4, :cond_0

    move-object p4, p0

    .end local p4    # "viewHolderCreator":Lcom/google/android/videos/flow/ViewHolderCreator;, "Lcom/google/android/videos/flow/ViewHolderCreator<*>;"
    :cond_0
    iput-object p4, p0, Lcom/google/android/videos/flow/SingleViewFlow;->viewHolderCreator:Lcom/google/android/videos/flow/ViewHolderCreator;

    .line 84
    return-void

    .line 79
    .restart local p4    # "viewHolderCreator":Lcom/google/android/videos/flow/ViewHolderCreator;, "Lcom/google/android/videos/flow/ViewHolderCreator<*>;"
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(ILcom/google/android/videos/flow/ViewHolderCreator;)V
    .locals 1
    .param p1, "layoutId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/flow/ViewHolderCreator",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 71
    .local p2, "viewHolderCreator":Lcom/google/android/videos/flow/ViewHolderCreator;, "Lcom/google/android/videos/flow/ViewHolderCreator<*>;"
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    invoke-direct {p0, p1, p1, v0, p2}, Lcom/google/android/videos/flow/SingleViewFlow;-><init>(IILjava/lang/Object;Lcom/google/android/videos/flow/ViewHolderCreator;)V

    .line 72
    return-void
.end method


# virtual methods
.method public bindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 1
    .param p1, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 125
    instance-of v0, p1, Lcom/google/android/videos/flow/Bindable;

    if-eqz v0, :cond_0

    .line 126
    check-cast p1, Lcom/google/android/videos/flow/Bindable;

    .end local p1    # "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-interface {p1}, Lcom/google/android/videos/flow/Bindable;->bind()V

    .line 128
    :cond_0
    return-void
.end method

.method public createViewHolder(ILandroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 4
    .param p1, "viewType"    # I
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 132
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iget v2, p0, Lcom/google/android/videos/flow/SingleViewFlow;->layoutId:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 133
    .local v0, "view":Landroid/view/View;
    new-instance v1, Lcom/google/android/videos/flow/BasicViewHolderCreator$BasicViewHolder;

    invoke-direct {v1, v0}, Lcom/google/android/videos/flow/BasicViewHolderCreator$BasicViewHolder;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x1

    return v0
.end method

.method public final getItemIdentifier(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/videos/flow/SingleViewFlow;->identifier:Ljava/lang/Object;

    return-object v0
.end method

.method public final getViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 93
    iget v0, p0, Lcom/google/android/videos/flow/SingleViewFlow;->viewType:I

    return v0
.end method

.method public notifyItemChanged()V
    .locals 2

    .prologue
    .line 117
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/videos/flow/SingleViewFlow;->notifyItemsChanged(II)V

    .line 118
    return-void
.end method

.method protected onInitViewTypes(Landroid/util/SparseArray;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/videos/flow/ViewHolderCreator",
            "<*>;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 99
    .local p1, "viewTypes":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/google/android/videos/flow/ViewHolderCreator<*>;>;"
    iget v1, p0, Lcom/google/android/videos/flow/SingleViewFlow;->viewType:I

    invoke-virtual {p1, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/flow/ViewHolderCreator;

    .line 100
    .local v0, "existing":Lcom/google/android/videos/flow/ViewHolderCreator;, "Lcom/google/android/videos/flow/ViewHolderCreator<*>;"
    if-nez v0, :cond_1

    .line 101
    iget v1, p0, Lcom/google/android/videos/flow/SingleViewFlow;->viewType:I

    iget-object v2, p0, Lcom/google/android/videos/flow/SingleViewFlow;->viewHolderCreator:Lcom/google/android/videos/flow/ViewHolderCreator;

    invoke-virtual {p1, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 106
    :cond_0
    return-void

    .line 102
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/flow/SingleViewFlow;->viewHolderCreator:Lcom/google/android/videos/flow/ViewHolderCreator;

    if-eq v0, v1, :cond_0

    .line 103
    new-instance v1, Ljava/lang/IllegalArgumentException;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Existing ViewHolderCreator for view type 0x%1$x is not for the current SingleViewFlow"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/videos/flow/SingleViewFlow;->viewType:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
