.class Lcom/google/android/videos/store/ConfigurationStore$GetConfigProto;
.super Ljava/lang/Object;
.source "ConfigurationStore.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/ConfigurationStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetConfigProto"
.end annotation


# instance fields
.field private final account:Ljava/lang/String;

.field private final callback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/UserConfiguration;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/videos/store/ConfigurationStore;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/store/ConfigurationStore;Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V
    .locals 0
    .param p2, "account"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/UserConfiguration;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 294
    .local p3, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/UserConfiguration;>;"
    iput-object p1, p0, Lcom/google/android/videos/store/ConfigurationStore$GetConfigProto;->this$0:Lcom/google/android/videos/store/ConfigurationStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 295
    iput-object p2, p0, Lcom/google/android/videos/store/ConfigurationStore$GetConfigProto;->account:Ljava/lang/String;

    .line 296
    iput-object p3, p0, Lcom/google/android/videos/store/ConfigurationStore$GetConfigProto;->callback:Lcom/google/android/videos/async/Callback;

    .line 297
    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 301
    const/4 v12, 0x0

    .line 302
    .local v12, "result":Lcom/google/wireless/android/video/magma/proto/UserConfiguration;
    iget-object v0, p0, Lcom/google/android/videos/store/ConfigurationStore$GetConfigProto;->this$0:Lcom/google/android/videos/store/ConfigurationStore;

    # getter for: Lcom/google/android/videos/store/ConfigurationStore;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v0}, Lcom/google/android/videos/store/ConfigurationStore;->access$100(Lcom/google/android/videos/store/ConfigurationStore;)Lcom/google/android/videos/store/Database;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "user_configuration"

    # getter for: Lcom/google/android/videos/store/ConfigurationStore;->GET_PROJECTION:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/videos/store/ConfigurationStore;->access$600()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "config_account = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/videos/store/ConfigurationStore$GetConfigProto;->account:Ljava/lang/String;

    aput-object v6, v4, v7

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 304
    .local v8, "cursor":Landroid/database/Cursor;
    const/4 v11, 0x0

    .line 306
    .local v11, "exception":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    :goto_0
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 307
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v9

    .line 308
    .local v9, "data":[B
    invoke-static {v9}, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->parseFrom([B)Lcom/google/wireless/android/video/magma/proto/UserConfiguration;
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v12

    .line 309
    goto :goto_0

    .line 313
    .end local v9    # "data":[B
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 315
    :goto_1
    if-nez v11, :cond_1

    .line 316
    iget-object v0, p0, Lcom/google/android/videos/store/ConfigurationStore$GetConfigProto;->callback:Lcom/google/android/videos/async/Callback;

    iget-object v1, p0, Lcom/google/android/videos/store/ConfigurationStore$GetConfigProto;->account:Ljava/lang/String;

    invoke-interface {v0, v1, v12}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 320
    :goto_2
    return-void

    .line 310
    :catch_0
    move-exception v10

    .line 311
    .local v10, "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    move-object v11, v10

    .line 313
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .end local v10    # "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    .line 318
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/store/ConfigurationStore$GetConfigProto;->callback:Lcom/google/android/videos/async/Callback;

    iget-object v1, p0, Lcom/google/android/videos/store/ConfigurationStore$GetConfigProto;->account:Ljava/lang/String;

    invoke-interface {v0, v1, v11}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_2
.end method
