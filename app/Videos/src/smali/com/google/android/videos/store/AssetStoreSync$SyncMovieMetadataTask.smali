.class Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;
.super Lcom/google/android/videos/store/AssetStoreSync$SyncAssetMetadataTask;
.source "AssetStoreSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/AssetStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncMovieMetadataTask"
.end annotation


# instance fields
.field private final movieIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/videos/store/AssetStoreSync;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .param p2, "priority"    # I
    .param p4, "account"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/store/SyncTaskManager$SharedStates",
            "<*>;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 756
    .local p3, "states":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<*>;"
    .local p5, "movieIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    .line 757
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/videos/store/AssetStoreSync$SyncAssetMetadataTask;-><init>(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V

    .line 758
    iput-object p5, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->movieIds:Ljava/util/List;

    .line 759
    return-void
.end method

.method private checkMetadataUpToDate()Z
    .locals 18

    .prologue
    .line 782
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 783
    .local v9, "upToDateIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 784
    .local v12, "upToDateIdsNeedingUserAssetsRefresh":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->movieIds:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->config:Lcom/google/android/videos/Config;
    invoke-static {v4}, Lcom/google/android/videos/store/AssetStoreSync;->access$500(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/Config;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/videos/Config;->resyncVideoAfterMillis()J

    move-result-wide v4

    const-string v6, "videos"

    const-string v7, "video_id"

    const-string v8, "video_synced_timestamp"

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->account:Ljava/lang/String;

    const/4 v11, 0x6

    # invokes: Lcom/google/android/videos/store/AssetStoreSync;->filterUpToDateIds(Ljava/util/List;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ILjava/util/List;)Z
    invoke-static/range {v2 .. v12}, Lcom/google/android/videos/store/AssetStoreSync;->access$600(Lcom/google/android/videos/store/AssetStoreSync;Ljava/util/List;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ILjava/util/List;)Z

    move-result v13

    .line 789
    .local v13, "allUpToDate":Z
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v14, v2, -0x1

    .local v14, "i":I
    :goto_0
    if-ltz v14, :cond_0

    .line 790
    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 791
    .local v15, "movieId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->priority:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    # invokes: Lcom/google/android/videos/store/AssetStoreSync;->maybeSyncVideoPoster(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V
    invoke-static {v2, v3, v4, v15}, Lcom/google/android/videos/store/AssetStoreSync;->access$700(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V

    .line 792
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->priority:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    # invokes: Lcom/google/android/videos/store/AssetStoreSync;->maybeSyncVideoScreenshot(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V
    invoke-static {v2, v3, v4, v15}, Lcom/google/android/videos/store/AssetStoreSync;->access$800(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V

    .line 789
    add-int/lit8 v14, v14, -0x1

    goto :goto_0

    .line 797
    .end local v15    # "movieId":Ljava/lang/String;
    :cond_0
    invoke-interface {v12}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 798
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v2}, Lcom/google/android/videos/store/AssetStoreSync;->access$900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v17

    .line 799
    .local v17, "transaction":Landroid/database/sqlite/SQLiteDatabase;
    const/16 v16, 0x0

    .line 801
    .local v16, "success":Z
    :try_start_0
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v14, v2, -0x1

    :goto_1
    if-ltz v14, :cond_1

    .line 802
    invoke-interface {v12, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 803
    .restart local v15    # "movieId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->account:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-static {v0, v2, v15}, Lcom/google/android/videos/store/UserAssetsUtil;->refreshVideoRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 801
    add-int/lit8 v14, v14, -0x1

    goto :goto_1

    .line 805
    .end local v15    # "movieId":Ljava/lang/String;
    :cond_1
    const/16 v16, 0x1

    .line 807
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v2}, Lcom/google/android/videos/store/AssetStoreSync;->access$900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v2

    const/4 v3, 0x4

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->account:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v12, v4, v5

    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v2, v0, v1, v3, v4}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 811
    .end local v16    # "success":Z
    .end local v17    # "transaction":Landroid/database/sqlite/SQLiteDatabase;
    :cond_2
    return v13

    .line 807
    .restart local v16    # "success":Z
    .restart local v17    # "transaction":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v3}, Lcom/google/android/videos/store/AssetStoreSync;->access$900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v3

    const/4 v4, 0x4

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->account:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v12, v5, v6

    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v3, v0, v1, v4, v5}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v2
.end method

.method private requestAssets()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 815
    iget-object v3, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->movieIds:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    .line 816
    .local v1, "count":I
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 817
    .local v0, "assetIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 818
    iget-object v3, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->movieIds:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromMovieId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 817
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 820
    :cond_0
    invoke-virtual {p0, v0, v4, v4}, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->requestAssets(Ljava/util/List;ZZ)Z

    move-result v3

    return v3
.end method


# virtual methods
.method protected doSync()V
    .locals 4

    .prologue
    .line 763
    invoke-direct {p0}, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->checkMetadataUpToDate()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 773
    :cond_0
    :goto_0
    return-void

    .line 766
    :cond_1
    invoke-direct {p0}, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->requestAssets()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 769
    iget-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->movieIds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 770
    const/16 v0, 0x14

    new-instance v1, Lcom/google/android/videos/store/SyncTaskManager$DataException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not download metadata for movie(s) "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->movieIds:Ljava/util/List;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/videos/store/SyncTaskManager$DataException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->onSyncError(ILjava/lang/Exception;)V

    goto :goto_0
.end method

.method public onResponse(Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;)V
    .locals 19
    .param p1, "request"    # Lcom/google/android/videos/api/AssetsRequest;
    .param p2, "response"    # Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    .prologue
    .line 825
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 826
    .local v9, "processed":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 827
    .local v3, "assets":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 828
    .local v10, "nowTimestamp":J
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v14}, Lcom/google/android/videos/store/AssetStoreSync;->access$900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v14

    invoke-virtual {v14}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v13

    .line 829
    .local v13, "transaction":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v12, 0x0

    .line 831
    .local v12, "success":Z
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    :try_start_0
    array-length v14, v3

    if-ge v6, v14, :cond_1

    .line 832
    aget-object v4, v3, v6

    .line 833
    .local v4, "candidate":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    const/4 v14, 0x6

    invoke-static {v4, v14}, Lcom/google/android/videos/store/PurchaseStoreUtil;->isAssetOfType(Lcom/google/wireless/android/video/magma/proto/AssetResource;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v14

    if-nez v14, :cond_0

    .line 831
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 837
    :cond_0
    :try_start_1
    invoke-static {v4}, Lcom/google/android/videos/store/PurchaseStoreUtil;->checkMetadataAsset(Lcom/google/wireless/android/video/magma/proto/AssetResource;)V
    :try_end_1
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 842
    :try_start_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->playCountry:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->assetImageUriCreator:Lcom/google/android/videos/ui/AssetImageUriCreator;
    invoke-static {v15}, Lcom/google/android/videos/store/AssetStoreSync;->access$1000(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/ui/AssetImageUriCreator;

    move-result-object v15

    invoke-static {v4, v14, v10, v11, v15}, Lcom/google/android/videos/store/PurchaseStoreUtil;->buildMovieContentValues(Lcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/lang/String;JLcom/google/android/videos/ui/AssetImageUriCreator;)Landroid/content/ContentValues;

    move-result-object v8

    .line 844
    .local v8, "movieValues":Landroid/content/ContentValues;
    invoke-static {v4}, Lcom/google/android/videos/store/PurchaseStoreUtil;->buildMovieAssetContentValues(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Landroid/content/ContentValues;

    move-result-object v2

    .line 845
    .local v2, "assetValues":Landroid/content/ContentValues;
    iget-object v14, v4, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v7, v14, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    .line 848
    .local v7, "movieId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->movieIds:Ljava/util/List;

    invoke-interface {v14, v7}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 849
    invoke-interface {v9, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 850
    invoke-static {v8, v13}, Lcom/google/android/videos/store/PurchaseStoreUtil;->loadMissingDataIntoVideoContentValues(Landroid/content/ContentValues;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 851
    const-string v14, "videos"

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->VIDEOS_EQUAL_COLUMNS:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/videos/store/AssetStoreSync;->access$1100()[Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v8, v15}, Lcom/google/android/videos/utils/DbUtils;->updateOrReplace(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;[Ljava/lang/String;)J

    .line 852
    const-string v14, "assets"

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->ASSETS_EQUAL_COLUMNS:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/videos/store/AssetStoreSync;->access$1200()[Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v2, v15}, Lcom/google/android/videos/utils/DbUtils;->updateOrReplace(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;[Ljava/lang/String;)J

    .line 853
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->account:Ljava/lang/String;

    invoke-static {v13, v14, v7}, Lcom/google/android/videos/store/UserAssetsUtil;->refreshVideoRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 857
    .end local v2    # "assetValues":Landroid/content/ContentValues;
    .end local v4    # "candidate":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .end local v7    # "movieId":Ljava/lang/String;
    .end local v8    # "movieValues":Landroid/content/ContentValues;
    :catchall_0
    move-exception v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v15}, Lcom/google/android/videos/store/AssetStoreSync;->access$900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v15

    const/16 v16, 0x3

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aput-object v9, v17, v18

    move/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v15, v13, v12, v0, v1}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v14

    .line 838
    .restart local v4    # "candidate":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :catch_0
    move-exception v5

    .line 839
    .local v5, "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    :try_start_3
    const-string v14, "Metadata error when syncing movies"

    invoke-static {v14, v5}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 855
    .end local v4    # "candidate":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .end local v5    # "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    :cond_1
    const/4 v12, 0x1

    .line 857
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v14}, Lcom/google/android/videos/store/AssetStoreSync;->access$900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v14

    const/4 v15, 0x3

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object v9, v16, v17

    move-object/from16 v0, v16

    invoke-virtual {v14, v13, v12, v15, v0}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 861
    const/4 v6, 0x0

    :goto_2
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v14

    if-ge v6, v14, :cond_2

    .line 862
    invoke-interface {v9, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 863
    .restart local v7    # "movieId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->priority:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    # invokes: Lcom/google/android/videos/store/AssetStoreSync;->maybeSyncVideoPoster(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V
    invoke-static {v14, v15, v0, v7}, Lcom/google/android/videos/store/AssetStoreSync;->access$700(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V

    .line 864
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->priority:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    # invokes: Lcom/google/android/videos/store/AssetStoreSync;->maybeSyncVideoScreenshot(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V
    invoke-static {v14, v15, v0, v7}, Lcom/google/android/videos/store/AssetStoreSync;->access$800(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V

    .line 861
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 866
    .end local v7    # "movieId":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 751
    check-cast p1, Lcom/google/android/videos/api/AssetsRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->onResponse(Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;)V

    return-void
.end method
