.class public abstract Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$BytesRequestHandler;
.super Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;
.source "DefaultKnowledgeRequesters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "BytesRequestHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        "T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler",
        "<TR;TE;TT;",
        "Lcom/google/android/videos/utils/ByteArray;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 414
    .local p0, "this":Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$BytesRequestHandler;, "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$BytesRequestHandler<TR;TE;TT;>;"
    invoke-direct {p0}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public shouldUpdate(Lcom/google/android/videos/utils/ByteArray;Lcom/google/android/videos/utils/ByteArray;)Z
    .locals 1
    .param p1, "storedResponse"    # Lcom/google/android/videos/utils/ByteArray;
    .param p2, "freshResponse"    # Lcom/google/android/videos/utils/ByteArray;

    .prologue
    .line 422
    .local p0, "this":Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$BytesRequestHandler;, "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$BytesRequestHandler<TR;TE;TT;>;"
    invoke-static {p1, p2}, Lcom/google/android/videos/utils/ByteArray;->dataEqual(Lcom/google/android/videos/utils/ByteArray;Lcom/google/android/videos/utils/ByteArray;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic shouldUpdate(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 414
    .local p0, "this":Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$BytesRequestHandler;, "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$BytesRequestHandler<TR;TE;TT;>;"
    check-cast p1, Lcom/google/android/videos/utils/ByteArray;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/videos/utils/ByteArray;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$BytesRequestHandler;->shouldUpdate(Lcom/google/android/videos/utils/ByteArray;Lcom/google/android/videos/utils/ByteArray;)Z

    move-result v0

    return v0
.end method
