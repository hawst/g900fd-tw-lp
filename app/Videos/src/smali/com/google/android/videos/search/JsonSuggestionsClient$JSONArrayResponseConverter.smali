.class Lcom/google/android/videos/search/JsonSuggestionsClient$JSONArrayResponseConverter;
.super Lcom/google/android/videos/converter/HttpResponseConverter;
.source "JsonSuggestionsClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/search/JsonSuggestionsClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "JSONArrayResponseConverter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/converter/HttpResponseConverter",
        "<",
        "Lorg/json/JSONArray;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/google/android/videos/converter/HttpResponseConverter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/search/JsonSuggestionsClient$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/search/JsonSuggestionsClient$1;

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/google/android/videos/search/JsonSuggestionsClient$JSONArrayResponseConverter;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic convertResponseEntity(Lorg/apache/http/HttpEntity;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 125
    invoke-virtual {p0, p1}, Lcom/google/android/videos/search/JsonSuggestionsClient$JSONArrayResponseConverter;->convertResponseEntity(Lorg/apache/http/HttpEntity;)Lorg/json/JSONArray;

    move-result-object v0

    return-object v0
.end method

.method protected convertResponseEntity(Lorg/apache/http/HttpEntity;)Lorg/json/JSONArray;
    .locals 3
    .param p1, "entity"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 130
    :try_start_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-static {p1}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 131
    :catch_0
    move-exception v0

    .line 132
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Lcom/google/android/videos/converter/ConverterException;

    invoke-direct {v1, v0}, Lcom/google/android/videos/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
