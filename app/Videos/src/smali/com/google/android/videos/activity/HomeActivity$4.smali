.class Lcom/google/android/videos/activity/HomeActivity$4;
.super Ljava/lang/Object;
.source "HomeActivity.java"

# interfaces
.implements Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/activity/HomeActivity;->addSearchExpandListener(Landroid/view/MenuItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/activity/HomeActivity;


# direct methods
.method constructor <init>(Lcom/google/android/videos/activity/HomeActivity;)V
    .locals 0

    .prologue
    .line 472
    iput-object p1, p0, Lcom/google/android/videos/activity/HomeActivity$4;->this$0:Lcom/google/android/videos/activity/HomeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemActionCollapse(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 475
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity$4;->this$0:Lcom/google/android/videos/activity/HomeActivity;

    # getter for: Lcom/google/android/videos/activity/HomeActivity;->mediaRouteProvider:Lcom/google/android/videos/remote/MediaRouteProvider;
    invoke-static {v0}, Lcom/google/android/videos/activity/HomeActivity;->access$200(Lcom/google/android/videos/activity/HomeActivity;)Lcom/google/android/videos/remote/MediaRouteProvider;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/videos/remote/MediaRouteProvider;->setForceHidden(Z)V

    .line 476
    const/4 v0, 0x1

    return v0
.end method

.method public onMenuItemActionExpand(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 480
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity$4;->this$0:Lcom/google/android/videos/activity/HomeActivity;

    # getter for: Lcom/google/android/videos/activity/HomeActivity;->mediaRouteProvider:Lcom/google/android/videos/remote/MediaRouteProvider;
    invoke-static {v0}, Lcom/google/android/videos/activity/HomeActivity;->access$200(Lcom/google/android/videos/activity/HomeActivity;)Lcom/google/android/videos/remote/MediaRouteProvider;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/videos/remote/MediaRouteProvider;->setForceHidden(Z)V

    .line 481
    return v1
.end method
