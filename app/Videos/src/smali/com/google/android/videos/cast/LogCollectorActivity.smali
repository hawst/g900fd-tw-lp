.class public Lcom/google/android/videos/cast/LogCollectorActivity;
.super Landroid/app/Activity;
.source "LogCollectorActivity.java"


# static fields
.field private static final callerWhitelist:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final logcatCmd:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 37
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "logcat"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "-v"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "time"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "-d"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/google/android/videos/cast/LogCollectorActivity;->logcatCmd:Ljava/util/List;

    .line 40
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/google/android/videos/cast/LogCollectorActivity;->callerWhitelist:Ljava/util/Set;

    .line 42
    sget-object v0, Lcom/google/android/videos/cast/LogCollectorActivity;->callerWhitelist:Ljava/util/Set;

    const-string v1, "com.google.android.apps.eureka"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 43
    sget-object v0, Lcom/google/android/videos/cast/LogCollectorActivity;->callerWhitelist:Ljava/util/Set;

    const-string v1, "com.google.eureka.feedback"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 44
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private collectLogcatOutput(Ljava/util/List;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 47
    .local p1, "logcatCmd":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    .line 48
    .local v1, "path":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    const-string v2, "pmlog.txt"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .local v0, "file":Ljava/io/File;
    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/cast/LogCollectorActivity;->collectLogcatOutput(Ljava/util/List;Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 50
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    .line 52
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private collectLogcatOutput(Ljava/util/List;Ljava/io/File;)Z
    .locals 12
    .param p2, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/io/File;",
            ")Z"
        }
    .end annotation

    .prologue
    .local p1, "logcatCmd":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v10, 0x0

    .line 79
    const/16 v1, 0x400

    .line 80
    .local v1, "bufferSize":I
    new-array v0, v1, [B

    .line 82
    .local v0, "buffer":[B
    const/4 v6, 0x0

    .line 83
    .local v6, "logcatProcess":Ljava/lang/Process;
    const/4 v3, 0x0

    .line 84
    .local v3, "inputStream":Ljava/io/InputStream;
    const/4 v7, 0x0

    .line 87
    .local v7, "outputStream":Ljava/io/OutputStream;
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v11

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/String;

    invoke-interface {p1, v9}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/String;

    invoke-virtual {v11, v9}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v6

    .line 88
    new-instance v4, Ljava/io/BufferedInputStream;

    invoke-virtual {v6}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v9

    invoke-direct {v4, v9}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    .end local v3    # "inputStream":Ljava/io/InputStream;
    .local v4, "inputStream":Ljava/io/InputStream;
    :try_start_1
    new-instance v8, Ljava/io/BufferedOutputStream;

    new-instance v9, Ljava/io/FileOutputStream;

    invoke-direct {v9, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v8, v9}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 90
    .end local v7    # "outputStream":Ljava/io/OutputStream;
    .local v8, "outputStream":Ljava/io/OutputStream;
    :try_start_2
    invoke-direct {p0, v8}, Lcom/google/android/videos/cast/LogCollectorActivity;->writeHeader(Ljava/io/OutputStream;)V

    .line 91
    const/4 v5, 0x0

    .line 92
    .local v5, "len":I
    :goto_0
    invoke-virtual {v4, v0}, Ljava/io/InputStream;->read([B)I

    move-result v5

    const/4 v9, -0x1

    if-eq v5, v9, :cond_2

    .line 93
    const/4 v9, 0x0

    invoke-virtual {v8, v0, v9, v5}, Ljava/io/OutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_0

    .line 98
    .end local v5    # "len":I
    :catch_0
    move-exception v2

    move-object v7, v8

    .end local v8    # "outputStream":Ljava/io/OutputStream;
    .restart local v7    # "outputStream":Ljava/io/OutputStream;
    move-object v3, v4

    .line 99
    .end local v4    # "inputStream":Ljava/io/InputStream;
    .local v2, "e":Ljava/io/IOException;
    .restart local v3    # "inputStream":Ljava/io/InputStream;
    :goto_1
    :try_start_3
    const-string v9, "Error collecting logs. Ironic, right?"

    invoke-static {v9, v2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 101
    if-eqz v7, :cond_0

    .line 103
    :try_start_4
    invoke-virtual {v7}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 109
    :cond_0
    :goto_2
    if-eqz v3, :cond_1

    .line 111
    :try_start_5
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    :cond_1
    :goto_3
    move v9, v10

    .line 118
    .end local v2    # "e":Ljava/io/IOException;
    :goto_4
    return v9

    .line 96
    .end local v3    # "inputStream":Ljava/io/InputStream;
    .end local v7    # "outputStream":Ljava/io/OutputStream;
    .restart local v4    # "inputStream":Ljava/io/InputStream;
    .restart local v5    # "len":I
    .restart local v8    # "outputStream":Ljava/io/OutputStream;
    :cond_2
    :try_start_6
    invoke-virtual {v6}, Ljava/lang/Process;->destroy()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 97
    const/4 v9, 0x1

    .line 101
    if-eqz v8, :cond_3

    .line 103
    :try_start_7
    invoke-virtual {v8}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    .line 109
    :cond_3
    :goto_5
    if-eqz v4, :cond_4

    .line 111
    :try_start_8
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    :cond_4
    :goto_6
    move-object v7, v8

    .end local v8    # "outputStream":Ljava/io/OutputStream;
    .restart local v7    # "outputStream":Ljava/io/OutputStream;
    move-object v3, v4

    .line 114
    .end local v4    # "inputStream":Ljava/io/InputStream;
    .restart local v3    # "inputStream":Ljava/io/InputStream;
    goto :goto_4

    .line 104
    .end local v3    # "inputStream":Ljava/io/InputStream;
    .end local v7    # "outputStream":Ljava/io/OutputStream;
    .restart local v4    # "inputStream":Ljava/io/InputStream;
    .restart local v8    # "outputStream":Ljava/io/OutputStream;
    :catch_1
    move-exception v2

    .line 105
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v10, "Could not close output stream."

    invoke-static {v10, v2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5

    .line 112
    .end local v2    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v2

    .line 113
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v10, "Could not close input stream."

    invoke-static {v10, v2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_6

    .line 104
    .end local v4    # "inputStream":Ljava/io/InputStream;
    .end local v5    # "len":I
    .end local v8    # "outputStream":Ljava/io/OutputStream;
    .restart local v3    # "inputStream":Ljava/io/InputStream;
    .restart local v7    # "outputStream":Ljava/io/OutputStream;
    :catch_3
    move-exception v2

    .line 105
    const-string v9, "Could not close output stream."

    invoke-static {v9, v2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 112
    :catch_4
    move-exception v2

    .line 113
    const-string v9, "Could not close input stream."

    invoke-static {v9, v2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 101
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v9

    :goto_7
    if-eqz v7, :cond_5

    .line 103
    :try_start_9
    invoke-virtual {v7}, Ljava/io/OutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    .line 109
    :cond_5
    :goto_8
    if-eqz v3, :cond_6

    .line 111
    :try_start_a
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    .line 114
    :cond_6
    :goto_9
    throw v9

    .line 104
    :catch_5
    move-exception v2

    .line 105
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v10, "Could not close output stream."

    invoke-static {v10, v2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_8

    .line 112
    .end local v2    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v2

    .line 113
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v10, "Could not close input stream."

    invoke-static {v10, v2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_9

    .line 101
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "inputStream":Ljava/io/InputStream;
    .restart local v4    # "inputStream":Ljava/io/InputStream;
    :catchall_1
    move-exception v9

    move-object v3, v4

    .end local v4    # "inputStream":Ljava/io/InputStream;
    .restart local v3    # "inputStream":Ljava/io/InputStream;
    goto :goto_7

    .end local v3    # "inputStream":Ljava/io/InputStream;
    .end local v7    # "outputStream":Ljava/io/OutputStream;
    .restart local v4    # "inputStream":Ljava/io/InputStream;
    .restart local v8    # "outputStream":Ljava/io/OutputStream;
    :catchall_2
    move-exception v9

    move-object v7, v8

    .end local v8    # "outputStream":Ljava/io/OutputStream;
    .restart local v7    # "outputStream":Ljava/io/OutputStream;
    move-object v3, v4

    .end local v4    # "inputStream":Ljava/io/InputStream;
    .restart local v3    # "inputStream":Ljava/io/InputStream;
    goto :goto_7

    .line 98
    :catch_7
    move-exception v2

    goto :goto_1

    .end local v3    # "inputStream":Ljava/io/InputStream;
    .restart local v4    # "inputStream":Ljava/io/InputStream;
    :catch_8
    move-exception v2

    move-object v3, v4

    .end local v4    # "inputStream":Ljava/io/InputStream;
    .restart local v3    # "inputStream":Ljava/io/InputStream;
    goto :goto_1
.end method

.method private writeHeader(Ljava/io/OutputStream;)V
    .locals 6
    .param p1, "os"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 122
    const/4 v1, 0x0

    .line 124
    .local v1, "packageInfo":Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/videos/cast/LogCollectorActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/videos/cast/LogCollectorActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 128
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 129
    .local v2, "sb":Ljava/lang/StringBuilder;
    const-string v3, "*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    invoke-virtual {p0}, Lcom/google/android/videos/cast/LogCollectorActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    iget-object v3, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    sget-object v3, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    const-string v3, " - API "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 140
    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    const-string v3, "*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/OutputStream;->write([B)V

    .line 148
    .end local v2    # "sb":Ljava/lang/StringBuilder;
    :goto_0
    return-void

    .line 125
    :catch_0
    move-exception v0

    .line 126
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_0
.end method


# virtual methods
.method protected onStart()V
    .locals 5

    .prologue
    .line 57
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 59
    invoke-virtual {p0}, Lcom/google/android/videos/cast/LogCollectorActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    .line 61
    .local v0, "callingActivity":Landroid/content/ComponentName;
    if-eqz v0, :cond_0

    sget-object v3, Lcom/google/android/videos/cast/LogCollectorActivity;->callerWhitelist:Ljava/util/Set;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 62
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/cast/LogCollectorActivity;->finish()V

    .line 76
    :goto_0
    return-void

    .line 66
    :cond_1
    sget-object v3, Lcom/google/android/videos/cast/LogCollectorActivity;->logcatCmd:Ljava/util/List;

    invoke-direct {p0, v3}, Lcom/google/android/videos/cast/LogCollectorActivity;->collectLogcatOutput(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 68
    .local v1, "result":Ljava/lang/String;
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 69
    .local v2, "returnIntent":Landroid/content/Intent;
    if-eqz v1, :cond_2

    .line 70
    const-string v3, "logcat_file"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 71
    const/4 v3, -0x1

    invoke-virtual {p0, v3, v2}, Lcom/google/android/videos/cast/LogCollectorActivity;->setResult(ILandroid/content/Intent;)V

    .line 75
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/videos/cast/LogCollectorActivity;->finish()V

    goto :goto_0

    .line 73
    :cond_2
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/google/android/videos/cast/LogCollectorActivity;->setResult(I)V

    goto :goto_1
.end method
