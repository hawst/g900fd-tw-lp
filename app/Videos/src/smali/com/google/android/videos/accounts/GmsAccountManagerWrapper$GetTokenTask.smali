.class Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;
.super Landroid/os/AsyncTask;
.source "GmsAccountManagerWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetTokenTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final account:Landroid/accounts/Account;

.field private final activity:Landroid/support/v4/app/FragmentActivity;

.field private final authenticatee:Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

.field private errorCodeForDialog:I

.field private exception:Ljava/lang/Exception;

.field private intentForStartActivity:Landroid/content/Intent;

.field final synthetic this$0:Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;Landroid/support/v4/app/FragmentActivity;Landroid/accounts/Account;Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;)V
    .locals 1
    .param p2, "activity"    # Landroid/support/v4/app/FragmentActivity;
    .param p3, "account"    # Landroid/accounts/Account;
    .param p4, "authenticatee"    # Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    .prologue
    .line 169
    iput-object p1, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->this$0:Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 161
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->errorCodeForDialog:I

    .line 170
    iput-object p2, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->activity:Landroid/support/v4/app/FragmentActivity;

    .line 171
    iput-object p3, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->account:Landroid/accounts/Account;

    .line 172
    iput-object p4, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->authenticatee:Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    .line 173
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;)Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->authenticatee:Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    return-object v0
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 155
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/String;
    .locals 6
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 177
    iget-object v3, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->this$0:Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;

    monitor-enter v3

    .line 178
    :try_start_0
    iget-object v2, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->this$0:Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;

    iget-object v4, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->account:Landroid/accounts/Account;

    # invokes: Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->getCachedToken(Landroid/accounts/Account;)Ljava/lang/String;
    invoke-static {v2, v4}, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->access$100(Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    .line 179
    .local v1, "token":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 180
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v2, v1

    .line 206
    :goto_0
    return-object v2

    .line 183
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->this$0:Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;

    # getter for: Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->applicationContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->access$200(Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;)Landroid/content/Context;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->account:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->this$0:Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;

    iget-object v5, v5, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->scope:Ljava/lang/String;

    invoke-static {v2, v4, v5}, Lcom/google/android/gms/auth/GoogleAuthUtil;->getToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 184
    if-eqz v1, :cond_1

    .line 185
    iget-object v2, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->this$0:Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;

    iget-object v4, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->account:Landroid/accounts/Account;

    # invokes: Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->setCachedToken(Landroid/accounts/Account;Ljava/lang/String;)V
    invoke-static {v2, v4, v1}, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->access$300(Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;Landroid/accounts/Account;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/google/android/gms/auth/GooglePlayServicesAvailabilityException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/gms/auth/UserRecoverableAuthException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 186
    :try_start_2
    monitor-exit v3

    move-object v2, v1

    goto :goto_0

    .line 188
    :catch_0
    move-exception v0

    .line 189
    .local v0, "e":Lcom/google/android/gms/auth/GooglePlayServicesAvailabilityException;
    iget-object v2, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->activity:Landroid/support/v4/app/FragmentActivity;

    if-eqz v2, :cond_2

    .line 190
    invoke-virtual {v0}, Lcom/google/android/gms/auth/GooglePlayServicesAvailabilityException;->getConnectionStatusCode()I

    move-result v2

    iput v2, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->errorCodeForDialog:I

    .line 206
    .end local v0    # "e":Lcom/google/android/gms/auth/GooglePlayServicesAvailabilityException;
    :cond_1
    :goto_1
    const/4 v2, 0x0

    monitor-exit v3

    goto :goto_0

    .line 207
    .end local v1    # "token":Ljava/lang/String;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 192
    .restart local v0    # "e":Lcom/google/android/gms/auth/GooglePlayServicesAvailabilityException;
    .restart local v1    # "token":Ljava/lang/String;
    :cond_2
    :try_start_3
    iput-object v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->exception:Ljava/lang/Exception;

    goto :goto_1

    .line 194
    .end local v0    # "e":Lcom/google/android/gms/auth/GooglePlayServicesAvailabilityException;
    :catch_1
    move-exception v0

    .line 195
    .local v0, "e":Lcom/google/android/gms/auth/UserRecoverableAuthException;
    iget-object v2, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->activity:Landroid/support/v4/app/FragmentActivity;

    if-eqz v2, :cond_3

    .line 196
    invoke-virtual {v0}, Lcom/google/android/gms/auth/UserRecoverableAuthException;->getIntent()Landroid/content/Intent;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->intentForStartActivity:Landroid/content/Intent;

    goto :goto_1

    .line 198
    :cond_3
    iput-object v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->exception:Ljava/lang/Exception;

    goto :goto_1

    .line 200
    .end local v0    # "e":Lcom/google/android/gms/auth/UserRecoverableAuthException;
    :catch_2
    move-exception v0

    .line 201
    .local v0, "e":Ljava/io/IOException;
    iget-object v2, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->this$0:Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    # setter for: Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->lastAuthTimeoutTimestamp:J
    invoke-static {v2, v4, v5}, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->access$402(Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;J)J

    .line 202
    iput-object v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->exception:Ljava/lang/Exception;

    goto :goto_1

    .line 203
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 204
    .local v0, "e":Ljava/lang/Exception;
    iput-object v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->exception:Ljava/lang/Exception;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public onActivityResult(I)V
    .locals 4
    .param p1, "resultCode"    # I

    .prologue
    .line 235
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->this$0:Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;

    iget-object v1, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->account:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->activity:Landroid/support/v4/app/FragmentActivity;

    iget-object v3, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->authenticatee:Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->getAuthTokenInternal(Landroid/accounts/Account;Landroid/app/Activity;Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;)V

    .line 242
    :goto_0
    return-void

    .line 240
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->authenticatee:Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    iget-object v1, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->account:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;->onNotAuthenticated(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 155
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 3
    .param p1, "authToken"    # Ljava/lang/String;

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->authenticatee:Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    instance-of v0, v0, Lcom/google/android/videos/async/CancelableAuthenticatee;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->authenticatee:Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    check-cast v0, Lcom/google/android/videos/async/CancelableAuthenticatee;

    invoke-virtual {v0}, Lcom/google/android/videos/async/CancelableAuthenticatee;->isCanceled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    :goto_0
    return-void

    .line 216
    :cond_0
    if-eqz p1, :cond_1

    .line 217
    iget-object v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->authenticatee:Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    iget-object v1, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->account:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;->onAuthenticated(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 218
    :cond_1
    iget v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->errorCodeForDialog:I

    if-eqz v0, :cond_2

    .line 219
    iget-object v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->this$0:Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;

    iget-object v1, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->account:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    # invokes: Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->saveTask(Ljava/lang/String;Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;)V
    invoke-static {v0, v1, p0}, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->access$500(Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;Ljava/lang/String;Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;)V

    .line 220
    iget-object v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->this$0:Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;

    iget-object v1, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->activity:Landroid/support/v4/app/FragmentActivity;

    iget v2, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->errorCodeForDialog:I

    invoke-static {v0, v1, v2}, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GmsDialogFragment;->show(Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;Landroid/support/v4/app/FragmentActivity;I)V

    goto :goto_0

    .line 221
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->intentForStartActivity:Landroid/content/Intent;

    if-eqz v0, :cond_3

    .line 222
    iget-object v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->this$0:Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;

    iget-object v1, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->account:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    # invokes: Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->saveTask(Ljava/lang/String;Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;)V
    invoke-static {v0, v1, p0}, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->access$500(Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;Ljava/lang/String;Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;)V

    .line 223
    iget-object v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->activity:Landroid/support/v4/app/FragmentActivity;

    iget-object v1, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->intentForStartActivity:Landroid/content/Intent;

    const/16 v2, 0x388

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 224
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->exception:Ljava/lang/Exception;

    if-eqz v0, :cond_4

    .line 225
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GetToken error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->exception:Ljava/lang/Exception;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 226
    iget-object v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->authenticatee:Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    iget-object v1, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->account:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->exception:Ljava/lang/Exception;

    invoke-interface {v0, v1, v2}, Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;->onAuthenticationError(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 229
    :cond_4
    const-string v0, "GetToken error: could not get token and no exception"

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 230
    iget-object v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->authenticatee:Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    iget-object v1, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->account:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;->onNotAuthenticated(Ljava/lang/String;)V

    goto :goto_0
.end method
