.class final Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;
.super Ljava/lang/Object;
.source "ManageDownloadsHelper.java"

# interfaces
.implements Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/ManageDownloadsHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "LibraryItemClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/ui/ManageDownloadsHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/ui/ManageDownloadsHelper;)V
    .locals 0

    .prologue
    .line 353
    iput-object p1, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/ManageDownloadsHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/ui/ManageDownloadsHelper;Lcom/google/android/videos/ui/ManageDownloadsHelper$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/ui/ManageDownloadsHelper;
    .param p2, "x1"    # Lcom/google/android/videos/ui/ManageDownloadsHelper$1;

    .prologue
    .line 353
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;-><init>(Lcom/google/android/videos/ui/ManageDownloadsHelper;)V

    return-void
.end method

.method private handleItemClick(Lcom/google/android/videos/adapter/VideosDataSource;Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V
    .locals 7
    .param p1, "dataSource"    # Lcom/google/android/videos/adapter/VideosDataSource;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "videoId"    # Ljava/lang/String;
    .param p4, "account"    # Ljava/lang/String;
    .param p5, "view"    # Landroid/view/View;

    .prologue
    .line 393
    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->isActive(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 394
    iget-object v0, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/ManageDownloadsHelper;

    # getter for: Lcom/google/android/videos/ui/ManageDownloadsHelper;->activity:Landroid/support/v4/app/FragmentActivity;
    invoke-static {v0}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->access$600(Lcom/google/android/videos/ui/ManageDownloadsHelper;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/16 v1, 0xf

    invoke-static {v0, p3, p4, v1}, Lcom/google/android/videos/utils/PlayStoreUtil;->viewMovieDetails(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;I)V

    .line 403
    .end local p5    # "view":Landroid/view/View;
    :goto_0
    return-void

    .line 395
    .restart local p5    # "view":Landroid/view/View;
    :cond_0
    instance-of v0, p1, Lcom/google/android/videos/adapter/MoviesDataSource;

    if-eqz v0, :cond_1

    .line 396
    check-cast p5, Lcom/google/android/videos/ui/DownloadItemView;

    .end local p5    # "view":Landroid/view/View;
    invoke-direct {p0, p4, p3, p5}, Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;->startMovieDetailsActivity(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/ui/DownloadItemView;)V

    goto :goto_0

    .line 398
    .restart local p5    # "view":Landroid/view/View;
    :cond_1
    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getSeasonId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    .line 399
    .local v3, "seasonId":Ljava/lang/String;
    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getShowId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    .line 400
    .local v4, "showId":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/ManageDownloadsHelper;

    # getter for: Lcom/google/android/videos/ui/ManageDownloadsHelper;->activity:Landroid/support/v4/app/FragmentActivity;
    invoke-static {v0}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->access$600(Lcom/google/android/videos/ui/ManageDownloadsHelper;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/ManageDownloadsHelper;

    # getter for: Lcom/google/android/videos/ui/ManageDownloadsHelper;->activity:Landroid/support/v4/app/FragmentActivity;
    invoke-static {v0}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->access$600(Lcom/google/android/videos/ui/ManageDownloadsHelper;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v5, 0x0

    move-object v1, p4

    move-object v2, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/activity/WatchActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private handleMiniPosterClick(Lcom/google/android/videos/adapter/VideosDataSource;Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V
    .locals 10
    .param p1, "dataSource"    # Lcom/google/android/videos/adapter/VideosDataSource;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "videoId"    # Ljava/lang/String;
    .param p4, "account"    # Ljava/lang/String;
    .param p5, "view"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    .line 407
    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getShowId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    .line 408
    .local v2, "showId":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 409
    iget-object v0, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/ManageDownloadsHelper;

    # getter for: Lcom/google/android/videos/ui/ManageDownloadsHelper;->activity:Landroid/support/v4/app/FragmentActivity;
    invoke-static {v0}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->access$600(Lcom/google/android/videos/ui/ManageDownloadsHelper;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getSeasonId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    move-object v1, p4

    move-object v4, p3

    move v6, v5

    invoke-static/range {v0 .. v6}, Lcom/google/android/videos/activity/ShowActivity;->createEpisodeIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/content/Intent;

    move-result-object v8

    .line 411
    .local v8, "intent":Landroid/content/Intent;
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 412
    new-instance v9, Lcom/google/android/videos/activity/ShowActivity$SharedElements;

    invoke-direct {v9}, Lcom/google/android/videos/activity/ShowActivity$SharedElements;-><init>()V

    .line 413
    .local v9, "shared":Lcom/google/android/videos/activity/ShowActivity$SharedElements;
    invoke-static {p5}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->findThumbnailView(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    iput-object v0, v9, Lcom/google/android/videos/activity/ShowActivity$SharedElements;->poster:Landroid/view/View;

    .line 414
    iget-object v0, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/ManageDownloadsHelper;

    # getter for: Lcom/google/android/videos/ui/ManageDownloadsHelper;->activity:Landroid/support/v4/app/FragmentActivity;
    invoke-static {v0}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->access$600(Lcom/google/android/videos/ui/ManageDownloadsHelper;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, v8, v9}, Lcom/google/android/videos/activity/ShowActivity;->createAnimationBundleV21(Landroid/app/Activity;Landroid/content/Intent;Lcom/google/android/videos/activity/ShowActivity$SharedElements;)Landroid/os/Bundle;

    move-result-object v7

    .line 415
    .local v7, "bundle":Landroid/os/Bundle;
    iget-object v0, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/ManageDownloadsHelper;

    # getter for: Lcom/google/android/videos/ui/ManageDownloadsHelper;->activity:Landroid/support/v4/app/FragmentActivity;
    invoke-static {v0}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->access$600(Lcom/google/android/videos/ui/ManageDownloadsHelper;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, v8, v7}, Landroid/support/v4/app/ActivityCompat;->startActivity(Landroid/app/Activity;Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 422
    .end local v7    # "bundle":Landroid/os/Bundle;
    .end local v8    # "intent":Landroid/content/Intent;
    .end local v9    # "shared":Lcom/google/android/videos/activity/ShowActivity$SharedElements;
    .end local p5    # "view":Landroid/view/View;
    :goto_0
    return-void

    .line 417
    .restart local v8    # "intent":Landroid/content/Intent;
    .restart local p5    # "view":Landroid/view/View;
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/ManageDownloadsHelper;

    # getter for: Lcom/google/android/videos/ui/ManageDownloadsHelper;->activity:Landroid/support/v4/app/FragmentActivity;
    invoke-static {v0}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->access$600(Lcom/google/android/videos/ui/ManageDownloadsHelper;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 420
    .end local v8    # "intent":Landroid/content/Intent;
    :cond_1
    check-cast p5, Lcom/google/android/videos/ui/DownloadItemView;

    .end local p5    # "view":Landroid/view/View;
    invoke-direct {p0, p4, p3, p5}, Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;->startMovieDetailsActivity(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/ui/DownloadItemView;)V

    goto :goto_0
.end method

.method private handlePinClick(Lcom/google/android/videos/adapter/VideosDataSource;Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "dataSource"    # Lcom/google/android/videos/adapter/VideosDataSource;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "videoId"    # Ljava/lang/String;
    .param p4, "account"    # Ljava/lang/String;

    .prologue
    .line 378
    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getPinningStatus(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v8

    .line 379
    .local v8, "pinningStatus":Ljava/lang/Integer;
    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->isPinned(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 380
    iget-object v0, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/ManageDownloadsHelper;

    # getter for: Lcom/google/android/videos/ui/ManageDownloadsHelper;->pinHelper:Lcom/google/android/videos/ui/PinHelper;
    invoke-static {v0}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->access$700(Lcom/google/android/videos/ui/ManageDownloadsHelper;)Lcom/google/android/videos/ui/PinHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/ManageDownloadsHelper;

    # getter for: Lcom/google/android/videos/ui/ManageDownloadsHelper;->activity:Landroid/support/v4/app/FragmentActivity;
    invoke-static {v1}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->access$600(Lcom/google/android/videos/ui/ManageDownloadsHelper;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getShowTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getPinningStatusReason(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v7

    move-object v2, p4

    move-object v3, p3

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/videos/ui/PinHelper;->onUnpinClicked(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Integer;)V

    .line 389
    :goto_0
    return-void

    .line 383
    :cond_0
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v8, v0}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 384
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;->showErrorDialog(Lcom/google/android/videos/adapter/VideosDataSource;Landroid/database/Cursor;)V

    goto :goto_0

    .line 386
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/ManageDownloadsHelper;

    # getter for: Lcom/google/android/videos/ui/ManageDownloadsHelper;->pinHelper:Lcom/google/android/videos/ui/PinHelper;
    invoke-static {v0}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->access$700(Lcom/google/android/videos/ui/ManageDownloadsHelper;)Lcom/google/android/videos/ui/PinHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/ManageDownloadsHelper;

    # getter for: Lcom/google/android/videos/ui/ManageDownloadsHelper;->activity:Landroid/support/v4/app/FragmentActivity;
    invoke-static {v1}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->access$600(Lcom/google/android/videos/ui/ManageDownloadsHelper;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1, p4, p3}, Lcom/google/android/videos/ui/PinHelper;->pinVideo(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    iget-object v0, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/ManageDownloadsHelper;

    # getter for: Lcom/google/android/videos/ui/ManageDownloadsHelper;->eventLogger:Lcom/google/android/videos/logging/EventLogger;
    invoke-static {v0}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->access$800(Lcom/google/android/videos/ui/ManageDownloadsHelper;)Lcom/google/android/videos/logging/EventLogger;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/videos/logging/EventLogger;->onPinClick(Z)V

    goto :goto_0
.end method

.method private showErrorDialog(Lcom/google/android/videos/adapter/VideosDataSource;Landroid/database/Cursor;)V
    .locals 7
    .param p1, "dataSource"    # Lcom/google/android/videos/adapter/VideosDataSource;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 438
    iget-object v0, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/ManageDownloadsHelper;

    # getter for: Lcom/google/android/videos/ui/ManageDownloadsHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;
    invoke-static {v0}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->access$400(Lcom/google/android/videos/ui/ManageDownloadsHelper;)Lcom/google/android/videos/ui/SyncHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v1

    .line 439
    .local v1, "account":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 449
    :goto_0
    return-void

    .line 442
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/ManageDownloadsHelper;

    # getter for: Lcom/google/android/videos/ui/ManageDownloadsHelper;->activity:Landroid/support/v4/app/FragmentActivity;
    invoke-static {v0}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->access$600(Lcom/google/android/videos/ui/ManageDownloadsHelper;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getPinningStatusReason(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getDownloadSize(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getPinningDrmErrorCode(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->isRental(Landroid/database/Cursor;)Z

    move-result v6

    invoke-static/range {v0 .. v6}, Lcom/google/android/videos/ui/PinHelper;->showErrorDialog(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Long;Ljava/lang/Integer;Z)V

    goto :goto_0
.end method

.method private startMovieDetailsActivity(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/ui/DownloadItemView;)V
    .locals 5
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "itemView"    # Lcom/google/android/videos/ui/DownloadItemView;

    .prologue
    .line 426
    iget-object v3, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/ManageDownloadsHelper;

    # getter for: Lcom/google/android/videos/ui/ManageDownloadsHelper;->activity:Landroid/support/v4/app/FragmentActivity;
    invoke-static {v3}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->access$600(Lcom/google/android/videos/ui/ManageDownloadsHelper;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3, p1, p2}, Lcom/google/android/videos/activity/MovieDetailsActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 427
    .local v1, "intent":Landroid/content/Intent;
    sget v3, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v4, 0x15

    if-lt v3, v4, :cond_0

    .line 428
    new-instance v2, Lcom/google/android/videos/activity/MovieDetailsActivity$SharedElements;

    invoke-direct {v2}, Lcom/google/android/videos/activity/MovieDetailsActivity$SharedElements;-><init>()V

    .line 429
    .local v2, "shared":Lcom/google/android/videos/activity/MovieDetailsActivity$SharedElements;
    const v3, 0x7f0f00c9

    invoke-virtual {p3, v3}, Lcom/google/android/videos/ui/DownloadItemView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/videos/activity/MovieDetailsActivity$SharedElements;->poster:Landroid/view/View;

    .line 430
    iget-object v3, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/ManageDownloadsHelper;

    # getter for: Lcom/google/android/videos/ui/ManageDownloadsHelper;->activity:Landroid/support/v4/app/FragmentActivity;
    invoke-static {v3}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->access$600(Lcom/google/android/videos/ui/ManageDownloadsHelper;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3, v1, v2}, Lcom/google/android/videos/activity/MovieDetailsActivity;->createAnimationBundleV21(Landroid/app/Activity;Landroid/content/Intent;Lcom/google/android/videos/activity/MovieDetailsActivity$SharedElements;)Landroid/os/Bundle;

    move-result-object v0

    .line 431
    .local v0, "bundle":Landroid/os/Bundle;
    iget-object v3, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/ManageDownloadsHelper;

    # getter for: Lcom/google/android/videos/ui/ManageDownloadsHelper;->activity:Landroid/support/v4/app/FragmentActivity;
    invoke-static {v3}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->access$600(Lcom/google/android/videos/ui/ManageDownloadsHelper;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3, v1, v0}, Landroid/support/v4/app/ActivityCompat;->startActivity(Landroid/app/Activity;Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 435
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v2    # "shared":Lcom/google/android/videos/activity/MovieDetailsActivity$SharedElements;
    :goto_0
    return-void

    .line 433
    :cond_0
    iget-object v3, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/ManageDownloadsHelper;

    # getter for: Lcom/google/android/videos/ui/ManageDownloadsHelper;->activity:Landroid/support/v4/app/FragmentActivity;
    invoke-static {v3}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->access$600(Lcom/google/android/videos/ui/ManageDownloadsHelper;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method public onItemClick(Lcom/google/android/videos/adapter/DataSource;ILandroid/view/View;)V
    .locals 6
    .param p2, "position"    # I
    .param p3, "view"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/adapter/DataSource",
            "<*>;I",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 357
    .local p1, "dataSource":Lcom/google/android/videos/adapter/DataSource;, "Lcom/google/android/videos/adapter/DataSource<*>;"
    move-object v1, p1

    check-cast v1, Lcom/google/android/videos/adapter/VideosDataSource;

    .line 358
    .local v1, "videosDataSource":Lcom/google/android/videos/adapter/VideosDataSource;
    invoke-interface {v1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/database/Cursor;

    .line 359
    .local v2, "cursor":Landroid/database/Cursor;
    invoke-interface {v1, v2}, Lcom/google/android/videos/adapter/VideosDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    .line 360
    .local v3, "videoId":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/ManageDownloadsHelper;

    # getter for: Lcom/google/android/videos/ui/ManageDownloadsHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;
    invoke-static {v0}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->access$400(Lcom/google/android/videos/ui/ManageDownloadsHelper;)Lcom/google/android/videos/ui/SyncHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v4

    .line 362
    .local v4, "account":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 370
    iget-object v0, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/ManageDownloadsHelper;

    # getter for: Lcom/google/android/videos/ui/ManageDownloadsHelper;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;
    invoke-static {v0}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->access$500(Lcom/google/android/videos/ui/ManageDownloadsHelper;)Lcom/google/android/videos/logging/UiEventLoggingHelper;

    move-result-object v0

    invoke-static {p3}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->findUiElementNode(Landroid/view/View;)Lcom/google/android/videos/logging/UiElementNode;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->sendClickEvent(Lcom/google/android/videos/logging/UiElementNode;)V

    move-object v0, p0

    move-object v5, p3

    .line 371
    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;->handleItemClick(Lcom/google/android/videos/adapter/VideosDataSource;Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V

    .line 374
    :goto_0
    return-void

    .line 364
    :sswitch_0
    invoke-direct {p0, v1, v2, v3, v4}, Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;->handlePinClick(Lcom/google/android/videos/adapter/VideosDataSource;Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    move-object v0, p0

    move-object v5, p3

    .line 367
    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;->handleMiniPosterClick(Lcom/google/android/videos/adapter/VideosDataSource;Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V

    goto :goto_0

    .line 362
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0f00d1 -> :sswitch_0
        0x7f0f00d5 -> :sswitch_1
    .end sparse-switch
.end method
