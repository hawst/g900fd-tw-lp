.class public final Lcom/google/android/videos/proto/FilmProtos$Album;
.super Lcom/google/protobuf/nano/MessageNano;
.source "FilmProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/proto/FilmProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Album"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/videos/proto/FilmProtos$Album;


# instance fields
.field public metajamId:[Ljava/lang/String;

.field public mid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 564
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 565
    invoke-virtual {p0}, Lcom/google/android/videos/proto/FilmProtos$Album;->clear()Lcom/google/android/videos/proto/FilmProtos$Album;

    .line 566
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/videos/proto/FilmProtos$Album;
    .locals 2

    .prologue
    .line 547
    sget-object v0, Lcom/google/android/videos/proto/FilmProtos$Album;->_emptyArray:[Lcom/google/android/videos/proto/FilmProtos$Album;

    if-nez v0, :cond_1

    .line 548
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 550
    :try_start_0
    sget-object v0, Lcom/google/android/videos/proto/FilmProtos$Album;->_emptyArray:[Lcom/google/android/videos/proto/FilmProtos$Album;

    if-nez v0, :cond_0

    .line 551
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/videos/proto/FilmProtos$Album;

    sput-object v0, Lcom/google/android/videos/proto/FilmProtos$Album;->_emptyArray:[Lcom/google/android/videos/proto/FilmProtos$Album;

    .line 553
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 555
    :cond_1
    sget-object v0, Lcom/google/android/videos/proto/FilmProtos$Album;->_emptyArray:[Lcom/google/android/videos/proto/FilmProtos$Album;

    return-object v0

    .line 553
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/videos/proto/FilmProtos$Album;
    .locals 1

    .prologue
    .line 569
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Album;->mid:Ljava/lang/String;

    .line 570
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Album;->metajamId:[Ljava/lang/String;

    .line 571
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/proto/FilmProtos$Album;->cachedSize:I

    .line 572
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 7

    .prologue
    .line 627
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 628
    .local v4, "size":I
    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Album;->mid:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 629
    const/4 v5, 0x1

    iget-object v6, p0, Lcom/google/android/videos/proto/FilmProtos$Album;->mid:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 632
    :cond_0
    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Album;->metajamId:[Ljava/lang/String;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Album;->metajamId:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_3

    .line 633
    const/4 v0, 0x0

    .line 634
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 635
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Album;->metajamId:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_2

    .line 636
    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Album;->metajamId:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 637
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 638
    add-int/lit8 v0, v0, 0x1

    .line 639
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 635
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 643
    .end local v2    # "element":Ljava/lang/String;
    :cond_2
    add-int/2addr v4, v1

    .line 644
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 646
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_3
    return v4
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 577
    if-ne p1, p0, :cond_1

    .line 595
    :cond_0
    :goto_0
    return v1

    .line 580
    :cond_1
    instance-of v3, p1, Lcom/google/android/videos/proto/FilmProtos$Album;

    if-nez v3, :cond_2

    move v1, v2

    .line 581
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 583
    check-cast v0, Lcom/google/android/videos/proto/FilmProtos$Album;

    .line 584
    .local v0, "other":Lcom/google/android/videos/proto/FilmProtos$Album;
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Album;->mid:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 585
    iget-object v3, v0, Lcom/google/android/videos/proto/FilmProtos$Album;->mid:Ljava/lang/String;

    if-eqz v3, :cond_4

    move v1, v2

    .line 586
    goto :goto_0

    .line 588
    :cond_3
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Album;->mid:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Album;->mid:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 589
    goto :goto_0

    .line 591
    :cond_4
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Album;->metajamId:[Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Album;->metajamId:[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 593
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 600
    const/16 v0, 0x11

    .line 601
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Album;->mid:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 603
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Album;->metajamId:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 605
    return v0

    .line 601
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Album;->mid:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/FilmProtos$Album;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 654
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 655
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 659
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 660
    :sswitch_0
    return-object p0

    .line 665
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Album;->mid:Ljava/lang/String;

    goto :goto_0

    .line 669
    :sswitch_2
    const/16 v5, 0x12

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 671
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Album;->metajamId:[Ljava/lang/String;

    if-nez v5, :cond_2

    move v1, v4

    .line 672
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 673
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 674
    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Album;->metajamId:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 676
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 677
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 678
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 676
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 671
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Album;->metajamId:[Ljava/lang/String;

    array-length v1, v5

    goto :goto_1

    .line 681
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 682
    iput-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Album;->metajamId:[Ljava/lang/String;

    goto :goto_0

    .line 655
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 541
    invoke-virtual {p0, p1}, Lcom/google/android/videos/proto/FilmProtos$Album;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/FilmProtos$Album;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 611
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Album;->mid:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 612
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Album;->mid:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 614
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Album;->metajamId:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Album;->metajamId:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 615
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Album;->metajamId:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 616
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Album;->metajamId:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 617
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 618
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 615
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 622
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 623
    return-void
.end method
