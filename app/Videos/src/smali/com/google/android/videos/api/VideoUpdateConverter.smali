.class final Lcom/google/android/videos/api/VideoUpdateConverter;
.super Lcom/google/android/videos/converter/HttpResponseConverter;
.source "VideoUpdateConverter.java"

# interfaces
.implements Lcom/google/android/videos/converter/RequestConverter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/converter/HttpResponseConverter",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
        ">;",
        "Lcom/google/android/videos/converter/RequestConverter",
        "<",
        "Lcom/google/android/videos/api/VideoUpdateRequest;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        ">;"
    }
.end annotation


# instance fields
.field private final baseUri:Ljava/lang/String;

.field private final byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArrayPool;)V
    .locals 2
    .param p1, "baseUri"    # Landroid/net/Uri;
    .param p2, "byteArrayPool"    # Lcom/google/android/videos/utils/ByteArrayPool;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/videos/converter/HttpResponseConverter;-><init>()V

    .line 34
    new-instance v0, Lcom/google/android/videos/utils/UriBuilder;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "video"

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;->addSegment(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/utils/UriBuilder;->build()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/api/VideoUpdateConverter;->baseUri:Ljava/lang/String;

    .line 35
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/utils/ByteArrayPool;

    iput-object v0, p0, Lcom/google/android/videos/api/VideoUpdateConverter;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;

    .line 36
    return-void
.end method


# virtual methods
.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 25
    check-cast p1, Lcom/google/android/videos/api/VideoUpdateRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/api/VideoUpdateConverter;->convertRequest(Lcom/google/android/videos/api/VideoUpdateRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public convertRequest(Lcom/google/android/videos/api/VideoUpdateRequest;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 5
    .param p1, "request"    # Lcom/google/android/videos/api/VideoUpdateRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 40
    new-instance v3, Lcom/google/android/videos/utils/UriBuilder;

    iget-object v4, p0, Lcom/google/android/videos/api/VideoUpdateConverter;->baseUri:Ljava/lang/String;

    invoke-direct {v3, v4}, Lcom/google/android/videos/utils/UriBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lcom/google/android/videos/api/VideoUpdateRequest;->videoId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/videos/utils/UriBuilder;->addSegment(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/videos/utils/UriBuilder;->build()Ljava/lang/String;

    move-result-object v1

    .line 41
    .local v1, "uri":Ljava/lang/String;
    new-instance v2, Lcom/google/wireless/android/video/magma/proto/VideoResource;

    invoke-direct {v2}, Lcom/google/wireless/android/video/magma/proto/VideoResource;-><init>()V

    .line 42
    .local v2, "videoResource":Lcom/google/wireless/android/video/magma/proto/VideoResource;
    new-instance v3, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-direct {v3}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;-><init>()V

    iput-object v3, v2, Lcom/google/wireless/android/video/magma/proto/VideoResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 43
    iget-object v3, v2, Lcom/google/wireless/android/video/magma/proto/VideoResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v4, p1, Lcom/google/android/videos/api/VideoUpdateRequest;->videoId:Ljava/lang/String;

    iput-object v4, v3, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    .line 44
    iget-object v3, p1, Lcom/google/android/videos/api/VideoUpdateRequest;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    iput-object v3, v2, Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    .line 45
    invoke-static {v2}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v0

    .line 46
    .local v0, "content":[B
    invoke-static {v1, v0}, Lcom/google/android/videos/async/HttpUriRequestFactory;->createPut(Ljava/lang/String;[B)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v3

    return-object v3
.end method

.method public convertResponseEntity(Lorg/apache/http/HttpEntity;)Lcom/google/wireless/android/video/magma/proto/VideoResource;
    .locals 2
    .param p1, "entity"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 52
    new-instance v0, Lcom/google/wireless/android/video/magma/proto/VideoResource;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;-><init>()V

    iget-object v1, p0, Lcom/google/android/videos/api/VideoUpdateConverter;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;

    invoke-static {v0, p1, v1}, Lcom/google/android/videos/utils/EntityUtils;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;Lorg/apache/http/HttpEntity;Lcom/google/android/videos/utils/ByteArrayPool;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/VideoResource;

    return-object v0
.end method

.method public bridge synthetic convertResponseEntity(Lorg/apache/http/HttpEntity;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lcom/google/android/videos/api/VideoUpdateConverter;->convertResponseEntity(Lorg/apache/http/HttpEntity;)Lcom/google/wireless/android/video/magma/proto/VideoResource;

    move-result-object v0

    return-object v0
.end method
