.class Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;
.super Ljava/lang/Object;
.source "LegacyPlayerStreamsSelector.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DrmCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/drm/DrmRequest;",
        "Lcom/google/android/videos/drm/DrmResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final authenticateOfflineRequest:Z

.field private expectedResponses:I

.field private onErrorAlreadyInvoked:Z

.field private final originalCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/player/VideoInfo;",
            "Lcom/google/android/videos/streams/LegacyStreamSelection;",
            ">;"
        }
    .end annotation
.end field

.field private final selection:Lcom/google/android/videos/streams/LegacyStreamSelection;

.field final synthetic this$0:Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;

.field private final videoInfo:Lcom/google/android/videos/player/VideoInfo;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;Lcom/google/android/videos/player/VideoInfo;Lcom/google/android/videos/streams/LegacyStreamSelection;ZLcom/google/android/videos/async/Callback;)V
    .locals 1
    .param p2, "videoInfo"    # Lcom/google/android/videos/player/VideoInfo;
    .param p3, "selection"    # Lcom/google/android/videos/streams/LegacyStreamSelection;
    .param p4, "authenticateOfflineRequest"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/player/VideoInfo;",
            "Lcom/google/android/videos/streams/LegacyStreamSelection;",
            "Z",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/player/VideoInfo;",
            "Lcom/google/android/videos/streams/LegacyStreamSelection;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 145
    .local p5, "originalCallback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/player/VideoInfo;Lcom/google/android/videos/streams/LegacyStreamSelection;>;"
    iput-object p1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;->this$0:Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    iput-object p2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    .line 147
    iput-object p3, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;->selection:Lcom/google/android/videos/streams/LegacyStreamSelection;

    .line 148
    iput-boolean p4, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;->authenticateOfflineRequest:Z

    .line 149
    iput-object p5, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;->originalCallback:Lcom/google/android/videos/async/Callback;

    .line 150
    iget-boolean v0, p3, Lcom/google/android/videos/streams/LegacyStreamSelection;->supportsQualityToggle:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    iput v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;->expectedResponses:I

    .line 151
    return-void

    .line 150
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/drm/DrmRequest;Ljava/lang/Exception;)V
    .locals 6
    .param p1, "request"    # Lcom/google/android/videos/drm/DrmRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    const/4 v5, 0x1

    .line 163
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DRM error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/videos/drm/DrmRequest;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 164
    iget-boolean v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;->onErrorAlreadyInvoked:Z

    if-eqz v1, :cond_0

    .line 185
    :goto_0
    return-void

    .line 169
    :cond_0
    iput-boolean v5, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;->onErrorAlreadyInvoked:Z

    .line 171
    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-boolean v1, v1, Lcom/google/android/videos/player/VideoInfo;->isOffline:Z

    if-nez v1, :cond_1

    instance-of v1, p2, Lcom/google/android/videos/drm/DrmFallbackException;

    if-eqz v1, :cond_1

    move-object v0, p2

    .line 172
    check-cast v0, Lcom/google/android/videos/drm/DrmFallbackException;

    .line 173
    .local v0, "drmFallbackException":Lcom/google/android/videos/drm/DrmFallbackException;
    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;->this$0:Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;

    iget-object v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget v3, v0, Lcom/google/android/videos/drm/DrmFallbackException;->fallbackDrmLevel:I

    iget-object v4, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;->originalCallback:Lcom/google/android/videos/async/Callback;

    # invokes: Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;->selectDrmStreamsInternal(Lcom/google/android/videos/player/VideoInfo;ILcom/google/android/videos/async/Callback;)V
    invoke-static {v1, v2, v3, v4}, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;->access$000(Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;Lcom/google/android/videos/player/VideoInfo;ILcom/google/android/videos/async/Callback;)V

    goto :goto_0

    .line 178
    .end local v0    # "drmFallbackException":Lcom/google/android/videos/drm/DrmFallbackException;
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-boolean v1, v1, Lcom/google/android/videos/player/VideoInfo;->isOffline:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;->authenticateOfflineRequest:Z

    if-nez v1, :cond_2

    .line 180
    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;->this$0:Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;

    iget-object v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-object v3, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;->selection:Lcom/google/android/videos/streams/LegacyStreamSelection;

    iget-object v4, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;->originalCallback:Lcom/google/android/videos/async/Callback;

    # invokes: Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;->licenseStreams(Lcom/google/android/videos/player/VideoInfo;Lcom/google/android/videos/streams/LegacyStreamSelection;ZLcom/google/android/videos/async/Callback;)V
    invoke-static {v1, v2, v3, v5, v4}, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;->access$100(Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;Lcom/google/android/videos/player/VideoInfo;Lcom/google/android/videos/streams/LegacyStreamSelection;ZLcom/google/android/videos/async/Callback;)V

    goto :goto_0

    .line 184
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;->originalCallback:Lcom/google/android/videos/async/Callback;

    iget-object v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    invoke-interface {v1, v2, p2}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 133
    check-cast p1, Lcom/google/android/videos/drm/DrmRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;->onError(Lcom/google/android/videos/drm/DrmRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/drm/DrmResponse;)V
    .locals 3
    .param p1, "request"    # Lcom/google/android/videos/drm/DrmRequest;
    .param p2, "response"    # Lcom/google/android/videos/drm/DrmResponse;

    .prologue
    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DRM response ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;->expectedResponses:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/videos/drm/DrmResponse;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 156
    iget v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;->expectedResponses:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;->expectedResponses:I

    if-nez v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;->originalCallback:Lcom/google/android/videos/async/Callback;

    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-object v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;->selection:Lcom/google/android/videos/streams/LegacyStreamSelection;

    invoke-interface {v0, v1, v2}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 159
    :cond_0
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 133
    check-cast p1, Lcom/google/android/videos/drm/DrmRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/videos/drm/DrmResponse;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;->onResponse(Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/drm/DrmResponse;)V

    return-void
.end method
