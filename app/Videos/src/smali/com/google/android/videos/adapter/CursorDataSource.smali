.class public abstract Lcom/google/android/videos/adapter/CursorDataSource;
.super Lcom/google/android/videos/adapter/AbstractDataSource;
.source "CursorDataSource.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/adapter/AbstractDataSource",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private cursor:Landroid/database/Cursor;

.field private cursorObserver:Landroid/database/DataSetObserver;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/android/videos/adapter/AbstractDataSource;-><init>()V

    return-void
.end method

.method private getCursorObserver()Landroid/database/DataSetObserver;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/videos/adapter/CursorDataSource;->cursorObserver:Landroid/database/DataSetObserver;

    if-nez v0, :cond_0

    .line 53
    new-instance v0, Lcom/google/android/videos/adapter/CursorDataSource$1;

    invoke-direct {v0, p0}, Lcom/google/android/videos/adapter/CursorDataSource$1;-><init>(Lcom/google/android/videos/adapter/CursorDataSource;)V

    iput-object v0, p0, Lcom/google/android/videos/adapter/CursorDataSource;->cursorObserver:Landroid/database/DataSetObserver;

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/adapter/CursorDataSource;->cursorObserver:Landroid/database/DataSetObserver;

    return-object v0
.end method


# virtual methods
.method public changeCursor(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "newCursor"    # Landroid/database/Cursor;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/videos/adapter/CursorDataSource;->cursor:Landroid/database/Cursor;

    if-ne p1, v0, :cond_0

    .line 49
    :goto_0
    return-void

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/adapter/CursorDataSource;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    .line 40
    iget-object v0, p0, Lcom/google/android/videos/adapter/CursorDataSource;->cursor:Landroid/database/Cursor;

    invoke-direct {p0}, Lcom/google/android/videos/adapter/CursorDataSource;->getCursorObserver()Landroid/database/DataSetObserver;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 41
    iget-object v0, p0, Lcom/google/android/videos/adapter/CursorDataSource;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 43
    :cond_1
    iput-object p1, p0, Lcom/google/android/videos/adapter/CursorDataSource;->cursor:Landroid/database/Cursor;

    .line 44
    iget-object v0, p0, Lcom/google/android/videos/adapter/CursorDataSource;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_2

    .line 45
    iget-object v0, p0, Lcom/google/android/videos/adapter/CursorDataSource;->cursor:Landroid/database/Cursor;

    invoke-direct {p0}, Lcom/google/android/videos/adapter/CursorDataSource;->getCursorObserver()Landroid/database/DataSetObserver;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 48
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/videos/adapter/CursorDataSource;->notifyChanged()V

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/videos/adapter/CursorDataSource;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/adapter/CursorDataSource;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Landroid/database/Cursor;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/videos/adapter/CursorDataSource;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/adapter/CursorDataSource;->cursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 24
    iget-object v0, p0, Lcom/google/android/videos/adapter/CursorDataSource;->cursor:Landroid/database/Cursor;

    return-object v0

    .line 26
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", size is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/videos/adapter/CursorDataSource;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IndexOutOfBoundsException;
        }
    .end annotation

    .prologue
    .line 11
    invoke-virtual {p0, p1}, Lcom/google/android/videos/adapter/CursorDataSource;->getItem(I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public hasCursor()Z
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/videos/adapter/CursorDataSource;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCursor(Landroid/database/Cursor;)Z
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 77
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/adapter/CursorDataSource;->cursor:Landroid/database/Cursor;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
