.class public abstract Lcom/google/android/videos/ui/playnext/CardItemView;
.super Lcom/google/android/videos/ui/playnext/ClusterItemView;
.source "CardItemView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/videos/ui/playnext/CardItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 22
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/videos/ui/playnext/CardItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/ui/playnext/ClusterItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/CardItemView;->getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;

    move-result-object v0

    invoke-interface {v0, p0, p1, p2, p3}, Lcom/google/android/play/cardview/CardViewGroupDelegate;->initialize(Landroid/view/View;Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    return-void
.end method


# virtual methods
.method public getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/google/android/play/cardview/CardViewGroupDelegates;->IMPL:Lcom/google/android/play/cardview/CardViewGroupDelegate;

    return-object v0
.end method

.method public setBackgroundColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/CardItemView;->getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Lcom/google/android/play/cardview/CardViewGroupDelegate;->setBackgroundColor(Landroid/view/View;I)V

    .line 38
    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 1
    .param p1, "resid"    # I

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/CardItemView;->getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Lcom/google/android/play/cardview/CardViewGroupDelegate;->setBackgroundResource(Landroid/view/View;I)V

    .line 47
    return-void
.end method
