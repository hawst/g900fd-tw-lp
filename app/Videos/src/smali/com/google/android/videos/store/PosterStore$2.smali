.class Lcom/google/android/videos/store/PosterStore$2;
.super Ljava/lang/Object;
.source "PosterStore.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/store/PosterStore;->getImage(Ljava/lang/String;Lcom/google/android/videos/store/FileStore;Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/store/PosterStore;

.field final synthetic val$bitmapCacheKey:Ljava/lang/String;

.field final synthetic val$bitmapConverter:Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;

.field final synthetic val$callback:Lcom/google/android/videos/async/Callback;

.field final synthetic val$fileName:Ljava/lang/String;

.field final synthetic val$fileStore:Lcom/google/android/videos/store/FileStore;


# direct methods
.method constructor <init>(Lcom/google/android/videos/store/PosterStore;Lcom/google/android/videos/store/FileStore;Ljava/lang/String;Lcom/google/android/videos/async/Callback;Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lcom/google/android/videos/store/PosterStore$2;->this$0:Lcom/google/android/videos/store/PosterStore;

    iput-object p2, p0, Lcom/google/android/videos/store/PosterStore$2;->val$fileStore:Lcom/google/android/videos/store/FileStore;

    iput-object p3, p0, Lcom/google/android/videos/store/PosterStore$2;->val$fileName:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/videos/store/PosterStore$2;->val$callback:Lcom/google/android/videos/async/Callback;

    iput-object p5, p0, Lcom/google/android/videos/store/PosterStore$2;->val$bitmapConverter:Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;

    iput-object p6, p0, Lcom/google/android/videos/store/PosterStore$2;->val$bitmapCacheKey:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 130
    :try_start_0
    iget-object v2, p0, Lcom/google/android/videos/store/PosterStore$2;->val$fileStore:Lcom/google/android/videos/store/FileStore;

    iget-object v3, p0, Lcom/google/android/videos/store/PosterStore$2;->val$fileName:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/videos/store/PosterStore$2;->this$0:Lcom/google/android/videos/store/PosterStore;

    # getter for: Lcom/google/android/videos/store/PosterStore;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;
    invoke-static {v4}, Lcom/google/android/videos/store/PosterStore;->access$000(Lcom/google/android/videos/store/PosterStore;)Lcom/google/android/videos/utils/ByteArrayPool;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/videos/store/FileStore;->getBytes(Ljava/lang/String;Lcom/google/android/videos/utils/ByteArrayPool;)Lcom/google/android/videos/utils/ByteArray;

    move-result-object v0

    .line 131
    .local v0, "byteArray":Lcom/google/android/videos/utils/ByteArray;
    if-nez v0, :cond_0

    .line 132
    iget-object v2, p0, Lcom/google/android/videos/store/PosterStore$2;->val$callback:Lcom/google/android/videos/async/Callback;

    iget-object v3, p0, Lcom/google/android/videos/store/PosterStore$2;->val$fileName:Ljava/lang/String;

    new-instance v4, Lcom/google/android/videos/store/PosterStore$NoStoredPosterException;

    iget-object v5, p0, Lcom/google/android/videos/store/PosterStore$2;->val$fileName:Ljava/lang/String;

    invoke-direct {v4, v5}, Lcom/google/android/videos/store/PosterStore$NoStoredPosterException;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v3, v4}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 153
    .end local v0    # "byteArray":Lcom/google/android/videos/utils/ByteArray;
    :goto_0
    return-void

    .line 135
    .restart local v0    # "byteArray":Lcom/google/android/videos/utils/ByteArray;
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/store/PosterStore$2;->this$0:Lcom/google/android/videos/store/PosterStore;

    # getter for: Lcom/google/android/videos/store/PosterStore;->cpuExecutor:Ljava/util/concurrent/Executor;
    invoke-static {v2}, Lcom/google/android/videos/store/PosterStore;->access$200(Lcom/google/android/videos/store/PosterStore;)Ljava/util/concurrent/Executor;

    move-result-object v2

    new-instance v3, Lcom/google/android/videos/store/PosterStore$2$1;

    invoke-direct {v3, p0, v0}, Lcom/google/android/videos/store/PosterStore$2$1;-><init>(Lcom/google/android/videos/store/PosterStore$2;Lcom/google/android/videos/utils/ByteArray;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 150
    .end local v0    # "byteArray":Lcom/google/android/videos/utils/ByteArray;
    :catch_0
    move-exception v1

    .line 151
    .local v1, "e":Ljava/io/IOException;
    iget-object v2, p0, Lcom/google/android/videos/store/PosterStore$2;->val$callback:Lcom/google/android/videos/async/Callback;

    iget-object v3, p0, Lcom/google/android/videos/store/PosterStore$2;->val$fileName:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method
