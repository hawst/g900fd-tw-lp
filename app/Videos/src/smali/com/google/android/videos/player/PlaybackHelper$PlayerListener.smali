.class public interface abstract Lcom/google/android/videos/player/PlaybackHelper$PlayerListener;
.super Ljava/lang/Object;
.source "PlaybackHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/PlaybackHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PlayerListener"
.end annotation


# virtual methods
.method public abstract onPlayerPause(III)V
.end method

.method public abstract onPlayerPlay()V
.end method

.method public abstract onPlayerScrubbingStart()V
.end method

.method public abstract onPlayerStop()V
.end method
