.class public Lcom/google/android/videos/remote/MediaRouteProviderDummy;
.super Ljava/lang/Object;
.source "MediaRouteProviderDummy.java"

# interfaces
.implements Lcom/google/android/videos/remote/MediaRouteProvider;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getMediaRouter()Landroid/support/v7/media/MediaRouter;
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSupportedRouteTypes()Landroid/support/v7/media/MediaRouteSelector;
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 29
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 17
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 21
    return-void
.end method

.method public setForceHidden(Z)V
    .locals 0
    .param p1, "forceHidden"    # Z

    .prologue
    .line 25
    return-void
.end method

.method public showPicker()V
    .locals 0

    .prologue
    .line 33
    return-void
.end method
