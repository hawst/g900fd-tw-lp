.class public interface abstract Lcom/google/android/videos/tagging/KnowledgeClient;
.super Ljava/lang/Object;
.source "KnowledgeClient.java"


# virtual methods
.method public abstract getFeedbackClient()Lcom/google/android/videos/tagging/FeedbackClient;
.end method

.method public abstract requestKnowledgeBundle(Lcom/google/android/videos/tagging/KnowledgeRequest;Lcom/google/android/videos/async/Callback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/tagging/KnowledgeRequest;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeRequest;",
            "Lcom/google/android/videos/tagging/KnowledgeBundle;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract requestPinnedKnowledgeBundle(Lcom/google/android/videos/tagging/KnowledgeRequest;ILcom/google/android/videos/async/Callback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/tagging/KnowledgeRequest;",
            "I",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeRequest;",
            "Lcom/google/android/videos/tagging/KnowledgeBundle;",
            ">;)V"
        }
    .end annotation
.end method
