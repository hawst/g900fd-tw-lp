.class public Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView$DetailsViewBinder;
.super Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView$Binder;
.source "EpisodeClusterItemView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DetailsViewBinder"
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;ZLcom/google/android/videos/logging/UiElementNode;)V
    .locals 0
    .param p4, "unpurchasedEpisodesHelper"    # Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;
    .param p5, "allowDownloads"    # Z
    .param p6, "clusterUiElementNode"    # Lcom/google/android/videos/logging/UiElementNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;",
            "Z",
            "Lcom/google/android/videos/logging/UiElementNode;",
            ")V"
        }
    .end annotation

    .prologue
    .line 364
    .local p1, "screenshotRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    .local p2, "showBannerRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    .local p3, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Landroid/graphics/Bitmap;>;"
    invoke-direct/range {p0 .. p6}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView$Binder;-><init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;ZLcom/google/android/videos/logging/UiElementNode;)V

    .line 366
    return-void
.end method


# virtual methods
.method protected bridge synthetic onBind(Lcom/google/android/videos/ui/playnext/ClusterItemView;Lcom/google/android/videos/adapter/DataSource;I)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/ui/playnext/ClusterItemView;
    .param p2, "x1"    # Lcom/google/android/videos/adapter/DataSource;
    .param p3, "x2"    # I

    .prologue
    .line 358
    check-cast p1, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;

    .end local p1    # "x0":Lcom/google/android/videos/ui/playnext/ClusterItemView;
    check-cast p2, Lcom/google/android/videos/adapter/EpisodesDataSource;

    .end local p2    # "x1":Lcom/google/android/videos/adapter/DataSource;
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView$DetailsViewBinder;->onBind(Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;Lcom/google/android/videos/adapter/EpisodesDataSource;I)V

    return-void
.end method

.method protected onBind(Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;Lcom/google/android/videos/adapter/EpisodesDataSource;I)V
    .locals 2
    .param p1, "view"    # Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;
    .param p2, "dataSource"    # Lcom/google/android/videos/adapter/EpisodesDataSource;
    .param p3, "index"    # I

    .prologue
    .line 370
    invoke-virtual {p2, p3}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getItem(I)Landroid/database/Cursor;

    move-result-object v0

    .line 371
    .local v0, "cursor":Landroid/database/Cursor;
    invoke-virtual {p2, v0}, Lcom/google/android/videos/adapter/EpisodesDataSource;->isPurchased(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 372
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView$DetailsViewBinder;->bindPurchasedEpisodeCard(Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;Lcom/google/android/videos/adapter/EpisodesDataSource;Landroid/database/Cursor;)V

    .line 376
    :goto_0
    return-void

    .line 374
    :cond_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView$DetailsViewBinder;->bindUnpurchasedEpisodeCard(Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;Lcom/google/android/videos/adapter/EpisodesDataSource;Landroid/database/Cursor;)V

    goto :goto_0
.end method
