.class public Lcom/google/android/videos/streams/MediaStream;
.super Ljava/lang/Object;
.source "MediaStream.java"


# instance fields
.field public final info:Lcom/google/android/videos/proto/StreamInfo;

.field public final isOffline:Z

.field public final itagInfo:Lcom/google/android/videos/streams/ItagInfo;

.field public final uri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Lcom/google/android/videos/streams/ItagInfo;Lcom/google/android/videos/proto/StreamInfo;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "itagInfo"    # Lcom/google/android/videos/streams/ItagInfo;
    .param p3, "streamInfo"    # Lcom/google/android/videos/proto/StreamInfo;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/videos/streams/MediaStream;->uri:Landroid/net/Uri;

    .line 22
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/streams/ItagInfo;

    iput-object v0, p0, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    .line 23
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/proto/StreamInfo;

    iput-object v0, p0, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    .line 24
    const-string v0, "file"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/videos/streams/MediaStream;->isOffline:Z

    .line 25
    return-void
.end method

.method public static gdataFormatToItag(I)I
    .locals 1
    .param p0, "gdataFormat"    # I

    .prologue
    .line 62
    packed-switch p0, :pswitch_data_0

    .line 96
    :pswitch_0
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 64
    :pswitch_1
    const/16 v0, 0x3d

    goto :goto_0

    .line 66
    :pswitch_2
    const/16 v0, 0x3f

    goto :goto_0

    .line 68
    :pswitch_3
    const/16 v0, 0x50

    goto :goto_0

    .line 70
    :pswitch_4
    const/16 v0, 0x51

    goto :goto_0

    .line 72
    :pswitch_5
    const/16 v0, 0x58

    goto :goto_0

    .line 74
    :pswitch_6
    const/16 v0, 0x3e

    goto :goto_0

    .line 76
    :pswitch_7
    const/16 v0, 0x72

    goto :goto_0

    .line 78
    :pswitch_8
    const/16 v0, 0x71

    goto :goto_0

    .line 80
    :pswitch_9
    const/16 v0, 0x77

    goto :goto_0

    .line 82
    :pswitch_a
    const/16 v0, 0x40

    goto :goto_0

    .line 84
    :pswitch_b
    const/16 v0, 0x9f

    goto :goto_0

    .line 86
    :pswitch_c
    const/16 v0, 0xba

    goto :goto_0

    .line 88
    :pswitch_d
    const/16 v0, 0xb4

    goto :goto_0

    .line 90
    :pswitch_e
    const/16 v0, 0xbe

    goto :goto_0

    .line 92
    :pswitch_f
    const/16 v0, 0xc1

    goto :goto_0

    .line 94
    :pswitch_10
    const/16 v0, 0xbc

    goto :goto_0

    .line 62
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_0
        :pswitch_f
        :pswitch_0
        :pswitch_10
    .end packed-switch
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 29
    if-ne p0, p1, :cond_0

    .line 30
    const/4 v1, 0x1

    .line 36
    :goto_0
    return v1

    .line 32
    :cond_0
    instance-of v1, p1, Lcom/google/android/videos/streams/MediaStream;

    if-nez v1, :cond_1

    .line 33
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 35
    check-cast v0, Lcom/google/android/videos/streams/MediaStream;

    .line 36
    .local v0, "other":Lcom/google/android/videos/streams/MediaStream;
    iget-object v1, p0, Lcom/google/android/videos/streams/MediaStream;->uri:Landroid/net/Uri;

    iget-object v2, v0, Lcom/google/android/videos/streams/MediaStream;->uri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/videos/streams/MediaStream;->uri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[uri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/streams/MediaStream;->uri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", itag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget v1, v1, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sizeInBytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-wide v2, v1, Lcom/google/android/videos/proto/StreamInfo;->sizeInBytes:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lastModifiedTimestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-wide v2, v1, Lcom/google/android/videos/proto/StreamInfo;->lastModifiedTimestamp:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", itagInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isOffline="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/videos/streams/MediaStream;->isOffline:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
