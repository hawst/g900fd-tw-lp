.class public Lcom/google/android/videos/player/logging/DerivedStatsHelper;
.super Ljava/lang/Object;
.source "DerivedStatsHelper.java"


# instance fields
.field private currentDerivedState:I

.field private currentItag:I

.field private derivedStats:Lcom/google/android/videos/player/logging/DerivedStats;

.field private final earlyPlaybackCutoffTime:I

.field private formatSelectionCount:I

.field private joiningTimeSet:Z

.field private lastItagTransitionTimeMs:I

.field private lastStateTransitionTimeMs:I

.field private playWhenReady:Z

.field private playbackState:I

.field private playerType:I

.field private seeking:Z

.field private videoDurationMs:I

.field private final videoStreams:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "earlyPlaybackCutoffTime"    # I

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput p1, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->earlyPlaybackCutoffTime:I

    .line 44
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->videoStreams:Landroid/util/SparseArray;

    .line 45
    return-void
.end method

.method private onStateChanged(I)V
    .locals 8
    .param p1, "sessionTimeMs"    # I

    .prologue
    const/4 v2, 0x1

    .line 82
    iget-boolean v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->playWhenReady:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->seeking:Z

    if-nez v0, :cond_2

    .line 83
    iget v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->playbackState:I

    packed-switch v0, :pswitch_data_0

    .line 101
    const/4 v6, 0x0

    .line 108
    .local v6, "derivedState":I
    :goto_0
    iget v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->currentDerivedState:I

    if-ne v6, v0, :cond_3

    .line 137
    :goto_1
    return-void

    .line 86
    .end local v6    # "derivedState":I
    :pswitch_0
    iget-boolean v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->joiningTimeSet:Z

    if-nez v0, :cond_0

    .line 87
    const/4 v6, 0x2

    .restart local v6    # "derivedState":I
    goto :goto_0

    .line 88
    .end local v6    # "derivedState":I
    :cond_0
    iget v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->currentDerivedState:I

    if-ne v0, v2, :cond_1

    iget v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->lastStateTransitionTimeMs:I

    sub-int v0, p1, v0

    const/16 v1, 0x7d0

    if-le v0, v1, :cond_1

    .line 90
    const/4 v6, 0x3

    .restart local v6    # "derivedState":I
    goto :goto_0

    .line 92
    .end local v6    # "derivedState":I
    :cond_1
    const/4 v6, 0x0

    .line 94
    .restart local v6    # "derivedState":I
    goto :goto_0

    .line 97
    .end local v6    # "derivedState":I
    :pswitch_1
    iput-boolean v2, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->joiningTimeSet:Z

    .line 98
    const/4 v6, 0x1

    .line 99
    .restart local v6    # "derivedState":I
    goto :goto_0

    .line 105
    .end local v6    # "derivedState":I
    :cond_2
    const/4 v6, 0x0

    .restart local v6    # "derivedState":I
    goto :goto_0

    .line 112
    :cond_3
    iget v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->lastStateTransitionTimeMs:I

    sub-int v7, p1, v0

    .line 113
    .local v7, "elapsedTimeMs":I
    iget v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->currentDerivedState:I

    packed-switch v0, :pswitch_data_1

    .line 135
    :goto_2
    iput v6, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->currentDerivedState:I

    .line 136
    iput p1, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->lastStateTransitionTimeMs:I

    goto :goto_1

    .line 115
    :pswitch_2
    iput-boolean v2, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->joiningTimeSet:Z

    .line 116
    iget-object v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->derivedStats:Lcom/google/android/videos/player/logging/DerivedStats;

    iput v7, v0, Lcom/google/android/videos/player/logging/DerivedStats;->joiningTimeMs:I

    goto :goto_2

    .line 119
    :pswitch_3
    iget v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->earlyPlaybackCutoffTime:I

    if-gt p1, v0, :cond_4

    .line 120
    iget-object v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->derivedStats:Lcom/google/android/videos/player/logging/DerivedStats;

    iget v1, v0, Lcom/google/android/videos/player/logging/DerivedStats;->earlyRebufferingCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/videos/player/logging/DerivedStats;->earlyRebufferingCount:I

    .line 121
    iget-object v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->derivedStats:Lcom/google/android/videos/player/logging/DerivedStats;

    iget v1, v0, Lcom/google/android/videos/player/logging/DerivedStats;->earlyRebufferingTimeMs:I

    add-int/2addr v1, v7

    iput v1, v0, Lcom/google/android/videos/player/logging/DerivedStats;->earlyRebufferingTimeMs:I

    .line 123
    :cond_4
    iget-object v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->derivedStats:Lcom/google/android/videos/player/logging/DerivedStats;

    iget v1, v0, Lcom/google/android/videos/player/logging/DerivedStats;->totalRebufferingCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/videos/player/logging/DerivedStats;->totalRebufferingCount:I

    .line 124
    iget-object v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->derivedStats:Lcom/google/android/videos/player/logging/DerivedStats;

    iget v1, v0, Lcom/google/android/videos/player/logging/DerivedStats;->totalRebufferingTimeMs:I

    add-int/2addr v1, v7

    iput v1, v0, Lcom/google/android/videos/player/logging/DerivedStats;->totalRebufferingTimeMs:I

    goto :goto_2

    .line 127
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->derivedStats:Lcom/google/android/videos/player/logging/DerivedStats;

    iget v1, v0, Lcom/google/android/videos/player/logging/DerivedStats;->totalPlayingTimeMs:I

    add-int/2addr v1, v7

    iput v1, v0, Lcom/google/android/videos/player/logging/DerivedStats;->totalPlayingTimeMs:I

    .line 128
    iget v2, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->currentItag:I

    iget v3, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->lastItagTransitionTimeMs:I

    iget v4, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->currentDerivedState:I

    iget v5, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->lastStateTransitionTimeMs:I

    move-object v0, p0

    move v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->updateAggregateFormatStats(IIIII)V

    goto :goto_2

    .line 83
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 113
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private updateAggregateFormatStats(IIIII)V
    .locals 10
    .param p1, "sessionTimeMs"    # I
    .param p2, "itag"    # I
    .param p3, "lastItagTransitionTimeMs"    # I
    .param p4, "derivedState"    # I
    .param p5, "lastStateTransitionTimeMs"    # I

    .prologue
    .line 187
    const/4 v2, 0x1

    if-eq p4, v2, :cond_0

    .line 203
    :goto_0
    return-void

    .line 191
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->videoStreams:Landroid/util/SparseArray;

    invoke-virtual {v2, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/streams/MediaStream;

    .line 192
    .local v1, "mediaStream":Lcom/google/android/videos/streams/MediaStream;
    if-nez v1, :cond_1

    .line 193
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Discarding segment for unknown itag: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 196
    :cond_1
    invoke-static {p5, p3}, Ljava/lang/Math;->max(II)I

    move-result v2

    sub-int v0, p1, v2

    .line 198
    .local v0, "formatPlayingTimeMs":I
    iget-object v2, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->derivedStats:Lcom/google/android/videos/player/logging/DerivedStats;

    iget-wide v4, v2, Lcom/google/android/videos/player/logging/DerivedStats;->aggregateFormatStatsPlayingTimeMs:J

    int-to-long v6, v0

    add-long/2addr v4, v6

    iput-wide v4, v2, Lcom/google/android/videos/player/logging/DerivedStats;->aggregateFormatStatsPlayingTimeMs:J

    .line 199
    iget-object v2, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->derivedStats:Lcom/google/android/videos/player/logging/DerivedStats;

    iget-wide v4, v2, Lcom/google/android/videos/player/logging/DerivedStats;->videoHeightTimesPlayingTimeMs:J

    iget-object v3, v1, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    iget v3, v3, Lcom/google/android/videos/streams/ItagInfo;->height:I

    int-to-long v6, v3

    int-to-long v8, v0

    mul-long/2addr v6, v8

    add-long/2addr v4, v6

    iput-wide v4, v2, Lcom/google/android/videos/player/logging/DerivedStats;->videoHeightTimesPlayingTimeMs:J

    .line 201
    iget-object v2, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->derivedStats:Lcom/google/android/videos/player/logging/DerivedStats;

    iget-wide v4, v2, Lcom/google/android/videos/player/logging/DerivedStats;->videoBandwidthTimesPlayingTimeMs:J

    iget-object v3, v1, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-wide v6, v3, Lcom/google/android/videos/proto/StreamInfo;->sizeInBytes:J

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    iget v3, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->videoDurationMs:I

    int-to-long v8, v3

    div-long/2addr v6, v8

    int-to-long v8, v0

    mul-long/2addr v6, v8

    add-long/2addr v4, v6

    iput-wide v4, v2, Lcom/google/android/videos/player/logging/DerivedStats;->videoBandwidthTimesPlayingTimeMs:J

    goto :goto_0
.end method


# virtual methods
.method public endSession(I)Lcom/google/android/videos/player/logging/DerivedStats;
    .locals 1
    .param p1, "sessionTimeMs"    # I

    .prologue
    .line 174
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->playWhenReady:Z

    .line 175
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->playbackState:I

    .line 176
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->onStateChanged(I)V

    .line 177
    iget-object v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->derivedStats:Lcom/google/android/videos/player/logging/DerivedStats;

    return-object v0
.end method

.method public onDroppedFrames(I)V
    .locals 2
    .param p1, "count"    # I

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->derivedStats:Lcom/google/android/videos/player/logging/DerivedStats;

    iget v1, v0, Lcom/google/android/videos/player/logging/DerivedStats;->droppedFrameCount:I

    add-int/2addr v1, p1

    iput v1, v0, Lcom/google/android/videos/player/logging/DerivedStats;->droppedFrameCount:I

    .line 149
    return-void
.end method

.method public onError()V
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->derivedStats:Lcom/google/android/videos/player/logging/DerivedStats;

    iget v1, v0, Lcom/google/android/videos/player/logging/DerivedStats;->errorCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/videos/player/logging/DerivedStats;->errorCount:I

    .line 141
    return-void
.end method

.method public onFailed()V
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->derivedStats:Lcom/google/android/videos/player/logging/DerivedStats;

    iget v1, v0, Lcom/google/android/videos/player/logging/DerivedStats;->failureCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/videos/player/logging/DerivedStats;->failureCount:I

    .line 145
    return-void
.end method

.method public onFormatEnabled(II)V
    .locals 7
    .param p1, "sessionTimeMs"    # I
    .param p2, "itag"    # I

    .prologue
    const/4 v6, 0x1

    .line 152
    iget v2, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->currentItag:I

    iget v3, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->lastItagTransitionTimeMs:I

    iget v4, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->currentDerivedState:I

    iget v5, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->lastStateTransitionTimeMs:I

    move-object v0, p0

    move v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->updateAggregateFormatStats(IIIII)V

    .line 154
    iget-object v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->derivedStats:Lcom/google/android/videos/player/logging/DerivedStats;

    iget-object v0, v0, Lcom/google/android/videos/player/logging/DerivedStats;->itagsUsed:Ljava/util/HashSet;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 155
    iput p2, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->currentItag:I

    .line 156
    iput p1, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->lastItagTransitionTimeMs:I

    .line 157
    iget v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->playerType:I

    if-eq v0, v6, :cond_0

    iget v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->playerType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 158
    :cond_0
    iget v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->formatSelectionCount:I

    if-nez v0, :cond_2

    .line 159
    iget-object v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->derivedStats:Lcom/google/android/videos/player/logging/DerivedStats;

    iput p2, v0, Lcom/google/android/videos/player/logging/DerivedStats;->firstItag:I

    .line 160
    iget v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->formatSelectionCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->formatSelectionCount:I

    .line 167
    :cond_1
    :goto_0
    return-void

    .line 161
    :cond_2
    iget v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->formatSelectionCount:I

    if-ne v0, v6, :cond_1

    .line 162
    iget-object v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->derivedStats:Lcom/google/android/videos/player/logging/DerivedStats;

    iput p2, v0, Lcom/google/android/videos/player/logging/DerivedStats;->secondItag:I

    .line 163
    iget-object v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->derivedStats:Lcom/google/android/videos/player/logging/DerivedStats;

    iput p1, v0, Lcom/google/android/videos/player/logging/DerivedStats;->secondItagSelectionTimeMs:I

    .line 164
    iget v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->formatSelectionCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->formatSelectionCount:I

    goto :goto_0
.end method

.method public onNetworkType(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->derivedStats:Lcom/google/android/videos/player/logging/DerivedStats;

    iget-object v0, v0, Lcom/google/android/videos/player/logging/DerivedStats;->connectionTypesUsed:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 171
    return-void
.end method

.method public onStateChanged(IZI)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "playWhenReady"    # Z
    .param p3, "playbackState"    # I

    .prologue
    .line 75
    iput-boolean p2, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->playWhenReady:Z

    .line 76
    iput p3, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->playbackState:I

    .line 77
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->onStateChanged(I)V

    .line 78
    return-void
.end method

.method public onUserSeekingChanged(IZ)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "seeking"    # Z

    .prologue
    .line 70
    iput-boolean p2, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->seeking:Z

    .line 71
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->onStateChanged(I)V

    .line 72
    return-void
.end method

.method public startSession(IILjava/util/List;)V
    .locals 4
    .param p1, "playerType"    # I
    .param p2, "videoDurationMs"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "mediaStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    const/4 v3, 0x0

    .line 48
    iput p1, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->playerType:I

    .line 49
    iput p2, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->videoDurationMs:I

    .line 50
    new-instance v2, Lcom/google/android/videos/player/logging/DerivedStats;

    invoke-direct {v2}, Lcom/google/android/videos/player/logging/DerivedStats;-><init>()V

    iput-object v2, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->derivedStats:Lcom/google/android/videos/player/logging/DerivedStats;

    .line 51
    iput v3, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->currentDerivedState:I

    .line 52
    iput v3, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->lastStateTransitionTimeMs:I

    .line 53
    const/4 v2, -0x1

    iput v2, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->currentItag:I

    .line 54
    iput v3, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->lastItagTransitionTimeMs:I

    .line 55
    iput-boolean v3, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->joiningTimeSet:Z

    .line 56
    iput-boolean v3, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->seeking:Z

    .line 57
    iput-boolean v3, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->playWhenReady:Z

    .line 58
    const/4 v2, 0x1

    iput v2, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->playbackState:I

    .line 59
    iput v3, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->formatSelectionCount:I

    .line 61
    iget-object v2, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->videoStreams:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->clear()V

    .line 62
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/streams/MediaStream;

    .line 63
    .local v1, "mediaStream":Lcom/google/android/videos/streams/MediaStream;
    iget-object v2, v1, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    iget v2, v2, Lcom/google/android/videos/streams/ItagInfo;->height:I

    if-lez v2, :cond_0

    .line 64
    iget-object v2, p0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->videoStreams:Landroid/util/SparseArray;

    iget-object v3, v1, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget v3, v3, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    invoke-virtual {v2, v3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 67
    .end local v1    # "mediaStream":Lcom/google/android/videos/streams/MediaStream;
    :cond_1
    return-void
.end method
