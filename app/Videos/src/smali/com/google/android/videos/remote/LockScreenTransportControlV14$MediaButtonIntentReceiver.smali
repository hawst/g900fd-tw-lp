.class public Lcom/google/android/videos/remote/LockScreenTransportControlV14$MediaButtonIntentReceiver;
.super Landroid/content/BroadcastReceiver;
.source "LockScreenTransportControlV14.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/remote/LockScreenTransportControlV14;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MediaButtonIntentReceiver"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 214
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 217
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Received intent: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 218
    const-string v2, "android.intent.action.MEDIA_BUTTON"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 219
    const-string v2, "android.intent.extra.KEY_EVENT"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/view/KeyEvent;

    .line 220
    .local v0, "event":Landroid/view/KeyEvent;
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_1

    .line 249
    .end local v0    # "event":Landroid/view/KeyEvent;
    :cond_0
    :goto_0
    return-void

    .line 225
    .restart local v0    # "event":Landroid/view/KeyEvent;
    :cond_1
    invoke-static {p1}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getRemoteTracker()Lcom/google/android/videos/remote/RemoteTracker;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/videos/remote/RemoteTracker;->getLockScreenTransportControl()Lcom/google/android/videos/remote/LockScreenTransportControlV14;

    move-result-object v1

    .line 228
    .local v1, "instance":Lcom/google/android/videos/remote/LockScreenTransportControlV14;
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 246
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unrecognized event: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 230
    :sswitch_0
    # getter for: Lcom/google/android/videos/remote/LockScreenTransportControlV14;->isPlaying:Z
    invoke-static {v1}, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->access$100(Lcom/google/android/videos/remote/LockScreenTransportControlV14;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 231
    invoke-virtual {v1}, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->onPause()V

    goto :goto_0

    .line 233
    :cond_2
    invoke-virtual {v1}, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->onPlay()V

    goto :goto_0

    .line 237
    :sswitch_1
    invoke-virtual {v1}, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->onPlay()V

    goto :goto_0

    .line 240
    :sswitch_2
    invoke-virtual {v1}, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->onPause()V

    goto :goto_0

    .line 243
    :sswitch_3
    invoke-virtual {v1}, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->onDisconnect()V

    goto :goto_0

    .line 228
    nop

    :sswitch_data_0
    .sparse-switch
        0x55 -> :sswitch_0
        0x56 -> :sswitch_3
        0x7e -> :sswitch_1
        0x7f -> :sswitch_2
    .end sparse-switch
.end method
