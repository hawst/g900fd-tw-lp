.class final Lcom/google/android/videos/tagging/SongCards;
.super Lcom/google/android/videos/tagging/Cards;
.source "SongCards.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/tagging/SongCards$SongSecondaryActionButtonHelper;,
        Lcom/google/android/videos/tagging/SongCards$GoToStoreOnClickListener;
    }
.end annotation


# direct methods
.method public static create(Lcom/google/android/videos/tagging/KnowledgeEntity$Song;Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;Lcom/google/android/videos/async/Requester;Landroid/app/Activity;Lcom/google/android/videos/store/StoreStatusMonitor;Ljava/lang/String;Lcom/google/android/videos/logging/EventLogger;)Landroid/view/View;
    .locals 17
    .param p0, "song"    # Lcom/google/android/videos/tagging/KnowledgeEntity$Song;
    .param p1, "cardInflater"    # Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;
    .param p3, "activity"    # Landroid/app/Activity;
    .param p4, "storeStatusMonitor"    # Lcom/google/android/videos/store/StoreStatusMonitor;
    .param p5, "account"    # Ljava/lang/String;
    .param p6, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Song;",
            "Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/app/Activity;",
            "Lcom/google/android/videos/store/StoreStatusMonitor;",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/logging/EventLogger;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 34
    .local p2, "imageRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;>;"
    const v2, 0x7f0400c1

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;->inflate(I)Landroid/view/View;

    move-result-object v15

    .line 35
    .local v15, "songCard":Landroid/view/View;
    invoke-virtual {v15}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    .line 37
    .local v13, "resources":Landroid/content/res/Resources;
    const v2, 0x7f0f0204

    invoke-virtual {v15, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    .line 38
    .local v10, "albumArtView":Landroid/widget/ImageView;
    const/4 v2, 0x4

    invoke-virtual {v10, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 39
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;->image:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    if-eqz v2, :cond_0

    .line 40
    const v2, 0x7f0b01e1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;->name:Ljava/lang/String;

    aput-object v7, v3, v5

    invoke-virtual {v13, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 43
    :cond_0
    const v2, 0x7f0e01a3

    invoke-virtual {v13, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v10, v0, v1, v2}, Lcom/google/android/videos/tagging/SongCards;->requestSongImage(Landroid/widget/ImageView;Lcom/google/android/videos/tagging/KnowledgeEntity$Song;Lcom/google/android/videos/async/Requester;I)V

    .line 46
    const v2, 0x7f0f0205

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;->name:Ljava/lang/String;

    invoke-static {v15, v2, v3}, Lcom/google/android/videos/tagging/SongCards;->showViewAndSetText(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    .line 48
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;->performer:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v14, 0x0

    .line 52
    .local v14, "songArtistView":Landroid/view/View;
    :goto_0
    const v2, 0x7f0f0203

    invoke-virtual {v15, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    .line 53
    .local v16, "songCardBody":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;->googlePlayUrl:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v11, 0x0

    .line 56
    .local v11, "goToStoreOnClickListener":Lcom/google/android/videos/tagging/SongCards$GoToStoreOnClickListener;
    :goto_1
    if-eqz v11, :cond_4

    .line 57
    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;->googlePlayUrl:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/videos/tagging/SongCards;->getAlbumId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 69
    .local v6, "albumId":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 70
    const v2, 0x7f0f0207

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    .line 71
    .local v4, "button":Landroid/widget/Button;
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 72
    new-instance v2, Lcom/google/android/videos/tagging/SongCards$SongSecondaryActionButtonHelper;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;->name:Ljava/lang/String;

    move-object/from16 v3, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    invoke-direct/range {v2 .. v9}, Lcom/google/android/videos/tagging/SongCards$SongSecondaryActionButtonHelper;-><init>(Landroid/app/Activity;Landroid/widget/Button;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/store/StoreStatusMonitor;Ljava/lang/String;Lcom/google/android/videos/logging/EventLogger;)V

    invoke-virtual {v2}, Lcom/google/android/videos/tagging/SongCards$SongSecondaryActionButtonHelper;->setup()V

    .line 75
    if-eqz v14, :cond_1

    .line 76
    invoke-interface/range {p1 .. p1}, Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;->getCardWidth()I

    move-result v2

    move-object/from16 v0, v16

    invoke-static {v0, v2}, Lcom/google/android/videos/tagging/SongCards;->layout(Landroid/view/View;I)V

    .line 77
    invoke-static {v14}, Lcom/google/android/videos/tagging/SongCards;->getBottom(Landroid/view/View;)I

    move-result v2

    invoke-virtual {v4}, Landroid/widget/Button;->getTop()I

    move-result v3

    if-le v2, v3, :cond_1

    .line 78
    const/16 v2, 0x8

    invoke-virtual {v14, v2}, Landroid/view/View;->setVisibility(I)V

    .line 83
    .end local v4    # "button":Landroid/widget/Button;
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/google/android/videos/tagging/CardTag;->forSong(Lcom/google/android/videos/tagging/KnowledgeEntity$Song;)Lcom/google/android/videos/tagging/CardTag;

    move-result-object v2

    invoke-virtual {v15, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 84
    return-object v15

    .line 48
    .end local v6    # "albumId":Ljava/lang/String;
    .end local v11    # "goToStoreOnClickListener":Lcom/google/android/videos/tagging/SongCards$GoToStoreOnClickListener;
    .end local v14    # "songArtistView":Landroid/view/View;
    .end local v16    # "songCardBody":Landroid/view/View;
    :cond_2
    const v2, 0x7f0f0206

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;->performer:Ljava/lang/String;

    invoke-static {v15, v2, v3}, Lcom/google/android/videos/tagging/SongCards;->showViewAndSetText(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    move-result-object v14

    goto :goto_0

    .line 53
    .restart local v14    # "songArtistView":Landroid/view/View;
    .restart local v16    # "songCardBody":Landroid/view/View;
    :cond_3
    new-instance v11, Lcom/google/android/videos/tagging/SongCards$GoToStoreOnClickListener;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;->googlePlayUrl:Ljava/lang/String;

    move-object/from16 v0, p3

    move-object/from16 v1, p5

    invoke-direct {v11, v0, v2, v1}, Lcom/google/android/videos/tagging/SongCards$GoToStoreOnClickListener;-><init>(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 59
    .restart local v11    # "goToStoreOnClickListener":Lcom/google/android/videos/tagging/SongCards$GoToStoreOnClickListener;
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;->name:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/videos/tagging/SongCards;->toQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 60
    .local v12, "query":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;->performer:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 61
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;->performer:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/videos/tagging/SongCards;->toQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 63
    :cond_5
    new-instance v2, Lcom/google/android/videos/tagging/Cards$WebSearchOnClickListener;

    const/4 v3, 0x3

    move-object/from16 v0, p3

    move-object/from16 v1, p6

    invoke-direct {v2, v0, v12, v1, v3}, Lcom/google/android/videos/tagging/Cards$WebSearchOnClickListener;-><init>(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/videos/logging/EventLogger;I)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2
.end method

.method private static getAlbumId(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "storeUrl"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 88
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    move-object v0, v4

    .line 99
    :cond_0
    :goto_0
    return-object v0

    .line 91
    :cond_1
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 92
    .local v3, "uri":Landroid/net/Uri;
    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 93
    .local v1, "path":Ljava/lang/String;
    const-string v5, "id"

    invoke-virtual {v3, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 94
    .local v0, "id":Ljava/lang/String;
    const-string v5, "tid"

    invoke-virtual {v3, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 95
    .local v2, "tid":Ljava/lang/String;
    const-string v5, "/store/music/album"

    invoke-static {v1, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "song-"

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    :cond_2
    move-object v0, v4

    .line 99
    goto :goto_0
.end method
