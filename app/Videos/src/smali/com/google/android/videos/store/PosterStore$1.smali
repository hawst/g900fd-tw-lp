.class Lcom/google/android/videos/store/PosterStore$1;
.super Ljava/lang/Object;
.source "PosterStore.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/store/PosterStore;->getBitmapBytes(Ljava/lang/String;Lcom/google/android/videos/store/FileStore;Lcom/google/android/videos/async/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/store/PosterStore;

.field final synthetic val$callback:Lcom/google/android/videos/async/Callback;

.field final synthetic val$fileName:Ljava/lang/String;

.field final synthetic val$fileStore:Lcom/google/android/videos/store/FileStore;


# direct methods
.method constructor <init>(Lcom/google/android/videos/store/PosterStore;Lcom/google/android/videos/store/FileStore;Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/google/android/videos/store/PosterStore$1;->this$0:Lcom/google/android/videos/store/PosterStore;

    iput-object p2, p0, Lcom/google/android/videos/store/PosterStore$1;->val$fileStore:Lcom/google/android/videos/store/FileStore;

    iput-object p3, p0, Lcom/google/android/videos/store/PosterStore$1;->val$fileName:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/videos/store/PosterStore$1;->val$callback:Lcom/google/android/videos/async/Callback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 95
    :try_start_0
    iget-object v2, p0, Lcom/google/android/videos/store/PosterStore$1;->val$fileStore:Lcom/google/android/videos/store/FileStore;

    iget-object v3, p0, Lcom/google/android/videos/store/PosterStore$1;->val$fileName:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/videos/store/PosterStore$1;->this$0:Lcom/google/android/videos/store/PosterStore;

    # getter for: Lcom/google/android/videos/store/PosterStore;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;
    invoke-static {v4}, Lcom/google/android/videos/store/PosterStore;->access$000(Lcom/google/android/videos/store/PosterStore;)Lcom/google/android/videos/utils/ByteArrayPool;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/videos/store/FileStore;->getBytes(Ljava/lang/String;Lcom/google/android/videos/utils/ByteArrayPool;)Lcom/google/android/videos/utils/ByteArray;

    move-result-object v1

    .line 96
    .local v1, "storedBytes":Lcom/google/android/videos/utils/ByteArray;
    if-nez v1, :cond_0

    .line 97
    iget-object v2, p0, Lcom/google/android/videos/store/PosterStore$1;->val$callback:Lcom/google/android/videos/async/Callback;

    iget-object v3, p0, Lcom/google/android/videos/store/PosterStore$1;->val$fileName:Ljava/lang/String;

    new-instance v4, Lcom/google/android/videos/store/PosterStore$NoStoredPosterException;

    iget-object v5, p0, Lcom/google/android/videos/store/PosterStore$1;->val$fileName:Ljava/lang/String;

    invoke-direct {v4, v5}, Lcom/google/android/videos/store/PosterStore$NoStoredPosterException;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v3, v4}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 104
    .end local v1    # "storedBytes":Lcom/google/android/videos/utils/ByteArray;
    :goto_0
    return-void

    .line 100
    .restart local v1    # "storedBytes":Lcom/google/android/videos/utils/ByteArray;
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/store/PosterStore$1;->val$callback:Lcom/google/android/videos/async/Callback;

    iget-object v3, p0, Lcom/google/android/videos/store/PosterStore$1;->val$fileName:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 101
    .end local v1    # "storedBytes":Lcom/google/android/videos/utils/ByteArray;
    :catch_0
    move-exception v0

    .line 102
    .local v0, "e":Ljava/io/IOException;
    iget-object v2, p0, Lcom/google/android/videos/store/PosterStore$1;->val$callback:Lcom/google/android/videos/async/Callback;

    iget-object v3, p0, Lcom/google/android/videos/store/PosterStore$1;->val$fileName:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method
