.class Lcom/google/android/videos/pano/datasource/MyWishlistDataSource$1;
.super Ljava/lang/Object;
.source "MyWishlistDataSource.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/store/WishlistStore$WishlistRequest;",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;


# direct methods
.method constructor <init>(Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource$1;->this$0:Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/store/WishlistStore$WishlistRequest;Ljava/lang/Exception;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/store/WishlistStore$WishlistRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource$1;->this$0:Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;

    iget-object v0, v0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource$1;->this$0:Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;

    # getter for: Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->updateArrayRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->access$200(Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 73
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 38
    check-cast p1, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource$1;->onError(Lcom/google/android/videos/store/WishlistStore$WishlistRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/store/WishlistStore$WishlistRequest;Landroid/database/Cursor;)V
    .locals 8
    .param p1, "request"    # Lcom/google/android/videos/store/WishlistStore$WishlistRequest;
    .param p2, "response"    # Landroid/database/Cursor;

    .prologue
    .line 43
    :try_start_0
    new-instance v4, Lcom/google/android/videos/api/AssetsRequest$Builder;

    invoke-direct {v4}, Lcom/google/android/videos/api/AssetsRequest$Builder;-><init>()V

    iget-object v5, p0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource$1;->this$0:Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;

    invoke-virtual {v5}, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->getAccount()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/videos/api/AssetsRequest$Builder;->account(Ljava/lang/String;)Lcom/google/android/videos/api/AssetsRequest$Builder;

    move-result-object v4

    const/16 v5, 0x13

    invoke-virtual {v4, v5}, Lcom/google/android/videos/api/AssetsRequest$Builder;->addFlags(I)Lcom/google/android/videos/api/AssetsRequest$Builder;

    move-result-object v1

    .line 48
    .local v1, "assetRequestBuilder":Lcom/google/android/videos/api/AssetsRequest$Builder;
    :cond_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 49
    const/4 v4, 0x1

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 50
    .local v2, "itemId":Ljava/lang/String;
    const/4 v4, 0x0

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 51
    .local v3, "itemType":I
    invoke-static {v3, v2}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromAssetTypeAndId(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 52
    .local v0, "assetId":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v1, v0}, Lcom/google/android/videos/api/AssetsRequest$Builder;->maybeAddId(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 57
    .end local v0    # "assetId":Ljava/lang/String;
    .end local v2    # "itemId":Ljava/lang/String;
    .end local v3    # "itemType":I
    :cond_1
    invoke-virtual {v1}, Lcom/google/android/videos/api/AssetsRequest$Builder;->getIdCount()I

    move-result v4

    if-eqz v4, :cond_3

    .line 58
    iget-object v4, p0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource$1;->this$0:Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;

    # getter for: Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->assetRequester:Lcom/google/android/videos/async/Requester;
    invoke-static {v4}, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->access$100(Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;)Lcom/google/android/videos/async/Requester;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/videos/api/AssetsRequest$Builder;->build()Lcom/google/android/videos/api/AssetsRequest;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource$1;->this$0:Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;

    iget-object v6, v6, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->handler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource$1;->this$0:Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;

    # getter for: Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->assetResponseCallback:Lcom/google/android/videos/async/Callback;
    invoke-static {v7}, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->access$000(Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;)Lcom/google/android/videos/async/Callback;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/videos/async/HandlerCallback;->create(Landroid/os/Handler;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/HandlerCallback;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    :goto_0
    if-eqz p2, :cond_2

    .line 65
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    .line 68
    :cond_2
    return-void

    .line 61
    :cond_3
    :try_start_1
    iget-object v4, p0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource$1;->this$0:Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;

    iget-object v4, v4, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->handler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource$1;->this$0:Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;

    # getter for: Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->updateArrayRunnable:Ljava/lang/Runnable;
    invoke-static {v5}, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->access$200(Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;)Ljava/lang/Runnable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 64
    .end local v1    # "assetRequestBuilder":Lcom/google/android/videos/api/AssetsRequest$Builder;
    :catchall_0
    move-exception v4

    if-eqz p2, :cond_4

    .line 65
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v4
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 38
    check-cast p1, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource$1;->onResponse(Lcom/google/android/videos/store/WishlistStore$WishlistRequest;Landroid/database/Cursor;)V

    return-void
.end method
