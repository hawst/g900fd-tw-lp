.class public Lcom/google/android/videos/ContentNotificationService;
.super Landroid/app/IntentService;
.source "ContentNotificationService.java"


# instance fields
.field private contentNotificationManager:Lcom/google/android/videos/ContentNotificationManager;

.field private preferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/google/android/videos/ContentNotificationService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 33
    return-void
.end method

.method private static buildShowUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "showId"    # Ljava/lang/String;
    .param p2, "action"    # Ljava/lang/String;

    .prologue
    .line 92
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "new-episodes"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getPendingIntentForAction(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "videoId"    # Ljava/lang/String;
    .param p4, "seasonId"    # Ljava/lang/String;
    .param p5, "showId"    # Ljava/lang/String;
    .param p6, "episodeIds"    # [Ljava/lang/String;

    .prologue
    .line 78
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/videos/ContentNotificationService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "authAccount"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "video_id"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "season_id"

    invoke-virtual {v1, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "show_id"

    invoke-virtual {v1, v2, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "video_ids"

    invoke-virtual {v1, v2, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 86
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {p2, p5, p1}, Lcom/google/android/videos/ContentNotificationService;->buildShowUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 88
    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public onCreate()V
    .locals 2

    .prologue
    .line 37
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 38
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v0

    .line 39
    .local v0, "videosGlobals":Lcom/google/android/videos/VideosGlobals;
    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getContentNotificationManager()Lcom/google/android/videos/ContentNotificationManager;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/ContentNotificationService;->contentNotificationManager:Lcom/google/android/videos/ContentNotificationManager;

    .line 40
    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/ContentNotificationService;->preferences:Landroid/content/SharedPreferences;

    .line 41
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 12
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/high16 v5, 0x10000000

    .line 45
    if-nez p1, :cond_0

    .line 73
    :goto_0
    return-void

    .line 48
    :cond_0
    const-string v0, "authAccount"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 49
    .local v1, "account":Ljava/lang/String;
    const-string v0, "video_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 50
    .local v4, "videoId":Ljava/lang/String;
    const-string v0, "season_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 51
    .local v3, "seasonId":Ljava/lang/String;
    const-string v0, "show_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 52
    .local v2, "showId":Ljava/lang/String;
    const-string v0, "video_ids"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 53
    .local v8, "episodeIds":[Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    .line 55
    .local v6, "action":Ljava/lang/String;
    const-string v0, "com.google.android.videos.DETAILS"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, p0

    .line 56
    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/activity/LauncherActivity;->createEpisodeDeepLinkingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ContentNotificationService;->startActivity(Landroid/content/Intent;)V

    .line 72
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/videos/ContentNotificationService;->contentNotificationManager:Lcom/google/android/videos/ContentNotificationManager;

    const/4 v5, 0x1

    invoke-virtual {v0, v1, v2, v8, v5}, Lcom/google/android/videos/ContentNotificationManager;->dismissContentNotification(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Z)V

    goto :goto_0

    .line 58
    :cond_2
    const-string v0, "com.google.android.videos.PLAY"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, p0

    .line 59
    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/activity/LauncherActivity;->createWatchEpisodeDeepLinkingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ContentNotificationService;->startActivity(Landroid/content/Intent;)V

    .line 62
    const/4 v0, 0x0

    :try_start_0
    new-instance v5, Landroid/content/Intent;

    const-string v11, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-direct {v5, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v11, 0x40000000    # 2.0f

    invoke-static {p0, v0, v5, v11}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/PendingIntent;->send()V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 64
    :catch_0
    move-exception v7

    .line 65
    .local v7, "e":Landroid/app/PendingIntent$CanceledException;
    const-string v0, "Error when broadcasting close system dialogs intent"

    invoke-static {v0, v7}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 67
    .end local v7    # "e":Landroid/app/PendingIntent$CanceledException;
    :cond_3
    const-string v0, "com.google.android.videos.DOWNLOAD"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 68
    iget-object v0, p0, Lcom/google/android/videos/ContentNotificationService;->preferences:Landroid/content/SharedPreferences;

    invoke-static {p0, v0}, Lcom/google/android/videos/utils/SettingsUtil;->getDownloadQualityItag(Landroid/content/Context;Landroid/content/SharedPreferences;)I

    move-result v9

    .line 69
    .local v9, "quality":I
    iget-object v0, p0, Lcom/google/android/videos/ContentNotificationService;->preferences:Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/google/android/videos/utils/SettingsUtil;->getPreferredStorageIndex(Landroid/content/SharedPreferences;)I

    move-result v10

    .line 70
    .local v10, "storage":I
    invoke-static {p0, v1, v4, v9, v10}, Lcom/google/android/videos/pinning/PinService;->requestPin(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_1
.end method
