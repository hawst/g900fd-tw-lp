.class final Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackSubmitConverter;
.super Lcom/google/android/videos/converter/HttpResponseConverter;
.source "DefaultFeedbackClient.java"

# interfaces
.implements Lcom/google/android/videos/converter/RequestConverter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/DefaultFeedbackClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "FeedbackSubmitConverter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/converter/HttpResponseConverter",
        "<",
        "Ljava/lang/Void;",
        ">;",
        "Lcom/google/android/videos/converter/RequestConverter",
        "<",
        "Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        ">;"
    }
.end annotation


# instance fields
.field private final submitUrlTemplate:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "submitUrlTemplate"    # Ljava/lang/String;

    .prologue
    .line 279
    invoke-direct {p0}, Lcom/google/android/videos/converter/HttpResponseConverter;-><init>()V

    .line 280
    iput-object p1, p0, Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackSubmitConverter;->submitUrlTemplate:Ljava/lang/String;

    .line 281
    return-void
.end method


# virtual methods
.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 274
    check-cast p1, Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackSubmitConverter;->convertRequest(Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public convertRequest(Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 7
    .param p1, "request"    # Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 285
    iget-object v4, p0, Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackSubmitConverter;->submitUrlTemplate:Ljava/lang/String;

    const-string v5, "{$GF_TOKEN}"

    iget-object v6, p1, Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;->token:Ljava/lang/String;

    invoke-static {v6}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 286
    .local v3, "submitUrl":Ljava/lang/String;
    new-instance v2, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v2, v3}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 287
    .local v2, "httpPost":Lorg/apache/http/client/methods/HttpPost;
    iget-object v4, p1, Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;->report:Lcom/google/android/videos/tagging/FeedbackClient$FeedbackReport;

    invoke-interface {v4}, Lcom/google/android/videos/tagging/FeedbackClient$FeedbackReport;->buildMessage()Lcom/google/protobuf/nano/MessageNano;

    move-result-object v4

    invoke-static {v4}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v0

    .line 288
    .local v0, "body":[B
    new-instance v1, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-direct {v1, v0}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    .line 289
    .local v1, "entity":Lorg/apache/http/entity/ByteArrayEntity;
    const-string v4, "application/x-protobuf"

    invoke-virtual {v1, v4}, Lorg/apache/http/entity/ByteArrayEntity;->setContentType(Ljava/lang/String;)V

    .line 290
    invoke-virtual {v2, v1}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 291
    return-object v2
.end method

.method protected bridge synthetic convertResponseEntity(Lorg/apache/http/HttpEntity;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 274
    invoke-virtual {p0, p1}, Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackSubmitConverter;->convertResponseEntity(Lorg/apache/http/HttpEntity;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected convertResponseEntity(Lorg/apache/http/HttpEntity;)Ljava/lang/Void;
    .locals 3
    .param p1, "entity"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 296
    invoke-static {p1}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v0

    .line 297
    .local v0, "responseBody":Ljava/lang/String;
    const-string v1, "\"success\":true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 298
    const/4 v1, 0x0

    return-object v1

    .line 300
    :cond_0
    new-instance v1, Lcom/google/android/videos/converter/ConverterException;

    const-string v2, "Couldn\'t find success signal in feedback submission response"

    invoke-direct {v1, v2}, Lcom/google/android/videos/converter/ConverterException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
