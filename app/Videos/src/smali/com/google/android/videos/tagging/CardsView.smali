.class public Lcom/google/android/videos/tagging/CardsView;
.super Landroid/widget/ScrollView;
.source "CardsView.java"

# interfaces
.implements Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;
.implements Lcom/google/android/videos/tagging/SuggestionGridLayout$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/tagging/CardsView$Listener;
    }
.end annotation


# static fields
.field private static final PULSE_ANIMATION_INTERPOLATOR:Landroid/animation/TimeInterpolator;


# instance fields
.field private final cardBounds:Landroid/graphics/Rect;

.field private final cardHolder:Lcom/google/android/videos/tagging/SuggestionGridLayout;

.field private cardPendingPulseAnimation:Landroid/view/View;

.field private final cards:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private cardsShown:I

.field private final inflater:Landroid/view/LayoutInflater;

.field private listener:Lcom/google/android/videos/tagging/CardsView$Listener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/videos/tagging/CardsView;->PULSE_ANIMATION_INTERPOLATOR:Landroid/animation/TimeInterpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/videos/tagging/CardsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/videos/tagging/CardsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 57
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 58
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/tagging/CardsView;->inflater:Landroid/view/LayoutInflater;

    .line 59
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsView;->inflater:Landroid/view/LayoutInflater;

    const v1, 0x7f040021

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/tagging/SuggestionGridLayout;

    iput-object v0, p0, Lcom/google/android/videos/tagging/CardsView;->cardHolder:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    .line 61
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsView;->cardHolder:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->setOnDismissListener(Lcom/google/android/videos/tagging/SuggestionGridLayout$OnDismissListener;)V

    .line 62
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsView;->cardHolder:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/CardsView;->addView(Landroid/view/View;)V

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/tagging/CardsView;->cards:Ljava/util/List;

    .line 64
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/tagging/CardsView;->cardBounds:Landroid/graphics/Rect;

    .line 65
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/tagging/CardsView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/tagging/CardsView;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/videos/tagging/CardsView;->maybePulseCard()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/videos/tagging/CardsView;)Lcom/google/android/videos/tagging/SuggestionGridLayout;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/CardsView;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsView;->cardHolder:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    return-object v0
.end method

.method private insertAt(Ljava/util/List;I)V
    .locals 5
    .param p2, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 138
    .local p1, "cards":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 150
    :cond_0
    return-void

    .line 141
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 142
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 143
    .local v0, "card":Landroid/view/View;
    add-int v2, p2, v1

    .line 144
    .local v2, "insertLocation":I
    iget-object v3, p0, Lcom/google/android/videos/tagging/CardsView;->cards:Ljava/util/List;

    invoke-interface {v3, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 145
    iget-object v3, p0, Lcom/google/android/videos/tagging/CardsView;->cardHolder:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v3, v0, v2}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->addView(Landroid/view/View;I)V

    .line 146
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-eq v3, v4, :cond_2

    .line 147
    iget v3, p0, Lcom/google/android/videos/tagging/CardsView;->cardsShown:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/videos/tagging/CardsView;->cardsShown:I

    .line 141
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private maybePulseCard()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x2

    .line 321
    iget-object v1, p0, Lcom/google/android/videos/tagging/CardsView;->cardPendingPulseAnimation:Landroid/view/View;

    .line 322
    .local v1, "card":Landroid/view/View;
    if-nez v1, :cond_1

    .line 343
    :cond_0
    :goto_0
    return-void

    .line 326
    :cond_1
    iget-object v3, p0, Lcom/google/android/videos/tagging/CardsView;->cardBounds:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v4

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v5

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v6

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v7

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 328
    iget-object v3, p0, Lcom/google/android/videos/tagging/CardsView;->cardBounds:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/google/android/videos/tagging/CardsView;->cardHolder:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v4}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getLeft()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/videos/tagging/CardsView;->getScrollX()I

    move-result v5

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/google/android/videos/tagging/CardsView;->cardHolder:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v5}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getTop()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/videos/tagging/CardsView;->getScrollY()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->offset(II)V

    .line 330
    iget-object v3, p0, Lcom/google/android/videos/tagging/CardsView;->cardBounds:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/videos/tagging/CardsView;->getTopInset()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/videos/tagging/CardsView;->getWidth()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/videos/tagging/CardsView;->getHeight()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/videos/tagging/CardsView;->getTranslationY()F

    move-result v7

    float-to-int v7, v7

    sub-int/2addr v6, v7

    invoke-virtual {p0}, Lcom/google/android/videos/tagging/CardsView;->getPaddingBottom()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-virtual {v3, v9, v4, v5, v6}, Landroid/graphics/Rect;->intersects(IIII)Z

    move-result v2

    .line 332
    .local v2, "cardVisible":Z
    if-eqz v2, :cond_0

    .line 333
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/videos/tagging/CardsView;->cardPendingPulseAnimation:Landroid/view/View;

    .line 334
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 335
    .local v0, "animatorSet":Landroid/animation/AnimatorSet;
    new-array v3, v8, [Landroid/animation/Animator;

    const-string v4, "scaleX"

    new-array v5, v8, [F

    fill-array-data v5, :array_0

    invoke-static {v1, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    aput-object v4, v3, v9

    const/4 v4, 0x1

    const-string v5, "scaleY"

    new-array v6, v8, [F

    fill-array-data v6, :array_1

    invoke-static {v1, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 338
    const-wide/16 v4, 0x12c

    invoke-virtual {v0, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 339
    sget-object v3, Lcom/google/android/videos/tagging/CardsView;->PULSE_ANIMATION_INTERPOLATOR:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 340
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 341
    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    goto/16 :goto_0

    .line 335
    :array_0
    .array-data 4
        0x3f866666    # 1.05f
        0x3f800000    # 1.0f
    .end array-data

    :array_1
    .array-data 4
        0x3f866666    # 1.05f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private onCardHiddenOrRemoved(Landroid/view/View;)V
    .locals 1
    .param p1, "card"    # Landroid/view/View;

    .prologue
    .line 183
    iget v0, p0, Lcom/google/android/videos/tagging/CardsView;->cardsShown:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/videos/tagging/CardsView;->cardsShown:I

    .line 184
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsView;->listener:Lcom/google/android/videos/tagging/CardsView$Listener;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsView;->listener:Lcom/google/android/videos/tagging/CardsView$Listener;

    invoke-interface {v0, p1}, Lcom/google/android/videos/tagging/CardsView$Listener;->onCardDismissed(Landroid/view/View;)V

    .line 186
    iget v0, p0, Lcom/google/android/videos/tagging/CardsView;->cardsShown:I

    if-nez v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsView;->listener:Lcom/google/android/videos/tagging/CardsView$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/tagging/CardsView$Listener;->onAllCardsDismissed()V

    .line 190
    :cond_0
    return-void
.end method

.method private scrollToInternal(I)V
    .locals 10
    .param p1, "index"    # I

    .prologue
    const/16 v9, 0x8

    const/4 v5, 0x0

    .line 283
    const/4 v4, 0x0

    .line 284
    .local v4, "toScrollTo":I
    add-int/lit8 v2, p1, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_0

    .line 285
    iget-object v6, p0, Lcom/google/android/videos/tagging/CardsView;->cards:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 286
    .local v0, "card":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v6

    if-eq v6, v9, :cond_3

    .line 287
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v6

    iget-object v7, p0, Lcom/google/android/videos/tagging/CardsView;->cardHolder:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v7}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getPaddingTop()I

    move-result v7

    sub-int v4, v6, v7

    .line 294
    .end local v0    # "card":Landroid/view/View;
    :cond_0
    iget-object v6, p0, Lcom/google/android/videos/tagging/CardsView;->cardHolder:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v6}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getPaddingTop()I

    move-result v6

    add-int/2addr v6, v4

    invoke-virtual {p0}, Lcom/google/android/videos/tagging/CardsView;->getScrollY()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-virtual {p0}, Lcom/google/android/videos/tagging/CardsView;->getHeight()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/videos/tagging/CardsView;->getPaddingBottom()I

    move-result v8

    sub-int/2addr v7, v8

    if-lt v6, v7, :cond_4

    const/4 v3, 0x1

    .line 296
    .local v3, "skipAnimations":Z
    :goto_1
    if-eqz v3, :cond_1

    .line 297
    iget-object v6, p0, Lcom/google/android/videos/tagging/CardsView;->cardHolder:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v6, v5}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->setLayoutTransitionsEnabled(Z)V

    .line 299
    :cond_1
    iget-object v6, p0, Lcom/google/android/videos/tagging/CardsView;->cards:Ljava/util/List;

    invoke-interface {v6, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 300
    .restart local v0    # "card":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v6

    if-ne v6, v9, :cond_2

    .line 301
    iget v6, p0, Lcom/google/android/videos/tagging/CardsView;->cardsShown:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/android/videos/tagging/CardsView;->cardsShown:I

    .line 302
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 304
    :cond_2
    iput-object v0, p0, Lcom/google/android/videos/tagging/CardsView;->cardPendingPulseAnimation:Landroid/view/View;

    .line 307
    move v1, v4

    .line 308
    .local v1, "finalToScrollTo":I
    new-instance v5, Lcom/google/android/videos/tagging/CardsView$1;

    invoke-direct {v5, p0, v1, v3}, Lcom/google/android/videos/tagging/CardsView$1;-><init>(Lcom/google/android/videos/tagging/CardsView;IZ)V

    invoke-virtual {p0, v5}, Lcom/google/android/videos/tagging/CardsView;->post(Ljava/lang/Runnable;)Z

    .line 318
    return-void

    .line 284
    .end local v1    # "finalToScrollTo":I
    .end local v3    # "skipAnimations":Z
    :cond_3
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .end local v0    # "card":Landroid/view/View;
    :cond_4
    move v3, v5

    .line 294
    goto :goto_1
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsView;->cardHolder:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->removeAllViews()V

    .line 162
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsView;->cards:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 163
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/tagging/CardsView;->cardsShown:I

    .line 164
    return-void
.end method

.method public dismiss(Landroid/view/View;Z)V
    .locals 2
    .param p1, "card"    # Landroid/view/View;
    .param p2, "immediate"    # Z

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsView;->cards:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "Card not in the view hierarchy of this CardsView"

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;)V

    .line 203
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 214
    :goto_0
    return-void

    .line 206
    :cond_0
    if-eqz p2, :cond_2

    .line 207
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;

    iget-boolean v0, v0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->removeOnDismiss:Z

    if-eqz v0, :cond_1

    .line 208
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsView;->cardHolder:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->removeView(Landroid/view/View;)V

    .line 210
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/videos/tagging/CardsView;->onViewDismissed(Landroid/view/View;)V

    goto :goto_0

    .line 212
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsView;->cardHolder:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->dismissView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public getCardWidth()I
    .locals 4

    .prologue
    .line 86
    iget-object v2, p0, Lcom/google/android/videos/tagging/CardsView;->cardHolder:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v2}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 87
    .local v0, "params":Landroid/widget/FrameLayout$LayoutParams;
    iget v2, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    if-gez v2, :cond_0

    .line 89
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/CardsView;->getMeasuredWidth()I

    move-result v1

    .line 92
    .local v1, "width":I
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/CardsView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 93
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/CardsView;->getPaddingLeft()I

    move-result v2

    sub-int v2, v1, v2

    invoke-virtual {p0}, Lcom/google/android/videos/tagging/CardsView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    sub-int/2addr v2, v3

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    sub-int/2addr v2, v3

    .line 95
    .end local v1    # "width":I
    :goto_0
    return v2

    :cond_0
    iget v2, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    goto :goto_0
.end method

.method public getTopInset()I
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsView;->cardHolder:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getPaddingTop()I

    move-result v0

    return v0
.end method

.method public hide(Landroid/view/View;)Z
    .locals 2
    .param p1, "card"    # Landroid/view/View;

    .prologue
    const/16 v1, 0x8

    .line 172
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsView;->cards:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 173
    const/4 v0, 0x0

    .line 179
    :goto_0
    return v0

    .line 175
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v1, :cond_1

    .line 176
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 177
    invoke-direct {p0, p1}, Lcom/google/android/videos/tagging/CardsView;->onCardHiddenOrRemoved(Landroid/view/View;)V

    .line 179
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public inCardsBounds(FF)Z
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 365
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsView;->cardHolder:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getLeft()I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, v0, p1

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsView;->cardHolder:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getRight()I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsView;->cardHolder:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getTop()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/videos/tagging/CardsView;->cardHolder:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v1}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getPaddingTop()I

    move-result v1

    add-int/2addr v0, v1

    int-to-float v0, v0

    cmpg-float v0, v0, p2

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsView;->cardHolder:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getBottom()I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, p2, v0

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/videos/tagging/CardsView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, p2, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public inflate(I)Landroid/view/View;
    .locals 3
    .param p1, "layoutId"    # I

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsView;->inflater:Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcom/google/android/videos/tagging/CardsView;->cardHolder:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public insertAfter(Ljava/util/List;Landroid/view/View;)V
    .locals 4
    .param p2, "existingCard"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 120
    .local p1, "newCards":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 135
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    iget-object v3, p0, Lcom/google/android/videos/tagging/CardsView;->cards:Ljava/util/List;

    invoke-interface {v3, p2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 124
    .local v2, "index":I
    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    .line 125
    iget-object v3, p0, Lcom/google/android/videos/tagging/CardsView;->cards:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .line 127
    :cond_2
    add-int/lit8 v3, v2, 0x1

    invoke-direct {p0, p1, v3}, Lcom/google/android/videos/tagging/CardsView;->insertAt(Ljava/util/List;I)V

    .line 128
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 129
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 130
    .local v0, "card":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_3

    .line 131
    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/CardsView;->scrollTo(Landroid/view/View;)Z

    goto :goto_0

    .line 128
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public isAnyCardShown()Z
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lcom/google/android/videos/tagging/CardsView;->cardsShown:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onScrollChanged(IIII)V
    .locals 1
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "oldl"    # I
    .param p4, "oldt"    # I

    .prologue
    .line 347
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    .line 348
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsView;->listener:Lcom/google/android/videos/tagging/CardsView$Listener;

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsView;->listener:Lcom/google/android/videos/tagging/CardsView$Listener;

    invoke-interface {v0, p2}, Lcom/google/android/videos/tagging/CardsView$Listener;->onCardsViewScrollChanged(I)V

    .line 351
    :cond_0
    invoke-direct {p0}, Lcom/google/android/videos/tagging/CardsView;->maybePulseCard()V

    .line 352
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 372
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/videos/tagging/CardsView;->inCardsBounds(FF)Z

    move-result v0

    if-nez v0, :cond_0

    .line 374
    const/4 v0, 0x0

    .line 376
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onViewDismissed(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 218
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;

    iget-boolean v0, v0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->removeOnDismiss:Z

    if-eqz v0, :cond_1

    .line 220
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsView;->cards:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 221
    invoke-direct {p0, p1}, Lcom/google/android/videos/tagging/CardsView;->onCardHiddenOrRemoved(Landroid/view/View;)V

    .line 226
    :cond_0
    :goto_0
    return-void

    .line 224
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/videos/tagging/CardsView;->hide(Landroid/view/View;)Z

    goto :goto_0
.end method

.method public scrollTo(Landroid/view/View;)Z
    .locals 2
    .param p1, "card"    # Landroid/view/View;

    .prologue
    .line 235
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    iget-object v1, p0, Lcom/google/android/videos/tagging/CardsView;->cards:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 237
    .local v0, "index":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 238
    const/4 v1, 0x0

    .line 241
    :goto_0
    return v1

    .line 240
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/videos/tagging/CardsView;->scrollToInternal(I)V

    .line 241
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public scrollTo(Lcom/google/android/videos/tagging/KnowledgeEntity;)Z
    .locals 7
    .param p1, "knowledgeEntity"    # Lcom/google/android/videos/tagging/KnowledgeEntity;

    .prologue
    const/4 v5, 0x0

    .line 253
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    const/4 v2, -0x1

    .line 255
    .local v2, "index":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lcom/google/android/videos/tagging/CardsView;->cards:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 256
    iget-object v4, p0, Lcom/google/android/videos/tagging/CardsView;->cards:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    .line 257
    .local v3, "viewTag":Ljava/lang/Object;
    instance-of v4, v3, Lcom/google/android/videos/tagging/CardTag;

    if-eqz v4, :cond_1

    check-cast v3, Lcom/google/android/videos/tagging/CardTag;

    .end local v3    # "viewTag":Ljava/lang/Object;
    iget-object v4, v3, Lcom/google/android/videos/tagging/CardTag;->knowledgeEntity:Lcom/google/android/videos/tagging/KnowledgeEntity;

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 259
    move v2, v1

    .line 263
    :cond_0
    const/4 v4, -0x1

    if-ne v2, v4, :cond_2

    move v4, v5

    .line 278
    :goto_1
    return v4

    .line 255
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 266
    :cond_2
    invoke-direct {p0, v2}, Lcom/google/android/videos/tagging/CardsView;->scrollToInternal(I)V

    .line 268
    add-int/lit8 v1, v2, 0x1

    :goto_2
    iget-object v4, p0, Lcom/google/android/videos/tagging/CardsView;->cards:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_4

    .line 269
    iget-object v4, p0, Lcom/google/android/videos/tagging/CardsView;->cards:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 270
    .local v0, "card":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    .line 271
    .restart local v3    # "viewTag":Ljava/lang/Object;
    instance-of v4, v3, Lcom/google/android/videos/tagging/CardTag;

    if-eqz v4, :cond_3

    check-cast v3, Lcom/google/android/videos/tagging/CardTag;

    .end local v3    # "viewTag":Ljava/lang/Object;
    iget-object v4, v3, Lcom/google/android/videos/tagging/CardTag;->knowledgeEntity:Lcom/google/android/videos/tagging/KnowledgeEntity;

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/16 v6, 0x8

    if-ne v4, v6, :cond_3

    .line 274
    iget v4, p0, Lcom/google/android/videos/tagging/CardsView;->cardsShown:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/android/videos/tagging/CardsView;->cardsShown:I

    .line 275
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 268
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 278
    .end local v0    # "card":Landroid/view/View;
    :cond_4
    const/4 v4, 0x1

    goto :goto_1
.end method

.method public setCardDismissListener(Lcom/google/android/videos/tagging/CardsView$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/videos/tagging/CardsView$Listener;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/google/android/videos/tagging/CardsView;->listener:Lcom/google/android/videos/tagging/CardsView$Listener;

    .line 69
    return-void
.end method

.method public setChangeAppearingAnimatorEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsView;->cardHolder:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->setChangeAppearingAnimatorEnabled(Z)V

    .line 154
    return-void
.end method

.method public setTopInset(I)V
    .locals 2
    .param p1, "topInset"    # I

    .prologue
    const/4 v1, 0x0

    .line 81
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsView;->cardHolder:Lcom/google/android/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v0, v1, p1, v1, v1}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->setPadding(IIII)V

    .line 82
    return-void
.end method

.method public setTranslationY(F)V
    .locals 0
    .param p1, "translationY"    # F

    .prologue
    .line 356
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->setTranslationY(F)V

    .line 357
    invoke-direct {p0}, Lcom/google/android/videos/tagging/CardsView;->maybePulseCard()V

    .line 358
    return-void
.end method

.method public show(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 106
    .local p1, "cards":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/CardsView;->clear()V

    .line 107
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/tagging/CardsView;->insertAt(Ljava/util/List;I)V

    .line 108
    return-void
.end method
