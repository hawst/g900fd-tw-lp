.class public Lcom/google/android/videos/utils/AudioInfoUtil;
.super Ljava/lang/Object;
.source "AudioInfoUtil.java"


# direct methods
.method public static areEqual(Lcom/google/wireless/android/video/magma/proto/AudioInfo;Lcom/google/wireless/android/video/magma/proto/AudioInfo;Z)Z
    .locals 4
    .param p0, "audioInfo1"    # Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    .param p1, "audioInfo2"    # Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    .param p2, "ignoreChannelCount"    # Z

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 100
    if-nez p0, :cond_2

    .line 101
    if-nez p1, :cond_1

    .line 105
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 101
    goto :goto_0

    .line 102
    :cond_2
    if-nez p1, :cond_3

    move v0, v1

    .line 103
    goto :goto_0

    .line 105
    :cond_3
    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->audioSourceType:I

    iget v3, p1, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->audioSourceType:I

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->type:I

    iget v3, p1, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->type:I

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->language:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->language:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    if-nez p2, :cond_0

    iget-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->audio51:Z

    iget-boolean v3, p1, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->audio51:Z

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public static containsInfo(Ljava/util/List;Lcom/google/wireless/android/video/magma/proto/AudioInfo;Z)Z
    .locals 2
    .param p1, "newInfo"    # Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    .param p2, "ignoreChannelCount"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AudioInfo;",
            ">;",
            "Lcom/google/wireless/android/video/magma/proto/AudioInfo;",
            "Z)Z"
        }
    .end annotation

    .prologue
    .line 82
    .local p0, "infos":Ljava/util/List;, "Ljava/util/List<Lcom/google/wireless/android/video/magma/proto/AudioInfo;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 83
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    invoke-static {v1, p1, p2}, Lcom/google/android/videos/utils/AudioInfoUtil;->areEqual(Lcom/google/wireless/android/video/magma/proto/AudioInfo;Lcom/google/wireless/android/video/magma/proto/AudioInfo;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 84
    const/4 v1, 0x1

    .line 87
    :goto_1
    return v1

    .line 82
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 87
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static containsStreamWithInfo(Ljava/util/List;Lcom/google/wireless/android/video/magma/proto/AudioInfo;Z)Z
    .locals 2
    .param p1, "newInfo"    # Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    .param p2, "ignoreChannelCount"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;",
            "Lcom/google/wireless/android/video/magma/proto/AudioInfo;",
            "Z)Z"
        }
    .end annotation

    .prologue
    .line 72
    .local p0, "streams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 73
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/streams/MediaStream;

    iget-object v1, v1, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-object v1, v1, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    invoke-static {v1, p1, p2}, Lcom/google/android/videos/utils/AudioInfoUtil;->areEqual(Lcom/google/wireless/android/video/magma/proto/AudioInfo;Lcom/google/wireless/android/video/magma/proto/AudioInfo;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 74
    const/4 v1, 0x1

    .line 77
    :goto_1
    return v1

    .line 72
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 77
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private static getOriginalLanguageStreamIndex(Ljava/util/List;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;)I"
        }
    .end annotation

    .prologue
    .local p0, "streams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    const/4 v3, 0x1

    .line 44
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 45
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/streams/MediaStream;

    iget-object v2, v2, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-object v0, v2, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    .line 46
    .local v0, "audioInfo":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    if-eqz v0, :cond_0

    iget v2, v0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->type:I

    if-ne v2, v3, :cond_0

    iget v2, v0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->audioSourceType:I

    if-ne v2, v3, :cond_0

    .line 51
    .end local v0    # "audioInfo":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    .end local v1    # "i":I
    :goto_1
    return v1

    .line 44
    .restart local v0    # "audioInfo":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    .restart local v1    # "i":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 51
    .end local v0    # "audioInfo":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public static getPreferredStreamIndex(Ljava/util/List;Landroid/content/Context;Landroid/content/SharedPreferences;)I
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "preferences"    # Landroid/content/SharedPreferences;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;",
            "Landroid/content/Context;",
            "Landroid/content/SharedPreferences;",
            ")I"
        }
    .end annotation

    .prologue
    .line 24
    .local p0, "streams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0238

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 26
    .local v0, "originalAudioPreferenceValue":Ljava/lang/String;
    const-string v4, "audio_language"

    invoke-interface {p2, v4, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 29
    .local v3, "userPreference":Ljava/lang/String;
    invoke-static {p0}, Lcom/google/android/videos/utils/AudioInfoUtil;->getOriginalLanguageStreamIndex(Ljava/util/List;)I

    move-result v1

    .line 30
    .local v1, "originalLanguageIndex":I
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    if-ltz v1, :cond_0

    .line 39
    .end local v1    # "originalLanguageIndex":I
    :goto_0
    return v1

    .line 34
    .restart local v1    # "originalLanguageIndex":I
    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/google/android/videos/utils/AudioInfoUtil;->getPreferredStreamIndex(Ljava/util/List;Ljava/lang/String;)I

    move-result v2

    .line 35
    .local v2, "systemLanguageIndex":I
    if-ltz v2, :cond_1

    move v1, v2

    .line 36
    goto :goto_0

    .line 39
    :cond_1
    const/4 v4, 0x0

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_0
.end method

.method private static getPreferredStreamIndex(Ljava/util/List;Ljava/lang/String;)I
    .locals 7
    .param p1, "preferredLanguage"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    .prologue
    .line 55
    .local p0, "streams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    const/4 v2, -0x1

    .line 56
    .local v2, "bestStreamIndex":I
    const/4 v1, 0x0

    .line 57
    .local v1, "bestScore":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v5

    if-ge v3, v5, :cond_1

    .line 58
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/videos/streams/MediaStream;

    iget-object v5, v5, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-object v0, v5, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    .line 59
    .local v0, "audioInfo":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    if-eqz v0, :cond_0

    iget v5, v0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->type:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    .line 60
    iget-object v5, v0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->language:Ljava/lang/String;

    invoke-static {p1, v5}, Lcom/google/android/videos/utils/Util;->getLanguageMatchScore(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 61
    .local v4, "score":I
    if-le v4, v1, :cond_0

    .line 62
    move v1, v4

    .line 63
    move v2, v3

    .line 57
    .end local v4    # "score":I
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 67
    .end local v0    # "audioInfo":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    :cond_1
    return v2
.end method
