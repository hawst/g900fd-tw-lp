.class public Lcom/google/android/videos/remote/RemoteTracker;
.super Ljava/lang/Object;
.source "RemoteTracker.java"

# interfaces
.implements Lcom/google/android/videos/ui/ArtworkMonitor$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/remote/RemoteTracker$ResumeState;,
        Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;,
        Lcom/google/android/videos/remote/RemoteTracker$ControlListener;,
        Lcom/google/android/videos/remote/RemoteTracker$MediaRouteManagerListener;
    }
.end annotation


# instance fields
.field private final accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

.field private clientState:Lcom/google/android/videos/remote/ClientState;

.field private final context:Landroid/content/Context;

.field private final controlListener:Lcom/google/android/videos/remote/RemoteTracker$ControlListener;

.field private currentRemote:Lcom/google/android/videos/remote/RemoteControl;

.field private final database:Lcom/google/android/videos/store/Database;

.field private final executor:Ljava/util/concurrent/Executor;

.field private isPlaying:Z

.field private lastResumeTimestampFlushMillis:J

.field private final lockScreenTransportControl:Lcom/google/android/videos/remote/LockScreenTransportControlV14;

.field private final notificationTransportControl:Lcom/google/android/videos/remote/NotificationTransportControl;

.field private posterBitmap:Landroid/graphics/Bitmap;

.field private final posterMonitor:Lcom/google/android/videos/ui/ArtworkMonitor;

.field private final resumeState:Lcom/google/android/videos/remote/RemoteTracker$ResumeState;

.field private final transportControlListener:Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;

.field private final transportControls:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/remote/TransportControl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/store/Database;Ljava/util/concurrent/Executor;Lcom/google/android/videos/store/PosterStore;Lcom/google/android/videos/remote/MediaRouteManager;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .param p3, "database"    # Lcom/google/android/videos/store/Database;
    .param p4, "localExecutor"    # Ljava/util/concurrent/Executor;
    .param p5, "posterStore"    # Lcom/google/android/videos/store/PosterStore;
    .param p6, "mediaRouteManager"    # Lcom/google/android/videos/remote/MediaRouteManager;

    .prologue
    const/4 v5, 0x0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object p1, p0, Lcom/google/android/videos/remote/RemoteTracker;->context:Landroid/content/Context;

    .line 71
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/accounts/AccountManagerWrapper;

    iput-object v3, p0, Lcom/google/android/videos/remote/RemoteTracker;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    .line 72
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/store/Database;

    iput-object v3, p0, Lcom/google/android/videos/remote/RemoteTracker;->database:Lcom/google/android/videos/store/Database;

    .line 73
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/Executor;

    iput-object v3, p0, Lcom/google/android/videos/remote/RemoteTracker;->executor:Ljava/util/concurrent/Executor;

    .line 74
    new-instance v3, Lcom/google/android/videos/remote/RemoteTracker$ControlListener;

    invoke-direct {v3, p0, v5}, Lcom/google/android/videos/remote/RemoteTracker$ControlListener;-><init>(Lcom/google/android/videos/remote/RemoteTracker;Lcom/google/android/videos/remote/RemoteTracker$1;)V

    iput-object v3, p0, Lcom/google/android/videos/remote/RemoteTracker;->controlListener:Lcom/google/android/videos/remote/RemoteTracker$ControlListener;

    .line 75
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/google/android/videos/remote/RemoteTracker;->transportControls:Ljava/util/List;

    .line 76
    new-instance v3, Lcom/google/android/videos/remote/RemoteTracker$ResumeState;

    invoke-direct {v3, v5}, Lcom/google/android/videos/remote/RemoteTracker$ResumeState;-><init>(Lcom/google/android/videos/remote/RemoteTracker$1;)V

    iput-object v3, p0, Lcom/google/android/videos/remote/RemoteTracker;->resumeState:Lcom/google/android/videos/remote/RemoteTracker$ResumeState;

    .line 77
    new-instance v3, Lcom/google/android/videos/ui/ArtworkMonitor;

    invoke-direct {v3, p5, p3, p0}, Lcom/google/android/videos/ui/ArtworkMonitor;-><init>(Lcom/google/android/videos/store/PosterStore;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/ui/ArtworkMonitor$Listener;)V

    iput-object v3, p0, Lcom/google/android/videos/remote/RemoteTracker;->posterMonitor:Lcom/google/android/videos/ui/ArtworkMonitor;

    .line 79
    invoke-static {p0, p1}, Lcom/google/android/videos/remote/NotificationTransportControl;->newInstance(Lcom/google/android/videos/remote/RemoteTracker;Landroid/content/Context;)Lcom/google/android/videos/remote/NotificationTransportControl;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/remote/RemoteTracker;->notificationTransportControl:Lcom/google/android/videos/remote/NotificationTransportControl;

    .line 80
    iget-object v3, p0, Lcom/google/android/videos/remote/RemoteTracker;->transportControls:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/videos/remote/RemoteTracker;->notificationTransportControl:Lcom/google/android/videos/remote/NotificationTransportControl;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    invoke-static {p0, p1}, Lcom/google/android/videos/remote/LockScreenTransportControl;->newInstanceOrNull(Lcom/google/android/videos/remote/RemoteTracker;Landroid/content/Context;)Lcom/google/android/videos/remote/LockScreenTransportControlV14;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/remote/RemoteTracker;->lockScreenTransportControl:Lcom/google/android/videos/remote/LockScreenTransportControlV14;

    .line 83
    iget-object v3, p0, Lcom/google/android/videos/remote/RemoteTracker;->lockScreenTransportControl:Lcom/google/android/videos/remote/LockScreenTransportControlV14;

    if-eqz v3, :cond_0

    .line 84
    iget-object v3, p0, Lcom/google/android/videos/remote/RemoteTracker;->transportControls:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/videos/remote/RemoteTracker;->lockScreenTransportControl:Lcom/google/android/videos/remote/LockScreenTransportControlV14;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    :cond_0
    new-instance v3, Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;

    invoke-direct {v3, p0, v5}, Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;-><init>(Lcom/google/android/videos/remote/RemoteTracker;Lcom/google/android/videos/remote/RemoteTracker$1;)V

    iput-object v3, p0, Lcom/google/android/videos/remote/RemoteTracker;->transportControlListener:Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;

    .line 88
    iget-object v3, p0, Lcom/google/android/videos/remote/RemoteTracker;->transportControls:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/remote/TransportControl;

    .line 89
    .local v2, "transportControl":Lcom/google/android/videos/remote/TransportControl;
    iget-object v3, p0, Lcom/google/android/videos/remote/RemoteTracker;->transportControlListener:Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;

    invoke-virtual {v2, v3}, Lcom/google/android/videos/remote/TransportControl;->addListener(Lcom/google/android/videos/remote/TransportControl$Listener;)V

    goto :goto_0

    .line 92
    .end local v2    # "transportControl":Lcom/google/android/videos/remote/TransportControl;
    :cond_1
    new-instance v1, Lcom/google/android/videos/remote/RemoteTracker$MediaRouteManagerListener;

    invoke-direct {v1, p0, v5}, Lcom/google/android/videos/remote/RemoteTracker$MediaRouteManagerListener;-><init>(Lcom/google/android/videos/remote/RemoteTracker;Lcom/google/android/videos/remote/RemoteTracker$1;)V

    .line 93
    .local v1, "mediaRouteManagerListener":Lcom/google/android/videos/remote/RemoteTracker$MediaRouteManagerListener;
    invoke-virtual {p6, v1}, Lcom/google/android/videos/remote/MediaRouteManager;->register(Lcom/google/android/videos/remote/MediaRouteManager$Listener;)V

    .line 94
    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/videos/remote/RemoteTracker;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/remote/RemoteTracker;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemoteTracker;->updateAll()V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/videos/remote/RemoteTracker;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/remote/RemoteTracker;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemoteTracker;->onErrorChanged()V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/videos/remote/RemoteTracker;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/remote/RemoteTracker;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemoteTracker;->onPlayerStateChanged()V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/videos/remote/RemoteTracker;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/remote/RemoteTracker;

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->isPlaying:Z

    return v0
.end method

.method static synthetic access$1302(Lcom/google/android/videos/remote/RemoteTracker;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/remote/RemoteTracker;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/google/android/videos/remote/RemoteTracker;->isPlaying:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/google/android/videos/remote/RemoteTracker;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/remote/RemoteTracker;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemoteTracker;->onVideoInfoChanged()V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/videos/remote/RemoteTracker;Lcom/google/android/videos/remote/RemoteVideoInfo;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/remote/RemoteTracker;
    .param p1, "x1"    # Lcom/google/android/videos/remote/RemoteVideoInfo;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/google/android/videos/remote/RemoteTracker;->showPlaybackScreen(Lcom/google/android/videos/remote/RemoteVideoInfo;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/videos/remote/RemoteTracker;)Lcom/google/android/videos/store/Database;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/remote/RemoteTracker;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->database:Lcom/google/android/videos/store/Database;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/videos/remote/RemoteTracker;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/remote/RemoteTracker;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/videos/remote/RemoteTracker;)Lcom/google/android/videos/remote/RemoteControl;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/remote/RemoteTracker;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/android/videos/remote/RemoteTracker;Lcom/google/android/videos/remote/RemoteControl;)Lcom/google/android/videos/remote/RemoteControl;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/remote/RemoteTracker;
    .param p1, "x1"    # Lcom/google/android/videos/remote/RemoteControl;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/android/videos/remote/RemoteTracker;)Lcom/google/android/videos/remote/RemoteTracker$ControlListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/remote/RemoteTracker;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->controlListener:Lcom/google/android/videos/remote/RemoteTracker$ControlListener;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/videos/remote/RemoteTracker;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/remote/RemoteTracker;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemoteTracker;->maybeCacheResumeTimestamp()V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/videos/remote/RemoteTracker;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/remote/RemoteTracker;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/google/android/videos/remote/RemoteTracker;->flushResumeTimestampIfDirty(Z)V

    return-void
.end method

.method private flushResumeTimestampIfDirty(Z)V
    .locals 12
    .param p1, "uploadChanges"    # Z

    .prologue
    const/4 v6, 0x0

    const/4 v8, 0x0

    .line 325
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->resumeState:Lcom/google/android/videos/remote/RemoteTracker$ResumeState;

    iget-boolean v0, v0, Lcom/google/android/videos/remote/RemoteTracker$ResumeState;->dirty:Z

    if-nez v0, :cond_0

    .line 369
    :goto_0
    return-void

    .line 328
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->resumeState:Lcom/google/android/videos/remote/RemoteTracker$ResumeState;

    iput-boolean v8, v0, Lcom/google/android/videos/remote/RemoteTracker$ResumeState;->dirty:Z

    .line 329
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->lastResumeTimestampFlushMillis:J

    .line 331
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "New resume timestamp = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteTracker;->resumeState:Lcom/google/android/videos/remote/RemoteTracker$ResumeState;

    iget v1, v1, Lcom/google/android/videos/remote/RemoteTracker$ResumeState;->timestamp:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 332
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 333
    .local v2, "values":Landroid/content/ContentValues;
    const-string v0, "last_playback_is_dirty"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 335
    const-string v0, "last_playback_start_timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 336
    const-string v0, "last_watched_timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 337
    const-string v0, "resume_timestamp"

    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteTracker;->resumeState:Lcom/google/android/videos/remote/RemoteTracker$ResumeState;

    iget v1, v1, Lcom/google/android/videos/remote/RemoteTracker$ResumeState;->timestamp:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 339
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->resumeState:Lcom/google/android/videos/remote/RemoteTracker$ResumeState;

    iget-object v3, v0, Lcom/google/android/videos/remote/RemoteTracker$ResumeState;->account:Ljava/lang/String;

    .line 340
    .local v3, "account":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->resumeState:Lcom/google/android/videos/remote/RemoteTracker$ResumeState;

    iget-object v4, v0, Lcom/google/android/videos/remote/RemoteTracker$ResumeState;->videoId:Ljava/lang/String;

    .line 341
    .local v4, "videoId":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->clientState:Lcom/google/android/videos/remote/ClientState;

    if-nez v0, :cond_2

    .line 343
    .local v8, "shouldSetEpisodeData":Z
    :goto_1
    if-eqz v8, :cond_3

    invoke-virtual {p0}, Lcom/google/android/videos/remote/RemoteTracker;->getSeasonId()Ljava/lang/String;

    move-result-object v5

    .line 344
    .local v5, "seasonId":Ljava/lang/String;
    :goto_2
    if-eqz v8, :cond_1

    invoke-virtual {p0}, Lcom/google/android/videos/remote/RemoteTracker;->getShowId()Ljava/lang/String;

    move-result-object v6

    .line 345
    .local v6, "showId":Ljava/lang/String;
    :cond_1
    iget-object v9, p0, Lcom/google/android/videos/remote/RemoteTracker;->executor:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/google/android/videos/remote/RemoteTracker$1;

    move-object v1, p0

    move v7, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/remote/RemoteTracker$1;-><init>(Lcom/google/android/videos/remote/RemoteTracker;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v9, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 341
    .end local v5    # "seasonId":Ljava/lang/String;
    .end local v6    # "showId":Ljava/lang/String;
    .end local v8    # "shouldSetEpisodeData":Z
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->clientState:Lcom/google/android/videos/remote/ClientState;

    iget-object v0, v0, Lcom/google/android/videos/remote/ClientState;->videoId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteTracker;->resumeState:Lcom/google/android/videos/remote/RemoteTracker$ResumeState;

    iget-object v1, v1, Lcom/google/android/videos/remote/RemoteTracker$ResumeState;->videoId:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    goto :goto_1

    .restart local v8    # "shouldSetEpisodeData":Z
    :cond_3
    move-object v5, v6

    .line 343
    goto :goto_2
.end method

.method private getAccountName()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 260
    iget-object v5, p0, Lcom/google/android/videos/remote/RemoteTracker;->clientState:Lcom/google/android/videos/remote/ClientState;

    if-nez v5, :cond_1

    .line 269
    :cond_0
    :goto_0
    return-object v4

    .line 264
    :cond_1
    iget-object v5, p0, Lcom/google/android/videos/remote/RemoteTracker;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v5}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getAccounts()[Landroid/accounts/Account;

    move-result-object v1

    .local v1, "arr$":[Landroid/accounts/Account;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_0

    aget-object v0, v1, v2

    .line 265
    .local v0, "account":Landroid/accounts/Account;
    iget-object v5, p0, Lcom/google/android/videos/remote/RemoteTracker;->clientState:Lcom/google/android/videos/remote/ClientState;

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/google/android/videos/remote/ClientState;->accountMatches(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 266
    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_0

    .line 264
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private maybeCacheResumeTimestamp()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 282
    iget-object v4, p0, Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    if-nez v4, :cond_1

    .line 319
    :cond_0
    :goto_0
    return-void

    .line 286
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/videos/remote/RemoteTracker;->lastResumeTimestampFlushMillis:J

    sub-long/2addr v4, v6

    const-wide/32 v6, 0x1d4c0

    cmp-long v4, v4, v6

    if-lez v4, :cond_2

    .line 288
    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/google/android/videos/remote/RemoteTracker;->flushResumeTimestampIfDirty(Z)V

    .line 291
    :cond_2
    iget-object v4, p0, Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v4}, Lcom/google/android/videos/remote/RemoteControl;->getVideoInfo()Lcom/google/android/videos/remote/RemoteVideoInfo;

    move-result-object v3

    .line 292
    .local v3, "videoInfo":Lcom/google/android/videos/remote/RemoteVideoInfo;
    iget-object v4, p0, Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v4}, Lcom/google/android/videos/remote/RemoteControl;->getPlayerState()Lcom/google/android/videos/remote/PlayerState;

    move-result-object v1

    .line 293
    .local v1, "playerState":Lcom/google/android/videos/remote/PlayerState;
    if-eqz v3, :cond_0

    if-eqz v1, :cond_0

    .line 297
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemoteTracker;->getAccountName()Ljava/lang/String;

    move-result-object v0

    .line 298
    .local v0, "account":Ljava/lang/String;
    iget-object v2, v1, Lcom/google/android/videos/remote/PlayerState;->videoId:Ljava/lang/String;

    .line 299
    .local v2, "videoId":Ljava/lang/String;
    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    .line 303
    iget-object v4, p0, Lcom/google/android/videos/remote/RemoteTracker;->resumeState:Lcom/google/android/videos/remote/RemoteTracker$ResumeState;

    iget-object v4, v4, Lcom/google/android/videos/remote/RemoteTracker$ResumeState;->account:Ljava/lang/String;

    invoke-static {v4, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/videos/remote/RemoteTracker;->resumeState:Lcom/google/android/videos/remote/RemoteTracker$ResumeState;

    iget-object v4, v4, Lcom/google/android/videos/remote/RemoteTracker$ResumeState;->videoId:Ljava/lang/String;

    invoke-static {v4, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 307
    :cond_3
    invoke-direct {p0, v8}, Lcom/google/android/videos/remote/RemoteTracker;->flushResumeTimestampIfDirty(Z)V

    .line 310
    :cond_4
    iget-object v4, p0, Lcom/google/android/videos/remote/RemoteTracker;->resumeState:Lcom/google/android/videos/remote/RemoteTracker$ResumeState;

    iput-object v0, v4, Lcom/google/android/videos/remote/RemoteTracker$ResumeState;->account:Ljava/lang/String;

    .line 311
    iget-object v4, p0, Lcom/google/android/videos/remote/RemoteTracker;->resumeState:Lcom/google/android/videos/remote/RemoteTracker$ResumeState;

    iget v5, v1, Lcom/google/android/videos/remote/PlayerState;->time:I

    iput v5, v4, Lcom/google/android/videos/remote/RemoteTracker$ResumeState;->timestamp:I

    .line 312
    iget-object v4, p0, Lcom/google/android/videos/remote/RemoteTracker;->resumeState:Lcom/google/android/videos/remote/RemoteTracker$ResumeState;

    iput-object v2, v4, Lcom/google/android/videos/remote/RemoteTracker$ResumeState;->videoId:Ljava/lang/String;

    .line 313
    iget-object v4, p0, Lcom/google/android/videos/remote/RemoteTracker;->resumeState:Lcom/google/android/videos/remote/RemoteTracker$ResumeState;

    iput-boolean v8, v4, Lcom/google/android/videos/remote/RemoteTracker$ResumeState;->dirty:Z

    .line 316
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 317
    invoke-direct {p0, v8}, Lcom/google/android/videos/remote/RemoteTracker;->flushResumeTimestampIfDirty(Z)V

    goto :goto_0
.end method

.method private onErrorChanged()V
    .locals 3

    .prologue
    .line 414
    invoke-static {}, Lcom/google/android/videos/utils/Preconditions;->checkMainThread()V

    .line 415
    iget-object v2, p0, Lcom/google/android/videos/remote/RemoteTracker;->transportControls:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/remote/TransportControl;

    .line 416
    .local v1, "transportControl":Lcom/google/android/videos/remote/TransportControl;
    invoke-virtual {v1}, Lcom/google/android/videos/remote/TransportControl;->onErrorChanged()V

    goto :goto_0

    .line 418
    .end local v1    # "transportControl":Lcom/google/android/videos/remote/TransportControl;
    :cond_0
    return-void
.end method

.method private onPlayerStateChanged()V
    .locals 3

    .prologue
    .line 424
    invoke-static {}, Lcom/google/android/videos/utils/Preconditions;->checkMainThread()V

    .line 425
    iget-object v2, p0, Lcom/google/android/videos/remote/RemoteTracker;->transportControls:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/remote/TransportControl;

    .line 426
    .local v1, "transportControl":Lcom/google/android/videos/remote/TransportControl;
    invoke-virtual {v1}, Lcom/google/android/videos/remote/TransportControl;->onPlayerStateChanged()V

    goto :goto_0

    .line 428
    .end local v1    # "transportControl":Lcom/google/android/videos/remote/TransportControl;
    :cond_0
    return-void
.end method

.method private onScreenNameChanged()V
    .locals 3

    .prologue
    .line 404
    invoke-static {}, Lcom/google/android/videos/utils/Preconditions;->checkMainThread()V

    .line 405
    iget-object v2, p0, Lcom/google/android/videos/remote/RemoteTracker;->transportControls:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/remote/TransportControl;

    .line 406
    .local v1, "transportControl":Lcom/google/android/videos/remote/TransportControl;
    invoke-virtual {v1}, Lcom/google/android/videos/remote/TransportControl;->onScreenNameChanged()V

    goto :goto_0

    .line 408
    .end local v1    # "transportControl":Lcom/google/android/videos/remote/TransportControl;
    :cond_0
    return-void
.end method

.method private onVideoInfoChanged()V
    .locals 5

    .prologue
    .line 437
    invoke-static {}, Lcom/google/android/videos/utils/Preconditions;->checkMainThread()V

    .line 440
    invoke-virtual {p0}, Lcom/google/android/videos/remote/RemoteTracker;->getVideoInfo()Lcom/google/android/videos/remote/RemoteVideoInfo;

    move-result-object v2

    .line 441
    .local v2, "videoInfo":Lcom/google/android/videos/remote/RemoteVideoInfo;
    if-eqz v2, :cond_0

    new-instance v3, Lcom/google/android/videos/remote/ClientState;

    iget-object v4, v2, Lcom/google/android/videos/remote/RemoteVideoInfo;->opaqueString:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/videos/remote/ClientState;-><init>(Landroid/net/Uri;)V

    :goto_0
    iput-object v3, p0, Lcom/google/android/videos/remote/RemoteTracker;->clientState:Lcom/google/android/videos/remote/ClientState;

    .line 443
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemoteTracker;->updatePoster()V

    .line 445
    iget-object v3, p0, Lcom/google/android/videos/remote/RemoteTracker;->transportControls:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/remote/TransportControl;

    .line 446
    .local v1, "transportControl":Lcom/google/android/videos/remote/TransportControl;
    invoke-virtual {v1}, Lcom/google/android/videos/remote/TransportControl;->onVideoInfoChanged()V

    goto :goto_1

    .line 441
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "transportControl":Lcom/google/android/videos/remote/TransportControl;
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 448
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    return-void
.end method

.method private showPlaybackScreen(Lcom/google/android/videos/remote/RemoteVideoInfo;)Z
    .locals 8
    .param p1, "videoInfo"    # Lcom/google/android/videos/remote/RemoteVideoInfo;

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    const/high16 v5, 0x10000000

    .line 378
    if-eqz p1, :cond_0

    move v0, v6

    :goto_0
    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    .line 380
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemoteTracker;->getAccountName()Ljava/lang/String;

    move-result-object v1

    .line 381
    .local v1, "account":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 397
    :goto_1
    return v2

    .end local v1    # "account":Ljava/lang/String;
    :cond_0
    move v0, v2

    .line 378
    goto :goto_0

    .line 386
    .restart local v1    # "account":Ljava/lang/String;
    :cond_1
    iget-boolean v0, p1, Lcom/google/android/videos/remote/RemoteVideoInfo;->isTrailer:Z

    if-eqz v0, :cond_2

    .line 387
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/videos/remote/RemoteTracker;->context:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/videos/remote/RemoteTracker;->clientState:Lcom/google/android/videos/remote/ClientState;

    iget-object v3, v3, Lcom/google/android/videos/remote/ClientState;->videoId:Ljava/lang/String;

    invoke-static {v2, v1, v3, v5}, Lcom/google/android/videos/activity/LauncherActivity;->createWatchTrailerDeepLinkingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :goto_2
    move v2, v6

    .line 397
    goto :goto_1

    .line 389
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->clientState:Lcom/google/android/videos/remote/ClientState;

    iget-object v0, v0, Lcom/google/android/videos/remote/ClientState;->seasonId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 390
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/videos/remote/RemoteTracker;->context:Landroid/content/Context;

    iget-object v3, p1, Lcom/google/android/videos/remote/RemoteVideoInfo;->videoId:Ljava/lang/String;

    invoke-static {v2, v1, v3, v5}, Lcom/google/android/videos/activity/LauncherActivity;->createWatchMovieDeepLinkingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_2

    .line 393
    :cond_3
    iget-object v7, p0, Lcom/google/android/videos/remote/RemoteTracker;->context:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/videos/remote/RemoteTracker;->clientState:Lcom/google/android/videos/remote/ClientState;

    iget-object v2, v2, Lcom/google/android/videos/remote/ClientState;->showId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/remote/RemoteTracker;->clientState:Lcom/google/android/videos/remote/ClientState;

    iget-object v3, v3, Lcom/google/android/videos/remote/ClientState;->seasonId:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/videos/remote/RemoteVideoInfo;->videoId:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/activity/LauncherActivity;->createWatchEpisodeDeepLinkingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_2
.end method

.method private updateAll()V
    .locals 0

    .prologue
    .line 250
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemoteTracker;->onScreenNameChanged()V

    .line 251
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemoteTracker;->onErrorChanged()V

    .line 252
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemoteTracker;->onPlayerStateChanged()V

    .line 253
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemoteTracker;->onVideoInfoChanged()V

    .line 254
    return-void
.end method

.method private updatePoster()V
    .locals 3

    .prologue
    .line 455
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->clientState:Lcom/google/android/videos/remote/ClientState;

    if-eqz v0, :cond_1

    .line 456
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->clientState:Lcom/google/android/videos/remote/ClientState;

    invoke-virtual {v0}, Lcom/google/android/videos/remote/ClientState;->isMovie()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 457
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->posterMonitor:Lcom/google/android/videos/ui/ArtworkMonitor;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/videos/remote/RemoteTracker;->clientState:Lcom/google/android/videos/remote/ClientState;

    iget-object v2, v2, Lcom/google/android/videos/remote/ClientState;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/ui/ArtworkMonitor;->setMonitored(ILjava/lang/String;)V

    .line 464
    :goto_0
    return-void

    .line 459
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->posterMonitor:Lcom/google/android/videos/ui/ArtworkMonitor;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/videos/remote/RemoteTracker;->clientState:Lcom/google/android/videos/remote/ClientState;

    iget-object v2, v2, Lcom/google/android/videos/remote/ClientState;->showId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/ui/ArtworkMonitor;->setMonitored(ILjava/lang/String;)V

    goto :goto_0

    .line 462
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->posterMonitor:Lcom/google/android/videos/ui/ArtworkMonitor;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/ArtworkMonitor;->reset()V

    goto :goto_0
.end method


# virtual methods
.method protected disableRemoteControl(Lcom/google/android/videos/remote/TransportControl;)V
    .locals 1
    .param p1, "transportControl"    # Lcom/google/android/videos/remote/TransportControl;

    .prologue
    .line 126
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->transportControlListener:Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;

    invoke-virtual {p1, v0}, Lcom/google/android/videos/remote/TransportControl;->removeListener(Lcom/google/android/videos/remote/TransportControl$Listener;)V

    .line 128
    return-void
.end method

.method protected enableRemoteControl(Lcom/google/android/videos/remote/TransportControl;)V
    .locals 1
    .param p1, "transportControl"    # Lcom/google/android/videos/remote/TransportControl;

    .prologue
    .line 118
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->transportControlListener:Lcom/google/android/videos/remote/RemoteTracker$TransportControlListener;

    invoke-virtual {p1, v0}, Lcom/google/android/videos/remote/TransportControl;->addListener(Lcom/google/android/videos/remote/TransportControl$Listener;)V

    .line 120
    return-void
.end method

.method public getError()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    invoke-static {}, Lcom/google/android/videos/utils/Preconditions;->checkMainThread()V

    .line 158
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v0}, Lcom/google/android/videos/remote/RemoteControl;->getErrorMessage()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLockScreenTransportControl()Lcom/google/android/videos/remote/LockScreenTransportControlV14;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->lockScreenTransportControl:Lcom/google/android/videos/remote/LockScreenTransportControlV14;

    return-object v0
.end method

.method public getNotificationTransportControl()Lcom/google/android/videos/remote/NotificationTransportControl;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->notificationTransportControl:Lcom/google/android/videos/remote/NotificationTransportControl;

    return-object v0
.end method

.method public getPlayerState()Lcom/google/android/videos/remote/PlayerState;
    .locals 1

    .prologue
    .line 173
    invoke-static {}, Lcom/google/android/videos/utils/Preconditions;->checkMainThread()V

    .line 174
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v0}, Lcom/google/android/videos/remote/RemoteControl;->getPlayerState()Lcom/google/android/videos/remote/PlayerState;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPosterBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 189
    invoke-static {}, Lcom/google/android/videos/utils/Preconditions;->checkMainThread()V

    .line 190
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->posterBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getScreenName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    invoke-static {}, Lcom/google/android/videos/utils/Preconditions;->checkMainThread()V

    .line 149
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v0}, Lcom/google/android/videos/remote/RemoteControl;->getScreenName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSeasonId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    invoke-static {}, Lcom/google/android/videos/utils/Preconditions;->checkMainThread()V

    .line 218
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->clientState:Lcom/google/android/videos/remote/ClientState;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->clientState:Lcom/google/android/videos/remote/ClientState;

    iget-object v0, v0, Lcom/google/android/videos/remote/ClientState;->seasonId:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getShowId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 198
    invoke-static {}, Lcom/google/android/videos/utils/Preconditions;->checkMainThread()V

    .line 199
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->clientState:Lcom/google/android/videos/remote/ClientState;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->clientState:Lcom/google/android/videos/remote/ClientState;

    iget-object v0, v0, Lcom/google/android/videos/remote/ClientState;->showId:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getVideoInfo()Lcom/google/android/videos/remote/RemoteVideoInfo;
    .locals 1

    .prologue
    .line 181
    invoke-static {}, Lcom/google/android/videos/utils/Preconditions;->checkMainThread()V

    .line 182
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v0}, Lcom/google/android/videos/remote/RemoteControl;->getVideoInfo()Lcom/google/android/videos/remote/RemoteVideoInfo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasError()Z
    .locals 1

    .prologue
    .line 165
    invoke-static {}, Lcom/google/android/videos/utils/Preconditions;->checkMainThread()V

    .line 166
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v0}, Lcom/google/android/videos/remote/RemoteControl;->getError()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPlaying(Ljava/lang/String;)Z
    .locals 1
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 232
    invoke-virtual {p0}, Lcom/google/android/videos/remote/RemoteTracker;->isPlayingAnyVideo()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/videos/remote/RemoteTracker;->getVideoInfo()Lcom/google/android/videos/remote/RemoteVideoInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/videos/remote/RemoteVideoInfo;->videoId:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isPlayingAnyVideo()Z
    .locals 4

    .prologue
    .line 239
    invoke-virtual {p0}, Lcom/google/android/videos/remote/RemoteTracker;->getPlayerState()Lcom/google/android/videos/remote/PlayerState;

    move-result-object v0

    .line 240
    .local v0, "playerState":Lcom/google/android/videos/remote/PlayerState;
    invoke-virtual {p0}, Lcom/google/android/videos/remote/RemoteTracker;->getVideoInfo()Lcom/google/android/videos/remote/RemoteVideoInfo;

    move-result-object v1

    .line 241
    .local v1, "videoInfo":Lcom/google/android/videos/remote/RemoteVideoInfo;
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    iget v2, v0, Lcom/google/android/videos/remote/PlayerState;->state:I

    if-eqz v2, :cond_0

    iget v2, v0, Lcom/google/android/videos/remote/PlayerState;->state:I

    const/4 v3, 0x5

    if-eq v2, v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isPlayingLocallyOwnedVideo()Z
    .locals 1

    .prologue
    .line 225
    invoke-virtual {p0}, Lcom/google/android/videos/remote/RemoteTracker;->isPlayingAnyVideo()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/videos/remote/RemoteTracker;->getAccountName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onImageChanged(Lcom/google/android/videos/ui/ArtworkMonitor;Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "monitor"    # Lcom/google/android/videos/ui/ArtworkMonitor;
    .param p2, "image"    # Landroid/graphics/Bitmap;

    .prologue
    .line 469
    invoke-static {}, Lcom/google/android/videos/utils/Preconditions;->checkMainThread()V

    .line 470
    iget-object v2, p0, Lcom/google/android/videos/remote/RemoteTracker;->posterBitmap:Landroid/graphics/Bitmap;

    if-ne v2, p2, :cond_1

    .line 478
    :cond_0
    return-void

    .line 474
    :cond_1
    iput-object p2, p0, Lcom/google/android/videos/remote/RemoteTracker;->posterBitmap:Landroid/graphics/Bitmap;

    .line 475
    iget-object v2, p0, Lcom/google/android/videos/remote/RemoteTracker;->transportControls:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/remote/TransportControl;

    .line 476
    .local v1, "transportControl":Lcom/google/android/videos/remote/TransportControl;
    invoke-virtual {v1}, Lcom/google/android/videos/remote/TransportControl;->onPosterBitmapChanged()V

    goto :goto_0
.end method

.method public registerCustomTransportControl(Lcom/google/android/videos/remote/TransportControl;)V
    .locals 1
    .param p1, "transportControl"    # Lcom/google/android/videos/remote/TransportControl;

    .prologue
    .line 100
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->transportControls:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    return-void
.end method

.method public unregisterCustomTransportControl(Lcom/google/android/videos/remote/TransportControl;)V
    .locals 1
    .param p1, "transportControl"    # Lcom/google/android/videos/remote/TransportControl;

    .prologue
    .line 109
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker;->transportControls:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 111
    return-void
.end method
