.class Lcom/google/android/videos/ui/EidrIdConverterHelper$2;
.super Ljava/lang/Object;
.source "EidrIdConverterHelper.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/ui/EidrIdConverterHelper;->syncPurchasesForEidrId()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/ui/EidrIdConverterHelper;


# direct methods
.method constructor <init>(Lcom/google/android/videos/ui/EidrIdConverterHelper;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper$2;->this$0:Lcom/google/android/videos/ui/EidrIdConverterHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Ljava/lang/Exception;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unable to sync "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->eidrId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 138
    iget-object v0, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper$2;->this$0:Lcom/google/android/videos/ui/EidrIdConverterHelper;

    # invokes: Lcom/google/android/videos/ui/EidrIdConverterHelper;->onFailed()V
    invoke-static {v0}, Lcom/google/android/videos/ui/EidrIdConverterHelper;->access$300(Lcom/google/android/videos/ui/EidrIdConverterHelper;)V

    .line 139
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 128
    check-cast p1, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/EidrIdConverterHelper$2;->onError(Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Ljava/lang/Void;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;
    .param p2, "response"    # Ljava/lang/Void;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper$2;->this$0:Lcom/google/android/videos/ui/EidrIdConverterHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/EidrIdConverterHelper;->convert(I)V

    .line 133
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 128
    check-cast p1, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/lang/Void;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/EidrIdConverterHelper$2;->onResponse(Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Ljava/lang/Void;)V

    return-void
.end method
