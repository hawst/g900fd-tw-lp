.class public final Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequest;
.super Lcom/google/android/videos/async/Request;
.source "DefaultKnowledgeRequesters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TagStreamHttpRequest"
.end annotation


# instance fields
.field public final ifModifiedSince:Ljava/lang/String;

.field public final url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "ifModifiedSince"    # Ljava/lang/String;

    .prologue
    .line 100
    invoke-direct {p0, p1}, Lcom/google/android/videos/async/Request;-><init>(Ljava/lang/String;)V

    .line 101
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequest;->url:Ljava/lang/String;

    .line 102
    iput-object p3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequest;->ifModifiedSince:Ljava/lang/String;

    .line 103
    return-void
.end method
