.class Lcom/google/android/videos/activity/AssetDetailsActivity$StatusBarRecolorListener;
.super Ljava/lang/Object;
.source "AssetDetailsActivity.java"

# interfaces
.implements Landroid/transition/Transition$TransitionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/activity/AssetDetailsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "StatusBarRecolorListener"
.end annotation


# instance fields
.field private final animator:Landroid/animation/ObjectAnimator;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/ui/StatusBarDrawerLayout;I)V
    .locals 3
    .param p1, "drawerLayout"    # Lcom/google/android/videos/ui/StatusBarDrawerLayout;
    .param p2, "endColor"    # I

    .prologue
    .line 546
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 547
    sget-object v0, Lcom/google/android/videos/ui/StatusBarDrawerLayout;->STATUS_BAR_BACKGROUND_COLOR:Landroid/util/Property;

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    aput p2, v1, v2

    invoke-static {p1, v0, v1}, Landroid/animation/ObjectAnimator;->ofArgb(Ljava/lang/Object;Landroid/util/Property;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$StatusBarRecolorListener;->animator:Landroid/animation/ObjectAnimator;

    .line 549
    return-void
.end method

.method public constructor <init>(Lcom/google/android/videos/ui/StatusBarDrawerLayout;II)V
    .locals 3
    .param p1, "drawerLayout"    # Lcom/google/android/videos/ui/StatusBarDrawerLayout;
    .param p2, "startColor"    # I
    .param p3, "endColor"    # I

    .prologue
    .line 555
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 557
    sget-object v0, Lcom/google/android/videos/ui/StatusBarDrawerLayout;->STATUS_BAR_BACKGROUND_COLOR:Landroid/util/Property;

    const/4 v1, 0x2

    new-array v1, v1, [I

    const/4 v2, 0x0

    aput p2, v1, v2

    const/4 v2, 0x1

    aput p3, v1, v2

    invoke-static {p1, v0, v1}, Landroid/animation/ObjectAnimator;->ofArgb(Ljava/lang/Object;Landroid/util/Property;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$StatusBarRecolorListener;->animator:Landroid/animation/ObjectAnimator;

    .line 559
    return-void
.end method


# virtual methods
.method public onTransitionCancel(Landroid/transition/Transition;)V
    .locals 1
    .param p1, "transition"    # Landroid/transition/Transition;

    .prologue
    .line 573
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$StatusBarRecolorListener;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 574
    return-void
.end method

.method public onTransitionEnd(Landroid/transition/Transition;)V
    .locals 1
    .param p1, "transition"    # Landroid/transition/Transition;

    .prologue
    .line 568
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$StatusBarRecolorListener;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->end()V

    .line 569
    return-void
.end method

.method public onTransitionPause(Landroid/transition/Transition;)V
    .locals 1
    .param p1, "transition"    # Landroid/transition/Transition;

    .prologue
    .line 578
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$StatusBarRecolorListener;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->pause()V

    .line 579
    return-void
.end method

.method public onTransitionResume(Landroid/transition/Transition;)V
    .locals 1
    .param p1, "transition"    # Landroid/transition/Transition;

    .prologue
    .line 583
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$StatusBarRecolorListener;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->resume()V

    .line 584
    return-void
.end method

.method public onTransitionStart(Landroid/transition/Transition;)V
    .locals 1
    .param p1, "transition"    # Landroid/transition/Transition;

    .prologue
    .line 563
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$StatusBarRecolorListener;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 564
    return-void
.end method
