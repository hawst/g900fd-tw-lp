.class Lcom/google/android/videos/ui/PinHelper$SettingAdapter;
.super Landroid/widget/ArrayAdapter;
.source "PinHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/PinHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SettingAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private final preferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/content/SharedPreferences;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "preferences"    # Landroid/content/SharedPreferences;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/content/SharedPreferences;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 647
    .local p3, "settingFlags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const v0, 0x7f040035

    invoke-direct {p0, p1, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 648
    iput-object p2, p0, Lcom/google/android/videos/ui/PinHelper$SettingAdapter;->preferences:Landroid/content/SharedPreferences;

    .line 649
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 653
    move-object v4, p2

    .line 654
    .local v4, "view":Landroid/view/View;
    if-nez v4, :cond_0

    .line 655
    invoke-virtual {p0}, Lcom/google/android/videos/ui/PinHelper$SettingAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 656
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f040035

    const/4 v6, 0x0

    invoke-virtual {v2, v5, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 659
    .end local v2    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/ui/PinHelper$SettingAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 660
    .local v0, "context":Landroid/content/Context;
    const v5, 0x7f0f00fd

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 661
    .local v1, "headerView":Landroid/widget/TextView;
    const v5, 0x7f0f00fe

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 662
    .local v3, "settingView":Landroid/widget/TextView;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/ui/PinHelper$SettingAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 679
    :goto_0
    :pswitch_0
    return-object v4

    .line 664
    :pswitch_1
    const v5, 0x7f0b021d

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    .line 665
    iget-object v5, p0, Lcom/google/android/videos/ui/PinHelper$SettingAdapter;->preferences:Landroid/content/SharedPreferences;

    invoke-static {v0, v5}, Lcom/google/android/videos/utils/SettingsUtil;->getDownloadNetworkPreferenceText(Landroid/content/Context;Landroid/content/SharedPreferences;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 669
    :pswitch_2
    const v5, 0x7f0b0220

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    .line 670
    iget-object v5, p0, Lcom/google/android/videos/ui/PinHelper$SettingAdapter;->preferences:Landroid/content/SharedPreferences;

    invoke-static {v0, v5}, Lcom/google/android/videos/utils/SettingsUtil;->getDownloadQualityPreferenceText(Landroid/content/Context;Landroid/content/SharedPreferences;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 674
    :pswitch_3
    const v5, 0x7f0b0236

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    .line 675
    iget-object v5, p0, Lcom/google/android/videos/ui/PinHelper$SettingAdapter;->preferences:Landroid/content/SharedPreferences;

    invoke-static {v0, v5}, Lcom/google/android/videos/utils/SettingsUtil;->getAudioPreferenceText(Landroid/content/Context;Landroid/content/SharedPreferences;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 662
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
