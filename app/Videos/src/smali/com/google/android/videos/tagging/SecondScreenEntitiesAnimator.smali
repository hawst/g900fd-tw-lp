.class public Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;
.super Lcom/google/android/videos/ui/DefaultItemAnimator;
.source "SecondScreenEntitiesAnimator.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/ui/DefaultItemAnimator",
        "<",
        "Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/videos/ui/DefaultItemAnimator;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;
    .param p1, "x1"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->onRemoveAnimationEnd(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;
    .param p1, "x1"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->onAddAnimationEnd(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;Landroid/support/v7/widget/RecyclerView$ViewHolder;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;
    .param p1, "x1"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "x2"    # Z

    .prologue
    .line 21
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->onChangeAnimationEnd(Landroid/support/v7/widget/RecyclerView$ViewHolder;Z)V

    return-void
.end method

.method private setScale(Landroid/view/View;FF)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "scaleX"    # F
    .param p3, "scaleY"    # F

    .prologue
    .line 170
    invoke-static {p1, p2}, Landroid/support/v4/view/ViewCompat;->setScaleX(Landroid/view/View;F)V

    .line 171
    invoke-static {p1, p3}, Landroid/support/v4/view/ViewCompat;->setScaleY(Landroid/view/View;F)V

    .line 172
    return-void
.end method

.method private startScaleChange(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;Z)V
    .locals 4
    .param p1, "holder"    # Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;
    .param p2, "isOld"    # Z

    .prologue
    .line 126
    iget-object v1, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->imageFrame:Landroid/widget/FrameLayout;

    invoke-static {v1}, Landroid/support/v4/view/ViewCompat;->animate(Landroid/view/View;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    .line 127
    .local v0, "scaleAnimator":Landroid/support/v4/view/ViewPropertyAnimatorCompat;
    iget v1, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->targetScale:F

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->scaleX(F)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v1

    iget v2, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->targetScale:F

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->scaleY(F)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->getChangeDuration()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setDuration(J)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    .line 129
    new-instance v1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$4;

    invoke-direct {v1, p0, p1, p2, v0}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$4;-><init>(Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;ZLandroid/support/v4/view/ViewPropertyAnimatorCompat;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setListener(Landroid/support/v4/view/ViewPropertyAnimatorListener;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    .line 142
    invoke-virtual {v0}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->start()V

    .line 143
    return-void
.end method

.method private startTranslationChange(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;II)V
    .locals 4
    .param p1, "holder"    # Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;
    .param p2, "translationX"    # I
    .param p3, "translationY"    # I

    .prologue
    .line 111
    iget-object v1, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->itemView:Landroid/view/View;

    invoke-static {v1}, Landroid/support/v4/view/ViewCompat;->animate(Landroid/view/View;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    .line 112
    .local v0, "translationAnimator":Landroid/support/v4/view/ViewPropertyAnimatorCompat;
    int-to-float v1, p2

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->translationX(F)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v1

    int-to-float v2, p3

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->translationY(F)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->getChangeDuration()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setDuration(J)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    .line 114
    new-instance v1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$3;

    invoke-direct {v1, p0, v0, p1}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$3;-><init>(Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;Landroid/support/v4/view/ViewPropertyAnimatorCompat;Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setListener(Landroid/support/v4/view/ViewPropertyAnimatorListener;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    .line 122
    invoke-virtual {v0}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->start()V

    .line 123
    return-void
.end method


# virtual methods
.method public endAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 164
    move-object v0, p1

    check-cast v0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    .line 165
    .local v0, "entityViewHolder":Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;
    iget-object v1, v0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->imageFrame:Landroid/widget/FrameLayout;

    invoke-static {v1}, Landroid/support/v4/view/ViewCompat;->animate(Landroid/view/View;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->cancel()V

    .line 166
    invoke-super {p0, p1}, Lcom/google/android/videos/ui/DefaultItemAnimator;->endAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 167
    return-void
.end method

.method protected bridge synthetic prepareAddAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 21
    check-cast p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    .end local p1    # "x0":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->prepareAddAnimation(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;)V

    return-void
.end method

.method protected prepareAddAnimation(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;)V
    .locals 2
    .param p1, "holder"    # Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    .prologue
    const/4 v1, 0x0

    .line 44
    iget-object v0, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->imageFrame:Landroid/widget/FrameLayout;

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->setScale(Landroid/view/View;FF)V

    .line 45
    return-void
.end method

.method protected bridge synthetic prepareChangeAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;Landroid/support/v7/widget/RecyclerView$ViewHolder;IIII)V
    .locals 7
    .param p1, "x0"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "x1"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p3, "x2"    # I
    .param p4, "x3"    # I
    .param p5, "x4"    # I
    .param p6, "x5"    # I

    .prologue
    .line 21
    move-object v1, p1

    check-cast v1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    move-object v2, p2

    check-cast v2, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    move-object v0, p0

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->prepareChangeAnimation(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;IIII)V

    return-void
.end method

.method protected prepareChangeAnimation(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;IIII)V
    .locals 10
    .param p1, "oldHolder"    # Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;
    .param p2, "newHolder"    # Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;
    .param p3, "fromX"    # I
    .param p4, "fromY"    # I
    .param p5, "toX"    # I
    .param p6, "toY"    # I

    .prologue
    .line 69
    iget-object v8, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->itemView:Landroid/view/View;

    invoke-static {v8}, Landroid/support/v4/view/ViewCompat;->getTranslationX(Landroid/view/View;)F

    move-result v6

    .line 70
    .local v6, "prevTranslationX":F
    iget-object v8, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->itemView:Landroid/view/View;

    invoke-static {v8}, Landroid/support/v4/view/ViewCompat;->getTranslationY(Landroid/view/View;)F

    move-result v7

    .line 71
    .local v7, "prevTranslationY":F
    move-object v3, p1

    .line 72
    .local v3, "oldEntityViewHolder":Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;
    move-object v2, p2

    .line 73
    .local v2, "newEntityViewHolder":Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;
    iget-object v8, v3, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->imageFrame:Landroid/widget/FrameLayout;

    invoke-static {v8}, Landroid/support/v4/view/ViewCompat;->getScaleX(Landroid/view/View;)F

    move-result v4

    .line 74
    .local v4, "prevScaleX":F
    iget-object v8, v3, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->imageFrame:Landroid/widget/FrameLayout;

    invoke-static {v8}, Landroid/support/v4/view/ViewCompat;->getScaleY(Landroid/view/View;)F

    move-result v5

    .line 75
    .local v5, "prevScaleY":F
    invoke-virtual {p0, p1}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->endAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 76
    sub-int v8, p5, p3

    int-to-float v8, v8

    sub-float/2addr v8, v6

    float-to-int v0, v8

    .line 77
    .local v0, "deltaX":I
    sub-int v8, p6, p4

    int-to-float v8, v8

    sub-float/2addr v8, v7

    float-to-int v1, v8

    .line 79
    .local v1, "deltaY":I
    iget-object v8, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->itemView:Landroid/view/View;

    invoke-static {v8, v6}, Landroid/support/v4/view/ViewCompat;->setTranslationX(Landroid/view/View;F)V

    .line 80
    iget-object v8, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->itemView:Landroid/view/View;

    invoke-static {v8, v7}, Landroid/support/v4/view/ViewCompat;->setTranslationY(Landroid/view/View;F)V

    .line 81
    iget-object v8, v3, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->imageFrame:Landroid/widget/FrameLayout;

    invoke-direct {p0, v8, v4, v5}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->setScale(Landroid/view/View;FF)V

    .line 82
    if-eqz p2, :cond_0

    iget-object v8, p2, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->itemView:Landroid/view/View;

    if-eqz v8, :cond_0

    .line 84
    invoke-virtual {p0, p2}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->endAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 85
    iget-object v8, p2, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->itemView:Landroid/view/View;

    neg-int v9, v0

    int-to-float v9, v9

    invoke-static {v8, v9}, Landroid/support/v4/view/ViewCompat;->setTranslationX(Landroid/view/View;F)V

    .line 86
    iget-object v8, p2, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->itemView:Landroid/view/View;

    neg-int v9, v1

    int-to-float v9, v9

    invoke-static {v8, v9}, Landroid/support/v4/view/ViewCompat;->setTranslationY(Landroid/view/View;F)V

    .line 87
    iget-object v8, v2, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->imageFrame:Landroid/widget/FrameLayout;

    invoke-direct {p0, v8, v4, v5}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->setScale(Landroid/view/View;FF)V

    .line 89
    :cond_0
    return-void
.end method

.method protected bridge synthetic runAddAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 21
    check-cast p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    .end local p1    # "x0":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->runAddAnimation(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;)V

    return-void
.end method

.method protected runAddAnimation(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;)V
    .locals 6
    .param p1, "holder"    # Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 49
    iget-object v1, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->imageFrame:Landroid/widget/FrameLayout;

    .line 50
    .local v1, "view":Landroid/view/View;
    invoke-static {v1}, Landroid/support/v4/view/ViewCompat;->animate(Landroid/view/View;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    .line 51
    .local v0, "animation":Landroid/support/v4/view/ViewPropertyAnimatorCompat;
    invoke-virtual {v0, v3}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->scaleX(F)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->scaleY(F)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->getAddDuration()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setDuration(J)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    .line 52
    new-instance v2, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$2;

    invoke-direct {v2, p0, p1, v0}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$2;-><init>(Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;Landroid/support/v4/view/ViewPropertyAnimatorCompat;)V

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setListener(Landroid/support/v4/view/ViewPropertyAnimatorListener;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->start()V

    .line 64
    return-void
.end method

.method protected runNewViewChangeAnimation(Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo",
            "<",
            "Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "changeInfo":Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;, "Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo<Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;>;"
    const/4 v1, 0x0

    .line 105
    iget-object v0, p1, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;->newHolder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    check-cast v0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->startTranslationChange(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;II)V

    .line 106
    iget-object v0, p1, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;->newHolder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    check-cast v0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->startScaleChange(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;Z)V

    .line 107
    return-void
.end method

.method protected runOldViewChangeAnimation(Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo",
            "<",
            "Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "changeInfo":Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;, "Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo<Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;>;"
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 93
    iget-object v0, p1, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;->newHolder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p1, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;->oldHolder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    invoke-virtual {p0, v0, v2}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->onChangeAnimationEnd(Landroid/support/v7/widget/RecyclerView$ViewHolder;Z)V

    .line 101
    :goto_0
    return-void

    .line 98
    :cond_0
    iget-object v0, p1, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;->oldHolder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    check-cast v0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->startTranslationChange(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;II)V

    .line 99
    iget-object v0, p1, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;->oldHolder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    check-cast v0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    invoke-direct {p0, v0, v2}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->startScaleChange(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;Z)V

    goto :goto_0
.end method

.method protected bridge synthetic runRemoveAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 21
    check-cast p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    .end local p1    # "x0":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->runRemoveAnimation(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;)V

    return-void
.end method

.method protected runRemoveAnimation(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;)V
    .locals 5
    .param p1, "holder"    # Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    .prologue
    const/4 v4, 0x0

    .line 25
    iget-object v1, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->imageFrame:Landroid/widget/FrameLayout;

    .line 26
    .local v1, "view":Landroid/view/View;
    invoke-static {v1}, Landroid/support/v4/view/ViewCompat;->animate(Landroid/view/View;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    .line 27
    .local v0, "animation":Landroid/support/v4/view/ViewPropertyAnimatorCompat;
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->getRemoveDuration()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setDuration(J)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->scaleX(F)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->scaleY(F)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    .line 28
    new-instance v2, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$1;

    invoke-direct {v2, p0, p1, v0}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$1;-><init>(Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;Landroid/support/v4/view/ViewPropertyAnimatorCompat;)V

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setListener(Landroid/support/v4/view/ViewPropertyAnimatorListener;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->start()V

    .line 40
    return-void
.end method

.method protected bridge synthetic setViewStateAfterEndAdditionAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 21
    check-cast p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    .end local p1    # "x0":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->setViewStateAfterEndAdditionAnimation(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;)V

    return-void
.end method

.method protected setViewStateAfterEndAdditionAnimation(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;)V
    .locals 2
    .param p1, "holder"    # Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 147
    iget-object v0, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->imageFrame:Landroid/widget/FrameLayout;

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->setScale(Landroid/view/View;FF)V

    .line 148
    return-void
.end method

.method protected bridge synthetic setViewStateAfterEndChangeAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 21
    check-cast p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    .end local p1    # "x0":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->setViewStateAfterEndChangeAnimation(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;)V

    return-void
.end method

.method protected setViewStateAfterEndChangeAnimation(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;)V
    .locals 4
    .param p1, "item"    # Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    .prologue
    const/4 v3, 0x0

    .line 157
    iget-object v0, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->imageFrame:Landroid/widget/FrameLayout;

    iget v1, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->targetScale:F

    iget v2, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->targetScale:F

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->setScale(Landroid/view/View;FF)V

    .line 158
    iget-object v0, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->itemView:Landroid/view/View;

    invoke-static {v0, v3}, Landroid/support/v4/view/ViewCompat;->setTranslationX(Landroid/view/View;F)V

    .line 159
    iget-object v0, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->itemView:Landroid/view/View;

    invoke-static {v0, v3}, Landroid/support/v4/view/ViewCompat;->setTranslationY(Landroid/view/View;F)V

    .line 160
    return-void
.end method

.method protected bridge synthetic setViewStateAfterEndRemoveAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 21
    check-cast p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    .end local p1    # "x0":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->setViewStateAfterEndRemoveAnimation(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;)V

    return-void
.end method

.method protected setViewStateAfterEndRemoveAnimation(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;)V
    .locals 2
    .param p1, "holder"    # Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    .prologue
    const/4 v1, 0x0

    .line 152
    iget-object v0, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->imageFrame:Landroid/widget/FrameLayout;

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->setScale(Landroid/view/View;FF)V

    .line 153
    return-void
.end method
