.class public Lcom/google/android/videos/ui/playnext/ShowClusterItemView$Binder;
.super Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;
.source "ShowClusterItemView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/playnext/ShowClusterItemView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Binder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder",
        "<",
        "Lcom/google/android/videos/ui/playnext/ShowClusterItemView;",
        "Lcom/google/android/videos/adapter/ShowsDataSource;",
        ">;"
    }
.end annotation


# instance fields
.field private final posterRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/logging/UiElementNode;)V
    .locals 1
    .param p2, "clusterUiElementNode"    # Lcom/google/android/videos/logging/UiElementNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/google/android/videos/logging/UiElementNode;",
            ")V"
        }
    .end annotation

    .prologue
    .line 83
    .local p1, "posterRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;-><init>(Lcom/google/android/videos/logging/UiElementNode;I)V

    .line 84
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/ShowClusterItemView$Binder;->posterRequester:Lcom/google/android/videos/async/Requester;

    .line 85
    return-void
.end method


# virtual methods
.method protected bridge synthetic onBind(Lcom/google/android/videos/ui/playnext/ClusterItemView;Lcom/google/android/videos/adapter/DataSource;I)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/ui/playnext/ClusterItemView;
    .param p2, "x1"    # Lcom/google/android/videos/adapter/DataSource;
    .param p3, "x2"    # I

    .prologue
    .line 77
    check-cast p1, Lcom/google/android/videos/ui/playnext/ShowClusterItemView;

    .end local p1    # "x0":Lcom/google/android/videos/ui/playnext/ClusterItemView;
    check-cast p2, Lcom/google/android/videos/adapter/ShowsDataSource;

    .end local p2    # "x1":Lcom/google/android/videos/adapter/DataSource;
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/videos/ui/playnext/ShowClusterItemView$Binder;->onBind(Lcom/google/android/videos/ui/playnext/ShowClusterItemView;Lcom/google/android/videos/adapter/ShowsDataSource;I)V

    return-void
.end method

.method protected onBind(Lcom/google/android/videos/ui/playnext/ShowClusterItemView;Lcom/google/android/videos/adapter/ShowsDataSource;I)V
    .locals 3
    .param p1, "view"    # Lcom/google/android/videos/ui/playnext/ShowClusterItemView;
    .param p2, "dataSource"    # Lcom/google/android/videos/adapter/ShowsDataSource;
    .param p3, "index"    # I

    .prologue
    .line 89
    invoke-virtual {p2, p3}, Lcom/google/android/videos/adapter/ShowsDataSource;->getItem(I)Landroid/database/Cursor;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/ShowClusterItemView$Binder;->posterRequester:Lcom/google/android/videos/async/Requester;

    invoke-virtual {p2}, Lcom/google/android/videos/adapter/ShowsDataSource;->isNetworkConnected()Z

    move-result v2

    invoke-virtual {p1, v0, v1, v2, p0}, Lcom/google/android/videos/ui/playnext/ShowClusterItemView;->bind(Landroid/database/Cursor;Lcom/google/android/videos/async/Requester;ZLcom/google/android/videos/ui/playnext/ShowClusterItemView$Binder;)V

    .line 90
    return-void
.end method
