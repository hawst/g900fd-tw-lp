.class Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;
.super Ljava/lang/Object;
.source "MovieDetailsRowHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OwnershipInfo"
.end annotation


# static fields
.field public static final NOT_OWNED:Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;


# instance fields
.field public final expirationTimestamp:J

.field public final preorder:Z

.field public final purchased:Z

.field public final resumeTimeMillis:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 293
    new-instance v1, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;

    const-wide v4, 0x7fffffffffffffffL

    move v3, v2

    move v6, v2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;-><init>(ZIJZ)V

    sput-object v1, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;->NOT_OWNED:Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;

    return-void
.end method

.method public constructor <init>(ZIJZ)V
    .locals 1
    .param p1, "purchased"    # Z
    .param p2, "resumeTimeMillis"    # I
    .param p3, "expirationTimestamp"    # J
    .param p5, "preorder"    # Z

    .prologue
    .line 302
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 303
    iput-boolean p1, p0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;->purchased:Z

    .line 304
    iput p2, p0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;->resumeTimeMillis:I

    .line 305
    iput-wide p3, p0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;->expirationTimestamp:J

    .line 306
    iput-boolean p5, p0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;->preorder:Z

    .line 307
    return-void
.end method
