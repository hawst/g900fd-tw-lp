.class Lcom/google/android/videos/tagging/Tag$ShapelessTag;
.super Lcom/google/android/videos/tagging/Tag;
.source "Tag.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/Tag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ShapelessTag"
.end annotation


# direct methods
.method private constructor <init>(IIZ)V
    .locals 1
    .param p1, "splitId"    # I
    .param p2, "timeMillis"    # I
    .param p3, "interpolates"    # Z

    .prologue
    .line 179
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/videos/tagging/Tag;-><init>(IIZLcom/google/android/videos/tagging/Tag$1;)V

    .line 180
    return-void
.end method

.method synthetic constructor <init>(IIZLcom/google/android/videos/tagging/Tag$1;)V
    .locals 0
    .param p1, "x0"    # I
    .param p2, "x1"    # I
    .param p3, "x2"    # Z
    .param p4, "x3"    # Lcom/google/android/videos/tagging/Tag$1;

    .prologue
    .line 176
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/tagging/Tag$ShapelessTag;-><init>(IIZ)V

    return-void
.end method


# virtual methods
.method public getTagShape(IFFF)Lcom/google/android/videos/tagging/Tag$TagShape;
    .locals 1
    .param p1, "atMillis"    # I
    .param p2, "width"    # F
    .param p3, "height"    # F
    .param p4, "minArea"    # F

    .prologue
    .line 184
    const/4 v0, 0x0

    return-object v0
.end method
