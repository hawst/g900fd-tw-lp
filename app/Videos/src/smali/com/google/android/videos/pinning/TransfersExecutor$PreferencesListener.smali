.class Lcom/google/android/videos/pinning/TransfersExecutor$PreferencesListener;
.super Ljava/lang/Object;
.source "TransfersExecutor.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pinning/TransfersExecutor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PreferencesListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pinning/TransfersExecutor;

.field private volatile transferOnWifiOnly:Z


# direct methods
.method private constructor <init>(Lcom/google/android/videos/pinning/TransfersExecutor;)V
    .locals 0

    .prologue
    .line 985
    iput-object p1, p0, Lcom/google/android/videos/pinning/TransfersExecutor$PreferencesListener;->this$0:Lcom/google/android/videos/pinning/TransfersExecutor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/pinning/TransfersExecutor;Lcom/google/android/videos/pinning/TransfersExecutor$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/pinning/TransfersExecutor;
    .param p2, "x1"    # Lcom/google/android/videos/pinning/TransfersExecutor$1;

    .prologue
    .line 985
    invoke-direct {p0, p1}, Lcom/google/android/videos/pinning/TransfersExecutor$PreferencesListener;-><init>(Lcom/google/android/videos/pinning/TransfersExecutor;)V

    return-void
.end method


# virtual methods
.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 3
    .param p1, "preferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 1002
    const-string v1, "download_policy"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1003
    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor$PreferencesListener;->this$0:Lcom/google/android/videos/pinning/TransfersExecutor;

    # getter for: Lcom/google/android/videos/pinning/TransfersExecutor;->context:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/videos/pinning/TransfersExecutor;->access$200(Lcom/google/android/videos/pinning/TransfersExecutor;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/pinning/TransfersExecutor$PreferencesListener;->this$0:Lcom/google/android/videos/pinning/TransfersExecutor;

    # getter for: Lcom/google/android/videos/pinning/TransfersExecutor;->videosGlobals:Lcom/google/android/videos/VideosGlobals;
    invoke-static {v2}, Lcom/google/android/videos/pinning/TransfersExecutor;->access$100(Lcom/google/android/videos/pinning/TransfersExecutor;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/videos/utils/SettingsUtil;->getDownloadOnWiFiOnly(Landroid/content/Context;Landroid/content/SharedPreferences;)Z

    move-result v0

    .line 1005
    .local v0, "transferOnWifiOnly":Z
    iget-boolean v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor$PreferencesListener;->transferOnWifiOnly:Z

    if-eq v1, v0, :cond_0

    .line 1006
    iput-boolean v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor$PreferencesListener;->transferOnWifiOnly:Z

    .line 1007
    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor$PreferencesListener;->this$0:Lcom/google/android/videos/pinning/TransfersExecutor;

    # invokes: Lcom/google/android/videos/pinning/TransfersExecutor;->pingUnlessIdle()V
    invoke-static {v1}, Lcom/google/android/videos/pinning/TransfersExecutor;->access$300(Lcom/google/android/videos/pinning/TransfersExecutor;)V

    .line 1010
    .end local v0    # "transferOnWifiOnly":Z
    :cond_0
    return-void
.end method

.method public register()V
    .locals 2

    .prologue
    .line 990
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor$PreferencesListener;->this$0:Lcom/google/android/videos/pinning/TransfersExecutor;

    # getter for: Lcom/google/android/videos/pinning/TransfersExecutor;->videosGlobals:Lcom/google/android/videos/VideosGlobals;
    invoke-static {v0}, Lcom/google/android/videos/pinning/TransfersExecutor;->access$100(Lcom/google/android/videos/pinning/TransfersExecutor;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 992
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor$PreferencesListener;->this$0:Lcom/google/android/videos/pinning/TransfersExecutor;

    # getter for: Lcom/google/android/videos/pinning/TransfersExecutor;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/videos/pinning/TransfersExecutor;->access$200(Lcom/google/android/videos/pinning/TransfersExecutor;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/pinning/TransfersExecutor$PreferencesListener;->this$0:Lcom/google/android/videos/pinning/TransfersExecutor;

    # getter for: Lcom/google/android/videos/pinning/TransfersExecutor;->videosGlobals:Lcom/google/android/videos/VideosGlobals;
    invoke-static {v1}, Lcom/google/android/videos/pinning/TransfersExecutor;->access$100(Lcom/google/android/videos/pinning/TransfersExecutor;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/SettingsUtil;->getDownloadOnWiFiOnly(Landroid/content/Context;Landroid/content/SharedPreferences;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor$PreferencesListener;->transferOnWifiOnly:Z

    .line 994
    return-void
.end method

.method public unregister()V
    .locals 1

    .prologue
    .line 997
    iget-object v0, p0, Lcom/google/android/videos/pinning/TransfersExecutor$PreferencesListener;->this$0:Lcom/google/android/videos/pinning/TransfersExecutor;

    # getter for: Lcom/google/android/videos/pinning/TransfersExecutor;->videosGlobals:Lcom/google/android/videos/VideosGlobals;
    invoke-static {v0}, Lcom/google/android/videos/pinning/TransfersExecutor;->access$100(Lcom/google/android/videos/pinning/TransfersExecutor;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 998
    return-void
.end method
