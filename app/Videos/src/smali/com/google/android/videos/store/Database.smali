.class public Lcom/google/android/videos/store/Database;
.super Ljava/lang/Object;
.source "Database.java"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Lcom/google/android/repolib/observers/Observable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/store/Database$BaseListener;,
        Lcom/google/android/videos/store/Database$Listener;
    }
.end annotation


# instance fields
.field private final dbHelper:Lcom/google/android/videos/store/DatabaseHelper;

.field private final listeners:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet",
            "<",
            "Lcom/google/android/videos/store/Database$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private final notificationHandler:Landroid/os/Handler;

.field private final updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Looper;Lcom/google/android/videos/logging/EventLogger;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "database"    # Ljava/lang/String;
    .param p3, "notificationLooper"    # Landroid/os/Looper;
    .param p4, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    invoke-static {}, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->asyncUpdateDispatcher()Lcom/google/android/repolib/observers/UpdateDispatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/store/Database;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    .line 147
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/store/Database;->listeners:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 152
    new-instance v0, Lcom/google/android/videos/store/DatabaseHelper;

    invoke-direct {v0, p1, p2, p4}, Lcom/google/android/videos/store/DatabaseHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/videos/logging/EventLogger;)V

    iput-object v0, p0, Lcom/google/android/videos/store/Database;->dbHelper:Lcom/google/android/videos/store/DatabaseHelper;

    .line 153
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p3, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/videos/store/Database;->notificationHandler:Landroid/os/Handler;

    .line 154
    return-void
.end method


# virtual methods
.method public addListener(Lcom/google/android/videos/store/Database$Listener;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/videos/store/Database$Listener;

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/videos/store/Database;->listeners:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    .line 201
    return-void
.end method

.method public addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/videos/store/Database;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 210
    return-void
.end method

.method analyzeDatabase()V
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/videos/store/Database;->dbHelper:Lcom/google/android/videos/store/DatabaseHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/store/DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "ANALYZE"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 158
    return-void
.end method

.method public beginTransaction()Landroid/database/sqlite/SQLiteDatabase;
    .locals 2

    .prologue
    .line 174
    iget-object v1, p0, Lcom/google/android/videos/store/Database;->dbHelper:Lcom/google/android/videos/store/DatabaseHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/store/DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 175
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 176
    return-object v0
.end method

.method public varargs endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "success"    # Z
    .param p3, "event"    # I
    .param p4, "eventParams"    # [Ljava/lang/Object;

    .prologue
    .line 190
    if-eqz p2, :cond_0

    .line 191
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 193
    :cond_0
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 194
    if-eqz p2, :cond_1

    if-eqz p3, :cond_1

    .line 195
    iget-object v0, p0, Lcom/google/android/videos/store/Database;->notificationHandler:Landroid/os/Handler;

    invoke-virtual {v0, p3, p4}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 197
    :cond_1
    return-void
.end method

.method public getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/videos/store/Database;->dbHelper:Lcom/google/android/videos/store/DatabaseHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/store/DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 220
    iget v0, p1, Landroid/os/Message;->what:I

    .line 221
    .local v0, "event":I
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, [Ljava/lang/Object;

    move-object v3, v4

    check-cast v3, [Ljava/lang/Object;

    .line 222
    .local v3, "params":[Ljava/lang/Object;
    iget-object v4, p0, Lcom/google/android/videos/store/Database;->listeners:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v4}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/store/Database$Listener;

    .line 223
    .local v2, "listener":Lcom/google/android/videos/store/Database$Listener;
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 225
    :pswitch_0
    aget-object v4, v3, v6

    check-cast v4, Ljava/lang/String;

    invoke-interface {v2, v4}, Lcom/google/android/videos/store/Database$Listener;->onPurchasesUpdated(Ljava/lang/String;)V

    goto :goto_0

    .line 228
    :pswitch_1
    aget-object v4, v3, v6

    check-cast v4, Ljava/lang/String;

    invoke-interface {v2, v4}, Lcom/google/android/videos/store/Database$Listener;->onWishlistUpdated(Ljava/lang/String;)V

    goto :goto_0

    .line 231
    :pswitch_2
    aget-object v4, v3, v6

    check-cast v4, Ljava/util/List;

    invoke-interface {v2, v4}, Lcom/google/android/videos/store/Database$Listener;->onMovieMetadataUpdated(Ljava/util/List;)V

    goto :goto_0

    .line 234
    :pswitch_3
    aget-object v4, v3, v6

    check-cast v4, Ljava/lang/String;

    aget-object v5, v3, v7

    check-cast v5, Ljava/util/List;

    invoke-interface {v2, v4, v5}, Lcom/google/android/videos/store/Database$Listener;->onMovieUserAssetsUpdated(Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0

    .line 237
    :pswitch_4
    aget-object v4, v3, v6

    check-cast v4, Ljava/lang/String;

    invoke-interface {v2, v4}, Lcom/google/android/videos/store/Database$Listener;->onShowMetadataUpdated(Ljava/lang/String;)V

    goto :goto_0

    .line 240
    :pswitch_5
    aget-object v4, v3, v6

    check-cast v4, Ljava/lang/String;

    aget-object v5, v3, v7

    check-cast v5, Ljava/lang/String;

    invoke-interface {v2, v4, v5}, Lcom/google/android/videos/store/Database$Listener;->onShowUserAssetsUpdated(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 243
    :pswitch_6
    aget-object v4, v3, v6

    check-cast v4, Ljava/lang/String;

    invoke-interface {v2, v4}, Lcom/google/android/videos/store/Database$Listener;->onPosterUpdated(Ljava/lang/String;)V

    goto :goto_0

    .line 246
    :pswitch_7
    aget-object v4, v3, v6

    check-cast v4, Ljava/lang/String;

    invoke-interface {v2, v4}, Lcom/google/android/videos/store/Database$Listener;->onShowPosterUpdated(Ljava/lang/String;)V

    goto :goto_0

    .line 249
    :pswitch_8
    aget-object v4, v3, v6

    check-cast v4, Ljava/lang/String;

    invoke-interface {v2, v4}, Lcom/google/android/videos/store/Database$Listener;->onScreenshotUpdated(Ljava/lang/String;)V

    goto :goto_0

    .line 252
    :pswitch_9
    aget-object v4, v3, v6

    check-cast v4, Ljava/lang/String;

    invoke-interface {v2, v4}, Lcom/google/android/videos/store/Database$Listener;->onShowBannerUpdated(Ljava/lang/String;)V

    goto :goto_0

    .line 255
    :pswitch_a
    aget-object v4, v3, v6

    check-cast v4, Ljava/lang/String;

    aget-object v5, v3, v7

    check-cast v5, Ljava/lang/String;

    invoke-interface {v2, v4, v5}, Lcom/google/android/videos/store/Database$Listener;->onPinningStateChanged(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 258
    :pswitch_b
    aget-object v4, v3, v6

    check-cast v4, Ljava/lang/String;

    aget-object v5, v3, v7

    check-cast v5, Ljava/lang/String;

    invoke-interface {v2, v4, v5}, Lcom/google/android/videos/store/Database$Listener;->onWatchTimestampsUpdated(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 262
    .end local v2    # "listener":Lcom/google/android/videos/store/Database$Listener;
    :cond_0
    iget-object v4, p0, Lcom/google/android/videos/store/Database;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v4}, Lcom/google/android/repolib/observers/UpdateDispatcher;->update()V

    .line 263
    return v7

    .line 223
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public removeListener(Lcom/google/android/videos/store/Database$Listener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/google/android/videos/store/Database$Listener;

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/videos/store/Database;->listeners:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/videos/store/Database;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 215
    return-void
.end method
