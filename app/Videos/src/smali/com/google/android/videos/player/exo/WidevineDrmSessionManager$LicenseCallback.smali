.class Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$LicenseCallback;
.super Ljava/lang/Object;
.source "WidevineDrmSessionManager.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LicenseCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Ljava/lang/Object;",
        "[B>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;)V
    .locals 0

    .prologue
    .line 245
    iput-object p1, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$LicenseCallback;->this$0:Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;
    .param p2, "x1"    # Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$1;

    .prologue
    .line 245
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$LicenseCallback;-><init>(Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;)V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2
    .param p1, "request"    # Ljava/lang/Object;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 254
    instance-of v1, p2, Lcom/google/android/videos/api/CencLicenseException;

    if-eqz v1, :cond_0

    move-object v1, p2

    check-cast v1, Lcom/google/android/videos/api/CencLicenseException;

    invoke-virtual {v1}, Lcom/google/android/videos/api/CencLicenseException;->failImmediately()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 256
    .local v0, "failImmediately":Z
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$LicenseCallback;->this$0:Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;

    # invokes: Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->onError(Ljava/lang/Exception;Z)V
    invoke-static {v1, p2, v0}, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->access$800(Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;Ljava/lang/Exception;Z)V

    .line 257
    return-void

    .line 254
    .end local v0    # "failImmediately":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 245
    check-cast p2, [B

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$LicenseCallback;->onResponse(Ljava/lang/Object;[B)V

    return-void
.end method

.method public onResponse(Ljava/lang/Object;[B)V
    .locals 1
    .param p1, "request"    # Ljava/lang/Object;
    .param p2, "response"    # [B

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager$LicenseCallback;->this$0:Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;

    # invokes: Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->onLicenseReceived()V
    invoke-static {v0}, Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;->access$900(Lcom/google/android/videos/player/exo/WidevineDrmSessionManager;)V

    .line 250
    return-void
.end method
