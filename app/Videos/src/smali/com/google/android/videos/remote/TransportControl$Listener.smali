.class public interface abstract Lcom/google/android/videos/remote/TransportControl$Listener;
.super Ljava/lang/Object;
.source "TransportControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/remote/TransportControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onDisconnect()V
.end method

.method public abstract onDismiss()V
.end method

.method public abstract onMute()V
.end method

.method public abstract onPause()V
.end method

.method public abstract onPlay()V
.end method

.method public abstract onSeek(I)V
.end method

.method public abstract onSelect()V
.end method

.method public abstract onUnmute()V
.end method
