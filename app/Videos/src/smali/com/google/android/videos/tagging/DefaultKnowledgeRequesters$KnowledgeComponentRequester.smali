.class public final Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;
.super Ljava/lang/Object;
.source "DefaultKnowledgeRequesters.java"

# interfaces
.implements Lcom/google/android/videos/async/Requester;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "KnowledgeComponentRequester"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;",
        "E:",
        "Ljava/lang/Object;",
        "T:",
        "Ljava/lang/Object;",
        "S:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Requester",
        "<TR;TE;>;"
    }
.end annotation


# instance fields
.field private final fileStore:Lcom/google/android/videos/store/AbstractFileStore;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/store/AbstractFileStore",
            "<",
            "Ljava/lang/String;",
            "TS;>;"
        }
    .end annotation
.end field

.field private final removeByPrefix:Z

.field private final requestAfterMillis:J

.field private final requestHandler:Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler",
            "<TR;TE;TT;TS;>;"
        }
    .end annotation
.end field

.field private final targetRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<TT;TS;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/videos/store/AbstractFileStore;ZJLcom/google/android/videos/async/Requester;Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;)V
    .locals 1
    .param p2, "removeByPrefix"    # Z
    .param p3, "requestAfterMillis"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/store/AbstractFileStore",
            "<",
            "Ljava/lang/String;",
            "TS;>;ZJ",
            "Lcom/google/android/videos/async/Requester",
            "<TT;TS;>;",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler",
            "<TR;TE;TT;TS;>;)V"
        }
    .end annotation

    .prologue
    .line 457
    .local p0, "this":Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;, "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester<TR;TE;TT;TS;>;"
    .local p1, "fileStore":Lcom/google/android/videos/store/AbstractFileStore;, "Lcom/google/android/videos/store/AbstractFileStore<Ljava/lang/String;TS;>;"
    .local p5, "targetRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TT;TS;>;"
    .local p6, "requestHandler":Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;, "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler<TR;TE;TT;TS;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 458
    iput-object p1, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->fileStore:Lcom/google/android/videos/store/AbstractFileStore;

    .line 459
    iput-boolean p2, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->removeByPrefix:Z

    .line 460
    iput-wide p3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->requestAfterMillis:J

    .line 461
    iput-object p5, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->targetRequester:Lcom/google/android/videos/async/Requester;

    .line 462
    iput-object p6, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->requestHandler:Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;

    .line 463
    return-void
.end method

.method static synthetic access$300(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;
    .param p1, "x1"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Ljava/lang/Object;
    .param p4, "x4"    # Ljava/lang/Object;
    .param p5, "x5"    # Ljava/lang/Object;
    .param p6, "x6"    # Lcom/google/android/videos/async/Callback;

    .prologue
    .line 438
    invoke-direct/range {p0 .. p6}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->handleResponse(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;ZLjava/lang/Object;Ljava/lang/Exception;Lcom/google/android/videos/async/Callback;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;
    .param p1, "x1"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Ljava/lang/Object;
    .param p4, "x4"    # Ljava/lang/Exception;
    .param p5, "x5"    # Lcom/google/android/videos/async/Callback;

    .prologue
    .line 438
    invoke-direct/range {p0 .. p5}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->handleError(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;ZLjava/lang/Object;Ljava/lang/Exception;Lcom/google/android/videos/async/Callback;)V

    return-void
.end method

.method private handleError(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;ZLjava/lang/Object;Ljava/lang/Exception;Lcom/google/android/videos/async/Callback;)V
    .locals 6
    .param p2, "hadResponse"    # Z
    .param p4, "exception"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;ZTE;",
            "Ljava/lang/Exception;",
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;)V"
        }
    .end annotation

    .prologue
    .line 576
    .local p0, "this":Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;, "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester<TR;TE;TT;TS;>;"
    .local p1, "request":Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;, "TR;"
    .local p3, "oldResponse":Ljava/lang/Object;, "TE;"
    .local p5, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TR;TE;>;"
    const/4 v2, 0x0

    .line 577
    .local v2, "resolved":Z
    const/4 v3, 0x0

    .line 578
    .local v3, "resolvedResponse":Ljava/lang/Object;, "TE;"
    if-eqz p2, :cond_0

    .line 580
    :try_start_0
    iget-object v4, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->requestHandler:Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;

    invoke-virtual {v4, p1, p3, p4}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;->resolveUnderlyingRequestError(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Exception;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 582
    const/4 v2, 0x1

    .line 587
    .end local v3    # "resolvedResponse":Ljava/lang/Object;, "TE;"
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;->toFileName()Ljava/lang/String;

    move-result-object v1

    .line 588
    .local v1, "fileName":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 590
    iget-object v4, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->fileStore:Lcom/google/android/videos/store/AbstractFileStore;

    if-eqz v4, :cond_1

    .line 592
    :try_start_1
    iget-object v4, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->fileStore:Lcom/google/android/videos/store/AbstractFileStore;

    iget v5, p1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;->storage:I

    invoke-virtual {v4, v1, v5}, Lcom/google/android/videos/store/AbstractFileStore;->touch(Ljava/lang/Object;I)V
    :try_end_1
    .catch Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException; {:try_start_1 .. :try_end_1} :catch_1

    .line 599
    :cond_1
    :goto_1
    invoke-interface {p5, p1, v3}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 613
    :goto_2
    return-void

    .line 583
    .end local v1    # "fileName":Ljava/lang/String;
    .restart local v3    # "resolvedResponse":Ljava/lang/Object;, "TE;"
    :catch_0
    move-exception v0

    .line 584
    .local v0, "e":Ljava/lang/Exception;
    move-object p4, v0

    goto :goto_0

    .line 593
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v3    # "resolvedResponse":Ljava/lang/Object;, "TE;"
    .restart local v1    # "fileName":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 594
    .local v0, "e":Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error touching stored response for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " on storage "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;->storage:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 600
    .end local v0    # "e":Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;
    :cond_2
    instance-of v4, p4, Lorg/apache/http/client/HttpResponseException;

    if-eqz v4, :cond_4

    move-object v4, p4

    check-cast v4, Lorg/apache/http/client/HttpResponseException;

    invoke-virtual {v4}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v4

    div-int/lit8 v4, v4, 0x64

    const/4 v5, 0x4

    if-ne v4, v5, :cond_4

    .line 603
    iget-object v4, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->fileStore:Lcom/google/android/videos/store/AbstractFileStore;

    if-eqz v4, :cond_3

    .line 604
    iget v4, p1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;->storage:I

    invoke-direct {p0, v1, v4}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->remove(Ljava/lang/String;I)V

    .line 606
    :cond_3
    invoke-interface {p5, p1, p4}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_2

    .line 607
    :cond_4
    if-eqz p2, :cond_5

    .line 609
    invoke-interface {p5, p1, p3}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_2

    .line 611
    :cond_5
    invoke-interface {p5, p1, p4}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_2
.end method

.method private handleResponse(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    .locals 8
    .param p2, "hadResponse"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;ZTS;TE;TS;",
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;)V"
        }
    .end annotation

    .prologue
    .line 543
    .local p0, "this":Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;, "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester<TR;TE;TT;TS;>;"
    .local p1, "request":Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;, "TR;"
    .local p3, "storedResponse":Ljava/lang/Object;, "TS;"
    .local p4, "oldResponse":Ljava/lang/Object;, "TE;"
    .local p5, "freshResponse":Ljava/lang/Object;, "TS;"
    .local p6, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TR;TE;>;"
    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->requestHandler:Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;

    invoke-virtual {v0, p1, p5}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;->createComponent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/android/videos/converter/ConverterException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v7

    .line 552
    .local v7, "response":Ljava/lang/Object;, "TE;"
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->fileStore:Lcom/google/android/videos/store/AbstractFileStore;

    if-eqz v0, :cond_2

    .line 553
    invoke-virtual {p1}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;->toFileName()Ljava/lang/String;

    move-result-object v6

    .line 555
    .local v6, "fileName":Ljava/lang/String;
    if-eqz p2, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->requestHandler:Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;

    invoke-virtual {v0, p3, p5}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;->shouldUpdate(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 556
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->removeByPrefix:Z

    if-eqz v0, :cond_1

    .line 558
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->fileStore:Lcom/google/android/videos/store/AbstractFileStore;

    iget v1, p1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;->storage:I

    invoke-virtual {v0, v6, v1}, Lcom/google/android/videos/store/AbstractFileStore;->removeByPrefix(Ljava/lang/Object;I)V

    .line 560
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->fileStore:Lcom/google/android/videos/store/AbstractFileStore;

    iget v1, p1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;->storage:I

    invoke-virtual {v0, v6, v1, p5}, Lcom/google/android/videos/store/AbstractFileStore;->put(Ljava/lang/Object;ILjava/lang/Object;)V
    :try_end_1
    .catch Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException; {:try_start_1 .. :try_end_1} :catch_2

    .line 571
    .end local v6    # "fileName":Ljava/lang/String;
    :cond_2
    :goto_0
    invoke-interface {p6, p1, v7}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 572
    .end local v7    # "response":Ljava/lang/Object;, "TE;"
    :goto_1
    return-void

    .line 544
    :catch_0
    move-exception v4

    .local v4, "e":Lcom/google/android/videos/converter/ConverterException;
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p4

    move-object v5, p6

    .line 545
    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->handleError(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;ZLjava/lang/Object;Ljava/lang/Exception;Lcom/google/android/videos/async/Callback;)V

    goto :goto_1

    .line 547
    .end local v4    # "e":Lcom/google/android/videos/converter/ConverterException;
    :catch_1
    move-exception v4

    .local v4, "e":Ljava/io/IOException;
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p4

    move-object v5, p6

    .line 548
    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->handleError(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;ZLjava/lang/Object;Ljava/lang/Exception;Lcom/google/android/videos/async/Callback;)V

    goto :goto_1

    .line 562
    .end local v4    # "e":Ljava/io/IOException;
    .restart local v6    # "fileName":Ljava/lang/String;
    .restart local v7    # "response":Ljava/lang/Object;, "TE;"
    :cond_3
    :try_start_2
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->fileStore:Lcom/google/android/videos/store/AbstractFileStore;

    iget v1, p1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;->storage:I

    invoke-virtual {v0, v6, v1}, Lcom/google/android/videos/store/AbstractFileStore;->touch(Ljava/lang/Object;I)V
    :try_end_2
    .catch Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 564
    :catch_2
    move-exception v4

    .line 565
    .local v4, "e":Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error saving response for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " on storage "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;->storage:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 567
    invoke-interface {p6, p1, v4}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method private remove(Ljava/lang/String;I)V
    .locals 3
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "storage"    # I

    .prologue
    .line 529
    .local p0, "this":Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;, "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester<TR;TE;TT;TS;>;"
    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->removeByPrefix:Z

    if-eqz v1, :cond_0

    .line 530
    iget-object v1, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->fileStore:Lcom/google/android/videos/store/AbstractFileStore;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/videos/store/AbstractFileStore;->removeByPrefix(Ljava/lang/Object;I)V

    .line 537
    :goto_0
    return-void

    .line 532
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->fileStore:Lcom/google/android/videos/store/AbstractFileStore;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/videos/store/AbstractFileStore;->remove(Ljava/lang/Object;I)V
    :try_end_0
    .catch Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 534
    :catch_0
    move-exception v0

    .line 535
    .local v0, "e":Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error deleting file "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from storage "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public request(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;Lcom/google/android/videos/async/Callback;)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;)V"
        }
    .end annotation

    .prologue
    .line 467
    .local p0, "this":Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;, "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester<TR;TE;TT;TS;>;"
    .local p1, "request":Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;, "TR;"
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TR;TE;>;"
    const/4 v11, 0x0

    .line 468
    .local v11, "hasResponse":Z
    const/4 v12, 0x0

    .line 469
    .local v12, "reconstructedResponse":Ljava/lang/Object;, "TE;"
    const/4 v13, 0x0

    .line 470
    .local v13, "storedResponse":Ljava/lang/Object;, "TS;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->fileStore:Lcom/google/android/videos/store/AbstractFileStore;

    if-eqz v2, :cond_3

    .line 471
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;->toFileName()Ljava/lang/String;

    move-result-object v10

    .line 473
    .local v10, "fileName":Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->fileStore:Lcom/google/android/videos/store/AbstractFileStore;

    move-object/from16 v0, p1

    iget v3, v0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;->storage:I

    invoke-virtual {v2, v10, v3}, Lcom/google/android/videos/store/AbstractFileStore;->get(Ljava/lang/Object;I)Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v13

    .line 479
    .end local v13    # "storedResponse":Ljava/lang/Object;, "TS;"
    :goto_0
    if-eqz v13, :cond_0

    .line 481
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->requestHandler:Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0, v13}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;->createComponent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/google/android/videos/converter/ConverterException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v12

    .line 482
    const/4 v11, 0x1

    .line 496
    .end local v12    # "reconstructedResponse":Ljava/lang/Object;, "TE;"
    :cond_0
    :goto_1
    if-eqz v11, :cond_2

    .line 498
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->fileStore:Lcom/google/android/videos/store/AbstractFileStore;

    move-object/from16 v0, p1

    iget v8, v0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;->storage:I

    invoke-virtual {v4, v10, v8}, Lcom/google/android/videos/store/AbstractFileStore;->getLastModified(Ljava/lang/Object;I)J

    move-result-wide v14

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->requestAfterMillis:J

    move-wide/from16 v16, v0

    add-long v14, v14, v16

    cmp-long v2, v2, v14

    if-gez v2, :cond_1

    .line 501
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v12}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_2
    .catch Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException; {:try_start_2 .. :try_end_2} :catch_4

    .line 525
    .end local v10    # "fileName":Ljava/lang/String;
    :goto_2
    return-void

    .line 474
    .restart local v10    # "fileName":Ljava/lang/String;
    .restart local v12    # "reconstructedResponse":Ljava/lang/Object;, "TE;"
    .restart local v13    # "storedResponse":Ljava/lang/Object;, "TS;"
    :catch_0
    move-exception v9

    .line 475
    .local v9, "e":Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error reading stored knowledge component "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " from storage "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;->storage:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v9}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 483
    .end local v9    # "e":Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;
    .end local v13    # "storedResponse":Ljava/lang/Object;, "TS;"
    :catch_1
    move-exception v9

    .line 486
    .local v9, "e":Ljava/lang/ClassCastException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error reconstructing stored knowledge component "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v9}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 487
    move-object/from16 v0, p1

    iget v2, v0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;->storage:I

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v2}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->remove(Ljava/lang/String;I)V

    goto :goto_1

    .line 488
    .end local v9    # "e":Ljava/lang/ClassCastException;
    :catch_2
    move-exception v9

    .line 489
    .local v9, "e":Lcom/google/android/videos/converter/ConverterException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error reconstructing stored knowledge component "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v9}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 490
    move-object/from16 v0, p1

    iget v2, v0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;->storage:I

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v2}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->remove(Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 491
    .end local v9    # "e":Lcom/google/android/videos/converter/ConverterException;
    :catch_3
    move-exception v9

    .line 492
    .local v9, "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error reconstructing stored knowledge component "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v9}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 493
    move-object/from16 v0, p1

    iget v2, v0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;->storage:I

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v2}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->remove(Ljava/lang/String;I)V

    goto/16 :goto_1

    .end local v9    # "e":Ljava/io/IOException;
    .end local v12    # "reconstructedResponse":Ljava/lang/Object;, "TE;"
    :cond_1
    move-object v6, v13

    move-object v7, v12

    .line 511
    .end local v10    # "fileName":Ljava/lang/String;
    :goto_3
    move v5, v11

    .line 514
    .local v5, "hadResponse":Z
    .local v6, "finalStoredResponse":Ljava/lang/Object;, "TS;"
    .local v7, "oldResponse":Ljava/lang/Object;, "TE;"
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->targetRequester:Lcom/google/android/videos/async/Requester;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->requestHandler:Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0, v6}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;->createUnderlyingRequest(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    new-instance v2, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester$1;

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v8, p2

    invoke-direct/range {v2 .. v8}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester$1;-><init>(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;ZLjava/lang/Object;Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    invoke-interface {v14, v15, v2}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    goto/16 :goto_2

    .line 504
    .end local v5    # "hadResponse":Z
    .end local v6    # "finalStoredResponse":Ljava/lang/Object;, "TS;"
    .end local v7    # "oldResponse":Ljava/lang/Object;, "TE;"
    .restart local v10    # "fileName":Ljava/lang/String;
    :catch_4
    move-exception v9

    .line 505
    .local v9, "e":Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error getting last modified timestamp of file "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " from storage "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;->storage:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v9}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .end local v9    # "e":Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;
    :cond_2
    move-object v6, v13

    move-object v7, v12

    goto :goto_3

    .end local v10    # "fileName":Ljava/lang/String;
    .restart local v12    # "reconstructedResponse":Ljava/lang/Object;, "TE;"
    .restart local v13    # "storedResponse":Ljava/lang/Object;, "TS;"
    :cond_3
    move-object v6, v13

    move-object v7, v12

    goto :goto_3
.end method

.method public bridge synthetic request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/google/android/videos/async/Callback;

    .prologue
    .line 438
    .local p0, "this":Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;, "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester<TR;TE;TT;TS;>;"
    check-cast p1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;->request(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;Lcom/google/android/videos/async/Callback;)V

    return-void
.end method
