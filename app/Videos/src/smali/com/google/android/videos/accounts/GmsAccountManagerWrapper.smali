.class public Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;
.super Lcom/google/android/videos/accounts/AccountManagerWrapper;
.source "GmsAccountManagerWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GmsDialogFragment;,
        Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;
    }
.end annotation


# instance fields
.field private final applicationContext:Landroid/content/Context;

.field private cacheAccount:Landroid/accounts/Account;

.field private cacheExpirationTimestamp:J

.field private cachedToken:Ljava/lang/String;

.field private lastAuthTimeoutTimestamp:J

.field private savedTask:Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;

.field private final tokenCacheDurationMs:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;J)V
    .locals 3
    .param p1, "applicationContext"    # Landroid/content/Context;
    .param p2, "scope"    # Ljava/lang/String;
    .param p3, "tokenCacheDurationMs"    # J

    .prologue
    .line 60
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.google"

    invoke-direct {p0, v0, p2, v1}, Lcom/google/android/videos/accounts/AccountManagerWrapper;-><init>(Landroid/accounts/AccountManager;Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->lastAuthTimeoutTimestamp:J

    .line 61
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->applicationContext:Landroid/content/Context;

    .line 62
    iput-wide p3, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->tokenCacheDurationMs:J

    .line 63
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;
    .param p1, "x1"    # Landroid/accounts/Account;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->getCachedToken(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->applicationContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;Landroid/accounts/Account;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;
    .param p1, "x1"    # Landroid/accounts/Account;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->setCachedToken(Landroid/accounts/Account;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$402(Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;
    .param p1, "x1"    # J

    .prologue
    .line 44
    iput-wide p1, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->lastAuthTimeoutTimestamp:J

    return-wide p1
.end method

.method static synthetic access$500(Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;Ljava/lang/String;Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->saveTask(Ljava/lang/String;Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;)V

    return-void
.end method

.method private getCachedToken(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->cachedToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->cacheAccount:Landroid/accounts/Account;

    invoke-virtual {p1, v0}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->cacheExpirationTimestamp:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->cachedToken:Ljava/lang/String;

    .line 152
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private saveTask(Ljava/lang/String;Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;)V
    .locals 2
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "currentTask"    # Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->savedTask:Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;

    .line 132
    .local v0, "oldTask":Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;
    iput-object p2, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->savedTask:Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;

    .line 133
    if-eqz v0, :cond_0

    .line 134
    # getter for: Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->authenticatee:Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;
    invoke-static {v0}, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->access$000(Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;)Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;->onNotAuthenticated(Ljava/lang/String;)V

    .line 136
    :cond_0
    return-void
.end method

.method private setCachedToken(Landroid/accounts/Account;Ljava/lang/String;)V
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "token"    # Ljava/lang/String;

    .prologue
    .line 139
    if-nez p2, :cond_0

    .line 145
    :goto_0
    return-void

    .line 142
    :cond_0
    iput-object p2, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->cachedToken:Ljava/lang/String;

    .line 143
    iput-object p1, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->cacheAccount:Landroid/accounts/Account;

    .line 144
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->tokenCacheDurationMs:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->cacheExpirationTimestamp:J

    goto :goto_0
.end method


# virtual methods
.method protected declared-synchronized blockingGetAuthTokenInternal(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 10
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException;
        }
    .end annotation

    .prologue
    .line 77
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->getCachedToken(Landroid/accounts/Account;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 78
    .local v1, "token":Ljava/lang/String;
    if-eqz v1, :cond_0

    move-object v2, v1

    .line 91
    .end local v1    # "token":Ljava/lang/String;
    .local v2, "token":Ljava/lang/String;
    :goto_0
    monitor-exit p0

    return-object v2

    .line 81
    .end local v2    # "token":Ljava/lang/String;
    .restart local v1    # "token":Ljava/lang/String;
    :cond_0
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->lastAuthTimeoutTimestamp:J

    const-wide/16 v8, 0xfa

    add-long/2addr v6, v8

    cmp-long v3, v4, v6

    if-gez v3, :cond_1

    .line 83
    const-string v3, "Cannot get user auth; within timeout retry period"

    invoke-static {v3}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 84
    new-instance v3, Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException;

    new-instance v4, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v4}, Ljava/util/concurrent/TimeoutException;-><init>()V

    invoke-direct {v3, v4}, Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException;-><init>(Ljava/lang/Throwable;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 77
    .end local v1    # "token":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 87
    .restart local v1    # "token":Ljava/lang/String;
    :cond_1
    :try_start_2
    iget-object v3, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->applicationContext:Landroid/content/Context;

    iget-object v4, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->scope:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {v3, v4, v5, v6}, Lcom/google/android/gms/auth/GoogleAuthUtil;->getTokenWithNotification(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    .line 89
    if-eqz v1, :cond_2

    .line 90
    invoke-direct {p0, p1, v1}, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->setCachedToken(Landroid/accounts/Account;Ljava/lang/String;)V

    move-object v2, v1

    .line 91
    .end local v1    # "token":Ljava/lang/String;
    .restart local v2    # "token":Ljava/lang/String;
    goto :goto_0

    .line 93
    .end local v2    # "token":Ljava/lang/String;
    .restart local v1    # "token":Ljava/lang/String;
    :cond_2
    new-instance v3, Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException;

    const-string v4, "Null auth token"

    invoke-direct {v3, v4}, Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/google/android/gms/auth/UserRecoverableAuthException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/google/android/gms/auth/GoogleAuthException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 95
    :catch_0
    move-exception v0

    .line 96
    .local v0, "e":Ljava/io/IOException;
    :try_start_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->lastAuthTimeoutTimestamp:J

    .line 97
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cannot get user auth; network error"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 98
    new-instance v3, Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException;

    invoke-direct {v3, v0}, Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 99
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 100
    .local v0, "e":Lcom/google/android/gms/auth/UserRecoverableAuthException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cannot get user auth: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/auth/UserRecoverableAuthException;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 101
    new-instance v3, Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException;

    invoke-direct {v3, v0}, Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 102
    .end local v0    # "e":Lcom/google/android/gms/auth/UserRecoverableAuthException;
    :catch_2
    move-exception v0

    .line 103
    .local v0, "e":Lcom/google/android/gms/auth/GoogleAuthException;
    const-string v3, "Cannot get user auth"

    invoke-static {v3, v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 104
    new-instance v3, Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException;

    invoke-direct {v3, v0}, Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException;-><init>(Ljava/lang/Throwable;)V

    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method protected getAuthTokenInternal(Landroid/accounts/Account;Landroid/app/Activity;Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;)V
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "activity"    # Landroid/app/Activity;
    .param p3, "callback"    # Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    .prologue
    .line 111
    instance-of v0, p2, Landroid/support/v4/app/FragmentActivity;

    const-string v1, "activity must be a FragmentActivity"

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;)V

    .line 113
    new-instance v0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;

    check-cast p2, Landroid/support/v4/app/FragmentActivity;

    .end local p2    # "activity":Landroid/app/Activity;
    invoke-direct {v0, p0, p2, p1, p3}, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;-><init>(Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;Landroid/support/v4/app/FragmentActivity;Landroid/accounts/Account;Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 114
    return-void
.end method

.method public declared-synchronized invalidateAuthToken(Ljava/lang/String;)V
    .locals 1
    .param p1, "authToken"    # Ljava/lang/String;

    .prologue
    .line 67
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->applicationContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/gms/auth/GoogleAuthUtil;->invalidateToken(Landroid/content/Context;Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->cachedToken:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->cachedToken:Ljava/lang/String;

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->cacheAccount:Landroid/accounts/Account;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    :cond_0
    monitor-exit p0

    return-void

    .line 67
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)Z
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    .line 118
    const/16 v2, 0x388

    if-eq p1, v2, :cond_1

    .line 127
    :cond_0
    :goto_0
    return v1

    .line 121
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->savedTask:Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;

    .line 122
    .local v0, "task":Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;->savedTask:Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;

    .line 123
    if-eqz v0, :cond_0

    .line 126
    invoke-virtual {v0, p2}, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper$GetTokenTask;->onActivityResult(I)V

    .line 127
    const/4 v1, 0x1

    goto :goto_0
.end method
