.class public final Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline;
.super Ljava/lang/Object;
.source "SubtitleWindowSettingsTimeline.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline$1;,
        Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline$Builder;
    }
.end annotation


# instance fields
.field private final settings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleWindowSettings;",
            ">;"
        }
    .end annotation
.end field

.field final startTimes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleWindowSettings;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    .local p1, "startTimes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .local p2, "settings":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/subtitles/SubtitleWindowSettings;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "startTimes and settings differ in size"

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 27
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline;->startTimes:Ljava/util/List;

    .line 28
    invoke-static {p2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline;->settings:Ljava/util/List;

    .line 29
    return-void

    .line 25
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method synthetic constructor <init>(Ljava/util/List;Ljava/util/List;Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/util/List;
    .param p2, "x1"    # Ljava/util/List;
    .param p3, "x2"    # Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline$1;

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline;-><init>(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public declared-synchronized getSubtitleWindowSettingsAt(I)Lcom/google/android/videos/subtitles/SubtitleWindowSettings;
    .locals 4
    .param p1, "timeMillis"    # I

    .prologue
    .line 35
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline;->startTimes:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    move-result v0

    .line 36
    .local v0, "index":I
    if-ltz v0, :cond_0

    .line 37
    iget-object v2, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline;->settings:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 46
    :goto_0
    monitor-exit p0

    return-object v2

    .line 42
    :cond_0
    neg-int v2, v0

    add-int/lit8 v1, v2, -0x2

    .line 43
    .local v1, "previousIntervalIndex":I
    if-ltz v1, :cond_1

    .line 44
    :try_start_1
    iget-object v2, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline;->settings:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 46
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 35
    .end local v0    # "index":I
    .end local v1    # "previousIntervalIndex":I
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 54
    .local v0, "builder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline;->startTimes:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 55
    const-string v2, "["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline;->startTimes:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline;->settings:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 61
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
