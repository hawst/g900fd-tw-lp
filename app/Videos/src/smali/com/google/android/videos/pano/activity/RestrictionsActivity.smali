.class public Lcom/google/android/videos/pano/activity/RestrictionsActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "RestrictionsActivity.java"

# interfaces
.implements Lcom/google/android/videos/ui/RestrictionsHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/pano/activity/RestrictionsActivity$PreferenceEntryListener;,
        Lcom/google/android/videos/pano/activity/RestrictionsActivity$PreferenceListener;
    }
.end annotation


# instance fields
.field private actions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/recline/app/DialogFragment$Action;",
            ">;"
        }
    .end annotation
.end field

.field private dialogFragment:Lcom/google/android/recline/app/DialogFragment;

.field private helper:Lcom/google/android/videos/ui/RestrictionsHelper;

.field private preferences:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/preference/Preference;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 174
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/videos/pano/activity/RestrictionsActivity;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/activity/RestrictionsActivity;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->preferences:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/pano/activity/RestrictionsActivity;Landroid/preference/Preference;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/pano/activity/RestrictionsActivity;
    .param p1, "x1"    # Landroid/preference/Preference;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->openPreference(Landroid/preference/Preference;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/videos/pano/activity/RestrictionsActivity;)Lcom/google/android/videos/ui/RestrictionsHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/activity/RestrictionsActivity;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->helper:Lcom/google/android/videos/ui/RestrictionsHelper;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/videos/pano/activity/RestrictionsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/pano/activity/RestrictionsActivity;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->updateActions()V

    return-void
.end method

.method private getText(Landroid/preference/Preference;)Ljava/lang/String;
    .locals 2
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 158
    instance-of v1, p1, Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_1

    .line 159
    check-cast p1, Landroid/preference/CheckBoxPreference;

    .end local p1    # "preference":Landroid/preference/Preference;
    invoke-virtual {p1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    const v0, 0x7f0b01e7

    .line 160
    .local v0, "textId":I
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 162
    .end local v0    # "textId":I
    :goto_1
    return-object v1

    .line 159
    :cond_0
    const v0, 0x7f0b01e8

    goto :goto_0

    .line 162
    .restart local p1    # "preference":Landroid/preference/Preference;
    :cond_1
    check-cast p1, Landroid/preference/ListPreference;

    .end local p1    # "preference":Landroid/preference/Preference;
    invoke-virtual {p1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method private openPreference(Landroid/preference/CheckBoxPreference;)V
    .locals 6
    .param p1, "preference"    # Landroid/preference/CheckBoxPreference;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 95
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 96
    .local v1, "noYesActions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/recline/app/DialogFragment$Action;>;"
    new-instance v2, Lcom/google/android/recline/app/DialogFragment$Action$Builder;

    invoke-direct {v2}, Lcom/google/android/recline/app/DialogFragment$Action$Builder;-><init>()V

    const v5, 0x7f0b01e8

    invoke-virtual {p0, v5}, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->title(Ljava/lang/String;)Lcom/google/android/recline/app/DialogFragment$Action$Builder;

    move-result-object v2

    const-string v5, "0"

    invoke-virtual {v2, v5}, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->key(Ljava/lang/String;)Lcom/google/android/recline/app/DialogFragment$Action$Builder;

    move-result-object v5

    invoke-virtual {p1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    invoke-virtual {v5, v2}, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->checked(Z)Lcom/google/android/recline/app/DialogFragment$Action$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->build()Lcom/google/android/recline/app/DialogFragment$Action;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 102
    new-instance v2, Lcom/google/android/recline/app/DialogFragment$Action$Builder;

    invoke-direct {v2}, Lcom/google/android/recline/app/DialogFragment$Action$Builder;-><init>()V

    const v5, 0x7f0b01e7

    invoke-virtual {p0, v5}, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->title(Ljava/lang/String;)Lcom/google/android/recline/app/DialogFragment$Action$Builder;

    move-result-object v2

    const-string v5, "1"

    invoke-virtual {v2, v5}, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->key(Ljava/lang/String;)Lcom/google/android/recline/app/DialogFragment$Action$Builder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v5

    invoke-virtual {v2, v5}, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->checked(Z)Lcom/google/android/recline/app/DialogFragment$Action$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->build()Lcom/google/android/recline/app/DialogFragment$Action;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    new-instance v2, Lcom/google/android/recline/app/DialogFragment$Builder;

    invoke-direct {v2}, Lcom/google/android/recline/app/DialogFragment$Builder;-><init>()V

    invoke-virtual {p1}, Landroid/preference/CheckBoxPreference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/google/android/recline/app/DialogFragment$Builder;->title(Ljava/lang/String;)Lcom/google/android/recline/app/DialogFragment$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/recline/app/DialogFragment$Builder;->actions(Ljava/util/ArrayList;)Lcom/google/android/recline/app/DialogFragment$Builder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v5

    if-eqz v5, :cond_1

    :goto_1
    invoke-virtual {v2, v3}, Lcom/google/android/recline/app/DialogFragment$Builder;->selectedIndex(I)Lcom/google/android/recline/app/DialogFragment$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/recline/app/DialogFragment$Builder;->build()Lcom/google/android/recline/app/DialogFragment;

    move-result-object v0

    .line 113
    .local v0, "dialog":Lcom/google/android/recline/app/DialogFragment;
    new-instance v2, Lcom/google/android/videos/pano/activity/RestrictionsActivity$PreferenceEntryListener;

    invoke-direct {v2, p0, p1}, Lcom/google/android/videos/pano/activity/RestrictionsActivity$PreferenceEntryListener;-><init>(Lcom/google/android/videos/pano/activity/RestrictionsActivity;Landroid/preference/Preference;)V

    invoke-virtual {v0, v2}, Lcom/google/android/recline/app/DialogFragment;->setListener(Lcom/google/android/recline/app/DialogFragment$Action$Listener;)V

    .line 114
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/google/android/recline/app/DialogFragment;->add(Landroid/app/FragmentManager;Lcom/google/android/recline/app/DialogFragment;)V

    .line 115
    return-void

    .end local v0    # "dialog":Lcom/google/android/recline/app/DialogFragment;
    :cond_0
    move v2, v4

    .line 96
    goto :goto_0

    :cond_1
    move v3, v4

    .line 108
    goto :goto_1
.end method

.method private openPreference(Landroid/preference/ListPreference;)V
    .locals 8
    .param p1, "preference"    # Landroid/preference/ListPreference;

    .prologue
    .line 118
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 119
    .local v5, "preferenceActions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/recline/app/DialogFragment$Action;>;"
    invoke-virtual {p1}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/CharSequence;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v2, v0, v3

    .line 120
    .local v2, "entry":Ljava/lang/CharSequence;
    new-instance v6, Lcom/google/android/recline/app/DialogFragment$Action$Builder;

    invoke-direct {v6}, Lcom/google/android/recline/app/DialogFragment$Action$Builder;-><init>()V

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->title(Ljava/lang/String;)Lcom/google/android/recline/app/DialogFragment$Action$Builder;

    move-result-object v6

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->key(Ljava/lang/String;)Lcom/google/android/recline/app/DialogFragment$Action$Builder;

    move-result-object v6

    invoke-virtual {p1}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->checked(Z)Lcom/google/android/recline/app/DialogFragment$Action$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->build()Lcom/google/android/recline/app/DialogFragment$Action;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 126
    .end local v2    # "entry":Ljava/lang/CharSequence;
    :cond_0
    new-instance v6, Lcom/google/android/recline/app/DialogFragment$Builder;

    invoke-direct {v6}, Lcom/google/android/recline/app/DialogFragment$Builder;-><init>()V

    invoke-virtual {p1}, Landroid/preference/ListPreference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/recline/app/DialogFragment$Builder;->title(Ljava/lang/String;)Lcom/google/android/recline/app/DialogFragment$Builder;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/google/android/recline/app/DialogFragment$Builder;->actions(Ljava/util/ArrayList;)Lcom/google/android/recline/app/DialogFragment$Builder;

    move-result-object v6

    invoke-virtual {p1}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/google/android/recline/app/DialogFragment$Builder;->selectedIndex(I)Lcom/google/android/recline/app/DialogFragment$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/recline/app/DialogFragment$Builder;->build()Lcom/google/android/recline/app/DialogFragment;

    move-result-object v1

    .line 131
    .local v1, "dialog":Lcom/google/android/recline/app/DialogFragment;
    new-instance v6, Lcom/google/android/videos/pano/activity/RestrictionsActivity$PreferenceEntryListener;

    invoke-direct {v6, p0, p1}, Lcom/google/android/videos/pano/activity/RestrictionsActivity$PreferenceEntryListener;-><init>(Lcom/google/android/videos/pano/activity/RestrictionsActivity;Landroid/preference/Preference;)V

    invoke-virtual {v1, v6}, Lcom/google/android/recline/app/DialogFragment;->setListener(Lcom/google/android/recline/app/DialogFragment$Action$Listener;)V

    .line 132
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    invoke-static {v6, v1}, Lcom/google/android/recline/app/DialogFragment;->add(Landroid/app/FragmentManager;Lcom/google/android/recline/app/DialogFragment;)V

    .line 133
    return-void
.end method

.method private openPreference(Landroid/preference/Preference;)V
    .locals 1
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 87
    instance-of v0, p1, Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    .line 88
    check-cast p1, Landroid/preference/CheckBoxPreference;

    .end local p1    # "preference":Landroid/preference/Preference;
    invoke-direct {p0, p1}, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->openPreference(Landroid/preference/CheckBoxPreference;)V

    .line 92
    :goto_0
    return-void

    .line 90
    .restart local p1    # "preference":Landroid/preference/Preference;
    :cond_0
    check-cast p1, Landroid/preference/ListPreference;

    .end local p1    # "preference":Landroid/preference/Preference;
    invoke-direct {p0, p1}, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->openPreference(Landroid/preference/ListPreference;)V

    goto :goto_0
.end method

.method private updateActions()V
    .locals 7

    .prologue
    .line 136
    new-instance v3, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->preferences:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 137
    .local v3, "preferenceKeys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v4, Lcom/google/android/videos/pano/activity/RestrictionsActivity$1;

    invoke-direct {v4, p0}, Lcom/google/android/videos/pano/activity/RestrictionsActivity$1;-><init>(Lcom/google/android/videos/pano/activity/RestrictionsActivity;)V

    invoke-static {v3, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 143
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->actions:Ljava/util/ArrayList;

    .line 144
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 145
    .local v1, "key":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->preferences:Ljava/util/HashMap;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/preference/Preference;

    .line 146
    .local v2, "preference":Landroid/preference/Preference;
    iget-object v4, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->actions:Ljava/util/ArrayList;

    new-instance v5, Lcom/google/android/recline/app/DialogFragment$Action$Builder;

    invoke-direct {v5}, Lcom/google/android/recline/app/DialogFragment$Action$Builder;-><init>()V

    invoke-virtual {v2}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->title(Ljava/lang/String;)Lcom/google/android/recline/app/DialogFragment$Action$Builder;

    move-result-object v5

    invoke-virtual {v2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->key(Ljava/lang/String;)Lcom/google/android/recline/app/DialogFragment$Action$Builder;

    move-result-object v5

    invoke-direct {p0, v2}, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->getText(Landroid/preference/Preference;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->description(Ljava/lang/String;)Lcom/google/android/recline/app/DialogFragment$Action$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->build()Lcom/google/android/recline/app/DialogFragment$Action;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 152
    .end local v1    # "key":Ljava/lang/String;
    .end local v2    # "preference":Landroid/preference/Preference;
    :cond_0
    iget-object v4, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->dialogFragment:Lcom/google/android/recline/app/DialogFragment;

    if-eqz v4, :cond_1

    .line 153
    iget-object v4, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->dialogFragment:Lcom/google/android/recline/app/DialogFragment;

    iget-object v5, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->actions:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Lcom/google/android/recline/app/DialogFragment;->setActions(Ljava/util/ArrayList;)V

    .line 155
    :cond_1
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->helper:Lcom/google/android/videos/ui/RestrictionsHelper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/videos/ui/RestrictionsHelper;->onActivityResult(IILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    :goto_0
    return-void

    .line 77
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 41
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 42
    new-instance v3, Lcom/google/android/videos/ui/RestrictionsHelper;

    invoke-direct {v3, p0, p0, v5}, Lcom/google/android/videos/ui/RestrictionsHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/ui/RestrictionsHelper$Listener;Landroid/view/View;)V

    iput-object v3, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->helper:Lcom/google/android/videos/ui/RestrictionsHelper;

    .line 43
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->preferences:Ljava/util/HashMap;

    .line 45
    const v3, 0x7f0b0200

    invoke-virtual {p0, v3}, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 46
    .local v2, "title":Ljava/lang/String;
    const v3, 0x7f0b0201

    invoke-virtual {p0, v3}, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 48
    .local v1, "description":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->helper:Lcom/google/android/videos/ui/RestrictionsHelper;

    invoke-virtual {v3}, Lcom/google/android/videos/ui/RestrictionsHelper;->createAllowUnratedPreference()Landroid/preference/CheckBoxPreference;

    move-result-object v0

    .line 49
    .local v0, "allowUnratedPreference":Landroid/preference/Preference;
    iget-object v3, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->preferences:Ljava/util/HashMap;

    invoke-virtual {v0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    invoke-direct {p0}, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->updateActions()V

    .line 51
    new-instance v3, Lcom/google/android/recline/app/DialogFragment$Builder;

    invoke-direct {v3}, Lcom/google/android/recline/app/DialogFragment$Builder;-><init>()V

    invoke-virtual {v3, v2}, Lcom/google/android/recline/app/DialogFragment$Builder;->title(Ljava/lang/String;)Lcom/google/android/recline/app/DialogFragment$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/android/recline/app/DialogFragment$Builder;->description(Ljava/lang/String;)Lcom/google/android/recline/app/DialogFragment$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->actions:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Lcom/google/android/recline/app/DialogFragment$Builder;->actions(Ljava/util/ArrayList;)Lcom/google/android/recline/app/DialogFragment$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/recline/app/DialogFragment$Builder;->build()Lcom/google/android/recline/app/DialogFragment;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->dialogFragment:Lcom/google/android/recline/app/DialogFragment;

    .line 56
    iget-object v3, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->dialogFragment:Lcom/google/android/recline/app/DialogFragment;

    new-instance v4, Lcom/google/android/videos/pano/activity/RestrictionsActivity$PreferenceListener;

    invoke-direct {v4, p0, v5}, Lcom/google/android/videos/pano/activity/RestrictionsActivity$PreferenceListener;-><init>(Lcom/google/android/videos/pano/activity/RestrictionsActivity;Lcom/google/android/videos/pano/activity/RestrictionsActivity$1;)V

    invoke-virtual {v3, v4}, Lcom/google/android/recline/app/DialogFragment;->setListener(Lcom/google/android/recline/app/DialogFragment$Action$Listener;)V

    .line 57
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->dialogFragment:Lcom/google/android/recline/app/DialogFragment;

    invoke-static {v3, v4}, Lcom/google/android/recline/app/DialogFragment;->add(Landroid/app/FragmentManager;Lcom/google/android/recline/app/DialogFragment;)V

    .line 58
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 68
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onPause()V

    .line 69
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->helper:Lcom/google/android/videos/ui/RestrictionsHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/RestrictionsHelper;->onPause()V

    .line 70
    return-void
.end method

.method public onRestrictionPreferenceCreated(Landroid/preference/ListPreference;)V
    .locals 2
    .param p1, "preference"    # Landroid/preference/ListPreference;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->preferences:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/preference/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    invoke-direct {p0}, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->updateActions()V

    .line 84
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 62
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    .line 63
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->helper:Lcom/google/android/videos/ui/RestrictionsHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/RestrictionsHelper;->onResume()V

    .line 64
    return-void
.end method
