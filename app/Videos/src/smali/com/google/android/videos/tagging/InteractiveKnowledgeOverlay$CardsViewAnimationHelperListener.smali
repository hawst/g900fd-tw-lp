.class final Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$CardsViewAnimationHelperListener;
.super Ljava/lang/Object;
.source "InteractiveKnowledgeOverlay.java"

# interfaces
.implements Lcom/google/android/videos/tagging/CardsViewAnimationHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "CardsViewAnimationHelperListener"
.end annotation


# instance fields
.field private final hideTagsWhenObscured:Z

.field final synthetic this$0:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;Z)V
    .locals 0
    .param p2, "hideTagsWhenObscured"    # Z

    .prologue
    .line 585
    iput-object p1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$CardsViewAnimationHelperListener;->this$0:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 586
    iput-boolean p2, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$CardsViewAnimationHelperListener;->hideTagsWhenObscured:Z

    .line 587
    return-void
.end method


# virtual methods
.method public onCardListCollapseProgress(F)V
    .locals 4
    .param p1, "progress"    # F

    .prologue
    .line 611
    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$CardsViewAnimationHelperListener;->this$0:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    # getter for: Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->listener:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;
    invoke-static {v1}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->access$000(Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;)Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 612
    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$CardsViewAnimationHelperListener;->this$0:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    # getter for: Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->listener:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;
    invoke-static {v1}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->access$000(Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;)Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;->onCardListCollapseProgress(F)V

    .line 614
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$CardsViewAnimationHelperListener;->hideTagsWhenObscured:Z

    if-eqz v1, :cond_1

    .line 615
    const/4 v1, 0x0

    const/high16 v2, 0x3fc00000    # 1.5f

    mul-float/2addr v2, p1

    const/high16 v3, 0x3f000000    # 0.5f

    sub-float/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 616
    .local v0, "alpha":F
    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$CardsViewAnimationHelperListener;->this$0:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    # getter for: Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->tagsView:Lcom/google/android/videos/tagging/TagsView;
    invoke-static {v1}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->access$100(Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;)Lcom/google/android/videos/tagging/TagsView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/videos/tagging/TagsView;->setAlpha(F)V

    .line 618
    .end local v0    # "alpha":F
    :cond_1
    return-void
.end method

.method public onCardListCollapsed(I)V
    .locals 2
    .param p1, "eventCause"    # I

    .prologue
    .line 601
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$CardsViewAnimationHelperListener;->this$0:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    # getter for: Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->listener:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;
    invoke-static {v0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->access$000(Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;)Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 602
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$CardsViewAnimationHelperListener;->this$0:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    # getter for: Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->listener:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;
    invoke-static {v0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->access$000(Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;)Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;->onCardListCollapsed(I)V

    .line 604
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$CardsViewAnimationHelperListener;->hideTagsWhenObscured:Z

    if-eqz v0, :cond_1

    .line 605
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$CardsViewAnimationHelperListener;->this$0:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    # getter for: Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->tagsView:Lcom/google/android/videos/tagging/TagsView;
    invoke-static {v0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->access$100(Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;)Lcom/google/android/videos/tagging/TagsView;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/google/android/videos/tagging/TagsView;->setAlpha(F)V

    .line 607
    :cond_1
    return-void
.end method

.method public onCardListExpanded(I)V
    .locals 2
    .param p1, "eventCause"    # I

    .prologue
    .line 591
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$CardsViewAnimationHelperListener;->this$0:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    # getter for: Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->listener:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;
    invoke-static {v0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->access$000(Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;)Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 592
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$CardsViewAnimationHelperListener;->this$0:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    # getter for: Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->listener:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;
    invoke-static {v0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->access$000(Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;)Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;->onCardListExpanded(I)V

    .line 594
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$CardsViewAnimationHelperListener;->hideTagsWhenObscured:Z

    if-eqz v0, :cond_1

    .line 595
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$CardsViewAnimationHelperListener;->this$0:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    # getter for: Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->tagsView:Lcom/google/android/videos/tagging/TagsView;
    invoke-static {v0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->access$100(Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;)Lcom/google/android/videos/tagging/TagsView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/tagging/TagsView;->setAlpha(F)V

    .line 597
    :cond_1
    return-void
.end method
