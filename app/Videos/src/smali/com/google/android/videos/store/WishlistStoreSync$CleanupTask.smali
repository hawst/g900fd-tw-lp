.class Lcom/google/android/videos/store/WishlistStoreSync$CleanupTask;
.super Lcom/google/android/videos/store/SyncTaskManager$SyncTask;
.source "WishlistStoreSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/WishlistStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CleanupTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/store/WishlistStoreSync;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/store/WishlistStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V
    .locals 1
    .param p2, "priority"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/store/SyncTaskManager$SharedStates",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 292
    .local p3, "states":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<*>;"
    iput-object p1, p0, Lcom/google/android/videos/store/WishlistStoreSync$CleanupTask;->this$0:Lcom/google/android/videos/store/WishlistStoreSync;

    .line 293
    # getter for: Lcom/google/android/videos/store/WishlistStoreSync;->syncTaskManager:Lcom/google/android/videos/store/SyncTaskManager;
    invoke-static {p1}, Lcom/google/android/videos/store/WishlistStoreSync;->access$000(Lcom/google/android/videos/store/WishlistStoreSync;)Lcom/google/android/videos/store/SyncTaskManager;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;-><init>(Lcom/google/android/videos/store/SyncTaskManager;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V

    .line 294
    return-void
.end method


# virtual methods
.method protected doSync()V
    .locals 19

    .prologue
    .line 299
    const/4 v2, 0x0

    const-string v3, "wishlist"

    # getter for: Lcom/google/android/videos/store/WishlistStoreSync;->ACCOUNT_COLUMN:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/videos/store/WishlistStoreSync;->access$400()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-string v6, "wishlist_account"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQueryString(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 301
    .local v16, "sql":Ljava/lang/String;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 302
    .local v11, "accounts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/WishlistStoreSync$CleanupTask;->this$0:Lcom/google/android/videos/store/WishlistStoreSync;

    # getter for: Lcom/google/android/videos/store/WishlistStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v2}, Lcom/google/android/videos/store/WishlistStoreSync;->access$100(Lcom/google/android/videos/store/WishlistStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v2, v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 304
    .local v12, "cursor":Landroid/database/Cursor;
    :goto_0
    :try_start_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 305
    const/4 v2, 0x0

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v11, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 308
    :catchall_0
    move-exception v2

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_0
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 311
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/WishlistStoreSync$CleanupTask;->this$0:Lcom/google/android/videos/store/WishlistStoreSync;

    # getter for: Lcom/google/android/videos/store/WishlistStoreSync;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;
    invoke-static {v2}, Lcom/google/android/videos/store/WishlistStoreSync;->access$500(Lcom/google/android/videos/store/WishlistStoreSync;)Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getAccounts()[Landroid/accounts/Account;

    move-result-object v15

    .line 312
    .local v15, "knownAccounts":[Landroid/accounts/Account;
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_1
    array-length v2, v15

    if-ge v13, v2, :cond_1

    .line 313
    aget-object v2, v15, v13

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v11, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 312
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 315
    :cond_1
    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 334
    :goto_2
    return-void

    .line 319
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/WishlistStoreSync$CleanupTask;->this$0:Lcom/google/android/videos/store/WishlistStoreSync;

    # getter for: Lcom/google/android/videos/store/WishlistStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v2}, Lcom/google/android/videos/store/WishlistStoreSync;->access$100(Lcom/google/android/videos/store/WishlistStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v18

    .line 320
    .local v18, "transaction":Landroid/database/sqlite/SQLiteDatabase;
    const/16 v17, 0x0

    .line 323
    .local v17, "success":Z
    :try_start_1
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 324
    .local v10, "account":Ljava/lang/String;
    const-string v2, "wishlist"

    const-string v3, "wishlist_account = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v10, v4, v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_3

    .line 328
    .end local v10    # "account":Ljava/lang/String;
    .end local v14    # "i$":Ljava/util/Iterator;
    :catchall_1
    move-exception v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/store/WishlistStoreSync$CleanupTask;->this$0:Lcom/google/android/videos/store/WishlistStoreSync;

    # getter for: Lcom/google/android/videos/store/WishlistStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v3}, Lcom/google/android/videos/store/WishlistStoreSync;->access$100(Lcom/google/android/videos/store/WishlistStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v3, v0, v1, v4, v5}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v2

    .line 326
    .restart local v14    # "i$":Ljava/util/Iterator;
    :cond_3
    const/16 v17, 0x1

    .line 328
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/WishlistStoreSync$CleanupTask;->this$0:Lcom/google/android/videos/store/WishlistStoreSync;

    # getter for: Lcom/google/android/videos/store/WishlistStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v2}, Lcom/google/android/videos/store/WishlistStoreSync;->access$100(Lcom/google/android/videos/store/WishlistStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v2, v0, v1, v3, v4}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    goto :goto_2
.end method
