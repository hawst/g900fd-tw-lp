.class Lcom/google/android/videos/store/FilteredDatabaseObservable$1;
.super Ljava/lang/Object;
.source "FilteredDatabaseObservable.java"

# interfaces
.implements Lcom/google/android/repolib/observers/UpdatablesChanged;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/store/FilteredDatabaseObservable;-><init>(Lcom/google/android/videos/store/Database;[Ljava/lang/Integer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/store/FilteredDatabaseObservable;

.field final synthetic val$database:Lcom/google/android/videos/store/Database;


# direct methods
.method constructor <init>(Lcom/google/android/videos/store/FilteredDatabaseObservable;Lcom/google/android/videos/store/Database;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lcom/google/android/videos/store/FilteredDatabaseObservable$1;->this$0:Lcom/google/android/videos/store/FilteredDatabaseObservable;

    iput-object p2, p0, Lcom/google/android/videos/store/FilteredDatabaseObservable$1;->val$database:Lcom/google/android/videos/store/Database;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public firstUpdatableAdded()V
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/videos/store/FilteredDatabaseObservable$1;->val$database:Lcom/google/android/videos/store/Database;

    iget-object v1, p0, Lcom/google/android/videos/store/FilteredDatabaseObservable$1;->this$0:Lcom/google/android/videos/store/FilteredDatabaseObservable;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/store/Database;->addListener(Lcom/google/android/videos/store/Database$Listener;)V

    .line 32
    return-void
.end method

.method public lastUpdatableRemoved()V
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/videos/store/FilteredDatabaseObservable$1;->val$database:Lcom/google/android/videos/store/Database;

    iget-object v1, p0, Lcom/google/android/videos/store/FilteredDatabaseObservable$1;->this$0:Lcom/google/android/videos/store/FilteredDatabaseObservable;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/store/Database;->removeListener(Lcom/google/android/videos/store/Database$Listener;)Z

    .line 37
    return-void
.end method
