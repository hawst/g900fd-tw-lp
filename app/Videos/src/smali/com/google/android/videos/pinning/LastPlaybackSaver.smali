.class public Lcom/google/android/videos/pinning/LastPlaybackSaver;
.super Ljava/lang/Object;
.source "LastPlaybackSaver.java"


# instance fields
.field private final account:Ljava/lang/String;

.field private final context:Landroid/content/Context;

.field private final database:Lcom/google/android/videos/store/Database;

.field private final isTrailer:Z

.field private final localExecutor:Ljava/util/concurrent/Executor;

.field private playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

.field private seasonId:Ljava/lang/String;

.field private showId:Ljava/lang/String;

.field private videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ZLcom/google/android/videos/store/Database;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "isTrailer"    # Z
    .param p4, "database"    # Lcom/google/android/videos/store/Database;
    .param p5, "localExecutor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver;->context:Landroid/content/Context;

    .line 35
    iput-object p2, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver;->account:Ljava/lang/String;

    .line 36
    iput-boolean p3, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver;->isTrailer:Z

    .line 37
    iput-object p4, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver;->database:Lcom/google/android/videos/store/Database;

    .line 38
    iput-object p5, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver;->localExecutor:Ljava/util/concurrent/Executor;

    .line 39
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/pinning/LastPlaybackSaver;)Lcom/google/android/videos/store/Database;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pinning/LastPlaybackSaver;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver;->database:Lcom/google/android/videos/store/Database;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/pinning/LastPlaybackSaver;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pinning/LastPlaybackSaver;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver;->account:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/pinning/LastPlaybackSaver;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pinning/LastPlaybackSaver;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver;->context:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "seasonId"    # Ljava/lang/String;
    .param p3, "showId"    # Ljava/lang/String;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver;->videoId:Ljava/lang/String;

    .line 43
    iput-object p2, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver;->seasonId:Ljava/lang/String;

    .line 44
    iput-object p3, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver;->showId:Ljava/lang/String;

    .line 45
    new-instance v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    .line 46
    return-void
.end method

.method public onPaused()V
    .locals 4

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->stopTimestampMsec:J

    .line 57
    return-void
.end method

.method public onPlaying()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 49
    iget-object v0, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    iget-wide v0, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->startTimestampMsec:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->startTimestampMsec:J

    .line 51
    iget-object v0, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    iput-wide v4, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->stopTimestampMsec:J

    .line 53
    :cond_0
    return-void
.end method

.method public persistLastPlayback(I)V
    .locals 12
    .param p1, "positionMillis"    # I

    .prologue
    const-wide/16 v10, 0x0

    .line 60
    iget-boolean v0, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver;->isTrailer:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver;->account:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    int-to-long v8, p1

    iput-wide v8, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->positionMsec:J

    .line 64
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 65
    .local v2, "values":Landroid/content/ContentValues;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 66
    .local v6, "now":J
    iget-object v0, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    iget-wide v0, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->startTimestampMsec:J

    cmp-long v0, v0, v10

    if-nez v0, :cond_2

    .line 67
    iget-object v0, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    iput-wide v6, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->startTimestampMsec:J

    .line 69
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    iget-wide v0, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->stopTimestampMsec:J

    cmp-long v0, v0, v10

    if-nez v0, :cond_3

    .line 70
    iget-object v0, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    iput-wide v6, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->stopTimestampMsec:J

    .line 72
    :cond_3
    const-string v0, "last_playback_is_dirty"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 73
    const-string v0, "last_playback_start_timestamp"

    iget-object v1, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    iget-wide v8, v1, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->startTimestampMsec:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 74
    const-string v0, "last_watched_timestamp"

    iget-object v1, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    iget-wide v8, v1, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->stopTimestampMsec:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 75
    const-string v0, "resume_timestamp"

    iget-object v1, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    iget-wide v8, v1, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->positionMsec:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 77
    iget-object v3, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver;->videoId:Ljava/lang/String;

    .line 78
    .local v3, "finalVideoId":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver;->seasonId:Ljava/lang/String;

    .line 79
    .local v4, "finalSeasonId":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver;->showId:Ljava/lang/String;

    .line 80
    .local v5, "finalShowId":Ljava/lang/String;
    iget-object v8, p0, Lcom/google/android/videos/pinning/LastPlaybackSaver;->localExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/google/android/videos/pinning/LastPlaybackSaver$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/pinning/LastPlaybackSaver$1;-><init>(Lcom/google/android/videos/pinning/LastPlaybackSaver;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
