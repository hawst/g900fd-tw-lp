.class Lcom/google/android/videos/VideosGlobalsImpl$5;
.super Lcom/google/android/videos/store/Database$BaseListener;
.source "VideosGlobalsImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/VideosGlobalsImpl;->getVerticalsHelper()Lcom/google/android/videos/ui/VerticalsHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/VideosGlobalsImpl;


# direct methods
.method constructor <init>(Lcom/google/android/videos/VideosGlobalsImpl;)V
    .locals 0

    .prologue
    .line 968
    iput-object p1, p0, Lcom/google/android/videos/VideosGlobalsImpl$5;->this$0:Lcom/google/android/videos/VideosGlobalsImpl;

    invoke-direct {p0}, Lcom/google/android/videos/store/Database$BaseListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onMovieMetadataUpdated(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 976
    .local p1, "movieIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl$5;->this$0:Lcom/google/android/videos/VideosGlobalsImpl;

    # getter for: Lcom/google/android/videos/VideosGlobalsImpl;->verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;
    invoke-static {v0}, Lcom/google/android/videos/VideosGlobalsImpl;->access$300(Lcom/google/android/videos/VideosGlobalsImpl;)Lcom/google/android/videos/ui/VerticalsHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/ui/VerticalsHelper;->updateUsersContent()V

    .line 977
    return-void
.end method

.method public onPurchasesUpdated(Ljava/lang/String;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 971
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl$5;->this$0:Lcom/google/android/videos/VideosGlobalsImpl;

    # getter for: Lcom/google/android/videos/VideosGlobalsImpl;->verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;
    invoke-static {v0}, Lcom/google/android/videos/VideosGlobalsImpl;->access$300(Lcom/google/android/videos/VideosGlobalsImpl;)Lcom/google/android/videos/ui/VerticalsHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/ui/VerticalsHelper;->updateUsersContent()V

    .line 972
    return-void
.end method

.method public onShowMetadataUpdated(Ljava/lang/String;)V
    .locals 1
    .param p1, "showId"    # Ljava/lang/String;

    .prologue
    .line 981
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl$5;->this$0:Lcom/google/android/videos/VideosGlobalsImpl;

    # getter for: Lcom/google/android/videos/VideosGlobalsImpl;->verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;
    invoke-static {v0}, Lcom/google/android/videos/VideosGlobalsImpl;->access$300(Lcom/google/android/videos/VideosGlobalsImpl;)Lcom/google/android/videos/ui/VerticalsHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/ui/VerticalsHelper;->updateUsersContent()V

    .line 982
    return-void
.end method
