.class Lcom/google/android/videos/player/DirectorInitializer$5;
.super Ljava/lang/Object;
.source "DirectorInitializer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/player/DirectorInitializer;->onAllTasksCompleted()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/player/DirectorInitializer;


# direct methods
.method constructor <init>(Lcom/google/android/videos/player/DirectorInitializer;)V
    .locals 0

    .prologue
    .line 742
    iput-object p1, p0, Lcom/google/android/videos/player/DirectorInitializer$5;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 21

    .prologue
    .line 745
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/DirectorInitializer$5;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->listener:Lcom/google/android/videos/player/DirectorInitializer$Listener;
    invoke-static {v2}, Lcom/google/android/videos/player/DirectorInitializer;->access$700(Lcom/google/android/videos/player/DirectorInitializer;)Lcom/google/android/videos/player/DirectorInitializer$Listener;

    move-result-object v2

    new-instance v3, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/DirectorInitializer$5;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->isRental:Z
    invoke-static {v4}, Lcom/google/android/videos/player/DirectorInitializer;->access$1400(Lcom/google/android/videos/player/DirectorInitializer;)Z

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/player/DirectorInitializer$5;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->showShortClockDialog:Z
    invoke-static {v5}, Lcom/google/android/videos/player/DirectorInitializer;->access$1500(Lcom/google/android/videos/player/DirectorInitializer;)Z

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/player/DirectorInitializer$5;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->shortClockMillis:J
    invoke-static {v6}, Lcom/google/android/videos/player/DirectorInitializer;->access$1600(Lcom/google/android/videos/player/DirectorInitializer;)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/videos/player/DirectorInitializer$5;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->durationMillis:I
    invoke-static {v8}, Lcom/google/android/videos/player/DirectorInitializer;->access$1700(Lcom/google/android/videos/player/DirectorInitializer;)I

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/videos/player/DirectorInitializer$5;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->resumeTimeMillis:I
    invoke-static {v9}, Lcom/google/android/videos/player/DirectorInitializer;->access$1800(Lcom/google/android/videos/player/DirectorInitializer;)I

    move-result v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/videos/player/DirectorInitializer$5;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->streams:Lcom/google/android/videos/streams/Streams;
    invoke-static {v10}, Lcom/google/android/videos/player/DirectorInitializer;->access$1900(Lcom/google/android/videos/player/DirectorInitializer;)Lcom/google/android/videos/streams/Streams;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/videos/player/DirectorInitializer$5;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->drmIdentifiers:Lcom/google/android/videos/drm/DrmManager$Identifiers;
    invoke-static {v11}, Lcom/google/android/videos/player/DirectorInitializer;->access$2000(Lcom/google/android/videos/player/DirectorInitializer;)Lcom/google/android/videos/drm/DrmManager$Identifiers;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/player/DirectorInitializer$5;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->cencKeySetId:[B
    invoke-static {v12}, Lcom/google/android/videos/player/DirectorInitializer;->access$600(Lcom/google/android/videos/player/DirectorInitializer;)[B

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/videos/player/DirectorInitializer$5;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->cencSecurityLevel:I
    invoke-static {v13}, Lcom/google/android/videos/player/DirectorInitializer;->access$500(Lcom/google/android/videos/player/DirectorInitializer;)I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/player/DirectorInitializer$5;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->subtitleMode:I
    invoke-static {v14}, Lcom/google/android/videos/player/DirectorInitializer;->access$2100(Lcom/google/android/videos/player/DirectorInitializer;)I

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/player/DirectorInitializer$5;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->subtitleDefaultLanguage:Ljava/lang/String;
    invoke-static {v15}, Lcom/google/android/videos/player/DirectorInitializer;->access$2200(Lcom/google/android/videos/player/DirectorInitializer;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/player/DirectorInitializer$5;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    move-object/from16 v16, v0

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->haveAudioInDeviceLanguage:Z
    invoke-static/range {v16 .. v16}, Lcom/google/android/videos/player/DirectorInitializer;->access$2300(Lcom/google/android/videos/player/DirectorInitializer;)Z

    move-result v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/player/DirectorInitializer$5;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    move-object/from16 v17, v0

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->videoTitle:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/google/android/videos/player/DirectorInitializer;->access$2400(Lcom/google/android/videos/player/DirectorInitializer;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/player/DirectorInitializer$5;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    move-object/from16 v18, v0

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->showTitle:Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/google/android/videos/player/DirectorInitializer;->access$2500(Lcom/google/android/videos/player/DirectorInitializer;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/player/DirectorInitializer$5;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->posterUri:Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Lcom/google/android/videos/player/DirectorInitializer;->access$2600(Lcom/google/android/videos/player/DirectorInitializer;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/player/DirectorInitializer$5;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    move-object/from16 v20, v0

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->pinnedStorage:I
    invoke-static/range {v20 .. v20}, Lcom/google/android/videos/player/DirectorInitializer;->access$2700(Lcom/google/android/videos/player/DirectorInitializer;)I

    move-result v20

    invoke-direct/range {v3 .. v20}, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;-><init>(ZZJIILcom/google/android/videos/streams/Streams;Lcom/google/android/videos/drm/DrmManager$Identifiers;[BIILjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v2, v3}, Lcom/google/android/videos/player/DirectorInitializer$Listener;->onInitialized(Lcom/google/android/videos/player/DirectorInitializer$InitializationData;)V

    .line 749
    return-void
.end method
