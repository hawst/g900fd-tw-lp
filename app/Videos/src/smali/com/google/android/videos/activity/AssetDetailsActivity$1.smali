.class Lcom/google/android/videos/activity/AssetDetailsActivity$1;
.super Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;
.source "AssetDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/activity/AssetDetailsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/activity/AssetDetailsActivity;

.field final synthetic val$useTransitions:Z


# direct methods
.method constructor <init>(Lcom/google/android/videos/activity/AssetDetailsActivity;Landroid/content/Context;Z)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$1;->this$0:Lcom/google/android/videos/activity/AssetDetailsActivity;

    iput-boolean p3, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$1;->val$useTransitions:Z

    invoke-direct {p0, p2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected addBackgroundView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    .line 136
    iget-object v3, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$1;->this$0:Lcom/google/android/videos/activity/AssetDetailsActivity;

    const v2, 0x7f040017

    invoke-virtual {p1, v2, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, v3, Lcom/google/android/videos/activity/AssetDetailsActivity;->backgroundView:Landroid/view/ViewGroup;

    .line 138
    iget-object v2, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$1;->this$0:Lcom/google/android/videos/activity/AssetDetailsActivity;

    iget-object v2, v2, Lcom/google/android/videos/activity/AssetDetailsActivity;->backgroundView:Landroid/view/ViewGroup;

    invoke-virtual {p2, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 139
    iget-object v3, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$1;->this$0:Lcom/google/android/videos/activity/AssetDetailsActivity;

    iget-object v2, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$1;->this$0:Lcom/google/android/videos/activity/AssetDetailsActivity;

    iget-object v2, v2, Lcom/google/android/videos/activity/AssetDetailsActivity;->backgroundView:Landroid/view/ViewGroup;

    const v4, 0x7f0f00c1

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/ui/FocusedImageView;

    iput-object v2, v3, Lcom/google/android/videos/activity/AssetDetailsActivity;->backgroundImageView:Lcom/google/android/videos/ui/FocusedImageView;

    .line 140
    iget-object v2, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$1;->this$0:Lcom/google/android/videos/activity/AssetDetailsActivity;

    iget-object v2, v2, Lcom/google/android/videos/activity/AssetDetailsActivity;->backgroundView:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 141
    .local v0, "params":Landroid/view/ViewGroup$LayoutParams;
    iget-object v2, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$1;->this$0:Lcom/google/android/videos/activity/AssetDetailsActivity;

    invoke-virtual {v2}, Lcom/google/android/videos/activity/AssetDetailsActivity;->getScreenshotHeight()I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 143
    iget-boolean v2, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$1;->val$useTransitions:Z

    if-eqz v2, :cond_0

    .line 147
    move-object v1, p2

    .local v1, "parent":Landroid/view/ViewParent;
    :goto_0
    if-eqz v1, :cond_0

    move-object v2, v1

    .line 148
    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2, v5}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    .line 149
    iget-object v2, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$1;->this$0:Lcom/google/android/videos/activity/AssetDetailsActivity;

    iget-object v2, v2, Lcom/google/android/videos/activity/AssetDetailsActivity;->playLayout:Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;

    if-ne v1, v2, :cond_1

    .line 154
    .end local v1    # "parent":Landroid/view/ViewParent;
    :cond_0
    return-void

    .line 147
    .restart local v1    # "parent":Landroid/view/ViewParent;
    :cond_1
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_0
.end method

.method protected addContentView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 158
    iget-object v1, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$1;->this$0:Lcom/google/android/videos/activity/AssetDetailsActivity;

    const v0, 0x7f0400b8

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, v1, Lcom/google/android/videos/activity/AssetDetailsActivity;->contentLayout:Landroid/support/v7/widget/RecyclerView;

    .line 160
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$1;->this$0:Lcom/google/android/videos/activity/AssetDetailsActivity;

    iget-object v0, v0, Lcom/google/android/videos/activity/AssetDetailsActivity;->contentLayout:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 161
    return-void
.end method

.method protected addHeroView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 0
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 166
    return-void
.end method

.method protected getHeaderHeight()I
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/videos/activity/AssetDetailsActivity$1;->this$0:Lcom/google/android/videos/activity/AssetDetailsActivity;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->getHeaderHeight()I

    move-result v0

    return v0
.end method

.method protected getTabMode()I
    .locals 1

    .prologue
    .line 180
    const/4 v0, 0x2

    return v0
.end method

.method protected hasViewPager()Z
    .locals 1

    .prologue
    .line 185
    const/4 v0, 0x0

    return v0
.end method

.method protected useBuiltInActionBar()Z
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x1

    return v0
.end method
