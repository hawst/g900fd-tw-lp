.class public final Lcom/google/android/videos/pano/binders/MovieBinder;
.super Ljava/lang/Object;
.source "MovieBinder.java"

# interfaces
.implements Lcom/google/android/repolib/common/Binder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Binder",
        "<",
        "Lcom/google/android/videos/pano/model/MovieItem;",
        "Lcom/google/android/videos/pano/ui/MovieCardView;",
        ">;"
    }
.end annotation


# instance fields
.field private final activity:Landroid/app/Activity;

.field private final bitmapRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/videos/async/Requester;Landroid/app/Activity;)V
    .locals 0
    .param p2, "activity"    # Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/app/Activity;",
            ")V"
        }
    .end annotation

    .prologue
    .line 24
    .local p1, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/async/ControllableRequest<Landroid/net/Uri;>;Landroid/graphics/Bitmap;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/android/videos/pano/binders/MovieBinder;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    .line 26
    iput-object p2, p0, Lcom/google/android/videos/pano/binders/MovieBinder;->activity:Landroid/app/Activity;

    .line 27
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/pano/binders/MovieBinder;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/binders/MovieBinder;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/videos/pano/binders/MovieBinder;->activity:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public bind(Lcom/google/android/videos/pano/model/MovieItem;Lcom/google/android/videos/pano/ui/MovieCardView;)V
    .locals 7
    .param p1, "item"    # Lcom/google/android/videos/pano/model/MovieItem;
    .param p2, "view"    # Lcom/google/android/videos/pano/ui/MovieCardView;

    .prologue
    .line 31
    invoke-virtual {p1}, Lcom/google/android/videos/pano/model/MovieItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/google/android/videos/pano/ui/MovieCardView;->setTitle(Ljava/lang/CharSequence;)V

    .line 32
    invoke-virtual {p1}, Lcom/google/android/videos/pano/model/MovieItem;->getSubtitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/videos/pano/model/MovieItem;->getBadgeResourceId()I

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/videos/pano/model/MovieItem;->getDurationInSeconds()I

    move-result v3

    invoke-virtual {p1}, Lcom/google/android/videos/pano/model/MovieItem;->getCheapestOffer()Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/videos/pano/model/MovieItem;->getStarRating()F

    move-result v5

    move-object v0, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/videos/pano/ui/MovieCardView;->setExtra(Ljava/lang/String;IILcom/google/android/videos/utils/OfferUtil$CheapestOffer;F)V

    .line 34
    invoke-virtual {p1}, Lcom/google/android/videos/pano/model/MovieItem;->getImageResourceId()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/google/android/videos/pano/ui/MovieCardView;->setImageResource(I)V

    .line 35
    invoke-virtual {p1}, Lcom/google/android/videos/pano/model/MovieItem;->getProgress()F

    move-result v0

    invoke-virtual {p2, v0}, Lcom/google/android/videos/pano/ui/MovieCardView;->setProgress(F)V

    .line 36
    invoke-virtual {p1}, Lcom/google/android/videos/pano/model/MovieItem;->getImageUri()Ljava/lang/String;

    move-result-object v6

    .line 38
    .local v6, "imageUri":Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 39
    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/pano/binders/MovieBinder;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    invoke-virtual {p2, v0, v1}, Lcom/google/android/videos/pano/ui/MovieCardView;->setImageUri(Landroid/net/Uri;Lcom/google/android/videos/async/Requester;)V

    .line 41
    :cond_0
    new-instance v0, Lcom/google/android/videos/pano/binders/MovieBinder$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/videos/pano/binders/MovieBinder$1;-><init>(Lcom/google/android/videos/pano/binders/MovieBinder;Lcom/google/android/videos/pano/model/MovieItem;Lcom/google/android/videos/pano/ui/MovieCardView;)V

    invoke-virtual {p2, v0}, Lcom/google/android/videos/pano/ui/MovieCardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    return-void
.end method

.method public bridge synthetic bind(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 19
    check-cast p1, Lcom/google/android/videos/pano/model/MovieItem;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/videos/pano/ui/MovieCardView;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pano/binders/MovieBinder;->bind(Lcom/google/android/videos/pano/model/MovieItem;Lcom/google/android/videos/pano/ui/MovieCardView;)V

    return-void
.end method
