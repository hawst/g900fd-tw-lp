.class interface abstract Lcom/google/android/videos/player/LocalPlaybackViewHolder;
.super Ljava/lang/Object;
.source "LocalPlaybackViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;,
        Lcom/google/android/videos/player/LocalPlaybackViewHolder$PrimaryScreenViewHolder;
    }
.end annotation


# virtual methods
.method public abstract getPlayerView()Lcom/google/android/videos/player/PlayerView;
.end method

.method public abstract getRemoteScreenInfoOverlay()Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;
.end method

.method public abstract getSubtitlesOverlay()Lcom/google/android/videos/player/overlay/SubtitlesOverlay;
.end method

.method public abstract getTaggingKnowledgeView()Lcom/google/android/videos/tagging/KnowledgeView;
.end method

.method public abstract getTaggingKnowledgeViewResources()Landroid/content/res/Resources;
.end method

.method public abstract getTaglessKnowledgeView()Lcom/google/android/videos/tagging/KnowledgeView;
.end method

.method public abstract release()V
.end method
