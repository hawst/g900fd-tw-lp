.class public Lcom/google/android/videos/ui/PinHelper$SingleDownloadSettingDialogFragment;
.super Lcom/google/android/videos/ui/SelectionDialogFragment;
.source "PinHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/PinHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SingleDownloadSettingDialogFragment"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 415
    invoke-direct {p0}, Lcom/google/android/videos/ui/SelectionDialogFragment;-><init>()V

    return-void
.end method

.method public static showAudioPreferenceInstance(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "manager"    # Landroid/support/v4/app/FragmentManager;
    .param p2, "preferences"    # Landroid/content/SharedPreferences;
    .param p3, "account"    # Ljava/lang/String;
    .param p4, "videoId"    # Ljava/lang/String;
    .param p5, "pinOnSelection"    # Z

    .prologue
    .line 441
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 442
    .local v10, "resources":Landroid/content/res/Resources;
    const v0, 0x7f0b0237

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, Lcom/google/android/videos/utils/SettingsUtil;->getAllAudioPreferenceTexts(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v4

    const v0, 0x7f120006

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, p2}, Lcom/google/android/videos/utils/SettingsUtil;->getAudioPreferenceValue(Landroid/content/Context;Landroid/content/SharedPreferences;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "audio_language"

    const/4 v8, 0x4

    move-object v0, p1

    move-object v1, p3

    move-object v2, p4

    move/from16 v9, p5

    invoke-static/range {v0 .. v9}, Lcom/google/android/videos/ui/PinHelper$SingleDownloadSettingDialogFragment;->showInstance(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    .line 448
    return-void
.end method

.method private static showInstance(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 3
    .param p0, "manager"    # Landroid/support/v4/app/FragmentManager;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "texts"    # [Ljava/lang/String;
    .param p5, "values"    # [Ljava/lang/String;
    .param p6, "initialValue"    # Ljava/lang/String;
    .param p7, "preferenceName"    # Ljava/lang/String;
    .param p8, "preferenceFlag"    # I
    .param p9, "pinOnSelection"    # Z

    .prologue
    .line 453
    invoke-static {p3, p4, p5, p6}, Lcom/google/android/videos/ui/SelectionDialogFragment;->buildArguments(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 454
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "preference_name"

    invoke-virtual {v0, v2, p7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    const-string v2, "preference_flag"

    invoke-virtual {v0, v2, p8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 456
    const-string v2, "video_id"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    const-string v2, "pin_on_selection"

    invoke-virtual {v0, v2, p9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 459
    new-instance v1, Lcom/google/android/videos/ui/PinHelper$SingleDownloadSettingDialogFragment;

    invoke-direct {v1}, Lcom/google/android/videos/ui/PinHelper$SingleDownloadSettingDialogFragment;-><init>()V

    .line 460
    .local v1, "instance":Lcom/google/android/videos/ui/PinHelper$SingleDownloadSettingDialogFragment;
    invoke-virtual {v1, v0}, Lcom/google/android/videos/ui/PinHelper$SingleDownloadSettingDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 461
    const-string v2, "DownloadSingleSettingDialog"

    invoke-virtual {v1, p0, v2}, Lcom/google/android/videos/ui/PinHelper$SingleDownloadSettingDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 462
    return-void
.end method

.method public static showNetworkPreferenceInstance(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "manager"    # Landroid/support/v4/app/FragmentManager;
    .param p2, "preferences"    # Landroid/content/SharedPreferences;
    .param p3, "account"    # Ljava/lang/String;
    .param p4, "videoId"    # Ljava/lang/String;
    .param p5, "pinOnSelection"    # Z

    .prologue
    .line 419
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 420
    .local v10, "resources":Landroid/content/res/Resources;
    const v0, 0x7f0b0128

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f120001

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    const v0, 0x7f120002

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, p2}, Lcom/google/android/videos/utils/SettingsUtil;->getDownloadNetworkPreferenceValue(Landroid/content/Context;Landroid/content/SharedPreferences;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "download_policy"

    const/4 v8, 0x1

    move-object v0, p1

    move-object v1, p3

    move-object v2, p4

    move/from16 v9, p5

    invoke-static/range {v0 .. v9}, Lcom/google/android/videos/ui/PinHelper$SingleDownloadSettingDialogFragment;->showInstance(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    .line 426
    return-void
.end method

.method public static showQualityPreferenceInstance(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "manager"    # Landroid/support/v4/app/FragmentManager;
    .param p2, "preferences"    # Landroid/content/SharedPreferences;
    .param p3, "account"    # Ljava/lang/String;
    .param p4, "videoId"    # Ljava/lang/String;
    .param p5, "pinOnSelection"    # Z

    .prologue
    .line 430
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 431
    .local v10, "resources":Landroid/content/res/Resources;
    const v0, 0x7f0b012a

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f120003

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    const v0, 0x7f120004

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, p2}, Lcom/google/android/videos/utils/SettingsUtil;->getDownloadQualityPreferenceValue(Landroid/content/Context;Landroid/content/SharedPreferences;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "download_quality"

    const/4 v8, 0x2

    move-object v0, p1

    move-object v1, p3

    move-object v2, p4

    move/from16 v9, p5

    invoke-static/range {v0 .. v9}, Lcom/google/android/videos/ui/PinHelper$SingleDownloadSettingDialogFragment;->showInstance(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    .line 437
    return-void
.end method


# virtual methods
.method protected onSelection(Ljava/lang/String;)V
    .locals 9
    .param p1, "selectedValue"    # Ljava/lang/String;

    .prologue
    .line 471
    invoke-virtual {p0}, Lcom/google/android/videos/ui/PinHelper$SingleDownloadSettingDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 472
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    if-nez v0, :cond_0

    .line 500
    :goto_0
    return-void

    .line 476
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/ui/PinHelper$SingleDownloadSettingDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    .line 477
    .local v2, "arguments":Landroid/os/Bundle;
    invoke-static {v0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v4

    .line 478
    .local v4, "globals":Lcom/google/android/videos/VideosGlobals;
    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v5

    .line 480
    .local v5, "preferences":Landroid/content/SharedPreferences;
    const-string v6, "pin_on_selection"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 485
    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "preference_name"

    invoke-virtual {v2, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 491
    :cond_1
    const-string v6, "download_setting_shown_flags"

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 492
    .local v1, "alreadyShownFlags":I
    const-string v6, "preference_flag"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    or-int v3, v1, v6

    .line 493
    .local v3, "combinedShownFlags":I
    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "download_setting_shown_flags"

    invoke-interface {v6, v7, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "preference_name"

    invoke-virtual {v2, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 497
    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v6

    const-string v7, "authAccount"

    invoke-virtual {v2, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "video_id"

    invoke-virtual {v2, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    # invokes: Lcom/google/android/videos/ui/PinHelper;->requestPin(Landroid/app/Activity;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v6, v7, v8}, Lcom/google/android/videos/ui/PinHelper;->access$200(Landroid/app/Activity;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onSelectionCanceled()V
    .locals 1

    .prologue
    .line 466
    invoke-virtual {p0}, Lcom/google/android/videos/ui/PinHelper$SingleDownloadSettingDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    # invokes: Lcom/google/android/videos/ui/PinHelper;->onPinCanceled(Landroid/app/Activity;)V
    invoke-static {v0}, Lcom/google/android/videos/ui/PinHelper;->access$300(Landroid/app/Activity;)V

    .line 467
    return-void
.end method
