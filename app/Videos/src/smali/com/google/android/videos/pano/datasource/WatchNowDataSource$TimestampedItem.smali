.class Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;
.super Ljava/lang/Object;
.source "WatchNowDataSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/datasource/WatchNowDataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TimestampedItem"
.end annotation


# instance fields
.field public final item:Lcom/google/android/videos/pano/model/VideoItem;

.field public final lastActivityTimestamp:J


# direct methods
.method private constructor <init>(Lcom/google/android/videos/pano/model/VideoItem;J)V
    .locals 0
    .param p1, "item"    # Lcom/google/android/videos/pano/model/VideoItem;
    .param p2, "lastActivityTimestamp"    # J

    .prologue
    .line 463
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 464
    iput-object p1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;->item:Lcom/google/android/videos/pano/model/VideoItem;

    .line 465
    iput-wide p2, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;->lastActivityTimestamp:J

    .line 466
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/pano/model/VideoItem;JLcom/google/android/videos/pano/datasource/WatchNowDataSource$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/pano/model/VideoItem;
    .param p2, "x1"    # J
    .param p4, "x2"    # Lcom/google/android/videos/pano/datasource/WatchNowDataSource$1;

    .prologue
    .line 459
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;-><init>(Lcom/google/android/videos/pano/model/VideoItem;J)V

    return-void
.end method
