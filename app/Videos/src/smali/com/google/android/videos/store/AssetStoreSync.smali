.class public Lcom/google/android/videos/store/AssetStoreSync;
.super Ljava/lang/Object;
.source "AssetStoreSync.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;,
        Lcom/google/android/videos/store/AssetStoreSync$DeleteOrphanedMetadataTask;,
        Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;,
        Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;,
        Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;,
        Lcom/google/android/videos/store/AssetStoreSync$SyncAssetMetadataTask;,
        Lcom/google/android/videos/store/AssetStoreSync$OrphanShowBannerQuery;,
        Lcom/google/android/videos/store/AssetStoreSync$OrphanShowPosterQuery;,
        Lcom/google/android/videos/store/AssetStoreSync$OrphanVideoScreenshotQuery;,
        Lcom/google/android/videos/store/AssetStoreSync$OrphanVideoPosterQuery;,
        Lcom/google/android/videos/store/AssetStoreSync$StoredImageQuery;,
        Lcom/google/android/videos/store/AssetStoreSync$MissingShowBannerQuery;,
        Lcom/google/android/videos/store/AssetStoreSync$MissingShowPosterQuery;,
        Lcom/google/android/videos/store/AssetStoreSync$MissingVideoScreenshotQuery;,
        Lcom/google/android/videos/store/AssetStoreSync$MissingVideoPosterQuery;,
        Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;
    }
.end annotation


# static fields
.field private static final ASSETS_EQUAL_COLUMNS:[Ljava/lang/String;

.field private static final SEASONS_EQUAL_COLUMNS:[Ljava/lang/String;

.field private static final SHOWS_EQUAL_COLUMNS:[Ljava/lang/String;

.field private static final VIDEOS_EQUAL_COLUMNS:[Ljava/lang/String;


# instance fields
.field private final assetImageUriCreator:Lcom/google/android/videos/ui/AssetImageUriCreator;

.field private final byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;

.field private final conditionalHttpEntityRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/ConditionalHttpRequest",
            "<",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;",
            "Lcom/google/android/videos/async/ConditionalHttpResponse",
            "<",
            "Lorg/apache/http/HttpEntity;",
            ">;>;"
        }
    .end annotation
.end field

.field private final config:Lcom/google/android/videos/Config;

.field private final configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

.field private final database:Lcom/google/android/videos/store/Database;

.field private final networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

.field private final screenshotFileStore:Lcom/google/android/videos/store/FileStore;

.field private final showBannerFileStore:Lcom/google/android/videos/store/FileStore;

.field private final showPosterFileStore:Lcom/google/android/videos/store/FileStore;

.field private final syncAssetsRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final syncBitmaps:Z

.field private final syncTaskManager:Lcom/google/android/videos/store/SyncTaskManager;

.field private final videoPosterFileStore:Lcom/google/android/videos/store/FileStore;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 309
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "season_id"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/videos/store/AssetStoreSync;->SEASONS_EQUAL_COLUMNS:[Ljava/lang/String;

    .line 313
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "shows_id"

    aput-object v1, v0, v2

    const-string v1, "shows_banner_uri"

    aput-object v1, v0, v3

    const-string v1, "shows_poster_uri"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/videos/store/AssetStoreSync;->SHOWS_EQUAL_COLUMNS:[Ljava/lang/String;

    .line 319
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "video_id"

    aput-object v1, v0, v2

    const-string v1, "poster_uri"

    aput-object v1, v0, v3

    const-string v1, "screenshot_uri"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/videos/store/AssetStoreSync;->VIDEOS_EQUAL_COLUMNS:[Ljava/lang/String;

    .line 325
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "assets_type"

    aput-object v1, v0, v2

    const-string v1, "assets_id"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/videos/store/AssetStoreSync;->ASSETS_EQUAL_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/videos/Config;Lcom/google/android/videos/utils/ByteArrayPool;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/FileStore;Lcom/google/android/videos/store/FileStore;Lcom/google/android/videos/store/FileStore;Lcom/google/android/videos/store/FileStore;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/SyncTaskManager;Lcom/google/android/videos/ui/AssetImageUriCreator;Z)V
    .locals 1
    .param p1, "config"    # Lcom/google/android/videos/Config;
    .param p2, "byteArrayPool"    # Lcom/google/android/videos/utils/ByteArrayPool;
    .param p3, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;
    .param p4, "configurationStore"    # Lcom/google/android/videos/store/ConfigurationStore;
    .param p5, "database"    # Lcom/google/android/videos/store/Database;
    .param p6, "videoPosterFileStore"    # Lcom/google/android/videos/store/FileStore;
    .param p7, "screenshotFileStore"    # Lcom/google/android/videos/store/FileStore;
    .param p8, "showPosterFileStore"    # Lcom/google/android/videos/store/FileStore;
    .param p9, "showBannerFileStore"    # Lcom/google/android/videos/store/FileStore;
    .param p12, "syncTaskManager"    # Lcom/google/android/videos/store/SyncTaskManager;
    .param p13, "assetImageUriCreator"    # Lcom/google/android/videos/ui/AssetImageUriCreator;
    .param p14, "syncBitmaps"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/Config;",
            "Lcom/google/android/videos/utils/ByteArrayPool;",
            "Lcom/google/android/videos/utils/NetworkStatus;",
            "Lcom/google/android/videos/store/ConfigurationStore;",
            "Lcom/google/android/videos/store/Database;",
            "Lcom/google/android/videos/store/FileStore;",
            "Lcom/google/android/videos/store/FileStore;",
            "Lcom/google/android/videos/store/FileStore;",
            "Lcom/google/android/videos/store/FileStore;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/ConditionalHttpRequest",
            "<",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;",
            "Lcom/google/android/videos/async/ConditionalHttpResponse",
            "<",
            "Lorg/apache/http/HttpEntity;",
            ">;>;",
            "Lcom/google/android/videos/store/SyncTaskManager;",
            "Lcom/google/android/videos/ui/AssetImageUriCreator;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 361
    .local p10, "syncAssetsRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;>;"
    .local p11, "conditionalHttpEntityRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/async/ConditionalHttpRequest<Lorg/apache/http/client/methods/HttpUriRequest;>;Lcom/google/android/videos/async/ConditionalHttpResponse<Lorg/apache/http/HttpEntity;>;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 362
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/Config;

    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync;->config:Lcom/google/android/videos/Config;

    .line 363
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/utils/ByteArrayPool;

    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;

    .line 364
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/utils/NetworkStatus;

    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 365
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/ConfigurationStore;

    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    .line 366
    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/Database;

    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;

    .line 367
    invoke-static {p6}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/FileStore;

    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync;->videoPosterFileStore:Lcom/google/android/videos/store/FileStore;

    .line 368
    invoke-static {p7}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/FileStore;

    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync;->screenshotFileStore:Lcom/google/android/videos/store/FileStore;

    .line 369
    invoke-static {p8}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/FileStore;

    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync;->showPosterFileStore:Lcom/google/android/videos/store/FileStore;

    .line 370
    invoke-static {p9}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/FileStore;

    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync;->showBannerFileStore:Lcom/google/android/videos/store/FileStore;

    .line 371
    invoke-static {p10}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync;->syncAssetsRequester:Lcom/google/android/videos/async/Requester;

    .line 372
    invoke-static {p11}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync;->conditionalHttpEntityRequester:Lcom/google/android/videos/async/Requester;

    .line 374
    invoke-static {p12}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/SyncTaskManager;

    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync;->syncTaskManager:Lcom/google/android/videos/store/SyncTaskManager;

    .line 375
    iput-object p13, p0, Lcom/google/android/videos/store/AssetStoreSync;->assetImageUriCreator:Lcom/google/android/videos/ui/AssetImageUriCreator;

    .line 376
    iput-boolean p14, p0, Lcom/google/android/videos/store/AssetStoreSync;->syncBitmaps:Z

    .line 377
    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/ui/AssetImageUriCreator;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/AssetStoreSync;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync;->assetImageUriCreator:Lcom/google/android/videos/ui/AssetImageUriCreator;

    return-object v0
.end method

.method static synthetic access$1100()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/google/android/videos/store/AssetStoreSync;->VIDEOS_EQUAL_COLUMNS:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/google/android/videos/store/AssetStoreSync;->ASSETS_EQUAL_COLUMNS:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/store/AssetStoreSync;
    .param p1, "x1"    # I
    .param p2, "x2"    # Lcom/google/android/videos/store/SyncTaskManager$SharedStates;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/store/AssetStoreSync;->maybeSyncShowPoster(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/store/AssetStoreSync;
    .param p1, "x1"    # I
    .param p2, "x2"    # Lcom/google/android/videos/store/SyncTaskManager$SharedStates;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/store/AssetStoreSync;->maybeSyncShowBanner(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1500()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/google/android/videos/store/AssetStoreSync;->SHOWS_EQUAL_COLUMNS:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1600()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/google/android/videos/store/AssetStoreSync;->SEASONS_EQUAL_COLUMNS:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/utils/NetworkStatus;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/AssetStoreSync;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/async/Requester;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/AssetStoreSync;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync;->conditionalHttpEntityRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/utils/ByteArrayPool;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/AssetStoreSync;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/SyncTaskManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/AssetStoreSync;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync;->syncTaskManager:Lcom/google/android/videos/store/SyncTaskManager;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/store/AssetStoreSync;
    .param p1, "x1"    # I
    .param p2, "x2"    # Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/store/AssetStoreSync;->removeOrphanedBitmaps(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/ConfigurationStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/AssetStoreSync;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/async/Requester;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/AssetStoreSync;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync;->syncAssetsRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/Config;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/AssetStoreSync;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync;->config:Lcom/google/android/videos/Config;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/videos/store/AssetStoreSync;Ljava/util/List;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ILjava/util/List;)Z
    .locals 2
    .param p0, "x0"    # Lcom/google/android/videos/store/AssetStoreSync;
    .param p1, "x1"    # Ljava/util/List;
    .param p2, "x2"    # J
    .param p4, "x3"    # Ljava/lang/String;
    .param p5, "x4"    # Ljava/lang/String;
    .param p6, "x5"    # Ljava/lang/String;
    .param p7, "x6"    # Ljava/util/List;
    .param p8, "x7"    # Ljava/lang/String;
    .param p9, "x8"    # I
    .param p10, "x9"    # Ljava/util/List;

    .prologue
    .line 60
    invoke-direct/range {p0 .. p10}, Lcom/google/android/videos/store/AssetStoreSync;->filterUpToDateIds(Ljava/util/List;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ILjava/util/List;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/store/AssetStoreSync;
    .param p1, "x1"    # I
    .param p2, "x2"    # Lcom/google/android/videos/store/SyncTaskManager$SharedStates;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/store/AssetStoreSync;->maybeSyncVideoPoster(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/store/AssetStoreSync;
    .param p1, "x1"    # I
    .param p2, "x2"    # Lcom/google/android/videos/store/SyncTaskManager$SharedStates;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/store/AssetStoreSync;->maybeSyncVideoScreenshot(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/Database;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/AssetStoreSync;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;

    return-object v0
.end method

.method private filterUpToDateIds(Ljava/util/List;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ILjava/util/List;)Z
    .locals 22
    .param p2, "validityMillis"    # J
    .param p4, "table"    # Ljava/lang/String;
    .param p5, "idColumn"    # Ljava/lang/String;
    .param p6, "syncTimestampColumn"    # Ljava/lang/String;
    .param p8, "account"    # Ljava/lang/String;
    .param p9, "assetType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 472
    .local p1, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p7, "upToDateIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p10, "upToDateIdsNeedingUserAssetsRefresh":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "%s LEFT JOIN user_assets ON user_assets_account = ? AND user_assets_type = %d AND user_assets_id = %s"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p4, v7, v8

    const/4 v8, 0x1

    invoke-static/range {p9 .. p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    aput-object p5, v7, v8

    invoke-static {v2, v5, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 474
    .local v3, "source":Ljava/lang/String;
    const/4 v2, 0x3

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p6, v4, v2

    const/4 v2, 0x1

    aput-object p5, v4, v2

    const/4 v2, 0x2

    const-string v5, "metadata_timestamp_at_last_sync"

    aput-object v5, v4, v2

    .line 480
    .local v4, "projection":[Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    .line 481
    .local v18, "nowTimestamp":J
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v14

    .line 482
    .local v14, "count":I
    new-array v2, v14, [Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Ljava/lang/String;

    .line 485
    .local v10, "allIds":[Ljava/lang/String;
    const/4 v13, 0x0

    .local v13, "batchStart":I
    :goto_0
    if-ge v13, v14, :cond_4

    .line 486
    add-int/lit8 v2, v13, 0x32

    invoke-static {v14, v2}, Ljava/lang/Math;->min(II)I

    move-result v11

    .line 487
    .local v11, "batchEnd":I
    sub-int v12, v11, v13

    .line 488
    .local v12, "batchSize":I
    add-int/lit8 v2, v12, 0x1

    new-array v6, v2, [Ljava/lang/String;

    .line 489
    .local v6, "whereArgs":[Ljava/lang/String;
    const/4 v2, 0x0

    aput-object p8, v6, v2

    .line 490
    const/4 v2, 0x1

    invoke-static {v10, v13, v6, v2, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 491
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v2}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-static {v0, v12}, Lcom/google/android/videos/utils/DbUtils;->buildInMultipleParamsClause(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 494
    .local v15, "cursor":Landroid/database/Cursor;
    :cond_0
    :goto_1
    :try_start_0
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 495
    const/4 v2, 0x0

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 496
    .local v20, "syncTimestamp":J
    cmp-long v2, v20, v18

    if-gtz v2, :cond_2

    add-long v8, v20, p2

    cmp-long v2, v18, v8

    if-gtz v2, :cond_2

    const/16 v17, 0x1

    .line 498
    .local v17, "upToDate":Z
    :goto_2
    if-eqz v17, :cond_0

    .line 501
    const/4 v2, 0x1

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 502
    .local v16, "id":Ljava/lang/String;
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 503
    if-eqz p7, :cond_1

    .line 504
    move-object/from16 v0, p7

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 506
    :cond_1
    if-eqz p10, :cond_0

    const/4 v2, 0x2

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    cmp-long v2, v8, v20

    if-eqz v2, :cond_0

    .line 507
    move-object/from16 v0, p10

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 511
    .end local v16    # "id":Ljava/lang/String;
    .end local v17    # "upToDate":Z
    .end local v20    # "syncTimestamp":J
    :catchall_0
    move-exception v2

    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    throw v2

    .line 496
    .restart local v20    # "syncTimestamp":J
    :cond_2
    const/16 v17, 0x0

    goto :goto_2

    .line 511
    .end local v20    # "syncTimestamp":J
    :cond_3
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 485
    move v13, v11

    goto :goto_0

    .line 514
    .end local v6    # "whereArgs":[Ljava/lang/String;
    .end local v11    # "batchEnd":I
    .end local v12    # "batchSize":I
    .end local v15    # "cursor":Landroid/database/Cursor;
    :cond_4
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    return v2
.end method

.method private maybeSyncShowBanner(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V
    .locals 15
    .param p1, "parentPriority"    # I
    .param p3, "showId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/store/SyncTaskManager$SharedStates",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 603
    .local p2, "states":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<*>;"
    iget-boolean v1, p0, Lcom/google/android/videos/store/AssetStoreSync;->syncBitmaps:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "local:sb:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->addScheduledAsset(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 620
    :cond_0
    :goto_0
    return-void

    .line 606
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v1}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "shows"

    sget-object v3, Lcom/google/android/videos/store/AssetStoreSync$MissingShowBannerQuery;->PROJECTION:[Ljava/lang/String;

    const-string v4, "NOT shows_banner_synced AND shows_id = ?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p3, v5, v7

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "1"

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 610
    .local v14, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 611
    const/4 v1, 0x0

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 612
    .local v6, "showBannerUri":Landroid/net/Uri;
    new-instance v1, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;

    add-int/lit8 v3, p1, -0x1

    iget-object v7, p0, Lcom/google/android/videos/store/AssetStoreSync;->showBannerFileStore:Lcom/google/android/videos/store/FileStore;

    const-string v8, "shows"

    const-string v9, "shows_banner_synced"

    const-string v10, "shows_id"

    const-string v11, "show_banners"

    const-string v12, "banner_show_id"

    const/16 v13, 0xa

    move-object v2, p0

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    invoke-direct/range {v1 .. v13}, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;-><init>(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/videos/store/FileStore;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v1}, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->schedule()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 618
    .end local v6    # "showBannerUri":Landroid/net/Uri;
    :cond_2
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private maybeSyncShowPoster(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V
    .locals 15
    .param p1, "parentPriority"    # I
    .param p3, "showId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/store/SyncTaskManager$SharedStates",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 577
    .local p2, "states":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<*>;"
    iget-boolean v1, p0, Lcom/google/android/videos/store/AssetStoreSync;->syncBitmaps:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "local:sp:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->addScheduledAsset(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 594
    :cond_0
    :goto_0
    return-void

    .line 580
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v1}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "shows"

    sget-object v3, Lcom/google/android/videos/store/AssetStoreSync$MissingShowPosterQuery;->PROJECTION:[Ljava/lang/String;

    const-string v4, "NOT shows_poster_synced AND shows_id = ?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p3, v5, v7

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "1"

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 584
    .local v14, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 585
    const/4 v1, 0x0

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 586
    .local v6, "showPosterUri":Landroid/net/Uri;
    new-instance v1, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;

    add-int/lit8 v3, p1, -0x2

    iget-object v7, p0, Lcom/google/android/videos/store/AssetStoreSync;->showPosterFileStore:Lcom/google/android/videos/store/FileStore;

    const-string v8, "shows"

    const-string v9, "shows_poster_synced"

    const-string v10, "shows_id"

    const-string v11, "show_posters"

    const-string v12, "poster_show_id"

    const/16 v13, 0x8

    move-object v2, p0

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    invoke-direct/range {v1 .. v13}, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;-><init>(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/videos/store/FileStore;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v1}, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->schedule()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 592
    .end local v6    # "showPosterUri":Landroid/net/Uri;
    :cond_2
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private maybeSyncVideoPoster(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V
    .locals 15
    .param p1, "parentPriority"    # I
    .param p3, "videoId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/store/SyncTaskManager$SharedStates",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 524
    .local p2, "states":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<*>;"
    iget-boolean v1, p0, Lcom/google/android/videos/store/AssetStoreSync;->syncBitmaps:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "local:p:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->addScheduledAsset(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 541
    :cond_0
    :goto_0
    return-void

    .line 527
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v1}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "videos"

    sget-object v3, Lcom/google/android/videos/store/AssetStoreSync$MissingVideoPosterQuery;->PROJECTION:[Ljava/lang/String;

    const-string v4, "NOT poster_synced AND video_id = ?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p3, v5, v7

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 531
    .local v14, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 532
    const/4 v1, 0x0

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 533
    .local v6, "posterUri":Landroid/net/Uri;
    new-instance v1, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;

    add-int/lit8 v3, p1, -0x2

    iget-object v7, p0, Lcom/google/android/videos/store/AssetStoreSync;->videoPosterFileStore:Lcom/google/android/videos/store/FileStore;

    const-string v8, "videos"

    const-string v9, "poster_synced"

    const-string v10, "video_id"

    const-string v11, "posters"

    const-string v12, "poster_video_id"

    const/4 v13, 0x7

    move-object v2, p0

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    invoke-direct/range {v1 .. v13}, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;-><init>(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/videos/store/FileStore;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v1}, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->schedule()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 539
    .end local v6    # "posterUri":Landroid/net/Uri;
    :cond_2
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private maybeSyncVideoScreenshot(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V
    .locals 15
    .param p1, "parentPriority"    # I
    .param p3, "videoId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/store/SyncTaskManager$SharedStates",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 551
    .local p2, "states":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<*>;"
    iget-boolean v1, p0, Lcom/google/android/videos/store/AssetStoreSync;->syncBitmaps:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "local:s:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->addScheduledAsset(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 568
    :cond_0
    :goto_0
    return-void

    .line 554
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v1}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "videos"

    sget-object v3, Lcom/google/android/videos/store/AssetStoreSync$MissingVideoScreenshotQuery;->PROJECTION:[Ljava/lang/String;

    const-string v4, "NOT screenshot_synced AND video_id = ?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p3, v5, v7

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 558
    .local v14, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 559
    const/4 v1, 0x0

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 560
    .local v6, "screenshotUri":Landroid/net/Uri;
    new-instance v1, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;

    add-int/lit8 v3, p1, 0x1

    iget-object v7, p0, Lcom/google/android/videos/store/AssetStoreSync;->screenshotFileStore:Lcom/google/android/videos/store/FileStore;

    const-string v8, "videos"

    const-string v9, "screenshot_synced"

    const-string v10, "video_id"

    const-string v11, "screenshots"

    const-string v12, "screenshot_video_id"

    const/16 v13, 0x9

    move-object v2, p0

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    invoke-direct/range {v1 .. v13}, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;-><init>(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/videos/store/FileStore;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v1}, Lcom/google/android/videos/store/AssetStoreSync$SyncBitmapTask;->schedule()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 566
    .end local v6    # "screenshotUri":Landroid/net/Uri;
    :cond_2
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private removeOrphanedBitmaps(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V
    .locals 11
    .param p1, "priority"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/store/SyncTaskManager$SharedStates",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 624
    .local p2, "states":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<*>;"
    iget-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v0}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "posters"

    sget-object v2, Lcom/google/android/videos/store/AssetStoreSync$OrphanVideoPosterQuery;->POSTERS_VIDEO_ID_COLUMN:[Ljava/lang/String;

    const-string v3, "NOT EXISTS (SELECT NULL FROM videos WHERE video_id = poster_video_id AND poster_uri IS NOT NULL)"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 628
    .local v10, "cursor":Landroid/database/Cursor;
    :goto_0
    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 629
    new-instance v0, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;

    const/4 v1, 0x0

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/videos/store/AssetStoreSync;->videoPosterFileStore:Lcom/google/android/videos/store/FileStore;

    const-string v6, "videos"

    const-string v7, "video_id"

    const-string v8, "posters"

    const-string v9, "poster_video_id"

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v9}, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;-><init>(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;Lcom/google/android/videos/store/FileStore;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;->schedule()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 634
    :catchall_0
    move-exception v0

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 637
    iget-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v0}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "screenshots"

    sget-object v2, Lcom/google/android/videos/store/AssetStoreSync$OrphanVideoScreenshotQuery;->SCREENSHOTS_VIDEO_ID_COLUMN:[Ljava/lang/String;

    const-string v3, "NOT EXISTS (SELECT NULL FROM videos WHERE video_id = screenshot_video_id AND screenshot_uri IS NOT NULL)"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 641
    :goto_1
    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 642
    new-instance v0, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;

    const/4 v1, 0x0

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/videos/store/AssetStoreSync;->screenshotFileStore:Lcom/google/android/videos/store/FileStore;

    const-string v6, "videos"

    const-string v7, "video_id"

    const-string v8, "screenshots"

    const-string v9, "screenshot_video_id"

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v9}, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;-><init>(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;Lcom/google/android/videos/store/FileStore;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;->schedule()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 647
    :catchall_1
    move-exception v0

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 650
    iget-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v0}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "show_posters"

    sget-object v2, Lcom/google/android/videos/store/AssetStoreSync$OrphanShowPosterQuery;->SHOW_POSTERS_SHOW_ID_COLUMN:[Ljava/lang/String;

    const-string v3, "NOT EXISTS (SELECT NULL FROM shows WHERE shows_id = poster_show_id)"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 654
    :goto_2
    :try_start_2
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 655
    new-instance v0, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;

    const/4 v1, 0x0

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/videos/store/AssetStoreSync;->showPosterFileStore:Lcom/google/android/videos/store/FileStore;

    const-string v6, "seasons"

    const-string v7, "show_id"

    const-string v8, "show_posters"

    const-string v9, "poster_show_id"

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v9}, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;-><init>(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;Lcom/google/android/videos/store/FileStore;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;->schedule()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_2

    .line 660
    :catchall_2
    move-exception v0

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 663
    iget-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v0}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "show_banners"

    sget-object v2, Lcom/google/android/videos/store/AssetStoreSync$OrphanShowBannerQuery;->SHOW_BANNERS_SHOW_ID_COLUMN:[Ljava/lang/String;

    const-string v3, "NOT EXISTS (SELECT NULL FROM shows WHERE shows_id = banner_show_id)"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 667
    :goto_3
    :try_start_3
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 668
    new-instance v0, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;

    const/4 v1, 0x0

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/videos/store/AssetStoreSync;->showBannerFileStore:Lcom/google/android/videos/store/FileStore;

    const-string v6, "seasons"

    const-string v7, "show_id"

    const-string v8, "show_banners"

    const-string v9, "banner_show_id"

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v9}, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;-><init>(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;Lcom/google/android/videos/store/FileStore;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;->schedule()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    goto :goto_3

    .line 673
    :catchall_3
    move-exception v0

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_3
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 675
    return-void
.end method


# virtual methods
.method deleteOrphanedMetadata(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V
    .locals 1
    .param p1, "priority"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/store/SyncTaskManager$SharedStates",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 457
    .local p2, "states":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<*>;"
    new-instance v0, Lcom/google/android/videos/store/AssetStoreSync$DeleteOrphanedMetadataTask;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/videos/store/AssetStoreSync$DeleteOrphanedMetadataTask;-><init>(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V

    invoke-virtual {v0}, Lcom/google/android/videos/store/AssetStoreSync$DeleteOrphanedMetadataTask;->schedule()V

    .line 458
    return-void
.end method

.method syncFullShowMetadata(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;)V
    .locals 14
    .param p1, "priority"    # I
    .param p3, "account"    # Ljava/lang/String;
    .param p4, "params"    # Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/store/SyncTaskManager$SharedStates",
            "<*>;",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;",
            ")V"
        }
    .end annotation

    .prologue
    .line 431
    .local p2, "states":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<*>;"
    invoke-static/range {p3 .. p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 432
    invoke-static/range {p4 .. p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 436
    move-object/from16 v0, p4

    iget-object v6, v0, Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;->showId:Ljava/lang/String;

    .line 437
    .local v6, "showId":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "local:fs:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->addScheduledAsset(Ljava/lang/String;)Z

    move-result v12

    .line 438
    .local v12, "needsTask":Z
    new-instance v8, Ljava/util/ArrayList;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;->seasonIds:Ljava/util/Set;
    invoke-static/range {p4 .. p4}, Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;->access$000(Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;)Ljava/util/Set;

    move-result-object v1

    invoke-direct {v8, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 439
    .local v8, "seasonIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v11, v1, -0x1

    .local v11, "i":I
    :goto_0
    if-ltz v11, :cond_0

    .line 440
    invoke-interface {v8, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromSeasonId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 441
    .local v13, "seasonAssetId":Ljava/lang/String;
    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->addScheduledAsset(Ljava/lang/String;)Z

    move-result v1

    or-int/2addr v12, v1

    .line 439
    add-int/lit8 v11, v11, -0x1

    goto :goto_0

    .line 443
    .end local v13    # "seasonAssetId":Ljava/lang/String;
    :cond_0
    new-instance v9, Ljava/util/ArrayList;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;->episodeIds:Ljava/util/Set;
    invoke-static/range {p4 .. p4}, Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;->access$100(Lcom/google/android/videos/store/AssetStoreSync$FullShowSyncParams;)Ljava/util/Set;

    move-result-object v1

    invoke-direct {v9, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 444
    .local v9, "episodeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v11, v1, -0x1

    :goto_1
    if-ltz v11, :cond_1

    .line 445
    invoke-interface {v9, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromEpisodeId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 446
    .local v10, "episodeAssetId":Ljava/lang/String;
    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->addScheduledAsset(Ljava/lang/String;)Z

    move-result v1

    or-int/2addr v12, v1

    .line 444
    add-int/lit8 v11, v11, -0x1

    goto :goto_1

    .line 448
    .end local v10    # "episodeAssetId":Ljava/lang/String;
    :cond_1
    if-nez v12, :cond_2

    .line 454
    :goto_2
    return-void

    .line 452
    :cond_2
    new-instance v1, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;

    const/4 v7, 0x1

    move-object v2, p0

    move v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    invoke-direct/range {v1 .. v9}, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;-><init>(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;Ljava/util/List;)V

    invoke-virtual {v1}, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->schedule()V

    goto :goto_2
.end method

.method syncMovieMetadata(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;Ljava/util/List;)V
    .locals 7
    .param p1, "priority"    # I
    .param p3, "account"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/store/SyncTaskManager$SharedStates",
            "<*>;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 387
    .local p2, "states":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<*>;"
    .local p4, "movieIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 388
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 391
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v6, v0, -0x1

    .local v6, "i":I
    :goto_0
    if-ltz v6, :cond_1

    .line 392
    invoke-interface {p4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromMovieId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->addScheduledAsset(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 393
    invoke-interface {p4, v6}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 391
    :cond_0
    add-int/lit8 v6, v6, -0x1

    goto :goto_0

    .line 397
    :cond_1
    invoke-interface {p4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 398
    new-instance v0, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;-><init>(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {v0}, Lcom/google/android/videos/store/AssetStoreSync$SyncMovieMetadataTask;->schedule()V

    .line 400
    :cond_2
    return-void
.end method

.method syncShowMetadata(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "priority"    # I
    .param p3, "account"    # Ljava/lang/String;
    .param p4, "showId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/store/SyncTaskManager$SharedStates",
            "<*>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "states":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<*>;"
    const/4 v7, 0x0

    .line 407
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 408
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 411
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "local:fs:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->isAssetScheduled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p4}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromShowId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->addScheduledAsset(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 417
    :cond_0
    :goto_0
    return-void

    .line 416
    :cond_1
    new-instance v0, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;

    const/4 v6, 0x0

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v8, v7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;-><init>(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;Ljava/util/List;)V

    invoke-virtual {v0}, Lcom/google/android/videos/store/AssetStoreSync$SyncShowMetadataTask;->schedule()V

    goto :goto_0
.end method
