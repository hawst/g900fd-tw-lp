.class Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;
.super Lcom/google/android/videos/store/SyncTaskManager$SyncTask;
.source "AssetStoreSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/AssetStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RemoveBitmapTask"
.end annotation


# instance fields
.field private final bitmapItemIdColumn:Ljava/lang/String;

.field private final bitmapTable:Ljava/lang/String;

.field private final fileStore:Lcom/google/android/videos/store/FileStore;

.field private final itemId:Ljava/lang/String;

.field private final metadataItemIdColumn:Ljava/lang/String;

.field private final metadataTable:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/videos/store/AssetStoreSync;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;Lcom/google/android/videos/store/FileStore;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2, "priority"    # I
    .param p4, "itemId"    # Ljava/lang/String;
    .param p5, "fileStore"    # Lcom/google/android/videos/store/FileStore;
    .param p6, "metadataTable"    # Ljava/lang/String;
    .param p7, "metadataItemIdColumn"    # Ljava/lang/String;
    .param p8, "bitmapTable"    # Ljava/lang/String;
    .param p9, "bitmapItemIdColumn"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/store/SyncTaskManager$SharedStates",
            "<*>;",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/store/FileStore;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1595
    .local p3, "states":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<*>;"
    iput-object p1, p0, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    .line 1596
    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->syncTaskManager:Lcom/google/android/videos/store/SyncTaskManager;
    invoke-static {p1}, Lcom/google/android/videos/store/AssetStoreSync;->access$200(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/SyncTaskManager;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;-><init>(Lcom/google/android/videos/store/SyncTaskManager;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V

    .line 1597
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;->itemId:Ljava/lang/String;

    .line 1598
    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/FileStore;

    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;->fileStore:Lcom/google/android/videos/store/FileStore;

    .line 1599
    invoke-static {p6}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;->metadataTable:Ljava/lang/String;

    .line 1601
    invoke-static {p7}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;->metadataItemIdColumn:Ljava/lang/String;

    .line 1603
    invoke-static {p8}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;->bitmapTable:Ljava/lang/String;

    .line 1605
    invoke-static {p9}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;->bitmapItemIdColumn:Ljava/lang/String;

    .line 1607
    return-void
.end method


# virtual methods
.method protected doSync()V
    .locals 15

    .prologue
    const/4 v13, 0x1

    const/4 v14, 0x0

    .line 1611
    iget-object v1, p0, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v1}, Lcom/google/android/videos/store/AssetStoreSync;->access$900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1612
    .local v0, "transaction":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v12, 0x0

    .line 1614
    .local v12, "success":Z
    const/4 v11, 0x1

    .line 1615
    .local v11, "posterStillNeeded":Z
    :try_start_0
    iget-object v1, p0, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;->metadataTable:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;->metadataItemIdColumn:Ljava/lang/String;

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;->metadataItemIdColumn:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " = ?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;->itemId:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "1"

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v9

    .line 1618
    .local v9, "cursor":Landroid/database/Cursor;
    :try_start_1
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    move v11, v13

    .line 1620
    :goto_0
    :try_start_2
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 1622
    if-nez v11, :cond_0

    .line 1623
    iget-object v1, p0, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;->bitmapTable:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;->bitmapItemIdColumn:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " = ?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;->itemId:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1625
    :try_start_3
    iget-object v1, p0, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;->fileStore:Lcom/google/android/videos/store/FileStore;

    iget-object v2, p0, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;->itemId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/videos/store/FileStore;->delete(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1630
    :cond_0
    :goto_1
    const/4 v12, 0x1

    .line 1632
    iget-object v1, p0, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v1}, Lcom/google/android/videos/store/AssetStoreSync;->access$900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v1

    new-array v2, v14, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v12, v14, v2}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 1634
    return-void

    :cond_1
    move v11, v14

    .line 1618
    goto :goto_0

    .line 1620
    :catchall_0
    move-exception v1

    :try_start_4
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1632
    .end local v9    # "cursor":Landroid/database/Cursor;
    :catchall_1
    move-exception v1

    iget-object v2, p0, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v2}, Lcom/google/android/videos/store/AssetStoreSync;->access$900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v2

    new-array v3, v14, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v12, v14, v3}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v1

    .line 1626
    .restart local v9    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v10

    .line 1627
    .local v10, "e":Ljava/io/IOException;
    const/16 v1, 0xa

    :try_start_5
    invoke-virtual {p0, v1, v10}, Lcom/google/android/videos/store/AssetStoreSync$RemoveBitmapTask;->onSyncError(ILjava/lang/Exception;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1
.end method
