.class public Lcom/google/android/videos/welcome/FreeMoviePosterView;
.super Landroid/widget/ImageView;
.source "FreeMoviePosterView.java"


# static fields
.field private static final COLUMN_COUNT_ATTR:[I


# instance fields
.field private final columnCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 21
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f010189

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/videos/welcome/FreeMoviePosterView;->COLUMN_COUNT_ATTR:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 26
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/videos/welcome/FreeMoviePosterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 30
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    sget-object v2, Lcom/google/android/videos/welcome/FreeMoviePosterView;->COLUMN_COUNT_ATTR:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 32
    .local v0, "attrsArray":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v1, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/videos/welcome/FreeMoviePosterView;->columnCount:I

    .line 33
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 34
    iget v2, p0, Lcom/google/android/videos/welcome/FreeMoviePosterView;->columnCount:I

    if-lez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-static {v1}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 35
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 6
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 39
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    const/high16 v4, -0x80000000

    if-ne v3, v4, :cond_0

    .line 40
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 41
    .local v0, "available":I
    iget v3, p0, Lcom/google/android/videos/welcome/FreeMoviePosterView;->columnCount:I

    div-int v2, v0, v3

    .line 42
    .local v2, "widthPx":I
    int-to-float v3, v2

    const v4, 0x3f31a787

    div-float/2addr v3, v4

    float-to-int v1, v3

    .line 43
    .local v1, "heightPx":I
    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 44
    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 46
    .end local v0    # "available":I
    .end local v1    # "heightPx":I
    .end local v2    # "widthPx":I
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onMeasure(II)V

    .line 47
    return-void
.end method
