.class Lcom/google/android/videos/pano/activity/BaseDetailsActivity$UpdateNotifier;
.super Lcom/google/android/videos/store/Database$BaseListener;
.source "BaseDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/activity/BaseDetailsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateNotifier"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)V
    .locals 0

    .prologue
    .line 524
    iput-object p1, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$UpdateNotifier;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    invoke-direct {p0}, Lcom/google/android/videos/store/Database$BaseListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;Lcom/google/android/videos/pano/activity/BaseDetailsActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/pano/activity/BaseDetailsActivity;
    .param p2, "x1"    # Lcom/google/android/videos/pano/activity/BaseDetailsActivity$1;

    .prologue
    .line 524
    invoke-direct {p0, p1}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$UpdateNotifier;-><init>(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)V

    return-void
.end method


# virtual methods
.method public onPurchasesUpdated(Ljava/lang/String;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 533
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$UpdateNotifier;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    invoke-virtual {v0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->onPurchasesUpdated()V

    .line 534
    return-void
.end method

.method public onWatchTimestampsUpdated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;

    .prologue
    .line 538
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$UpdateNotifier;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    invoke-virtual {v0, p2}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->onWatchTimestampsUpdated(Ljava/lang/String;)V

    .line 539
    return-void
.end method

.method public onWishlistUpdated(Ljava/lang/String;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 528
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$UpdateNotifier;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    invoke-virtual {v0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->onWishlistUpdated()V

    .line 529
    return-void
.end method
