.class public abstract Lcom/google/android/videos/async/TaskStatus;
.super Ljava/lang/Object;
.source "TaskStatus.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isCancelled(Lcom/google/android/videos/async/TaskStatus;)Z
    .locals 1
    .param p0, "taskStatus"    # Lcom/google/android/videos/async/TaskStatus;

    .prologue
    .line 22
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/videos/async/TaskStatus;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract isCancelled()Z
.end method
