.class final Lcom/google/android/videos/api/GcmRegisterConverter;
.super Ljava/lang/Object;
.source "GcmRegisterConverter.java"

# interfaces
.implements Lcom/google/android/videos/converter/RequestConverter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/converter/RequestConverter",
        "<",
        "Lcom/google/android/videos/api/GcmRegisterRequest;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        ">;"
    }
.end annotation


# instance fields
.field private final uri:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/net/Uri;J)V
    .locals 2
    .param p1, "baseUri"    # Landroid/net/Uri;
    .param p2, "gservicesId"    # J

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Lcom/google/android/videos/utils/UriBuilder;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "device"

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;->addSegment(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v0

    invoke-static {p2, p3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;->addSegment(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v0

    const-string v1, "gcm"

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;->addSegment(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/utils/UriBuilder;->build()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/api/GcmRegisterConverter;->uri:Ljava/lang/String;

    .line 30
    return-void
.end method


# virtual methods
.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 17
    check-cast p1, Lcom/google/android/videos/api/GcmRegisterRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/api/GcmRegisterConverter;->convertRequest(Lcom/google/android/videos/api/GcmRegisterRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public convertRequest(Lcom/google/android/videos/api/GcmRegisterRequest;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 3
    .param p1, "request"    # Lcom/google/android/videos/api/GcmRegisterRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 34
    new-instance v0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;-><init>()V

    .line 35
    .local v0, "message":Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;
    iget-object v1, p1, Lcom/google/android/videos/api/GcmRegisterRequest;->registrationId:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->registrationId:Ljava/lang/String;

    .line 36
    iget-object v1, p0, Lcom/google/android/videos/api/GcmRegisterConverter;->uri:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/videos/async/HttpUriRequestFactory;->createPut(Ljava/lang/String;[B)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v1

    return-object v1
.end method
