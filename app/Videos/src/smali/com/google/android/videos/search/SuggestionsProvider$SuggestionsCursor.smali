.class Lcom/google/android/videos/search/SuggestionsProvider$SuggestionsCursor;
.super Landroid/database/MatrixCursor;
.source "SuggestionsProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/search/SuggestionsProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SuggestionsCursor"
.end annotation


# instance fields
.field private final seen:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>([Ljava/lang/String;)V
    .locals 1
    .param p1, "columnNames"    # [Ljava/lang/String;

    .prologue
    .line 171
    invoke-direct {p0, p1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 172
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/search/SuggestionsProvider$SuggestionsCursor;->seen:Ljava/util/HashSet;

    .line 173
    return-void
.end method

.method private static normalize(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 201
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 202
    .local v2, "result":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 203
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 204
    .local v0, "c":C
    invoke-static {v0}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 205
    invoke-static {v0}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 202
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 208
    .end local v0    # "c":C
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method


# virtual methods
.method public add(Landroid/database/Cursor;)V
    .locals 8
    .param p1, "suggestions"    # Landroid/database/Cursor;

    .prologue
    .line 176
    invoke-virtual {p0}, Lcom/google/android/videos/search/SuggestionsProvider$SuggestionsCursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v1

    .line 177
    .local v1, "columns":[Ljava/lang/String;
    array-length v6, v1

    new-array v0, v6, [I

    .line 178
    .local v0, "columnIndices":[I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v6, v0

    if-ge v2, v6, :cond_1

    .line 179
    aget-object v6, v1, v2

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    aput v6, v0, v2

    .line 178
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 194
    .local v4, "key":Ljava/lang/String;
    .local v5, "values":[Ljava/lang/Object;
    :cond_0
    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/google/android/videos/search/SuggestionsProvider$SuggestionsCursor;->getCount()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    .line 195
    invoke-virtual {p0, v5}, Lcom/google/android/videos/search/SuggestionsProvider$SuggestionsCursor;->addRow([Ljava/lang/Object;)V

    .line 182
    .end local v4    # "key":Ljava/lang/String;
    .end local v5    # "values":[Ljava/lang/Object;
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 183
    const/4 v6, 0x1

    aget v6, v0, v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 184
    .restart local v4    # "key":Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/videos/search/SuggestionsProvider$SuggestionsCursor;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 185
    iget-object v6, p0, Lcom/google/android/videos/search/SuggestionsProvider$SuggestionsCursor;->seen:Ljava/util/HashSet;

    invoke-virtual {v6, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 186
    iget-object v6, p0, Lcom/google/android/videos/search/SuggestionsProvider$SuggestionsCursor;->seen:Ljava/util/HashSet;

    invoke-virtual {v6, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 187
    array-length v6, v1

    new-array v5, v6, [Ljava/lang/Object;

    .line 188
    .restart local v5    # "values":[Ljava/lang/Object;
    const/4 v2, 0x0

    :goto_1
    array-length v6, v5

    if-ge v2, v6, :cond_0

    .line 189
    aget v3, v0, v2

    .line 190
    .local v3, "index":I
    if-ltz v3, :cond_2

    .line 191
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    .line 188
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 198
    .end local v3    # "index":I
    .end local v4    # "key":Ljava/lang/String;
    .end local v5    # "values":[Ljava/lang/Object;
    :cond_3
    return-void
.end method
