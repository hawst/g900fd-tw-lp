.class public Lcom/google/android/videos/welcome/WelcomeFlow;
.super Lcom/google/android/videos/flow/Flow;
.source "WelcomeFlow.java"

# interfaces
.implements Lcom/google/android/videos/welcome/Welcome$OnEligibilityChangedListener;


# instance fields
.field private account:Ljava/lang/String;

.field private final bitmapRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private contentInVertical:Z

.field private dimmed:Z

.field private downloadedOnly:Z

.field private selectedWelcomes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/welcome/Welcome;",
            ">;"
        }
    .end annotation
.end field

.field private final topSpacing:I

.field private final welcomes:[Lcom/google/android/videos/welcome/Welcome;


# direct methods
.method public varargs constructor <init>(Lcom/google/android/videos/async/Requester;I[Lcom/google/android/videos/welcome/Welcome;)V
    .locals 2
    .param p2, "topSpacing"    # I
    .param p3, "welcomes"    # [Lcom/google/android/videos/welcome/Welcome;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;I[",
            "Lcom/google/android/videos/welcome/Welcome;",
            ")V"
        }
    .end annotation

    .prologue
    .line 39
    .local p1, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Landroid/graphics/Bitmap;>;"
    invoke-direct {p0}, Lcom/google/android/videos/flow/Flow;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    .line 41
    iput p2, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->topSpacing:I

    .line 42
    iput-object p3, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->welcomes:[Lcom/google/android/videos/welcome/Welcome;

    .line 43
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->selectedWelcomes:Ljava/util/List;

    .line 44
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p3

    if-ge v0, v1, :cond_0

    .line 45
    aget-object v1, p3, v0

    invoke-virtual {v1, p0}, Lcom/google/android/videos/welcome/Welcome;->setEligibilityChangedListener(Lcom/google/android/videos/welcome/Welcome$OnEligibilityChangedListener;)V

    .line 44
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 47
    :cond_0
    return-void
.end method

.method private refreshWelcomes()V
    .locals 12

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 97
    iget-object v8, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->account:Ljava/lang/String;

    if-nez v8, :cond_0

    .line 98
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/videos/welcome/WelcomeFlow;->setWelcomes(Ljava/util/List;)V

    .line 117
    :goto_0
    return-void

    .line 101
    :cond_0
    new-instance v5, Ljava/util/ArrayList;

    const/4 v8, 0x2

    invoke-direct {v5, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 102
    .local v5, "newWelcomes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/welcome/Welcome;>;"
    const/4 v2, 0x0

    .line 103
    .local v2, "havePromoWelcome":Z
    const/4 v1, 0x0

    .line 104
    .local v1, "haveDefaultWelcome":Z
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    iget-object v8, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->welcomes:[Lcom/google/android/videos/welcome/Welcome;

    array-length v8, v8

    if-ge v3, v8, :cond_6

    .line 105
    iget-object v8, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->welcomes:[Lcom/google/android/videos/welcome/Welcome;

    aget-object v8, v8, v3

    invoke-virtual {v8}, Lcom/google/android/videos/welcome/Welcome;->isUserDismissable()Z

    move-result v4

    .line 106
    .local v4, "isPromoWelcome":Z
    if-eqz v4, :cond_3

    if-nez v2, :cond_2

    move v0, v6

    .line 107
    .local v0, "canShow":Z
    :goto_2
    if-eqz v0, :cond_1

    iget-object v8, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->welcomes:[Lcom/google/android/videos/welcome/Welcome;

    aget-object v8, v8, v3

    iget-object v9, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->account:Ljava/lang/String;

    iget-boolean v10, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->contentInVertical:Z

    iget-boolean v11, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->downloadedOnly:Z

    invoke-virtual {v8, v9, v10, v11}, Lcom/google/android/videos/welcome/Welcome;->prepareIfEligible(Ljava/lang/String;ZZ)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 108
    iget-object v8, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->welcomes:[Lcom/google/android/videos/welcome/Welcome;

    aget-object v8, v8, v3

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 109
    if-eqz v4, :cond_5

    .line 110
    const/4 v2, 0x1

    .line 104
    :cond_1
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .end local v0    # "canShow":Z
    :cond_2
    move v0, v7

    .line 106
    goto :goto_2

    :cond_3
    if-nez v1, :cond_4

    move v0, v6

    goto :goto_2

    :cond_4
    move v0, v7

    goto :goto_2

    .line 112
    .restart local v0    # "canShow":Z
    :cond_5
    const/4 v1, 0x1

    goto :goto_3

    .line 116
    .end local v0    # "canShow":Z
    .end local v4    # "isPromoWelcome":Z
    :cond_6
    invoke-direct {p0, v5}, Lcom/google/android/videos/welcome/WelcomeFlow;->setWelcomes(Ljava/util/List;)V

    goto :goto_0
.end method

.method private setWelcomes(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/welcome/Welcome;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "selectedWelcomes":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/welcome/Welcome;>;"
    const/4 v3, 0x0

    .line 121
    iget-object v2, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->selectedWelcomes:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 122
    iget-object v2, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->selectedWelcomes:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    .line 123
    .local v1, "oldCount":I
    if-lez v1, :cond_0

    .line 124
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->selectedWelcomes:Ljava/util/List;

    .line 125
    invoke-virtual {p0, v3, v1}, Lcom/google/android/videos/welcome/WelcomeFlow;->notifyItemsRemoved(II)V

    .line 127
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 128
    .local v0, "newCount":I
    if-lez v0, :cond_1

    .line 129
    iput-object p1, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->selectedWelcomes:Ljava/util/List;

    .line 130
    invoke-virtual {p0, v3, v0}, Lcom/google/android/videos/welcome/WelcomeFlow;->notifyItemsInserted(II)V

    .line 133
    .end local v0    # "newCount":I
    .end local v1    # "oldCount":I
    :cond_1
    return-void
.end method


# virtual methods
.method public bindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 4
    .param p1, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 157
    iget-object v3, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->selectedWelcomes:Ljava/util/List;

    invoke-interface {v3, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/welcome/Welcome;

    .line 158
    .local v2, "welcome":Lcom/google/android/videos/welcome/Welcome;
    iget-object v1, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    check-cast v1, Lcom/google/android/videos/ui/playnext/WelcomeCardView;

    .line 159
    .local v1, "view":Lcom/google/android/videos/ui/playnext/WelcomeCardView;
    iget-boolean v3, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->dimmed:Z

    invoke-virtual {v1, v3}, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->setDimmed(Z)V

    .line 160
    iget-object v3, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    invoke-virtual {v1, v3, v2}, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->init(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/welcome/Welcome;)V

    .line 162
    if-nez p2, :cond_0

    iget v3, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->topSpacing:I

    if-eqz v3, :cond_0

    .line 163
    invoke-virtual {v1}, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    .line 165
    .local v0, "params":Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
    iget v3, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->topSpacing:I

    invoke-static {v3}, Lcom/google/android/play/utils/Compound;->intLengthToCompound(I)I

    move-result v3

    iput v3, v0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->topMarginCompound:I

    .line 167
    .end local v0    # "params":Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->selectedWelcomes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemIdentifier(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->selectedWelcomes:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/welcome/Welcome;

    iget-object v0, v0, Lcom/google/android/videos/welcome/Welcome;->promoId:Ljava/lang/String;

    return-object v0
.end method

.method public getViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->selectedWelcomes:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/welcome/Welcome;

    invoke-virtual {v0}, Lcom/google/android/videos/welcome/Welcome;->getLayoutResourceId()I

    move-result v0

    return v0
.end method

.method public onEligibilityChanged()V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/google/android/videos/welcome/WelcomeFlow;->refreshWelcomes()V

    .line 94
    return-void
.end method

.method protected onInitViewTypes(Landroid/util/SparseArray;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/videos/flow/ViewHolderCreator",
            "<*>;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 149
    .local p1, "viewTypes":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/google/android/videos/flow/ViewHolderCreator<*>;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->welcomes:[Lcom/google/android/videos/welcome/Welcome;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 150
    iget-object v2, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->welcomes:[Lcom/google/android/videos/welcome/Welcome;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/android/videos/welcome/Welcome;->getLayoutResourceId()I

    move-result v1

    .line 151
    .local v1, "layoutId":I
    invoke-static {p1, v1, v1}, Lcom/google/android/videos/flow/BasicViewHolderCreator;->addOrVerify(Landroid/util/SparseArray;II)V

    .line 149
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 153
    .end local v1    # "layoutId":I
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 50
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->welcomes:[Lcom/google/android/videos/welcome/Welcome;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 51
    iget-object v1, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->welcomes:[Lcom/google/android/videos/welcome/Welcome;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/videos/welcome/Welcome;->onStart()V

    .line 50
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53
    :cond_0
    invoke-direct {p0}, Lcom/google/android/videos/welcome/WelcomeFlow;->refreshWelcomes()V

    .line 54
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 57
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->welcomes:[Lcom/google/android/videos/welcome/Welcome;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 58
    iget-object v1, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->welcomes:[Lcom/google/android/videos/welcome/Welcome;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/videos/welcome/Welcome;->onStop()V

    .line 57
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 60
    :cond_0
    return-void
.end method

.method public setAccount(Ljava/lang/String;)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->account:Ljava/lang/String;

    .line 88
    invoke-direct {p0}, Lcom/google/android/videos/welcome/WelcomeFlow;->refreshWelcomes()V

    .line 89
    return-void
.end method

.method public setContentInVertical(Z)V
    .locals 1
    .param p1, "contentInVertical"    # Z

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->contentInVertical:Z

    if-eq v0, p1, :cond_0

    .line 74
    iput-boolean p1, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->contentInVertical:Z

    .line 75
    invoke-direct {p0}, Lcom/google/android/videos/welcome/WelcomeFlow;->refreshWelcomes()V

    .line 77
    :cond_0
    return-void
.end method

.method public setDimmed(Z)V
    .locals 2
    .param p1, "dimmed"    # Z

    .prologue
    .line 63
    iget-boolean v1, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->dimmed:Z

    if-eq v1, p1, :cond_0

    .line 64
    iput-boolean p1, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->dimmed:Z

    .line 65
    invoke-virtual {p0}, Lcom/google/android/videos/welcome/WelcomeFlow;->getCount()I

    move-result v0

    .line 66
    .local v0, "count":I
    if-lez v0, :cond_0

    .line 67
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/videos/welcome/WelcomeFlow;->notifyItemsChanged(II)V

    .line 70
    .end local v0    # "count":I
    :cond_0
    return-void
.end method

.method public setDownloadedOnly(Z)V
    .locals 1
    .param p1, "downloadedOnly"    # Z

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->downloadedOnly:Z

    if-eq v0, p1, :cond_0

    .line 81
    iput-boolean p1, p0, Lcom/google/android/videos/welcome/WelcomeFlow;->downloadedOnly:Z

    .line 82
    invoke-direct {p0}, Lcom/google/android/videos/welcome/WelcomeFlow;->refreshWelcomes()V

    .line 84
    :cond_0
    return-void
.end method
