.class public Lcom/google/android/videos/ui/RemoteScreenPanelHelper;
.super Ljava/lang/Object;
.source "RemoteScreenPanelHelper.java"

# interfaces
.implements Lcom/google/android/videos/ui/ArtworkMonitor$Listener;


# static fields
.field private static final FOCUS_POINT_CENTER:Landroid/graphics/PointF;

.field private static final FOCUS_POINT_NATURAL:Landroid/graphics/PointF;


# instance fields
.field private final deviceView:Landroid/widget/TextView;

.field private final interactiveKnowledgeOverlay:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

.field private playerState:I

.field private final remoteScreenPanel:Landroid/view/View;

.field private screenName:Ljava/lang/String;

.field private final screenshotMonitor:Lcom/google/android/videos/ui/ArtworkMonitor;

.field private final screenshotView:Lcom/google/android/videos/ui/FocusedImageView;

.field private final spinner:Landroid/widget/ProgressBar;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f000000    # 0.5f

    .line 29
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v2, v2}, Landroid/graphics/PointF;-><init>(FF)V

    sput-object v0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->FOCUS_POINT_CENTER:Landroid/graphics/PointF;

    .line 30
    new-instance v0, Landroid/graphics/PointF;

    const v1, 0x3e4ccccd    # 0.2f

    invoke-direct {v0, v2, v1}, Landroid/graphics/PointF;-><init>(FF)V

    sput-object v0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->FOCUS_POINT_NATURAL:Landroid/graphics/PointF;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;Lcom/google/android/videos/store/PosterStore;Lcom/google/android/videos/store/Database;Landroid/view/View;)V
    .locals 4
    .param p1, "interactiveKnowledgeOverlay"    # Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;
    .param p2, "posterStore"    # Lcom/google/android/videos/store/PosterStore;
    .param p3, "database"    # Lcom/google/android/videos/store/Database;
    .param p4, "remotePanel"    # Landroid/view/View;

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->interactiveKnowledgeOverlay:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    .line 45
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->remoteScreenPanel:Landroid/view/View;

    .line 46
    const v0, 0x7f0f01ec

    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->deviceView:Landroid/widget/TextView;

    .line 47
    const v0, 0x7f0f01eb

    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/FocusedImageView;

    iput-object v0, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->screenshotView:Lcom/google/android/videos/ui/FocusedImageView;

    .line 48
    const v0, 0x7f0f00c5

    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->spinner:Landroid/widget/ProgressBar;

    .line 49
    new-instance v0, Lcom/google/android/videos/ui/ArtworkMonitor;

    invoke-direct {v0, p2, p3, p0}, Lcom/google/android/videos/ui/ArtworkMonitor;-><init>(Lcom/google/android/videos/store/PosterStore;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/ui/ArtworkMonitor$Listener;)V

    iput-object v0, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->screenshotMonitor:Lcom/google/android/videos/ui/ArtworkMonitor;

    .line 50
    iget-object v0, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->screenshotView:Lcom/google/android/videos/ui/FocusedImageView;

    new-instance v1, Landroid/graphics/LightingColorFilter;

    const v2, -0x9f9fa0

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Landroid/graphics/LightingColorFilter;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/FocusedImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 51
    invoke-virtual {p0}, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->reset()V

    .line 52
    return-void
.end method

.method private updateText()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 96
    iget-object v2, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->interactiveKnowledgeOverlay:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->interactiveKnowledgeOverlay:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    invoke-virtual {v2}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->hasContent()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    iget v2, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->playerState:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 98
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->deviceView:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 124
    :goto_0
    return-void

    .line 101
    :cond_2
    iget-object v2, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->deviceView:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 102
    const/4 v0, 0x0

    .line 103
    .local v0, "deviceText":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->screenName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 104
    iget-object v2, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->remoteScreenPanel:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 105
    .local v1, "resources":Landroid/content/res/Resources;
    iget v2, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->playerState:I

    packed-switch v2, :pswitch_data_0

    .line 123
    .end local v1    # "resources":Landroid/content/res/Resources;
    :cond_3
    :goto_1
    :pswitch_0
    iget-object v2, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->deviceView:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 107
    .restart local v1    # "resources":Landroid/content/res/Resources;
    :pswitch_1
    const v2, 0x7f0b01ce

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->screenName:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 108
    goto :goto_1

    .line 110
    :pswitch_2
    const v2, 0x7f0b01cd

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->screenName:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 111
    goto :goto_1

    .line 113
    :pswitch_3
    const v2, 0x7f0b01cc

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->screenName:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 114
    goto :goto_1

    .line 105
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private updateViews()V
    .locals 3

    .prologue
    .line 91
    iget-object v1, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->spinner:Landroid/widget/ProgressBar;

    iget v0, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->playerState:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 92
    invoke-direct {p0}, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->updateText()V

    .line 93
    return-void

    .line 91
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public init(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "screenName"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 59
    invoke-virtual {p0, p1}, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->setScreenName(Ljava/lang/String;)V

    .line 60
    invoke-virtual {p0, p2}, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->setVideoId(Ljava/lang/String;)V

    .line 61
    iput v1, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->playerState:I

    .line 62
    invoke-direct {p0}, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->updateViews()V

    .line 63
    iget-object v0, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->remoteScreenPanel:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 64
    return-void
.end method

.method public onImageChanged(Lcom/google/android/videos/ui/ArtworkMonitor;Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "monitor"    # Lcom/google/android/videos/ui/ArtworkMonitor;
    .param p2, "image"    # Landroid/graphics/Bitmap;

    .prologue
    .line 130
    invoke-static {}, Lcom/google/android/videos/utils/Preconditions;->checkMainThread()V

    .line 131
    if-eqz p2, :cond_0

    .line 132
    iget-object v0, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->screenshotView:Lcom/google/android/videos/ui/FocusedImageView;

    invoke-virtual {v0, p2}, Lcom/google/android/videos/ui/FocusedImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 133
    iget-object v0, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->screenshotView:Lcom/google/android/videos/ui/FocusedImageView;

    sget-object v1, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->FOCUS_POINT_NATURAL:Landroid/graphics/PointF;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/FocusedImageView;->setFocusPoint(Landroid/graphics/PointF;)V

    .line 138
    :goto_0
    return-void

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->screenshotView:Lcom/google/android/videos/ui/FocusedImageView;

    const v1, 0x7f020039

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/FocusedImageView;->setImageResource(I)V

    .line 136
    iget-object v0, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->screenshotView:Lcom/google/android/videos/ui/FocusedImageView;

    sget-object v1, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->FOCUS_POINT_CENTER:Landroid/graphics/PointF;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/FocusedImageView;->setFocusPoint(Landroid/graphics/PointF;)V

    goto :goto_0
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->screenshotMonitor:Lcom/google/android/videos/ui/ArtworkMonitor;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/ArtworkMonitor;->reset()V

    .line 68
    iget-object v0, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->remoteScreenPanel:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 69
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->setScreenName(Ljava/lang/String;)V

    .line 70
    return-void
.end method

.method public setKeepScreenOn(Z)V
    .locals 1
    .param p1, "on"    # Z

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->remoteScreenPanel:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setKeepScreenOn(Z)V

    .line 56
    return-void
.end method

.method public setPlayerState(I)V
    .locals 1
    .param p1, "playerState"    # I

    .prologue
    .line 84
    iget v0, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->playerState:I

    if-eq v0, p1, :cond_0

    .line 85
    iput p1, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->playerState:I

    .line 86
    invoke-direct {p0}, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->updateViews()V

    .line 88
    :cond_0
    return-void
.end method

.method public setScreenName(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->screenName:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 78
    iput-object p1, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->screenName:Ljava/lang/String;

    .line 79
    invoke-direct {p0}, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->updateText()V

    .line 81
    :cond_0
    return-void
.end method

.method public setVideoId(Ljava/lang/String;)V
    .locals 2
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->screenshotMonitor:Lcom/google/android/videos/ui/ArtworkMonitor;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Lcom/google/android/videos/ui/ArtworkMonitor;->setMonitored(ILjava/lang/String;)V

    .line 74
    return-void
.end method
