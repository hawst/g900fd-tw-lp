.class public Lcom/google/android/videos/utils/ByteArrayPool;
.super Ljava/lang/Object;
.source "ByteArrayPool.java"


# static fields
.field protected static final BUF_COMPARATOR:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mBuffersByLastUse:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;"
        }
    .end annotation
.end field

.field private final mBuffersBySize:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentSize:I

.field private final mSizeLimit:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lcom/google/android/videos/utils/ByteArrayPool$1;

    invoke-direct {v0}, Lcom/google/android/videos/utils/ByteArrayPool$1;-><init>()V

    sput-object v0, Lcom/google/android/videos/utils/ByteArrayPool;->BUF_COMPARATOR:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2
    .param p1, "sizeLimit"    # I

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/utils/ByteArrayPool;->mBuffersByLastUse:Ljava/util/List;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/videos/utils/ByteArrayPool;->mBuffersBySize:Ljava/util/List;

    .line 50
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/utils/ByteArrayPool;->mCurrentSize:I

    .line 70
    iput p1, p0, Lcom/google/android/videos/utils/ByteArrayPool;->mSizeLimit:I

    .line 71
    return-void
.end method

.method private declared-synchronized trim()V
    .locals 3

    .prologue
    .line 127
    monitor-enter p0

    :goto_0
    :try_start_0
    iget v1, p0, Lcom/google/android/videos/utils/ByteArrayPool;->mCurrentSize:I

    iget v2, p0, Lcom/google/android/videos/utils/ByteArrayPool;->mSizeLimit:I

    if-le v1, v2, :cond_0

    .line 128
    iget-object v1, p0, Lcom/google/android/videos/utils/ByteArrayPool;->mBuffersByLastUse:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/utils/ByteArray;

    .line 129
    .local v0, "buf":Lcom/google/android/videos/utils/ByteArray;
    iget-object v1, p0, Lcom/google/android/videos/utils/ByteArrayPool;->mBuffersBySize:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 130
    iget v1, p0, Lcom/google/android/videos/utils/ByteArrayPool;->mCurrentSize:I

    iget v2, v0, Lcom/google/android/videos/utils/ByteArray;->capacity:I

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/videos/utils/ByteArrayPool;->mCurrentSize:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 127
    .end local v0    # "buf":Lcom/google/android/videos/utils/ByteArray;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 132
    :cond_0
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public declared-synchronized clear()V
    .locals 1

    .prologue
    .line 118
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/utils/ByteArrayPool;->mBuffersByLastUse:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 119
    iget-object v0, p0, Lcom/google/android/videos/utils/ByteArrayPool;->mBuffersBySize:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 120
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/utils/ByteArrayPool;->mCurrentSize:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    monitor-exit p0

    return-void

    .line 118
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getBuf(I)Lcom/google/android/videos/utils/ByteArray;
    .locals 4
    .param p1, "len"    # I

    .prologue
    .line 82
    monitor-enter p0

    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/google/android/videos/utils/ByteArrayPool;->mBuffersBySize:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 83
    iget-object v2, p0, Lcom/google/android/videos/utils/ByteArrayPool;->mBuffersBySize:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/utils/ByteArray;

    .line 84
    .local v0, "buf":Lcom/google/android/videos/utils/ByteArray;
    iget v2, v0, Lcom/google/android/videos/utils/ByteArray;->capacity:I

    if-lt v2, p1, :cond_0

    .line 85
    iget v2, p0, Lcom/google/android/videos/utils/ByteArrayPool;->mCurrentSize:I

    iget v3, v0, Lcom/google/android/videos/utils/ByteArray;->capacity:I

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/google/android/videos/utils/ByteArrayPool;->mCurrentSize:I

    .line 86
    iget-object v2, p0, Lcom/google/android/videos/utils/ByteArrayPool;->mBuffersBySize:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 87
    iget-object v2, p0, Lcom/google/android/videos/utils/ByteArrayPool;->mBuffersByLastUse:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91
    .end local v0    # "buf":Lcom/google/android/videos/utils/ByteArray;
    :goto_1
    monitor-exit p0

    return-object v0

    .line 82
    .restart local v0    # "buf":Lcom/google/android/videos/utils/ByteArray;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 91
    .end local v0    # "buf":Lcom/google/android/videos/utils/ByteArray;
    :cond_1
    :try_start_1
    new-instance v0, Lcom/google/android/videos/utils/ByteArray;

    invoke-direct {v0, p1}, Lcom/google/android/videos/utils/ByteArray;-><init>(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 82
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized returnBuf(Lcom/google/android/videos/utils/ByteArray;)V
    .locals 3
    .param p1, "buf"    # Lcom/google/android/videos/utils/ByteArray;

    .prologue
    .line 101
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget v1, p1, Lcom/google/android/videos/utils/ByteArray;->capacity:I

    iget v2, p0, Lcom/google/android/videos/utils/ByteArrayPool;->mSizeLimit:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-le v1, v2, :cond_1

    .line 112
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 104
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/videos/utils/ByteArrayPool;->mBuffersByLastUse:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    iget-object v1, p0, Lcom/google/android/videos/utils/ByteArrayPool;->mBuffersBySize:Ljava/util/List;

    sget-object v2, Lcom/google/android/videos/utils/ByteArrayPool;->BUF_COMPARATOR:Ljava/util/Comparator;

    invoke-static {v1, p1, v2}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v0

    .line 106
    .local v0, "pos":I
    if-gez v0, :cond_2

    .line 107
    neg-int v1, v0

    add-int/lit8 v0, v1, -0x1

    .line 109
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/utils/ByteArrayPool;->mBuffersBySize:Ljava/util/List;

    invoke-interface {v1, v0, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 110
    iget v1, p0, Lcom/google/android/videos/utils/ByteArrayPool;->mCurrentSize:I

    iget v2, p1, Lcom/google/android/videos/utils/ByteArray;->capacity:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/videos/utils/ByteArrayPool;->mCurrentSize:I

    .line 111
    invoke-direct {p0}, Lcom/google/android/videos/utils/ByteArrayPool;->trim()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 101
    .end local v0    # "pos":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
