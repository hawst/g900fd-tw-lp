.class abstract Lcom/google/android/videos/pinning/Downloader;
.super Ljava/lang/Object;
.source "Downloader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/pinning/Downloader$ReservedSpaceQuery;,
        Lcom/google/android/videos/pinning/Downloader$SetupResult;,
        Lcom/google/android/videos/pinning/Downloader$ProgressListener;
    }
.end annotation


# instance fields
.field protected final database:Lcom/google/android/videos/store/Database;

.field private final debug:Z

.field protected final details:Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;

.field protected final key:Lcom/google/android/videos/pinning/DownloadKey;

.field private lastReportedProgress:J

.field private mediaStream:Lcom/google/android/videos/streams/MediaStream;

.field private final progressListener:Lcom/google/android/videos/pinning/Downloader$ProgressListener;

.field protected final rootFilesDir:Ljava/io/File;

.field protected final surroundSound:Z

.field private final taskStatus:Lcom/google/android/videos/async/TaskStatus;

.field protected final videosGlobals:Lcom/google/android/videos/VideosGlobals;


# direct methods
.method protected constructor <init>(Lcom/google/android/videos/VideosGlobals;Lcom/google/android/videos/pinning/Downloader$ProgressListener;Lcom/google/android/videos/pinning/DownloadKey;Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/io/File;Lcom/google/android/videos/async/TaskStatus;Z)V
    .locals 3
    .param p1, "videosGlobals"    # Lcom/google/android/videos/VideosGlobals;
    .param p2, "progressListener"    # Lcom/google/android/videos/pinning/Downloader$ProgressListener;
    .param p3, "key"    # Lcom/google/android/videos/pinning/DownloadKey;
    .param p4, "details"    # Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;
    .param p5, "rootFilesDir"    # Ljava/io/File;
    .param p6, "taskStatus"    # Lcom/google/android/videos/async/TaskStatus;
    .param p7, "debug"    # Z

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/VideosGlobals;

    iput-object v0, p0, Lcom/google/android/videos/pinning/Downloader;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    .line 100
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/pinning/Downloader$ProgressListener;

    iput-object v0, p0, Lcom/google/android/videos/pinning/Downloader;->progressListener:Lcom/google/android/videos/pinning/Downloader$ProgressListener;

    .line 101
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/pinning/DownloadKey;

    iput-object v0, p0, Lcom/google/android/videos/pinning/Downloader;->key:Lcom/google/android/videos/pinning/DownloadKey;

    .line 102
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;

    iput-object v0, p0, Lcom/google/android/videos/pinning/Downloader;->details:Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;

    .line 103
    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, Lcom/google/android/videos/pinning/Downloader;->rootFilesDir:Ljava/io/File;

    .line 104
    iput-object p6, p0, Lcom/google/android/videos/pinning/Downloader;->taskStatus:Lcom/google/android/videos/async/TaskStatus;

    .line 105
    iput-boolean p7, p0, Lcom/google/android/videos/pinning/Downloader;->debug:Z

    .line 106
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pinning/Downloader;->database:Lcom/google/android/videos/store/Database;

    .line 107
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "enable_surround_sound"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/videos/pinning/Downloader;->surroundSound:Z

    .line 108
    return-void
.end method

.method private downloadKnowledge(Lcom/google/android/videos/streams/MediaStream;J)V
    .locals 6
    .param p1, "mediaStream"    # Lcom/google/android/videos/streams/MediaStream;
    .param p2, "dashStreamTimestampForKnowledge"    # J

    .prologue
    .line 160
    iget-object v4, p0, Lcom/google/android/videos/pinning/Downloader;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getKnowledgeClient()Lcom/google/android/videos/tagging/KnowledgeClient;

    move-result-object v1

    .line 161
    .local v1, "knowledgeClient":Lcom/google/android/videos/tagging/KnowledgeClient;
    if-nez v1, :cond_0

    .line 180
    :goto_0
    return-void

    .line 165
    :cond_0
    iget-object v4, p0, Lcom/google/android/videos/pinning/Downloader;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/videos/pinning/Downloader;->key:Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v5, v5, Lcom/google/android/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/android/videos/store/ConfigurationStore;->getPlayCountry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 166
    .local v3, "userCountry":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v2

    .line 167
    .local v2, "knowledgeFuture":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/tagging/KnowledgeRequest;Lcom/google/android/videos/tagging/KnowledgeBundle;>;"
    iget-object v4, p1, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    iget-boolean v4, v4, Lcom/google/android/videos/streams/ItagInfo;->isDash:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/videos/pinning/Downloader;->key:Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v4, v4, Lcom/google/android/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/videos/pinning/Downloader;->key:Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v5, v5, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-static {v4, v5, v3, p2, p3}, Lcom/google/android/videos/tagging/KnowledgeRequest;->createForDashDownloadWithCurrentLocale(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/videos/tagging/KnowledgeRequest;

    move-result-object v4

    :goto_1
    iget-object v5, p0, Lcom/google/android/videos/pinning/Downloader;->details:Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;

    iget v5, v5, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->storage:I

    invoke-interface {v1, v4, v5, v2}, Lcom/google/android/videos/tagging/KnowledgeClient;->requestPinnedKnowledgeBundle(Lcom/google/android/videos/tagging/KnowledgeRequest;ILcom/google/android/videos/async/Callback;)V

    .line 175
    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 176
    :catch_0
    move-exception v0

    .line 177
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    const-string v4, "Unable to fetch knowledge"

    invoke-static {v4, v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 167
    .end local v0    # "e":Ljava/util/concurrent/ExecutionException;
    :cond_1
    iget-object v4, p0, Lcom/google/android/videos/pinning/Downloader;->key:Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v4, v4, Lcom/google/android/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/videos/pinning/Downloader;->key:Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v5, v5, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-static {v4, v5, v3, p1}, Lcom/google/android/videos/tagging/KnowledgeRequest;->createWithCurrentLocale(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/streams/MediaStream;)Lcom/google/android/videos/tagging/KnowledgeRequest;

    move-result-object v4

    goto :goto_1
.end method

.method private downloadStoryboard(Lcom/google/wireless/android/video/magma/proto/Storyboard;)V
    .locals 7
    .param p1, "storyboard"    # Lcom/google/wireless/android/video/magma/proto/Storyboard;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/PinningTask$PinningException;
        }
    .end annotation

    .prologue
    .line 231
    if-nez p1, :cond_1

    .line 254
    :cond_0
    :goto_0
    return-void

    .line 237
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v2

    .line 238
    .local v2, "syncCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/String;Ljava/lang/Void;>;"
    iget-object v3, p0, Lcom/google/android/videos/pinning/Downloader;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v3}, Lcom/google/android/videos/VideosGlobals;->getStoryboardClient()Lcom/google/android/videos/store/StoryboardClient;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/videos/pinning/Downloader;->key:Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v4, v4, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/videos/pinning/Downloader;->details:Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;

    iget v5, v5, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->storage:I

    invoke-virtual {v3, v4, p1, v5, v2}, Lcom/google/android/videos/store/StoryboardClient;->saveOfflineStoryboard(Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/Storyboard;ILcom/google/android/videos/async/Callback;)V

    .line 240
    invoke-virtual {v2}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;

    .line 241
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v3, p1, Lcom/google/wireless/android/video/magma/proto/Storyboard;->urls:[Ljava/lang/String;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 242
    invoke-virtual {p0}, Lcom/google/android/videos/pinning/Downloader;->isCanceled()Z

    move-result v3

    if-nez v3, :cond_0

    .line 245
    invoke-direct {p0, p1, v1}, Lcom/google/android/videos/pinning/Downloader;->downloadStoryboardImage(Lcom/google/wireless/android/video/magma/proto/Storyboard;I)V
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 241
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 247
    .end local v1    # "i":I
    .end local v2    # "syncCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/String;Ljava/lang/Void;>;"
    :catch_0
    move-exception v0

    .line 248
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    const-string v3, "Unable to offline storyboard"

    invoke-static {v3, v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 249
    new-instance v3, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getMessage()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/16 v6, 0x1a

    invoke-direct {v3, v4, v5, v6}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    throw v3

    .line 253
    .end local v0    # "e":Ljava/util/concurrent/ExecutionException;
    .restart local v1    # "i":I
    .restart local v2    # "syncCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/String;Ljava/lang/Void;>;"
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/videos/pinning/Downloader;->notifyProgress()V

    goto :goto_0
.end method

.method private downloadStoryboardImage(Lcom/google/wireless/android/video/magma/proto/Storyboard;I)V
    .locals 5
    .param p1, "storyboard"    # Lcom/google/wireless/android/video/magma/proto/Storyboard;
    .param p2, "imageIndex"    # I

    .prologue
    .line 260
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v2

    .line 261
    .local v2, "syncCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/store/StoryboardImageRequest;Landroid/graphics/Bitmap;>;"
    new-instance v1, Lcom/google/android/videos/store/StoryboardImageRequest;

    iget-object v3, p0, Lcom/google/android/videos/pinning/Downloader;->key:Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v3, v3, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-direct {v1, v3, p1, p2}, Lcom/google/android/videos/store/StoryboardImageRequest;-><init>(Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/Storyboard;I)V

    .line 263
    .local v1, "request":Lcom/google/android/videos/store/StoryboardImageRequest;
    iget-object v3, p0, Lcom/google/android/videos/pinning/Downloader;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v3}, Lcom/google/android/videos/VideosGlobals;->getStoryboardClient()Lcom/google/android/videos/store/StoryboardClient;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/videos/pinning/Downloader;->details:Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;

    iget v4, v4, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->storage:I

    invoke-virtual {v3, v1, v4, v2}, Lcom/google/android/videos/store/StoryboardClient;->saveStoryboardImage(Lcom/google/android/videos/store/StoryboardImageRequest;ILcom/google/android/videos/async/Callback;)V

    .line 265
    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 270
    :goto_0
    return-void

    .line 266
    :catch_0
    move-exception v0

    .line 267
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    const-string v3, "Unable to fetch storyboard image"

    invoke-static {v3, v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private downloadSubtitleTrack(Lcom/google/android/videos/subtitles/SubtitleTrack;)V
    .locals 4
    .param p1, "track"    # Lcom/google/android/videos/subtitles/SubtitleTrack;

    .prologue
    .line 215
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Requesting subtitles "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 216
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v1

    .line 217
    .local v1, "syncCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/subtitles/SubtitleTrack;Lcom/google/android/videos/subtitles/Subtitles;>;"
    iget-object v2, p0, Lcom/google/android/videos/pinning/Downloader;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getSubtitlesClient()Lcom/google/android/videos/store/SubtitlesClient;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/pinning/Downloader;->details:Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;

    iget v3, v3, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->storage:I

    invoke-virtual {v2, p1, v3, v1}, Lcom/google/android/videos/store/SubtitlesClient;->saveSubtitles(Lcom/google/android/videos/subtitles/SubtitleTrack;ILcom/google/android/videos/async/Callback;)V

    .line 219
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 224
    :goto_0
    return-void

    .line 220
    :catch_0
    move-exception v0

    .line 221
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    const-string v2, "Unable to fetch subtitle track"

    invoke-static {v2, v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private downloadSubtitles(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/PinningTask$PinningException;
        }
    .end annotation

    .prologue
    .line 187
    .local p1, "subtitleTracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/subtitles/SubtitleTrack;>;"
    :try_start_0
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v2

    .line 188
    .local v2, "syncCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/String;Ljava/lang/Void;>;"
    iget-object v4, p0, Lcom/google/android/videos/pinning/Downloader;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getSubtitlesClient()Lcom/google/android/videos/store/SubtitlesClient;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/videos/pinning/Downloader;->key:Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v5, v5, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/videos/pinning/Downloader;->details:Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;

    iget v6, v6, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->storage:I

    invoke-virtual {v4, v5, p1, v6, v2}, Lcom/google/android/videos/store/SubtitlesClient;->saveOfflineSubtitleTracks(Ljava/lang/String;Ljava/util/List;ILcom/google/android/videos/async/Callback;)V

    .line 190
    invoke-virtual {v2}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;

    .line 191
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 192
    invoke-virtual {p0}, Lcom/google/android/videos/pinning/Downloader;->isCanceled()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 209
    :goto_1
    return-void

    .line 197
    :cond_0
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/videos/subtitles/SubtitleTrack;

    invoke-direct {p0, v4}, Lcom/google/android/videos/pinning/Downloader;->downloadSubtitleTrack(Lcom/google/android/videos/subtitles/SubtitleTrack;)V
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 191
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 199
    .end local v1    # "i":I
    .end local v2    # "syncCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/String;Ljava/lang/Void;>;"
    :catch_0
    move-exception v0

    .line 200
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    const-string v4, "Unable to offline subtitles"

    invoke-static {v4, v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 201
    new-instance v4, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getMessage()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/16 v7, 0x10

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    throw v4

    .line 205
    .end local v0    # "e":Ljava/util/concurrent/ExecutionException;
    .restart local v1    # "i":I
    .restart local v2    # "syncCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/String;Ljava/lang/Void;>;"
    :cond_1
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 206
    .local v3, "values":Landroid/content/ContentValues;
    const-string v4, "have_subtitles"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 207
    iget-object v4, p0, Lcom/google/android/videos/pinning/Downloader;->database:Lcom/google/android/videos/store/Database;

    iget-object v5, p0, Lcom/google/android/videos/pinning/Downloader;->key:Lcom/google/android/videos/pinning/DownloadKey;

    invoke-static {v4, v5, v3}, Lcom/google/android/videos/pinning/PinningDbHelper;->updatePinningStateForVideo(Lcom/google/android/videos/store/Database;Lcom/google/android/videos/pinning/DownloadKey;Landroid/content/ContentValues;)V

    .line 208
    invoke-virtual {p0}, Lcom/google/android/videos/pinning/Downloader;->notifyProgress()V

    goto :goto_1
.end method

.method private getNeededBytes(JLjava/lang/String;)J
    .locals 7
    .param p1, "streamSize"    # J
    .param p3, "relativeFilePath"    # Ljava/lang/String;

    .prologue
    .line 337
    move-wide v2, p1

    .line 338
    .local v2, "remainingSize":J
    new-instance v1, Ljava/io/File;

    iget-object v4, p0, Lcom/google/android/videos/pinning/Downloader;->rootFilesDir:Ljava/io/File;

    invoke-direct {v1, v4, p3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 339
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 341
    :try_start_0
    invoke-static {v1}, Lcom/google/android/videos/utils/OfflineUtil;->getRecursiveFileSize(Ljava/io/File;)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 347
    :cond_0
    :goto_0
    return-wide v2

    .line 342
    :catch_0
    move-exception v0

    .line 344
    .local v0, "e":Ljava/io/IOException;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error while fetching file size "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private persistBytesDownloaded(J)V
    .locals 3
    .param p1, "downloaded"    # J

    .prologue
    .line 359
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 360
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "download_bytes_downloaded"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 361
    iget-object v1, p0, Lcom/google/android/videos/pinning/Downloader;->database:Lcom/google/android/videos/store/Database;

    iget-object v2, p0, Lcom/google/android/videos/pinning/Downloader;->key:Lcom/google/android/videos/pinning/DownloadKey;

    invoke-static {v1, v2, v0}, Lcom/google/android/videos/pinning/PinningDbHelper;->updatePinningStateForVideo(Lcom/google/android/videos/store/Database;Lcom/google/android/videos/pinning/DownloadKey;Landroid/content/ContentValues;)V

    .line 362
    return-void
.end method


# virtual methods
.method protected final checkSufficientFreeSpace(JLjava/lang/String;)V
    .locals 15
    .param p1, "videoSize"    # J
    .param p3, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/PinningTask$PinningException;
        }
    .end annotation

    .prologue
    .line 317
    invoke-direct/range {p0 .. p3}, Lcom/google/android/videos/pinning/Downloader;->getNeededBytes(JLjava/lang/String;)J

    move-result-wide v10

    .line 318
    .local v10, "neededBytes":J
    iget-object v1, p0, Lcom/google/android/videos/pinning/Downloader;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v1}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 319
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "purchased_assets"

    sget-object v2, Lcom/google/android/videos/pinning/Downloader$ReservedSpaceQuery;->PROJECTION:[Ljava/lang/String;

    const-string v3, "asset_type IN (6,20) AND (pinned IS NOT NULL AND pinned > 0) AND pinning_status != 4 AND NOT (account = ? AND asset_id = ?)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/videos/pinning/Downloader;->key:Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v6, v6, Lcom/google/android/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/google/android/videos/pinning/Downloader;->key:Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v6, v6, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 323
    .local v14, "reservedSpaceCursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    .line 324
    iget-object v1, p0, Lcom/google/android/videos/pinning/Downloader;->rootFilesDir:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/utils/Util;->getAvailableSize(Ljava/lang/String;)J

    move-result-wide v8

    .line 325
    .local v8, "available":J
    const/4 v1, 0x0

    invoke-interface {v14, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-wide/16 v12, 0x0

    .line 327
    .local v12, "reserved":J
    :goto_0
    sub-long v2, v8, v12

    cmp-long v1, v2, v10

    if-gez v1, :cond_1

    .line 328
    new-instance v1, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    const-string v2, "insufficient free space"

    const/4 v3, 0x1

    const/4 v4, 0x7

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 332
    .end local v8    # "available":J
    .end local v12    # "reserved":J
    :catchall_0
    move-exception v1

    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    throw v1

    .line 325
    .restart local v8    # "available":J
    :cond_0
    const/4 v1, 0x0

    :try_start_1
    invoke-interface {v14, v1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v12

    goto :goto_0

    .line 332
    .restart local v12    # "reserved":J
    :cond_1
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 334
    return-void
.end method

.method protected debug(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "downloadPath"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 371
    iget-boolean v0, p0, Lcom/google/android/videos/pinning/Downloader;->debug:Z

    if-eqz v0, :cond_0

    .line 372
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 374
    :cond_0
    invoke-static {p2}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 375
    return-void
.end method

.method protected final doProgress(JJZ)V
    .locals 3
    .param p1, "downloaded"    # J
    .param p3, "progressGranularity"    # J
    .param p5, "last"    # Z

    .prologue
    .line 351
    iget-wide v0, p0, Lcom/google/android/videos/pinning/Downloader;->lastReportedProgress:J

    sub-long v0, p1, v0

    cmp-long v0, v0, p3

    if-gtz v0, :cond_0

    if-eqz p5, :cond_1

    .line 352
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/pinning/Downloader;->persistBytesDownloaded(J)V

    .line 353
    iput-wide p1, p0, Lcom/google/android/videos/pinning/Downloader;->lastReportedProgress:J

    .line 354
    invoke-virtual {p0}, Lcom/google/android/videos/pinning/Downloader;->notifyProgress()V

    .line 356
    :cond_1
    return-void
.end method

.method public final download()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/PinningTask$PinningException;
        }
    .end annotation

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/google/android/videos/pinning/Downloader;->setupDownload()Lcom/google/android/videos/pinning/Downloader$SetupResult;

    move-result-object v0

    .line 119
    .local v0, "setupResult":Lcom/google/android/videos/pinning/Downloader$SetupResult;
    invoke-virtual {p0}, Lcom/google/android/videos/pinning/Downloader;->isCanceled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 140
    :cond_0
    :goto_0
    return-void

    .line 122
    :cond_1
    iget-object v1, v0, Lcom/google/android/videos/pinning/Downloader$SetupResult;->mediaStream:Lcom/google/android/videos/streams/MediaStream;

    iput-object v1, p0, Lcom/google/android/videos/pinning/Downloader;->mediaStream:Lcom/google/android/videos/streams/MediaStream;

    .line 124
    iget-object v1, v0, Lcom/google/android/videos/pinning/Downloader$SetupResult;->subtitleTracks:Ljava/util/List;

    invoke-direct {p0, v1}, Lcom/google/android/videos/pinning/Downloader;->downloadSubtitles(Ljava/util/List;)V

    .line 125
    invoke-virtual {p0}, Lcom/google/android/videos/pinning/Downloader;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 129
    iget-object v1, v0, Lcom/google/android/videos/pinning/Downloader$SetupResult;->storyboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

    invoke-direct {p0, v1}, Lcom/google/android/videos/pinning/Downloader;->downloadStoryboard(Lcom/google/wireless/android/video/magma/proto/Storyboard;)V

    .line 130
    invoke-virtual {p0}, Lcom/google/android/videos/pinning/Downloader;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 134
    iget-object v1, v0, Lcom/google/android/videos/pinning/Downloader$SetupResult;->mediaStream:Lcom/google/android/videos/streams/MediaStream;

    iget-wide v2, v0, Lcom/google/android/videos/pinning/Downloader$SetupResult;->dashStreamTimestampForKnowledge:J

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/videos/pinning/Downloader;->downloadKnowledge(Lcom/google/android/videos/streams/MediaStream;J)V

    .line 135
    invoke-virtual {p0}, Lcom/google/android/videos/pinning/Downloader;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 139
    invoke-virtual {p0}, Lcom/google/android/videos/pinning/Downloader;->downloadMedia()V

    goto :goto_0
.end method

.method protected abstract downloadMedia()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/PinningTask$PinningException;
        }
    .end annotation
.end method

.method public final getMediaStream()Lcom/google/android/videos/streams/MediaStream;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/videos/pinning/Downloader;->mediaStream:Lcom/google/android/videos/streams/MediaStream;

    return-object v0
.end method

.method protected final getStreams(ZZ)Lcom/google/android/videos/streams/Streams;
    .locals 12
    .param p1, "useDash"    # Z
    .param p2, "isEpisode"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/PinningTask$PinningException;
        }
    .end annotation

    .prologue
    const/16 v11, 0xf

    const/4 v5, 0x1

    .line 292
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v10

    .line 293
    .local v10, "streamsCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/android/videos/streams/Streams;>;"
    new-instance v0, Lcom/google/android/videos/api/MpdGetRequest;

    iget-object v1, p0, Lcom/google/android/videos/pinning/Downloader;->key:Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v1, v1, Lcom/google/android/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/pinning/Downloader;->key:Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v2, v2, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/pinning/Downloader;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v3}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/videos/Config;->useSslForDownloads()Z

    move-result v6

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    move v3, p2

    move v4, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/api/MpdGetRequest;-><init>(Ljava/lang/String;Ljava/lang/String;ZZZZLjava/util/Locale;)V

    .line 295
    .local v0, "request":Lcom/google/android/videos/api/MpdGetRequest;
    iget-object v1, p0, Lcom/google/android/videos/pinning/Downloader;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/videos/api/ApiRequesters;->getStreamsRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v1

    invoke-interface {v1, v0, v10}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 297
    :try_start_0
    invoke-virtual {v10}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/videos/streams/Streams;

    .line 298
    .local v9, "streams":Lcom/google/android/videos/streams/Streams;
    iget-object v1, v9, Lcom/google/android/videos/streams/Streams;->mediaStreams:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 299
    new-instance v1, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    const-string v2, "empty list of permitted streams "

    const/4 v3, 0x1

    const/16 v4, 0xf

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    throw v1
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 303
    .end local v9    # "streams":Lcom/google/android/videos/streams/Streams;
    :catch_0
    move-exception v8

    .line 304
    .local v8, "e":Ljava/util/concurrent/ExecutionException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "failed to get streams for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/pinning/Downloader;->key:Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v2, v2, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 305
    new-instance v1, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    const-string v2, "could not fetch permitted streams"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v11}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    throw v1

    .line 302
    .end local v8    # "e":Ljava/util/concurrent/ExecutionException;
    .restart local v9    # "streams":Lcom/google/android/videos/streams/Streams;
    :cond_0
    return-object v9
.end method

.method protected isCanceled()Z
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/videos/pinning/Downloader;->taskStatus:Lcom/google/android/videos/async/TaskStatus;

    invoke-virtual {v0}, Lcom/google/android/videos/async/TaskStatus;->isCancelled()Z

    move-result v0

    return v0
.end method

.method protected final notifyProgress()V
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/videos/pinning/Downloader;->progressListener:Lcom/google/android/videos/pinning/Downloader$ProgressListener;

    invoke-interface {v0}, Lcom/google/android/videos/pinning/Downloader$ProgressListener;->onDownloadProgress()V

    .line 285
    return-void
.end method

.method protected final persistDownloadSize(J)V
    .locals 7
    .param p1, "size"    # J

    .prologue
    .line 365
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 366
    .local v0, "values":Landroid/content/ContentValues;
    const-string v2, "pinning_download_size"

    const-wide/16 v4, 0x0

    cmp-long v1, p1, v4

    if-lez v1, :cond_0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 367
    iget-object v1, p0, Lcom/google/android/videos/pinning/Downloader;->database:Lcom/google/android/videos/store/Database;

    iget-object v2, p0, Lcom/google/android/videos/pinning/Downloader;->key:Lcom/google/android/videos/pinning/DownloadKey;

    invoke-static {v1, v2, v0}, Lcom/google/android/videos/pinning/PinningDbHelper;->updatePinningStateForVideo(Lcom/google/android/videos/store/Database;Lcom/google/android/videos/pinning/DownloadKey;Landroid/content/ContentValues;)V

    .line 368
    return-void

    .line 366
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected abstract setupDownload()Lcom/google/android/videos/pinning/Downloader$SetupResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/PinningTask$PinningException;
        }
    .end annotation
.end method
