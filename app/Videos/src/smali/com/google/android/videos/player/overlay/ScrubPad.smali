.class public Lcom/google/android/videos/player/overlay/ScrubPad;
.super Landroid/widget/FrameLayout;
.source "ScrubPad.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/overlay/ScrubPad$Listener;
    }
.end annotation


# static fields
.field public static final STATE_COLLAPSED:I = 0x0

.field public static final STATE_EXPANDED:I = 0x1


# instance fields
.field private collapsedScaleX:F

.field private collapsedScaleY:F

.field private expandedHeight:I

.field private expandedWidth:I

.field private expandingState:I

.field private fadeInAnimator:Landroid/animation/ObjectAnimator;

.field private fadeOutAnimator:Landroid/animation/ObjectAnimator;

.field private final foregroundColor:I

.field private final graphicLayout:Landroid/widget/LinearLayout;

.field private isBeingDragged:Z

.field private listener:Lcom/google/android/videos/player/overlay/ScrubPad$Listener;

.field private marginBottom:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 75
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 63
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->expandingState:I

    .line 76
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400b9

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 77
    const v0, 0x7f0f01ed

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/overlay/ScrubPad;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->graphicLayout:Landroid/widget/LinearLayout;

    .line 78
    const v0, 0x7f0a00c3

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/overlay/ScrubPad;->setBackgroundResource(I)V

    .line 79
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/ScrubPad;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00e9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->foregroundColor:I

    .line 80
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget v1, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->foregroundColor:I

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/overlay/ScrubPad;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 81
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e01ed

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->marginBottom:F

    .line 82
    return-void
.end method

.method private adjustGraphicScale(FF)V
    .locals 3
    .param p1, "scrubPadScaleX"    # F
    .param p2, "scrubPadScaleY"    # F

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 353
    cmpl-float v0, p1, p2

    if-ltz v0, :cond_1

    .line 354
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->graphicLayout:Landroid/widget/LinearLayout;

    div-float v1, p2, p1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setScaleX(F)V

    .line 355
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->graphicLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setScaleY(F)V

    .line 360
    :cond_0
    :goto_0
    return-void

    .line 356
    :cond_1
    cmpg-float v0, p1, p2

    if-gez v0, :cond_0

    .line 357
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->graphicLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setScaleX(F)V

    .line 358
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->graphicLayout:Landroid/widget/LinearLayout;

    div-float v1, p1, p2

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setScaleY(F)V

    goto :goto_0
.end method

.method private expandTo(FF)V
    .locals 4
    .param p1, "targetScaleX"    # F
    .param p2, "targetScaleY"    # F

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 204
    iget v1, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->collapsedScaleX:F

    invoke-static {p1, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-static {v1, v3}, Ljava/lang/Math;->min(FF)F

    move-result p1

    .line 205
    iget v1, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->collapsedScaleY:F

    invoke-static {p2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-static {v1, v3}, Ljava/lang/Math;->min(FF)F

    move-result p2

    .line 206
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/ScrubPad;->getScaleX()F

    move-result v1

    cmpl-float v1, p1, v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/ScrubPad;->getScaleY()F

    move-result v1

    cmpl-float v1, p2, v1

    if-nez v1, :cond_1

    .line 226
    :cond_0
    :goto_0
    return-void

    .line 209
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/ScrubPad;->getScaleX()F

    move-result v1

    iget v2, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->collapsedScaleX:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_2

    .line 211
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/videos/player/overlay/ScrubPad;->setVisibility(I)V

    .line 213
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/videos/player/overlay/ScrubPad;->setScaleX(F)V

    .line 214
    invoke-virtual {p0, p2}, Lcom/google/android/videos/player/overlay/ScrubPad;->setScaleY(F)V

    .line 215
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/player/overlay/ScrubPad;->adjustGraphicScale(FF)V

    .line 216
    iget v1, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->collapsedScaleX:F

    cmpl-float v1, p1, v1

    if-nez v1, :cond_3

    .line 219
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/google/android/videos/player/overlay/ScrubPad;->setVisibility(I)V

    .line 221
    :cond_3
    iget v1, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->collapsedScaleY:F

    sub-float v1, p2, v1

    iget v2, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->collapsedScaleY:F

    sub-float v2, v3, v2

    div-float v0, v1, v2

    .line 222
    .local v0, "scaleRatio":F
    invoke-direct {p0, v0}, Lcom/google/android/videos/player/overlay/ScrubPad;->updateForegroundAlpha(F)V

    .line 223
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->listener:Lcom/google/android/videos/player/overlay/ScrubPad$Listener;

    if-eqz v1, :cond_0

    .line 224
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->listener:Lcom/google/android/videos/player/overlay/ScrubPad$Listener;

    invoke-interface {v1, v0}, Lcom/google/android/videos/player/overlay/ScrubPad$Listener;->onScaling(F)V

    goto :goto_0
.end method

.method private onMoveFinished(F)V
    .locals 8
    .param p1, "velocity"    # F

    .prologue
    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 136
    iget-boolean v6, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->isBeingDragged:Z

    if-nez v6, :cond_0

    .line 160
    :goto_0
    return-void

    .line 141
    :cond_0
    const v6, 0x3e99999a    # 0.3f

    mul-float/2addr v6, p1

    iget v7, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->expandedHeight:I

    int-to-float v7, v7

    div-float/2addr v6, v7

    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/ScrubPad;->getScaleY()F

    move-result v7

    add-float v3, v6, v7

    .line 143
    .local v3, "projectedScale":F
    sub-float v6, v0, v3

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    sub-float v7, v5, v3

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    cmpg-float v6, v6, v7

    if-gez v6, :cond_2

    .line 144
    const/4 v1, 0x0

    .line 151
    .local v1, "destinationState":I
    :goto_1
    iget v6, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->expandingState:I

    if-ne v1, v6, :cond_1

    .line 152
    iget v6, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->expandingState:I

    if-nez v6, :cond_3

    const/4 v2, 0x1

    .line 153
    .local v2, "nextState":I
    :goto_2
    iget v6, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->expandingState:I

    if-nez v6, :cond_4

    .line 154
    .local v0, "currentStateScale":F
    :goto_3
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/ScrubPad;->getScaleY()F

    move-result v5

    sub-float/2addr v5, v0

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const v6, 0x3ecccccd    # 0.4f

    cmpl-float v5, v5, v6

    if-lez v5, :cond_1

    .line 155
    move v1, v2

    .line 158
    .end local v0    # "currentStateScale":F
    .end local v2    # "nextState":I
    :cond_1
    invoke-virtual {p0, v1}, Lcom/google/android/videos/player/overlay/ScrubPad;->smoothExpandToState(I)V

    .line 159
    iput-boolean v4, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->isBeingDragged:Z

    goto :goto_0

    .line 146
    .end local v1    # "destinationState":I
    :cond_2
    const/4 v1, 0x1

    .restart local v1    # "destinationState":I
    goto :goto_1

    :cond_3
    move v2, v4

    .line 152
    goto :goto_2

    .restart local v2    # "nextState":I
    :cond_4
    move v0, v5

    .line 153
    goto :goto_3
.end method

.method private updateForegroundAlpha(F)V
    .locals 6
    .param p1, "scaleRatio"    # F

    .prologue
    const/high16 v5, 0x3f000000    # 0.5f

    .line 339
    cmpg-float v2, p1, v5

    if-gez v2, :cond_0

    .line 340
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    const/high16 v3, -0x1000000

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v2}, Lcom/google/android/videos/player/overlay/ScrubPad;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 347
    :goto_0
    return-void

    .line 343
    :cond_0
    iget v2, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->foregroundColor:I

    invoke-static {v2}, Landroid/graphics/Color;->alpha(I)I

    move-result v1

    .line 344
    .local v1, "endAlpha":I
    const/high16 v2, 0x437f0000    # 255.0f

    rsub-int v3, v1, 0xff

    int-to-float v3, v3

    sub-float v4, p1, v5

    mul-float/2addr v3, v4

    div-float/2addr v3, v5

    sub-float/2addr v2, v3

    float-to-int v0, v2

    .line 345
    .local v0, "alpha":I
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    iget v3, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->foregroundColor:I

    invoke-static {v3}, Landroid/graphics/Color;->red(I)I

    move-result v3

    iget v4, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->foregroundColor:I

    invoke-static {v4}, Landroid/graphics/Color;->green(I)I

    move-result v4

    iget v5, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->foregroundColor:I

    invoke-static {v5}, Landroid/graphics/Color;->blue(I)I

    move-result v5

    invoke-static {v0, v3, v4, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v2}, Lcom/google/android/videos/player/overlay/ScrubPad;->setForeground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method


# virtual methods
.method public cancelGesture()V
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/overlay/ScrubPad;->onMoveFinished(F)V

    .line 133
    return-void
.end method

.method public fadeInGraphic()V
    .locals 5

    .prologue
    .line 280
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->fadeOutAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->fadeOutAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->fadeOutAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 283
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->fadeInAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->fadeInAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 288
    :goto_0
    return-void

    .line 286
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->graphicLayout:Landroid/widget/LinearLayout;

    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v2, 0x1

    new-array v2, v2, [F

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->fadeInAnimator:Landroid/animation/ObjectAnimator;

    .line 287
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->fadeInAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0
.end method

.method public fadeOutGraphic(Z)V
    .locals 5
    .param p1, "immediateHide"    # Z

    .prologue
    const/4 v4, 0x0

    .line 265
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->fadeInAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->fadeInAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->fadeInAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 268
    :cond_0
    if-eqz p1, :cond_2

    .line 269
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->graphicLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 277
    :cond_1
    :goto_0
    return-void

    .line 272
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->fadeOutAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->fadeOutAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_1

    .line 275
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->graphicLayout:Landroid/widget/LinearLayout;

    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v2, 0x1

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->fadeOutAnimator:Landroid/animation/ObjectAnimator;

    .line 276
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->fadeOutAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0
.end method

.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 325
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 309
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/ScrubPad;->getScaleX()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 310
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->expandingState:I

    .line 317
    :goto_0
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->listener:Lcom/google/android/videos/player/overlay/ScrubPad$Listener;

    if-eqz v0, :cond_0

    .line 318
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->listener:Lcom/google/android/videos/player/overlay/ScrubPad$Listener;

    iget v1, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->expandingState:I

    invoke-interface {v0, v1}, Lcom/google/android/videos/player/overlay/ScrubPad$Listener;->onExpandingStateChanged(I)V

    .line 320
    :cond_0
    return-void

    .line 314
    :cond_1
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/overlay/ScrubPad;->setVisibility(I)V

    .line 315
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->expandingState:I

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 330
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 304
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/overlay/ScrubPad;->setVisibility(I)V

    .line 305
    return-void
.end method

.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 6
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 292
    sget-object v3, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    invoke-virtual {v3}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 293
    .local v1, "scaleX":F
    sget-object v3, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    invoke-virtual {v3}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v2

    .line 294
    .local v2, "scaleY":F
    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/player/overlay/ScrubPad;->adjustGraphicScale(FF)V

    .line 295
    iget v3, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->collapsedScaleY:F

    sub-float v3, v2, v3

    const/high16 v4, 0x3f800000    # 1.0f

    iget v5, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->collapsedScaleY:F

    sub-float/2addr v4, v5

    div-float v0, v3, v4

    .line 296
    .local v0, "scaleRatio":F
    invoke-direct {p0, v0}, Lcom/google/android/videos/player/overlay/ScrubPad;->updateForegroundAlpha(F)V

    .line 297
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->listener:Lcom/google/android/videos/player/overlay/ScrubPad$Listener;

    if-eqz v3, :cond_0

    .line 298
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->listener:Lcom/google/android/videos/player/overlay/ScrubPad$Listener;

    invoke-interface {v3, v0}, Lcom/google/android/videos/player/overlay/ScrubPad$Listener;->onScaling(F)V

    .line 300
    :cond_0
    return-void
.end method

.method public onFling(F)V
    .locals 1
    .param p1, "velocityY"    # F

    .prologue
    .line 127
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->isBeingDragged:Z

    .line 128
    neg-float v0, p1

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/overlay/ScrubPad;->onMoveFinished(F)V

    .line 129
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 86
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 87
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/ScrubPad;->getMeasuredWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->expandedWidth:I

    .line 88
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/ScrubPad;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->expandedHeight:I

    .line 89
    return-void
.end method

.method public onScroll(F)V
    .locals 5
    .param p1, "distanceY"    # F

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 119
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->isBeingDragged:Z

    .line 121
    iget v1, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->expandedHeight:I

    int-to-float v1, v1

    div-float v0, p1, v1

    .line 122
    .local v0, "scaleFactor":F
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/ScrubPad;->getScaleX()F

    move-result v1

    iget v2, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->collapsedScaleX:F

    sub-float v2, v4, v2

    mul-float/2addr v2, v0

    add-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/ScrubPad;->getScaleY()F

    move-result v2

    iget v3, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->collapsedScaleY:F

    sub-float v3, v4, v3

    mul-float/2addr v3, v0

    add-float/2addr v2, v3

    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/player/overlay/ScrubPad;->expandTo(FF)V

    .line 124
    return-void
.end method

.method public setCollapsedPositionAndSize(FFII)V
    .locals 5
    .param p1, "collapsedPositionX"    # F
    .param p2, "collapsedPositionY"    # F
    .param p3, "collapsedWidth"    # I
    .param p4, "collapsedHeight"    # I

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 93
    iget v2, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->expandedWidth:I

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->expandedHeight:I

    if-nez v2, :cond_1

    .line 112
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    int-to-float v2, p3

    mul-float/2addr v2, v4

    iget v3, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->expandedWidth:I

    int-to-float v3, v3

    div-float v0, v2, v3

    .line 97
    .local v0, "collapsedScaleX":F
    int-to-float v2, p4

    mul-float/2addr v2, v4

    iget v3, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->expandedHeight:I

    int-to-float v3, v3

    div-float v1, v2, v3

    .line 98
    .local v1, "collapsedScaleY":F
    iget v2, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->collapsedScaleX:F

    cmpl-float v2, v2, v0

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->collapsedScaleY:F

    cmpl-float v2, v2, v1

    if-eqz v2, :cond_0

    .line 99
    iput v0, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->collapsedScaleX:F

    .line 100
    iput v1, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->collapsedScaleY:F

    .line 101
    div-int/lit8 v2, p3, 0x2

    int-to-float v2, v2

    add-float/2addr v2, p1

    invoke-virtual {p0, v2}, Lcom/google/android/videos/player/overlay/ScrubPad;->setPivotX(F)V

    .line 105
    iget v2, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->expandedHeight:I

    sub-int/2addr v2, p4

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->marginBottom:F

    sub-float/2addr v2, v3

    sub-float v3, v4, v1

    div-float/2addr v2, v3

    invoke-virtual {p0, v2}, Lcom/google/android/videos/player/overlay/ScrubPad;->setPivotY(F)V

    .line 106
    iget v2, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->expandingState:I

    if-nez v2, :cond_2

    .line 107
    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/player/overlay/ScrubPad;->expandTo(FF)V

    goto :goto_0

    .line 109
    :cond_2
    invoke-direct {p0, v4, v4}, Lcom/google/android/videos/player/overlay/ScrubPad;->expandTo(FF)V

    goto :goto_0
.end method

.method public setExpandingState(I)V
    .locals 7
    .param p1, "newState"    # I

    .prologue
    const v6, 0x7f0a00e9

    const/high16 v5, 0x3f800000    # 1.0f

    .line 179
    iget v4, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->expandingState:I

    if-ne p1, v4, :cond_1

    .line 201
    :cond_0
    :goto_0
    return-void

    .line 182
    :cond_1
    const/4 v4, 0x1

    if-ne p1, v4, :cond_2

    .line 183
    invoke-virtual {p0, v5}, Lcom/google/android/videos/player/overlay/ScrubPad;->setScaleX(F)V

    .line 184
    invoke-virtual {p0, v5}, Lcom/google/android/videos/player/overlay/ScrubPad;->setScaleY(F)V

    .line 185
    invoke-direct {p0, v5, v5}, Lcom/google/android/videos/player/overlay/ScrubPad;->adjustGraphicScale(FF)V

    .line 186
    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/ScrubPad;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-direct {v4, v5}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v4}, Lcom/google/android/videos/player/overlay/ScrubPad;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 197
    :goto_1
    iput p1, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->expandingState:I

    .line 198
    iget-object v4, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->listener:Lcom/google/android/videos/player/overlay/ScrubPad$Listener;

    if-eqz v4, :cond_0

    .line 199
    iget-object v4, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->listener:Lcom/google/android/videos/player/overlay/ScrubPad$Listener;

    invoke-interface {v4, p1}, Lcom/google/android/videos/player/overlay/ScrubPad$Listener;->onExpandingStateChanged(I)V

    goto :goto_0

    .line 188
    :cond_2
    iget v4, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->collapsedScaleX:F

    invoke-virtual {p0, v4}, Lcom/google/android/videos/player/overlay/ScrubPad;->setScaleX(F)V

    .line 189
    iget v4, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->collapsedScaleY:F

    invoke-virtual {p0, v4}, Lcom/google/android/videos/player/overlay/ScrubPad;->setScaleY(F)V

    .line 190
    iget v4, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->collapsedScaleX:F

    iget v5, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->collapsedScaleY:F

    invoke-direct {p0, v4, v5}, Lcom/google/android/videos/player/overlay/ScrubPad;->adjustGraphicScale(FF)V

    .line 191
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/ScrubPad;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 192
    .local v1, "foregroundColor":I
    invoke-static {v1}, Landroid/graphics/Color;->red(I)I

    move-result v3

    .line 193
    .local v3, "red":I
    invoke-static {v1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    .line 194
    .local v2, "green":I
    invoke-static {v1}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    .line 195
    .local v0, "blue":I
    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    const/16 v5, 0xff

    invoke-static {v5, v3, v2, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v5

    invoke-direct {v4, v5}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v4}, Lcom/google/android/videos/player/overlay/ScrubPad;->setForeground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method public setListener(Lcom/google/android/videos/player/overlay/ScrubPad$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/videos/player/overlay/ScrubPad$Listener;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->listener:Lcom/google/android/videos/player/overlay/ScrubPad$Listener;

    .line 116
    return-void
.end method

.method public smoothExpandToState(I)V
    .locals 12
    .param p1, "newState"    # I

    .prologue
    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 232
    if-ne p1, v9, :cond_1

    .line 233
    const/high16 v5, 0x3f800000    # 1.0f

    .line 234
    .local v5, "targetScaleX":F
    const/high16 v6, 0x3f800000    # 1.0f

    .line 239
    .local v6, "targetScaleY":F
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/ScrubPad;->getScaleX()F

    move-result v7

    cmpl-float v7, v5, v7

    if-nez v7, :cond_2

    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/ScrubPad;->getScaleY()F

    move-result v7

    cmpl-float v7, v6, v7

    if-nez v7, :cond_2

    .line 240
    iget v7, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->expandingState:I

    if-eq v7, p1, :cond_0

    .line 241
    iput p1, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->expandingState:I

    .line 242
    iget-object v7, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->listener:Lcom/google/android/videos/player/overlay/ScrubPad$Listener;

    if-eqz v7, :cond_0

    .line 243
    iget-object v7, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->listener:Lcom/google/android/videos/player/overlay/ScrubPad$Listener;

    iget v8, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->expandingState:I

    invoke-interface {v7, v8}, Lcom/google/android/videos/player/overlay/ScrubPad$Listener;->onExpandingStateChanged(I)V

    .line 262
    :cond_0
    :goto_1
    return-void

    .line 236
    .end local v5    # "targetScaleX":F
    .end local v6    # "targetScaleY":F
    :cond_1
    iget v5, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->collapsedScaleX:F

    .line 237
    .restart local v5    # "targetScaleX":F
    iget v6, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->collapsedScaleY:F

    .restart local v6    # "targetScaleY":F
    goto :goto_0

    .line 248
    :cond_2
    const/high16 v7, 0x43fa0000    # 500.0f

    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/ScrubPad;->getScaleX()F

    move-result v8

    sub-float v8, v5, v8

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    mul-float/2addr v7, v8

    iget v8, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->collapsedScaleX:F

    sub-float v8, v11, v8

    div-float/2addr v7, v8

    float-to-long v2, v7

    .line 250
    .local v2, "duration":J
    sget-object v7, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    new-array v8, v9, [F

    aput v5, v8, v10

    invoke-static {v7, v8}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    .line 251
    .local v1, "scaleXHolder":Landroid/animation/PropertyValuesHolder;
    sget-object v7, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    new-array v8, v9, [F

    aput v6, v8, v10

    invoke-static {v7, v8}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v4

    .line 252
    .local v4, "scaleYHolder":Landroid/animation/PropertyValuesHolder;
    const/4 v7, 0x2

    new-array v7, v7, [Landroid/animation/PropertyValuesHolder;

    aput-object v1, v7, v10

    aput-object v4, v7, v9

    invoke-static {p0, v7}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 254
    .local v0, "animator":Landroid/animation/ValueAnimator;
    sget v7, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v8, 0x15

    if-lt v7, v8, :cond_3

    .line 255
    new-instance v7, Landroid/view/animation/PathInterpolator;

    const v8, 0x3ecccccd    # 0.4f

    const/4 v9, 0x0

    const v10, 0x3e4ccccd    # 0.2f

    invoke-direct {v7, v8, v9, v10, v11}, Landroid/view/animation/PathInterpolator;-><init>(FFFF)V

    invoke-virtual {v0, v7}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 259
    :goto_2
    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 260
    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 261
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_1

    .line 257
    :cond_3
    new-instance v7, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v7}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v7}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    goto :goto_2
.end method

.method public toggle()V
    .locals 2

    .prologue
    .line 167
    iget v1, p0, Lcom/google/android/videos/player/overlay/ScrubPad;->expandingState:I

    if-nez v1, :cond_0

    .line 168
    const/4 v0, 0x1

    .line 172
    .local v0, "newState":I
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/overlay/ScrubPad;->smoothExpandToState(I)V

    .line 173
    return-void

    .line 170
    .end local v0    # "newState":I
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "newState":I
    goto :goto_0
.end method
