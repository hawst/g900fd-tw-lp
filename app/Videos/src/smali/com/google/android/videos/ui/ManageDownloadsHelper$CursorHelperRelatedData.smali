.class final Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;
.super Ljava/lang/Object;
.source "ManageDownloadsHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/ManageDownloadsHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "CursorHelperRelatedData"
.end annotation


# instance fields
.field private final dataSource:Lcom/google/android/videos/adapter/VideosDataSource;

.field public downloadedBytes:J

.field private pendingCursor:Z

.field public requiredBytes:J


# direct methods
.method private constructor <init>(Lcom/google/android/videos/adapter/VideosDataSource;)V
    .locals 0
    .param p1, "dataSource"    # Lcom/google/android/videos/adapter/VideosDataSource;

    .prologue
    .line 463
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 464
    iput-object p1, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;->dataSource:Lcom/google/android/videos/adapter/VideosDataSource;

    .line 465
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/adapter/VideosDataSource;Lcom/google/android/videos/ui/ManageDownloadsHelper$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/adapter/VideosDataSource;
    .param p2, "x1"    # Lcom/google/android/videos/ui/ManageDownloadsHelper$1;

    .prologue
    .line 454
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;-><init>(Lcom/google/android/videos/adapter/VideosDataSource;)V

    return-void
.end method

.method static synthetic access$202(Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;
    .param p1, "x1"    # Z

    .prologue
    .line 454
    iput-boolean p1, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;->pendingCursor:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;)Lcom/google/android/videos/adapter/VideosDataSource;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;

    .prologue
    .line 454
    iget-object v0, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;->dataSource:Lcom/google/android/videos/adapter/VideosDataSource;

    return-object v0
.end method


# virtual methods
.method public changeCursor(Landroid/database/Cursor;)V
    .locals 10
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const-wide/16 v6, 0x0

    .line 468
    iget-object v5, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;->dataSource:Lcom/google/android/videos/adapter/VideosDataSource;

    invoke-interface {v5, p1}, Lcom/google/android/videos/adapter/VideosDataSource;->changeCursor(Landroid/database/Cursor;)V

    .line 469
    iput-wide v6, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;->requiredBytes:J

    .line 470
    iput-wide v6, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;->downloadedBytes:J

    .line 471
    if-eqz p1, :cond_2

    .line 472
    const/4 v2, 0x0

    .local v2, "i":I
    iget-object v5, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;->dataSource:Lcom/google/android/videos/adapter/VideosDataSource;

    invoke-interface {v5}, Lcom/google/android/videos/adapter/VideosDataSource;->getCount()I

    move-result v4

    .local v4, "size":I
    :goto_0
    if-ge v2, v4, :cond_2

    .line 473
    iget-object v5, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;->dataSource:Lcom/google/android/videos/adapter/VideosDataSource;

    invoke-interface {v5, v2}, Lcom/google/android/videos/adapter/VideosDataSource;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/database/Cursor;

    .line 474
    .local v3, "item":Landroid/database/Cursor;
    iget-object v5, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;->dataSource:Lcom/google/android/videos/adapter/VideosDataSource;

    invoke-interface {v5, v3}, Lcom/google/android/videos/adapter/VideosDataSource;->isPinned(Landroid/database/Cursor;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 475
    iget-object v5, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;->dataSource:Lcom/google/android/videos/adapter/VideosDataSource;

    invoke-interface {v5, v3}, Lcom/google/android/videos/adapter/VideosDataSource;->getDownloadSize(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v1

    .line 476
    .local v1, "downloadSize":Ljava/lang/Long;
    if-eqz v1, :cond_0

    .line 477
    iget-wide v6, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;->requiredBytes:J

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    add-long/2addr v6, v8

    iput-wide v6, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;->requiredBytes:J

    .line 480
    :cond_0
    iget-object v5, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;->dataSource:Lcom/google/android/videos/adapter/VideosDataSource;

    invoke-interface {v5, v3}, Lcom/google/android/videos/adapter/VideosDataSource;->getBytesDownloaded(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    .line 481
    .local v0, "bytesDownloaded":Ljava/lang/Long;
    if-eqz v0, :cond_1

    .line 482
    iget-wide v6, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;->downloadedBytes:J

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    add-long/2addr v6, v8

    iput-wide v6, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;->downloadedBytes:J

    .line 472
    .end local v0    # "bytesDownloaded":Ljava/lang/Long;
    .end local v1    # "downloadSize":Ljava/lang/Long;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 487
    .end local v2    # "i":I
    .end local v3    # "item":Landroid/database/Cursor;
    .end local v4    # "size":I
    :cond_2
    return-void
.end method
