.class final Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "MovieDetailsFlowHelper.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/videos/flow/Bindable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/MovieDetailsFlowHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SubheadingDownloadViewHolder"
.end annotation


# instance fields
.field private final downloadStatusView:Landroid/widget/TextView;

.field private final downloadView:Lcom/google/android/videos/ui/DownloadView;

.field private final subheadingView:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;Landroid/view/View;)V
    .locals 1
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 485
    iput-object p1, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    .line 486
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 487
    const v0, 0x7f0f017d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->subheadingView:Landroid/widget/TextView;

    .line 488
    const v0, 0x7f0f00d1

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/DownloadView;

    iput-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->downloadView:Lcom/google/android/videos/ui/DownloadView;

    .line 489
    const v0, 0x7f0f017f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->downloadStatusView:Landroid/widget/TextView;

    .line 490
    # getter for: Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->allowDownloads:Z
    invoke-static {p1}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->access$600(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 491
    const v0, 0x7f0f017e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->setGone(Landroid/view/View;)V

    .line 492
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->downloadView:Lcom/google/android/videos/ui/DownloadView;

    invoke-direct {p0, v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->setGone(Landroid/view/View;)V

    .line 493
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->downloadStatusView:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->setGone(Landroid/view/View;)V

    .line 497
    :cond_0
    :goto_0
    return-void

    .line 494
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->downloadView:Lcom/google/android/videos/ui/DownloadView;

    if-eqz v0, :cond_0

    .line 495
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->downloadView:Lcom/google/android/videos/ui/DownloadView;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/DownloadView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private setGone(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 500
    if-eqz p1, :cond_0

    .line 501
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 503
    :cond_0
    return-void
.end method


# virtual methods
.method public bind()V
    .locals 7

    .prologue
    .line 507
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->subheadingView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    # getter for: Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->subheading:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->access$700(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 509
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    # getter for: Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->allowDownloads:Z
    invoke-static {v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->access$600(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    # getter for: Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->downloadStatus:Lcom/google/android/videos/store/VideoDownloadStatus;
    invoke-static {v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->access$800(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Lcom/google/android/videos/store/VideoDownloadStatus;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 510
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->downloadView:Lcom/google/android/videos/ui/DownloadView;

    iget-object v1, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    # getter for: Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->movieTitle:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->access$500(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/DownloadView;->setTitle(Ljava/lang/String;)V

    .line 511
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    # getter for: Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->downloadStatus:Lcom/google/android/videos/store/VideoDownloadStatus;
    invoke-static {v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->access$800(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Lcom/google/android/videos/store/VideoDownloadStatus;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/videos/store/VideoDownloadStatus;->haveLicense:Z

    iget-object v1, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    # getter for: Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->downloadStatus:Lcom/google/android/videos/store/VideoDownloadStatus;
    invoke-static {v1}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->access$800(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Lcom/google/android/videos/store/VideoDownloadStatus;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/videos/store/VideoDownloadStatus;->downloadSize:Ljava/lang/Long;

    iget-object v2, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    # getter for: Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->downloadStatus:Lcom/google/android/videos/store/VideoDownloadStatus;
    invoke-static {v2}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->access$800(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Lcom/google/android/videos/store/VideoDownloadStatus;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/videos/store/VideoDownloadStatus;->bytesDownloaded:Ljava/lang/Long;

    invoke-static {v0, v1, v2}, Lcom/google/android/videos/pinning/PinningStatusHelper;->getProgressFraction(ZLjava/lang/Long;Ljava/lang/Long;)F

    move-result v5

    .line 513
    .local v5, "progress":F
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->downloadView:Lcom/google/android/videos/ui/DownloadView;

    iget-object v1, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    # getter for: Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->account:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->access$900(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    # getter for: Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->videoId:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->access$200(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    # getter for: Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->downloadStatus:Lcom/google/android/videos/store/VideoDownloadStatus;
    invoke-static {v3}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->access$800(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Lcom/google/android/videos/store/VideoDownloadStatus;

    move-result-object v3

    iget-boolean v3, v3, Lcom/google/android/videos/store/VideoDownloadStatus;->pinned:Z

    iget-object v4, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    # getter for: Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->downloadStatus:Lcom/google/android/videos/store/VideoDownloadStatus;
    invoke-static {v4}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->access$800(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Lcom/google/android/videos/store/VideoDownloadStatus;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/videos/store/VideoDownloadStatus;->pinningStatus:Ljava/lang/Integer;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/videos/ui/DownloadView;->update(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Integer;F)V

    .line 515
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->downloadStatusView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 516
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    # getter for: Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->downloadStatus:Lcom/google/android/videos/store/VideoDownloadStatus;
    invoke-static {v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->access$800(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Lcom/google/android/videos/store/VideoDownloadStatus;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/videos/store/VideoDownloadStatus;->pinningStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    # getter for: Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->downloadStatus:Lcom/google/android/videos/store/VideoDownloadStatus;
    invoke-static {v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->access$800(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Lcom/google/android/videos/store/VideoDownloadStatus;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/videos/store/VideoDownloadStatus;->pinningStatus:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 518
    .local v6, "pinningStatus":I
    :goto_0
    packed-switch v6, :pswitch_data_0

    .line 537
    .end local v5    # "progress":F
    .end local v6    # "pinningStatus":I
    :cond_0
    :goto_1
    return-void

    .line 516
    .restart local v5    # "progress":F
    :cond_1
    const/4 v6, 0x5

    goto :goto_0

    .line 520
    .restart local v6    # "pinningStatus":I
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->downloadStatusView:Landroid/widget/TextView;

    const v1, 0x7f0b011e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 523
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->downloadStatusView:Landroid/widget/TextView;

    const v1, 0x7f0b0121

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 526
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->downloadStatusView:Landroid/widget/TextView;

    const v1, 0x7f0b011f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 529
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->downloadStatusView:Landroid/widget/TextView;

    const v1, 0x7f0b0120

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 532
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->downloadStatusView:Landroid/widget/TextView;

    const v1, 0x7f0b0124

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 518
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 541
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    # getter for: Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->downloadStatus:Lcom/google/android/videos/store/VideoDownloadStatus;
    invoke-static {v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->access$800(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Lcom/google/android/videos/store/VideoDownloadStatus;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 542
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    # getter for: Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->infoPanelItemClickListener:Lcom/google/android/videos/ui/MovieDetailsFlowHelper$InfoPanelItemClickListener;
    invoke-static {v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->access$400(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Lcom/google/android/videos/ui/MovieDetailsFlowHelper$InfoPanelItemClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;->this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    # getter for: Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->downloadStatus:Lcom/google/android/videos/store/VideoDownloadStatus;
    invoke-static {v1}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->access$800(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Lcom/google/android/videos/store/VideoDownloadStatus;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$InfoPanelItemClickListener;->onPinClick(Lcom/google/android/videos/store/VideoDownloadStatus;)V

    .line 544
    :cond_0
    return-void
.end method
