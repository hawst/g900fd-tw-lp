.class public final Lcom/google/android/videos/async/ConditionalHttpRequest;
.super Ljava/lang/Object;
.source "ConditionalHttpRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/async/ConditionalHttpRequest$Converter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final IDENTITY:Lcom/google/android/videos/converter/RequestConverter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/converter/RequestConverter",
            "<",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final eTag:Ljava/lang/String;

.field public final lastModified:Ljava/lang/String;

.field public final targetRequest:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TR;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lcom/google/android/videos/async/ConditionalHttpRequest$1;

    invoke-direct {v0}, Lcom/google/android/videos/async/ConditionalHttpRequest$1;-><init>()V

    sput-object v0, Lcom/google/android/videos/async/ConditionalHttpRequest;->IDENTITY:Lcom/google/android/videos/converter/RequestConverter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "eTag"    # Ljava/lang/String;
    .param p3, "lastModified"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 62
    .local p0, "this":Lcom/google/android/videos/async/ConditionalHttpRequest;, "Lcom/google/android/videos/async/ConditionalHttpRequest<TR;>;"
    .local p1, "targetRequest":Ljava/lang/Object;, "TR;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/google/android/videos/async/ConditionalHttpRequest;->targetRequest:Ljava/lang/Object;

    .line 64
    iput-object p2, p0, Lcom/google/android/videos/async/ConditionalHttpRequest;->eTag:Ljava/lang/String;

    .line 65
    iput-object p3, p0, Lcom/google/android/videos/async/ConditionalHttpRequest;->lastModified:Ljava/lang/String;

    .line 66
    return-void
.end method

.method public static create(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/async/ConditionalHttpRequest;
    .locals 1
    .param p1, "eTag"    # Ljava/lang/String;
    .param p2, "lastModified"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(TR;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/videos/async/ConditionalHttpRequest",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 24
    .local p0, "request":Ljava/lang/Object;, "TR;"
    new-instance v0, Lcom/google/android/videos/async/ConditionalHttpRequest;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/videos/async/ConditionalHttpRequest;-><init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static createConverter()Lcom/google/android/videos/converter/RequestConverter;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/converter/RequestConverter",
            "<",
            "Lcom/google/android/videos/async/ConditionalHttpRequest",
            "<",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    new-instance v0, Lcom/google/android/videos/async/ConditionalHttpRequest$Converter;

    sget-object v1, Lcom/google/android/videos/async/ConditionalHttpRequest;->IDENTITY:Lcom/google/android/videos/converter/RequestConverter;

    invoke-direct {v0, v1}, Lcom/google/android/videos/async/ConditionalHttpRequest$Converter;-><init>(Lcom/google/android/videos/converter/RequestConverter;)V

    return-object v0
.end method
