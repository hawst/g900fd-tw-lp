.class public Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;
.super Lcom/google/android/videos/ui/playnext/CardItemView;
.source "MovieSuggestionClusterItemView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView$Binder;
    }
.end annotation


# instance fields
.field private boundAsset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

.field private descriptionView:Landroid/widget/TextView;

.field private overflowView:Landroid/view/View;

.field private priceView:Lcom/google/android/play/layout/PlayCardLabelView;

.field private ratingBar:Lcom/google/android/play/layout/StarRatingBar;

.field private thumbnailBitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

.field private titleView:Landroid/widget/TextView;

.field private yearAndDurationView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/playnext/CardItemView;-><init>(Landroid/content/Context;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/ui/playnext/CardItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/ui/playnext/CardItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 60
    return-void
.end method

.method private final getYearAndDurationText(II)Ljava/lang/String;
    .locals 8
    .param p1, "year"    # I
    .param p2, "duration"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 120
    if-nez p1, :cond_0

    move-object v1, v0

    .line 121
    .local v1, "yearText":Ljava/lang/String;
    :goto_0
    if-nez p2, :cond_1

    .line 123
    .local v0, "durationText":Ljava/lang/String;
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    aput-object v1, v3, v7

    aput-object v0, v3, v6

    invoke-static {v2, v6, v3}, Lcom/google/android/videos/utils/Util;->buildListString(Landroid/content/res/Resources;Z[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 120
    .end local v0    # "durationText":Ljava/lang/String;
    .end local v1    # "yearText":Ljava/lang/String;
    :cond_0
    invoke-static {p1}, Lcom/google/android/videos/utils/TimeUtil;->getStandaloneYearString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 121
    .restart local v1    # "yearText":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0138

    new-array v4, v6, [Ljava/lang/Object;

    div-int/lit8 v5, p2, 0x3c

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method bind(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/android/videos/async/Requester;ZLcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView$Binder;)V
    .locals 11
    .param p1, "assetResource"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p3, "isNetworkConnected"    # Z
    .param p4, "binder"    # Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView$Binder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;Z",
            "Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView$Binder;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Landroid/graphics/Bitmap;>;"
    const/16 v5, 0x8

    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 78
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->titleView:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v1, v1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    iget-object v7, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    .line 80
    .local v7, "metadata":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->descriptionView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->descriptionView:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 82
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->descriptionView:Landroid/widget/TextView;

    iget-object v1, v7, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->yearAndDurationView:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 85
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->yearAndDurationView:Landroid/widget/TextView;

    invoke-static {v7}, Lcom/google/android/videos/api/AssetResourceUtil;->getYearIfAvailable(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)I

    move-result v1

    iget v2, v7, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->durationSec:I

    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->getYearAndDurationText(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v0, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    invoke-static {v9, v0}, Lcom/google/android/videos/utils/OfferUtil;->getCheapestOffer(Z[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;)Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    move-result-object v6

    .line 88
    .local v6, "cheapestOffer":Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    if-nez v6, :cond_3

    .line 89
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->priceView:Lcom/google/android/play/layout/PlayCardLabelView;

    invoke-virtual {v0, v5}, Lcom/google/android/play/layout/PlayCardLabelView;->setVisibility(I)V

    .line 97
    :goto_0
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->ratingBar:Lcom/google/android/play/layout/StarRatingBar;

    if-eqz v0, :cond_1

    .line 98
    iget-object v0, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->viewerRating:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    const/4 v1, 0x2

    invoke-static {v0, v9, v1}, Lcom/google/android/videos/api/AssetResourceUtil;->getAggregatedUserRating([Lcom/google/wireless/android/video/magma/proto/ViewerRating;II)Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    move-result-object v8

    .line 101
    .local v8, "viewerRating":Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    if-nez v8, :cond_4

    .line 102
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->ratingBar:Lcom/google/android/play/layout/StarRatingBar;

    invoke-virtual {v0, v5}, Lcom/google/android/play/layout/StarRatingBar;->setVisibility(I)V

    .line 109
    .end local v8    # "viewerRating":Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    :cond_1
    :goto_1
    iput-object p1, p0, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->boundAsset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 110
    const v1, 0x7f0f00c9

    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->thumbnailBitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

    const/4 v4, 0x0

    const-class v5, Landroid/net/Uri;

    move-object v0, p0

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->registerBitmapView(ILcom/google/android/videos/ui/BitmapLoader$BitmapView;Lcom/google/android/videos/async/Requester;Landroid/graphics/Bitmap;Ljava/lang/Class;)V

    .line 112
    if-nez p3, :cond_5

    move v0, v9

    :goto_2
    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->setDimmedStyle(Z)V

    .line 114
    if-eqz p1, :cond_2

    .line 115
    invoke-virtual {p4, p0, p1}, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView$Binder;->childImpression(Lcom/google/android/videos/ui/playnext/ClusterItemView;Lcom/google/wireless/android/video/magma/proto/AssetResource;)V

    .line 117
    :cond_2
    return-void

    .line 91
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->priceView:Lcom/google/android/play/layout/PlayCardLabelView;

    invoke-virtual {v0, v10}, Lcom/google/android/play/layout/PlayCardLabelView;->setVisibility(I)V

    .line 92
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->priceView:Lcom/google/android/play/layout/PlayCardLabelView;

    iget-object v1, v6, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->offer:Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    iget-object v1, v1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedAmount:Ljava/lang/String;

    iget-object v2, v6, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->offer:Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    iget-object v2, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedFullAmount:Ljava/lang/String;

    const/4 v3, 0x4

    const-string v4, ""

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/play/layout/PlayCardLabelView;->setText(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    .line 104
    .restart local v8    # "viewerRating":Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    :cond_4
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->ratingBar:Lcom/google/android/play/layout/StarRatingBar;

    invoke-virtual {v0, v10}, Lcom/google/android/play/layout/StarRatingBar;->setVisibility(I)V

    .line 105
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->ratingBar:Lcom/google/android/play/layout/StarRatingBar;

    iget v1, v8, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->ratingScore:F

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/StarRatingBar;->setRating(F)V

    goto :goto_1

    .end local v8    # "viewerRating":Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    :cond_5
    move v0, v10

    .line 112
    goto :goto_2
.end method

.method protected collectClickableViews([Landroid/view/View;)V
    .locals 2
    .param p1, "clickableViews"    # [Landroid/view/View;

    .prologue
    .line 139
    const/4 v0, 0x0

    aput-object p0, p1, v0

    .line 140
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->overflowView:Landroid/view/View;

    aput-object v1, p1, v0

    .line 141
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 64
    invoke-super {p0}, Lcom/google/android/videos/ui/playnext/CardItemView;->onFinishInflate()V

    .line 65
    const v0, 0x7f0f00a8

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->titleView:Landroid/widget/TextView;

    .line 66
    const v0, 0x7f0f0102

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->descriptionView:Landroid/widget/TextView;

    .line 67
    const v0, 0x7f0f00cb

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->yearAndDurationView:Landroid/widget/TextView;

    .line 68
    const v0, 0x7f0f00d2

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->overflowView:Landroid/view/View;

    .line 69
    const v0, 0x7f0f00d3

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayCardLabelView;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->priceView:Lcom/google/android/play/layout/PlayCardLabelView;

    .line 70
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->thumbnailView:Landroid/widget/ImageView;

    const v1, 0x7f0a00d7

    invoke-static {v0, v1}, Lcom/google/android/videos/ui/BitmapLoader;->createDefaultBitmapView(Landroid/widget/ImageView;I)Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->thumbnailBitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

    .line 72
    const v0, 0x7f0f00db

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/StarRatingBar;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->ratingBar:Lcom/google/android/play/layout/StarRatingBar;

    .line 73
    return-void
.end method

.method protected onGenerateBitmapViewRequest(ILjava/lang/Class;)Ljava/lang/Object;
    .locals 7
    .param p1, "id"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Q:",
            "Ljava/lang/Object;",
            ">(I",
            "Ljava/lang/Class",
            "<TQ;>;)TQ;"
        }
    .end annotation

    .prologue
    .line 128
    .local p2, "requestType":Ljava/lang/Class;, "Ljava/lang/Class<TQ;>;"
    const v0, 0x7f0f00c9

    if-ne p1, v0, :cond_1

    .line 129
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->boundAsset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getHeight()I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/api/AssetResourceUtil;->findBestImageUrl([Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;IIIFZ)Ljava/lang/String;

    move-result-object v6

    .line 132
    .local v6, "url":Ljava/lang/String;
    if-nez v6, :cond_0

    const/4 v0, 0x0

    .line 134
    .end local v6    # "url":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 132
    .restart local v6    # "url":Ljava/lang/String;
    :cond_0
    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 134
    .end local v6    # "url":Ljava/lang/String;
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/google/android/videos/ui/playnext/CardItemView;->onGenerateBitmapViewRequest(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method
