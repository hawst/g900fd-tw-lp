.class Lcom/google/android/videos/player/DirectorInitializer$13;
.super Ljava/lang/Object;
.source "DirectorInitializer.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/player/DirectorInitializer;->initOfflineStoryboardCallback()Lcom/google/android/videos/async/Callback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Ljava/lang/String;",
        "Lcom/google/wireless/android/video/magma/proto/Storyboard;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/player/DirectorInitializer;


# direct methods
.method constructor <init>(Lcom/google/android/videos/player/DirectorInitializer;)V
    .locals 0

    .prologue
    .line 906
    iput-object p1, p0, Lcom/google/android/videos/player/DirectorInitializer$13;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 906
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/player/DirectorInitializer$13;->onError(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public onError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 2
    .param p1, "request"    # Ljava/lang/String;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 916
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Could not load storyboard for video with id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 917
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/videos/player/DirectorInitializer$13;->onResponse(Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/Storyboard;)V

    .line 918
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 906
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/wireless/android/video/magma/proto/Storyboard;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/player/DirectorInitializer$13;->onResponse(Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/Storyboard;)V

    return-void
.end method

.method public onResponse(Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/Storyboard;)V
    .locals 3
    .param p1, "request"    # Ljava/lang/String;
    .param p2, "response"    # Lcom/google/wireless/android/video/magma/proto/Storyboard;

    .prologue
    .line 909
    if-eqz p2, :cond_0

    .line 910
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer$13;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/wireless/android/video/magma/proto/Storyboard;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    # setter for: Lcom/google/android/videos/player/DirectorInitializer;->offlineStoryboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;
    invoke-static {v0, v1}, Lcom/google/android/videos/player/DirectorInitializer;->access$4002(Lcom/google/android/videos/player/DirectorInitializer;[Lcom/google/wireless/android/video/magma/proto/Storyboard;)[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    .line 912
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer$13;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # invokes: Lcom/google/android/videos/player/DirectorInitializer;->onTaskCompleted()V
    invoke-static {v0}, Lcom/google/android/videos/player/DirectorInitializer;->access$3300(Lcom/google/android/videos/player/DirectorInitializer;)V

    .line 913
    return-void
.end method
