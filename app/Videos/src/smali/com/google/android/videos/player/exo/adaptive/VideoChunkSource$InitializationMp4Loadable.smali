.class Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource$InitializationMp4Loadable;
.super Lcom/google/android/exoplayer/chunk/Chunk;
.source "VideoChunkSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InitializationMp4Loadable"
.end annotation


# instance fields
.field private final extractor:Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;

.field final synthetic this$0:Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;Lcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/exoplayer/upstream/DataSpec;ILcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;)V
    .locals 1
    .param p2, "dataSource"    # Lcom/google/android/exoplayer/upstream/DataSource;
    .param p3, "dataSpec"    # Lcom/google/android/exoplayer/upstream/DataSpec;
    .param p4, "trigger"    # I
    .param p5, "extractor"    # Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    .param p6, "representation"    # Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    .prologue
    .line 360
    iput-object p1, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource$InitializationMp4Loadable;->this$0:Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;

    .line 361
    iget-object v0, p6, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->format:Lcom/google/android/exoplayer/chunk/Format;

    invoke-direct {p0, p2, p3, v0, p4}, Lcom/google/android/exoplayer/chunk/Chunk;-><init>(Lcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/exoplayer/upstream/DataSpec;Lcom/google/android/exoplayer/chunk/Format;I)V

    .line 362
    iput-object p5, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource$InitializationMp4Loadable;->extractor:Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;

    .line 363
    return-void
.end method


# virtual methods
.method protected consumeStream(Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;)V
    .locals 3
    .param p1, "stream"    # Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 367
    iget-object v1, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource$InitializationMp4Loadable;->extractor:Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->read(Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;Lcom/google/android/exoplayer/SampleHolder;)I

    move-result v0

    .line 368
    .local v0, "result":I
    const/16 v1, 0x1a

    if-eq v0, v1, :cond_0

    .line 369
    new-instance v1, Lcom/google/android/exoplayer/ParserException;

    const-string v2, "Invalid initialization data"

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 371
    :cond_0
    return-void
.end method
