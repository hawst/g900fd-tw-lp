.class public Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;
.super Ljava/lang/Object;
.source "PlaybackPreparationLogger.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;
    }
.end annotation


# instance fields
.field private displayType:I

.field private hasFailures:Z

.field private isOffline:Z

.field private isTrailer:Z

.field private playerType:I

.field private receivedExtraInfo:Z

.field private released:Z

.field private seasonId:Ljava/lang/String;

.field private sessionNonce:Ljava/lang/String;

.field private showId:Ljava/lang/String;

.field private final startTime:J

.field private final tasks:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;",
            ">;"
        }
    .end annotation
.end field

.field private videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/Config;)V
    .locals 2
    .param p1, "config"    # Lcom/google/android/videos/Config;

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->tasks:Landroid/util/SparseArray;

    .line 50
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->startTime:J

    .line 51
    invoke-interface {p1}, Lcom/google/android/videos/Config;->usePlaybackPreparationLogger()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->released:Z

    .line 52
    return-void

    .line 51
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getMillis()I
    .locals 4

    .prologue
    .line 145
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->startTime:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method private static isRepeatableTask(I)Z
    .locals 1
    .param p0, "task"    # I

    .prologue
    .line 154
    const/16 v0, 0x8

    if-eq p0, v0, :cond_0

    const/16 v0, 0x9

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized addExtraInfo(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 1
    .param p1, "sessionNonce"    # Ljava/lang/String;
    .param p2, "playerType"    # I
    .param p3, "displayType"    # I
    .param p4, "videoId"    # Ljava/lang/String;
    .param p5, "showId"    # Ljava/lang/String;
    .param p6, "seasonId"    # Ljava/lang/String;
    .param p7, "isTrailer"    # Z
    .param p8, "isOffline"    # Z

    .prologue
    .line 106
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->receivedExtraInfo:Z

    .line 107
    iput-object p1, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->sessionNonce:Ljava/lang/String;

    .line 108
    iput p2, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->playerType:I

    .line 109
    iput p3, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->displayType:I

    .line 110
    iput-object p4, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->videoId:Ljava/lang/String;

    .line 111
    iput-object p5, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->showId:Ljava/lang/String;

    .line 112
    iput-object p6, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->seasonId:Ljava/lang/String;

    .line 113
    iput-boolean p7, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->isTrailer:Z

    .line 114
    iput-boolean p8, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->isOffline:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 115
    monitor-exit p0

    return-void

    .line 106
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized flushAndRelease(Lcom/google/android/videos/logging/EventLogger;)V
    .locals 14
    .param p1, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;

    .prologue
    .line 122
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->released:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->receivedExtraInfo:Z

    if-nez v0, :cond_1

    .line 123
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->released:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139
    :goto_0
    monitor-exit p0

    return-void

    .line 128
    :cond_1
    const/4 v11, 0x0

    .local v11, "i":I
    :try_start_1
    iget-object v0, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->tasks:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v12

    .local v12, "size":I
    :goto_1
    if-ge v11, v12, :cond_4

    .line 129
    iget-object v0, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->tasks:Landroid/util/SparseArray;

    invoke-virtual {v0, v11}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;

    .line 130
    .local v13, "timing":Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;
    iget v0, v13, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;->end:I

    iget v1, v13, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;->start:I

    if-lt v0, v1, :cond_2

    iget v0, v13, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;->end:I

    if-gtz v0, :cond_3

    .line 131
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->hasFailures:Z

    .line 132
    const/4 v0, 0x1

    iput-boolean v0, v13, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;->hasFailures:Z

    .line 128
    :cond_3
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 136
    .end local v13    # "timing":Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->hasFailures:Z

    iget-object v2, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->sessionNonce:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->playerType:I

    iget v4, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->displayType:I

    iget-object v5, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->videoId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->showId:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->seasonId:Ljava/lang/String;

    iget-boolean v8, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->isTrailer:Z

    iget-boolean v9, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->isOffline:Z

    iget-object v10, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->tasks:Landroid/util/SparseArray;

    move-object v0, p1

    invoke-interface/range {v0 .. v10}, Lcom/google/android/videos/logging/EventLogger;->onPlaybackPreparationStats(ZLjava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLandroid/util/SparseArray;)V

    .line 138
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->released:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 122
    .end local v11    # "i":I
    .end local v12    # "size":I
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized recordTaskEnd(I)V
    .locals 1
    .param p1, "task"    # I

    .prologue
    .line 72
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->recordTaskEnd(IZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    monitor-exit p0

    return-void

    .line 72
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized recordTaskEnd(IZ)V
    .locals 3
    .param p1, "task"    # I
    .param p2, "successful"    # Z

    .prologue
    .line 81
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->released:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    .line 102
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 85
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->tasks:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;

    .line 86
    .local v0, "timing":Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;
    if-nez v0, :cond_2

    .line 87
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Don\'t have start time for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-static {v1, v2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 88
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->released:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 81
    .end local v0    # "timing":Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 91
    .restart local v0    # "timing":Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;
    :cond_2
    :try_start_2
    iget v1, v0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;->end:I

    if-lez v1, :cond_3

    .line 92
    invoke-static {p1}, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->isRepeatableTask(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 93
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Already recorded task info for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-static {v1, v2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 94
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->released:Z

    goto :goto_0

    .line 98
    :cond_3
    if-nez p2, :cond_4

    .line 99
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;->hasFailures:Z

    .line 101
    :cond_4
    invoke-direct {p0}, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->getMillis()I

    move-result v1

    iput v1, v0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;->end:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized recordTaskStart(I)V
    .locals 3
    .param p1, "task"    # I

    .prologue
    .line 55
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->released:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    .line 69
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 59
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->tasks:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 60
    invoke-static {p1}, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->isRepeatableTask(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 61
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Already recorded task info for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-static {v1, v2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 62
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->released:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 55
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 66
    :cond_2
    :try_start_2
    new-instance v0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;

    invoke-direct {v0}, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;-><init>()V

    .line 67
    .local v0, "timing":Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;
    invoke-direct {p0}, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->getMillis()I

    move-result v1

    iput v1, v0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;->start:I

    .line 68
    iget-object v1, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->tasks:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized setHasFailures()V
    .locals 1

    .prologue
    .line 118
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->hasFailures:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119
    monitor-exit p0

    return-void

    .line 118
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
