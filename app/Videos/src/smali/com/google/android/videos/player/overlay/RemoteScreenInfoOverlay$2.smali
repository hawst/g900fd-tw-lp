.class Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay$2;
.super Ljava/lang/Object;
.source "RemoteScreenInfoOverlay.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;


# direct methods
.method constructor <init>(Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay$2;->this$0:Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/16 v2, 0x8

    .line 102
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay$2;->this$0:Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;

    # getter for: Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->watchInfoOverlay:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->access$000(Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 103
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay$2;->this$0:Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->watchInfoState:I
    invoke-static {v0, v1}, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->access$102(Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;I)I

    .line 104
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay$2;->this$0:Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;

    invoke-virtual {v0, v2}, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->setVisibility(I)V

    .line 105
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 98
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay$2;->this$0:Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;

    const/4 v1, 0x3

    # setter for: Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->watchInfoState:I
    invoke-static {v0, v1}, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->access$102(Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;I)I

    .line 94
    return-void
.end method
