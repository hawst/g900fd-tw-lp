.class public abstract Lcom/google/android/videos/remote/RemoteControl;
.super Ljava/lang/Object;
.source "RemoteControl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/remote/RemoteControl$UpdateHandler;
    }
.end annotation


# instance fields
.field private cachedPlayerState:Lcom/google/android/videos/remote/PlayerState;

.field private cachedPlayerStateMillis:J

.field private final context:Landroid/content/Context;

.field private error:I

.field private frozenPlayerTime:I

.field protected final handler:Landroid/os/Handler;

.field protected final listeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/videos/remote/RemoteControlListener;",
            ">;"
        }
    .end annotation
.end field

.field private playerState:Lcom/google/android/videos/remote/PlayerState;

.field private playingUpdatesEnabled:Z

.field private retryAction:Lcom/google/android/videos/utils/RetryAction;

.field private subtitleTrackList:Lcom/google/android/videos/remote/SubtitleTrackList;

.field private videoInfo:Lcom/google/android/videos/remote/RemoteVideoInfo;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/remote/RemoteControl;->error:I

    .line 63
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/remote/RemoteControl;->frozenPlayerTime:I

    .line 70
    iput-object p1, p0, Lcom/google/android/videos/remote/RemoteControl;->context:Landroid/content/Context;

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/remote/RemoteControl;->listeners:Ljava/util/ArrayList;

    .line 72
    new-instance v0, Lcom/google/android/videos/remote/RemoteControl$UpdateHandler;

    invoke-direct {v0, p0}, Lcom/google/android/videos/remote/RemoteControl$UpdateHandler;-><init>(Lcom/google/android/videos/remote/RemoteControl;)V

    iput-object v0, p0, Lcom/google/android/videos/remote/RemoteControl;->handler:Landroid/os/Handler;

    .line 73
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/videos/remote/RemoteControl;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/remote/RemoteControl;
    .param p1, "x1"    # I

    .prologue
    .line 27
    iput p1, p0, Lcom/google/android/videos/remote/RemoteControl;->error:I

    return p1
.end method

.method static synthetic access$102(Lcom/google/android/videos/remote/RemoteControl;Lcom/google/android/videos/utils/RetryAction;)Lcom/google/android/videos/utils/RetryAction;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/remote/RemoteControl;
    .param p1, "x1"    # Lcom/google/android/videos/utils/RetryAction;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/google/android/videos/remote/RemoteControl;->retryAction:Lcom/google/android/videos/utils/RetryAction;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/videos/remote/RemoteControl;)[Lcom/google/android/videos/remote/RemoteControlListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/remote/RemoteControl;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemoteControl;->getListeners()[Lcom/google/android/videos/remote/RemoteControlListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/remote/RemoteControl;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/remote/RemoteControl;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemoteControl;->onMessagePlayingUpdate()V

    return-void
.end method

.method private enablePlayingUpdates(Z)V
    .locals 4
    .param p1, "enabled"    # Z

    .prologue
    .line 432
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteControl;->handler:Landroid/os/Handler;

    monitor-enter v1

    .line 433
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/videos/remote/RemoteControl;->playingUpdatesEnabled:Z

    if-ne p1, v0, :cond_0

    .line 434
    monitor-exit v1

    .line 445
    :goto_0
    return-void

    .line 437
    :cond_0
    if-eqz p1, :cond_1

    .line 438
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteControl;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/videos/remote/RemoteControl;->handler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 443
    :goto_1
    iput-boolean p1, p0, Lcom/google/android/videos/remote/RemoteControl;->playingUpdatesEnabled:Z

    .line 444
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 440
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteControl;->handler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method private getListeners()[Lcom/google/android/videos/remote/RemoteControlListener;
    .locals 2

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteControl;->listeners:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteControl;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lcom/google/android/videos/remote/RemoteControlListener;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/videos/remote/RemoteControlListener;

    return-object v0
.end method

.method private onMessagePlayingUpdate()V
    .locals 6

    .prologue
    .line 410
    invoke-virtual {p0}, Lcom/google/android/videos/remote/RemoteControl;->updateExtrapolatePlayerTime()V

    .line 411
    invoke-virtual {p0}, Lcom/google/android/videos/remote/RemoteControl;->onPlayingUpdate()V

    .line 413
    iget-object v2, p0, Lcom/google/android/videos/remote/RemoteControl;->handler:Landroid/os/Handler;

    monitor-enter v2

    .line 414
    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/videos/remote/RemoteControl;->playingUpdatesEnabled:Z

    if-eqz v1, :cond_1

    .line 415
    const/16 v0, 0x3e8

    .line 416
    .local v0, "delayMillis":I
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteControl;->playerState:Lcom/google/android/videos/remote/PlayerState;

    if-eqz v1, :cond_0

    .line 420
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteControl;->playerState:Lcom/google/android/videos/remote/PlayerState;

    iget v1, v1, Lcom/google/android/videos/remote/PlayerState;->time:I

    rem-int/lit16 v1, v1, 0x3e8

    rsub-int v0, v1, 0x5dc

    .line 422
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteControl;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/videos/remote/RemoteControl;->handler:Landroid/os/Handler;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    int-to-long v4, v0

    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 424
    .end local v0    # "delayMillis":I
    :cond_1
    monitor-exit v2

    .line 425
    return-void

    .line 424
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private declared-synchronized updatePlayerState(Lcom/google/android/videos/remote/PlayerState;)V
    .locals 3
    .param p1, "playerState"    # Lcom/google/android/videos/remote/PlayerState;

    .prologue
    .line 335
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/videos/remote/RemoteControl;->playerState:Lcom/google/android/videos/remote/PlayerState;

    .line 337
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteControl;->playerState:Lcom/google/android/videos/remote/PlayerState;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/videos/remote/RemoteControl;->isPlayerTimeFrozen()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 338
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteControl;->playerState:Lcom/google/android/videos/remote/PlayerState;

    iget v2, p0, Lcom/google/android/videos/remote/RemoteControl;->frozenPlayerTime:I

    iput v2, v1, Lcom/google/android/videos/remote/PlayerState;->time:I

    .line 341
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteControl;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 342
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteControl;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/remote/RemoteControlListener;

    invoke-interface {v1, p1}, Lcom/google/android/videos/remote/RemoteControlListener;->onPlayerStateChanged(Lcom/google/android/videos/remote/PlayerState;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 341
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 344
    :cond_1
    monitor-exit p0

    return-void

    .line 335
    .end local v0    # "i":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method public addListener(Lcom/google/android/videos/remote/RemoteControlListener;)V
    .locals 1
    .param p1, "remoteListener"    # Lcom/google/android/videos/remote/RemoteControlListener;

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteControl;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 238
    return-void
.end method

.method protected calculateExtrapolatedPlayerTime()I
    .locals 4

    .prologue
    .line 398
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteControl;->cachedPlayerState:Lcom/google/android/videos/remote/PlayerState;

    iget v1, v1, Lcom/google/android/videos/remote/PlayerState;->time:I

    invoke-virtual {p0}, Lcom/google/android/videos/remote/RemoteControl;->getCachedPlayerStateAgeMillis()I

    move-result v2

    const/16 v3, 0x7530

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    add-int v0, v1, v2

    .line 400
    .local v0, "time":I
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteControl;->videoInfo:Lcom/google/android/videos/remote/RemoteVideoInfo;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteControl;->videoInfo:Lcom/google/android/videos/remote/RemoteVideoInfo;

    iget v1, v1, Lcom/google/android/videos/remote/RemoteVideoInfo;->durationMillis:I

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteControl;->videoInfo:Lcom/google/android/videos/remote/RemoteVideoInfo;

    iget v1, v1, Lcom/google/android/videos/remote/RemoteVideoInfo;->durationMillis:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .end local v0    # "time":I
    :cond_0
    return v0
.end method

.method public clearError()V
    .locals 2

    .prologue
    .line 153
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/videos/remote/RemoteControl;->error:I

    .line 154
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/videos/remote/RemoteControl;->retryAction:Lcom/google/android/videos/utils/RetryAction;

    .line 156
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteControl;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 157
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteControl;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/remote/RemoteControlListener;

    invoke-interface {v1}, Lcom/google/android/videos/remote/RemoteControlListener;->onRemoteControlErrorCleared()V

    .line 156
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 159
    :cond_0
    return-void
.end method

.method public abstract fling(Ljava/lang/String;Lcom/google/android/videos/remote/RemoteVideoInfo;IZZZ)I
.end method

.method protected freezePlayerTime(I)V
    .locals 1
    .param p1, "timeMillis"    # I

    .prologue
    .line 359
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    .line 360
    iput p1, p0, Lcom/google/android/videos/remote/RemoteControl;->frozenPlayerTime:I

    .line 361
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteControl;->playerState:Lcom/google/android/videos/remote/PlayerState;

    invoke-direct {p0, v0}, Lcom/google/android/videos/remote/RemoteControl;->updatePlayerState(Lcom/google/android/videos/remote/PlayerState;)V

    .line 362
    return-void

    .line 359
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized getCachedPlayerStateAgeMillis()I
    .locals 4

    .prologue
    .line 145
    monitor-enter p0

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/videos/remote/RemoteControl;->cachedPlayerStateMillis:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sub-long/2addr v0, v2

    long-to-int v0, v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteControl;->context:Landroid/content/Context;

    return-object v0
.end method

.method public getError()I
    .locals 1

    .prologue
    .line 184
    iget v0, p0, Lcom/google/android/videos/remote/RemoteControl;->error:I

    return v0
.end method

.method public getErrorMessage()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 192
    iget v3, p0, Lcom/google/android/videos/remote/RemoteControl;->error:I

    if-nez v3, :cond_1

    .line 193
    const/4 v0, 0x0

    .line 229
    :cond_0
    :goto_0
    return-object v0

    .line 197
    :cond_1
    iget v3, p0, Lcom/google/android/videos/remote/RemoteControl;->error:I

    packed-switch v3, :pswitch_data_0

    .line 215
    const v2, 0x7f0b015e

    .line 218
    .local v2, "stringId":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/videos/remote/RemoteControl;->context:Landroid/content/Context;

    new-array v4, v8, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/videos/remote/RemoteControl;->getScreenName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v3, v2, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 220
    .local v0, "msg":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/videos/remote/RemoteControl;->isAttemptingReconnect()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 221
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 222
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    iget-object v3, p0, Lcom/google/android/videos/remote/RemoteControl;->context:Landroid/content/Context;

    const v4, 0x7f0b015d

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/videos/remote/RemoteControl;->getReconnectionAttempt()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {p0}, Lcom/google/android/videos/remote/RemoteControl;->getMaximumReconnectionAttempts()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 199
    .end local v0    # "msg":Ljava/lang/String;
    .end local v1    # "sb":Ljava/lang/StringBuilder;
    .end local v2    # "stringId":I
    :pswitch_0
    const v2, 0x7f0b0159

    .line 200
    .restart local v2    # "stringId":I
    goto :goto_1

    .line 202
    .end local v2    # "stringId":I
    :pswitch_1
    const v2, 0x7f0b015a

    .line 203
    .restart local v2    # "stringId":I
    goto :goto_1

    .line 205
    .end local v2    # "stringId":I
    :pswitch_2
    const v2, 0x7f0b015c

    .line 206
    .restart local v2    # "stringId":I
    goto :goto_1

    .line 208
    .end local v2    # "stringId":I
    :pswitch_3
    const v2, 0x7f0b015b

    .line 209
    .restart local v2    # "stringId":I
    goto :goto_1

    .line 212
    .end local v2    # "stringId":I
    :pswitch_4
    const v2, 0x7f0b015e

    .line 213
    .restart local v2    # "stringId":I
    goto :goto_1

    .line 197
    nop

    :pswitch_data_0
    .packed-switch -0x3ec
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getMaximumReconnectionAttempts()I
    .locals 1

    .prologue
    .line 252
    const/4 v0, 0x0

    return v0
.end method

.method public declared-synchronized getPlayerState()Lcom/google/android/videos/remote/PlayerState;
    .locals 1

    .prologue
    .line 122
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteControl;->playerState:Lcom/google/android/videos/remote/PlayerState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getReconnectionAttempt()I
    .locals 1

    .prologue
    .line 245
    const/4 v0, -0x1

    return v0
.end method

.method public getRetryAction()Lcom/google/android/videos/utils/RetryAction;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteControl;->retryAction:Lcom/google/android/videos/utils/RetryAction;

    return-object v0
.end method

.method public getRouteInfo()Landroid/support/v7/media/MediaRouter$RouteInfo;
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract getScreenName()Ljava/lang/String;
.end method

.method public declared-synchronized getSubtitleTrackList()Lcom/google/android/videos/remote/SubtitleTrackList;
    .locals 1

    .prologue
    .line 137
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteControl;->subtitleTrackList:Lcom/google/android/videos/remote/SubtitleTrackList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getVideoInfo()Lcom/google/android/videos/remote/RemoteVideoInfo;
    .locals 1

    .prologue
    .line 149
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteControl;->videoInfo:Lcom/google/android/videos/remote/RemoteVideoInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected handleMessage(Landroid/os/Message;)V
    .locals 0
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 467
    return-void
.end method

.method public isAttemptingReconnect()Z
    .locals 2

    .prologue
    .line 259
    invoke-virtual {p0}, Lcom/google/android/videos/remote/RemoteControl;->getReconnectionAttempt()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isPlayerTimeFrozen()Z
    .locals 2

    .prologue
    .line 376
    iget v0, p0, Lcom/google/android/videos/remote/RemoteControl;->frozenPlayerTime:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final declared-synchronized notifyAudioTracksChanged(Ljava/util/List;I)V
    .locals 2
    .param p2, "selectedTrackIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AudioInfo;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 324
    .local p1, "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/google/wireless/android/video/magma/proto/AudioInfo;>;"
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteControl;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 325
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteControl;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/remote/RemoteControlListener;

    invoke-interface {v1, p1, p2}, Lcom/google/android/videos/remote/RemoteControlListener;->onAudioTracksChanged(Ljava/util/List;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 324
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 327
    :cond_0
    monitor-exit p0

    return-void

    .line 324
    .end local v0    # "i":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method protected onDisconnected()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 454
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteControl;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 455
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteControl;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/remote/RemoteControlListener;

    invoke-interface {v1}, Lcom/google/android/videos/remote/RemoteControlListener;->onDisconnected()V

    .line 454
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 458
    :cond_0
    invoke-virtual {p0, v2}, Lcom/google/android/videos/remote/RemoteControl;->updateCachedPlayerState(Lcom/google/android/videos/remote/PlayerState;)V

    .line 459
    invoke-virtual {p0, v2}, Lcom/google/android/videos/remote/RemoteControl;->updateCachedSubtitleTracks(Lcom/google/android/videos/remote/SubtitleTrackList;)V

    .line 460
    invoke-virtual {p0, v2}, Lcom/google/android/videos/remote/RemoteControl;->updateCachedVideoInfo(Lcom/google/android/videos/remote/RemoteVideoInfo;)V

    .line 461
    return-void
.end method

.method public onError(I)V
    .locals 1
    .param p1, "error"    # I

    .prologue
    .line 162
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/videos/remote/RemoteControl;->onError(ILcom/google/android/videos/utils/RetryAction;)V

    .line 163
    return-void
.end method

.method public onError(ILcom/google/android/videos/utils/RetryAction;)V
    .locals 2
    .param p1, "error"    # I
    .param p2, "retryAction"    # Lcom/google/android/videos/utils/RetryAction;

    .prologue
    .line 166
    const/16 v0, -0x3e8

    if-gt p1, v0, :cond_0

    const/16 v0, -0x3ec

    if-lt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    .line 168
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteControl;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/videos/remote/RemoteControl$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/videos/remote/RemoteControl$1;-><init>(Lcom/google/android/videos/remote/RemoteControl;ILcom/google/android/videos/utils/RetryAction;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 181
    return-void

    .line 166
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onPlayingUpdate()V
    .locals 0

    .prologue
    .line 350
    return-void
.end method

.method public abstract pause(Ljava/lang/String;)Z
.end method

.method public abstract play(Ljava/lang/String;)Z
.end method

.method public abstract prepareForFling()V
.end method

.method public removeListener(Lcom/google/android/videos/remote/RemoteControlListener;)V
    .locals 1
    .param p1, "remoteListener"    # Lcom/google/android/videos/remote/RemoteControlListener;

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteControl;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 234
    return-void
.end method

.method public abstract requestAudioTracks()Z
.end method

.method public abstract seekTo(Ljava/lang/String;I)Z
.end method

.method public abstract setAudioTrack(Lcom/google/wireless/android/video/magma/proto/AudioInfo;)Z
.end method

.method public abstract setSubtitles(Lcom/google/android/videos/subtitles/SubtitleTrack;)Z
.end method

.method public abstract stop()V
.end method

.method protected unfreezePlayerTime()V
    .locals 1

    .prologue
    .line 368
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/remote/RemoteControl;->frozenPlayerTime:I

    .line 369
    return-void
.end method

.method protected declared-synchronized updateCachedPlayerState(Lcom/google/android/videos/remote/PlayerState;)V
    .locals 4
    .param p1, "cachedPlayerState"    # Lcom/google/android/videos/remote/PlayerState;

    .prologue
    const/4 v0, 0x0

    .line 274
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/videos/remote/RemoteControl;->cachedPlayerState:Lcom/google/android/videos/remote/PlayerState;

    .line 275
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/videos/remote/RemoteControl;->cachedPlayerStateMillis:J

    .line 277
    if-eqz p1, :cond_1

    .line 278
    new-instance v1, Lcom/google/android/videos/remote/PlayerState;

    invoke-direct {v1, p1}, Lcom/google/android/videos/remote/PlayerState;-><init>(Lcom/google/android/videos/remote/PlayerState;)V

    invoke-direct {p0, v1}, Lcom/google/android/videos/remote/RemoteControl;->updatePlayerState(Lcom/google/android/videos/remote/PlayerState;)V

    .line 279
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteControl;->playerState:Lcom/google/android/videos/remote/PlayerState;

    iget v1, v1, Lcom/google/android/videos/remote/PlayerState;->state:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/videos/remote/RemoteControl;->enablePlayingUpdates(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 284
    :goto_0
    monitor-exit p0

    return-void

    .line 281
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-direct {p0, v0}, Lcom/google/android/videos/remote/RemoteControl;->updatePlayerState(Lcom/google/android/videos/remote/PlayerState;)V

    .line 282
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/videos/remote/RemoteControl;->enablePlayingUpdates(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 274
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final declared-synchronized updateCachedSubtitleTracks(Lcom/google/android/videos/remote/SubtitleTrackList;)V
    .locals 3
    .param p1, "trackList"    # Lcom/google/android/videos/remote/SubtitleTrackList;

    .prologue
    .line 310
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteControl;->subtitleTrackList:Lcom/google/android/videos/remote/SubtitleTrackList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v1, p1, :cond_1

    .line 317
    :cond_0
    monitor-exit p0

    return-void

    .line 313
    :cond_1
    :try_start_1
    iput-object p1, p0, Lcom/google/android/videos/remote/RemoteControl;->subtitleTrackList:Lcom/google/android/videos/remote/SubtitleTrackList;

    .line 314
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteControl;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 315
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteControl;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/remote/RemoteControlListener;

    iget-object v2, p0, Lcom/google/android/videos/remote/RemoteControl;->subtitleTrackList:Lcom/google/android/videos/remote/SubtitleTrackList;

    invoke-interface {v1, v2}, Lcom/google/android/videos/remote/RemoteControlListener;->onSubtitleTracksChanged(Lcom/google/android/videos/remote/SubtitleTrackList;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 314
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 310
    .end local v0    # "i":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method protected declared-synchronized updateCachedVideoInfo(Lcom/google/android/videos/remote/RemoteVideoInfo;)V
    .locals 2
    .param p1, "videoInfo"    # Lcom/google/android/videos/remote/RemoteVideoInfo;

    .prologue
    .line 291
    monitor-enter p0

    if-eqz p1, :cond_1

    .line 292
    :try_start_0
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteControl;->videoInfo:Lcom/google/android/videos/remote/RemoteVideoInfo;

    invoke-virtual {p1, v1}, Lcom/google/android/videos/remote/RemoteVideoInfo;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_2

    .line 303
    :cond_0
    monitor-exit p0

    return-void

    .line 295
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteControl;->videoInfo:Lcom/google/android/videos/remote/RemoteVideoInfo;

    if-eqz v1, :cond_0

    .line 299
    :cond_2
    iput-object p1, p0, Lcom/google/android/videos/remote/RemoteControl;->videoInfo:Lcom/google/android/videos/remote/RemoteVideoInfo;

    .line 300
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteControl;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 301
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteControl;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/remote/RemoteControlListener;

    invoke-interface {v1, p1}, Lcom/google/android/videos/remote/RemoteControlListener;->onVideoInfoChanged(Lcom/google/android/videos/remote/RemoteVideoInfo;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 300
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 291
    .end local v0    # "i":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method protected declared-synchronized updateExtrapolatePlayerTime()V
    .locals 2

    .prologue
    .line 384
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/videos/remote/RemoteControl;->isPlayerTimeFrozen()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 392
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 387
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/videos/remote/RemoteControl;->calculateExtrapolatedPlayerTime()I

    move-result v0

    .line 388
    .local v0, "newTime":I
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteControl;->playerState:Lcom/google/android/videos/remote/PlayerState;

    iget v1, v1, Lcom/google/android/videos/remote/PlayerState;->time:I

    if-eq v0, v1, :cond_0

    .line 389
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteControl;->playerState:Lcom/google/android/videos/remote/PlayerState;

    iput v0, v1, Lcom/google/android/videos/remote/PlayerState;->time:I

    .line 390
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteControl;->playerState:Lcom/google/android/videos/remote/PlayerState;

    invoke-direct {p0, v1}, Lcom/google/android/videos/remote/RemoteControl;->updatePlayerState(Lcom/google/android/videos/remote/PlayerState;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 384
    .end local v0    # "newTime":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
