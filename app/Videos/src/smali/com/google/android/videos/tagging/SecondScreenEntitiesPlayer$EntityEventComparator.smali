.class Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEventComparator;
.super Ljava/lang/Object;
.source "SecondScreenEntitiesPlayer.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EntityEventComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEventComparator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 282
    new-instance v0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEventComparator;

    invoke-direct {v0}, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEventComparator;-><init>()V

    sput-object v0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEventComparator;->INSTANCE:Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEventComparator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 280
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;)I
    .locals 2
    .param p1, "eventA"    # Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;
    .param p2, "eventB"    # Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;

    .prologue
    .line 286
    iget v0, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;->timestamp:I

    iget v1, p2, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;->timestamp:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 280
    check-cast p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEventComparator;->compare(Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer$EntityEvent;)I

    move-result v0

    return v0
.end method
