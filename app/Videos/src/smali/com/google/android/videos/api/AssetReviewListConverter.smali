.class final Lcom/google/android/videos/api/AssetReviewListConverter;
.super Lcom/google/android/videos/converter/HttpResponseConverter;
.source "AssetReviewListConverter.java"

# interfaces
.implements Lcom/google/android/videos/converter/RequestConverter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/converter/HttpResponseConverter",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;",
        ">;",
        "Lcom/google/android/videos/converter/RequestConverter",
        "<",
        "Lcom/google/android/videos/api/AssetReviewListRequest;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        ">;"
    }
.end annotation


# instance fields
.field private final baseUri:Ljava/lang/String;

.field private final byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArrayPool;)V
    .locals 2
    .param p1, "baseUri"    # Landroid/net/Uri;
    .param p2, "byteArrayPool"    # Lcom/google/android/videos/utils/ByteArrayPool;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/videos/converter/HttpResponseConverter;-><init>()V

    .line 35
    new-instance v0, Lcom/google/android/videos/utils/UriBuilder;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "review"

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;->addSegment(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v0

    const-string v1, "list"

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;->addSegment(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/utils/UriBuilder;->build()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/api/AssetReviewListConverter;->baseUri:Ljava/lang/String;

    .line 39
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/utils/ByteArrayPool;

    iput-object v0, p0, Lcom/google/android/videos/api/AssetReviewListConverter;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;

    .line 40
    return-void
.end method


# virtual methods
.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 24
    check-cast p1, Lcom/google/android/videos/api/AssetReviewListRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/api/AssetReviewListConverter;->convertRequest(Lcom/google/android/videos/api/AssetReviewListRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public convertRequest(Lcom/google/android/videos/api/AssetReviewListRequest;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 4
    .param p1, "request"    # Lcom/google/android/videos/api/AssetReviewListRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 44
    iget-object v1, p0, Lcom/google/android/videos/api/AssetReviewListConverter;->baseUri:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/videos/api/ApiUriBuilder;->create(Ljava/lang/String;)Lcom/google/android/videos/api/ApiUriBuilder;

    move-result-object v2

    const-string v3, "at"

    iget-boolean v1, p1, Lcom/google/android/videos/api/AssetReviewListRequest;->criticReviews:Z

    if-eqz v1, :cond_0

    const-string v1, "editor"

    :goto_0
    invoke-virtual {v2, v3, v1}, Lcom/google/android/videos/api/ApiUriBuilder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/api/ApiUriBuilder;

    move-result-object v1

    const-string v2, "pageToken"

    iget v3, p1, Lcom/google/android/videos/api/AssetReviewListRequest;->page:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/api/ApiUriBuilder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/api/ApiUriBuilder;

    move-result-object v1

    const-string v2, "maxResults"

    iget v3, p1, Lcom/google/android/videos/api/AssetReviewListRequest;->maxResults:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/api/ApiUriBuilder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/api/ApiUriBuilder;

    move-result-object v1

    const-string v2, "aid"

    iget-object v3, p1, Lcom/google/android/videos/api/AssetReviewListRequest;->id:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/api/ApiUriBuilder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/api/ApiUriBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/videos/api/AssetReviewListRequest;->userCountry:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/videos/api/ApiUriBuilder;->restrictCountry(Ljava/lang/String;)Lcom/google/android/videos/api/ApiUriBuilder;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/videos/api/ApiUriBuilder;->restrictLocale(Ljava/util/Locale;)Lcom/google/android/videos/api/ApiUriBuilder;

    move-result-object v0

    .line 52
    .local v0, "uriBuilder":Lcom/google/android/videos/api/ApiUriBuilder;
    invoke-virtual {v0}, Lcom/google/android/videos/api/ApiUriBuilder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/async/HttpUriRequestFactory;->createGet(Landroid/net/Uri;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v1

    return-object v1

    .line 44
    .end local v0    # "uriBuilder":Lcom/google/android/videos/api/ApiUriBuilder;
    :cond_0
    const-string v1, "user"

    goto :goto_0
.end method

.method public convertResponseEntity(Lorg/apache/http/HttpEntity;)Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;
    .locals 2
    .param p1, "entity"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 58
    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;-><init>()V

    iget-object v1, p0, Lcom/google/android/videos/api/AssetReviewListConverter;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;

    invoke-static {v0, p1, v1}, Lcom/google/android/videos/utils/EntityUtils;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;Lorg/apache/http/HttpEntity;Lcom/google/android/videos/utils/ByteArrayPool;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;

    return-object v0
.end method

.method public bridge synthetic convertResponseEntity(Lorg/apache/http/HttpEntity;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/google/android/videos/api/AssetReviewListConverter;->convertResponseEntity(Lorg/apache/http/HttpEntity;)Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;

    move-result-object v0

    return-object v0
.end method
