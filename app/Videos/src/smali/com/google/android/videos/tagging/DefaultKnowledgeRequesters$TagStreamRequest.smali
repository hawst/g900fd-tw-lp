.class public final Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;
.super Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;
.source "DefaultKnowledgeRequesters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TagStreamRequest"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest$RequestHandler;
    }
.end annotation


# instance fields
.field private final uri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/tagging/KnowledgeRequest;ILandroid/net/Uri;)V
    .locals 0
    .param p1, "knowledgeRequest"    # Lcom/google/android/videos/tagging/KnowledgeRequest;
    .param p2, "storage"    # I
    .param p3, "uri"    # Landroid/net/Uri;

    .prologue
    .line 640
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;-><init>(Lcom/google/android/videos/tagging/KnowledgeRequest;I)V

    .line 641
    iput-object p3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;->uri:Landroid/net/Uri;

    .line 642
    return-void
.end method

.method static synthetic access$500(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;

    .prologue
    .line 619
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;->uri:Landroid/net/Uri;

    return-object v0
.end method

.method public static from(Lcom/google/android/videos/tagging/KnowledgeRequest;ILcom/google/android/videos/Config;)Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;
    .locals 5
    .param p0, "knowledgeRequest"    # Lcom/google/android/videos/tagging/KnowledgeRequest;
    .param p1, "storage"    # I
    .param p2, "config"    # Lcom/google/android/videos/Config;

    .prologue
    .line 623
    invoke-interface {p2}, Lcom/google/android/videos/Config;->baseKnowledgeUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/tagging/KnowledgeRequest;->videoId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "cr"

    iget-object v4, p0, Lcom/google/android/videos/tagging/KnowledgeRequest;->userCountry:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 626
    .local v1, "uriBuilder":Landroid/net/Uri$Builder;
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeRequest;->locale:Ljava/util/Locale;

    .line 627
    .local v0, "locale":Ljava/util/Locale;
    if-eqz v0, :cond_0

    .line 628
    const-string v2, "lr"

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 630
    :cond_0
    const-string v2, "fmt"

    iget v3, p0, Lcom/google/android/videos/tagging/KnowledgeRequest;->videoItag:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 631
    iget-object v2, p0, Lcom/google/android/videos/tagging/KnowledgeRequest;->videoTimestamp:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 632
    const-string v2, "ts"

    iget-object v3, p0, Lcom/google/android/videos/tagging/KnowledgeRequest;->videoTimestamp:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 634
    :cond_1
    new-instance v2, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v2, p0, p1, v3}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;-><init>(Lcom/google/android/videos/tagging/KnowledgeRequest;ILandroid/net/Uri;)V

    return-object v2
.end method


# virtual methods
.method public toFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 646
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;->baseFileName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
