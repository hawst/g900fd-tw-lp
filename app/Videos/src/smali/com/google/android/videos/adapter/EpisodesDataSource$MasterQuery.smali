.class public interface abstract Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery;
.super Ljava/lang/Object;
.source "EpisodesDataSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/adapter/EpisodesDataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "MasterQuery"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow;,
        Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForOnDevice;,
        Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForSingleShow;
    }
.end annotation


# static fields
.field public static final BASE_PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 343
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "user_assets_account"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "user_assets_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "shows_title"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "last_watched_episode_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "next_show_episode_id"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "next_show_episode_in_same_season"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "owned_complete_show"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "shows_banner_uri"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "shows_poster_uri"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "last_activity_timestamp"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery;->BASE_PROJECTION:[Ljava/lang/String;

    return-void
.end method
