.class public interface abstract Lcom/google/android/videos/player/PlayerSurface;
.super Ljava/lang/Object;
.source "PlayerSurface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/PlayerSurface$OnDisplayParametersChangedListener;,
        Lcom/google/android/videos/player/PlayerSurface$Listener;
    }
.end annotation


# virtual methods
.method public abstract closeShutter()V
.end method

.method public abstract getSurfaceHolder()Landroid/view/SurfaceHolder;
.end method

.method public abstract getVerticalLetterboxFraction()F
.end method

.method public abstract getVideoDisplayHeight()I
.end method

.method public abstract getVideoDisplayWidth()I
.end method

.method public abstract isSurfaceCreated()Z
.end method

.method public abstract openShutter()V
.end method

.method public abstract recreateSurface()V
.end method

.method public abstract release()V
.end method

.method public abstract setListener(Lcom/google/android/videos/player/PlayerSurface$Listener;)V
.end method

.method public abstract setOnDisplayParametersChangedListener(Lcom/google/android/videos/player/PlayerSurface$OnDisplayParametersChangedListener;)V
.end method

.method public abstract setVideoSize(II)V
.end method

.method public abstract setVisible(Z)V
.end method

.method public abstract setZoom(I)V
.end method

.method public abstract setZoomSupported(Z)V
.end method

.method public abstract zoomSupported()Z
.end method
