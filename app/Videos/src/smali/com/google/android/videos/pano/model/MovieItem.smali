.class public Lcom/google/android/videos/pano/model/MovieItem;
.super Lcom/google/android/videos/pano/model/VideoItem;
.source "MovieItem.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/content/Intent;JIIF)V
    .locals 19
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "subtitle"    # Ljava/lang/String;
    .param p4, "posterUri"    # Ljava/lang/String;
    .param p5, "wallpaperUri"    # Ljava/lang/String;
    .param p6, "width"    # I
    .param p7, "height"    # I
    .param p8, "intent"    # Landroid/content/Intent;
    .param p9, "expirationTimestamp"    # J
    .param p11, "releaseYear"    # I
    .param p12, "durationSec"    # I
    .param p13, "progress"    # F

    .prologue
    .line 19
    const/4 v15, 0x0

    sget-object v16, Lcom/google/android/videos/pano/model/MovieItem;->NO_RATINGS:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    const/16 v17, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move-object/from16 v9, p8

    move-wide/from16 v10, p9

    move/from16 v12, p11

    move/from16 v13, p12

    move/from16 v14, p13

    invoke-direct/range {v1 .. v17}, Lcom/google/android/videos/pano/model/MovieItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/content/Intent;JIIFLcom/google/android/videos/utils/OfferUtil$CheapestOffer;[Lcom/google/wireless/android/video/magma/proto/ViewerRating;I)V

    .line 22
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/content/Intent;JIIFLcom/google/android/videos/utils/OfferUtil$CheapestOffer;[Lcom/google/wireless/android/video/magma/proto/ViewerRating;I)V
    .locals 19
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "subtitle"    # Ljava/lang/String;
    .param p4, "posterUri"    # Ljava/lang/String;
    .param p5, "wallpaperUri"    # Ljava/lang/String;
    .param p6, "width"    # I
    .param p7, "height"    # I
    .param p8, "intent"    # Landroid/content/Intent;
    .param p9, "expirationTimestamp"    # J
    .param p11, "releaseYear"    # I
    .param p12, "durationSec"    # I
    .param p13, "progress"    # F
    .param p14, "cheapestOffer"    # Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    .param p15, "aggregatedRatings"    # [Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    .param p16, "badgeResourceId"    # I

    .prologue
    .line 29
    const/4 v9, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    move-wide/from16 v10, p9

    move/from16 v12, p11

    move/from16 v13, p12

    move/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move/from16 v17, p16

    invoke-direct/range {v0 .. v17}, Lcom/google/android/videos/pano/model/VideoItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/content/Intent;ZJIIFLcom/google/android/videos/utils/OfferUtil$CheapestOffer;[Lcom/google/wireless/android/video/magma/proto/ViewerRating;I)V

    .line 32
    return-void
.end method

.method public static createMovieItem(Landroid/content/Context;Lcom/google/wireless/android/video/magma/proto/AssetResource;Z)Lcom/google/android/videos/pano/model/VideoItem;
    .locals 20
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "asset"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p2, "purchased"    # Z

    .prologue
    .line 35
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    .line 36
    .local v2, "metadata":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/google/android/videos/pano/ui/PanoHelper;->getAssetPoster(Lcom/google/wireless/android/video/magma/proto/AssetResource;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    .line 37
    .local v7, "posterUri":Ljava/lang/String;
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/google/android/videos/pano/ui/PanoHelper;->getAssetWallpaper(Lcom/google/wireless/android/video/magma/proto/AssetResource;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    .line 38
    .local v8, "wallpaperUri":Ljava/lang/String;
    invoke-static/range {p0 .. p0}, Lcom/google/android/videos/pano/ui/PanoHelper;->getDefaultPosterHeight(Landroid/content/Context;)I

    move-result v10

    .line 39
    .local v10, "posterHeight":I
    new-instance v3, Lcom/google/android/videos/pano/model/MovieItem;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v4, v4, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    iget-object v5, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title:Ljava/lang/String;

    const/4 v6, 0x0

    int-to-float v9, v10

    const v11, 0x3f31a787

    mul-float/2addr v9, v11

    float-to-int v9, v9

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v11, v11, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v11}, Lcom/google/android/videos/pano/activity/MovieDetailsActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v11

    const-wide v12, 0x7fffffffffffffffL

    invoke-static {v2}, Lcom/google/android/videos/api/AssetResourceUtil;->getYearIfAvailable(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)I

    move-result v14

    if-eqz p2, :cond_0

    const/4 v15, 0x0

    :goto_0
    const/16 v16, 0x0

    if-eqz p2, :cond_1

    const/16 v17, 0x0

    :goto_1
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->viewerRating:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    move-object/from16 v18, v0

    if-eqz p2, :cond_2

    const v19, 0x7f0200e7

    :goto_2
    invoke-direct/range {v3 .. v19}, Lcom/google/android/videos/pano/model/MovieItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/content/Intent;JIIFLcom/google/android/videos/utils/OfferUtil$CheapestOffer;[Lcom/google/wireless/android/video/magma/proto/ViewerRating;I)V

    return-object v3

    :cond_0
    iget v15, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->durationSec:I

    goto :goto_0

    :cond_1
    const/16 v17, 0x1

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    move-object/from16 v18, v0

    invoke-static/range {v17 .. v18}, Lcom/google/android/videos/utils/OfferUtil;->getCheapestOffer(Z[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;)Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    move-result-object v17

    goto :goto_1

    :cond_2
    const/16 v19, 0x0

    goto :goto_2
.end method
