.class Lcom/google/android/videos/pano/repositories/GenresRepository$1;
.super Ljava/lang/Object;
.source "GenresRepository.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/repositories/GenresRepository;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/api/CategoryListRequest;",
        "Lcom/google/wireless/android/video/magma/proto/CategoryListResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pano/repositories/GenresRepository;


# direct methods
.method constructor <init>(Lcom/google/android/videos/pano/repositories/GenresRepository;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/google/android/videos/pano/repositories/GenresRepository$1;->this$0:Lcom/google/android/videos/pano/repositories/GenresRepository;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/api/CategoryListRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/api/CategoryListRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/GenresRepository$1;->this$0:Lcom/google/android/videos/pano/repositories/GenresRepository;

    # getter for: Lcom/google/android/videos/pano/repositories/GenresRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;
    invoke-static {v0}, Lcom/google/android/videos/pano/repositories/GenresRepository;->access$000(Lcom/google/android/videos/pano/repositories/GenresRepository;)Lcom/google/android/repolib/repositories/ArrayRepository;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/repolib/repositories/ArrayRepository;->clear()V

    .line 49
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 39
    check-cast p1, Lcom/google/android/videos/api/CategoryListRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pano/repositories/GenresRepository$1;->onError(Lcom/google/android/videos/api/CategoryListRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/api/CategoryListRequest;Lcom/google/wireless/android/video/magma/proto/CategoryListResponse;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/api/CategoryListRequest;
    .param p2, "response"    # Lcom/google/wireless/android/video/magma/proto/CategoryListResponse;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/GenresRepository$1;->this$0:Lcom/google/android/videos/pano/repositories/GenresRepository;

    # getter for: Lcom/google/android/videos/pano/repositories/GenresRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;
    invoke-static {v0}, Lcom/google/android/videos/pano/repositories/GenresRepository;->access$000(Lcom/google/android/videos/pano/repositories/GenresRepository;)Lcom/google/android/repolib/repositories/ArrayRepository;

    move-result-object v0

    iget-object v1, p2, Lcom/google/wireless/android/video/magma/proto/CategoryListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/VideoCategory;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/repolib/repositories/ArrayRepository;->setItems(Ljava/util/Collection;)V

    .line 44
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 39
    check-cast p1, Lcom/google/android/videos/api/CategoryListRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/wireless/android/video/magma/proto/CategoryListResponse;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pano/repositories/GenresRepository$1;->onResponse(Lcom/google/android/videos/api/CategoryListRequest;Lcom/google/wireless/android/video/magma/proto/CategoryListResponse;)V

    return-void
.end method
