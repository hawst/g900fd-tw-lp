.class Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;
.super Landroid/support/v4/app/FragmentPagerAdapter;
.source "HomeActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/activity/HomeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HomePageAdapter"
.end annotation


# instance fields
.field private currentItem:Ljava/lang/Object;

.field private currentPosition:I

.field final synthetic this$0:Lcom/google/android/videos/activity/HomeActivity;

.field private verticals:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/videos/activity/HomeActivity;Landroid/support/v4/app/FragmentManager;)V
    .locals 1
    .param p2, "fm"    # Landroid/support/v4/app/FragmentManager;

    .prologue
    .line 615
    iput-object p1, p0, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->this$0:Lcom/google/android/videos/activity/HomeActivity;

    .line 616
    invoke-direct {p0, p2}, Landroid/support/v4/app/FragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 617
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->verticals:Ljava/util/List;

    .line 618
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->currentPosition:I

    .line 619
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;Landroid/view/ViewGroup;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;
    .param p1, "x1"    # Landroid/view/ViewGroup;

    .prologue
    .line 609
    invoke-direct {p0, p1}, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->detachCurrentFragment(Landroid/view/ViewGroup;)V

    return-void
.end method

.method private detachCurrentFragment(Landroid/view/ViewGroup;)V
    .locals 3
    .param p1, "container"    # Landroid/view/ViewGroup;

    .prologue
    .line 709
    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->this$0:Lcom/google/android/videos/activity/HomeActivity;

    invoke-virtual {v1}, Lcom/google/android/videos/activity/HomeActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 710
    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    if-eqz v0, :cond_0

    .line 711
    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->this$0:Lcom/google/android/videos/activity/HomeActivity;

    invoke-virtual {v1}, Lcom/google/android/videos/activity/HomeActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentTransaction;->detach(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 713
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->currentItem:Ljava/lang/Object;

    .line 714
    return-void
.end method


# virtual methods
.method public changeItem(Landroid/view/ViewGroup;I)V
    .locals 3
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 673
    if-ltz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->verticals:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Vertical position is out of bounds: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 675
    invoke-virtual {p0, p1}, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->startUpdate(Landroid/view/ViewGroup;)V

    .line 676
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->currentItem:Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 677
    iget v0, p0, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->currentPosition:I

    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->currentItem:Ljava/lang/Object;

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 681
    :goto_1
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->currentItem:Ljava/lang/Object;

    .line 682
    iput p2, p0, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->currentPosition:I

    .line 683
    invoke-virtual {p0, p1}, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->finishUpdate(Landroid/view/ViewGroup;)V

    .line 684
    return-void

    .line 673
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 679
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->detachCurrentFragment(Landroid/view/ViewGroup;)V

    goto :goto_1
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 629
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->verticals:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCurrentHomeFragment()Lcom/google/android/videos/activity/HomeFragment;
    .locals 3

    .prologue
    .line 687
    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->currentItem:Ljava/lang/Object;

    instance-of v1, v1, Lcom/google/android/videos/activity/HomeFragment;

    if-eqz v1, :cond_0

    .line 688
    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->currentItem:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/videos/activity/HomeFragment;

    .line 694
    :goto_0
    return-object v1

    .line 690
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->this$0:Lcom/google/android/videos/activity/HomeActivity;

    invoke-virtual {v1}, Lcom/google/android/videos/activity/HomeActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const v2, 0x7f0f011e

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 691
    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    instance-of v1, v0, Lcom/google/android/videos/activity/HomeFragment;

    if-eqz v1, :cond_1

    .line 692
    check-cast v0, Lcom/google/android/videos/activity/HomeFragment;

    .end local v0    # "fragment":Landroid/support/v4/app/Fragment;
    move-object v1, v0

    goto :goto_0

    .line 694
    .restart local v0    # "fragment":Landroid/support/v4/app/Fragment;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 634
    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->verticals:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 635
    .local v0, "vertical":I
    sparse-switch v0, :sswitch_data_0

    .line 643
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected vertical: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 637
    :sswitch_0
    new-instance v1, Lcom/google/android/videos/activity/WatchNowFragment;

    invoke-direct {v1}, Lcom/google/android/videos/activity/WatchNowFragment;-><init>()V

    .line 641
    :goto_0
    return-object v1

    .line 639
    :sswitch_1
    invoke-static {}, Lcom/google/android/videos/activity/MyLibraryFragment;->newInstance()Lcom/google/android/videos/activity/MyLibraryFragment;

    move-result-object v1

    goto :goto_0

    .line 641
    :sswitch_2
    new-instance v1, Lcom/google/android/videos/activity/WishlistFragment;

    invoke-direct {v1}, Lcom/google/android/videos/activity/WishlistFragment;-><init>()V

    goto :goto_0

    .line 635
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x8 -> :sswitch_2
        0x10 -> :sswitch_1
    .end sparse-switch
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 669
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->verticals:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 4
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 655
    instance-of v1, p1, Lcom/google/android/videos/activity/WatchNowFragment;

    if-eqz v1, :cond_0

    .line 656
    const/4 v0, 0x4

    .line 664
    .local v0, "vertical":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->verticals:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->verticals:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    :goto_1
    return v1

    .line 657
    .end local v0    # "vertical":I
    :cond_0
    instance-of v1, p1, Lcom/google/android/videos/activity/MyLibraryFragment;

    if-eqz v1, :cond_1

    .line 658
    const/16 v0, 0x10

    .restart local v0    # "vertical":I
    goto :goto_0

    .line 659
    .end local v0    # "vertical":I
    :cond_1
    instance-of v1, p1, Lcom/google/android/videos/activity/WishlistFragment;

    if-eqz v1, :cond_2

    .line 660
    const/16 v0, 0x8

    .restart local v0    # "vertical":I
    goto :goto_0

    .line 662
    .end local v0    # "vertical":I
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected item: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 664
    .restart local v0    # "vertical":I
    :cond_3
    const/4 v1, -0x2

    goto :goto_1
.end method

.method public bridge synthetic getPageTitle(I)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 609
    invoke-virtual {p0, p1}, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->getPageTitle(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPageTitle(I)Ljava/lang/String;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 649
    iget-object v1, p0, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->this$0:Lcom/google/android/videos/activity/HomeActivity;

    iget-object v0, p0, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->verticals:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/videos/ui/VerticalsHelper;->getVerticalTitleResourceId(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/videos/activity/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVerticalPosition(I)I
    .locals 3
    .param p1, "vertical"    # I

    .prologue
    .line 698
    const/4 v1, -0x1

    .line 699
    .local v1, "position":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->verticals:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 700
    iget-object v2, p0, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->verticals:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, p1, :cond_1

    .line 701
    move v1, v0

    .line 705
    :cond_0
    return v1

    .line 699
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setVerticals(I)V
    .locals 1
    .param p1, "verticals"    # I

    .prologue
    .line 622
    invoke-static {p1}, Lcom/google/android/videos/ui/VideosDrawerHelper;->verticalsMaskToList(I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->verticals:Ljava/util/List;

    .line 623
    if-nez p1, :cond_0

    const/4 v0, -0x1

    :goto_0
    iput v0, p0, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->currentPosition:I

    .line 624
    invoke-virtual {p0}, Lcom/google/android/videos/activity/HomeActivity$HomePageAdapter;->notifyDataSetChanged()V

    .line 625
    return-void

    .line 623
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
