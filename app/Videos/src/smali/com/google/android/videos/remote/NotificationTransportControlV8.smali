.class public Lcom/google/android/videos/remote/NotificationTransportControlV8;
.super Lcom/google/android/videos/remote/NotificationTransportControl;
.source "NotificationTransportControlV8.java"


# instance fields
.field protected handler:Landroid/os/Handler;

.field protected icon:Landroid/graphics/Bitmap;

.field protected notification:Landroid/app/Notification;


# direct methods
.method protected constructor <init>(Lcom/google/android/videos/remote/RemoteTracker;Landroid/content/Context;)V
    .locals 2
    .param p1, "remoteTracker"    # Lcom/google/android/videos/remote/RemoteTracker;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/remote/NotificationTransportControl;-><init>(Lcom/google/android/videos/remote/RemoteTracker;Landroid/content/Context;)V

    .line 37
    new-instance v0, Lcom/google/android/videos/remote/NotificationTransportControlV8$1;

    invoke-virtual {p2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/videos/remote/NotificationTransportControlV8$1;-><init>(Lcom/google/android/videos/remote/NotificationTransportControlV8;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControlV8;->handler:Landroid/os/Handler;

    .line 46
    return-void
.end method

.method private static scaleBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "maxWidth"    # I
    .param p2, "maxHeight"    # I

    .prologue
    .line 167
    if-nez p0, :cond_0

    .line 168
    const/4 v5, 0x0

    .line 182
    :goto_0
    return-object v5

    .line 171
    :cond_0
    const/high16 v2, 0x3f800000    # 1.0f

    .line 172
    .local v2, "scaleX":F
    const/high16 v3, 0x3f800000    # 1.0f

    .line 173
    .local v3, "scaleY":F
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    if-le v5, p1, :cond_1

    .line 174
    int-to-float v5, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    div-float v2, v5, v6

    .line 176
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    if-le v5, p2, :cond_2

    .line 177
    int-to-float v5, p2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    div-float v3, v5, v6

    .line 179
    :cond_2
    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 180
    .local v1, "scale":F
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v1

    float-to-int v4, v5

    .line 181
    .local v4, "width":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v1

    float-to-int v0, v5

    .line 182
    .local v0, "height":I
    const/4 v5, 0x0

    invoke-static {p0, v4, v0, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v5

    goto :goto_0
.end method


# virtual methods
.method protected createContentView(Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;)Landroid/widget/RemoteViews;
    .locals 3
    .param p1, "service"    # Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;

    .prologue
    .line 149
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Lcom/google/android/videos/remote/NotificationTransportControlV8;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0400b5

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method

.method protected createNotification(Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;)V
    .locals 2
    .param p1, "service"    # Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;

    .prologue
    const/4 v1, 0x0

    .line 50
    const-string v0, "Creating notification"

    invoke-static {v0}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 51
    invoke-static {}, Lcom/google/android/videos/utils/Preconditions;->checkMainThread()V

    .line 52
    iget-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControlV8;->notification:Landroid/app/Notification;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 54
    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControlV8;->notification:Landroid/app/Notification;

    .line 55
    iget-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControlV8;->notification:Landroid/app/Notification;

    iput v1, v0, Landroid/app/Notification;->defaults:I

    .line 56
    iget-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControlV8;->notification:Landroid/app/Notification;

    const v1, 0x7f0200db

    iput v1, v0, Landroid/app/Notification;->icon:I

    .line 57
    iget-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControlV8;->notification:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0xa

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 58
    iget-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControlV8;->notification:Landroid/app/Notification;

    const/4 v1, 0x7

    invoke-virtual {p1, v1}, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->getPendingIntent(I)Landroid/app/PendingIntent;

    move-result-object v1

    iput-object v1, v0, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    .line 59
    iget-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControlV8;->notification:Landroid/app/Notification;

    const/4 v1, 0x6

    invoke-virtual {p1, v1}, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->getPendingIntent(I)Landroid/app/PendingIntent;

    move-result-object v1

    iput-object v1, v0, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    .line 60
    invoke-virtual {p0, p1}, Lcom/google/android/videos/remote/NotificationTransportControlV8;->updateNotification(Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;)V

    .line 61
    return-void

    :cond_0
    move v0, v1

    .line 52
    goto :goto_0
.end method

.method protected destroyNotification()V
    .locals 3

    .prologue
    .line 125
    const-string v1, "Destroying notification"

    invoke-static {v1}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 126
    invoke-static {}, Lcom/google/android/videos/utils/Preconditions;->checkMainThread()V

    .line 127
    iget-object v1, p0, Lcom/google/android/videos/remote/NotificationTransportControlV8;->notification:Landroid/app/Notification;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 130
    iget-object v1, p0, Lcom/google/android/videos/remote/NotificationTransportControlV8;->handler:Landroid/os/Handler;

    const/16 v2, 0x64

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 132
    invoke-virtual {p0}, Lcom/google/android/videos/remote/NotificationTransportControlV8;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "notification"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 134
    .local v0, "manager":Landroid/app/NotificationManager;
    const v1, 0x7f0f003b

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 135
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/videos/remote/NotificationTransportControlV8;->notification:Landroid/app/Notification;

    .line 136
    return-void

    .line 127
    .end local v0    # "manager":Landroid/app/NotificationManager;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onPosterBitmapChanged()V
    .locals 2

    .prologue
    .line 140
    iget-object v1, p0, Lcom/google/android/videos/remote/NotificationTransportControlV8;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v1}, Lcom/google/android/videos/remote/RemoteTracker;->getPosterBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 141
    .local v0, "posterBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p0, v0}, Lcom/google/android/videos/remote/NotificationTransportControlV8;->scaleBitmapForIcon(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/remote/NotificationTransportControlV8;->icon:Landroid/graphics/Bitmap;

    .line 142
    invoke-super {p0}, Lcom/google/android/videos/remote/NotificationTransportControl;->onPosterBitmapChanged()V

    .line 143
    return-void
.end method

.method protected scaleBitmapForIcon(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 157
    invoke-virtual {p0}, Lcom/google/android/videos/remote/NotificationTransportControlV8;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 158
    .local v2, "res":Landroid/content/res/Resources;
    const v3, 0x7f0e01bd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 159
    .local v1, "maxWidth":F
    const v3, 0x7f0e01be

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 160
    .local v0, "maxHeight":F
    float-to-int v3, v1

    float-to-int v4, v0

    invoke-static {p1, v3, v4}, Lcom/google/android/videos/remote/NotificationTransportControlV8;->scaleBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v3

    return-object v3
.end method

.method public final updateNotification(Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;)V
    .locals 2
    .param p1, "service"    # Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;

    .prologue
    .line 65
    invoke-static {}, Lcom/google/android/videos/utils/Preconditions;->checkMainThread()V

    .line 66
    iget-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControlV8;->notification:Landroid/app/Notification;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 68
    iget-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControlV8;->notification:Landroid/app/Notification;

    invoke-virtual {p0, p1}, Lcom/google/android/videos/remote/NotificationTransportControlV8;->createContentView(Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;)Landroid/widget/RemoteViews;

    move-result-object v1

    iput-object v1, v0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 72
    iget-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControlV8;->handler:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 73
    return-void

    .line 66
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected updateNotificationInternal(Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;)V
    .locals 10
    .param p1, "service"    # Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;

    .prologue
    const v9, 0x7f0f01e7

    const/4 v5, 0x1

    const/4 v6, 0x0

    const v8, 0x7f0f01e9

    .line 76
    invoke-static {}, Lcom/google/android/videos/utils/Preconditions;->checkMainThread()V

    .line 77
    iget-object v4, p0, Lcom/google/android/videos/remote/NotificationTransportControlV8;->notification:Landroid/app/Notification;

    if-eqz v4, :cond_1

    move v4, v5

    :goto_0
    invoke-static {v4}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 79
    iget-object v4, p0, Lcom/google/android/videos/remote/NotificationTransportControlV8;->notification:Landroid/app/Notification;

    iget-object v0, v4, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 80
    .local v0, "contentView":Landroid/widget/RemoteViews;
    const v4, 0x7f0f01e5

    invoke-virtual {p0}, Lcom/google/android/videos/remote/NotificationTransportControlV8;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v4, v7}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 81
    const v4, 0x7f0f01e8

    invoke-virtual {p0}, Lcom/google/android/videos/remote/NotificationTransportControlV8;->getStatus()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v4, v7}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 82
    iget-object v4, p0, Lcom/google/android/videos/remote/NotificationTransportControlV8;->icon:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_2

    .line 83
    iget-object v4, p0, Lcom/google/android/videos/remote/NotificationTransportControlV8;->icon:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v9, v4}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 88
    :goto_1
    iget-object v4, p0, Lcom/google/android/videos/remote/NotificationTransportControlV8;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v4}, Lcom/google/android/videos/remote/RemoteTracker;->getPlayerState()Lcom/google/android/videos/remote/PlayerState;

    move-result-object v1

    .line 89
    .local v1, "playerState":Lcom/google/android/videos/remote/PlayerState;
    iget-object v4, p0, Lcom/google/android/videos/remote/NotificationTransportControlV8;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v4}, Lcom/google/android/videos/remote/RemoteTracker;->getVideoInfo()Lcom/google/android/videos/remote/RemoteVideoInfo;

    move-result-object v3

    .line 91
    .local v3, "videoInfo":Lcom/google/android/videos/remote/RemoteVideoInfo;
    const/4 v2, 0x0

    .line 92
    .local v2, "showPlayPause":Z
    iget-object v4, p0, Lcom/google/android/videos/remote/NotificationTransportControlV8;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v4}, Lcom/google/android/videos/remote/RemoteTracker;->getError()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_0

    if-eqz v1, :cond_0

    if-eqz v3, :cond_0

    .line 93
    iget v4, v1, Lcom/google/android/videos/remote/PlayerState;->state:I

    packed-switch v4, :pswitch_data_0

    .line 118
    :cond_0
    :goto_2
    if-eqz v2, :cond_3

    :goto_3
    invoke-virtual {v0, v8, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 120
    const v4, 0x7f0f003b

    iget-object v5, p0, Lcom/google/android/videos/remote/NotificationTransportControlV8;->notification:Landroid/app/Notification;

    invoke-virtual {p1, v4, v5}, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->startForeground(ILandroid/app/Notification;)V

    .line 121
    return-void

    .end local v0    # "contentView":Landroid/widget/RemoteViews;
    .end local v1    # "playerState":Lcom/google/android/videos/remote/PlayerState;
    .end local v2    # "showPlayPause":Z
    .end local v3    # "videoInfo":Lcom/google/android/videos/remote/RemoteVideoInfo;
    :cond_1
    move v4, v6

    .line 77
    goto :goto_0

    .line 85
    .restart local v0    # "contentView":Landroid/widget/RemoteViews;
    :cond_2
    const v4, 0x7f0200db

    invoke-virtual {v0, v9, v4}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_1

    .line 95
    .restart local v1    # "playerState":Lcom/google/android/videos/remote/PlayerState;
    .restart local v2    # "showPlayPause":Z
    .restart local v3    # "videoInfo":Lcom/google/android/videos/remote/RemoteVideoInfo;
    :pswitch_0
    const/4 v2, 0x1

    .line 96
    const v4, 0x7f0201c7

    invoke-virtual {v0, v8, v4}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 97
    invoke-virtual {p1, v5}, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->getPendingIntent(I)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v0, v8, v4}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 99
    const-string v4, "setContentDescription"

    invoke-virtual {p0}, Lcom/google/android/videos/remote/NotificationTransportControlV8;->getContext()Landroid/content/Context;

    move-result-object v5

    const v7, 0x7f0b01c1

    invoke-virtual {v5, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v8, v4, v5}, Landroid/widget/RemoteViews;->setCharSequence(ILjava/lang/String;Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 103
    :pswitch_1
    const/4 v2, 0x1

    .line 104
    const v4, 0x7f0201c8

    invoke-virtual {v0, v8, v4}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 105
    invoke-virtual {p1, v6}, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->getPendingIntent(I)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v0, v8, v4}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 107
    const-string v4, "setContentDescription"

    invoke-virtual {p0}, Lcom/google/android/videos/remote/NotificationTransportControlV8;->getContext()Landroid/content/Context;

    move-result-object v5

    const v7, 0x7f0b01c0

    invoke-virtual {v5, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v8, v4, v5}, Landroid/widget/RemoteViews;->setCharSequence(ILjava/lang/String;Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 118
    :cond_3
    const/16 v6, 0x8

    goto :goto_3

    .line 93
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
