.class Lcom/google/android/videos/remote/NotificationTransportControlV16;
.super Lcom/google/android/videos/remote/NotificationTransportControlV8;
.source "NotificationTransportControlV16.java"


# direct methods
.method constructor <init>(Lcom/google/android/videos/remote/RemoteTracker;Landroid/content/Context;)V
    .locals 0
    .param p1, "remoteTracker"    # Lcom/google/android/videos/remote/RemoteTracker;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/remote/NotificationTransportControlV8;-><init>(Lcom/google/android/videos/remote/RemoteTracker;Landroid/content/Context;)V

    .line 24
    return-void
.end method


# virtual methods
.method protected createContentView(Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;)Landroid/widget/RemoteViews;
    .locals 3
    .param p1, "service"    # Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;

    .prologue
    .line 28
    invoke-super {p0, p1}, Lcom/google/android/videos/remote/NotificationTransportControlV8;->createContentView(Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;)Landroid/widget/RemoteViews;

    move-result-object v0

    .line 29
    .local v0, "contentView":Landroid/widget/RemoteViews;
    const v1, 0x7f0f01ea

    const/4 v2, 0x5

    invoke-virtual {p1, v2}, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->getPendingIntent(I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 31
    return-object v0
.end method

.method protected updateNotificationInternal(Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;)V
    .locals 2
    .param p1, "service"    # Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControlV16;->notification:Landroid/app/Notification;

    iget-object v1, p0, Lcom/google/android/videos/remote/NotificationTransportControlV16;->notification:Landroid/app/Notification;

    iget-object v1, v1, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    iput-object v1, v0, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    .line 37
    invoke-super {p0, p1}, Lcom/google/android/videos/remote/NotificationTransportControlV8;->updateNotificationInternal(Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;)V

    .line 38
    return-void
.end method
