.class public Lcom/google/android/videos/api/VideoCollectionListRequest;
.super Lcom/google/android/videos/async/Request;
.source "VideoCollectionListRequest.java"


# instance fields
.field public final countryRestriction:Ljava/lang/String;

.field public final localeRestriction:Ljava/lang/String;

.field public final maxChildren:I

.field public final maxResults:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;I)V
    .locals 6
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "countryRestriction"    # Ljava/lang/String;
    .param p3, "localeRestriction"    # Ljava/util/Locale;
    .param p4, "maxResults"    # I

    .prologue
    .line 22
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/api/VideoCollectionListRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;II)V

    .line 23
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;II)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "countryRestriction"    # Ljava/lang/String;
    .param p3, "localeRestriction"    # Ljava/util/Locale;
    .param p4, "maxResults"    # I
    .param p5, "maxChildren"    # I

    .prologue
    .line 27
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/async/Request;-><init>(Ljava/lang/String;Z)V

    .line 28
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/videos/api/VideoCollectionListRequest;->countryRestriction:Ljava/lang/String;

    .line 29
    invoke-static {p3}, Lcom/google/android/videos/utils/LocaleUtils;->toString(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/api/VideoCollectionListRequest;->localeRestriction:Ljava/lang/String;

    .line 30
    iput p4, p0, Lcom/google/android/videos/api/VideoCollectionListRequest;->maxResults:I

    .line 31
    iput p5, p0, Lcom/google/android/videos/api/VideoCollectionListRequest;->maxChildren:I

    .line 32
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 36
    if-ne p0, p1, :cond_1

    .line 41
    :cond_0
    :goto_0
    return v1

    .line 37
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 39
    check-cast v0, Lcom/google/android/videos/api/VideoCollectionListRequest;

    .line 41
    .local v0, "that":Lcom/google/android/videos/api/VideoCollectionListRequest;
    iget v3, p0, Lcom/google/android/videos/api/VideoCollectionListRequest;->maxResults:I

    iget v4, v0, Lcom/google/android/videos/api/VideoCollectionListRequest;->maxResults:I

    if-ne v3, v4, :cond_4

    iget v3, p0, Lcom/google/android/videos/api/VideoCollectionListRequest;->maxChildren:I

    iget v4, v0, Lcom/google/android/videos/api/VideoCollectionListRequest;->maxChildren:I

    if-ne v3, v4, :cond_4

    iget-object v3, p0, Lcom/google/android/videos/api/VideoCollectionListRequest;->account:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/api/VideoCollectionListRequest;->account:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/videos/api/VideoCollectionListRequest;->countryRestriction:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/api/VideoCollectionListRequest;->countryRestriction:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 49
    iget-object v2, p0, Lcom/google/android/videos/api/VideoCollectionListRequest;->account:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/videos/api/VideoCollectionListRequest;->account:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 50
    .local v0, "result":I
    :goto_0
    mul-int/lit8 v3, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/api/VideoCollectionListRequest;->countryRestriction:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/videos/api/VideoCollectionListRequest;->countryRestriction:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 51
    mul-int/lit8 v2, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/videos/api/VideoCollectionListRequest;->localeRestriction:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/api/VideoCollectionListRequest;->localeRestriction:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 52
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/videos/api/VideoCollectionListRequest;->maxResults:I

    add-int v0, v1, v2

    .line 53
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/videos/api/VideoCollectionListRequest;->maxChildren:I

    add-int v0, v1, v2

    .line 54
    return v0

    .end local v0    # "result":I
    :cond_1
    move v0, v1

    .line 49
    goto :goto_0

    .restart local v0    # "result":I
    :cond_2
    move v2, v1

    .line 50
    goto :goto_1
.end method
