.class public Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;
.super Landroid/transition/Transition;
.source "TransitionUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/TransitionUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ImageTintTransition"
.end annotation


# instance fields
.field private final evaluator:Landroid/animation/ArgbEvaluator;

.field private fromColor:I

.field private toColor:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 219
    invoke-direct {p0}, Landroid/transition/Transition;-><init>()V

    .line 220
    new-instance v0, Landroid/animation/ArgbEvaluator;

    invoke-direct {v0}, Landroid/animation/ArgbEvaluator;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;->evaluator:Landroid/animation/ArgbEvaluator;

    .line 221
    return-void
.end method


# virtual methods
.method public captureEndValues(Landroid/transition/TransitionValues;)V
    .locals 0
    .param p1, "values"    # Landroid/transition/TransitionValues;

    .prologue
    .line 237
    return-void
.end method

.method public captureStartValues(Landroid/transition/TransitionValues;)V
    .locals 0
    .param p1, "values"    # Landroid/transition/TransitionValues;

    .prologue
    .line 233
    return-void
.end method

.method public createAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
    .locals 6
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "startValues"    # Landroid/transition/TransitionValues;
    .param p3, "endValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 242
    iget-object v0, p3, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    const-string v1, "colorFilter"

    iget-object v2, p0, Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;->evaluator:Landroid/animation/ArgbEvaluator;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;->fromColor:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;->toColor:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofObject(Ljava/lang/Object;Ljava/lang/String;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    return-object v0
.end method

.method public setFromColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 224
    iput p1, p0, Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;->fromColor:I

    .line 225
    return-void
.end method

.method public setToColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 228
    iput p1, p0, Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;->toColor:I

    .line 229
    return-void
.end method
