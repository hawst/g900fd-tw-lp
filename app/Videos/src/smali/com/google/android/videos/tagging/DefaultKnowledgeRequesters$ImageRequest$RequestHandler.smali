.class public final Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest$RequestHandler;
.super Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$BytesRequestHandler;
.source "DefaultKnowledgeRequesters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RequestHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$BytesRequestHandler",
        "<",
        "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;",
        "Landroid/graphics/Bitmap;",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field private final bytesConverter:Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;


# direct methods
.method public constructor <init>(I)V
    .locals 2
    .param p1, "desiredWidth"    # I

    .prologue
    .line 821
    invoke-direct {p0}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$BytesRequestHandler;-><init>()V

    .line 822
    new-instance v0, Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;-><init>(IZ)V

    iput-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest$RequestHandler;->bytesConverter:Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;

    .line 823
    return-void
.end method


# virtual methods
.method public createComponent(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;Lcom/google/android/videos/utils/ByteArray;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;
    .param p2, "response"    # Lcom/google/android/videos/utils/ByteArray;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 828
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest$RequestHandler;->bytesConverter:Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;

    invoke-virtual {v0, p2}, Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;->convertResponse(Lcom/google/android/videos/utils/ByteArray;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic createComponent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 816
    check-cast p1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/videos/utils/ByteArray;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest$RequestHandler;->createComponent(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;Lcom/google/android/videos/utils/ByteArray;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public createUnderlyingRequest(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;Lcom/google/android/videos/utils/ByteArray;)Landroid/net/Uri;
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;
    .param p2, "unused"    # Lcom/google/android/videos/utils/ByteArray;

    .prologue
    .line 833
    # getter for: Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;->image:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
    invoke-static {p1}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;->access$600(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;)Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;->url:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic createUnderlyingRequest(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 816
    check-cast p1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/videos/utils/ByteArray;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest$RequestHandler;->createUnderlyingRequest(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;Lcom/google/android/videos/utils/ByteArray;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
