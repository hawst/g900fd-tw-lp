.class public final Lcom/google/android/videos/pano/datasource/DataSources;
.super Ljava/lang/Object;
.source "DataSources.java"


# instance fields
.field public final newEpisodesDataSource:Lcom/google/android/videos/pano/datasource/BaseDataSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/pano/datasource/BaseDataSource",
            "<",
            "Lcom/google/android/videos/pano/model/VideoItem;",
            ">;"
        }
    .end annotation
.end field

.field public final watchNowDataSource:Lcom/google/android/videos/pano/datasource/BaseDataSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/pano/datasource/BaseDataSource",
            "<",
            "Lcom/google/android/videos/pano/model/VideoItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/Config;Lcom/google/android/videos/async/Requester;Landroid/content/SharedPreferences;Lcom/google/android/videos/ContentNotificationManager;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/repolib/observers/Observable;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "signInManager"    # Lcom/google/android/videos/accounts/SignInManager;
    .param p3, "purchaseStore"    # Lcom/google/android/videos/store/PurchaseStore;
    .param p4, "database"    # Lcom/google/android/videos/store/Database;
    .param p5, "configurationStore"    # Lcom/google/android/videos/store/ConfigurationStore;
    .param p6, "config"    # Lcom/google/android/videos/Config;
    .param p8, "preferences"    # Landroid/content/SharedPreferences;
    .param p9, "contentNotificationManager"    # Lcom/google/android/videos/ContentNotificationManager;
    .param p10, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;
    .param p11, "localeObservable"    # Lcom/google/android/repolib/observers/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/videos/accounts/SignInManager;",
            "Lcom/google/android/videos/store/PurchaseStore;",
            "Lcom/google/android/videos/store/Database;",
            "Lcom/google/android/videos/store/ConfigurationStore;",
            "Lcom/google/android/videos/Config;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/PromotionsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;",
            ">;",
            "Landroid/content/SharedPreferences;",
            "Lcom/google/android/videos/ContentNotificationManager;",
            "Lcom/google/android/videos/utils/NetworkStatus;",
            "Lcom/google/android/repolib/observers/Observable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 33
    .local p7, "promotionRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/PromotionsRequest;Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v2, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p5

    move-object/from16 v10, p8

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v2 .. v12}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;-><init>(Landroid/content/Context;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/Config;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/ConfigurationStore;Landroid/content/SharedPreferences;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/repolib/observers/Observable;)V

    iput-object v2, p0, Lcom/google/android/videos/pano/datasource/DataSources;->watchNowDataSource:Lcom/google/android/videos/pano/datasource/BaseDataSource;

    .line 37
    new-instance v2, Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;

    move-object/from16 v0, p9

    move-object/from16 v1, p4

    invoke-direct {v2, p1, p2, v0, v1}, Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;-><init>(Landroid/content/Context;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/ContentNotificationManager;Lcom/google/android/videos/store/Database;)V

    iput-object v2, p0, Lcom/google/android/videos/pano/datasource/DataSources;->newEpisodesDataSource:Lcom/google/android/videos/pano/datasource/BaseDataSource;

    .line 39
    return-void
.end method
