.class public Lcom/google/android/videos/ui/PinHelper$UnpinConfirmDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "PinHelper.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/PinHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UnpinConfirmDialogFragment"
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 509
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static showInstance(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "manager"    # Landroid/support/v4/app/FragmentManager;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "videoId"    # Ljava/lang/String;
    .param p4, "title"    # Ljava/lang/String;
    .param p5, "showTitle"    # Ljava/lang/String;
    .param p6, "pinningStatus"    # I
    .param p7, "pinningStatusReason"    # I

    .prologue
    .line 517
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 518
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    const-string v2, "video_id"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    const-string v2, "video_title"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    const-string v2, "show_title"

    invoke-virtual {v0, v2, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    const-string v2, "pinning_status"

    invoke-virtual {v0, v2, p6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 523
    const-string v2, "pinning_status_reason"

    invoke-virtual {v0, v2, p7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 525
    new-instance v1, Lcom/google/android/videos/ui/PinHelper$UnpinConfirmDialogFragment;

    invoke-direct {v1}, Lcom/google/android/videos/ui/PinHelper$UnpinConfirmDialogFragment;-><init>()V

    .line 526
    .local v1, "instance":Lcom/google/android/videos/ui/PinHelper$UnpinConfirmDialogFragment;
    invoke-virtual {v1, v0}, Lcom/google/android/videos/ui/PinHelper$UnpinConfirmDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 527
    const-string v2, "UnpinConfirmDialog"

    invoke-virtual {v1, p1, v2}, Lcom/google/android/videos/ui/PinHelper$UnpinConfirmDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 529
    # invokes: Lcom/google/android/videos/ui/PinHelper;->getEventLogger(Landroid/content/Context;)Lcom/google/android/videos/logging/EventLogger;
    invoke-static {p0}, Lcom/google/android/videos/ui/PinHelper;->access$400(Landroid/content/Context;)Lcom/google/android/videos/logging/EventLogger;

    move-result-object v3

    const/4 v2, 0x3

    if-ne p6, v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-interface {v3, v2}, Lcom/google/android/videos/logging/EventLogger;->onShowDownloadDialog(Z)V

    .line 531
    return-void

    .line 529
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 574
    invoke-virtual {p0}, Lcom/google/android/videos/ui/PinHelper$UnpinConfirmDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 575
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    if-nez v0, :cond_1

    .line 584
    :cond_0
    :goto_0
    return-void

    .line 578
    :cond_1
    const/4 v3, -0x1

    if-ne p2, v3, :cond_2

    const/4 v2, 0x1

    .line 579
    .local v2, "remove":Z
    :goto_1
    # invokes: Lcom/google/android/videos/ui/PinHelper;->getEventLogger(Landroid/content/Context;)Lcom/google/android/videos/logging/EventLogger;
    invoke-static {v0}, Lcom/google/android/videos/ui/PinHelper;->access$400(Landroid/content/Context;)Lcom/google/android/videos/logging/EventLogger;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/google/android/videos/logging/EventLogger;->onDismissDownloadDialog(Z)V

    .line 580
    if-eqz v2, :cond_0

    .line 581
    invoke-static {v0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v1

    .line 582
    .local v1, "globals":Lcom/google/android/videos/VideosGlobals;
    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getPinHelper()Lcom/google/android/videos/ui/PinHelper;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/videos/ui/PinHelper$UnpinConfirmDialogFragment;->account:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/videos/ui/PinHelper$UnpinConfirmDialogFragment;->videoId:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/google/android/videos/ui/PinHelper;->unpinVideo(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 578
    .end local v1    # "globals":Lcom/google/android/videos/VideosGlobals;
    .end local v2    # "remove":Z
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 14
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x0

    const/16 v13, 0xa

    const/4 v8, 0x1

    .line 535
    invoke-virtual {p0}, Lcom/google/android/videos/ui/PinHelper$UnpinConfirmDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 536
    .local v0, "args":Landroid/os/Bundle;
    const-string v10, "authAccount"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/google/android/videos/ui/PinHelper$UnpinConfirmDialogFragment;->account:Ljava/lang/String;

    .line 537
    const-string v10, "video_id"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/google/android/videos/ui/PinHelper$UnpinConfirmDialogFragment;->videoId:Ljava/lang/String;

    .line 539
    const-string v10, "show_title"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_2

    move v1, v8

    .line 540
    .local v1, "isShow":Z
    :goto_0
    const-string v10, "video_title"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 541
    .local v6, "title":Ljava/lang/String;
    const-string v10, "pinning_status"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 543
    .local v4, "pinningStatus":I
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 544
    .local v2, "message":Landroid/text/SpannableStringBuilder;
    if-eqz v1, :cond_0

    .line 545
    const-string v10, "show_title"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v10

    invoke-virtual {v10, v13}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 547
    :cond_0
    invoke-virtual {v2, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v10

    invoke-virtual {v10, v13}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 548
    new-instance v10, Landroid/text/style/StyleSpan;

    invoke-direct {v10, v8}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v11

    const/16 v12, 0x21

    invoke-virtual {v2, v10, v9, v11, v12}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 550
    invoke-virtual {v2, v13}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 552
    const v7, 0x7f0b012e

    .line 553
    .local v7, "titleId":I
    if-eqz v1, :cond_3

    const v3, 0x7f0b0130

    .line 555
    .local v3, "messageId":I
    :goto_1
    const/4 v9, 0x3

    if-ne v4, v9, :cond_5

    .line 556
    if-eqz v1, :cond_4

    const v7, 0x7f0b012d

    .line 562
    :cond_1
    :goto_2
    invoke-virtual {p0, v3}, Lcom/google/android/videos/ui/PinHelper$UnpinConfirmDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 564
    new-instance v8, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/videos/ui/PinHelper$UnpinConfirmDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-direct {v8, v9}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v8, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    const/high16 v9, 0x1040000

    invoke-virtual {v8, v9, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    const v9, 0x7f0b010b

    invoke-virtual {v8, v9, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    invoke-virtual {v8, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v8

    return-object v8

    .end local v1    # "isShow":Z
    .end local v2    # "message":Landroid/text/SpannableStringBuilder;
    .end local v3    # "messageId":I
    .end local v4    # "pinningStatus":I
    .end local v6    # "title":Ljava/lang/String;
    .end local v7    # "titleId":I
    :cond_2
    move v1, v9

    .line 539
    goto :goto_0

    .line 553
    .restart local v1    # "isShow":Z
    .restart local v2    # "message":Landroid/text/SpannableStringBuilder;
    .restart local v4    # "pinningStatus":I
    .restart local v6    # "title":Ljava/lang/String;
    .restart local v7    # "titleId":I
    :cond_3
    const v3, 0x7f0b012f

    goto :goto_1

    .line 556
    .restart local v3    # "messageId":I
    :cond_4
    const v7, 0x7f0b012c

    goto :goto_2

    .line 558
    :cond_5
    if-ne v4, v8, :cond_1

    .line 559
    const-string v8, "pinning_status_reason"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 560
    .local v5, "pinningStatusReason":I
    invoke-static {v5}, Lcom/google/android/videos/pinning/PinningStatusHelper;->getPendingReasonTextId(I)I

    move-result v3

    goto :goto_2
.end method
