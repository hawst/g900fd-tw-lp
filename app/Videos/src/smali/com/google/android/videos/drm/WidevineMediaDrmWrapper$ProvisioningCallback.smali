.class Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$ProvisioningCallback;
.super Ljava/lang/Object;
.source "WidevineMediaDrmWrapper.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProvisioningCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        "[B>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;)V
    .locals 0

    .prologue
    .line 560
    iput-object p1, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$ProvisioningCallback;->this$0:Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;
    .param p2, "x1"    # Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$1;

    .prologue
    .line 560
    invoke-direct {p0, p1}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$ProvisioningCallback;-><init>(Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 560
    check-cast p1, Lorg/apache/http/client/methods/HttpUriRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$ProvisioningCallback;->onError(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onError(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 569
    iget-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$ProvisioningCallback;->this$0:Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->onProvisioningError(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/Exception;)V

    .line 570
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 560
    check-cast p1, Lorg/apache/http/client/methods/HttpUriRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, [B

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$ProvisioningCallback;->onResponse(Lorg/apache/http/client/methods/HttpUriRequest;[B)V

    return-void
.end method

.method public onResponse(Lorg/apache/http/client/methods/HttpUriRequest;[B)V
    .locals 1
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p2, "response"    # [B

    .prologue
    .line 564
    iget-object v0, p0, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$ProvisioningCallback;->this$0:Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->onProvisioningResponse(Lorg/apache/http/client/methods/HttpUriRequest;[B)V

    .line 565
    return-void
.end method
