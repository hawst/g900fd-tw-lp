.class public Lcom/google/android/videos/ui/ItemsWithHeadingFlow;
.super Lcom/google/android/videos/flow/Flow;
.source "ItemsWithHeadingFlow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;,
        Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;,
        Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Lcom/google/android/videos/ui/playnext/ClusterItemView;",
        "D::",
        "Lcom/google/android/videos/adapter/DataSource",
        "<*>;>",
        "Lcom/google/android/videos/flow/Flow;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private final clickListener:Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;

.field private final clusterItemViewBinder:Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder",
            "<TV;TD;>;"
        }
    .end annotation
.end field

.field private dataSource:Lcom/google/android/videos/adapter/DataSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TD;"
        }
    .end annotation
.end field

.field private final dataSourceObserver:Landroid/database/DataSetObserver;

.field private final headingIdentifier:Ljava/lang/Object;

.field private final layoutId:I

.field private maxItems:I

.field private final sectionHeadingBinder:Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;Lcom/google/android/videos/adapter/DataSource;ILcom/google/android/videos/ui/playnext/ClusterItemView$Binder;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;I)V
    .locals 9
    .param p1, "sectionHeadingBinder"    # Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;
    .param p3, "layoutId"    # I
    .param p5, "clickListener"    # Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;
    .param p6, "maxItems"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;",
            "TD;I",
            "Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder",
            "<TV;TD;>;",
            "Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 192
    .local p0, "this":Lcom/google/android/videos/ui/ItemsWithHeadingFlow;, "Lcom/google/android/videos/ui/ItemsWithHeadingFlow<TV;TD;>;"
    .local p2, "dataSource":Lcom/google/android/videos/adapter/DataSource;, "TD;"
    .local p4, "clusterItemViewBinder":Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;, "Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder<TV;TD;>;"
    new-instance v7, Ljava/lang/Object;

    invoke-direct {v7}, Ljava/lang/Object;-><init>()V

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v8}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;-><init>(Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;Lcom/google/android/videos/adapter/DataSource;ILcom/google/android/videos/ui/playnext/ClusterItemView$Binder;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;ILjava/lang/Object;Z)V

    .line 194
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;Lcom/google/android/videos/adapter/DataSource;ILcom/google/android/videos/ui/playnext/ClusterItemView$Binder;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;ILjava/lang/Object;Z)V
    .locals 3
    .param p1, "sectionHeadingBinder"    # Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;
    .param p3, "layoutId"    # I
    .param p5, "clickListener"    # Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;
    .param p6, "maxItems"    # I
    .param p7, "headingIdentifier"    # Ljava/lang/Object;
    .param p8, "registerObserver"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;",
            "TD;I",
            "Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder",
            "<TV;TD;>;",
            "Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;",
            "I",
            "Ljava/lang/Object;",
            "Z)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/videos/ui/ItemsWithHeadingFlow;, "Lcom/google/android/videos/ui/ItemsWithHeadingFlow<TV;TD;>;"
    .local p2, "dataSource":Lcom/google/android/videos/adapter/DataSource;, "TD;"
    .local p4, "clusterItemViewBinder":Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;, "Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder<TV;TD;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 206
    invoke-direct {p0}, Lcom/google/android/videos/flow/Flow;-><init>()V

    .line 207
    iput-object p1, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->sectionHeadingBinder:Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;

    .line 208
    if-lez p3, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    .line 209
    iput p3, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->layoutId:I

    .line 210
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;

    iput-object v0, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->clusterItemViewBinder:Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;

    .line 211
    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;

    iput-object v0, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->clickListener:Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;

    .line 212
    iput p6, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->maxItems:I

    .line 213
    if-eqz p1, :cond_0

    if-eqz p7, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    invoke-static {v2}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    .line 214
    iput-object p7, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->headingIdentifier:Ljava/lang/Object;

    .line 216
    if-eqz p8, :cond_3

    .line 217
    new-instance v0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$1;

    invoke-direct {v0, p0}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$1;-><init>(Lcom/google/android/videos/ui/ItemsWithHeadingFlow;)V

    iput-object v0, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->dataSourceObserver:Landroid/database/DataSetObserver;

    .line 231
    :goto_1
    invoke-virtual {p0, p2}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->setDataSource(Lcom/google/android/videos/adapter/DataSource;)V

    .line 232
    return-void

    :cond_2
    move v0, v2

    .line 208
    goto :goto_0

    .line 228
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->dataSourceObserver:Landroid/database/DataSetObserver;

    goto :goto_1
.end method

.method private configureListeners([Landroid/view/View;I)V
    .locals 4
    .param p1, "clickableViews"    # [Landroid/view/View;
    .param p2, "dataSourcePosition"    # I

    .prologue
    .line 349
    .local p0, "this":Lcom/google/android/videos/ui/ItemsWithHeadingFlow;, "Lcom/google/android/videos/ui/ItemsWithHeadingFlow<TV;TD;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    .line 350
    aget-object v1, p1, v0

    .line 351
    .local v1, "view":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 352
    const v2, 0x7f0f0040

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 353
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 354
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 349
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 357
    .end local v1    # "view":Landroid/view/View;
    :cond_1
    return-void
.end method

.method private getDataSourcePosition(I)I
    .locals 1
    .param p1, "flowPosition"    # I

    .prologue
    .line 308
    .local p0, "this":Lcom/google/android/videos/ui/ItemsWithHeadingFlow;, "Lcom/google/android/videos/ui/ItemsWithHeadingFlow<TV;TD;>;"
    iget-object v0, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->sectionHeadingBinder:Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;

    if-eqz v0, :cond_0

    add-int/lit8 p1, p1, -0x1

    .end local p1    # "flowPosition":I
    :cond_0
    return p1
.end method

.method private isSectionHeadingPosition(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 304
    .local p0, "this":Lcom/google/android/videos/ui/ItemsWithHeadingFlow;, "Lcom/google/android/videos/ui/ItemsWithHeadingFlow<TV;TD;>;"
    iget-object v0, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->sectionHeadingBinder:Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public bindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 8
    .param p1, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .local p0, "this":Lcom/google/android/videos/ui/ItemsWithHeadingFlow;, "Lcom/google/android/videos/ui/ItemsWithHeadingFlow<TV;TD;>;"
    const/4 v4, 0x0

    .line 334
    invoke-direct {p0, p2}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->isSectionHeadingPosition(I)Z

    move-result v5

    if-eqz v5, :cond_1

    move-object v3, p1

    .line 335
    check-cast v3, Lcom/google/android/videos/ui/SectionHeadingHelper;

    .line 336
    .local v3, "sectionHeadingHelper":Lcom/google/android/videos/ui/SectionHeadingHelper;
    iget-object v5, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->sectionHeadingBinder:Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;

    iget v6, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->maxItems:I

    if-ltz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->dataSource:Lcom/google/android/videos/adapter/DataSource;

    invoke-interface {v6}, Lcom/google/android/videos/adapter/DataSource;->getCount()I

    move-result v6

    iget v7, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->maxItems:I

    sub-int/2addr v6, v7

    invoke-static {v4, v6}, Ljava/lang/Math;->max(II)I

    move-result v4

    :cond_0
    invoke-interface {v5, v3, v4}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;->bindSectionHeading(Lcom/google/android/videos/ui/SectionHeadingHelper;I)V

    .line 346
    .end local v3    # "sectionHeadingHelper":Lcom/google/android/videos/ui/SectionHeadingHelper;
    :goto_0
    return-void

    .line 339
    :cond_1
    invoke-direct {p0, p2}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->getDataSourcePosition(I)I

    move-result v2

    .line 341
    .local v2, "dataSourcePosition":I
    iget-object v1, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    check-cast v1, Lcom/google/android/videos/ui/playnext/ClusterItemView;

    .line 342
    .local v1, "clusterItemView":Lcom/google/android/videos/ui/playnext/ClusterItemView;, "TV;"
    iget-object v4, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->clusterItemViewBinder:Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;

    iget-object v5, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->dataSource:Lcom/google/android/videos/adapter/DataSource;

    invoke-virtual {v4, v1, v5, v2}, Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;->bind(Lcom/google/android/videos/ui/playnext/ClusterItemView;Lcom/google/android/videos/adapter/DataSource;I)[Landroid/view/View;

    move-result-object v0

    .line 344
    .local v0, "clickableViews":[Landroid/view/View;
    invoke-direct {p0, v0, v2}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->configureListeners([Landroid/view/View;I)V

    goto :goto_0
.end method

.method public getCount()I
    .locals 3

    .prologue
    .local p0, "this":Lcom/google/android/videos/ui/ItemsWithHeadingFlow;, "Lcom/google/android/videos/ui/ItemsWithHeadingFlow<TV;TD;>;"
    const/4 v1, 0x0

    .line 293
    iget-object v2, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->dataSource:Lcom/google/android/videos/adapter/DataSource;

    if-nez v2, :cond_1

    move v0, v1

    .line 294
    .local v0, "dataCount":I
    :goto_0
    iget v2, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->maxItems:I

    if-ltz v2, :cond_0

    iget v2, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->maxItems:I

    if-le v0, v2, :cond_0

    .line 295
    iget v0, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->maxItems:I

    .line 297
    :cond_0
    if-nez v0, :cond_2

    .line 300
    :goto_1
    return v1

    .line 293
    .end local v0    # "dataCount":I
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->dataSource:Lcom/google/android/videos/adapter/DataSource;

    invoke-interface {v2}, Lcom/google/android/videos/adapter/DataSource;->getCount()I

    move-result v0

    goto :goto_0

    .line 300
    .restart local v0    # "dataCount":I
    :cond_2
    iget-object v2, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->sectionHeadingBinder:Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;

    if-nez v2, :cond_3

    :goto_2
    add-int/2addr v1, v0

    goto :goto_1

    :cond_3
    const/4 v1, 0x1

    goto :goto_2
.end method

.method public getItemIdentifier(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 325
    .local p0, "this":Lcom/google/android/videos/ui/ItemsWithHeadingFlow;, "Lcom/google/android/videos/ui/ItemsWithHeadingFlow<TV;TD;>;"
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->isSectionHeadingPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->headingIdentifier:Ljava/lang/Object;

    .line 328
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->dataSource:Lcom/google/android/videos/adapter/DataSource;

    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->getDataSourcePosition(I)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/videos/adapter/DataSource;->getItemIdentifier(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 313
    .local p0, "this":Lcom/google/android/videos/ui/ItemsWithHeadingFlow;, "Lcom/google/android/videos/ui/ItemsWithHeadingFlow<TV;TD;>;"
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->isSectionHeadingPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0400be

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->layoutId:I

    goto :goto_0
.end method

.method public notifyItemStatesChanged()V
    .locals 2

    .prologue
    .line 379
    .local p0, "this":Lcom/google/android/videos/ui/ItemsWithHeadingFlow;, "Lcom/google/android/videos/ui/ItemsWithHeadingFlow<TV;TD;>;"
    invoke-virtual {p0}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->getCount()I

    move-result v0

    .line 380
    .local v0, "count":I
    if-lez v0, :cond_0

    .line 381
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->notifyItemsChanged(II)V

    .line 383
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 361
    .local p0, "this":Lcom/google/android/videos/ui/ItemsWithHeadingFlow;, "Lcom/google/android/videos/ui/ItemsWithHeadingFlow<TV;TD;>;"
    const v1, 0x7f0f0040

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 364
    .local v0, "dataSourcePosition":I
    iget-object v1, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->dataSource:Lcom/google/android/videos/adapter/DataSource;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->dataSource:Lcom/google/android/videos/adapter/DataSource;

    invoke-interface {v1}, Lcom/google/android/videos/adapter/DataSource;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 365
    iget-object v1, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->clickListener:Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;

    iget-object v2, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->dataSource:Lcom/google/android/videos/adapter/DataSource;

    invoke-interface {v1, v2, v0, p1}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;->onItemClick(Lcom/google/android/videos/adapter/DataSource;ILandroid/view/View;)V

    .line 372
    :goto_0
    return-void

    .line 367
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Clicked on an item that\'s outside the data source bounds. dataSource:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->dataSource:Lcom/google/android/videos/adapter/DataSource;

    if-nez v1, :cond_1

    const-string v1, "null"

    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", count:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->dataSource:Lcom/google/android/videos/adapter/DataSource;

    if-nez v1, :cond_2

    const/4 v1, 0x0

    :goto_2
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", position: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->dataSource:Lcom/google/android/videos/adapter/DataSource;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->dataSource:Lcom/google/android/videos/adapter/DataSource;

    invoke-interface {v1}, Lcom/google/android/videos/adapter/DataSource;->getCount()I

    move-result v1

    goto :goto_2
.end method

.method onDataSourceChanged()V
    .locals 0

    .prologue
    .line 288
    .local p0, "this":Lcom/google/android/videos/ui/ItemsWithHeadingFlow;, "Lcom/google/android/videos/ui/ItemsWithHeadingFlow<TV;TD;>;"
    invoke-virtual {p0}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->notifyDataSetChanged()V

    .line 289
    return-void
.end method

.method protected onInitViewTypes(Landroid/util/SparseArray;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/videos/flow/ViewHolderCreator",
            "<*>;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 319
    .local p0, "this":Lcom/google/android/videos/ui/ItemsWithHeadingFlow;, "Lcom/google/android/videos/ui/ItemsWithHeadingFlow<TV;TD;>;"
    .local p1, "viewTypes":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/google/android/videos/flow/ViewHolderCreator<*>;>;"
    const v0, 0x7f0400be

    invoke-static {p1, v0}, Lcom/google/android/videos/flow/SectionHeadingViewHolderCreator;->addOrVerify(Landroid/util/SparseArray;I)V

    .line 320
    iget v0, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->layoutId:I

    iget v1, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->layoutId:I

    invoke-static {p1, v0, v1}, Lcom/google/android/videos/flow/BasicViewHolderCreator;->addOrVerify(Landroid/util/SparseArray;II)V

    .line 321
    return-void
.end method

.method public setDataSource(Lcom/google/android/videos/adapter/DataSource;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/videos/ui/ItemsWithHeadingFlow;, "Lcom/google/android/videos/ui/ItemsWithHeadingFlow<TV;TD;>;"
    .local p1, "dataSource":Lcom/google/android/videos/adapter/DataSource;, "TD;"
    const/4 v4, 0x0

    .line 260
    iget-object v2, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->dataSource:Lcom/google/android/videos/adapter/DataSource;

    if-eq v2, p1, :cond_2

    .line 261
    iget-object v2, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->dataSource:Lcom/google/android/videos/adapter/DataSource;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->dataSourceObserver:Landroid/database/DataSetObserver;

    if-eqz v2, :cond_0

    .line 262
    iget-object v2, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->dataSource:Lcom/google/android/videos/adapter/DataSource;

    iget-object v3, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->dataSourceObserver:Landroid/database/DataSetObserver;

    invoke-interface {v2, v3}, Lcom/google/android/videos/adapter/DataSource;->unregisterObserver(Landroid/database/DataSetObserver;)V

    .line 265
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->getCount()I

    move-result v1

    .line 266
    .local v1, "oldCount":I
    iput-object p1, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->dataSource:Lcom/google/android/videos/adapter/DataSource;

    .line 267
    invoke-virtual {p0}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->getCount()I

    move-result v0

    .line 269
    .local v0, "newCount":I
    if-eqz p1, :cond_1

    iget-object v2, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->dataSourceObserver:Landroid/database/DataSetObserver;

    if-eqz v2, :cond_1

    .line 270
    iget-object v2, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->dataSourceObserver:Landroid/database/DataSetObserver;

    invoke-interface {p1, v2}, Lcom/google/android/videos/adapter/DataSource;->registerObserver(Landroid/database/DataSetObserver;)V

    .line 273
    :cond_1
    if-lez v1, :cond_3

    if-nez v0, :cond_3

    .line 274
    invoke-virtual {p0, v4, v1}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->notifyItemsRemoved(II)V

    .line 281
    .end local v0    # "newCount":I
    .end local v1    # "oldCount":I
    :cond_2
    :goto_0
    return-void

    .line 275
    .restart local v0    # "newCount":I
    .restart local v1    # "oldCount":I
    :cond_3
    if-nez v1, :cond_4

    if-lez v0, :cond_4

    .line 276
    invoke-virtual {p0, v4, v0}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->notifyItemsInserted(II)V

    goto :goto_0

    .line 278
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public setMaxItems(I)V
    .locals 3
    .param p1, "maxItems"    # I

    .prologue
    .line 240
    .local p0, "this":Lcom/google/android/videos/ui/ItemsWithHeadingFlow;, "Lcom/google/android/videos/ui/ItemsWithHeadingFlow<TV;TD;>;"
    iget v2, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->maxItems:I

    if-eq v2, p1, :cond_0

    .line 241
    invoke-virtual {p0}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->getCount()I

    move-result v1

    .line 242
    .local v1, "oldCount":I
    iput p1, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->maxItems:I

    .line 243
    invoke-virtual {p0}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->getCount()I

    move-result v0

    .line 245
    .local v0, "newCount":I
    if-le v1, v0, :cond_1

    .line 246
    sub-int v2, v1, v0

    invoke-virtual {p0, v0, v2}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->notifyItemsRemoved(II)V

    .line 251
    .end local v0    # "newCount":I
    .end local v1    # "oldCount":I
    :cond_0
    :goto_0
    return-void

    .line 247
    .restart local v0    # "newCount":I
    .restart local v1    # "oldCount":I
    :cond_1
    if-ge v1, v0, :cond_0

    .line 248
    sub-int v2, v0, v1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->notifyItemsInserted(II)V

    goto :goto_0
.end method
