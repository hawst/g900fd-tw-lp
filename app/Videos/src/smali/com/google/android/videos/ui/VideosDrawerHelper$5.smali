.class Lcom/google/android/videos/ui/VideosDrawerHelper$5;
.super Ljava/lang/Object;
.source "VideosDrawerHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/ui/VideosDrawerHelper;->createVerticalItem(Landroid/app/Activity;IIIZ)Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/ui/VideosDrawerHelper;

.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$vertical:I


# direct methods
.method constructor <init>(Lcom/google/android/videos/ui/VideosDrawerHelper;Landroid/app/Activity;I)V
    .locals 0

    .prologue
    .line 201
    iput-object p1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper$5;->this$0:Lcom/google/android/videos/ui/VideosDrawerHelper;

    iput-object p2, p0, Lcom/google/android/videos/ui/VideosDrawerHelper$5;->val$activity:Landroid/app/Activity;

    iput p3, p0, Lcom/google/android/videos/ui/VideosDrawerHelper$5;->val$vertical:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper$5;->val$activity:Landroid/app/Activity;

    instance-of v0, v0, Lcom/google/android/videos/activity/HomeActivity;

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper$5;->val$activity:Landroid/app/Activity;

    check-cast v0, Lcom/google/android/videos/activity/HomeActivity;

    iget v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper$5;->val$vertical:I

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/HomeActivity;->onVerticalSelected(I)V

    .line 210
    :goto_0
    return-void

    .line 207
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper$5;->val$activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper$5;->val$activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/videos/ui/VideosDrawerHelper$5;->this$0:Lcom/google/android/videos/ui/VideosDrawerHelper;

    # getter for: Lcom/google/android/videos/ui/VideosDrawerHelper;->account:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/videos/ui/VideosDrawerHelper;->access$100(Lcom/google/android/videos/ui/VideosDrawerHelper;)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/google/android/videos/ui/VideosDrawerHelper$5;->val$vertical:I

    invoke-static {v3}, Lcom/google/android/videos/ui/VerticalsHelper;->getVerticalExternalName(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/videos/activity/HomeActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
