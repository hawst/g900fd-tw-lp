.class public Lcom/google/android/videos/ui/WrapContentViewPager;
.super Landroid/support/v4/view/ViewPager;
.source "WrapContentViewPager.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 10
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    .line 25
    invoke-super {p0, p1, p2}, Landroid/support/v4/view/ViewPager;->onMeasure(II)V

    .line 26
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v7

    const/high16 v8, -0x80000000

    if-eq v7, v8, :cond_0

    .line 55
    :goto_0
    return-void

    .line 30
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/ui/WrapContentViewPager;->getMeasuredWidth()I

    move-result v7

    invoke-static {v7, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 31
    invoke-virtual {p0}, Lcom/google/android/videos/ui/WrapContentViewPager;->getPaddingTop()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/videos/ui/WrapContentViewPager;->getPaddingBottom()I

    move-result v8

    add-int v5, v7, v8

    .line 32
    .local v5, "padding":I
    const/4 v1, 0x0

    .line 34
    .local v1, "contentHeight":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/videos/ui/WrapContentViewPager;->getChildCount()I

    move-result v7

    if-ge v4, v7, :cond_4

    .line 35
    invoke-virtual {p0, v4}, Lcom/google/android/videos/ui/WrapContentViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 36
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    .line 37
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/support/v4/view/ViewPager$LayoutParams;

    .line 38
    .local v6, "params":Landroid/support/v4/view/ViewPager$LayoutParams;
    iget-boolean v7, v6, Landroid/support/v4/view/ViewPager$LayoutParams;->isDecor:Z

    if-eqz v7, :cond_3

    .line 39
    iget v7, v6, Landroid/support/v4/view/ViewPager$LayoutParams;->gravity:I

    and-int/lit8 v3, v7, 0x70

    .line 40
    .local v3, "gravity":I
    const/16 v7, 0x30

    if-eq v3, v7, :cond_1

    const/16 v7, 0x50

    if-ne v3, v7, :cond_2

    .line 42
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v5, v7

    .line 34
    .end local v3    # "gravity":I
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 45
    .restart local v3    # "gravity":I
    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    invoke-static {v1, v7}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_2

    .line 48
    .end local v3    # "gravity":I
    :cond_3
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    invoke-static {v1, v7}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_2

    .line 51
    .end local v0    # "child":Landroid/view/View;
    .end local v6    # "params":Landroid/support/v4/view/ViewPager$LayoutParams;
    :cond_4
    add-int v7, v1, v5

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 53
    .local v2, "desiredHeight":I
    invoke-static {v2, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 54
    invoke-super {p0, p1, p2}, Landroid/support/v4/view/ViewPager;->onMeasure(II)V

    goto :goto_0
.end method
