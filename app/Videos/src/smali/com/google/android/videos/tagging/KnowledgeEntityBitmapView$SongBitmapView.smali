.class Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$SongBitmapView;
.super Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;
.source "KnowledgeEntityBitmapView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SongBitmapView"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView",
        "<",
        "Landroid/widget/ImageView;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/videos/tagging/KnowledgeEntity$Song;Landroid/widget/ImageView;I)V
    .locals 6
    .param p1, "entity"    # Lcom/google/android/videos/tagging/KnowledgeEntity$Song;
    .param p2, "imageView"    # Landroid/widget/ImageView;
    .param p3, "imageDimension"    # I

    .prologue
    const/4 v1, 0x0

    .line 111
    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, v1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;-><init>(Lcom/google/android/play/image/AvatarCropTransformation;Lcom/google/android/videos/tagging/KnowledgeEntity;Landroid/view/View;ILcom/google/android/videos/tagging/KnowledgeEntityBitmapView$1;)V

    .line 112
    return-void
.end method


# virtual methods
.method postProcess(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "response"    # Landroid/graphics/Bitmap;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$SongBitmapView;->entity:Lcom/google/android/videos/tagging/KnowledgeEntity;

    iget-object v1, v0, Lcom/google/android/videos/tagging/KnowledgeEntity;->image:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$SongBitmapView;->view:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    iget v2, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$SongBitmapView;->imageDimension:I

    invoke-static {v1, v0, v2}, Lcom/google/android/videos/tagging/Cards;->setImageMatrixIfSquareCropExists(Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Landroid/widget/ImageView;I)V

    .line 117
    return-object p1
.end method

.method public bridge synthetic setThumbnail(Ljava/lang/Object;Z)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Z

    .prologue
    .line 108
    check-cast p1, Landroid/graphics/Bitmap;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-super {p0, p1, p2}, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;->setThumbnail(Landroid/graphics/Bitmap;Z)V

    return-void
.end method

.method updateImage(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "response"    # Landroid/graphics/Bitmap;

    .prologue
    .line 122
    if-eqz p1, :cond_0

    .line 123
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$SongBitmapView;->view:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 127
    :goto_0
    return-void

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$SongBitmapView;->view:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/google/android/videos/tagging/Cards;->setDefaultSongImage(Landroid/widget/ImageView;)V

    goto :goto_0
.end method
