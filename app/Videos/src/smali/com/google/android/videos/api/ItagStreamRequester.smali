.class final Lcom/google/android/videos/api/ItagStreamRequester;
.super Ljava/lang/Object;
.source "ItagStreamRequester.java"

# interfaces
.implements Lcom/google/android/videos/async/Requester;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/api/ItagStreamRequester$1;,
        Lcom/google/android/videos/api/ItagStreamRequester$ItagInfoStoreRefreshCallback;,
        Lcom/google/android/videos/api/ItagStreamRequester$MpdCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Requester",
        "<",
        "Lcom/google/android/videos/api/MpdGetRequest;",
        "Lcom/google/android/videos/streams/Streams;",
        ">;"
    }
.end annotation


# static fields
.field private static final supportedSubtitleFormats:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final appendDoNotCountParam:Z

.field private final itagInfoStore:Lcom/google/android/videos/store/ItagInfoStore;

.field private final mpdRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/MpdGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 40
    new-array v0, v4, [Ljava/lang/Integer;

    const/4 v1, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/videos/utils/CollectionUtil;->immutableList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/android/videos/api/ItagStreamRequester;->supportedSubtitleFormats:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/videos/store/ItagInfoStore;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/Config;)V
    .locals 1
    .param p1, "itagInfoStore"    # Lcom/google/android/videos/store/ItagInfoStore;
    .param p3, "config"    # Lcom/google/android/videos/Config;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/store/ItagInfoStore;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/MpdGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;",
            ">;",
            "Lcom/google/android/videos/Config;",
            ")V"
        }
    .end annotation

    .prologue
    .line 47
    .local p2, "mpdRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/ItagInfoStore;

    iput-object v0, p0, Lcom/google/android/videos/api/ItagStreamRequester;->itagInfoStore:Lcom/google/android/videos/store/ItagInfoStore;

    .line 49
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/api/ItagStreamRequester;->mpdRequester:Lcom/google/android/videos/async/Requester;

    .line 50
    invoke-interface {p3}, Lcom/google/android/videos/Config;->appendDoNotCountParam()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/videos/api/ItagStreamRequester;->appendDoNotCountParam:Z

    .line 51
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/videos/api/ItagStreamRequester;)Lcom/google/android/videos/store/ItagInfoStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/api/ItagStreamRequester;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/videos/api/ItagStreamRequester;->itagInfoStore:Lcom/google/android/videos/store/ItagInfoStore;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/api/ItagStreamRequester;Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;)Lcom/google/android/videos/streams/Streams;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/api/ItagStreamRequester;
    .param p1, "x1"    # Lcom/google/android/videos/api/MpdGetRequest;
    .param p2, "x2"    # Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/api/ItagStreamRequester;->convertToStreams(Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;)Lcom/google/android/videos/streams/Streams;

    move-result-object v0

    return-object v0
.end method

.method private convertToStreams(Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;)Lcom/google/android/videos/streams/Streams;
    .locals 20
    .param p1, "request"    # Lcom/google/android/videos/api/MpdGetRequest;
    .param p2, "response"    # Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;

    .prologue
    .line 59
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 60
    .local v10, "streams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    move-object/from16 v0, p2

    iget-object v15, v0, Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;->resource:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;

    iget-object v2, v15, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->representations:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

    .local v2, "arr$":[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;
    array-length v7, v2

    .local v7, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v7, :cond_8

    aget-object v8, v2, v4

    .line 61
    .local v8, "r":Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/api/ItagStreamRequester;->itagInfoStore:Lcom/google/android/videos/store/ItagInfoStore;

    iget v0, v8, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->format:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Lcom/google/android/videos/store/ItagInfoStore;->getItagInfo(I)Lcom/google/android/videos/streams/ItagInfo;

    move-result-object v5

    .line 62
    .local v5, "itagInfo":Lcom/google/android/videos/streams/ItagInfo;
    if-eqz v5, :cond_7

    .line 63
    new-instance v14, Lcom/google/android/videos/utils/UriBuilder;

    iget-object v15, v8, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->playbackUrl:Ljava/lang/String;

    invoke-direct {v14, v15}, Lcom/google/android/videos/utils/UriBuilder;-><init>(Ljava/lang/String;)V

    .line 64
    .local v14, "uriBuilder":Lcom/google/android/videos/utils/UriBuilder;
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/google/android/videos/api/ItagStreamRequester;->appendDoNotCountParam:Z

    if-eqz v15, :cond_0

    .line 68
    const-string v15, "dnc"

    const-string v16, "1"

    invoke-virtual/range {v14 .. v16}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 70
    :cond_0
    iget-boolean v15, v5, Lcom/google/android/videos/streams/ItagInfo;->isDash:Z

    if-eqz v15, :cond_1

    .line 74
    const-string v15, "keepalive"

    const-string v16, "yes"

    invoke-virtual/range {v14 .. v16}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 82
    :goto_1
    invoke-virtual {v14}, Lcom/google/android/videos/utils/UriBuilder;->build()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    .line 84
    .local v13, "uri":Landroid/net/Uri;
    new-instance v9, Lcom/google/android/videos/proto/StreamInfo;

    invoke-direct {v9}, Lcom/google/android/videos/proto/StreamInfo;-><init>()V

    .line 85
    .local v9, "streamInfo":Lcom/google/android/videos/proto/StreamInfo;
    iget v15, v8, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->format:I

    iput v15, v9, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    .line 86
    iget-wide v0, v8, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->fileSize:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    iput-wide v0, v9, Lcom/google/android/videos/proto/StreamInfo;->sizeInBytes:J

    .line 87
    iget-wide v0, v8, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->timestampMillis:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    iput-wide v0, v9, Lcom/google/android/videos/proto/StreamInfo;->lastModifiedTimestamp:J

    .line 88
    iget-object v15, v8, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->index:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    if-eqz v15, :cond_3

    iget-object v15, v8, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->index:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    iget-wide v0, v15, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;->first:J

    move-wide/from16 v16, v0

    :goto_2
    move-wide/from16 v0, v16

    iput-wide v0, v9, Lcom/google/android/videos/proto/StreamInfo;->dashIndexStart:J

    .line 89
    iget-object v15, v8, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->index:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    if-eqz v15, :cond_4

    iget-object v15, v8, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->index:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    iget-wide v0, v15, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;->last:J

    move-wide/from16 v16, v0

    :goto_3
    move-wide/from16 v0, v16

    iput-wide v0, v9, Lcom/google/android/videos/proto/StreamInfo;->dashIndexEnd:J

    .line 90
    iget-object v15, v8, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->init:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    if-eqz v15, :cond_5

    iget-object v15, v8, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->init:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    iget-wide v0, v15, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;->first:J

    move-wide/from16 v16, v0

    :goto_4
    move-wide/from16 v0, v16

    iput-wide v0, v9, Lcom/google/android/videos/proto/StreamInfo;->dashInitStart:J

    .line 91
    iget-object v15, v8, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->init:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    if-eqz v15, :cond_6

    iget-object v15, v8, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->init:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    iget-wide v0, v15, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;->last:J

    move-wide/from16 v16, v0

    :goto_5
    move-wide/from16 v0, v16

    iput-wide v0, v9, Lcom/google/android/videos/proto/StreamInfo;->dashInitEnd:J

    .line 92
    iget-object v15, v8, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->codec:Ljava/lang/String;

    iput-object v15, v9, Lcom/google/android/videos/proto/StreamInfo;->codec:Ljava/lang/String;

    .line 93
    iget-object v15, v8, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    iput-object v15, v9, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    .line 94
    new-instance v15, Lcom/google/android/videos/streams/MediaStream;

    invoke-direct {v15, v13, v5, v9}, Lcom/google/android/videos/streams/MediaStream;-><init>(Landroid/net/Uri;Lcom/google/android/videos/streams/ItagInfo;Lcom/google/android/videos/proto/StreamInfo;)V

    invoke-interface {v10, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    .end local v9    # "streamInfo":Lcom/google/android/videos/proto/StreamInfo;
    .end local v13    # "uri":Landroid/net/Uri;
    .end local v14    # "uriBuilder":Lcom/google/android/videos/utils/UriBuilder;
    :goto_6
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 75
    .restart local v14    # "uriBuilder":Lcom/google/android/videos/utils/UriBuilder;
    :cond_1
    iget-boolean v15, v5, Lcom/google/android/videos/streams/ItagInfo;->isMulti:Z

    if-eqz v15, :cond_2

    .line 77
    const-string v15, "widevine"

    invoke-virtual {v14, v15}, Lcom/google/android/videos/utils/UriBuilder;->scheme(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    goto :goto_1

    .line 80
    :cond_2
    const-string v15, "http"

    invoke-virtual {v14, v15}, Lcom/google/android/videos/utils/UriBuilder;->scheme(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    goto :goto_1

    .line 88
    .restart local v9    # "streamInfo":Lcom/google/android/videos/proto/StreamInfo;
    .restart local v13    # "uri":Landroid/net/Uri;
    :cond_3
    const-wide/16 v16, 0x0

    goto :goto_2

    .line 89
    :cond_4
    const-wide/16 v16, 0x0

    goto :goto_3

    .line 90
    :cond_5
    const-wide/16 v16, 0x0

    goto :goto_4

    .line 91
    :cond_6
    const-wide/16 v16, 0x0

    goto :goto_5

    .line 96
    .end local v9    # "streamInfo":Lcom/google/android/videos/proto/StreamInfo;
    .end local v13    # "uri":Landroid/net/Uri;
    .end local v14    # "uriBuilder":Lcom/google/android/videos/utils/UriBuilder;
    :cond_7
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Discarding stream with unrecognized itag: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget v0, v8, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->format:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    goto :goto_6

    .line 102
    .end local v5    # "itagInfo":Lcom/google/android/videos/streams/ItagInfo;
    .end local v8    # "r":Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;
    :cond_8
    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    .line 103
    .local v12, "tracks":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/videos/subtitles/SubtitleTrack;>;"
    move-object/from16 v0, p2

    iget-object v15, v0, Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;->resource:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;

    iget-object v2, v15, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->captions:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    .local v2, "arr$":[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;
    array-length v7, v2

    const/4 v4, 0x0

    :goto_7
    if-ge v4, v7, :cond_c

    aget-object v3, v2, v4

    .line 105
    .local v3, "c":Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;
    sget-object v15, Lcom/google/android/videos/api/ItagStreamRequester;->supportedSubtitleFormats:Ljava/util/List;

    iget-wide v0, v3, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->format:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    long-to-int v0, v0

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_a

    .line 103
    :cond_9
    :goto_8
    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    .line 108
    :cond_a
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v3, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->lang:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget-boolean v0, v3, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->forced:Z

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    iget-object v0, v3, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->name:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 109
    .local v6, "key":Ljava/lang/String;
    invoke-interface {v12, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/videos/subtitles/SubtitleTrack;

    .line 110
    .local v11, "track":Lcom/google/android/videos/subtitles/SubtitleTrack;
    if-eqz v11, :cond_b

    iget v15, v11, Lcom/google/android/videos/subtitles/SubtitleTrack;->format:I

    int-to-long v0, v15

    move-wide/from16 v16, v0

    iget-wide v0, v3, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->format:J

    move-wide/from16 v18, v0

    cmp-long v15, v16, v18

    if-gez v15, :cond_9

    .line 111
    :cond_b
    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/google/android/videos/api/MpdGetRequest;->videoId:Ljava/lang/String;

    invoke-static {v15, v3}, Lcom/google/android/videos/subtitles/SubtitleTrack;->create(Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;)Lcom/google/android/videos/subtitles/SubtitleTrack;

    move-result-object v15

    invoke-interface {v12, v6, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_8

    .line 115
    .end local v3    # "c":Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;
    .end local v6    # "key":Ljava/lang/String;
    .end local v11    # "track":Lcom/google/android/videos/subtitles/SubtitleTrack;
    :cond_c
    new-instance v15, Lcom/google/android/videos/streams/Streams;

    invoke-interface {v12}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/google/android/videos/utils/CollectionUtil;->immutableCopyOf(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v16

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;->resource:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->storyboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v15, v10, v0, v1}, Lcom/google/android/videos/streams/Streams;-><init>(Ljava/util/List;Ljava/util/List;[Lcom/google/wireless/android/video/magma/proto/Storyboard;)V

    return-object v15
.end method


# virtual methods
.method public request(Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/android/videos/async/Callback;)V
    .locals 3
    .param p1, "request"    # Lcom/google/android/videos/api/MpdGetRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/api/MpdGetRequest;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/api/MpdGetRequest;",
            "Lcom/google/android/videos/streams/Streams;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/android/videos/streams/Streams;>;"
    iget-object v0, p0, Lcom/google/android/videos/api/ItagStreamRequester;->mpdRequester:Lcom/google/android/videos/async/Requester;

    new-instance v1, Lcom/google/android/videos/api/ItagStreamRequester$MpdCallback;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p2, v2}, Lcom/google/android/videos/api/ItagStreamRequester$MpdCallback;-><init>(Lcom/google/android/videos/api/ItagStreamRequester;Lcom/google/android/videos/async/Callback;Lcom/google/android/videos/api/ItagStreamRequester$1;)V

    invoke-interface {v0, p1, v1}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 56
    return-void
.end method

.method public bridge synthetic request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/google/android/videos/async/Callback;

    .prologue
    .line 38
    check-cast p1, Lcom/google/android/videos/api/MpdGetRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/api/ItagStreamRequester;->request(Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/android/videos/async/Callback;)V

    return-void
.end method
