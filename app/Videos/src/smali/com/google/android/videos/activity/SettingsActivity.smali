.class public Lcom/google/android/videos/activity/SettingsActivity;
.super Lcom/google/android/videos/activity/VideosPreferenceActivity;
.source "SettingsActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/activity/SettingsActivity$SyncUserConfigurationCallback;
    }
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private audioLanguagePreference:Landroid/preference/ListPreference;

.field private captionsOverridePreference:Landroid/preference/Preference;

.field private config:Lcom/google/android/videos/Config;

.field private configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

.field private dmaCategory:Landroid/preference/Preference;

.field private dmaUnlinkAccountPreference:Landroid/preference/Preference;

.field private downloadNetworkPreference:Landroid/preference/ListPreference;

.field private downloadQualityPreference:Landroid/preference/ListPreference;

.field private eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private inProgressDialog:Landroid/app/Dialog;

.field private itagInfoStore:Lcom/google/android/videos/store/ItagInfoStore;

.field private preferenceScreen:Landroid/preference/PreferenceScreen;

.field private preferences:Landroid/content/SharedPreferences;

.field private signInManager:Lcom/google/android/videos/accounts/SignInManager;

.field private unlinkAccountCallback:Lcom/google/android/videos/async/CancelableCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/CancelableCallback",
            "<",
            "Lcom/google/android/videos/api/LinkedAccountRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private versionPreference:Landroid/preference/Preference;

.field private videosGlobals:Lcom/google/android/videos/VideosGlobals;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/videos/activity/VideosPreferenceActivity;-><init>()V

    .line 370
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/videos/activity/SettingsActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/activity/SettingsActivity;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->account:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/activity/SettingsActivity;)Lcom/google/android/videos/store/ConfigurationStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/activity/SettingsActivity;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/activity/SettingsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/activity/SettingsActivity;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/videos/activity/SettingsActivity;->dismissInProgressDialog()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/videos/activity/SettingsActivity;)Lcom/google/android/videos/VideosGlobals;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/activity/SettingsActivity;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/videos/activity/SettingsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/activity/SettingsActivity;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/videos/activity/SettingsActivity;->onUserConfigurationChanged()V

    return-void
.end method

.method private configureDownloadStorageEntry()Z
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 229
    iget-object v6, p0, Lcom/google/android/videos/activity/SettingsActivity;->preferenceScreen:Landroid/preference/PreferenceScreen;

    const-string v7, "download_storage"

    invoke-virtual {v6, v7}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/ListPreference;

    .line 231
    .local v3, "storagePreference":Landroid/preference/ListPreference;
    invoke-static {p0}, Lcom/google/android/videos/utils/OfflineUtil;->getSupportedRootFilesDir(Landroid/content/Context;)[Ljava/io/File;

    move-result-object v2

    .line 232
    .local v2, "storageOptions":[Ljava/io/File;
    iget-object v6, p0, Lcom/google/android/videos/activity/SettingsActivity;->config:Lcom/google/android/videos/Config;

    invoke-interface {v6}, Lcom/google/android/videos/Config;->allowDownloads()Z

    move-result v6

    if-eqz v6, :cond_0

    array-length v6, v2

    if-gt v6, v4, :cond_1

    .line 233
    :cond_0
    iget-object v4, p0, Lcom/google/android/videos/activity/SettingsActivity;->preferenceScreen:Landroid/preference/PreferenceScreen;

    iget-object v6, p0, Lcom/google/android/videos/activity/SettingsActivity;->preferenceScreen:Landroid/preference/PreferenceScreen;

    const-string v7, "download_storage"

    invoke-virtual {v6, v7}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    move v4, v5

    .line 248
    :goto_0
    return v4

    .line 236
    :cond_1
    array-length v6, v2

    if-le v6, v8, :cond_2

    .line 237
    const-string v6, "Skipped storage options: don\'t know how to map external storage device to the user."

    invoke-static {v6}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 239
    :cond_2
    new-array v0, v8, [Ljava/lang/CharSequence;

    const v6, 0x7f0b0206

    invoke-virtual {p0, v6}, Lcom/google/android/videos/activity/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v0, v5

    const v6, 0x7f0b0207

    invoke-virtual {p0, v6}, Lcom/google/android/videos/activity/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v0, v4

    .line 243
    .local v0, "entries":[Ljava/lang/CharSequence;
    new-array v1, v8, [Ljava/lang/CharSequence;

    const-string v6, "0"

    aput-object v6, v1, v5

    const-string v5, "1"

    aput-object v5, v1, v4

    .line 244
    .local v1, "entryValues":[Ljava/lang/CharSequence;
    invoke-virtual {v3, v0}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 245
    invoke-virtual {v3, v1}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 246
    iget-object v5, p0, Lcom/google/android/videos/activity/SettingsActivity;->preferences:Landroid/content/SharedPreferences;

    invoke-static {v5}, Lcom/google/android/videos/utils/SettingsUtil;->getPreferredStorageIndex(Landroid/content/SharedPreferences;)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/preference/ListPreference;->setValueIndex(I)V

    .line 247
    invoke-virtual {v3, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_0
.end method

.method public static createIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/videos/activity/SettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method private dismissInProgressDialog()V
    .locals 1

    .prologue
    .line 353
    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->inProgressDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->inProgressDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->inProgressDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 355
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->inProgressDialog:Landroid/app/Dialog;

    .line 357
    :cond_0
    return-void
.end method

.method private onUserConfigurationChanged()V
    .locals 4

    .prologue
    .line 252
    iget-object v1, p0, Lcom/google/android/videos/activity/SettingsActivity;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    iget-object v2, p0, Lcom/google/android/videos/activity/SettingsActivity;->account:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/store/ConfigurationStore;->isLinkedToPartner(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 253
    iget-object v1, p0, Lcom/google/android/videos/activity/SettingsActivity;->dmaCategory:Landroid/preference/Preference;

    if-eqz v1, :cond_0

    .line 254
    iget-object v1, p0, Lcom/google/android/videos/activity/SettingsActivity;->preferenceScreen:Landroid/preference/PreferenceScreen;

    iget-object v2, p0, Lcom/google/android/videos/activity/SettingsActivity;->dmaCategory:Landroid/preference/Preference;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 255
    iget-object v1, p0, Lcom/google/android/videos/activity/SettingsActivity;->preferenceScreen:Landroid/preference/PreferenceScreen;

    iget-object v2, p0, Lcom/google/android/videos/activity/SettingsActivity;->dmaUnlinkAccountPreference:Landroid/preference/Preference;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 256
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/videos/activity/SettingsActivity;->dmaCategory:Landroid/preference/Preference;

    .line 264
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/videos/activity/SettingsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getApplicationVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/activity/SettingsActivity;->config:Lcom/google/android/videos/Config;

    invoke-interface {v2}, Lcom/google/android/videos/Config;->deviceCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 266
    .local v0, "versionSummary":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/videos/activity/SettingsActivity;->account:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 267
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/activity/SettingsActivity;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    iget-object v3, p0, Lcom/google/android/videos/activity/SettingsActivity;->account:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/videos/store/ConfigurationStore;->getPlayCountry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 269
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/activity/SettingsActivity;->versionPreference:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 270
    return-void

    .line 258
    .end local v0    # "versionSummary":Ljava/lang/String;
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/activity/SettingsActivity;->dmaCategory:Landroid/preference/Preference;

    if-nez v1, :cond_0

    .line 259
    iget-object v1, p0, Lcom/google/android/videos/activity/SettingsActivity;->preferenceScreen:Landroid/preference/PreferenceScreen;

    const-string v2, "connected_accounts_category"

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/activity/SettingsActivity;->dmaCategory:Landroid/preference/Preference;

    .line 260
    iget-object v1, p0, Lcom/google/android/videos/activity/SettingsActivity;->preferenceScreen:Landroid/preference/PreferenceScreen;

    iget-object v2, p0, Lcom/google/android/videos/activity/SettingsActivity;->dmaCategory:Landroid/preference/Preference;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 261
    iget-object v1, p0, Lcom/google/android/videos/activity/SettingsActivity;->preferenceScreen:Landroid/preference/PreferenceScreen;

    iget-object v2, p0, Lcom/google/android/videos/activity/SettingsActivity;->dmaUnlinkAccountPreference:Landroid/preference/Preference;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method


# virtual methods
.method protected configureSettings(Lcom/google/android/videos/activity/VideosPreferenceFragment;)V
    .locals 22
    .param p1, "fragment"    # Lcom/google/android/videos/activity/VideosPreferenceFragment;

    .prologue
    .line 105
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v18

    const-string v19, "youtube"

    invoke-virtual/range {v18 .. v19}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    .line 106
    const v18, 0x7f080001

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->addPreferencesFromResource(I)V

    .line 108
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/videos/VideosGlobals;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lcom/google/android/videos/utils/NetworkStatus;->isMobileNetworkCapable()Z

    move-result v13

    .line 110
    .local v13, "isMobileNetworkCapable":Z
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/activity/SettingsActivity;->preferenceScreen:Landroid/preference/PreferenceScreen;

    .line 112
    const-string v18, "audio_language"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v18

    check-cast v18, Landroid/preference/ListPreference;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/activity/SettingsActivity;->audioLanguagePreference:Landroid/preference/ListPreference;

    .line 113
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->audioLanguagePreference:Landroid/preference/ListPreference;

    move-object/from16 v18, v0

    invoke-static/range {p0 .. p0}, Lcom/google/android/videos/utils/SettingsUtil;->getAllAudioPreferenceTexts(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 114
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->audioLanguagePreference:Landroid/preference/ListPreference;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->audioLanguagePreference:Landroid/preference/ListPreference;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 115
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->audioLanguagePreference:Landroid/preference/ListPreference;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 117
    sget v18, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v19, 0x15

    move/from16 v0, v18

    move/from16 v1, v19

    if-lt v0, v1, :cond_4

    .line 118
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->preferenceScreen:Landroid/preference/PreferenceScreen;

    move-object/from16 v18, v0

    const-string v19, "captioning_settings"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 119
    const-string v18, "captioning_system_settings"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    .line 121
    .local v5, "captionsSystemPreference":Landroid/preference/Preference;
    const v18, 0x7f0b023c

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Landroid/preference/Preference;->setTitle(I)V

    .line 122
    const v18, 0x7f0b023d

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Landroid/preference/Preference;->setSummary(I)V

    .line 134
    .end local v5    # "captionsSystemPreference":Landroid/preference/Preference;
    :goto_0
    const-string v18, "warning_streaming_bandwidth"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    .line 135
    .local v4, "bandwithStreamingPref":Landroid/preference/Preference;
    const-string v18, "adaptive_disable_hd_on_mobile_network"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    .line 137
    .local v8, "disableHdPref":Landroid/preference/Preference;
    if-nez v13, :cond_7

    .line 138
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->preferenceScreen:Landroid/preference/PreferenceScreen;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 139
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->preferenceScreen:Landroid/preference/PreferenceScreen;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 140
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->preferenceScreen:Landroid/preference/PreferenceScreen;

    move-object/from16 v18, v0

    const-string v19, "mobile_network_streaming_category"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 158
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->config:Lcom/google/android/videos/Config;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/google/android/videos/Config;->allowDownloads()Z

    move-result v18

    if-eqz v18, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->itagInfoStore:Lcom/google/android/videos/store/ItagInfoStore;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/videos/store/ItagInfoStore;->getAllowedDownloadQualities()Ljava/util/Set;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/Set;->size()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-gt v0, v1, :cond_b

    :cond_0
    const/16 v16, 0x1

    .line 160
    .local v16, "removeDownloadQuality":Z
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->config:Lcom/google/android/videos/Config;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/google/android/videos/Config;->allowDownloads()Z

    move-result v18

    if-eqz v18, :cond_1

    if-nez v13, :cond_c

    :cond_1
    const/4 v15, 0x1

    .line 161
    .local v15, "removeDownloadNetwork":Z
    :goto_3
    invoke-direct/range {p0 .. p0}, Lcom/google/android/videos/activity/SettingsActivity;->configureDownloadStorageEntry()Z

    move-result v18

    if-nez v18, :cond_d

    const/16 v17, 0x1

    .line 162
    .local v17, "removeDownloadStorage":Z
    :goto_4
    if-eqz v16, :cond_e

    .line 163
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->preferenceScreen:Landroid/preference/PreferenceScreen;

    move-object/from16 v18, v0

    const-string v19, "download_quality"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 169
    :goto_5
    if-eqz v15, :cond_f

    .line 170
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->preferenceScreen:Landroid/preference/PreferenceScreen;

    move-object/from16 v18, v0

    const-string v19, "download_policy"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 176
    :goto_6
    if-eqz v15, :cond_2

    if-eqz v16, :cond_2

    if-eqz v17, :cond_2

    .line 177
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->preferenceScreen:Landroid/preference/PreferenceScreen;

    move-object/from16 v18, v0

    const-string v19, "download_category"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 180
    :cond_2
    const-string v18, "enable_surround_sound"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v11

    .line 182
    .local v11, "enableSurroundSoundPreference":Landroid/preference/Preference;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->config:Lcom/google/android/videos/Config;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/google/android/videos/Config;->allowSurroundSoundFormats()Z

    move-result v18

    if-nez v18, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->config:Lcom/google/android/videos/Config;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/google/android/videos/Config;->audioVirtualizerEnabled()Z

    move-result v18

    if-nez v18, :cond_10

    .line 183
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->preferenceScreen:Landroid/preference/PreferenceScreen;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->preferenceScreen:Landroid/preference/PreferenceScreen;

    move-object/from16 v18, v0

    const-string v19, "surround_sound_category"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 185
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->preferenceScreen:Landroid/preference/PreferenceScreen;

    move-object/from16 v18, v0

    const-string v19, "surround_sound_demo"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 198
    :cond_3
    :goto_7
    const-string v18, "enable_info_cards"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v10

    .line 199
    .local v10, "enableInfoCardsPreference":Landroid/preference/Preference;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/videos/VideosGlobals;->getKnowledgeClient()Lcom/google/android/videos/tagging/KnowledgeClient;

    move-result-object v18

    if-nez v18, :cond_12

    .line 200
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->preferenceScreen:Landroid/preference/PreferenceScreen;

    move-object/from16 v18, v0

    const-string v19, "general_category"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 201
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->preferenceScreen:Landroid/preference/PreferenceScreen;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 206
    :goto_8
    const-string v18, "gservices_id"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v12

    .line 207
    .local v12, "gServicesPreference":Landroid/preference/Preference;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->config:Lcom/google/android/videos/Config;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/google/android/videos/Config;->gservicesId()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 209
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v19, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    sget-object v19, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 211
    .local v7, "deviceSummary":Ljava/lang/String;
    const-string v18, "device_summary"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 213
    const-string v18, "open_source_licenses"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 215
    const-string v18, "dma_unlink_account"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/activity/SettingsActivity;->dmaUnlinkAccountPreference:Landroid/preference/Preference;

    .line 216
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->dmaUnlinkAccountPreference:Landroid/preference/Preference;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 217
    const-string v18, "connected_accounts_category"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/activity/SettingsActivity;->dmaCategory:Landroid/preference/Preference;

    .line 218
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->preferenceScreen:Landroid/preference/PreferenceScreen;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->dmaCategory:Landroid/preference/Preference;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 219
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->preferenceScreen:Landroid/preference/PreferenceScreen;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->dmaUnlinkAccountPreference:Landroid/preference/Preference;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 220
    const-string v18, "version"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/activity/SettingsActivity;->versionPreference:Landroid/preference/Preference;

    .line 221
    invoke-direct/range {p0 .. p0}, Lcom/google/android/videos/activity/SettingsActivity;->onUserConfigurationChanged()V

    .line 222
    return-void

    .line 123
    .end local v4    # "bandwithStreamingPref":Landroid/preference/Preference;
    .end local v7    # "deviceSummary":Ljava/lang/String;
    .end local v8    # "disableHdPref":Landroid/preference/Preference;
    .end local v10    # "enableInfoCardsPreference":Landroid/preference/Preference;
    .end local v11    # "enableSurroundSoundPreference":Landroid/preference/Preference;
    .end local v12    # "gServicesPreference":Landroid/preference/Preference;
    .end local v15    # "removeDownloadNetwork":Z
    .end local v16    # "removeDownloadQuality":Z
    .end local v17    # "removeDownloadStorage":Z
    :cond_4
    sget v18, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v19, 0x13

    move/from16 v0, v18

    move/from16 v1, v19

    if-lt v0, v1, :cond_6

    .line 124
    const-string v18, "captioning_settings"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/activity/SettingsActivity;->captionsOverridePreference:Landroid/preference/Preference;

    .line 125
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->captionsOverridePreference:Landroid/preference/Preference;

    move-object/from16 v18, v0

    const v19, 0x7f0b023b

    invoke-virtual/range {v18 .. v19}, Landroid/preference/Preference;->setTitle(I)V

    .line 126
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->captionsOverridePreference:Landroid/preference/Preference;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->preferences:Landroid/content/SharedPreferences;

    move-object/from16 v18, v0

    const-string v20, "captioning_enabled"

    const/16 v21, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v18

    if-eqz v18, :cond_5

    const v18, 0x7f0b0240

    :goto_9
    move-object/from16 v0, v19

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(I)V

    goto/16 :goto_0

    :cond_5
    const v18, 0x7f0b023f

    goto :goto_9

    .line 130
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->preferenceScreen:Landroid/preference/PreferenceScreen;

    move-object/from16 v18, v0

    const-string v19, "captioning_system_settings"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_0

    .line 143
    .restart local v4    # "bandwithStreamingPref":Landroid/preference/Preference;
    .restart local v8    # "disableHdPref":Landroid/preference/Preference;
    :cond_7
    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 145
    const-string v18, "window"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/SettingsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/view/WindowManager;

    invoke-interface/range {v18 .. v18}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v9

    .line 147
    .local v9, "display":Landroid/view/Display;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->config:Lcom/google/android/videos/Config;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/google/android/videos/Config;->useDashForStreaming()Z

    move-result v18

    if-eqz v18, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->config:Lcom/google/android/videos/Config;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/google/android/videos/Config;->supportsAdaptivePlayback()Z

    move-result v18

    if-eqz v18, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->config:Lcom/google/android/videos/Config;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v9}, Lcom/google/android/videos/Config;->videoHeightCap(Landroid/view/Display;)I

    move-result v18

    const/16 v19, 0x2d0

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_9

    :cond_8
    const/4 v14, 0x1

    .line 150
    .local v14, "removeAdaptiveDisableHdOption":Z
    :goto_a
    if-eqz v14, :cond_a

    .line 151
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->preferenceScreen:Landroid/preference/PreferenceScreen;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_1

    .line 147
    .end local v14    # "removeAdaptiveDisableHdOption":Z
    :cond_9
    const/4 v14, 0x0

    goto :goto_a

    .line 153
    .restart local v14    # "removeAdaptiveDisableHdOption":Z
    :cond_a
    move-object/from16 v0, p0

    invoke-virtual {v8, v0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto/16 :goto_1

    .line 158
    .end local v9    # "display":Landroid/view/Display;
    .end local v14    # "removeAdaptiveDisableHdOption":Z
    :cond_b
    const/16 v16, 0x0

    goto/16 :goto_2

    .line 160
    .restart local v16    # "removeDownloadQuality":Z
    :cond_c
    const/4 v15, 0x0

    goto/16 :goto_3

    .line 161
    .restart local v15    # "removeDownloadNetwork":Z
    :cond_d
    const/16 v17, 0x0

    goto/16 :goto_4

    .line 165
    .restart local v17    # "removeDownloadStorage":Z
    :cond_e
    const-string v18, "download_quality"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v18

    check-cast v18, Landroid/preference/ListPreference;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/activity/SettingsActivity;->downloadQualityPreference:Landroid/preference/ListPreference;

    .line 166
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->downloadQualityPreference:Landroid/preference/ListPreference;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->downloadQualityPreference:Landroid/preference/ListPreference;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 167
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->downloadQualityPreference:Landroid/preference/ListPreference;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto/16 :goto_5

    .line 172
    :cond_f
    const-string v18, "download_policy"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v18

    check-cast v18, Landroid/preference/ListPreference;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/activity/SettingsActivity;->downloadNetworkPreference:Landroid/preference/ListPreference;

    .line 173
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->downloadNetworkPreference:Landroid/preference/ListPreference;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->downloadNetworkPreference:Landroid/preference/ListPreference;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 174
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->downloadNetworkPreference:Landroid/preference/ListPreference;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto/16 :goto_6

    .line 187
    .restart local v11    # "enableSurroundSoundPreference":Landroid/preference/Preference;
    :cond_10
    const-string v18, "surround_sound_demo"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    .line 188
    .local v6, "demoPreference":Landroid/preference/Preference;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->config:Lcom/google/android/videos/Config;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/google/android/videos/Config;->soundWelcomeVideoId()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_11

    .line 189
    move-object/from16 v0, p0

    invoke-virtual {v6, v0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 193
    :goto_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->config:Lcom/google/android/videos/Config;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/google/android/videos/Config;->audioVirtualizerEnabled()Z

    move-result v18

    if-eqz v18, :cond_3

    .line 194
    const v18, 0x7f0b0217

    move/from16 v0, v18

    invoke-virtual {v11, v0}, Landroid/preference/Preference;->setSummary(I)V

    .line 195
    move-object/from16 v0, p0

    invoke-virtual {v11, v0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto/16 :goto_7

    .line 191
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/SettingsActivity;->preferenceScreen:Landroid/preference/PreferenceScreen;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_b

    .line 203
    .end local v6    # "demoPreference":Landroid/preference/Preference;
    .restart local v10    # "enableInfoCardsPreference":Landroid/preference/Preference;
    :cond_12
    move-object/from16 v0, p0

    invoke-virtual {v10, v0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto/16 :goto_8
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 364
    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->unlinkAccountCallback:Lcom/google/android/videos/async/CancelableCallback;

    if-eqz v0, :cond_0

    .line 365
    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->unlinkAccountCallback:Lcom/google/android/videos/async/CancelableCallback;

    invoke-virtual {v0}, Lcom/google/android/videos/async/CancelableCallback;->cancel()V

    .line 366
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->unlinkAccountCallback:Lcom/google/android/videos/async/CancelableCallback;

    .line 368
    :cond_0
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v3, 0x1

    .line 318
    new-instance v0, Landroid/app/Dialog;

    invoke-direct {v0, p0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->inProgressDialog:Landroid/app/Dialog;

    .line 319
    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->inProgressDialog:Landroid/app/Dialog;

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 320
    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->inProgressDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x7f0a00c7

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 321
    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->inProgressDialog:Landroid/app/Dialog;

    new-instance v1, Landroid/widget/ProgressBar;

    iget-object v2, p0, Lcom/google/android/videos/activity/SettingsActivity;->inProgressDialog:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 322
    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->inProgressDialog:Landroid/app/Dialog;

    invoke-virtual {v0, p0}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 323
    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->inProgressDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 325
    new-instance v0, Lcom/google/android/videos/activity/SettingsActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/videos/activity/SettingsActivity$1;-><init>(Lcom/google/android/videos/activity/SettingsActivity;)V

    invoke-static {v0}, Lcom/google/android/videos/async/CancelableCallback;->create(Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/CancelableCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->unlinkAccountCallback:Lcom/google/android/videos/async/CancelableCallback;

    .line 347
    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/api/ApiRequesters;->getUnlinkAccountRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v0

    new-instance v1, Lcom/google/android/videos/api/LinkedAccountRequest;

    iget-object v2, p0, Lcom/google/android/videos/activity/SettingsActivity;->account:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/google/android/videos/api/LinkedAccountRequest;-><init>(Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/google/android/videos/activity/SettingsActivity;->unlinkAccountCallback:Lcom/google/android/videos/async/CancelableCallback;

    invoke-static {p0, v2}, Lcom/google/android/videos/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/ActivityCallback;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 350
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 76
    invoke-super {p0, p1}, Lcom/google/android/videos/activity/VideosPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 77
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    .line 78
    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->config:Lcom/google/android/videos/Config;

    .line 79
    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->preferences:Landroid/content/SharedPreferences;

    .line 80
    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    .line 81
    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    invoke-virtual {v0}, Lcom/google/android/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->account:Ljava/lang/String;

    .line 82
    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 83
    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getItagInfoStore()Lcom/google/android/videos/store/ItagInfoStore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->itagInfoStore:Lcom/google/android/videos/store/ItagInfoStore;

    .line 84
    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    .line 86
    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->account:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    iget-object v1, p0, Lcom/google/android/videos/activity/SettingsActivity;->account:Ljava/lang/String;

    new-instance v2, Lcom/google/android/videos/activity/SettingsActivity$SyncUserConfigurationCallback;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/videos/activity/SettingsActivity$SyncUserConfigurationCallback;-><init>(Lcom/google/android/videos/activity/SettingsActivity;Lcom/google/android/videos/activity/SettingsActivity$1;)V

    invoke-static {p0, v2}, Lcom/google/android/videos/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/ActivityCallback;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/store/ConfigurationStore;->syncUserConfiguration(Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V

    .line 90
    :cond_0
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    .line 276
    iget-object v2, p0, Lcom/google/android/videos/activity/SettingsActivity;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, p2}, Lcom/google/android/videos/logging/EventLogger;->onPreferenceChange(Ljava/lang/String;Ljava/lang/Object;)V

    .line 277
    iget-object v2, p0, Lcom/google/android/videos/activity/SettingsActivity;->audioLanguagePreference:Landroid/preference/ListPreference;

    if-eq p1, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/videos/activity/SettingsActivity;->downloadNetworkPreference:Landroid/preference/ListPreference;

    if-eq p1, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/videos/activity/SettingsActivity;->downloadQualityPreference:Landroid/preference/ListPreference;

    if-ne p1, v2, :cond_1

    :cond_0
    move-object v0, p1

    .line 279
    check-cast v0, Landroid/preference/ListPreference;

    .line 280
    .local v0, "listPreference":Landroid/preference/ListPreference;
    check-cast p2, Ljava/lang/String;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {v0, p2}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v1

    .line 281
    .local v1, "selected":I
    invoke-virtual {v0}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v2

    aget-object v2, v2, v1

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 283
    .end local v0    # "listPreference":Landroid/preference/ListPreference;
    .end local v1    # "selected":I
    :cond_1
    const/4 v2, 0x1

    return v2
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 8
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    .line 290
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v6

    .line 291
    .local v6, "preferenceKey":Ljava/lang/String;
    const-string v0, "surround_sound_demo"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 292
    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->config:Lcom/google/android/videos/Config;

    invoke-interface {v0}, Lcom/google/android/videos/Config;->soundWelcomeVideoId()Ljava/lang/String;

    move-result-object v2

    .line 293
    .local v2, "videoId":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/videos/activity/SettingsActivity;->account:Ljava/lang/String;

    move-object v0, p0

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/activity/WatchActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/SettingsActivity;->startActivity(Landroid/content/Intent;)V

    .line 310
    .end local v2    # "videoId":Ljava/lang/String;
    :cond_0
    :goto_0
    return v5

    .line 294
    :cond_1
    const-string v0, "open_source_licenses"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 295
    new-instance v7, Landroid/webkit/WebView;

    invoke-direct {v7, p0}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 296
    .local v7, "webView":Landroid/webkit/WebView;
    new-instance v0, Landroid/webkit/WebViewClient;

    invoke-direct {v0}, Landroid/webkit/WebViewClient;-><init>()V

    invoke-virtual {v7, v0}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 297
    invoke-virtual {v7}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 298
    const-string v0, "file:///android_asset/license.html"

    invoke-virtual {v7, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 299
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b022d

    invoke-virtual {p0, v1}, Lcom/google/android/videos/activity/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    .line 302
    .end local v7    # "webView":Landroid/webkit/WebView;
    :cond_2
    const-string v0, "dma_unlink_account"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 303
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b022f

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0b0230

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0b0231

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0b010d

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 94
    invoke-super {p0}, Lcom/google/android/videos/activity/VideosPreferenceActivity;->onResume()V

    .line 95
    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/logging/EventLogger;->onSettingsPageOpened()V

    .line 96
    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->captionsOverridePreference:Landroid/preference/Preference;

    if-eqz v0, :cond_0

    .line 97
    iget-object v1, p0, Lcom/google/android/videos/activity/SettingsActivity;->captionsOverridePreference:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity;->preferences:Landroid/content/SharedPreferences;

    const-string v2, "captioning_enabled"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0b0240

    :goto_0
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(I)V

    .line 101
    :cond_0
    return-void

    .line 97
    :cond_1
    const v0, 0x7f0b023f

    goto :goto_0
.end method
