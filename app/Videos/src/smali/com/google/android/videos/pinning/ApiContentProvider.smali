.class public Lcom/google/android/videos/pinning/ApiContentProvider;
.super Landroid/content/ContentProvider;
.source "ApiContentProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/pinning/ApiContentProvider$Query;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 126
    return-void
.end method

.method private static getPinningStatus(Landroid/database/Cursor;)I
    .locals 2
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v0, 0x1

    .line 94
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 102
    :goto_0
    :pswitch_0
    return v0

    .line 96
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 98
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 100
    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 94
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 118
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 108
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 113
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 25
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/pinning/ApiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v14

    .line 46
    .local v14, "context":Landroid/content/Context;
    invoke-virtual {v14}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v18

    .line 47
    .local v18, "packageManager":Landroid/content/pm/PackageManager;
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v3

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v20

    .line 48
    .local v20, "packages":[Ljava/lang/String;
    if-nez v20, :cond_0

    .line 49
    const/16 v22, 0x0

    .line 90
    :goto_0
    return-object v22

    .line 51
    :cond_0
    invoke-static {v14}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v24

    .line 52
    .local v24, "videosGlobals":Lcom/google/android/videos/VideosGlobals;
    invoke-virtual/range {v24 .. v24}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v13

    .line 53
    .local v13, "config":Lcom/google/android/videos/Config;
    const/16 v21, 0x0

    .line 54
    .local v21, "referer":Ljava/lang/String;
    move-object/from16 v12, v20

    .local v12, "arr$":[Ljava/lang/String;
    array-length v0, v12

    move/from16 v17, v0

    .local v17, "len$":I
    const/16 v16, 0x0

    .local v16, "i$":I
    :goto_1
    move/from16 v0, v16

    move/from16 v1, v17

    if-ge v0, v1, :cond_1

    aget-object v19, v12, v16

    .line 55
    .local v19, "packageName":Ljava/lang/String;
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-static {v0, v1, v13}, Lcom/google/android/videos/utils/TrustedAppUtil;->getTrustedAppReferer(Ljava/lang/String;Landroid/content/pm/PackageManager;Lcom/google/android/videos/Config;)Ljava/lang/String;

    move-result-object v21

    .line 56
    if-eqz v21, :cond_2

    .line 60
    .end local v19    # "packageName":Ljava/lang/String;
    :cond_1
    invoke-virtual/range {v24 .. v24}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-interface {v3, v0}, Lcom/google/android/videos/logging/EventLogger;->onExternalApiQuery(Ljava/lang/String;)V

    .line 61
    if-nez v21, :cond_3

    .line 62
    const/16 v22, 0x0

    goto :goto_0

    .line 54
    .restart local v19    # "packageName":Ljava/lang/String;
    :cond_2
    add-int/lit8 v16, v16, 0x1

    goto :goto_1

    .line 65
    .end local v19    # "packageName":Ljava/lang/String;
    :cond_3
    const-string v3, "/movies/downloads"

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 66
    const/16 v22, 0x0

    goto :goto_0

    .line 68
    :cond_4
    const-string v3, "authAccount"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 69
    .local v11, "account":Ljava/lang/String;
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 70
    const/16 v22, 0x0

    goto :goto_0

    .line 73
    :cond_5
    invoke-virtual/range {v24 .. v24}, Lcom/google/android/videos/VideosGlobals;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 74
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v3, "purchased_assets, assets, videos ON asset_type = 6 AND assets_type = 6 AND assets_id = asset_id AND assets_id = video_id"

    sget-object v4, Lcom/google/android/videos/pinning/ApiContentProvider$Query;->PROJECTION:[Ljava/lang/String;

    const-string v5, "account = ? AND pinning_status NOT NULL AND title_eidr_id NOT NULL"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v11, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v2 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 78
    .local v15, "cursor":Landroid/database/Cursor;
    new-instance v22, Landroid/database/MatrixCursor;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "eidr"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "download_status"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "title"

    aput-object v5, v3, v4

    move-object/from16 v0, v22

    invoke-direct {v0, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 79
    .local v22, "result":Landroid/database/MatrixCursor;
    const/4 v3, 0x3

    new-array v0, v3, [Ljava/lang/Object;

    move-object/from16 v23, v0

    .line 81
    .local v23, "row":[Ljava/lang/Object;
    :goto_2
    :try_start_0
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 82
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface {v15, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/videos/utils/EidrId;->convertToFullEidr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v23, v3

    .line 83
    const/4 v3, 0x1

    invoke-static {v15}, Lcom/google/android/videos/pinning/ApiContentProvider;->getPinningStatus(Landroid/database/Cursor;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v23, v3

    .line 84
    const/4 v3, 0x2

    const/4 v4, 0x2

    invoke-interface {v15, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v23, v3

    .line 85
    invoke-virtual/range {v22 .. v23}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 88
    :catchall_0
    move-exception v3

    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    throw v3

    :cond_6
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 123
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
