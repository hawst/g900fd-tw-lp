.class Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$ThumbnailBitmapView;
.super Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;
.source "KnowledgeEntityBitmapView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ThumbnailBitmapView"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView",
        "<",
        "Landroid/widget/ImageView;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/widget/ImageView;)V
    .locals 6
    .param p1, "imageView"    # Landroid/widget/ImageView;

    .prologue
    const/4 v1, 0x0

    .line 49
    const/4 v4, 0x0

    move-object v0, p0

    move-object v2, v1

    move-object v3, p1

    move-object v5, v1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;-><init>(Lcom/google/android/play/image/AvatarCropTransformation;Lcom/google/android/videos/tagging/KnowledgeEntity;Landroid/view/View;ILcom/google/android/videos/tagging/KnowledgeEntityBitmapView$1;)V

    .line 50
    return-void
.end method


# virtual methods
.method postProcess(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p1, "response"    # Landroid/graphics/Bitmap;

    .prologue
    .line 54
    return-object p1
.end method

.method public bridge synthetic setThumbnail(Ljava/lang/Object;Z)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Z

    .prologue
    .line 46
    check-cast p1, Landroid/graphics/Bitmap;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-super {p0, p1, p2}, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;->setThumbnail(Landroid/graphics/Bitmap;Z)V

    return-void
.end method

.method updateImage(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "response"    # Landroid/graphics/Bitmap;

    .prologue
    .line 59
    if-eqz p1, :cond_0

    .line 60
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$ThumbnailBitmapView;->view:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 65
    :goto_0
    return-void

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$ThumbnailBitmapView;->view:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$ThumbnailBitmapView;->view:Landroid/view/View;

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_0
.end method
