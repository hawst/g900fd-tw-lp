.class Lcom/google/android/videos/VideosGlobalsImpl$3;
.super Ljava/lang/Thread;
.source "VideosGlobalsImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/VideosGlobalsImpl;->postStartup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/VideosGlobalsImpl;


# direct methods
.method constructor <init>(Lcom/google/android/videos/VideosGlobalsImpl;)V
    .locals 0

    .prologue
    .line 395
    iput-object p1, p0, Lcom/google/android/videos/VideosGlobalsImpl$3;->this$0:Lcom/google/android/videos/VideosGlobalsImpl;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 399
    iget-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl$3;->this$0:Lcom/google/android/videos/VideosGlobalsImpl;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobalsImpl;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v0

    .line 400
    .local v0, "account":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 402
    :try_start_0
    iget-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl$3;->this$0:Lcom/google/android/videos/VideosGlobalsImpl;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobalsImpl;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->blockingGetAuthToken(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 408
    :cond_0
    :goto_0
    sget v1, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_1

    .line 417
    :try_start_1
    const-string v1, "video/avc"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/google/android/exoplayer/MediaCodecUtil;->warmCodec(Ljava/lang/String;Z)V

    .line 418
    const-string v1, "video/avc"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/exoplayer/MediaCodecUtil;->warmCodec(Ljava/lang/String;Z)V

    .line 419
    const-string v1, "audio/mp4a-latm"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/exoplayer/MediaCodecUtil;->warmCodec(Ljava/lang/String;Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 426
    :goto_1
    iget-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl$3;->this$0:Lcom/google/android/videos/VideosGlobalsImpl;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobalsImpl;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper;->getDefaultSecurityLevel(Lcom/google/android/videos/logging/EventLogger;)I

    .line 428
    :cond_1
    return-void

    .line 420
    :catch_0
    move-exception v1

    goto :goto_1

    .line 403
    :catch_1
    move-exception v1

    goto :goto_0
.end method
