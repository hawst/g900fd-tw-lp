.class Lcom/google/android/videos/tagging/Cards;
.super Ljava/lang/Object;
.source "Cards.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/tagging/Cards$WebSearchOnClickListener;,
        Lcom/google/android/videos/tagging/Cards$SecondaryActionButtonHelper;
    }
.end annotation


# direct methods
.method protected static buildCharacterNamesString(Landroid/content/res/Resources;Lcom/google/android/videos/tagging/KnowledgeEntity$Person;)Ljava/lang/String;
    .locals 8
    .param p0, "resources"    # Landroid/content/res/Resources;
    .param p1, "actor"    # Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    .prologue
    const/4 v7, 0x0

    .line 55
    const/4 v1, 0x0

    .line 56
    .local v1, "characterNames":Ljava/lang/String;
    iget-object v4, p1, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->characterNames:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    .line 57
    .local v0, "characterNameCount":I
    if-eqz v0, :cond_1

    .line 58
    new-array v3, v0, [Ljava/lang/String;

    .line 59
    .local v3, "quotedCharacterNames":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 60
    const v4, 0x7f0b01d5

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p1, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->characterNames:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {p0, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    .line 59
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 63
    :cond_0
    invoke-static {p0, v7, v3}, Lcom/google/android/videos/utils/Util;->buildListString(Landroid/content/res/Resources;Z[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 65
    .end local v2    # "i":I
    .end local v3    # "quotedCharacterNames":[Ljava/lang/String;
    :cond_1
    return-object v1
.end method

.method protected static getBottom(Landroid/view/View;)I
    .locals 2
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 48
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v0

    goto :goto_0
.end method

.method protected static layout(Landroid/view/View;I)V
    .locals 3
    .param p0, "card"    # Landroid/view/View;
    .param p1, "cardWidth"    # I

    .prologue
    const/4 v2, 0x0

    .line 127
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {p1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/view/View;->measure(II)V

    .line 129
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0, v2, v2, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 130
    return-void
.end method

.method protected static requestActorImage(Landroid/app/Activity;Landroid/widget/TextView;Lcom/google/android/videos/tagging/KnowledgeEntity$Person;Lcom/google/android/videos/async/Requester;IZ)V
    .locals 6
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "textView"    # Landroid/widget/TextView;
    .param p2, "entity"    # Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    .param p4, "imageDimension"    # I
    .param p5, "large"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/widget/TextView;",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Person;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    .line 98
    .local p3, "imageRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;>;"
    iget-object v1, p2, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->image:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    if-nez v1, :cond_0

    .line 99
    invoke-static {p1, p2, p5}, Lcom/google/android/videos/tagging/Cards;->setDefaultActorImage(Landroid/widget/TextView;Lcom/google/android/videos/tagging/KnowledgeEntity$Person;Z)V

    .line 106
    :goto_0
    return-void

    .line 102
    :cond_0
    new-instance v0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$ActorBitmapView;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/play/image/AvatarCropTransformation;->getFullAvatarCrop(Landroid/content/res/Resources;)Lcom/google/android/play/image/AvatarCropTransformation;

    move-result-object v1

    move-object v2, p2

    move-object v3, p1

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$ActorBitmapView;-><init>(Lcom/google/android/play/image/AvatarCropTransformation;Lcom/google/android/videos/tagging/KnowledgeEntity$Person;Landroid/widget/TextView;IZ)V

    .line 105
    .local v0, "bitmapView":Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;, "Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView<*>;"
    iget-object v1, p2, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->image:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    invoke-static {v0, p3, v1}, Lcom/google/android/videos/ui/BitmapLoader;->setBitmapAsync(Lcom/google/android/videos/ui/BitmapLoader$BitmapView;Lcom/google/android/videos/async/Requester;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected static requestImage(Landroid/widget/ImageView;Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Lcom/google/android/videos/async/Requester;)V
    .locals 1
    .param p0, "imageView"    # Landroid/widget/ImageView;
    .param p1, "image"    # Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ImageView;",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 121
    .local p2, "imageRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;>;"
    new-instance v0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$ThumbnailBitmapView;

    invoke-direct {v0, p0}, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$ThumbnailBitmapView;-><init>(Landroid/widget/ImageView;)V

    .line 123
    .local v0, "bitmapView":Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;, "Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView<*>;"
    invoke-static {v0, p2, p1}, Lcom/google/android/videos/ui/BitmapLoader;->setBitmapAsync(Lcom/google/android/videos/ui/BitmapLoader$BitmapView;Lcom/google/android/videos/async/Requester;Ljava/lang/Object;)V

    .line 124
    return-void
.end method

.method protected static requestSongImage(Landroid/widget/ImageView;Lcom/google/android/videos/tagging/KnowledgeEntity$Song;Lcom/google/android/videos/async/Requester;I)V
    .locals 2
    .param p0, "imageView"    # Landroid/widget/ImageView;
    .param p1, "song"    # Lcom/google/android/videos/tagging/KnowledgeEntity$Song;
    .param p3, "imageDimension"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ImageView;",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Song;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 110
    .local p2, "imageRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;>;"
    iget-object v1, p1, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;->image:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    if-nez v1, :cond_0

    .line 111
    invoke-static {p0}, Lcom/google/android/videos/tagging/Cards;->setDefaultSongImage(Landroid/widget/ImageView;)V

    .line 117
    :goto_0
    return-void

    .line 114
    :cond_0
    new-instance v0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$SongBitmapView;

    invoke-direct {v0, p1, p0, p3}, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$SongBitmapView;-><init>(Lcom/google/android/videos/tagging/KnowledgeEntity$Song;Landroid/widget/ImageView;I)V

    .line 116
    .local v0, "bitmapView":Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;, "Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView<*>;"
    iget-object v1, p1, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;->image:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    invoke-static {v0, p2, v1}, Lcom/google/android/videos/ui/BitmapLoader;->setBitmapAsync(Lcom/google/android/videos/ui/BitmapLoader$BitmapView;Lcom/google/android/videos/async/Requester;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected static setDefaultActorImage(Landroid/widget/TextView;Lcom/google/android/videos/tagging/KnowledgeEntity$Person;Z)V
    .locals 3
    .param p0, "textView"    # Landroid/widget/TextView;
    .param p1, "entity"    # Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    .param p2, "large"    # Z

    .prologue
    .line 81
    if-eqz p2, :cond_0

    .line 82
    const v0, 0x7f020037

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 86
    :goto_0
    iget-object v0, p1, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->name:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    return-void

    .line 84
    :cond_0
    const v0, 0x7f020036

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method protected static setDefaultSongImage(Landroid/widget/ImageView;)V
    .locals 2
    .param p0, "imageView"    # Landroid/widget/ImageView;

    .prologue
    .line 90
    invoke-virtual {p0}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0085

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 92
    const v0, 0x7f020098

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 93
    return-void
.end method

.method protected static setImageMatrixIfSquareCropExists(Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Landroid/widget/ImageView;I)V
    .locals 7
    .param p0, "image"    # Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
    .param p1, "imageView"    # Landroid/widget/ImageView;
    .param p2, "imageSize"    # I

    .prologue
    const/4 v6, 0x0

    .line 70
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;->getSquareCropRegionIndex()I

    move-result v2

    .line 71
    .local v2, "squareCropRegionIndex":I
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 72
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    invoke-virtual {p0, v2, v3}, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;->getCropRegion(ILandroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v0

    .line 73
    .local v0, "cropRect":Landroid/graphics/RectF;
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 74
    .local v1, "matrix":Landroid/graphics/Matrix;
    new-instance v3, Landroid/graphics/RectF;

    int-to-float v4, p2

    int-to-float v5, p2

    invoke-direct {v3, v6, v6, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    sget-object v4, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v1, v0, v3, v4}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 75
    sget-object v3, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p1, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 76
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 78
    .end local v0    # "cropRect":Landroid/graphics/RectF;
    .end local v1    # "matrix":Landroid/graphics/Matrix;
    :cond_0
    return-void
.end method

.method protected static showViewAndSetText(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;
    .locals 2
    .param p0, "root"    # Landroid/view/View;
    .param p1, "textViewId"    # I
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 41
    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 42
    .local v0, "textView":Landroid/widget/TextView;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 43
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 44
    return-object v0
.end method

.method protected static toQuery(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "phrase"    # Ljava/lang/String;

    .prologue
    .line 133
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "\""

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "\""

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 136
    .end local p0    # "phrase":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .restart local p0    # "phrase":Ljava/lang/String;
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method
