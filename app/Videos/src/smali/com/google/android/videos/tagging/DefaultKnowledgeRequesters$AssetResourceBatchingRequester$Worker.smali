.class final Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;
.super Ljava/lang/Object;
.source "DefaultKnowledgeRequesters.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "Worker"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/api/AssetsRequest;",
        "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private firstException:Ljava/lang/Exception;

.field private hadSuccessfulResponse:Z

.field private nextIndex:I

.field private final originalCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;"
        }
    .end annotation
.end field

.field private final originalRequest:Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;

.field private final resources:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;Lcom/google/android/videos/async/Callback;)V
    .locals 1
    .param p2, "originalRequest"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 223
    .local p3, "originalCallback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;Lcom/google/android/videos/utils/ByteArray;>;"
    iput-object p1, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->this$0:Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 224
    iput-object p2, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->originalRequest:Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;

    .line 225
    iput-object p3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->originalCallback:Lcom/google/android/videos/async/Callback;

    .line 227
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->resources:Ljava/util/List;

    .line 228
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->nextIndex:I

    .line 229
    return-void
.end method

.method private maybeAddAssetResource(Lcom/google/wireless/android/video/magma/proto/AssetResource;)V
    .locals 11
    .param p1, "resourceFromResponse"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 274
    iget-object v10, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez v10, :cond_1

    .line 305
    :cond_0
    :goto_0
    return-void

    .line 277
    :cond_1
    iget-object v6, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 278
    .local v6, "resourceId":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    iget-object v10, v6, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->mid:Ljava/lang/String;

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 281
    iget-object v10, v6, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_5

    move v0, v8

    .line 282
    .local v0, "hasYouTubeId":Z
    :goto_1
    iget-object v10, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    if-eqz v10, :cond_6

    iget-object v10, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v4, v10, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    .line 284
    .local v4, "images":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    :goto_2
    if-nez v4, :cond_7

    move v3, v9

    .line 285
    .local v3, "imageCount":I
    :goto_3
    const/4 v7, 0x0

    .line 286
    .local v7, "selectedImages":Ljava/util/List;, "Ljava/util/List<Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_4
    if-ge v1, v3, :cond_8

    .line 287
    aget-object v2, v4, v1

    .line 288
    .local v2, "image":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    iget v9, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->type:I

    const/4 v10, 0x3

    if-eq v9, v10, :cond_2

    iget v9, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->type:I

    if-ne v9, v8, :cond_4

    .line 289
    :cond_2
    if-nez v7, :cond_3

    .line 290
    new-instance v7, Ljava/util/ArrayList;

    .end local v7    # "selectedImages":Ljava/util/List;, "Ljava/util/List<Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;>;"
    sub-int v9, v3, v1

    invoke-direct {v7, v9}, Ljava/util/ArrayList;-><init>(I)V

    .line 292
    .restart local v7    # "selectedImages":Ljava/util/List;, "Ljava/util/List<Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;>;"
    :cond_3
    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 286
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .end local v0    # "hasYouTubeId":Z
    .end local v1    # "i":I
    .end local v2    # "image":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .end local v3    # "imageCount":I
    .end local v4    # "images":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .end local v7    # "selectedImages":Ljava/util/List;, "Ljava/util/List<Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;>;"
    :cond_5
    move v0, v9

    .line 281
    goto :goto_1

    .line 282
    .restart local v0    # "hasYouTubeId":Z
    :cond_6
    const/4 v4, 0x0

    goto :goto_2

    .line 284
    .restart local v4    # "images":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    :cond_7
    array-length v3, v4

    goto :goto_3

    .line 295
    .restart local v1    # "i":I
    .restart local v3    # "imageCount":I
    .restart local v7    # "selectedImages":Ljava/util/List;, "Ljava/util/List<Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;>;"
    :cond_8
    if-nez v0, :cond_9

    if-eqz v7, :cond_0

    .line 298
    :cond_9
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/AssetResource;-><init>()V

    .line 299
    .local v5, "resource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    iput-object v6, v5, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 300
    if-eqz v7, :cond_a

    .line 301
    new-instance v8, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    invoke-direct {v8}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;-><init>()V

    iput-object v8, v5, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    .line 302
    iget-object v9, v5, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    new-array v8, v8, [Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    invoke-interface {v7, v8}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    iput-object v8, v9, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    .line 304
    :cond_a
    iget-object v8, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->resources:Ljava/util/List;

    invoke-interface {v8, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/api/AssetsRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/api/AssetsRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->firstException:Ljava/lang/Exception;

    if-nez v0, :cond_0

    .line 310
    iput-object p2, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->firstException:Ljava/lang/Exception;

    .line 312
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->requestNextBatch()V

    .line 313
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 212
    check-cast p1, Lcom/google/android/videos/api/AssetsRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->onError(Lcom/google/android/videos/api/AssetsRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/api/AssetsRequest;
    .param p2, "response"    # Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    .prologue
    .line 265
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->hadSuccessfulResponse:Z

    .line 266
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p2, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 267
    iget-object v1, p2, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    aget-object v1, v1, v0

    invoke-direct {p0, v1}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->maybeAddAssetResource(Lcom/google/wireless/android/video/magma/proto/AssetResource;)V

    .line 266
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 269
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->requestNextBatch()V

    .line 270
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 212
    check-cast p1, Lcom/google/android/videos/api/AssetsRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->onResponse(Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;)V

    return-void
.end method

.method public requestNextBatch()V
    .locals 9

    .prologue
    .line 232
    iget-object v3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->originalRequest:Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;

    iget-object v3, v3, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;->assetResourceIds:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    .line 233
    .local v0, "idsSize":I
    iget v3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->nextIndex:I

    if-lt v3, v0, :cond_2

    .line 234
    if-eqz v0, :cond_0

    iget-boolean v3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->hadSuccessfulResponse:Z

    if-eqz v3, :cond_1

    .line 235
    :cond_0
    new-instance v2, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    invoke-direct {v2}, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;-><init>()V

    .line 236
    .local v2, "response":Lcom/google/wireless/android/video/magma/proto/AssetListResponse;
    iget-object v3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->resources:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->resources:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-interface {v3, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iput-object v3, v2, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 237
    iget-object v3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->originalCallback:Lcom/google/android/videos/async/Callback;

    iget-object v4, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->originalRequest:Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;

    new-instance v5, Lcom/google/android/videos/utils/ByteArray;

    invoke-static {v2}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/videos/utils/ByteArray;-><init>([B)V

    invoke-interface {v3, v4, v5}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 261
    .end local v2    # "response":Lcom/google/wireless/android/video/magma/proto/AssetListResponse;
    :goto_0
    return-void

    .line 240
    :cond_1
    iget-object v3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->originalCallback:Lcom/google/android/videos/async/Callback;

    iget-object v4, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->originalRequest:Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;

    iget-object v5, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->firstException:Ljava/lang/Exception;

    invoke-interface {v3, v4, v5}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0

    .line 245
    :cond_2
    new-instance v3, Lcom/google/android/videos/api/AssetsRequest$Builder;

    invoke-direct {v3}, Lcom/google/android/videos/api/AssetsRequest$Builder;-><init>()V

    iget-object v4, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->originalRequest:Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;

    iget-object v4, v4, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;->account:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/videos/api/AssetsRequest$Builder;->account(Ljava/lang/String;)Lcom/google/android/videos/api/AssetsRequest$Builder;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/google/android/videos/api/AssetsRequest$Builder;->addFlags(I)Lcom/google/android/videos/api/AssetsRequest$Builder;

    move-result-object v1

    .line 247
    .local v1, "requestBuilder":Lcom/google/android/videos/api/AssetsRequest$Builder;
    iget-object v3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->originalRequest:Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;

    iget-object v3, v3, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;->userCountry:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 248
    iget-object v3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->originalRequest:Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;

    iget-object v3, v3, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;->userCountry:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/google/android/videos/api/AssetsRequest$Builder;->userCountry(Ljava/lang/String;)Lcom/google/android/videos/api/AssetsRequest$Builder;

    .line 251
    :cond_3
    :goto_1
    iget v3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->nextIndex:I

    if-ge v3, v0, :cond_4

    iget-object v3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->originalRequest:Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;

    iget-object v3, v3, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;->assetResourceIds:Ljava/util/List;

    iget v4, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->nextIndex:I

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/google/android/videos/api/AssetsRequest$Builder;->maybeAddId(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 252
    iget v3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->nextIndex:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->nextIndex:I

    goto :goto_1

    .line 254
    :cond_4
    invoke-virtual {v1}, Lcom/google/android/videos/api/AssetsRequest$Builder;->getIdCount()I

    move-result v3

    if-nez v3, :cond_5

    .line 256
    iget-object v4, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->originalCallback:Lcom/google/android/videos/async/Callback;

    iget-object v5, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->originalRequest:Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;

    new-instance v6, Lcom/google/android/videos/converter/ConverterException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Request contains an invalid ID "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->originalRequest:Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;

    iget-object v3, v3, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;->assetResourceIds:Ljava/util/List;

    iget v8, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->nextIndex:I

    invoke-interface {v3, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v6, v3}, Lcom/google/android/videos/converter/ConverterException;-><init>(Ljava/lang/String;)V

    invoke-interface {v4, v5, v6}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0

    .line 260
    :cond_5
    iget-object v3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->this$0:Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;

    # getter for: Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;->apiAssetsCachingRequester:Lcom/google/android/videos/async/Requester;
    invoke-static {v3}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;->access$200(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;)Lcom/google/android/videos/async/Requester;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/videos/api/AssetsRequest$Builder;->build()Lcom/google/android/videos/api/AssetsRequest;

    move-result-object v4

    invoke-interface {v3, v4, p0}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    goto/16 :goto_0
.end method
