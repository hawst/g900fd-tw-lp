.class public Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;
.super Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;
.source "TvWidevineMediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;
    }
.end annotation


# instance fields
.field private final bufferUnderrunTimeoutHandler:Ljava/lang/Runnable;

.field private fillingBuffer:Z

.field private final handler:Landroid/os/Handler;

.field private listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

.field private seekDesiredPos:I

.field private seekWhenPrepared:I

.field private state:Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;)V
    .locals 1
    .param p1, "delegate"    # Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;-><init>(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;)V

    .line 37
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->seekDesiredPos:I

    .line 44
    sget-object v0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;->IDLE:Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;

    iput-object v0, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->state:Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;

    .line 49
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->handler:Landroid/os/Handler;

    .line 50
    invoke-direct {p0}, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->createBufferUnderrunTimeoutHandler()Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->bufferUnderrunTimeoutHandler:Ljava/lang/Runnable;

    .line 51
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;
    .param p1, "x1"    # I

    .prologue
    .line 23
    iput p1, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->seekDesiredPos:I

    return p1
.end method

.method private createBufferUnderrunTimeoutHandler()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 191
    new-instance v0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$2;

    invoke-direct {v0, p0}, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$2;-><init>(Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;)V

    return-object v0
.end method


# virtual methods
.method public getCurrentPosition()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->seekDesiredPos:I

    if-ltz v0, :cond_0

    .line 105
    iget v0, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->seekDesiredPos:I

    .line 107
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->getCurrentPosition()I

    move-result v0

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->state:Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;

    sget-object v1, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;->PLAYING:Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->fillingBuffer:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onInfo(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;II)Z
    .locals 7
    .param p1, "mediaPlayer"    # Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v4, -0x1

    .line 124
    packed-switch p2, :pswitch_data_0

    .line 144
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->onInfo(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;II)Z

    .line 145
    return v6

    .line 126
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->getDuration()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->getCurrentPosition()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-long v0, v2

    .line 127
    .local v0, "remainingTime":J
    iget-object v2, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->state:Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;

    sget-object v3, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;->PLAYING:Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;

    if-ne v2, v3, :cond_1

    const-wide/32 v2, 0xea60

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 128
    invoke-super {p0}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->pause()V

    .line 129
    const/16 v2, 0x2bd

    invoke-super {p0, p1, v2, v4}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->onInfo(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;II)Z

    .line 130
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->fillingBuffer:Z

    .line 132
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->bufferUnderrunTimeoutHandler:Ljava/lang/Runnable;

    const-wide/16 v4, 0x4e20

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 136
    .end local v0    # "remainingTime":J
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->bufferUnderrunTimeoutHandler:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 137
    iput-boolean v6, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->fillingBuffer:Z

    .line 138
    iget-object v2, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->state:Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;

    sget-object v3, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;->PLAYING:Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;

    if-ne v2, v3, :cond_0

    .line 139
    invoke-super {p0}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->start()V

    .line 140
    const/16 v2, 0x2be

    invoke-super {p0, p1, v2, v4}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->onInfo(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;II)Z

    goto :goto_0

    .line 124
    nop

    :pswitch_data_0
    .packed-switch 0x2f1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onSeekComplete(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;)V
    .locals 4
    .param p1, "mediaPlayer"    # Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    .prologue
    .line 150
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->fillingBuffer:Z

    .line 151
    iget v0, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->seekWhenPrepared:I

    if-eqz v0, :cond_1

    .line 152
    iget v0, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->seekWhenPrepared:I

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->seekTo(I)V

    .line 163
    :goto_0
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->state:Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;

    sget-object v1, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;->PAUSED:Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;

    if-ne v0, v1, :cond_2

    .line 169
    invoke-super {p0}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->start()V

    .line 170
    invoke-super {p0}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->pause()V

    .line 174
    :cond_0
    :goto_1
    invoke-super {p0, p1}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->onSeekComplete(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;)V

    .line 175
    return-void

    .line 156
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$1;

    invoke-direct {v1, p0}, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$1;-><init>(Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 171
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->state:Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;

    sget-object v1, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;->PLAYING:Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;

    if-ne v0, v1, :cond_0

    .line 172
    invoke-super {p0}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->start()V

    goto :goto_1
.end method

.method public pause()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->fillingBuffer:Z

    if-eqz v0, :cond_0

    .line 72
    :goto_0
    return-void

    .line 70
    :cond_0
    invoke-super {p0}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->pause()V

    .line 71
    sget-object v0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;->PAUSED:Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;

    iput-object v0, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->state:Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;

    goto :goto_0
.end method

.method public release()V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->bufferUnderrunTimeoutHandler:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 84
    invoke-super {p0}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->release()V

    .line 85
    sget-object v0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;->IDLE:Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;

    iput-object v0, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->state:Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;

    .line 86
    return-void
.end method

.method public seekTo(I)V
    .locals 2
    .param p1, "msec"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 90
    iput p1, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->seekDesiredPos:I

    .line 91
    iget-boolean v0, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->fillingBuffer:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->state:Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;

    sget-object v1, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;->PLAYING:Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;

    if-eq v0, v1, :cond_1

    .line 92
    :cond_0
    iput p1, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->seekWhenPrepared:I

    .line 100
    :goto_0
    return-void

    .line 95
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->seekWhenPrepared:I

    .line 98
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->fillingBuffer:Z

    .line 99
    invoke-super {p0, p1}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->seekTo(I)V

    goto :goto_0
.end method

.method public setListener(Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    .prologue
    .line 179
    invoke-super {p0, p1}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->setListener(Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;)V

    .line 180
    iput-object p1, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    .line 181
    return-void
.end method

.method public start()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->fillingBuffer:Z

    if-eqz v0, :cond_1

    .line 63
    :cond_0
    :goto_0
    return-void

    .line 58
    :cond_1
    invoke-super {p0}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->start()V

    .line 59
    sget-object v0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;->PLAYING:Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;

    iput-object v0, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->state:Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer$State;

    .line 60
    iget v0, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->seekWhenPrepared:I

    if-eqz v0, :cond_0

    .line 61
    iget v0, p0, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->seekWhenPrepared:I

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;->seekTo(I)V

    goto :goto_0
.end method
