.class public final Lcom/google/android/videos/pano/binders/ShowBinder;
.super Ljava/lang/Object;
.source "ShowBinder.java"

# interfaces
.implements Lcom/google/android/repolib/common/Binder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Binder",
        "<",
        "Lcom/google/android/videos/pano/model/ShowItem;",
        "Lcom/google/android/videos/pano/ui/ShowCardView;",
        ">;"
    }
.end annotation


# instance fields
.field private final activity:Landroid/app/Activity;

.field private final bitmapRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/videos/async/Requester;Landroid/app/Activity;)V
    .locals 0
    .param p2, "activity"    # Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/app/Activity;",
            ")V"
        }
    .end annotation

    .prologue
    .line 26
    .local p1, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/async/ControllableRequest<Landroid/net/Uri;>;Landroid/graphics/Bitmap;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/videos/pano/binders/ShowBinder;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    .line 28
    iput-object p2, p0, Lcom/google/android/videos/pano/binders/ShowBinder;->activity:Landroid/app/Activity;

    .line 29
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/pano/binders/ShowBinder;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/binders/ShowBinder;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/videos/pano/binders/ShowBinder;->activity:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public bind(Lcom/google/android/videos/pano/model/ShowItem;Lcom/google/android/videos/pano/ui/ShowCardView;)V
    .locals 3
    .param p1, "item"    # Lcom/google/android/videos/pano/model/ShowItem;
    .param p2, "view"    # Lcom/google/android/videos/pano/ui/ShowCardView;

    .prologue
    .line 33
    invoke-virtual {p1}, Lcom/google/android/videos/pano/model/ShowItem;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/google/android/videos/pano/ui/ShowCardView;->setText(Ljava/lang/CharSequence;)V

    .line 34
    invoke-virtual {p1}, Lcom/google/android/videos/pano/model/ShowItem;->getImageResourceId()I

    move-result v1

    invoke-virtual {p2, v1}, Lcom/google/android/videos/pano/ui/ShowCardView;->setImageResource(I)V

    .line 35
    invoke-virtual {p1}, Lcom/google/android/videos/pano/model/ShowItem;->getStarRating()F

    move-result v1

    invoke-virtual {p2, v1}, Lcom/google/android/videos/pano/ui/ShowCardView;->setRating(F)V

    .line 36
    invoke-virtual {p1}, Lcom/google/android/videos/pano/model/ShowItem;->getImageUri()Ljava/lang/String;

    move-result-object v0

    .line 38
    .local v0, "imageUri":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 39
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/pano/binders/ShowBinder;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    invoke-virtual {p2, v1, v2}, Lcom/google/android/videos/pano/ui/ShowCardView;->setImageUri(Landroid/net/Uri;Lcom/google/android/videos/async/Requester;)V

    .line 42
    :cond_0
    new-instance v1, Lcom/google/android/videos/pano/binders/ShowBinder$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/videos/pano/binders/ShowBinder$1;-><init>(Lcom/google/android/videos/pano/binders/ShowBinder;Lcom/google/android/videos/pano/model/ShowItem;Lcom/google/android/videos/pano/ui/ShowCardView;)V

    invoke-virtual {p2, v1}, Lcom/google/android/videos/pano/ui/ShowCardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    return-void
.end method

.method public bridge synthetic bind(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 20
    check-cast p1, Lcom/google/android/videos/pano/model/ShowItem;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/videos/pano/ui/ShowCardView;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pano/binders/ShowBinder;->bind(Lcom/google/android/videos/pano/model/ShowItem;Lcom/google/android/videos/pano/ui/ShowCardView;)V

    return-void
.end method
