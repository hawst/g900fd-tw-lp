.class public abstract Lcom/google/android/videos/ui/CursorHelper$HomeShowPurchaseCursorHelper;
.super Lcom/google/android/videos/ui/CursorHelper$HomePurchaseCursorHelper;
.source "CursorHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/CursorHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "HomeShowPurchaseCursorHelper"
.end annotation


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/videos/Config;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/ui/SyncHelper;Lcom/google/android/videos/utils/DownloadedOnlyManager;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "config"    # Lcom/google/android/videos/Config;
    .param p3, "uiHandler"    # Landroid/os/Handler;
    .param p4, "database"    # Lcom/google/android/videos/store/Database;
    .param p5, "store"    # Lcom/google/android/videos/store/PurchaseStore;
    .param p6, "syncHelper"    # Lcom/google/android/videos/ui/SyncHelper;
    .param p7, "downloadedOnlyManager"    # Lcom/google/android/videos/utils/DownloadedOnlyManager;

    .prologue
    .line 456
    invoke-direct/range {p0 .. p7}, Lcom/google/android/videos/ui/CursorHelper$HomePurchaseCursorHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/Config;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/ui/SyncHelper;Lcom/google/android/videos/utils/DownloadedOnlyManager;)V

    .line 457
    return-void
.end method


# virtual methods
.method public onShowMetadataUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1, "showId"    # Ljava/lang/String;

    .prologue
    .line 461
    invoke-virtual {p0}, Lcom/google/android/videos/ui/CursorHelper$HomeShowPurchaseCursorHelper;->scheduleUpdate()V

    .line 462
    return-void
.end method

.method public onShowUserAssetsUpdated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;

    .prologue
    .line 466
    invoke-virtual {p0, p1}, Lcom/google/android/videos/ui/CursorHelper$HomeShowPurchaseCursorHelper;->maybeScheduleUpdate(Ljava/lang/String;)V

    .line 467
    return-void
.end method
