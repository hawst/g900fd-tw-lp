.class public Lcom/google/android/videos/player/legacy/DrmMediaPlayer;
.super Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;
.source "DrmMediaPlayer.java"

# interfaces
.implements Lcom/google/android/videos/drm/DrmManager$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/legacy/DrmMediaPlayer$1;
    }
.end annotation


# instance fields
.field private final appLevelDrm:Z

.field private final drmManager:Lcom/google/android/videos/drm/DrmManager;

.field private final isGoogleTv:Z


# direct methods
.method public constructor <init>(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;Landroid/content/Context;Lcom/google/android/videos/drm/DrmManager;Z)V
    .locals 1
    .param p1, "delegate"    # Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "drmManager"    # Lcom/google/android/videos/drm/DrmManager;
    .param p4, "appLevelDrm"    # Z

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;-><init>(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;)V

    .line 37
    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/utils/Util;->isGoogleTv(Landroid/content/pm/PackageManager;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/videos/player/legacy/DrmMediaPlayer;->isGoogleTv:Z

    .line 38
    iput-object p3, p0, Lcom/google/android/videos/player/legacy/DrmMediaPlayer;->drmManager:Lcom/google/android/videos/drm/DrmManager;

    .line 39
    iput-boolean p4, p0, Lcom/google/android/videos/player/legacy/DrmMediaPlayer;->appLevelDrm:Z

    .line 40
    return-void
.end method

.method private getPlayableUri(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    iget-object v1, p0, Lcom/google/android/videos/player/legacy/DrmMediaPlayer;->drmManager:Lcom/google/android/videos/drm/DrmManager;

    iget-boolean v2, p0, Lcom/google/android/videos/player/legacy/DrmMediaPlayer;->appLevelDrm:Z

    invoke-virtual {v1, p1, v2}, Lcom/google/android/videos/drm/DrmManager;->getPlayableUri(Landroid/net/Uri;Z)Landroid/net/Uri;

    move-result-object v0

    .line 51
    .local v0, "playableUri":Landroid/net/Uri;
    if-nez v0, :cond_0

    .line 53
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t obtain playable stream for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 55
    :cond_0
    return-object v0
.end method

.method private toMediaPlayerErrorCode(I)I
    .locals 1
    .param p1, "errorCode"    # I

    .prologue
    .line 110
    packed-switch p1, :pswitch_data_0

    .line 126
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 112
    :pswitch_0
    sget v0, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_AUTHENTICATION_FAILURE:I

    goto :goto_0

    .line 114
    :pswitch_1
    sget v0, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_NO_ACTIVE_PURCHASE_AGREEMENT:I

    goto :goto_0

    .line 116
    :pswitch_2
    sget v0, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_CONCURRENT_PLAYBACK:I

    goto :goto_0

    .line 118
    :pswitch_3
    sget v0, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_UNUSUAL_ACTIVITY:I

    goto :goto_0

    .line 120
    :pswitch_4
    sget v0, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_STREAMING_UNAVAILABLE:I

    goto :goto_0

    .line 122
    :pswitch_5
    sget v0, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_CANNOT_ACTIVATE_RENTAL:I

    goto :goto_0

    .line 124
    :pswitch_6
    sget v0, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_CONCURRENT_PLAYBACKS_BY_ACCOUNT:I

    goto :goto_0

    .line 110
    :pswitch_data_0
    .packed-switch 0x201
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public final onError(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;II)Z
    .locals 2
    .param p1, "mediaPlayer"    # Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    .line 99
    const/4 v1, 0x1

    if-ne p2, v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/videos/player/legacy/DrmMediaPlayer;->isGoogleTv:Z

    if-eqz v1, :cond_0

    .line 101
    invoke-direct {p0, p3}, Lcom/google/android/videos/player/legacy/DrmMediaPlayer;->toMediaPlayerErrorCode(I)I

    move-result v0

    .line 102
    .local v0, "mediaPlayerError":I
    if-eqz v0, :cond_0

    .line 103
    move p3, v0

    .line 106
    .end local v0    # "mediaPlayerError":I
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->onError(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;II)Z

    move-result v1

    return v1
.end method

.method public final onHeartbeatError(Ljava/lang/String;I)V
    .locals 2
    .param p1, "assetPath"    # Ljava/lang/String;
    .param p2, "errorCode"    # I

    .prologue
    .line 74
    invoke-direct {p0, p2}, Lcom/google/android/videos/player/legacy/DrmMediaPlayer;->toMediaPlayerErrorCode(I)I

    move-result v0

    .line 75
    .local v0, "mediaPlayerError":I
    if-nez v0, :cond_0

    .line 76
    sget v0, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_TERMINATE_REQUESTED:I

    .line 78
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/videos/player/legacy/DrmMediaPlayer;->notifyError(II)Z

    .line 79
    return-void
.end method

.method public final onPlaybackStopped(Ljava/lang/String;Lcom/google/android/videos/drm/DrmManager$StopReason;)V
    .locals 3
    .param p1, "assetPath"    # Ljava/lang/String;
    .param p2, "reason"    # Lcom/google/android/videos/drm/DrmManager$StopReason;

    .prologue
    const/4 v2, 0x1

    .line 83
    sget-object v0, Lcom/google/android/videos/player/legacy/DrmMediaPlayer$1;->$SwitchMap$com$google$android$videos$drm$DrmManager$StopReason:[I

    invoke-virtual {p2}, Lcom/google/android/videos/drm/DrmManager$StopReason;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 95
    :goto_0
    return-void

    .line 85
    :pswitch_0
    const/16 v0, 0x1f

    invoke-virtual {p0, v2, v0}, Lcom/google/android/videos/player/legacy/DrmMediaPlayer;->notifyError(II)Z

    goto :goto_0

    .line 89
    :pswitch_1
    const/16 v0, 0x20

    invoke-virtual {p0, v2, v0}, Lcom/google/android/videos/player/legacy/DrmMediaPlayer;->notifyError(II)Z

    goto :goto_0

    .line 83
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final release()V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/DrmMediaPlayer;->drmManager:Lcom/google/android/videos/drm/DrmManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/drm/DrmManager;->setListener(Lcom/google/android/videos/drm/DrmManager$Listener;)V

    .line 68
    invoke-super {p0}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->release()V

    .line 69
    return-void
.end method

.method public setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    .local p3, "headers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p2}, Lcom/google/android/videos/player/legacy/DrmMediaPlayer;->getPlayableUri(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 46
    .local v0, "playableUri":Landroid/net/Uri;
    invoke-super {p0, p1, v0, p3}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    .line 47
    return-void
.end method

.method public final setListener(Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    .prologue
    .line 60
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/DrmMediaPlayer;->drmManager:Lcom/google/android/videos/drm/DrmManager;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/drm/DrmManager;->setListener(Lcom/google/android/videos/drm/DrmManager$Listener;)V

    .line 62
    invoke-super {p0, p1}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->setListener(Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;)V

    .line 63
    return-void
.end method
