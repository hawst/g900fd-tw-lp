.class public Lcom/google/android/videos/ui/VideoCollectionHelper;
.super Ljava/lang/Object;
.source "VideoCollectionHelper.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/VideoCollectionHelper$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<TT;",
        "Lcom/google/wireless/android/video/magma/proto/VideoCollectionListResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final collectionsCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<TT;",
            "Lcom/google/wireless/android/video/magma/proto/VideoCollectionListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final collectionsRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<TT;",
            "Lcom/google/wireless/android/video/magma/proto/VideoCollectionListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private currentRequest:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final errorHelper:Lcom/google/android/videos/utils/ErrorHelper;

.field private final eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private final listener:Lcom/google/android/videos/ui/VideoCollectionHelper$Listener;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/ui/VideoCollectionHelper$Listener;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/utils/ErrorHelper;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p3, "listener"    # Lcom/google/android/videos/ui/VideoCollectionHelper$Listener;
    .param p4, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;
    .param p5, "errorHelper"    # Lcom/google/android/videos/utils/ErrorHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/google/android/videos/async/Requester",
            "<TT;",
            "Lcom/google/wireless/android/video/magma/proto/VideoCollectionListResponse;",
            ">;",
            "Lcom/google/android/videos/ui/VideoCollectionHelper$Listener;",
            "Lcom/google/android/videos/logging/EventLogger;",
            "Lcom/google/android/videos/utils/ErrorHelper;",
            ")V"
        }
    .end annotation

    .prologue
    .line 50
    .local p0, "this":Lcom/google/android/videos/ui/VideoCollectionHelper;, "Lcom/google/android/videos/ui/VideoCollectionHelper<TT;>;"
    .local p2, "requester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TT;Lcom/google/wireless/android/video/magma/proto/VideoCollectionListResponse;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/ui/VideoCollectionHelper;->collectionsRequester:Lcom/google/android/videos/async/Requester;

    .line 53
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/VideoCollectionHelper$Listener;

    iput-object v0, p0, Lcom/google/android/videos/ui/VideoCollectionHelper;->listener:Lcom/google/android/videos/ui/VideoCollectionHelper$Listener;

    .line 54
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/logging/EventLogger;

    iput-object v0, p0, Lcom/google/android/videos/ui/VideoCollectionHelper;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 55
    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/utils/ErrorHelper;

    iput-object v0, p0, Lcom/google/android/videos/ui/VideoCollectionHelper;->errorHelper:Lcom/google/android/videos/utils/ErrorHelper;

    .line 56
    invoke-static {p1, p0}, Lcom/google/android/videos/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/ActivityCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/VideoCollectionHelper;->collectionsCallback:Lcom/google/android/videos/async/Callback;

    .line 57
    return-void
.end method


# virtual methods
.method public init(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 65
    .local p0, "this":Lcom/google/android/videos/ui/VideoCollectionHelper;, "Lcom/google/android/videos/ui/VideoCollectionHelper<TT;>;"
    .local p1, "request":Ljava/lang/Object;, "TT;"
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/VideoCollectionHelper;->currentRequest:Ljava/lang/Object;

    .line 66
    iget-object v0, p0, Lcom/google/android/videos/ui/VideoCollectionHelper;->collectionsRequester:Lcom/google/android/videos/async/Requester;

    iget-object v1, p0, Lcom/google/android/videos/ui/VideoCollectionHelper;->currentRequest:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/videos/ui/VideoCollectionHelper;->collectionsCallback:Lcom/google/android/videos/async/Callback;

    invoke-interface {v0, v1, v2}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 67
    return-void
.end method

.method public onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 3
    .param p2, "exception"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    .prologue
    .line 94
    .local p0, "this":Lcom/google/android/videos/ui/VideoCollectionHelper;, "Lcom/google/android/videos/ui/VideoCollectionHelper<TT;>;"
    .local p1, "request":Ljava/lang/Object;, "TT;"
    iget-object v1, p0, Lcom/google/android/videos/ui/VideoCollectionHelper;->currentRequest:Ljava/lang/Object;

    if-eq p1, v1, :cond_0

    .line 101
    :goto_0
    return-void

    .line 97
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/videos/ui/VideoCollectionHelper;->currentRequest:Ljava/lang/Object;

    .line 98
    iget-object v1, p0, Lcom/google/android/videos/ui/VideoCollectionHelper;->errorHelper:Lcom/google/android/videos/utils/ErrorHelper;

    invoke-virtual {v1, p2}, Lcom/google/android/videos/utils/ErrorHelper;->getHumanizedRepresentation(Ljava/lang/Throwable;)Landroid/util/Pair;

    move-result-object v0

    .line 99
    .local v0, "humanized":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v2, p0, Lcom/google/android/videos/ui/VideoCollectionHelper;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v2, v1}, Lcom/google/android/videos/logging/EventLogger;->onSuggestionsError(I)V

    .line 100
    iget-object v2, p0, Lcom/google/android/videos/ui/VideoCollectionHelper;->listener:Lcom/google/android/videos/ui/VideoCollectionHelper$Listener;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-interface {v2, v1}, Lcom/google/android/videos/ui/VideoCollectionHelper$Listener;->onCollectionsError(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onResponse(Ljava/lang/Object;Lcom/google/wireless/android/video/magma/proto/VideoCollectionListResponse;)V
    .locals 2
    .param p2, "response"    # Lcom/google/wireless/android/video/magma/proto/VideoCollectionListResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/google/wireless/android/video/magma/proto/VideoCollectionListResponse;",
            ")V"
        }
    .end annotation

    .prologue
    .line 85
    .local p0, "this":Lcom/google/android/videos/ui/VideoCollectionHelper;, "Lcom/google/android/videos/ui/VideoCollectionHelper<TT;>;"
    .local p1, "request":Ljava/lang/Object;, "TT;"
    iget-object v0, p0, Lcom/google/android/videos/ui/VideoCollectionHelper;->currentRequest:Ljava/lang/Object;

    if-eq p1, v0, :cond_0

    .line 90
    :goto_0
    return-void

    .line 88
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/ui/VideoCollectionHelper;->currentRequest:Ljava/lang/Object;

    .line 89
    iget-object v0, p0, Lcom/google/android/videos/ui/VideoCollectionHelper;->listener:Lcom/google/android/videos/ui/VideoCollectionHelper$Listener;

    iget-object v1, p2, Lcom/google/wireless/android/video/magma/proto/VideoCollectionListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    invoke-interface {v0, v1}, Lcom/google/android/videos/ui/VideoCollectionHelper$Listener;->onCollectionsAvailable([Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;)V

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 20
    .local p0, "this":Lcom/google/android/videos/ui/VideoCollectionHelper;, "Lcom/google/android/videos/ui/VideoCollectionHelper<TT;>;"
    check-cast p2, Lcom/google/wireless/android/video/magma/proto/VideoCollectionListResponse;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/VideoCollectionHelper;->onResponse(Ljava/lang/Object;Lcom/google/wireless/android/video/magma/proto/VideoCollectionListResponse;)V

    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 73
    .local p0, "this":Lcom/google/android/videos/ui/VideoCollectionHelper;, "Lcom/google/android/videos/ui/VideoCollectionHelper<TT;>;"
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/ui/VideoCollectionHelper;->currentRequest:Ljava/lang/Object;

    .line 74
    return-void
.end method
