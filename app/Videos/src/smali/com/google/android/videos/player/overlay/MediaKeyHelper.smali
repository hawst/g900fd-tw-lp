.class public Lcom/google/android/videos/player/overlay/MediaKeyHelper;
.super Ljava/lang/Object;
.source "MediaKeyHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/overlay/MediaKeyHelper$Listener;
    }
.end annotation


# instance fields
.field private currentTime:I

.field private keyInitiatingScrubbing:I

.field private final listener:Lcom/google/android/videos/player/overlay/MediaKeyHelper$Listener;

.field private scrubbingTime:I

.field private totalTime:I


# direct methods
.method public constructor <init>(Lcom/google/android/videos/player/overlay/MediaKeyHelper$Listener;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/videos/player/overlay/MediaKeyHelper$Listener;

    .prologue
    const/high16 v1, -0x80000000

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/player/overlay/MediaKeyHelper$Listener;

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->listener:Lcom/google/android/videos/player/overlay/MediaKeyHelper$Listener;

    .line 40
    iput v1, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->currentTime:I

    .line 41
    iput v1, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->totalTime:I

    .line 42
    iput v1, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->scrubbingTime:I

    .line 43
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->keyInitiatingScrubbing:I

    .line 44
    return-void
.end method

.method private static isHandledMediaKey(I)Z
    .locals 1
    .param p0, "keyCode"    # I

    .prologue
    .line 119
    const/16 v0, 0x4f

    if-eq p0, v0, :cond_0

    const/16 v0, 0x55

    if-eq p0, v0, :cond_0

    const/16 v0, 0x56

    if-eq p0, v0, :cond_0

    const/16 v0, 0x7e

    if-eq p0, v0, :cond_0

    const/16 v0, 0x7f

    if-eq p0, v0, :cond_0

    const/16 v0, 0xaf

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isMediaKey(I)Z
    .locals 1
    .param p0, "keyCode"    # I

    .prologue
    .line 128
    const/16 v0, 0x5a

    if-eq p0, v0, :cond_0

    const/16 v0, 0x57

    if-eq p0, v0, :cond_0

    const/16 v0, 0x55

    if-eq p0, v0, :cond_0

    const/16 v0, 0x58

    if-eq p0, v0, :cond_0

    const/16 v0, 0x59

    if-eq p0, v0, :cond_0

    const/16 v0, 0x56

    if-eq p0, v0, :cond_0

    const/16 v0, 0x7e

    if-eq p0, v0, :cond_0

    const/16 v0, 0x7f

    if-eq p0, v0, :cond_0

    const/16 v0, 0x82

    if-eq p0, v0, :cond_0

    const/16 v0, 0x4f

    if-eq p0, v0, :cond_0

    const/16 v0, 0xaf

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v4, 0x59

    const/4 v2, 0x0

    const/high16 v3, -0x80000000

    const/4 v1, 0x1

    .line 52
    iget v0, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->scrubbingTime:I

    if-eq v0, v3, :cond_0

    iget v0, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->keyInitiatingScrubbing:I

    if-eq p1, v0, :cond_0

    move v0, v1

    .line 96
    :goto_0
    return v0

    .line 56
    :cond_0
    if-eq p1, v4, :cond_1

    const/16 v0, 0x5a

    if-ne p1, v0, :cond_5

    .line 58
    :cond_1
    iget v0, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->currentTime:I

    if-eq v0, v3, :cond_3

    iget v0, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->totalTime:I

    if-eq v0, v3, :cond_3

    .line 59
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 60
    iget v0, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->currentTime:I

    iput v0, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->scrubbingTime:I

    .line 61
    iput p1, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->keyInitiatingScrubbing:I

    .line 62
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->listener:Lcom/google/android/videos/player/overlay/MediaKeyHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/MediaKeyHelper$Listener;->onScrubbingStart()V

    .line 64
    :cond_2
    iget v3, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->scrubbingTime:I

    if-ne p1, v4, :cond_4

    const/16 v0, -0x4e20

    :goto_1
    add-int/2addr v0, v3

    iput v0, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->scrubbingTime:I

    .line 66
    iget v0, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->scrubbingTime:I

    iget v3, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->totalTime:I

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->scrubbingTime:I

    .line 67
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->listener:Lcom/google/android/videos/player/overlay/MediaKeyHelper$Listener;

    iget v2, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->scrubbingTime:I

    invoke-interface {v0, v2}, Lcom/google/android/videos/player/overlay/MediaKeyHelper$Listener;->onUpdateScrubberTime(I)V

    :cond_3
    move v0, v1

    .line 69
    goto :goto_0

    .line 64
    :cond_4
    const/16 v0, 0x4e20

    goto :goto_1

    .line 70
    :cond_5
    invoke-static {p1}, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->isHandledMediaKey(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 72
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-lez v0, :cond_6

    move v0, v1

    .line 74
    goto :goto_0

    .line 76
    :cond_6
    sparse-switch p1, :sswitch_data_0

    :goto_2
    move v0, v1

    .line 94
    goto :goto_0

    .line 79
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->listener:Lcom/google/android/videos/player/overlay/MediaKeyHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/MediaKeyHelper$Listener;->onTogglePlayPause()V

    goto :goto_2

    .line 82
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->listener:Lcom/google/android/videos/player/overlay/MediaKeyHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/MediaKeyHelper$Listener;->onPlay()V

    goto :goto_2

    .line 86
    :sswitch_2
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->listener:Lcom/google/android/videos/player/overlay/MediaKeyHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/MediaKeyHelper$Listener;->onPause()V

    goto :goto_2

    .line 89
    :sswitch_3
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->listener:Lcom/google/android/videos/player/overlay/MediaKeyHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/MediaKeyHelper$Listener;->onCC()V

    goto :goto_2

    :cond_7
    move v0, v2

    .line 96
    goto :goto_0

    .line 76
    nop

    :sswitch_data_0
    .sparse-switch
        0x4f -> :sswitch_0
        0x55 -> :sswitch_0
        0x56 -> :sswitch_2
        0x7e -> :sswitch_1
        0x7f -> :sswitch_2
        0xaf -> :sswitch_3
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/high16 v4, -0x80000000

    .line 100
    const/16 v2, 0x59

    if-eq p1, v2, :cond_0

    const/16 v2, 0x5a

    if-ne p1, v2, :cond_2

    .line 102
    :cond_0
    iget v2, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->scrubbingTime:I

    if-eq v2, v4, :cond_1

    iget v2, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->keyInitiatingScrubbing:I

    if-ne p1, v2, :cond_1

    .line 105
    iget-object v2, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->listener:Lcom/google/android/videos/player/overlay/MediaKeyHelper$Listener;

    iget v3, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->scrubbingTime:I

    invoke-interface {v2, v3}, Lcom/google/android/videos/player/overlay/MediaKeyHelper$Listener;->onScrubbingEnd(I)V

    .line 106
    iput v4, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->scrubbingTime:I

    .line 107
    iput v1, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->keyInitiatingScrubbing:I

    .line 115
    :cond_1
    :goto_0
    return v0

    .line 111
    :cond_2
    invoke-static {p1}, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->isHandledMediaKey(I)Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    .line 115
    goto :goto_0
.end method

.method public setTimes(III)V
    .locals 0
    .param p1, "currentTime"    # I
    .param p2, "totalTime"    # I
    .param p3, "bufferedTime"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->currentTime:I

    .line 48
    iput p2, p0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->totalTime:I

    .line 49
    return-void
.end method
