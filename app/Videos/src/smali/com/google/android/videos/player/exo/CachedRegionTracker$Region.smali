.class Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;
.super Ljava/lang/Object;
.source "CachedRegionTracker.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/exo/CachedRegionTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Region"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;",
        ">;"
    }
.end annotation


# instance fields
.field public endIndex:I

.field public endOffset:J

.field public startOffset:J


# direct methods
.method public constructor <init>(JJ)V
    .locals 1
    .param p1, "position"    # J
    .param p3, "length"    # J

    .prologue
    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165
    iput-wide p1, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;->startOffset:J

    .line 166
    iput-wide p3, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;->endOffset:J

    .line 167
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;)I
    .locals 4
    .param p1, "another"    # Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;

    .prologue
    .line 171
    iget-wide v0, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;->startOffset:J

    iget-wide v2, p1, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;->startOffset:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 148
    check-cast p1, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;->compareTo(Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;)I

    move-result v0

    return v0
.end method
