.class Lcom/google/android/videos/activity/SettingsActivity$SyncUserConfigurationCallback;
.super Ljava/lang/Object;
.source "SettingsActivity.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/activity/SettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncUserConfigurationCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/activity/SettingsActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/activity/SettingsActivity;)V
    .locals 0

    .prologue
    .line 370
    iput-object p1, p0, Lcom/google/android/videos/activity/SettingsActivity$SyncUserConfigurationCallback;->this$0:Lcom/google/android/videos/activity/SettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/activity/SettingsActivity;Lcom/google/android/videos/activity/SettingsActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/activity/SettingsActivity;
    .param p2, "x1"    # Lcom/google/android/videos/activity/SettingsActivity$1;

    .prologue
    .line 370
    invoke-direct {p0, p1}, Lcom/google/android/videos/activity/SettingsActivity$SyncUserConfigurationCallback;-><init>(Lcom/google/android/videos/activity/SettingsActivity;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 370
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/activity/SettingsActivity$SyncUserConfigurationCallback;->onError(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public onError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "request"    # Ljava/lang/String;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 380
    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity$SyncUserConfigurationCallback;->this$0:Lcom/google/android/videos/activity/SettingsActivity;

    # invokes: Lcom/google/android/videos/activity/SettingsActivity;->dismissInProgressDialog()V
    invoke-static {v0}, Lcom/google/android/videos/activity/SettingsActivity;->access$300(Lcom/google/android/videos/activity/SettingsActivity;)V

    .line 381
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 370
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/lang/Void;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/activity/SettingsActivity$SyncUserConfigurationCallback;->onResponse(Ljava/lang/String;Ljava/lang/Void;)V

    return-void
.end method

.method public onResponse(Ljava/lang/String;Ljava/lang/Void;)V
    .locals 1
    .param p1, "request"    # Ljava/lang/String;
    .param p2, "response"    # Ljava/lang/Void;

    .prologue
    .line 374
    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity$SyncUserConfigurationCallback;->this$0:Lcom/google/android/videos/activity/SettingsActivity;

    # invokes: Lcom/google/android/videos/activity/SettingsActivity;->dismissInProgressDialog()V
    invoke-static {v0}, Lcom/google/android/videos/activity/SettingsActivity;->access$300(Lcom/google/android/videos/activity/SettingsActivity;)V

    .line 375
    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity$SyncUserConfigurationCallback;->this$0:Lcom/google/android/videos/activity/SettingsActivity;

    # invokes: Lcom/google/android/videos/activity/SettingsActivity;->onUserConfigurationChanged()V
    invoke-static {v0}, Lcom/google/android/videos/activity/SettingsActivity;->access$500(Lcom/google/android/videos/activity/SettingsActivity;)V

    .line 376
    return-void
.end method
