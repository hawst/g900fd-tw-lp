.class final Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$RelatedItem;
.super Lcom/google/android/videos/pano/model/Item;
.source "BaseDetailsRowHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "RelatedItem"
.end annotation


# instance fields
.field private final asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

.field private final posterHeight:I

.field private final posterWidth:I

.field final synthetic this$0:Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;Lcom/google/wireless/android/video/magma/proto/AssetResource;II)V
    .locals 0
    .param p2, "asset"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p3, "posterHeight"    # I
    .param p4, "posterWidth"    # I

    .prologue
    .line 407
    iput-object p1, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$RelatedItem;->this$0:Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;

    invoke-direct {p0}, Lcom/google/android/videos/pano/model/Item;-><init>()V

    .line 408
    iput-object p2, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$RelatedItem;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 409
    iput p3, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$RelatedItem;->posterHeight:I

    .line 410
    iput p4, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$RelatedItem;->posterWidth:I

    .line 411
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;Lcom/google/wireless/android/video/magma/proto/AssetResource;IILcom/google/android/videos/pano/ui/BaseDetailsRowHelper$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;
    .param p2, "x1"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p3, "x2"    # I
    .param p4, "x3"    # I
    .param p5, "x4"    # Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$1;

    .prologue
    .line 401
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$RelatedItem;-><init>(Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;Lcom/google/wireless/android/video/magma/proto/AssetResource;II)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 464
    if-ne p1, p0, :cond_1

    .line 465
    const/4 v1, 0x1

    .line 476
    :cond_0
    :goto_0
    return v1

    .line 467
    :cond_1
    instance-of v2, p1, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$RelatedItem;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 470
    check-cast v0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$RelatedItem;

    .line 472
    .local v0, "item":Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$RelatedItem;
    iget-object v2, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$RelatedItem;->this$0:Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;

    iget-object v3, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$RelatedItem;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # invokes: Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->getDetailsIntent(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Landroid/content/Intent;
    invoke-static {v2, v3}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->access$300(Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;Lcom/google/wireless/android/video/magma/proto/AssetResource;)Landroid/content/Intent;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$RelatedItem;->this$0:Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;

    iget-object v4, v0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$RelatedItem;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # invokes: Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->getDetailsIntent(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Landroid/content/Intent;
    invoke-static {v3, v4}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->access$300(Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;Lcom/google/wireless/android/video/magma/proto/AssetResource;)Landroid/content/Intent;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/videos/utils/Util;->areEqual(Landroid/content/Intent;Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 476
    invoke-super {p0, p1}, Lcom/google/android/videos/pano/model/Item;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getCheapestOffer()Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    .locals 2

    .prologue
    .line 454
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$RelatedItem;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v1, v1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/OfferUtil;->getCheapestOffer(Z[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;)Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    move-result-object v0

    return-object v0
.end method

.method public getDurationInSeconds()I
    .locals 1

    .prologue
    .line 459
    const/4 v0, -0x1

    return v0
.end method

.method public getImageHeight()I
    .locals 1

    .prologue
    .line 444
    iget v0, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$RelatedItem;->posterHeight:I

    return v0
.end method

.method public getImageUri()Ljava/lang/String;
    .locals 2

    .prologue
    .line 420
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$RelatedItem;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v1, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$RelatedItem;->this$0:Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;

    iget-object v1, v1, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->context:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/google/android/videos/pano/ui/PanoHelper;->getAssetPoster(Lcom/google/wireless/android/video/magma/proto/AssetResource;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getImageWidth()I
    .locals 1

    .prologue
    .line 439
    iget v0, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$RelatedItem;->posterWidth:I

    return v0
.end method

.method public getStarRating()F
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$RelatedItem;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->viewerRating:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    invoke-static {v0}, Lcom/google/android/videos/pano/model/VideoItem;->getStarRating([Lcom/google/wireless/android/video/magma/proto/ViewerRating;)F

    move-result v0

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 415
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$RelatedItem;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title:Ljava/lang/String;

    return-object v0
.end method

.method public onClick(Landroid/app/Activity;Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "viewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    .prologue
    .line 426
    const/4 v0, 0x0

    .line 427
    .local v0, "bundle":Landroid/os/Bundle;
    iget-object v2, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$RelatedItem;->this$0:Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;

    iget-object v3, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$RelatedItem;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # invokes: Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->getDetailsIntent(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Landroid/content/Intent;
    invoke-static {v2, v3}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->access$300(Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;Lcom/google/wireless/android/video/magma/proto/AssetResource;)Landroid/content/Intent;

    move-result-object v1

    .line 428
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p2, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    instance-of v2, v2, Lcom/google/android/videos/pano/ui/VideoCardView;

    if-eqz v2, :cond_0

    .line 429
    iget-object v2, p2, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    check-cast v2, Lcom/google/android/videos/pano/ui/VideoCardView;

    invoke-virtual {v2}, Lcom/google/android/videos/pano/ui/VideoCardView;->getImageView()Landroid/widget/ImageView;

    move-result-object v2

    const-string v3, "hero"

    invoke-static {p1, v2, v3}, Landroid/support/v4/app/ActivityOptionsCompat;->makeSceneTransitionAnimation(Landroid/app/Activity;Landroid/view/View;Ljava/lang/String;)Landroid/support/v4/app/ActivityOptionsCompat;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/ActivityOptionsCompat;->toBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 432
    invoke-static {v1}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->markAsRunningAnimation(Landroid/content/Intent;)V

    .line 434
    :cond_0
    invoke-virtual {p1, v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 435
    return-void
.end method
