.class Lcom/google/android/videos/cache/PersistentCache$4;
.super Ljava/lang/Object;
.source "PersistentCache.java"

# interfaces
.implements Ljava/io/FilenameFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/cache/PersistentCache;-><init>(Lcom/google/android/videos/cache/Converter;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/cache/PersistentCache;

.field final synthetic val$suffix:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/videos/cache/PersistentCache;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 181
    .local p0, "this":Lcom/google/android/videos/cache/PersistentCache$4;, "Lcom/google/android/videos/cache/PersistentCache.4;"
    iput-object p1, p0, Lcom/google/android/videos/cache/PersistentCache$4;->this$0:Lcom/google/android/videos/cache/PersistentCache;

    iput-object p2, p0, Lcom/google/android/videos/cache/PersistentCache$4;->val$suffix:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public accept(Ljava/io/File;Ljava/lang/String;)Z
    .locals 2
    .param p1, "dir"    # Ljava/io/File;
    .param p2, "filename"    # Ljava/lang/String;

    .prologue
    .line 184
    .local p0, "this":Lcom/google/android/videos/cache/PersistentCache$4;, "Lcom/google/android/videos/cache/PersistentCache.4;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/videos/cache/PersistentCache$4;->val$suffix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".cache"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
