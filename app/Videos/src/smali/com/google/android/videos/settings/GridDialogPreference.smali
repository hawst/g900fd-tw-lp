.class public abstract Lcom/google/android/videos/settings/GridDialogPreference;
.super Landroid/preference/DialogPreference;
.source "GridDialogPreference.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/settings/GridDialogPreference$SavedState;,
        Lcom/google/android/videos/settings/GridDialogPreference$ListPreferenceAdapter;
    }
.end annotation


# instance fields
.field private mEntryTitles:[Ljava/lang/CharSequence;

.field private mEntryValues:[I

.field private mGridItemLayout:I

.field private mSummary:Ljava/lang/CharSequence;

.field private mValue:I

.field private mValueIndex:I

.field private mValueSet:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v4, 0x0

    .line 50
    invoke-direct {p0, p1, p2}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    const/4 v3, 0x2

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-virtual {p1, p2, v3, v4, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 53
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/settings/GridDialogPreference;->mEntryTitles:[Ljava/lang/CharSequence;

    .line 54
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    .line 55
    .local v1, "entryValuesText":[Ljava/lang/CharSequence;
    array-length v3, v1

    new-array v3, v3, [I

    iput-object v3, p0, Lcom/google/android/videos/settings/GridDialogPreference;->mEntryValues:[I

    .line 56
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/videos/settings/GridDialogPreference;->mEntryValues:[I

    array-length v3, v3

    if-ge v2, v3, :cond_0

    .line 57
    iget-object v3, p0, Lcom/google/android/videos/settings/GridDialogPreference;->mEntryValues:[I

    aget-object v4, v1, v2

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/videos/settings/GridDialogPreference;->decodeStringValue(Ljava/lang/String;)I

    move-result v4

    aput v4, v3, v2

    .line 56
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 59
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 60
    return-void

    .line 51
    nop

    :array_0
    .array-data 4
        0x10100b2
        0x10101f8
    .end array-data
.end method

.method static synthetic access$100(Lcom/google/android/videos/settings/GridDialogPreference;Ljava/lang/Object;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/settings/GridDialogPreference;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 29
    invoke-virtual {p0, p1}, Lcom/google/android/videos/settings/GridDialogPreference;->callChangeListener(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/settings/GridDialogPreference;)[I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/settings/GridDialogPreference;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/videos/settings/GridDialogPreference;->mEntryValues:[I

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/settings/GridDialogPreference;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/settings/GridDialogPreference;

    .prologue
    .line 29
    iget v0, p0, Lcom/google/android/videos/settings/GridDialogPreference;->mGridItemLayout:I

    return v0
.end method


# virtual methods
.method protected abstract decodeStringValue(Ljava/lang/String;)I
.end method

.method protected getIndexForValue(I)I
    .locals 4
    .param p1, "value"    # I

    .prologue
    .line 200
    iget-object v2, p0, Lcom/google/android/videos/settings/GridDialogPreference;->mEntryValues:[I

    .line 201
    .local v2, "values":[I
    array-length v0, v2

    .line 202
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 203
    aget v3, v2, v1

    if-ne v3, p1, :cond_0

    .line 208
    .end local v1    # "i":I
    :goto_1
    return v1

    .line 202
    .restart local v1    # "i":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 208
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public getSelectedValueTitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 139
    iget v0, p0, Lcom/google/android/videos/settings/GridDialogPreference;->mValueIndex:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/videos/settings/GridDialogPreference;->mValueIndex:I

    invoke-virtual {p0, v0}, Lcom/google/android/videos/settings/GridDialogPreference;->getTitleAt(I)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSummary()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/videos/settings/GridDialogPreference;->mSummary:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/settings/GridDialogPreference;->mSummary:Ljava/lang/CharSequence;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/settings/GridDialogPreference;->getSelectedValueTitle()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method protected getTitleAt(I)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/videos/settings/GridDialogPreference;->mEntryTitles:[Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/settings/GridDialogPreference;->mEntryTitles:[Ljava/lang/CharSequence;

    array-length v0, v0

    if-gt v0, p1, :cond_1

    .line 129
    :cond_0
    const/4 v0, 0x0

    .line 132
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/settings/GridDialogPreference;->mEntryTitles:[Ljava/lang/CharSequence;

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public getTitles()[Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/videos/settings/GridDialogPreference;->mEntryTitles:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getValue()I
    .locals 1

    .prologue
    .line 235
    iget v0, p0, Lcom/google/android/videos/settings/GridDialogPreference;->mValue:I

    return v0
.end method

.method protected getValueAt(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/videos/settings/GridDialogPreference;->mEntryValues:[I

    aget v0, v0, p1

    return v0
.end method

.method public getValues()[I
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/videos/settings/GridDialogPreference;->mEntryValues:[I

    return-object v0
.end method

.method protected abstract onBindGridItem(Landroid/view/View;I)V
.end method

.method protected onGetDefaultValue(Landroid/content/res/TypedArray;I)Ljava/lang/Object;
    .locals 1
    .param p1, "a"    # Landroid/content/res/TypedArray;
    .param p2, "index"    # I

    .prologue
    .line 240
    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/settings/GridDialogPreference;->decodeStringValue(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
    .locals 6
    .param p1, "builder"    # Landroid/app/AlertDialog$Builder;

    .prologue
    const/4 v5, 0x0

    .line 166
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V

    .line 168
    invoke-virtual {p0}, Lcom/google/android/videos/settings/GridDialogPreference;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/videos/settings/GridDialogPreference;->getDialogLayoutResource()I

    move-result v4

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 169
    .local v1, "picker":Landroid/view/View;
    const v3, 0x7f0f011b

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    .line 170
    .local v0, "grid":Landroid/widget/GridView;
    new-instance v3, Lcom/google/android/videos/settings/GridDialogPreference$ListPreferenceAdapter;

    invoke-direct {v3, p0, v5}, Lcom/google/android/videos/settings/GridDialogPreference$ListPreferenceAdapter;-><init>(Lcom/google/android/videos/settings/GridDialogPreference;Lcom/google/android/videos/settings/GridDialogPreference$1;)V

    invoke-virtual {v0, v3}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 171
    new-instance v3, Lcom/google/android/videos/settings/GridDialogPreference$1;

    invoke-direct {v3, p0}, Lcom/google/android/videos/settings/GridDialogPreference$1;-><init>(Lcom/google/android/videos/settings/GridDialogPreference;)V

    invoke-virtual {v0, v3}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 186
    iget v3, p0, Lcom/google/android/videos/settings/GridDialogPreference;->mValue:I

    invoke-virtual {p0, v3}, Lcom/google/android/videos/settings/GridDialogPreference;->getIndexForValue(I)I

    move-result v2

    .line 187
    .local v2, "selectedPosition":I
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 188
    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setSelection(I)V

    .line 190
    :cond_0
    invoke-virtual {p1, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 191
    invoke-virtual {p1, v5, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 193
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 263
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/google/android/videos/settings/GridDialogPreference$SavedState;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 265
    :cond_0
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 272
    :goto_0
    return-void

    :cond_1
    move-object v0, p1

    .line 269
    check-cast v0, Lcom/google/android/videos/settings/GridDialogPreference$SavedState;

    .line 270
    .local v0, "myState":Lcom/google/android/videos/settings/GridDialogPreference$SavedState;
    invoke-virtual {v0}, Lcom/google/android/videos/settings/GridDialogPreference$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/preference/DialogPreference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 271
    iget v1, v0, Lcom/google/android/videos/settings/GridDialogPreference$SavedState;->value:I

    invoke-virtual {p0, v1}, Lcom/google/android/videos/settings/GridDialogPreference;->setValue(I)V

    goto :goto_0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 250
    invoke-super {p0}, Landroid/preference/DialogPreference;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    .line 251
    .local v1, "superState":Landroid/os/Parcelable;
    invoke-virtual {p0}, Lcom/google/android/videos/settings/GridDialogPreference;->isPersistent()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 258
    .end local v1    # "superState":Landroid/os/Parcelable;
    :goto_0
    return-object v1

    .line 256
    .restart local v1    # "superState":Landroid/os/Parcelable;
    :cond_0
    new-instance v0, Lcom/google/android/videos/settings/GridDialogPreference$SavedState;

    invoke-direct {v0, v1}, Lcom/google/android/videos/settings/GridDialogPreference$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 257
    .local v0, "myState":Lcom/google/android/videos/settings/GridDialogPreference$SavedState;
    invoke-virtual {p0}, Lcom/google/android/videos/settings/GridDialogPreference;->getValue()I

    move-result v2

    iput v2, v0, Lcom/google/android/videos/settings/GridDialogPreference$SavedState;->value:I

    move-object v1, v0

    .line 258
    goto :goto_0
.end method

.method protected onSetInitialValue(ZLjava/lang/Object;)V
    .locals 1
    .param p1, "restoreValue"    # Z
    .param p2, "defaultValue"    # Ljava/lang/Object;

    .prologue
    .line 245
    if-eqz p1, :cond_0

    iget v0, p0, Lcom/google/android/videos/settings/GridDialogPreference;->mValue:I

    invoke-virtual {p0, v0}, Lcom/google/android/videos/settings/GridDialogPreference;->getPersistedInt(I)I

    move-result v0

    .end local p2    # "defaultValue":Ljava/lang/Object;
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/videos/settings/GridDialogPreference;->setValue(I)V

    .line 246
    return-void

    .line 245
    .restart local p2    # "defaultValue":Ljava/lang/Object;
    :cond_0
    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "defaultValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public setGridItemLayoutResource(I)V
    .locals 0
    .param p1, "layoutResId"    # I

    .prologue
    .line 68
    iput p1, p0, Lcom/google/android/videos/settings/GridDialogPreference;->mGridItemLayout:I

    .line 69
    return-void
.end method

.method public setSummary(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "summary"    # Ljava/lang/CharSequence;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/google/android/videos/settings/GridDialogPreference;->mSummary:Ljava/lang/CharSequence;

    .line 161
    invoke-virtual {p0}, Lcom/google/android/videos/settings/GridDialogPreference;->notifyChanged()V

    .line 162
    return-void
.end method

.method public setTitles([Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "titles"    # [Ljava/lang/CharSequence;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/google/android/videos/settings/GridDialogPreference;->mEntryTitles:[Ljava/lang/CharSequence;

    .line 96
    return-void
.end method

.method public setValue(I)V
    .locals 3
    .param p1, "value"    # I

    .prologue
    const/4 v1, 0x1

    .line 218
    iget v2, p0, Lcom/google/android/videos/settings/GridDialogPreference;->mValue:I

    if-eq v2, p1, :cond_2

    move v0, v1

    .line 219
    .local v0, "changed":Z
    :goto_0
    if-nez v0, :cond_0

    iget-boolean v2, p0, Lcom/google/android/videos/settings/GridDialogPreference;->mValueSet:Z

    if-nez v2, :cond_1

    .line 220
    :cond_0
    iput p1, p0, Lcom/google/android/videos/settings/GridDialogPreference;->mValue:I

    .line 221
    invoke-virtual {p0, p1}, Lcom/google/android/videos/settings/GridDialogPreference;->getIndexForValue(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/videos/settings/GridDialogPreference;->mValueIndex:I

    .line 222
    iput-boolean v1, p0, Lcom/google/android/videos/settings/GridDialogPreference;->mValueSet:Z

    .line 223
    invoke-virtual {p0, p1}, Lcom/google/android/videos/settings/GridDialogPreference;->persistInt(I)Z

    .line 224
    if-eqz v0, :cond_1

    .line 225
    invoke-virtual {p0}, Lcom/google/android/videos/settings/GridDialogPreference;->shouldDisableDependents()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/videos/settings/GridDialogPreference;->notifyDependencyChange(Z)V

    .line 226
    invoke-virtual {p0}, Lcom/google/android/videos/settings/GridDialogPreference;->notifyChanged()V

    .line 229
    :cond_1
    return-void

    .line 218
    .end local v0    # "changed":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setValues([I)V
    .locals 1
    .param p1, "values"    # [I

    .prologue
    .line 77
    iput-object p1, p0, Lcom/google/android/videos/settings/GridDialogPreference;->mEntryValues:[I

    .line 78
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/settings/GridDialogPreference;->mValueSet:Z

    .line 79
    return-void
.end method
