.class public Lcom/google/android/videos/search/VideoSearchProvider;
.super Landroid/content/ContentProvider;
.source "VideoSearchProvider.java"


# static fields
.field protected static final PROJECTION:[Ljava/lang/String;

.field static final SUGGEST_THUMBNAIL_URI:Landroid/net/Uri;

.field private static final URI_MATCHER:Landroid/content/UriMatcher;


# instance fields
.field private bitmapConverter:Lcom/google/android/videos/utils/BytesToPipeConverter;

.field private purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

.field private syncBitmapBytesCacheRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;"
        }
    .end annotation
.end field

.field private videosGlobals:Lcom/google/android/videos/VideosGlobals;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 46
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "content"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "com.google.android.videos"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "thumbnail/suggest"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/videos/search/VideoSearchProvider;->SUGGEST_THUMBNAIL_URI:Landroid/net/Uri;

    .line 59
    invoke-static {}, Lcom/google/android/videos/search/VideoSearchProvider;->createUriMatcher()Landroid/content/UriMatcher;

    move-result-object v0

    sput-object v0, Lcom/google/android/videos/search/VideoSearchProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    .line 60
    invoke-static {}, Lcom/google/android/videos/search/VideoSearchProvider;->createSearchProjection()[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/videos/search/VideoSearchProvider;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method private static createSearchProjection()[Ljava/lang/String;
    .locals 8

    .prologue
    .line 232
    new-instance v4, Landroid/net/Uri$Builder;

    invoke-direct {v4}, Landroid/net/Uri$Builder;-><init>()V

    const-string v5, "content"

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "com.google.android.videos"

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "thumbnail"

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 237
    .local v0, "baseThumbnailUriString":Ljava/lang/String;
    const-string v1, "http://www.youtube.com/watch/?v="

    .line 238
    .local v1, "baseWatchUriString":Ljava/lang/String;
    const-string v2, "&season_id=tvseason-"

    .line 239
    .local v2, "seasonParameterPrefix":Ljava/lang/String;
    const-string v3, "max(purchase_timestamp_seconds * 1000,last_watched_timestamp)"

    .line 242
    .local v3, "timestampFunction":Ljava/lang/String;
    const/16 v4, 0x8

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "video_id AS _id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "title AS suggest_text_1"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/\' || "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "video_id"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AS "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "suggest_icon_1"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "\'android.intent.action.VIEW\' AS suggest_intent_action"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\' || "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "video_id"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " || ifnull(\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\' || "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "episode_season_id"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", \'\')"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AS "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "suggest_intent_data"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x5

    const-string v6, "account AS suggest_intent_extra_data"

    aput-object v6, v4, v5

    const/4 v5, 0x6

    const-string v6, "video_id AS suggest_shortcut_id"

    aput-object v6, v4, v5

    const/4 v5, 0x7

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AS "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "suggest_last_access_hint"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    return-object v4
.end method

.method private static createUriMatcher()Landroid/content/UriMatcher;
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 258
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    .line 259
    .local v0, "matcher":Landroid/content/UriMatcher;
    const-string v1, "com.google.android.videos"

    const-string v2, "search_suggest_query"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 260
    const-string v1, "com.google.android.videos"

    const-string v2, "search_suggest_query/*"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 261
    const-string v1, "com.google.android.videos"

    const-string v2, "search_suggest_shortcut/*"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 262
    const-string v1, "com.google.android.videos"

    const-string v2, "thumbnail/show/*"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 263
    const-string v1, "com.google.android.videos"

    const-string v2, "thumbnail/suggest/*"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 264
    const-string v1, "com.google.android.videos"

    const-string v2, "thumbnail/*"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 265
    return-object v0
.end method

.method private enforceGlobalSearchPermssion()V
    .locals 3

    .prologue
    .line 269
    invoke-virtual {p0}, Lcom/google/android/videos/search/VideoSearchProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "android.permission.GLOBAL_SEARCH"

    const-string v2, "insufficient permission"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    return-void
.end method

.method private getLimit(Landroid/net/Uri;)I
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 205
    const-string v0, "limit"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Util;->parseInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private getPurchaseCursor(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Landroid/database/Cursor;
    .locals 5
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .prologue
    .line 156
    :try_start_0
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v0

    .line 157
    .local v0, "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;>;"
    iget-object v3, p0, Lcom/google/android/videos/search/VideoSearchProvider;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    invoke-virtual {v3, p1, v0}, Lcom/google/android/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/videos/async/Callback;)V

    .line 158
    invoke-virtual {v0}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 167
    .end local v0    # "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;>;"
    :goto_0
    return-object v3

    .line 159
    :catch_0
    move-exception v2

    .line 160
    .local v2, "ee":Ljava/util/concurrent/ExecutionException;
    invoke-virtual {v2}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    instance-of v3, v3, Landroid/os/OperationCanceledException;

    if-eqz v3, :cond_0

    .line 161
    invoke-virtual {v2}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    check-cast v3, Landroid/os/OperationCanceledException;

    throw v3

    .line 163
    :cond_0
    const-string v3, "Exception fetching purchases."

    invoke-virtual {v2}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 167
    .end local v2    # "ee":Ljava/util/concurrent/ExecutionException;
    :goto_1
    const/4 v3, 0x0

    goto :goto_0

    .line 164
    :catch_1
    move-exception v1

    .line 165
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "Exception fetching purchases."

    invoke-virtual {v1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public static getSuggestThumbnailPath(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p0, "remoteUri"    # Ljava/lang/String;

    .prologue
    .line 68
    sget-object v0, Lcom/google/android/videos/search/VideoSearchProvider;->SUGGEST_THUMBNAIL_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized init()V
    .locals 2

    .prologue
    .line 118
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/search/VideoSearchProvider;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    if-nez v0, :cond_0

    .line 119
    invoke-virtual {p0}, Lcom/google/android/videos/search/VideoSearchProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/search/VideoSearchProvider;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    .line 120
    iget-object v0, p0, Lcom/google/android/videos/search/VideoSearchProvider;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStore()Lcom/google/android/videos/store/PurchaseStore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/search/VideoSearchProvider;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    .line 121
    iget-object v0, p0, Lcom/google/android/videos/search/VideoSearchProvider;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getBitmapRequesters()Lcom/google/android/videos/bitmap/BitmapRequesters;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/bitmap/BitmapRequesters;->getSyncBitmapBytesMemoryCacheRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v0

    new-instance v1, Lcom/google/android/videos/search/VideoSearchProvider$1;

    invoke-direct {v1, p0}, Lcom/google/android/videos/search/VideoSearchProvider$1;-><init>(Lcom/google/android/videos/search/VideoSearchProvider;)V

    invoke-static {v0, v1}, Lcom/google/android/videos/async/ConvertingRequester;->create(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/converter/RequestConverter;)Lcom/google/android/videos/async/Requester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/search/VideoSearchProvider;->syncBitmapBytesCacheRequester:Lcom/google/android/videos/async/Requester;

    .line 129
    new-instance v0, Lcom/google/android/videos/utils/BytesToPipeConverter;

    iget-object v1, p0, Lcom/google/android/videos/search/VideoSearchProvider;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getNetworkExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/videos/utils/BytesToPipeConverter;-><init>(Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lcom/google/android/videos/search/VideoSearchProvider;->bitmapConverter:Lcom/google/android/videos/utils/BytesToPipeConverter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 131
    :cond_0
    monitor-exit p0

    return-void

    .line 118
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private makeThumbnailRequest(Landroid/net/Uri;)Landroid/os/ParcelFileDescriptor;
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 182
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    .line 183
    .local v2, "itemId":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v0

    .line 184
    .local v0, "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/String;Lcom/google/android/videos/utils/ByteArray;>;"
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    const-string v4, "thumbnail/suggest"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 185
    iget-object v3, p0, Lcom/google/android/videos/search/VideoSearchProvider;->syncBitmapBytesCacheRequester:Lcom/google/android/videos/async/Requester;

    invoke-interface {v3, v2, v0}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 192
    :goto_0
    :try_start_0
    iget-object v4, p0, Lcom/google/android/videos/search/VideoSearchProvider;->bitmapConverter:Lcom/google/android/videos/utils/BytesToPipeConverter;

    invoke-virtual {v0}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/utils/ByteArray;

    invoke-virtual {v4, v3}, Lcom/google/android/videos/utils/BytesToPipeConverter;->convertResponse(Lcom/google/android/videos/utils/ByteArray;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/videos/converter/ConverterException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 198
    :goto_1
    return-object v3

    .line 186
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    const-string v4, "thumbnail/show"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 187
    iget-object v3, p0, Lcom/google/android/videos/search/VideoSearchProvider;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v3}, Lcom/google/android/videos/VideosGlobals;->getPosterStore()Lcom/google/android/videos/store/PosterStore;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2, v0}, Lcom/google/android/videos/store/PosterStore;->getBytes(ILjava/lang/String;Lcom/google/android/videos/async/Callback;)V

    goto :goto_0

    .line 189
    :cond_1
    iget-object v3, p0, Lcom/google/android/videos/search/VideoSearchProvider;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v3}, Lcom/google/android/videos/VideosGlobals;->getPosterStore()Lcom/google/android/videos/store/PosterStore;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v2, v0}, Lcom/google/android/videos/store/PosterStore;->getBytes(ILjava/lang/String;Lcom/google/android/videos/async/Callback;)V

    goto :goto_0

    .line 193
    :catch_0
    move-exception v1

    .line 194
    .local v1, "e":Ljava/util/concurrent/ExecutionException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception fetching icon bytes for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 198
    .end local v1    # "e":Ljava/util/concurrent/ExecutionException;
    :goto_2
    const/4 v3, 0x0

    goto :goto_1

    .line 195
    :catch_1
    move-exception v1

    .line 196
    .local v1, "e":Lcom/google/android/videos/converter/ConverterException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception converting icon bytes for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/videos/converter/ConverterException;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method private queryInternal(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/store/CancellationSignalHolder;)Landroid/database/Cursor;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projectionIn"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;
    .param p6, "cancellationSignalHolder"    # Lcom/google/android/videos/store/CancellationSignalHolder;

    .prologue
    const/4 v1, 0x0

    .line 93
    invoke-direct {p0}, Lcom/google/android/videos/search/VideoSearchProvider;->init()V

    .line 94
    invoke-direct {p0}, Lcom/google/android/videos/search/VideoSearchProvider;->enforceGlobalSearchPermssion()V

    .line 96
    iget-object v2, p0, Lcom/google/android/videos/search/VideoSearchProvider;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/videos/Config;->panoEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 114
    :cond_0
    :goto_0
    return-object v1

    .line 100
    :cond_1
    sget-object v2, Lcom/google/android/videos/search/VideoSearchProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 101
    .local v0, "match":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    const/4 v2, 0x3

    if-eq v0, v2, :cond_0

    .line 105
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 107
    :pswitch_0
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/google/android/videos/search/VideoSearchProvider;->getLimit(Landroid/net/Uri;)I

    move-result v2

    invoke-direct {p0, v1, v2, p6}, Lcom/google/android/videos/search/VideoSearchProvider;->querySearch(Ljava/lang/String;ILcom/google/android/videos/store/CancellationSignalHolder;)Landroid/database/Cursor;

    move-result-object v1

    goto :goto_0

    .line 109
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/videos/search/VideoSearchProvider;->getLimit(Landroid/net/Uri;)I

    move-result v1

    invoke-direct {p0, v1, p6}, Lcom/google/android/videos/search/VideoSearchProvider;->zeroQuerySearch(ILcom/google/android/videos/store/CancellationSignalHolder;)Landroid/database/Cursor;

    move-result-object v1

    goto :goto_0

    .line 111
    :pswitch_2
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/videos/search/VideoSearchProvider;->shortcutCursor(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    goto :goto_0

    .line 105
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private querySearch(Ljava/lang/String;ILcom/google/android/videos/store/CancellationSignalHolder;)Landroid/database/Cursor;
    .locals 7
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "limit"    # I
    .param p3, "cancellationSignalHolder"    # Lcom/google/android/videos/store/CancellationSignalHolder;

    .prologue
    .line 143
    sget-object v1, Lcom/google/android/videos/search/VideoSearchProvider;->PROJECTION:[Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object v2, p1

    move v3, p2

    move-object v6, p3

    invoke-static/range {v1 .. v6}, Lcom/google/android/videos/store/PurchaseRequests;->createActivePurchasesRequestForGlobalSearch([Ljava/lang/String;Ljava/lang/String;IJLcom/google/android/videos/store/CancellationSignalHolder;)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v0

    .line 145
    .local v0, "request":Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    invoke-direct {p0, v0}, Lcom/google/android/videos/search/VideoSearchProvider;->getPurchaseCursor(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method private shortcutCursor(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 4
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 149
    sget-object v1, Lcom/google/android/videos/search/VideoSearchProvider;->PROJECTION:[Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v1, p1, v2, v3}, Lcom/google/android/videos/store/PurchaseRequests;->createActivePurchaseRequestForGlobalSearch([Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v0

    .line 151
    .local v0, "request":Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    invoke-direct {p0, v0}, Lcom/google/android/videos/search/VideoSearchProvider;->getPurchaseCursor(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method private zeroQuerySearch(ILcom/google/android/videos/store/CancellationSignalHolder;)Landroid/database/Cursor;
    .locals 4
    .param p1, "limit"    # I
    .param p2, "cancellationSignalHolder"    # Lcom/google/android/videos/store/CancellationSignalHolder;

    .prologue
    .line 136
    sget-object v1, Lcom/google/android/videos/search/VideoSearchProvider;->PROJECTION:[Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v1, p1, v2, v3, p2}, Lcom/google/android/videos/store/PurchaseRequests;->createNowPlayingRequestForGlobalSearch([Ljava/lang/String;IJLcom/google/android/videos/store/CancellationSignalHolder;)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v0

    .line 138
    .local v0, "request":Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    invoke-direct {p0, v0}, Lcom/google/android/videos/search/VideoSearchProvider;->getPurchaseCursor(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 227
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 211
    const-string v0, "vnd.android.cursor.dir/vnd.android.search.suggest"

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 216
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x1

    return v0
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "access"    # Ljava/lang/String;

    .prologue
    .line 172
    invoke-direct {p0}, Lcom/google/android/videos/search/VideoSearchProvider;->init()V

    .line 174
    const/4 v0, 0x0

    .line 175
    .local v0, "result":Landroid/os/ParcelFileDescriptor;
    sget-object v1, Lcom/google/android/videos/search/VideoSearchProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 176
    invoke-direct {p0, p1}, Lcom/google/android/videos/search/VideoSearchProvider;->makeThumbnailRequest(Landroid/net/Uri;)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 178
    :cond_0
    return-object v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projectionIn"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 79
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/search/VideoSearchProvider;->queryInternal(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/store/CancellationSignalHolder;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projectionIn"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;
    .param p6, "cancellationSignal"    # Landroid/os/CancellationSignal;

    .prologue
    .line 86
    if-nez p6, :cond_0

    const/4 v6, 0x0

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/search/VideoSearchProvider;->queryInternal(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/store/CancellationSignalHolder;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v6, Lcom/google/android/videos/store/CancellationSignalHolder;

    invoke-direct {v6, p6}, Lcom/google/android/videos/store/CancellationSignalHolder;-><init>(Landroid/os/CancellationSignal;)V

    goto :goto_0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 222
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
