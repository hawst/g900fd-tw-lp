.class public interface abstract Lcom/google/android/videos/player/PlaybackHelper;
.super Ljava/lang/Object;
.source "PlaybackHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/PlaybackHelper$PlayerListener;,
        Lcom/google/android/videos/player/PlaybackHelper$DisplayNotSecureException;,
        Lcom/google/android/videos/player/PlaybackHelper$DisplayNotFoundException;
    }
.end annotation


# virtual methods
.method public abstract getDisplayType()I
.end method

.method public abstract init(Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/player/PlaybackHelper$DisplayNotFoundException;,
            Lcom/google/android/videos/player/PlaybackHelper$DisplayNotSecureException;
        }
    .end annotation
.end method

.method public abstract initKnowledgeViewHelper(Lcom/google/android/videos/tagging/KnowledgeViewHelper;)V
.end method

.method public abstract load(Lcom/google/android/videos/player/DirectorInitializer$InitializationData;)V
.end method

.method public abstract maybeJoinCurrentPlayback(Ljava/lang/String;)Z
.end method

.method public abstract onHQ()V
.end method

.method public abstract onScrubbingCanceled()V
.end method

.method public abstract onScrubbingStart(Z)V
.end method

.method public abstract onSeekTo(ZIZ)V
.end method

.method public abstract onSelectedAudioTrackIndexChanged(I)V
.end method

.method public abstract onSelectedSubtitleTrackChanged(Lcom/google/android/videos/subtitles/SubtitleTrack;)V
.end method

.method public abstract pause()V
.end method

.method public abstract play(Z)V
.end method

.method public abstract release()V
.end method

.method public abstract setErrorState(Ljava/lang/String;Lcom/google/android/videos/utils/RetryAction;)V
.end method

.method public abstract setState(ZZ)V
.end method

.method public abstract setViews(Lcom/google/android/videos/player/PlayerView;Lcom/google/android/videos/player/overlay/ControllerOverlay;Lcom/google/android/videos/player/overlay/SubtitlesOverlay;Lcom/google/android/videos/ui/RemoteScreenPanelHelper;Lcom/google/android/videos/tagging/KnowledgeView;Lcom/google/android/videos/tagging/KnowledgeView;)V
.end method
