.class Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "SecondScreenEntitiesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "EntityViewHolder"
.end annotation


# instance fields
.field public bitmapView:Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView",
            "<+",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public circularForegroundDrawable:Landroid/graphics/drawable/Drawable;

.field public firstLineTextView:Landroid/widget/TextView;

.field public imageFrame:Landroid/widget/FrameLayout;

.field public imageView:Landroid/view/View;

.field public retangularForegroundDrawable:Landroid/graphics/drawable/Drawable;

.field public secondLineTextView:Landroid/widget/TextView;

.field public targetScale:F

.field public textFrame:Landroid/widget/FrameLayout;

.field final synthetic this$0:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;Landroid/view/View;)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;

    .prologue
    const v3, 0x7f0201d4

    .line 232
    iput-object p1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->this$0:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;

    .line 233
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 234
    const v1, 0x7f0f0109

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->imageFrame:Landroid/widget/FrameLayout;

    .line 235
    const v1, 0x7f0f01d5

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->textFrame:Landroid/widget/FrameLayout;

    .line 236
    const v1, 0x7f0f01f3

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->imageView:Landroid/view/View;

    .line 237
    const v1, 0x7f0f01f4

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->firstLineTextView:Landroid/widget/TextView;

    .line 238
    const v1, 0x7f0f01f5

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->secondLineTextView:Landroid/widget/TextView;

    .line 239
    new-instance v0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder$1;-><init>(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;)V

    .line 248
    .local v0, "listener":Landroid/view/View$OnClickListener;
    iget-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->imageFrame:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 249
    iget-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->textFrame:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 250
    iget-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->imageFrame:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getForeground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->retangularForegroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 251
    sget v1, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    .line 252
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->circularForegroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 258
    :goto_0
    return-void

    .line 255
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->circularForegroundDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method
