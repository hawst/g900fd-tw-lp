.class final Lcom/google/android/videos/store/StoryboardClient$4;
.super Lcom/google/android/videos/store/AbstractFileStore;
.source "StoryboardClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/store/StoryboardClient;->getStoryboardStore(Landroid/content/Context;)Lcom/google/android/videos/store/AbstractFileStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/store/AbstractFileStore",
        "<",
        "Ljava/lang/String;",
        "Lcom/google/wireless/android/video/magma/proto/Storyboard;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/videos/cache/Converter;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Context;

    .prologue
    .line 123
    .local p2, "x1":Lcom/google/android/videos/cache/Converter;, "Lcom/google/android/videos/cache/Converter<Lcom/google/wireless/android/video/magma/proto/Storyboard;>;"
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/store/AbstractFileStore;-><init>(Landroid/content/Context;Lcom/google/android/videos/cache/Converter;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic generateFilepath(Ljava/io/File;Ljava/lang/Object;)Ljava/io/File;
    .locals 1
    .param p1, "x0"    # Ljava/io/File;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 123
    check-cast p2, Ljava/lang/String;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/store/StoryboardClient$4;->generateFilepath(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method protected generateFilepath(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;
    .locals 4
    .param p1, "rootFileDir"    # Ljava/io/File;
    .param p2, "videoId"    # Ljava/lang/String;

    .prologue
    .line 126
    new-instance v0, Ljava/io/File;

    # invokes: Lcom/google/android/videos/store/StoryboardClient;->getStoryboardDir(Ljava/io/File;)Ljava/io/File;
    invoke-static {p1}, Lcom/google/android/videos/store/StoryboardClient;->access$100(Ljava/io/File;)Ljava/io/File;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".stb"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method
