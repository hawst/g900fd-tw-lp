.class public Lcom/google/android/videos/ui/RemoveItemDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "RemoveItemDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/RemoveItemDialogFragment$OnRemovedListener;
    }
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private itemId:Ljava/lang/String;

.field private itemIsShow:Z

.field private reportRemove:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 28
    return-void
.end method

.method private static showInstanceInternal(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 4
    .param p0, "activity"    # Landroid/support/v4/app/FragmentActivity;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "itemId"    # Ljava/lang/String;
    .param p3, "itemTitle"    # Ljava/lang/String;
    .param p4, "itemIsShow"    # Z
    .param p5, "reportRemove"    # Z

    .prologue
    .line 49
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 50
    .local v0, "args":Landroid/os/Bundle;
    const-string v3, "authAccount"

    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    const-string v3, "remove_item_id"

    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const-string v3, "remove_item_title"

    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const-string v2, "remove_item_is_show"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 57
    const-string v2, "remove_report"

    invoke-virtual {v0, v2, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 59
    new-instance v1, Lcom/google/android/videos/ui/RemoveItemDialogFragment;

    invoke-direct {v1}, Lcom/google/android/videos/ui/RemoveItemDialogFragment;-><init>()V

    .line 60
    .local v1, "instance":Lcom/google/android/videos/ui/RemoveItemDialogFragment;
    invoke-virtual {v1, v0}, Lcom/google/android/videos/ui/RemoveItemDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 61
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "RemoveItemDialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/ui/RemoveItemDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 62
    return-void
.end method

.method public static showInstanceWithCallback(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "itemId"    # Ljava/lang/String;
    .param p3, "itemTitle"    # Ljava/lang/String;
    .param p4, "itemIsShow"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/support/v4/app/FragmentActivity;",
            ":",
            "Lcom/google/android/videos/ui/RemoveItemDialogFragment$OnRemovedListener;",
            ">(TT;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 39
    .local p0, "activity":Landroid/support/v4/app/FragmentActivity;, "TT;"
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/ui/RemoveItemDialogFragment;->showInstanceInternal(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 40
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v4, 0x1

    .line 85
    const/4 v2, -0x1

    if-ne p2, v2, :cond_0

    .line 86
    invoke-virtual {p0}, Lcom/google/android/videos/ui/RemoveItemDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 87
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    invoke-static {v0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStoreSync()Lcom/google/android/videos/store/PurchaseStoreSync;

    move-result-object v1

    .line 89
    .local v1, "purchaseStoreSync":Lcom/google/android/videos/store/PurchaseStoreSync;
    iget-boolean v2, p0, Lcom/google/android/videos/ui/RemoveItemDialogFragment;->itemIsShow:Z

    if-eqz v2, :cond_1

    .line 90
    iget-object v2, p0, Lcom/google/android/videos/ui/RemoveItemDialogFragment;->account:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/ui/RemoveItemDialogFragment;->itemId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/videos/store/PurchaseStoreSync;->setHiddenStateForShow(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 94
    :goto_0
    iget-boolean v2, p0, Lcom/google/android/videos/ui/RemoveItemDialogFragment;->reportRemove:Z

    if-eqz v2, :cond_0

    instance-of v2, v0, Lcom/google/android/videos/ui/RemoveItemDialogFragment$OnRemovedListener;

    if-eqz v2, :cond_0

    .line 95
    check-cast v0, Lcom/google/android/videos/ui/RemoveItemDialogFragment$OnRemovedListener;

    .end local v0    # "activity":Landroid/support/v4/app/FragmentActivity;
    iget-object v2, p0, Lcom/google/android/videos/ui/RemoveItemDialogFragment;->account:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/ui/RemoveItemDialogFragment;->itemId:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/google/android/videos/ui/RemoveItemDialogFragment;->itemIsShow:Z

    invoke-interface {v0, v2, v3, v4}, Lcom/google/android/videos/ui/RemoveItemDialogFragment$OnRemovedListener;->onRemoved(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 98
    .end local v1    # "purchaseStoreSync":Lcom/google/android/videos/store/PurchaseStoreSync;
    :cond_0
    return-void

    .line 92
    .restart local v0    # "activity":Landroid/support/v4/app/FragmentActivity;
    .restart local v1    # "purchaseStoreSync":Lcom/google/android/videos/store/PurchaseStoreSync;
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/ui/RemoveItemDialogFragment;->account:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/ui/RemoveItemDialogFragment;->itemId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/videos/store/PurchaseStoreSync;->setHiddenStateForMovie(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/google/android/videos/ui/RemoveItemDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 67
    .local v0, "args":Landroid/os/Bundle;
    const-string v4, "authAccount"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/videos/ui/RemoveItemDialogFragment;->account:Ljava/lang/String;

    .line 68
    const-string v4, "remove_item_id"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/videos/ui/RemoveItemDialogFragment;->itemId:Ljava/lang/String;

    .line 69
    const-string v4, "remove_item_is_show"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/videos/ui/RemoveItemDialogFragment;->itemIsShow:Z

    .line 70
    const-string v4, "remove_report"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/videos/ui/RemoveItemDialogFragment;->reportRemove:Z

    .line 71
    const-string v4, "remove_item_title"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 72
    .local v2, "itemTitle":Ljava/lang/String;
    iget-boolean v4, p0, Lcom/google/android/videos/ui/RemoveItemDialogFragment;->itemIsShow:Z

    if-eqz v4, :cond_0

    const v3, 0x7f0b010a

    .line 75
    .local v3, "messageId":I
    :goto_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/videos/ui/RemoveItemDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v1, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 76
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    const v4, 0x7f0b0108

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 77
    invoke-virtual {p0}, Lcom/google/android/videos/ui/RemoveItemDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-virtual {v4, v3, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 78
    const v4, 0x7f0b010c

    invoke-virtual {v1, v4, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 79
    const v4, 0x7f0b010d

    invoke-virtual {v1, v4, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 80
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    return-object v4

    .line 72
    .end local v1    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v3    # "messageId":I
    :cond_0
    const v3, 0x7f0b0109

    goto :goto_0
.end method
