.class public Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$Binder;
.super Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;
.source "ActorFilmographyClusterItemView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Binder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder",
        "<",
        "Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;",
        "Lcom/google/android/videos/adapter/ArrayDataSource",
        "<",
        "Lcom/google/android/videos/tagging/KnowledgeEntity$Film;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final imageRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final itemClickListener:Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$ItemClickListener;

.field private final storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/videos/store/StoreStatusMonitor;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/async/Requester;Ljava/lang/String;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "storeStatusMonitor"    # Lcom/google/android/videos/store/StoreStatusMonitor;
    .param p3, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;
    .param p5, "account"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/google/android/videos/store/StoreStatusMonitor;",
            "Lcom/google/android/videos/logging/EventLogger;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 176
    .local p4, "imageRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;>;"
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;-><init>(Lcom/google/android/videos/logging/UiElementNode;I)V

    .line 177
    iput-object p4, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$Binder;->imageRequester:Lcom/google/android/videos/async/Requester;

    .line 178
    iput-object p2, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$Binder;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    .line 179
    new-instance v0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$ItemClickListener;

    invoke-direct {v0, p1, p2, p3, p5}, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$ItemClickListener;-><init>(Landroid/app/Activity;Lcom/google/android/videos/store/StoreStatusMonitor;Lcom/google/android/videos/logging/EventLogger;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$Binder;->itemClickListener:Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$ItemClickListener;

    .line 180
    return-void
.end method


# virtual methods
.method public getItemClickListener()Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$ItemClickListener;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$Binder;->itemClickListener:Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$ItemClickListener;

    return-object v0
.end method

.method protected onBind(Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;Lcom/google/android/videos/adapter/ArrayDataSource;I)V
    .locals 4
    .param p1, "view"    # Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;
    .param p3, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;",
            "Lcom/google/android/videos/adapter/ArrayDataSource",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Film;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 189
    .local p2, "dataSource":Lcom/google/android/videos/adapter/ArrayDataSource;, "Lcom/google/android/videos/adapter/ArrayDataSource<Lcom/google/android/videos/tagging/KnowledgeEntity$Film;>;"
    invoke-virtual {p2, p3}, Lcom/google/android/videos/adapter/ArrayDataSource;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Film;

    .line 190
    .local v0, "item":Lcom/google/android/videos/tagging/KnowledgeEntity$Film;
    iget-object v1, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$Binder;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    iget-object v2, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$Binder;->imageRequester:Lcom/google/android/videos/async/Requester;

    invoke-virtual {p2}, Lcom/google/android/videos/adapter/ArrayDataSource;->isNetworkConnected()Z

    move-result v3

    invoke-virtual {p1, v1, v0, v2, v3}, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->bind(Lcom/google/android/videos/store/StoreStatusMonitor;Lcom/google/android/videos/tagging/KnowledgeEntity$Film;Lcom/google/android/videos/async/Requester;Z)V

    .line 191
    return-void
.end method

.method protected bridge synthetic onBind(Lcom/google/android/videos/ui/playnext/ClusterItemView;Lcom/google/android/videos/adapter/DataSource;I)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/ui/playnext/ClusterItemView;
    .param p2, "x1"    # Lcom/google/android/videos/adapter/DataSource;
    .param p3, "x2"    # I

    .prologue
    .line 166
    check-cast p1, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;

    .end local p1    # "x0":Lcom/google/android/videos/ui/playnext/ClusterItemView;
    check-cast p2, Lcom/google/android/videos/adapter/ArrayDataSource;

    .end local p2    # "x1":Lcom/google/android/videos/adapter/DataSource;
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$Binder;->onBind(Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;Lcom/google/android/videos/adapter/ArrayDataSource;I)V

    return-void
.end method
