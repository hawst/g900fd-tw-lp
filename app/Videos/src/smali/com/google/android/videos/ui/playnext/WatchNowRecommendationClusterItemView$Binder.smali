.class public final Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView$Binder;
.super Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;
.source "WatchNowRecommendationClusterItemView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Binder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder",
        "<",
        "Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;",
        "Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;",
        ">;"
    }
.end annotation


# instance fields
.field private final bitmapRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final cpuExecutor:Ljava/util/concurrent/Executor;

.field private final imageUriCreator:Lcom/google/android/videos/ui/AssetImageUriCreator;

.field private final supportsDirectPurchase:Z


# direct methods
.method public constructor <init>(Lcom/google/android/videos/async/Requester;Ljava/util/concurrent/Executor;ZLcom/google/android/videos/logging/UiElementNode;Lcom/google/android/videos/ui/AssetImageUriCreator;)V
    .locals 1
    .param p2, "cpuExecutor"    # Ljava/util/concurrent/Executor;
    .param p3, "supportsDirectPurchase"    # Z
    .param p4, "clusterUiElementNode"    # Lcom/google/android/videos/logging/UiElementNode;
    .param p5, "imageUriCreator"    # Lcom/google/android/videos/ui/AssetImageUriCreator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Ljava/util/concurrent/Executor;",
            "Z",
            "Lcom/google/android/videos/logging/UiElementNode;",
            "Lcom/google/android/videos/ui/AssetImageUriCreator;",
            ")V"
        }
    .end annotation

    .prologue
    .line 441
    .local p1, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Landroid/graphics/Bitmap;>;"
    const/16 v0, 0x8

    invoke-direct {p0, p4, v0}, Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;-><init>(Lcom/google/android/videos/logging/UiElementNode;I)V

    .line 442
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView$Binder;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    .line 443
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView$Binder;->cpuExecutor:Ljava/util/concurrent/Executor;

    .line 444
    iput-boolean p3, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView$Binder;->supportsDirectPurchase:Z

    .line 445
    iput-object p5, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView$Binder;->imageUriCreator:Lcom/google/android/videos/ui/AssetImageUriCreator;

    .line 446
    return-void
.end method


# virtual methods
.method protected bridge synthetic onBind(Lcom/google/android/videos/ui/playnext/ClusterItemView;Lcom/google/android/videos/adapter/DataSource;I)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/ui/playnext/ClusterItemView;
    .param p2, "x1"    # Lcom/google/android/videos/adapter/DataSource;
    .param p3, "x2"    # I

    .prologue
    .line 429
    check-cast p1, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;

    .end local p1    # "x0":Lcom/google/android/videos/ui/playnext/ClusterItemView;
    check-cast p2, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;

    .end local p2    # "x1":Lcom/google/android/videos/adapter/DataSource;
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView$Binder;->onBind(Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;I)V

    return-void
.end method

.method protected onBind(Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;I)V
    .locals 8
    .param p1, "view"    # Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;
    .param p2, "dataSource"    # Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;
    .param p3, "index"    # I

    .prologue
    .line 451
    invoke-virtual {p2, p3}, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;->getItem(I)Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView$Binder;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    iget-object v3, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView$Binder;->cpuExecutor:Ljava/util/concurrent/Executor;

    iget-object v4, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView$Binder;->imageUriCreator:Lcom/google/android/videos/ui/AssetImageUriCreator;

    iget-boolean v5, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView$Binder;->supportsDirectPurchase:Z

    invoke-virtual {p2}, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;->isNetworkConnected()Z

    move-result v6

    move-object v0, p1

    move-object v7, p0

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->bind(Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;Lcom/google/android/videos/async/Requester;Ljava/util/concurrent/Executor;Lcom/google/android/videos/ui/AssetImageUriCreator;ZZLcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView$Binder;)V

    .line 453
    return-void
.end method
