.class Lcom/google/android/videos/ui/TransitionUtil$CircularSplash$1;
.super Landroid/animation/AnimatorListenerAdapter;
.source "TransitionUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->createAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash$1;->this$0:Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;

    iput-object p2, p0, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash$1;->val$view:Landroid/view/View;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash$1;->this$0:Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;

    # getter for: Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->isReveal:Z
    invoke-static {v0}, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->access$000(Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash$1;->val$view:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 176
    :cond_0
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash$1;->this$0:Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;

    # getter for: Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->isReveal:Z
    invoke-static {v0}, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;->access$000(Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/google/android/videos/ui/TransitionUtil$CircularSplash$1;->val$view:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 167
    :cond_0
    return-void
.end method
