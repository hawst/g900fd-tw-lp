.class public Lcom/google/android/videos/drm/DrmResponse;
.super Ljava/lang/Object;
.source "DrmResponse.java"


# instance fields
.field public final allowsOffline:Z

.field public final allowsStreaming:Z

.field public final ids:Lcom/google/android/videos/drm/DrmManager$Identifiers;

.field public final refreshed:Z

.field public final remainingSeconds:I

.field public final secondsSinceActivation:I

.field public final timestamp:J


# direct methods
.method public constructor <init>(JZZIILcom/google/android/videos/drm/DrmManager$Identifiers;Z)V
    .locals 1
    .param p1, "timestamp"    # J
    .param p3, "allowsStreaming"    # Z
    .param p4, "allowsOffline"    # Z
    .param p5, "remainingSeconds"    # I
    .param p6, "secondsSinceActivation"    # I
    .param p7, "ids"    # Lcom/google/android/videos/drm/DrmManager$Identifiers;
    .param p8, "refreshed"    # Z

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-wide p1, p0, Lcom/google/android/videos/drm/DrmResponse;->timestamp:J

    .line 58
    iput-boolean p3, p0, Lcom/google/android/videos/drm/DrmResponse;->allowsStreaming:Z

    .line 59
    iput-boolean p4, p0, Lcom/google/android/videos/drm/DrmResponse;->allowsOffline:Z

    .line 60
    iput p5, p0, Lcom/google/android/videos/drm/DrmResponse;->remainingSeconds:I

    .line 61
    iput p6, p0, Lcom/google/android/videos/drm/DrmResponse;->secondsSinceActivation:I

    .line 62
    iput-object p7, p0, Lcom/google/android/videos/drm/DrmResponse;->ids:Lcom/google/android/videos/drm/DrmManager$Identifiers;

    .line 63
    iput-boolean p8, p0, Lcom/google/android/videos/drm/DrmResponse;->refreshed:Z

    .line 64
    return-void
.end method


# virtual methods
.method public isActivated()Z
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/google/android/videos/drm/DrmResponse;->secondsSinceActivation:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[st="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/videos/drm/DrmResponse;->allowsStreaming:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ol="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/videos/drm/DrmResponse;->allowsOffline:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "timestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/videos/drm/DrmResponse;->timestamp:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "remaining="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/videos/drm/DrmResponse;->remainingSeconds:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "activated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/videos/drm/DrmResponse;->secondsSinceActivation:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
