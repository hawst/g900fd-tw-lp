.class Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;
.super Ljava/lang/Object;
.source "ShowDetailsRowHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EpisodeContainer"
.end annotation


# instance fields
.field public final assetResource:Lcom/google/wireless/android/video/magma/proto/AssetResource;

.field public final cheapestOffer:Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

.field public final expirationTimestamp:J

.field public final purchased:Z

.field public final resumeTimeMillis:I


# direct methods
.method private constructor <init>(Lcom/google/wireless/android/video/magma/proto/AssetResource;IJ)V
    .locals 1
    .param p1, "assetResource"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p2, "resumeTimeMillis"    # I
    .param p3, "expirationTimestamp"    # J

    .prologue
    .line 684
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 685
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->purchased:Z

    .line 686
    iput-object p1, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->assetResource:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 687
    iput p2, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->resumeTimeMillis:I

    .line 688
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->cheapestOffer:Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    .line 689
    iput-wide p3, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->expirationTimestamp:J

    .line 690
    return-void
.end method

.method private constructor <init>(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;)V
    .locals 2
    .param p1, "assetResource"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p2, "cheapestOffer"    # Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    .prologue
    const/4 v0, 0x0

    .line 692
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 693
    iput-boolean v0, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->purchased:Z

    .line 694
    iput-object p1, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->assetResource:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 695
    iput v0, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->resumeTimeMillis:I

    .line 696
    iput-object p2, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->cheapestOffer:Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    .line 697
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->expirationTimestamp:J

    .line 698
    return-void
.end method

.method public static create(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/android/videos/store/PurchaseStore;Ljava/lang/String;)Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;
    .locals 11
    .param p0, "assetResource"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p1, "purchaseStore"    # Lcom/google/android/videos/store/PurchaseStore;
    .param p2, "account"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    .line 659
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v5

    .line 660
    .local v5, "purchaseCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;>;"
    iget-object v7, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v4, v7, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    .line 661
    .local v4, "id":Ljava/lang/String;
    sget-object v7, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$Query;->COLUMNS:[Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {p2, v7, v4, v8, v9}, Lcom/google/android/videos/store/PurchaseRequests;->createActivePurchaseRequestForUser(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v7

    invoke-virtual {p1, v7, v5}, Lcom/google/android/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/videos/async/Callback;)V

    .line 664
    :try_start_0
    invoke-virtual {v5}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 666
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 667
    const/4 v7, 0x0

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 668
    .local v6, "resumeTimeMillis":I
    const/4 v7, 0x2

    invoke-interface {v0, v7}, Landroid/database/Cursor;->isNull(I)Z

    move-result v7

    if-eqz v7, :cond_0

    const-wide v2, 0x7fffffffffffffffL

    .line 670
    .local v2, "expirationTimestamp":J
    :goto_0
    new-instance v7, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;

    invoke-direct {v7, p0, v6, v2, v3}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;-><init>(Lcom/google/wireless/android/video/magma/proto/AssetResource;IJ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 673
    :try_start_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_0

    .line 679
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v2    # "expirationTimestamp":J
    .end local v6    # "resumeTimeMillis":I
    :goto_1
    return-object v7

    .line 668
    .restart local v0    # "cursor":Landroid/database/Cursor;
    .restart local v6    # "resumeTimeMillis":I
    :cond_0
    const/4 v7, 0x2

    :try_start_3
    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v7

    int-to-long v2, v7

    goto :goto_0

    .line 673
    .end local v6    # "resumeTimeMillis":I
    :cond_1
    :try_start_4
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_4 .. :try_end_4} :catch_0

    .line 679
    .end local v0    # "cursor":Landroid/database/Cursor;
    :goto_2
    new-instance v7, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;

    iget-object v8, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    invoke-static {v10, v8}, Lcom/google/android/videos/utils/OfferUtil;->getCheapestOffer(Z[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;)Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    move-result-object v8

    invoke-direct {v7, p0, v8}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;-><init>(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;)V

    goto :goto_1

    .line 673
    .restart local v0    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v7

    :try_start_5
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v7
    :try_end_5
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_5 .. :try_end_5} :catch_0

    .line 675
    .end local v0    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v1

    .line 676
    .local v1, "e":Ljava/util/concurrent/ExecutionException;
    const-string v7, "Failed to fetch purchases"

    invoke-virtual {v1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method


# virtual methods
.method public watchedToEndCredits()Z
    .locals 2

    .prologue
    .line 701
    iget v0, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->resumeTimeMillis:I

    iget-object v1, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->assetResource:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v1, v1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget v1, v1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->startOfCreditSec:I

    mul-int/lit16 v1, v1, 0x3e8

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
