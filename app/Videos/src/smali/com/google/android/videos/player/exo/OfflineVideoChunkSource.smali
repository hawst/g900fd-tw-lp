.class public Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;
.super Ljava/lang/Object;
.source "OfflineVideoChunkSource.java"

# interfaces
.implements Lcom/google/android/exoplayer/chunk/ChunkSource;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/exo/OfflineVideoChunkSource$InitializationMp4Loadable;
    }
.end annotation


# instance fields
.field private final cache:Lcom/google/android/exoplayer/upstream/cache/Cache;

.field private volatile cacheRegionTracker:Lcom/google/android/videos/player/exo/CachedRegionTracker;

.field private final dataSource:Lcom/google/android/exoplayer/upstream/DataSource;

.field private final extractor:Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;

.field private volatile lastSeekByteOffset:J

.field private final representation:Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

.field private final trackInfo:Lcom/google/android/exoplayer/TrackInfo;


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;Lcom/google/android/exoplayer/upstream/cache/Cache;)V
    .locals 6
    .param p1, "dataSource"    # Lcom/google/android/exoplayer/upstream/DataSource;
    .param p2, "representation"    # Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    .param p3, "cache"    # Lcom/google/android/exoplayer/upstream/cache/Cache;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->dataSource:Lcom/google/android/exoplayer/upstream/DataSource;

    .line 49
    iput-object p2, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->representation:Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    .line 50
    iput-object p3, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->cache:Lcom/google/android/exoplayer/upstream/cache/Cache;

    .line 51
    new-instance v0, Lcom/google/android/exoplayer/TrackInfo;

    iget-object v1, p2, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->format:Lcom/google/android/exoplayer/chunk/Format;

    iget-object v1, v1, Lcom/google/android/exoplayer/chunk/Format;->mimeType:Ljava/lang/String;

    iget-wide v2, p2, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->periodDurationMs:J

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/exoplayer/TrackInfo;-><init>(Ljava/lang/String;J)V

    iput-object v0, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->trackInfo:Lcom/google/android/exoplayer/TrackInfo;

    .line 53
    new-instance v0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;

    invoke-direct {v0}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->extractor:Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;

    .line 54
    return-void
.end method

.method private static getIndexAnchor(Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;)J
    .locals 6
    .param p0, "representation"    # Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->getIndexUri()Lcom/google/android/exoplayer/dash/mpd/RangedUri;

    move-result-object v0

    .line 171
    .local v0, "rangedUri":Lcom/google/android/exoplayer/dash/mpd/RangedUri;
    iget-wide v2, v0, Lcom/google/android/exoplayer/dash/mpd/RangedUri;->start:J

    iget-wide v4, v0, Lcom/google/android/exoplayer/dash/mpd/RangedUri;->length:J

    add-long/2addr v2, v4

    return-wide v2
.end method

.method private static newInitializationChunk(Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;Lcom/google/android/exoplayer/upstream/DataSource;I)Lcom/google/android/exoplayer/chunk/Chunk;
    .locals 7
    .param p0, "representation"    # Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    .param p1, "extractor"    # Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    .param p2, "dataSource"    # Lcom/google/android/exoplayer/upstream/DataSource;
    .param p3, "trigger"    # I

    .prologue
    .line 147
    new-instance v0, Lcom/google/android/exoplayer/upstream/DataSpec;

    iget-object v1, p0, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->uri:Landroid/net/Uri;

    const-wide/16 v2, 0x0

    invoke-static {p0}, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->getIndexAnchor(Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;)J

    move-result-wide v4

    invoke-virtual {p0}, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->getCacheKey()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/exoplayer/upstream/DataSpec;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    .line 149
    .local v0, "dataSpec":Lcom/google/android/exoplayer/upstream/DataSpec;
    new-instance v1, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource$InitializationMp4Loadable;

    iget-object v6, p0, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->format:Lcom/google/android/exoplayer/chunk/Format;

    move-object v2, p2

    move-object v3, v0

    move v4, p3

    move-object v5, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource$InitializationMp4Loadable;-><init>(Lcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/exoplayer/upstream/DataSpec;ILcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;Lcom/google/android/exoplayer/chunk/Format;)V

    return-object v1
.end method

.method private static newMediaChunk(Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;Lcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/exoplayer/parser/SegmentIndex;II)Lcom/google/android/exoplayer/chunk/Chunk;
    .locals 23
    .param p0, "representation"    # Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    .param p1, "extractor"    # Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    .param p2, "dataSource"    # Lcom/google/android/exoplayer/upstream/DataSource;
    .param p3, "sidx"    # Lcom/google/android/exoplayer/parser/SegmentIndex;
    .param p4, "index"    # I
    .param p5, "trigger"    # I

    .prologue
    .line 156
    move-object/from16 v0, p3

    iget v3, v0, Lcom/google/android/exoplayer/parser/SegmentIndex;->length:I

    add-int/lit8 v3, v3, -0x1

    move/from16 v0, p4

    if-ne v0, v3, :cond_0

    const/16 v18, -0x1

    .line 157
    .local v18, "nextIndex":I
    :goto_0
    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/android/exoplayer/parser/SegmentIndex;->timesUs:[J

    aget-wide v14, v3, p4

    .line 159
    .local v14, "startTimeUs":J
    const/4 v3, -0x1

    move/from16 v0, v18

    if-ne v0, v3, :cond_1

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/android/exoplayer/parser/SegmentIndex;->timesUs:[J

    aget-wide v8, v3, p4

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/android/exoplayer/parser/SegmentIndex;->durationsUs:[J

    aget-wide v10, v3, p4

    add-long v16, v8, v10

    .line 161
    .local v16, "endTimeUs":J
    :goto_1
    invoke-static/range {p0 .. p0}, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->getIndexAnchor(Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;)J

    move-result-wide v8

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/android/exoplayer/parser/SegmentIndex;->offsets:[J

    aget-wide v10, v3, p4

    add-long v4, v8, v10

    .line 162
    .local v4, "offset":J
    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/android/exoplayer/parser/SegmentIndex;->sizes:[I

    aget v3, v3, p4

    int-to-long v6, v3

    .line 163
    .local v6, "size":J
    new-instance v2, Lcom/google/android/exoplayer/upstream/DataSpec;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->uri:Landroid/net/Uri;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->getCacheKey()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v2 .. v8}, Lcom/google/android/exoplayer/upstream/DataSpec;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    .line 165
    .local v2, "dataSpec":Lcom/google/android/exoplayer/upstream/DataSpec;
    new-instance v9, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->format:Lcom/google/android/exoplayer/chunk/Format;

    const/16 v20, 0x0

    const-wide/16 v21, 0x0

    move-object/from16 v10, p2

    move-object v11, v2

    move/from16 v13, p5

    move-object/from16 v19, p1

    invoke-direct/range {v9 .. v22}, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;-><init>(Lcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/exoplayer/upstream/DataSpec;Lcom/google/android/exoplayer/chunk/Format;IJJILcom/google/android/exoplayer/parser/Extractor;ZJ)V

    return-object v9

    .line 156
    .end local v2    # "dataSpec":Lcom/google/android/exoplayer/upstream/DataSpec;
    .end local v4    # "offset":J
    .end local v6    # "size":J
    .end local v14    # "startTimeUs":J
    .end local v16    # "endTimeUs":J
    .end local v18    # "nextIndex":I
    :cond_0
    add-int/lit8 v18, p4, 0x1

    goto :goto_0

    .line 159
    .restart local v14    # "startTimeUs":J
    .restart local v18    # "nextIndex":I
    :cond_1
    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/android/exoplayer/parser/SegmentIndex;->timesUs:[J

    aget-wide v16, v3, v18

    goto :goto_1
.end method


# virtual methods
.method public continueBuffering(J)V
    .locals 0
    .param p1, "playbackPositionUs"    # J

    .prologue
    .line 97
    return-void
.end method

.method public disable(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/google/android/exoplayer/chunk/MediaChunk;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 88
    .local p1, "queue":Ljava/util/List;, "Ljava/util/List<+Lcom/google/android/exoplayer/chunk/MediaChunk;>;"
    iget-object v0, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->cacheRegionTracker:Lcom/google/android/videos/player/exo/CachedRegionTracker;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->cacheRegionTracker:Lcom/google/android/videos/player/exo/CachedRegionTracker;

    invoke-virtual {v0}, Lcom/google/android/videos/player/exo/CachedRegionTracker;->release()V

    .line 90
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->cacheRegionTracker:Lcom/google/android/videos/player/exo/CachedRegionTracker;

    .line 92
    :cond_0
    return-void
.end method

.method public enable()V
    .locals 0

    .prologue
    .line 84
    return-void
.end method

.method public getCachedPosition()I
    .locals 4

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->cacheRegionTracker:Lcom/google/android/videos/player/exo/CachedRegionTracker;

    if-nez v0, :cond_0

    .line 63
    const/4 v0, -0x1

    .line 65
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->cacheRegionTracker:Lcom/google/android/videos/player/exo/CachedRegionTracker;

    iget-wide v2, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->lastSeekByteOffset:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/videos/player/exo/CachedRegionTracker;->getRegionEndTimeMs(J)I

    move-result v0

    goto :goto_0
.end method

.method public final getChunkOperation(Ljava/util/List;JJLcom/google/android/exoplayer/chunk/ChunkOperationHolder;)V
    .locals 12
    .param p2, "seekPositionUs"    # J
    .param p4, "playbackPositionUs"    # J
    .param p6, "out"    # Lcom/google/android/exoplayer/chunk/ChunkOperationHolder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/google/android/exoplayer/chunk/MediaChunk;",
            ">;JJ",
            "Lcom/google/android/exoplayer/chunk/ChunkOperationHolder;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "queue":Ljava/util/List;, "Ljava/util/List<+Lcom/google/android/exoplayer/chunk/MediaChunk;>;"
    const/4 v7, 0x0

    .line 102
    move-object/from16 v0, p6

    iget-object v2, v0, Lcom/google/android/exoplayer/chunk/ChunkOperationHolder;->chunk:Lcom/google/android/exoplayer/chunk/Chunk;

    if-eqz v2, :cond_1

    .line 133
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->extractor:Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;

    invoke-virtual {v2}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->getFormat()Lcom/google/android/exoplayer/MediaFormat;

    move-result-object v2

    if-nez v2, :cond_2

    .line 108
    iget-object v2, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->representation:Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    iget-object v3, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->extractor:Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;

    iget-object v4, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->dataSource:Lcom/google/android/exoplayer/upstream/DataSource;

    invoke-static {v2, v3, v4, v7}, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->newInitializationChunk(Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;Lcom/google/android/exoplayer/upstream/DataSource;I)Lcom/google/android/exoplayer/chunk/Chunk;

    move-result-object v8

    .line 110
    .local v8, "initializationChunk":Lcom/google/android/exoplayer/chunk/Chunk;
    move-object/from16 v0, p6

    iput-object v8, v0, Lcom/google/android/exoplayer/chunk/ChunkOperationHolder;->chunk:Lcom/google/android/exoplayer/chunk/Chunk;

    goto :goto_0

    .line 112
    .end local v8    # "initializationChunk":Lcom/google/android/exoplayer/chunk/Chunk;
    :cond_2
    iget-object v2, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->cacheRegionTracker:Lcom/google/android/videos/player/exo/CachedRegionTracker;

    if-nez v2, :cond_3

    .line 113
    new-instance v2, Lcom/google/android/videos/player/exo/CachedRegionTracker;

    iget-object v3, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->cache:Lcom/google/android/exoplayer/upstream/cache/Cache;

    iget-object v4, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->representation:Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    iget-object v5, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->extractor:Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;

    invoke-virtual {v5}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->getIndex()Lcom/google/android/exoplayer/parser/SegmentIndex;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/videos/player/exo/CachedRegionTracker;-><init>(Lcom/google/android/exoplayer/upstream/cache/Cache;Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;Lcom/google/android/exoplayer/parser/SegmentIndex;)V

    iput-object v2, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->cacheRegionTracker:Lcom/google/android/videos/player/exo/CachedRegionTracker;

    .line 117
    :cond_3
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 118
    iget-object v2, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->extractor:Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;

    invoke-virtual {v2}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->getIndex()Lcom/google/android/exoplayer/parser/SegmentIndex;

    move-result-object v10

    .line 119
    .local v10, "segmentIndex":Lcom/google/android/exoplayer/parser/SegmentIndex;
    iget-object v2, v10, Lcom/google/android/exoplayer/parser/SegmentIndex;->timesUs:[J

    invoke-static {v2, p2, p3}, Ljava/util/Arrays;->binarySearch([JJ)I

    move-result v6

    .line 120
    .local v6, "nextIndex":I
    if-gez v6, :cond_4

    neg-int v2, v6

    add-int/lit8 v6, v2, -0x2

    .line 121
    :cond_4
    iget-object v2, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->representation:Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    invoke-static {v2}, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->getIndexAnchor(Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;)J

    move-result-wide v2

    iget-object v4, v10, Lcom/google/android/exoplayer/parser/SegmentIndex;->offsets:[J

    aget-wide v4, v4, v6

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->lastSeekByteOffset:J

    .line 126
    .end local v10    # "segmentIndex":Lcom/google/android/exoplayer/parser/SegmentIndex;
    :goto_1
    const/4 v2, -0x1

    if-eq v6, v2, :cond_0

    .line 130
    iget-object v2, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->representation:Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    iget-object v3, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->extractor:Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;

    iget-object v4, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->dataSource:Lcom/google/android/exoplayer/upstream/DataSource;

    iget-object v5, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->extractor:Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;

    invoke-virtual {v5}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->getIndex()Lcom/google/android/exoplayer/parser/SegmentIndex;

    move-result-object v5

    invoke-static/range {v2 .. v7}, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->newMediaChunk(Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;Lcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/exoplayer/parser/SegmentIndex;II)Lcom/google/android/exoplayer/chunk/Chunk;

    move-result-object v9

    .line 132
    .local v9, "nextMediaChunk":Lcom/google/android/exoplayer/chunk/Chunk;
    move-object/from16 v0, p6

    iput-object v9, v0, Lcom/google/android/exoplayer/chunk/ChunkOperationHolder;->chunk:Lcom/google/android/exoplayer/chunk/Chunk;

    goto :goto_0

    .line 123
    .end local v6    # "nextIndex":I
    .end local v9    # "nextMediaChunk":Lcom/google/android/exoplayer/chunk/Chunk;
    :cond_5
    move-object/from16 v0, p6

    iget v2, v0, Lcom/google/android/exoplayer/chunk/ChunkOperationHolder;->queueSize:I

    add-int/lit8 v2, v2, -0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer/chunk/MediaChunk;

    iget v6, v2, Lcom/google/android/exoplayer/chunk/MediaChunk;->nextChunkIndex:I

    .restart local v6    # "nextIndex":I
    goto :goto_1
.end method

.method public getError()Ljava/io/IOException;
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getMaxVideoDimensions(Lcom/google/android/exoplayer/MediaFormat;)V
    .locals 2
    .param p1, "out"    # Lcom/google/android/exoplayer/MediaFormat;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->trackInfo:Lcom/google/android/exoplayer/TrackInfo;

    iget-object v0, v0, Lcom/google/android/exoplayer/TrackInfo;->mimeType:Ljava/lang/String;

    const-string v1, "video"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->representation:Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    iget-object v0, v0, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->format:Lcom/google/android/exoplayer/chunk/Format;

    iget v0, v0, Lcom/google/android/exoplayer/chunk/Format;->width:I

    iget-object v1, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->representation:Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    iget-object v1, v1, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->format:Lcom/google/android/exoplayer/chunk/Format;

    iget v1, v1, Lcom/google/android/exoplayer/chunk/Format;->height:I

    invoke-virtual {p1, v0, v1}, Lcom/google/android/exoplayer/MediaFormat;->setMaxVideoDimensions(II)V

    .line 75
    :cond_0
    return-void
.end method

.method public final getTrackInfo()Lcom/google/android/exoplayer/TrackInfo;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;->trackInfo:Lcom/google/android/exoplayer/TrackInfo;

    return-object v0
.end method

.method public onChunkLoadError(Lcom/google/android/exoplayer/chunk/Chunk;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "chunk"    # Lcom/google/android/exoplayer/chunk/Chunk;
    .param p2, "e"    # Ljava/lang/Exception;

    .prologue
    .line 143
    return-void
.end method
