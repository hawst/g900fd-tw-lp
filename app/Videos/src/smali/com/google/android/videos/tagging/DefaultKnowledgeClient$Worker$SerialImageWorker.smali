.class Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$SerialImageWorker;
.super Ljava/lang/Object;
.source "DefaultKnowledgeClient.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SerialImageWorker"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private callInProgress:Z

.field private callPending:Z

.field private final images:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            ">;"
        }
    .end annotation
.end field

.field private nextImageIndex:I

.field final synthetic this$1:Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 259
    .local p2, "images":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/KnowledgeEntity$Image;>;"
    iput-object p1, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$SerialImageWorker;->this$1:Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 260
    iput-object p2, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$SerialImageWorker;->images:Ljava/util/List;

    .line 261
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$SerialImageWorker;->nextImageIndex:I

    .line 262
    return-void
.end method

.method private declared-synchronized requestNextImage()V
    .locals 4

    .prologue
    .line 280
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$SerialImageWorker;->callPending:Z

    .line 281
    iget-boolean v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$SerialImageWorker;->callInProgress:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 296
    :cond_0
    monitor-exit p0

    return-void

    .line 286
    :cond_1
    :goto_0
    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$SerialImageWorker;->callPending:Z

    if-eqz v0, :cond_0

    .line 287
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$SerialImageWorker;->callPending:Z

    .line 288
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$SerialImageWorker;->callInProgress:Z

    .line 289
    iget v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$SerialImageWorker;->nextImageIndex:I

    iget-object v1, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$SerialImageWorker;->images:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 290
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$SerialImageWorker;->this$1:Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;

    # invokes: Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->onImageLoadingFinished()V
    invoke-static {v0}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->access$600(Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;)V

    .line 294
    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$SerialImageWorker;->callInProgress:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 280
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 292
    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$SerialImageWorker;->this$1:Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;

    # getter for: Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->localImageRequester:Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;
    invoke-static {v0}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->access$700(Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;)Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$SerialImageWorker;->images:Ljava/util/List;

    iget v2, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$SerialImageWorker;->nextImageIndex:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$SerialImageWorker;->nextImageIndex:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    invoke-virtual {v1, v0, p0}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->request(Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Lcom/google/android/videos/async/Callback;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 275
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$SerialImageWorker;->this$1:Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;

    # invokes: Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->onImageRequestError(Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Ljava/lang/Exception;)V
    invoke-static {v0, p1, p2}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->access$500(Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Ljava/lang/Exception;)V

    .line 276
    invoke-direct {p0}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$SerialImageWorker;->requestNextImage()V

    .line 277
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 251
    check-cast p1, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$SerialImageWorker;->onError(Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "request"    # Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
    .param p2, "response"    # Landroid/graphics/Bitmap;

    .prologue
    .line 270
    invoke-direct {p0}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$SerialImageWorker;->requestNextImage()V

    .line 271
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 251
    check-cast p1, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Landroid/graphics/Bitmap;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$SerialImageWorker;->onResponse(Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public start()V
    .locals 0

    .prologue
    .line 265
    invoke-direct {p0}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$SerialImageWorker;->requestNextImage()V

    .line 266
    return-void
.end method
