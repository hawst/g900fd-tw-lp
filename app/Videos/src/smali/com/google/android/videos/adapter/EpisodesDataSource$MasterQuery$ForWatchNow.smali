.class public abstract Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow;
.super Ljava/lang/Object;
.source "EpisodesDataSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ForWatchNow"
.end annotation


# static fields
.field private static final PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 548
    sget-object v0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery;->BASE_PROJECTION:[Ljava/lang/String;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "(SELECT COUNT(*) FROM purchased_assets WHERE account = user_assets_account AND asset_type = 20 AND root_asset_id = user_assets_id AND purchase_status = 2 AND NOT (hidden IN (1, 3)))"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "season_seqno_of_last_activity"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "(SELECT episode_seqno FROM assets, user_assets AS user_episodes WHERE user_episodes.user_assets_account = user_assets.user_assets_account AND user_episodes.user_assets_type = 20 AND assets_type = 20 AND user_episodes.user_assets_id = assets_id AND root_id = user_assets.user_assets_id AND season_seqno = season_seqno_of_last_activity AND user_episodes.last_activity_timestamp >= :mlat ORDER BY episode_seqno LIMIT 1)"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "(SELECT last_watched_episode_seqno FROM purchased_assets WHERE account = user_assets_account AND asset_type = 20 AND asset_id IS last_watched_episode_id AND last_watched_timestamp >= :mlat)"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "last_watched_season_seqno"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "watched_to_end_credit"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/DbUtils;->extend([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public static createRequest(Ljava/lang/String;ZJZZI)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 12
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "downloadedOnly"    # Z
    .param p2, "minLastActivityTimestamp"    # J
    .param p4, "excludeFinishedLastWatched"    # Z
    .param p5, "allowUnpurchased"    # Z
    .param p6, "maxEpisodesPerShow"    # I

    .prologue
    .line 593
    const/4 v1, 0x2

    new-array v7, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v7, v1

    const/4 v1, 0x1

    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v7, v1

    .line 597
    .local v7, "whereArgs":[Ljava/lang/String;
    new-instance v0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow$1;

    move-object v1, p0

    move/from16 v2, p6

    move v3, p1

    move/from16 v4, p4

    move/from16 v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow$1;-><init>(Ljava/lang/String;IZZZ)V

    .line 649
    .local v0, "subRequestCreator":Lcom/google/android/videos/store/PurchaseStore$SubRequestCreator;
    if-eqz p1, :cond_0

    const-string v6, "user_assets_account = ? AND last_activity_timestamp >= :mlat AND (pinned IS NOT NULL AND pinned > 0)"

    .line 650
    .local v6, "where":Ljava/lang/String;
    :goto_0
    new-instance v1, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    const/4 v2, 0x0

    const-string v3, "user_assets, shows ON user_assets_type = 18 AND user_assets_id = shows_id, purchased_assets ON account = user_assets_account AND asset_type = 20 AND root_asset_id = user_assets_id"

    sget-object v4, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow;->PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    const-string v8, "user_assets_id"

    const-string v9, "last_activity_timestamp DESC"

    const/4 v10, -0x1

    move-object v11, v0

    invoke-direct/range {v1 .. v11}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/videos/store/PurchaseStore$SubRequestCreator;)V

    return-object v1

    .line 649
    .end local v6    # "where":Ljava/lang/String;
    :cond_0
    const-string v6, "user_assets_account = ? AND last_activity_timestamp >= :mlat"

    goto :goto_0
.end method
