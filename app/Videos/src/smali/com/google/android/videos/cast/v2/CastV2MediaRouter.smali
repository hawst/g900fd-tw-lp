.class public Lcom/google/android/videos/cast/v2/CastV2MediaRouter;
.super Lcom/google/android/videos/remote/RemoteControlListener$BaseRemoteControlListener;
.source "CastV2MediaRouter.java"

# interfaces
.implements Lcom/google/android/videos/cast/CastMediaRouter;


# instance fields
.field private final context:Landroid/content/Context;

.field private final controlCategory:Ljava/lang/String;

.field private managerListener:Lcom/google/android/videos/remote/MediaRouteManager$Listener;

.field private final mediaRouteSelector:Landroid/support/v7/media/MediaRouteSelector;

.field private final mediaRouter:Landroid/support/v7/media/MediaRouter;

.field private final mpdUrlGetRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/MpdUrlGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final preferences:Landroid/content/SharedPreferences;

.field private final receiverAppId:Ljava/lang/String;

.field private final recommendationsRequestFactory:Lcom/google/android/videos/api/RecommendationsRequest$Factory;

.field private final recommendationsRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/RecommendationsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private remoteControl:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

.field private final robotTokenRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/RobotTokenRequest;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final userDisplayName:Ljava/lang/String;

.field private final verboseLogging:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/api/RecommendationsRequest$Factory;Lcom/google/android/videos/async/Requester;Landroid/content/SharedPreferences;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "receiverAppId"    # Ljava/lang/String;
    .param p5, "recommendationsRequestFactory"    # Lcom/google/android/videos/api/RecommendationsRequest$Factory;
    .param p7, "preferences"    # Landroid/content/SharedPreferences;
    .param p8, "verboseLogging"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/MpdUrlGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;",
            ">;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/RecommendationsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;",
            ">;",
            "Lcom/google/android/videos/api/RecommendationsRequest$Factory;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/RobotTokenRequest;",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/SharedPreferences;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 56
    .local p3, "mpdUrlGetRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/MpdUrlGetRequest;Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;>;"
    .local p4, "recommendationsRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/RecommendationsRequest;Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;>;"
    .local p6, "robotTokenRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/RobotTokenRequest;Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemoteControlListener$BaseRemoteControlListener;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->context:Landroid/content/Context;

    .line 59
    iput-object p2, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->receiverAppId:Ljava/lang/String;

    .line 60
    invoke-static {p2}, Lcom/google/android/gms/cast/CastMediaControlIntent;->categoryForCast(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->controlCategory:Ljava/lang/String;

    .line 61
    iput-object p3, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->mpdUrlGetRequester:Lcom/google/android/videos/async/Requester;

    .line 62
    iput-object p6, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->robotTokenRequester:Lcom/google/android/videos/async/Requester;

    .line 63
    iput-object p4, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->recommendationsRequester:Lcom/google/android/videos/async/Requester;

    .line 64
    iput-object p5, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->recommendationsRequestFactory:Lcom/google/android/videos/api/RecommendationsRequest$Factory;

    .line 65
    invoke-static {p1}, Landroid/support/v7/media/MediaRouter;->getInstance(Landroid/content/Context;)Landroid/support/v7/media/MediaRouter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->mediaRouter:Landroid/support/v7/media/MediaRouter;

    .line 66
    new-instance v0, Landroid/support/v7/media/MediaRouteSelector$Builder;

    invoke-direct {v0}, Landroid/support/v7/media/MediaRouteSelector$Builder;-><init>()V

    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->controlCategory:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v7/media/MediaRouteSelector$Builder;->addControlCategory(Ljava/lang/String;)Landroid/support/v7/media/MediaRouteSelector$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouteSelector$Builder;->build()Landroid/support/v7/media/MediaRouteSelector;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->mediaRouteSelector:Landroid/support/v7/media/MediaRouteSelector;

    .line 68
    iput-object p7, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->preferences:Landroid/content/SharedPreferences;

    .line 69
    iput-boolean p8, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->verboseLogging:Z

    .line 71
    invoke-static {}, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->getDeviceName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->userDisplayName:Ljava/lang/String;

    .line 72
    return-void
.end method

.method private static getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public createRemote(Landroid/support/v7/media/MediaRouter$RouteInfo;Lcom/google/android/videos/remote/MediaRouteManager$Listener;)V
    .locals 11
    .param p1, "routeInfo"    # Landroid/support/v7/media/MediaRouter$RouteInfo;
    .param p2, "listener"    # Lcom/google/android/videos/remote/MediaRouteManager$Listener;

    .prologue
    .line 101
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    iput-object p2, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->managerListener:Lcom/google/android/videos/remote/MediaRouteManager$Listener;

    .line 104
    new-instance v0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->receiverAppId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->mpdUrlGetRequester:Lcom/google/android/videos/async/Requester;

    iget-object v6, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->recommendationsRequester:Lcom/google/android/videos/async/Requester;

    iget-object v7, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->recommendationsRequestFactory:Lcom/google/android/videos/api/RecommendationsRequest$Factory;

    iget-object v8, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->robotTokenRequester:Lcom/google/android/videos/async/Requester;

    iget-object v9, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->preferences:Landroid/content/SharedPreferences;

    iget-boolean v10, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->verboseLogging:Z

    move-object v3, p1

    move-object v4, p0

    invoke-direct/range {v0 .. v10}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/support/v7/media/MediaRouter$RouteInfo;Lcom/google/android/videos/cast/CastMediaRouter;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/api/RecommendationsRequest$Factory;Lcom/google/android/videos/async/Requester;Landroid/content/SharedPreferences;Z)V

    iput-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->remoteControl:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    .line 107
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->remoteControl:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->addListener(Lcom/google/android/videos/remote/RemoteControlListener;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->remoteControl:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    invoke-virtual {v0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->startConnect()V

    .line 109
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->managerListener:Lcom/google/android/videos/remote/MediaRouteManager$Listener;

    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->remoteControl:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    invoke-interface {v0, v1}, Lcom/google/android/videos/remote/MediaRouteManager$Listener;->onRemoteControlSelected(Lcom/google/android/videos/remote/RemoteControl;)V

    .line 110
    return-void
.end method

.method public getRouteSelector()Landroid/support/v7/media/MediaRouteSelector;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->mediaRouteSelector:Landroid/support/v7/media/MediaRouteSelector;

    return-object v0
.end method

.method public getUserDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->userDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public isCastDevice(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z
    .locals 1
    .param p1, "routeInfo"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->controlCategory:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->supportsControlCategory(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public onDisconnected()V
    .locals 2

    .prologue
    .line 115
    invoke-static {}, Lcom/google/android/videos/utils/Preconditions;->checkMainThread()V

    .line 116
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->remoteControl:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    invoke-virtual {v0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->getRouteInfo()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->mediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-virtual {v1}, Landroid/support/v7/media/MediaRouter;->getSelectedRoute()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;->mediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter;->getDefaultRoute()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->select()V

    .line 120
    :cond_0
    return-void
.end method

.method public start()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 95
    invoke-static {}, Lcom/google/android/videos/utils/Preconditions;->checkMainThread()V

    .line 97
    return-void
.end method
