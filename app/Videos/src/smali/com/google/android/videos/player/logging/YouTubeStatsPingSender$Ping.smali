.class public final Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;
.super Lcom/google/android/videos/async/Request;
.source "YouTubeStatsPingSender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Ping"
.end annotation


# instance fields
.field public final sessionNonce:Ljava/lang/String;

.field public final uri:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "sessionNonce"    # Ljava/lang/String;
    .param p3, "uri"    # Ljava/lang/String;

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/async/Request;-><init>(Ljava/lang/String;Z)V

    .line 52
    iput-object p2, p0, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;->sessionNonce:Ljava/lang/String;

    .line 53
    iput-object p3, p0, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;->uri:Ljava/lang/String;

    .line 54
    return-void
.end method

.method public static createQoePing(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;
    .locals 2
    .param p0, "sessionNonce"    # Ljava/lang/String;
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 43
    new-instance v0, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0, p1}, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static createVssPing(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;
    .locals 1
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "sessionNonce"    # Ljava/lang/String;
    .param p2, "uri"    # Ljava/lang/String;

    .prologue
    .line 47
    new-instance v0, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
