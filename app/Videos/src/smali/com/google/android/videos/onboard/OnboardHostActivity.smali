.class public Lcom/google/android/videos/onboard/OnboardHostActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "OnboardHostActivity.java"


# static fields
.field private static final HOST_FRAGMENT_TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/google/android/videos/onboard/VideosOnboardHostFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/videos/onboard/OnboardHostActivity;->HOST_FRAGMENT_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method

.method public static createIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/videos/onboard/OnboardHostActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method private initializeFragment()V
    .locals 5

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/google/android/videos/onboard/OnboardHostActivity;->hostFragment()Lcom/google/android/videos/onboard/VideosOnboardHostFragment;

    move-result-object v0

    .line 34
    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    if-nez v0, :cond_0

    .line 35
    invoke-virtual {p0}, Lcom/google/android/videos/onboard/OnboardHostActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 36
    .local v1, "transaction":Landroid/support/v4/app/FragmentTransaction;
    const v2, 0x1020002

    new-instance v3, Lcom/google/android/videos/onboard/VideosOnboardHostFragment;

    invoke-direct {v3}, Lcom/google/android/videos/onboard/VideosOnboardHostFragment;-><init>()V

    sget-object v4, Lcom/google/android/videos/onboard/OnboardHostActivity;->HOST_FRAGMENT_TAG:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 37
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 39
    .end local v1    # "transaction":Landroid/support/v4/app/FragmentTransaction;
    :cond_0
    return-void
.end method

.method private startSync()V
    .locals 4

    .prologue
    .line 47
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v1

    .line 48
    .local v1, "videosGlobals":Lcom/google/android/videos/VideosGlobals;
    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v0

    .line 49
    .local v0, "account":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 50
    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/videos/accounts/SignInManager;->chooseFirstAccount()Ljava/lang/String;

    move-result-object v0

    .line 52
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 53
    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/google/android/videos/store/SyncService;->startSync(Landroid/accounts/Account;Z)V

    .line 56
    :cond_1
    return-void
.end method


# virtual methods
.method protected hostFragment()Lcom/google/android/videos/onboard/VideosOnboardHostFragment;
    .locals 2

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/google/android/videos/onboard/OnboardHostActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    sget-object v1, Lcom/google/android/videos/onboard/OnboardHostActivity;->HOST_FRAGMENT_TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/onboard/VideosOnboardHostFragment;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 28
    invoke-direct {p0}, Lcom/google/android/videos/onboard/OnboardHostActivity;->startSync()V

    .line 29
    invoke-direct {p0}, Lcom/google/android/videos/onboard/OnboardHostActivity;->initializeFragment()V

    .line 30
    return-void
.end method

.method public onFinishedOnboardFlow()V
    .locals 0

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/google/android/videos/onboard/OnboardHostActivity;->finish()V

    .line 60
    return-void
.end method
