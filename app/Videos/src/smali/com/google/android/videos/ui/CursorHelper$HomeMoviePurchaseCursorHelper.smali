.class public abstract Lcom/google/android/videos/ui/CursorHelper$HomeMoviePurchaseCursorHelper;
.super Lcom/google/android/videos/ui/CursorHelper$HomePurchaseCursorHelper;
.source "CursorHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/CursorHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "HomeMoviePurchaseCursorHelper"
.end annotation


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/videos/Config;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/ui/SyncHelper;Lcom/google/android/videos/utils/DownloadedOnlyManager;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "config"    # Lcom/google/android/videos/Config;
    .param p3, "uiHandler"    # Landroid/os/Handler;
    .param p4, "database"    # Lcom/google/android/videos/store/Database;
    .param p5, "store"    # Lcom/google/android/videos/store/PurchaseStore;
    .param p6, "syncHelper"    # Lcom/google/android/videos/ui/SyncHelper;
    .param p7, "downloadedOnlyManager"    # Lcom/google/android/videos/utils/DownloadedOnlyManager;

    .prologue
    .line 383
    invoke-direct/range {p0 .. p7}, Lcom/google/android/videos/ui/CursorHelper$HomePurchaseCursorHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/Config;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/ui/SyncHelper;Lcom/google/android/videos/utils/DownloadedOnlyManager;)V

    .line 384
    return-void
.end method


# virtual methods
.method public onMovieMetadataUpdated(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 388
    .local p1, "movieIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/google/android/videos/ui/CursorHelper$HomeMoviePurchaseCursorHelper;->scheduleUpdate()V

    .line 389
    return-void
.end method

.method public onMovieUserAssetsUpdated(Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 393
    .local p2, "movieIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0, p1}, Lcom/google/android/videos/ui/CursorHelper$HomeMoviePurchaseCursorHelper;->maybeScheduleUpdate(Ljava/lang/String;)V

    .line 394
    return-void
.end method
