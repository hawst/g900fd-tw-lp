.class Lcom/google/android/videos/player/DirectorInitializer$2;
.super Ljava/lang/Object;
.source "DirectorInitializer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/player/DirectorInitializer;->maybePreemptDrm(Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/player/DirectorInitializer;

.field final synthetic val$isOffline:Z

.field final synthetic val$mimeType:Ljava/lang/String;

.field final synthetic val$psshData:[B


# direct methods
.method constructor <init>(Lcom/google/android/videos/player/DirectorInitializer;[BLjava/lang/String;Z)V
    .locals 0

    .prologue
    .line 418
    iput-object p1, p0, Lcom/google/android/videos/player/DirectorInitializer$2;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    iput-object p2, p0, Lcom/google/android/videos/player/DirectorInitializer$2;->val$psshData:[B

    iput-object p3, p0, Lcom/google/android/videos/player/DirectorInitializer$2;->val$mimeType:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/google/android/videos/player/DirectorInitializer$2;->val$isOffline:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 421
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer$2;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->listener:Lcom/google/android/videos/player/DirectorInitializer$Listener;
    invoke-static {v0}, Lcom/google/android/videos/player/DirectorInitializer;->access$700(Lcom/google/android/videos/player/DirectorInitializer;)Lcom/google/android/videos/player/DirectorInitializer$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/player/DirectorInitializer$2;->val$psshData:[B

    iget-object v2, p0, Lcom/google/android/videos/player/DirectorInitializer$2;->val$mimeType:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/videos/player/DirectorInitializer$2;->val$isOffline:Z

    iget-object v4, p0, Lcom/google/android/videos/player/DirectorInitializer$2;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->cencSecurityLevel:I
    invoke-static {v4}, Lcom/google/android/videos/player/DirectorInitializer;->access$500(Lcom/google/android/videos/player/DirectorInitializer;)I

    move-result v4

    iget-object v5, p0, Lcom/google/android/videos/player/DirectorInitializer$2;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->cencKeySetId:[B
    invoke-static {v5}, Lcom/google/android/videos/player/DirectorInitializer;->access$600(Lcom/google/android/videos/player/DirectorInitializer;)[B

    move-result-object v5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/videos/player/DirectorInitializer$Listener;->onDrmData([BLjava/lang/String;ZI[B)V

    .line 422
    return-void
.end method
