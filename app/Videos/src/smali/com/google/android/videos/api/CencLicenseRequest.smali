.class public Lcom/google/android/videos/api/CencLicenseRequest;
.super Lcom/google/android/videos/async/Request;
.source "CencLicenseRequest.java"


# instance fields
.field public final data:[B

.field public final isAlreadyPinned:Z

.field public final keyRequestType:I

.field public final timeSinceStartedMillis:J

.field public final url:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;[BZJ)V
    .locals 0
    .param p1, "keyRequestType"    # I
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "url"    # Ljava/lang/String;
    .param p4, "data"    # [B
    .param p5, "isAlreadyPinned"    # Z
    .param p6, "timeSinceStartedMillis"    # J

    .prologue
    .line 27
    invoke-direct {p0, p2}, Lcom/google/android/videos/async/Request;-><init>(Ljava/lang/String;)V

    .line 28
    iput p1, p0, Lcom/google/android/videos/api/CencLicenseRequest;->keyRequestType:I

    .line 29
    iput-object p3, p0, Lcom/google/android/videos/api/CencLicenseRequest;->url:Ljava/lang/String;

    .line 30
    iput-object p4, p0, Lcom/google/android/videos/api/CencLicenseRequest;->data:[B

    .line 31
    iput-boolean p5, p0, Lcom/google/android/videos/api/CencLicenseRequest;->isAlreadyPinned:Z

    .line 32
    iput-wide p6, p0, Lcom/google/android/videos/api/CencLicenseRequest;->timeSinceStartedMillis:J

    .line 33
    return-void
.end method
