.class Lcom/google/android/videos/pano/nowtifications/NowtificationFromPromoItemFactory;
.super Ljava/lang/Object;
.source "NowtificationFromPromoItemFactory.java"

# interfaces
.implements Lcom/google/android/repolib/common/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Factory",
        "<",
        "Lcom/google/android/videos/pano/nowtifications/Nowtification;",
        "Lcom/google/android/videos/pano/model/GroupedPromoItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final bitmapRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final context:Landroid/content/Context;

.field private final promoOverlayDrawable:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/videos/async/Requester;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Landroid/graphics/Bitmap;>;"
    const/4 v5, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/google/android/videos/pano/nowtifications/NowtificationFromPromoItemFactory;->context:Landroid/content/Context;

    .line 46
    iput-object p2, p0, Lcom/google/android/videos/pano/nowtifications/NowtificationFromPromoItemFactory;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    .line 47
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 48
    .local v1, "resources":Landroid/content/res/Resources;
    const v2, 0x7f0200af

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 49
    .local v0, "promoOverlayBitmap":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v2, v1, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v2, p0, Lcom/google/android/videos/pano/nowtifications/NowtificationFromPromoItemFactory;->promoOverlayDrawable:Landroid/graphics/drawable/Drawable;

    .line 50
    iget-object v2, p0, Lcom/google/android/videos/pano/nowtifications/NowtificationFromPromoItemFactory;->promoOverlayDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 52
    return-void
.end method


# virtual methods
.method public createFrom(Lcom/google/android/videos/pano/model/GroupedPromoItem;)Lcom/google/android/videos/pano/nowtifications/Nowtification;
    .locals 17
    .param p1, "groupedItem"    # Lcom/google/android/videos/pano/model/GroupedPromoItem;

    .prologue
    .line 58
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/videos/pano/model/GroupedPromoItem;->getPromoItem()Lcom/google/android/videos/pano/model/PromoItem;

    move-result-object v8

    .line 59
    .local v8, "item":Lcom/google/android/videos/pano/model/PromoItem;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/videos/pano/model/GroupedPromoItem;->getGroup()Ljava/lang/String;

    move-result-object v5

    .line 60
    .local v5, "group":Ljava/lang/String;
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 61
    .local v4, "extras":Landroid/os/Bundle;
    const-string v12, "android.backgroundImageUri"

    iget-object v13, v8, Lcom/google/android/videos/pano/model/PromoItem;->wallpaperUri:Ljava/lang/String;

    invoke-static {v13}, Lcom/google/android/videos/pano/nowtifications/NowtificationsContentProvider;->createBitmapUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    invoke-virtual {v13}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v4, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v1

    .line 64
    .local v1, "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Landroid/net/Uri;Landroid/graphics/Bitmap;>;"
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/pano/nowtifications/NowtificationFromPromoItemFactory;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    iget-object v13, v8, Lcom/google/android/videos/pano/model/PromoItem;->posterUri:Ljava/lang/String;

    invoke-static {v13}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    invoke-interface {v12, v13, v1}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 65
    const/4 v10, 0x0

    .line 67
    .local v10, "posterBitmap":Landroid/graphics/Bitmap;
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;

    move-result-object v12

    move-object v0, v12

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v10, v0
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 74
    :goto_0
    const/4 v9, 0x0

    .line 75
    .local v9, "largeIcon":Landroid/graphics/Bitmap;
    if-eqz v10, :cond_0

    .line 76
    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v14

    invoke-static {v12, v13, v14}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 78
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v9}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 79
    .local v2, "canvas":Landroid/graphics/Canvas;
    new-instance v11, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/pano/nowtifications/NowtificationFromPromoItemFactory;->context:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    invoke-direct {v11, v12, v10}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 80
    .local v11, "posterDrawable":Landroid/graphics/drawable/Drawable;
    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v14

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v15

    invoke-virtual {v11, v12, v13, v14, v15}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 81
    invoke-virtual {v11, v2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 82
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/pano/nowtifications/NowtificationFromPromoItemFactory;->promoOverlayDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v12, v2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 85
    .end local v2    # "canvas":Landroid/graphics/Canvas;
    .end local v11    # "posterDrawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    new-instance v6, Landroid/content/Intent;

    iget-object v12, v8, Lcom/google/android/videos/pano/model/PromoItem;->intent:Landroid/content/Intent;

    invoke-direct {v6, v12}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 86
    .local v6, "intent":Landroid/content/Intent;
    const/4 v12, 0x1

    invoke-virtual {v6, v12}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    invoke-virtual {v6, v12}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 87
    const/4 v12, 0x2

    new-array v7, v12, [Landroid/content/Intent;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/videos/pano/nowtifications/NowtificationFromPromoItemFactory;->context:Landroid/content/Context;

    invoke-static {v13}, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->createIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v13

    aput-object v13, v7, v12

    const/4 v12, 0x1

    aput-object v6, v7, v12

    .line 90
    .local v7, "intents":[Landroid/content/Intent;
    new-instance v13, Lcom/google/android/videos/pano/nowtifications/Nowtification;

    new-instance v12, Landroid/app/Notification$Builder;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/pano/nowtifications/NowtificationFromPromoItemFactory;->context:Landroid/content/Context;

    invoke-direct {v12, v14}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const/4 v14, 0x0

    invoke-virtual {v12, v14}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v12

    const/4 v14, 0x0

    invoke-virtual {v12, v14}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    move-result-object v12

    const/4 v14, 0x1

    invoke-virtual {v12, v14}, Landroid/app/Notification$Builder;->setOnlyAlertOnce(Z)Landroid/app/Notification$Builder;

    move-result-object v12

    const/4 v14, 0x1

    invoke-virtual {v12, v14}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v12

    const v14, 0x7f0200cf

    invoke-virtual {v12, v14}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v12

    iget-object v14, v8, Lcom/google/android/videos/pano/model/PromoItem;->title:Ljava/lang/String;

    invoke-virtual {v12, v14}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v14

    iget-object v12, v8, Lcom/google/android/videos/pano/model/PromoItem;->subtitle:Ljava/lang/String;

    invoke-static {v12, v5}, Lcom/google/android/videos/utils/Util;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/CharSequence;

    invoke-virtual {v14, v12}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/pano/nowtifications/NowtificationFromPromoItemFactory;->context:Landroid/content/Context;

    const v15, 0x7f0b007f

    invoke-virtual {v14, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Landroid/app/Notification$Builder;->setContentInfo(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v12

    invoke-virtual {v12, v4}, Landroid/app/Notification$Builder;->setExtras(Landroid/os/Bundle;)Landroid/app/Notification$Builder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/pano/nowtifications/NowtificationFromPromoItemFactory;->context:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0a0079

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getColor(I)I

    move-result v14

    invoke-virtual {v12, v14}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;

    move-result-object v12

    invoke-virtual {v12, v5}, Landroid/app/Notification$Builder;->setGroup(Ljava/lang/String;)Landroid/app/Notification$Builder;

    move-result-object v12

    const-string v14, "recommendation"

    invoke-virtual {v12, v14}, Landroid/app/Notification$Builder;->setCategory(Ljava/lang/String;)Landroid/app/Notification$Builder;

    move-result-object v12

    invoke-virtual {v12, v9}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/pano/nowtifications/NowtificationFromPromoItemFactory;->context:Landroid/content/Context;

    const/4 v15, 0x0

    const/high16 v16, 0x8000000

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0}, Landroid/app/PendingIntent;->getActivities(Landroid/content/Context;I[Landroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v14

    invoke-virtual {v12, v14}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v12

    invoke-virtual {v8}, Lcom/google/android/videos/pano/model/PromoItem;->getEntityId()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v12, v14}, Lcom/google/android/videos/pano/nowtifications/Nowtification;-><init>(Landroid/app/Notification$Builder;Ljava/lang/String;)V

    return-object v13

    .line 68
    .end local v6    # "intent":Landroid/content/Intent;
    .end local v7    # "intents":[Landroid/content/Intent;
    .end local v9    # "largeIcon":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v3

    .line 69
    .local v3, "e":Ljava/util/concurrent/CancellationException;
    const-string v12, "Failed to load poster bitmap."

    invoke-static {v12, v3}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 70
    .end local v3    # "e":Ljava/util/concurrent/CancellationException;
    :catch_1
    move-exception v3

    .line 71
    .local v3, "e":Ljava/util/concurrent/ExecutionException;
    const-string v12, "Failed to load poster bitmap."

    invoke-static {v12, v3}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method

.method public bridge synthetic createFrom(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 37
    check-cast p1, Lcom/google/android/videos/pano/model/GroupedPromoItem;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/pano/nowtifications/NowtificationFromPromoItemFactory;->createFrom(Lcom/google/android/videos/pano/model/GroupedPromoItem;)Lcom/google/android/videos/pano/nowtifications/Nowtification;

    move-result-object v0

    return-object v0
.end method
