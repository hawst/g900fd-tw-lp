.class final Lcom/google/android/videos/player/exo/BlockBufferPool;
.super Ljava/lang/Object;
.source "BlockBufferPool.java"

# interfaces
.implements Lcom/google/android/exoplayer/upstream/Allocator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/exo/BlockBufferPool$1;,
        Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;
    }
.end annotation


# instance fields
.field private allocationCount:I

.field private allocations:[[B

.field private availableFragmentCount:I

.field private availableFragmentIndices:[I

.field private availableFragmentOffsets:[I

.field private fragmentCount:I

.field public final fragmentLength:I


# direct methods
.method public constructor <init>(II)V
    .locals 3
    .param p1, "fragmentLength"    # I
    .param p2, "initialBlockFragmentCount"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    if-lez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer/util/Assertions;->checkArgument(Z)V

    .line 40
    if-lez p2, :cond_1

    :goto_1
    invoke-static {v1}, Lcom/google/android/exoplayer/util/Assertions;->checkArgument(Z)V

    .line 41
    iput p1, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->fragmentLength:I

    .line 44
    const/4 v0, 0x5

    new-array v0, v0, [[B

    iput-object v0, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->allocations:[[B

    .line 48
    mul-int/lit8 v0, p2, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->availableFragmentIndices:[I

    .line 49
    mul-int/lit8 v0, p2, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->availableFragmentOffsets:[I

    .line 51
    invoke-direct {p0, p2}, Lcom/google/android/videos/player/exo/BlockBufferPool;->allocateNewBlock(I)V

    .line 52
    return-void

    :cond_0
    move v0, v2

    .line 39
    goto :goto_0

    :cond_1
    move v1, v2

    .line 40
    goto :goto_1
.end method

.method private allocateNewBlock(I)V
    .locals 5
    .param p1, "blockFragmentCount"    # I

    .prologue
    .line 127
    iget v2, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->allocationCount:I

    iget-object v3, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->allocations:[[B

    array-length v3, v3

    if-ne v2, v3, :cond_0

    .line 129
    iget-object v2, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->allocations:[[B

    iget-object v3, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->allocations:[[B

    array-length v3, v3

    invoke-direct {p0, v2, v3}, Lcom/google/android/videos/player/exo/BlockBufferPool;->expandArray([[BI)[[B

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->allocations:[[B

    .line 131
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->availableFragmentIndices:[I

    array-length v2, v2

    iget v3, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->fragmentCount:I

    add-int/2addr v3, p1

    if-ge v2, v3, :cond_1

    .line 133
    iget-object v2, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->availableFragmentIndices:[I

    array-length v2, v2

    iget v3, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->fragmentCount:I

    add-int/2addr v3, p1

    iget-object v4, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->availableFragmentIndices:[I

    array-length v4, v4

    sub-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 135
    .local v0, "extraCapacity":I
    iget-object v2, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->availableFragmentIndices:[I

    invoke-direct {p0, v2, v0}, Lcom/google/android/videos/player/exo/BlockBufferPool;->expandArray([II)[I

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->availableFragmentIndices:[I

    .line 136
    iget-object v2, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->availableFragmentOffsets:[I

    invoke-direct {p0, v2, v0}, Lcom/google/android/videos/player/exo/BlockBufferPool;->expandArray([II)[I

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->availableFragmentOffsets:[I

    .line 138
    .end local v0    # "extraCapacity":I
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->allocations:[[B

    iget v3, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->allocationCount:I

    iget v4, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->fragmentLength:I

    mul-int/2addr v4, p1

    new-array v4, v4, [B

    aput-object v4, v2, v3

    .line 139
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_0
    if-ge v1, p1, :cond_2

    .line 140
    iget-object v2, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->availableFragmentIndices:[I

    iget v3, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->availableFragmentCount:I

    iget v4, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->allocationCount:I

    aput v4, v2, v3

    .line 141
    iget-object v2, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->availableFragmentOffsets:[I

    iget v3, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->availableFragmentCount:I

    iget v4, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->fragmentLength:I

    mul-int/2addr v4, v1

    aput v4, v2, v3

    .line 142
    iget v2, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->availableFragmentCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->availableFragmentCount:I

    .line 139
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 144
    :cond_2
    iget v2, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->allocationCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->allocationCount:I

    .line 145
    iget v2, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->fragmentCount:I

    add-int/2addr v2, p1

    iput v2, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->fragmentCount:I

    .line 146
    return-void
.end method

.method private expandArray([II)[I
    .locals 3
    .param p1, "original"    # [I
    .param p2, "extraCapacity"    # I

    .prologue
    const/4 v2, 0x0

    .line 159
    array-length v1, p1

    add-int/2addr v1, p2

    new-array v0, v1, [I

    .line 160
    .local v0, "expanded":[I
    array-length v1, p1

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 161
    return-object v0
.end method

.method private expandArray([[BI)[[B
    .locals 3
    .param p1, "original"    # [[B
    .param p2, "extraCapacity"    # I

    .prologue
    const/4 v2, 0x0

    .line 153
    array-length v1, p1

    add-int/2addr v1, p2

    new-array v0, v1, [[B

    .line 154
    .local v0, "expanded":[[B
    array-length v1, p1

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 155
    return-object v0
.end method

.method private requiredBufferCount(J)I
    .locals 5
    .param p1, "size"    # J

    .prologue
    .line 149
    iget v0, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->fragmentLength:I

    int-to-long v0, v0

    add-long/2addr v0, p1

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    iget v2, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->fragmentLength:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method


# virtual methods
.method public declared-synchronized allocate(I)Lcom/google/android/exoplayer/upstream/Allocation;
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 66
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;-><init>(Lcom/google/android/videos/player/exo/BlockBufferPool;Lcom/google/android/videos/player/exo/BlockBufferPool$1;)V

    .line 67
    .local v0, "allocation":Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;
    invoke-virtual {p0, v0, p1}, Lcom/google/android/videos/player/exo/BlockBufferPool;->ensureCapacity(Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 68
    monitor-exit p0

    return-object v0

    .line 66
    .end local v0    # "allocation":Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method declared-synchronized ensureCapacity(Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;I)V
    .locals 9
    .param p1, "allocation"    # Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;
    .param p2, "size"    # I

    .prologue
    const/4 v1, 0x0

    .line 72
    monitor-enter p0

    int-to-long v6, p2

    :try_start_0
    invoke-direct {p0, v6, v7}, Lcom/google/android/videos/player/exo/BlockBufferPool;->requiredBufferCount(J)I

    move-result v5

    .line 73
    .local v5, "requiredBufferCount":I
    iget-object v6, p1, Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;->buffers:[[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v6, :cond_0

    .line 74
    .local v1, "existingBufferCount":I
    :goto_0
    if-gt v5, v1, :cond_1

    .line 105
    :goto_1
    monitor-exit p0

    return-void

    .line 73
    .end local v1    # "existingBufferCount":I
    :cond_0
    :try_start_1
    iget-object v6, p1, Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;->buffers:[[B

    array-length v1, v6

    goto :goto_0

    .line 80
    .restart local v1    # "existingBufferCount":I
    :cond_1
    iget v6, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->availableFragmentCount:I

    if-ge v6, v5, :cond_2

    .line 81
    iget v6, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->availableFragmentCount:I

    sub-int v6, v5, v6

    invoke-direct {p0, v6}, Lcom/google/android/videos/player/exo/BlockBufferPool;->allocateNewBlock(I)V

    .line 84
    :cond_2
    new-array v0, v5, [[B

    .line 85
    .local v0, "buffers":[[B
    new-array v2, v5, [I

    .line 86
    .local v2, "fragmentIndices":[I
    new-array v3, v5, [I

    .line 89
    .local v3, "fragmentOffsets":[I
    if-lez v1, :cond_3

    .line 90
    iget-object v6, p1, Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;->buffers:[[B

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v6, v7, v0, v8, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 91
    iget-object v6, p1, Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;->fragmentIndices:[I

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v6, v7, v2, v8, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 92
    iget-object v6, p1, Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;->fragmentOffsets:[I

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v6, v7, v3, v8, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 96
    :cond_3
    move v4, v1

    .local v4, "i":I
    :goto_2
    if-ge v4, v5, :cond_4

    .line 97
    iget v6, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->availableFragmentCount:I

    add-int/lit8 v6, v6, -0x1

    iput v6, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->availableFragmentCount:I

    .line 98
    iget-object v6, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->availableFragmentIndices:[I

    iget v7, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->availableFragmentCount:I

    aget v6, v6, v7

    aput v6, v2, v4

    .line 99
    iget-object v6, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->availableFragmentOffsets:[I

    iget v7, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->availableFragmentCount:I

    aget v6, v6, v7

    aput v6, v3, v4

    .line 100
    iget-object v6, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->allocations:[[B

    aget v7, v2, v4

    aget-object v6, v6, v7

    aput-object v6, v0, v4

    .line 96
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 104
    :cond_4
    invoke-virtual {p1, v0, v2, v3}, Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;->init([[B[I[I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 72
    .end local v0    # "buffers":[[B
    .end local v1    # "existingBufferCount":I
    .end local v2    # "fragmentIndices":[I
    .end local v3    # "fragmentOffsets":[I
    .end local v4    # "i":I
    .end local v5    # "requiredBufferCount":I
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6
.end method

.method public declared-synchronized getAllocatedSize()I
    .locals 2

    .prologue
    .line 56
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->fragmentCount:I

    iget v1, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->availableFragmentCount:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->fragmentLength:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    mul-int/2addr v0, v1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized release(Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;)V
    .locals 5
    .param p1, "allocation"    # Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;

    .prologue
    .line 113
    monitor-enter p0

    :try_start_0
    iget-object v1, p1, Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;->fragmentIndices:[I

    array-length v0, v1

    .line 114
    .local v0, "releasedFragmentCount":I
    iget-object v1, p1, Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;->fragmentIndices:[I

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->availableFragmentIndices:[I

    iget v4, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->availableFragmentCount:I

    invoke-static {v1, v2, v3, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 116
    iget-object v1, p1, Lcom/google/android/videos/player/exo/BlockBufferPool$AllocationImpl;->fragmentOffsets:[I

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->availableFragmentOffsets:[I

    iget v4, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->availableFragmentCount:I

    invoke-static {v1, v2, v3, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 118
    iget v1, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->availableFragmentCount:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/google/android/videos/player/exo/BlockBufferPool;->availableFragmentCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119
    monitor-exit p0

    return-void

    .line 113
    .end local v0    # "releasedFragmentCount":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized trim(I)V
    .locals 0
    .param p1, "targetSize"    # I

    .prologue
    .line 62
    monitor-enter p0

    monitor-exit p0

    return-void
.end method
