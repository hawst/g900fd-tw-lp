.class public interface abstract Lcom/google/android/videos/logging/EventLogger;
.super Ljava/lang/Object;
.source "EventLogger.java"


# virtual methods
.method public abstract onAppDrmInit(II)V
.end method

.method public abstract onAppDrmInitFailed(IIII)V
.end method

.method public abstract onCaptionSettingsPageOpened()V
.end method

.method public abstract onCastDisplayDetected()V
.end method

.method public abstract onDatabaseUpgrade(II)V
.end method

.method public abstract onDatabaseUpgradeError(IILjava/lang/Exception;)V
.end method

.method public abstract onDeviceCapabilitiesStreamFilter(Lcom/google/android/videos/streams/MediaStream;IZ)V
.end method

.method public abstract onDirectPurchase(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onDismissDownloadDialog(Z)V
.end method

.method public abstract onDismissDownloadErrorDialog()V
.end method

.method public abstract onExpandRecentActors()V
.end method

.method public abstract onExternalApiCallFailed(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
.end method

.method public abstract onExternalApiCallSuccess(ILjava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onExternalApiQuery(Ljava/lang/String;)V
.end method

.method public abstract onFrameworkDrmInit(J)V
.end method

.method public abstract onFrameworkDrmInitFailed()V
.end method

.method public abstract onHelpAndFeedbackOpened()V
.end method

.method public abstract onInfoCardDismissed(I)V
.end method

.method public abstract onInfoCardFeedbackReport(Z)V
.end method

.method public abstract onInfoCardsCollapsed(I)V
.end method

.method public abstract onInfoCardsExpanded(IZ)V
.end method

.method public abstract onInfoCardsShown(Z)V
.end method

.method public abstract onLicenseRefreshCompleted(Ljava/lang/String;)V
.end method

.method public abstract onLicenseRefreshError(Ljava/lang/String;Ljava/lang/Throwable;)V
.end method

.method public abstract onLicenseReleaseCompleted(Ljava/lang/String;)V
.end method

.method public abstract onLicenseReleaseError(Ljava/lang/String;Ljava/lang/Throwable;)V
.end method

.method public abstract onModularDrmInit(I)V
.end method

.method public abstract onMovieDetailsPageOpened(Ljava/lang/String;)V
.end method

.method public abstract onOpenedPlayStoreForAsset(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method public abstract onOpenedPlayStoreForMovies(II)V
.end method

.method public abstract onOpenedPlayStoreForMoviesAndShows(II)V
.end method

.method public abstract onOpenedPlayStoreForShows(II)V
.end method

.method public abstract onPinAction(Ljava/lang/String;ZII)V
.end method

.method public abstract onPinClick(Z)V
.end method

.method public abstract onPinningCompleted(Ljava/lang/String;)V
.end method

.method public abstract onPinningError(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Throwable;ZZII)V
.end method

.method public abstract onPlayStoreSearch(ILjava/lang/String;I)V
.end method

.method public abstract onPlayStoreUriOpen(ILjava/lang/String;I)V
.end method

.method public abstract onPlaybackInit(Ljava/lang/String;I)V
.end method

.method public abstract onPlaybackInitError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZILjava/lang/Exception;)V
.end method

.method public abstract onPlaybackPreparationStats(ZLjava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLandroid/util/SparseArray;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZZ",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onPlayerDroppedFrames(I)V
.end method

.method public abstract onPlayerEnded(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/google/android/videos/player/logging/DerivedStats;)V
.end method

.method public abstract onPlayerError(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZIIILjava/lang/Exception;)V
.end method

.method public abstract onPlayerFailed(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZIIILjava/lang/Exception;)V
.end method

.method public abstract onPlayerFormatEnabled(II)V
.end method

.method public abstract onPlayerFormatSelected(II)V
.end method

.method public abstract onPlayerNetworkType()V
.end method

.method public abstract onPlayerSeekingEnd(ZII)V
.end method

.method public abstract onPlayerSeekingStart(ZI)V
.end method

.method public abstract onPlayerStarted(Lcom/google/android/videos/player/logging/SessionTimeProvider;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
.end method

.method public abstract onPlayerStateChanged(IZI)V
.end method

.method public abstract onPlayerSubtitleEnabled(Lcom/google/android/videos/subtitles/SubtitleTrack;)V
.end method

.method public abstract onPlayerSubtitleError(Lcom/google/android/videos/subtitles/SubtitleTrack;)V
.end method

.method public abstract onPlayerSubtitleSelected(Lcom/google/android/videos/subtitles/SubtitleTrack;)V
.end method

.method public abstract onPostPurchaseDialogOpened(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onPreferenceChange(Ljava/lang/String;Ljava/lang/Object;)V
.end method

.method public abstract onPremiumWatchPageOpened(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
.end method

.method public abstract onRemoveItemDialogShown(Ljava/lang/String;)V
.end method

.method public abstract onSettingsPageOpened()V
.end method

.method public abstract onShowDownloadDialog(Z)V
.end method

.method public abstract onShowDownloadErrorDialog(ILjava/lang/Long;Ljava/lang/Integer;)V
.end method

.method public abstract onShowPageOpened(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onSuggestionsError(I)V
.end method

.method public abstract onTransfersPing([II)V
.end method

.method public abstract onUnpinVideo(Ljava/lang/String;Z)V
.end method

.method public abstract onVerticalOpened(I)V
.end method

.method public abstract onVideosStart(Landroid/app/Activity;ILjava/lang/String;)V
.end method

.method public abstract onWebSearch(II)V
.end method

.method public abstract onWishlistAction(Ljava/lang/String;IZI)V
.end method
