.class public Lcom/google/android/videos/api/CategoryListRequest;
.super Lcom/google/android/videos/async/Request;
.source "CategoryListRequest.java"


# instance fields
.field public final countryRestriction:Ljava/lang/String;

.field public final localeRestriction:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "countryRestriction"    # Ljava/lang/String;
    .param p3, "localeRestriction"    # Ljava/util/Locale;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/videos/async/Request;-><init>(Ljava/lang/String;)V

    .line 23
    iput-object p2, p0, Lcom/google/android/videos/api/CategoryListRequest;->countryRestriction:Ljava/lang/String;

    .line 24
    invoke-static {p3}, Lcom/google/android/videos/utils/LocaleUtils;->toString(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/api/CategoryListRequest;->localeRestriction:Ljava/lang/String;

    .line 25
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 29
    if-ne p0, p1, :cond_1

    .line 34
    :cond_0
    :goto_0
    return v1

    .line 30
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 32
    check-cast v0, Lcom/google/android/videos/api/CategoryListRequest;

    .line 34
    .local v0, "that":Lcom/google/android/videos/api/CategoryListRequest;
    iget-object v3, p0, Lcom/google/android/videos/api/CategoryListRequest;->account:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/api/CategoryListRequest;->account:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/videos/api/CategoryListRequest;->countryRestriction:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/api/CategoryListRequest;->countryRestriction:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/videos/api/CategoryListRequest;->localeRestriction:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/api/CategoryListRequest;->localeRestriction:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 41
    iget-object v2, p0, Lcom/google/android/videos/api/CategoryListRequest;->account:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/videos/api/CategoryListRequest;->account:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 42
    .local v0, "result":I
    :goto_0
    mul-int/lit8 v3, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/api/CategoryListRequest;->countryRestriction:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/videos/api/CategoryListRequest;->countryRestriction:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 43
    mul-int/lit8 v2, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/videos/api/CategoryListRequest;->localeRestriction:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/api/CategoryListRequest;->localeRestriction:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 44
    return v0

    .end local v0    # "result":I
    :cond_1
    move v0, v1

    .line 41
    goto :goto_0

    .restart local v0    # "result":I
    :cond_2
    move v2, v1

    .line 42
    goto :goto_1
.end method
