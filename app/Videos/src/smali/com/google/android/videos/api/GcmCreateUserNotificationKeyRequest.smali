.class public Lcom/google/android/videos/api/GcmCreateUserNotificationKeyRequest;
.super Lcom/google/android/videos/async/Request;
.source "GcmCreateUserNotificationKeyRequest.java"


# instance fields
.field public final authToken:Ljava/lang/String;

.field public final projectId:Ljava/lang/String;

.field public final registrationId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "projectId"    # Ljava/lang/String;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "authToken"    # Ljava/lang/String;
    .param p4, "registrationId"    # Ljava/lang/String;

    .prologue
    .line 16
    invoke-direct {p0, p2}, Lcom/google/android/videos/async/Request;-><init>(Ljava/lang/String;)V

    .line 17
    iput-object p4, p0, Lcom/google/android/videos/api/GcmCreateUserNotificationKeyRequest;->registrationId:Ljava/lang/String;

    .line 18
    iput-object p3, p0, Lcom/google/android/videos/api/GcmCreateUserNotificationKeyRequest;->authToken:Ljava/lang/String;

    .line 19
    iput-object p1, p0, Lcom/google/android/videos/api/GcmCreateUserNotificationKeyRequest;->projectId:Ljava/lang/String;

    .line 20
    return-void
.end method
