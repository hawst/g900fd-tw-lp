.class public Lcom/google/android/videos/welcome/DefaultShowsWelcome;
.super Lcom/google/android/videos/welcome/Welcome;
.source "DefaultShowsWelcome.java"


# instance fields
.field private account:Ljava/lang/String;

.field private actionResIds:[I

.field private final activity:Landroid/app/Activity;

.field private final config:Lcom/google/android/videos/Config;

.field private detailMessage:Ljava/lang/String;

.field private freeBrowseUri:Landroid/net/Uri;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/videos/Config;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "config"    # Lcom/google/android/videos/Config;

    .prologue
    .line 27
    const-string v0, "defaultShows"

    invoke-direct {p0, v0}, Lcom/google/android/videos/welcome/Welcome;-><init>(Ljava/lang/String;)V

    .line 28
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/videos/welcome/DefaultShowsWelcome;->activity:Landroid/app/Activity;

    .line 29
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/Config;

    iput-object v0, p0, Lcom/google/android/videos/welcome/DefaultShowsWelcome;->config:Lcom/google/android/videos/Config;

    .line 30
    return-void
.end method


# virtual methods
.method public getActionResIds()[I
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/videos/welcome/DefaultShowsWelcome;->actionResIds:[I

    return-object v0
.end method

.method public getDefaultBitmapResId()I
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    return v0
.end method

.method public getDetailMessage()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/videos/welcome/DefaultShowsWelcome;->detailMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getNetworkBitmapUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/videos/welcome/DefaultShowsWelcome;->title:Ljava/lang/String;

    return-object v0
.end method

.method public onAction(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    const/16 v3, 0x8

    .line 78
    iget-object v0, p0, Lcom/google/android/videos/welcome/DefaultShowsWelcome;->freeBrowseUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/google/android/videos/welcome/DefaultShowsWelcome;->activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/videos/welcome/DefaultShowsWelcome;->freeBrowseUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/videos/welcome/DefaultShowsWelcome;->account:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/videos/utils/PlayStoreUtil;->startForUri(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)V

    .line 83
    :goto_0
    return-void

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/welcome/DefaultShowsWelcome;->activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/videos/welcome/DefaultShowsWelcome;->account:Ljava/lang/String;

    invoke-static {v0, v1, v3}, Lcom/google/android/videos/utils/PlayStoreUtil;->viewShowsVertical(Landroid/app/Activity;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public prepareIfEligible(Ljava/lang/String;ZZ)Z
    .locals 8
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "contentInVertical"    # Z
    .param p3, "downloadedOnly"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 35
    if-nez p2, :cond_0

    if-eqz p3, :cond_1

    :cond_0
    move v1, v2

    .line 48
    :goto_0
    return v1

    .line 38
    :cond_1
    iput-object p1, p0, Lcom/google/android/videos/welcome/DefaultShowsWelcome;->account:Ljava/lang/String;

    .line 39
    iget-object v3, p0, Lcom/google/android/videos/welcome/DefaultShowsWelcome;->config:Lcom/google/android/videos/Config;

    invoke-interface {v3}, Lcom/google/android/videos/Config;->showsWelcomeFreeBrowseUri()Landroid/net/Uri;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/welcome/DefaultShowsWelcome;->freeBrowseUri:Landroid/net/Uri;

    .line 40
    iget-object v3, p0, Lcom/google/android/videos/welcome/DefaultShowsWelcome;->freeBrowseUri:Landroid/net/Uri;

    if-eqz v3, :cond_2

    move v0, v1

    .line 41
    .local v0, "free":Z
    :goto_1
    iget-object v4, p0, Lcom/google/android/videos/welcome/DefaultShowsWelcome;->activity:Landroid/app/Activity;

    if-eqz v0, :cond_3

    const v3, 0x7f0b00ef

    :goto_2
    invoke-virtual {v4, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/welcome/DefaultShowsWelcome;->title:Ljava/lang/String;

    .line 43
    iget-object v4, p0, Lcom/google/android/videos/welcome/DefaultShowsWelcome;->activity:Landroid/app/Activity;

    if-eqz v0, :cond_4

    const v3, 0x7f0b00f0

    :goto_3
    new-array v5, v1, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/videos/welcome/DefaultShowsWelcome;->activity:Landroid/app/Activity;

    const v7, 0x7f0b007f

    invoke-virtual {v6, v7}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v4, v3, v5}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/welcome/DefaultShowsWelcome;->detailMessage:Ljava/lang/String;

    .line 46
    const/4 v3, 0x2

    new-array v3, v3, [I

    const v4, 0x7f0201c2

    aput v4, v3, v2

    if-eqz v0, :cond_5

    const v2, 0x7f0b00f8

    :goto_4
    aput v2, v3, v1

    iput-object v3, p0, Lcom/google/android/videos/welcome/DefaultShowsWelcome;->actionResIds:[I

    goto :goto_0

    .end local v0    # "free":Z
    :cond_2
    move v0, v2

    .line 40
    goto :goto_1

    .line 41
    .restart local v0    # "free":Z
    :cond_3
    const v3, 0x7f0b00e8

    goto :goto_2

    .line 43
    :cond_4
    const v3, 0x7f0b00ee

    goto :goto_3

    .line 46
    :cond_5
    const v2, 0x7f0b00f6

    goto :goto_4
.end method
