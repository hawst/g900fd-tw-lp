.class public Lcom/google/android/videos/ui/DownloadItemView;
.super Lcom/google/android/videos/ui/playnext/ClusterItemView;
.source "DownloadItemView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/DownloadItemView$Binder;
    }
.end annotation


# instance fields
.field private downloadProgressView:Landroid/widget/TextView;

.field private downloadView:Lcom/google/android/videos/ui/DownloadView;

.field private episodeInfoView:Landroid/widget/TextView;

.field private percentFormat:Ljava/text/NumberFormat;

.field private posterRequestId:Ljava/lang/String;

.field private thumbnailBitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

.field private titleView:Landroid/widget/TextView;

.field private watchProgressView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/playnext/ClusterItemView;-><init>(Landroid/content/Context;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/ui/playnext/ClusterItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 57
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/ui/playnext/ClusterItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 58
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/ui/DownloadItemView;Lcom/google/android/videos/adapter/MoviesDataSource;ILcom/google/android/videos/async/Requester;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/ui/DownloadItemView;
    .param p1, "x1"    # Lcom/google/android/videos/adapter/MoviesDataSource;
    .param p2, "x2"    # I
    .param p3, "x3"    # Lcom/google/android/videos/async/Requester;

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/ui/DownloadItemView;->bindMovie(Lcom/google/android/videos/adapter/MoviesDataSource;ILcom/google/android/videos/async/Requester;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/videos/ui/DownloadItemView;Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;ILcom/google/android/videos/async/Requester;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/ui/DownloadItemView;
    .param p1, "x1"    # Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;
    .param p2, "x2"    # I
    .param p3, "x3"    # Lcom/google/android/videos/async/Requester;

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/ui/DownloadItemView;->bindEpisode(Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;ILcom/google/android/videos/async/Requester;)V

    return-void
.end method

.method private bindEpisode(Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;ILcom/google/android/videos/async/Requester;)V
    .locals 12
    .param p1, "dataSource"    # Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;
    .param p2, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;",
            "I",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "posterRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 160
    invoke-virtual {p0}, Lcom/google/android/videos/ui/DownloadItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 161
    .local v2, "resources":Landroid/content/res/Resources;
    invoke-virtual {p1, p2}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->getItem(I)Landroid/database/Cursor;

    move-result-object v0

    .line 163
    .local v0, "cursor":Landroid/database/Cursor;
    invoke-virtual {p1, v0}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->getTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    .line 164
    .local v1, "episodeTitle":Ljava/lang/String;
    invoke-virtual {p1, v0}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->getShowTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    .line 165
    .local v4, "showTitle":Ljava/lang/String;
    const v6, 0x7f0b0258

    new-array v7, v11, [Ljava/lang/Object;

    aput-object v4, v7, v9

    aput-object v1, v7, v10

    invoke-virtual {v2, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 166
    .local v5, "title":Ljava/lang/String;
    invoke-direct {p0, p1, v0, p3, v5}, Lcom/google/android/videos/ui/DownloadItemView;->bindVideo(Lcom/google/android/videos/adapter/VideosDataSource;Landroid/database/Cursor;Lcom/google/android/videos/async/Requester;Ljava/lang/String;)V

    .line 168
    invoke-virtual {p1, v0}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->getSeasonTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    .line 169
    .local v3, "seasonTitle":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 170
    const v6, 0x7f0b00c7

    new-array v7, v10, [Ljava/lang/Object;

    invoke-virtual {p1, v0}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->getSeasonNumber(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {v2, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 174
    :cond_0
    iget-object v6, p0, Lcom/google/android/videos/ui/DownloadItemView;->episodeInfoView:Landroid/widget/TextView;

    const v7, 0x7f0b00c8

    new-array v8, v11, [Ljava/lang/Object;

    aput-object v3, v8, v9

    invoke-virtual {p1, v0}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->getEpisodeNumber(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v2, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    invoke-virtual {p1, v0}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->getShowId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/videos/ui/DownloadItemView;->posterRequestId:Ljava/lang/String;

    .line 180
    return-void
.end method

.method private bindMovie(Lcom/google/android/videos/adapter/MoviesDataSource;ILcom/google/android/videos/async/Requester;)V
    .locals 2
    .param p1, "dataSource"    # Lcom/google/android/videos/adapter/MoviesDataSource;
    .param p2, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/adapter/MoviesDataSource;",
            "I",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 151
    .local p3, "posterRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    invoke-virtual {p1, p2}, Lcom/google/android/videos/adapter/MoviesDataSource;->getItem(I)Landroid/database/Cursor;

    move-result-object v0

    .line 152
    .local v0, "cursor":Landroid/database/Cursor;
    invoke-virtual {p1, v0}, Lcom/google/android/videos/adapter/MoviesDataSource;->getTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v0, p3, v1}, Lcom/google/android/videos/ui/DownloadItemView;->bindVideo(Lcom/google/android/videos/adapter/VideosDataSource;Landroid/database/Cursor;Lcom/google/android/videos/async/Requester;Ljava/lang/String;)V

    .line 154
    invoke-virtual {p1, v0}, Lcom/google/android/videos/adapter/MoviesDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/ui/DownloadItemView;->posterRequestId:Ljava/lang/String;

    .line 155
    return-void
.end method

.method private bindVideo(Lcom/google/android/videos/adapter/VideosDataSource;Landroid/database/Cursor;Lcom/google/android/videos/async/Requester;Ljava/lang/String;)V
    .locals 31
    .param p1, "dataSource"    # Lcom/google/android/videos/adapter/VideosDataSource;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p4, "title"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/adapter/VideosDataSource;",
            "Landroid/database/Cursor;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 81
    .local p3, "posterRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/ui/DownloadItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    .line 85
    .local v27, "resources":Landroid/content/res/Resources;
    invoke-interface/range {p1 .. p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getPinningStatus(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v6

    .line 86
    .local v6, "pinningStatus":Ljava/lang/Integer;
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    const/16 v23, 0x1

    .line 88
    .local v23, "isDownloaded":Z
    :goto_0
    invoke-interface/range {p1 .. p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getHaveLicense(Landroid/database/Cursor;)Z

    move-result v22

    .line 90
    .local v22, "haveLicense":Z
    invoke-interface/range {p1 .. p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getDownloadSize(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v15

    .line 91
    .local v15, "downloadSize":Ljava/lang/Long;
    invoke-interface/range {p1 .. p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getBytesDownloaded(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v13

    .line 93
    .local v13, "bytesDownloaded":Ljava/lang/Long;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/ui/DownloadItemView;->getContext()Landroid/content/Context;

    move-result-object v14

    .line 95
    .local v14, "context":Landroid/content/Context;
    if-eqz v23, :cond_1

    .line 96
    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v14, v2, v3}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v21

    .line 97
    .local v21, "formattedTotal":Ljava/lang/String;
    const v2, 0x7f0b025a

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v21, v3, v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    .line 108
    .end local v21    # "formattedTotal":Ljava/lang/String;
    .local v16, "downloadStatus":Ljava/lang/String;
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/ui/DownloadItemView;->downloadProgressView:Landroid/widget/TextView;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/ui/DownloadItemView;->titleView:Landroid/widget/TextView;

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    invoke-interface/range {p1 .. p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getResumeTimestamp(Landroid/database/Cursor;)I

    move-result v28

    .line 113
    .local v28, "resumeTimestamp":I
    if-eqz v28, :cond_5

    .line 114
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/ui/DownloadItemView;->watchProgressView:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 116
    invoke-interface/range {p1 .. p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getDurationSeconds(Landroid/database/Cursor;)I

    move-result v2

    mul-int/lit16 v0, v2, 0x3e8

    move/from16 v17, v0

    .line 117
    .local v17, "duration":I
    move/from16 v0, v17

    int-to-long v2, v0

    const-wide/32 v4, 0x36ee80

    cmp-long v2, v2, v4

    if-lez v2, :cond_4

    const/16 v29, 0x1

    .line 119
    .local v29, "showHours":Z
    :goto_2
    invoke-static/range {v28 .. v29}, Lcom/google/android/videos/utils/TimeUtil;->getDurationString(IZ)Ljava/lang/String;

    move-result-object v20

    .line 120
    .local v20, "formattedResume":Ljava/lang/String;
    move/from16 v0, v17

    move/from16 v1, v29

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/TimeUtil;->getDurationString(IZ)Ljava/lang/String;

    move-result-object v19

    .line 122
    .local v19, "formattedDuration":Ljava/lang/String;
    new-instance v26, Landroid/text/SpannableString;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v26

    invoke-direct {v0, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 125
    .local v26, "progressString":Landroid/text/SpannableString;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/ui/DownloadItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0079

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v25

    .line 126
    .local v25, "progressColor":I
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    move/from16 v0, v25

    invoke-direct {v2, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v3, 0x0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0x21

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 129
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/ui/DownloadItemView;->watchProgressView:Landroid/widget/TextView;

    sget-object v3, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    move-object/from16 v0, v26

    invoke-virtual {v2, v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 134
    .end local v17    # "duration":I
    .end local v19    # "formattedDuration":Ljava/lang/String;
    .end local v20    # "formattedResume":Ljava/lang/String;
    .end local v25    # "progressColor":I
    .end local v26    # "progressString":Landroid/text/SpannableString;
    .end local v29    # "showHours":Z
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/ui/DownloadItemView;->downloadView:Lcom/google/android/videos/ui/DownloadView;

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Lcom/google/android/videos/ui/DownloadView;->setTitle(Ljava/lang/String;)V

    .line 135
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/ui/DownloadItemView;->downloadView:Lcom/google/android/videos/ui/DownloadView;

    invoke-interface/range {p1 .. p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getAccount(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    invoke-interface/range {p1 .. p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    invoke-interface/range {p1 .. p2}, Lcom/google/android/videos/adapter/VideosDataSource;->isPinned(Landroid/database/Cursor;)Z

    move-result v5

    move/from16 v0, v22

    invoke-static {v0, v15, v13}, Lcom/google/android/videos/pinning/PinningStatusHelper;->getProgressFraction(ZLjava/lang/Long;Ljava/lang/Long;)F

    move-result v7

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/videos/ui/DownloadView;->update(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Integer;F)V

    .line 139
    const v8, 0x7f0f00c9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/videos/ui/DownloadItemView;->thumbnailBitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

    const/4 v11, 0x0

    const-class v12, Ljava/lang/String;

    move-object/from16 v7, p0

    move-object/from16 v10, p3

    invoke-virtual/range {v7 .. v12}, Lcom/google/android/videos/ui/DownloadItemView;->registerBitmapView(ILcom/google/android/videos/ui/BitmapLoader$BitmapView;Lcom/google/android/videos/async/Requester;Landroid/graphics/Bitmap;Ljava/lang/Class;)V

    .line 142
    invoke-interface/range {p1 .. p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v30

    .line 144
    .local v30, "videoId":Ljava/lang/String;
    const v2, 0x7f0b0075

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-static {v0, v2, v1}, Lcom/google/android/videos/ui/TransitionUtil;->encodeAndSetTransitionName(Landroid/view/View;ILjava/lang/String;)V

    .line 145
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/ui/DownloadItemView;->thumbnailView:Landroid/widget/ImageView;

    const v3, 0x7f0b0077

    move-object/from16 v0, v30

    invoke-static {v2, v3, v0}, Lcom/google/android/videos/ui/TransitionUtil;->encodeAndSetTransitionName(Landroid/view/View;ILjava/lang/String;)V

    .line 146
    return-void

    .line 86
    .end local v13    # "bytesDownloaded":Ljava/lang/Long;
    .end local v14    # "context":Landroid/content/Context;
    .end local v15    # "downloadSize":Ljava/lang/Long;
    .end local v16    # "downloadStatus":Ljava/lang/String;
    .end local v22    # "haveLicense":Z
    .end local v23    # "isDownloaded":Z
    .end local v28    # "resumeTimestamp":I
    .end local v30    # "videoId":Ljava/lang/String;
    :cond_0
    const/16 v23, 0x0

    goto/16 :goto_0

    .line 98
    .restart local v13    # "bytesDownloaded":Ljava/lang/Long;
    .restart local v14    # "context":Landroid/content/Context;
    .restart local v15    # "downloadSize":Ljava/lang/Long;
    .restart local v22    # "haveLicense":Z
    .restart local v23    # "isDownloaded":Z
    :cond_1
    if-eqz v13, :cond_2

    if-nez v15, :cond_3

    .line 99
    :cond_2
    const v2, 0x7f0b0259

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    .restart local v16    # "downloadStatus":Ljava/lang/String;
    goto/16 :goto_1

    .line 101
    .end local v16    # "downloadStatus":Ljava/lang/String;
    :cond_3
    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v14, v2, v3}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v21

    .line 102
    .restart local v21    # "formattedTotal":Ljava/lang/String;
    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v14, v2, v3}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v18

    .line 103
    .local v18, "formattedCurrent":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/ui/DownloadItemView;->percentFormat:Ljava/text/NumberFormat;

    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-double v4, v4

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    long-to-double v8, v8

    div-double/2addr v4, v8

    invoke-virtual {v2, v4, v5}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v24

    .line 105
    .local v24, "percent":Ljava/lang/String;
    const v2, 0x7f0b0257

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v24, v3, v4

    const/4 v4, 0x1

    aput-object v18, v3, v4

    const/4 v4, 0x2

    aput-object v21, v3, v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    .restart local v16    # "downloadStatus":Ljava/lang/String;
    goto/16 :goto_1

    .line 117
    .end local v18    # "formattedCurrent":Ljava/lang/String;
    .end local v21    # "formattedTotal":Ljava/lang/String;
    .end local v24    # "percent":Ljava/lang/String;
    .restart local v17    # "duration":I
    .restart local v28    # "resumeTimestamp":I
    :cond_4
    const/16 v29, 0x0

    goto/16 :goto_2

    .line 131
    .end local v17    # "duration":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/ui/DownloadItemView;->watchProgressView:Landroid/widget/TextView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3
.end method

.method public static newEpisodesBinder(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/logging/UiElementNode;)Lcom/google/android/videos/ui/DownloadItemView$Binder;
    .locals 1
    .param p1, "clusterUiElementNode"    # Lcom/google/android/videos/logging/UiElementNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/google/android/videos/logging/UiElementNode;",
            ")",
            "Lcom/google/android/videos/ui/DownloadItemView$Binder",
            "<",
            "Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 221
    .local p0, "posterRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    new-instance v0, Lcom/google/android/videos/ui/DownloadItemView$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/videos/ui/DownloadItemView$2;-><init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/logging/UiElementNode;)V

    return-object v0
.end method

.method public static newMoviesBinder(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/logging/UiElementNode;)Lcom/google/android/videos/ui/DownloadItemView$Binder;
    .locals 1
    .param p1, "clusterUiElementNode"    # Lcom/google/android/videos/logging/UiElementNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/google/android/videos/logging/UiElementNode;",
            ")",
            "Lcom/google/android/videos/ui/DownloadItemView$Binder",
            "<",
            "Lcom/google/android/videos/adapter/MoviesDataSource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 210
    .local p0, "posterRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    new-instance v0, Lcom/google/android/videos/ui/DownloadItemView$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/videos/ui/DownloadItemView$1;-><init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/logging/UiElementNode;)V

    return-object v0
.end method


# virtual methods
.method protected collectClickableViews([Landroid/view/View;)V
    .locals 2
    .param p1, "clickableViews"    # [Landroid/view/View;

    .prologue
    .line 192
    const/4 v0, 0x0

    aput-object p0, p1, v0

    .line 193
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/videos/ui/DownloadItemView;->downloadView:Lcom/google/android/videos/ui/DownloadView;

    aput-object v1, p1, v0

    .line 194
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 62
    invoke-super {p0}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->onFinishInflate()V

    .line 64
    const v0, 0x7f0f00d1

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/DownloadItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/DownloadView;

    iput-object v0, p0, Lcom/google/android/videos/ui/DownloadItemView;->downloadView:Lcom/google/android/videos/ui/DownloadView;

    .line 65
    iget-object v0, p0, Lcom/google/android/videos/ui/DownloadItemView;->thumbnailView:Landroid/widget/ImageView;

    const v1, 0x7f0a00d7

    invoke-static {v0, v1}, Lcom/google/android/videos/ui/BitmapLoader;->createDefaultBitmapView(Landroid/widget/ImageView;I)Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/DownloadItemView;->thumbnailBitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

    .line 67
    const v0, 0x7f0f00a8

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/DownloadItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/DownloadItemView;->titleView:Landroid/widget/TextView;

    .line 68
    const v0, 0x7f0f00ce

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/DownloadItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/DownloadItemView;->watchProgressView:Landroid/widget/TextView;

    .line 69
    const v0, 0x7f0f00fb

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/DownloadItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/DownloadItemView;->episodeInfoView:Landroid/widget/TextView;

    .line 70
    const v0, 0x7f0f00fc

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/DownloadItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/DownloadItemView;->downloadProgressView:Landroid/widget/TextView;

    .line 72
    invoke-static {}, Ljava/text/NumberFormat;->getPercentInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/DownloadItemView;->percentFormat:Ljava/text/NumberFormat;

    .line 73
    iget-object v0, p0, Lcom/google/android/videos/ui/DownloadItemView;->percentFormat:Ljava/text/NumberFormat;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 75
    iget-object v0, p0, Lcom/google/android/videos/ui/DownloadItemView;->thumbnailView:Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/google/android/videos/utils/ViewUtil;->removeOutlineProvider(Landroid/view/View;)V

    .line 76
    iget-object v0, p0, Lcom/google/android/videos/ui/DownloadItemView;->downloadView:Lcom/google/android/videos/ui/DownloadView;

    invoke-static {v0}, Lcom/google/android/videos/utils/ViewUtil;->removeOutlineProvider(Landroid/view/View;)V

    .line 77
    return-void
.end method

.method protected onGenerateBitmapViewRequest(ILjava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .param p1, "id"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Q:",
            "Ljava/lang/Object;",
            ">(I",
            "Ljava/lang/Class",
            "<TQ;>;)TQ;"
        }
    .end annotation

    .prologue
    .line 184
    .local p2, "requestType":Ljava/lang/Class;, "Ljava/lang/Class<TQ;>;"
    const v0, 0x7f0f00c9

    if-ne p1, v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/google/android/videos/ui/DownloadItemView;->posterRequestId:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 187
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->onGenerateBitmapViewRequest(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method
