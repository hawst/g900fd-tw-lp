.class public Lcom/google/android/videos/pinning/DownloadNotificationManager;
.super Ljava/lang/Object;
.source "DownloadNotificationManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoInfoQuery;,
        Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;,
        Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;,
        Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;,
        Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;,
        Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;,
        Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;,
        Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadSingle;,
        Lcom/google/android/videos/pinning/DownloadNotificationManager$OngoingNotificationHandler;
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final database:Lcom/google/android/videos/store/Database;

.field private final localExecutor:Ljava/util/concurrent/Executor;

.field private final mainHandler:Landroid/os/Handler;

.field private notificationHandler:Lcom/google/android/videos/pinning/DownloadNotificationManager$OngoingNotificationHandler;

.field private final notificationManager:Landroid/app/NotificationManager;

.field private final notificationUtil:Lcom/google/android/videos/NotificationUtil;

.field private pendingNotificationTags:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final posterStore:Lcom/google/android/videos/store/PosterStore;

.field private final videoDataMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Ljava/util/concurrent/Executor;Lcom/google/android/videos/NotificationUtil;Lcom/google/android/videos/store/PosterStore;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mainHandler"    # Landroid/os/Handler;
    .param p3, "database"    # Lcom/google/android/videos/store/Database;
    .param p4, "localExecutor"    # Ljava/util/concurrent/Executor;
    .param p5, "videosPlatformUtil"    # Lcom/google/android/videos/NotificationUtil;
    .param p6, "posterStore"    # Lcom/google/android/videos/store/PosterStore;

    .prologue
    .line 232
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 233
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->context:Landroid/content/Context;

    .line 234
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->mainHandler:Landroid/os/Handler;

    .line 235
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/Database;

    iput-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->database:Lcom/google/android/videos/store/Database;

    .line 236
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->localExecutor:Ljava/util/concurrent/Executor;

    .line 237
    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/NotificationUtil;

    iput-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->notificationUtil:Lcom/google/android/videos/NotificationUtil;

    .line 238
    invoke-static {p6}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/PosterStore;

    iput-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->posterStore:Lcom/google/android/videos/store/PosterStore;

    .line 239
    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->notificationManager:Landroid/app/NotificationManager;

    .line 241
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->videoDataMap:Ljava/util/Map;

    .line 242
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->pendingNotificationTags:Ljava/util/Set;

    .line 243
    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/videos/pinning/DownloadNotificationManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/pinning/DownloadNotificationManager;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->refreshVideoData()V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/videos/pinning/DownloadNotificationManager;)Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pinning/DownloadNotificationManager;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->computeNotificationData()Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/videos/pinning/DownloadNotificationManager;Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/pinning/DownloadNotificationManager;
    .param p1, "x1"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->updateAllNotification(Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/videos/pinning/DownloadNotificationManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pinning/DownloadNotificationManager;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->mainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/videos/pinning/DownloadNotificationManager;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pinning/DownloadNotificationManager;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->computeVideoData(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/videos/pinning/DownloadNotificationManager;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pinning/DownloadNotificationManager;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->videoDataMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/videos/pinning/DownloadNotificationManager;Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/pinning/DownloadNotificationManager;
    .param p1, "x1"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;
    .param p2, "x2"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
    .param p3, "x3"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->updateNotifications(Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)V

    return-void
.end method

.method static synthetic access$2500(Lcom/google/android/videos/pinning/DownloadNotificationManager;)Lcom/google/android/videos/store/Database;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pinning/DownloadNotificationManager;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->database:Lcom/google/android/videos/store/Database;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/google/android/videos/pinning/DownloadNotificationManager;Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/pinning/DownloadNotificationManager;
    .param p1, "x1"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->cancelNotification(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)V

    return-void
.end method

.method private cancelNotification(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)V
    .locals 4
    .param p1, "videoData"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;

    .prologue
    .line 358
    if-nez p1, :cond_0

    .line 380
    :goto_0
    return-void

    .line 361
    :cond_0
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->pinningStatus:I
    invoke-static {p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$2400(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 373
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->notificationManager:Landroid/app/NotificationManager;

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->account:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$000(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v1

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->videoId:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$200(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v2

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->showId:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$300(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->createPendingTag(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0f0038

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    goto :goto_0

    .line 363
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->notificationManager:Landroid/app/NotificationManager;

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->account:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$000(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v1

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->videoId:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$200(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v2

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->showId:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$300(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->createCompletedTag(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0f0039

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    goto :goto_0

    .line 368
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->notificationManager:Landroid/app/NotificationManager;

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->account:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$000(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v1

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->videoId:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$200(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->createErrorTag(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0f003a

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    goto :goto_0

    .line 361
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private computeNotificationData()Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;
    .locals 22

    .prologue
    .line 619
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 620
    .local v10, "downloadsCompleted":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;>;"
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    .line 621
    .local v11, "downloadsPending":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 622
    .local v6, "downloadsError":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;>;"
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 623
    .local v13, "ongoingVideoData":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->videoDataMap:Ljava/util/Map;

    monitor-enter v4

    .line 624
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->videoDataMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;

    .line 625
    .local v16, "videoData":Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->showId:Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$300(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->videoId:Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$200(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromMovieId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 628
    .local v8, "assetId":Ljava/lang/String;
    :goto_1
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->pinningStatus:I
    invoke-static/range {v16 .. v16}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$2400(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 643
    :pswitch_0
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->downloadSize:J
    invoke-static/range {v16 .. v16}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$900(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)J

    move-result-wide v18

    const-wide/16 v20, -0x1

    cmp-long v2, v18, v20

    if-eqz v2, :cond_1

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->bytesDownloaded:J
    invoke-static/range {v16 .. v16}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$1200(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)J

    move-result-wide v18

    const-wide/16 v20, 0x0

    cmp-long v2, v18, v20

    if-nez v2, :cond_2

    .line 647
    :cond_1
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->pinningStatusReason:I
    invoke-static/range {v16 .. v16}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$800(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)I

    move-result v15

    .line 648
    .local v15, "pendingReason":I
    const/4 v2, 0x2

    if-eq v15, v2, :cond_0

    const/16 v2, 0x8

    if-eq v15, v2, :cond_0

    .line 655
    .end local v15    # "pendingReason":I
    :cond_2
    invoke-interface {v11, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;

    .line 656
    .local v14, "pending":Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;
    if-nez v14, :cond_5

    .line 657
    new-instance v14, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;

    .end local v14    # "pending":Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;
    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-direct {v14, v0, v2}, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;-><init>(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;Lcom/google/android/videos/pinning/DownloadNotificationManager$1;)V

    .line 658
    .restart local v14    # "pending":Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;
    invoke-interface {v11, v8, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 670
    .end local v8    # "assetId":Ljava/lang/String;
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v14    # "pending":Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;
    .end local v16    # "videoData":Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
    :catchall_0
    move-exception v2

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 625
    .restart local v12    # "i$":Ljava/util/Iterator;
    .restart local v16    # "videoData":Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
    :cond_3
    :try_start_1
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->showId:Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$300(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromShowId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    goto :goto_1

    .line 630
    .restart local v8    # "assetId":Ljava/lang/String;
    :pswitch_1
    move-object/from16 v0, v16

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 634
    :pswitch_2
    invoke-interface {v10, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;

    .line 635
    .local v9, "completed":Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;
    if-nez v9, :cond_4

    .line 636
    new-instance v9, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;

    .end local v9    # "completed":Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;
    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-direct {v9, v0, v2}, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;-><init>(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;Lcom/google/android/videos/pinning/DownloadNotificationManager$1;)V

    .line 637
    .restart local v9    # "completed":Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;
    invoke-interface {v10, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 639
    :cond_4
    iget-object v2, v9, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;->videoIds:Ljava/util/List;

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->videoId:Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$200(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 660
    .end local v9    # "completed":Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;
    .restart local v14    # "pending":Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;
    :cond_5
    iget-object v2, v14, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;->videoIds:Ljava/util/List;

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->videoId:Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$200(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 664
    .end local v14    # "pending":Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;
    :pswitch_3
    new-instance v2, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;

    const/4 v5, 0x0

    move-object/from16 v0, v16

    invoke-direct {v2, v0, v5}, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;-><init>(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;Lcom/google/android/videos/pinning/DownloadNotificationManager$1;)V

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 670
    .end local v8    # "assetId":Ljava/lang/String;
    .end local v16    # "videoData":Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
    :cond_6
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 671
    invoke-interface {v13}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    const/4 v3, 0x0

    .line 673
    .local v3, "downloadsOngoing":Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;
    :goto_2
    new-instance v2, Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;

    invoke-interface {v11}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v10}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;-><init>(Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/List;Lcom/google/android/videos/pinning/DownloadNotificationManager$1;)V

    return-object v2

    .line 671
    .end local v3    # "downloadsOngoing":Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;
    :cond_7
    # invokes: Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->create(Ljava/util/List;)Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;
    invoke-static {v13}, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->access$3000(Ljava/util/List;)Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;

    move-result-object v3

    goto :goto_2

    .line 628
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private computeVideoData(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
    .locals 10
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 585
    iget-object v1, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v1}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 586
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "purchased_assets, videos ON asset_type IN (6,20) AND asset_id = video_id LEFT JOIN (seasons, shows ON show_id = shows_id) ON episode_season_id IS season_id"

    sget-object v2, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoInfoQuery;->PROJECTION:[Ljava/lang/String;

    const-string v3, "pinning_notification_active AND pinning_status IS NOT NULL AND pinning_status != 5 AND account = ? AND asset_id = ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v4, v6

    const/4 v6, 0x1

    aput-object p2, v4, v6

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 590
    .local v9, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 591
    invoke-direct {p0, v9}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->getVideoInfo(Landroid/database/Cursor;)Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 596
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v5

    :cond_0
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private createCompletedTag(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "showId"    # Ljava/lang/String;

    .prologue
    .line 475
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v1, p2

    .line 476
    .local v1, "id":Ljava/lang/String;
    :goto_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v2, "video_"

    .line 478
    .local v2, "prefix":Ljava/lang/String;
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 479
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v3, "completed_"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 480
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 481
    const-string v3, "_"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 482
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 483
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 484
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v1    # "id":Ljava/lang/String;
    .end local v2    # "prefix":Ljava/lang/String;
    :cond_0
    move-object v1, p3

    .line 475
    goto :goto_0

    .line 476
    .restart local v1    # "id":Ljava/lang/String;
    :cond_1
    const-string v2, "show_"

    goto :goto_1
.end method

.method private createErrorTag(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;

    .prologue
    .line 492
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 493
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "error_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 494
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 495
    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 496
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 497
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private createPendingTag(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "showId"    # Ljava/lang/String;

    .prologue
    .line 488
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->createCompletedTag(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private varargs dismissNotificationFor(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoIds"    # [Ljava/lang/String;

    .prologue
    .line 509
    if-eqz p2, :cond_0

    array-length v0, p2

    if-nez v0, :cond_1

    .line 557
    :cond_0
    :goto_0
    return-void

    .line 512
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->localExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/videos/pinning/DownloadNotificationManager$3;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$3;-><init>(Lcom/google/android/videos/pinning/DownloadNotificationManager;[Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private getPoster(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 10
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 564
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    move v3, v7

    .line 565
    .local v3, "isEpisode":Z
    :goto_0
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v6

    .line 566
    .local v6, "posterCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    const/4 v5, 0x0

    .line 567
    .local v5, "poster":Landroid/graphics/Bitmap;
    iget-object v9, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->posterStore:Lcom/google/android/videos/store/PosterStore;

    if-eqz v3, :cond_0

    move v8, v7

    :cond_0
    if-eqz v3, :cond_2

    move-object v7, p2

    :goto_1
    invoke-virtual {v9, v8, v7, v6}, Lcom/google/android/videos/store/PosterStore;->getImage(ILjava/lang/String;Lcom/google/android/videos/async/Callback;)V

    .line 572
    const-wide/16 v8, 0x3e8

    :try_start_0
    invoke-virtual {v6, v8, v9}, Lcom/google/android/videos/async/SyncCallback;->getResponse(J)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v5, v0
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_1

    .line 581
    :goto_2
    iget-object v7, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->context:Landroid/content/Context;

    invoke-static {v7, v5}, Lcom/google/android/videos/NotificationUtil;->scaleBitmapForNotification(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v7

    return-object v7

    .end local v3    # "isEpisode":Z
    .end local v5    # "poster":Landroid/graphics/Bitmap;
    .end local v6    # "posterCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    :cond_1
    move v3, v8

    .line 564
    goto :goto_0

    .restart local v3    # "isEpisode":Z
    .restart local v5    # "poster":Landroid/graphics/Bitmap;
    .restart local v6    # "posterCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    :cond_2
    move-object v7, p1

    .line 567
    goto :goto_1

    .line 573
    :catch_0
    move-exception v2

    .line 574
    .local v2, "e":Ljava/util/concurrent/ExecutionException;
    invoke-virtual {v2}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v7

    if-eqz v7, :cond_3

    invoke-virtual {v2}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    .line 575
    .local v4, "message":Ljava/lang/String;
    :goto_3
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Could not get poster: videoId="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", showId="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ": "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    goto :goto_2

    .line 574
    .end local v4    # "message":Ljava/lang/String;
    :cond_3
    invoke-virtual {v2}, Ljava/util/concurrent/ExecutionException;->getMessage()Ljava/lang/String;

    move-result-object v4

    goto :goto_3

    .line 576
    .end local v2    # "e":Ljava/util/concurrent/ExecutionException;
    :catch_1
    move-exception v2

    .line 577
    .local v2, "e":Ljava/util/concurrent/TimeoutException;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Timeout when getting poster: videoId="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", showId="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private getVideoInfo(Landroid/database/Cursor;)Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
    .locals 23
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 678
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 679
    .local v5, "account":Ljava/lang/String;
    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 680
    .local v6, "videoId":Ljava/lang/String;
    const/4 v4, 0x4

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 681
    .local v8, "showId":Ljava/lang/String;
    const/4 v11, 0x0

    .line 683
    .local v11, "poster":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->videoDataMap:Ljava/util/Map;

    monitor-enter v7

    .line 684
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->videoDataMap:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v21

    .local v21, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;

    .line 685
    .local v22, "videoData":Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->videoId:Ljava/lang/String;
    invoke-static/range {v22 .. v22}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$200(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    if-eqz v8, :cond_0

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->showId:Ljava/lang/String;
    invoke-static/range {v22 .. v22}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$300(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v8, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 687
    :cond_1
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->poster:Landroid/graphics/Bitmap;
    invoke-static/range {v22 .. v22}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$600(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 691
    .end local v22    # "videoData":Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
    :cond_2
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 692
    if-eqz v11, :cond_3

    .line 694
    :goto_0
    new-instance v4, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;

    const/4 v7, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x5

    move-object/from16 v0, p1

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v12, 0x7

    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    const/4 v14, 0x6

    const-wide/16 v16, -0x1

    move-object/from16 v0, p1

    move-wide/from16 v1, v16

    invoke-static {v0, v14, v1, v2}, Lcom/google/android/videos/utils/DbUtils;->getLongOrDefault(Landroid/database/Cursor;IJ)J

    move-result-wide v14

    const/16 v16, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    const/16 v17, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    const/16 v18, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/DbUtils;->getIntOrNull(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v18

    const/16 v19, 0xb

    const/16 v20, -0x1

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/google/android/videos/utils/DbUtils;->getIntOrDefault(Landroid/database/Cursor;II)I

    move-result v19

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_4

    const/16 v19, 0x1

    :goto_1
    const/16 v20, 0x0

    invoke-direct/range {v4 .. v20}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;JJIILjava/lang/Integer;ZLcom/google/android/videos/pinning/DownloadNotificationManager$1;)V

    return-object v4

    .line 691
    .end local v21    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 692
    .restart local v21    # "i$":Ljava/util/Iterator;
    :cond_3
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v8}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->getPoster(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v11

    goto :goto_0

    .line 694
    :cond_4
    const/16 v19, 0x0

    goto :goto_1
.end method

.method private refreshVideoData()V
    .locals 11

    .prologue
    const/4 v4, 0x0

    .line 601
    iget-object v1, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v1}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 602
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "purchased_assets, videos ON asset_type IN (6,20) AND asset_id = video_id LEFT JOIN (seasons, shows ON show_id = shows_id) ON episode_season_id IS season_id"

    sget-object v2, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoInfoQuery;->PROJECTION:[Ljava/lang/String;

    const-string v3, "pinning_notification_active AND pinning_status IS NOT NULL AND pinning_status != 5"

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    move-object v8, v4

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 606
    .local v9, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v2, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->videoDataMap:Ljava/util/Map;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 607
    :try_start_1
    iget-object v1, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->videoDataMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 608
    :goto_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 609
    invoke-direct {p0, v9}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->getVideoInfo(Landroid/database/Cursor;)Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;

    move-result-object v10

    .line 610
    .local v10, "videoData":Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
    iget-object v1, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->videoDataMap:Ljava/util/Map;

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->account:Ljava/lang/String;
    invoke-static {v10}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$000(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v3

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->videoId:Ljava/lang/String;
    invoke-static {v10}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$200(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    invoke-interface {v1, v3, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 612
    .end local v10    # "videoData":Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 614
    :catchall_1
    move-exception v1

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v1

    .line 612
    :cond_0
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 614
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 616
    return-void
.end method

.method private updateAllNotification(Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;)V
    .locals 1
    .param p1, "notificationData"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;

    .prologue
    .line 312
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;->downloadsOngoing:Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;
    invoke-static {p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;->access$2000(Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;)Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->updateOngoingNotification(Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;)V

    .line 313
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;->downloadsPending:Ljava/util/Collection;
    invoke-static {p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;->access$2100(Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;)Ljava/util/Collection;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->updatePendingNotifications(Ljava/util/Collection;)V

    .line 314
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;->downloadsCompleted:Ljava/util/Collection;
    invoke-static {p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;->access$2200(Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;)Ljava/util/Collection;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->updateCompletedNotifications(Ljava/util/Collection;)V

    .line 315
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;->downloadsError:Ljava/util/Collection;
    invoke-static {p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;->access$2300(Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;)Ljava/util/Collection;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->updateErrorNotifications(Ljava/util/Collection;)V

    .line 316
    return-void
.end method

.method private updateCompletedNotificationFor(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;)V
    .locals 9
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "showId"    # Ljava/lang/String;
    .param p4, "completed"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;

    .prologue
    .line 449
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->createCompletedTag(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 450
    .local v0, "tag":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->notificationManager:Landroid/app/NotificationManager;

    const v2, 0x7f0f0039

    iget-object v3, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->notificationUtil:Lcom/google/android/videos/NotificationUtil;

    iget-object v4, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->context:Landroid/content/Context;

    iget-object v5, p4, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;->videoIds:Ljava/util/List;

    iget-object v6, p4, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;->seasonId:Ljava/lang/String;

    iget-object v7, p4, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;->showId:Ljava/lang/String;

    invoke-static {v4, p1, v5, v6, v7}, Lcom/google/android/videos/pinning/NotificationsCallbackService;->createCompletedPendingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->context:Landroid/content/Context;

    iget-object v6, p4, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;->videoIds:Ljava/util/List;

    iget-object v7, p4, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;->seasonId:Ljava/lang/String;

    iget-object v8, p4, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;->showId:Ljava/lang/String;

    invoke-static {v5, p1, v6, v7, v8}, Lcom/google/android/videos/pinning/NotificationsCallbackService;->createCompletedDeletedPendingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual {v3, p4, v4, v5}, Lcom/google/android/videos/NotificationUtil;->createDownloadCompletedNotification(Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 456
    return-void
.end method

.method private updateCompletedNotifications(Ljava/util/Collection;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 401
    .local p1, "completed":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;

    .line 402
    .local v0, "complete":Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;
    iget-object v3, v0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;->account:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;->videoIds:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;->showId:Ljava/lang/String;

    invoke-direct {p0, v3, v2, v4, v0}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->updateCompletedNotificationFor(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;)V

    goto :goto_0

    .line 405
    .end local v0    # "complete":Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;
    :cond_0
    return-void
.end method

.method private updateErrorNotificationFor(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;)V
    .locals 10
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "error"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;

    .prologue
    .line 465
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->createErrorTag(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 466
    .local v0, "tag":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->notificationManager:Landroid/app/NotificationManager;

    const v2, 0x7f0f003a

    iget-object v3, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->notificationUtil:Lcom/google/android/videos/NotificationUtil;

    iget-object v4, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->context:Landroid/content/Context;

    iget-object v5, p3, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;->account:Ljava/lang/String;

    iget-object v6, p3, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;->videoId:Ljava/lang/String;

    iget-object v7, p3, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;->seasonId:Ljava/lang/String;

    iget-object v8, p3, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;->showId:Ljava/lang/String;

    invoke-static {v4, v5, v6, v7, v8}, Lcom/google/android/videos/pinning/NotificationsCallbackService;->createErrorPendingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->context:Landroid/content/Context;

    iget-object v6, p3, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;->account:Ljava/lang/String;

    iget-object v7, p3, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;->videoId:Ljava/lang/String;

    iget-object v8, p3, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;->seasonId:Ljava/lang/String;

    iget-object v9, p3, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;->showId:Ljava/lang/String;

    invoke-static {v5, v6, v7, v8, v9}, Lcom/google/android/videos/pinning/NotificationsCallbackService;->createErrorDeletedPendingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual {v3, p3, v4, v5}, Lcom/google/android/videos/NotificationUtil;->createDownloadErrorNotification(Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 472
    return-void
.end method

.method private updateErrorNotifications(Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 459
    .local p1, "errors":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;

    .line 460
    .local v0, "error":Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;
    iget-object v2, v0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;->account:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;->videoId:Ljava/lang/String;

    invoke-direct {p0, v2, v3, v0}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->updateErrorNotificationFor(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;)V

    goto :goto_0

    .line 462
    .end local v0    # "error":Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;
    :cond_0
    return-void
.end method

.method private updateNotifications(Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)V
    .locals 6
    .param p1, "notificationData"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;
    .param p2, "newVideoData"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
    .param p3, "oldVideoData"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;

    .prologue
    const/4 v2, 0x2

    const/4 v1, -0x1

    .line 320
    if-eqz p2, :cond_3

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->pinningStatus:I
    invoke-static {p2}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$2400(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)I

    move-result v0

    .line 321
    .local v0, "newPinningStatus":I
    :goto_0
    if-eqz p3, :cond_0

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->pinningStatus:I
    invoke-static {p3}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$2400(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)I

    move-result v1

    .line 322
    .local v1, "oldPinningStatus":I
    :cond_0
    if-nez p2, :cond_1

    .line 324
    invoke-direct {p0, p3}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->cancelNotification(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)V

    .line 328
    :cond_1
    if-ne v0, v2, :cond_4

    if-ne v1, v2, :cond_4

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->bytesDownloaded:J
    invoke-static {p2}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$1200(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)J

    move-result-wide v2

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->bytesDownloaded:J
    invoke-static {p3}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$1200(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->downloadSize:J
    invoke-static {p2}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$900(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)J

    move-result-wide v2

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->bytesDownloaded:J
    invoke-static {p3}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$1200(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    .line 338
    :cond_2
    :goto_1
    return-void

    .end local v0    # "newPinningStatus":I
    .end local v1    # "oldPinningStatus":I
    :cond_3
    move v0, v1

    .line 320
    goto :goto_0

    .line 334
    .restart local v0    # "newPinningStatus":I
    .restart local v1    # "oldPinningStatus":I
    :cond_4
    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->updateNotificationsForStatus(Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;I)V

    .line 335
    if-eq v0, v1, :cond_2

    .line 336
    invoke-direct {p0, p1, v1}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->updateNotificationsForStatus(Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;I)V

    goto :goto_1
.end method

.method private updateNotificationsForStatus(Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;I)V
    .locals 1
    .param p1, "notificationData"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;
    .param p2, "pinningStatus"    # I

    .prologue
    .line 341
    packed-switch p2, :pswitch_data_0

    .line 355
    :goto_0
    return-void

    .line 343
    :pswitch_0
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;->downloadsOngoing:Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;
    invoke-static {p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;->access$2000(Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;)Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->updateOngoingNotification(Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;)V

    goto :goto_0

    .line 346
    :pswitch_1
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;->downloadsPending:Ljava/util/Collection;
    invoke-static {p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;->access$2100(Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;)Ljava/util/Collection;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->updatePendingNotifications(Ljava/util/Collection;)V

    goto :goto_0

    .line 349
    :pswitch_2
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;->downloadsCompleted:Ljava/util/Collection;
    invoke-static {p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;->access$2200(Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;)Ljava/util/Collection;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->updateCompletedNotifications(Ljava/util/Collection;)V

    goto :goto_0

    .line 352
    :pswitch_3
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;->downloadsError:Ljava/util/Collection;
    invoke-static {p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;->access$2300(Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;)Ljava/util/Collection;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->updateErrorNotifications(Ljava/util/Collection;)V

    goto :goto_0

    .line 341
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private updateOngoingNotification(Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;)V
    .locals 6
    .param p1, "ongoing"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;

    .prologue
    .line 384
    iget-object v1, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->notificationHandler:Lcom/google/android/videos/pinning/DownloadNotificationManager$OngoingNotificationHandler;

    if-eqz v1, :cond_0

    .line 385
    if-eqz p1, :cond_1

    iget v1, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->count:I

    if-lez v1, :cond_1

    .line 386
    iget-object v1, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->context:Landroid/content/Context;

    iget-object v2, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->firstAccount:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->singleVideoId:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->singleSeasonId:Ljava/lang/String;

    iget-object v5, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->singleShowId:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/videos/pinning/NotificationsCallbackService;->createDownloadingOngoingPendingIntentForVideo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 390
    .local v0, "ongoingIntent":Landroid/app/PendingIntent;
    iget-object v1, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->notificationHandler:Lcom/google/android/videos/pinning/DownloadNotificationManager$OngoingNotificationHandler;

    const v2, 0x7f0f0037

    iget-object v3, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->notificationUtil:Lcom/google/android/videos/NotificationUtil;

    invoke-virtual {v3, p1, v0}, Lcom/google/android/videos/NotificationUtil;->createDownloadsOngoingNotification(Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;Landroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/videos/pinning/DownloadNotificationManager$OngoingNotificationHandler;->onOngoingNotification(ILandroid/app/Notification;)V

    .line 398
    .end local v0    # "ongoingIntent":Landroid/app/PendingIntent;
    :cond_0
    :goto_0
    return-void

    .line 393
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->notificationHandler:Lcom/google/android/videos/pinning/DownloadNotificationManager$OngoingNotificationHandler;

    invoke-interface {v1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$OngoingNotificationHandler;->onOngoingNotificationCancelled()V

    goto :goto_0
.end method

.method private updatePendingNotificationFor(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;)Ljava/lang/String;
    .locals 9
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "showId"    # Ljava/lang/String;
    .param p4, "pending"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;

    .prologue
    .line 434
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->createPendingTag(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 435
    .local v0, "tag":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->notificationManager:Landroid/app/NotificationManager;

    const v2, 0x7f0f0038

    iget-object v3, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->notificationUtil:Lcom/google/android/videos/NotificationUtil;

    iget-object v4, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->context:Landroid/content/Context;

    iget-object v5, p4, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;->videoIds:Ljava/util/List;

    iget-object v6, p4, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;->seasonId:Ljava/lang/String;

    iget-object v7, p4, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;->showId:Ljava/lang/String;

    invoke-static {v4, p1, v5, v6, v7}, Lcom/google/android/videos/pinning/NotificationsCallbackService;->createPendingPendingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->context:Landroid/content/Context;

    iget-object v6, p4, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;->videoIds:Ljava/util/List;

    iget-object v7, p4, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;->seasonId:Ljava/lang/String;

    iget-object v8, p4, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;->showId:Ljava/lang/String;

    invoke-static {v5, p1, v6, v7, v8}, Lcom/google/android/videos/pinning/NotificationsCallbackService;->createPendingDeletedPendingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual {v3, p4, v4, v5}, Lcom/google/android/videos/NotificationUtil;->createDownloadPendingNotification(Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 441
    return-object v0
.end method

.method private updatePendingNotifications(Ljava/util/Collection;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 408
    .local p1, "pending":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;>;"
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 409
    .local v2, "newTags":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;

    .line 410
    .local v0, "downloadPending":Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;
    iget v4, v0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;->pinningStatusReason:I

    const/16 v5, 0x20

    if-eq v4, v5, :cond_0

    .line 416
    iget-object v5, v0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;->account:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;->videoIds:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v6, v0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;->showId:Ljava/lang/String;

    invoke-direct {p0, v5, v4, v6, v0}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->updatePendingNotificationFor(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;)Ljava/lang/String;

    move-result-object v3

    .line 418
    .local v3, "tag":Ljava/lang/String;
    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 421
    .end local v0    # "downloadPending":Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;
    .end local v3    # "tag":Ljava/lang/String;
    :cond_1
    iget-object v4, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->pendingNotificationTags:Ljava/util/Set;

    invoke-interface {v4, v2}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 423
    iget-object v4, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->pendingNotificationTags:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 424
    .restart local v3    # "tag":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->notificationManager:Landroid/app/NotificationManager;

    const v5, 0x7f0f0038

    invoke-virtual {v4, v3, v5}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    goto :goto_1

    .line 426
    .end local v3    # "tag":Ljava/lang/String;
    :cond_2
    iput-object v2, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->pendingNotificationTags:Ljava/util/Set;

    .line 427
    return-void
.end method


# virtual methods
.method public varargs dismissCompletedNotification(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoIds"    # [Ljava/lang/String;

    .prologue
    .line 501
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->dismissNotificationFor(Ljava/lang/String;[Ljava/lang/String;)V

    .line 502
    return-void
.end method

.method public dismissErrorNotification(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;

    .prologue
    .line 505
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->dismissNotificationFor(Ljava/lang/String;[Ljava/lang/String;)V

    .line 506
    return-void
.end method

.method public onDownloadsStateChanged()V
    .locals 2

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->localExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/videos/pinning/DownloadNotificationManager$1;

    invoke-direct {v1, p0}, Lcom/google/android/videos/pinning/DownloadNotificationManager$1;-><init>(Lcom/google/android/videos/pinning/DownloadNotificationManager;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 272
    return-void
.end method

.method public onDownloadsStateChanged(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->localExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/videos/pinning/DownloadNotificationManager$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/videos/pinning/DownloadNotificationManager$2;-><init>(Lcom/google/android/videos/pinning/DownloadNotificationManager;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 308
    return-void
.end method

.method public setHandler(Lcom/google/android/videos/pinning/DownloadNotificationManager$OngoingNotificationHandler;)V
    .locals 0
    .param p1, "notificationHandler"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$OngoingNotificationHandler;

    .prologue
    .line 246
    iput-object p1, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager;->notificationHandler:Lcom/google/android/videos/pinning/DownloadNotificationManager$OngoingNotificationHandler;

    .line 247
    if-eqz p1, :cond_0

    .line 249
    invoke-virtual {p0}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->onDownloadsStateChanged()V

    .line 251
    :cond_0
    return-void
.end method
