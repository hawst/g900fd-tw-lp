.class Lcom/google/android/videos/store/PosterStore$ImageRequester;
.super Ljava/lang/Object;
.source "PosterStore.java"

# interfaces
.implements Lcom/google/android/videos/async/Requester;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/PosterStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ImageRequester"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Requester",
        "<",
        "Ljava/lang/String;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field public final cacheKeySuffix:Ljava/lang/String;

.field public final converter:Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;

.field public final fileStore:Lcom/google/android/videos/store/FileStore;

.field final synthetic this$0:Lcom/google/android/videos/store/PosterStore;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/store/PosterStore;Lcom/google/android/videos/store/FileStore;Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;Ljava/lang/String;)V
    .locals 0
    .param p2, "fileStore"    # Lcom/google/android/videos/store/FileStore;
    .param p3, "converter"    # Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;
    .param p4, "cacheKeySuffix"    # Ljava/lang/String;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/google/android/videos/store/PosterStore$ImageRequester;->this$0:Lcom/google/android/videos/store/PosterStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169
    iput-object p2, p0, Lcom/google/android/videos/store/PosterStore$ImageRequester;->fileStore:Lcom/google/android/videos/store/FileStore;

    .line 170
    iput-object p3, p0, Lcom/google/android/videos/store/PosterStore$ImageRequester;->converter:Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;

    .line 171
    iput-object p4, p0, Lcom/google/android/videos/store/PosterStore$ImageRequester;->cacheKeySuffix:Ljava/lang/String;

    .line 172
    return-void
.end method


# virtual methods
.method public bridge synthetic request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/google/android/videos/async/Callback;

    .prologue
    .line 161
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/store/PosterStore$ImageRequester;->request(Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V

    return-void
.end method

.method public request(Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V
    .locals 6
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 181
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    iget-object v0, p0, Lcom/google/android/videos/store/PosterStore$ImageRequester;->this$0:Lcom/google/android/videos/store/PosterStore;

    iget-object v2, p0, Lcom/google/android/videos/store/PosterStore$ImageRequester;->fileStore:Lcom/google/android/videos/store/FileStore;

    iget-object v3, p0, Lcom/google/android/videos/store/PosterStore$ImageRequester;->converter:Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;

    invoke-virtual {p0, p1}, Lcom/google/android/videos/store/PosterStore$ImageRequester;->toBitmapCacheKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v1, p1

    move-object v5, p2

    # invokes: Lcom/google/android/videos/store/PosterStore;->getImage(Ljava/lang/String;Lcom/google/android/videos/store/FileStore;Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/store/PosterStore;->access$300(Lcom/google/android/videos/store/PosterStore;Ljava/lang/String;Lcom/google/android/videos/store/FileStore;Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V

    .line 182
    return-void
.end method

.method public toBitmapCacheKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 175
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/store/PosterStore$ImageRequester;->cacheKeySuffix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
