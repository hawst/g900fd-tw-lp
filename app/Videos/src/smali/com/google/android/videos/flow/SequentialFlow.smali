.class public Lcom/google/android/videos/flow/SequentialFlow;
.super Lcom/google/android/videos/flow/Flow;
.source "SequentialFlow.java"

# interfaces
.implements Lcom/google/android/videos/flow/Flow$FlowChangesListener;


# instance fields
.field private final components:[Lcom/google/android/videos/flow/Flow;

.field private final endPositions:[I


# direct methods
.method public varargs constructor <init>([Lcom/google/android/videos/flow/Flow;)V
    .locals 4
    .param p1, "components"    # [Lcom/google/android/videos/flow/Flow;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/videos/flow/Flow;-><init>()V

    .line 33
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/google/android/videos/flow/Flow;

    iput-object v3, p0, Lcom/google/android/videos/flow/SequentialFlow;->components:[Lcom/google/android/videos/flow/Flow;

    .line 34
    array-length v3, p1

    new-array v3, v3, [I

    iput-object v3, p0, Lcom/google/android/videos/flow/SequentialFlow;->endPositions:[I

    .line 37
    const/4 v2, 0x0

    .line 38
    .local v2, "nextStartPosition":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, p1

    if-ge v1, v3, :cond_0

    .line 39
    aget-object v0, p1, v1

    .line 42
    .local v0, "component":Lcom/google/android/videos/flow/Flow;
    invoke-virtual {v0}, Lcom/google/android/videos/flow/Flow;->getVisibleCount()I

    move-result v3

    add-int/2addr v2, v3

    .line 44
    iget-object v3, p0, Lcom/google/android/videos/flow/SequentialFlow;->endPositions:[I

    aput v2, v3, v1

    .line 45
    invoke-virtual {v0, p0}, Lcom/google/android/videos/flow/Flow;->setFlowChangesListener(Lcom/google/android/videos/flow/Flow$FlowChangesListener;)V

    .line 38
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 47
    .end local v0    # "component":Lcom/google/android/videos/flow/Flow;
    :cond_0
    return-void
.end method

.method private adjustEndPositionsFrom(II)V
    .locals 3
    .param p1, "startComponentIndex"    # I
    .param p2, "delta"    # I

    .prologue
    .line 203
    move v0, p1

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/flow/SequentialFlow;->components:[Lcom/google/android/videos/flow/Flow;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 204
    iget-object v1, p0, Lcom/google/android/videos/flow/SequentialFlow;->endPositions:[I

    aget v2, v1, v0

    add-int/2addr v2, p2

    aput v2, v1, v0

    .line 203
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 206
    :cond_0
    return-void
.end method

.method private componentIndexOf(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 65
    iget-object v1, p0, Lcom/google/android/videos/flow/SequentialFlow;->endPositions:[I

    invoke-static {v1, p1}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v0

    .line 66
    .local v0, "componentIndex":I
    if-ltz v0, :cond_1

    .line 70
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 71
    iget-object v1, p0, Lcom/google/android/videos/flow/SequentialFlow;->endPositions:[I

    aget v1, v1, v0

    if-eq v1, p1, :cond_0

    move v1, v0

    .line 76
    :goto_0
    return v1

    :cond_1
    xor-int/lit8 v1, v0, -0x1

    goto :goto_0
.end method

.method private indexOf(Lcom/google/android/videos/flow/Flow;)I
    .locals 2
    .param p1, "component"    # Lcom/google/android/videos/flow/Flow;

    .prologue
    .line 194
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/flow/SequentialFlow;->components:[Lcom/google/android/videos/flow/Flow;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 195
    iget-object v1, p0, Lcom/google/android/videos/flow/SequentialFlow;->components:[Lcom/google/android/videos/flow/Flow;

    aget-object v1, v1, v0

    if-ne p1, v1, :cond_0

    .line 199
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 194
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 199
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private startPositionOf(I)I
    .locals 2
    .param p1, "componentIndex"    # I

    .prologue
    .line 57
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/flow/SequentialFlow;->endPositions:[I

    add-int/lit8 v1, p1, -0x1

    aget v0, v0, v1

    goto :goto_0
.end method


# virtual methods
.method public bindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 4
    .param p1, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 103
    if-ltz p2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/videos/flow/SequentialFlow;->getCount()I

    move-result v2

    if-ge p2, v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    const-string v3, "position out of bounds"

    invoke-static {v2, v3}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;)V

    .line 105
    invoke-direct {p0, p2}, Lcom/google/android/videos/flow/SequentialFlow;->componentIndexOf(I)I

    move-result v1

    .line 106
    .local v1, "componentIndex":I
    iget-object v2, p0, Lcom/google/android/videos/flow/SequentialFlow;->components:[Lcom/google/android/videos/flow/Flow;

    aget-object v0, v2, v1

    .line 107
    .local v0, "component":Lcom/google/android/videos/flow/Flow;
    invoke-direct {p0, v1}, Lcom/google/android/videos/flow/SequentialFlow;->startPositionOf(I)I

    move-result v2

    sub-int v2, p2, v2

    invoke-virtual {v0, p1, v2}, Lcom/google/android/videos/flow/Flow;->bindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V

    .line 108
    return-void

    .line 103
    .end local v0    # "component":Lcom/google/android/videos/flow/Flow;
    .end local v1    # "componentIndex":I
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/videos/flow/SequentialFlow;->components:[Lcom/google/android/videos/flow/Flow;

    array-length v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/flow/SequentialFlow;->startPositionOf(I)I

    move-result v0

    return v0
.end method

.method public getItemIdentifier(I)Ljava/lang/Object;
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 112
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/videos/flow/SequentialFlow;->getCount()I

    move-result v2

    if-ge p1, v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    const-string v3, "position out of bounds"

    invoke-static {v2, v3}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;)V

    .line 114
    invoke-direct {p0, p1}, Lcom/google/android/videos/flow/SequentialFlow;->componentIndexOf(I)I

    move-result v1

    .line 115
    .local v1, "componentIndex":I
    iget-object v2, p0, Lcom/google/android/videos/flow/SequentialFlow;->components:[Lcom/google/android/videos/flow/Flow;

    aget-object v0, v2, v1

    .line 116
    .local v0, "component":Lcom/google/android/videos/flow/Flow;
    invoke-direct {p0, v1}, Lcom/google/android/videos/flow/SequentialFlow;->startPositionOf(I)I

    move-result v2

    sub-int v2, p1, v2

    invoke-virtual {v0, v2}, Lcom/google/android/videos/flow/Flow;->getItemIdentifier(I)Ljava/lang/Object;

    move-result-object v2

    return-object v2

    .line 112
    .end local v0    # "component":Lcom/google/android/videos/flow/Flow;
    .end local v1    # "componentIndex":I
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getViewType(I)I
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 86
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/videos/flow/SequentialFlow;->getCount()I

    move-result v2

    if-ge p1, v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    const-string v3, "position out of bounds"

    invoke-static {v2, v3}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;)V

    .line 88
    invoke-direct {p0, p1}, Lcom/google/android/videos/flow/SequentialFlow;->componentIndexOf(I)I

    move-result v1

    .line 89
    .local v1, "componentIndex":I
    iget-object v2, p0, Lcom/google/android/videos/flow/SequentialFlow;->components:[Lcom/google/android/videos/flow/Flow;

    aget-object v0, v2, v1

    .line 90
    .local v0, "component":Lcom/google/android/videos/flow/Flow;
    invoke-direct {p0, v1}, Lcom/google/android/videos/flow/SequentialFlow;->startPositionOf(I)I

    move-result v2

    sub-int v2, p1, v2

    invoke-virtual {v0, v2}, Lcom/google/android/videos/flow/Flow;->getViewType(I)I

    move-result v2

    return v2

    .line 86
    .end local v0    # "component":Lcom/google/android/videos/flow/Flow;
    .end local v1    # "componentIndex":I
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onDataSetChanged(Lcom/google/android/videos/flow/Flow;)V
    .locals 5
    .param p1, "flow"    # Lcom/google/android/videos/flow/Flow;

    .prologue
    .line 121
    invoke-direct {p0, p1}, Lcom/google/android/videos/flow/SequentialFlow;->indexOf(Lcom/google/android/videos/flow/Flow;)I

    move-result v0

    .line 122
    .local v0, "componentIndex":I
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 123
    iget-object v3, p0, Lcom/google/android/videos/flow/SequentialFlow;->endPositions:[I

    aget v3, v3, v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/flow/SequentialFlow;->startPositionOf(I)I

    move-result v4

    sub-int v2, v3, v4

    .line 124
    .local v2, "oldSize":I
    invoke-virtual {p1}, Lcom/google/android/videos/flow/Flow;->getVisibleCount()I

    move-result v1

    .line 125
    .local v1, "newSize":I
    sub-int v3, v1, v2

    invoke-direct {p0, v0, v3}, Lcom/google/android/videos/flow/SequentialFlow;->adjustEndPositionsFrom(II)V

    .line 126
    invoke-virtual {p0}, Lcom/google/android/videos/flow/SequentialFlow;->notifyDataSetChanged()V

    .line 128
    .end local v1    # "newSize":I
    .end local v2    # "oldSize":I
    :cond_0
    return-void
.end method

.method protected onInitViewTypes(Landroid/util/SparseArray;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/videos/flow/ViewHolderCreator",
            "<*>;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 96
    .local p1, "viewTypes":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/google/android/videos/flow/ViewHolderCreator<*>;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/flow/SequentialFlow;->components:[Lcom/google/android/videos/flow/Flow;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 97
    iget-object v1, p0, Lcom/google/android/videos/flow/SequentialFlow;->components:[Lcom/google/android/videos/flow/Flow;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lcom/google/android/videos/flow/Flow;->onInitViewTypes(Landroid/util/SparseArray;)V

    .line 96
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 99
    :cond_0
    return-void
.end method

.method public onItemsChanged(Lcom/google/android/videos/flow/Flow;II)V
    .locals 5
    .param p1, "flow"    # Lcom/google/android/videos/flow/Flow;
    .param p2, "positionStart"    # I
    .param p3, "positionCount"    # I

    .prologue
    .line 153
    invoke-direct {p0, p1}, Lcom/google/android/videos/flow/SequentialFlow;->indexOf(Lcom/google/android/videos/flow/Flow;)I

    move-result v0

    .line 154
    .local v0, "componentIndex":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_3

    .line 155
    iget-object v2, p0, Lcom/google/android/videos/flow/SequentialFlow;->endPositions:[I

    aget v2, v2, v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/flow/SequentialFlow;->startPositionOf(I)I

    move-result v3

    sub-int v1, v2, v3

    .line 156
    .local v1, "oldSize":I
    if-ltz p2, :cond_0

    if-ltz p3, :cond_0

    add-int v2, p2, p3

    if-le v2, v1, :cond_1

    .line 158
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Inconsistency: a "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " reports "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " items changed at "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " whereas the size was only "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 162
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/videos/flow/Flow;->getVisibleCount()I

    move-result v2

    if-eq v1, v2, :cond_2

    .line 163
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Inconsistency: a "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " reports "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " items changed at "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " while also changing its size"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 167
    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/videos/flow/SequentialFlow;->startPositionOf(I)I

    move-result v2

    add-int/2addr v2, p2

    invoke-virtual {p0, v2, p3}, Lcom/google/android/videos/flow/SequentialFlow;->notifyItemsChanged(II)V

    .line 169
    .end local v1    # "oldSize":I
    :cond_3
    return-void
.end method

.method public onItemsInserted(Lcom/google/android/videos/flow/Flow;II)V
    .locals 6
    .param p1, "flow"    # Lcom/google/android/videos/flow/Flow;
    .param p2, "positionStart"    # I
    .param p3, "positionCount"    # I

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/google/android/videos/flow/SequentialFlow;->indexOf(Lcom/google/android/videos/flow/Flow;)I

    move-result v0

    .line 133
    .local v0, "componentIndex":I
    const/4 v3, -0x1

    if-eq v0, v3, :cond_3

    .line 134
    iget-object v3, p0, Lcom/google/android/videos/flow/SequentialFlow;->endPositions:[I

    aget v3, v3, v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/flow/SequentialFlow;->startPositionOf(I)I

    move-result v4

    sub-int v2, v3, v4

    .line 135
    .local v2, "oldSize":I
    if-ltz p2, :cond_0

    if-le p2, v2, :cond_1

    .line 136
    :cond_0
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Inconsistency: a "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " reports "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " items inserted at "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " whereas the old size was only "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 140
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/videos/flow/Flow;->getVisibleCount()I

    move-result v3

    add-int v4, v2, p3

    sub-int v1, v3, v4

    .line 141
    .local v1, "error":I
    if-eqz v1, :cond_2

    .line 142
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Inconsistency: a "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " reports "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " items inserted at "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " but its size change is off by "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 146
    :cond_2
    invoke-direct {p0, v0, p3}, Lcom/google/android/videos/flow/SequentialFlow;->adjustEndPositionsFrom(II)V

    .line 147
    invoke-direct {p0, v0}, Lcom/google/android/videos/flow/SequentialFlow;->startPositionOf(I)I

    move-result v3

    add-int/2addr v3, p2

    invoke-virtual {p0, v3, p3}, Lcom/google/android/videos/flow/SequentialFlow;->notifyItemsInserted(II)V

    .line 149
    .end local v1    # "error":I
    .end local v2    # "oldSize":I
    :cond_3
    return-void
.end method

.method public onItemsRemoved(Lcom/google/android/videos/flow/Flow;II)V
    .locals 6
    .param p1, "flow"    # Lcom/google/android/videos/flow/Flow;
    .param p2, "positionStart"    # I
    .param p3, "positionCount"    # I

    .prologue
    .line 173
    invoke-direct {p0, p1}, Lcom/google/android/videos/flow/SequentialFlow;->indexOf(Lcom/google/android/videos/flow/Flow;)I

    move-result v0

    .line 174
    .local v0, "componentIndex":I
    const/4 v3, -0x1

    if-eq v0, v3, :cond_3

    .line 175
    iget-object v3, p0, Lcom/google/android/videos/flow/SequentialFlow;->endPositions:[I

    aget v3, v3, v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/flow/SequentialFlow;->startPositionOf(I)I

    move-result v4

    sub-int v2, v3, v4

    .line 176
    .local v2, "oldSize":I
    if-ltz p2, :cond_0

    if-ltz p3, :cond_0

    add-int v3, p2, p3

    if-le v3, v2, :cond_1

    .line 178
    :cond_0
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Inconsistency: a "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " reports "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " items removed at "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " whereas the size was only "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 182
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/videos/flow/Flow;->getVisibleCount()I

    move-result v3

    sub-int v4, v2, p3

    sub-int v1, v3, v4

    .line 183
    .local v1, "error":I
    if-eqz v1, :cond_2

    .line 184
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Inconsistency: a "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " reports "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " items removed at "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " but its size change is off by "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 188
    :cond_2
    neg-int v3, p3

    invoke-direct {p0, v0, v3}, Lcom/google/android/videos/flow/SequentialFlow;->adjustEndPositionsFrom(II)V

    .line 189
    invoke-direct {p0, v0}, Lcom/google/android/videos/flow/SequentialFlow;->startPositionOf(I)I

    move-result v3

    add-int/2addr v3, p2

    invoke-virtual {p0, v3, p3}, Lcom/google/android/videos/flow/SequentialFlow;->notifyItemsRemoved(II)V

    .line 191
    .end local v1    # "error":I
    .end local v2    # "oldSize":I
    :cond_3
    return-void
.end method
