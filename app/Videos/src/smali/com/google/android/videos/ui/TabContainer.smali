.class public Lcom/google/android/videos/ui/TabContainer;
.super Landroid/widget/HorizontalScrollView;
.source "TabContainer.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/TabContainer$PagePreSelectionListener;
    }
.end annotation


# instance fields
.field private mLastScrollTo:I

.field private mPagePreSelectionListener:Lcom/google/android/videos/ui/TabContainer$PagePreSelectionListener;

.field private mScrollState:I

.field private mTabStrip:Lcom/google/android/videos/ui/TabStrip;

.field private final mTitleOffset:I

.field private mViewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/ui/TabContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/TabContainer;->setHorizontalScrollBarEnabled(Z)V

    .line 46
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0201

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/ui/TabContainer;->mTitleOffset:I

    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/ui/TabContainer;)Lcom/google/android/videos/ui/TabContainer$PagePreSelectionListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/TabContainer;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/videos/ui/TabContainer;->mPagePreSelectionListener:Lcom/google/android/videos/ui/TabContainer$PagePreSelectionListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/ui/TabContainer;)Landroid/support/v4/view/ViewPager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/TabContainer;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/videos/ui/TabContainer;->mViewPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/ui/TabContainer;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/ui/TabContainer;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/ui/TabContainer;->scrollToChild(II)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/videos/ui/TabContainer;)Lcom/google/android/videos/ui/TabStrip;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/TabContainer;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/videos/ui/TabContainer;->mTabStrip:Lcom/google/android/videos/ui/TabStrip;

    return-object v0
.end method

.method private scrollToChild(II)V
    .locals 5
    .param p1, "childIndex"    # I
    .param p2, "extraOffset"    # I

    .prologue
    .line 143
    iget-object v4, p0, Lcom/google/android/videos/ui/TabContainer;->mTabStrip:Lcom/google/android/videos/ui/TabStrip;

    invoke-virtual {v4}, Lcom/google/android/videos/ui/TabStrip;->getChildCount()I

    move-result v2

    .line 144
    .local v2, "tabStripChildCount":I
    if-eqz v2, :cond_0

    if-ltz p1, :cond_0

    if-lt p1, v2, :cond_1

    .line 165
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    iget-object v4, p0, Lcom/google/android/videos/ui/TabContainer;->mTabStrip:Lcom/google/android/videos/ui/TabStrip;

    invoke-virtual {v4, p1}, Lcom/google/android/videos/ui/TabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 149
    .local v0, "selectedChild":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 153
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 154
    .local v1, "selectedLeft":I
    add-int v3, v1, p2

    .line 155
    .local v3, "targetScrollX":I
    if-gtz p1, :cond_2

    if-lez p2, :cond_3

    .line 156
    :cond_2
    iget v4, p0, Lcom/google/android/videos/ui/TabContainer;->mTitleOffset:I

    sub-int/2addr v3, v4

    .line 159
    :cond_3
    iget v4, p0, Lcom/google/android/videos/ui/TabContainer;->mLastScrollTo:I

    if-eq v3, v4, :cond_0

    .line 163
    iput v3, p0, Lcom/google/android/videos/ui/TabContainer;->mLastScrollTo:I

    .line 164
    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lcom/google/android/videos/ui/TabContainer;->scrollTo(II)V

    goto :goto_0
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 51
    invoke-super {p0}, Landroid/widget/HorizontalScrollView;->onFinishInflate()V

    .line 52
    const v0, 0x7f0f00c3

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/TabContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/TabStrip;

    iput-object v0, p0, Lcom/google/android/videos/ui/TabContainer;->mTabStrip:Lcom/google/android/videos/ui/TabStrip;

    .line 53
    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 131
    iput p1, p0, Lcom/google/android/videos/ui/TabContainer;->mScrollState:I

    .line 132
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 4
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 116
    iget-object v3, p0, Lcom/google/android/videos/ui/TabContainer;->mTabStrip:Lcom/google/android/videos/ui/TabStrip;

    invoke-virtual {v3}, Lcom/google/android/videos/ui/TabStrip;->getChildCount()I

    move-result v2

    .line 117
    .local v2, "tabStripChildCount":I
    if-eqz v2, :cond_0

    if-ltz p1, :cond_0

    if-lt p1, v2, :cond_1

    .line 127
    :cond_0
    :goto_0
    return-void

    .line 121
    :cond_1
    iget-object v3, p0, Lcom/google/android/videos/ui/TabContainer;->mTabStrip:Lcom/google/android/videos/ui/TabStrip;

    invoke-virtual {v3, p1, p2}, Lcom/google/android/videos/ui/TabStrip;->onPageScrolled(IF)V

    .line 123
    iget-object v3, p0, Lcom/google/android/videos/ui/TabContainer;->mTabStrip:Lcom/google/android/videos/ui/TabStrip;

    invoke-virtual {v3, p1}, Lcom/google/android/videos/ui/TabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 124
    .local v1, "selectedTitle":Landroid/view/View;
    if-nez v1, :cond_2

    const/4 v0, 0x0

    .line 126
    .local v0, "extraOffset":I
    :goto_1
    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/ui/TabContainer;->scrollToChild(II)V

    goto :goto_0

    .line 124
    .end local v0    # "extraOffset":I
    :cond_2
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, p2

    float-to-int v0, v3

    goto :goto_1
.end method

.method public onPageSelected(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 136
    iget v0, p0, Lcom/google/android/videos/ui/TabContainer;->mScrollState:I

    if-nez v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/google/android/videos/ui/TabContainer;->mTabStrip:Lcom/google/android/videos/ui/TabStrip;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/ui/TabStrip;->onPageSelected(I)V

    .line 138
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/ui/TabContainer;->scrollToChild(II)V

    .line 140
    :cond_0
    return-void
.end method

.method public setPagePreSelectionListener(Lcom/google/android/videos/ui/TabContainer$PagePreSelectionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/videos/ui/TabContainer$PagePreSelectionListener;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/google/android/videos/ui/TabContainer;->mPagePreSelectionListener:Lcom/google/android/videos/ui/TabContainer$PagePreSelectionListener;

    .line 68
    return-void
.end method

.method public setSelectedIndicatorColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/videos/ui/TabContainer;->mTabStrip:Lcom/google/android/videos/ui/TabStrip;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/ui/TabStrip;->setSelectedIndicatorColor(I)V

    .line 60
    return-void
.end method

.method public setViewPager(Landroid/support/v4/view/ViewPager;)V
    .locals 8
    .param p1, "viewPager"    # Landroid/support/v4/view/ViewPager;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/google/android/videos/ui/TabContainer;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 79
    iget-object v5, p0, Lcom/google/android/videos/ui/TabContainer;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v5}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    .line 80
    .local v0, "adapter":Landroid/support/v4/view/PagerAdapter;
    invoke-virtual {p0}, Lcom/google/android/videos/ui/TabContainer;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 81
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 82
    const v5, 0x7f0400c5

    iget-object v6, p0, Lcom/google/android/videos/ui/TabContainer;->mTabStrip:Lcom/google/android/videos/ui/TabStrip;

    const/4 v7, 0x0

    invoke-virtual {v3, v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 84
    .local v4, "title":Landroid/widget/TextView;
    invoke-virtual {v0, v1}, Landroid/support/v4/view/PagerAdapter;->getPageTitle(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    move v2, v1

    .line 86
    .local v2, "indexToSelect":I
    new-instance v5, Lcom/google/android/videos/ui/TabContainer$1;

    invoke-direct {v5, p0, v2}, Lcom/google/android/videos/ui/TabContainer$1;-><init>(Lcom/google/android/videos/ui/TabContainer;I)V

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    iget-object v5, p0, Lcom/google/android/videos/ui/TabContainer;->mTabStrip:Lcom/google/android/videos/ui/TabStrip;

    invoke-virtual {v5, v4}, Lcom/google/android/videos/ui/TabStrip;->addView(Landroid/view/View;)V

    .line 81
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 97
    .end local v2    # "indexToSelect":I
    .end local v4    # "title":Landroid/widget/TextView;
    :cond_0
    iget-object v5, p0, Lcom/google/android/videos/ui/TabContainer;->mTabStrip:Lcom/google/android/videos/ui/TabStrip;

    invoke-virtual {v5}, Lcom/google/android/videos/ui/TabStrip;->syncTitleColor()V

    .line 101
    iget-object v5, p0, Lcom/google/android/videos/ui/TabContainer;->mTabStrip:Lcom/google/android/videos/ui/TabStrip;

    invoke-virtual {v5}, Lcom/google/android/videos/ui/TabStrip;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v5

    new-instance v6, Lcom/google/android/videos/ui/TabContainer$2;

    invoke-direct {v6, p0}, Lcom/google/android/videos/ui/TabContainer$2;-><init>(Lcom/google/android/videos/ui/TabContainer;)V

    invoke-virtual {v5, v6}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 112
    return-void
.end method
