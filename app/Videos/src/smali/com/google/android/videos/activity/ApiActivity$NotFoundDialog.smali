.class public Lcom/google/android/videos/activity/ApiActivity$NotFoundDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "ApiActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/activity/ApiActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NotFoundDialog"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 394
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static showInstance(Landroid/support/v4/app/FragmentActivity;)V
    .locals 3
    .param p0, "activity"    # Landroid/support/v4/app/FragmentActivity;

    .prologue
    .line 397
    new-instance v0, Lcom/google/android/videos/activity/ApiActivity$NotFoundDialog;

    invoke-direct {v0}, Lcom/google/android/videos/activity/ApiActivity$NotFoundDialog;-><init>()V

    .line 398
    .local v0, "instance":Lcom/google/android/videos/activity/ApiActivity$NotFoundDialog;
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "NotFoundDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/activity/ApiActivity$NotFoundDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 399
    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 403
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/videos/activity/ApiActivity$NotFoundDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b014a

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0b010c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 411
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 412
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ApiActivity$NotFoundDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/activity/ApiActivity;

    .line 413
    .local v0, "activity":Lcom/google/android/videos/activity/ApiActivity;
    const/4 v1, 0x3

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    # invokes: Lcom/google/android/videos/activity/ApiActivity;->finishWithFailure(ILjava/lang/Throwable;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/videos/activity/ApiActivity;->access$200(Lcom/google/android/videos/activity/ApiActivity;ILjava/lang/Throwable;)V

    .line 414
    return-void
.end method
