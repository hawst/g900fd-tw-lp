.class public Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;
.super Landroid/support/v7/app/MediaRouteControllerDialog;
.source "VideoMediaRouteControllerDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;
    }
.end annotation


# instance fields
.field private playPauseButton:Landroid/widget/ImageButton;

.field private playbackTimeText:Landroid/widget/TextView;

.field private playbackTitleText:Landroid/widget/TextView;

.field private final remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

.field private final strBuilder:Ljava/lang/StringBuilder;

.field private final transportControl:Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;

.field private videoThumbnailIcon:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Landroid/support/v7/app/MediaRouteControllerDialog;-><init>(Landroid/content/Context;)V

    .line 39
    invoke-static {p1}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v0

    .line 40
    .local v0, "videosGlobals":Lcom/google/android/videos/VideosGlobals;
    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getRemoteTracker()Lcom/google/android/videos/remote/RemoteTracker;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    .line 41
    new-instance v1, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;

    iget-object v2, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v3}, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;-><init>(Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;Lcom/google/android/videos/remote/RemoteTracker;Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$1;)V

    iput-object v1, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->transportControl:Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;

    .line 42
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v1, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->strBuilder:Ljava/lang/StringBuilder;

    .line 43
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;)Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->transportControl:Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->updatePlaybackTime(II)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;
    .param p1, "x1"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->updatePlayPauseIcon(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->playbackTitleText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->updateThumbnailImage()V

    return-void
.end method

.method private static scaleBitmapForThumbnail(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 223
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 224
    .local v1, "res":Landroid/content/res/Resources;
    const v2, 0x7f0e01c1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 225
    .local v0, "height":I
    invoke-static {p1, v0}, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->scaleBitmapToFitHeight(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v2

    return-object v2
.end method

.method private static scaleBitmapToFitHeight(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 5
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "height"    # I

    .prologue
    .line 232
    if-nez p0, :cond_0

    .line 233
    const/4 v3, 0x0

    .line 239
    :goto_0
    return-object v3

    .line 236
    :cond_0
    int-to-float v3, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float v0, v3, v4

    .line 237
    .local v0, "scaleY":F
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v0

    float-to-int v2, v3

    .line 238
    .local v2, "scaledWidth":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v0

    float-to-int v1, v3

    .line 239
    .local v1, "scaledHeight":I
    const/4 v3, 0x0

    invoke-static {p0, v2, v1, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    goto :goto_0
.end method

.method private updatePlayPauseIcon(I)V
    .locals 2
    .param p1, "state"    # I

    .prologue
    .line 99
    packed-switch p1, :pswitch_data_0

    .line 108
    iget-object v0, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->playPauseButton:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 111
    :goto_0
    return-void

    .line 102
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->playPauseButton:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_0

    .line 99
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private updatePlaybackTime(II)V
    .locals 6
    .param p1, "currentTimeMillis"    # I
    .param p2, "durationMillis"    # I

    .prologue
    const/4 v1, 0x0

    .line 114
    int-to-long v2, p2

    const-wide/32 v4, 0x36ee80

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    const/4 v0, 0x1

    .line 115
    .local v0, "durationIsHourOrMore":Z
    :goto_0
    iget-object v2, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->strBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 116
    iget-object v1, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->strBuilder:Ljava/lang/StringBuilder;

    invoke-static {p1, v0}, Lcom/google/android/videos/utils/TimeUtil;->getDurationString(IZ)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    iget-object v1, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->strBuilder:Ljava/lang/StringBuilder;

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    iget-object v1, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->strBuilder:Ljava/lang/StringBuilder;

    invoke-static {p2, v0}, Lcom/google/android/videos/utils/TimeUtil;->getDurationString(IZ)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    iget-object v1, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->playbackTimeText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->strBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    return-void

    .end local v0    # "durationIsHourOrMore":Z
    :cond_0
    move v0, v1

    .line 114
    goto :goto_0
.end method

.method private updateThumbnailImage()V
    .locals 3

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->videoThumbnailIcon:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v2}, Lcom/google/android/videos/remote/RemoteTracker;->getPosterBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->scaleBitmapForThumbnail(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 125
    return-void
.end method


# virtual methods
.method public onAttachedToWindow()V
    .locals 2

    .prologue
    .line 129
    const-string v0, "connect to remote tracker"

    invoke-static {v0}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 130
    iget-object v0, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    iget-object v1, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->transportControl:Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/remote/RemoteTracker;->registerCustomTransportControl(Lcom/google/android/videos/remote/TransportControl;)V

    .line 131
    iget-object v0, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    iget-object v1, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->transportControl:Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/remote/RemoteTracker;->enableRemoteControl(Lcom/google/android/videos/remote/TransportControl;)V

    .line 132
    invoke-super {p0}, Landroid/support/v7/app/MediaRouteControllerDialog;->onAttachedToWindow()V

    .line 133
    return-void
.end method

.method public onCreateMediaControlView(Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f0400c6

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 50
    .local v2, "playbackControlLayout":Landroid/view/View;
    const v5, 0x7f0f020b

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->videoThumbnailIcon:Landroid/widget/ImageView;

    .line 51
    iget-object v5, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->videoThumbnailIcon:Landroid/widget/ImageView;

    new-instance v6, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$1;

    invoke-direct {v6, p0}, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$1;-><init>(Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    const v5, 0x7f0f020e

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->playPauseButton:Landroid/widget/ImageButton;

    .line 60
    iget-object v5, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->playPauseButton:Landroid/widget/ImageButton;

    new-instance v6, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$2;

    invoke-direct {v6, p0}, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$2;-><init>(Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    const v5, 0x7f0f020c

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->playbackTitleText:Landroid/widget/TextView;

    .line 69
    const v5, 0x7f0f020d

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->playbackTimeText:Landroid/widget/TextView;

    .line 71
    iget-object v5, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v5}, Lcom/google/android/videos/remote/RemoteTracker;->isPlayingAnyVideo()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 73
    iget-object v5, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v5}, Lcom/google/android/videos/remote/RemoteTracker;->getVideoInfo()Lcom/google/android/videos/remote/RemoteVideoInfo;

    move-result-object v4

    .line 74
    .local v4, "videoInfo":Lcom/google/android/videos/remote/RemoteVideoInfo;
    if-eqz v4, :cond_0

    .line 75
    iget-object v5, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->playbackTitleText:Landroid/widget/TextView;

    iget-object v6, v4, Lcom/google/android/videos/remote/RemoteVideoInfo;->videoTitle:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    :cond_0
    iget-object v5, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v5}, Lcom/google/android/videos/remote/RemoteTracker;->getPlayerState()Lcom/google/android/videos/remote/PlayerState;

    move-result-object v3

    .line 79
    .local v3, "playerState":Lcom/google/android/videos/remote/PlayerState;
    const/4 v0, 0x0

    .line 80
    .local v0, "currentTime":I
    const/4 v1, 0x0

    .line 81
    .local v1, "duration":I
    if-eqz v3, :cond_1

    .line 82
    iget v5, v3, Lcom/google/android/videos/remote/PlayerState;->state:I

    invoke-direct {p0, v5}, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->updatePlayPauseIcon(I)V

    .line 83
    iget v0, v3, Lcom/google/android/videos/remote/PlayerState;->time:I

    .line 85
    :cond_1
    if-eqz v4, :cond_2

    .line 86
    iget v1, v4, Lcom/google/android/videos/remote/RemoteVideoInfo;->durationMillis:I

    .line 88
    :cond_2
    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->updatePlaybackTime(II)V

    .line 90
    invoke-direct {p0}, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->updateThumbnailImage()V

    .line 95
    .end local v0    # "currentTime":I
    .end local v1    # "duration":I
    .end local v3    # "playerState":Lcom/google/android/videos/remote/PlayerState;
    .end local v4    # "videoInfo":Lcom/google/android/videos/remote/RemoteVideoInfo;
    :goto_0
    return-object v2

    .line 92
    :cond_3
    const/16 v5, 0x8

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 137
    const-string v0, "disconnect from remote tracker"

    invoke-static {v0}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 138
    iget-object v0, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    iget-object v1, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->transportControl:Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/remote/RemoteTracker;->unregisterCustomTransportControl(Lcom/google/android/videos/remote/TransportControl;)V

    .line 139
    iget-object v0, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    iget-object v1, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->transportControl:Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/remote/RemoteTracker;->disableRemoteControl(Lcom/google/android/videos/remote/TransportControl;)V

    .line 140
    invoke-super {p0}, Landroid/support/v7/app/MediaRouteControllerDialog;->onDetachedFromWindow()V

    .line 141
    return-void
.end method
