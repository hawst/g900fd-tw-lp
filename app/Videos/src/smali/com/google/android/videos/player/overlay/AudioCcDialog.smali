.class public Lcom/google/android/videos/player/overlay/AudioCcDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "AudioCcDialog.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/overlay/AudioCcDialog$ArrayListPagerAdapter;,
        Lcom/google/android/videos/player/overlay/AudioCcDialog$SubtitleListHelper;,
        Lcom/google/android/videos/player/overlay/AudioCcDialog$AudioListHelper;,
        Lcom/google/android/videos/player/overlay/AudioCcDialog$ListHelper;
    }
.end annotation


# instance fields
.field private tabContainer:Lcom/google/android/videos/ui/TabContainer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 312
    return-void
.end method

.method private static buildList(Landroid/content/Context;Lcom/google/android/videos/player/overlay/AudioCcDialog$ListHelper;I)Landroid/widget/ListView;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p2, "selectedIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/videos/player/overlay/AudioCcDialog$ListHelper",
            "<*>;I)",
            "Landroid/widget/ListView;"
        }
    .end annotation

    .prologue
    .local p1, "listHelper":Lcom/google/android/videos/player/overlay/AudioCcDialog$ListHelper;, "Lcom/google/android/videos/player/overlay/AudioCcDialog$ListHelper<*>;"
    const/4 v2, 0x1

    .line 151
    new-instance v1, Landroid/widget/ListView;

    invoke-direct {v1, p0}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 152
    .local v1, "listView":Landroid/widget/ListView;
    invoke-virtual {p1, v1}, Lcom/google/android/videos/player/overlay/AudioCcDialog$ListHelper;->getFooter(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 153
    .local v0, "footer":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 154
    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 156
    :cond_0
    invoke-virtual {v1, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 157
    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 158
    invoke-virtual {v1, p2}, Landroid/widget/ListView;->setSelection(I)V

    .line 159
    invoke-virtual {v1, p2, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 160
    invoke-virtual {v1, p1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 161
    return-object v1
.end method

.method private static fromByteArrays(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[B>;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AudioInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 176
    .local p0, "codedTracks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    if-nez p0, :cond_1

    .line 177
    const/4 v2, 0x0

    .line 187
    :cond_0
    return-object v2

    .line 179
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 180
    .local v2, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/wireless/android/video/magma/proto/AudioInfo;>;"
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [B

    .line 182
    .local v3, "track":[B
    :try_start_0
    new-instance v4, Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    invoke-direct {v4}, Lcom/google/wireless/android/video/magma/proto/AudioInfo;-><init>()V

    invoke-static {v4, v3}, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 183
    :catch_0
    move-exception v0

    .line 184
    .local v0, "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4
.end method

.method private getColorAccent(Landroid/content/Context;)I
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 122
    const/4 v0, -0x1

    .line 123
    .local v0, "colorAccent":I
    sget v3, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v4, 0x15

    if-lt v3, v4, :cond_0

    .line 124
    const/4 v3, 0x1

    new-array v1, v3, [I

    const v3, 0x1010435

    aput v3, v1, v6

    .line 125
    .local v1, "colorAccentAttr":[I
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 126
    .local v2, "typedArray":Landroid/content/res/TypedArray;
    invoke-virtual {v2, v6, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 127
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 129
    .end local v1    # "colorAccentAttr":[I
    .end local v2    # "typedArray":Landroid/content/res/TypedArray;
    :cond_0
    if-ne v0, v5, :cond_1

    .line 130
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0079

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    .line 132
    :cond_1
    return v0
.end method

.method public static showInstance(Landroid/support/v4/app/FragmentActivity;Ljava/util/ArrayList;ILjava/util/ArrayList;I)Landroid/content/DialogInterface;
    .locals 4
    .param p0, "activity"    # Landroid/support/v4/app/FragmentActivity;
    .param p2, "selectedSubtitleIndex"    # I
    .param p4, "selectedAudioTrackIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/FragmentActivity;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AudioInfo;",
            ">;I)",
            "Landroid/content/DialogInterface;"
        }
    .end annotation

    .prologue
    .line 56
    .local p1, "subtitles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/subtitles/SubtitleTrack;>;"
    .local p3, "audioTracks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/wireless/android/video/magma/proto/AudioInfo;>;"
    instance-of v2, p0, Lcom/google/android/videos/player/overlay/TrackChangeListener;

    invoke-static {v2}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 58
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 59
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v2, "subtitles"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 60
    const-string v2, "selected_subtitle"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 61
    const-string v2, "audio_tracks"

    invoke-static {p3}, Lcom/google/android/videos/player/overlay/AudioCcDialog;->toByteArrays(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 62
    const-string v2, "selected_audio_track"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 64
    new-instance v1, Lcom/google/android/videos/player/overlay/AudioCcDialog;

    invoke-direct {v1}, Lcom/google/android/videos/player/overlay/AudioCcDialog;-><init>()V

    .line 65
    .local v1, "instance":Lcom/google/android/videos/player/overlay/AudioCcDialog;
    invoke-virtual {v1, v0}, Lcom/google/android/videos/player/overlay/AudioCcDialog;->setArguments(Landroid/os/Bundle;)V

    .line 66
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "AudioCcDialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/player/overlay/AudioCcDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 68
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    .line 69
    invoke-virtual {v1}, Lcom/google/android/videos/player/overlay/AudioCcDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    return-object v2
.end method

.method private static toByteArrays(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AudioInfo;",
            ">;)",
            "Ljava/util/ArrayList",
            "<[B>;"
        }
    .end annotation

    .prologue
    .line 165
    .local p0, "audioTracks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/wireless/android/video/magma/proto/AudioInfo;>;"
    if-nez p0, :cond_1

    .line 166
    const/4 v1, 0x0

    .line 172
    :cond_0
    return-object v1

    .line 168
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 169
    .local v1, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    .line 170
    .local v2, "track":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    invoke-static {v2}, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/AudioCcDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 76
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v11, "audio_tracks"

    invoke-virtual {v0, v11}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v11

    check-cast v11, Ljava/util/ArrayList;

    invoke-static {v11}, Lcom/google/android/videos/player/overlay/AudioCcDialog;->fromByteArrays(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    .line 79
    .local v2, "audioTracks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/wireless/android/video/magma/proto/AudioInfo;>;"
    const-string v11, "subtitles"

    invoke-virtual {v0, v11}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v7

    check-cast v7, Ljava/util/ArrayList;

    .line 82
    .local v7, "subtitles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/subtitles/SubtitleTrack;>;"
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/AudioCcDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v11

    invoke-direct {v3, v11}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 83
    .local v3, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 84
    .local v4, "context":Landroid/content/Context;
    new-instance v8, Ljava/util/ArrayList;

    const/4 v11, 0x2

    invoke-direct {v8, v11}, Ljava/util/ArrayList;-><init>(I)V

    .line 85
    .local v8, "titles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v10, Ljava/util/ArrayList;

    const/4 v11, 0x2

    invoke-direct {v10, v11}, Ljava/util/ArrayList;-><init>(I)V

    .line 87
    .local v10, "views":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    if-eqz v2, :cond_0

    .line 88
    new-instance v11, Lcom/google/android/videos/player/overlay/AudioCcDialog$AudioListHelper;

    invoke-direct {v11, p0, v4, v2}, Lcom/google/android/videos/player/overlay/AudioCcDialog$AudioListHelper;-><init>(Lcom/google/android/videos/player/overlay/AudioCcDialog;Landroid/content/Context;Ljava/util/ArrayList;)V

    const-string v12, "selected_audio_track"

    invoke-virtual {v0, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v12

    invoke-static {v4, v11, v12}, Lcom/google/android/videos/player/overlay/AudioCcDialog;->buildList(Landroid/content/Context;Lcom/google/android/videos/player/overlay/AudioCcDialog$ListHelper;I)Landroid/widget/ListView;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/AudioCcDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0b01ee

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 93
    :cond_0
    if-eqz v7, :cond_1

    .line 94
    const-string v11, "selected_subtitle"

    invoke-virtual {v0, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 95
    .local v6, "selectedPosition":I
    new-instance v11, Lcom/google/android/videos/player/overlay/AudioCcDialog$SubtitleListHelper;

    invoke-direct {v11, p0, v4, v7, v6}, Lcom/google/android/videos/player/overlay/AudioCcDialog$SubtitleListHelper;-><init>(Lcom/google/android/videos/player/overlay/AudioCcDialog;Landroid/content/Context;Ljava/util/ArrayList;I)V

    invoke-static {v4, v11, v6}, Lcom/google/android/videos/player/overlay/AudioCcDialog;->buildList(Landroid/content/Context;Lcom/google/android/videos/player/overlay/AudioCcDialog$ListHelper;I)Landroid/widget/ListView;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/AudioCcDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0b01ec

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    .end local v6    # "selectedPosition":I
    :cond_1
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_2

    .line 101
    const/4 v11, 0x0

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/CharSequence;

    invoke-virtual {v3, v11}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 102
    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/view/View;

    invoke-virtual {v3, v11}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 116
    :goto_0
    const v11, 0x104000a

    const/4 v12, 0x0

    invoke-virtual {v3, v11, v12}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 117
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v11

    return-object v11

    .line 104
    :cond_2
    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    .line 106
    .local v5, "inflater":Landroid/view/LayoutInflater;
    const v11, 0x7f040019

    const/4 v12, 0x0

    invoke-virtual {v5, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 107
    .local v1, "audioCcDialogView":Landroid/view/ViewGroup;
    const v11, 0x7f0f00c4

    invoke-virtual {v1, v11}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/support/v4/view/ViewPager;

    .line 108
    .local v9, "viewPager":Landroid/support/v4/view/ViewPager;
    new-instance v11, Lcom/google/android/videos/player/overlay/AudioCcDialog$ArrayListPagerAdapter;

    invoke-direct {v11, v8, v10}, Lcom/google/android/videos/player/overlay/AudioCcDialog$ArrayListPagerAdapter;-><init>(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    invoke-virtual {v9, v11}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 109
    invoke-virtual {v9, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 110
    const v11, 0x7f0f00c2

    invoke-virtual {v1, v11}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Lcom/google/android/videos/ui/TabContainer;

    iput-object v11, p0, Lcom/google/android/videos/player/overlay/AudioCcDialog;->tabContainer:Lcom/google/android/videos/ui/TabContainer;

    .line 111
    iget-object v11, p0, Lcom/google/android/videos/player/overlay/AudioCcDialog;->tabContainer:Lcom/google/android/videos/ui/TabContainer;

    invoke-virtual {v11, v9}, Lcom/google/android/videos/ui/TabContainer;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 112
    iget-object v11, p0, Lcom/google/android/videos/player/overlay/AudioCcDialog;->tabContainer:Lcom/google/android/videos/ui/TabContainer;

    invoke-direct {p0, v4}, Lcom/google/android/videos/player/overlay/AudioCcDialog;->getColorAccent(Landroid/content/Context;)I

    move-result v12

    invoke-virtual {v11, v12}, Lcom/google/android/videos/ui/TabContainer;->setSelectedIndicatorColor(I)V

    .line 113
    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method public onPageScrollStateChanged(I)V
    .locals 1
    .param p1, "scrollState"    # I

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/AudioCcDialog;->tabContainer:Lcom/google/android/videos/ui/TabContainer;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/ui/TabContainer;->onPageScrollStateChanged(I)V

    .line 138
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/AudioCcDialog;->tabContainer:Lcom/google/android/videos/ui/TabContainer;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/videos/ui/TabContainer;->onPageScrolled(IFI)V

    .line 143
    return-void
.end method

.method public onPageSelected(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/AudioCcDialog;->tabContainer:Lcom/google/android/videos/ui/TabContainer;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/ui/TabContainer;->onPageSelected(I)V

    .line 148
    return-void
.end method
