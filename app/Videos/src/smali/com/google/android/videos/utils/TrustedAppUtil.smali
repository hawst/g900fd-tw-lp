.class public final Lcom/google/android/videos/utils/TrustedAppUtil;
.super Ljava/lang/Object;
.source "TrustedAppUtil.java"


# static fields
.field private static final TRUSTED_APPS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/content/pm/Signature;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    .line 27
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 28
    .local v2, "trustedApps":Ljava/util/Map;, "Ljava/util/Map<Landroid/content/pm/Signature;Ljava/lang/String;>;"
    const-string v0, "MIIDlzCCAn-gAwIBAgIEQcOdFDANBgkqhkiG9w0BAQsFADB8MQswCQYDVQQGEwJ1czELMAkGA1UECBMCY2ExEDAOBgNVBAcTB2J1cmJhbmsxEDAOBgNVBAoTB3N0dWRpb3MxGTAXBgNVBAsTEGRpc25leWdvb2dsZXBsYXkxITAfBgNVBAMTGGRpc25leW1vdmllc2FueXdoZXJlMjAxNDAeFw0xNDA2MTIyMjA5NTFaFw00MTEwMjgyMjA5NTFaMHwxCzAJBgNVBAYTAnVzMQswCQYDVQQIEwJjYTEQMA4GA1UEBxMHYnVyYmFuazEQMA4GA1UEChMHc3R1ZGlvczEZMBcGA1UECxMQZGlzbmV5Z29vZ2xlcGxheTEhMB8GA1UEAxMYZGlzbmV5bW92aWVzYW55d2hlcmUyMDE0MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmgl7r3iFr4hjA017dYGpfCG37TNM3FEJRxvpeG6jYetUpXydo9EcAM472Bk4bxH7Re0xfVrPRFQWhw3OyNHrRSkHkeNzNgKinSjqbwpgiNg8eEf56F5ft3iIT3-JmFzfC4_ixaeCE3cddf39ydGuB3qsyjDHs_qdMjTF7l1EC0LNOqkZcff2WwAVdRNRs-tJZxtmRJpYKtAMe4eHzP-M_NwzD-fLuDcDN3A4AGOJIQ92zrt9mP4IufdL0jozLwWjZyK5LfnT7IUiohHHsHxWHren6gT5SMoJWNGBh4SGea237h68mVn_afcHSXeyfQ8dhZqZTnPkdqc1HaGCbl3GNQIDAQABoyEwHzAdBgNVHQ4EFgQURoXS9Qti8krJ-E5JgSXy42QJ9CkwDQYJKoZIhvcNAQELBQADggEBAENhqxFeVwYo-cbzobs-Er-uNr9OpuR0gmM49MoePVDfDaGHAssgKX3cIG_6_QfthYLAbVlayb9et9NhSlfKhTeaW25aV6C3LQGeAYoyx_8ov00ldkCUtAeThynX_fGo5OUWPbW9LMeyb5ltRmXWEPLxAMLllcnQfV7F-ELs2LfE1MLfpmkcMHnja11xtptxjtUnWnoE4JvSWyP-w3zV4oq6PGqWbJxjnbEpqZlvSDRsgQin1igXTTAma8xVphQrQ0YeWrUbV_Cp3-6rRWYdvulA_6qHZV_3KWyPV6hXMidmd6xdqF_ImenkjmgfIfY2M1iKbfEl-l-PXE0tFPYOm0g"

    .line 43
    .local v0, "dmaAppRelease":Ljava/lang/String;
    new-instance v3, Landroid/content/pm/Signature;

    invoke-static {v0, v5}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/content/pm/Signature;-><init>([B)V

    const-string v4, "DMA"

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    const-string v1, "MIIB5TCCAU6gAwIBAgIEUEck6zANBgkqhkiG9w0BAQUFADA3MQswCQYDVQQGEwJVUzEQMA4GA1UEChMHQW5kcm9pZDEWMBQGA1UEAxMNQW5kcm9pZCBEZWJ1ZzAeFw0xMjA5MDUxMDA5NDdaFw00MjA4MjkxMDA5NDdaMDcxCzAJBgNVBAYTAlVTMRAwDgYDVQQKEwdBbmRyb2lkMRYwFAYDVQQDEw1BbmRyb2lkIERlYnVnMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDn8kllBmswRd5s8GRIkXzhHdB9tXIFTiwUEpsNI95mxmFczxBrX2zB7jtF1LWLgq7UpI3i9nB25vLxWSYfPBi56hLQE-c9ZYOxwHVnAfhlaVmHgR8wCxcdZLlQAuRIEkJHX52tdNb5hrTxls1yR3mhKt4tAKV_iYlYJ6U_b1RfDwIDAQABMA0GCSqGSIb3DQEBBQUAA4GBAKQg120X6yyqhstwzPxTt5NciExBJ2Andyofl_Wfqqd0j4TOgJJGRMrlX4Xzy6Bnlqq0I2GvCZ7mqFGz-9ksZo8gc_fgAzglVAH-CWCIrBh0shSNYibhAqFE2o1f5C8ioLd9qBDjlRXIr0NnxGCCUgJsHhuR15ZiQUFvYmFPRVIO"

    .line 53
    .local v1, "dmaTestApp":Ljava/lang/String;
    new-instance v3, Landroid/content/pm/Signature;

    invoke-static {v1, v5}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/content/pm/Signature;-><init>([B)V

    const-string v4, "DMA simulator"

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v3

    sput-object v3, Lcom/google/android/videos/utils/TrustedAppUtil;->TRUSTED_APPS:Ljava/util/Map;

    .line 56
    return-void
.end method

.method public static getTrustedAppReferer(Ljava/lang/String;Landroid/content/pm/PackageManager;Lcom/google/android/videos/Config;)Ljava/lang/String;
    .locals 9
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "packageManager"    # Landroid/content/pm/PackageManager;
    .param p2, "config"    # Lcom/google/android/videos/Config;

    .prologue
    const/4 v7, 0x0

    .line 64
    if-nez p0, :cond_1

    move-object v4, v7

    .line 84
    :cond_0
    :goto_0
    return-object v4

    .line 67
    :cond_1
    invoke-interface {p2}, Lcom/google/android/videos/Config;->externalApiEnabled()Z

    move-result v8

    if-nez v8, :cond_2

    move-object v4, v7

    .line 68
    goto :goto_0

    .line 72
    :cond_2
    const/16 v8, 0x40

    :try_start_0
    invoke-virtual {p1, p0, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v8

    iget-object v6, v8, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    .local v6, "signatures":[Landroid/content/pm/Signature;
    move-object v0, v6

    .local v0, "arr$":[Landroid/content/pm/Signature;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_3

    aget-object v5, v0, v2

    .line 79
    .local v5, "signature":Landroid/content/pm/Signature;
    sget-object v8, Lcom/google/android/videos/utils/TrustedAppUtil;->TRUSTED_APPS:Ljava/util/Map;

    invoke-interface {v8, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 80
    .local v4, "referer":Ljava/lang/String;
    if-nez v4, :cond_0

    .line 78
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 74
    .end local v0    # "arr$":[Landroid/content/pm/Signature;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "referer":Ljava/lang/String;
    .end local v5    # "signature":Landroid/content/pm/Signature;
    .end local v6    # "signatures":[Landroid/content/pm/Signature;
    :catch_0
    move-exception v1

    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    move-object v4, v7

    .line 75
    goto :goto_0

    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v0    # "arr$":[Landroid/content/pm/Signature;
    .restart local v2    # "i$":I
    .restart local v3    # "len$":I
    .restart local v6    # "signatures":[Landroid/content/pm/Signature;
    :cond_3
    move-object v4, v7

    .line 84
    goto :goto_0
.end method
