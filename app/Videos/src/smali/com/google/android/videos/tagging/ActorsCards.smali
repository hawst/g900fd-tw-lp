.class final Lcom/google/android/videos/tagging/ActorsCards;
.super Lcom/google/android/videos/tagging/Cards;
.source "ActorsCards.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/tagging/ActorsCards$ActorCardItemClickListener;
    }
.end annotation


# direct methods
.method private static createActorsCardItems(Ljava/util/List;Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;ILcom/google/android/videos/tagging/KnowledgeBundle$OnActorClickListener;ILcom/google/android/videos/async/Requester;ILandroid/app/Activity;Lcom/google/android/videos/tagging/CardTag;)Landroid/view/View;
    .locals 15
    .param p1, "cardInflater"    # Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;
    .param p2, "cardLayoutId"    # I
    .param p3, "onActorsCardClickListener"    # Lcom/google/android/videos/tagging/KnowledgeBundle$OnActorClickListener;
    .param p4, "itemLayoutId"    # I
    .param p6, "avatarImageDimenId"    # I
    .param p7, "activity"    # Landroid/app/Activity;
    .param p8, "cardTag"    # Lcom/google/android/videos/tagging/CardTag;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Person;",
            ">;",
            "Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;",
            "I",
            "Lcom/google/android/videos/tagging/KnowledgeBundle$OnActorClickListener;",
            "I",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;I",
            "Landroid/app/Activity;",
            "Lcom/google/android/videos/tagging/CardTag;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 50
    .local p0, "actors":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/KnowledgeEntity$Person;>;"
    .local p5, "imageRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;>;"
    invoke-virtual/range {p7 .. p7}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    move/from16 v0, p6

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 51
    .local v5, "avatarImageSize":I
    invoke-interface/range {p1 .. p2}, Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;->inflate(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup;

    .line 52
    .local v8, "actorsCard":Landroid/view/ViewGroup;
    invoke-virtual {v8}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v13

    .line 53
    .local v13, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {v8}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    .line 54
    .local v14, "resources":Landroid/content/res/Resources;
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v7

    .line 55
    .local v7, "actorCount":I
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    if-ge v12, v7, :cond_1

    .line 56
    invoke-interface {p0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    .line 58
    .local v3, "actor":Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    const/4 v1, 0x0

    move/from16 v0, p4

    invoke-virtual {v13, v0, v8, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v10

    .line 60
    .local v10, "cardItemView":Landroid/view/View;
    const v1, 0x7f0f00f9

    iget-object v4, v3, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->name:Ljava/lang/String;

    invoke-static {v10, v1, v4}, Lcom/google/android/videos/tagging/ActorsCards;->showViewAndSetText(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    .line 62
    invoke-static {v14, v3}, Lcom/google/android/videos/tagging/ActorsCards;->buildCharacterNamesString(Landroid/content/res/Resources;Lcom/google/android/videos/tagging/KnowledgeEntity$Person;)Ljava/lang/String;

    move-result-object v11

    .line 63
    .local v11, "characterNames":Ljava/lang/String;
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 64
    const v1, 0x7f0f00fa

    invoke-static {v10, v1, v11}, Lcom/google/android/videos/tagging/ActorsCards;->showViewAndSetText(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    .line 67
    :cond_0
    const v1, 0x7f0f00f8

    invoke-virtual {v10, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 68
    .local v2, "textView":Landroid/widget/TextView;
    const/4 v6, 0x0

    move-object/from16 v1, p7

    move-object/from16 v4, p5

    invoke-static/range {v1 .. v6}, Lcom/google/android/videos/tagging/ActorsCards;->requestActorImage(Landroid/app/Activity;Landroid/widget/TextView;Lcom/google/android/videos/tagging/KnowledgeEntity$Person;Lcom/google/android/videos/async/Requester;IZ)V

    .line 70
    invoke-virtual {v8, v10}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 72
    new-instance v9, Lcom/google/android/videos/tagging/ActorsCards$ActorCardItemClickListener;

    move-object/from16 v0, p3

    invoke-direct {v9, v2, v3, v0}, Lcom/google/android/videos/tagging/ActorsCards$ActorCardItemClickListener;-><init>(Landroid/view/View;Lcom/google/android/videos/tagging/KnowledgeEntity$Person;Lcom/google/android/videos/tagging/KnowledgeBundle$OnActorClickListener;)V

    .line 74
    .local v9, "actorsCardItemClickListener":Lcom/google/android/videos/tagging/ActorsCards$ActorCardItemClickListener;
    invoke-virtual {v10, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 76
    .end local v2    # "textView":Landroid/widget/TextView;
    .end local v3    # "actor":Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    .end local v9    # "actorsCardItemClickListener":Lcom/google/android/videos/tagging/ActorsCards$ActorCardItemClickListener;
    .end local v10    # "cardItemView":Landroid/view/View;
    .end local v11    # "characterNames":Ljava/lang/String;
    :cond_1
    move-object/from16 v0, p8

    invoke-virtual {v8, v0}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    .line 77
    return-object v8
.end method

.method public static createCurrentActorsCard(Ljava/util/List;Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;Lcom/google/android/videos/tagging/KnowledgeBundle$OnActorClickListener;Lcom/google/android/videos/async/Requester;Landroid/app/Activity;)Landroid/view/View;
    .locals 9
    .param p1, "cardInflater"    # Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;
    .param p2, "onActorsCardClickListener"    # Lcom/google/android/videos/tagging/KnowledgeBundle$OnActorClickListener;
    .param p4, "activity"    # Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Person;",
            ">;",
            "Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;",
            "Lcom/google/android/videos/tagging/KnowledgeBundle$OnActorClickListener;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/app/Activity;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 32
    .local p0, "currentActors":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/KnowledgeEntity$Person;>;"
    .local p3, "imageRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;>;"
    const v2, 0x7f040031

    const v4, 0x7f040032

    const v6, 0x7f0e01a8

    sget-object v8, Lcom/google/android/videos/tagging/CardTag;->CURRENT_ACTORS:Lcom/google/android/videos/tagging/CardTag;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v5, p3

    move-object v7, p4

    invoke-static/range {v0 .. v8}, Lcom/google/android/videos/tagging/ActorsCards;->createActorsCardItems(Ljava/util/List;Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;ILcom/google/android/videos/tagging/KnowledgeBundle$OnActorClickListener;ILcom/google/android/videos/async/Requester;ILandroid/app/Activity;Lcom/google/android/videos/tagging/CardTag;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static createRecentActorsCard(Ljava/util/List;Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;Lcom/google/android/videos/tagging/KnowledgeBundle$OnActorClickListener;Lcom/google/android/videos/async/Requester;Landroid/app/Activity;)Landroid/view/View;
    .locals 9
    .param p1, "cardInflater"    # Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;
    .param p2, "onRecentActorsCardClickListener"    # Lcom/google/android/videos/tagging/KnowledgeBundle$OnActorClickListener;
    .param p4, "activity"    # Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Person;",
            ">;",
            "Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;",
            "Lcom/google/android/videos/tagging/KnowledgeBundle$OnActorClickListener;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/app/Activity;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 40
    .local p0, "recentActors":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/KnowledgeEntity$Person;>;"
    .local p3, "imageRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;>;"
    const v2, 0x7f0400ae

    const v4, 0x7f0400af

    const v6, 0x7f0e01a5

    sget-object v8, Lcom/google/android/videos/tagging/CardTag;->RECENT_ACTORS:Lcom/google/android/videos/tagging/CardTag;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v5, p3

    move-object v7, p4

    invoke-static/range {v0 .. v8}, Lcom/google/android/videos/tagging/ActorsCards;->createActorsCardItems(Ljava/util/List;Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;ILcom/google/android/videos/tagging/KnowledgeBundle$OnActorClickListener;ILcom/google/android/videos/async/Requester;ILandroid/app/Activity;Lcom/google/android/videos/tagging/CardTag;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
