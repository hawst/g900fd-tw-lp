.class final Lcom/google/android/videos/api/UpdateAssetVisibilityConverter;
.super Ljava/lang/Object;
.source "UpdateAssetVisibilityConverter.java"

# interfaces
.implements Lcom/google/android/videos/converter/RequestConverter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/converter/RequestConverter",
        "<",
        "Lcom/google/android/videos/api/UpdateAssetVisibilityRequest;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        ">;"
    }
.end annotation


# instance fields
.field private final baseUri:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/net/Uri;)V
    .locals 2
    .param p1, "baseUri"    # Landroid/net/Uri;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Lcom/google/android/videos/utils/UriBuilder;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "userlibrary"

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;->addSegment(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/utils/UriBuilder;->build()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/api/UpdateAssetVisibilityConverter;->baseUri:Ljava/lang/String;

    .line 27
    return-void
.end method


# virtual methods
.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 15
    check-cast p1, Lcom/google/android/videos/api/UpdateAssetVisibilityRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/api/UpdateAssetVisibilityConverter;->convertRequest(Lcom/google/android/videos/api/UpdateAssetVisibilityRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public convertRequest(Lcom/google/android/videos/api/UpdateAssetVisibilityRequest;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 4
    .param p1, "request"    # Lcom/google/android/videos/api/UpdateAssetVisibilityRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 32
    new-instance v2, Lcom/google/android/videos/utils/UriBuilder;

    iget-object v1, p0, Lcom/google/android/videos/api/UpdateAssetVisibilityConverter;->baseUri:Ljava/lang/String;

    invoke-direct {v2, v1}, Lcom/google/android/videos/utils/UriBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p1, Lcom/google/android/videos/api/UpdateAssetVisibilityRequest;->hidden:Z

    if-eqz v1, :cond_0

    const-string v1, "hide"

    :goto_0
    invoke-virtual {v2, v1}, Lcom/google/android/videos/utils/UriBuilder;->addSegment(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v1

    const-string v2, "aid"

    iget-object v3, p1, Lcom/google/android/videos/api/UpdateAssetVisibilityRequest;->assetResourceId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/videos/utils/UriBuilder;->build()Ljava/lang/String;

    move-result-object v0

    .line 36
    .local v0, "uri":Ljava/lang/String;
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/videos/async/HttpUriRequestFactory;->createPost(Ljava/lang/String;[B)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v1

    return-object v1

    .line 32
    .end local v0    # "uri":Ljava/lang/String;
    :cond_0
    const-string v1, "unhide"

    goto :goto_0
.end method
