.class public Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;
.super Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;
.source "ShowDetailsRowHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$SeasonComparator;,
        Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeComparator;,
        Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$Query;,
        Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;
    }
.end annotation


# static fields
.field private static final episodeComparator:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeComparator;

.field private static final seasonComparator:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$SeasonComparator;


# instance fields
.field private cachedEpisodes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[",
            "Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final contentView:Landroid/view/View;

.field private lastSeasonListRow:Landroid/support/v17/leanback/widget/ListRow;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 86
    new-instance v0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeComparator;

    invoke-direct {v0, v1}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeComparator;-><init>(Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$1;)V

    sput-object v0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->episodeComparator:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeComparator;

    .line 87
    new-instance v0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$SeasonComparator;

    invoke-direct {v0, v1}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$SeasonComparator;-><init>(Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$1;)V

    sput-object v0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->seasonComparator:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$SeasonComparator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$OnPurchaseActionListener;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/api/RecommendationsRequest$Factory;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/WishlistStore;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/async/Requester;Landroid/view/View;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "itemId"    # Ljava/lang/String;
    .param p4, "onPurchaseActionListener"    # Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$OnPurchaseActionListener;
    .param p6, "relatedRequestFactory"    # Lcom/google/android/videos/api/RecommendationsRequest$Factory;
    .param p10, "purchaseStore"    # Lcom/google/android/videos/store/PurchaseStore;
    .param p11, "wishlistStore"    # Lcom/google/android/videos/store/WishlistStore;
    .param p12, "configurationStore"    # Lcom/google/android/videos/store/ConfigurationStore;
    .param p14, "contentView"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$OnPurchaseActionListener;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;",
            "Lcom/google/android/videos/api/RecommendationsRequest$Factory;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/RecommendationsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;",
            ">;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetReviewListRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;",
            ">;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/CategoryListRequest;",
            "Lcom/google/wireless/android/video/magma/proto/CategoryListResponse;",
            ">;",
            "Lcom/google/android/videos/store/PurchaseStore;",
            "Lcom/google/android/videos/store/WishlistStore;",
            "Lcom/google/android/videos/store/ConfigurationStore;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 112
    .local p5, "assetsCachingRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;>;"
    .local p7, "relatedRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/RecommendationsRequest;Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;>;"
    .local p8, "reviewsRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/AssetReviewListRequest;Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;>;"
    .local p9, "categoryListRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/CategoryListRequest;Lcom/google/wireless/android/video/magma/proto/CategoryListResponse;>;"
    .local p13, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/async/ControllableRequest<Landroid/net/Uri;>;Landroid/graphics/Bitmap;>;"
    invoke-direct/range {p0 .. p13}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$OnPurchaseActionListener;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/api/RecommendationsRequest$Factory;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/WishlistStore;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/async/Requester;)V

    .line 115
    iput-object p14, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->contentView:Landroid/view/View;

    .line 116
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->cachedEpisodes:Ljava/util/Map;

    .line 117
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;)Landroid/support/v17/leanback/widget/ListRow;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->lastSeasonListRow:Landroid/support/v17/leanback/widget/ListRow;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->contentView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;
    .param p1, "x1"    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->getEpisodeTitle(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;
    .param p1, "x1"    # Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->getOnOwnedEpisodeClickAction(Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private addEpisode(Landroid/support/v17/leanback/widget/ArrayObjectAdapter;Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;)V
    .locals 11
    .param p1, "itemArrayObjectAdapter"    # Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
    .param p2, "episode"    # Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;

    .prologue
    .line 261
    iget-boolean v0, p2, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->purchased:Z

    if-nez v0, :cond_0

    iget-object v0, p2, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->cheapestOffer:Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    if-nez v0, :cond_0

    .line 357
    :goto_0
    return-void

    .line 264
    :cond_0
    iget-object v9, p2, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->assetResource:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 265
    .local v9, "episodeAsset":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    iget-object v1, v9, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    .line 275
    .local v1, "metadata":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    const/4 v2, 0x4

    const v3, 0x7f0e01b4

    const v4, 0x3fe38e39

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->getImageUri(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;IIFF)Ljava/lang/String;

    move-result-object v10

    .line 278
    .local v10, "imageUri":Ljava/lang/String;
    new-instance v2, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;

    iget-object v0, v9, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    iget-object v0, v9, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->itemId:Ljava/lang/String;

    const/4 v6, 0x0

    const/16 v7, 0xc

    invoke-direct/range {v2 .. v7}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 281
    .local v2, "purchase":Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;
    new-instance v3, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$4;

    move-object v4, p0

    move-object v5, v1

    move-object v6, p2

    move-object v7, v10

    move-object v8, v2

    invoke-direct/range {v3 .. v9}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$4;-><init>(Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;Ljava/lang/String;Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;Lcom/google/wireless/android/video/magma/proto/AssetResource;)V

    invoke-virtual {p1, v3}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private addEpisodeActions()V
    .locals 14

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->getLastAndNextEpisode()Landroid/util/Pair;

    move-result-object v10

    .line 168
    .local v10, "lastAndNext":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;>;"
    if-nez v10, :cond_1

    .line 202
    :cond_0
    :goto_0
    return-void

    .line 172
    :cond_1
    iget-object v11, v10, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v11, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;

    .line 173
    .local v11, "lastWatched":Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;
    iget-object v12, v10, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v12, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;

    .line 175
    .local v12, "nextEpisode":Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;
    iget-object v1, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    .line 176
    .local v13, "resources":Landroid/content/res/Resources;
    if-eqz v11, :cond_2

    invoke-virtual {v11}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->watchedToEndCredits()Z

    move-result v1

    if-nez v1, :cond_2

    .line 177
    const-wide/16 v2, 0x0

    const v1, 0x7f0b00b4

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v7, v11, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->assetResource:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v7, v7, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v7, v7, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->sequenceNumber:Ljava/lang/String;

    aput-object v7, v4, v5

    invoke-virtual {v13, v1, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {p0, v11}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->getOnOwnedEpisodeClickAction(Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;)Landroid/content/Intent;

    move-result-object v6

    move-object v1, p0

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->addAction(JLjava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    .line 181
    :cond_2
    if-eqz v12, :cond_0

    .line 182
    iget-object v1, v12, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->assetResource:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v1, v1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v9, v1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->sequenceNumber:Ljava/lang/String;

    .line 183
    .local v9, "episodeNumber":Ljava/lang/String;
    iget-boolean v1, v12, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->purchased:Z

    if-eqz v1, :cond_3

    .line 184
    const-wide/16 v2, 0x1

    const v1, 0x7f0b00b5

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v9, v4, v5

    invoke-virtual {v13, v1, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {p0, v12}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->getOnOwnedEpisodeClickAction(Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;)Landroid/content/Intent;

    move-result-object v6

    move-object v1, p0

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->addAction(JLjava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    goto :goto_0

    .line 186
    :cond_3
    iget-object v1, v12, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->cheapestOffer:Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    if-eqz v1, :cond_0

    .line 187
    new-instance v0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;

    iget-object v1, v12, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->assetResource:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v1, v1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v1, v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    iget-object v3, v12, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->assetResource:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v3, v3, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v2, v3, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->itemId:Ljava/lang/String;

    const/4 v4, 0x0

    const/16 v5, 0xc

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 190
    .local v0, "purchase":Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;
    iget-object v1, v12, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->cheapestOffer:Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    invoke-virtual {v1}, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->isFree()Z

    move-result v1

    if-eqz v1, :cond_4

    const v1, 0x7f0b00b5

    :goto_1
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v9, v3, v4

    invoke-virtual {v13, v1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 192
    .local v6, "title":Ljava/lang/String;
    new-instance v2, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$3;

    const-wide/16 v4, 0x2

    const/4 v7, 0x0

    move-object v3, p0

    move-object v8, v0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$3;-><init>(Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;JLjava/lang/String;Ljava/lang/String;Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;)V

    .line 199
    .local v2, "action":Lcom/google/android/videos/pano/ui/ClickableAction;
    invoke-virtual {p0, v2}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->addAction(Landroid/support/v17/leanback/widget/Action;)V

    goto/16 :goto_0

    .line 190
    .end local v2    # "action":Lcom/google/android/videos/pano/ui/ClickableAction;
    .end local v6    # "title":Ljava/lang/String;
    :cond_4
    const v1, 0x7f0b00b6

    goto :goto_1
.end method

.method private addSeasonPass(Landroid/support/v17/leanback/widget/ArrayObjectAdapter;Lcom/google/wireless/android/video/magma/proto/AssetResource;[Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;Ljava/lang/String;I)V
    .locals 25
    .param p1, "itemArrayObjectAdapter"    # Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
    .param p2, "season"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p3, "episodes"    # [Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;
    .param p4, "posterUri"    # Ljava/lang/String;
    .param p5, "posterSize"    # I

    .prologue
    .line 369
    if-nez p2, :cond_1

    .line 400
    :cond_0
    :goto_0
    return-void

    .line 373
    :cond_1
    const/4 v3, 0x0

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/OfferUtil;->getCheapestOffer(Z[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;)Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    move-result-object v19

    .line 374
    .local v19, "cheapestOffer":Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    if-eqz v19, :cond_4

    const/16 v24, 0x1

    .line 375
    .local v24, "showSeasonPass":Z
    :goto_1
    if-eqz v24, :cond_3

    .line 376
    const/16 v23, 0x1

    .line 377
    .local v23, "ownAllEpisode":Z
    const/16 v22, 0x0

    .local v22, "i":I
    :goto_2
    move-object/from16 v0, p3

    array-length v3, v0

    move/from16 v0, v22

    if-ge v0, v3, :cond_2

    .line 378
    aget-object v3, p3, v22

    iget-boolean v3, v3, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->purchased:Z

    if-nez v3, :cond_5

    .line 379
    const/16 v23, 0x0

    .line 383
    :cond_2
    if-nez v23, :cond_6

    const/16 v24, 0x1

    .line 386
    .end local v22    # "i":I
    .end local v23    # "ownAllEpisode":Z
    :cond_3
    :goto_3
    if-eqz v24, :cond_0

    .line 387
    new-instance v2, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;

    const/4 v3, 0x0

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v4, v4, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v5, v5, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    const/4 v6, 0x0

    const/16 v7, 0xc

    invoke-direct/range {v2 .. v7}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 389
    .local v2, "purchase":Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->context:Landroid/content/Context;

    const v4, 0x7f0b00ab

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 390
    .local v6, "buySeasonString":Ljava/lang/String;
    new-instance v3, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$5;

    const-wide/16 v4, -0x3

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const-wide v14, 0x7fffffffffffffffL

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->viewerRating:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    move-object/from16 v20, v0

    move-object/from16 v4, p0

    move-object/from16 v8, p4

    move/from16 v10, p5

    move/from16 v11, p5

    move-object/from16 v21, v2

    invoke-direct/range {v3 .. v21}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$5;-><init>(Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/content/Intent;ZJIIFLcom/google/android/videos/utils/OfferUtil$CheapestOffer;[Lcom/google/wireless/android/video/magma/proto/ViewerRating;Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 374
    .end local v2    # "purchase":Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;
    .end local v6    # "buySeasonString":Ljava/lang/String;
    .end local v24    # "showSeasonPass":Z
    :cond_4
    const/16 v24, 0x0

    goto :goto_1

    .line 377
    .restart local v22    # "i":I
    .restart local v23    # "ownAllEpisode":Z
    .restart local v24    # "showSeasonPass":Z
    :cond_5
    add-int/lit8 v22, v22, 0x1

    goto :goto_2

    .line 383
    :cond_6
    const/16 v24, 0x0

    goto :goto_3
.end method

.method private anyEpisodesPurchased()Z
    .locals 12

    .prologue
    const/4 v10, 0x0

    .line 237
    invoke-virtual {p0}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->getSeasons()[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v9

    .line 238
    .local v9, "seasons":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    if-nez v9, :cond_1

    .line 253
    :cond_0
    :goto_0
    return v10

    .line 242
    :cond_1
    move-object v0, v9

    .local v0, "arr$":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    move v5, v4

    .end local v0    # "arr$":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .end local v4    # "i$":I
    .end local v6    # "len$":I
    .local v5, "i$":I
    :goto_1
    if-ge v5, v6, :cond_0

    aget-object v8, v0, v5

    .line 243
    .local v8, "season":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    iget-object v11, v8, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v11, v11, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-direct {p0, v11}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->getEpisodes(Ljava/lang/String;)[Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;

    move-result-object v3

    .line 244
    .local v3, "episodes":[Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;
    if-nez v3, :cond_3

    .line 242
    .end local v5    # "i$":I
    :cond_2
    add-int/lit8 v4, v5, 0x1

    .restart local v4    # "i$":I
    move v5, v4

    .end local v4    # "i$":I
    .restart local v5    # "i$":I
    goto :goto_1

    .line 247
    :cond_3
    move-object v1, v3

    .local v1, "arr$":[Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;
    array-length v7, v1

    .local v7, "len$":I
    const/4 v4, 0x0

    .end local v5    # "i$":I
    .restart local v4    # "i$":I
    :goto_2
    if-ge v4, v7, :cond_2

    aget-object v2, v1, v4

    .line 248
    .local v2, "episode":Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;
    iget-boolean v11, v2, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->purchased:Z

    if-eqz v11, :cond_4

    .line 249
    const/4 v10, 0x1

    goto :goto_0

    .line 247
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_2
.end method

.method private getEpisodeListRow(Ljava/lang/String;Ljava/lang/String;)Landroid/support/v17/leanback/widget/ListRow;
    .locals 12
    .param p1, "seasonId"    # Ljava/lang/String;
    .param p2, "seasonName"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    .line 437
    invoke-direct {p0, p1}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->getEpisodes(Ljava/lang/String;)[Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;

    move-result-object v3

    .line 438
    .local v3, "episodes":[Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;
    if-eqz v3, :cond_0

    array-length v0, v3

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v9

    .line 455
    :goto_0
    return-object v0

    .line 442
    :cond_1
    new-instance v1, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    new-instance v0, Lcom/google/android/videos/pano/ui/ItemPresenter;

    iget-object v2, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    invoke-direct {v0, v2}, Lcom/google/android/videos/pano/ui/ItemPresenter;-><init>(Lcom/google/android/videos/async/Requester;)V

    invoke-direct {v1, v0}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/Presenter;)V

    .line 445
    .local v1, "itemArrayObjectAdapter":Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
    invoke-virtual {p0}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->getAsset()Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->context:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/google/android/videos/pano/ui/PanoHelper;->getAssetPoster(Lcom/google/wireless/android/video/magma/proto/AssetResource;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 446
    .local v4, "posterUri":Ljava/lang/String;
    invoke-direct {p0, p1}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->getSeason(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->getPosterHeight(Landroid/content/Context;)I

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->addSeasonPass(Landroid/support/v17/leanback/widget/ArrayObjectAdapter;Lcom/google/wireless/android/video/magma/proto/AssetResource;[Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;Ljava/lang/String;I)V

    .line 449
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    array-length v0, v3

    if-ge v8, v0, :cond_2

    .line 450
    aget-object v6, v3, v8

    .line 451
    .local v6, "episode":Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;
    invoke-direct {p0, v1, v6}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->addEpisode(Landroid/support/v17/leanback/widget/ArrayObjectAdapter;Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;)V

    .line 449
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 454
    .end local v6    # "episode":Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;
    :cond_2
    new-instance v7, Landroid/support/v17/leanback/widget/HeaderItem;

    const-wide/16 v10, 0x3

    invoke-direct {v7, v10, v11, p2, v9}, Landroid/support/v17/leanback/widget/HeaderItem;-><init>(JLjava/lang/String;Ljava/lang/String;)V

    .line 455
    .local v7, "headerItem":Landroid/support/v17/leanback/widget/HeaderItem;
    new-instance v0, Landroid/support/v17/leanback/widget/ListRow;

    invoke-direct {v0, v7, v1}, Landroid/support/v17/leanback/widget/ListRow;-><init>(Landroid/support/v17/leanback/widget/HeaderItem;Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    goto :goto_0
.end method

.method private getEpisodeTitle(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/lang/String;
    .locals 5
    .param p1, "metadata"    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    .prologue
    .line 360
    if-nez p1, :cond_0

    .line 361
    const/4 v0, 0x0

    .line 363
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->context:Landroid/content/Context;

    const v1, 0x7f0b00c6

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->sequenceNumber:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getEpisodes(Ljava/lang/String;)[Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;
    .locals 12
    .param p1, "seasonId"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 590
    iget-object v7, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->cachedEpisodes:Ljava/util/Map;

    invoke-interface {v7, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 591
    iget-object v7, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->cachedEpisodes:Ljava/util/Map;

    invoke-interface {v7, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;

    .line 621
    :goto_0
    return-object v7

    .line 594
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->getSeason(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v6

    .line 595
    .local v6, "season":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    if-eqz v6, :cond_1

    iget-object v7, v6, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    array-length v7, v7

    if-nez v7, :cond_2

    :cond_1
    move-object v7, v8

    .line 596
    goto :goto_0

    .line 599
    :cond_2
    iget-object v7, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->account:Ljava/lang/String;

    invoke-direct {p0, v7, v6}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->getEpisodesAsync(Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/util/ArrayList;

    move-result-object v0

    .line 602
    .local v0, "callbacks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;>;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 604
    .local v2, "episodes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    :try_start_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v3, v7, :cond_4

    .line 605
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/videos/async/SyncCallback;

    const-wide/16 v10, 0x3a98

    invoke-virtual {v7, v10, v11}, Lcom/google/android/videos/async/SyncCallback;->getResponse(J)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    iget-object v4, v7, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 607
    .local v4, "resourceList":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    const/4 v1, 0x0

    .local v1, "e":I
    :goto_2
    array-length v7, v4

    if-ge v1, v7, :cond_3

    .line 608
    aget-object v7, v4, v1

    iget-object v9, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    iget-object v10, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->account:Ljava/lang/String;

    invoke-static {v7, v9, v10}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->create(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/android/videos/store/PurchaseStore;Ljava/lang/String;)Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 607
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 604
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 611
    .end local v1    # "e":I
    .end local v4    # "resourceList":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :cond_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-array v5, v7, [Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;

    .line 612
    .local v5, "result":[Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "result":[Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;
    check-cast v5, [Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;

    .line 613
    .restart local v5    # "result":[Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;
    sget-object v7, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->episodeComparator:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeComparator;

    invoke-static {v5, v7}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 614
    iget-object v7, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->cachedEpisodes:Ljava/util/Map;

    invoke-interface {v7, p1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    move-object v7, v5

    .line 615
    goto :goto_0

    .line 616
    .end local v5    # "result":[Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;
    :catch_0
    move-exception v1

    .line 617
    .local v1, "e":Ljava/util/concurrent/TimeoutException;
    const-string v7, "timeout"

    invoke-static {v7}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .end local v1    # "e":Ljava/util/concurrent/TimeoutException;
    :goto_3
    move-object v7, v8

    .line 621
    goto :goto_0

    .line 618
    :catch_1
    move-exception v1

    .line 619
    .local v1, "e":Ljava/util/concurrent/ExecutionException;
    const-string v7, "Failed to fetch purchases"

    invoke-virtual {v1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v9

    invoke-static {v7, v9}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3
.end method

.method private getEpisodesAsync(Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/util/ArrayList;
    .locals 6
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "season"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/videos/async/SyncCallback",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 565
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 567
    .local v2, "callbacks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;>;>;"
    new-instance v4, Lcom/google/android/videos/api/AssetsRequest$Builder;

    invoke-direct {v4}, Lcom/google/android/videos/api/AssetsRequest$Builder;-><init>()V

    invoke-virtual {v4, p1}, Lcom/google/android/videos/api/AssetsRequest$Builder;->account(Ljava/lang/String;)Lcom/google/android/videos/api/AssetsRequest$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    invoke-virtual {v5, p1}, Lcom/google/android/videos/store/ConfigurationStore;->getPlayCountry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/videos/api/AssetsRequest$Builder;->userCountry(Ljava/lang/String;)Lcom/google/android/videos/api/AssetsRequest$Builder;

    move-result-object v4

    const/16 v5, 0x1f

    invoke-virtual {v4, v5}, Lcom/google/android/videos/api/AssetsRequest$Builder;->addFlags(I)Lcom/google/android/videos/api/AssetsRequest$Builder;

    move-result-object v0

    .line 573
    .local v0, "builder":Lcom/google/android/videos/api/AssetsRequest$Builder;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v4, p2, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    array-length v4, v4

    if-ge v3, v4, :cond_1

    .line 574
    iget-object v4, p2, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    aget-object v4, v4, v3

    invoke-static {v4}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromAssetResourceId(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/videos/api/AssetsRequest$Builder;->maybeAddId(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 575
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v1

    .line 576
    .local v1, "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;>;"
    iget-object v4, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->assetsCachingRequester:Lcom/google/android/videos/async/Requester;

    invoke-virtual {v0}, Lcom/google/android/videos/api/AssetsRequest$Builder;->build()Lcom/google/android/videos/api/AssetsRequest;

    move-result-object v5

    invoke-interface {v4, v5, v1}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 577
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 578
    invoke-virtual {v0}, Lcom/google/android/videos/api/AssetsRequest$Builder;->clearIds()V

    .line 573
    .end local v1    # "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;>;"
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 581
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/videos/api/AssetsRequest$Builder;->getIdCount()I

    move-result v4

    if-lez v4, :cond_2

    .line 582
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v1

    .line 583
    .restart local v1    # "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;>;"
    iget-object v4, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->assetsCachingRequester:Lcom/google/android/videos/async/Requester;

    invoke-virtual {v0}, Lcom/google/android/videos/api/AssetsRequest$Builder;->build()Lcom/google/android/videos/api/AssetsRequest;

    move-result-object v5

    invoke-interface {v4, v5, v1}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 584
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 586
    .end local v1    # "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;>;"
    :cond_2
    return-object v2
.end method

.method private getLastAndNextEpisode()Landroid/util/Pair;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;",
            "Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205
    const/4 v6, 0x0

    .line 206
    .local v6, "lastWatched":Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;
    const/4 v10, 0x0

    .line 208
    .local v10, "nextEpisode":Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;
    invoke-virtual {p0}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->getSeasons()[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v13

    .line 209
    .local v13, "seasons":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    if-nez v13, :cond_0

    .line 210
    const/4 v14, 0x0

    .line 233
    :goto_0
    return-object v14

    .line 212
    :cond_0
    invoke-direct {p0}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->getLastWatchedEpisodeId()Ljava/lang/String;

    move-result-object v7

    .line 213
    .local v7, "lastWatchedId":Ljava/lang/String;
    move-object v0, v13

    .local v0, "arr$":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    array-length v8, v0

    .local v8, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    move v5, v4

    .end local v0    # "arr$":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .end local v4    # "i$":I
    .end local v8    # "len$":I
    .local v5, "i$":I
    :goto_1
    if-ge v5, v8, :cond_6

    aget-object v11, v0, v5

    .line 214
    .local v11, "season":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    iget-object v14, v11, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v12, v14, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    .line 215
    .local v12, "seasonId":Ljava/lang/String;
    invoke-direct {p0, v12}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->getEpisodes(Ljava/lang/String;)[Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;

    move-result-object v3

    .line 216
    .local v3, "episodes":[Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;
    if-nez v3, :cond_1

    .line 217
    const/4 v14, 0x0

    goto :goto_0

    .line 219
    :cond_1
    move-object v1, v3

    .local v1, "arr$":[Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;
    array-length v9, v1

    .local v9, "len$":I
    const/4 v4, 0x0

    .end local v5    # "i$":I
    .restart local v4    # "i$":I
    :goto_2
    if-ge v4, v9, :cond_5

    aget-object v2, v1, v4

    .line 220
    .local v2, "episode":Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;
    if-nez v10, :cond_2

    iget-boolean v14, v2, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->purchased:Z

    if-eqz v14, :cond_2

    .line 221
    move-object v10, v2

    .line 223
    :cond_2
    iget-object v14, v2, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->assetResource:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v14, v14, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v14, v14, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-static {v7, v14}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 224
    move-object v6, v2

    .line 225
    const/4 v10, 0x0

    .line 219
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 226
    :cond_4
    if-eqz v6, :cond_3

    if-nez v10, :cond_3

    .line 227
    move-object v10, v2

    .line 213
    .end local v2    # "episode":Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;
    :cond_5
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    .end local v4    # "i$":I
    .restart local v5    # "i$":I
    goto :goto_1

    .line 233
    .end local v1    # "arr$":[Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;
    .end local v3    # "episodes":[Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;
    .end local v9    # "len$":I
    .end local v11    # "season":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .end local v12    # "seasonId":Ljava/lang/String;
    :cond_6
    invoke-static {v6, v10}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v14

    goto :goto_0
.end method

.method private getLastWatchedEpisodeId()Ljava/lang/String;
    .locals 12

    .prologue
    .line 628
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v2

    .line 629
    .local v2, "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;>;"
    iget-object v6, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    iget-object v7, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->account:Ljava/lang/String;

    sget-object v8, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$Query;->COLUMNS:[Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    iget-object v9, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->itemId:Ljava/lang/String;

    invoke-static {v7, v8, v10, v11, v9}, Lcom/google/android/videos/store/PurchaseRequests;->createMyLastWatchedEpisodeRequest(Ljava/lang/String;[Ljava/lang/String;JLjava/lang/String;)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v7

    invoke-virtual {v6, v7, v2}, Lcom/google/android/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/videos/async/Callback;)V

    .line 632
    const/4 v5, 0x0

    .line 633
    .local v5, "lastWatchedId":Ljava/lang/String;
    const/4 v3, 0x0

    .line 635
    .local v3, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Landroid/database/Cursor;

    move-object v3, v0

    .line 636
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 637
    const/4 v6, 0x1

    invoke-interface {v3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 642
    :cond_0
    if-eqz v3, :cond_1

    .line 643
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 646
    :cond_1
    :goto_0
    return-object v5

    .line 639
    :catch_0
    move-exception v4

    .line 640
    .local v4, "e":Ljava/util/concurrent/ExecutionException;
    :try_start_1
    const-string v6, "Couldn\'t fetch the last watched episode"

    invoke-static {v6}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 642
    if-eqz v3, :cond_1

    .line 643
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 642
    .end local v4    # "e":Ljava/util/concurrent/ExecutionException;
    :catchall_0
    move-exception v6

    if-eqz v3, :cond_2

    .line 643
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v6
.end method

.method private getOnOwnedEpisodeClickAction(Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;)Landroid/content/Intent;
    .locals 6
    .param p1, "episode"    # Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;

    .prologue
    .line 403
    iget-object v0, p1, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->assetResource:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v2, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    .line 404
    .local v2, "episodeId":Ljava/lang/String;
    iget-object v0, p1, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->assetResource:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    .line 407
    .local v3, "seasonId":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->account:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->itemId:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected static getPosterHeight(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 530
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e01b9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    const v1, 0x3fe38e39

    div-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private getSeason(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .locals 4
    .param p1, "seasonId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 535
    invoke-virtual {p0}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->getSeasons()[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v1

    .line 536
    .local v1, "seasons":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    if-nez v1, :cond_1

    .line 544
    :cond_0
    :goto_0
    return-object v2

    .line 539
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v3, v1

    if-ge v0, v3, :cond_0

    .line 540
    aget-object v3, v1, v0

    iget-object v3, v3, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v3, v3, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-static {p1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 541
    aget-object v2, v1, v0

    goto :goto_0

    .line 539
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private loadAllEpisodes()V
    .locals 2

    .prologue
    .line 476
    invoke-virtual {p0}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->getSeasons()[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v0

    .line 477
    .local v0, "seasons":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    if-eqz v0, :cond_0

    .line 478
    iget-object v1, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->account:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->loadEpisodesOfSeasons(Ljava/lang/String;[Lcom/google/wireless/android/video/magma/proto/AssetResource;)V

    .line 480
    :cond_0
    return-void
.end method

.method private loadEpisodesOfSeasons(Ljava/lang/String;[Lcom/google/wireless/android/video/magma/proto/AssetResource;)V
    .locals 6
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "seasons"    # [Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    .line 548
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 549
    .local v0, "callbacks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;>;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, p2

    if-ge v2, v3, :cond_0

    .line 550
    aget-object v3, p2, v2

    invoke-direct {p0, p1, v3}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->getEpisodesAsync(Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 549
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 553
    :cond_0
    const/4 v2, 0x0

    :goto_1
    :try_start_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 554
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/async/SyncCallback;

    const-wide/16 v4, 0x3a98

    invoke-virtual {v3, v4, v5}, Lcom/google/android/videos/async/SyncCallback;->getResponse(J)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 553
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 556
    :catch_0
    move-exception v1

    .line 557
    .local v1, "e":Ljava/util/concurrent/TimeoutException;
    const-string v3, "timeout"

    invoke-static {v3}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 561
    .end local v1    # "e":Ljava/util/concurrent/TimeoutException;
    :cond_1
    :goto_2
    return-void

    .line 558
    :catch_1
    move-exception v1

    .line 559
    .local v1, "e":Ljava/util/concurrent/ExecutionException;
    const-string v3, "Failed to fetch episodes"

    invoke-virtual {v1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method


# virtual methods
.method public addDetailsSectionActions()V
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 134
    invoke-direct {p0}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->loadAllEpisodes()V

    .line 135
    invoke-direct {p0}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->addEpisodeActions()V

    .line 137
    iget-object v1, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 139
    .local v7, "resources":Landroid/content/res/Resources;
    invoke-direct {p0}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->anyEpisodesPurchased()Z

    move-result v1

    if-nez v1, :cond_0

    .line 140
    new-instance v0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$1;

    const-wide/16 v2, 0x6

    const v1, 0x7f0b00b3

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$1;-><init>(Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;JLjava/lang/String;Ljava/lang/String;)V

    .line 149
    .local v0, "action":Lcom/google/android/videos/pano/ui/ClickableAction;
    invoke-virtual {p0, v0}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->addAction(Landroid/support/v17/leanback/widget/Action;)V

    .line 152
    .end local v0    # "action":Lcom/google/android/videos/pano/ui/ClickableAction;
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->isAddedToWishlist()Z

    move-result v6

    .line 153
    .local v6, "addedToWishlist":Z
    if-eqz v6, :cond_1

    const v8, 0x7f0b00b1

    .line 155
    .local v8, "wishlistTitleId":I
    :goto_0
    new-instance v0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$2;

    const-wide/16 v2, 0x0

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$2;-><init>(Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;JLjava/lang/String;Ljava/lang/String;Z)V

    .line 163
    .restart local v0    # "action":Lcom/google/android/videos/pano/ui/ClickableAction;
    invoke-virtual {p0, v0}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->addAction(Landroid/support/v17/leanback/widget/Action;)V

    .line 164
    return-void

    .line 153
    .end local v0    # "action":Lcom/google/android/videos/pano/ui/ClickableAction;
    .end local v8    # "wishlistTitleId":I
    :cond_1
    const v8, 0x7f0b00b0

    goto :goto_0
.end method

.method public getActiveSeason()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 483
    invoke-direct {p0}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->getLastAndNextEpisode()Landroid/util/Pair;

    move-result-object v0

    .line 484
    .local v0, "lastAndNext":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;>;"
    if-nez v0, :cond_1

    .line 494
    :cond_0
    :goto_0
    return-object v3

    .line 487
    :cond_1
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;

    .line 488
    .local v1, "lastWatched":Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;
    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;

    .line 489
    .local v2, "nextEpisode":Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->watchedToEndCredits()Z

    move-result v4

    if-nez v4, :cond_2

    .line 490
    iget-object v3, v1, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->assetResource:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v3, v3, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v3, v3, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    goto :goto_0

    .line 491
    :cond_2
    if-eqz v2, :cond_0

    .line 492
    iget-object v3, v2, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$EpisodeContainer;->assetResource:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v3, v3, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v3, v3, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    goto :goto_0
.end method

.method protected getAssetId(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "showId"    # Ljava/lang/String;

    .prologue
    .line 472
    invoke-static {p1}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromShowId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getRelatedRequest()Lcom/google/android/videos/api/RecommendationsRequest;
    .locals 5

    .prologue
    .line 460
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->relatedRequestFactory:Lcom/google/android/videos/api/RecommendationsRequest$Factory;

    iget-object v1, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->itemId:Ljava/lang/String;

    const/16 v3, 0xf

    const/16 v4, 0x1b

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/videos/api/RecommendationsRequest$Factory;->createForRelatedShows(Ljava/lang/String;Ljava/lang/String;II)Lcom/google/android/videos/api/RecommendationsRequest;

    move-result-object v0

    return-object v0
.end method

.method public getSeasons()[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 498
    invoke-virtual {p0}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->getAsset()Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v0

    .line 499
    .local v0, "asset":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    if-eqz v0, :cond_0

    iget-object v6, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    array-length v6, v6

    const/4 v8, 0x1

    if-ge v6, v8, :cond_1

    :cond_0
    move-object v5, v7

    .line 526
    :goto_0
    return-object v5

    .line 503
    :cond_1
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v2

    .line 504
    .local v2, "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;>;"
    new-instance v6, Lcom/google/android/videos/api/AssetsRequest$Builder;

    invoke-direct {v6}, Lcom/google/android/videos/api/AssetsRequest$Builder;-><init>()V

    iget-object v8, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->account:Ljava/lang/String;

    invoke-virtual {v6, v8}, Lcom/google/android/videos/api/AssetsRequest$Builder;->account(Ljava/lang/String;)Lcom/google/android/videos/api/AssetsRequest$Builder;

    move-result-object v6

    iget-object v8, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    iget-object v9, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->account:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/google/android/videos/store/ConfigurationStore;->getPlayCountry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/google/android/videos/api/AssetsRequest$Builder;->userCountry(Ljava/lang/String;)Lcom/google/android/videos/api/AssetsRequest$Builder;

    move-result-object v6

    const/16 v8, 0x19

    invoke-virtual {v6, v8}, Lcom/google/android/videos/api/AssetsRequest$Builder;->addFlags(I)Lcom/google/android/videos/api/AssetsRequest$Builder;

    move-result-object v1

    .line 509
    .local v1, "builder":Lcom/google/android/videos/api/AssetsRequest$Builder;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    iget-object v6, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    array-length v6, v6

    if-ge v4, v6, :cond_3

    .line 511
    iget-object v6, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    aget-object v6, v6, v4

    invoke-static {v6}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromAssetResourceId(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/google/android/videos/api/AssetsRequest$Builder;->maybeAddId(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 512
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "id too long: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v8, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->itemId:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    move-object v5, v7

    .line 513
    goto :goto_0

    .line 509
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 516
    :cond_3
    iget-object v6, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->assetsCachingRequester:Lcom/google/android/videos/async/Requester;

    invoke-virtual {v1}, Lcom/google/android/videos/api/AssetsRequest$Builder;->build()Lcom/google/android/videos/api/AssetsRequest;

    move-result-object v8

    invoke-interface {v6, v8, v2}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 518
    const-wide/16 v8, 0x3a98

    :try_start_0
    invoke-virtual {v2, v8, v9}, Lcom/google/android/videos/async/SyncCallback;->getResponse(J)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    iget-object v5, v6, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 519
    .local v5, "seasons":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    sget-object v6, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->seasonComparator:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$SeasonComparator;

    invoke-static {v5, v6}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 521
    .end local v5    # "seasons":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :catch_0
    move-exception v3

    .line 522
    .local v3, "e":Ljava/util/concurrent/TimeoutException;
    const-string v6, "timeout"

    invoke-static {v6}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .end local v3    # "e":Ljava/util/concurrent/TimeoutException;
    :goto_2
    move-object v5, v7

    .line 526
    goto :goto_0

    .line 523
    :catch_1
    move-exception v3

    .line 524
    .local v3, "e":Ljava/util/concurrent/ExecutionException;
    const-string v6, "Failed to fetch purchases"

    invoke-virtual {v3}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method protected getWishlistRequest()Lcom/google/android/videos/store/WishlistStore$WishlistRequest;
    .locals 4

    .prologue
    .line 467
    new-instance v0, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    iget-object v1, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->account:Ljava/lang/String;

    const/16 v2, 0x12

    iget-object v3, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->itemId:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    return-object v0
.end method

.method public onAdapterUpdateStart()V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->cachedEpisodes:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 130
    return-void
.end method

.method public onPurchasesUpdated()V
    .locals 0

    .prologue
    .line 125
    return-void
.end method

.method public onWatchTimestampsUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 121
    return-void
.end method

.method public setEpisodeListRows(Ljava/util/ArrayList;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v17/leanback/widget/Row;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 412
    .local p1, "rows":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/support/v17/leanback/widget/Row;>;"
    invoke-virtual {p0}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->getSeasons()[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v8

    .line 413
    .local v8, "seasons":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    invoke-virtual {p0}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->getActiveSeason()Ljava/lang/String;

    move-result-object v0

    .line 414
    .local v0, "activeSeasonId":Ljava/lang/String;
    const/4 v9, 0x0

    iput-object v9, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->lastSeasonListRow:Landroid/support/v17/leanback/widget/ListRow;

    .line 415
    if-eqz v8, :cond_2

    .line 416
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 419
    .local v2, "firstSeasonRowPosition":I
    move v4, v2

    .line 420
    .local v4, "position":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v9, v8

    if-ge v3, v9, :cond_2

    .line 421
    aget-object v9, v8, v3

    iget-object v9, v9, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v6, v9, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    .line 422
    .local v6, "seasonId":Ljava/lang/String;
    aget-object v9, v8, v3

    iget-object v9, v9, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v10, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->context:Landroid/content/Context;

    invoke-static {v9, v10}, Lcom/google/android/videos/pano/ui/PanoHelper;->getSeasonTitle(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    .line 423
    .local v7, "seasonTitle":Ljava/lang/String;
    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 425
    move v4, v2

    .line 427
    :cond_0
    invoke-direct {p0, v6, v7}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->getEpisodeListRow(Ljava/lang/String;Ljava/lang/String;)Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v1

    .line 428
    .local v1, "episodes":Landroid/support/v17/leanback/widget/ListRow;
    if-eqz v1, :cond_1

    .line 429
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "position":I
    .local v5, "position":I
    invoke-virtual {p1, v4, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 430
    iput-object v1, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->lastSeasonListRow:Landroid/support/v17/leanback/widget/ListRow;

    move v4, v5

    .line 420
    .end local v5    # "position":I
    .restart local v4    # "position":I
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 434
    .end local v1    # "episodes":Landroid/support/v17/leanback/widget/ListRow;
    .end local v2    # "firstSeasonRowPosition":I
    .end local v3    # "i":I
    .end local v4    # "position":I
    .end local v6    # "seasonId":Ljava/lang/String;
    .end local v7    # "seasonTitle":Ljava/lang/String;
    :cond_2
    return-void
.end method
