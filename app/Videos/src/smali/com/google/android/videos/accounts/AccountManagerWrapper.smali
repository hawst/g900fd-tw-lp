.class public abstract Lcom/google/android/videos/accounts/AccountManagerWrapper;
.super Ljava/lang/Object;
.source "AccountManagerWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException;,
        Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;
    }
.end annotation


# instance fields
.field protected final accountManager:Landroid/accounts/AccountManager;

.field protected final accountType:Ljava/lang/String;

.field protected final scope:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Landroid/accounts/AccountManager;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "accountManager"    # Landroid/accounts/AccountManager;
    .param p2, "scope"    # Ljava/lang/String;
    .param p3, "accountType"    # Ljava/lang/String;

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountManager;

    iput-object v0, p0, Lcom/google/android/videos/accounts/AccountManagerWrapper;->accountManager:Landroid/accounts/AccountManager;

    .line 59
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/accounts/AccountManagerWrapper;->scope:Ljava/lang/String;

    .line 60
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/accounts/AccountManagerWrapper;->accountType:Ljava/lang/String;

    .line 61
    return-void
.end method

.method private static normalizeAccountName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "accountName"    # Ljava/lang/String;

    .prologue
    .line 195
    invoke-static {p0}, Lcom/google/android/videos/utils/Util;->toLowerInvariant(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 196
    if-eqz p0, :cond_0

    const-string v0, "@googlemail.com"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0xf

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "@gmail.com"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 199
    .end local p0    # "accountName":Ljava/lang/String;
    :cond_0
    return-object p0
.end method

.method private onNoAccountToAuthenticate(Ljava/lang/String;Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;)V
    .locals 4
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "callback"    # Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    .prologue
    .line 114
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    invoke-interface {p2, p1}, Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;->onNotAuthenticated(Ljava/lang/String;)V

    .line 120
    :goto_0
    return-void

    .line 117
    :cond_0
    new-instance v0, Landroid/accounts/AuthenticatorException;

    const-string v1, "User account \'%s\' not found."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/accounts/AuthenticatorException;-><init>(Ljava/lang/String;)V

    invoke-interface {p2, p1, v0}, Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;->onAuthenticationError(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method


# virtual methods
.method public accountExists(Ljava/lang/String;)Z
    .locals 1
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 106
    invoke-virtual {p0, p1}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public addAccount(Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;)V
    .locals 8
    .param p1, "activity"    # Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/accounts/AccountManagerCallback",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "callback":Landroid/accounts/AccountManagerCallback;, "Landroid/accounts/AccountManagerCallback<Landroid/os/Bundle;>;"
    const/4 v3, 0x0

    .line 110
    iget-object v0, p0, Lcom/google/android/videos/accounts/AccountManagerWrapper;->accountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/videos/accounts/AccountManagerWrapper;->accountType:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/accounts/AccountManagerWrapper;->scope:Ljava/lang/String;

    move-object v4, v3

    move-object v5, p1

    move-object v6, p2

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 111
    return-void
.end method

.method public addOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;Z)V
    .locals 2
    .param p1, "listener"    # Landroid/accounts/OnAccountsUpdateListener;
    .param p2, "updateImmediately"    # Z

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/videos/accounts/AccountManagerWrapper;->accountManager:Landroid/accounts/AccountManager;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2}, Landroid/accounts/AccountManager;->addOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;Landroid/os/Handler;Z)V

    .line 74
    return-void
.end method

.method public blockingAuthenticate(Ljava/lang/String;)Z
    .locals 2
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 167
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->blockingGetAuthToken(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    const/4 v1, 0x1

    .line 174
    :goto_0
    return v1

    .line 169
    :catch_0
    move-exception v0

    .line 171
    .local v0, "e":Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException;
    invoke-virtual {v0}, Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-nez v1, :cond_0

    .line 172
    const-string v1, "Authentication failed"

    invoke-static {v1, v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 174
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public blockingGetAuthToken(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "accountName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException;
        }
    .end annotation

    .prologue
    .line 187
    invoke-virtual {p0, p1}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 188
    .local v0, "account":Landroid/accounts/Account;
    if-nez v0, :cond_0

    .line 189
    new-instance v1, Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException;

    const-string v2, "No such account"

    invoke-direct {v1, v2}, Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 191
    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->blockingGetAuthTokenInternal(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected abstract blockingGetAuthTokenInternal(Landroid/accounts/Account;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException;
        }
    .end annotation
.end method

.method public getAccount(Ljava/lang/String;)Landroid/accounts/Account;
    .locals 4
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 89
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 99
    :cond_0
    :goto_0
    return-object v2

    .line 92
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getAccounts()[Landroid/accounts/Account;

    move-result-object v0

    .line 93
    .local v0, "accounts":[Landroid/accounts/Account;
    invoke-static {p1}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->normalizeAccountName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 94
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v3, v0

    if-ge v1, v3, :cond_0

    .line 95
    aget-object v3, v0, v1

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->normalizeAccountName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 96
    aget-object v2, v0, v1

    goto :goto_0

    .line 94
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public getAccountType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/videos/accounts/AccountManagerWrapper;->accountType:Ljava/lang/String;

    return-object v0
.end method

.method public getAccounts()[Landroid/accounts/Account;
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/videos/accounts/AccountManagerWrapper;->accountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/videos/accounts/AccountManagerWrapper;->accountType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method public getAuthToken(Ljava/lang/String;Landroid/app/Activity;Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;)V
    .locals 1
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "activity"    # Landroid/app/Activity;
    .param p3, "callback"    # Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    .prologue
    .line 132
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    invoke-virtual {p0, p1}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 135
    .local v0, "account":Landroid/accounts/Account;
    if-nez v0, :cond_0

    .line 136
    invoke-direct {p0, p1, p3}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->onNoAccountToAuthenticate(Ljava/lang/String;Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;)V

    .line 140
    :goto_0
    return-void

    .line 138
    :cond_0
    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getAuthTokenInternal(Landroid/accounts/Account;Landroid/app/Activity;Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;)V

    goto :goto_0
.end method

.method protected abstract getAuthTokenInternal(Landroid/accounts/Account;Landroid/app/Activity;Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;)V
.end method

.method public getScope()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/videos/accounts/AccountManagerWrapper;->scope:Ljava/lang/String;

    return-object v0
.end method

.method public abstract invalidateAuthToken(Ljava/lang/String;)V
.end method

.method public onActivityResult(IILandroid/content/Intent;)Z
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 156
    const/4 v0, 0x0

    return v0
.end method

.method public removeOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/accounts/OnAccountsUpdateListener;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/videos/accounts/AccountManagerWrapper;->accountManager:Landroid/accounts/AccountManager;

    invoke-virtual {v0, p1}, Landroid/accounts/AccountManager;->removeOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;)V

    .line 78
    return-void
.end method
