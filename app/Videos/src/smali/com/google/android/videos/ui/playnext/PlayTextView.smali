.class public Lcom/google/android/videos/ui/playnext/PlayTextView;
.super Landroid/widget/TextView;
.source "PlayTextView.java"


# instance fields
.field private final ellipsizeFadeOutDrawable:Landroid/graphics/drawable/GradientDrawable;

.field private isEllipsized:Z

.field private final lastLineFadeOutSize:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/ui/playnext/PlayTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v7, 0x0

    .line 38
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    sget-object v3, Lcom/google/android/videos/R$styleable;->PlayTextView:[I

    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 41
    .local v2, "viewAttrs":Landroid/content/res/TypedArray;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 42
    .local v1, "res":Landroid/content/res/Resources;
    const/4 v3, 0x5

    const v4, 0x7f0a00d6

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 44
    .local v0, "overdrawColor":I
    const/4 v3, 0x6

    const v4, 0x7f0e017f

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/videos/ui/playnext/PlayTextView;->lastLineFadeOutSize:I

    .line 46
    new-instance v3, Landroid/graphics/drawable/GradientDrawable;

    sget-object v4, Landroid/graphics/drawable/GradientDrawable$Orientation;->LEFT_RIGHT:Landroid/graphics/drawable/GradientDrawable$Orientation;

    const/4 v5, 0x2

    new-array v5, v5, [I

    const v6, 0xffffff

    and-int/2addr v6, v0

    aput v6, v5, v7

    const/4 v6, 0x1

    aput v0, v5, v6

    invoke-direct {v3, v4, v5}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    iput-object v3, p0, Lcom/google/android/videos/ui/playnext/PlayTextView;->ellipsizeFadeOutDrawable:Landroid/graphics/drawable/GradientDrawable;

    .line 48
    iput-boolean v7, p0, Lcom/google/android/videos/ui/playnext/PlayTextView;->isEllipsized:Z

    .line 50
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 51
    return-void
.end method


# virtual methods
.method public isEllipsized()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/android/videos/ui/playnext/PlayTextView;->isEllipsized:Z

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 59
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 61
    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/PlayTextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    .line 62
    .local v2, "layout":Landroid/text/Layout;
    if-nez v2, :cond_1

    .line 84
    :cond_0
    :goto_0
    return-void

    .line 66
    :cond_1
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/google/android/videos/ui/playnext/PlayTextView;->isEllipsized:Z

    .line 67
    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/PlayTextView;->getHeight()I

    move-result v1

    .line 68
    .local v1, "height":I
    const/4 v3, 0x1

    .local v3, "line":I
    :goto_1
    invoke-virtual {v2}, Landroid/text/Layout;->getLineCount()I

    move-result v5

    if-ge v3, v5, :cond_0

    .line 69
    invoke-virtual {v2, v3}, Landroid/text/Layout;->getLineBottom(I)I

    move-result v5

    if-le v5, v1, :cond_2

    .line 70
    add-int/lit8 v5, v3, -0x1

    invoke-virtual {v2, v5}, Landroid/text/Layout;->getLineWidth(I)F

    move-result v5

    float-to-int v4, v5

    .line 71
    .local v4, "prevLineEnd":I
    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/PlayTextView;->getPaddingLeft()I

    move-result v5

    add-int v0, v5, v4

    .line 73
    .local v0, "fadeOutRight":I
    iget-object v5, p0, Lcom/google/android/videos/ui/playnext/PlayTextView;->ellipsizeFadeOutDrawable:Landroid/graphics/drawable/GradientDrawable;

    iget v6, p0, Lcom/google/android/videos/ui/playnext/PlayTextView;->lastLineFadeOutSize:I

    sub-int v6, v0, v6

    add-int/lit8 v7, v3, -0x1

    invoke-virtual {v2, v7}, Landroid/text/Layout;->getLineTop(I)I

    move-result v7

    add-int/lit8 v8, v3, -0x1

    invoke-virtual {v2, v8}, Landroid/text/Layout;->getLineBottom(I)I

    move-result v8

    invoke-virtual {v5, v6, v7, v0, v8}, Landroid/graphics/drawable/GradientDrawable;->setBounds(IIII)V

    .line 78
    iget-object v5, p0, Lcom/google/android/videos/ui/playnext/PlayTextView;->ellipsizeFadeOutDrawable:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v5, p1}, Landroid/graphics/drawable/GradientDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 79
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/videos/ui/playnext/PlayTextView;->isEllipsized:Z

    goto :goto_0

    .line 68
    .end local v0    # "fadeOutRight":I
    .end local v4    # "prevLineEnd":I
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method
