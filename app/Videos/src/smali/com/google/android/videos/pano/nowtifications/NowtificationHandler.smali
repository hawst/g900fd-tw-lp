.class public Lcom/google/android/videos/pano/nowtifications/NowtificationHandler;
.super Ljava/lang/Object;
.source "NowtificationHandler.java"


# instance fields
.field private final nowtificationsRepository:Lcom/google/android/repolib/repositories/Repository;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/repositories/Repository",
            "<",
            "Lcom/google/android/videos/pano/nowtifications/Nowtification;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 27
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 48
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-static/range {p1 .. p1}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v22

    .line 50
    .local v22, "videosGlobals":Lcom/google/android/videos/VideosGlobals;
    invoke-virtual/range {v22 .. v22}, Lcom/google/android/videos/VideosGlobals;->getBitmapRequesters()Lcom/google/android/videos/bitmap/BitmapRequesters;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/videos/bitmap/BitmapRequesters;->getSyncBitmapRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v16

    .line 52
    .local v16, "syncBitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Landroid/graphics/Bitmap;>;"
    invoke-virtual/range {v22 .. v22}, Lcom/google/android/videos/VideosGlobals;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v4

    .line 53
    .local v4, "signInManager":Lcom/google/android/videos/accounts/SignInManager;
    invoke-virtual/range {v22 .. v22}, Lcom/google/android/videos/VideosGlobals;->getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v5

    .line 54
    .local v5, "configurationStore":Lcom/google/android/videos/store/ConfigurationStore;
    invoke-virtual/range {v22 .. v22}, Lcom/google/android/videos/VideosGlobals;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v6

    .line 55
    .local v6, "database":Lcom/google/android/videos/store/Database;
    invoke-virtual/range {v22 .. v22}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStore()Lcom/google/android/videos/store/PurchaseStore;

    move-result-object v7

    .line 56
    .local v7, "purchaseStore":Lcom/google/android/videos/store/PurchaseStore;
    invoke-virtual/range {v22 .. v22}, Lcom/google/android/videos/VideosGlobals;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/videos/api/ApiRequesters;->getVideoCollectionGetRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v10

    .line 58
    .local v10, "videoCollectionGetRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/VideoCollectionGetRequest;Lcom/google/wireless/android/video/magma/proto/VideoCollectionGetResponse;>;"
    invoke-virtual/range {v22 .. v22}, Lcom/google/android/videos/VideosGlobals;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v8

    .line 59
    .local v8, "networkStatus":Lcom/google/android/videos/utils/NetworkStatus;
    invoke-virtual/range {v22 .. v22}, Lcom/google/android/videos/VideosGlobals;->getDataSources()Lcom/google/android/videos/pano/datasource/DataSources;

    move-result-object v11

    .line 60
    .local v11, "dataSources":Lcom/google/android/videos/pano/datasource/DataSources;
    invoke-virtual/range {v22 .. v22}, Lcom/google/android/videos/VideosGlobals;->getLocaleObservable()Lcom/google/android/repolib/observers/Observable;

    move-result-object v9

    .line 62
    .local v9, "localObservable":Lcom/google/android/repolib/observers/Observable;
    invoke-virtual/range {v22 .. v22}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/videos/Config;->suggestionsEnabled()Z

    move-result v15

    .line 64
    .local v15, "suggestionsEnabled":Z
    if-eqz v15, :cond_0

    const/16 v2, 0x32

    move-object/from16 v3, p1

    invoke-static/range {v2 .. v10}, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->createTopShowsRepository(ILandroid/content/Context;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/repolib/observers/Observable;Lcom/google/android/videos/async/Requester;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v20

    .line 70
    .local v20, "topShowsRepository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<Lcom/google/android/videos/pano/model/VideoItem;>;"
    :goto_0
    if-eqz v15, :cond_1

    const/16 v2, 0x32

    move-object/from16 v3, p1

    invoke-static/range {v2 .. v10}, Lcom/google/android/videos/pano/repositories/StaticVideoCollectionsRepository;->createTopMoviesRepository(ILandroid/content/Context;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/repolib/observers/Observable;Lcom/google/android/videos/async/Requester;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v18

    .line 76
    .local v18, "topMoviesRepository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<Lcom/google/android/videos/pano/model/VideoItem;>;"
    :goto_1
    iget-object v2, v11, Lcom/google/android/videos/pano/datasource/DataSources;->watchNowDataSource:Lcom/google/android/videos/pano/datasource/BaseDataSource;

    invoke-static {v2}, Lcom/google/android/videos/pano/repositories/DataSourceRepository;->dataSourceRepository(Lcom/google/android/videos/pano/datasource/BaseDataSource;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/videos/pano/nowtifications/NowtificationHandler;->createWatchNowRepository(Landroid/content/Context;Lcom/google/android/repolib/repositories/Repository;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v25

    .line 78
    .local v25, "watchNowNowtificationRepository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<Lcom/google/android/repolib/common/Entity;>;"
    const v2, 0x7f0b00a2

    invoke-static/range {v25 .. v25}, Lcom/google/android/repolib/common/EntityNotInRepositoryPredicate;->entityNotInRepositoryPredicate(Lcom/google/android/repolib/repositories/Repository;)Lcom/google/android/repolib/common/Predicate;

    move-result-object v3

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/videos/pano/nowtifications/NowtificationHandler;->createRepository(Landroid/content/Context;Lcom/google/android/repolib/repositories/Repository;ILcom/google/android/repolib/common/Predicate;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v17

    .line 81
    .local v17, "topMoviesNowtificationRepository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<Lcom/google/android/repolib/common/Entity;>;"
    const/4 v2, 0x2

    move-object/from16 v0, v17

    invoke-static {v0, v2}, Lcom/google/android/repolib/repositories/LimitingRepository;->limitingRepository(Lcom/google/android/repolib/repositories/Repository;I)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Lcom/google/android/repolib/repositories/Repository;

    move-object/from16 v0, v25

    invoke-static {v0, v2, v3}, Lcom/google/android/repolib/repositories/ConcatenatingRepository;->concatenatingRepository(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/repositories/Repository;[Lcom/google/android/repolib/repositories/Repository;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v24

    .line 84
    .local v24, "watchNowAndTopMoviesConcatenatedRepositories":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<Lcom/google/android/repolib/common/Entity;>;"
    const v2, 0x7f0b00a3

    invoke-static/range {v24 .. v24}, Lcom/google/android/repolib/common/EntityNotInRepositoryPredicate;->entityNotInRepositoryPredicate(Lcom/google/android/repolib/repositories/Repository;)Lcom/google/android/repolib/common/Predicate;

    move-result-object v3

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/videos/pano/nowtifications/NowtificationHandler;->createRepository(Landroid/content/Context;Lcom/google/android/repolib/repositories/Repository;ILcom/google/android/repolib/common/Predicate;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v19

    .line 87
    .local v19, "topShowsNowtificationRepository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<Lcom/google/android/repolib/common/Entity;>;"
    const/4 v2, 0x2

    move-object/from16 v0, v19

    invoke-static {v0, v2}, Lcom/google/android/repolib/repositories/LimitingRepository;->limitingRepository(Lcom/google/android/repolib/repositories/Repository;I)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Lcom/google/android/repolib/repositories/Repository;

    move-object/from16 v0, v24

    invoke-static {v0, v2, v3}, Lcom/google/android/repolib/repositories/ConcatenatingRepository;->concatenatingRepository(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/repositories/Repository;[Lcom/google/android/repolib/repositories/Repository;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v23

    .line 90
    .local v23, "watchNowAndTopMoviesAndTopShowsConcatenatedRepositories":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<Lcom/google/android/repolib/common/Entity;>;"
    iget-object v2, v11, Lcom/google/android/videos/pano/datasource/DataSources;->newEpisodesDataSource:Lcom/google/android/videos/pano/datasource/BaseDataSource;

    invoke-static {v2}, Lcom/google/android/videos/pano/repositories/DataSourceRepository;->dataSourceRepository(Lcom/google/android/videos/pano/datasource/BaseDataSource;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static/range {v23 .. v23}, Lcom/google/android/repolib/common/EntityNotInRepositoryPredicate;->entityNotInRepositoryPredicate(Lcom/google/android/repolib/repositories/Repository;)Lcom/google/android/repolib/common/Predicate;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-static {v0, v2, v3, v1}, Lcom/google/android/videos/pano/nowtifications/NowtificationHandler;->createRepository(Landroid/content/Context;Lcom/google/android/repolib/repositories/Repository;ILcom/google/android/repolib/common/Predicate;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v12

    .line 94
    .local v12, "newEpisodesRepository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<Lcom/google/android/repolib/common/Entity;>;"
    new-instance v2, Lcom/google/android/videos/pano/nowtifications/NowtificationFromItemFactory;

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-direct {v2, v0, v1}, Lcom/google/android/videos/pano/nowtifications/NowtificationFromItemFactory;-><init>(Landroid/content/Context;Lcom/google/android/videos/async/Requester;)V

    invoke-static {v2}, Lcom/google/android/repolib/common/ClassMappingFactory;->classMappingFactory(Lcom/google/android/repolib/common/Factory;)Lcom/google/android/repolib/common/ClassMappingFactory;

    move-result-object v14

    .line 97
    .local v14, "nowtificationFactories":Lcom/google/android/repolib/common/ClassMappingFactory;, "Lcom/google/android/repolib/common/ClassMappingFactory<Lcom/google/android/videos/pano/nowtifications/Nowtification;Lcom/google/android/repolib/common/Entity;>;"
    const-class v2, Lcom/google/android/videos/pano/model/GroupedPromoItem;

    new-instance v3, Lcom/google/android/videos/pano/nowtifications/NowtificationFromPromoItemFactory;

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-direct {v3, v0, v1}, Lcom/google/android/videos/pano/nowtifications/NowtificationFromPromoItemFactory;-><init>(Landroid/content/Context;Lcom/google/android/videos/async/Requester;)V

    invoke-virtual {v14, v2, v3}, Lcom/google/android/repolib/common/ClassMappingFactory;->bind(Ljava/lang/Class;Lcom/google/android/repolib/common/Factory;)V

    .line 99
    invoke-virtual/range {v22 .. v22}, Lcom/google/android/videos/VideosGlobals;->getNetworkExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v12, v3}, Lcom/google/android/repolib/repositories/LimitingRepository;->limitingRepository(Lcom/google/android/repolib/repositories/Repository;I)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v3

    const/16 v26, 0x0

    move/from16 v0, v26

    new-array v0, v0, [Lcom/google/android/repolib/repositories/Repository;

    move-object/from16 v26, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    invoke-static {v0, v3, v1}, Lcom/google/android/repolib/repositories/ConcatenatingRepository;->concatenatingRepository(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/repositories/Repository;[Lcom/google/android/repolib/repositories/Repository;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v3

    invoke-static {v2, v3, v14}, Lcom/google/android/repolib/repositories/TransformingRepository;->asyncTransformingRepository(Ljava/util/concurrent/Executor;Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/common/Factory;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/pano/nowtifications/NowtificationHandler;->nowtificationsRepository:Lcom/google/android/repolib/repositories/Repository;

    .line 105
    new-instance v13, Lcom/google/android/videos/pano/nowtifications/NowtificationDispatcher;

    move-object/from16 v0, p1

    invoke-direct {v13, v0}, Lcom/google/android/videos/pano/nowtifications/NowtificationDispatcher;-><init>(Landroid/content/Context;)V

    .line 106
    .local v13, "nowtificationDispatcher":Lcom/google/android/videos/pano/nowtifications/NowtificationDispatcher;
    const/16 v2, 0x2710

    invoke-static {v2}, Lcom/google/android/repolib/observers/LowPassFilterUpdateDispatcher;->lowPassFilterUpdateDispatcher(I)Lcom/google/android/repolib/observers/UpdateDispatcher;

    move-result-object v21

    .line 108
    .local v21, "updateDispatcher":Lcom/google/android/repolib/observers/UpdateDispatcher;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/pano/nowtifications/NowtificationHandler;->nowtificationsRepository:Lcom/google/android/repolib/repositories/Repository;

    move-object/from16 v0, v21

    invoke-interface {v2, v0}, Lcom/google/android/repolib/repositories/Repository;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 109
    new-instance v2, Lcom/google/android/videos/pano/nowtifications/NowtificationHandler$1;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v13}, Lcom/google/android/videos/pano/nowtifications/NowtificationHandler$1;-><init>(Lcom/google/android/videos/pano/nowtifications/NowtificationHandler;Lcom/google/android/videos/pano/nowtifications/NowtificationDispatcher;)V

    move-object/from16 v0, v21

    invoke-interface {v0, v2}, Lcom/google/android/repolib/observers/UpdateDispatcher;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 116
    return-void

    .line 64
    .end local v12    # "newEpisodesRepository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<Lcom/google/android/repolib/common/Entity;>;"
    .end local v13    # "nowtificationDispatcher":Lcom/google/android/videos/pano/nowtifications/NowtificationDispatcher;
    .end local v14    # "nowtificationFactories":Lcom/google/android/repolib/common/ClassMappingFactory;, "Lcom/google/android/repolib/common/ClassMappingFactory<Lcom/google/android/videos/pano/nowtifications/Nowtification;Lcom/google/android/repolib/common/Entity;>;"
    .end local v17    # "topMoviesNowtificationRepository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<Lcom/google/android/repolib/common/Entity;>;"
    .end local v18    # "topMoviesRepository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<Lcom/google/android/videos/pano/model/VideoItem;>;"
    .end local v19    # "topShowsNowtificationRepository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<Lcom/google/android/repolib/common/Entity;>;"
    .end local v20    # "topShowsRepository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<Lcom/google/android/videos/pano/model/VideoItem;>;"
    .end local v21    # "updateDispatcher":Lcom/google/android/repolib/observers/UpdateDispatcher;
    .end local v23    # "watchNowAndTopMoviesAndTopShowsConcatenatedRepositories":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<Lcom/google/android/repolib/common/Entity;>;"
    .end local v24    # "watchNowAndTopMoviesConcatenatedRepositories":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<Lcom/google/android/repolib/common/Entity;>;"
    .end local v25    # "watchNowNowtificationRepository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<Lcom/google/android/repolib/common/Entity;>;"
    :cond_0
    invoke-static {}, Lcom/google/android/repolib/repositories/EmptyRepository;->emptyRepository()Lcom/google/android/repolib/repositories/Repository;

    move-result-object v20

    goto/16 :goto_0

    .line 70
    .restart local v20    # "topShowsRepository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<Lcom/google/android/videos/pano/model/VideoItem;>;"
    :cond_1
    invoke-static {}, Lcom/google/android/repolib/repositories/EmptyRepository;->emptyRepository()Lcom/google/android/repolib/repositories/Repository;

    move-result-object v18

    goto/16 :goto_1
.end method

.method static synthetic access$000(Lcom/google/android/videos/pano/nowtifications/NowtificationHandler;)Lcom/google/android/repolib/repositories/Repository;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/nowtifications/NowtificationHandler;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/videos/pano/nowtifications/NowtificationHandler;->nowtificationsRepository:Lcom/google/android/repolib/repositories/Repository;

    return-object v0
.end method

.method private static createRepository(Landroid/content/Context;Lcom/google/android/repolib/repositories/Repository;ILcom/google/android/repolib/common/Predicate;)Lcom/google/android/repolib/repositories/Repository;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p2, "groupId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/repolib/repositories/Repository",
            "<",
            "Lcom/google/android/videos/pano/model/VideoItem;",
            ">;I",
            "Lcom/google/android/repolib/common/Predicate",
            "<",
            "Lcom/google/android/repolib/common/Entity;",
            ">;)",
            "Lcom/google/android/repolib/repositories/Repository",
            "<",
            "Lcom/google/android/repolib/common/Entity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 132
    .local p1, "repository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<Lcom/google/android/videos/pano/model/VideoItem;>;"
    .local p3, "predicate":Lcom/google/android/repolib/common/Predicate;, "Lcom/google/android/repolib/common/Predicate<Lcom/google/android/repolib/common/Entity;>;"
    new-instance v0, Lcom/google/android/videos/pano/model/GroupedVideoItem$GroupedVideoItemFactory;

    invoke-direct {v0, p0, p2}, Lcom/google/android/videos/pano/model/GroupedVideoItem$GroupedVideoItemFactory;-><init>(Landroid/content/Context;I)V

    invoke-static {p1, v0}, Lcom/google/android/repolib/repositories/TransformingRepository;->transformingRepository(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/common/Factory;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/google/android/repolib/repositories/FilteringRepository;->filteringRepository(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/common/Predicate;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v0

    return-object v0
.end method

.method private static createWatchNowRepository(Landroid/content/Context;Lcom/google/android/repolib/repositories/Repository;)Lcom/google/android/repolib/repositories/Repository;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/repolib/repositories/Repository",
            "<",
            "Lcom/google/android/videos/pano/model/VideoItem;",
            ">;)",
            "Lcom/google/android/repolib/repositories/Repository",
            "<",
            "Lcom/google/android/repolib/common/Entity;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "repository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<Lcom/google/android/videos/pano/model/VideoItem;>;"
    const v3, 0x7f0b0096

    .line 120
    new-instance v1, Lcom/google/android/videos/pano/model/GroupedVideoItem$GroupedVideoItemFactory;

    invoke-direct {v1, p0, v3}, Lcom/google/android/videos/pano/model/GroupedVideoItem$GroupedVideoItemFactory;-><init>(Landroid/content/Context;I)V

    invoke-static {v1}, Lcom/google/android/repolib/common/ClassMappingFactory;->classMappingFactory(Lcom/google/android/repolib/common/Factory;)Lcom/google/android/repolib/common/ClassMappingFactory;

    move-result-object v0

    .line 123
    .local v0, "groupItemFactories":Lcom/google/android/repolib/common/ClassMappingFactory;, "Lcom/google/android/repolib/common/ClassMappingFactory<Lcom/google/android/repolib/common/Entity;Lcom/google/android/videos/pano/model/VideoItem;>;"
    const-class v1, Lcom/google/android/videos/pano/model/PromoItem;

    new-instance v2, Lcom/google/android/videos/pano/model/GroupedPromoItem$GroupedPromoItemFactory;

    invoke-direct {v2, p0, v3}, Lcom/google/android/videos/pano/model/GroupedPromoItem$GroupedPromoItemFactory;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/repolib/common/ClassMappingFactory;->bind(Ljava/lang/Class;Lcom/google/android/repolib/common/Factory;)V

    .line 125
    invoke-static {p1, v0}, Lcom/google/android/repolib/repositories/TransformingRepository;->transformingRepository(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/common/Factory;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/google/android/repolib/repositories/LimitingRepository;->limitingRepository(Lcom/google/android/repolib/repositories/Repository;I)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v1

    return-object v1
.end method
