.class public Lcom/google/android/videos/tagging/SuggestionGridLayout;
.super Landroid/view/ViewGroup;
.source "SuggestionGridLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/tagging/SuggestionGridLayout$1;,
        Lcom/google/android/videos/tagging/SuggestionGridLayout$SimpleGridItem;,
        Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;,
        Lcom/google/android/videos/tagging/SuggestionGridLayout$SwipeCallback;,
        Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;,
        Lcom/google/android/videos/tagging/SuggestionGridLayout$OnDismissListener;
    }
.end annotation


# instance fields
.field private final mAnimatingViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final mChangeAppearingAnimator:Landroid/animation/Animator;

.field private mColCount:I

.field private mColWidth:I

.field private mContentWidth:I

.field private final mGridItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mHitRect:Landroid/graphics/Rect;

.field private final mHorizontalItemMargin:I

.field private mIsDragging:Z

.field private mItemBottoms:[I

.field private final mLayoutTransition:Landroid/animation/LayoutTransition;

.field private mMaxColumnWidth:I

.field private mOnDismissListener:Lcom/google/android/videos/tagging/SuggestionGridLayout$OnDismissListener;

.field mReinitStackBitmap:Z

.field private final mSwiper:Lcom/google/android/videos/tagging/SwipeHelper;

.field private final mVerticalItemMargin:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 63
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/tagging/SuggestionGridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 67
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/videos/tagging/SuggestionGridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 71
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iput-object v4, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mHitRect:Landroid/graphics/Rect;

    .line 45
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    .line 46
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    .line 48
    iput-boolean v7, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mReinitStackBitmap:Z

    .line 59
    iput-boolean v7, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mIsDragging:Z

    .line 73
    sget-object v4, Lcom/google/android/videos/R$styleable;->SuggestionGridLayout:[I

    invoke-virtual {p1, p2, v4, p3, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 75
    .local v0, "a":Landroid/content/res/TypedArray;
    const v4, 0x7fffffff

    invoke-virtual {v0, v5, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    iput v4, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mMaxColumnWidth:I

    .line 77
    invoke-virtual {v0, v7, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    iput v4, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mColCount:I

    .line 78
    iget v4, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mColCount:I

    new-array v4, v4, [I

    iput-object v4, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mItemBottoms:[I

    .line 79
    const/4 v4, 0x3

    invoke-virtual {v0, v4, v7}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    iput v4, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mVerticalItemMargin:I

    .line 81
    const/4 v4, 0x2

    invoke-virtual {v0, v4, v7}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    iput v4, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mHorizontalItemMargin:I

    .line 83
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 85
    invoke-virtual {p0, v7}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->setClipToPadding(Z)V

    .line 86
    invoke-virtual {p0, v7}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->setClipChildren(Z)V

    .line 87
    invoke-virtual {p0, v5}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->setChildrenDrawingOrderEnabled(Z)V

    .line 89
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mLayoutTransition:Landroid/animation/LayoutTransition;

    .line 90
    iget-object v4, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mLayoutTransition:Landroid/animation/LayoutTransition;

    if-eqz v4, :cond_0

    .line 91
    iget-object v4, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mLayoutTransition:Landroid/animation/LayoutTransition;

    invoke-direct {p0, v4}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->configureTransition(Landroid/animation/LayoutTransition;)V

    .line 92
    iget-object v4, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mLayoutTransition:Landroid/animation/LayoutTransition;

    invoke-virtual {v4, v7}, Landroid/animation/LayoutTransition;->getAnimator(I)Landroid/animation/Animator;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mChangeAppearingAnimator:Landroid/animation/Animator;

    .line 97
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v1, v4, Landroid/util/DisplayMetrics;->density:F

    .line 98
    .local v1, "density":F
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v3

    .line 99
    .local v3, "vc":Landroid/view/ViewConfiguration;
    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v2

    .line 101
    .local v2, "pagingSlop":I
    new-instance v4, Lcom/google/android/videos/tagging/SwipeHelper;

    new-instance v5, Lcom/google/android/videos/tagging/SuggestionGridLayout$SwipeCallback;

    invoke-direct {v5, p0, v6}, Lcom/google/android/videos/tagging/SuggestionGridLayout$SwipeCallback;-><init>(Lcom/google/android/videos/tagging/SuggestionGridLayout;Lcom/google/android/videos/tagging/SuggestionGridLayout$1;)V

    int-to-float v6, v2

    invoke-direct {v4, v7, v5, v1, v6}, Lcom/google/android/videos/tagging/SwipeHelper;-><init>(ILcom/google/android/videos/tagging/SwipeHelper$Callback;FF)V

    iput-object v4, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mSwiper:Lcom/google/android/videos/tagging/SwipeHelper;

    .line 102
    return-void

    .line 94
    .end local v1    # "density":F
    .end local v2    # "pagingSlop":I
    .end local v3    # "vc":Landroid/view/ViewConfiguration;
    :cond_0
    iput-object v6, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mChangeAppearingAnimator:Landroid/animation/Animator;

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/google/android/videos/tagging/SuggestionGridLayout;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/SuggestionGridLayout;

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mIsDragging:Z

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/videos/tagging/SuggestionGridLayout;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/tagging/SuggestionGridLayout;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mIsDragging:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/videos/tagging/SuggestionGridLayout;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/SuggestionGridLayout;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mHitRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/tagging/SuggestionGridLayout;Landroid/view/View;)Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/SuggestionGridLayout;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/videos/tagging/SuggestionGridLayout;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/SuggestionGridLayout;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$501(Lcom/google/android/videos/tagging/SuggestionGridLayout;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/tagging/SuggestionGridLayout;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 37
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/videos/tagging/SuggestionGridLayout;)Lcom/google/android/videos/tagging/SuggestionGridLayout$OnDismissListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/SuggestionGridLayout;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mOnDismissListener:Lcom/google/android/videos/tagging/SuggestionGridLayout$OnDismissListener;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/videos/tagging/SuggestionGridLayout;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/SuggestionGridLayout;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method private addNewCardsToDeal(Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;)V
    .locals 4
    .param p1, "i"    # Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;

    .prologue
    .line 128
    invoke-interface {p1}, Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;->getViews()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 131
    .local v1, "v":Landroid/view/View;
    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    goto :goto_0

    .line 133
    .end local v1    # "v":Landroid/view/View;
    :cond_0
    return-void
.end method

.method private configureTransition(Landroid/animation/LayoutTransition;)V
    .locals 7
    .param p1, "transition"    # Landroid/animation/LayoutTransition;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x2

    .line 105
    const-string v1, "translationY"

    new-array v2, v5, [F

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v4, v4

    aput v4, v2, v3

    const/4 v3, 0x1

    const/4 v4, 0x0

    aput v4, v2, v3

    invoke-static {v6, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 107
    .local v0, "appearingAnimator":Landroid/animation/ObjectAnimator;
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 108
    invoke-virtual {p1, v5, v0}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    .line 109
    const-wide/16 v2, 0x0

    invoke-virtual {p1, v5, v2, v3}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 110
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v6}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    .line 111
    return-void
.end method

.method private getGridItemForView(Landroid/view/View;)Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 420
    iget-object v3, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 421
    .local v0, "childCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 422
    iget-object v3, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;

    .line 423
    .local v2, "item":Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;
    invoke-interface {v2}, Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;->getViews()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 427
    .end local v2    # "item":Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;
    :goto_1
    return-object v2

    .line 421
    .restart local v2    # "item":Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 427
    .end local v2    # "item":Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private setupLayoutParams(Landroid/view/View;I)Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "column"    # I

    .prologue
    .line 347
    iget v2, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mColCount:I

    if-lt p2, v2, :cond_0

    .line 348
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Column exceeds column count."

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 350
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 351
    .local v0, "lp":Landroid/view/ViewGroup$LayoutParams;
    if-nez v0, :cond_1

    .line 352
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 354
    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v2

    if-eqz v2, :cond_4

    move-object v2, v0

    :goto_0
    check-cast v2, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;

    move-object v1, v2

    check-cast v1, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;

    .line 355
    .local v1, "sglp":Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;
    const/4 v2, -0x2

    if-eq p2, v2, :cond_2

    .line 356
    iput p2, v1, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->column:I

    .line 358
    :cond_2
    iget-boolean v2, v1, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->noPadding:Z

    if-eqz v2, :cond_3

    iget v2, v1, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->column:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_3

    .line 359
    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->noPadding:Z

    .line 360
    const-string v2, "SGL: only spanAllColumns views can have no padding"

    invoke-static {v2}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 362
    :cond_3
    invoke-virtual {p1, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 363
    return-object v1

    .line 354
    .end local v1    # "sglp":Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;
    :cond_4
    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    goto :goto_0
.end method


# virtual methods
.method public addView(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 316
    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->addView(Landroid/view/View;I)V

    .line 317
    return-void
.end method

.method public addView(Landroid/view/View;I)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "index"    # I

    .prologue
    .line 321
    const/4 v2, -0x2

    invoke-direct {p0, p1, v2}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->setupLayoutParams(Landroid/view/View;I)Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;

    move-result-object v1

    .line 322
    .local v1, "sglp":Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;
    new-instance v0, Lcom/google/android/videos/tagging/SuggestionGridLayout$SimpleGridItem;

    invoke-direct {v0, p0, p1}, Lcom/google/android/videos/tagging/SuggestionGridLayout$SimpleGridItem;-><init>(Lcom/google/android/videos/tagging/SuggestionGridLayout;Landroid/view/View;)V

    .line 323
    .local v0, "item":Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;
    const/4 v2, -0x1

    if-ne p2, v2, :cond_0

    .line 324
    iget-object v2, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 328
    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->addNewCardsToDeal(Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;)V

    .line 329
    invoke-super {p0, p1, p2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 330
    return-void

    .line 326
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v2, p2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public addViewToColumn(Landroid/view/View;I)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "column"    # I

    .prologue
    .line 339
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->setupLayoutParams(Landroid/view/View;I)Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;

    move-result-object v1

    .line 340
    .local v1, "sglp":Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;
    new-instance v0, Lcom/google/android/videos/tagging/SuggestionGridLayout$SimpleGridItem;

    invoke-direct {v0, p0, p1}, Lcom/google/android/videos/tagging/SuggestionGridLayout$SimpleGridItem;-><init>(Lcom/google/android/videos/tagging/SuggestionGridLayout;Landroid/view/View;)V

    .line 341
    .local v0, "item":Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;
    iget-object v2, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 342
    invoke-direct {p0, v0}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->addNewCardsToDeal(Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;)V

    .line 343
    invoke-super {p0, p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 344
    return-void
.end method

.method public checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 2
    .param p1, "lp"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 437
    instance-of v0, p1, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;

    if-eqz v0, :cond_0

    iget v0, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dismissView(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 370
    invoke-direct {p0, p1}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;

    move-result-object v0

    if-nez v0, :cond_0

    .line 371
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "View is not a child of this SuggestionGridLayout"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 373
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 374
    iget-object v0, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mSwiper:Lcom/google/android/videos/tagging/SwipeHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/SwipeHelper;->cancelOngoingDrag()V

    .line 376
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mSwiper:Lcom/google/android/videos/tagging/SwipeHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/videos/tagging/SwipeHelper;->dismissChild(Landroid/view/View;F)V

    .line 377
    return-void
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "child"    # Landroid/view/View;
    .param p3, "drawingTime"    # J

    .prologue
    .line 685
    const/4 v0, 0x0

    .line 687
    .local v0, "restoreCanvas":Z
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v1

    .line 690
    .local v1, "ret":Z
    if-eqz v0, :cond_0

    .line 691
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 693
    :cond_0
    return v1
.end method

.method public generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 4

    .prologue
    .line 432
    new-instance v0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;-><init>(III)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 447
    new-instance v0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1, "lp"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 442
    new-instance v0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;

    invoke-direct {v0, p1}, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method protected getChildDrawingOrder(II)I
    .locals 7
    .param p1, "childCount"    # I
    .param p2, "index"    # I

    .prologue
    .line 138
    iget-object v5, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 154
    .end local p2    # "index":I
    :goto_0
    return p2

    .line 141
    .restart local p2    # "index":I
    :cond_0
    iget-object v5, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 142
    .local v0, "animSize":I
    sub-int v1, p1, v0

    .line 143
    .local v1, "animStartsAt":I
    if-lt p2, v1, :cond_1

    .line 144
    iget-object v5, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    sub-int v6, p2, v1

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    invoke-virtual {p0, v5}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->indexOfChild(Landroid/view/View;)I

    move-result p2

    goto :goto_0

    .line 146
    :cond_1
    move v3, p2

    .line 147
    .local v3, "result":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-gt v2, v3, :cond_3

    .line 148
    invoke-virtual {p0, v2}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 149
    .local v4, "v":Landroid/view/View;
    iget-object v5, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 150
    add-int/lit8 v3, v3, 0x1

    .line 147
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .end local v4    # "v":Landroid/view/View;
    :cond_3
    move p2, v3

    .line 154
    goto :goto_0
.end method

.method public getColumnCount()I
    .locals 1

    .prologue
    .line 179
    iget v0, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mColCount:I

    return v0
.end method

.method public getMaxColumnWidth()I
    .locals 1

    .prologue
    .line 166
    iget v0, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mMaxColumnWidth:I

    return v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    .line 186
    iget-object v2, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mSwiper:Lcom/google/android/videos/tagging/SwipeHelper;

    invoke-virtual {v2, p1}, Lcom/google/android/videos/tagging/SwipeHelper;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 187
    .local v0, "intercepted":Z
    if-eqz v0, :cond_0

    .line 188
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 190
    :cond_0
    if-nez v0, :cond_1

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    :goto_0
    return v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 14
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 285
    iget-object v10, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mItemBottoms:[I

    invoke-virtual {p0}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getPaddingTop()I

    move-result v11

    invoke-static {v10, v11}, Ljava/util/Arrays;->fill([II)V

    .line 286
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getMeasuredWidth()I

    move-result v10

    iget v11, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mContentWidth:I

    sub-int/2addr v10, v11

    div-int/lit8 v0, v10, 0x2

    .line 288
    .local v0, "beginLeft":I
    iget-object v10, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 289
    .local v3, "childCount":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v3, :cond_4

    .line 290
    iget-object v10, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;

    .line 291
    .local v2, "child":Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;
    invoke-interface {v2}, Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;->getGridLayoutParams()Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;

    move-result-object v7

    .line 293
    .local v7, "lp":Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;
    iget v10, v7, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->column:I

    const/4 v11, -0x1

    if-ne v10, v11, :cond_0

    const/4 v4, 0x0

    .line 294
    .local v4, "column":I
    :goto_1
    iget-boolean v10, v7, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->noPadding:Z

    if-eqz v10, :cond_1

    const/4 v6, 0x0

    .line 295
    .local v6, "left":I
    :goto_2
    iget-object v10, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mItemBottoms:[I

    aget v10, v10, v4

    iget v11, v7, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->topMargin:I

    add-int v9, v10, v11

    .line 296
    .local v9, "top":I
    invoke-interface {v2}, Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;->isGone()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 298
    invoke-interface {v2, v6, v9, v6, v9}, Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;->gridLayout(IIII)V

    .line 289
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 293
    .end local v4    # "column":I
    .end local v6    # "left":I
    .end local v9    # "top":I
    :cond_0
    iget v4, v7, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->column:I

    goto :goto_1

    .line 294
    .restart local v4    # "column":I
    :cond_1
    iget v10, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mColWidth:I

    iget v11, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mHorizontalItemMargin:I

    add-int/2addr v10, v11

    mul-int/2addr v10, v4

    add-int v6, v0, v10

    goto :goto_2

    .line 301
    .restart local v6    # "left":I
    .restart local v9    # "top":I
    :cond_2
    invoke-interface {v2}, Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;->getMeasuredWidth()I

    move-result v10

    add-int v8, v6, v10

    .line 302
    .local v8, "right":I
    invoke-interface {v2}, Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;->getMeasuredHeight()I

    move-result v10

    add-int v1, v9, v10

    .line 303
    .local v1, "bottom":I
    invoke-interface {v2, v6, v9, v8, v1}, Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;->gridLayout(IIII)V

    .line 305
    iget v10, v7, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->column:I

    const/4 v11, -0x1

    if-ne v10, v11, :cond_3

    .line 306
    iget-object v10, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mItemBottoms:[I

    iget v11, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mVerticalItemMargin:I

    add-int/2addr v11, v1

    iget v12, v7, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v11, v12

    invoke-static {v10, v11}, Ljava/util/Arrays;->fill([II)V

    goto :goto_3

    .line 308
    :cond_3
    iget-object v10, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mItemBottoms:[I

    iget v11, v7, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->column:I

    iget v12, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mVerticalItemMargin:I

    add-int/2addr v12, v1

    iget v13, v7, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v12, v13

    aput v12, v10, v11

    goto :goto_3

    .line 311
    .end local v1    # "bottom":I
    .end local v2    # "child":Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;
    .end local v4    # "column":I
    .end local v6    # "left":I
    .end local v7    # "lp":Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;
    .end local v8    # "right":I
    .end local v9    # "top":I
    :cond_4
    return-void
.end method

.method protected onMeasure(II)V
    .locals 28
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 203
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v22

    .line 204
    .local v22, "widthMode":I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v12

    .line 205
    .local v12, "heightMode":I
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v23

    .line 206
    .local v23, "widthSize":I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v13

    .line 208
    .local v13, "heightSize":I
    const/16 v21, 0x0

    .line 209
    .local v21, "width":I
    const/4 v7, 0x0

    .line 210
    .local v7, "childWidth":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mHorizontalItemMargin:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mColCount:I

    move/from16 v25, v0

    add-int/lit8 v25, v25, -0x1

    mul-int v16, v24, v25

    .line 212
    .local v16, "margin":I
    sparse-switch v22, :sswitch_data_0

    .line 228
    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mColCount:I

    move/from16 v24, v0

    mul-int v24, v24, v7

    add-int v24, v24, v16

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mContentWidth:I

    .line 229
    move-object/from16 v0, p0

    iput v7, v0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mColWidth:I

    .line 230
    const/high16 v24, 0x40000000    # 2.0f

    move/from16 v0, v24

    invoke-static {v7, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v20

    .line 231
    .local v20, "singleColumnWidthSpec":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mContentWidth:I

    move/from16 v24, v0

    const/high16 v25, 0x40000000    # 2.0f

    invoke-static/range {v24 .. v25}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 232
    .local v3, "allColumnsWidthSpec":I
    const/high16 v24, 0x40000000    # 2.0f

    move/from16 v0, v21

    move/from16 v1, v24

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 235
    .local v2, "allColumnsNoPaddingWidthSpec":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mItemBottoms:[I

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-static/range {v24 .. v25}, Ljava/util/Arrays;->fill([II)V

    .line 237
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 239
    .local v5, "childCount":I
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_1
    if-ge v14, v5, :cond_4

    .line 240
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;

    .line 241
    .local v4, "child":Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;
    invoke-interface {v4}, Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;->isGone()Z

    move-result v24

    if-eqz v24, :cond_1

    .line 239
    :cond_0
    :goto_2
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 214
    .end local v2    # "allColumnsNoPaddingWidthSpec":I
    .end local v3    # "allColumnsWidthSpec":I
    .end local v4    # "child":Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;
    .end local v5    # "childCount":I
    .end local v14    # "i":I
    .end local v20    # "singleColumnWidthSpec":I
    :sswitch_0
    move/from16 v21, v23

    .line 215
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getPaddingLeft()I

    move-result v24

    sub-int v24, v21, v24

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getPaddingRight()I

    move-result v25

    sub-int v18, v24, v25

    .line 216
    .local v18, "paddedWidth":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mMaxColumnWidth:I

    move/from16 v24, v0

    sub-int v25, v18, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mColCount:I

    move/from16 v26, v0

    div-int v25, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 217
    goto/16 :goto_0

    .line 219
    .end local v18    # "paddedWidth":I
    :sswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getPaddingLeft()I

    move-result v24

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getPaddingRight()I

    move-result v25

    add-int v19, v24, v25

    .line 220
    .local v19, "padding":I
    sub-int v18, v23, v19

    .line 221
    .restart local v18    # "paddedWidth":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mMaxColumnWidth:I

    move/from16 v24, v0

    sub-int v25, v18, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mColCount:I

    move/from16 v26, v0

    div-int v25, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 222
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mColCount:I

    move/from16 v24, v0

    mul-int v24, v24, v7

    add-int v24, v24, v16

    add-int v21, v24, v19

    .line 223
    goto/16 :goto_0

    .line 225
    .end local v18    # "paddedWidth":I
    .end local v19    # "padding":I
    :sswitch_2
    new-instance v24, Ljava/lang/IllegalArgumentException;

    const-string v25, "Cannot measure SuggestionGridLayout with mode UNSPECIFIED"

    invoke-direct/range {v24 .. v25}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v24

    .line 244
    .restart local v2    # "allColumnsNoPaddingWidthSpec":I
    .restart local v3    # "allColumnsWidthSpec":I
    .restart local v4    # "child":Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;
    .restart local v5    # "childCount":I
    .restart local v14    # "i":I
    .restart local v20    # "singleColumnWidthSpec":I
    :cond_1
    invoke-interface {v4}, Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;->getGridLayoutParams()Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;

    move-result-object v15

    .line 248
    .local v15, "lp":Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;
    const/16 v24, 0x0

    const/16 v25, 0x0

    invoke-static/range {v24 .. v25}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 250
    .local v6, "childHeightSpec":I
    iget v10, v15, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->column:I

    .line 251
    .local v10, "column":I
    const/16 v24, -0x1

    move/from16 v0, v24

    if-ne v10, v0, :cond_3

    .line 252
    iget-boolean v0, v15, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->noPadding:Z

    move/from16 v24, v0

    if-eqz v24, :cond_2

    .line 253
    move v8, v2

    .line 257
    .local v8, "childWidthSpec":I
    :goto_3
    const/4 v10, 0x0

    .line 261
    :goto_4
    invoke-interface {v4, v8, v6}, Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;->gridMeasure(II)V

    .line 262
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mItemBottoms:[I

    move-object/from16 v24, v0

    aget v25, v24, v10

    iget v0, v15, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->topMargin:I

    move/from16 v26, v0

    invoke-interface {v4}, Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;->getMeasuredHeight()I

    move-result v27

    add-int v26, v26, v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mVerticalItemMargin:I

    move/from16 v27, v0

    add-int v26, v26, v27

    iget v0, v15, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->bottomMargin:I

    move/from16 v27, v0

    add-int v26, v26, v27

    add-int v25, v25, v26

    aput v25, v24, v10

    .line 264
    iget v0, v15, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->column:I

    move/from16 v24, v0

    const/16 v25, -0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_0

    .line 265
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mItemBottoms:[I

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mItemBottoms:[I

    move-object/from16 v25, v0

    aget v25, v25, v10

    invoke-static/range {v24 .. v25}, Ljava/util/Arrays;->fill([II)V

    goto/16 :goto_2

    .line 255
    .end local v8    # "childWidthSpec":I
    :cond_2
    move v8, v3

    .restart local v8    # "childWidthSpec":I
    goto :goto_3

    .line 259
    .end local v8    # "childWidthSpec":I
    :cond_3
    move/from16 v8, v20

    .restart local v8    # "childWidthSpec":I
    goto :goto_4

    .line 269
    .end local v4    # "child":Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;
    .end local v6    # "childHeightSpec":I
    .end local v8    # "childWidthSpec":I
    .end local v10    # "column":I
    .end local v15    # "lp":Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;
    :cond_4
    move v11, v13

    .line 270
    .local v11, "height":I
    const/high16 v24, 0x40000000    # 2.0f

    move/from16 v0, v24

    if-eq v12, v0, :cond_7

    .line 271
    const/16 v17, 0x0

    .line 272
    .local v17, "maxColHeight":I
    const/4 v14, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mColCount:I

    move/from16 v24, v0

    move/from16 v0, v24

    if-ge v14, v0, :cond_6

    .line 273
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mItemBottoms:[I

    move-object/from16 v24, v0

    aget v24, v24, v14

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mVerticalItemMargin:I

    move/from16 v25, v0

    sub-int v9, v24, v25

    .line 274
    .local v9, "colHeight":I
    move/from16 v0, v17

    if-le v9, v0, :cond_5

    .line 275
    move/from16 v17, v9

    .line 272
    :cond_5
    add-int/lit8 v14, v14, 0x1

    goto :goto_5

    .line 278
    .end local v9    # "colHeight":I
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getPaddingTop()I

    move-result v24

    add-int v24, v24, v17

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getPaddingBottom()I

    move-result v25

    add-int v11, v24, v25

    .line 280
    .end local v17    # "maxColHeight":I
    :cond_7
    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1, v11}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->setMeasuredDimension(II)V

    .line 281
    return-void

    .line 212
    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_2
        0x40000000 -> :sswitch_0
    .end sparse-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mSwiper:Lcom/google/android/videos/tagging/SwipeHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/tagging/SwipeHelper;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 196
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 198
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public removeAllViews()V
    .locals 3

    .prologue
    .line 393
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getChildCount()I

    move-result v0

    .line 394
    .local v0, "childCount":I
    add-int/lit8 v1, v0, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_0

    .line 395
    invoke-virtual {p0, v1}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->removeGridItem(Landroid/view/View;)V

    .line 394
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 397
    :cond_0
    return-void
.end method

.method public removeGridItem(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 400
    iget-object v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 402
    iget-object v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mSwiper:Lcom/google/android/videos/tagging/SwipeHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/tagging/SwipeHelper;->cancelOngoingDrag()V

    .line 405
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;

    move-result-object v0

    .line 407
    .local v0, "item":Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;
    if-eqz v0, :cond_2

    .line 408
    invoke-interface {v0, p1}, Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;->removeView(Landroid/view/View;)V

    .line 409
    invoke-interface {v0}, Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;->getViews()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 410
    iget-object v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 412
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 413
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 417
    :goto_0
    return-void

    .line 415
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SGL: removeGridItem with non-grid item "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public removeView(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 335
    invoke-virtual {p0, p1}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->removeGridItem(Landroid/view/View;)V

    .line 336
    return-void
.end method

.method public setChangeAppearingAnimatorEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mLayoutTransition:Landroid/animation/LayoutTransition;

    if-eqz v0, :cond_0

    .line 122
    iget-object v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mLayoutTransition:Landroid/animation/LayoutTransition;

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mChangeAppearingAnimator:Landroid/animation/Animator;

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    .line 125
    :cond_0
    return-void

    .line 122
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setColumnCount(I)V
    .locals 1
    .param p1, "cols"    # I

    .prologue
    .line 170
    iget v0, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mColCount:I

    if-ne v0, p1, :cond_0

    .line 176
    :goto_0
    return-void

    .line 173
    :cond_0
    iput p1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mColCount:I

    .line 174
    new-array v0, p1, [I

    iput-object v0, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mItemBottoms:[I

    .line 175
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->requestLayout()V

    goto :goto_0
.end method

.method public setLayoutTransitionsEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 117
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mLayoutTransition:Landroid/animation/LayoutTransition;

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 118
    return-void

    .line 117
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setMaxColumnWidth(I)V
    .locals 1
    .param p1, "width"    # I

    .prologue
    .line 158
    iget v0, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mMaxColumnWidth:I

    if-ne v0, p1, :cond_0

    .line 163
    :goto_0
    return-void

    .line 161
    :cond_0
    iput p1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mMaxColumnWidth:I

    .line 162
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/SuggestionGridLayout;->requestLayout()V

    goto :goto_0
.end method

.method public setOnDismissListener(Lcom/google/android/videos/tagging/SuggestionGridLayout$OnDismissListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/videos/tagging/SuggestionGridLayout$OnDismissListener;

    .prologue
    .line 380
    iput-object p1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout;->mOnDismissListener:Lcom/google/android/videos/tagging/SuggestionGridLayout$OnDismissListener;

    .line 381
    return-void
.end method
