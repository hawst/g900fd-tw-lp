.class public Lcom/google/android/videos/ui/AssetImageUriCreator;
.super Ljava/lang/Object;
.source "AssetImageUriCreator.java"


# instance fields
.field public final maxEpisodeScreenshotWidth:I

.field public final maxMoviePosterWidth:I

.field public final maxMovieScreenshotWidth:I

.field public final maxShowBannerWidth:I

.field public final maxShowPosterWidth:I


# direct methods
.method private constructor <init>(IIIII)V
    .locals 0
    .param p1, "maxMoviePosterWidth"    # I
    .param p2, "maxMovieScreenshotWidth"    # I
    .param p3, "maxShowPosterWidth"    # I
    .param p4, "maxShowBannerWidth"    # I
    .param p5, "maxEpisodeScreenshotWidth"    # I

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput p1, p0, Lcom/google/android/videos/ui/AssetImageUriCreator;->maxMoviePosterWidth:I

    .line 26
    iput p2, p0, Lcom/google/android/videos/ui/AssetImageUriCreator;->maxMovieScreenshotWidth:I

    .line 27
    iput p3, p0, Lcom/google/android/videos/ui/AssetImageUriCreator;->maxShowPosterWidth:I

    .line 28
    iput p4, p0, Lcom/google/android/videos/ui/AssetImageUriCreator;->maxShowBannerWidth:I

    .line 29
    iput p5, p0, Lcom/google/android/videos/ui/AssetImageUriCreator;->maxEpisodeScreenshotWidth:I

    .line 30
    return-void
.end method

.method public static createForMobile(Landroid/content/res/Resources;)Lcom/google/android/videos/ui/AssetImageUriCreator;
    .locals 7
    .param p0, "resources"    # Landroid/content/res/Resources;

    .prologue
    const/high16 v3, 0x3fc00000    # 1.5f

    .line 33
    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    .line 34
    .local v6, "displayMetrics":Landroid/util/DisplayMetrics;
    iget v0, v6, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v1, v6, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 36
    .local v2, "maxScreenWidth":I
    iget v0, v6, Landroid/util/DisplayMetrics;->density:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    .line 37
    int-to-float v0, v2

    iget v1, v6, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v1

    mul-float/2addr v0, v3

    float-to-int v2, v0

    .line 40
    :cond_0
    new-instance v0, Lcom/google/android/videos/ui/AssetImageUriCreator;

    const v1, 0x7f0e017c

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const v3, 0x7f0e017d

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    const v4, 0x7f0e017e

    invoke-virtual {p0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/ui/AssetImageUriCreator;-><init>(IIIII)V

    return-object v0
.end method

.method public static createForPano(Landroid/content/res/Resources;)Lcom/google/android/videos/ui/AssetImageUriCreator;
    .locals 6
    .param p0, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 50
    const v0, 0x7f0e01b5

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 51
    .local v2, "wallpaperWidth":I
    new-instance v0, Lcom/google/android/videos/ui/AssetImageUriCreator;

    const v1, 0x7f0e01b2

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const v3, 0x7f0e01b3

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    const v4, 0x7f0e01b4

    invoke-virtual {p0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/ui/AssetImageUriCreator;-><init>(IIIII)V

    return-object v0
.end method


# virtual methods
.method public getEpisodeScreenshotUrl(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/lang/String;
    .locals 6
    .param p1, "episodeResource"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    .line 85
    iget-object v0, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/videos/ui/AssetImageUriCreator;->maxEpisodeScreenshotWidth:I

    iget v3, p0, Lcom/google/android/videos/ui/AssetImageUriCreator;->maxEpisodeScreenshotWidth:I

    int-to-float v3, v3

    const v4, 0x3fe38e39

    div-float/2addr v3, v4

    float-to-int v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/api/AssetResourceUtil;->findBestImageUrl(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;IIIFZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMoviePosterUrl(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/lang/String;
    .locals 6
    .param p1, "movieResource"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    .line 61
    iget-object v0, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/videos/ui/AssetImageUriCreator;->maxMoviePosterWidth:I

    iget v3, p0, Lcom/google/android/videos/ui/AssetImageUriCreator;->maxMoviePosterWidth:I

    int-to-float v3, v3

    const v4, 0x3f31a787

    div-float/2addr v3, v4

    float-to-int v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/api/AssetResourceUtil;->findBestImageUrl(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;IIIFZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMovieScreenshotUrl(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/lang/String;
    .locals 6
    .param p1, "movieResource"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    .line 67
    iget-object v0, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/videos/ui/AssetImageUriCreator;->maxMovieScreenshotWidth:I

    iget v3, p0, Lcom/google/android/videos/ui/AssetImageUriCreator;->maxMovieScreenshotWidth:I

    int-to-float v3, v3

    const v4, 0x4018f5c3    # 2.39f

    div-float/2addr v3, v4

    float-to-int v3, v3

    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/api/AssetResourceUtil;->findBestImageUrl(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;IIIFZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getShowBannerUrl(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/lang/String;
    .locals 6
    .param p1, "showResource"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    .line 79
    iget-object v0, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/videos/ui/AssetImageUriCreator;->maxShowBannerWidth:I

    iget v3, p0, Lcom/google/android/videos/ui/AssetImageUriCreator;->maxShowBannerWidth:I

    int-to-float v3, v3

    const v4, 0x3fe38e39

    div-float/2addr v3, v4

    float-to-int v3, v3

    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/api/AssetResourceUtil;->findBestImageUrl(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;IIIFZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getShowPosterUrl(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/lang/String;
    .locals 6
    .param p1, "showResource"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    .line 73
    iget-object v0, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/videos/ui/AssetImageUriCreator;->maxShowPosterWidth:I

    iget v3, p0, Lcom/google/android/videos/ui/AssetImageUriCreator;->maxShowPosterWidth:I

    int-to-float v3, v3

    const/high16 v4, 0x3f800000    # 1.0f

    div-float/2addr v3, v4

    float-to-int v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/api/AssetResourceUtil;->findBestImageUrl(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;IIIFZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
