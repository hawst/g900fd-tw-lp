.class public Lcom/google/android/videos/settings/EdgeTypePreference;
.super Lcom/google/android/videos/settings/GridDialogPreference;
.source "EdgeTypePreference.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/settings/GridDialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const v0, 0x7f04003f

    invoke-virtual {p0, v0}, Lcom/google/android/videos/settings/EdgeTypePreference;->setDialogLayoutResource(I)V

    .line 27
    const v0, 0x7f040040

    invoke-virtual {p0, v0}, Lcom/google/android/videos/settings/EdgeTypePreference;->setGridItemLayoutResource(I)V

    .line 28
    return-void
.end method


# virtual methods
.method protected decodeStringValue(Ljava/lang/String;)I
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-static {p1}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method protected onBindGridItem(Landroid/view/View;I)V
    .locals 12
    .param p1, "view"    # Landroid/view/View;
    .param p2, "index"    # I

    .prologue
    .line 42
    const v1, 0x7f0f011c

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/google/android/exoplayer/text/SubtitleView;

    .line 44
    .local v8, "preview":Lcom/google/android/exoplayer/text/SubtitleView;
    sget-object v7, Lcom/google/android/exoplayer/text/CaptionStyleCompat;->DEFAULT:Lcom/google/android/exoplayer/text/CaptionStyleCompat;

    .line 45
    .local v7, "defaultStyle":Lcom/google/android/exoplayer/text/CaptionStyleCompat;
    new-instance v0, Lcom/google/android/exoplayer/text/CaptionStyleCompat;

    iget v1, v7, Lcom/google/android/exoplayer/text/CaptionStyleCompat;->foregroundColor:I

    iget v2, v7, Lcom/google/android/exoplayer/text/CaptionStyleCompat;->backgroundColor:I

    iget v3, v7, Lcom/google/android/exoplayer/text/CaptionStyleCompat;->windowColor:I

    invoke-virtual {p0, p2}, Lcom/google/android/videos/settings/EdgeTypePreference;->getValueAt(I)I

    move-result v4

    iget v5, v7, Lcom/google/android/exoplayer/text/CaptionStyleCompat;->edgeColor:I

    iget-object v6, v7, Lcom/google/android/exoplayer/text/CaptionStyleCompat;->typeface:Landroid/graphics/Typeface;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/exoplayer/text/CaptionStyleCompat;-><init>(IIIIILandroid/graphics/Typeface;)V

    .line 48
    .local v0, "thisStyle":Lcom/google/android/exoplayer/text/CaptionStyleCompat;
    invoke-virtual {v8, v0}, Lcom/google/android/exoplayer/text/SubtitleView;->setStyle(Lcom/google/android/exoplayer/text/CaptionStyleCompat;)V

    .line 50
    invoke-virtual {p0}, Lcom/google/android/videos/settings/EdgeTypePreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v9, v1, Landroid/util/DisplayMetrics;->density:F

    .line 51
    .local v9, "scale":F
    const/high16 v1, 0x42000000    # 32.0f

    mul-float/2addr v1, v9

    invoke-virtual {v8, v1}, Lcom/google/android/exoplayer/text/SubtitleView;->setTextSize(F)V

    .line 53
    invoke-virtual {p0, p2}, Lcom/google/android/videos/settings/EdgeTypePreference;->getTitleAt(I)Ljava/lang/CharSequence;

    move-result-object v11

    .line 54
    .local v11, "title":Ljava/lang/CharSequence;
    if-eqz v11, :cond_0

    .line 55
    const v1, 0x7f0f011a

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 56
    .local v10, "summary":Landroid/widget/TextView;
    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    .end local v10    # "summary":Landroid/widget/TextView;
    :cond_0
    return-void
.end method

.method public shouldDisableDependents()Z
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/google/android/videos/settings/EdgeTypePreference;->getValue()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0}, Lcom/google/android/videos/settings/GridDialogPreference;->shouldDisableDependents()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
