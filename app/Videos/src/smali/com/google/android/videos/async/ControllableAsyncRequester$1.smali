.class Lcom/google/android/videos/async/ControllableAsyncRequester$1;
.super Ljava/lang/Object;
.source "ControllableAsyncRequester.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/async/ControllableAsyncRequester;->request(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/async/ControllableAsyncRequester;

.field final synthetic val$callback:Lcom/google/android/videos/async/Callback;

.field final synthetic val$request:Lcom/google/android/videos/async/ControllableRequest;


# direct methods
.method constructor <init>(Lcom/google/android/videos/async/ControllableAsyncRequester;Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/Callback;)V
    .locals 0

    .prologue
    .line 41
    .local p0, "this":Lcom/google/android/videos/async/ControllableAsyncRequester$1;, "Lcom/google/android/videos/async/ControllableAsyncRequester.1;"
    iput-object p1, p0, Lcom/google/android/videos/async/ControllableAsyncRequester$1;->this$0:Lcom/google/android/videos/async/ControllableAsyncRequester;

    iput-object p2, p0, Lcom/google/android/videos/async/ControllableAsyncRequester$1;->val$request:Lcom/google/android/videos/async/ControllableRequest;

    iput-object p3, p0, Lcom/google/android/videos/async/ControllableAsyncRequester$1;->val$callback:Lcom/google/android/videos/async/Callback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private onCancelled(Lcom/google/android/videos/async/ControllableRequest;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<TR;>;)V"
        }
    .end annotation

    .prologue
    .line 66
    .local p0, "this":Lcom/google/android/videos/async/ControllableAsyncRequester$1;, "Lcom/google/android/videos/async/ControllableAsyncRequester.1;"
    .local p1, "request":Lcom/google/android/videos/async/ControllableRequest;, "Lcom/google/android/videos/async/ControllableRequest<TR;>;"
    iget-object v0, p0, Lcom/google/android/videos/async/ControllableAsyncRequester$1;->val$callback:Lcom/google/android/videos/async/Callback;

    instance-of v0, v0, Lcom/google/android/videos/async/NewCallback;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/google/android/videos/async/ControllableAsyncRequester$1;->val$callback:Lcom/google/android/videos/async/Callback;

    check-cast v0, Lcom/google/android/videos/async/NewCallback;

    invoke-interface {v0, p1}, Lcom/google/android/videos/async/NewCallback;->onCancelled(Ljava/lang/Object;)V

    .line 69
    :cond_0
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 44
    .local p0, "this":Lcom/google/android/videos/async/ControllableAsyncRequester$1;, "Lcom/google/android/videos/async/ControllableAsyncRequester.1;"
    iget-object v1, p0, Lcom/google/android/videos/async/ControllableAsyncRequester$1;->val$request:Lcom/google/android/videos/async/ControllableRequest;

    invoke-virtual {v1}, Lcom/google/android/videos/async/ControllableRequest;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 46
    :try_start_0
    iget-object v1, p0, Lcom/google/android/videos/async/ControllableAsyncRequester$1;->this$0:Lcom/google/android/videos/async/ControllableAsyncRequester;

    # getter for: Lcom/google/android/videos/async/ControllableAsyncRequester;->target:Lcom/google/android/videos/async/Requester;
    invoke-static {v1}, Lcom/google/android/videos/async/ControllableAsyncRequester;->access$000(Lcom/google/android/videos/async/ControllableAsyncRequester;)Lcom/google/android/videos/async/Requester;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/async/ControllableAsyncRequester$1;->val$request:Lcom/google/android/videos/async/ControllableRequest;

    iget-object v2, v2, Lcom/google/android/videos/async/ControllableRequest;->data:Ljava/lang/Object;

    new-instance v3, Lcom/google/android/videos/async/ControllableAsyncRequester$1$1;

    invoke-direct {v3, p0}, Lcom/google/android/videos/async/ControllableAsyncRequester$1$1;-><init>(Lcom/google/android/videos/async/ControllableAsyncRequester$1;)V

    invoke-interface {v1, v2, v3}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    :goto_0
    return-void

    .line 56
    :catch_0
    move-exception v0

    .line 57
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "target requester should catch exception and pass to callback.onError"

    invoke-static {v1}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 58
    iget-object v1, p0, Lcom/google/android/videos/async/ControllableAsyncRequester$1;->val$callback:Lcom/google/android/videos/async/Callback;

    iget-object v2, p0, Lcom/google/android/videos/async/ControllableAsyncRequester$1;->val$request:Lcom/google/android/videos/async/ControllableRequest;

    invoke-interface {v1, v2, v0}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0

    .line 61
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/async/ControllableAsyncRequester$1;->val$request:Lcom/google/android/videos/async/ControllableRequest;

    invoke-direct {p0, v1}, Lcom/google/android/videos/async/ControllableAsyncRequester$1;->onCancelled(Lcom/google/android/videos/async/ControllableRequest;)V

    goto :goto_0
.end method
