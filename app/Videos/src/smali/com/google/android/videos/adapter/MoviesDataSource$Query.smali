.class public abstract Lcom/google/android/videos/adapter/MoviesDataSource$Query;
.super Ljava/lang/Object;
.source "MoviesDataSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/adapter/MoviesDataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Query"
.end annotation


# static fields
.field private static final PROJECTION:[Ljava/lang/String;

.field private static final PROJECTION_WITH_EXPIRY:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 226
    const/16 v0, 0x1d

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "purchase_status"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "merged_expiration_timestamp"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "video_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "release_year"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "duration_seconds"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "(pinned IS NOT NULL AND pinned > 0)"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "(license_type IS NOT NULL AND license_type != 0)"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "pinning_status"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "pinning_status_reason"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "pinning_drm_error_code"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "download_bytes_downloaded"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "pinning_download_size"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "resume_timestamp"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "poster_uri"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "screenshot_uri"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "last_activity_timestamp"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "account"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "description"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "writers"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "directors"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "actors"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "producers"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "rating_name"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "format_type"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "badge_surround_sound"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "badge_knowledge"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "has_subtitles"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "purchase_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/videos/adapter/MoviesDataSource$Query;->PROJECTION:[Ljava/lang/String;

    .line 260
    sget-object v0, Lcom/google/android/videos/adapter/MoviesDataSource$Query;->PROJECTION:[Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    sput-object v0, Lcom/google/android/videos/adapter/MoviesDataSource$Query;->PROJECTION_WITH_EXPIRY:[Ljava/lang/String;

    .line 261
    sget-object v0, Lcom/google/android/videos/adapter/MoviesDataSource$Query;->PROJECTION_WITH_EXPIRY:[Ljava/lang/String;

    const-string v1, "CASE WHEN merged_expiration_timestamp < :expiry THEN 0 ELSE purchase_status END as merged_status"

    aput-object v1, v0, v3

    .line 262
    return-void
.end method

.method private static createExpiryTimestamp(J)Ljava/lang/String;
    .locals 2
    .param p0, "nowTimestamp"    # J

    .prologue
    .line 341
    const-wide/32 v0, 0x5265c00

    sub-long v0, p0, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static createLibraryRequest(Ljava/lang/String;ZJ)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 10
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "downloadedOnly"    # Z
    .param p2, "nowTimestamp"    # J

    .prologue
    const/4 v1, 0x0

    .line 302
    if-eqz p1, :cond_0

    const-string v5, "NOT (hidden IN (1, 3)) AND purchase_status = 2 AND account = ? AND (pinned IS NOT NULL AND pinned > 0)"

    .line 305
    .local v5, "where":Ljava/lang/String;
    :goto_0
    const/4 v0, 0x2

    new-array v6, v0, [Ljava/lang/String;

    invoke-static {p2, p3}, Lcom/google/android/videos/adapter/MoviesDataSource$Query;->createExpiryTimestamp(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v1

    const/4 v0, 0x1

    aput-object p0, v6, v0

    .line 309
    .local v6, "whereArgs":[Ljava/lang/String;
    new-instance v0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    const-string v2, "purchased_assets, user_assets, videos ON asset_type = 6 AND user_assets_type = 6 AND account = user_assets_account AND asset_id = user_assets_id AND asset_id = video_id"

    sget-object v3, Lcom/google/android/videos/adapter/MoviesDataSource$Query;->PROJECTION_WITH_EXPIRY:[Ljava/lang/String;

    const-string v4, "rating_id"

    const/4 v7, 0x0

    const-string v8, "merged_status <> 2, merged_expiration_timestamp, title"

    const/4 v9, -0x1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0

    .line 302
    .end local v5    # "where":Ljava/lang/String;
    .end local v6    # "whereArgs":[Ljava/lang/String;
    :cond_0
    const-string v5, "NOT (hidden IN (1, 3)) AND purchase_status = 2 AND account = ?"

    goto :goto_0
.end method

.method public static createNewRequest(Ljava/lang/String;JJ[Ljava/lang/String;Z)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 13
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "nowTimestamp"    # J
    .param p3, "recentActiveMillis"    # J
    .param p5, "excludedVideoIds"    # [Ljava/lang/String;
    .param p6, "downloadedOnly"    # Z

    .prologue
    .line 321
    if-eqz p6, :cond_1

    const-string v6, "NOT (hidden IN (1, 3)) AND purchase_status = 2 AND account = ? AND (purchase_type = 1 OR :nowTimestamp - last_activity_timestamp <= min(4 * (1 - resume_timestamp / (duration_seconds * 1000.0)), 1) * :recentActiveMillis) AND NOT ifnull(watched_to_end_credit, 0) AND (pinned IS NOT NULL AND pinned > 0)"

    .line 324
    .local v6, "where":Ljava/lang/String;
    :goto_0
    if-eqz p5, :cond_2

    move-object/from16 v0, p5

    array-length v11, v0

    .line 325
    .local v11, "excludedVideosCount":I
    :goto_1
    add-int/lit8 v1, v11, 0x4

    new-array v7, v1, [Ljava/lang/String;

    .line 326
    .local v7, "whereArgs":[Ljava/lang/String;
    const/4 v1, 0x0

    invoke-static {p1, p2}, Lcom/google/android/videos/adapter/MoviesDataSource$Query;->createExpiryTimestamp(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v7, v1

    .line 327
    const/4 v1, 0x1

    aput-object p0, v7, v1

    .line 328
    const/4 v1, 0x2

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v7, v1

    .line 329
    const/4 v1, 0x3

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v7, v1

    .line 330
    if-eqz v11, :cond_0

    .line 331
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND NOT ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "video_id"

    move-object/from16 v0, p5

    array-length v3, v0

    invoke-static {v2, v3}, Lcom/google/android/videos/utils/DbUtils;->buildInMultipleParamsClause(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 334
    const/4 v1, 0x0

    const/4 v2, 0x4

    move-object/from16 v0, p5

    invoke-static {v0, v1, v7, v2, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 336
    :cond_0
    new-instance v1, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    const/4 v2, 0x0

    const-string v3, "purchased_assets, user_assets, videos ON asset_type = 6 AND user_assets_type = 6 AND account = user_assets_account AND asset_id = user_assets_id AND asset_id = video_id"

    sget-object v4, Lcom/google/android/videos/adapter/MoviesDataSource$Query;->PROJECTION_WITH_EXPIRY:[Ljava/lang/String;

    const-string v5, "rating_id"

    const/4 v8, 0x0

    const-string v9, "merged_status <> 2, last_activity_timestamp DESC"

    const/4 v10, -0x1

    invoke-direct/range {v1 .. v10}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v1

    .line 321
    .end local v6    # "where":Ljava/lang/String;
    .end local v7    # "whereArgs":[Ljava/lang/String;
    .end local v11    # "excludedVideosCount":I
    :cond_1
    const-string v6, "NOT (hidden IN (1, 3)) AND purchase_status = 2 AND account = ? AND (purchase_type = 1 OR :nowTimestamp - last_activity_timestamp <= min(4 * (1 - resume_timestamp / (duration_seconds * 1000.0)), 1) * :recentActiveMillis) AND NOT ifnull(watched_to_end_credit, 0)"

    goto :goto_0

    .line 324
    .restart local v6    # "where":Ljava/lang/String;
    :cond_2
    const/4 v11, 0x0

    goto :goto_1
.end method

.method public static createPreorderRequest(Ljava/lang/String;)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 10
    .param p0, "account"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 314
    new-instance v0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    const-string v2, "purchased_assets, user_assets, videos ON asset_type = 6 AND user_assets_type = 6 AND account = user_assets_account AND asset_id = user_assets_id AND asset_id = video_id"

    sget-object v3, Lcom/google/android/videos/adapter/MoviesDataSource$Query;->PROJECTION:[Ljava/lang/String;

    const-string v4, "rating_id"

    const-string v5, "NOT (hidden IN (1, 3)) AND purchase_status = 6 AND account = ?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    aput-object p0, v6, v1

    const/4 v7, 0x0

    const-string v8, "title"

    const/4 v9, -0x1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method
