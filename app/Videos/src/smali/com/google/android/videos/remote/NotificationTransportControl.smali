.class public abstract Lcom/google/android/videos/remote/NotificationTransportControl;
.super Lcom/google/android/videos/remote/TransportControl;
.source "NotificationTransportControl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private isShowRequested:Z

.field private previousPlaybackState:I

.field private service:Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;


# direct methods
.method protected constructor <init>(Lcom/google/android/videos/remote/RemoteTracker;Landroid/content/Context;)V
    .locals 1
    .param p1, "remoteTracker"    # Lcom/google/android/videos/remote/RemoteTracker;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/google/android/videos/remote/TransportControl;-><init>(Lcom/google/android/videos/remote/RemoteTracker;)V

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->previousPlaybackState:I

    .line 70
    iput-object p2, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->context:Landroid/content/Context;

    .line 71
    return-void
.end method

.method private hide()V
    .locals 4

    .prologue
    .line 162
    const-string v0, "Remote notification hide() called"

    invoke-static {v0}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 163
    iget-boolean v0, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->isShowRequested:Z

    if-eqz v0, :cond_0

    .line 164
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->isShowRequested:Z

    .line 165
    iget-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->context:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->context:Landroid/content/Context;

    const-class v3, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 167
    :cond_0
    return-void
.end method

.method public static newInstance(Lcom/google/android/videos/remote/RemoteTracker;Landroid/content/Context;)Lcom/google/android/videos/remote/NotificationTransportControl;
    .locals 2
    .param p0, "remoteTracker"    # Lcom/google/android/videos/remote/RemoteTracker;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 46
    new-instance v0, Lcom/google/android/videos/remote/NotificationTransportControlV21;

    invoke-direct {v0, p0, p1}, Lcom/google/android/videos/remote/NotificationTransportControlV21;-><init>(Lcom/google/android/videos/remote/RemoteTracker;Landroid/content/Context;)V

    .line 50
    :goto_0
    return-object v0

    .line 47
    :cond_0
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    .line 48
    new-instance v0, Lcom/google/android/videos/remote/NotificationTransportControlV16;

    invoke-direct {v0, p0, p1}, Lcom/google/android/videos/remote/NotificationTransportControlV16;-><init>(Lcom/google/android/videos/remote/RemoteTracker;Landroid/content/Context;)V

    goto :goto_0

    .line 50
    :cond_1
    new-instance v0, Lcom/google/android/videos/remote/NotificationTransportControlV8;

    invoke-direct {v0, p0, p1}, Lcom/google/android/videos/remote/NotificationTransportControlV8;-><init>(Lcom/google/android/videos/remote/RemoteTracker;Landroid/content/Context;)V

    goto :goto_0
.end method

.method private shouldBeShowing()Z
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v0}, Lcom/google/android/videos/remote/RemoteTracker;->getVideoInfo()Lcom/google/android/videos/remote/RemoteVideoInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v0}, Lcom/google/android/videos/remote/RemoteTracker;->getPlayerState()Lcom/google/android/videos/remote/PlayerState;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v0}, Lcom/google/android/videos/remote/RemoteTracker;->getPlayerState()Lcom/google/android/videos/remote/PlayerState;

    move-result-object v0

    iget v0, v0, Lcom/google/android/videos/remote/PlayerState;->state:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v0}, Lcom/google/android/videos/remote/RemoteTracker;->getScreenName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private show()V
    .locals 4

    .prologue
    .line 151
    const-string v0, "Remote notification show() called"

    invoke-static {v0}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 152
    iget-boolean v0, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->isShowRequested:Z

    if-nez v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->context:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->context:Landroid/content/Context;

    const-class v3, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 154
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->isShowRequested:Z

    .line 156
    :cond_0
    return-void
.end method

.method private updateVisibility()V
    .locals 2

    .prologue
    .line 173
    invoke-direct {p0}, Lcom/google/android/videos/remote/NotificationTransportControl;->shouldBeShowing()Z

    move-result v0

    .line 174
    .local v0, "shouldBeShowing":Z
    iget-boolean v1, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->isShowRequested:Z

    if-ne v1, v0, :cond_0

    .line 182
    :goto_0
    return-void

    .line 177
    :cond_0
    if-eqz v0, :cond_1

    .line 178
    invoke-direct {p0}, Lcom/google/android/videos/remote/NotificationTransportControl;->show()V

    goto :goto_0

    .line 180
    :cond_1
    invoke-direct {p0}, Lcom/google/android/videos/remote/NotificationTransportControl;->hide()V

    goto :goto_0
.end method


# virtual methods
.method final attachService(Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;)V
    .locals 0
    .param p1, "service"    # Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->service:Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;

    .line 56
    invoke-virtual {p0, p1}, Lcom/google/android/videos/remote/NotificationTransportControl;->createNotification(Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;)V

    .line 57
    return-void
.end method

.method protected abstract createNotification(Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;)V
.end method

.method protected abstract destroyNotification()V
.end method

.method final detachService()V
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/android/videos/remote/NotificationTransportControl;->destroyNotification()V

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->service:Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;

    .line 62
    return-void
.end method

.method protected final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->context:Landroid/content/Context;

    return-object v0
.end method

.method protected getStatus()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 123
    invoke-virtual {p0}, Lcom/google/android/videos/remote/NotificationTransportControl;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 124
    .local v0, "context":Landroid/content/Context;
    iget-object v3, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v3}, Lcom/google/android/videos/remote/RemoteTracker;->getScreenName()Ljava/lang/String;

    move-result-object v2

    .line 125
    .local v2, "screenName":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v3}, Lcom/google/android/videos/remote/RemoteTracker;->getPlayerState()Lcom/google/android/videos/remote/PlayerState;

    move-result-object v1

    .line 126
    .local v1, "playerState":Lcom/google/android/videos/remote/PlayerState;
    iget-object v3, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v3}, Lcom/google/android/videos/remote/RemoteTracker;->hasError()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 127
    iget-object v3, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v3}, Lcom/google/android/videos/remote/RemoteTracker;->getError()Ljava/lang/String;

    move-result-object v3

    .line 143
    :goto_0
    return-object v3

    .line 128
    :cond_0
    if-eqz v1, :cond_1

    .line 129
    iget v3, v1, Lcom/google/android/videos/remote/PlayerState;->state:I

    packed-switch v3, :pswitch_data_0

    .line 140
    :pswitch_0
    iget-object v3, v1, Lcom/google/android/videos/remote/PlayerState;->errorMessage:Ljava/lang/String;

    goto :goto_0

    .line 131
    :pswitch_1
    const v3, 0x7f0b01cf

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 133
    :pswitch_2
    const v3, 0x7f0b01ce

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 135
    :pswitch_3
    const v3, 0x7f0b01cd

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 137
    :pswitch_4
    const v3, 0x7f0b01cc

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 143
    :cond_1
    const-string v3, ""

    goto :goto_0

    .line 129
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method protected getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v0}, Lcom/google/android/videos/remote/RemoteTracker;->getVideoInfo()Lcom/google/android/videos/remote/RemoteVideoInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v0}, Lcom/google/android/videos/remote/RemoteTracker;->getVideoInfo()Lcom/google/android/videos/remote/RemoteVideoInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/videos/remote/RemoteVideoInfo;->videoTitle:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public onErrorChanged()V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->service:Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->service:Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;

    invoke-virtual {p0, v0}, Lcom/google/android/videos/remote/NotificationTransportControl;->updateNotification(Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;)V

    .line 85
    :cond_0
    return-void
.end method

.method public onPlayerStateChanged()V
    .locals 3

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/google/android/videos/remote/NotificationTransportControl;->updateVisibility()V

    .line 92
    iget-object v1, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v1}, Lcom/google/android/videos/remote/RemoteTracker;->getPlayerState()Lcom/google/android/videos/remote/PlayerState;

    move-result-object v0

    .line 93
    .local v0, "playerState":Lcom/google/android/videos/remote/PlayerState;
    iget-object v1, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->service:Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 94
    iget v1, v0, Lcom/google/android/videos/remote/PlayerState;->state:I

    iget v2, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->previousPlaybackState:I

    if-eq v1, v2, :cond_0

    .line 95
    iget v1, v0, Lcom/google/android/videos/remote/PlayerState;->state:I

    iput v1, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->previousPlaybackState:I

    .line 96
    iget-object v1, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->service:Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;

    invoke-virtual {p0, v1}, Lcom/google/android/videos/remote/NotificationTransportControl;->updateNotification(Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;)V

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 99
    :cond_1
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->previousPlaybackState:I

    goto :goto_0
.end method

.method public onPosterBitmapChanged()V
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->service:Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->service:Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;

    invoke-virtual {p0, v0}, Lcom/google/android/videos/remote/NotificationTransportControl;->updateNotification(Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;)V

    .line 116
    :cond_0
    return-void
.end method

.method public onScreenNameChanged()V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->service:Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->service:Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;

    invoke-virtual {p0, v0}, Lcom/google/android/videos/remote/NotificationTransportControl;->updateNotification(Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;)V

    .line 78
    :cond_0
    return-void
.end method

.method public onVideoInfoChanged()V
    .locals 1

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/google/android/videos/remote/NotificationTransportControl;->updateVisibility()V

    .line 106
    iget-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->service:Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControl;->service:Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;

    invoke-virtual {p0, v0}, Lcom/google/android/videos/remote/NotificationTransportControl;->updateNotification(Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;)V

    .line 109
    :cond_0
    return-void
.end method

.method protected abstract updateNotification(Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;)V
.end method
