.class Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingMasterSubCursorWrapper;
.super Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;
.source "CursorHelper.java"

# interfaces
.implements Lcom/google/android/videos/store/MasterSubCursor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/CursorHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ReferenceCountingMasterSubCursorWrapper"
.end annotation


# instance fields
.field private final cursor:Lcom/google/android/videos/store/MasterSubCursor;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/store/MasterSubCursor;)V
    .locals 0
    .param p1, "cursor"    # Lcom/google/android/videos/store/MasterSubCursor;

    .prologue
    .line 262
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;-><init>(Landroid/database/Cursor;)V

    .line 263
    iput-object p1, p0, Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingMasterSubCursorWrapper;->cursor:Lcom/google/android/videos/store/MasterSubCursor;

    .line 264
    return-void
.end method


# virtual methods
.method public getCurrentSubRange()Lcom/google/android/videos/store/MasterSubCursor;
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingMasterSubCursorWrapper;->cursor:Lcom/google/android/videos/store/MasterSubCursor;

    invoke-interface {v0}, Lcom/google/android/videos/store/MasterSubCursor;->getCurrentSubRange()Lcom/google/android/videos/store/MasterSubCursor;

    move-result-object v0

    return-object v0
.end method

.method public getMasterInt(I)I
    .locals 1
    .param p1, "columnIndex"    # I

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingMasterSubCursorWrapper;->cursor:Lcom/google/android/videos/store/MasterSubCursor;

    invoke-interface {v0, p1}, Lcom/google/android/videos/store/MasterSubCursor;->getMasterInt(I)I

    move-result v0

    return v0
.end method

.method public getMasterLong(I)J
    .locals 2
    .param p1, "columnIndex"    # I

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingMasterSubCursorWrapper;->cursor:Lcom/google/android/videos/store/MasterSubCursor;

    invoke-interface {v0, p1}, Lcom/google/android/videos/store/MasterSubCursor;->getMasterLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getMasterString(I)Ljava/lang/String;
    .locals 1
    .param p1, "columnIndex"    # I

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingMasterSubCursorWrapper;->cursor:Lcom/google/android/videos/store/MasterSubCursor;

    invoke-interface {v0, p1}, Lcom/google/android/videos/store/MasterSubCursor;->getMasterString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNonEmptySubRangeCount()I
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingMasterSubCursorWrapper;->cursor:Lcom/google/android/videos/store/MasterSubCursor;

    invoke-interface {v0}, Lcom/google/android/videos/store/MasterSubCursor;->getNonEmptySubRangeCount()I

    move-result v0

    return v0
.end method
