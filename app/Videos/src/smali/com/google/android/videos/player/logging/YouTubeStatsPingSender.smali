.class public Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;
.super Landroid/os/Handler;
.source "YouTubeStatsPingSender.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;
.implements Lcom/google/android/videos/converter/RequestConverter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/Handler;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;",
        "Ljava/lang/Void;",
        ">;",
        "Lcom/google/android/videos/converter/RequestConverter",
        "<",
        "Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        ">;"
    }
.end annotation


# instance fields
.field private failedSessionNonce:Ljava/lang/String;

.field private final pendingPings:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;",
            ">;"
        }
    .end annotation
.end field

.field private final pingCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final pingRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private sendingPing:Z


# direct methods
.method public constructor <init>(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Ljava/util/concurrent/Executor;)V
    .locals 3
    .param p1, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .param p2, "httpClient"    # Lorg/apache/http/client/HttpClient;
    .param p3, "networkExecutor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 69
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 70
    sget-object v2, Lcom/google/android/videos/converter/HttpResponseConverter;->VOID:Lcom/google/android/videos/converter/HttpResponseConverter;

    invoke-static {p2, v2}, Lcom/google/android/videos/async/HttpRequester;->create(Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/async/HttpRequester;

    move-result-object v1

    .line 72
    .local v1, "httpRequester":Lcom/google/android/videos/async/HttpRequester;, "Lcom/google/android/videos/async/HttpRequester<Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/Void;>;"
    invoke-static {p1, p0, v1}, Lcom/google/android/videos/async/HttpAuthenticatingRequester;->create(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/converter/RequestConverter;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/HttpAuthenticatingRequester;

    move-result-object v0

    .line 74
    .local v0, "httpAuthenticatingRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;Ljava/lang/Void;>;"
    invoke-static {p3, v0}, Lcom/google/android/videos/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/AsyncRequester;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->pingRequester:Lcom/google/android/videos/async/Requester;

    .line 75
    invoke-static {p0, p0}, Lcom/google/android/videos/async/HandlerCallback;->create(Landroid/os/Handler;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/HandlerCallback;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->pingCallback:Lcom/google/android/videos/async/Callback;

    .line 76
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->pendingPings:Ljava/util/Queue;

    .line 77
    return-void
.end method

.method private maybeSendNextPing()V
    .locals 3

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->sendingPing:Z

    if-eqz v0, :cond_1

    .line 97
    :cond_0
    :goto_0
    return-void

    .line 92
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->failedSessionNonce:Ljava/lang/String;

    .line 93
    iget-object v0, p0, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->pendingPings:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->sendingPing:Z

    .line 94
    iget-boolean v0, p0, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->sendingPing:Z

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->pingRequester:Lcom/google/android/videos/async/Requester;

    iget-object v1, p0, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->pendingPings:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->pingCallback:Lcom/google/android/videos/async/Callback;

    invoke-interface {v0, v1, v2}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    goto :goto_0

    .line 93
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 34
    check-cast p1, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->convertRequest(Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public convertRequest(Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 101
    iget-object v0, p1, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;->uri:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/videos/async/HttpUriRequestFactory;->createGet(Ljava/lang/String;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 136
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 143
    :goto_0
    return-void

    .line 138
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->pingRequester:Lcom/google/android/videos/async/Requester;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;

    iget-object v2, p0, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->pingCallback:Lcom/google/android/videos/async/Callback;

    invoke-interface {v1, v0, v2}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    goto :goto_0

    .line 136
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onError(Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;Ljava/lang/Exception;)V
    .locals 6
    .param p1, "ping"    # Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;
    .param p2, "error"    # Ljava/lang/Exception;

    .prologue
    const/4 v4, 0x0

    .line 112
    instance-of v2, p2, Lorg/apache/http/client/HttpResponseException;

    if-eqz v2, :cond_1

    move-object v0, p2

    .line 113
    check-cast v0, Lorg/apache/http/client/HttpResponseException;

    .line 114
    .local v0, "httpResponseException":Lorg/apache/http/client/HttpResponseException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to send yt stats ping. Status code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 116
    iget-object v2, p1, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;->sessionNonce:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->failedSessionNonce:Ljava/lang/String;

    .line 117
    :goto_0
    iget-object v2, p0, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->pendingPings:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 118
    iget-object v2, p0, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->pendingPings:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;

    iget-object v1, v2, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;->sessionNonce:Ljava/lang/String;

    .line 119
    .local v1, "sessionNonce":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->failedSessionNonce:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 120
    iget-object v2, p0, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->pendingPings:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    goto :goto_0

    .line 126
    .end local v1    # "sessionNonce":Ljava/lang/String;
    :cond_0
    iput-boolean v4, p0, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->sendingPing:Z

    .line 127
    invoke-direct {p0}, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->maybeSendNextPing()V

    .line 132
    .end local v0    # "httpResponseException":Lorg/apache/http/client/HttpResponseException;
    :goto_1
    return-void

    .line 130
    :cond_1
    invoke-virtual {p0, v4, p1}, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    const-wide/32 v4, 0x493e0

    invoke-virtual {p0, v2, v4, v5}, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_1
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 34
    check-cast p1, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->onError(Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;Ljava/lang/Void;)V
    .locals 1
    .param p1, "ping"    # Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;
    .param p2, "response"    # Ljava/lang/Void;

    .prologue
    .line 106
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->sendingPing:Z

    .line 107
    invoke-direct {p0}, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->maybeSendNextPing()V

    .line 108
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 34
    check-cast p1, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/lang/Void;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->onResponse(Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;Ljava/lang/Void;)V

    return-void
.end method

.method public sendPing(Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;

    .prologue
    .line 80
    iget-object v0, p1, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender$Ping;->sessionNonce:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->failedSessionNonce:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    :goto_0
    return-void

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->pendingPings:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 84
    invoke-direct {p0}, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;->maybeSendNextPing()V

    goto :goto_0
.end method
