.class public Lcom/google/android/videos/activity/MovieDetailsActivity;
.super Lcom/google/android/videos/activity/AssetDetailsActivity;
.source "MovieDetailsActivity.java"

# interfaces
.implements Lcom/google/android/repolib/observers/Updatable;
.implements Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;
.implements Lcom/google/android/videos/ui/MovieDetailsFlowHelper$InfoPanelItemClickListener;
.implements Lcom/google/android/videos/ui/RemoveItemDialogFragment$OnRemovedListener;
.implements Lcom/google/android/videos/ui/StatusHelper$OnRetryListener;
.implements Lcom/google/android/videos/ui/VerticalsHelper$OnContentChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/activity/MovieDetailsActivity$MetaDataDialogFragment;,
        Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;,
        Lcom/google/android/videos/activity/MovieDetailsActivity$DownloadStatusQuery;,
        Lcom/google/android/videos/activity/MovieDetailsActivity$DatabaseListener;,
        Lcom/google/android/videos/activity/MovieDetailsActivity$SeeMoreOnClickListener;,
        Lcom/google/android/videos/activity/MovieDetailsActivity$SharedElements;
    }
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private allowDownloads:Z

.field private backgroundImageTinter:Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;

.field private bannerTextHelper:Lcom/google/android/videos/ui/BannerTextHelper;

.field private currentPurchaseRequest:Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

.field private database:Lcom/google/android/videos/store/Database;

.field private databaseListener:Lcom/google/android/videos/activity/MovieDetailsActivity$DatabaseListener;

.field private downloadStatusCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private flowLayoutManager:Lcom/google/android/play/layout/FlowLayoutManager;

.field private haveHeaderColor:Z

.field private initializedSuggestions:Z

.field private movieDetailsFlowHelper:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

.field private networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

.field private pendingPurchaseRequest:Z

.field private pinHelper:Lcom/google/android/videos/ui/PinHelper;

.field private pinned:Ljava/lang/Boolean;

.field private pinningErrorDialog:Z

.field private pinningStatus:Ljava/lang/Integer;

.field private pinningStatusReason:Ljava/lang/Integer;

.field private posterLaidOut:Z

.field private posterLoaded:Z

.field private posterStoreScreenshotRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private purchaseCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private purchaseSyncCallback:Lcom/google/android/videos/async/NewCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/NewCallback",
            "<",
            "Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private rootUiElementNode:Lcom/google/android/videos/logging/RootUiElementNode;

.field private startDownload:Z

.field private started:Z

.field private statusHelper:Lcom/google/android/videos/ui/StatusHelper;

.field private storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

.field private suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

.field private suggestionsEnabled:Z

.field private suggestionsHelper:Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;

.field private syncCompleted:Z

.field private syncException:Ljava/lang/Exception;

.field private volatile syncTaskControl:Lcom/google/android/videos/async/TaskControl;

.field private video:Lcom/google/android/videos/store/VideoMetadata;

.field private videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;-><init>()V

    .line 1111
    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/videos/activity/MovieDetailsActivity;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/activity/MovieDetailsActivity;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/google/android/videos/activity/MovieDetailsActivity;->onMovieMetadataUpdated(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/videos/activity/MovieDetailsActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/activity/MovieDetailsActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/google/android/videos/activity/MovieDetailsActivity;->onPosterUpdated(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/videos/activity/MovieDetailsActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/activity/MovieDetailsActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 111
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/activity/MovieDetailsActivity;->onPinningStateChanged(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/videos/activity/MovieDetailsActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/activity/MovieDetailsActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/google/android/videos/activity/MovieDetailsActivity;->onScreenshotUpdated(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/videos/activity/MovieDetailsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/activity/MovieDetailsActivity;

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->onSyncSuccess()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/videos/activity/MovieDetailsActivity;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/activity/MovieDetailsActivity;
    .param p1, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/google/android/videos/activity/MovieDetailsActivity;->onSyncError(Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/videos/activity/MovieDetailsActivity;)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/activity/MovieDetailsActivity;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->currentPurchaseRequest:Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/videos/activity/MovieDetailsActivity;Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/activity/MovieDetailsActivity;
    .param p1, "x1"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->currentPurchaseRequest:Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/videos/activity/MovieDetailsActivity;Landroid/database/Cursor;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/activity/MovieDetailsActivity;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/google/android/videos/activity/MovieDetailsActivity;->onPurchaseCursor(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/videos/activity/MovieDetailsActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/activity/MovieDetailsActivity;

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->pendingPurchaseRequest:Z

    return v0
.end method

.method static synthetic access$602(Lcom/google/android/videos/activity/MovieDetailsActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/activity/MovieDetailsActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 111
    iput-boolean p1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->pendingPurchaseRequest:Z

    return p1
.end method

.method static synthetic access$700(Lcom/google/android/videos/activity/MovieDetailsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/activity/MovieDetailsActivity;

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->refreshPurchase()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/videos/activity/MovieDetailsActivity;Landroid/database/Cursor;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/activity/MovieDetailsActivity;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/google/android/videos/activity/MovieDetailsActivity;->onDownloadStatusCursor(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/videos/activity/MovieDetailsActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/activity/MovieDetailsActivity;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->account:Ljava/lang/String;

    return-object v0
.end method

.method public static createAnimationBundleV21(Landroid/app/Activity;Landroid/content/Intent;Lcom/google/android/videos/activity/MovieDetailsActivity$SharedElements;)Landroid/os/Bundle;
    .locals 5
    .param p0, "fromActivity"    # Landroid/app/Activity;
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "from"    # Lcom/google/android/videos/activity/MovieDetailsActivity$SharedElements;

    .prologue
    const/4 v4, 0x1

    .line 154
    const-string v3, "run_animation"

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 156
    const-string v3, "video_id"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 158
    .local v0, "assetId":Ljava/lang/String;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 160
    .local v2, "transitionPairs":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Landroid/view/View;Ljava/lang/String;>;>;"
    iget-object v3, p2, Lcom/google/android/videos/activity/MovieDetailsActivity$SharedElements;->poster:Landroid/view/View;

    if-eqz v3, :cond_0

    .line 161
    const-string v3, "have_poster"

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 162
    const v3, 0x7f0b0077

    invoke-static {p0, v3, v0}, Lcom/google/android/videos/ui/TransitionUtil;->encodeTransitionName(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 164
    .local v1, "transitionName":Ljava/lang/String;
    iget-object v3, p2, Lcom/google/android/videos/activity/MovieDetailsActivity$SharedElements;->poster:Landroid/view/View;

    invoke-static {v3, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    .end local v1    # "transitionName":Ljava/lang/String;
    :cond_0
    iget-object v3, p2, Lcom/google/android/videos/activity/MovieDetailsActivity$SharedElements;->infoArea:Landroid/view/View;

    if-eqz v3, :cond_1

    .line 168
    const-string v3, "have_info_area"

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 169
    const v3, 0x7f0b0075

    invoke-static {p0, v3, v0}, Lcom/google/android/videos/ui/TransitionUtil;->encodeTransitionName(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 171
    .restart local v1    # "transitionName":Ljava/lang/String;
    iget-object v3, p2, Lcom/google/android/videos/activity/MovieDetailsActivity$SharedElements;->infoArea:Landroid/view/View;

    invoke-static {v3, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 174
    .end local v1    # "transitionName":Ljava/lang/String;
    :cond_1
    iget-object v3, p2, Lcom/google/android/videos/activity/MovieDetailsActivity$SharedElements;->screenshot:Landroid/view/View;

    if-eqz v3, :cond_2

    .line 175
    const-string v3, "have_screenshot"

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 176
    const v3, 0x7f0b0078

    invoke-static {p0, v3, v0}, Lcom/google/android/videos/ui/TransitionUtil;->encodeTransitionName(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 178
    .restart local v1    # "transitionName":Ljava/lang/String;
    iget-object v3, p2, Lcom/google/android/videos/activity/MovieDetailsActivity$SharedElements;->screenshot:Landroid/view/View;

    invoke-static {v3, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 181
    .end local v1    # "transitionName":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/Window;->setSharedElementsUseOverlay(Z)V

    .line 183
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Landroid/util/Pair;

    invoke-interface {v2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Landroid/util/Pair;

    invoke-static {p0, v3}, Landroid/app/ActivityOptions;->makeSceneTransitionAnimation(Landroid/app/Activity;[Landroid/util/Pair;)Landroid/app/ActivityOptions;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v3

    return-object v3
.end method

.method public static createIntent(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/videos/store/VideoMetadata;Lcom/google/android/videos/store/VideoDownloadStatus;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoData"    # Lcom/google/android/videos/store/VideoMetadata;
    .param p3, "downloadStatus"    # Lcom/google/android/videos/store/VideoDownloadStatus;

    .prologue
    .line 132
    iget-object v1, p2, Lcom/google/android/videos/store/VideoMetadata;->id:Ljava/lang/String;

    invoke-static {p0, p1, v1}, Lcom/google/android/videos/activity/MovieDetailsActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 133
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "video_metadata"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 134
    const-string v1, "video_download_status"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 135
    return-object v0
.end method

.method public static createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;

    .prologue
    .line 117
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/google/android/videos/activity/MovieDetailsActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "video_id"

    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "authAccount"

    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "startDownload"    # Z
    .param p4, "pinningErrorDialog"    # Z

    .prologue
    .line 124
    invoke-static {p0, p1, p2}, Lcom/google/android/videos/activity/MovieDetailsActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 125
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "start_download"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 126
    const-string v1, "pinning_error_dialog"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 127
    return-object v0
.end method

.method private initSuggestionsIfAppropriate()V
    .locals 2

    .prologue
    .line 951
    iget-boolean v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->suggestionsEnabled:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->initializedSuggestions:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->pendingEnterTransition:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->runningEnterTransition:Z

    if-nez v0, :cond_0

    .line 953
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->suggestionsHelper:Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;

    iget-object v1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->init(Ljava/lang/String;)V

    .line 954
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->initializedSuggestions:Z

    .line 956
    :cond_0
    return-void
.end method

.method private onDownloadStatusCursor(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 978
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 979
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->movieDetailsFlowHelper:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    invoke-direct {p0, p1}, Lcom/google/android/videos/activity/MovieDetailsActivity;->readDownloadStatus(Landroid/database/Cursor;)Lcom/google/android/videos/store/VideoDownloadStatus;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->onVideoDownloadStatus(Lcom/google/android/videos/store/VideoDownloadStatus;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 982
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 984
    return-void

    .line 982
    :catchall_0
    move-exception v0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private onMovieMetadataUpdated(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 823
    .local p1, "movieIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videoId:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 824
    invoke-direct {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->refreshPurchase()V

    .line 826
    :cond_0
    return-void
.end method

.method private onPinningStateChanged(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;

    .prologue
    .line 835
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->account:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videoId:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 836
    invoke-direct {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->refreshDownloadStatus()V

    .line 838
    :cond_0
    return-void
.end method

.method private onPosterUpdated(Ljava/lang/String;)V
    .locals 1
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 829
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videoId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 830
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->movieDetailsFlowHelper:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->loadPoster()V

    .line 832
    :cond_0
    return-void
.end method

.method private onPurchaseCursor(Landroid/database/Cursor;)V
    .locals 17
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 886
    :try_start_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 887
    sget v1, Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;->PURCHASE_TYPE:I

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const/4 v12, 0x1

    .line 888
    .local v12, "isRental":Z
    :goto_0
    sget v1, Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;->FORMAT_TYPE:I

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    const/4 v13, 0x1

    .line 889
    .local v13, "hdPurchased":Z
    :goto_1
    if-eqz v13, :cond_3

    sget v1, Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;->BADGE_SURROUND_SOUND:I

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v1

    if-eqz v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/videos/Config;->allowSurroundSoundFormats()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v14, 0x1

    .line 892
    .local v14, "surroundSoundPurchasedAndAvailable":Z
    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/videos/Config;->knowledgeEnabled()Z

    move-result v1

    if-eqz v1, :cond_4

    sget v1, Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;->BADGE_KNOWLEDGE:I

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v15, 0x1

    .line 894
    .local v15, "knowledgeEnabled":Z
    :goto_3
    sget v1, Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;->BADGE_CAPTION:I

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v16

    .line 895
    .local v16, "captionAvailable":Z
    new-instance v1, Lcom/google/android/videos/store/VideoMetadata;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videoId:Ljava/lang/String;

    sget v3, Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;->TITLE:I

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;->DESCRIPTION:I

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;->RELEASE_YEAR:I

    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v5

    if-eqz v5, :cond_5

    const/4 v5, 0x0

    :goto_4
    sget v6, Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;->VIDEO_DURATION_SECONDS:I

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    sget v7, Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;->DIRECTORS:I

    move-object/from16 v0, p1

    invoke-static {v0, v7}, Lcom/google/android/videos/utils/DbUtils;->getStringList(Landroid/database/Cursor;I)Ljava/util/List;

    move-result-object v7

    sget v8, Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;->WRITERS:I

    move-object/from16 v0, p1

    invoke-static {v0, v8}, Lcom/google/android/videos/utils/DbUtils;->getStringList(Landroid/database/Cursor;I)Ljava/util/List;

    move-result-object v8

    sget v9, Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;->ACTORS:I

    move-object/from16 v0, p1

    invoke-static {v0, v9}, Lcom/google/android/videos/utils/DbUtils;->getStringList(Landroid/database/Cursor;I)Ljava/util/List;

    move-result-object v9

    sget v10, Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;->PRODUCERS:I

    move-object/from16 v0, p1

    invoke-static {v0, v10}, Lcom/google/android/videos/utils/DbUtils;->getStringList(Landroid/database/Cursor;I)Ljava/util/List;

    move-result-object v10

    sget v11, Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;->RATING_NAME:I

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct/range {v1 .. v16}, Lcom/google/android/videos/store/VideoMetadata;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;ZZZZZ)V

    invoke-direct/range {p0 .. p1}, Lcom/google/android/videos/activity/MovieDetailsActivity;->readDownloadStatus(Landroid/database/Cursor;)Lcom/google/android/videos/store/VideoDownloadStatus;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2}, Lcom/google/android/videos/activity/MovieDetailsActivity;->onVideoMetadata(Lcom/google/android/videos/store/VideoMetadata;Lcom/google/android/videos/store/VideoDownloadStatus;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 916
    .end local v12    # "isRental":Z
    .end local v13    # "hdPurchased":Z
    .end local v14    # "surroundSoundPurchasedAndAvailable":Z
    .end local v15    # "knowledgeEnabled":Z
    .end local v16    # "captionAvailable":Z
    :cond_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    .line 918
    return-void

    .line 887
    :cond_1
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 888
    .restart local v12    # "isRental":Z
    :cond_2
    const/4 v13, 0x0

    goto/16 :goto_1

    .line 889
    .restart local v13    # "hdPurchased":Z
    :cond_3
    const/4 v14, 0x0

    goto/16 :goto_2

    .line 892
    .restart local v14    # "surroundSoundPurchasedAndAvailable":Z
    :cond_4
    const/4 v15, 0x0

    goto :goto_3

    .line 895
    .restart local v15    # "knowledgeEnabled":Z
    .restart local v16    # "captionAvailable":Z
    :cond_5
    :try_start_1
    sget v5, Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;->RELEASE_YEAR:I

    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v5

    goto :goto_4

    .line 916
    .end local v12    # "isRental":Z
    .end local v13    # "hdPurchased":Z
    .end local v14    # "surroundSoundPurchasedAndAvailable":Z
    .end local v15    # "knowledgeEnabled":Z
    .end local v16    # "captionAvailable":Z
    :catchall_0
    move-exception v1

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private onScreenshotUpdated(Ljava/lang/String;)V
    .locals 1
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 841
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videoId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 842
    invoke-virtual {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->loadBackgroundImage()V

    .line 844
    :cond_0
    return-void
.end method

.method private onSyncError(Ljava/lang/Exception;)V
    .locals 1
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 879
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->syncCompleted:Z

    .line 880
    iput-object p1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->syncException:Ljava/lang/Exception;

    .line 881
    invoke-direct {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->updateVisibilities()V

    .line 882
    return-void
.end method

.method private onSyncSuccess()V
    .locals 1

    .prologue
    .line 874
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->syncCompleted:Z

    .line 875
    invoke-direct {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->updateVisibilities()V

    .line 876
    return-void
.end method

.method private onVideoMetadata(Lcom/google/android/videos/store/VideoMetadata;Lcom/google/android/videos/store/VideoDownloadStatus;)V
    .locals 5
    .param p1, "video"    # Lcom/google/android/videos/store/VideoMetadata;
    .param p2, "downloadStatus"    # Lcom/google/android/videos/store/VideoDownloadStatus;

    .prologue
    const/4 v1, 0x0

    .line 922
    invoke-virtual {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "video_metadata"

    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 923
    invoke-virtual {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "video_download_status"

    invoke-virtual {v2, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 925
    if-eqz p1, :cond_4

    const/4 v0, 0x1

    .line 926
    .local v0, "hadVideo":Z
    :goto_0
    iput-object p1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->video:Lcom/google/android/videos/store/VideoMetadata;

    .line 927
    iget-object v2, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->movieDetailsFlowHelper:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    invoke-virtual {v2, p1, p2}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->onVideo(Lcom/google/android/videos/store/VideoMetadata;Lcom/google/android/videos/store/VideoDownloadStatus;)V

    .line 928
    iget-object v2, p1, Lcom/google/android/videos/store/VideoMetadata;->title:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/android/videos/activity/MovieDetailsActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 930
    invoke-direct {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->initSuggestionsIfAppropriate()V

    .line 932
    iget-boolean v2, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->startDownload:Z

    if-eqz v2, :cond_0

    .line 933
    iget-object v2, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->pinHelper:Lcom/google/android/videos/ui/PinHelper;

    iget-object v3, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->account:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videoId:Ljava/lang/String;

    invoke-virtual {v2, p0, v3, v4}, Lcom/google/android/videos/ui/PinHelper;->pinVideo(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 934
    iput-boolean v1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->startDownload:Z

    .line 936
    :cond_0
    iget-boolean v2, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->pinningErrorDialog:Z

    if-eqz v2, :cond_2

    .line 937
    iget-boolean v2, p2, Lcom/google/android/videos/store/VideoDownloadStatus;->pinned:Z

    if-nez v2, :cond_1

    iget-object v2, p2, Lcom/google/android/videos/store/VideoDownloadStatus;->pinningStatus:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    iget-object v2, p2, Lcom/google/android/videos/store/VideoDownloadStatus;->pinningStatus:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    .line 939
    invoke-direct {p0, p2}, Lcom/google/android/videos/activity/MovieDetailsActivity;->showPinningErrorDialog(Lcom/google/android/videos/store/VideoDownloadStatus;)V

    .line 941
    :cond_1
    iput-boolean v1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->pinningErrorDialog:Z

    .line 944
    :cond_2
    if-nez v0, :cond_3

    .line 945
    invoke-virtual {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->supportInvalidateOptionsMenu()V

    .line 947
    :cond_3
    invoke-direct {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->updateVisibilities()V

    .line 948
    return-void

    .end local v0    # "hadVideo":Z
    :cond_4
    move v0, v1

    .line 925
    goto :goto_0
.end method

.method private readDownloadStatus(Landroid/database/Cursor;)Lcom/google/android/videos/store/VideoDownloadStatus;
    .locals 8
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 987
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/google/android/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->pinned:Ljava/lang/Boolean;

    .line 988
    const/4 v0, 0x2

    invoke-static {p1, v0}, Lcom/google/android/videos/utils/DbUtils;->getIntOrNull(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->pinningStatus:Ljava/lang/Integer;

    .line 989
    const/4 v0, 0x3

    invoke-static {p1, v0}, Lcom/google/android/videos/utils/DbUtils;->getIntOrNull(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->pinningStatusReason:Ljava/lang/Integer;

    .line 990
    new-instance v0, Lcom/google/android/videos/store/VideoDownloadStatus;

    iget-object v1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->pinned:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    const/4 v2, 0x1

    invoke-static {p1, v2}, Lcom/google/android/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v2

    iget-object v3, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->pinningStatus:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->pinningStatusReason:Ljava/lang/Integer;

    const/4 v5, 0x6

    invoke-static {p1, v5}, Lcom/google/android/videos/utils/DbUtils;->getIntOrNull(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x4

    invoke-static {p1, v6}, Lcom/google/android/videos/utils/DbUtils;->getLongOrNull(Landroid/database/Cursor;I)Ljava/lang/Long;

    move-result-object v6

    const/4 v7, 0x5

    invoke-static {p1, v7}, Lcom/google/android/videos/utils/DbUtils;->getLongOrNull(Landroid/database/Cursor;I)Ljava/lang/Long;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/store/VideoDownloadStatus;-><init>(ZZLjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;)V

    return-object v0
.end method

.method private refreshDownloadStatus()V
    .locals 4

    .prologue
    .line 867
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    iget-object v1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->account:Ljava/lang/String;

    sget-object v2, Lcom/google/android/videos/activity/MovieDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videoId:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/google/android/videos/store/PurchaseRequests;->createPurchaseRequestForUser(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->downloadStatusCallback:Lcom/google/android/videos/async/Callback;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/videos/async/Callback;)V

    .line 871
    return-void
.end method

.method private refreshPurchase()V
    .locals 3

    .prologue
    .line 857
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->currentPurchaseRequest:Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    if-eqz v0, :cond_0

    .line 858
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->pendingPurchaseRequest:Z

    .line 864
    :goto_0
    return-void

    .line 860
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->account:Ljava/lang/String;

    sget-object v1, Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videoId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/videos/store/PurchaseRequests;->createPurchaseRequestForUser(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->currentPurchaseRequest:Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .line 862
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    iget-object v1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->currentPurchaseRequest:Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    iget-object v2, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->purchaseCallback:Lcom/google/android/videos/async/Callback;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/videos/async/Callback;)V

    goto :goto_0
.end method

.method private showPinningErrorDialog(Lcom/google/android/videos/store/VideoDownloadStatus;)V
    .locals 7
    .param p1, "status"    # Lcom/google/android/videos/store/VideoDownloadStatus;

    .prologue
    .line 813
    iget-object v1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videoId:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/videos/store/VideoDownloadStatus;->pinningStatusReason:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v4, p1, Lcom/google/android/videos/store/VideoDownloadStatus;->downloadSize:Ljava/lang/Long;

    iget-object v5, p1, Lcom/google/android/videos/store/VideoDownloadStatus;->drmErrorCode:Ljava/lang/Integer;

    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->video:Lcom/google/android/videos/store/VideoMetadata;

    iget-boolean v6, v0, Lcom/google/android/videos/store/VideoMetadata;->isRental:Z

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/google/android/videos/ui/PinHelper;->showErrorDialog(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Long;Ljava/lang/Integer;Z)V

    .line 815
    return-void
.end method

.method private startEnterTransitionIfReady()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 539
    sget v3, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v4, 0x15

    if-lt v3, v4, :cond_2

    .line 540
    invoke-virtual {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "have_screenshot"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_3

    const/4 v0, 0x1

    .line 541
    .local v0, "requireHeaderColor":Z
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "have_poster"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 542
    .local v1, "requirePoster":Z
    iget-boolean v2, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->posterLaidOut:Z

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->posterLoaded:Z

    if-nez v2, :cond_0

    if-nez v1, :cond_2

    :cond_0
    iget-boolean v2, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->haveHeaderColor:Z

    if-nez v2, :cond_1

    if-nez v0, :cond_2

    .line 545
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->startEnterTransition()V

    .line 548
    .end local v0    # "requireHeaderColor":Z
    .end local v1    # "requirePoster":Z
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 540
    goto :goto_0
.end method

.method private startSync()V
    .locals 2

    .prologue
    .line 683
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->syncTaskControl:Lcom/google/android/videos/async/TaskControl;

    invoke-static {v0}, Lcom/google/android/videos/async/TaskControl;->cancel(Lcom/google/android/videos/async/TaskControl;)V

    .line 684
    new-instance v0, Lcom/google/android/videos/async/TaskControl;

    invoke-direct {v0}, Lcom/google/android/videos/async/TaskControl;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->syncTaskControl:Lcom/google/android/videos/async/TaskControl;

    .line 685
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->syncCompleted:Z

    .line 686
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->syncException:Ljava/lang/Exception;

    .line 687
    invoke-direct {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->updateVisibilities()V

    .line 688
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    iget-object v1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->account:Ljava/lang/String;

    invoke-virtual {v0, v1, p0, p0}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getAuthToken(Ljava/lang/String;Landroid/app/Activity;Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;)V

    .line 689
    return-void
.end method

.method private updateVisibilities()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 959
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->video:Lcom/google/android/videos/store/VideoMetadata;

    if-eqz v0, :cond_0

    .line 960
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->contentLayout:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 961
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->statusHelper:Lcom/google/android/videos/ui/StatusHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/StatusHelper;->hide()V

    .line 974
    :goto_0
    return-void

    .line 963
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->contentLayout:Landroid/support/v7/widget/RecyclerView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 964
    iget-boolean v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->syncCompleted:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->currentPurchaseRequest:Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    if-nez v0, :cond_2

    .line 965
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->syncException:Ljava/lang/Exception;

    if-eqz v0, :cond_1

    .line 966
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->statusHelper:Lcom/google/android/videos/ui/StatusHelper;

    iget-object v1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->errorHelper:Lcom/google/android/videos/utils/ErrorHelper;

    iget-object v2, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->syncException:Ljava/lang/Exception;

    invoke-virtual {v1, v2}, Lcom/google/android/videos/utils/ErrorHelper;->humanize(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/ui/StatusHelper;->setErrorMessage(Ljava/lang/CharSequence;Z)V

    goto :goto_0

    .line 968
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->statusHelper:Lcom/google/android/videos/ui/StatusHelper;

    const v1, 0x7f0b014a

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/ui/StatusHelper;->setErrorMessage(IZ)V

    goto :goto_0

    .line 971
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->statusHelper:Lcom/google/android/videos/ui/StatusHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/StatusHelper;->setLoading()V

    goto :goto_0
.end method


# virtual methods
.method protected createNonSharedTransition(Z)Landroid/transition/TransitionSet;
    .locals 14
    .param p1, "isEnter"    # Z

    .prologue
    .line 441
    invoke-super {p0, p1}, Lcom/google/android/videos/activity/AssetDetailsActivity;->createNonSharedTransition(Z)Landroid/transition/TransitionSet;

    move-result-object v8

    .line 443
    .local v8, "set":Landroid/transition/TransitionSet;
    invoke-virtual {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v10

    const-string v11, "have_screenshot"

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 444
    .local v4, "haveSharedScreenshot":Z
    invoke-virtual {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v10

    const-string v11, "have_poster"

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 445
    .local v3, "haveSharedPoster":Z
    invoke-virtual {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v10

    const-string v11, "have_info_area"

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 447
    .local v2, "haveSharedInfoArea":Z
    const v10, 0x7f0b0075

    iget-object v11, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videoId:Ljava/lang/String;

    invoke-static {p0, v10, v11}, Lcom/google/android/videos/ui/TransitionUtil;->encodeTransitionName(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 450
    .local v5, "infoAreaName":Ljava/lang/String;
    if-nez v4, :cond_5

    .line 451
    if-eqz p1, :cond_3

    .line 452
    new-instance v7, Landroid/transition/Fade;

    invoke-direct {v7}, Landroid/transition/Fade;-><init>()V

    .line 453
    .local v7, "screenshot":Landroid/transition/Fade;
    iget-object v10, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->backgroundImageView:Lcom/google/android/videos/ui/FocusedImageView;

    invoke-virtual {v7, v10}, Landroid/transition/Fade;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 454
    invoke-virtual {v8, v7}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 456
    new-instance v10, Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;

    invoke-direct {v10}, Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;-><init>()V

    iput-object v10, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->backgroundImageTinter:Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;

    .line 457
    iget v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->backgroundColor:I

    .line 458
    .local v0, "color":I
    iget-object v10, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->backgroundImageView:Lcom/google/android/videos/ui/FocusedImageView;

    invoke-virtual {v10, v0}, Lcom/google/android/videos/ui/FocusedImageView;->setColorFilter(I)V

    .line 459
    iget-object v10, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->backgroundImageTinter:Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;

    invoke-virtual {v10, v0}, Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;->setFromColor(I)V

    .line 460
    iget-object v10, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->backgroundImageTinter:Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;->setToColor(I)V

    .line 461
    iget-object v10, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->backgroundImageTinter:Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;

    iget-object v11, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->backgroundImageView:Lcom/google/android/videos/ui/FocusedImageView;

    invoke-virtual {v10, v11}, Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 462
    iget-object v10, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->backgroundImageTinter:Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;

    const-wide/16 v12, 0x12c

    invoke-virtual {v10, v12, v13}, Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;->setStartDelay(J)Landroid/transition/Transition;

    .line 463
    iget-object v10, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->backgroundImageTinter:Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;

    invoke-virtual {v8, v10}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 470
    .end local v0    # "color":I
    :goto_0
    new-instance v1, Landroid/transition/Fade;

    invoke-direct {v1}, Landroid/transition/Fade;-><init>()V

    .line 471
    .local v1, "fades":Landroid/transition/Fade;
    if-eqz p1, :cond_4

    const-wide/16 v10, 0xc8

    :goto_1
    invoke-virtual {v1, v10, v11}, Landroid/transition/Fade;->setStartDelay(J)Landroid/transition/Transition;

    .line 472
    invoke-virtual {v8, v1}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 473
    const v10, 0x7f0f00a8

    invoke-virtual {v1, v10}, Landroid/transition/Fade;->addTarget(I)Landroid/transition/Transition;

    .line 474
    const v10, 0x7f0b007d

    invoke-virtual {p0, v10}, Lcom/google/android/videos/activity/MovieDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10}, Landroid/transition/Fade;->addTarget(Ljava/lang/String;)Landroid/transition/Transition;

    .line 475
    const v10, 0x7f0b007b

    invoke-virtual {p0, v10}, Lcom/google/android/videos/activity/MovieDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10}, Landroid/transition/Fade;->addTarget(Ljava/lang/String;)Landroid/transition/Transition;

    .line 476
    const v10, 0x7f0b007c

    invoke-virtual {p0, v10}, Lcom/google/android/videos/activity/MovieDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10}, Landroid/transition/Fade;->addTarget(Ljava/lang/String;)Landroid/transition/Transition;

    .line 478
    if-nez v2, :cond_0

    .line 479
    invoke-virtual {v1, v5}, Landroid/transition/Fade;->addTarget(Ljava/lang/String;)Landroid/transition/Transition;

    .line 481
    :cond_0
    if-nez v3, :cond_1

    .line 482
    const v10, 0x7f0f00c9

    invoke-virtual {v1, v10}, Landroid/transition/Fade;->addTarget(I)Landroid/transition/Transition;

    .line 507
    .end local v1    # "fades":Landroid/transition/Fade;
    .end local v7    # "screenshot":Landroid/transition/Fade;
    :cond_1
    :goto_2
    new-instance v6, Landroid/transition/TransitionSet;

    invoke-direct {v6}, Landroid/transition/TransitionSet;-><init>()V

    .line 508
    .local v6, "playButton":Landroid/transition/TransitionSet;
    new-instance v10, Landroid/transition/Fade;

    invoke-direct {v10}, Landroid/transition/Fade;-><init>()V

    invoke-virtual {v6, v10}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 509
    new-instance v10, Lcom/google/android/videos/ui/TransitionUtil$ExpandTransition;

    invoke-direct {v10, p1}, Lcom/google/android/videos/ui/TransitionUtil$ExpandTransition;-><init>(Z)V

    invoke-virtual {v6, v10}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 510
    if-eqz p1, :cond_6

    const-wide/16 v10, 0xc8

    :goto_3
    invoke-virtual {v6, v10, v11}, Landroid/transition/TransitionSet;->setStartDelay(J)Landroid/transition/TransitionSet;

    .line 511
    const v10, 0x7f0f00e1

    invoke-virtual {v6, v10}, Landroid/transition/TransitionSet;->addTarget(I)Landroid/transition/TransitionSet;

    .line 512
    invoke-virtual {v8, v6}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 514
    if-nez p1, :cond_2

    .line 516
    new-instance v9, Landroid/transition/Slide;

    invoke-direct {v9}, Landroid/transition/Slide;-><init>()V

    .line 517
    .local v9, "slide":Landroid/transition/Transition;
    const v10, 0x7f0f00d9

    invoke-virtual {v9, v10}, Landroid/transition/Transition;->addTarget(I)Landroid/transition/Transition;

    .line 518
    const v10, 0x7f0f01f6

    invoke-virtual {v9, v10}, Landroid/transition/Transition;->addTarget(I)Landroid/transition/Transition;

    .line 519
    invoke-virtual {v8, v9}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 522
    .end local v9    # "slide":Landroid/transition/Transition;
    :cond_2
    return-object v8

    .line 465
    .end local v6    # "playButton":Landroid/transition/TransitionSet;
    :cond_3
    new-instance v7, Landroid/transition/Fade;

    invoke-direct {v7}, Landroid/transition/Fade;-><init>()V

    .line 466
    .restart local v7    # "screenshot":Landroid/transition/Fade;
    iget-object v10, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->backgroundImageView:Lcom/google/android/videos/ui/FocusedImageView;

    invoke-virtual {v7, v10}, Landroid/transition/Fade;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 467
    invoke-virtual {v8, v7}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    goto/16 :goto_0

    .line 471
    .restart local v1    # "fades":Landroid/transition/Fade;
    :cond_4
    const-wide/16 v10, 0x0

    goto/16 :goto_1

    .line 485
    .end local v1    # "fades":Landroid/transition/Fade;
    .end local v7    # "screenshot":Landroid/transition/Fade;
    :cond_5
    new-instance v9, Landroid/transition/Slide;

    invoke-direct {v9}, Landroid/transition/Slide;-><init>()V

    .line 486
    .local v9, "slide":Landroid/transition/Slide;
    new-instance v10, Lcom/google/android/videos/ui/TransitionUtil$ZeroStartDelayPropagation;

    invoke-direct {v10}, Lcom/google/android/videos/ui/TransitionUtil$ZeroStartDelayPropagation;-><init>()V

    invoke-virtual {v9, v10}, Landroid/transition/Slide;->setPropagation(Landroid/transition/TransitionPropagation;)V

    .line 487
    invoke-virtual {v8, v9}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 488
    const v10, 0x7f0f00a8

    invoke-virtual {v9, v10}, Landroid/transition/Slide;->addTarget(I)Landroid/transition/Transition;

    .line 489
    const v10, 0x7f0b007d

    invoke-virtual {p0, v10}, Lcom/google/android/videos/activity/MovieDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/transition/Slide;->addTarget(Ljava/lang/String;)Landroid/transition/Transition;

    .line 490
    const v10, 0x7f0b007b

    invoke-virtual {p0, v10}, Lcom/google/android/videos/activity/MovieDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/transition/Slide;->addTarget(Ljava/lang/String;)Landroid/transition/Transition;

    .line 491
    const v10, 0x7f0b007c

    invoke-virtual {p0, v10}, Lcom/google/android/videos/activity/MovieDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/transition/Slide;->addTarget(Ljava/lang/String;)Landroid/transition/Transition;

    .line 492
    invoke-virtual {v9, v5}, Landroid/transition/Slide;->addTarget(Ljava/lang/String;)Landroid/transition/Transition;

    .line 493
    const v10, 0x7f0f00c9

    invoke-virtual {v9, v10}, Landroid/transition/Slide;->addTarget(I)Landroid/transition/Transition;

    .line 494
    new-instance v10, Lcom/google/android/videos/activity/MovieDetailsActivity$5;

    invoke-direct {v10, p0}, Lcom/google/android/videos/activity/MovieDetailsActivity$5;-><init>(Lcom/google/android/videos/activity/MovieDetailsActivity;)V

    invoke-virtual {v9, v10}, Landroid/transition/Slide;->addListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/Transition;

    goto/16 :goto_2

    .line 510
    .end local v9    # "slide":Landroid/transition/Slide;
    .restart local v6    # "playButton":Landroid/transition/TransitionSet;
    :cond_6
    const-wide/16 v10, 0x0

    goto :goto_3
.end method

.method protected createSharedTransition(Z)Landroid/transition/TransitionSet;
    .locals 13
    .param p1, "isEnter"    # Z

    .prologue
    .line 395
    invoke-super {p0, p1}, Lcom/google/android/videos/activity/AssetDetailsActivity;->createSharedTransition(Z)Landroid/transition/TransitionSet;

    move-result-object v7

    .line 397
    .local v7, "set":Landroid/transition/TransitionSet;
    new-instance v2, Landroid/view/animation/PathInterpolator;

    const v9, 0x3ecccccd    # 0.4f

    const/4 v10, 0x0

    const v11, 0x3e4ccccd    # 0.2f

    const/high16 v12, 0x3f800000    # 1.0f

    invoke-direct {v2, v9, v10, v11, v12}, Landroid/view/animation/PathInterpolator;-><init>(FFFF)V

    .line 399
    .local v2, "interpolator":Landroid/view/animation/Interpolator;
    const v9, 0x7f0b0077

    iget-object v10, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videoId:Ljava/lang/String;

    invoke-static {p0, v9, v10}, Lcom/google/android/videos/ui/TransitionUtil;->encodeTransitionName(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 401
    .local v5, "posterTransitionName":Ljava/lang/String;
    const v9, 0x7f0b0075

    iget-object v10, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videoId:Ljava/lang/String;

    invoke-static {p0, v9, v10}, Lcom/google/android/videos/ui/TransitionUtil;->encodeTransitionName(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 404
    .local v0, "infoAreaTransitionName":Ljava/lang/String;
    new-instance v4, Landroid/transition/ChangeImageTransform;

    invoke-direct {v4}, Landroid/transition/ChangeImageTransform;-><init>()V

    .line 405
    .local v4, "posterTransform":Landroid/transition/ChangeImageTransform;
    invoke-virtual {v4, v5}, Landroid/transition/ChangeImageTransform;->addTarget(Ljava/lang/String;)Landroid/transition/Transition;

    .line 406
    invoke-virtual {v4, v2}, Landroid/transition/ChangeImageTransform;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/Transition;

    .line 407
    const-wide/16 v10, 0x12c

    invoke-virtual {v4, v10, v11}, Landroid/transition/ChangeImageTransform;->setDuration(J)Landroid/transition/Transition;

    .line 408
    invoke-virtual {v7, v4}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 410
    new-instance v1, Lcom/google/android/videos/ui/TransitionUtil$InfoBounds;

    invoke-direct {v1, v0}, Lcom/google/android/videos/ui/TransitionUtil$InfoBounds;-><init>(Ljava/lang/String;)V

    .line 411
    .local v1, "infoBounds":Lcom/google/android/videos/ui/TransitionUtil$InfoBounds;
    invoke-virtual {v1, v2}, Lcom/google/android/videos/ui/TransitionUtil$InfoBounds;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/Transition;

    .line 412
    const-wide/16 v10, 0x12c

    invoke-virtual {v1, v10, v11}, Lcom/google/android/videos/ui/TransitionUtil$InfoBounds;->setDuration(J)Landroid/transition/Transition;

    .line 413
    invoke-virtual {v7, v1}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 415
    new-instance v3, Lcom/google/android/videos/ui/TransitionUtil$MoveTopLeft;

    invoke-direct {v3}, Lcom/google/android/videos/ui/TransitionUtil$MoveTopLeft;-><init>()V

    .line 416
    .local v3, "posterMove":Landroid/transition/Transition;
    invoke-virtual {v3, v5}, Landroid/transition/Transition;->addTarget(Ljava/lang/String;)Landroid/transition/Transition;

    .line 417
    invoke-virtual {v3, v2}, Landroid/transition/Transition;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/Transition;

    .line 418
    invoke-virtual {v1}, Lcom/google/android/videos/ui/TransitionUtil$InfoBounds;->getPathMotion()Landroid/transition/PathMotion;

    move-result-object v9

    invoke-virtual {v3, v9}, Landroid/transition/Transition;->setPathMotion(Landroid/transition/PathMotion;)V

    .line 419
    const-wide/16 v10, 0x12c

    invoke-virtual {v3, v10, v11}, Landroid/transition/Transition;->setDuration(J)Landroid/transition/Transition;

    .line 420
    invoke-virtual {v7, v3}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 423
    invoke-virtual {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    const-string v10, "have_screenshot"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 424
    const v9, 0x7f0b0078

    iget-object v10, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videoId:Ljava/lang/String;

    invoke-static {p0, v9, v10}, Lcom/google/android/videos/ui/TransitionUtil;->encodeTransitionName(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 426
    .local v8, "transitionName":Ljava/lang/String;
    iget-object v9, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->backgroundImageView:Lcom/google/android/videos/ui/FocusedImageView;

    invoke-virtual {v9, v8}, Lcom/google/android/videos/ui/FocusedImageView;->setTransitionName(Ljava/lang/String;)V

    .line 428
    new-instance v6, Landroid/transition/ChangeBounds;

    invoke-direct {v6}, Landroid/transition/ChangeBounds;-><init>()V

    .line 429
    .local v6, "screenshotBounds":Landroid/transition/ChangeBounds;
    iget-object v9, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->backgroundImageView:Lcom/google/android/videos/ui/FocusedImageView;

    invoke-virtual {v6, v9}, Landroid/transition/ChangeBounds;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 430
    invoke-virtual {v6, v2}, Landroid/transition/ChangeBounds;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/Transition;

    .line 431
    const-wide/16 v10, 0x12c

    invoke-virtual {v6, v10, v11}, Landroid/transition/ChangeBounds;->setDuration(J)Landroid/transition/Transition;

    .line 432
    invoke-virtual {v7, v6}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 435
    .end local v6    # "screenshotBounds":Landroid/transition/ChangeBounds;
    .end local v8    # "transitionName":Ljava/lang/String;
    :cond_0
    return-object v7
.end method

.method protected getHeaderHeight()I
    .locals 3

    .prologue
    .line 589
    invoke-virtual {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->getStandardScreenshotHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0182

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e00f9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method protected getLayoutId()I
    .locals 1

    .prologue
    .line 575
    const v0, 0x7f0400c8

    return v0
.end method

.method protected getScreenshotHeight()I
    .locals 3

    .prologue
    .line 581
    invoke-virtual {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->getStandardScreenshotHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0181

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getSupportParentActivityIntent()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 782
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->account:Ljava/lang/String;

    const-string v1, "movies"

    invoke-static {p0, v0, v1}, Lcom/google/android/videos/activity/HomeActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public loadBackgroundImage()V
    .locals 6

    .prologue
    .line 848
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->posterStoreScreenshotRequester:Lcom/google/android/videos/async/Requester;

    if-nez v0, :cond_0

    .line 849
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getPosterStore()Lcom/google/android/videos/store/PosterStore;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/videos/store/PosterStore;->getRequester(I)Lcom/google/android/videos/async/Requester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->posterStoreScreenshotRequester:Lcom/google/android/videos/async/Requester;

    .line 852
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->posterStoreScreenshotRequester:Lcom/google/android/videos/async/Requester;

    iget-object v2, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->cpuExecutor:Ljava/util/concurrent/Executor;

    iget-object v3, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videoId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->missingBackgroundBitmap:Landroid/graphics/Bitmap;

    const/4 v5, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/ui/BitmapLoader;->setBitmapAsync(Lcom/google/android/videos/ui/BitmapLoader$BitmapPaletteView;Lcom/google/android/videos/async/Requester;Ljava/util/concurrent/Executor;Ljava/lang/Object;Landroid/graphics/Bitmap;Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;)V

    .line 854
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 601
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->onActivityResult(IILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 605
    :goto_0
    return-void

    .line 604
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/videos/activity/AssetDetailsActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onAuthenticated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "authToken"    # Ljava/lang/String;

    .prologue
    .line 693
    iget-object v1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videoId:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->createForId(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    move-result-object v0

    .line 695
    .local v0, "purchaseSyncRequest":Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;
    iget-object v1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

    iget-object v2, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->syncTaskControl:Lcom/google/android/videos/async/TaskControl;

    invoke-static {v0, v2}, Lcom/google/android/videos/async/ControllableRequest;->create(Ljava/lang/Object;Lcom/google/android/videos/async/TaskStatus;)Lcom/google/android/videos/async/ControllableRequest;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->purchaseSyncCallback:Lcom/google/android/videos/async/NewCallback;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/store/PurchaseStoreSync;->syncPurchasesForVideo(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V

    .line 697
    iget-object v1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->wishlistStoreSync:Lcom/google/android/videos/store/WishlistStoreSync;

    const/4 v2, 0x0

    invoke-static {p1, v2}, Lcom/google/android/videos/async/ControllableRequest;->create(Ljava/lang/Object;Lcom/google/android/videos/async/TaskStatus;)Lcom/google/android/videos/async/ControllableRequest;

    move-result-object v2

    invoke-static {}, Lcom/google/android/videos/async/IgnoreCallback;->get()Lcom/google/android/videos/async/NewCallback;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/store/WishlistStoreSync;->syncWishlist(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V

    .line 699
    return-void
.end method

.method public onAuthenticationError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 710
    invoke-direct {p0, p2}, Lcom/google/android/videos/activity/MovieDetailsActivity;->onSyncError(Ljava/lang/Exception;)V

    .line 711
    return-void
.end method

.method protected onBackgroundColorExtracted(IZ)V
    .locals 1
    .param p1, "color"    # I
    .param p2, "isFinal"    # Z

    .prologue
    .line 552
    iget-boolean v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->pendingEnterTransition:Z

    if-nez v0, :cond_1

    .line 565
    :cond_0
    :goto_0
    return-void

    .line 556
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->backgroundImageTinter:Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;

    if-eqz v0, :cond_2

    .line 557
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->backgroundImageTinter:Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;->setFromColor(I)V

    .line 558
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->backgroundImageView:Lcom/google/android/videos/ui/FocusedImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/ui/FocusedImageView;->setColorFilter(I)V

    .line 561
    :cond_2
    if-eqz p2, :cond_0

    .line 562
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->haveHeaderColor:Z

    .line 563
    invoke-direct {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->startEnterTransitionIfReady()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 24
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 253
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v23

    .line 254
    .local v23, "intent":Landroid/content/Intent;
    const-string v2, "authAccount"

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->account:Ljava/lang/String;

    .line 255
    const-string v2, "start_download"

    const/4 v3, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->startDownload:Z

    .line 256
    const-string v2, "pinning_error_dialog"

    const/4 v3, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->pinningErrorDialog:Z

    .line 257
    const-string v2, "video_id"

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videoId:Ljava/lang/String;

    .line 260
    invoke-super/range {p0 .. p1}, Lcom/google/android/videos/activity/AssetDetailsActivity;->onCreate(Landroid/os/Bundle;)V

    .line 262
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->account:Ljava/lang/String;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videoId:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 263
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "invalid arguments format: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->account:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/videos/utils/Hashing;->sha1(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videoId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 264
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->finish()V

    .line 390
    :goto_0
    return-void

    .line 268
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 270
    new-instance v2, Lcom/google/android/videos/ui/BannerTextHelper;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->playLayout:Lcom/google/android/videos/ui/ImmersiveHeaderListLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    new-instance v5, Lcom/google/android/videos/utils/DownloadedOnlyManager;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v6}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/videos/utils/DownloadedOnlyManager;-><init>(Landroid/content/SharedPreferences;)V

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/videos/ui/BannerTextHelper;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/videos/utils/DownloadedOnlyManager;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->bannerTextHelper:Lcom/google/android/videos/ui/BannerTextHelper;

    .line 273
    const v2, 0x7f0f0210

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/videos/activity/MovieDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-static {v0, v2, v1}, Lcom/google/android/videos/ui/StatusHelper;->createFromParent(Landroid/content/Context;Landroid/view/View;Lcom/google/android/videos/ui/StatusHelper$OnRetryListener;)Lcom/google/android/videos/ui/StatusHelper;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->statusHelper:Lcom/google/android/videos/ui/StatusHelper;

    .line 275
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->statusHelper:Lcom/google/android/videos/ui/StatusHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/StatusHelper;->init()V

    .line 277
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->database:Lcom/google/android/videos/store/Database;

    .line 278
    new-instance v2, Lcom/google/android/videos/activity/MovieDetailsActivity$DatabaseListener;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/google/android/videos/activity/MovieDetailsActivity$DatabaseListener;-><init>(Lcom/google/android/videos/activity/MovieDetailsActivity;Lcom/google/android/videos/activity/MovieDetailsActivity$1;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->databaseListener:Lcom/google/android/videos/activity/MovieDetailsActivity$DatabaseListener;

    .line 279
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/videos/Config;->suggestionsEnabled()Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->suggestionsEnabled:Z

    .line 281
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getUiEventLoggingHelper()Lcom/google/android/videos/logging/UiEventLoggingHelper;

    move-result-object v7

    .line 282
    .local v7, "uiEventLoggingHelper":Lcom/google/android/videos/logging/UiEventLoggingHelper;
    new-instance v2, Lcom/google/android/videos/logging/RootUiElementNodeImpl;

    const/4 v3, 0x6

    invoke-direct {v2, v3, v7}, Lcom/google/android/videos/logging/RootUiElementNodeImpl;-><init>(ILcom/google/android/videos/logging/UiEventLoggingHelper;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->rootUiElementNode:Lcom/google/android/videos/logging/RootUiElementNode;

    .line 285
    new-instance v2, Lcom/google/android/videos/store/StoreStatusMonitor;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->database:Lcom/google/android/videos/store/Database;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v5}, Lcom/google/android/videos/VideosGlobals;->getWishlistStore()Lcom/google/android/videos/store/WishlistStore;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3, v4, v5}, Lcom/google/android/videos/store/StoreStatusMonitor;-><init>(Landroid/content/Context;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/WishlistStore;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    .line 287
    new-instance v2, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v3}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v7}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/store/StoreStatusMonitor;Lcom/google/android/videos/store/PurchaseStoreSync;Lcom/google/android/videos/logging/UiEventLoggingHelper;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    .line 291
    new-instance v8, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/videos/api/ApiRequesters;->getRecommendationsRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getRecommendationsRequestFactory()Lcom/google/android/videos/api/RecommendationsRequest$Factory;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getErrorHelper()Lcom/google/android/videos/utils/ErrorHelper;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->account:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0029

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v16

    move-object/from16 v9, p0

    move-object/from16 v17, v7

    invoke-direct/range {v8 .. v17}, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/api/RecommendationsRequest$Factory;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/utils/ErrorHelper;Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;Ljava/lang/String;ILcom/google/android/videos/logging/UiEventLoggingHelper;)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->suggestionsHelper:Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;

    .line 302
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/videos/Config;->allowDownloads()Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->allowDownloads:Z

    .line 304
    new-instance v8, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getPosterStore()Lcom/google/android/videos/store/PosterStore;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/videos/store/PosterStore;->getRequester(I)Lcom/google/android/videos/async/Requester;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->suggestionsHelper:Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->getSuggestionsDataSource()Lcom/google/android/videos/adapter/ArrayDataSource;

    move-result-object v12

    new-instance v13, Lcom/google/android/videos/activity/MovieDetailsActivity$SeeMoreOnClickListener;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v2}, Lcom/google/android/videos/activity/MovieDetailsActivity$SeeMoreOnClickListener;-><init>(Lcom/google/android/videos/activity/MovieDetailsActivity;Lcom/google/android/videos/activity/MovieDetailsActivity$1;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getBitmapRequesters()Lcom/google/android/videos/bitmap/BitmapRequesters;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/videos/bitmap/BitmapRequesters;->getPosterArtRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->suggestionsHelper:Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->allowDownloads:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->account:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videoId:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getRemoteTracker()Lcom/google/android/videos/remote/RemoteTracker;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->rootUiElementNode:Lcom/google/android/videos/logging/RootUiElementNode;

    move-object/from16 v20, v0

    move-object/from16 v9, p0

    move-object/from16 v10, p0

    invoke-direct/range {v8 .. v20}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;-><init>(Lcom/google/android/videos/activity/MovieDetailsActivity;Lcom/google/android/videos/ui/MovieDetailsFlowHelper$InfoPanelItemClickListener;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/adapter/ArrayDataSource;Landroid/view/View$OnClickListener;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;ZLjava/lang/String;Ljava/lang/String;Lcom/google/android/videos/remote/RemoteTracker;Lcom/google/android/videos/logging/UiElementNode;)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->movieDetailsFlowHelper:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    .line 318
    new-instance v2, Lcom/google/android/videos/activity/MovieDetailsActivity$1;

    const-string v3, "MovieDetailsActivity"

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/google/android/videos/activity/MovieDetailsActivity$1;-><init>(Lcom/google/android/videos/activity/MovieDetailsActivity;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->flowLayoutManager:Lcom/google/android/play/layout/FlowLayoutManager;

    .line 326
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->flowLayoutManager:Lcom/google/android/play/layout/FlowLayoutManager;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/play/layout/FlowLayoutManager;->setItemChangesAffectFlow(Z)Lcom/google/android/play/layout/FlowLayoutManager;

    .line 328
    new-instance v22, Lcom/google/android/videos/flow/FlowAdapter;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->movieDetailsFlowHelper:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->getFlow()Lcom/google/android/videos/flow/Flow;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-direct {v0, v2}, Lcom/google/android/videos/flow/FlowAdapter;-><init>(Lcom/google/android/videos/flow/Flow;)V

    .line 329
    .local v22, "flowAdapter":Lcom/google/android/videos/flow/FlowAdapter;
    const/4 v2, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Lcom/google/android/videos/flow/FlowAdapter;->setHasStableIds(Z)V

    .line 330
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->contentLayout:Landroid/support/v7/widget/RecyclerView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->flowLayoutManager:Lcom/google/android/play/layout/FlowLayoutManager;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 331
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->contentLayout:Landroid/support/v7/widget/RecyclerView;

    move-object/from16 v0, v22

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 332
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->contentLayout:Landroid/support/v7/widget/RecyclerView;

    new-instance v3, Lcom/google/android/videos/ui/ShuffleAddItemAnimator;

    invoke-direct {v3}, Lcom/google/android/videos/ui/ShuffleAddItemAnimator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(Landroid/support/v7/widget/RecyclerView$ItemAnimator;)V

    .line 334
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getPinHelper()Lcom/google/android/videos/ui/PinHelper;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->pinHelper:Lcom/google/android/videos/ui/PinHelper;

    .line 336
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->account:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videoId:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/videos/store/PurchaseStoreSync;->setHiddenStateForMovie(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 337
    new-instance v2, Lcom/google/android/videos/activity/MovieDetailsActivity$2;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/videos/activity/MovieDetailsActivity$2;-><init>(Lcom/google/android/videos/activity/MovieDetailsActivity;)V

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/videos/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/ActivityCallback;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->purchaseSyncCallback:Lcom/google/android/videos/async/NewCallback;

    .line 348
    new-instance v2, Lcom/google/android/videos/activity/MovieDetailsActivity$3;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/videos/activity/MovieDetailsActivity$3;-><init>(Lcom/google/android/videos/activity/MovieDetailsActivity;)V

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/videos/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/ActivityCallback;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->purchaseCallback:Lcom/google/android/videos/async/Callback;

    .line 373
    new-instance v2, Lcom/google/android/videos/activity/MovieDetailsActivity$4;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/videos/activity/MovieDetailsActivity$4;-><init>(Lcom/google/android/videos/activity/MovieDetailsActivity;)V

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/videos/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/ActivityCallback;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/activity/MovieDetailsActivity;->downloadStatusCallback:Lcom/google/android/videos/async/Callback;

    .line 386
    new-instance v21, Landroid/content/Intent;

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 387
    .local v21, "clearedIntent":Landroid/content/Intent;
    const-string v2, "start_download"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 388
    const-string v2, "pinning_error_dialog"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 389
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/MovieDetailsActivity;->setIntent(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public onEnabledVerticalsChanged(Ljava/lang/String;I)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "verticals"    # I

    .prologue
    .line 1012
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->account:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1013
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videosDrawerHelper:Lcom/google/android/videos/ui/VideosDrawerHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/ui/VideosDrawerHelper;->init(Ljava/lang/String;I)Z

    .line 1015
    :cond_0
    return-void
.end method

.method protected onEnterTransitionFinish()V
    .locals 0

    .prologue
    .line 569
    invoke-direct {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->initSuggestionsIfAppropriate()V

    .line 570
    invoke-direct {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->refreshPurchase()V

    .line 571
    return-void
.end method

.method protected onEnterTransitionStart()V
    .locals 1

    .prologue
    .line 650
    iget-boolean v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->activityRunning:Z

    if-eqz v0, :cond_0

    .line 651
    invoke-direct {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->startSync()V

    .line 653
    :cond_0
    return-void
.end method

.method public onHasContentChanged(Ljava/lang/String;Z)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "hasContent"    # Z

    .prologue
    .line 1008
    return-void
.end method

.method public onHelpSelected()V
    .locals 1

    .prologue
    .line 737
    const-string v0, "mobile_movie_object"

    invoke-static {p0, v0}, Lcom/google/android/videos/ui/HelpHelper;->startContextualHelp(Landroid/app/Activity;Ljava/lang/String;)V

    .line 738
    return-void
.end method

.method public onNotAuthenticated(Ljava/lang/String;)V
    .locals 2
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 703
    const v0, 0x7f0b0145

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/google/android/videos/utils/Util;->showToast(Landroid/content/Context;II)V

    .line 704
    invoke-virtual {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->finish()V

    .line 705
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 9
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x1

    .line 753
    iget-object v1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v7

    .line 754
    .local v7, "eventLogger":Lcom/google/android/videos/logging/EventLogger;
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 776
    invoke-static {p1, p0}, Lcom/google/android/videos/VideosApplication;->onCommonOptionsItemSelected(Landroid/view/MenuItem;Landroid/app/Activity;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-super {p0, p1}, Lcom/google/android/videos/activity/AssetDetailsActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    move v0, v8

    :cond_1
    move v8, v0

    :cond_2
    :goto_0
    return v8

    .line 756
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->pinned:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    .line 759
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->pinHelper:Lcom/google/android/videos/ui/PinHelper;

    iget-object v1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videoId:Ljava/lang/String;

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/videos/ui/PinHelper;->pinVideo(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 760
    invoke-interface {v7, v8}, Lcom/google/android/videos/logging/EventLogger;->onPinClick(Z)V

    goto :goto_0

    .line 763
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->pinned:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 766
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->pinHelper:Lcom/google/android/videos/ui/PinHelper;

    iget-object v2, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->account:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videoId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->video:Lcom/google/android/videos/store/VideoMetadata;

    iget-object v4, v1, Lcom/google/android/videos/store/VideoMetadata;->title:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->pinningStatus:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->pinningStatusReason:Ljava/lang/Integer;

    move-object v1, p0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/videos/ui/PinHelper;->onUnpinClicked(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Integer;)V

    .line 768
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videoId:Ljava/lang/String;

    invoke-interface {v7, v0, v8}, Lcom/google/android/videos/logging/EventLogger;->onUnpinVideo(Ljava/lang/String;Z)V

    goto :goto_0

    .line 771
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videoId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->video:Lcom/google/android/videos/store/VideoMetadata;

    iget-object v3, v3, Lcom/google/android/videos/store/VideoMetadata;->title:Ljava/lang/String;

    invoke-static {p0, v1, v2, v3, v0}, Lcom/google/android/videos/ui/RemoveItemDialogFragment;->showInstanceWithCallback(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 773
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videoId:Ljava/lang/String;

    invoke-interface {v7, v0}, Lcom/google/android/videos/logging/EventLogger;->onRemoveItemDialogShown(Ljava/lang/String;)V

    goto :goto_0

    .line 754
    :pswitch_data_0
    .packed-switch 0x7f0f0225
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 672
    invoke-super {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->onPause()V

    .line 673
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->rootUiElementNode:Lcom/google/android/videos/logging/RootUiElementNode;

    invoke-interface {v0}, Lcom/google/android/videos/logging/RootUiElementNode;->flushImpression()V

    .line 674
    return-void
.end method

.method public onPinClick(Lcom/google/android/videos/store/VideoDownloadStatus;)V
    .locals 7
    .param p1, "status"    # Lcom/google/android/videos/store/VideoDownloadStatus;

    .prologue
    .line 800
    iget-boolean v0, p1, Lcom/google/android/videos/store/VideoDownloadStatus;->pinned:Z

    if-eqz v0, :cond_0

    .line 801
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->pinHelper:Lcom/google/android/videos/ui/PinHelper;

    iget-object v2, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->account:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videoId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->video:Lcom/google/android/videos/store/VideoMetadata;

    iget-object v4, v1, Lcom/google/android/videos/store/VideoMetadata;->title:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/videos/store/VideoDownloadStatus;->pinningStatus:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v6, p1, Lcom/google/android/videos/store/VideoDownloadStatus;->pinningStatusReason:Ljava/lang/Integer;

    move-object v1, p0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/videos/ui/PinHelper;->onUnpinClicked(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Integer;)V

    .line 810
    :goto_0
    return-void

    .line 803
    :cond_0
    iget-object v0, p1, Lcom/google/android/videos/store/VideoDownloadStatus;->pinningStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/videos/store/VideoDownloadStatus;->pinningStatus:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 805
    invoke-direct {p0, p1}, Lcom/google/android/videos/activity/MovieDetailsActivity;->showPinningErrorDialog(Lcom/google/android/videos/store/VideoDownloadStatus;)V

    goto :goto_0

    .line 807
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->pinHelper:Lcom/google/android/videos/ui/PinHelper;

    iget-object v1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videoId:Ljava/lang/String;

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/videos/ui/PinHelper;->pinVideo(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 808
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/videos/logging/EventLogger;->onPinClick(Z)V

    goto :goto_0
.end method

.method public onPlayClick()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 795
    iget-object v1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videoId:Ljava/lang/String;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/activity/WatchActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->startActivity(Landroid/content/Intent;)V

    .line 796
    return-void
.end method

.method public onPosterLoaded()V
    .locals 1

    .prologue
    .line 533
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->posterLoaded:Z

    .line 534
    invoke-direct {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->startEnterTransitionIfReady()V

    .line 535
    return-void
.end method

.method public onPosterRendered()V
    .locals 1

    .prologue
    .line 527
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->posterLaidOut:Z

    .line 528
    invoke-direct {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->startEnterTransitionIfReady()V

    .line 529
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 742
    invoke-super {p0, p1}, Lcom/google/android/videos/activity/AssetDetailsActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 743
    const v3, 0x7f0f0227

    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->video:Lcom/google/android/videos/store/VideoMetadata;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->allowDownloads:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->pinned:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->pinned:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {p1, v3, v0}, Lcom/google/android/videos/utils/Util;->setMenuItemEnabled(Landroid/view/Menu;IZ)V

    .line 745
    const v3, 0x7f0f0226

    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->video:Lcom/google/android/videos/store/VideoMetadata;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->allowDownloads:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->pinned:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->pinned:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-static {p1, v3, v0}, Lcom/google/android/videos/utils/Util;->setMenuItemEnabled(Landroid/view/Menu;IZ)V

    .line 747
    const v0, 0x7f0f0225

    iget-object v3, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->video:Lcom/google/android/videos/store/VideoMetadata;

    if-eqz v3, :cond_0

    move v2, v1

    :cond_0
    invoke-static {p1, v0, v2}, Lcom/google/android/videos/utils/Util;->setMenuItemEnabled(Landroid/view/Menu;IZ)V

    .line 748
    return v1

    :cond_1
    move v0, v2

    .line 743
    goto :goto_0

    :cond_2
    move v0, v2

    .line 745
    goto :goto_1
.end method

.method public onRemoved(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "itemId"    # Ljava/lang/String;
    .param p3, "itemIsShow"    # Z

    .prologue
    .line 787
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->account:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videoId:Ljava/lang/String;

    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p3, :cond_0

    .line 789
    invoke-virtual {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->finish()V

    .line 791
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 657
    invoke-super {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->onResume()V

    .line 658
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->refreshContentRestrictions()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 659
    invoke-virtual {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->finish()V

    .line 668
    :cond_0
    :goto_0
    return-void

    .line 663
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->rootUiElementNode:Lcom/google/android/videos/logging/RootUiElementNode;

    invoke-interface {v0}, Lcom/google/android/videos/logging/RootUiElementNode;->startNewImpression()V

    .line 664
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videoId:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/videos/logging/EventLogger;->onMovieDetailsPageOpened(Ljava/lang/String;)V

    .line 665
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videosDrawerHelper:Lcom/google/android/videos/ui/VideosDrawerHelper;

    if-eqz v0, :cond_0

    .line 666
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videosDrawerHelper:Lcom/google/android/videos/ui/VideosDrawerHelper;

    iget-object v1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;

    iget-object v3, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->account:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/videos/ui/VerticalsHelper;->enabledVerticalsForUser(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/ui/VideosDrawerHelper;->init(Ljava/lang/String;I)Z

    goto :goto_0
.end method

.method public onRetry()V
    .locals 0

    .prologue
    .line 678
    invoke-direct {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->refreshPurchase()V

    .line 679
    invoke-direct {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->startSync()V

    .line 680
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 596
    invoke-super {p0, p1}, Lcom/google/android/videos/activity/AssetDetailsActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 597
    return-void
.end method

.method protected onStart()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 609
    invoke-super {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->onStart()V

    .line 610
    iget-object v4, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->account:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v4

    if-nez v4, :cond_0

    .line 611
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "account does not exist: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->account:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/videos/utils/Hashing;->sha1(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 612
    invoke-virtual {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->finish()V

    .line 646
    :goto_0
    return-void

    .line 616
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "video_metadata"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/store/VideoMetadata;

    .line 617
    .local v2, "metadata":Lcom/google/android/videos/store/VideoMetadata;
    invoke-virtual {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "video_download_status"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/VideoDownloadStatus;

    .line 619
    .local v0, "downloadStatus":Lcom/google/android/videos/store/VideoDownloadStatus;
    if-eqz v2, :cond_5

    if-eqz v0, :cond_5

    move v1, v3

    .line 620
    .local v1, "haveCachedInfo":Z
    :goto_1
    if-eqz v1, :cond_1

    .line 621
    invoke-direct {p0, v2, v0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->onVideoMetadata(Lcom/google/android/videos/store/VideoMetadata;Lcom/google/android/videos/store/VideoDownloadStatus;)V

    .line 623
    :cond_1
    if-eqz v1, :cond_2

    iget-boolean v4, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->pendingEnterTransition:Z

    if-nez v4, :cond_3

    .line 629
    :cond_2
    invoke-direct {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->refreshPurchase()V

    .line 632
    :cond_3
    iget-object v4, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->database:Lcom/google/android/videos/store/Database;

    iget-object v5, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->databaseListener:Lcom/google/android/videos/activity/MovieDetailsActivity$DatabaseListener;

    invoke-virtual {v4, v5}, Lcom/google/android/videos/store/Database;->addListener(Lcom/google/android/videos/store/Database$Listener;)V

    .line 633
    iget-object v4, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    iget-object v5, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->account:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->init(Ljava/lang/String;)V

    .line 634
    iget-boolean v4, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->pendingEnterTransition:Z

    if-nez v4, :cond_4

    .line 635
    invoke-direct {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->startSync()V

    .line 637
    :cond_4
    iget-object v4, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v4, p0}, Lcom/google/android/videos/utils/NetworkStatus;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 638
    invoke-virtual {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->update()V

    .line 639
    iget-object v4, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->movieDetailsFlowHelper:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    invoke-virtual {v4}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->registerWithRemoteTracker()V

    .line 640
    iget-object v4, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;

    invoke-virtual {v4, p0}, Lcom/google/android/videos/ui/VerticalsHelper;->addOnContentChangedListener(Lcom/google/android/videos/ui/VerticalsHelper$OnContentChangedListener;)V

    .line 641
    iget-object v4, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->mediaRouteProvider:Lcom/google/android/videos/remote/MediaRouteProvider;

    invoke-interface {v4}, Lcom/google/android/videos/remote/MediaRouteProvider;->onStart()V

    .line 642
    iget-object v4, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    iget-object v5, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->account:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/android/videos/store/StoreStatusMonitor;->init(Ljava/lang/String;)V

    .line 643
    iget-object v4, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->bannerTextHelper:Lcom/google/android/videos/ui/BannerTextHelper;

    invoke-virtual {v4}, Lcom/google/android/videos/ui/BannerTextHelper;->enable()V

    .line 644
    invoke-direct {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->updateVisibilities()V

    .line 645
    iput-boolean v3, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->started:Z

    goto :goto_0

    .line 619
    .end local v1    # "haveCachedInfo":Z
    :cond_5
    const/4 v1, 0x0

    goto :goto_1
.end method

.method protected onStop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 715
    iget-boolean v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->started:Z

    if-eqz v0, :cond_0

    .line 716
    iput-boolean v2, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->started:Z

    .line 717
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->movieDetailsFlowHelper:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->unregisterWithRemoteTracker()V

    .line 718
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->database:Lcom/google/android/videos/store/Database;

    iget-object v1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->databaseListener:Lcom/google/android/videos/activity/MovieDetailsActivity$DatabaseListener;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/store/Database;->removeListener(Lcom/google/android/videos/store/Database$Listener;)Z

    .line 719
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->reset()V

    .line 720
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->suggestionsHelper:Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->reset()V

    .line 721
    iput-boolean v2, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->initializedSuggestions:Z

    .line 722
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->currentPurchaseRequest:Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .line 723
    iput-boolean v2, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->pendingPurchaseRequest:Z

    .line 724
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->syncTaskControl:Lcom/google/android/videos/async/TaskControl;

    invoke-static {v0}, Lcom/google/android/videos/async/TaskControl;->cancel(Lcom/google/android/videos/async/TaskControl;)V

    .line 725
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0, p0}, Lcom/google/android/videos/utils/NetworkStatus;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 726
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/VerticalsHelper;->removeOnContentChangedListener(Lcom/google/android/videos/ui/VerticalsHelper$OnContentChangedListener;)V

    .line 727
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->mediaRouteProvider:Lcom/google/android/videos/remote/MediaRouteProvider;

    invoke-interface {v0}, Lcom/google/android/videos/remote/MediaRouteProvider;->onStop()V

    .line 728
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    invoke-virtual {v0}, Lcom/google/android/videos/store/StoreStatusMonitor;->reset()V

    .line 729
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->bannerTextHelper:Lcom/google/android/videos/ui/BannerTextHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/BannerTextHelper;->disable()V

    .line 730
    invoke-super {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->resetDrawer()V

    .line 732
    :cond_0
    invoke-super {p0}, Lcom/google/android/videos/activity/AssetDetailsActivity;->onStop()V

    .line 733
    return-void
.end method

.method public onStorylineClick()V
    .locals 1

    .prologue
    .line 819
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->video:Lcom/google/android/videos/store/VideoMetadata;

    invoke-static {p0, v0}, Lcom/google/android/videos/activity/MovieDetailsActivity$MetaDataDialogFragment;->showInstance(Landroid/support/v4/app/FragmentActivity;Lcom/google/android/videos/store/VideoMetadata;)V

    .line 820
    return-void
.end method

.method public update()V
    .locals 2

    .prologue
    .line 1000
    iget-object v1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v1}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v0

    .line 1001
    .local v0, "isConnected":Z
    iget-object v1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->suggestionsHelper:Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/ui/RelatedMovieSuggestionsHelper;->getSuggestionsDataSource()Lcom/google/android/videos/adapter/ArrayDataSource;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/videos/adapter/ArrayDataSource;->setNetworkConnected(Z)V

    .line 1002
    iget-object v1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity;->movieDetailsFlowHelper:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->setNetworkConnected(Z)V

    .line 1003
    return-void
.end method
