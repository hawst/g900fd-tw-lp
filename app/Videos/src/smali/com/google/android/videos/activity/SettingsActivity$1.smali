.class Lcom/google/android/videos/activity/SettingsActivity$1;
.super Ljava/lang/Object;
.source "SettingsActivity.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/activity/SettingsActivity;->onClick(Landroid/content/DialogInterface;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/api/LinkedAccountRequest;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/activity/SettingsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/videos/activity/SettingsActivity;)V
    .locals 0

    .prologue
    .line 325
    iput-object p1, p0, Lcom/google/android/videos/activity/SettingsActivity$1;->this$0:Lcom/google/android/videos/activity/SettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/api/LinkedAccountRequest;Ljava/lang/Exception;)V
    .locals 6
    .param p1, "request"    # Lcom/google/android/videos/api/LinkedAccountRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 337
    iget-object v1, p0, Lcom/google/android/videos/activity/SettingsActivity$1;->this$0:Lcom/google/android/videos/activity/SettingsActivity;

    # invokes: Lcom/google/android/videos/activity/SettingsActivity;->dismissInProgressDialog()V
    invoke-static {v1}, Lcom/google/android/videos/activity/SettingsActivity;->access$300(Lcom/google/android/videos/activity/SettingsActivity;)V

    .line 339
    iget-object v1, p0, Lcom/google/android/videos/activity/SettingsActivity$1;->this$0:Lcom/google/android/videos/activity/SettingsActivity;

    const v2, 0x7f0b0234

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/videos/activity/SettingsActivity$1;->this$0:Lcom/google/android/videos/activity/SettingsActivity;

    # getter for: Lcom/google/android/videos/activity/SettingsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;
    invoke-static {v5}, Lcom/google/android/videos/activity/SettingsActivity;->access$400(Lcom/google/android/videos/activity/SettingsActivity;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/videos/VideosGlobals;->getErrorHelper()Lcom/google/android/videos/utils/ErrorHelper;

    move-result-object v5

    invoke-virtual {v5, p2}, Lcom/google/android/videos/utils/ErrorHelper;->humanize(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/activity/SettingsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 341
    .local v0, "message":Ljava/lang/String;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/google/android/videos/activity/SettingsActivity$1;->this$0:Lcom/google/android/videos/activity/SettingsActivity;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0b010c

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 345
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 325
    check-cast p1, Lcom/google/android/videos/api/LinkedAccountRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/activity/SettingsActivity$1;->onError(Lcom/google/android/videos/api/LinkedAccountRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/api/LinkedAccountRequest;Ljava/lang/Void;)V
    .locals 7
    .param p1, "request"    # Lcom/google/android/videos/api/LinkedAccountRequest;
    .param p2, "response"    # Ljava/lang/Void;

    .prologue
    const/4 v6, 0x1

    .line 328
    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity$1;->this$0:Lcom/google/android/videos/activity/SettingsActivity;

    iget-object v1, p0, Lcom/google/android/videos/activity/SettingsActivity$1;->this$0:Lcom/google/android/videos/activity/SettingsActivity;

    const v2, 0x7f0b0233

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/videos/activity/SettingsActivity$1;->this$0:Lcom/google/android/videos/activity/SettingsActivity;

    # getter for: Lcom/google/android/videos/activity/SettingsActivity;->account:Ljava/lang/String;
    invoke-static {v5}, Lcom/google/android/videos/activity/SettingsActivity;->access$100(Lcom/google/android/videos/activity/SettingsActivity;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/activity/SettingsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v6}, Lcom/google/android/videos/utils/Util;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    .line 331
    iget-object v0, p0, Lcom/google/android/videos/activity/SettingsActivity$1;->this$0:Lcom/google/android/videos/activity/SettingsActivity;

    # getter for: Lcom/google/android/videos/activity/SettingsActivity;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;
    invoke-static {v0}, Lcom/google/android/videos/activity/SettingsActivity;->access$200(Lcom/google/android/videos/activity/SettingsActivity;)Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/activity/SettingsActivity$1;->this$0:Lcom/google/android/videos/activity/SettingsActivity;

    # getter for: Lcom/google/android/videos/activity/SettingsActivity;->account:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/videos/activity/SettingsActivity;->access$100(Lcom/google/android/videos/activity/SettingsActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/activity/SettingsActivity$1;->this$0:Lcom/google/android/videos/activity/SettingsActivity;

    new-instance v3, Lcom/google/android/videos/activity/SettingsActivity$SyncUserConfigurationCallback;

    iget-object v4, p0, Lcom/google/android/videos/activity/SettingsActivity$1;->this$0:Lcom/google/android/videos/activity/SettingsActivity;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lcom/google/android/videos/activity/SettingsActivity$SyncUserConfigurationCallback;-><init>(Lcom/google/android/videos/activity/SettingsActivity;Lcom/google/android/videos/activity/SettingsActivity$1;)V

    invoke-static {v2, v3}, Lcom/google/android/videos/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/ActivityCallback;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/store/ConfigurationStore;->syncUserConfiguration(Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V

    .line 333
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 325
    check-cast p1, Lcom/google/android/videos/api/LinkedAccountRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/lang/Void;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/activity/SettingsActivity$1;->onResponse(Lcom/google/android/videos/api/LinkedAccountRequest;Ljava/lang/Void;)V

    return-void
.end method
