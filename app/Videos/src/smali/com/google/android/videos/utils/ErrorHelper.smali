.class public Lcom/google/android/videos/utils/ErrorHelper;
.super Ljava/lang/Object;
.source "ErrorHelper.java"


# instance fields
.field private final context:Landroid/content/Context;

.field private final networkStatus:Lcom/google/android/videos/utils/NetworkStatus;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/videos/utils/NetworkStatus;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/videos/utils/ErrorHelper;->context:Landroid/content/Context;

    .line 27
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/utils/NetworkStatus;

    iput-object v0, p0, Lcom/google/android/videos/utils/ErrorHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 28
    return-void
.end method

.method public static isHttpException(Ljava/lang/Throwable;I)Z
    .locals 1
    .param p0, "t"    # Ljava/lang/Throwable;
    .param p1, "statusCode"    # I

    .prologue
    .line 36
    instance-of v0, p0, Lorg/apache/http/client/HttpResponseException;

    if-eqz v0, :cond_0

    check-cast p0, Lorg/apache/http/client/HttpResponseException;

    .end local p0    # "t":Ljava/lang/Throwable;
    invoke-virtual {p0}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getHumanizedRepresentation(Ljava/lang/Throwable;)Landroid/util/Pair;
    .locals 1
    .param p1, "throwable"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/videos/utils/ErrorHelper;->getHumanizedRepresentation(Ljava/lang/Throwable;Z)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public getHumanizedRepresentation(Ljava/lang/Throwable;Z)Landroid/util/Pair;
    .locals 10
    .param p1, "throwable"    # Ljava/lang/Throwable;
    .param p2, "assumeNetwork"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            "Z)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const v9, 0x7f0b017b

    const v5, 0x7f0b0146

    const/4 v7, 0x3

    const/4 v8, 0x0

    const/4 v6, 0x1

    .line 83
    if-nez p1, :cond_0

    .line 84
    iget-object v4, p0, Lcom/google/android/videos/utils/ErrorHelper;->context:Landroid/content/Context;

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    .line 130
    :goto_0
    return-object v4

    .line 86
    :cond_0
    instance-of v4, p1, Lcom/google/android/videos/streams/MissingStreamException;

    if-eqz v4, :cond_1

    .line 87
    iget-object v4, p0, Lcom/google/android/videos/utils/ErrorHelper;->context:Landroid/content/Context;

    const v5, 0x7f0b01f4

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    goto :goto_0

    .line 90
    :cond_1
    instance-of v4, p1, Landroid/accounts/AuthenticatorException;

    if-eqz v4, :cond_2

    .line 92
    iget-object v4, p0, Lcom/google/android/videos/utils/ErrorHelper;->context:Landroid/content/Context;

    const v5, 0x7f0b0145

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x7

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    goto :goto_0

    .line 95
    :cond_2
    instance-of v4, p1, Ljava/net/SocketException;

    if-eqz v4, :cond_4

    .line 97
    iget-object v4, p0, Lcom/google/android/videos/utils/ErrorHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v4}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 98
    iget-object v4, p0, Lcom/google/android/videos/utils/ErrorHelper;->context:Landroid/content/Context;

    const v5, 0x7f0b0142

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    goto :goto_0

    .line 100
    :cond_3
    iget-object v4, p0, Lcom/google/android/videos/utils/ErrorHelper;->context:Landroid/content/Context;

    invoke-virtual {v4, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    goto :goto_0

    .line 102
    :cond_4
    instance-of v4, p1, Lorg/apache/http/client/HttpResponseException;

    if-eqz v4, :cond_6

    move-object v2, p1

    .line 103
    check-cast v2, Lorg/apache/http/client/HttpResponseException;

    .line 104
    .local v2, "httpException":Lorg/apache/http/client/HttpResponseException;
    invoke-virtual {v2}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v3

    .line 105
    .local v3, "statusCode":I
    const/16 v4, 0x190

    if-gt v4, v3, :cond_5

    const/16 v4, 0x258

    if-ge v3, v4, :cond_5

    const v0, 0x7f0b0084

    .line 107
    .local v0, "errorStringId":I
    :goto_1
    iget-object v4, p0, Lcom/google/android/videos/utils/ErrorHelper;->context:Landroid/content/Context;

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v4, v0, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    goto/16 :goto_0

    .line 105
    .end local v0    # "errorStringId":I
    :cond_5
    const v0, 0x7f0b0143

    goto :goto_1

    .line 109
    .end local v2    # "httpException":Lorg/apache/http/client/HttpResponseException;
    .end local v3    # "statusCode":I
    :cond_6
    instance-of v4, p1, Lcom/google/android/videos/converter/ConverterException;

    if-eqz v4, :cond_7

    .line 111
    iget-object v4, p0, Lcom/google/android/videos/utils/ErrorHelper;->context:Landroid/content/Context;

    const v5, 0x7f0b0144

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    goto/16 :goto_0

    .line 113
    :cond_7
    instance-of v4, p1, Lcom/google/android/videos/utils/MediaNotMountedException;

    if-eqz v4, :cond_8

    move-object v1, p1

    .line 114
    check-cast v1, Lcom/google/android/videos/utils/MediaNotMountedException;

    .line 115
    .local v1, "exception":Lcom/google/android/videos/utils/MediaNotMountedException;
    iget-object v4, p0, Lcom/google/android/videos/utils/ErrorHelper;->context:Landroid/content/Context;

    iget v5, v1, Lcom/google/android/videos/utils/MediaNotMountedException;->errorStringResourceId:I

    new-array v6, v6, [Ljava/lang/Object;

    iget v7, v1, Lcom/google/android/videos/utils/MediaNotMountedException;->errorCode:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget v5, v1, Lcom/google/android/videos/utils/MediaNotMountedException;->errorCode:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    goto/16 :goto_0

    .line 119
    .end local v1    # "exception":Lcom/google/android/videos/utils/MediaNotMountedException;
    :cond_8
    instance-of v4, p1, Ljava/io/IOException;

    if-eqz v4, :cond_b

    .line 120
    if-eqz p2, :cond_a

    .line 121
    iget-object v4, p0, Lcom/google/android/videos/utils/ErrorHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v4}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 122
    iget-object v4, p0, Lcom/google/android/videos/utils/ErrorHelper;->context:Landroid/content/Context;

    const v5, 0x7f0b0141

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    goto/16 :goto_0

    .line 124
    :cond_9
    iget-object v4, p0, Lcom/google/android/videos/utils/ErrorHelper;->context:Landroid/content/Context;

    invoke-virtual {v4, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    goto/16 :goto_0

    .line 126
    :cond_a
    iget-object v4, p0, Lcom/google/android/videos/utils/ErrorHelper;->context:Landroid/content/Context;

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    goto/16 :goto_0

    .line 130
    :cond_b
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    invoke-virtual {p0, v4, p2}, Lcom/google/android/videos/utils/ErrorHelper;->getHumanizedRepresentation(Ljava/lang/Throwable;Z)Landroid/util/Pair;

    move-result-object v4

    goto/16 :goto_0
.end method

.method public humanize(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 1
    .param p1, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 48
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/videos/utils/ErrorHelper;->humanize(Ljava/lang/Throwable;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public humanize(Ljava/lang/Throwable;Z)Ljava/lang/String;
    .locals 1
    .param p1, "throwable"    # Ljava/lang/Throwable;
    .param p2, "assumeNetwork"    # Z

    .prologue
    .line 59
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/utils/ErrorHelper;->getHumanizedRepresentation(Ljava/lang/Throwable;Z)Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public showToast(Ljava/lang/String;)V
    .locals 2
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/videos/utils/ErrorHelper;->context:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lcom/google/android/videos/utils/Util;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    .line 153
    return-void
.end method

.method public showToast(Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 138
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/videos/utils/ErrorHelper;->humanize(Ljava/lang/Throwable;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/utils/ErrorHelper;->showToast(Ljava/lang/String;)V

    .line 139
    return-void
.end method
