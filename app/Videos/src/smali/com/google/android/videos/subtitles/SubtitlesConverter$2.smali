.class final Lcom/google/android/videos/subtitles/SubtitlesConverter$2;
.super Lcom/google/android/videos/utils/XmlParser$Rule;
.source "SubtitlesConverter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/subtitles/SubtitlesConverter;->addFormat1Rules(Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/google/android/videos/utils/XmlParser$Rule;-><init>()V

    return-void
.end method


# virtual methods
.method public end(Lcom/google/android/videos/utils/Stack;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 8
    .param p2, "attrs"    # Lorg/xml/sax/Attributes;
    .param p3, "chars"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/utils/Stack",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Lorg/xml/sax/Attributes;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "stack":Lcom/google/android/videos/utils/Stack;, "Lcom/google/android/videos/utils/Stack<Ljava/lang/Object;>;"
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 79
    const-class v4, Lcom/google/android/videos/subtitles/Subtitles$Builder;

    invoke-virtual {p1, v4}, Lcom/google/android/videos/utils/Stack;->peek(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/subtitles/Subtitles$Builder;

    .line 80
    .local v0, "builder":Lcom/google/android/videos/subtitles/Subtitles$Builder;
    new-array v4, v7, [Ljava/lang/String;

    const-string v5, "start"

    aput-object v5, v4, v1

    # invokes: Lcom/google/android/videos/subtitles/SubtitlesConverter;->getValue(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p2, v4}, Lcom/google/android/videos/subtitles/SubtitlesConverter;->access$000(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    # invokes: Lcom/google/android/videos/subtitles/SubtitlesConverter;->secondsToMillis(F)I
    invoke-static {v4}, Lcom/google/android/videos/subtitles/SubtitlesConverter;->access$100(F)I

    move-result v3

    .line 81
    .local v3, "startTimeMillis":I
    new-array v4, v7, [Ljava/lang/String;

    const-string v5, "dur"

    aput-object v5, v4, v1

    # invokes: Lcom/google/android/videos/subtitles/SubtitlesConverter;->getValue(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p2, v4}, Lcom/google/android/videos/subtitles/SubtitlesConverter;->access$000(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/high16 v5, 0x40a00000    # 5.0f

    invoke-static {v4, v5}, Lcom/google/android/videos/utils/Util;->parseFloat(Ljava/lang/String;F)F

    move-result v4

    # invokes: Lcom/google/android/videos/subtitles/SubtitlesConverter;->secondsToMillis(F)I
    invoke-static {v4}, Lcom/google/android/videos/subtitles/SubtitlesConverter;->access$100(F)I

    move-result v6

    .line 83
    .local v6, "durationMillis":I
    const-string v4, "\n"

    const-string v5, "<br/>"

    invoke-virtual {p3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 84
    .local v2, "text":Ljava/lang/String;
    add-int v4, v3, v6

    move v5, v1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/videos/subtitles/Subtitles$Builder;->addLineToWindow(ILjava/lang/String;IIZ)Lcom/google/android/videos/subtitles/Subtitles$Builder;

    .line 86
    return-void
.end method
