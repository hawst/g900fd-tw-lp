.class Lcom/google/android/videos/ui/DefaultItemAnimator$4;
.super Lcom/google/android/videos/ui/DefaultItemAnimator$VpaListenerAdapter;
.source "DefaultItemAnimator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/ui/DefaultItemAnimator;->runOldViewChangeAnimation(Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/ui/DefaultItemAnimator;

.field final synthetic val$changeInfo:Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;

.field final synthetic val$oldViewAnim:Landroid/support/v4/view/ViewPropertyAnimatorCompat;


# direct methods
.method constructor <init>(Lcom/google/android/videos/ui/DefaultItemAnimator;Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;Landroid/support/v4/view/ViewPropertyAnimatorCompat;)V
    .locals 0

    .prologue
    .line 228
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator$4;, "Lcom/google/android/videos/ui/DefaultItemAnimator.4;"
    iput-object p1, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$4;->this$0:Lcom/google/android/videos/ui/DefaultItemAnimator;

    iput-object p2, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$4;->val$changeInfo:Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;

    iput-object p3, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$4;->val$oldViewAnim:Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    invoke-direct {p0}, Lcom/google/android/videos/ui/DefaultItemAnimator$VpaListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator$4;, "Lcom/google/android/videos/ui/DefaultItemAnimator.4;"
    const/4 v2, 0x0

    .line 236
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$4;->val$oldViewAnim:Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setListener(Landroid/support/v4/view/ViewPropertyAnimatorListener;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    .line 237
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p1, v0}, Landroid/support/v4/view/ViewCompat;->setAlpha(Landroid/view/View;F)V

    .line 238
    invoke-static {p1, v2}, Landroid/support/v4/view/ViewCompat;->setTranslationX(Landroid/view/View;F)V

    .line 239
    invoke-static {p1, v2}, Landroid/support/v4/view/ViewCompat;->setTranslationY(Landroid/view/View;F)V

    .line 240
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$4;->this$0:Lcom/google/android/videos/ui/DefaultItemAnimator;

    iget-object v1, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$4;->val$changeInfo:Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;

    iget-object v1, v1, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;->oldHolder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/ui/DefaultItemAnimator;->dispatchChangeFinished(Landroid/support/v7/widget/RecyclerView$ViewHolder;Z)V

    .line 241
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$4;->this$0:Lcom/google/android/videos/ui/DefaultItemAnimator;

    # getter for: Lcom/google/android/videos/ui/DefaultItemAnimator;->mChangeAnimations:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/videos/ui/DefaultItemAnimator;->access$300(Lcom/google/android/videos/ui/DefaultItemAnimator;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$4;->val$changeInfo:Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;

    iget-object v1, v1, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;->oldHolder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 242
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$4;->this$0:Lcom/google/android/videos/ui/DefaultItemAnimator;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/DefaultItemAnimator;->dispatchFinishedWhenDone()V

    .line 243
    return-void
.end method

.method public onAnimationStart(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 231
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator$4;, "Lcom/google/android/videos/ui/DefaultItemAnimator.4;"
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$4;->this$0:Lcom/google/android/videos/ui/DefaultItemAnimator;

    iget-object v1, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$4;->val$changeInfo:Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;

    iget-object v1, v1, Lcom/google/android/videos/ui/DefaultItemAnimator$ChangeInfo;->oldHolder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/ui/DefaultItemAnimator;->dispatchChangeStarting(Landroid/support/v7/widget/RecyclerView$ViewHolder;Z)V

    .line 232
    return-void
.end method
