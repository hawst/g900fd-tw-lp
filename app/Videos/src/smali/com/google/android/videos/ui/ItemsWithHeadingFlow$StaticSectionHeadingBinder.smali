.class public Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;
.super Ljava/lang/Object;
.source "ItemsWithHeadingFlow.java"

# interfaces
.implements Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/ItemsWithHeadingFlow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StaticSectionHeadingBinder"
.end annotation


# instance fields
.field private final actionText:Ljava/lang/CharSequence;

.field private dimmed:Z

.field private final onClickListener:Landroid/view/View$OnClickListener;

.field private final subheading:Ljava/lang/CharSequence;

.field private final tag:Ljava/lang/Object;

.field private final tagKey:I

.field private final title:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;IIILandroid/view/View$OnClickListener;ILjava/lang/Object;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "titleResId"    # I
    .param p3, "subheadingResId"    # I
    .param p4, "actionTextResId"    # I
    .param p5, "onClickListener"    # Landroid/view/View$OnClickListener;
    .param p6, "tagKey"    # I
    .param p7, "tag"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x0

    .line 71
    if-nez p2, :cond_0

    move-object v1, v3

    :goto_0
    if-nez p3, :cond_1

    move-object v2, v3

    :goto_1
    if-nez p4, :cond_2

    :goto_2
    move-object v0, p0

    move-object v4, p5

    move v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;ILjava/lang/Object;)V

    .line 76
    return-void

    .line 71
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    goto :goto_2
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;ILjava/lang/Object;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/CharSequence;
    .param p2, "subheading"    # Ljava/lang/CharSequence;
    .param p3, "actionText"    # Ljava/lang/CharSequence;
    .param p4, "onClickListener"    # Landroid/view/View$OnClickListener;
    .param p5, "tagKey"    # I
    .param p6, "tag"    # Ljava/lang/Object;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;->title:Ljava/lang/CharSequence;

    .line 62
    iput-object p2, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;->subheading:Ljava/lang/CharSequence;

    .line 63
    iput-object p3, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;->actionText:Ljava/lang/CharSequence;

    .line 64
    iput-object p4, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;->onClickListener:Landroid/view/View$OnClickListener;

    .line 65
    iput p5, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;->tagKey:I

    .line 66
    iput-object p6, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;->tag:Ljava/lang/Object;

    .line 67
    return-void
.end method


# virtual methods
.method public bindSectionHeading(Lcom/google/android/videos/ui/SectionHeadingHelper;I)V
    .locals 3
    .param p1, "viewHolder"    # Lcom/google/android/videos/ui/SectionHeadingHelper;
    .param p2, "remaining"    # I

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;->title:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;->subheading:Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;->actionText:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/android/videos/ui/SectionHeadingHelper;->setTexts(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 81
    iget-object v0, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;->onClickListener:Landroid/view/View$OnClickListener;

    iget v1, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;->tagKey:I

    iget-object v2, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;->tag:Ljava/lang/Object;

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/android/videos/ui/SectionHeadingHelper;->setOnClickListener(Landroid/view/View$OnClickListener;ILjava/lang/Object;)V

    .line 82
    iget-boolean v0, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;->dimmed:Z

    invoke-virtual {p1, v0}, Lcom/google/android/videos/ui/SectionHeadingHelper;->setDimmed(Z)V

    .line 83
    return-void
.end method

.method public setDimmed(Z)Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;
    .locals 0
    .param p1, "dimmed"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;->dimmed:Z

    .line 93
    return-object p0
.end method
