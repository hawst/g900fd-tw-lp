.class Lcom/google/android/videos/drm/DrmManagerV12;
.super Lcom/google/android/videos/drm/DrmManager;
.source "DrmManagerV12.java"

# interfaces
.implements Landroid/drm/DrmManagerClient$OnErrorListener;
.implements Landroid/drm/DrmManagerClient$OnEventListener;
.implements Landroid/drm/DrmManagerClient$OnInfoListener;
.implements Lcom/google/android/videos/drm/DrmManager$Listener;


# instance fields
.field private final appLevelDrmManager:Lcom/google/android/videos/drm/DrmManagerV8;

.field private final config:Lcom/google/android/videos/Config;

.field private final deviceId:Ljava/lang/String;

.field private final drmManager:Landroid/drm/DrmManagerClient;

.field private final eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private volatile frameworkDrmLevel:I

.field private final gservicesId:J

.field private final networkStatus:Lcom/google/android/videos/utils/NetworkStatus;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/Config;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/videos/logging/EventLogger;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "networkExecutor"    # Ljava/util/concurrent/Executor;
    .param p3, "drmExecutor"    # Ljava/util/concurrent/Executor;
    .param p4, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .param p5, "config"    # Lcom/google/android/videos/Config;
    .param p6, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;
    .param p7, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;

    .prologue
    .line 69
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/videos/drm/DrmManager;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/videos/accounts/AccountManagerWrapper;)V

    .line 70
    iput-object p5, p0, Lcom/google/android/videos/drm/DrmManagerV12;->config:Lcom/google/android/videos/Config;

    .line 71
    iput-object p6, p0, Lcom/google/android/videos/drm/DrmManagerV12;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 72
    iput-object p7, p0, Lcom/google/android/videos/drm/DrmManagerV12;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 74
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    .line 75
    .local v7, "contentResolver":Landroid/content/ContentResolver;
    const-string v0, "android_id"

    invoke-static {v7, v0}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/drm/DrmManagerV12;->deviceId:Ljava/lang/String;

    .line 76
    invoke-interface {p5}, Lcom/google/android/videos/Config;->gservicesId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/videos/drm/DrmManagerV12;->gservicesId:J

    .line 78
    new-instance v0, Lcom/google/android/videos/drm/DrmManagerV8;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/drm/DrmManagerV8;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/Config;Lcom/google/android/videos/logging/EventLogger;)V

    iput-object v0, p0, Lcom/google/android/videos/drm/DrmManagerV12;->appLevelDrmManager:Lcom/google/android/videos/drm/DrmManagerV8;

    .line 80
    iget-object v0, p0, Lcom/google/android/videos/drm/DrmManagerV12;->appLevelDrmManager:Lcom/google/android/videos/drm/DrmManagerV8;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/drm/DrmManagerV8;->setListener(Lcom/google/android/videos/drm/DrmManager$Listener;)V

    .line 82
    new-instance v0, Landroid/drm/DrmManagerClient;

    invoke-direct {v0, p1}, Landroid/drm/DrmManagerClient;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/videos/drm/DrmManagerV12;->drmManager:Landroid/drm/DrmManagerClient;

    .line 83
    iget-object v0, p0, Lcom/google/android/videos/drm/DrmManagerV12;->drmManager:Landroid/drm/DrmManagerClient;

    invoke-virtual {v0, p0}, Landroid/drm/DrmManagerClient;->setOnInfoListener(Landroid/drm/DrmManagerClient$OnInfoListener;)V

    .line 84
    iget-object v0, p0, Lcom/google/android/videos/drm/DrmManagerV12;->drmManager:Landroid/drm/DrmManagerClient;

    invoke-virtual {v0, p0}, Landroid/drm/DrmManagerClient;->setOnEventListener(Landroid/drm/DrmManagerClient$OnEventListener;)V

    .line 85
    iget-object v0, p0, Lcom/google/android/videos/drm/DrmManagerV12;->drmManager:Landroid/drm/DrmManagerClient;

    invoke-virtual {v0, p0}, Landroid/drm/DrmManagerClient;->setOnErrorListener(Landroid/drm/DrmManagerClient$OnErrorListener;)V

    .line 87
    invoke-direct {p0}, Lcom/google/android/videos/drm/DrmManagerV12;->initFrameworkDrmLevel()V

    .line 88
    return-void
.end method

.method private eventType(Landroid/drm/DrmEvent;)Ljava/lang/String;
    .locals 1
    .param p1, "event"    # Landroid/drm/DrmEvent;

    .prologue
    .line 429
    invoke-virtual {p1}, Landroid/drm/DrmEvent;->getType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 446
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 430
    :sswitch_0
    const-string v0, "ALL_RIGHTS_REMOVED"

    goto :goto_0

    .line 431
    :sswitch_1
    const-string v0, "DRM_INFO_PROCESSED"

    goto :goto_0

    .line 432
    :sswitch_2
    const-string v0, "ACCOUNT_ALREADY_REGISTERED"

    goto :goto_0

    .line 434
    :sswitch_3
    const-string v0, "ALREADY_REGISTERED_BY_ANOTHER_ACCOUNT"

    goto :goto_0

    .line 435
    :sswitch_4
    const-string v0, "REMOVE_RIGHTS"

    goto :goto_0

    .line 436
    :sswitch_5
    const-string v0, "RIGHTS_INSTALLED"

    goto :goto_0

    .line 437
    :sswitch_6
    const-string v0, "WAIT_FOR_RIGHTS"

    goto :goto_0

    .line 438
    :sswitch_7
    const-string v0, "NO_INTERNET_CONNECTION"

    goto :goto_0

    .line 439
    :sswitch_8
    const-string v0, "NOT_SUPPORTED"

    goto :goto_0

    .line 440
    :sswitch_9
    const-string v0, "OUT_OF_MEMORY"

    goto :goto_0

    .line 441
    :sswitch_a
    const-string v0, "PROCESS_DRM_INFO_FAILED"

    goto :goto_0

    .line 442
    :sswitch_b
    const-string v0, "REMOVE_ALL_RIGHTS_FAILED"

    goto :goto_0

    .line 443
    :sswitch_c
    const-string v0, "RIGHTS_NOT_INSTALLED"

    goto :goto_0

    .line 444
    :sswitch_d
    const-string v0, "RIGHTS_RENEWAL_NOT_ALLOWED"

    goto :goto_0

    .line 429
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_3
        0x2 -> :sswitch_4
        0x3 -> :sswitch_5
        0x4 -> :sswitch_6
        0x5 -> :sswitch_2
        0x3e9 -> :sswitch_0
        0x3ea -> :sswitch_1
        0x7d1 -> :sswitch_c
        0x7d2 -> :sswitch_d
        0x7d3 -> :sswitch_8
        0x7d4 -> :sswitch_9
        0x7d5 -> :sswitch_7
        0x7d6 -> :sswitch_a
        0x7d7 -> :sswitch_b
    .end sparse-switch
.end method

.method private getConstraints(Landroid/net/Uri;)Landroid/content/ContentValues;
    .locals 3
    .param p1, "stream"    # Landroid/net/Uri;

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/videos/drm/DrmManagerV12;->drmManager:Landroid/drm/DrmManagerClient;

    invoke-virtual {p0, p1}, Lcom/google/android/videos/drm/DrmManagerV12;->stripQueryParameters(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/drm/DrmManagerClient;->getConstraints(Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v0

    return-object v0
.end method

.method private getError(Landroid/net/Uri;)Lcom/google/android/videos/drm/DrmException;
    .locals 6
    .param p1, "stream"    # Landroid/net/Uri;

    .prologue
    .line 178
    invoke-direct {p0, p1}, Lcom/google/android/videos/drm/DrmManagerV12;->getConstraints(Landroid/net/Uri;)Landroid/content/ContentValues;

    move-result-object v0

    .line 179
    .local v0, "constraints":Landroid/content/ContentValues;
    sget-object v1, Lcom/google/android/videos/drm/DrmException$DrmError;->UNKNOWN:Lcom/google/android/videos/drm/DrmException$DrmError;

    .line 180
    .local v1, "drmError":Lcom/google/android/videos/drm/DrmException$DrmError;
    const/4 v2, 0x0

    .line 181
    .local v2, "errorCode":I
    if-nez v0, :cond_0

    .line 182
    sget-object v1, Lcom/google/android/videos/drm/DrmException$DrmError;->NO_LICENSE:Lcom/google/android/videos/drm/DrmException$DrmError;

    .line 193
    :goto_0
    new-instance v4, Lcom/google/android/videos/drm/DrmException;

    invoke-direct {v4, v1, v2}, Lcom/google/android/videos/drm/DrmException;-><init>(Lcom/google/android/videos/drm/DrmException$DrmError;I)V

    return-object v4

    .line 184
    :cond_0
    const-string v4, "WVLastErrorKey"

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 185
    .local v3, "lastError":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "WVLastErrorKey is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 186
    const-string v4, "ok"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-direct {p0, v0}, Lcom/google/android/videos/drm/DrmManagerV12;->getRemainingTime(Landroid/content/ContentValues;)I

    move-result v4

    if-nez v4, :cond_1

    .line 187
    sget-object v1, Lcom/google/android/videos/drm/DrmException$DrmError;->LICENSE_EXPIRED:Lcom/google/android/videos/drm/DrmException$DrmError;

    goto :goto_0

    .line 189
    :cond_1
    invoke-virtual {p0, v3}, Lcom/google/android/videos/drm/DrmManagerV12;->parseErrorCode(Ljava/lang/String;)I

    move-result v2

    .line 190
    invoke-virtual {p0, v2}, Lcom/google/android/videos/drm/DrmManagerV12;->getDrmError(I)Lcom/google/android/videos/drm/DrmException$DrmError;

    move-result-object v1

    goto :goto_0
.end method

.method private getLicense(Landroid/net/Uri;Lcom/google/android/videos/drm/DrmManager$Identifiers;Z)Lcom/google/android/videos/drm/DrmResponse;
    .locals 11
    .param p1, "stream"    # Landroid/net/Uri;
    .param p2, "ids"    # Lcom/google/android/videos/drm/DrmManager$Identifiers;
    .param p3, "refreshed"    # Z

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 157
    invoke-direct {p0, p1}, Lcom/google/android/videos/drm/DrmManagerV12;->getConstraints(Landroid/net/Uri;)Landroid/content/ContentValues;

    move-result-object v0

    .line 158
    .local v0, "constraints":Landroid/content/ContentValues;
    if-eqz v0, :cond_0

    const-string v3, "WVLicenseTypeKey"

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 159
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No license for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 173
    :goto_0
    return-object v1

    .line 163
    :cond_1
    const-string v3, "WVLicenseTypeKey"

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 164
    .local v10, "mode":I
    and-int/lit8 v3, v10, 0x1

    if-eqz v3, :cond_2

    move v4, v5

    .line 165
    .local v4, "allowsStreaming":Z
    :goto_1
    and-int/lit8 v3, v10, 0x2

    if-eqz v3, :cond_3

    .line 166
    .local v5, "allowsOffline":Z
    :goto_2
    if-nez v10, :cond_4

    .line 167
    const-string v2, "License mode is 0"

    invoke-static {v2}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    goto :goto_0

    .end local v4    # "allowsStreaming":Z
    .end local v5    # "allowsOffline":Z
    :cond_2
    move v4, v2

    .line 164
    goto :goto_1

    .restart local v4    # "allowsStreaming":Z
    :cond_3
    move v5, v2

    .line 165
    goto :goto_2

    .line 171
    .restart local v5    # "allowsOffline":Z
    :cond_4
    invoke-direct {p0, v0}, Lcom/google/android/videos/drm/DrmManagerV12;->getSecondsSinceActivation(Landroid/content/ContentValues;)I

    move-result v7

    .line 173
    .local v7, "secondsSinceActivation":I
    new-instance v1, Lcom/google/android/videos/drm/DrmResponse;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {p0, v0}, Lcom/google/android/videos/drm/DrmManagerV12;->getRemainingTime(Landroid/content/ContentValues;)I

    move-result v6

    move-object v8, p2

    move v9, p3

    invoke-direct/range {v1 .. v9}, Lcom/google/android/videos/drm/DrmResponse;-><init>(JZZIILcom/google/android/videos/drm/DrmManager$Identifiers;Z)V

    goto :goto_0
.end method

.method private getRemainingTime(Landroid/content/ContentValues;)I
    .locals 2
    .param p1, "constraints"    # Landroid/content/ContentValues;

    .prologue
    .line 218
    const-string v1, "license_expiry_time"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 219
    .local v0, "seconds":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getSecondsSinceActivation(Landroid/content/ContentValues;)I
    .locals 2
    .param p1, "constraints"    # Landroid/content/ContentValues;

    .prologue
    .line 223
    const-string v1, "license_start_time"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 225
    .local v0, "seconds":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private initFrameworkDrmLevel()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    .line 91
    new-instance v1, Landroid/drm/DrmInfoRequest;

    const/4 v5, 0x1

    const-string v6, "video/wvm"

    invoke-direct {v1, v5, v6}, Landroid/drm/DrmInfoRequest;-><init>(ILjava/lang/String;)V

    .line 93
    .local v1, "drmInfoRequest":Landroid/drm/DrmInfoRequest;
    const-string v5, "WVPortalKey"

    iget-object v6, p0, Lcom/google/android/videos/drm/DrmManagerV12;->config:Lcom/google/android/videos/Config;

    invoke-interface {v6}, Lcom/google/android/videos/Config;->wvPortalName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 94
    iget-object v5, p0, Lcom/google/android/videos/drm/DrmManagerV12;->drmManager:Landroid/drm/DrmManagerClient;

    invoke-virtual {v5, v1}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v0

    .line 96
    .local v0, "drmInfo":Landroid/drm/DrmInfo;
    if-nez v0, :cond_0

    .line 98
    iget-object v5, p0, Lcom/google/android/videos/drm/DrmManagerV12;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    invoke-interface {v5}, Lcom/google/android/videos/logging/EventLogger;->onFrameworkDrmInitFailed()V

    .line 99
    const/4 v5, -0x2

    iput v5, p0, Lcom/google/android/videos/drm/DrmManagerV12;->frameworkDrmLevel:I

    .line 120
    :goto_0
    return-void

    .line 103
    :cond_0
    const-string v5, "WVDrmInfoRequestStatusKey"

    invoke-virtual {v0, v5}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 104
    .local v4, "drmInfoRequestStatusString":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 106
    iput v8, p0, Lcom/google/android/videos/drm/DrmManagerV12;->frameworkDrmLevel:I

    .line 107
    iget-object v5, p0, Lcom/google/android/videos/drm/DrmManagerV12;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    const-wide/16 v6, -0x1

    invoke-interface {v5, v6, v7}, Lcom/google/android/videos/logging/EventLogger;->onFrameworkDrmInit(J)V

    goto :goto_0

    .line 111
    :cond_1
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 112
    .local v2, "drmInfoRequestStatus":J
    iget-object v5, p0, Lcom/google/android/videos/drm/DrmManagerV12;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    invoke-interface {v5, v2, v3}, Lcom/google/android/videos/logging/EventLogger;->onFrameworkDrmInit(J)V

    .line 113
    const-wide/16 v6, 0x0

    cmp-long v5, v2, v6

    if-nez v5, :cond_2

    .line 114
    iput v8, p0, Lcom/google/android/videos/drm/DrmManagerV12;->frameworkDrmLevel:I

    goto :goto_0

    .line 115
    :cond_2
    const-wide/16 v6, 0x2

    cmp-long v5, v2, v6

    if-nez v5, :cond_3

    .line 116
    const/4 v5, 0x2

    iput v5, p0, Lcom/google/android/videos/drm/DrmManagerV12;->frameworkDrmLevel:I

    goto :goto_0

    .line 118
    :cond_3
    const/4 v5, -0x1

    iput v5, p0, Lcom/google/android/videos/drm/DrmManagerV12;->frameworkDrmLevel:I

    goto :goto_0
.end method

.method private invokePendingCallback(Landroid/drm/DrmInfo;ZZ)V
    .locals 16
    .param p1, "drmInfo"    # Landroid/drm/DrmInfo;
    .param p2, "requested"    # Z
    .param p3, "refreshed"    # Z

    .prologue
    .line 345
    new-instance v3, Lcom/google/android/videos/drm/DrmManager$Identifiers;

    const-string v4, "WVKeyIDKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    const-string v6, "WVAssetIDKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    const-string v8, "WVSystemIDKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-direct/range {v3 .. v9}, Lcom/google/android/videos/drm/DrmManager$Identifiers;-><init>(JJJ)V

    .line 350
    .local v3, "ids":Lcom/google/android/videos/drm/DrmManager$Identifiers;
    const-string v4, "VideosRequest"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/videos/drm/DrmRequest;

    .line 352
    .local v14, "request":Lcom/google/android/videos/drm/DrmRequest;
    const-string v4, "VideosCallback"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/videos/async/Callback;

    .line 355
    .local v11, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/drm/DrmResponse;>;"
    if-eqz v14, :cond_0

    if-nez v11, :cond_1

    .line 356
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Request or callback missing from DrmEvent "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 411
    :goto_0
    return-void

    .line 360
    :cond_1
    iget-object v4, v14, Lcom/google/android/videos/drm/DrmRequest;->stream:Lcom/google/android/videos/streams/MediaStream;

    iget-object v4, v4, Lcom/google/android/videos/streams/MediaStream;->uri:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/videos/drm/DrmManagerV12;->stripQueryParameters(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v10

    .line 362
    .local v10, "assetUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/google/android/videos/drm/DrmManagerV12;->getError(Landroid/net/Uri;)Lcom/google/android/videos/drm/DrmException;

    move-result-object v12

    .line 363
    .local v12, "error":Lcom/google/android/videos/drm/DrmException;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/drm/DrmManagerV12;->drmManager:Landroid/drm/DrmManagerClient;

    invoke-virtual {v10}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/drm/DrmManagerClient;->checkRightsStatus(Ljava/lang/String;)I

    move-result v15

    .line 364
    .local v15, "result":I
    if-eqz p2, :cond_2

    iget-object v4, v12, Lcom/google/android/videos/drm/DrmException;->drmError:Lcom/google/android/videos/drm/DrmException$DrmError;

    sget-object v5, Lcom/google/android/videos/drm/DrmException$DrmError;->AUTHENTICATION_FAILED:Lcom/google/android/videos/drm/DrmException$DrmError;

    if-ne v4, v5, :cond_2

    .line 366
    invoke-interface {v11, v14, v12}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0

    .line 367
    :cond_2
    iget-object v4, v14, Lcom/google/android/videos/drm/DrmRequest;->type:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    iget-boolean v4, v4, Lcom/google/android/videos/drm/DrmRequest$RequestType;->isOffline:Z

    if-nez v4, :cond_3

    if-eqz p3, :cond_7

    :cond_3
    if-nez v15, :cond_7

    .line 368
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-direct {v0, v10, v3, v1}, Lcom/google/android/videos/drm/DrmManagerV12;->getLicense(Landroid/net/Uri;Lcom/google/android/videos/drm/DrmManager$Identifiers;Z)Lcom/google/android/videos/drm/DrmResponse;

    move-result-object v13

    .line 369
    .local v13, "license":Lcom/google/android/videos/drm/DrmResponse;
    if-nez v13, :cond_4

    .line 372
    invoke-interface {v11, v14, v12}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0

    .line 373
    :cond_4
    iget-object v4, v14, Lcom/google/android/videos/drm/DrmRequest;->type:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    sget-object v5, Lcom/google/android/videos/drm/DrmRequest$RequestType;->OFFLINE_UNPIN:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    if-eq v4, v5, :cond_5

    iget-object v4, v14, Lcom/google/android/videos/drm/DrmRequest;->type:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    iget-boolean v4, v4, Lcom/google/android/videos/drm/DrmRequest$RequestType;->isOffline:Z

    if-eqz v4, :cond_6

    iget-boolean v4, v13, Lcom/google/android/videos/drm/DrmResponse;->allowsOffline:Z

    if-nez v4, :cond_6

    .line 377
    :cond_5
    invoke-interface {v11, v14, v12}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0

    .line 379
    :cond_6
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "License is valid "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 380
    invoke-interface {v11, v14, v13}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 383
    .end local v13    # "license":Lcom/google/android/videos/drm/DrmResponse;
    :cond_7
    iget-object v4, v14, Lcom/google/android/videos/drm/DrmRequest;->type:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    sget-object v5, Lcom/google/android/videos/drm/DrmRequest$RequestType;->OFFLINE_UNPIN:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    if-ne v4, v5, :cond_9

    iget-object v4, v12, Lcom/google/android/videos/drm/DrmException;->drmError:Lcom/google/android/videos/drm/DrmException$DrmError;

    sget-object v5, Lcom/google/android/videos/drm/DrmException$DrmError;->UNPIN_SUCCESSFUL:Lcom/google/android/videos/drm/DrmException$DrmError;

    if-eq v4, v5, :cond_8

    iget-object v4, v12, Lcom/google/android/videos/drm/DrmException;->drmError:Lcom/google/android/videos/drm/DrmException$DrmError;

    sget-object v5, Lcom/google/android/videos/drm/DrmException$DrmError;->NO_LICENSE:Lcom/google/android/videos/drm/DrmException$DrmError;

    if-ne v4, v5, :cond_9

    .line 387
    :cond_8
    const/4 v4, 0x0

    invoke-interface {v11, v14, v4}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 388
    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/drm/DrmManagerV12;->config:Lcom/google/android/videos/Config;

    invoke-interface {v4}, Lcom/google/android/videos/Config;->fallbackDrmErrorCodes()Ljava/util/List;

    move-result-object v4

    iget v5, v12, Lcom/google/android/videos/drm/DrmException;->errorCode:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 389
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/drm/DrmManagerV12;->appLevelDrmManager:Lcom/google/android/videos/drm/DrmManagerV8;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/videos/drm/DrmManagerV12;->frameworkDrmLevel:I

    iget v6, v12, Lcom/google/android/videos/drm/DrmException;->errorCode:I

    invoke-virtual {v4, v5, v6}, Lcom/google/android/videos/drm/DrmManagerV8;->setFrameworkDrmFallbackInfo(II)V

    .line 390
    const/4 v4, -0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/videos/drm/DrmManagerV12;->frameworkDrmLevel:I

    .line 391
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/drm/DrmManagerV12;->appLevelDrmManager:Lcom/google/android/videos/drm/DrmManagerV8;

    invoke-virtual {v4}, Lcom/google/android/videos/drm/DrmManagerV8;->getDrmLevel()I

    move-result v2

    .line 392
    .local v2, "appDrmLevel":I
    if-lez v2, :cond_a

    .line 393
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Falling back to application level drm: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 394
    new-instance v4, Lcom/google/android/videos/drm/DrmFallbackException;

    iget-object v5, v12, Lcom/google/android/videos/drm/DrmException;->drmError:Lcom/google/android/videos/drm/DrmException$DrmError;

    iget v6, v12, Lcom/google/android/videos/drm/DrmException;->errorCode:I

    invoke-direct {v4, v5, v6, v2}, Lcom/google/android/videos/drm/DrmFallbackException;-><init>(Lcom/google/android/videos/drm/DrmException$DrmError;II)V

    invoke-interface {v11, v14, v4}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 397
    :cond_a
    invoke-interface {v11, v14, v12}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 407
    .end local v2    # "appDrmLevel":I
    :cond_b
    invoke-interface {v11, v14, v12}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto/16 :goto_0
.end method

.method private logDrmInfoRequest(Landroid/drm/DrmInfoRequest;)V
    .locals 7
    .param p1, "drmRequest"    # Landroid/drm/DrmInfoRequest;

    .prologue
    .line 414
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "infoType="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/drm/DrmInfoRequest;->getInfoType()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mimeType="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/drm/DrmInfoRequest;->getMimeType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 415
    invoke-virtual {p1}, Landroid/drm/DrmInfoRequest;->keyIterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 416
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 417
    .local v1, "key":Ljava/lang/String;
    invoke-virtual {p1, v1}, Landroid/drm/DrmInfoRequest;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 418
    .local v2, "valueObject":Ljava/lang/Object;
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 419
    .local v3, "valueString":Ljava/lang/String;
    const-string v4, "WVCAUserDataKey"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 420
    invoke-static {v3}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "(^|&)oauth=.*?($|&)"

    const-string v6, "$1oauth=hidden$2"

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "(^|&)robottoken=.*?($|&)"

    const-string v6, "$1robottoken=hidden$2"

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 424
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 426
    .end local v1    # "key":Ljava/lang/String;
    .end local v2    # "valueObject":Ljava/lang/Object;
    .end local v3    # "valueString":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private shouldRefreshLicense(Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/drm/DrmResponse;)Z
    .locals 3
    .param p1, "request"    # Lcom/google/android/videos/drm/DrmRequest;
    .param p2, "existingLicense"    # Lcom/google/android/videos/drm/DrmResponse;

    .prologue
    .line 306
    iget-object v1, p1, Lcom/google/android/videos/drm/DrmRequest;->stream:Lcom/google/android/videos/streams/MediaStream;

    iget-object v1, v1, Lcom/google/android/videos/streams/MediaStream;->uri:Landroid/net/Uri;

    invoke-virtual {p0, v1}, Lcom/google/android/videos/drm/DrmManagerV12;->stripQueryParameters(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 307
    .local v0, "assetUri":Landroid/net/Uri;
    iget-object v1, p1, Lcom/google/android/videos/drm/DrmRequest;->type:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    sget-object v2, Lcom/google/android/videos/drm/DrmRequest$RequestType;->OFFLINE:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    if-ne v1, v2, :cond_0

    iget-object v1, p1, Lcom/google/android/videos/drm/DrmRequest;->authToken:Ljava/lang/String;

    if-eqz v1, :cond_1

    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/drm/DrmManagerV12;->drmManager:Landroid/drm/DrmManagerClient;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/drm/DrmManagerClient;->checkRightsStatus(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected getDrmError(I)Lcom/google/android/videos/drm/DrmException$DrmError;
    .locals 1
    .param p1, "errorCode"    # I

    .prologue
    .line 198
    sparse-switch p1, :sswitch_data_0

    .line 208
    invoke-super {p0, p1}, Lcom/google/android/videos/drm/DrmManager;->getDrmError(I)Lcom/google/android/videos/drm/DrmException$DrmError;

    move-result-object v0

    :goto_0
    return-object v0

    .line 200
    :sswitch_0
    sget-object v0, Lcom/google/android/videos/drm/DrmException$DrmError;->EMM_DECODE_FAILED:Lcom/google/android/videos/drm/DrmException$DrmError;

    goto :goto_0

    .line 202
    :sswitch_1
    sget-object v0, Lcom/google/android/videos/drm/DrmException$DrmError;->INVALID_KEYBOX_SYSTEM_ID:Lcom/google/android/videos/drm/DrmException$DrmError;

    goto :goto_0

    .line 204
    :sswitch_2
    sget-object v0, Lcom/google/android/videos/drm/DrmException$DrmError;->KEY_VERIFICATION_FAILED:Lcom/google/android/videos/drm/DrmException$DrmError;

    goto :goto_0

    .line 206
    :sswitch_3
    sget-object v0, Lcom/google/android/videos/drm/DrmException$DrmError;->NETWORK_FAILURE:Lcom/google/android/videos/drm/DrmException$DrmError;

    goto :goto_0

    .line 198
    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x2c -> :sswitch_1
        0x31 -> :sswitch_2
        0x198 -> :sswitch_3
    .end sparse-switch
.end method

.method public getDrmLevel()I
    .locals 2

    .prologue
    .line 124
    iget v0, p0, Lcom/google/android/videos/drm/DrmManagerV12;->frameworkDrmLevel:I

    .line 125
    .local v0, "frameworkDrmLevel":I
    if-gez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/drm/DrmManagerV12;->appLevelDrmManager:Lcom/google/android/videos/drm/DrmManagerV8;

    invoke-virtual {v1}, Lcom/google/android/videos/drm/DrmManagerV8;->getDrmLevel()I

    move-result v0

    .end local v0    # "frameworkDrmLevel":I
    :cond_0
    return v0
.end method

.method public getPlayableUri(Landroid/net/Uri;Z)Landroid/net/Uri;
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "appLevelDrm"    # Z

    .prologue
    .line 130
    if-eqz p2, :cond_0

    .line 131
    iget-object v0, p0, Lcom/google/android/videos/drm/DrmManagerV12;->appLevelDrmManager:Lcom/google/android/videos/drm/DrmManagerV8;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/videos/drm/DrmManagerV8;->getPlayableUri(Landroid/net/Uri;Z)Landroid/net/Uri;

    move-result-object p1

    .line 133
    .end local p1    # "uri":Landroid/net/Uri;
    :cond_0
    return-object p1
.end method

.method public onError(Landroid/drm/DrmManagerClient;Landroid/drm/DrmErrorEvent;)V
    .locals 3
    .param p1, "client"    # Landroid/drm/DrmManagerClient;
    .param p2, "event"    # Landroid/drm/DrmErrorEvent;

    .prologue
    .line 330
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DRM error "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, p2}, Lcom/google/android/videos/drm/DrmManagerV12;->eventType(Landroid/drm/DrmEvent;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/drm/DrmErrorEvent;->getUniqueId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/drm/DrmErrorEvent;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 331
    const-string v1, "drm_info_object"

    invoke-virtual {p2, v1}, Landroid/drm/DrmErrorEvent;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/drm/DrmInfo;

    .line 332
    .local v0, "drmInfo":Landroid/drm/DrmInfo;
    if-eqz v0, :cond_0

    .line 333
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/videos/drm/DrmManagerV12;->invokePendingCallback(Landroid/drm/DrmInfo;ZZ)V

    .line 337
    :goto_0
    return-void

    .line 335
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DrmInfo missing from DrmEvent "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onEvent(Landroid/drm/DrmManagerClient;Landroid/drm/DrmEvent;)V
    .locals 4
    .param p1, "client"    # Landroid/drm/DrmManagerClient;
    .param p2, "event"    # Landroid/drm/DrmEvent;

    .prologue
    const/4 v3, 0x1

    .line 314
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DRM event "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, p2}, Lcom/google/android/videos/drm/DrmManagerV12;->eventType(Landroid/drm/DrmEvent;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/drm/DrmEvent;->getUniqueId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/drm/DrmEvent;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 315
    const-string v1, "drm_info_object"

    invoke-virtual {p2, v1}, Landroid/drm/DrmEvent;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/drm/DrmInfo;

    .line 316
    .local v0, "drmInfo":Landroid/drm/DrmInfo;
    if-eqz v0, :cond_0

    .line 317
    invoke-direct {p0, v0, v3, v3}, Lcom/google/android/videos/drm/DrmManagerV12;->invokePendingCallback(Landroid/drm/DrmInfo;ZZ)V

    .line 321
    :goto_0
    return-void

    .line 319
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DrmInfo missing from DrmEvent "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onHeartbeatError(Ljava/lang/String;I)V
    .locals 0
    .param p1, "assetPathKey"    # Ljava/lang/String;
    .param p2, "cgiError"    # I

    .prologue
    .line 452
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/drm/DrmManagerV12;->notifyHeartbeatError(Ljava/lang/String;I)V

    .line 453
    return-void
.end method

.method public onInfo(Landroid/drm/DrmManagerClient;Landroid/drm/DrmInfoEvent;)V
    .locals 2
    .param p1, "client"    # Landroid/drm/DrmManagerClient;
    .param p2, "event"    # Landroid/drm/DrmInfoEvent;

    .prologue
    .line 325
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DRM info "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, p2}, Lcom/google/android/videos/drm/DrmManagerV12;->eventType(Landroid/drm/DrmEvent;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Landroid/drm/DrmInfoEvent;->getUniqueId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Landroid/drm/DrmInfoEvent;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 326
    return-void
.end method

.method public onPlaybackStopped(Ljava/lang/String;Lcom/google/android/videos/drm/DrmManager$StopReason;)V
    .locals 0
    .param p1, "assetPathKey"    # Ljava/lang/String;
    .param p2, "reason"    # Lcom/google/android/videos/drm/DrmManager$StopReason;

    .prologue
    .line 458
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/drm/DrmManagerV12;->notifyPlaybackStopped(Ljava/lang/String;Lcom/google/android/videos/drm/DrmManager$StopReason;)V

    .line 459
    return-void
.end method

.method public request(Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/async/Callback;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/drm/DrmRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/drm/DrmRequest;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/drm/DrmRequest;",
            "Lcom/google/android/videos/drm/DrmResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 138
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/drm/DrmResponse;>;"
    iget-object v0, p1, Lcom/google/android/videos/drm/DrmRequest;->stream:Lcom/google/android/videos/streams/MediaStream;

    iget-object v0, v0, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    iget v0, v0, Lcom/google/android/videos/streams/ItagInfo;->drmType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 139
    iget-object v0, p0, Lcom/google/android/videos/drm/DrmManagerV12;->appLevelDrmManager:Lcom/google/android/videos/drm/DrmManagerV8;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/drm/DrmManagerV8;->request(Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/async/Callback;)V

    .line 143
    :goto_0
    return-void

    .line 142
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/videos/drm/DrmManager;->request(Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/async/Callback;)V

    goto :goto_0
.end method

.method protected requestOfflineRights(Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/async/Callback;)V
    .locals 4
    .param p1, "request"    # Lcom/google/android/videos/drm/DrmRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/drm/DrmRequest;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/drm/DrmRequest;",
            "Lcom/google/android/videos/drm/DrmResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 148
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/drm/DrmResponse;>;"
    iget-object v2, p1, Lcom/google/android/videos/drm/DrmRequest;->stream:Lcom/google/android/videos/streams/MediaStream;

    iget-object v2, v2, Lcom/google/android/videos/streams/MediaStream;->uri:Landroid/net/Uri;

    invoke-virtual {p0, v2}, Lcom/google/android/videos/drm/DrmManagerV12;->stripQueryParameters(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 149
    .local v0, "assetUri":Landroid/net/Uri;
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/videos/drm/DrmManagerV12;->getLicense(Landroid/net/Uri;Lcom/google/android/videos/drm/DrmManager$Identifiers;Z)Lcom/google/android/videos/drm/DrmResponse;

    move-result-object v1

    .line 150
    .local v1, "license":Lcom/google/android/videos/drm/DrmResponse;
    if-eqz v1, :cond_0

    iget-boolean v2, v1, Lcom/google/android/videos/drm/DrmResponse;->allowsOffline:Z

    if-nez v2, :cond_0

    .line 151
    const/4 v1, 0x0

    .line 153
    :cond_0
    invoke-virtual {p0, p1, v1, p2}, Lcom/google/android/videos/drm/DrmManagerV12;->requestRights(Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/drm/DrmResponse;Lcom/google/android/videos/async/Callback;)V

    .line 154
    return-void
.end method

.method protected requestRights(Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/drm/DrmResponse;Lcom/google/android/videos/async/Callback;)V
    .locals 11
    .param p1, "request"    # Lcom/google/android/videos/drm/DrmRequest;
    .param p2, "existingLicense"    # Lcom/google/android/videos/drm/DrmResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/drm/DrmRequest;",
            "Lcom/google/android/videos/drm/DrmResponse;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/drm/DrmRequest;",
            "Lcom/google/android/videos/drm/DrmResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 231
    .local p3, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/drm/DrmResponse;>;"
    new-instance v2, Landroid/drm/DrmInfoRequest;

    const/4 v7, 0x3

    const-string v8, "video/wvm"

    invoke-direct {v2, v7, v8}, Landroid/drm/DrmInfoRequest;-><init>(ILjava/lang/String;)V

    .line 234
    .local v2, "drmInfoRequest":Landroid/drm/DrmInfoRequest;
    iget-object v7, p0, Lcom/google/android/videos/drm/DrmManagerV12;->config:Lcom/google/android/videos/Config;

    invoke-interface {v7}, Lcom/google/android/videos/Config;->wvClassicDrmServerUri()Ljava/lang/String;

    move-result-object v3

    .line 235
    .local v3, "drmServerUri":Ljava/lang/String;
    const-string v7, "WVDRMServerKey"

    invoke-virtual {v2, v7, v3}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 236
    const-string v7, "WVPortalKey"

    iget-object v8, p0, Lcom/google/android/videos/drm/DrmManagerV12;->config:Lcom/google/android/videos/Config;

    invoke-interface {v8}, Lcom/google/android/videos/Config;->wvPortalName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 237
    const-string v7, "WVAssetURIKey"

    iget-object v8, p1, Lcom/google/android/videos/drm/DrmRequest;->stream:Lcom/google/android/videos/streams/MediaStream;

    iget-object v8, v8, Lcom/google/android/videos/streams/MediaStream;->uri:Landroid/net/Uri;

    invoke-virtual {p0, v8}, Lcom/google/android/videos/drm/DrmManagerV12;->stripQueryParameters(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 238
    const-string v7, "WVDeviceIDKey"

    iget-object v8, p0, Lcom/google/android/videos/drm/DrmManagerV12;->deviceId:Ljava/lang/String;

    invoke-virtual {v2, v7, v8}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 239
    const-string v8, "WVLicenseTypeKey"

    iget-object v7, p1, Lcom/google/android/videos/drm/DrmRequest;->type:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    iget-boolean v7, v7, Lcom/google/android/videos/drm/DrmRequest$RequestType;->isOffline:Z

    if-eqz v7, :cond_5

    const-string v7, "3"

    :goto_0
    invoke-virtual {v2, v8, v7}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 241
    iget-object v7, p1, Lcom/google/android/videos/drm/DrmRequest;->ids:Lcom/google/android/videos/drm/DrmManager$Identifiers;

    if-eqz v7, :cond_0

    .line 242
    const-string v7, "WVKeyIDKey"

    iget-object v8, p1, Lcom/google/android/videos/drm/DrmRequest;->ids:Lcom/google/android/videos/drm/DrmManager$Identifiers;

    iget-wide v8, v8, Lcom/google/android/videos/drm/DrmManager$Identifiers;->keyId:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 243
    const-string v7, "WVAssetIDKey"

    iget-object v8, p1, Lcom/google/android/videos/drm/DrmRequest;->ids:Lcom/google/android/videos/drm/DrmManager$Identifiers;

    iget-wide v8, v8, Lcom/google/android/videos/drm/DrmManager$Identifiers;->assetId:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 244
    const-string v7, "WVSystemIDKey"

    iget-object v8, p1, Lcom/google/android/videos/drm/DrmRequest;->ids:Lcom/google/android/videos/drm/DrmManager$Identifiers;

    iget-wide v8, v8, Lcom/google/android/videos/drm/DrmManager$Identifiers;->systemId:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 247
    :cond_0
    iget-object v7, p1, Lcom/google/android/videos/drm/DrmRequest;->playbackId:Ljava/lang/String;

    if-eqz v7, :cond_1

    .line 248
    const-string v7, "WVStreamIDKey"

    iget-object v8, p1, Lcom/google/android/videos/drm/DrmRequest;->playbackId:Ljava/lang/String;

    invoke-virtual {v2, v7, v8}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 250
    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "v=2"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 251
    .local v6, "userData":Ljava/lang/StringBuilder;
    const-string v7, "&videoid="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p1, Lcom/google/android/videos/drm/DrmRequest;->videoId:Ljava/lang/String;

    invoke-static {v8}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    const-string v7, "&aid="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/google/android/videos/drm/DrmManagerV12;->gservicesId:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 253
    const-string v7, "&root="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v7, p0, Lcom/google/android/videos/drm/DrmManagerV12;->frameworkDrmLevel:I

    const/4 v9, 0x3

    if-eq v7, v9, :cond_6

    const/4 v7, 0x1

    :goto_1
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 254
    iget-object v7, p1, Lcom/google/android/videos/drm/DrmRequest;->authToken:Ljava/lang/String;

    if-eqz v7, :cond_2

    .line 255
    const-string v7, "&oauth="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p1, Lcom/google/android/videos/drm/DrmRequest;->authToken:Ljava/lang/String;

    invoke-static {v8}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    :cond_2
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/google/android/videos/drm/DrmResponse;->isActivated()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 258
    const-string v7, "&time_since_started="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p2, Lcom/google/android/videos/drm/DrmResponse;->secondsSinceActivation:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 260
    :cond_3
    iget-object v7, p1, Lcom/google/android/videos/drm/DrmRequest;->type:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    sget-object v8, Lcom/google/android/videos/drm/DrmRequest$RequestType;->OFFLINE_UNPIN:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    if-ne v7, v8, :cond_4

    .line 261
    const-string v7, "&unpin=true"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264
    :cond_4
    const-string v7, "WVCAUserDataKey"

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 266
    invoke-direct {p0, v2}, Lcom/google/android/videos/drm/DrmManagerV12;->logDrmInfoRequest(Landroid/drm/DrmInfoRequest;)V

    .line 270
    :try_start_0
    iget-object v8, p0, Lcom/google/android/videos/drm/DrmManagerV12;->drmManager:Landroid/drm/DrmManagerClient;

    monitor-enter v8
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 271
    :try_start_1
    iget-object v7, p0, Lcom/google/android/videos/drm/DrmManagerV12;->drmManager:Landroid/drm/DrmManagerClient;

    invoke-virtual {v7, v2}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v1

    .line 272
    .local v1, "drmInfo":Landroid/drm/DrmInfo;
    if-nez v1, :cond_9

    .line 273
    iget-object v7, p1, Lcom/google/android/videos/drm/DrmRequest;->stream:Lcom/google/android/videos/streams/MediaStream;

    iget-object v7, v7, Lcom/google/android/videos/streams/MediaStream;->uri:Landroid/net/Uri;

    invoke-direct {p0, v7}, Lcom/google/android/videos/drm/DrmManagerV12;->getConstraints(Landroid/net/Uri;)Landroid/content/ContentValues;

    move-result-object v0

    .line 274
    .local v0, "constraints":Landroid/content/ContentValues;
    if-eqz v0, :cond_7

    const-string v7, "WVLastErrorKey"

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 275
    const-string v7, "WVLastErrorKey"

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/videos/drm/DrmManagerV12;->parseErrorCode(Ljava/lang/String;)I

    move-result v5

    .line 276
    .local v5, "errorCode":I
    if-lez v5, :cond_7

    .line 277
    new-instance v7, Lcom/google/android/videos/drm/DrmException;

    invoke-virtual {p0, v5}, Lcom/google/android/videos/drm/DrmManagerV12;->getDrmError(I)Lcom/google/android/videos/drm/DrmException$DrmError;

    move-result-object v9

    invoke-direct {v7, v9, v5}, Lcom/google/android/videos/drm/DrmException;-><init>(Lcom/google/android/videos/drm/DrmException$DrmError;I)V

    throw v7

    .line 299
    .end local v0    # "constraints":Landroid/content/ContentValues;
    .end local v1    # "drmInfo":Landroid/drm/DrmInfo;
    .end local v5    # "errorCode":I
    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v7
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 300
    :catch_0
    move-exception v4

    .line 301
    .local v4, "e":Ljava/lang/Exception;
    invoke-interface {p3, p1, v4}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 303
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_2
    return-void

    .line 239
    .end local v6    # "userData":Ljava/lang/StringBuilder;
    :cond_5
    const-string v7, "1"

    goto/16 :goto_0

    .line 253
    .restart local v6    # "userData":Ljava/lang/StringBuilder;
    :cond_6
    const/4 v7, 0x0

    goto/16 :goto_1

    .line 280
    .restart local v0    # "constraints":Landroid/content/ContentValues;
    .restart local v1    # "drmInfo":Landroid/drm/DrmInfo;
    :cond_7
    :try_start_3
    iget-object v7, p1, Lcom/google/android/videos/drm/DrmRequest;->type:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    iget-boolean v7, v7, Lcom/google/android/videos/drm/DrmRequest$RequestType;->isOffline:Z

    if-eqz v7, :cond_8

    iget-object v7, p1, Lcom/google/android/videos/drm/DrmRequest;->ids:Lcom/google/android/videos/drm/DrmManager$Identifiers;

    if-eqz v7, :cond_8

    .line 281
    new-instance v7, Lcom/google/android/videos/drm/DrmException;

    sget-object v9, Lcom/google/android/videos/drm/DrmException$DrmError;->UNKNOWN:Lcom/google/android/videos/drm/DrmException$DrmError;

    const/4 v10, -0x3

    invoke-direct {v7, v9, v10}, Lcom/google/android/videos/drm/DrmException;-><init>(Lcom/google/android/videos/drm/DrmException$DrmError;I)V

    throw v7

    .line 284
    :cond_8
    new-instance v7, Lcom/google/android/videos/drm/DrmException;

    sget-object v9, Lcom/google/android/videos/drm/DrmException$DrmError;->NETWORK_FAILURE:Lcom/google/android/videos/drm/DrmException$DrmError;

    const/4 v10, -0x3

    invoke-direct {v7, v9, v10}, Lcom/google/android/videos/drm/DrmException;-><init>(Lcom/google/android/videos/drm/DrmException$DrmError;I)V

    throw v7

    .line 286
    .end local v0    # "constraints":Landroid/content/ContentValues;
    :cond_9
    const-string v7, "VideosRequest"

    invoke-virtual {v1, v7, p1}, Landroid/drm/DrmInfo;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 287
    const-string v7, "VideosCallback"

    invoke-virtual {v1, v7, p3}, Landroid/drm/DrmInfo;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 288
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/drm/DrmManagerV12;->shouldRefreshLicense(Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/drm/DrmResponse;)Z

    move-result v7

    if-nez v7, :cond_b

    .line 289
    const-string v7, "No need to refresh, using cached license"

    invoke-static {v7}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 290
    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-direct {p0, v1, v7, v9}, Lcom/google/android/videos/drm/DrmManagerV12;->invokePendingCallback(Landroid/drm/DrmInfo;ZZ)V

    .line 299
    :cond_a
    :goto_3
    monitor-exit v8

    goto :goto_2

    .line 291
    :cond_b
    iget-object v7, p0, Lcom/google/android/videos/drm/DrmManagerV12;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v7}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v7

    if-nez v7, :cond_c

    .line 292
    const-string v7, "Network unavailable, using cached license"

    invoke-static {v7}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 293
    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-direct {p0, v1, v7, v9}, Lcom/google/android/videos/drm/DrmManagerV12;->invokePendingCallback(Landroid/drm/DrmInfo;ZZ)V

    goto :goto_3

    .line 294
    :cond_c
    iget-object v7, p0, Lcom/google/android/videos/drm/DrmManagerV12;->drmManager:Landroid/drm/DrmManagerClient;

    invoke-virtual {v7, v1}, Landroid/drm/DrmManagerClient;->processDrmInfo(Landroid/drm/DrmInfo;)I

    move-result v7

    if-eqz v7, :cond_a

    .line 295
    new-instance v7, Lcom/google/android/videos/drm/DrmException;

    sget-object v9, Lcom/google/android/videos/drm/DrmException$DrmError;->UNKNOWN:Lcom/google/android/videos/drm/DrmException$DrmError;

    const/4 v10, -0x4

    invoke-direct {v7, v9, v10}, Lcom/google/android/videos/drm/DrmException;-><init>(Lcom/google/android/videos/drm/DrmException$DrmError;I)V

    throw v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method
