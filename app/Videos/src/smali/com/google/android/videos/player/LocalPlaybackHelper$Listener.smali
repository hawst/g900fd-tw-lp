.class public interface abstract Lcom/google/android/videos/player/LocalPlaybackHelper$Listener;
.super Ljava/lang/Object;
.source "LocalPlaybackHelper.java"

# interfaces
.implements Lcom/google/android/videos/player/PlaybackHelper$PlayerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/LocalPlaybackHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onDialogShown()V
.end method

.method public abstract onLegacyStreamSelected(Lcom/google/android/videos/streams/MediaStream;)V
.end method

.method public abstract onLocalPlaybackError()V
.end method
