.class public final Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;
.super Ljava/lang/Object;
.source "DynamicVideoCollectionsRepository.java"

# interfaces
.implements Lcom/google/android/repolib/observers/Updatable;
.implements Lcom/google/android/repolib/observers/UpdatablesChanged;
.implements Lcom/google/android/repolib/repositories/Repository;
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/observers/Updatable;",
        "Lcom/google/android/repolib/observers/UpdatablesChanged;",
        "Lcom/google/android/repolib/repositories/Repository",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;",
        ">;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/api/VideoCollectionListRequest;",
        "Lcom/google/wireless/android/video/magma/proto/VideoCollectionListResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final collectionListRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/VideoCollectionListRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoCollectionListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

.field private final databaseListener:Lcom/google/android/videos/store/FilteredDatabaseObservable;

.field private final localeObservable:Lcom/google/android/repolib/observers/Observable;

.field private final maxCollections:I

.field private final networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

.field private final repository:Lcom/google/android/repolib/repositories/ArrayRepository;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/repositories/ArrayRepository",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;",
            ">;"
        }
    .end annotation
.end field

.field private final signInManager:Lcom/google/android/videos/accounts/SignInManager;

.field private final storeStatusObservable:Lcom/google/android/videos/pano/repositories/StoreStatusObservable;

.field private final updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;


# direct methods
.method public constructor <init>(ILcom/google/android/videos/store/Database;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/repolib/observers/Observable;Lcom/google/android/videos/pano/repositories/StoreStatusObservable;)V
    .locals 4
    .param p1, "maxCollections"    # I
    .param p2, "database"    # Lcom/google/android/videos/store/Database;
    .param p3, "signInManager"    # Lcom/google/android/videos/accounts/SignInManager;
    .param p4, "configurationStore"    # Lcom/google/android/videos/store/ConfigurationStore;
    .param p6, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;
    .param p7, "localeObservable"    # Lcom/google/android/repolib/observers/Observable;
    .param p8, "storeStatusObservable"    # Lcom/google/android/videos/pano/repositories/StoreStatusObservable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/store/Database;",
            "Lcom/google/android/videos/accounts/SignInManager;",
            "Lcom/google/android/videos/store/ConfigurationStore;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/VideoCollectionListRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoCollectionListResponse;",
            ">;",
            "Lcom/google/android/videos/utils/NetworkStatus;",
            "Lcom/google/android/repolib/observers/Observable;",
            "Lcom/google/android/videos/pano/repositories/StoreStatusObservable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 55
    .local p5, "collectionListRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/VideoCollectionListRequest;Lcom/google/wireless/android/video/magma/proto/VideoCollectionListResponse;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p7, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->localeObservable:Lcom/google/android/repolib/observers/Observable;

    .line 57
    invoke-static {}, Lcom/google/android/repolib/repositories/ArrayRepository;->arrayRepository()Lcom/google/android/repolib/repositories/ArrayRepository;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    .line 58
    iput p1, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->maxCollections:I

    .line 59
    iput-object p3, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    .line 60
    iput-object p4, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    .line 61
    iput-object p5, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->collectionListRequester:Lcom/google/android/videos/async/Requester;

    .line 62
    iput-object p6, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 63
    iput-object p8, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->storeStatusObservable:Lcom/google/android/videos/pano/repositories/StoreStatusObservable;

    .line 64
    new-instance v0, Lcom/google/android/videos/store/FilteredDatabaseObservable;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const/4 v3, 0x6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {v0, p2, v1}, Lcom/google/android/videos/store/FilteredDatabaseObservable;-><init>(Lcom/google/android/videos/store/Database;[Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->databaseListener:Lcom/google/android/videos/store/FilteredDatabaseObservable;

    .line 67
    invoke-static {p0}, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->asyncUpdateDispatcher(Lcom/google/android/repolib/observers/UpdatablesChanged;)Lcom/google/android/repolib/observers/UpdateDispatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    .line 68
    return-void
.end method


# virtual methods
.method public addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 93
    return-void
.end method

.method public firstUpdatableAdded()V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/repolib/repositories/ArrayRepository;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 73
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/accounts/SignInManager;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 74
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->storeStatusObservable:Lcom/google/android/videos/pano/repositories/StoreStatusObservable;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/pano/repositories/StoreStatusObservable;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 75
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->databaseListener:Lcom/google/android/videos/store/FilteredDatabaseObservable;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/store/FilteredDatabaseObservable;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 76
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0, p0}, Lcom/google/android/videos/utils/NetworkStatus;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 77
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->localeObservable:Lcom/google/android/repolib/observers/Observable;

    invoke-interface {v0, p0}, Lcom/google/android/repolib/observers/Observable;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 78
    return-void
.end method

.method public indexer()Lcom/google/android/repolib/common/Indexer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/repolib/common/Indexer",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    invoke-virtual {v0}, Lcom/google/android/repolib/repositories/ArrayRepository;->indexer()Lcom/google/android/repolib/common/Indexer;

    move-result-object v0

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    invoke-virtual {v0}, Lcom/google/android/repolib/repositories/ArrayRepository;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public lastUpdatableRemoved()V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/repolib/repositories/ArrayRepository;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 83
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->localeObservable:Lcom/google/android/repolib/observers/Observable;

    invoke-interface {v0, p0}, Lcom/google/android/repolib/observers/Observable;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 84
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/accounts/SignInManager;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 85
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->storeStatusObservable:Lcom/google/android/videos/pano/repositories/StoreStatusObservable;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/pano/repositories/StoreStatusObservable;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 86
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->databaseListener:Lcom/google/android/videos/store/FilteredDatabaseObservable;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/store/FilteredDatabaseObservable;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 87
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0, p0}, Lcom/google/android/videos/utils/NetworkStatus;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 88
    return-void
.end method

.method public onError(Lcom/google/android/videos/api/VideoCollectionListRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/api/VideoCollectionListRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    invoke-virtual {v0}, Lcom/google/android/repolib/repositories/ArrayRepository;->clear()V

    .line 122
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 36
    check-cast p1, Lcom/google/android/videos/api/VideoCollectionListRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->onError(Lcom/google/android/videos/api/VideoCollectionListRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/api/VideoCollectionListRequest;Lcom/google/wireless/android/video/magma/proto/VideoCollectionListResponse;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/api/VideoCollectionListRequest;
    .param p2, "response"    # Lcom/google/wireless/android/video/magma/proto/VideoCollectionListResponse;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    iget-object v1, p2, Lcom/google/wireless/android/video/magma/proto/VideoCollectionListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/repolib/repositories/ArrayRepository;->setItems(Ljava/util/Collection;)V

    .line 117
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 36
    check-cast p1, Lcom/google/android/videos/api/VideoCollectionListRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/wireless/android/video/magma/proto/VideoCollectionListResponse;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->onResponse(Lcom/google/android/videos/api/VideoCollectionListRequest;Lcom/google/wireless/android/video/magma/proto/VideoCollectionListResponse;)V

    return-void
.end method

.method public removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 98
    return-void
.end method

.method public update()V
    .locals 6

    .prologue
    .line 102
    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    invoke-virtual {v1}, Lcom/google/android/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v0

    .line 103
    .local v0, "account":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 104
    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v1}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 105
    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->collectionListRequester:Lcom/google/android/videos/async/Requester;

    new-instance v2, Lcom/google/android/videos/api/VideoCollectionListRequest;

    iget-object v3, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    invoke-virtual {v3, v0}, Lcom/google/android/videos/store/ConfigurationStore;->getPlayCountry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    iget v5, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->maxCollections:I

    invoke-direct {v2, v0, v3, v4, v5}, Lcom/google/android/videos/api/VideoCollectionListRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;I)V

    invoke-interface {v1, v2, p0}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 111
    :cond_0
    :goto_0
    return-void

    .line 109
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/DynamicVideoCollectionsRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    invoke-virtual {v1}, Lcom/google/android/repolib/repositories/ArrayRepository;->clear()V

    goto :goto_0
.end method
