.class public Lcom/google/android/videos/remote/PlayerState;
.super Ljava/lang/Object;
.source "PlayerState.java"


# instance fields
.field public bufferedPercentage:I

.field public canRetry:Z

.field public errorMessage:Ljava/lang/String;

.field public state:I

.field public subtitleTrack:Lcom/google/android/videos/subtitles/SubtitleTrack;

.field public time:I

.field public videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/remote/PlayerState;)V
    .locals 8
    .param p1, "playerState"    # Lcom/google/android/videos/remote/PlayerState;

    .prologue
    .line 83
    iget-object v1, p1, Lcom/google/android/videos/remote/PlayerState;->videoId:Ljava/lang/String;

    iget v2, p1, Lcom/google/android/videos/remote/PlayerState;->state:I

    iget v3, p1, Lcom/google/android/videos/remote/PlayerState;->time:I

    iget v4, p1, Lcom/google/android/videos/remote/PlayerState;->bufferedPercentage:I

    iget-object v5, p1, Lcom/google/android/videos/remote/PlayerState;->subtitleTrack:Lcom/google/android/videos/subtitles/SubtitleTrack;

    iget-object v6, p1, Lcom/google/android/videos/remote/PlayerState;->errorMessage:Ljava/lang/String;

    iget-boolean v7, p1, Lcom/google/android/videos/remote/PlayerState;->canRetry:Z

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/remote/PlayerState;-><init>(Ljava/lang/String;IIILcom/google/android/videos/subtitles/SubtitleTrack;Ljava/lang/String;Z)V

    .line 90
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IIILcom/google/android/videos/subtitles/SubtitleTrack;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "state"    # I
    .param p3, "time"    # I
    .param p4, "bufferedPercentage"    # I
    .param p5, "subtitleTrack"    # Lcom/google/android/videos/subtitles/SubtitleTrack;
    .param p6, "errorMessage"    # Ljava/lang/String;
    .param p7, "canRetry"    # Z

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lcom/google/android/videos/remote/PlayerState;->videoId:Ljava/lang/String;

    .line 74
    iput p2, p0, Lcom/google/android/videos/remote/PlayerState;->state:I

    .line 75
    iput p3, p0, Lcom/google/android/videos/remote/PlayerState;->time:I

    .line 76
    iput p4, p0, Lcom/google/android/videos/remote/PlayerState;->bufferedPercentage:I

    .line 77
    iput-object p5, p0, Lcom/google/android/videos/remote/PlayerState;->subtitleTrack:Lcom/google/android/videos/subtitles/SubtitleTrack;

    .line 78
    iput-object p6, p0, Lcom/google/android/videos/remote/PlayerState;->errorMessage:Ljava/lang/String;

    .line 79
    iput-boolean p7, p0, Lcom/google/android/videos/remote/PlayerState;->canRetry:Z

    .line 80
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PlayerState [videoId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/remote/PlayerState;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/videos/remote/PlayerState;->state:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/videos/remote/PlayerState;->time:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", bufferedPercentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/videos/remote/PlayerState;->bufferedPercentage:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", subtitleTrack="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/remote/PlayerState;->subtitleTrack:Lcom/google/android/videos/subtitles/SubtitleTrack;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", errorMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/remote/PlayerState;->errorMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", canRetry="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/videos/remote/PlayerState;->canRetry:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
