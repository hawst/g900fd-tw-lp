.class public abstract Lcom/google/android/videos/ui/CursorHelper;
.super Lcom/google/android/videos/store/Database$BaseListener;
.source "CursorHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/CursorHelper$1;,
        Lcom/google/android/videos/ui/CursorHelper$WishlistShowsCursorHelper;,
        Lcom/google/android/videos/ui/CursorHelper$WishlistMoviesCursorHelper;,
        Lcom/google/android/videos/ui/CursorHelper$HomeWishlistCursorHelper;,
        Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;,
        Lcom/google/android/videos/ui/CursorHelper$ShowsCursorHelper;,
        Lcom/google/android/videos/ui/CursorHelper$WatchNowEpisodesCursorHelper;,
        Lcom/google/android/videos/ui/CursorHelper$PinnedTvCursorHelper;,
        Lcom/google/android/videos/ui/CursorHelper$HomeShowPurchaseCursorHelper;,
        Lcom/google/android/videos/ui/CursorHelper$NewMoviesCursorHelper;,
        Lcom/google/android/videos/ui/CursorHelper$MoviesPreorderCursorHelper;,
        Lcom/google/android/videos/ui/CursorHelper$MoviesLibraryCursorHelper;,
        Lcom/google/android/videos/ui/CursorHelper$HomeMoviePurchaseCursorHelper;,
        Lcom/google/android/videos/ui/CursorHelper$HomePurchaseCursorHelper;,
        Lcom/google/android/videos/ui/CursorHelper$HomeCursorHelper;,
        Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingMasterSubCursorWrapper;,
        Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;,
        Lcom/google/android/videos/ui/CursorHelper$ResultCallback;,
        Lcom/google/android/videos/ui/CursorHelper$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/videos/store/Database$BaseListener;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private final callback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<TS;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field protected final config:Lcom/google/android/videos/Config;

.field private currentRequest:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TS;"
        }
    .end annotation
.end field

.field private cursorWrapper:Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;

.field private final database:Lcom/google/android/videos/store/Database;

.field private downloadedOnly:Z

.field private final handler:Landroid/os/Handler;

.field private final listeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/videos/ui/CursorHelper$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private pendingCursorUpdate:Z

.field private final store:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<TS;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/videos/Config;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/async/Requester;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "config"    # Lcom/google/android/videos/Config;
    .param p3, "uiHandler"    # Landroid/os/Handler;
    .param p4, "database"    # Lcom/google/android/videos/store/Database;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/google/android/videos/Config;",
            "Landroid/os/Handler;",
            "Lcom/google/android/videos/store/Database;",
            "Lcom/google/android/videos/async/Requester",
            "<TS;",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 76
    .local p0, "this":Lcom/google/android/videos/ui/CursorHelper;, "Lcom/google/android/videos/ui/CursorHelper<TS;>;"
    .local p5, "store":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TS;Landroid/database/Cursor;>;"
    invoke-direct {p0}, Lcom/google/android/videos/store/Database$BaseListener;-><init>()V

    .line 77
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/Config;

    iput-object v0, p0, Lcom/google/android/videos/ui/CursorHelper;->config:Lcom/google/android/videos/Config;

    .line 78
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/videos/ui/CursorHelper;->handler:Landroid/os/Handler;

    .line 79
    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/ui/CursorHelper;->store:Lcom/google/android/videos/async/Requester;

    .line 80
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/Database;

    iput-object v0, p0, Lcom/google/android/videos/ui/CursorHelper;->database:Lcom/google/android/videos/store/Database;

    .line 82
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/ui/CursorHelper;->listeners:Ljava/util/Set;

    .line 83
    new-instance v0, Lcom/google/android/videos/ui/CursorHelper$ResultCallback;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/videos/ui/CursorHelper$ResultCallback;-><init>(Lcom/google/android/videos/ui/CursorHelper;Lcom/google/android/videos/ui/CursorHelper$1;)V

    invoke-static {p1, v0}, Lcom/google/android/videos/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/ActivityCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/CursorHelper;->callback:Lcom/google/android/videos/async/Callback;

    .line 84
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/videos/ui/CursorHelper;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/CursorHelper;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper;->currentRequest:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/videos/ui/CursorHelper;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/ui/CursorHelper;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/google/android/videos/ui/CursorHelper;->currentRequest:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/videos/ui/CursorHelper;Landroid/database/Cursor;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/ui/CursorHelper;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/CursorHelper;->changeCursor(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/videos/ui/CursorHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/CursorHelper;

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/google/android/videos/ui/CursorHelper;->pendingCursorUpdate:Z

    return v0
.end method

.method static synthetic access$302(Lcom/google/android/videos/ui/CursorHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/ui/CursorHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/google/android/videos/ui/CursorHelper;->pendingCursorUpdate:Z

    return p1
.end method

.method private changeCursor(Landroid/database/Cursor;)V
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 131
    .local p0, "this":Lcom/google/android/videos/ui/CursorHelper;, "Lcom/google/android/videos/ui/CursorHelper<TS;>;"
    iget-object v2, p0, Lcom/google/android/videos/ui/CursorHelper;->cursorWrapper:Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;

    if-eqz v2, :cond_0

    .line 132
    iget-object v2, p0, Lcom/google/android/videos/ui/CursorHelper;->cursorWrapper:Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;->close()V

    .line 134
    :cond_0
    invoke-static {p1}, Lcom/google/android/videos/ui/CursorHelper;->createReferenceCountingWrapper(Landroid/database/Cursor;)Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/ui/CursorHelper;->cursorWrapper:Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;

    .line 135
    iget-object v2, p0, Lcom/google/android/videos/ui/CursorHelper;->listeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/ui/CursorHelper$Listener;

    .line 136
    .local v1, "listener":Lcom/google/android/videos/ui/CursorHelper$Listener;
    invoke-interface {v1, p0}, Lcom/google/android/videos/ui/CursorHelper$Listener;->onCursorChanged(Lcom/google/android/videos/ui/CursorHelper;)V

    goto :goto_0

    .line 138
    .end local v1    # "listener":Lcom/google/android/videos/ui/CursorHelper$Listener;
    :cond_1
    return-void
.end method

.method private static createReferenceCountingWrapper(Landroid/database/Cursor;)Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;
    .locals 1
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 225
    if-nez p0, :cond_0

    .line 226
    const/4 v0, 0x0

    .line 231
    :goto_0
    return-object v0

    .line 228
    :cond_0
    instance-of v0, p0, Lcom/google/android/videos/store/MasterSubCursor;

    if-eqz v0, :cond_1

    .line 229
    new-instance v0, Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingMasterSubCursorWrapper;

    check-cast p0, Lcom/google/android/videos/store/MasterSubCursor;

    .end local p0    # "cursor":Landroid/database/Cursor;
    invoke-direct {v0, p0}, Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingMasterSubCursorWrapper;-><init>(Lcom/google/android/videos/store/MasterSubCursor;)V

    goto :goto_0

    .line 231
    .restart local p0    # "cursor":Landroid/database/Cursor;
    :cond_1
    new-instance v0, Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;

    invoke-direct {v0, p0}, Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;-><init>(Landroid/database/Cursor;)V

    goto :goto_0
.end method


# virtual methods
.method public final addListener(Lcom/google/android/videos/ui/CursorHelper$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/videos/ui/CursorHelper$Listener;

    .prologue
    .line 87
    .local p0, "this":Lcom/google/android/videos/ui/CursorHelper;, "Lcom/google/android/videos/ui/CursorHelper<TS;>;"
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper;->listeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 88
    return-void
.end method

.method protected abstract createCursorRequest(Ljava/lang/String;Z)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)TS;"
        }
    .end annotation
.end method

.method public final getCursor()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 181
    .local p0, "this":Lcom/google/android/videos/ui/CursorHelper;, "Lcom/google/android/videos/ui/CursorHelper<TS;>;"
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper;->account:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 182
    const/4 v0, 0x0

    .line 187
    :goto_0
    return-object v0

    .line 184
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper;->cursorWrapper:Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;

    if-eqz v0, :cond_1

    .line 185
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper;->cursorWrapper:Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;->increment()V

    .line 187
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper;->cursorWrapper:Lcom/google/android/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;

    goto :goto_0
.end method

.method protected final maybeScheduleUpdate(Ljava/lang/String;)V
    .locals 1
    .param p1, "accountWithChanges"    # Ljava/lang/String;

    .prologue
    .line 158
    .local p0, "this":Lcom/google/android/videos/ui/CursorHelper;, "Lcom/google/android/videos/ui/CursorHelper<TS;>;"
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper;->account:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    invoke-virtual {p0}, Lcom/google/android/videos/ui/CursorHelper;->scheduleUpdate()V

    .line 161
    :cond_0
    return-void
.end method

.method protected final onAccount(Ljava/lang/String;)V
    .locals 2
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .local p0, "this":Lcom/google/android/videos/ui/CursorHelper;, "Lcom/google/android/videos/ui/CursorHelper<TS;>;"
    const/4 v1, 0x0

    .line 121
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper;->account:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    :goto_0
    return-void

    .line 124
    :cond_0
    iput-object p1, p0, Lcom/google/android/videos/ui/CursorHelper;->account:Ljava/lang/String;

    .line 125
    iput-object v1, p0, Lcom/google/android/videos/ui/CursorHelper;->currentRequest:Ljava/lang/Object;

    .line 126
    invoke-direct {p0, v1}, Lcom/google/android/videos/ui/CursorHelper;->changeCursor(Landroid/database/Cursor;)V

    .line 127
    invoke-virtual {p0}, Lcom/google/android/videos/ui/CursorHelper;->updateCursor()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 107
    .local p0, "this":Lcom/google/android/videos/ui/CursorHelper;, "Lcom/google/android/videos/ui/CursorHelper<TS;>;"
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/videos/ui/CursorHelper;->changeCursor(Landroid/database/Cursor;)V

    .line 108
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 95
    .local p0, "this":Lcom/google/android/videos/ui/CursorHelper;, "Lcom/google/android/videos/ui/CursorHelper<TS;>;"
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/store/Database;->addListener(Lcom/google/android/videos/store/Database$Listener;)V

    .line 96
    invoke-virtual {p0}, Lcom/google/android/videos/ui/CursorHelper;->updateCursor()V

    .line 97
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 100
    .local p0, "this":Lcom/google/android/videos/ui/CursorHelper;, "Lcom/google/android/videos/ui/CursorHelper<TS;>;"
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/ui/CursorHelper;->currentRequest:Ljava/lang/Object;

    .line 101
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/ui/CursorHelper;->pendingCursorUpdate:Z

    .line 102
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper;->handler:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 103
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/store/Database;->removeListener(Lcom/google/android/videos/store/Database$Listener;)Z

    .line 104
    return-void
.end method

.method public final removeListener(Lcom/google/android/videos/ui/CursorHelper$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/videos/ui/CursorHelper$Listener;

    .prologue
    .line 91
    .local p0, "this":Lcom/google/android/videos/ui/CursorHelper;, "Lcom/google/android/videos/ui/CursorHelper<TS;>;"
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper;->listeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 92
    return-void
.end method

.method public run()V
    .locals 0

    .prologue
    .line 169
    .local p0, "this":Lcom/google/android/videos/ui/CursorHelper;, "Lcom/google/android/videos/ui/CursorHelper<TS;>;"
    invoke-virtual {p0}, Lcom/google/android/videos/ui/CursorHelper;->updateCursor()V

    .line 170
    return-void
.end method

.method protected final scheduleUpdate()V
    .locals 4

    .prologue
    .line 164
    .local p0, "this":Lcom/google/android/videos/ui/CursorHelper;, "Lcom/google/android/videos/ui/CursorHelper<TS;>;"
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper;->handler:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 165
    return-void
.end method

.method public setDownloadedOnly(Z)V
    .locals 2
    .param p1, "downloadedOnly"    # Z

    .prologue
    .local p0, "this":Lcom/google/android/videos/ui/CursorHelper;, "Lcom/google/android/videos/ui/CursorHelper<TS;>;"
    const/4 v1, 0x0

    .line 111
    iget-boolean v0, p0, Lcom/google/android/videos/ui/CursorHelper;->downloadedOnly:Z

    if-ne v0, p1, :cond_0

    .line 118
    :goto_0
    return-void

    .line 114
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/videos/ui/CursorHelper;->downloadedOnly:Z

    .line 115
    iput-object v1, p0, Lcom/google/android/videos/ui/CursorHelper;->currentRequest:Ljava/lang/Object;

    .line 116
    invoke-direct {p0, v1}, Lcom/google/android/videos/ui/CursorHelper;->changeCursor(Landroid/database/Cursor;)V

    .line 117
    invoke-virtual {p0}, Lcom/google/android/videos/ui/CursorHelper;->updateCursor()V

    goto :goto_0
.end method

.method protected final updateCursor()V
    .locals 3

    .prologue
    .line 141
    .local p0, "this":Lcom/google/android/videos/ui/CursorHelper;, "Lcom/google/android/videos/ui/CursorHelper<TS;>;"
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper;->handler:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 142
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper;->account:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 155
    :cond_0
    :goto_0
    return-void

    .line 145
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper;->currentRequest:Ljava/lang/Object;

    if-eqz v0, :cond_2

    .line 148
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/ui/CursorHelper;->pendingCursorUpdate:Z

    goto :goto_0

    .line 150
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper;->account:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/videos/ui/CursorHelper;->downloadedOnly:Z

    invoke-virtual {p0, v0, v1}, Lcom/google/android/videos/ui/CursorHelper;->createCursorRequest(Ljava/lang/String;Z)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/CursorHelper;->currentRequest:Ljava/lang/Object;

    .line 151
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper;->currentRequest:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper;->store:Lcom/google/android/videos/async/Requester;

    iget-object v1, p0, Lcom/google/android/videos/ui/CursorHelper;->currentRequest:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/videos/ui/CursorHelper;->callback:Lcom/google/android/videos/async/Callback;

    invoke-interface {v0, v1, v2}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    goto :goto_0
.end method
