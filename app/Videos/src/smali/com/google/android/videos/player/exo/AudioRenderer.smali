.class public Lcom/google/android/videos/player/exo/AudioRenderer;
.super Lcom/google/android/exoplayer/MediaCodecAudioTrackRenderer;
.source "AudioRenderer.java"


# instance fields
.field private enableVirtualizer:Z

.field private virtualizer:Landroid/media/audiofx/Virtualizer;


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/SampleSource;Lcom/google/android/exoplayer/drm/DrmSessionManager;ZFLandroid/os/Handler;Lcom/google/android/exoplayer/MediaCodecAudioTrackRenderer$EventListener;)V
    .locals 0
    .param p1, "source"    # Lcom/google/android/exoplayer/SampleSource;
    .param p2, "drmSessionManager"    # Lcom/google/android/exoplayer/drm/DrmSessionManager;
    .param p3, "playClearSamplesWithoutKeys"    # Z
    .param p4, "minBufferMultiplicationFactor"    # F
    .param p5, "eventHandler"    # Landroid/os/Handler;
    .param p6, "eventListener"    # Lcom/google/android/exoplayer/MediaCodecAudioTrackRenderer$EventListener;

    .prologue
    .line 30
    invoke-direct/range {p0 .. p6}, Lcom/google/android/exoplayer/MediaCodecAudioTrackRenderer;-><init>(Lcom/google/android/exoplayer/SampleSource;Lcom/google/android/exoplayer/drm/DrmSessionManager;ZFLandroid/os/Handler;Lcom/google/android/exoplayer/MediaCodecAudioTrackRenderer$EventListener;)V

    .line 32
    return-void
.end method


# virtual methods
.method public handleMessage(ILjava/lang/Object;)V
    .locals 2
    .param p1, "what"    # I
    .param p2, "message"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer/ExoPlaybackException;
        }
    .end annotation

    .prologue
    .line 53
    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    .line 54
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "message":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 55
    .local v0, "enableVirtualizer":Z
    iget-boolean v1, p0, Lcom/google/android/videos/player/exo/AudioRenderer;->enableVirtualizer:Z

    if-eq v1, v0, :cond_0

    .line 56
    iput-boolean v0, p0, Lcom/google/android/videos/player/exo/AudioRenderer;->enableVirtualizer:Z

    .line 57
    iget-object v1, p0, Lcom/google/android/videos/player/exo/AudioRenderer;->virtualizer:Landroid/media/audiofx/Virtualizer;

    if-eqz v1, :cond_0

    .line 58
    iget-object v1, p0, Lcom/google/android/videos/player/exo/AudioRenderer;->virtualizer:Landroid/media/audiofx/Virtualizer;

    invoke-virtual {v1, v0}, Landroid/media/audiofx/Virtualizer;->setEnabled(Z)I

    .line 64
    .end local v0    # "enableVirtualizer":Z
    :cond_0
    :goto_0
    return-void

    .line 62
    .restart local p2    # "message":Ljava/lang/Object;
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/google/android/exoplayer/MediaCodecAudioTrackRenderer;->handleMessage(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method protected onAudioSessionId(I)V
    .locals 2
    .param p1, "audioSessionId"    # I

    .prologue
    .line 36
    invoke-static {p1}, Lcom/google/android/videos/player/PlayerUtil;->createVirtualizerIfAvailableV18(I)Landroid/media/audiofx/Virtualizer;

    move-result-object v0

    .line 37
    .local v0, "virtualizer":Landroid/media/audiofx/Virtualizer;
    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/google/android/videos/player/exo/AudioRenderer;->enableVirtualizer:Z

    if-eqz v1, :cond_0

    .line 38
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/audiofx/Virtualizer;->setEnabled(Z)I

    .line 40
    :cond_0
    return-void
.end method

.method protected onDisabled()V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/videos/player/exo/AudioRenderer;->virtualizer:Landroid/media/audiofx/Virtualizer;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/google/android/videos/player/exo/AudioRenderer;->virtualizer:Landroid/media/audiofx/Virtualizer;

    invoke-virtual {v0}, Landroid/media/audiofx/Virtualizer;->release()V

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/player/exo/AudioRenderer;->virtualizer:Landroid/media/audiofx/Virtualizer;

    .line 48
    :cond_0
    invoke-super {p0}, Lcom/google/android/exoplayer/MediaCodecAudioTrackRenderer;->onDisabled()V

    .line 49
    return-void
.end method
