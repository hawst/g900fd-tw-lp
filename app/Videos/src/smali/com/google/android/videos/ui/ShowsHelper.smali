.class public Lcom/google/android/videos/ui/ShowsHelper;
.super Lcom/google/android/videos/store/Database$BaseListener;
.source "ShowsHelper.java"

# interfaces
.implements Lcom/google/android/repolib/observers/Updatable;
.implements Lcom/google/android/videos/activity/HomeFragment$HomeFragmentHelper;
.implements Lcom/google/android/videos/ui/CursorHelper$Listener;
.implements Lcom/google/android/videos/ui/SyncHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/ShowsHelper$1;,
        Lcom/google/android/videos/ui/ShowsHelper$ShowClickListener;
    }
.end annotation


# instance fields
.field private final activity:Lcom/google/android/videos/activity/HomeActivity;

.field private final database:Lcom/google/android/videos/store/Database;

.field private final listView:Landroid/support/v7/widget/RecyclerView;

.field private final networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

.field private pendingBitmapsUpdated:Z

.field private pendingCursor:Z

.field private final progressBar:Landroid/view/View;

.field private final showsCursorHelper:Lcom/google/android/videos/ui/CursorHelper$ShowsCursorHelper;

.field private final showsDataSource:Lcom/google/android/videos/adapter/ShowsDataSource;

.field private final showsFlowHelper:Lcom/google/android/videos/ui/ShowsFlowHelper;

.field private final syncHelper:Lcom/google/android/videos/ui/SyncHelper;

.field private final uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

.field private final welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/activity/HomeActivity;Landroid/view/View;Lcom/google/android/videos/ui/CursorHelper$ShowsCursorHelper;Lcom/google/android/videos/Config;Lcom/google/android/videos/utils/DownloadedOnlyManager;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PosterStore;Lcom/google/android/videos/utils/ErrorHelper;Lcom/google/android/videos/ui/SyncHelper;Lcom/google/android/videos/bitmap/BitmapRequesters;Lcom/google/android/videos/api/ApiRequesters;Lcom/google/android/videos/logging/UiElementNode;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/videos/logging/UiEventLoggingHelper;)V
    .locals 11
    .param p1, "activity"    # Lcom/google/android/videos/activity/HomeActivity;
    .param p2, "mainView"    # Landroid/view/View;
    .param p3, "showsCursorHelper"    # Lcom/google/android/videos/ui/CursorHelper$ShowsCursorHelper;
    .param p4, "config"    # Lcom/google/android/videos/Config;
    .param p5, "downloadedOnlyManager"    # Lcom/google/android/videos/utils/DownloadedOnlyManager;
    .param p6, "database"    # Lcom/google/android/videos/store/Database;
    .param p7, "posterStore"    # Lcom/google/android/videos/store/PosterStore;
    .param p8, "errorHelper"    # Lcom/google/android/videos/utils/ErrorHelper;
    .param p9, "syncHelper"    # Lcom/google/android/videos/ui/SyncHelper;
    .param p10, "bitmapRequesters"    # Lcom/google/android/videos/bitmap/BitmapRequesters;
    .param p11, "apiRequesters"    # Lcom/google/android/videos/api/ApiRequesters;
    .param p12, "parentNode"    # Lcom/google/android/videos/logging/UiElementNode;
    .param p13, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;
    .param p14, "uiEventLoggingHelper"    # Lcom/google/android/videos/logging/UiEventLoggingHelper;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/google/android/videos/store/Database$BaseListener;-><init>()V

    .line 77
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/activity/HomeActivity;

    iput-object v2, p0, Lcom/google/android/videos/ui/ShowsHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    .line 78
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->database:Lcom/google/android/videos/store/Database;

    .line 79
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/ui/CursorHelper$ShowsCursorHelper;

    iput-object v2, p0, Lcom/google/android/videos/ui/ShowsHelper;->showsCursorHelper:Lcom/google/android/videos/ui/CursorHelper$ShowsCursorHelper;

    .line 81
    invoke-static/range {p7 .. p7}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    invoke-static/range {p8 .. p8}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    invoke-static/range {p10 .. p10}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    invoke-static/range {p11 .. p11}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    invoke-static/range {p9 .. p9}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/ui/SyncHelper;

    iput-object v2, p0, Lcom/google/android/videos/ui/ShowsHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    .line 86
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 87
    invoke-static/range {p14 .. p14}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/logging/UiEventLoggingHelper;

    iput-object v2, p0, Lcom/google/android/videos/ui/ShowsHelper;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    .line 89
    const v2, 0x7f0f0029

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/RecyclerView;

    iput-object v2, p0, Lcom/google/android/videos/ui/ShowsHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    .line 90
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowsHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    new-instance v3, Lcom/google/android/videos/ui/ShuffleAddItemAnimator;

    invoke-direct {v3}, Lcom/google/android/videos/ui/ShuffleAddItemAnimator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(Landroid/support/v7/widget/RecyclerView$ItemAnimator;)V

    .line 91
    const v2, 0x7f0f0120

    invoke-virtual {p1, v2}, Lcom/google/android/videos/activity/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/ui/ShowsHelper;->progressBar:Landroid/view/View;

    .line 93
    new-instance v2, Lcom/google/android/videos/adapter/ShowsDataSource;

    invoke-direct {v2}, Lcom/google/android/videos/adapter/ShowsDataSource;-><init>()V

    iput-object v2, p0, Lcom/google/android/videos/ui/ShowsHelper;->showsDataSource:Lcom/google/android/videos/adapter/ShowsDataSource;

    .line 95
    new-instance v7, Lcom/google/android/videos/ui/ShowsHelper$ShowClickListener;

    const/4 v2, 0x0

    invoke-direct {v7, p0, v2}, Lcom/google/android/videos/ui/ShowsHelper$ShowClickListener;-><init>(Lcom/google/android/videos/ui/ShowsHelper;Lcom/google/android/videos/ui/ShowsHelper$1;)V

    .line 96
    .local v7, "showClickListener":Lcom/google/android/videos/ui/ShowsHelper$ShowClickListener;
    new-instance v8, Lcom/google/android/videos/ui/playnext/ShowClusterItemView$Binder;

    const/4 v2, 0x1

    move-object/from16 v0, p7

    invoke-virtual {v0, v2}, Lcom/google/android/videos/store/PosterStore;->getRequester(I)Lcom/google/android/videos/async/Requester;

    move-result-object v2

    new-instance v3, Lcom/google/android/videos/logging/GenericUiElementNode;

    const/16 v4, 0x1d

    move-object/from16 v0, p12

    move-object/from16 v1, p14

    invoke-direct {v3, v4, v0, v1}, Lcom/google/android/videos/logging/GenericUiElementNode;-><init>(ILcom/google/android/videos/logging/UiElementNode;Lcom/google/android/videos/logging/UiEventLoggingHelper;)V

    invoke-direct {v8, v2, v3}, Lcom/google/android/videos/ui/playnext/ShowClusterItemView$Binder;-><init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/logging/UiElementNode;)V

    .line 100
    .local v8, "showBinder":Lcom/google/android/videos/ui/playnext/ShowClusterItemView$Binder;
    new-instance v2, Lcom/google/android/videos/welcome/WelcomeFlow;

    invoke-interface/range {p10 .. p10}, Lcom/google/android/videos/bitmap/BitmapRequesters;->getBitmapRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    new-array v5, v5, [Lcom/google/android/videos/welcome/Welcome;

    const/4 v6, 0x0

    new-instance v10, Lcom/google/android/videos/welcome/DefaultShowsWelcome;

    invoke-direct {v10, p1, p4}, Lcom/google/android/videos/welcome/DefaultShowsWelcome;-><init>(Landroid/app/Activity;Lcom/google/android/videos/Config;)V

    aput-object v10, v5, v6

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/videos/welcome/WelcomeFlow;-><init>(Lcom/google/android/videos/async/Requester;I[Lcom/google/android/videos/welcome/Welcome;)V

    iput-object v2, p0, Lcom/google/android/videos/ui/ShowsHelper;->welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;

    .line 104
    new-instance v2, Lcom/google/android/videos/ui/ShowsFlowHelper;

    iget-object v5, p0, Lcom/google/android/videos/ui/ShowsHelper;->welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;

    iget-object v6, p0, Lcom/google/android/videos/ui/ShowsHelper;->showsDataSource:Lcom/google/android/videos/adapter/ShowsDataSource;

    move-object v3, p1

    move-object/from16 v4, p5

    invoke-direct/range {v2 .. v8}, Lcom/google/android/videos/ui/ShowsFlowHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/utils/DownloadedOnlyManager;Lcom/google/android/videos/welcome/WelcomeFlow;Lcom/google/android/videos/adapter/ShowsDataSource;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;Lcom/google/android/videos/ui/playnext/ShowClusterItemView$Binder;)V

    iput-object v2, p0, Lcom/google/android/videos/ui/ShowsHelper;->showsFlowHelper:Lcom/google/android/videos/ui/ShowsFlowHelper;

    .line 111
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowsHelper;->showsFlowHelper:Lcom/google/android/videos/ui/ShowsFlowHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/ShowsFlowHelper;->getFlow()Lcom/google/android/videos/flow/Flow;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/videos/flow/Flow;->setVisible(Z)V

    .line 113
    new-instance v9, Lcom/google/android/videos/flow/FlowAdapter;

    iget-object v2, p0, Lcom/google/android/videos/ui/ShowsHelper;->showsFlowHelper:Lcom/google/android/videos/ui/ShowsFlowHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/ShowsFlowHelper;->getFlow()Lcom/google/android/videos/flow/Flow;

    move-result-object v2

    invoke-direct {v9, v2}, Lcom/google/android/videos/flow/FlowAdapter;-><init>(Lcom/google/android/videos/flow/Flow;)V

    .line 114
    .local v9, "flowAdapter":Lcom/google/android/videos/flow/FlowAdapter;
    const/4 v2, 0x1

    invoke-virtual {v9, v2}, Lcom/google/android/videos/flow/FlowAdapter;->setHasStableIds(Z)V

    .line 115
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowsHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    new-instance v3, Lcom/google/android/videos/ui/DebugFlowLayoutManager;

    const-string v4, "ShowsFragment"

    invoke-direct {v3, v4}, Lcom/google/android/videos/ui/DebugFlowLayoutManager;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 116
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowsHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v9}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 117
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/videos/ui/ShowsHelper;)Lcom/google/android/videos/logging/UiEventLoggingHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/ShowsHelper;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/ui/ShowsHelper;)Lcom/google/android/videos/activity/HomeActivity;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/ShowsHelper;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/ui/ShowsHelper;)Lcom/google/android/videos/ui/SyncHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/ShowsHelper;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    return-object v0
.end method

.method private allowCursorChanges()Z
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/HomeActivity;->isTransitioning()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->isShown()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onBitmapsUpdated()V
    .locals 1

    .prologue
    .line 180
    invoke-direct {p0}, Lcom/google/android/videos/ui/ShowsHelper;->allowCursorChanges()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->pendingBitmapsUpdated:Z

    .line 182
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->showsDataSource:Lcom/google/android/videos/adapter/ShowsDataSource;

    invoke-virtual {v0}, Lcom/google/android/videos/adapter/ShowsDataSource;->notifyChanged()V

    .line 186
    :goto_0
    return-void

    .line 184
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->pendingBitmapsUpdated:Z

    goto :goto_0
.end method

.method private refreshShowsCursor()V
    .locals 2

    .prologue
    .line 161
    invoke-direct {p0}, Lcom/google/android/videos/ui/ShowsHelper;->allowCursorChanges()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->pendingCursor:Z

    .line 163
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->showsDataSource:Lcom/google/android/videos/adapter/ShowsDataSource;

    iget-object v1, p0, Lcom/google/android/videos/ui/ShowsHelper;->showsCursorHelper:Lcom/google/android/videos/ui/CursorHelper$ShowsCursorHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/ui/CursorHelper$ShowsCursorHelper;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/ShowsDataSource;->changeCursor(Landroid/database/Cursor;)V

    .line 164
    invoke-direct {p0}, Lcom/google/android/videos/ui/ShowsHelper;->updateVisibilities()V

    .line 168
    :goto_0
    return-void

    .line 166
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->pendingCursor:Z

    goto :goto_0
.end method

.method private updateVisibilities()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 193
    iget-object v1, p0, Lcom/google/android/videos/ui/ShowsHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/ui/SyncHelper;->getState()I

    move-result v0

    .line 194
    .local v0, "syncState":I
    packed-switch v0, :pswitch_data_0

    .line 224
    :goto_0
    return-void

    .line 198
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/videos/ui/ShowsHelper;->progressBar:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 199
    iget-object v1, p0, Lcom/google/android/videos/ui/ShowsHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    goto :goto_0

    .line 208
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/videos/ui/ShowsHelper;->showsDataSource:Lcom/google/android/videos/adapter/ShowsDataSource;

    invoke-virtual {v1}, Lcom/google/android/videos/adapter/ShowsDataSource;->hasCursor()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 209
    iget-object v1, p0, Lcom/google/android/videos/ui/ShowsHelper;->progressBar:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 210
    iget-object v1, p0, Lcom/google/android/videos/ui/ShowsHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    invoke-virtual {v1}, Lcom/google/android/videos/activity/HomeActivity;->isTransitioning()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 211
    iget-object v1, p0, Lcom/google/android/videos/ui/ShowsHelper;->showsFlowHelper:Lcom/google/android/videos/ui/ShowsFlowHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/ui/ShowsFlowHelper;->getFlow()Lcom/google/android/videos/flow/Flow;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/videos/flow/Flow;->setVisible(Z)V

    .line 212
    iget-object v1, p0, Lcom/google/android/videos/ui/ShowsHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    invoke-virtual {v1, p0}, Lcom/google/android/videos/activity/HomeActivity;->markAsReadyForTransitionV21(Ljava/lang/Object;)V

    .line 216
    :goto_1
    iget-object v1, p0, Lcom/google/android/videos/ui/ShowsHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v3}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    goto :goto_0

    .line 214
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/ui/ShowsHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lcom/google/android/videos/ui/ShowsHelper;->showsFlowHelper:Lcom/google/android/videos/ui/ShowsFlowHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/ShowsFlowHelper;->getFlow()Lcom/google/android/videos/flow/Flow;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/videos/ui/FlowAnimationUtil;->animateFlowAppearing(Landroid/support/v7/widget/RecyclerView;Lcom/google/android/videos/flow/Flow;)V

    goto :goto_1

    .line 218
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/ui/ShowsHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 219
    iget-object v1, p0, Lcom/google/android/videos/ui/ShowsHelper;->progressBar:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 194
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public onCursorChanged(Lcom/google/android/videos/ui/CursorHelper;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/ui/CursorHelper",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 155
    .local p1, "source":Lcom/google/android/videos/ui/CursorHelper;, "Lcom/google/android/videos/ui/CursorHelper<*>;"
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->showsCursorHelper:Lcom/google/android/videos/ui/CursorHelper$ShowsCursorHelper;

    if-ne p1, v0, :cond_0

    .line 156
    invoke-direct {p0}, Lcom/google/android/videos/ui/ShowsHelper;->refreshShowsCursor()V

    .line 158
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->showsCursorHelper:Lcom/google/android/videos/ui/CursorHelper$ShowsCursorHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/CursorHelper$ShowsCursorHelper;->onDestroy()V

    .line 151
    return-void
.end method

.method public onShowPosterUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 272
    invoke-direct {p0}, Lcom/google/android/videos/ui/ShowsHelper;->onBitmapsUpdated()V

    .line 273
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/HomeActivity;->isTransitioning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/activity/HomeActivity;->markAsPreparingForTransitionV21(Ljava/lang/Object;)V

    .line 123
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(Landroid/support/v7/widget/RecyclerView$ItemAnimator;)V

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/SyncHelper;->addListener(Lcom/google/android/videos/ui/SyncHelper$Listener;)V

    .line 126
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->showsFlowHelper:Lcom/google/android/videos/ui/ShowsFlowHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/ShowsFlowHelper;->onStart()V

    .line 127
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/store/Database;->addListener(Lcom/google/android/videos/store/Database$Listener;)V

    .line 128
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->showsCursorHelper:Lcom/google/android/videos/ui/CursorHelper$ShowsCursorHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/CursorHelper$ShowsCursorHelper;->addListener(Lcom/google/android/videos/ui/CursorHelper$Listener;)V

    .line 129
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->showsCursorHelper:Lcom/google/android/videos/ui/CursorHelper$ShowsCursorHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/CursorHelper$ShowsCursorHelper;->onStart()V

    .line 130
    invoke-direct {p0}, Lcom/google/android/videos/ui/ShowsHelper;->refreshShowsCursor()V

    .line 132
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/SyncHelper;->getState()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/ShowsHelper;->onSyncStateChanged(I)V

    .line 133
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0, p0}, Lcom/google/android/videos/utils/NetworkStatus;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 134
    invoke-virtual {p0}, Lcom/google/android/videos/ui/ShowsHelper;->update()V

    .line 135
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/SyncHelper;->removeListener(Lcom/google/android/videos/ui/SyncHelper$Listener;)V

    .line 140
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->showsFlowHelper:Lcom/google/android/videos/ui/ShowsFlowHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/ShowsFlowHelper;->onStop()V

    .line 141
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->showsDataSource:Lcom/google/android/videos/adapter/ShowsDataSource;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/ShowsDataSource;->changeCursor(Landroid/database/Cursor;)V

    .line 142
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/store/Database;->removeListener(Lcom/google/android/videos/store/Database$Listener;)Z

    .line 143
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->showsCursorHelper:Lcom/google/android/videos/ui/CursorHelper$ShowsCursorHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/CursorHelper$ShowsCursorHelper;->removeListener(Lcom/google/android/videos/ui/CursorHelper$Listener;)V

    .line 144
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->showsCursorHelper:Lcom/google/android/videos/ui/CursorHelper$ShowsCursorHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/CursorHelper$ShowsCursorHelper;->onStop()V

    .line 145
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0, p0}, Lcom/google/android/videos/utils/NetworkStatus;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 146
    return-void
.end method

.method public onSyncStateChanged(I)V
    .locals 2
    .param p1, "state"    # I

    .prologue
    .line 172
    iget-object v1, p0, Lcom/google/android/videos/ui/ShowsHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v0

    .line 173
    .local v0, "account":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 174
    iget-object v1, p0, Lcom/google/android/videos/ui/ShowsHelper;->welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/welcome/WelcomeFlow;->setAccount(Ljava/lang/String;)V

    .line 176
    :cond_0
    invoke-direct {p0}, Lcom/google/android/videos/ui/ShowsHelper;->updateVisibilities()V

    .line 177
    return-void
.end method

.method public onTransitioningChanged(Z)V
    .locals 2
    .param p1, "isTransitioning"    # Z

    .prologue
    .line 235
    if-nez p1, :cond_1

    .line 236
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lcom/google/android/videos/ui/ShuffleAddItemAnimator;

    invoke-direct {v1}, Lcom/google/android/videos/ui/ShuffleAddItemAnimator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(Landroid/support/v7/widget/RecyclerView$ItemAnimator;)V

    .line 237
    iget-boolean v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->pendingCursor:Z

    if-eqz v0, :cond_0

    .line 238
    invoke-direct {p0}, Lcom/google/android/videos/ui/ShowsHelper;->refreshShowsCursor()V

    .line 240
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/videos/ui/ShowsHelper;->pendingBitmapsUpdated:Z

    if-eqz v0, :cond_1

    .line 241
    invoke-direct {p0}, Lcom/google/android/videos/ui/ShowsHelper;->onBitmapsUpdated()V

    .line 244
    :cond_1
    return-void
.end method

.method public update()V
    .locals 3

    .prologue
    .line 228
    iget-object v1, p0, Lcom/google/android/videos/ui/ShowsHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v1}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v0

    .line 229
    .local v0, "isConnected":Z
    iget-object v1, p0, Lcom/google/android/videos/ui/ShowsHelper;->showsDataSource:Lcom/google/android/videos/adapter/ShowsDataSource;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/adapter/ShowsDataSource;->setNetworkConnected(Z)V

    .line 230
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowsHelper;->welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Lcom/google/android/videos/welcome/WelcomeFlow;->setDimmed(Z)V

    .line 231
    return-void

    .line 230
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
