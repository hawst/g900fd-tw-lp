.class public final Lcom/google/android/videos/utils/XmlParser;
.super Ljava/lang/Object;
.source "XmlParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/utils/XmlParser$1;,
        Lcom/google/android/videos/utils/XmlParser$EmptyAttributes;,
        Lcom/google/android/videos/utils/XmlParser$InternalHandler;,
        Lcom/google/android/videos/utils/XmlParser$Rule;
    }
.end annotation


# static fields
.field private static final EMPTY_ATTRS:Lorg/xml/sax/Attributes;

.field private static final PREFIXES_ONLY_FEATURES:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final factory:Ljavax/xml/parsers/SAXParserFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 51
    new-instance v1, Lcom/google/android/videos/utils/XmlParser$EmptyAttributes;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/videos/utils/XmlParser$EmptyAttributes;-><init>(Lcom/google/android/videos/utils/XmlParser$1;)V

    sput-object v1, Lcom/google/android/videos/utils/XmlParser;->EMPTY_ATTRS:Lorg/xml/sax/Attributes;

    .line 90
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 91
    .local v0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    const-string v1, "http://xml.org/sax/features/namespaces"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    const-string v1, "http://xml.org/sax/features/namespace-prefixes"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    sput-object v1, Lcom/google/android/videos/utils/XmlParser;->PREFIXES_ONLY_FEATURES:Ljava/util/Map;

    .line 94
    return-void
.end method

.method private constructor <init>(Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 120
    .local p1, "features":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Boolean;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/utils/XmlParser;->factory:Ljavax/xml/parsers/SAXParserFactory;

    .line 124
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 125
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;"
    iget-object v5, p0, Lcom/google/android/videos/utils/XmlParser;->factory:Ljavax/xml/parsers/SAXParserFactory;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v5, v3, v4}, Ljavax/xml/parsers/SAXParserFactory;->setFeature(Ljava/lang/String;Z)V
    :try_end_0
    .catch Lorg/xml/sax/SAXNotRecognizedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xml/sax/SAXNotSupportedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 127
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v0

    .line 129
    .local v0, "e":Lorg/xml/sax/SAXNotRecognizedException;
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "SAX initilization error"

    invoke-direct {v3, v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 130
    .end local v0    # "e":Lorg/xml/sax/SAXNotRecognizedException;
    :catch_1
    move-exception v0

    .line 131
    .local v0, "e":Lorg/xml/sax/SAXNotSupportedException;
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "SAX initilization error"

    invoke-direct {v3, v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 132
    .end local v0    # "e":Lorg/xml/sax/SAXNotSupportedException;
    :catch_2
    move-exception v0

    .line 133
    .local v0, "e":Ljavax/xml/parsers/ParserConfigurationException;
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "SAX initilization error"

    invoke-direct {v3, v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 135
    .end local v0    # "e":Ljavax/xml/parsers/ParserConfigurationException;
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_0
    return-void
.end method

.method static synthetic access$100()Lorg/xml/sax/Attributes;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/google/android/videos/utils/XmlParser;->EMPTY_ATTRS:Lorg/xml/sax/Attributes;

    return-object v0
.end method

.method public static createPrefixesOnlyParser()Lcom/google/android/videos/utils/XmlParser;
    .locals 2

    .prologue
    .line 114
    new-instance v0, Lcom/google/android/videos/utils/XmlParser;

    sget-object v1, Lcom/google/android/videos/utils/XmlParser;->PREFIXES_ONLY_FEATURES:Ljava/util/Map;

    invoke-direct {v0, v1}, Lcom/google/android/videos/utils/XmlParser;-><init>(Ljava/util/Map;)V

    return-object v0
.end method


# virtual methods
.method public parse(Ljava/io/InputStream;Ljava/util/Map;)Ljava/lang/Object;
    .locals 5
    .param p1, "input"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/utils/XmlParser$Rule;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 149
    .local p2, "rules":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/videos/utils/XmlParser$Rule;>;"
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    :try_start_0
    iget-object v4, p0, Lcom/google/android/videos/utils/XmlParser;->factory:Ljavax/xml/parsers/SAXParserFactory;

    monitor-enter v4
    :try_end_0
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_1

    .line 154
    :try_start_1
    iget-object v3, p0, Lcom/google/android/videos/utils/XmlParser;->factory:Ljavax/xml/parsers/SAXParserFactory;

    invoke-virtual {v3}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v3

    invoke-virtual {v3}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v2

    .line 155
    .local v2, "reader":Lorg/xml/sax/XMLReader;
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 156
    :try_start_2
    new-instance v1, Lcom/google/android/videos/utils/XmlParser$InternalHandler;

    invoke-direct {v1, p2}, Lcom/google/android/videos/utils/XmlParser$InternalHandler;-><init>(Ljava/util/Map;)V

    .line 157
    .local v1, "handler":Lcom/google/android/videos/utils/XmlParser$InternalHandler;
    invoke-interface {v2, v1}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 158
    new-instance v3, Lorg/xml/sax/InputSource;

    invoke-direct {v3, p1}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/InputStream;)V

    invoke-interface {v2, v3}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 159
    iget-object v3, v1, Lcom/google/android/videos/utils/XmlParser$InternalHandler;->result:Ljava/lang/Object;

    if-nez v3, :cond_0

    .line 161
    new-instance v3, Lcom/google/android/videos/converter/ConverterException;

    const-string v4, "XML is well-formed but invalid"

    invoke-direct {v3, v4}, Lcom/google/android/videos/converter/ConverterException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_2
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_2 .. :try_end_2} :catch_1

    .line 164
    .end local v1    # "handler":Lcom/google/android/videos/utils/XmlParser$InternalHandler;
    .end local v2    # "reader":Lorg/xml/sax/XMLReader;
    :catch_0
    move-exception v0

    .line 165
    .local v0, "e":Ljavax/xml/parsers/ParserConfigurationException;
    new-instance v3, Lcom/google/android/videos/converter/ConverterException;

    invoke-direct {v3, v0}, Lcom/google/android/videos/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 155
    .end local v0    # "e":Ljavax/xml/parsers/ParserConfigurationException;
    :catchall_0
    move-exception v3

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v3
    :try_end_4
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_4 .. :try_end_4} :catch_1

    .line 166
    :catch_1
    move-exception v0

    .line 167
    .local v0, "e":Lorg/xml/sax/SAXException;
    new-instance v3, Lcom/google/android/videos/converter/ConverterException;

    invoke-direct {v3, v0}, Lcom/google/android/videos/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 163
    .end local v0    # "e":Lorg/xml/sax/SAXException;
    .restart local v1    # "handler":Lcom/google/android/videos/utils/XmlParser$InternalHandler;
    .restart local v2    # "reader":Lorg/xml/sax/XMLReader;
    :cond_0
    :try_start_5
    iget-object v3, v1, Lcom/google/android/videos/utils/XmlParser$InternalHandler;->result:Ljava/lang/Object;
    :try_end_5
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_5 .. :try_end_5} :catch_1

    return-object v3
.end method
