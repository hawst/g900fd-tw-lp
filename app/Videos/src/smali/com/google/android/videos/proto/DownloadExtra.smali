.class public final Lcom/google/android/videos/proto/DownloadExtra;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DownloadExtra.java"


# instance fields
.field public streamInfos:[Lcom/google/android/videos/proto/StreamInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 27
    invoke-virtual {p0}, Lcom/google/android/videos/proto/DownloadExtra;->clear()Lcom/google/android/videos/proto/DownloadExtra;

    .line 28
    return-void
.end method

.method public static parseFrom([B)Lcom/google/android/videos/proto/DownloadExtra;
    .locals 1
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 130
    new-instance v0, Lcom/google/android/videos/proto/DownloadExtra;

    invoke-direct {v0}, Lcom/google/android/videos/proto/DownloadExtra;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/proto/DownloadExtra;

    return-object v0
.end method


# virtual methods
.method public clear()Lcom/google/android/videos/proto/DownloadExtra;
    .locals 1

    .prologue
    .line 31
    invoke-static {}, Lcom/google/android/videos/proto/StreamInfo;->emptyArray()[Lcom/google/android/videos/proto/StreamInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/proto/DownloadExtra;->streamInfos:[Lcom/google/android/videos/proto/StreamInfo;

    .line 32
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/proto/DownloadExtra;->cachedSize:I

    .line 33
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 4

    .prologue
    .line 76
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 77
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/videos/proto/DownloadExtra;->streamInfos:[Lcom/google/android/videos/proto/StreamInfo;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/videos/proto/DownloadExtra;->streamInfos:[Lcom/google/android/videos/proto/StreamInfo;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 78
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/videos/proto/DownloadExtra;->streamInfos:[Lcom/google/android/videos/proto/StreamInfo;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 79
    iget-object v3, p0, Lcom/google/android/videos/proto/DownloadExtra;->streamInfos:[Lcom/google/android/videos/proto/StreamInfo;

    aget-object v0, v3, v1

    .line 80
    .local v0, "element":Lcom/google/android/videos/proto/StreamInfo;
    if-eqz v0, :cond_0

    .line 81
    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 78
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 86
    .end local v0    # "element":Lcom/google/android/videos/proto/StreamInfo;
    .end local v1    # "i":I
    :cond_1
    return v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 38
    if-ne p1, p0, :cond_1

    .line 49
    :cond_0
    :goto_0
    return v1

    .line 41
    :cond_1
    instance-of v3, p1, Lcom/google/android/videos/proto/DownloadExtra;

    if-nez v3, :cond_2

    move v1, v2

    .line 42
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 44
    check-cast v0, Lcom/google/android/videos/proto/DownloadExtra;

    .line 45
    .local v0, "other":Lcom/google/android/videos/proto/DownloadExtra;
    iget-object v3, p0, Lcom/google/android/videos/proto/DownloadExtra;->streamInfos:[Lcom/google/android/videos/proto/StreamInfo;

    iget-object v4, v0, Lcom/google/android/videos/proto/DownloadExtra;->streamInfos:[Lcom/google/android/videos/proto/StreamInfo;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 47
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 54
    const/16 v0, 0x11

    .line 55
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/android/videos/proto/DownloadExtra;->streamInfos:[Lcom/google/android/videos/proto/StreamInfo;

    invoke-static {v1}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 57
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/DownloadExtra;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 94
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 95
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 99
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 100
    :sswitch_0
    return-object p0

    .line 105
    :sswitch_1
    const/16 v5, 0xa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 107
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/videos/proto/DownloadExtra;->streamInfos:[Lcom/google/android/videos/proto/StreamInfo;

    if-nez v5, :cond_2

    move v1, v4

    .line 108
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/videos/proto/StreamInfo;

    .line 110
    .local v2, "newArray":[Lcom/google/android/videos/proto/StreamInfo;
    if-eqz v1, :cond_1

    .line 111
    iget-object v5, p0, Lcom/google/android/videos/proto/DownloadExtra;->streamInfos:[Lcom/google/android/videos/proto/StreamInfo;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 113
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 114
    new-instance v5, Lcom/google/android/videos/proto/StreamInfo;

    invoke-direct {v5}, Lcom/google/android/videos/proto/StreamInfo;-><init>()V

    aput-object v5, v2, v1

    .line 115
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 116
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 113
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 107
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/videos/proto/StreamInfo;
    :cond_2
    iget-object v5, p0, Lcom/google/android/videos/proto/DownloadExtra;->streamInfos:[Lcom/google/android/videos/proto/StreamInfo;

    array-length v1, v5

    goto :goto_1

    .line 119
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/videos/proto/StreamInfo;
    :cond_3
    new-instance v5, Lcom/google/android/videos/proto/StreamInfo;

    invoke-direct {v5}, Lcom/google/android/videos/proto/StreamInfo;-><init>()V

    aput-object v5, v2, v1

    .line 120
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 121
    iput-object v2, p0, Lcom/google/android/videos/proto/DownloadExtra;->streamInfos:[Lcom/google/android/videos/proto/StreamInfo;

    goto :goto_0

    .line 95
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/android/videos/proto/DownloadExtra;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/DownloadExtra;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    iget-object v2, p0, Lcom/google/android/videos/proto/DownloadExtra;->streamInfos:[Lcom/google/android/videos/proto/StreamInfo;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/videos/proto/DownloadExtra;->streamInfos:[Lcom/google/android/videos/proto/StreamInfo;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 64
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/videos/proto/DownloadExtra;->streamInfos:[Lcom/google/android/videos/proto/StreamInfo;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 65
    iget-object v2, p0, Lcom/google/android/videos/proto/DownloadExtra;->streamInfos:[Lcom/google/android/videos/proto/StreamInfo;

    aget-object v0, v2, v1

    .line 66
    .local v0, "element":Lcom/google/android/videos/proto/StreamInfo;
    if-eqz v0, :cond_0

    .line 67
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 64
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 71
    .end local v0    # "element":Lcom/google/android/videos/proto/StreamInfo;
    .end local v1    # "i":I
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 72
    return-void
.end method
