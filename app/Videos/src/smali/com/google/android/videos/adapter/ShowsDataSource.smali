.class public Lcom/google/android/videos/adapter/ShowsDataSource;
.super Lcom/google/android/videos/adapter/CursorDataSource;
.source "ShowsDataSource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/adapter/ShowsDataSource$Query;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/google/android/videos/adapter/CursorDataSource;-><init>()V

    .line 31
    return-void
.end method

.method public static getShowId(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 20
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getShowTitle(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 24
    const/4 v0, 0x1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isAnyEpisodePinned(Landroid/database/Cursor;)Z
    .locals 1
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 28
    const/4 v0, 0x2

    invoke-static {p0, v0}, Lcom/google/android/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public getItemIdentifier(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lcom/google/android/videos/adapter/ShowsDataSource;->getItem(I)Landroid/database/Cursor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/adapter/ShowsDataSource;->getShowId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromShowId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
