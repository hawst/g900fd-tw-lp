.class public Lcom/google/android/videos/search/VideosDataManager;
.super Lcom/google/android/videos/store/Database$BaseListener;
.source "VideosDataManager.java"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Lcom/google/android/gms/appdatasearch/util/AppDataSearchDataManager;


# static fields
.field private static final MOVIES_SPEC:Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

.field private static final SHOWS_SPEC:Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;


# instance fields
.field private final db:Lcom/google/android/videos/store/Database;

.field private final handler:Landroid/os/Handler;

.field private final listener:Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;

.field private final moviesChangedFlag:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final showsChangedFlag:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 33
    const-string v0, "movies"

    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;->builder(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    const-string v1, "video_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->uriColumn(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    const-string v1, "title"

    new-instance v2, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;

    const-string v3, "title"

    invoke-direct {v2, v3}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;->indexPrefixes(Z)Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;->build()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->addSectionForColumn(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    const-string v1, "account"

    new-instance v2, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;

    const-string v3, "account"

    invoke-direct {v2, v3}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;->noIndex(Z)Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;->build()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->addSectionForColumn(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    const-string v1, "text1"

    const v2, 0x7f0b006d

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->addGlobalSearchSectionTemplate(Ljava/lang/String;I)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    const-string v1, "icon"

    const v2, 0x7f0b006e

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->addGlobalSearchSectionTemplate(Ljava/lang/String;I)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    const-string v1, "intent_data"

    const v2, 0x7f0b006f

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->addGlobalSearchSectionTemplate(Ljava/lang/String;I)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    const-string v1, "intent_extra_data"

    const v2, 0x7f0b0070

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->addGlobalSearchSectionTemplate(Ljava/lang/String;I)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->build()Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

    move-result-object v0

    sput-object v0, Lcom/google/android/videos/search/VideosDataManager;->MOVIES_SPEC:Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

    .line 49
    const-string v0, "shows"

    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;->builder(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    const-string v1, "show_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->uriColumn(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    const-string v1, "title"

    new-instance v2, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;

    const-string v3, "title"

    invoke-direct {v2, v3}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;->indexPrefixes(Z)Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;->build()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->addSectionForColumn(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    const-string v1, "account"

    new-instance v2, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;

    const-string v3, "account"

    invoke-direct {v2, v3}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;->noIndex(Z)Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;->build()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->addSectionForColumn(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    const-string v1, "text1"

    const v2, 0x7f0b0071

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->addGlobalSearchSectionTemplate(Ljava/lang/String;I)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    const-string v1, "icon"

    const v2, 0x7f0b0072

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->addGlobalSearchSectionTemplate(Ljava/lang/String;I)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    const-string v1, "intent_data"

    const v2, 0x7f0b0073

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->addGlobalSearchSectionTemplate(Ljava/lang/String;I)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    const-string v1, "intent_extra_data"

    const v2, 0x7f0b0074

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->addGlobalSearchSectionTemplate(Ljava/lang/String;I)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->build()Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

    move-result-object v0

    sput-object v0, Lcom/google/android/videos/search/VideosDataManager;->SHOWS_SPEC:Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/videos/store/Database;Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;)V
    .locals 3
    .param p1, "db"    # Lcom/google/android/videos/store/Database;
    .param p2, "listener"    # Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/google/android/videos/store/Database$BaseListener;-><init>()V

    .line 80
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v1, p0, Lcom/google/android/videos/search/VideosDataManager;->moviesChangedFlag:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 81
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v1, p0, Lcom/google/android/videos/search/VideosDataManager;->showsChangedFlag:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 86
    iput-object p1, p0, Lcom/google/android/videos/search/VideosDataManager;->db:Lcom/google/android/videos/store/Database;

    .line 87
    new-instance v0, Landroid/os/HandlerThread;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 89
    .local v0, "handlerThread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 90
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lcom/google/android/videos/search/VideosDataManager;->handler:Landroid/os/Handler;

    .line 91
    iput-object p2, p0, Lcom/google/android/videos/search/VideosDataManager;->listener:Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;

    .line 92
    if-eqz p2, :cond_0

    .line 93
    iget-object v1, p0, Lcom/google/android/videos/search/VideosDataManager;->db:Lcom/google/android/videos/store/Database;

    invoke-virtual {v1, p0}, Lcom/google/android/videos/store/Database;->addListener(Lcom/google/android/videos/store/Database$Listener;)V

    .line 95
    :cond_0
    return-void
.end method

.method private fillIcingMoviesSupportTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 215
    const-string v0, "DELETE FROM icing_movies"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 216
    const-string v0, "INSERT INTO icing_movies (icing_movie_id) SELECT asset_id FROM purchased_assets, videos ON asset_type = 6 AND asset_id = video_id WHERE NOT (hidden IN (1, 3)) AND purchase_status = 2 GROUP BY asset_id"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 224
    return-void
.end method

.method private fillIcingShowsSupportTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 227
    const-string v0, "DELETE FROM icing_shows"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 228
    const-string v0, "INSERT INTO icing_shows (icing_show_id) SELECT show_id FROM purchased_assets, videos, seasons, shows ON asset_type = 20 AND asset_id = video_id AND episode_season_id = season_id AND show_id = shows_id WHERE NOT (hidden IN (1, 3)) AND purchase_status = 2 GROUP BY show_id"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 236
    return-void
.end method

.method private scheduleTablesUpdateIfNeeded(ZZ)V
    .locals 4
    .param p1, "movies"    # Z
    .param p2, "shows"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 267
    if-eqz p1, :cond_0

    .line 268
    iget-object v0, p0, Lcom/google/android/videos/search/VideosDataManager;->moviesChangedFlag:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    .line 270
    :cond_0
    if-eqz p2, :cond_1

    .line 271
    iget-object v0, p0, Lcom/google/android/videos/search/VideosDataManager;->showsChangedFlag:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    .line 273
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/search/VideosDataManager;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 274
    iget-object v0, p0, Lcom/google/android/videos/search/VideosDataManager;->handler:Landroid/os/Handler;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 275
    return-void
.end method


# virtual methods
.method public cleanSequenceTable(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;J)V
    .locals 8
    .param p1, "table"    # Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    .param p2, "lastCommitedSeqNo"    # J

    .prologue
    const/4 v6, 0x0

    .line 99
    const/4 v0, 0x0

    .line 100
    .local v0, "sql":Ljava/lang/String;
    const-string v3, "movies"

    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->getTableName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 101
    const-string v0, "DELETE FROM icing_movies WHERE icing_movie_seq_no < ?"

    .line 107
    :goto_0
    iget-object v3, p0, Lcom/google/android/videos/search/VideosDataManager;->db:Lcom/google/android/videos/store/Database;

    invoke-virtual {v3}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 108
    .local v2, "transaction":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, 0x0

    .line 110
    .local v1, "success":Z
    const/4 v3, 0x1

    :try_start_0
    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111
    const/4 v1, 0x1

    .line 113
    iget-object v3, p0, Lcom/google/android/videos/search/VideosDataManager;->db:Lcom/google/android/videos/store/Database;

    new-array v4, v6, [Ljava/lang/Object;

    invoke-virtual {v3, v2, v1, v6, v4}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 115
    .end local v1    # "success":Z
    .end local v2    # "transaction":Landroid/database/sqlite/SQLiteDatabase;
    :cond_0
    return-void

    .line 102
    :cond_1
    const-string v3, "shows"

    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->getTableName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 103
    const-string v0, "DELETE FROM icing_shows WHERE icing_show_seq_no < ?"

    goto :goto_0

    .line 113
    .restart local v1    # "success":Z
    .restart local v2    # "transaction":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/google/android/videos/search/VideosDataManager;->db:Lcom/google/android/videos/store/Database;

    new-array v5, v6, [Ljava/lang/Object;

    invoke-virtual {v4, v2, v1, v6, v5}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v3
.end method

.method public getMaxSeqno(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)J
    .locals 4
    .param p1, "table"    # Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    .prologue
    .line 119
    const/4 v0, 0x0

    .line 120
    .local v0, "sql":Ljava/lang/String;
    const-string v1, "movies"

    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->getTableName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 121
    const-string v0, "SELECT MAX(icing_movie_seq_no) FROM icing_movies"

    .line 127
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/search/VideosDataManager;->db:Lcom/google/android/videos/store/Database;

    invoke-virtual {v1}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->simpleQueryForLong()J

    move-result-wide v2

    :goto_1
    return-wide v2

    .line 122
    :cond_0
    const-string v1, "shows"

    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->getTableName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 123
    const-string v0, "SELECT MAX(icing_show_seq_no) FROM icing_shows"

    goto :goto_0

    .line 125
    :cond_1
    const-wide/16 v2, 0x0

    goto :goto_1
.end method

.method public bridge synthetic getTableMappings()[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/google/android/videos/search/VideosDataManager;->getTableMappings()[Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

    move-result-object v0

    return-object v0
.end method

.method public getTableMappings()[Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;
    .locals 3

    .prologue
    .line 132
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/videos/search/VideosDataManager;->MOVIES_SPEC:Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/videos/search/VideosDataManager;->SHOWS_SPEC:Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x0

    .line 257
    iget-object v0, p0, Lcom/google/android/videos/search/VideosDataManager;->moviesChangedFlag:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/google/android/videos/search/VideosDataManager;->listener:Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;

    sget-object v1, Lcom/google/android/videos/search/VideosDataManager;->MOVIES_SPEC:Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

    invoke-interface {v0, v1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;->onTableChanged(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)Z

    .line 260
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/search/VideosDataManager;->showsChangedFlag:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 261
    iget-object v0, p0, Lcom/google/android/videos/search/VideosDataManager;->listener:Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;

    sget-object v1, Lcom/google/android/videos/search/VideosDataManager;->SHOWS_SPEC:Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

    invoke-interface {v0, v1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;->onTableChanged(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)Z

    .line 263
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public onMovieMetadataUpdated(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 245
    .local p1, "movieIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/search/VideosDataManager;->scheduleTablesUpdateIfNeeded(ZZ)V

    .line 246
    return-void
.end method

.method public onPurchasesUpdated(Ljava/lang/String;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 240
    invoke-direct {p0, v0, v0}, Lcom/google/android/videos/search/VideosDataManager;->scheduleTablesUpdateIfNeeded(ZZ)V

    .line 241
    return-void
.end method

.method public onShowMetadataUpdated(Ljava/lang/String;)V
    .locals 2
    .param p1, "showId"    # Ljava/lang/String;

    .prologue
    .line 250
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/search/VideosDataManager;->scheduleTablesUpdateIfNeeded(ZZ)V

    .line 251
    return-void
.end method

.method public querySequenceTable(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;JJ)Landroid/database/Cursor;
    .locals 8
    .param p1, "table"    # Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    .param p2, "lastSeqNo"    # J
    .param p4, "limit"    # J

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 137
    const/4 v0, 0x0

    .line 138
    .local v0, "sql":Ljava/lang/String;
    const-string v1, "movies"

    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->getTableName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 139
    const-string v0, "SELECT MAX(icing_movie_seq_no) AS seqno, icing_movie_id AS uri, CASE WHEN account IS NOT NULL THEN \'add\' ELSE \'del\' END as action, title AS section_title, account AS section_account, 1 AS doc_score FROM icing_movies LEFT JOIN (purchased_assets, videos ON asset_id = video_id AND asset_type = 6 AND NOT (hidden IN (1, 3)) AND purchase_status = 2) ON icing_movie_id = video_id WHERE icing_movie_seq_no > ? GROUP BY icing_movie_id ORDER BY icing_movie_seq_no LIMIT ?"

    .line 189
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/search/VideosDataManager;->db:Lcom/google/android/videos/store/Database;

    invoke-virtual {v1}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/String;

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {p4, p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v1, v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :goto_1
    return-object v1

    .line 160
    :cond_0
    const-string v1, "shows"

    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->getTableName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 161
    const-string v0, "SELECT MAX(icing_show_seq_no) AS seqno, icing_show_id AS uri, CASE WHEN account IS NOT NULL THEN \'add\' ELSE \'del\' END as action, shows_title AS section_title, account AS section_account,  1 AS doc_score FROM icing_shows LEFT JOIN (purchased_assets, videos, seasons, shows ON asset_id = video_id AND season_id = episode_season_id AND show_id = shows_id AND asset_type = 20 AND NOT (hidden IN (1, 3)) AND purchase_status = 2) ON icing_show_id = show_id WHERE icing_show_seq_no > ? GROUP BY icing_show_id ORDER BY icing_show_seq_no LIMIT ?"

    goto :goto_0

    .line 186
    :cond_1
    new-instance v1, Landroid/database/MatrixCursor;

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "seqno"

    aput-object v3, v2, v4

    const-string v3, "uri"

    aput-object v3, v2, v5

    const-string v3, "action"

    aput-object v3, v2, v6

    const/4 v3, 0x3

    const-string v4, "section_title"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "section_account"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "doc_score"

    aput-object v4, v2, v3

    invoke-direct {v1, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    goto :goto_1
.end method

.method public queryTagsTable(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;JJ)Landroid/database/Cursor;
    .locals 1
    .param p1, "table"    # Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    .param p2, "lastSeqNo"    # J
    .param p4, "limit"    # J

    .prologue
    .line 195
    const/4 v0, 0x0

    return-object v0
.end method

.method public recreateSequenceTable(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)V
    .locals 6
    .param p1, "table"    # Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    .prologue
    const/4 v5, 0x0

    .line 200
    iget-object v2, p0, Lcom/google/android/videos/search/VideosDataManager;->db:Lcom/google/android/videos/store/Database;

    invoke-virtual {v2}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 201
    .local v1, "transaction":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v0, 0x0

    .line 203
    .local v0, "success":Z
    :try_start_0
    const-string v2, "movies"

    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->getTableName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 204
    invoke-direct {p0, v1}, Lcom/google/android/videos/search/VideosDataManager;->fillIcingMoviesSupportTable(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 208
    :cond_0
    :goto_0
    const/4 v0, 0x1

    .line 210
    iget-object v2, p0, Lcom/google/android/videos/search/VideosDataManager;->db:Lcom/google/android/videos/store/Database;

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v2, v1, v0, v5, v3}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 212
    return-void

    .line 205
    :cond_1
    :try_start_1
    const-string v2, "shows"

    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->getTableName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 206
    invoke-direct {p0, v1}, Lcom/google/android/videos/search/VideosDataManager;->fillIcingShowsSupportTable(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 210
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/google/android/videos/search/VideosDataManager;->db:Lcom/google/android/videos/store/Database;

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v1, v0, v5, v4}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v2
.end method
