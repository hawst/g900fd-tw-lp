.class public abstract Lcom/google/android/videos/async/Request;
.super Ljava/lang/Object;
.source "Request.java"


# instance fields
.field public final account:Ljava/lang/String;

.field public final requireAuthentication:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 23
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/async/Request;-><init>(Ljava/lang/String;Z)V

    .line 24
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "requireAuthentication"    # Z

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    if-eqz p2, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "When authentication is required the account cannot be null."

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 29
    iput-object p1, p0, Lcom/google/android/videos/async/Request;->account:Ljava/lang/String;

    .line 30
    iput-boolean p2, p0, Lcom/google/android/videos/async/Request;->requireAuthentication:Z

    .line 31
    return-void

    .line 27
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
