.class public Lcom/google/android/videos/player/legacy/MediaPlayerException;
.super Ljava/lang/Exception;
.source "MediaPlayerException.java"


# static fields
.field public static final EXTRA_HEARTBEAT_AUTHENTICATION_FAILURE:I

.field public static final EXTRA_HEARTBEAT_CANNOT_ACTIVATE_RENTAL:I

.field public static final EXTRA_HEARTBEAT_CONCURRENT_PLAYBACK:I

.field public static final EXTRA_HEARTBEAT_CONCURRENT_PLAYBACKS_BY_ACCOUNT:I

.field public static final EXTRA_HEARTBEAT_NO_ACTIVE_PURCHASE_AGREEMENT:I

.field public static final EXTRA_HEARTBEAT_STREAMING_UNAVAILABLE:I

.field public static final EXTRA_HEARTBEAT_TERMINATE_REQUESTED:I

.field public static final EXTRA_HEARTBEAT_UNUSUAL_ACTIVITY:I


# instance fields
.field public final extra:I

.field public final what:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, -0xbb8

    .line 42
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v1, 0xf

    if-le v0, v1, :cond_0

    .line 44
    const/16 v0, -0xbb6

    sput v0, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_AUTHENTICATION_FAILURE:I

    .line 45
    const/16 v0, -0xbb5

    sput v0, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_NO_ACTIVE_PURCHASE_AGREEMENT:I

    .line 46
    const/16 v0, -0xbb4

    sput v0, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_CONCURRENT_PLAYBACK:I

    .line 47
    const/16 v0, -0xbb3

    sput v0, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_UNUSUAL_ACTIVITY:I

    .line 48
    const/16 v0, -0xbb2

    sput v0, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_STREAMING_UNAVAILABLE:I

    .line 49
    const/16 v0, -0xbb1

    sput v0, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_CANNOT_ACTIVATE_RENTAL:I

    .line 50
    const/16 v0, -0xbb0

    sput v0, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_CONCURRENT_PLAYBACKS_BY_ACCOUNT:I

    .line 51
    sput v2, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_TERMINATE_REQUESTED:I

    .line 63
    :goto_0
    return-void

    .line 54
    :cond_0
    sput v2, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_AUTHENTICATION_FAILURE:I

    .line 55
    const/16 v0, -0xbb9

    sput v0, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_NO_ACTIVE_PURCHASE_AGREEMENT:I

    .line 56
    const/16 v0, -0xbba

    sput v0, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_CONCURRENT_PLAYBACK:I

    .line 57
    const/16 v0, -0xbbb

    sput v0, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_UNUSUAL_ACTIVITY:I

    .line 58
    const/16 v0, -0xbbc

    sput v0, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_STREAMING_UNAVAILABLE:I

    .line 59
    const/16 v0, -0xbbd

    sput v0, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_CANNOT_ACTIVATE_RENTAL:I

    .line 60
    const v0, 0x7fffffff

    sput v0, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_CONCURRENT_PLAYBACKS_BY_ACCOUNT:I

    .line 61
    const/16 v0, -0xbbe

    sput v0, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_TERMINATE_REQUESTED:I

    goto :goto_0
.end method

.method public constructor <init>(II)V
    .locals 2
    .param p1, "what"    # I
    .param p2, "extra"    # I

    .prologue
    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "What: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", extra: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 70
    iput p1, p0, Lcom/google/android/videos/player/legacy/MediaPlayerException;->what:I

    .line 71
    iput p2, p0, Lcom/google/android/videos/player/legacy/MediaPlayerException;->extra:I

    .line 72
    return-void
.end method

.method public constructor <init>(ILjava/lang/Throwable;)V
    .locals 2
    .param p1, "what"    # I
    .param p2, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "What: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 76
    iput p1, p0, Lcom/google/android/videos/player/legacy/MediaPlayerException;->what:I

    .line 77
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/player/legacy/MediaPlayerException;->extra:I

    .line 78
    return-void
.end method
