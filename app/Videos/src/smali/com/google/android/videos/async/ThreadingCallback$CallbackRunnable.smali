.class Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;
.super Ljava/lang/Object;
.source "ThreadingCallback.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/async/ThreadingCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CallbackRunnable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field private exception:Ljava/lang/Exception;

.field private isCancelled:Z

.field private request:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TR;"
        }
    .end annotation
.end field

.field private response:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private success:Z

.field private target:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 78
    .local p0, "this":Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;, "Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable<TR;TE;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/async/ThreadingCallback$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/async/ThreadingCallback$1;

    .prologue
    .line 78
    .local p0, "this":Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;, "Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable<TR;TE;>;"
    invoke-direct {p0}, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;-><init>()V

    return-void
.end method


# virtual methods
.method public prepareForOnCancelled(Lcom/google/android/videos/async/Callback;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;TR;)V"
        }
    .end annotation

    .prologue
    .line 106
    .local p0, "this":Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;, "Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable<TR;TE;>;"
    .local p1, "target":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TR;TE;>;"
    .local p2, "request":Ljava/lang/Object;, "TR;"
    iput-object p1, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->target:Lcom/google/android/videos/async/Callback;

    .line 107
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->isCancelled:Z

    .line 108
    iput-object p2, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->request:Ljava/lang/Object;

    .line 109
    return-void
.end method

.method public prepareForOnError(Lcom/google/android/videos/async/Callback;Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2
    .param p3, "exception"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;TR;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;, "Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable<TR;TE;>;"
    .local p1, "target":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TR;TE;>;"
    .local p2, "request":Ljava/lang/Object;, "TR;"
    const/4 v1, 0x0

    .line 97
    iput-object p1, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->target:Lcom/google/android/videos/async/Callback;

    .line 98
    iput-object p2, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->request:Ljava/lang/Object;

    .line 99
    iput-object p3, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->exception:Ljava/lang/Exception;

    .line 100
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->response:Ljava/lang/Object;

    .line 101
    iput-boolean v1, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->success:Z

    .line 102
    iput-boolean v1, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->isCancelled:Z

    .line 103
    return-void
.end method

.method public prepareForOnResponse(Lcom/google/android/videos/async/Callback;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;TR;TE;)V"
        }
    .end annotation

    .prologue
    .line 88
    .local p0, "this":Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;, "Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable<TR;TE;>;"
    .local p1, "target":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TR;TE;>;"
    .local p2, "request":Ljava/lang/Object;, "TR;"
    .local p3, "response":Ljava/lang/Object;, "TE;"
    iput-object p1, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->target:Lcom/google/android/videos/async/Callback;

    .line 89
    iput-object p2, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->request:Ljava/lang/Object;

    .line 90
    iput-object p3, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->response:Ljava/lang/Object;

    .line 91
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->exception:Ljava/lang/Exception;

    .line 92
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->success:Z

    .line 93
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->isCancelled:Z

    .line 94
    return-void
.end method

.method public run()V
    .locals 6

    .prologue
    .local p0, "this":Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;, "Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable<TR;TE;>;"
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 115
    iget-boolean v1, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->isCancelled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->request:Ljava/lang/Object;

    instance-of v1, v1, Lcom/google/android/videos/async/ControllableRequest;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->request:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/videos/async/ControllableRequest;

    invoke-virtual {v1}, Lcom/google/android/videos/async/ControllableRequest;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 119
    .local v0, "cancelled":Z
    :goto_0
    if-eqz v0, :cond_3

    .line 120
    iget-object v1, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->target:Lcom/google/android/videos/async/Callback;

    instance-of v1, v1, Lcom/google/android/videos/async/NewCallback;

    if-eqz v1, :cond_1

    .line 121
    iget-object v1, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->target:Lcom/google/android/videos/async/Callback;

    check-cast v1, Lcom/google/android/videos/async/NewCallback;

    iget-object v3, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->request:Ljava/lang/Object;

    invoke-interface {v1, v3}, Lcom/google/android/videos/async/NewCallback;->onCancelled(Ljava/lang/Object;)V

    .line 123
    iget-object v1, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->response:Ljava/lang/Object;

    instance-of v1, v1, Ljava/io/Closeable;

    if-eqz v1, :cond_1

    .line 125
    :try_start_0
    iget-object v1, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->response:Ljava/lang/Object;

    check-cast v1, Ljava/io/Closeable;

    invoke-interface {v1}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    :cond_1
    :goto_1
    iput-object v5, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->target:Lcom/google/android/videos/async/Callback;

    .line 137
    iput-object v5, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->request:Ljava/lang/Object;

    .line 138
    iput-object v5, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->response:Ljava/lang/Object;

    .line 139
    iput-object v5, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->exception:Ljava/lang/Exception;

    .line 140
    iput-boolean v2, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->success:Z

    .line 141
    # invokes: Lcom/google/android/videos/async/ThreadingCallback;->releaseRunnable(Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;)V
    invoke-static {p0}, Lcom/google/android/videos/async/ThreadingCallback;->access$100(Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;)V

    .line 142
    return-void

    .end local v0    # "cancelled":Z
    :cond_2
    move v0, v2

    .line 115
    goto :goto_0

    .line 131
    .restart local v0    # "cancelled":Z
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->success:Z

    if-eqz v1, :cond_4

    .line 132
    iget-object v1, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->target:Lcom/google/android/videos/async/Callback;

    iget-object v3, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->request:Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->response:Ljava/lang/Object;

    invoke-interface {v1, v3, v4}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 134
    :cond_4
    iget-object v1, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->target:Lcom/google/android/videos/async/Callback;

    iget-object v3, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->request:Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/videos/async/ThreadingCallback$CallbackRunnable;->exception:Ljava/lang/Exception;

    invoke-interface {v1, v3, v4}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_1

    .line 126
    :catch_0
    move-exception v1

    goto :goto_1
.end method
