.class public abstract Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;
.super Ljava/lang/Object;
.source "ClusterItemView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/playnext/ClusterItemView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Binder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Lcom/google/android/videos/ui/playnext/ClusterItemView;",
        "D::",
        "Lcom/google/android/videos/adapter/DataSource",
        "<*>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final clickableViews:[Landroid/view/View;

.field private final clusterUiElementNode:Lcom/google/android/videos/logging/UiElementNode;


# direct methods
.method protected constructor <init>(Lcom/google/android/videos/logging/UiElementNode;I)V
    .locals 1
    .param p1, "clusterUiElementNode"    # Lcom/google/android/videos/logging/UiElementNode;
    .param p2, "maxClickableViewCount"    # I

    .prologue
    .line 93
    .local p0, "this":Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;, "Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder<TV;TD;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    iput-object p1, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;->clusterUiElementNode:Lcom/google/android/videos/logging/UiElementNode;

    .line 95
    new-array v0, p2, [Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;->clickableViews:[Landroid/view/View;

    .line 96
    return-void
.end method

.method private childImpression(Lcom/google/android/videos/ui/playnext/ClusterItemView;Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;I)V
    .locals 3
    .param p1, "view"    # Lcom/google/android/videos/ui/playnext/ClusterItemView;
    .param p2, "assetInfo"    # Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;
    .param p3, "offerType"    # I

    .prologue
    .line 132
    .local p0, "this":Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;, "Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder<TV;TD;>;"
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    new-instance v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;

    invoke-direct {v0}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;-><init>()V

    .line 134
    .local v0, "clientLogsCookie":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;
    iput-object p2, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    .line 135
    iput p3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;->offerType:I

    .line 136
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;->clusterUiElementNode:Lcom/google/android/videos/logging/UiElementNode;

    const/16 v2, 0x10

    invoke-virtual {p1, v1, v2, v0}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->initForLogging(Lcom/google/android/videos/logging/UiElementNode;ILcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;)V

    .line 137
    return-void
.end method


# virtual methods
.method public final bind(Lcom/google/android/videos/ui/playnext/ClusterItemView;Lcom/google/android/videos/adapter/DataSource;I)[Landroid/view/View;
    .locals 2
    .param p3, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;TD;I)[",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 106
    .local p0, "this":Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;, "Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder<TV;TD;>;"
    .local p1, "view":Lcom/google/android/videos/ui/playnext/ClusterItemView;, "TV;"
    .local p2, "dataSource":Lcom/google/android/videos/adapter/DataSource;, "TD;"
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;->onBind(Lcom/google/android/videos/ui/playnext/ClusterItemView;Lcom/google/android/videos/adapter/DataSource;I)V

    .line 107
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;->clickableViews:[Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;->clickableViews:[Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->collectClickableViews([Landroid/view/View;)V

    .line 109
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;->clickableViews:[Landroid/view/View;

    return-object v0
.end method

.method protected childImpression(Lcom/google/android/videos/ui/playnext/ClusterItemView;Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;)V
    .locals 1
    .param p1, "view"    # Lcom/google/android/videos/ui/playnext/ClusterItemView;
    .param p2, "assetInfo"    # Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    .prologue
    .line 128
    .local p0, "this":Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;, "Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder<TV;TD;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;->childImpression(Lcom/google/android/videos/ui/playnext/ClusterItemView;Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;I)V

    .line 129
    return-void
.end method

.method protected childImpression(Lcom/google/android/videos/ui/playnext/ClusterItemView;Lcom/google/wireless/android/video/magma/proto/AssetResource;)V
    .locals 4
    .param p1, "view"    # Lcom/google/android/videos/ui/playnext/ClusterItemView;
    .param p2, "assetResource"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    .line 119
    .local p0, "this":Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;, "Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder<TV;TD;>;"
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    iget-object v3, p2, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-static {v3}, Lcom/google/android/videos/api/AssetResourceUtil;->assetInfoFromAssetResourceId(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v0

    .line 122
    .local v0, "assetInfo":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;
    iget-object v3, p2, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    invoke-static {v3}, Lcom/google/android/videos/utils/OfferUtil;->getCheapest([Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    move-result-object v1

    .line 123
    .local v1, "offer":Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    if-nez v1, :cond_0

    const/4 v2, 0x0

    .line 124
    .local v2, "offerType":I
    :goto_0
    invoke-direct {p0, p1, v0, v2}, Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;->childImpression(Lcom/google/android/videos/ui/playnext/ClusterItemView;Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;I)V

    .line 125
    return-void

    .line 123
    .end local v2    # "offerType":I
    :cond_0
    iget v2, v1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formatType:I

    goto :goto_0
.end method

.method protected abstract onBind(Lcom/google/android/videos/ui/playnext/ClusterItemView;Lcom/google/android/videos/adapter/DataSource;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;TD;I)V"
        }
    .end annotation
.end method
