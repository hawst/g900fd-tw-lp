.class Lcom/google/android/videos/pinning/LegacyDownloader;
.super Lcom/google/android/videos/pinning/Downloader;
.source "LegacyDownloader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/pinning/LegacyDownloader$1;
    }
.end annotation


# static fields
.field private static final CONTENT_RANGE_HEADER:Ljava/util/regex/Pattern;


# instance fields
.field private final context:Landroid/content/Context;

.field private final drmManager:Lcom/google/android/videos/drm/DrmManager;

.field private mediaStream:Lcom/google/android/videos/streams/MediaStream;

.field private final preferences:Landroid/content/SharedPreferences;

.field private relativeFilePath:Ljava/lang/String;

.field private final streamsSelector:Lcom/google/android/videos/streams/LegacyStreamsSelector;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-string v0, "^bytes (\\d+)-(\\d+)/(\\d+)$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/videos/pinning/LegacyDownloader;->CONTENT_RANGE_HEADER:Ljava/util/regex/Pattern;

    return-void
.end method

.method constructor <init>(Lcom/google/android/videos/VideosGlobals;Lcom/google/android/videos/pinning/Downloader$ProgressListener;Lcom/google/android/videos/pinning/DownloadKey;Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/io/File;Lcom/google/android/videos/async/TaskStatus;Z)V
    .locals 1
    .param p1, "videosGlobals"    # Lcom/google/android/videos/VideosGlobals;
    .param p2, "progressListener"    # Lcom/google/android/videos/pinning/Downloader$ProgressListener;
    .param p3, "key"    # Lcom/google/android/videos/pinning/DownloadKey;
    .param p4, "details"    # Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;
    .param p5, "rootFilesDir"    # Ljava/io/File;
    .param p6, "taskStatus"    # Lcom/google/android/videos/async/TaskStatus;
    .param p7, "debug"    # Z

    .prologue
    .line 69
    invoke-direct/range {p0 .. p7}, Lcom/google/android/videos/pinning/Downloader;-><init>(Lcom/google/android/videos/VideosGlobals;Lcom/google/android/videos/pinning/Downloader$ProgressListener;Lcom/google/android/videos/pinning/DownloadKey;Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/io/File;Lcom/google/android/videos/async/TaskStatus;Z)V

    .line 70
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pinning/LegacyDownloader;->context:Landroid/content/Context;

    .line 71
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getLegacyStreamsSelector()Lcom/google/android/videos/streams/LegacyStreamsSelector;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pinning/LegacyDownloader;->streamsSelector:Lcom/google/android/videos/streams/LegacyStreamsSelector;

    .line 72
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getDrmManager()Lcom/google/android/videos/drm/DrmManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pinning/LegacyDownloader;->drmManager:Lcom/google/android/videos/drm/DrmManager;

    .line 73
    invoke-virtual {p1}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pinning/LegacyDownloader;->preferences:Landroid/content/SharedPreferences;

    .line 74
    return-void
.end method

.method private acquireLicense(Ljava/lang/String;Lcom/google/android/videos/streams/MediaStream;Ljava/lang/String;)V
    .locals 15
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "stream"    # Lcom/google/android/videos/streams/MediaStream;
    .param p3, "relativeFilePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/drm/DrmFallbackException;,
            Lcom/google/android/videos/pinning/PinningTask$PinningException;
        }
    .end annotation

    .prologue
    .line 149
    iget-object v2, p0, Lcom/google/android/videos/pinning/LegacyDownloader;->key:Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v2, v2, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v1, v2}, Lcom/google/android/videos/drm/DrmRequest;->createPinRequest(Ljava/lang/String;Lcom/google/android/videos/streams/MediaStream;Ljava/lang/String;)Lcom/google/android/videos/drm/DrmRequest;

    move-result-object v12

    .line 151
    .local v12, "licenseRequest":Lcom/google/android/videos/drm/DrmRequest;
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v11

    .line 152
    .local v11, "licenseCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/drm/DrmResponse;>;"
    iget-object v2, p0, Lcom/google/android/videos/pinning/LegacyDownloader;->drmManager:Lcom/google/android/videos/drm/DrmManager;

    invoke-virtual {v2, v12, v11}, Lcom/google/android/videos/drm/DrmManager;->request(Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/async/Callback;)V

    .line 154
    :try_start_0
    invoke-virtual {v11}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/videos/drm/DrmResponse;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 172
    .local v13, "licenseResponse":Lcom/google/android/videos/drm/DrmResponse;
    new-instance v10, Lcom/google/android/videos/proto/DownloadExtra;

    invoke-direct {v10}, Lcom/google/android/videos/proto/DownloadExtra;-><init>()V

    .line 173
    .local v10, "extra":Lcom/google/android/videos/proto/DownloadExtra;
    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/videos/proto/StreamInfo;

    const/4 v3, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    aput-object v5, v2, v3

    iput-object v2, v10, Lcom/google/android/videos/proto/DownloadExtra;->streamInfos:[Lcom/google/android/videos/proto/StreamInfo;

    .line 176
    invoke-static {}, Lcom/google/android/videos/pinning/PinningDbHelper;->getClearedLicenseContentValues()Landroid/content/ContentValues;

    move-result-object v14

    .line 177
    .local v14, "values":Landroid/content/ContentValues;
    const-string v2, "download_relative_filepath"

    move-object/from16 v0, p3

    invoke-virtual {v14, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    const-string v2, "license_type"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v14, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 179
    const-string v2, "license_last_synced_timestamp"

    iget-wide v6, v13, Lcom/google/android/videos/drm/DrmResponse;->timestamp:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v14, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 180
    const-string v2, "license_force_sync"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v14, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 181
    const-string v2, "license_file_path_key"

    new-instance v3, Ljava/io/File;

    iget-object v5, p0, Lcom/google/android/videos/pinning/LegacyDownloader;->rootFilesDir:Ljava/io/File;

    move-object/from16 v0, p3

    invoke-direct {v3, v5, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v14, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    const-string v2, "license_key_id"

    iget-object v3, v13, Lcom/google/android/videos/drm/DrmResponse;->ids:Lcom/google/android/videos/drm/DrmManager$Identifiers;

    iget-wide v6, v3, Lcom/google/android/videos/drm/DrmManager$Identifiers;->keyId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v14, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 184
    const-string v2, "license_asset_id"

    iget-object v3, v13, Lcom/google/android/videos/drm/DrmResponse;->ids:Lcom/google/android/videos/drm/DrmManager$Identifiers;

    iget-wide v6, v3, Lcom/google/android/videos/drm/DrmManager$Identifiers;->assetId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v14, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 185
    const-string v2, "license_system_id"

    iget-object v3, v13, Lcom/google/android/videos/drm/DrmResponse;->ids:Lcom/google/android/videos/drm/DrmManager$Identifiers;

    iget-wide v6, v3, Lcom/google/android/videos/drm/DrmManager$Identifiers;->systemId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v14, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 186
    const-string v2, "download_extra_proto"

    invoke-static {v10}, Lcom/google/android/videos/proto/DownloadExtra;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v3

    invoke-virtual {v14, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 187
    iget-object v2, p0, Lcom/google/android/videos/pinning/LegacyDownloader;->database:Lcom/google/android/videos/store/Database;

    iget-object v3, p0, Lcom/google/android/videos/pinning/LegacyDownloader;->key:Lcom/google/android/videos/pinning/DownloadKey;

    invoke-static {v2, v3, v14}, Lcom/google/android/videos/pinning/PinningDbHelper;->updatePinningStateForVideo(Lcom/google/android/videos/store/Database;Lcom/google/android/videos/pinning/DownloadKey;Landroid/content/ContentValues;)V

    .line 188
    invoke-virtual {p0}, Lcom/google/android/videos/pinning/LegacyDownloader;->notifyProgress()V

    .line 189
    return-void

    .line 155
    .end local v10    # "extra":Lcom/google/android/videos/proto/DownloadExtra;
    .end local v13    # "licenseResponse":Lcom/google/android/videos/drm/DrmResponse;
    .end local v14    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v9

    .line 156
    .local v9, "e":Ljava/util/concurrent/ExecutionException;
    invoke-virtual {v9}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v8

    .line 157
    .local v8, "cause":Ljava/lang/Throwable;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "failed to get offline rights "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 158
    instance-of v2, v8, Lcom/google/android/videos/drm/DrmFallbackException;

    if-eqz v2, :cond_0

    .line 159
    check-cast v8, Lcom/google/android/videos/drm/DrmFallbackException;

    .end local v8    # "cause":Ljava/lang/Throwable;
    throw v8

    .line 160
    .restart local v8    # "cause":Ljava/lang/Throwable;
    :cond_0
    instance-of v2, v8, Lcom/google/android/videos/drm/DrmException;

    if-eqz v2, :cond_2

    move-object v4, v8

    .line 161
    check-cast v4, Lcom/google/android/videos/drm/DrmException;

    .line 162
    .local v4, "drmException":Lcom/google/android/videos/drm/DrmException;
    new-instance v2, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    const-string v3, "could not acquire license"

    iget-object v5, v4, Lcom/google/android/videos/drm/DrmException;->drmError:Lcom/google/android/videos/drm/DrmException$DrmError;

    sget-object v6, Lcom/google/android/videos/drm/DrmException$DrmError;->NETWORK_FAILURE:Lcom/google/android/videos/drm/DrmException$DrmError;

    if-eq v5, v6, :cond_1

    const/4 v5, 0x1

    :goto_0
    invoke-direct {p0, v4}, Lcom/google/android/videos/pinning/LegacyDownloader;->parseErrorFromDrmException(Lcom/google/android/videos/drm/DrmException;)I

    move-result v6

    iget v7, v4, Lcom/google/android/videos/drm/DrmException;->errorCode:I

    invoke-direct/range {v2 .. v7}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;ZII)V

    throw v2

    :cond_1
    const/4 v5, 0x0

    goto :goto_0

    .line 167
    .end local v4    # "drmException":Lcom/google/android/videos/drm/DrmException;
    :cond_2
    new-instance v2, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    const-string v3, "could not cannot acquire license"

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-direct {v2, v3, v5, v6}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    throw v2
.end method

.method private getContentLength(Ljava/net/HttpURLConnection;)J
    .locals 12
    .param p1, "connection"    # Ljava/net/HttpURLConnection;

    .prologue
    .line 480
    const-wide/16 v0, -0x1

    .line 482
    .local v0, "contentLength":J
    const-string v7, "Content-Length"

    invoke-virtual {p1, v7}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 483
    .local v4, "contentLengthHeader":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 485
    :try_start_0
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 491
    :cond_0
    :goto_0
    const-string v7, "Content-Range"

    invoke-virtual {p1, v7}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 492
    .local v5, "contentRangeHeader":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 493
    sget-object v7, Lcom/google/android/videos/pinning/LegacyDownloader;->CONTENT_RANGE_HEADER:Ljava/util/regex/Pattern;

    invoke-virtual {v7, v5}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    .line 494
    .local v6, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v6}, Ljava/util/regex/Matcher;->find()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 496
    const/4 v7, 0x2

    :try_start_1
    invoke-virtual {v6, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    sub-long/2addr v8, v10

    const-wide/16 v10, 0x1

    add-long v2, v8, v10

    .line 498
    .local v2, "contentLengthFromRange":J
    const-wide/16 v8, 0x0

    cmp-long v7, v0, v8

    if-gez v7, :cond_2

    .line 501
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Using contentLength parsed from Content-Range "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 502
    move-wide v0, v2

    .line 517
    .end local v2    # "contentLengthFromRange":J
    .end local v6    # "matcher":Ljava/util/regex/Matcher;
    :cond_1
    :goto_1
    return-wide v0

    .line 503
    .restart local v2    # "contentLengthFromRange":J
    .restart local v6    # "matcher":Ljava/util/regex/Matcher;
    :cond_2
    cmp-long v7, v0, v2

    if-eqz v7, :cond_1

    .line 509
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v0

    goto :goto_1

    .line 486
    .end local v2    # "contentLengthFromRange":J
    .end local v5    # "contentRangeHeader":Ljava/lang/String;
    .end local v6    # "matcher":Ljava/util/regex/Matcher;
    :catch_0
    move-exception v7

    goto :goto_0

    .line 511
    .restart local v5    # "contentRangeHeader":Ljava/lang/String;
    .restart local v6    # "matcher":Ljava/util/regex/Matcher;
    :catch_1
    move-exception v7

    goto :goto_1
.end method

.method private is2xxStatusCode(I)Z
    .locals 1
    .param p1, "code"    # I

    .prologue
    .line 530
    const/16 v0, 0xc8

    if-lt p1, v0, :cond_0

    const/16 v0, 0x12c

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isContentTypeOk(Ljava/lang/String;)Z
    .locals 2
    .param p1, "contentType"    # Ljava/lang/String;

    .prologue
    .line 464
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/google/android/videos/utils/Util;->toLowerInvariant(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "text"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isLastModifiedValid(JJ)Z
    .locals 5
    .param p1, "persistedLastModified"    # J
    .param p3, "lastModified"    # J

    .prologue
    const-wide/16 v2, 0x3e8

    .line 536
    div-long v0, p1, v2

    div-long v2, p3, v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private logException(Ljava/lang/String;Ljava/lang/Exception;Z)V
    .locals 3
    .param p1, "networkUri"    # Ljava/lang/String;
    .param p2, "exception"    # Ljava/lang/Exception;
    .param p3, "logAsError"    # Z

    .prologue
    .line 521
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "download error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 522
    .local v0, "message":Ljava/lang/String;
    if-eqz p3, :cond_0

    .line 523
    invoke-static {v0, p2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 527
    :goto_0
    return-void

    .line 525
    :cond_0
    invoke-static {v0, p2}, Lcom/google/android/videos/L;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private makeConnection(Ljava/net/URL;J)Ljava/net/HttpURLConnection;
    .locals 4
    .param p1, "url"    # Ljava/net/URL;
    .param p2, "offset"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v1, 0x7530

    .line 469
    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 470
    .local v0, "connection":Ljava/net/HttpURLConnection;
    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 471
    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 472
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 473
    const-string v1, "Connection"

    const-string v2, "close"

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    const-string v1, "Range"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bytes="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->connect()V

    .line 476
    return-object v0
.end method

.method private parseErrorFromDrmException(Lcom/google/android/videos/drm/DrmException;)I
    .locals 3
    .param p1, "exception"    # Lcom/google/android/videos/drm/DrmException;

    .prologue
    const/4 v0, 0x1

    .line 192
    iget-object v1, p1, Lcom/google/android/videos/drm/DrmException;->drmError:Lcom/google/android/videos/drm/DrmException$DrmError;

    if-eqz v1, :cond_0

    .line 193
    sget-object v1, Lcom/google/android/videos/pinning/LegacyDownloader$1;->$SwitchMap$com$google$android$videos$drm$DrmException$DrmError:[I

    iget-object v2, p1, Lcom/google/android/videos/drm/DrmException;->drmError:Lcom/google/android/videos/drm/DrmException$DrmError;

    invoke-virtual {v2}, Lcom/google/android/videos/drm/DrmException$DrmError;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 224
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 195
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 197
    :pswitch_2
    const/16 v0, 0x18

    goto :goto_0

    .line 199
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 203
    :pswitch_4
    const/4 v0, 0x4

    goto :goto_0

    .line 205
    :pswitch_5
    const/16 v0, 0x13

    goto :goto_0

    .line 207
    :pswitch_6
    const/16 v0, 0x14

    goto :goto_0

    .line 209
    :pswitch_7
    const/16 v0, 0x15

    goto :goto_0

    .line 211
    :pswitch_8
    const/16 v0, 0x16

    goto :goto_0

    .line 213
    :pswitch_9
    const/16 v0, 0x17

    goto :goto_0

    .line 193
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private setupExistingDownload(Lcom/google/android/videos/streams/Streams;)Landroid/util/Pair;
    .locals 13
    .param p1, "streams"    # Lcom/google/android/videos/streams/Streams;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/streams/Streams;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/PinningTask$PinningException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x1

    .line 228
    iget-object v9, p0, Lcom/google/android/videos/pinning/LegacyDownloader;->details:Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;

    iget-object v6, v9, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->relativeFilePath:Ljava/lang/String;

    .line 229
    .local v6, "relativeFilePath":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 231
    new-instance v8, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    const-string v9, "empty relativeFilePath"

    const/16 v10, 0x12

    invoke-direct {v8, v9, v12, v10}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    throw v8

    .line 235
    :cond_0
    iget-object v9, p0, Lcom/google/android/videos/pinning/LegacyDownloader;->details:Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;

    iget-object v9, v9, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->extra:Lcom/google/android/videos/proto/DownloadExtra;

    iget-object v9, v9, Lcom/google/android/videos/proto/DownloadExtra;->streamInfos:[Lcom/google/android/videos/proto/StreamInfo;

    aget-object v0, v9, v11

    .line 236
    .local v0, "existingStreamInfo":Lcom/google/android/videos/proto/StreamInfo;
    iget v2, v0, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    .line 237
    .local v2, "itag":I
    const/4 v3, 0x0

    .line 238
    .local v3, "licensedStream":Lcom/google/android/videos/streams/MediaStream;
    const-wide/16 v4, 0x0

    .line 239
    .local v4, "licensedStreamLastModified":J
    iget-object v9, p1, Lcom/google/android/videos/streams/Streams;->mediaStreams:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/videos/streams/MediaStream;

    .line 240
    .local v7, "stream":Lcom/google/android/videos/streams/MediaStream;
    iget-object v9, v7, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget v9, v9, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    if-ne v9, v2, :cond_1

    iget-object v9, v0, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    iget-object v10, v7, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-object v10, v10, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    invoke-static {v9, v10, v11}, Lcom/google/android/videos/utils/AudioInfoUtil;->areEqual(Lcom/google/wireless/android/video/magma/proto/AudioInfo;Lcom/google/wireless/android/video/magma/proto/AudioInfo;Z)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 242
    move-object v3, v7

    .line 243
    iget-object v9, v7, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-wide v4, v9, Lcom/google/android/videos/proto/StreamInfo;->lastModifiedTimestamp:J

    .line 248
    .end local v7    # "stream":Lcom/google/android/videos/streams/MediaStream;
    :cond_2
    if-nez v3, :cond_3

    .line 250
    new-instance v8, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "licensed format is no longer permitted: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x6

    invoke-direct {v8, v9, v12, v10}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    throw v8

    .line 254
    :cond_3
    iget-wide v10, v0, Lcom/google/android/videos/proto/StreamInfo;->lastModifiedTimestamp:J

    invoke-direct {p0, v10, v11, v4, v5}, Lcom/google/android/videos/pinning/LegacyDownloader;->isLastModifiedValid(JJ)Z

    move-result v9

    if-nez v9, :cond_4

    .line 256
    new-instance v8, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    const-string v9, "Stream modified since the downloaded started"

    const/16 v10, 0xe

    invoke-direct {v8, v9, v12, v10}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    throw v8

    .line 260
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/videos/pinning/LegacyDownloader;->isCanceled()Z

    move-result v9

    if-eqz v9, :cond_6

    .line 269
    :cond_5
    :goto_0
    return-object v8

    .line 264
    :cond_6
    iget-object v9, v3, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-wide v10, v9, Lcom/google/android/videos/proto/StreamInfo;->sizeInBytes:J

    invoke-virtual {p0, v10, v11, v6}, Lcom/google/android/videos/pinning/LegacyDownloader;->checkSufficientFreeSpace(JLjava/lang/String;)V

    .line 265
    invoke-virtual {p0}, Lcom/google/android/videos/pinning/LegacyDownloader;->isCanceled()Z

    move-result v9

    if-nez v9, :cond_5

    .line 269
    invoke-static {v3, v6}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v8

    goto :goto_0
.end method

.method private setupNewDownload(Lcom/google/android/videos/streams/Streams;I)Landroid/util/Pair;
    .locals 16
    .param p1, "streams"    # Lcom/google/android/videos/streams/Streams;
    .param p2, "drmLevel"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/streams/Streams;",
            "I)",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/PinningTask$PinningException;
        }
    .end annotation

    .prologue
    .line 100
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/pinning/LegacyDownloader;->key:Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v13, v12, Lcom/google/android/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/pinning/LegacyDownloader;->key:Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v14, v12, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    const/4 v12, 0x1

    move/from16 v0, p2

    if-ne v0, v12, :cond_1

    const/4 v12, 0x1

    :goto_0
    invoke-static {v13, v14, v12}, Lcom/google/android/videos/utils/OfflineUtil;->getFilePathForLegacyDownload(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v10

    .line 103
    .local v10, "relativeFilePath":Ljava/lang/String;
    new-instance v7, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/pinning/LegacyDownloader;->rootFilesDir:Ljava/io/File;

    invoke-direct {v7, v12, v10}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 104
    .local v7, "file":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 105
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    .line 107
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/pinning/LegacyDownloader;->isCanceled()Z

    move-result v12

    if-eqz v12, :cond_2

    .line 108
    const/4 v12, 0x0

    .line 143
    :goto_1
    return-object v12

    .line 100
    .end local v7    # "file":Ljava/io/File;
    .end local v10    # "relativeFilePath":Ljava/lang/String;
    :cond_1
    const/4 v12, 0x0

    goto :goto_0

    .line 112
    .restart local v7    # "file":Ljava/io/File;
    .restart local v10    # "relativeFilePath":Ljava/lang/String;
    :cond_2
    :try_start_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/pinning/LegacyDownloader;->streamsSelector:Lcom/google/android/videos/streams/LegacyStreamsSelector;

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/google/android/videos/streams/Streams;->mediaStreams:Ljava/util/List;

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/videos/pinning/LegacyDownloader;->surroundSound:Z

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/pinning/LegacyDownloader;->details:Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;

    iget v15, v15, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->quality:I

    move/from16 v0, p2

    invoke-virtual {v12, v13, v14, v0, v15}, Lcom/google/android/videos/streams/LegacyStreamsSelector;->getDownloadDrmStreams(Ljava/util/List;ZII)Ljava/util/List;

    move-result-object v5

    .line 114
    .local v5, "downloadStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/pinning/LegacyDownloader;->context:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/videos/pinning/LegacyDownloader;->preferences:Landroid/content/SharedPreferences;

    invoke-static {v5, v12, v13}, Lcom/google/android/videos/utils/AudioInfoUtil;->getPreferredStreamIndex(Ljava/util/List;Landroid/content/Context;Landroid/content/SharedPreferences;)I

    move-result v4

    .line 116
    .local v4, "downloadStreamIndex":I
    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/streams/MediaStream;

    .line 118
    .local v3, "downloadStream":Lcom/google/android/videos/streams/MediaStream;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/pinning/LegacyDownloader;->isCanceled()Z

    move-result v12

    if-eqz v12, :cond_3

    .line 119
    const/4 v12, 0x0

    goto :goto_1

    .line 122
    :cond_3
    iget-object v12, v3, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-wide v12, v12, Lcom/google/android/videos/proto/StreamInfo;->sizeInBytes:J

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13}, Lcom/google/android/videos/pinning/LegacyDownloader;->persistDownloadSize(J)V

    .line 123
    iget-object v12, v3, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-wide v12, v12, Lcom/google/android/videos/proto/StreamInfo;->sizeInBytes:J

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13, v10}, Lcom/google/android/videos/pinning/LegacyDownloader;->checkSufficientFreeSpace(JLjava/lang/String;)V

    .line 124
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/pinning/LegacyDownloader;->isCanceled()Z

    move-result v12

    if-eqz v12, :cond_4

    .line 125
    const/4 v12, 0x0

    goto :goto_1

    .line 127
    :cond_4
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/pinning/LegacyDownloader;->key:Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v12, v12, Lcom/google/android/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v3, v10}, Lcom/google/android/videos/pinning/LegacyDownloader;->acquireLicense(Ljava/lang/String;Lcom/google/android/videos/streams/MediaStream;Ljava/lang/String;)V

    .line 128
    invoke-static {v3, v10}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    :try_end_0
    .catch Lcom/google/android/videos/streams/MissingStreamException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/videos/drm/DrmFallbackException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v12

    goto :goto_1

    .line 129
    .end local v3    # "downloadStream":Lcom/google/android/videos/streams/MediaStream;
    .end local v4    # "downloadStreamIndex":I
    .end local v5    # "downloadStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    :catch_0
    move-exception v6

    .line 130
    .local v6, "e":Lcom/google/android/videos/streams/MissingStreamException;
    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/google/android/videos/streams/Streams;->mediaStreams:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v11

    .line 131
    .local v11, "streamCount":I
    new-array v2, v11, [I

    .line 132
    .local v2, "candidates":[I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_2
    if-ge v8, v11, :cond_5

    .line 133
    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/google/android/videos/streams/Streams;->mediaStreams:Ljava/util/List;

    invoke-interface {v12, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/videos/streams/MediaStream;

    iget-object v12, v12, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget v12, v12, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    aput v12, v2, v8

    .line 132
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 135
    :cond_5
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "failed to select a download stream: drm="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", quality="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/videos/pinning/LegacyDownloader;->details:Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;

    iget v13, v13, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->quality:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", candidates="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v2}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 137
    .local v9, "message":Ljava/lang/String;
    new-instance v12, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    const/4 v13, 0x1

    const/4 v14, 0x6

    invoke-direct {v12, v9, v6, v13, v14}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;ZI)V

    throw v12

    .line 139
    .end local v2    # "candidates":[I
    .end local v6    # "e":Lcom/google/android/videos/streams/MissingStreamException;
    .end local v8    # "i":I
    .end local v9    # "message":Ljava/lang/String;
    .end local v11    # "streamCount":I
    :catch_1
    move-exception v6

    .line 140
    .local v6, "e":Lcom/google/android/videos/drm/DrmFallbackException;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/pinning/LegacyDownloader;->isCanceled()Z

    move-result v12

    if-eqz v12, :cond_6

    .line 141
    const/4 v12, 0x0

    goto/16 :goto_1

    .line 143
    :cond_6
    iget v12, v6, Lcom/google/android/videos/drm/DrmFallbackException;->fallbackDrmLevel:I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v12}, Lcom/google/android/videos/pinning/LegacyDownloader;->setupNewDownload(Lcom/google/android/videos/streams/Streams;I)Landroid/util/Pair;

    move-result-object v12

    goto/16 :goto_1
.end method


# virtual methods
.method public downloadMedia()V
    .locals 38
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/PinningTask$PinningException;
        }
    .end annotation

    .prologue
    .line 275
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/pinning/LegacyDownloader;->relativeFilePath:Ljava/lang/String;

    const-string v10, "download starting"

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v10}, Lcom/google/android/videos/pinning/LegacyDownloader;->debug(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/pinning/LegacyDownloader;->mediaStream:Lcom/google/android/videos/streams/MediaStream;

    iget-object v5, v5, Lcom/google/android/videos/streams/MediaStream;->uri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v22

    .line 278
    .local v22, "networkUri":Ljava/lang/String;
    new-instance v20, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/pinning/LegacyDownloader;->rootFilesDir:Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/videos/pinning/LegacyDownloader;->relativeFilePath:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-direct {v0, v5, v10}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 279
    .local v20, "file":Ljava/io/File;
    const/4 v12, 0x0

    .line 280
    .local v12, "connection":Ljava/net/HttpURLConnection;
    const/16 v21, 0x0

    .line 281
    .local v21, "is":Ljava/io/InputStream;
    const/16 v25, 0x0

    .line 282
    .local v25, "raf":Ljava/io/RandomAccessFile;
    const/16 v19, 0x0

    .line 288
    .local v19, "fc":Ljava/nio/channels/FileChannel;
    :try_start_0
    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    .line 289
    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    .line 290
    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->createNewFile()Z

    .line 292
    :cond_0
    new-instance v26, Ljava/io/RandomAccessFile;

    const-string v5, "rw"

    move-object/from16 v0, v26

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 293
    .end local v25    # "raf":Ljava/io/RandomAccessFile;
    .local v26, "raf":Ljava/io/RandomAccessFile;
    :try_start_1
    invoke-virtual/range {v26 .. v26}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v6

    .line 294
    .local v6, "downloaded":J
    move-object/from16 v0, v26

    invoke-virtual {v0, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 295
    invoke-virtual/range {v26 .. v26}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v19

    .line 305
    :try_start_2
    new-instance v32, Ljava/net/URL;

    move-object/from16 v0, v32

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 306
    .local v32, "url":Ljava/net/URL;
    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-direct {v0, v1, v6, v7}, Lcom/google/android/videos/pinning/LegacyDownloader;->makeConnection(Ljava/net/URL;J)Ljava/net/HttpURLConnection;

    move-result-object v12

    .line 307
    invoke-virtual {v12}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v28

    .line 309
    .local v28, "responseCode":I
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/google/android/videos/pinning/LegacyDownloader;->getContentLength(Ljava/net/HttpURLConnection;)J

    move-result-wide v14

    .line 310
    .local v14, "contentLength":J
    invoke-virtual {v12}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v16

    .line 312
    .local v16, "contentType":Ljava/lang/String;
    const/16 v5, 0x1a0

    move/from16 v0, v28

    if-ne v0, v5, :cond_3

    .line 315
    invoke-virtual {v12}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 316
    const-wide/16 v34, 0x1

    sub-long v34, v6, v34

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    move-wide/from16 v2, v34

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/videos/pinning/LegacyDownloader;->makeConnection(Ljava/net/URL;J)Ljava/net/HttpURLConnection;

    move-result-object v12

    .line 317
    invoke-virtual {v12}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v24

    .line 318
    .local v24, "newResponseCode":I
    invoke-virtual {v12}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v23

    .line 319
    .local v23, "newContentType":Ljava/lang/String;
    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/google/android/videos/pinning/LegacyDownloader;->is2xxStatusCode(I)Z

    move-result v5

    if-eqz v5, :cond_3

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/google/android/videos/pinning/LegacyDownloader;->isContentTypeOk(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 321
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/pinning/LegacyDownloader;->relativeFilePath:Ljava/lang/String;

    const-string v10, "download already completed"

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v10}, Lcom/google/android/videos/pinning/LegacyDownloader;->debug(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7}, Lcom/google/android/videos/pinning/LegacyDownloader;->persistDownloadSize(J)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 453
    const/4 v5, 0x1

    move-object/from16 v0, v19

    invoke-static {v0, v5}, Lcom/google/android/videos/utils/Util;->closeQuietly(Ljava/nio/channels/FileChannel;Z)V

    .line 454
    if-eqz v12, :cond_1

    .line 455
    invoke-static {v12}, Lcom/google/android/videos/utils/Util;->terminateInputStream(Ljava/net/HttpURLConnection;)V

    .line 456
    invoke-static/range {v21 .. v21}, Lcom/google/android/videos/utils/Util;->closeQuietly(Ljava/io/Closeable;)V

    .line 457
    invoke-virtual {v12}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 459
    :cond_1
    invoke-static/range {v26 .. v26}, Lcom/google/android/videos/utils/Util;->closeQuietly(Ljava/io/Closeable;)V

    .line 461
    .end local v23    # "newContentType":Ljava/lang/String;
    .end local v24    # "newResponseCode":I
    :goto_0
    return-void

    .line 296
    .end local v6    # "downloaded":J
    .end local v14    # "contentLength":J
    .end local v16    # "contentType":Ljava/lang/String;
    .end local v26    # "raf":Ljava/io/RandomAccessFile;
    .end local v28    # "responseCode":I
    .end local v32    # "url":Ljava/net/URL;
    .restart local v25    # "raf":Ljava/io/RandomAccessFile;
    :catch_0
    move-exception v17

    .line 297
    .local v17, "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    new-instance v18, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    const-string v5, "I/O exception while seeking within video"

    const/4 v10, 0x1

    const/16 v34, 0xe

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    move/from16 v2, v34

    invoke-direct {v0, v5, v1, v10, v2}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;ZI)V

    .line 300
    .local v18, "exception":Lcom/google/android/videos/pinning/PinningTask$PinningException;
    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2, v5}, Lcom/google/android/videos/pinning/LegacyDownloader;->logException(Ljava/lang/String;Ljava/lang/Exception;Z)V

    .line 301
    throw v18
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 449
    .end local v17    # "e":Ljava/io/IOException;
    .end local v18    # "exception":Lcom/google/android/videos/pinning/PinningTask$PinningException;
    :catch_1
    move-exception v17

    .line 450
    .local v17, "e":Ljava/lang/RuntimeException;
    :goto_2
    const/4 v5, 0x1

    :try_start_4
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2, v5}, Lcom/google/android/videos/pinning/LegacyDownloader;->logException(Ljava/lang/String;Ljava/lang/Exception;Z)V

    .line 451
    throw v17
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 453
    .end local v17    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v5

    :goto_3
    const/4 v10, 0x1

    move-object/from16 v0, v19

    invoke-static {v0, v10}, Lcom/google/android/videos/utils/Util;->closeQuietly(Ljava/nio/channels/FileChannel;Z)V

    .line 454
    if-eqz v12, :cond_2

    .line 455
    invoke-static {v12}, Lcom/google/android/videos/utils/Util;->terminateInputStream(Ljava/net/HttpURLConnection;)V

    .line 456
    invoke-static/range {v21 .. v21}, Lcom/google/android/videos/utils/Util;->closeQuietly(Ljava/io/Closeable;)V

    .line 457
    invoke-virtual {v12}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 459
    :cond_2
    invoke-static/range {v25 .. v25}, Lcom/google/android/videos/utils/Util;->closeQuietly(Ljava/io/Closeable;)V

    throw v5

    .line 327
    .end local v25    # "raf":Ljava/io/RandomAccessFile;
    .restart local v6    # "downloaded":J
    .restart local v14    # "contentLength":J
    .restart local v16    # "contentType":Ljava/lang/String;
    .restart local v26    # "raf":Ljava/io/RandomAccessFile;
    .restart local v28    # "responseCode":I
    .restart local v32    # "url":Ljava/net/URL;
    :cond_3
    :try_start_5
    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/google/android/videos/pinning/LegacyDownloader;->is2xxStatusCode(I)Z

    move-result v5

    if-nez v5, :cond_4

    .line 328
    new-instance v18, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "http status "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v10, 0x0

    const/16 v34, 0xe

    move-object/from16 v0, v18

    move/from16 v1, v34

    invoke-direct {v0, v5, v10, v1}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    .line 331
    .restart local v18    # "exception":Lcom/google/android/videos/pinning/PinningTask$PinningException;
    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2, v5}, Lcom/google/android/videos/pinning/LegacyDownloader;->logException(Ljava/lang/String;Ljava/lang/Exception;Z)V

    .line 332
    throw v18
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 365
    .end local v14    # "contentLength":J
    .end local v16    # "contentType":Ljava/lang/String;
    .end local v18    # "exception":Lcom/google/android/videos/pinning/PinningTask$PinningException;
    .end local v28    # "responseCode":I
    .end local v32    # "url":Ljava/net/URL;
    :catch_2
    move-exception v17

    .line 366
    .local v17, "e":Ljava/io/IOException;
    :try_start_6
    new-instance v18, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    const-string v5, "I/O exception while requesting video"

    const/4 v10, 0x0

    const/16 v34, 0xe

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    move/from16 v2, v34

    invoke-direct {v0, v5, v1, v10, v2}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;ZI)V

    .line 369
    .restart local v18    # "exception":Lcom/google/android/videos/pinning/PinningTask$PinningException;
    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2, v5}, Lcom/google/android/videos/pinning/LegacyDownloader;->logException(Ljava/lang/String;Ljava/lang/Exception;Z)V

    .line 370
    throw v18
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 449
    .end local v6    # "downloaded":J
    .end local v17    # "e":Ljava/io/IOException;
    .end local v18    # "exception":Lcom/google/android/videos/pinning/PinningTask$PinningException;
    :catch_3
    move-exception v17

    move-object/from16 v25, v26

    .end local v26    # "raf":Ljava/io/RandomAccessFile;
    .restart local v25    # "raf":Ljava/io/RandomAccessFile;
    goto :goto_2

    .line 333
    .end local v25    # "raf":Ljava/io/RandomAccessFile;
    .restart local v6    # "downloaded":J
    .restart local v14    # "contentLength":J
    .restart local v16    # "contentType":Ljava/lang/String;
    .restart local v26    # "raf":Ljava/io/RandomAccessFile;
    .restart local v28    # "responseCode":I
    .restart local v32    # "url":Ljava/net/URL;
    :cond_4
    const-wide/16 v34, 0x1

    cmp-long v5, v14, v34

    if-gez v5, :cond_5

    .line 334
    :try_start_7
    new-instance v18, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "content length "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v10, 0x1

    const/16 v34, 0xe

    move-object/from16 v0, v18

    move/from16 v1, v34

    invoke-direct {v0, v5, v10, v1}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    .line 337
    .restart local v18    # "exception":Lcom/google/android/videos/pinning/PinningTask$PinningException;
    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2, v5}, Lcom/google/android/videos/pinning/LegacyDownloader;->logException(Ljava/lang/String;Ljava/lang/Exception;Z)V

    .line 338
    throw v18

    .line 453
    .end local v6    # "downloaded":J
    .end local v14    # "contentLength":J
    .end local v16    # "contentType":Ljava/lang/String;
    .end local v18    # "exception":Lcom/google/android/videos/pinning/PinningTask$PinningException;
    .end local v28    # "responseCode":I
    .end local v32    # "url":Ljava/net/URL;
    :catchall_1
    move-exception v5

    move-object/from16 v25, v26

    .end local v26    # "raf":Ljava/io/RandomAccessFile;
    .restart local v25    # "raf":Ljava/io/RandomAccessFile;
    goto/16 :goto_3

    .line 341
    .end local v25    # "raf":Ljava/io/RandomAccessFile;
    .restart local v6    # "downloaded":J
    .restart local v14    # "contentLength":J
    .restart local v16    # "contentType":Ljava/lang/String;
    .restart local v26    # "raf":Ljava/io/RandomAccessFile;
    .restart local v28    # "responseCode":I
    .restart local v32    # "url":Ljava/net/URL;
    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/google/android/videos/pinning/LegacyDownloader;->isContentTypeOk(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 342
    new-instance v18, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "bad content type "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v12}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v10, 0x0

    const/16 v34, 0xe

    move-object/from16 v0, v18

    move/from16 v1, v34

    invoke-direct {v0, v5, v10, v1}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    .line 345
    .restart local v18    # "exception":Lcom/google/android/videos/pinning/PinningTask$PinningException;
    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2, v5}, Lcom/google/android/videos/pinning/LegacyDownloader;->logException(Ljava/lang/String;Ljava/lang/Exception;Z)V

    .line 346
    throw v18

    .line 349
    .end local v18    # "exception":Lcom/google/android/videos/pinning/PinningTask$PinningException;
    :cond_6
    const-wide/16 v34, 0x0

    cmp-long v5, v6, v34

    if-lez v5, :cond_8

    .line 351
    const-string v5, "Content-Range"

    invoke-virtual {v12, v5}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 352
    .local v13, "contentRangeHeader":Ljava/lang/String;
    if-eqz v13, :cond_7

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, "-"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v13, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 353
    :cond_7
    new-instance v18, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Content-Range "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, ", not "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v10, 0x0

    const/16 v34, 0xe

    move-object/from16 v0, v18

    move/from16 v1, v34

    invoke-direct {v0, v5, v10, v1}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    .line 356
    .restart local v18    # "exception":Lcom/google/android/videos/pinning/PinningTask$PinningException;
    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2, v5}, Lcom/google/android/videos/pinning/LegacyDownloader;->logException(Ljava/lang/String;Ljava/lang/Exception;Z)V

    .line 357
    throw v18

    .line 361
    .end local v13    # "contentRangeHeader":Ljava/lang/String;
    .end local v18    # "exception":Lcom/google/android/videos/pinning/PinningTask$PinningException;
    :cond_8
    add-long v30, v6, v14

    .line 362
    .local v30, "size":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/pinning/LegacyDownloader;->persistDownloadSize(J)V

    .line 364
    invoke-virtual {v12}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-result-object v21

    .line 373
    const-wide/16 v34, 0x64

    :try_start_8
    div-long v34, v30, v34

    const-wide/32 v36, 0x200000

    invoke-static/range {v34 .. v37}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    .line 375
    .local v8, "progressGranularity":J
    const/high16 v5, 0x20000

    new-array v4, v5, [B

    .line 376
    .local v4, "byteArray":[B
    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v11

    .line 377
    .local v11, "byteBuffer":Ljava/nio/ByteBuffer;
    const/16 v27, 0x0

    .line 378
    .local v27, "read":I
    :goto_4
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/pinning/LegacyDownloader;->isCanceled()Z
    :try_end_8
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-result v5

    if-nez v5, :cond_9

    const/4 v5, -0x1

    move/from16 v0, v27

    if-eq v0, v5, :cond_9

    .line 381
    const/4 v5, 0x0

    const/high16 v10, 0x20000

    :try_start_9
    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v5, v10}, Ljava/io/InputStream;->read([BII)I
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move-result v27

    .line 390
    :try_start_a
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/pinning/LegacyDownloader;->isCanceled()Z
    :try_end_a
    .catch Ljava/lang/RuntimeException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    move-result v5

    if-eqz v5, :cond_b

    .line 427
    :cond_9
    const/4 v5, 0x1

    :try_start_b
    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/nio/channels/FileChannel;->force(Z)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_6
    .catch Ljava/lang/RuntimeException; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 436
    :try_start_c
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/pinning/LegacyDownloader;->isCanceled()Z

    move-result v5

    if-eqz v5, :cond_11

    .line 437
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/pinning/LegacyDownloader;->relativeFilePath:Ljava/lang/String;

    const-string v10, "download canceled"

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v10}, Lcom/google/android/videos/pinning/LegacyDownloader;->debug(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_c
    .catch Ljava/lang/RuntimeException; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 453
    :goto_5
    const/4 v5, 0x1

    move-object/from16 v0, v19

    invoke-static {v0, v5}, Lcom/google/android/videos/utils/Util;->closeQuietly(Ljava/nio/channels/FileChannel;Z)V

    .line 454
    if-eqz v12, :cond_a

    .line 455
    invoke-static {v12}, Lcom/google/android/videos/utils/Util;->terminateInputStream(Ljava/net/HttpURLConnection;)V

    .line 456
    invoke-static/range {v21 .. v21}, Lcom/google/android/videos/utils/Util;->closeQuietly(Ljava/io/Closeable;)V

    .line 457
    invoke-virtual {v12}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 459
    :cond_a
    invoke-static/range {v26 .. v26}, Lcom/google/android/videos/utils/Util;->closeQuietly(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 382
    :catch_4
    move-exception v17

    .line 383
    .restart local v17    # "e":Ljava/io/IOException;
    :try_start_d
    new-instance v18, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    const-string v5, "I/O exception while downloading video"

    const/4 v10, 0x0

    const/16 v34, 0xe

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    move/from16 v2, v34

    invoke-direct {v0, v5, v1, v10, v2}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;ZI)V

    .line 386
    .restart local v18    # "exception":Lcom/google/android/videos/pinning/PinningTask$PinningException;
    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2, v5}, Lcom/google/android/videos/pinning/LegacyDownloader;->logException(Ljava/lang/String;Ljava/lang/Exception;Z)V

    .line 387
    throw v18
    :try_end_d
    .catch Ljava/lang/RuntimeException; {:try_start_d .. :try_end_d} :catch_3
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 396
    .end local v17    # "e":Ljava/io/IOException;
    .end local v18    # "exception":Lcom/google/android/videos/pinning/PinningTask$PinningException;
    :cond_b
    if-lez v27, :cond_f

    .line 397
    const/4 v5, 0x0

    :try_start_e
    invoke-virtual {v11, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 398
    move/from16 v0, v27

    invoke-virtual {v11, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 399
    const/16 v33, 0x0

    .line 400
    .local v33, "wrote":I
    :goto_6
    move/from16 v0, v33

    move/from16 v1, v27

    if-ge v0, v1, :cond_d

    .line 401
    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    move-result v29

    .line 402
    .local v29, "thisWrote":I
    const/4 v5, 0x1

    move/from16 v0, v29

    if-ge v0, v5, :cond_c

    .line 403
    new-instance v5, Ljava/io/IOException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "Failed to write any data to "

    move-object/from16 v0, v34

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v5, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_5
    .catch Ljava/lang/RuntimeException; {:try_start_e .. :try_end_e} :catch_3
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 413
    .end local v29    # "thisWrote":I
    .end local v33    # "wrote":I
    :catch_5
    move-exception v17

    .line 414
    .restart local v17    # "e":Ljava/io/IOException;
    :try_start_f
    new-instance v18, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    const-string v5, "I/O exception while writing video"

    const/4 v10, 0x0

    const/16 v34, 0xe

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    move/from16 v2, v34

    invoke-direct {v0, v5, v1, v10, v2}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;ZI)V

    .line 417
    .restart local v18    # "exception":Lcom/google/android/videos/pinning/PinningTask$PinningException;
    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2, v5}, Lcom/google/android/videos/pinning/LegacyDownloader;->logException(Ljava/lang/String;Ljava/lang/Exception;Z)V

    .line 418
    throw v18
    :try_end_f
    .catch Ljava/lang/RuntimeException; {:try_start_f .. :try_end_f} :catch_3
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 405
    .end local v17    # "e":Ljava/io/IOException;
    .end local v18    # "exception":Lcom/google/android/videos/pinning/PinningTask$PinningException;
    .restart local v29    # "thisWrote":I
    .restart local v33    # "wrote":I
    :cond_c
    add-int v33, v33, v29

    .line 406
    goto :goto_6

    .line 407
    .end local v29    # "thisWrote":I
    :cond_d
    move/from16 v0, v33

    move/from16 v1, v27

    if-eq v0, v1, :cond_e

    .line 408
    :try_start_10
    new-instance v5, Ljava/lang/IllegalStateException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "wrote "

    move-object/from16 v0, v34

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v33

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v34, " != "

    move-object/from16 v0, v34

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v27

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v5, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_5
    .catch Ljava/lang/RuntimeException; {:try_start_10 .. :try_end_10} :catch_3
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    .line 411
    :cond_e
    move/from16 v0, v27

    int-to-long v0, v0

    move-wide/from16 v34, v0

    add-long v6, v6, v34

    .line 421
    .end local v33    # "wrote":I
    :cond_f
    const/4 v5, -0x1

    move/from16 v0, v27

    if-ne v0, v5, :cond_10

    const/4 v10, 0x1

    :goto_7
    move-object/from16 v5, p0

    :try_start_11
    invoke-virtual/range {v5 .. v10}, Lcom/google/android/videos/pinning/LegacyDownloader;->doProgress(JJZ)V

    goto/16 :goto_4

    :cond_10
    const/4 v10, 0x0

    goto :goto_7

    .line 428
    :catch_6
    move-exception v17

    .line 429
    .restart local v17    # "e":Ljava/io/IOException;
    new-instance v18, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    const-string v5, "I/O exception while writing video"

    const/4 v10, 0x0

    const/16 v34, 0xe

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    move/from16 v2, v34

    invoke-direct {v0, v5, v1, v10, v2}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;ZI)V

    .line 432
    .restart local v18    # "exception":Lcom/google/android/videos/pinning/PinningTask$PinningException;
    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2, v5}, Lcom/google/android/videos/pinning/LegacyDownloader;->logException(Ljava/lang/String;Ljava/lang/Exception;Z)V

    .line 433
    throw v18

    .line 438
    .end local v17    # "e":Ljava/io/IOException;
    .end local v18    # "exception":Lcom/google/android/videos/pinning/PinningTask$PinningException;
    :cond_11
    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->length()J

    move-result-wide v34

    cmp-long v5, v34, v30

    if-eqz v5, :cond_13

    .line 441
    new-instance v18, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "download completed with unexpected size "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->length()J

    move-result-wide v34

    move-wide/from16 v0, v34

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, " expecting "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v30

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->length()J

    move-result-wide v34

    cmp-long v5, v34, v30

    if-lez v5, :cond_12

    const/4 v5, 0x1

    :goto_8
    const/16 v34, 0xe

    move-object/from16 v0, v18

    move/from16 v1, v34

    invoke-direct {v0, v10, v5, v1}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    .line 444
    .restart local v18    # "exception":Lcom/google/android/videos/pinning/PinningTask$PinningException;
    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2, v5}, Lcom/google/android/videos/pinning/LegacyDownloader;->logException(Ljava/lang/String;Ljava/lang/Exception;Z)V

    .line 445
    throw v18

    .line 441
    .end local v18    # "exception":Lcom/google/android/videos/pinning/PinningTask$PinningException;
    :cond_12
    const/4 v5, 0x0

    goto :goto_8

    .line 447
    :cond_13
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/pinning/LegacyDownloader;->relativeFilePath:Ljava/lang/String;

    const-string v10, "download completed"

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v10}, Lcom/google/android/videos/pinning/LegacyDownloader;->debug(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_11
    .catch Ljava/lang/RuntimeException; {:try_start_11 .. :try_end_11} :catch_3
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    goto/16 :goto_5

    .line 296
    .end local v4    # "byteArray":[B
    .end local v6    # "downloaded":J
    .end local v8    # "progressGranularity":J
    .end local v11    # "byteBuffer":Ljava/nio/ByteBuffer;
    .end local v14    # "contentLength":J
    .end local v16    # "contentType":Ljava/lang/String;
    .end local v27    # "read":I
    .end local v28    # "responseCode":I
    .end local v30    # "size":J
    .end local v32    # "url":Ljava/net/URL;
    :catch_7
    move-exception v17

    move-object/from16 v25, v26

    .end local v26    # "raf":Ljava/io/RandomAccessFile;
    .restart local v25    # "raf":Ljava/io/RandomAccessFile;
    goto/16 :goto_1
.end method

.method public setupDownload()Lcom/google/android/videos/pinning/Downloader$SetupResult;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/PinningTask$PinningException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 78
    iget-object v1, p0, Lcom/google/android/videos/pinning/LegacyDownloader;->drmManager:Lcom/google/android/videos/drm/DrmManager;

    invoke-virtual {v1}, Lcom/google/android/videos/drm/DrmManager;->getDrmLevel()I

    move-result v1

    if-gez v1, :cond_0

    .line 79
    new-instance v0, Lcom/google/android/videos/pinning/PinningTask$PinningException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Device unlocked. DrmLevel = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/pinning/LegacyDownloader;->drmManager:Lcom/google/android/videos/drm/DrmManager;

    invoke-virtual {v2}, Lcom/google/android/videos/drm/DrmManager;->getDrmLevel()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    throw v0

    .line 82
    :cond_0
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/videos/pinning/LegacyDownloader;->details:Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;

    iget-boolean v2, v2, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->isEpisode:Z

    invoke-virtual {p0, v1, v2}, Lcom/google/android/videos/pinning/LegacyDownloader;->getStreams(ZZ)Lcom/google/android/videos/streams/Streams;

    move-result-object v7

    .line 83
    .local v7, "streams":Lcom/google/android/videos/streams/Streams;
    invoke-virtual {p0}, Lcom/google/android/videos/pinning/LegacyDownloader;->isCanceled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 94
    :cond_1
    :goto_0
    return-object v0

    .line 86
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/pinning/LegacyDownloader;->details:Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;

    iget v1, v1, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->licenseType:I

    if-ne v1, v3, :cond_3

    invoke-direct {p0, v7}, Lcom/google/android/videos/pinning/LegacyDownloader;->setupExistingDownload(Lcom/google/android/videos/streams/Streams;)Landroid/util/Pair;

    move-result-object v6

    .line 89
    .local v6, "setup":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/videos/streams/MediaStream;Ljava/lang/String;>;"
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/videos/pinning/LegacyDownloader;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 92
    iget-object v0, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/videos/streams/MediaStream;

    iput-object v0, p0, Lcom/google/android/videos/pinning/LegacyDownloader;->mediaStream:Lcom/google/android/videos/streams/MediaStream;

    .line 93
    iget-object v0, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/videos/pinning/LegacyDownloader;->relativeFilePath:Ljava/lang/String;

    .line 94
    new-instance v0, Lcom/google/android/videos/pinning/Downloader$SetupResult;

    iget-object v1, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/videos/streams/MediaStream;

    const-wide/16 v2, 0x0

    iget-object v4, v7, Lcom/google/android/videos/streams/Streams;->captions:Ljava/util/List;

    iget-object v5, p0, Lcom/google/android/videos/pinning/LegacyDownloader;->context:Landroid/content/Context;

    iget-object v8, v7, Lcom/google/android/videos/streams/Streams;->storyboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    invoke-static {v5, v8}, Lcom/google/android/videos/utils/Util;->getBestStoryboard(Landroid/content/Context;[Lcom/google/wireless/android/video/magma/proto/Storyboard;)Lcom/google/wireless/android/video/magma/proto/Storyboard;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/pinning/Downloader$SetupResult;-><init>(Lcom/google/android/videos/streams/MediaStream;JLjava/util/List;Lcom/google/wireless/android/video/magma/proto/Storyboard;)V

    goto :goto_0

    .line 86
    .end local v6    # "setup":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/videos/streams/MediaStream;Ljava/lang/String;>;"
    :cond_3
    iget-object v1, p0, Lcom/google/android/videos/pinning/LegacyDownloader;->drmManager:Lcom/google/android/videos/drm/DrmManager;

    invoke-virtual {v1}, Lcom/google/android/videos/drm/DrmManager;->getDrmLevel()I

    move-result v1

    invoke-direct {p0, v7, v1}, Lcom/google/android/videos/pinning/LegacyDownloader;->setupNewDownload(Lcom/google/android/videos/streams/Streams;I)Landroid/util/Pair;

    move-result-object v6

    goto :goto_1
.end method
