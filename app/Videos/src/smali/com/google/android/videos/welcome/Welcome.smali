.class public abstract Lcom/google/android/videos/welcome/Welcome;
.super Ljava/lang/Object;
.source "Welcome.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/welcome/Welcome$OnEligibilityChangedListener;
    }
.end annotation


# instance fields
.field private listener:Lcom/google/android/videos/welcome/Welcome$OnEligibilityChangedListener;

.field public final promoId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "promoId"    # Ljava/lang/String;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/android/videos/welcome/Welcome;->promoId:Ljava/lang/String;

    .line 26
    return-void
.end method


# virtual methods
.method public abstract getActionResIds()[I
.end method

.method public abstract getDefaultBitmapResId()I
.end method

.method public abstract getDetailMessage()Ljava/lang/CharSequence;
.end method

.method public getLayoutResourceId()I
    .locals 1

    .prologue
    .line 52
    const v0, 0x7f0400d0

    return v0
.end method

.method public abstract getNetworkBitmapUri()Landroid/net/Uri;
.end method

.method public abstract getTitle()Ljava/lang/CharSequence;
.end method

.method public isUserDismissable()Z
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    return v0
.end method

.method protected final notifyEligibilityChanged()V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/videos/welcome/Welcome;->listener:Lcom/google/android/videos/welcome/Welcome$OnEligibilityChangedListener;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/google/android/videos/welcome/Welcome;->listener:Lcom/google/android/videos/welcome/Welcome$OnEligibilityChangedListener;

    invoke-interface {v0}, Lcom/google/android/videos/welcome/Welcome$OnEligibilityChangedListener;->onEligibilityChanged()V

    .line 83
    :cond_0
    return-void
.end method

.method public abstract onAction(I)V
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 28
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 30
    return-void
.end method

.method public abstract prepareIfEligible(Ljava/lang/String;ZZ)Z
.end method

.method public final setEligibilityChangedListener(Lcom/google/android/videos/welcome/Welcome$OnEligibilityChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/videos/welcome/Welcome$OnEligibilityChangedListener;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/google/android/videos/welcome/Welcome;->listener:Lcom/google/android/videos/welcome/Welcome$OnEligibilityChangedListener;

    .line 77
    return-void
.end method
