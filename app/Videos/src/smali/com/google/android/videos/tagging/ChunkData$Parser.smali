.class Lcom/google/android/videos/tagging/ChunkData$Parser;
.super Ljava/lang/Object;
.source "ChunkData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/ChunkData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Parser"
.end annotation


# instance fields
.field private final chunkStartMillis:I

.field private currentKeyframeIndex:I

.field private currentTagCount:I

.field private final keyframesByTime:Landroid/support/v4/util/SparseArrayCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/SparseArrayCompat",
            "<",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/videos/tagging/Tag;",
            ">;>;"
        }
    .end annotation
.end field

.field private final lastTagBySplitId:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/videos/tagging/Tag;",
            ">;"
        }
    .end annotation
.end field

.field private nextKeyframe:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/videos/tagging/Tag;",
            ">;"
        }
    .end annotation
.end field

.field private offset:I

.field private final tagStream:Lcom/google/android/videos/tagging/TagStream;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/tagging/TagStream;I)V
    .locals 1
    .param p1, "tagStream"    # Lcom/google/android/videos/tagging/TagStream;
    .param p2, "chunkStartMillis"    # I

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput-object p1, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->tagStream:Lcom/google/android/videos/tagging/TagStream;

    .line 87
    iput p2, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->chunkStartMillis:I

    .line 88
    new-instance v0, Landroid/support/v4/util/SparseArrayCompat;

    invoke-direct {v0}, Landroid/support/v4/util/SparseArrayCompat;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->keyframesByTime:Landroid/support/v4/util/SparseArrayCompat;

    .line 89
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->lastTagBySplitId:Landroid/util/SparseArray;

    .line 90
    return-void
.end method

.method private parseFrame(Lcom/google/android/videos/proto/TagProtos$Frame;Z)V
    .locals 9
    .param p1, "frame"    # Lcom/google/android/videos/proto/TagProtos$Frame;
    .param p2, "isFirstFrame"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 124
    iget v3, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->chunkStartMillis:I

    .line 125
    .local v3, "millis":I
    if-nez p2, :cond_1

    .line 126
    iget v1, p1, Lcom/google/android/videos/proto/TagProtos$Frame;->offset:I

    .line 127
    .local v1, "extraOffset":I
    if-gtz v1, :cond_0

    .line 128
    new-instance v5, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;

    const-string v6, "Frame invalid - no or non-positive offset"

    invoke-direct {v5, v6}, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 130
    :cond_0
    iget v7, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->offset:I

    add-int/2addr v7, v1

    iput v7, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->offset:I

    .line 131
    iget-object v7, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->tagStream:Lcom/google/android/videos/tagging/TagStream;

    iget v8, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->offset:I

    invoke-virtual {v7, v8}, Lcom/google/android/videos/tagging/TagStream;->toMillis(I)I

    move-result v7

    add-int/2addr v3, v7

    .line 134
    .end local v1    # "extraOffset":I
    :cond_1
    iget v7, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->currentKeyframeIndex:I

    iget-object v8, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->keyframesByTime:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v8}, Landroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v8

    if-lt v7, v8, :cond_3

    move v0, v5

    .line 135
    .local v0, "atStartOfKeyframe":Z
    :goto_0
    if-eqz v0, :cond_2

    .line 136
    iget-object v7, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->keyframesByTime:Landroid/support/v4/util/SparseArrayCompat;

    iget-object v8, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->nextKeyframe:Landroid/util/SparseArray;

    invoke-virtual {v7, v3, v8}, Landroid/support/v4/util/SparseArrayCompat;->append(ILjava/lang/Object;)V

    .line 139
    :cond_2
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget-object v7, p1, Lcom/google/android/videos/proto/TagProtos$Frame;->tagIn:[Lcom/google/android/videos/proto/TagProtos$Tag;

    array-length v7, v7

    if-ge v2, v7, :cond_4

    .line 140
    iget-object v7, p1, Lcom/google/android/videos/proto/TagProtos$Frame;->tagIn:[Lcom/google/android/videos/proto/TagProtos$Tag;

    aget-object v7, v7, v2

    invoke-direct {p0, v7, v3, v0, v5}, Lcom/google/android/videos/tagging/ChunkData$Parser;->parseTag(Lcom/google/android/videos/proto/TagProtos$Tag;IZZ)V

    .line 139
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .end local v0    # "atStartOfKeyframe":Z
    .end local v2    # "i":I
    :cond_3
    move v0, v6

    .line 134
    goto :goto_0

    .line 142
    .restart local v0    # "atStartOfKeyframe":Z
    .restart local v2    # "i":I
    :cond_4
    const/4 v2, 0x0

    :goto_2
    iget-object v5, p1, Lcom/google/android/videos/proto/TagProtos$Frame;->tagOut:[Lcom/google/android/videos/proto/TagProtos$Tag;

    array-length v5, v5

    if-ge v2, v5, :cond_5

    .line 143
    iget-object v5, p1, Lcom/google/android/videos/proto/TagProtos$Frame;->tagOut:[Lcom/google/android/videos/proto/TagProtos$Tag;

    aget-object v5, v5, v2

    invoke-direct {p0, v5, v3, v0, v6}, Lcom/google/android/videos/tagging/ChunkData$Parser;->parseTag(Lcom/google/android/videos/proto/TagProtos$Tag;IZZ)V

    .line 142
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 145
    :cond_5
    iget v5, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->currentTagCount:I

    if-nez v5, :cond_7

    .line 148
    iget-object v5, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->keyframesByTime:Landroid/support/v4/util/SparseArrayCompat;

    iget v6, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->currentKeyframeIndex:I

    invoke-virtual {v5, v6}, Landroid/support/v4/util/SparseArrayCompat;->removeAt(I)V

    .line 161
    :cond_6
    :goto_3
    return-void

    .line 149
    :cond_7
    iget v5, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->currentTagCount:I

    const/16 v7, 0x32

    if-lt v5, v7, :cond_6

    .line 151
    new-instance v5, Landroid/util/SparseArray;

    iget-object v7, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->lastTagBySplitId:Landroid/util/SparseArray;

    invoke-virtual {v7}, Landroid/util/SparseArray;->size()I

    move-result v7

    invoke-direct {v5, v7}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v5, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->nextKeyframe:Landroid/util/SparseArray;

    .line 152
    const/4 v2, 0x0

    :goto_4
    iget-object v5, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->lastTagBySplitId:Landroid/util/SparseArray;

    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    move-result v5

    if-ge v2, v5, :cond_9

    .line 153
    iget-object v5, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->lastTagBySplitId:Landroid/util/SparseArray;

    invoke-virtual {v5, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/videos/tagging/Tag;

    .line 154
    .local v4, "tag":Lcom/google/android/videos/tagging/Tag;
    iget-boolean v5, v4, Lcom/google/android/videos/tagging/Tag;->interpolates:Z

    if-eqz v5, :cond_8

    .line 155
    iget-object v5, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->nextKeyframe:Landroid/util/SparseArray;

    iget v7, v4, Lcom/google/android/videos/tagging/Tag;->splitId:I

    invoke-virtual {v5, v7, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 152
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 158
    .end local v4    # "tag":Lcom/google/android/videos/tagging/Tag;
    :cond_9
    iget v5, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->currentKeyframeIndex:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->currentKeyframeIndex:I

    .line 159
    iput v6, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->currentTagCount:I

    goto :goto_3
.end method

.method private parseTag(Lcom/google/android/videos/proto/TagProtos$Tag;IZZ)V
    .locals 5
    .param p1, "tagProto"    # Lcom/google/android/videos/proto/TagProtos$Tag;
    .param p2, "millis"    # I
    .param p3, "atStartOfKeyframe"    # Z
    .param p4, "isTagIn"    # Z

    .prologue
    .line 165
    invoke-static {p1, p2, p4}, Lcom/google/android/videos/tagging/Tag;->parseFrom(Lcom/google/android/videos/proto/TagProtos$Tag;IZ)Lcom/google/android/videos/tagging/Tag;

    move-result-object v2

    .line 166
    .local v2, "tag":Lcom/google/android/videos/tagging/Tag;
    iget-object v3, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->tagStream:Lcom/google/android/videos/tagging/TagStream;

    iget v4, v2, Lcom/google/android/videos/tagging/Tag;->splitId:I

    invoke-virtual {v3, v4}, Lcom/google/android/videos/tagging/TagStream;->getKnowledgeEntityBySplitId(I)Lcom/google/android/videos/tagging/KnowledgeEntity;

    move-result-object v3

    if-nez v3, :cond_1

    .line 184
    :cond_0
    :goto_0
    return-void

    .line 169
    :cond_1
    iget v3, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->currentTagCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->currentTagCount:I

    .line 171
    iget-object v3, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->lastTagBySplitId:Landroid/util/SparseArray;

    iget v4, v2, Lcom/google/android/videos/tagging/Tag;->splitId:I

    invoke-virtual {v3, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/tagging/Tag;

    .line 172
    .local v1, "oldTag":Lcom/google/android/videos/tagging/Tag;
    iget-object v3, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->lastTagBySplitId:Landroid/util/SparseArray;

    iget v4, v2, Lcom/google/android/videos/tagging/Tag;->splitId:I

    invoke-virtual {v3, v4, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 173
    if-eqz v1, :cond_2

    .line 174
    invoke-virtual {v1, v2}, Lcom/google/android/videos/tagging/Tag;->setNextTag(Lcom/google/android/videos/tagging/Tag;)V

    .line 175
    iget-boolean v3, v1, Lcom/google/android/videos/tagging/Tag;->interpolates:Z

    invoke-virtual {v2, v3}, Lcom/google/android/videos/tagging/Tag;->setInterpolatesIn(Z)V

    .line 180
    :cond_2
    iget-object v3, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->keyframesByTime:Landroid/support/v4/util/SparseArrayCompat;

    iget v4, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->currentKeyframeIndex:I

    invoke-virtual {v3, v4}, Landroid/support/v4/util/SparseArrayCompat;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    .line 181
    .local v0, "firstTagsBySplitId":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/google/android/videos/tagging/Tag;>;"
    if-nez p3, :cond_3

    iget v3, v2, Lcom/google/android/videos/tagging/Tag;->splitId:I

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v3

    if-gez v3, :cond_0

    .line 182
    :cond_3
    iget v3, v2, Lcom/google/android/videos/tagging/Tag;->splitId:I

    invoke-virtual {v0, v3, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public parseFrom([BII)Lcom/google/android/videos/tagging/ChunkData;
    .locals 11
    .param p1, "in"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    invoke-static {p1, p2, p3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->newInstance([BII)Lcom/google/protobuf/nano/CodedInputByteBufferNano;

    move-result-object v0

    .line 94
    .local v0, "buffer":Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    new-instance v10, Landroid/util/SparseArray;

    invoke-direct {v10}, Landroid/util/SparseArray;-><init>()V

    iput-object v10, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->nextKeyframe:Landroid/util/SparseArray;

    .line 95
    const/4 v3, 0x1

    .line 96
    .local v3, "isFirstFrame":Z
    :goto_0
    invoke-virtual {v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->isAtEnd()Z

    move-result v10

    if-nez v10, :cond_0

    .line 97
    new-instance v1, Lcom/google/android/videos/proto/TagProtos$Frame;

    invoke-direct {v1}, Lcom/google/android/videos/proto/TagProtos$Frame;-><init>()V

    .line 98
    .local v1, "frame":Lcom/google/android/videos/proto/TagProtos$Frame;
    invoke-virtual {v0, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 99
    invoke-direct {p0, v1, v3}, Lcom/google/android/videos/tagging/ChunkData$Parser;->parseFrame(Lcom/google/android/videos/proto/TagProtos$Frame;Z)V

    .line 100
    const/4 v3, 0x0

    .line 101
    goto :goto_0

    .line 102
    .end local v1    # "frame":Lcom/google/android/videos/proto/TagProtos$Frame;
    :cond_0
    iget-object v10, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->keyframesByTime:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v10}, Landroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v6

    .line 103
    .local v6, "keyframeCount":I
    new-array v7, v6, [I

    .line 104
    .local v7, "keyframeTimes":[I
    new-array v8, v6, [[Lcom/google/android/videos/tagging/Tag;

    .line 105
    .local v8, "keyframes":[[Lcom/google/android/videos/tagging/Tag;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v6, :cond_2

    .line 106
    iget-object v10, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->keyframesByTime:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v10, v2}, Landroid/support/v4/util/SparseArrayCompat;->keyAt(I)I

    move-result v10

    aput v10, v7, v2

    .line 107
    iget-object v10, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->keyframesByTime:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v10, v2}, Landroid/support/v4/util/SparseArrayCompat;->valueAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/util/SparseArray;

    .line 108
    .local v5, "keyframe":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/google/android/videos/tagging/Tag;>;"
    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    move-result v10

    new-array v9, v10, [Lcom/google/android/videos/tagging/Tag;

    .line 109
    .local v9, "tagsAtKeyframe":[Lcom/google/android/videos/tagging/Tag;
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_2
    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    move-result v10

    if-ge v4, v10, :cond_1

    .line 110
    invoke-virtual {v5, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/videos/tagging/Tag;

    aput-object v10, v9, v4

    .line 109
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 112
    :cond_1
    aput-object v9, v8, v2

    .line 105
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 115
    .end local v4    # "j":I
    .end local v5    # "keyframe":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/google/android/videos/tagging/Tag;>;"
    .end local v9    # "tagsAtKeyframe":[Lcom/google/android/videos/tagging/Tag;
    :cond_2
    iget-object v10, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->keyframesByTime:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v10}, Landroid/support/v4/util/SparseArrayCompat;->clear()V

    .line 116
    iget-object v10, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->lastTagBySplitId:Landroid/util/SparseArray;

    invoke-virtual {v10}, Landroid/util/SparseArray;->clear()V

    .line 117
    const/4 v10, 0x0

    iput-object v10, p0, Lcom/google/android/videos/tagging/ChunkData$Parser;->nextKeyframe:Landroid/util/SparseArray;

    .line 118
    new-instance v10, Lcom/google/android/videos/tagging/ChunkData;

    invoke-direct {v10, v7, v8}, Lcom/google/android/videos/tagging/ChunkData;-><init>([I[[Lcom/google/android/videos/tagging/Tag;)V

    return-object v10
.end method
