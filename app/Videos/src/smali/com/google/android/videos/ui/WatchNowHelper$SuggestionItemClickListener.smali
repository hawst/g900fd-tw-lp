.class final Lcom/google/android/videos/ui/WatchNowHelper$SuggestionItemClickListener;
.super Ljava/lang/Object;
.source "WatchNowHelper.java"

# interfaces
.implements Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/WatchNowHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SuggestionItemClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/ui/WatchNowHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/ui/WatchNowHelper;)V
    .locals 0

    .prologue
    .line 604
    iput-object p1, p0, Lcom/google/android/videos/ui/WatchNowHelper$SuggestionItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/ui/WatchNowHelper;Lcom/google/android/videos/ui/WatchNowHelper$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/ui/WatchNowHelper;
    .param p2, "x1"    # Lcom/google/android/videos/ui/WatchNowHelper$1;

    .prologue
    .line 604
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/WatchNowHelper$SuggestionItemClickListener;-><init>(Lcom/google/android/videos/ui/WatchNowHelper;)V

    return-void
.end method


# virtual methods
.method public onItemClick(Lcom/google/android/videos/adapter/DataSource;ILandroid/view/View;)V
    .locals 15
    .param p2, "position"    # I
    .param p3, "view"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/adapter/DataSource",
            "<*>;I",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 609
    .local p1, "dataSource":Lcom/google/android/videos/adapter/DataSource;, "Lcom/google/android/videos/adapter/DataSource<*>;"
    invoke-interface/range {p1 .. p2}, Lcom/google/android/videos/adapter/DataSource;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;

    iget-object v13, v0, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 610
    .local v13, "selectedItem":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    iget-object v0, v13, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget v6, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    .line 611
    .local v6, "itemType":I
    const/4 v0, 0x6

    if-eq v6, v0, :cond_1

    const/16 v0, 0x12

    if-eq v6, v0, :cond_1

    .line 612
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Don\'t know how to handle asset type "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 672
    :cond_0
    :goto_0
    return-void

    .line 615
    :cond_1
    iget-object v0, v13, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v12, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    .line 616
    .local v12, "itemId":Ljava/lang/String;
    const/4 v0, 0x6

    if-ne v6, v0, :cond_3

    const/4 v11, 0x1

    .line 618
    .local v11, "isMovie":Z
    :goto_1
    const/4 v10, 0x0

    .line 619
    .local v10, "handled":Z
    invoke-virtual/range {p3 .. p3}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 662
    :cond_2
    :goto_2
    if-nez v10, :cond_0

    .line 663
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper$SuggestionItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;
    invoke-static {v0}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1000(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/logging/UiEventLoggingHelper;

    move-result-object v0

    invoke-static/range {p3 .. p3}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->findUiElementNode(Landroid/view/View;)Lcom/google/android/videos/logging/UiElementNode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->sendClickEvent(Lcom/google/android/videos/logging/UiElementNode;)V

    .line 664
    if-eqz v11, :cond_5

    .line 665
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper$SuggestionItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;
    invoke-static {v0}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1100(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/activity/HomeActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper$SuggestionItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->suggestionsAccount:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1500(Lcom/google/android/videos/ui/WatchNowHelper;)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x6

    invoke-static {v0, v12, v1, v3}, Lcom/google/android/videos/utils/PlayStoreUtil;->viewMovieDetails(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 616
    .end local v10    # "handled":Z
    .end local v11    # "isMovie":Z
    :cond_3
    const/4 v11, 0x0

    goto :goto_1

    .line 621
    .restart local v10    # "handled":Z
    .restart local v11    # "isMovie":Z
    :sswitch_0
    if-eqz v11, :cond_2

    .line 622
    invoke-static {v13}, Lcom/google/android/videos/api/AssetResourceUtil;->findTrailerId(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/lang/String;

    move-result-object v2

    .line 623
    .local v2, "trailerId":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 624
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper$SuggestionItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;
    invoke-static {v0}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1100(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/activity/HomeActivity;

    move-result-object v7

    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper$SuggestionItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;
    invoke-static {v0}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1100(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/activity/HomeActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper$SuggestionItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->suggestionsAccount:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1500(Lcom/google/android/videos/ui/WatchNowHelper;)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/activity/WatchActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/google/android/videos/activity/HomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 626
    const/4 v10, 0x1

    goto :goto_2

    .line 631
    .end local v2    # "trailerId":Ljava/lang/String;
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper$SuggestionItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;
    invoke-static {v0}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1600(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/store/StoreStatusMonitor;

    move-result-object v0

    invoke-virtual {v0, v12, v6}, Lcom/google/android/videos/store/StoreStatusMonitor;->getStatus(Ljava/lang/String;I)I

    move-result v14

    .line 632
    .local v14, "storeStatus":I
    invoke-static {v14}, Lcom/google/android/videos/store/StoreStatusMonitor;->isPurchased(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 633
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper$SuggestionItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;
    invoke-static {v0}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1100(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/activity/HomeActivity;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper$SuggestionItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->suggestionsAccount:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1500(Lcom/google/android/videos/ui/WatchNowHelper;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v14}, Lcom/google/android/videos/store/StoreStatusMonitor;->isWishlisted(I)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v7, 0x1

    :goto_3
    const/4 v8, 0x6

    move-object v5, v12

    move-object/from16 v9, p3

    invoke-static/range {v3 .. v9}, Lcom/google/android/videos/store/WishlistService;->requestSetWishlisted(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZILandroid/view/View;)V

    .line 635
    const/4 v10, 0x1

    goto :goto_2

    .line 633
    :cond_4
    const/4 v7, 0x0

    goto :goto_3

    .line 639
    .end local v14    # "storeStatus":I
    :sswitch_2
    if-eqz v11, :cond_2

    .line 640
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper$SuggestionItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;
    invoke-static {v0}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1700(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v3, 0x6

    invoke-virtual {v0, v12, v1, v3}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->startMovieDirectPurchaseFlow(Ljava/lang/String;II)V

    .line 642
    const/4 v10, 0x1

    goto/16 :goto_2

    .line 646
    :sswitch_3
    if-eqz v11, :cond_2

    .line 647
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper$SuggestionItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;
    invoke-static {v0}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1700(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v3, 0x6

    invoke-virtual {v0, v12, v1, v3}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->startMovieDirectPurchaseFlow(Ljava/lang/String;II)V

    .line 649
    const/4 v10, 0x1

    goto/16 :goto_2

    .line 654
    :sswitch_4
    if-eqz v11, :cond_2

    .line 655
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper$SuggestionItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;
    invoke-static {v0}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1700(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v3, 0x6

    invoke-virtual {v0, v12, v1, v3}, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;->startMovieDirectPurchaseFlow(Ljava/lang/String;II)V

    .line 657
    const/4 v10, 0x1

    goto/16 :goto_2

    .line 668
    :cond_5
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper$SuggestionItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;
    invoke-static {v0}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1100(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/activity/HomeActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper$SuggestionItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->suggestionsAccount:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1500(Lcom/google/android/videos/ui/WatchNowHelper;)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x6

    invoke-static {v0, v12, v1, v3}, Lcom/google/android/videos/utils/PlayStoreUtil;->viewShowDetails(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 619
    :sswitch_data_0
    .sparse-switch
        0x7f0f00ca -> :sswitch_1
        0x7f0f00e1 -> :sswitch_0
        0x7f0f00ea -> :sswitch_2
        0x7f0f00eb -> :sswitch_3
        0x7f0f00ec -> :sswitch_4
        0x7f0f00ed -> :sswitch_4
    .end sparse-switch
.end method
