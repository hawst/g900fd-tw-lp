.class public abstract Lcom/google/android/videos/pinning/Task;
.super Ljava/lang/Object;
.source "Task.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/pinning/Task$Listener;,
        Lcom/google/android/videos/pinning/Task$TaskException;,
        Lcom/google/android/videos/pinning/Task$Key;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/google/android/videos/pinning/Task$Key;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field protected final eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private volatile executionThread:Ljava/lang/Thread;

.field public final key:Lcom/google/android/videos/pinning/Task$Key;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final listener:Lcom/google/android/videos/pinning/Task$Listener;

.field private final taskControl:Lcom/google/android/videos/async/TaskControl;

.field public final taskType:I

.field private final terminated:Landroid/os/ConditionVariable;

.field private final wakeLock:Landroid/os/PowerManager$WakeLock;

.field private final wifiLock:Landroid/net/wifi/WifiManager$WifiLock;


# direct methods
.method public constructor <init>(ILcom/google/android/videos/pinning/Task$Key;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/videos/pinning/Task$Listener;Lcom/google/android/videos/logging/EventLogger;)V
    .locals 1
    .param p1, "taskType"    # I
    .param p3, "wakeLock"    # Landroid/os/PowerManager$WakeLock;
    .param p4, "wifiLock"    # Landroid/net/wifi/WifiManager$WifiLock;
    .param p5, "listener"    # Lcom/google/android/videos/pinning/Task$Listener;
    .param p6, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;",
            "Landroid/os/PowerManager$WakeLock;",
            "Landroid/net/wifi/WifiManager$WifiLock;",
            "Lcom/google/android/videos/pinning/Task$Listener;",
            "Lcom/google/android/videos/logging/EventLogger;",
            ")V"
        }
    .end annotation

    .prologue
    .line 70
    .local p0, "this":Lcom/google/android/videos/pinning/Task;, "Lcom/google/android/videos/pinning/Task<TT;>;"
    .local p2, "key":Lcom/google/android/videos/pinning/Task$Key;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput p1, p0, Lcom/google/android/videos/pinning/Task;->taskType:I

    .line 72
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/pinning/Task$Key;

    iput-object v0, p0, Lcom/google/android/videos/pinning/Task;->key:Lcom/google/android/videos/pinning/Task$Key;

    .line 73
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager$WakeLock;

    iput-object v0, p0, Lcom/google/android/videos/pinning/Task;->wakeLock:Landroid/os/PowerManager$WakeLock;

    .line 74
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager$WifiLock;

    iput-object v0, p0, Lcom/google/android/videos/pinning/Task;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    .line 75
    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/pinning/Task$Listener;

    iput-object v0, p0, Lcom/google/android/videos/pinning/Task;->listener:Lcom/google/android/videos/pinning/Task$Listener;

    .line 76
    invoke-static {p6}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/logging/EventLogger;

    iput-object v0, p0, Lcom/google/android/videos/pinning/Task;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 77
    new-instance v0, Lcom/google/android/videos/async/TaskControl;

    invoke-direct {v0}, Lcom/google/android/videos/async/TaskControl;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/pinning/Task;->taskControl:Lcom/google/android/videos/async/TaskControl;

    .line 78
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/pinning/Task;->terminated:Landroid/os/ConditionVariable;

    .line 79
    return-void
.end method

.method protected static getThrowableToLog(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .locals 3
    .param p0, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 175
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/google/android/videos/pinning/Task$TaskException;

    if-ne v1, v2, :cond_0

    .line 176
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 177
    .local v0, "cause":Ljava/lang/Throwable;
    if-eqz v0, :cond_0

    .line 181
    .end local v0    # "cause":Ljava/lang/Throwable;
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    goto :goto_0
.end method


# virtual methods
.method public final cancel()V
    .locals 1

    .prologue
    .line 82
    .local p0, "this":Lcom/google/android/videos/pinning/Task;, "Lcom/google/android/videos/pinning/Task<TT;>;"
    monitor-enter p0

    .line 83
    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/pinning/Task;->taskControl:Lcom/google/android/videos/async/TaskControl;

    invoke-virtual {v0}, Lcom/google/android/videos/async/TaskControl;->cancel()V

    .line 84
    iget-object v0, p0, Lcom/google/android/videos/pinning/Task;->executionThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/google/android/videos/pinning/Task;->executionThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 87
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    iget-object v0, p0, Lcom/google/android/videos/pinning/Task;->terminated:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    .line 89
    return-void

    .line 87
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected abstract execute()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/Task$TaskException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation
.end method

.method protected final getTaskStatus()Lcom/google/android/videos/async/TaskStatus;
    .locals 1

    .prologue
    .line 96
    .local p0, "this":Lcom/google/android/videos/pinning/Task;, "Lcom/google/android/videos/pinning/Task<TT;>;"
    iget-object v0, p0, Lcom/google/android/videos/pinning/Task;->taskControl:Lcom/google/android/videos/async/TaskControl;

    return-object v0
.end method

.method protected final isCanceled()Z
    .locals 1

    .prologue
    .line 92
    .local p0, "this":Lcom/google/android/videos/pinning/Task;, "Lcom/google/android/videos/pinning/Task<TT;>;"
    iget-object v0, p0, Lcom/google/android/videos/pinning/Task;->taskControl:Lcom/google/android/videos/async/TaskControl;

    invoke-virtual {v0}, Lcom/google/android/videos/async/TaskControl;->isCancelled()Z

    move-result v0

    return v0
.end method

.method protected final notifyProgress()V
    .locals 1

    .prologue
    .line 165
    .local p0, "this":Lcom/google/android/videos/pinning/Task;, "Lcom/google/android/videos/pinning/Task<TT;>;"
    invoke-virtual {p0}, Lcom/google/android/videos/pinning/Task;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/google/android/videos/pinning/Task;->listener:Lcom/google/android/videos/pinning/Task$Listener;

    invoke-interface {v0, p0}, Lcom/google/android/videos/pinning/Task$Listener;->onProgress(Lcom/google/android/videos/pinning/Task;)V

    .line 168
    :cond_0
    return-void
.end method

.method protected abstract onCompleted()V
.end method

.method protected abstract onError(Ljava/lang/Throwable;ZZ)V
.end method

.method public final run()V
    .locals 5

    .prologue
    .line 101
    .local p0, "this":Lcom/google/android/videos/pinning/Task;, "Lcom/google/android/videos/pinning/Task<TT;>;"
    monitor-enter p0

    .line 102
    :try_start_0
    iget-object v1, p0, Lcom/google/android/videos/pinning/Task;->taskControl:Lcom/google/android/videos/async/TaskControl;

    invoke-virtual {v1}, Lcom/google/android/videos/async/TaskControl;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 103
    monitor-exit p0

    .line 132
    :goto_0
    return-void

    .line 105
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/pinning/Task;->executionThread:Ljava/lang/Thread;

    .line 106
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 107
    const/16 v1, 0xa

    invoke-static {v1}, Landroid/os/Process;->setThreadPriority(I)V

    .line 108
    iget-object v1, p0, Lcom/google/android/videos/pinning/Task;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    .line 109
    iget-object v1, p0, Lcom/google/android/videos/pinning/Task;->wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 111
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/videos/pinning/Task;->execute()V

    .line 112
    invoke-virtual {p0}, Lcom/google/android/videos/pinning/Task;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 113
    invoke-virtual {p0}, Lcom/google/android/videos/pinning/Task;->onCompleted()V

    .line 114
    iget-object v1, p0, Lcom/google/android/videos/pinning/Task;->listener:Lcom/google/android/videos/pinning/Task$Listener;

    invoke-interface {v1, p0}, Lcom/google/android/videos/pinning/Task$Listener;->onCompleted(Lcom/google/android/videos/pinning/Task;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/videos/pinning/Task$TaskException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 128
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/pinning/Task;->terminated:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->open()V

    .line 129
    iget-object v1, p0, Lcom/google/android/videos/pinning/Task;->wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 130
    iget-object v1, p0, Lcom/google/android/videos/pinning/Task;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto :goto_0

    .line 106
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 116
    :catch_0
    move-exception v1

    .line 128
    iget-object v1, p0, Lcom/google/android/videos/pinning/Task;->terminated:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->open()V

    .line 129
    iget-object v1, p0, Lcom/google/android/videos/pinning/Task;->wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 130
    iget-object v1, p0, Lcom/google/android/videos/pinning/Task;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto :goto_0

    .line 118
    :catch_1
    move-exception v0

    .line 119
    .local v0, "e":Lcom/google/android/videos/pinning/Task$TaskException;
    :try_start_3
    invoke-virtual {p0}, Lcom/google/android/videos/pinning/Task;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_2

    .line 120
    iget-object v1, p0, Lcom/google/android/videos/pinning/Task;->listener:Lcom/google/android/videos/pinning/Task$Listener;

    invoke-interface {v1, p0, v0}, Lcom/google/android/videos/pinning/Task$Listener;->onError(Lcom/google/android/videos/pinning/Task;Lcom/google/android/videos/pinning/Task$TaskException;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 128
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/pinning/Task;->terminated:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->open()V

    .line 129
    iget-object v1, p0, Lcom/google/android/videos/pinning/Task;->wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 130
    iget-object v1, p0, Lcom/google/android/videos/pinning/Task;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto :goto_0

    .line 122
    .end local v0    # "e":Lcom/google/android/videos/pinning/Task$TaskException;
    :catch_2
    move-exception v0

    .line 123
    .local v0, "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {p0}, Lcom/google/android/videos/pinning/Task;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_3

    .line 124
    iget-object v1, p0, Lcom/google/android/videos/pinning/Task;->listener:Lcom/google/android/videos/pinning/Task$Listener;

    new-instance v2, Lcom/google/android/videos/pinning/Task$TaskException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "unexpected exception executing task "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/google/android/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {v1, p0, v2}, Lcom/google/android/videos/pinning/Task$Listener;->onError(Lcom/google/android/videos/pinning/Task;Lcom/google/android/videos/pinning/Task$TaskException;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 128
    :cond_3
    iget-object v1, p0, Lcom/google/android/videos/pinning/Task;->terminated:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->open()V

    .line 129
    iget-object v1, p0, Lcom/google/android/videos/pinning/Task;->wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 130
    iget-object v1, p0, Lcom/google/android/videos/pinning/Task;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto/16 :goto_0

    .line 128
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v1

    iget-object v2, p0, Lcom/google/android/videos/pinning/Task;->terminated:Landroid/os/ConditionVariable;

    invoke-virtual {v2}, Landroid/os/ConditionVariable;->open()V

    .line 129
    iget-object v2, p0, Lcom/google/android/videos/pinning/Task;->wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 130
    iget-object v2, p0, Lcom/google/android/videos/pinning/Task;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    throw v1
.end method
