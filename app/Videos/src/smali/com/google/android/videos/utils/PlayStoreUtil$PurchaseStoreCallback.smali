.class Lcom/google/android/videos/utils/PlayStoreUtil$PurchaseStoreCallback;
.super Ljava/lang/Object;
.source "PlayStoreUtil.java"

# interfaces
.implements Lcom/google/android/videos/async/NewCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/utils/PlayStoreUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PurchaseStoreCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/NewCallback",
        "<",
        "Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final callback:Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler$Callback;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler$Callback;)V
    .locals 0
    .param p1, "callback"    # Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler$Callback;

    .prologue
    .line 501
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 502
    iput-object p1, p0, Lcom/google/android/videos/utils/PlayStoreUtil$PurchaseStoreCallback;->callback:Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler$Callback;

    .line 503
    return-void
.end method


# virtual methods
.method public onCancelled(Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    .prologue
    .line 522
    return-void
.end method

.method public bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 496
    check-cast p1, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/utils/PlayStoreUtil$PurchaseStoreCallback;->onCancelled(Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;)V

    return-void
.end method

.method public onError(Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/google/android/videos/utils/PlayStoreUtil$PurchaseStoreCallback;->callback:Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler$Callback;

    if-eqz v0, :cond_0

    .line 515
    iget-object v0, p0, Lcom/google/android/videos/utils/PlayStoreUtil$PurchaseStoreCallback;->callback:Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler$Callback;

    invoke-interface {v0, p2}, Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler$Callback;->onSyncError(Ljava/lang/Exception;)V

    .line 517
    :cond_0
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 496
    check-cast p1, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/utils/PlayStoreUtil$PurchaseStoreCallback;->onError(Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Ljava/lang/Void;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;
    .param p2, "response"    # Ljava/lang/Void;

    .prologue
    .line 507
    iget-object v0, p0, Lcom/google/android/videos/utils/PlayStoreUtil$PurchaseStoreCallback;->callback:Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler$Callback;

    if-eqz v0, :cond_0

    .line 508
    iget-object v0, p0, Lcom/google/android/videos/utils/PlayStoreUtil$PurchaseStoreCallback;->callback:Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler$Callback;

    invoke-interface {v0}, Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler$Callback;->onSyncCompleted()V

    .line 510
    :cond_0
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 496
    check-cast p1, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/lang/Void;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/utils/PlayStoreUtil$PurchaseStoreCallback;->onResponse(Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Ljava/lang/Void;)V

    return-void
.end method
