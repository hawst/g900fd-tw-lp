.class public Lcom/google/android/videos/logging/UiElementWrapper;
.super Ljava/lang/Object;
.source "UiElementWrapper.java"


# instance fields
.field public final children:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/videos/logging/UiElementWrapper;",
            ">;"
        }
    .end annotation
.end field

.field public final uiElement:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    invoke-direct {v0}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/logging/UiElementWrapper;->uiElement:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/logging/UiElementWrapper;->children:Ljava/util/ArrayList;

    .line 26
    return-void
.end method


# virtual methods
.method public addChild(Lcom/google/android/videos/logging/UiElementWrapper;)Z
    .locals 3
    .param p1, "newChild"    # Lcom/google/android/videos/logging/UiElementWrapper;

    .prologue
    .line 34
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    iget-object v2, p0, Lcom/google/android/videos/logging/UiElementWrapper;->children:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/logging/UiElementWrapper;

    .line 36
    .local v0, "child":Lcom/google/android/videos/logging/UiElementWrapper;
    invoke-virtual {p1, v0}, Lcom/google/android/videos/logging/UiElementWrapper;->elementEquals(Lcom/google/android/videos/logging/UiElementWrapper;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 37
    const/4 v2, 0x0

    .line 41
    .end local v0    # "child":Lcom/google/android/videos/logging/UiElementWrapper;
    :goto_0
    return v2

    .line 40
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/logging/UiElementWrapper;->children:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 41
    const/4 v2, 0x1

    goto :goto_0
.end method

.method protected clientLogsCookieEquals(Lcom/google/android/videos/logging/UiElementWrapper;)Z
    .locals 8
    .param p1, "other"    # Lcom/google/android/videos/logging/UiElementWrapper;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 93
    iget-object v6, p0, Lcom/google/android/videos/logging/UiElementWrapper;->uiElement:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    iget-object v1, v6, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->clientCookie:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;

    .line 94
    .local v1, "clientLogsCookie":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;
    iget-object v6, p1, Lcom/google/android/videos/logging/UiElementWrapper;->uiElement:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    iget-object v3, v6, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->clientCookie:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;

    .line 95
    .local v3, "otherClientLogsCookie":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;
    if-ne v1, v3, :cond_1

    .line 130
    :cond_0
    :goto_0
    return v4

    .line 98
    :cond_1
    if-eqz v1, :cond_2

    if-nez v3, :cond_3

    :cond_2
    move v4, v5

    .line 99
    goto :goto_0

    .line 102
    :cond_3
    iget v6, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;->offerType:I

    iget v7, v3, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;->offerType:I

    if-eq v6, v7, :cond_4

    move v4, v5

    .line 103
    goto :goto_0

    .line 106
    :cond_4
    iget-object v0, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    .line 107
    .local v0, "clientAssetInfo":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;
    iget-object v2, v3, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    .line 109
    .local v2, "otherAssetInfo":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;
    if-eq v0, v2, :cond_0

    .line 113
    if-eqz v0, :cond_5

    if-nez v2, :cond_6

    :cond_5
    move v4, v5

    .line 114
    goto :goto_0

    .line 117
    :cond_6
    iget v6, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    iget v7, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    if-eq v6, v7, :cond_7

    move v4, v5

    .line 118
    goto :goto_0

    .line 121
    :cond_7
    iget-object v6, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->assetId:Ljava/lang/String;

    iget-object v7, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->assetId:Ljava/lang/String;

    invoke-static {v6, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_8

    move v4, v5

    .line 122
    goto :goto_0

    .line 124
    :cond_8
    iget-object v6, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->seasonId:Ljava/lang/String;

    iget-object v7, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->seasonId:Ljava/lang/String;

    invoke-static {v6, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_9

    move v4, v5

    .line 125
    goto :goto_0

    .line 127
    :cond_9
    iget-object v6, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->showId:Ljava/lang/String;

    iget-object v7, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->showId:Ljava/lang/String;

    invoke-static {v6, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    move v4, v5

    .line 128
    goto :goto_0
.end method

.method protected cloneTo(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;)V
    .locals 1
    .param p1, "dest"    # Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/videos/logging/UiElementWrapper;->uiElement:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    iget-object v0, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->clientCookie:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;

    iput-object v0, p1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->clientCookie:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;

    .line 67
    iget-object v0, p0, Lcom/google/android/videos/logging/UiElementWrapper;->uiElement:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    iget v0, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->type:I

    iput v0, p1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->type:I

    .line 68
    return-void
.end method

.method public deepCloneAndWipeChildren(Lcom/google/android/videos/logging/EventProtoCache;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    .locals 5
    .param p1, "eventProtoCache"    # Lcom/google/android/videos/logging/EventProtoCache;

    .prologue
    .line 52
    invoke-virtual {p1}, Lcom/google/android/videos/logging/EventProtoCache;->obtainUiElement()Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    move-result-object v2

    .line 53
    .local v2, "to":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    invoke-virtual {p0, v2}, Lcom/google/android/videos/logging/UiElementWrapper;->cloneTo(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;)V

    .line 54
    iget v3, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->type:I

    if-lez v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 55
    iget-object v3, p0, Lcom/google/android/videos/logging/UiElementWrapper;->children:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 56
    .local v1, "size":I
    if-nez v1, :cond_1

    invoke-static {}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->emptyArray()[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    move-result-object v3

    :goto_1
    iput-object v3, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->child:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    .line 57
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    if-ge v0, v1, :cond_2

    .line 58
    iget-object v4, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->child:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    iget-object v3, p0, Lcom/google/android/videos/logging/UiElementWrapper;->children:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/logging/UiElementWrapper;

    invoke-virtual {v3, p1}, Lcom/google/android/videos/logging/UiElementWrapper;->deepCloneAndWipeChildren(Lcom/google/android/videos/logging/EventProtoCache;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    move-result-object v3

    aput-object v3, v4, v0

    .line 57
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 54
    .end local v0    # "i":I
    .end local v1    # "size":I
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 56
    .restart local v1    # "size":I
    :cond_1
    new-array v3, v1, [Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    goto :goto_1

    .line 60
    .restart local v0    # "i":I
    :cond_2
    iget-object v3, p0, Lcom/google/android/videos/logging/UiElementWrapper;->children:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 61
    return-object v2
.end method

.method protected elementEquals(Lcom/google/android/videos/logging/UiElementWrapper;)Z
    .locals 4
    .param p1, "other"    # Lcom/google/android/videos/logging/UiElementWrapper;

    .prologue
    const/4 v1, 0x0

    .line 75
    iget-object v0, p1, Lcom/google/android/videos/logging/UiElementWrapper;->uiElement:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    .line 76
    .local v0, "otherUiElement":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    iget-object v2, p0, Lcom/google/android/videos/logging/UiElementWrapper;->uiElement:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    if-ne v2, v0, :cond_1

    .line 77
    const/4 v1, 0x1

    .line 85
    :cond_0
    :goto_0
    return v1

    .line 79
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/logging/UiElementWrapper;->uiElement:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    .line 82
    iget-object v2, p0, Lcom/google/android/videos/logging/UiElementWrapper;->uiElement:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    iget v2, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->type:I

    iget v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->type:I

    if-ne v2, v3, :cond_0

    .line 85
    invoke-virtual {p0, p1}, Lcom/google/android/videos/logging/UiElementWrapper;->clientLogsCookieEquals(Lcom/google/android/videos/logging/UiElementWrapper;)Z

    move-result v1

    goto :goto_0
.end method
