.class public Lcom/google/android/videos/player/exo/UriRewriterDataSource;
.super Ljava/lang/Object;
.source "UriRewriterDataSource.java"

# interfaces
.implements Lcom/google/android/exoplayer/upstream/DataSource;


# instance fields
.field private final dataSource:Lcom/google/android/exoplayer/upstream/DataSource;

.field private final itag:I

.field private final syncStreamRequester:Lcom/google/android/videos/player/exo/SyncVideoStreamRequester;


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/videos/player/exo/SyncVideoStreamRequester;I)V
    .locals 0
    .param p1, "dataSource"    # Lcom/google/android/exoplayer/upstream/DataSource;
    .param p2, "syncStreamRequester"    # Lcom/google/android/videos/player/exo/SyncVideoStreamRequester;
    .param p3, "itag"    # I

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p2, p0, Lcom/google/android/videos/player/exo/UriRewriterDataSource;->syncStreamRequester:Lcom/google/android/videos/player/exo/SyncVideoStreamRequester;

    .line 33
    iput-object p1, p0, Lcom/google/android/videos/player/exo/UriRewriterDataSource;->dataSource:Lcom/google/android/exoplayer/upstream/DataSource;

    .line 34
    iput p3, p0, Lcom/google/android/videos/player/exo/UriRewriterDataSource;->itag:I

    .line 35
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/videos/player/exo/UriRewriterDataSource;->dataSource:Lcom/google/android/exoplayer/upstream/DataSource;

    invoke-interface {v0}, Lcom/google/android/exoplayer/upstream/DataSource;->close()V

    .line 66
    return-void
.end method

.method public open(Lcom/google/android/exoplayer/upstream/DataSpec;)J
    .locals 11
    .param p1, "dataSpec"    # Lcom/google/android/exoplayer/upstream/DataSpec;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42
    :try_start_0
    iget-object v2, p0, Lcom/google/android/videos/player/exo/UriRewriterDataSource;->syncStreamRequester:Lcom/google/android/videos/player/exo/SyncVideoStreamRequester;

    iget v3, p0, Lcom/google/android/videos/player/exo/UriRewriterDataSource;->itag:I

    invoke-virtual {v2, v3}, Lcom/google/android/videos/player/exo/SyncVideoStreamRequester;->getStream(I)Lcom/google/android/videos/streams/MediaStream;

    move-result-object v2

    iget-object v1, v2, Lcom/google/android/videos/streams/MediaStream;->uri:Landroid/net/Uri;

    .line 43
    .local v1, "uri":Landroid/net/Uri;
    if-nez v1, :cond_0

    .line 44
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Stream not found for itag:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/google/android/videos/player/exo/UriRewriterDataSource;->itag:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 46
    .end local v1    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v9

    .line 47
    .local v9, "e":Ljava/io/IOException;
    throw v9

    .line 48
    .end local v9    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v9

    .line 49
    .local v9, "e":Ljava/lang/Exception;
    const-string v2, "Error when fetching online streams"

    invoke-static {v2, v9}, Lcom/google/android/videos/L;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 50
    new-instance v10, Ljava/io/IOException;

    const-string v2, "Could not fetch online streams"

    invoke-direct {v10, v2, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 51
    .local v10, "ioException":Ljava/io/IOException;
    throw v10

    .line 53
    .end local v9    # "e":Ljava/lang/Exception;
    .end local v10    # "ioException":Ljava/io/IOException;
    .restart local v1    # "uri":Landroid/net/Uri;
    :cond_0
    new-instance v0, Lcom/google/android/exoplayer/upstream/DataSpec;

    iget-wide v2, p1, Lcom/google/android/exoplayer/upstream/DataSpec;->absoluteStreamPosition:J

    iget-wide v4, p1, Lcom/google/android/exoplayer/upstream/DataSpec;->length:J

    iget-object v6, p1, Lcom/google/android/exoplayer/upstream/DataSpec;->key:Ljava/lang/String;

    iget-wide v7, p1, Lcom/google/android/exoplayer/upstream/DataSpec;->position:J

    invoke-direct/range {v0 .. v8}, Lcom/google/android/exoplayer/upstream/DataSpec;-><init>(Landroid/net/Uri;JJLjava/lang/String;J)V

    .line 55
    .end local p1    # "dataSpec":Lcom/google/android/exoplayer/upstream/DataSpec;
    .local v0, "dataSpec":Lcom/google/android/exoplayer/upstream/DataSpec;
    iget-object v2, p0, Lcom/google/android/videos/player/exo/UriRewriterDataSource;->dataSource:Lcom/google/android/exoplayer/upstream/DataSource;

    invoke-interface {v2, v0}, Lcom/google/android/exoplayer/upstream/DataSource;->open(Lcom/google/android/exoplayer/upstream/DataSpec;)J

    move-result-wide v2

    return-wide v2
.end method

.method public read([BII)I
    .locals 1
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "readLength"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/videos/player/exo/UriRewriterDataSource;->dataSource:Lcom/google/android/exoplayer/upstream/DataSource;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/exoplayer/upstream/DataSource;->read([BII)I

    move-result v0

    return v0
.end method
