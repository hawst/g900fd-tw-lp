.class Lcom/google/android/videos/async/StoreCachingRequester$StoringCallback;
.super Ljava/lang/Object;
.source "StoreCachingRequester.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/async/StoreCachingRequester;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StoringCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<TR;TE;>;"
    }
.end annotation


# instance fields
.field private final originalRequest:Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pair",
            "<TR;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final targetCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Landroid/util/Pair",
            "<TR;",
            "Ljava/lang/Integer;",
            ">;TE;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/videos/async/StoreCachingRequester;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/async/StoreCachingRequester;Landroid/util/Pair;Lcom/google/android/videos/async/Callback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<TR;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Landroid/util/Pair",
            "<TR;",
            "Ljava/lang/Integer;",
            ">;TE;>;)V"
        }
    .end annotation

    .prologue
    .line 62
    .local p0, "this":Lcom/google/android/videos/async/StoreCachingRequester$StoringCallback;, "Lcom/google/android/videos/async/StoreCachingRequester<TR;TE;>.StoringCallback;"
    .local p2, "originalRequest":Landroid/util/Pair;, "Landroid/util/Pair<TR;Ljava/lang/Integer;>;"
    .local p3, "targetCallback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Landroid/util/Pair<TR;Ljava/lang/Integer;>;TE;>;"
    iput-object p1, p0, Lcom/google/android/videos/async/StoreCachingRequester$StoringCallback;->this$0:Lcom/google/android/videos/async/StoreCachingRequester;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p2, p0, Lcom/google/android/videos/async/StoreCachingRequester$StoringCallback;->originalRequest:Landroid/util/Pair;

    .line 64
    iput-object p3, p0, Lcom/google/android/videos/async/StoreCachingRequester$StoringCallback;->targetCallback:Lcom/google/android/videos/async/Callback;

    .line 65
    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2
    .param p2, "exception"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    .prologue
    .line 83
    .local p0, "this":Lcom/google/android/videos/async/StoreCachingRequester$StoringCallback;, "Lcom/google/android/videos/async/StoreCachingRequester<TR;TE;>.StoringCallback;"
    .local p1, "ignored":Ljava/lang/Object;, "TR;"
    iget-object v0, p0, Lcom/google/android/videos/async/StoreCachingRequester$StoringCallback;->targetCallback:Lcom/google/android/videos/async/Callback;

    iget-object v1, p0, Lcom/google/android/videos/async/StoreCachingRequester$StoringCallback;->originalRequest:Landroid/util/Pair;

    invoke-interface {v0, v1, p2}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 84
    return-void
.end method

.method public onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;TE;)V"
        }
    .end annotation

    .prologue
    .line 70
    .local p0, "this":Lcom/google/android/videos/async/StoreCachingRequester$StoringCallback;, "Lcom/google/android/videos/async/StoreCachingRequester<TR;TE;>.StoringCallback;"
    .local p1, "ignored":Ljava/lang/Object;, "TR;"
    .local p2, "response":Ljava/lang/Object;, "TE;"
    if-eqz p2, :cond_0

    .line 71
    :try_start_0
    iget-object v1, p0, Lcom/google/android/videos/async/StoreCachingRequester$StoringCallback;->this$0:Lcom/google/android/videos/async/StoreCachingRequester;

    # getter for: Lcom/google/android/videos/async/StoreCachingRequester;->fileStore:Lcom/google/android/videos/store/AbstractFileStore;
    invoke-static {v1}, Lcom/google/android/videos/async/StoreCachingRequester;->access$000(Lcom/google/android/videos/async/StoreCachingRequester;)Lcom/google/android/videos/store/AbstractFileStore;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/videos/async/StoreCachingRequester$StoringCallback;->originalRequest:Landroid/util/Pair;

    iget-object v3, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/videos/async/StoreCachingRequester$StoringCallback;->originalRequest:Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v3, v1, p2}, Lcom/google/android/videos/store/AbstractFileStore;->put(Ljava/lang/Object;ILjava/lang/Object;)V

    .line 75
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/async/StoreCachingRequester$StoringCallback;->targetCallback:Lcom/google/android/videos/async/Callback;

    iget-object v2, p0, Lcom/google/android/videos/async/StoreCachingRequester$StoringCallback;->originalRequest:Landroid/util/Pair;

    invoke-interface {v1, v2, p2}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 79
    :goto_1
    return-void

    .line 73
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/async/StoreCachingRequester$StoringCallback;->this$0:Lcom/google/android/videos/async/StoreCachingRequester;

    # getter for: Lcom/google/android/videos/async/StoreCachingRequester;->fileStore:Lcom/google/android/videos/store/AbstractFileStore;
    invoke-static {v1}, Lcom/google/android/videos/async/StoreCachingRequester;->access$000(Lcom/google/android/videos/async/StoreCachingRequester;)Lcom/google/android/videos/store/AbstractFileStore;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/videos/async/StoreCachingRequester$StoringCallback;->originalRequest:Landroid/util/Pair;

    iget-object v3, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/videos/async/StoreCachingRequester$StoringCallback;->originalRequest:Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v3, v1}, Lcom/google/android/videos/store/AbstractFileStore;->remove(Ljava/lang/Object;I)V
    :try_end_0
    .catch Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 76
    :catch_0
    move-exception v0

    .line 77
    .local v0, "e":Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;
    iget-object v1, p0, Lcom/google/android/videos/async/StoreCachingRequester$StoringCallback;->targetCallback:Lcom/google/android/videos/async/Callback;

    iget-object v2, p0, Lcom/google/android/videos/async/StoreCachingRequester$StoringCallback;->originalRequest:Landroid/util/Pair;

    invoke-interface {v1, v2, v0}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_1
.end method
