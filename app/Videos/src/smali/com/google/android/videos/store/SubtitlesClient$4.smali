.class final Lcom/google/android/videos/store/SubtitlesClient$4;
.super Lcom/google/android/videos/store/AbstractFileStore;
.source "SubtitlesClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/store/SubtitlesClient;->getSubtitleTracksStore(Landroid/content/Context;)Lcom/google/android/videos/store/AbstractFileStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/store/AbstractFileStore",
        "<",
        "Ljava/lang/String;",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/videos/subtitles/SubtitleTrack;",
        ">;>;"
    }
.end annotation


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/videos/cache/Converter;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Context;

    .prologue
    .line 149
    .local p2, "x1":Lcom/google/android/videos/cache/Converter;, "Lcom/google/android/videos/cache/Converter<Ljava/util/List<Lcom/google/android/videos/subtitles/SubtitleTrack;>;>;"
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/store/AbstractFileStore;-><init>(Landroid/content/Context;Lcom/google/android/videos/cache/Converter;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic generateFilepath(Ljava/io/File;Ljava/lang/Object;)Ljava/io/File;
    .locals 1
    .param p1, "x0"    # Ljava/io/File;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 149
    check-cast p2, Ljava/lang/String;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/store/SubtitlesClient$4;->generateFilepath(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method protected generateFilepath(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;
    .locals 4
    .param p1, "rootFileDir"    # Ljava/io/File;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 152
    new-instance v0, Ljava/io/File;

    # invokes: Lcom/google/android/videos/store/SubtitlesClient;->getSubtitlesDir(Ljava/io/File;)Ljava/io/File;
    invoke-static {p1}, Lcom/google/android/videos/store/SubtitlesClient;->access$200(Ljava/io/File;)Ljava/io/File;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".cc"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method
