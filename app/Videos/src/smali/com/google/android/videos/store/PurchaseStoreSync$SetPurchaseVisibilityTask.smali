.class Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;
.super Lcom/google/android/videos/store/SyncTaskManager$SyncTask;
.source "PurchaseStoreSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/PurchaseStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SetPurchaseVisibilityTask"
.end annotation


# instance fields
.field private final account:Ljava/lang/String;

.field private final hidden:Z

.field private final itemId:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

.field private final whereClause:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/store/PurchaseStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p2, "priority"    # I
    .param p4, "account"    # Ljava/lang/String;
    .param p5, "itemId"    # Ljava/lang/String;
    .param p6, "whereClause"    # Ljava/lang/String;
    .param p7, "hidden"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/store/SyncTaskManager$SharedStates",
            "<*>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 860
    .local p3, "states":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<*>;"
    iput-object p1, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    .line 861
    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->syncTaskManager:Lcom/google/android/videos/store/SyncTaskManager;
    invoke-static {p1}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$300(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/SyncTaskManager;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;-><init>(Lcom/google/android/videos/store/SyncTaskManager;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V

    .line 862
    iput-object p4, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;->account:Ljava/lang/String;

    .line 863
    iput-object p5, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;->itemId:Ljava/lang/String;

    .line 864
    iput-object p6, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;->whereClause:Ljava/lang/String;

    .line 865
    iput-boolean p7, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;->hidden:Z

    .line 866
    return-void
.end method


# virtual methods
.method protected doSync()V
    .locals 11

    .prologue
    const/4 v4, 0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 870
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 871
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v7, "hidden"

    iget-boolean v8, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;->hidden:Z

    if-eqz v8, :cond_0

    const/4 v4, 0x3

    :cond_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 874
    iget-object v4, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v4}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$100(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 875
    .local v3, "transaction":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v2, 0x0

    .line 876
    .local v2, "success":Z
    const/4 v1, 0x0

    .line 878
    .local v1, "modified":Z
    :try_start_0
    const-string v4, "purchased_assets"

    iget-object v7, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;->whereClause:Ljava/lang/String;

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;->account:Ljava/lang/String;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    iget-object v10, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;->itemId:Ljava/lang/String;

    aput-object v10, v8, v9

    invoke-virtual {v3, v4, v0, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-lez v4, :cond_2

    move v1, v5

    .line 880
    :goto_0
    const/4 v2, 0x1

    .line 882
    iget-object v4, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v4}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$100(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v7

    if-eqz v1, :cond_3

    move v4, v5

    :goto_1
    new-array v5, v5, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;->account:Ljava/lang/String;

    aput-object v8, v5, v6

    invoke-virtual {v7, v3, v2, v4, v5}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 884
    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    iget-boolean v4, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;->hidden:Z

    if-eqz v4, :cond_1

    .line 885
    new-instance v4, Lcom/google/android/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;

    iget-object v5, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    iget v6, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;->priority:I

    iget-object v7, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    iget-object v8, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;->account:Ljava/lang/String;

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/google/android/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;-><init>(Lcom/google/android/videos/store/PurchaseStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/google/android/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->schedule()V

    .line 888
    :cond_1
    return-void

    :cond_2
    move v1, v6

    .line 878
    goto :goto_0

    :cond_3
    move v4, v6

    .line 882
    goto :goto_1

    :catchall_0
    move-exception v4

    move-object v7, v4

    iget-object v4, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v4}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$100(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v8

    if-eqz v1, :cond_5

    move v4, v5

    :goto_2
    new-array v5, v5, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;->account:Ljava/lang/String;

    aput-object v9, v5, v6

    invoke-virtual {v8, v3, v2, v4, v5}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 884
    if-eqz v2, :cond_4

    if-eqz v1, :cond_4

    iget-boolean v4, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;->hidden:Z

    if-eqz v4, :cond_4

    .line 885
    new-instance v4, Lcom/google/android/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;

    iget-object v5, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    iget v6, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;->priority:I

    iget-object v8, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    iget-object v9, p0, Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;->account:Ljava/lang/String;

    invoke-direct {v4, v5, v6, v8, v9}, Lcom/google/android/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;-><init>(Lcom/google/android/videos/store/PurchaseStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/google/android/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->schedule()V

    :cond_4
    throw v7

    :cond_5
    move v4, v6

    .line 882
    goto :goto_2
.end method
