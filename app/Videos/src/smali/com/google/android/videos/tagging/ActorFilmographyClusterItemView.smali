.class public Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;
.super Lcom/google/android/videos/ui/playnext/CardItemView;
.source "ActorFilmographyClusterItemView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$ItemClickListener;,
        Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$Binder;
    }
.end annotation


# instance fields
.field private bitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

.field private itemImage:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

.field private titleView:Landroid/widget/TextView;

.field private wishlistView:Landroid/widget/ImageButton;

.field private yearAndDurationView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 55
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/ui/playnext/CardItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 60
    return-void
.end method

.method private bindWishlist(Lcom/google/android/videos/store/StoreStatusMonitor;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6
    .param p1, "storeStatusMonitor"    # Lcom/google/android/videos/store/StoreStatusMonitor;
    .param p2, "itemName"    # Ljava/lang/String;
    .param p3, "itemId"    # Ljava/lang/String;
    .param p4, "itemType"    # I

    .prologue
    const/16 v2, 0x8

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 74
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 75
    iget-object v1, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->wishlistView:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 93
    :goto_0
    return-void

    .line 78
    :cond_0
    invoke-virtual {p1, p3, p4}, Lcom/google/android/videos/store/StoreStatusMonitor;->getStatus(Ljava/lang/String;I)I

    move-result v0

    .line 79
    .local v0, "status":I
    invoke-static {v0}, Lcom/google/android/videos/store/StoreStatusMonitor;->isPurchased(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 80
    iget-object v1, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->wishlistView:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 83
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->wishlistView:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 84
    invoke-static {v0}, Lcom/google/android/videos/store/StoreStatusMonitor;->isWishlisted(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 85
    iget-object v1, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->wishlistView:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 86
    iget-object v1, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->wishlistView:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->wishlistView:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01b3

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p2, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 89
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->wishlistView:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 90
    iget-object v1, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->wishlistView:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->wishlistView:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01b2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p2, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method bind(Lcom/google/android/videos/store/StoreStatusMonitor;Lcom/google/android/videos/tagging/KnowledgeEntity$Film;Lcom/google/android/videos/async/Requester;Z)V
    .locals 13
    .param p1, "storeStatusMonitor"    # Lcom/google/android/videos/store/StoreStatusMonitor;
    .param p2, "item"    # Lcom/google/android/videos/tagging/KnowledgeEntity$Film;
    .param p4, "isNetworkConnected"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/store/StoreStatusMonitor;",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Film;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 102
    .local p3, "imageRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;>;"
    iget-object v0, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->titleView:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/google/android/videos/tagging/KnowledgeEntity$Film;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    iget-object v0, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b01df

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p2, Lcom/google/android/videos/tagging/KnowledgeEntity$Film;->title:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 105
    const/4 v10, 0x0

    .line 106
    .local v10, "releaseYear":I
    iget-object v0, p2, Lcom/google/android/videos/tagging/KnowledgeEntity$Film;->releaseDate:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/google/android/videos/tagging/KnowledgeEntity$Film;->releaseDate:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    .line 108
    :try_start_0
    iget-object v0, p2, Lcom/google/android/videos/tagging/KnowledgeEntity$Film;->releaseDate:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v10

    .line 114
    :cond_0
    :goto_0
    const/4 v12, 0x0

    .line 115
    .local v12, "yearAndDuration":Ljava/lang/String;
    instance-of v0, p2, Lcom/google/android/videos/tagging/KnowledgeEntity$Movie;

    if-eqz v0, :cond_3

    move-object v9, p2

    .line 116
    check-cast v9, Lcom/google/android/videos/tagging/KnowledgeEntity$Movie;

    .line 117
    .local v9, "movie":Lcom/google/android/videos/tagging/KnowledgeEntity$Movie;
    const v0, 0x3f31a787

    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->setThumbnailAspectRatio(F)V

    .line 118
    iget-object v0, v9, Lcom/google/android/videos/tagging/KnowledgeEntity$Movie;->image:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    iput-object v0, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->itemImage:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    .line 119
    iget v0, v9, Lcom/google/android/videos/tagging/KnowledgeEntity$Movie;->runningTime:I

    if-nez v0, :cond_1

    const/4 v6, 0x0

    .line 121
    .local v6, "duration":Ljava/lang/String;
    :goto_1
    if-nez v10, :cond_2

    move-object v12, v6

    .line 124
    :goto_2
    iget-object v0, v9, Lcom/google/android/videos/tagging/KnowledgeEntity$Movie;->title:Ljava/lang/String;

    iget-object v1, v9, Lcom/google/android/videos/tagging/KnowledgeEntity$Movie;->videoId:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->bindWishlist(Lcom/google/android/videos/store/StoreStatusMonitor;Ljava/lang/String;Ljava/lang/String;I)V

    .line 150
    .end local v6    # "duration":Ljava/lang/String;
    .end local v9    # "movie":Lcom/google/android/videos/tagging/KnowledgeEntity$Movie;
    :goto_3
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 151
    iget-object v0, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->yearAndDurationView:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 156
    :goto_4
    const v1, 0x7f0f00c9

    iget-object v2, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->bitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    move-object v0, p0

    move-object/from16 v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->registerBitmapView(ILcom/google/android/videos/ui/BitmapLoader$BitmapView;Lcom/google/android/videos/async/Requester;Landroid/graphics/Bitmap;Ljava/lang/Class;)V

    .line 157
    if-nez p4, :cond_8

    const/4 v0, 0x1

    :goto_5
    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->setDimmedStyle(Z)V

    .line 158
    return-void

    .line 109
    .end local v12    # "yearAndDuration":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 110
    .local v7, "e":Ljava/lang/NumberFormatException;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error reading the release date \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/videos/tagging/KnowledgeEntity$Film;->releaseDate:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' of film \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/videos/tagging/KnowledgeEntity$Film;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\': "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 119
    .end local v7    # "e":Ljava/lang/NumberFormatException;
    .restart local v9    # "movie":Lcom/google/android/videos/tagging/KnowledgeEntity$Movie;
    .restart local v12    # "yearAndDuration":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0138

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, v9, Lcom/google/android/videos/tagging/KnowledgeEntity$Movie;->runningTime:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    .line 121
    .restart local v6    # "duration":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v10}, Lcom/google/android/videos/utils/TimeUtil;->getStandaloneYearString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v6, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/videos/utils/Util;->buildListString(Landroid/content/res/Resources;Z[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_2

    .end local v6    # "duration":Ljava/lang/String;
    .end local v9    # "movie":Lcom/google/android/videos/tagging/KnowledgeEntity$Movie;
    :cond_3
    move-object v11, p2

    .line 127
    check-cast v11, Lcom/google/android/videos/tagging/KnowledgeEntity$TvShow;

    .line 129
    .local v11, "show":Lcom/google/android/videos/tagging/KnowledgeEntity$TvShow;
    iget-object v0, v11, Lcom/google/android/videos/tagging/KnowledgeEntity$TvShow;->image:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    if-eqz v0, :cond_6

    iget-object v0, v11, Lcom/google/android/videos/tagging/KnowledgeEntity$TvShow;->image:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    iget v0, v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;->originalAspectRatio:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_6

    iget-object v0, v11, Lcom/google/android/videos/tagging/KnowledgeEntity$TvShow;->image:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    iget v0, v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;->originalAspectRatio:F

    :goto_6
    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->setThumbnailAspectRatio(F)V

    .line 131
    iget-object v0, v11, Lcom/google/android/videos/tagging/KnowledgeEntity$TvShow;->image:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    iput-object v0, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->itemImage:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    .line 132
    const-string v8, ""

    .line 133
    .local v8, "endYear":Ljava/lang/String;
    iget-object v0, v11, Lcom/google/android/videos/tagging/KnowledgeEntity$TvShow;->endDate:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 135
    :try_start_1
    iget-object v0, v11, Lcom/google/android/videos/tagging/KnowledgeEntity$TvShow;->endDate:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v8

    .line 143
    :cond_4
    :goto_7
    if-eqz v10, :cond_5

    .line 144
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b01d7

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v8, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 147
    :cond_5
    iget-object v0, v11, Lcom/google/android/videos/tagging/KnowledgeEntity$TvShow;->title:Ljava/lang/String;

    iget-object v1, v11, Lcom/google/android/videos/tagging/KnowledgeEntity$TvShow;->showId:Ljava/lang/String;

    const/16 v2, 0x12

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->bindWishlist(Lcom/google/android/videos/store/StoreStatusMonitor;Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_3

    .line 129
    .end local v8    # "endYear":Ljava/lang/String;
    :cond_6
    const v0, 0x3faaaaab

    goto :goto_6

    .line 136
    .restart local v8    # "endYear":Ljava/lang/String;
    :catch_1
    move-exception v7

    .line 137
    .local v7, "e":Ljava/lang/IndexOutOfBoundsException;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error reading the end date \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v11, Lcom/google/android/videos/tagging/KnowledgeEntity$TvShow;->endDate:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' of TV show \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v11, Lcom/google/android/videos/tagging/KnowledgeEntity$TvShow;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\': "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Ljava/lang/IndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    goto :goto_7

    .line 153
    .end local v7    # "e":Ljava/lang/IndexOutOfBoundsException;
    .end local v8    # "endYear":Ljava/lang/String;
    .end local v11    # "show":Lcom/google/android/videos/tagging/KnowledgeEntity$TvShow;
    :cond_7
    iget-object v0, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->yearAndDurationView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 154
    iget-object v0, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->yearAndDurationView:Landroid/widget/TextView;

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 157
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_5
.end method

.method protected collectClickableViews([Landroid/view/View;)V
    .locals 2
    .param p1, "clickableViews"    # [Landroid/view/View;

    .prologue
    .line 162
    const/4 v0, 0x0

    aput-object p0, p1, v0

    .line 163
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->wishlistView:Landroid/widget/ImageButton;

    aput-object v1, p1, v0

    .line 164
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 64
    invoke-super {p0}, Lcom/google/android/videos/ui/playnext/CardItemView;->onFinishInflate()V

    .line 65
    const v0, 0x7f0f00a8

    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->titleView:Landroid/widget/TextView;

    .line 66
    const v0, 0x7f0f00cb

    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->yearAndDurationView:Landroid/widget/TextView;

    .line 67
    const v0, 0x7f0f00ca

    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->wishlistView:Landroid/widget/ImageButton;

    .line 68
    iget-object v0, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->thumbnailView:Landroid/widget/ImageView;

    const v1, 0x7f02006a

    invoke-static {v0, v1}, Lcom/google/android/videos/ui/BitmapLoader;->createDefaultBitmapView(Landroid/widget/ImageView;I)Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->bitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

    .line 70
    return-void
.end method

.method protected onGenerateBitmapViewRequest(ILjava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .param p1, "id"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Q:",
            "Ljava/lang/Object;",
            ">(I",
            "Ljava/lang/Class",
            "<TQ;>;)TQ;"
        }
    .end annotation

    .prologue
    .line 97
    .local p2, "requestType":Ljava/lang/Class;, "Ljava/lang/Class<TQ;>;"
    iget-object v0, p0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView;->itemImage:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
