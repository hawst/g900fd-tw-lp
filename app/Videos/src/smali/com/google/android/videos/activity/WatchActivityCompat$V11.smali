.class Lcom/google/android/videos/activity/WatchActivityCompat$V11;
.super Lcom/google/android/videos/activity/WatchActivityCompat;
.source "WatchActivityCompat.java"

# interfaces
.implements Landroid/support/v7/app/ActionBar$OnMenuVisibilityListener;
.implements Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper$Listener;
.implements Lcom/google/android/videos/utils/DockReceiver$DockListener;
.implements Lcom/google/android/videos/utils/HdmiReceiver$HdmiListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/activity/WatchActivityCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "V11"
.end annotation


# instance fields
.field private actionBar:Landroid/support/v7/app/ActionBar;

.field protected final activity:Lcom/google/android/videos/activity/WatchActivity;

.field private final backgroundColorDrawable:Landroid/graphics/drawable/ColorDrawable;

.field private final dockReceiver:Lcom/google/android/videos/utils/DockReceiver;

.field private fullscreenUiVisibilityHelper:Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;

.field private final hdmiReceiver:Lcom/google/android/videos/utils/HdmiReceiver;

.field private isMenuVisible:Z

.field private isUserInteractionExpected:Z

.field private isVideoPlaying:Z

.field private final window:Landroid/view/Window;

.field private zoomHelper:Lcom/google/android/videos/utils/ZoomHelper;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/activity/WatchActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/google/android/videos/activity/WatchActivity;

    .prologue
    .line 219
    invoke-direct {p0}, Lcom/google/android/videos/activity/WatchActivityCompat;-><init>()V

    .line 220
    iput-object p1, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/videos/activity/WatchActivity;

    .line 221
    new-instance v0, Lcom/google/android/videos/utils/HdmiReceiver;

    invoke-direct {v0, p1, p0}, Lcom/google/android/videos/utils/HdmiReceiver;-><init>(Landroid/content/Context;Lcom/google/android/videos/utils/HdmiReceiver$HdmiListener;)V

    iput-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->hdmiReceiver:Lcom/google/android/videos/utils/HdmiReceiver;

    .line 222
    new-instance v0, Lcom/google/android/videos/utils/DockReceiver;

    invoke-direct {v0, p1, p0}, Lcom/google/android/videos/utils/DockReceiver;-><init>(Landroid/content/Context;Lcom/google/android/videos/utils/DockReceiver$DockListener;)V

    iput-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->dockReceiver:Lcom/google/android/videos/utils/DockReceiver;

    .line 223
    invoke-virtual {p1}, Lcom/google/android/videos/activity/WatchActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->window:Landroid/view/Window;

    .line 224
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->window:Landroid/view/Window;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    .line 225
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/high16 v1, -0x1000000

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->backgroundColorDrawable:Landroid/graphics/drawable/ColorDrawable;

    .line 226
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->backgroundColorDrawable:Landroid/graphics/drawable/ColorDrawable;

    const/16 v1, 0x61

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/ColorDrawable;->setAlpha(I)V

    .line 227
    return-void
.end method


# virtual methods
.method protected isConnectedToExternalDisplay()Z
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->hdmiReceiver:Lcom/google/android/videos/utils/HdmiReceiver;

    invoke-virtual {v0}, Lcom/google/android/videos/utils/HdmiReceiver;->isHdmiPlugged()Z

    move-result v0

    return v0
.end method

.method protected lockOrientation()Z
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->dockReceiver:Lcom/google/android/videos/utils/DockReceiver;

    invoke-virtual {v0}, Lcom/google/android/videos/utils/DockReceiver;->getDockState()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/videos/activity/WatchActivity;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/WatchActivity;->isConnectedToRemote()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCardsViewScrollChanged(I)V
    .locals 2
    .param p1, "verticalScrollOrigin"    # I

    .prologue
    .line 345
    if-lez p1, :cond_0

    .line 346
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->backgroundColorDrawable:Landroid/graphics/drawable/ColorDrawable;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/ColorDrawable;->setAlpha(I)V

    .line 350
    :goto_0
    return-void

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->backgroundColorDrawable:Landroid/graphics/drawable/ColorDrawable;

    const/16 v1, 0x61

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/ColorDrawable;->setAlpha(I)V

    goto :goto_0
.end method

.method public onCreate()V
    .locals 5

    .prologue
    .line 232
    iget-object v2, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/videos/activity/WatchActivity;

    invoke-virtual {v2}, Lcom/google/android/videos/activity/WatchActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->actionBar:Landroid/support/v7/app/ActionBar;

    .line 233
    iget-object v2, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->actionBar:Landroid/support/v7/app/ActionBar;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/support/v7/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 234
    iget-object v2, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->actionBar:Landroid/support/v7/app/ActionBar;

    iget-object v3, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->backgroundColorDrawable:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v2, v3}, Landroid/support/v7/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 235
    iget-object v2, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/videos/activity/WatchActivity;

    iget-object v3, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->actionBar:Landroid/support/v7/app/ActionBar;

    iget-object v4, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->backgroundColorDrawable:Landroid/graphics/drawable/ColorDrawable;

    invoke-static {v2, v3, v4}, Lcom/google/android/videos/ui/DogfoodHelper;->addPawsIfNeeded(Landroid/content/Context;Landroid/support/v7/app/ActionBar;Landroid/graphics/drawable/Drawable;)V

    .line 236
    iget-object v2, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->actionBar:Landroid/support/v7/app/ActionBar;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/support/v7/app/ActionBar;->setNavigationMode(I)V

    .line 237
    const/16 v0, 0x1c

    .line 239
    .local v0, "displayOptions":I
    iget-object v2, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->actionBar:Landroid/support/v7/app/ActionBar;

    invoke-virtual {v2, v0, v0}, Landroid/support/v7/app/ActionBar;->setDisplayOptions(II)V

    .line 240
    iget-object v2, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->actionBar:Landroid/support/v7/app/ActionBar;

    invoke-virtual {v2, p0}, Landroid/support/v7/app/ActionBar;->addOnMenuVisibilityListener(Landroid/support/v7/app/ActionBar$OnMenuVisibilityListener;)V

    .line 242
    iget-object v2, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/videos/activity/WatchActivity;

    const v3, 0x7f0f020f

    invoke-virtual {v2, v3}, Lcom/google/android/videos/activity/WatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/player/PlayerView;

    .line 243
    .local v1, "playerView":Lcom/google/android/videos/player/PlayerView;
    new-instance v2, Lcom/google/android/videos/utils/ZoomHelper;

    iget-object v3, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/videos/activity/WatchActivity;

    invoke-virtual {v1}, Lcom/google/android/videos/player/PlayerView;->getPlayerSurface()Lcom/google/android/videos/player/PlayerSurface;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/google/android/videos/utils/ZoomHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/player/PlayerSurface;)V

    iput-object v2, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->zoomHelper:Lcom/google/android/videos/utils/ZoomHelper;

    .line 244
    new-instance v2, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;

    iget-object v3, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/videos/activity/WatchActivity;

    invoke-virtual {v3}, Lcom/google/android/videos/activity/WatchActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->actionBar:Landroid/support/v7/app/ActionBar;

    invoke-direct {v2, v3, v4, v1, p0}, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;-><init>(Landroid/view/Window;Landroid/support/v7/app/ActionBar;Lcom/google/android/videos/player/PlayerView;Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper$Listener;)V

    iput-object v2, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->fullscreenUiVisibilityHelper:Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;

    .line 246
    iget-object v2, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->fullscreenUiVisibilityHelper:Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->setFullscreen(Z)V

    .line 247
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 289
    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->zoomHelper:Lcom/google/android/videos/utils/ZoomHelper;

    iget-boolean v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->isVideoPlaying:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->isConnectedToExternalDisplay()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/videos/activity/WatchActivity;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/WatchActivity;->isConnectedToRemote()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, p1, p2, v0}, Lcom/google/android/videos/utils/ZoomHelper;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;Z)V

    .line 292
    return-void

    .line 289
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->actionBar:Landroid/support/v7/app/ActionBar;

    invoke-virtual {v0, p0}, Landroid/support/v7/app/ActionBar;->removeOnMenuVisibilityListener(Landroid/support/v7/app/ActionBar$OnMenuVisibilityListener;)V

    .line 282
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->fullscreenUiVisibilityHelper:Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->release()V

    .line 283
    return-void
.end method

.method public onDockState(I)V
    .locals 0
    .param p1, "dockState"    # I

    .prologue
    .line 365
    invoke-virtual {p0}, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->updateOrientation()V

    .line 366
    return-void
.end method

.method public onHdmiPluggedState(Z)V
    .locals 1
    .param p1, "plugged"    # Z

    .prologue
    .line 359
    invoke-virtual {p0}, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->updateScreenBrightness()V

    .line 360
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/videos/activity/WatchActivity;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/WatchActivity;->supportInvalidateOptionsMenu()V

    .line 361
    return-void
.end method

.method public onKnowledgeEnteredFullScreen()V
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->actionBar:Landroid/support/v7/app/ActionBar;

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->hide()V

    .line 402
    return-void
.end method

.method public onKnowledgeExitedFullScreen()V
    .locals 1

    .prologue
    .line 406
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->actionBar:Landroid/support/v7/app/ActionBar;

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->show()V

    .line 407
    return-void
.end method

.method public onMenuVisibilityChanged(Z)V
    .locals 2
    .param p1, "isVisible"    # Z

    .prologue
    .line 307
    iput-boolean p1, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->isMenuVisible:Z

    .line 308
    invoke-virtual {p0}, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->updateScreenBrightness()V

    .line 309
    iget-boolean v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->isUserInteractionExpected:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->isMenuVisible:Z

    if-nez v0, :cond_0

    .line 310
    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->fullscreenUiVisibilityHelper:Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;

    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/videos/activity/WatchActivity;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/WatchActivity;->isConnectedToRemote()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->setSystemUiHidden(Z)V

    .line 312
    :cond_0
    return-void

    .line 310
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onNavigationShown()V
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/videos/activity/WatchActivity;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/WatchActivity;->showControls()V

    .line 355
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v0, 0x1

    .line 296
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_1

    .line 297
    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/videos/activity/WatchActivity;

    invoke-virtual {v1}, Lcom/google/android/videos/activity/WatchActivity;->onUpPressed()V

    .line 302
    :cond_0
    :goto_0
    return v0

    .line 299
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->zoomHelper:Lcom/google/android/videos/utils/ZoomHelper;

    invoke-virtual {v1, p1}, Lcom/google/android/videos/utils/ZoomHelper;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 302
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPlaybackPaused()V
    .locals 1

    .prologue
    .line 273
    iget-boolean v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->isVideoPlaying:Z

    if-eqz v0, :cond_0

    .line 274
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->isVideoPlaying:Z

    .line 275
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/videos/activity/WatchActivity;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/WatchActivity;->supportInvalidateOptionsMenu()V

    .line 277
    :cond_0
    return-void
.end method

.method public onPlaybackStarted()V
    .locals 1

    .prologue
    .line 265
    iget-boolean v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->isVideoPlaying:Z

    if-nez v0, :cond_0

    .line 266
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->isVideoPlaying:Z

    .line 267
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/videos/activity/WatchActivity;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/WatchActivity;->supportInvalidateOptionsMenu()V

    .line 269
    :cond_0
    return-void
.end method

.method public onRemoteControlChanged()V
    .locals 1

    .prologue
    .line 370
    invoke-virtual {p0}, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->updateOrientation()V

    .line 371
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/videos/activity/WatchActivity;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/WatchActivity;->supportInvalidateOptionsMenu()V

    .line 372
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->hdmiReceiver:Lcom/google/android/videos/utils/HdmiReceiver;

    invoke-virtual {v0}, Lcom/google/android/videos/utils/HdmiReceiver;->register()V

    .line 252
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->dockReceiver:Lcom/google/android/videos/utils/DockReceiver;

    invoke-virtual {v0}, Lcom/google/android/videos/utils/DockReceiver;->register()V

    .line 253
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->hdmiReceiver:Lcom/google/android/videos/utils/HdmiReceiver;

    invoke-virtual {v0}, Lcom/google/android/videos/utils/HdmiReceiver;->unregister()V

    .line 258
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->dockReceiver:Lcom/google/android/videos/utils/DockReceiver;

    invoke-virtual {v0}, Lcom/google/android/videos/utils/DockReceiver;->unregister()V

    .line 259
    invoke-virtual {p0}, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->updateScreenBrightness()V

    .line 260
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->fullscreenUiVisibilityHelper:Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->setSystemUiHidden(Z)V

    .line 261
    return-void
.end method

.method public onUserInteractionEnding()V
    .locals 2

    .prologue
    .line 338
    iget-boolean v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->isMenuVisible:Z

    if-nez v0, :cond_0

    .line 339
    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->fullscreenUiVisibilityHelper:Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;

    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/videos/activity/WatchActivity;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/WatchActivity;->isConnectedToRemote()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->setSystemUiHidden(Z)V

    .line 341
    :cond_0
    return-void

    .line 339
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onUserInteractionExpected()V
    .locals 2

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/videos/activity/WatchActivity;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/WatchActivity;->supportInvalidateOptionsMenu()V

    .line 331
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->isUserInteractionExpected:Z

    .line 332
    invoke-virtual {p0}, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->updateScreenBrightness()V

    .line 333
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->fullscreenUiVisibilityHelper:Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->setSystemUiHidden(Z)V

    .line 334
    return-void
.end method

.method public onUserInteractionNotExpected()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 321
    iput-boolean v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->isUserInteractionExpected:Z

    .line 322
    invoke-virtual {p0}, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->updateScreenBrightness()V

    .line 323
    iget-boolean v1, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->isMenuVisible:Z

    if-nez v1, :cond_1

    .line 324
    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->fullscreenUiVisibilityHelper:Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;

    iget-object v2, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/videos/activity/WatchActivity;

    invoke-virtual {v2}, Lcom/google/android/videos/activity/WatchActivity;->isConnectedToRemote()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v1, v0}, Lcom/google/android/videos/ui/FullscreenUiVisibilityHelper;->setSystemUiHidden(Z)V

    .line 326
    :cond_1
    return-void
.end method

.method public onVideoTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 316
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->actionBar:Landroid/support/v7/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/support/v7/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 317
    return-void
.end method

.method protected final updateOrientation()V
    .locals 2

    .prologue
    .line 375
    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/videos/activity/WatchActivity;

    invoke-virtual {p0}, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->lockOrientation()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/videos/activity/WatchActivity;->setRequestedOrientation(I)V

    .line 378
    return-void

    .line 375
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method protected final updateScreenBrightness()V
    .locals 4

    .prologue
    .line 386
    iget-boolean v3, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->isUserInteractionExpected:Z

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->isMenuVisible:Z

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->isConnectedToExternalDisplay()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    .line 388
    .local v2, "dimScreen":Z
    :goto_0
    if-eqz v2, :cond_1

    const v1, 0x3c23d70a    # 0.01f

    .line 390
    .local v1, "brightness":F
    :goto_1
    iget-object v3, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->window:Landroid/view/Window;

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 391
    .local v0, "attributes":Landroid/view/WindowManager$LayoutParams;
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    .line 392
    iget-object v3, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->window:Landroid/view/Window;

    invoke-virtual {v3, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 393
    return-void

    .line 386
    .end local v0    # "attributes":Landroid/view/WindowManager$LayoutParams;
    .end local v1    # "brightness":F
    .end local v2    # "dimScreen":Z
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 388
    .restart local v2    # "dimScreen":Z
    :cond_1
    const/high16 v1, -0x40800000    # -1.0f

    goto :goto_1
.end method
