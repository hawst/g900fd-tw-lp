.class public final Lcom/google/android/videos/ui/WatchNowHelper;
.super Lcom/google/android/videos/store/Database$BaseListener;
.source "WatchNowHelper.java"

# interfaces
.implements Lcom/google/android/repolib/observers/Updatable;
.implements Lcom/google/android/videos/activity/HomeFragment$HomeFragmentHelper;
.implements Lcom/google/android/videos/ui/CursorHelper$Listener;
.implements Lcom/google/android/videos/ui/SyncHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;,
        Lcom/google/android/videos/ui/WatchNowHelper$SeeMoreOnClickListener;,
        Lcom/google/android/videos/ui/WatchNowHelper$SuggestionItemClickListener;,
        Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;
    }
.end annotation


# instance fields
.field private final activity:Lcom/google/android/videos/activity/HomeActivity;

.field private final config:Lcom/google/android/videos/Config;

.field private final configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

.field private final cursorHelpers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/videos/ui/CursorHelper",
            "<*>;",
            "Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;",
            ">;"
        }
    .end annotation
.end field

.field private final database:Lcom/google/android/videos/store/Database;

.field private final eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private final listView:Landroid/support/v7/widget/RecyclerView;

.field private final maxVideosPerSuggestionCluster:I

.field private final networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

.field private final newMoviesCursorHelper:Lcom/google/android/videos/ui/CursorHelper$NewMoviesCursorHelper;

.field private final newMoviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

.field private final newTvCursorHelper:Lcom/google/android/videos/ui/CursorHelper$WatchNowEpisodesCursorHelper;

.field private final newTvDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

.field private pendingScreenshotUpdatedNotification:Z

.field private pendingVideoSuggestions:[Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;

.field private final pinHelper:Lcom/google/android/videos/ui/PinHelper;

.field private final progressBar:Landroid/view/View;

.field private final storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

.field private final suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

.field private suggestionsAccount:Ljava/lang/String;

.field private final suggestionsEnabled:Z

.field private final syncHelper:Lcom/google/android/videos/ui/SyncHelper;

.field private final uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

.field private final unpurchasedEpisodesHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

.field private final videoCollectionsHelper:Lcom/google/android/videos/ui/VideoCollectionHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/ui/VideoCollectionHelper",
            "<",
            "Lcom/google/android/videos/api/VideoCollectionListRequest;",
            ">;"
        }
    .end annotation
.end field

.field private final videoSuggestionsDataSourceUpdater:Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;

.field private final watchNowDataSource:Lcom/google/android/videos/adapter/WatchNowDataSource;

.field private final watchNowFlowHelper:Lcom/google/android/videos/ui/WatchNowFlowHelper;

.field private final watchNowRecommendationDataSource:Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;

.field private final welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/Config;Lcom/google/android/videos/store/ConfigurationStore;Landroid/content/SharedPreferences;Lcom/google/android/videos/activity/HomeActivity;Landroid/view/View;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/PurchaseStoreSync;Lcom/google/android/videos/store/PosterStore;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/utils/ErrorHelper;Lcom/google/android/videos/ui/SyncHelper;Lcom/google/android/videos/ui/PinHelper;Lcom/google/android/videos/bitmap/BitmapRequesters;Ljava/util/concurrent/Executor;Lcom/google/android/videos/api/ApiRequesters;Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;Lcom/google/android/videos/remote/MediaRouteProvider;Lcom/google/android/videos/remote/RemoteTracker;Lcom/google/android/videos/utils/DownloadedOnlyManager;ZLcom/google/android/videos/logging/UiElementNode;Lcom/google/android/videos/logging/UiEventLoggingHelper;Lcom/google/android/videos/ui/AssetImageUriCreator;Lcom/google/android/videos/utils/NetworkStatus;)V
    .locals 24
    .param p1, "config"    # Lcom/google/android/videos/Config;
    .param p2, "configurationStore"    # Lcom/google/android/videos/store/ConfigurationStore;
    .param p3, "preferences"    # Landroid/content/SharedPreferences;
    .param p4, "activity"    # Lcom/google/android/videos/activity/HomeActivity;
    .param p5, "moviesView"    # Landroid/view/View;
    .param p6, "database"    # Lcom/google/android/videos/store/Database;
    .param p7, "purchaseStore"    # Lcom/google/android/videos/store/PurchaseStore;
    .param p8, "purchaseStoreSync"    # Lcom/google/android/videos/store/PurchaseStoreSync;
    .param p9, "posterStore"    # Lcom/google/android/videos/store/PosterStore;
    .param p10, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;
    .param p11, "errorHelper"    # Lcom/google/android/videos/utils/ErrorHelper;
    .param p12, "syncHelper"    # Lcom/google/android/videos/ui/SyncHelper;
    .param p13, "pinHelper"    # Lcom/google/android/videos/ui/PinHelper;
    .param p14, "bitmapRequesters"    # Lcom/google/android/videos/bitmap/BitmapRequesters;
    .param p15, "cpuExecutor"    # Ljava/util/concurrent/Executor;
    .param p16, "apiRequesters"    # Lcom/google/android/videos/api/ApiRequesters;
    .param p17, "suggestionOverflowMenuHelper"    # Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;
    .param p18, "mediaRouteProvider"    # Lcom/google/android/videos/remote/MediaRouteProvider;
    .param p19, "remoteTracker"    # Lcom/google/android/videos/remote/RemoteTracker;
    .param p20, "downloadedOnlyManager"    # Lcom/google/android/videos/utils/DownloadedOnlyManager;
    .param p21, "allowDownloads"    # Z
    .param p22, "parentNode"    # Lcom/google/android/videos/logging/UiElementNode;
    .param p23, "uiEventLoggingHelper"    # Lcom/google/android/videos/logging/UiEventLoggingHelper;
    .param p24, "imageUriCreator"    # Lcom/google/android/videos/ui/AssetImageUriCreator;
    .param p25, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;

    .prologue
    .line 142
    invoke-direct/range {p0 .. p0}, Lcom/google/android/videos/store/Database$BaseListener;-><init>()V

    .line 144
    invoke-static/range {p22 .. p22}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    invoke-static/range {p1 .. p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/videos/Config;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->config:Lcom/google/android/videos/Config;

    .line 146
    invoke-static/range {p2 .. p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/videos/store/ConfigurationStore;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    .line 147
    invoke-static/range {p4 .. p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/videos/activity/HomeActivity;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    .line 148
    invoke-static/range {p6 .. p6}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/videos/store/Database;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->database:Lcom/google/android/videos/store/Database;

    .line 149
    invoke-static/range {p10 .. p10}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/videos/logging/EventLogger;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 150
    invoke-static/range {p12 .. p12}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/videos/ui/SyncHelper;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    .line 151
    invoke-static/range {p13 .. p13}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/videos/ui/PinHelper;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->pinHelper:Lcom/google/android/videos/ui/PinHelper;

    .line 152
    invoke-static/range {p17 .. p17}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    .line 153
    invoke-static/range {p23 .. p23}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/videos/logging/UiEventLoggingHelper;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    .line 154
    invoke-static/range {p25 .. p25}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/videos/utils/NetworkStatus;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 156
    invoke-interface/range {p1 .. p1}, Lcom/google/android/videos/Config;->maxSuggestionsPerCluster()I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->maxVideosPerSuggestionCluster:I

    .line 164
    invoke-interface/range {p1 .. p1}, Lcom/google/android/videos/Config;->suggestionsEnabled()Z

    move-result v5

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->suggestionsEnabled:Z

    .line 167
    const v5, 0x7f0f0120

    move-object/from16 v0, p5

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->progressBar:Landroid/view/View;

    .line 169
    const v5, 0x7f0f0029

    move-object/from16 v0, p5

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/support/v7/widget/RecyclerView;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    .line 171
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/videos/activity/HomeActivity;->getStoreStatusMonitor()Lcom/google/android/videos/store/StoreStatusMonitor;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    .line 173
    new-instance v5, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;

    invoke-direct {v5}, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;-><init>()V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->watchNowRecommendationDataSource:Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;

    .line 174
    new-instance v5, Lcom/google/android/videos/ui/WatchNowHelper$1;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/ui/WatchNowHelper;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/videos/ui/WatchNowHelper;->maxVideosPerSuggestionCluster:I

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v5, v0, v1, v6, v7}, Lcom/google/android/videos/ui/WatchNowHelper$1;-><init>(Lcom/google/android/videos/ui/WatchNowHelper;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/store/StoreStatusMonitor;I)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->videoSuggestionsDataSourceUpdater:Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;

    .line 185
    new-instance v4, Lcom/google/android/videos/ui/VideoCollectionHelper;

    invoke-interface/range {p16 .. p16}, Lcom/google/android/videos/api/ApiRequesters;->getVideoCollectionListRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/videos/ui/WatchNowHelper;->videoSuggestionsDataSourceUpdater:Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;

    move-object/from16 v5, p4

    move-object/from16 v8, p10

    move-object/from16 v9, p11

    invoke-direct/range {v4 .. v9}, Lcom/google/android/videos/ui/VideoCollectionHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/ui/VideoCollectionHelper$Listener;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/utils/ErrorHelper;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/ui/WatchNowHelper;->videoCollectionsHelper:Lcom/google/android/videos/ui/VideoCollectionHelper;

    .line 192
    new-instance v5, Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-direct {v5}, Lcom/google/android/videos/adapter/MoviesDataSource;-><init>()V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->newMoviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    .line 193
    new-instance v5, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-direct {v5}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;-><init>()V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->newTvDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    .line 194
    new-instance v5, Lcom/google/android/videos/adapter/WatchNowDataSource;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/ui/WatchNowHelper;->newMoviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/videos/ui/WatchNowHelper;->newTvDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    move-object/from16 v0, p19

    invoke-direct {v5, v0, v6, v7}, Lcom/google/android/videos/adapter/WatchNowDataSource;-><init>(Lcom/google/android/videos/remote/RemoteTracker;Lcom/google/android/videos/adapter/MoviesDataSource;Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->watchNowDataSource:Lcom/google/android/videos/adapter/WatchNowDataSource;

    .line 196
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->newTvDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-interface/range {p16 .. p16}, Lcom/google/android/videos/api/ApiRequesters;->getAssetsCachingRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v6

    move-object/from16 v0, p4

    invoke-static {v0, v5, v6}, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->createForEpisodes(Landroid/app/Activity;Lcom/google/android/videos/adapter/DataSource;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->unpurchasedEpisodesHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    .line 199
    invoke-virtual/range {p4 .. p4}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e0100

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v21

    .line 202
    .local v21, "welcomeFlowSpacing":I
    new-instance v17, Lcom/google/android/videos/welcome/WelcomeFlow;

    invoke-interface/range {p14 .. p14}, Lcom/google/android/videos/bitmap/BitmapRequesters;->getBitmapRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v19

    const/4 v5, 0x5

    new-array v0, v5, [Lcom/google/android/videos/welcome/Welcome;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    new-instance v4, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;

    const-string v5, "watchnow"

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/ui/WatchNowHelper;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    invoke-interface/range {p16 .. p16}, Lcom/google/android/videos/api/ApiRequesters;->getPromotionsRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v14

    invoke-interface/range {p16 .. p16}, Lcom/google/android/videos/api/ApiRequesters;->getRedeemPromotionRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v15

    const/16 v16, 0x1

    move-object/from16 v6, p4

    move-object/from16 v7, p1

    move-object/from16 v8, p3

    move-object/from16 v9, p2

    move-object/from16 v10, p11

    move-object/from16 v11, p10

    move-object/from16 v13, p8

    invoke-direct/range {v4 .. v16}, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/videos/Config;Landroid/content/SharedPreferences;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/utils/ErrorHelper;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/store/StoreStatusMonitor;Lcom/google/android/videos/store/PurchaseStoreSync;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Z)V

    aput-object v4, v22, v23

    const/4 v10, 0x1

    new-instance v4, Lcom/google/android/videos/welcome/CastPromoWelcome;

    const-string v5, "watchnow"

    move-object/from16 v6, p4

    move-object/from16 v7, p3

    move-object/from16 v8, p2

    move-object/from16 v9, p18

    invoke-direct/range {v4 .. v9}, Lcom/google/android/videos/welcome/CastPromoWelcome;-><init>(Ljava/lang/String;Landroid/app/Activity;Landroid/content/SharedPreferences;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/remote/MediaRouteProvider;)V

    aput-object v4, v22, v10

    const/4 v5, 0x2

    new-instance v6, Lcom/google/android/videos/welcome/KnowledgePromoWelcome;

    const-string v7, "watchnow"

    move-object/from16 v0, p4

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-direct {v6, v7, v0, v1, v2}, Lcom/google/android/videos/welcome/KnowledgePromoWelcome;-><init>(Ljava/lang/String;Landroid/app/Activity;Lcom/google/android/videos/Config;Landroid/content/SharedPreferences;)V

    aput-object v6, v22, v5

    const/4 v5, 0x3

    new-instance v6, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;

    move-object/from16 v0, p4

    move-object/from16 v1, p2

    invoke-direct {v6, v0, v1}, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;-><init>(Lcom/google/android/videos/activity/HomeActivity;Lcom/google/android/videos/store/ConfigurationStore;)V

    aput-object v6, v22, v5

    const/4 v5, 0x4

    new-instance v6, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;

    move-object/from16 v0, p4

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v6, v0, v1, v2}, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;-><init>(Landroid/app/Activity;Lcom/google/android/videos/Config;Lcom/google/android/videos/store/ConfigurationStore;)V

    aput-object v6, v22, v5

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    move/from16 v2, v21

    move-object/from16 v3, v22

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/videos/welcome/WelcomeFlow;-><init>(Lcom/google/android/videos/async/Requester;I[Lcom/google/android/videos/welcome/Welcome;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/ui/WatchNowHelper;->welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;

    .line 216
    new-instance v4, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView$Binder;

    invoke-interface/range {p14 .. p14}, Lcom/google/android/videos/bitmap/BitmapRequesters;->getBitmapRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v5

    invoke-static/range {p4 .. p4}, Lcom/google/android/videos/utils/PlayStoreUtil;->supportsDirectPurchases(Landroid/content/Context;)Z

    move-result v7

    new-instance v8, Lcom/google/android/videos/logging/GenericUiElementNode;

    const/16 v6, 0xb

    move-object/from16 v0, p22

    move-object/from16 v1, p23

    invoke-direct {v8, v6, v0, v1}, Lcom/google/android/videos/logging/GenericUiElementNode;-><init>(ILcom/google/android/videos/logging/UiElementNode;Lcom/google/android/videos/logging/UiEventLoggingHelper;)V

    move-object/from16 v6, p15

    move-object/from16 v9, p24

    invoke-direct/range {v4 .. v9}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView$Binder;-><init>(Lcom/google/android/videos/async/Requester;Ljava/util/concurrent/Executor;ZLcom/google/android/videos/logging/UiElementNode;Lcom/google/android/videos/ui/AssetImageUriCreator;)V

    .line 223
    .local v4, "watchNowRecommendationBinder":Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView$Binder;
    new-instance v18, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView$Binder;

    const/4 v5, 0x2

    move-object/from16 v0, p9

    invoke-virtual {v0, v5}, Lcom/google/android/videos/store/PosterStore;->getRequester(I)Lcom/google/android/videos/async/Requester;

    move-result-object v5

    new-instance v6, Lcom/google/android/videos/logging/GenericUiElementNode;

    const/16 v7, 0x1e

    move-object/from16 v0, p22

    move-object/from16 v1, p23

    invoke-direct {v6, v7, v0, v1}, Lcom/google/android/videos/logging/GenericUiElementNode;-><init>(ILcom/google/android/videos/logging/UiElementNode;Lcom/google/android/videos/logging/UiEventLoggingHelper;)V

    move-object/from16 v0, v18

    move/from16 v1, p21

    invoke-direct {v0, v5, v1, v6}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView$Binder;-><init>(Lcom/google/android/videos/async/Requester;ZLcom/google/android/videos/logging/UiElementNode;)V

    .line 230
    .local v18, "watchNowBinder":Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView$Binder;
    new-instance v8, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-direct {v8, v5}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 231
    .local v8, "uiHandler":Landroid/os/Handler;
    new-instance v5, Lcom/google/android/videos/ui/CursorHelper$WatchNowEpisodesCursorHelper;

    const/4 v12, 0x1

    const/4 v13, 0x0

    const/4 v14, 0x1

    move-object/from16 v6, p4

    move-object/from16 v7, p1

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    move-object/from16 v11, p12

    move-object/from16 v15, p20

    invoke-direct/range {v5 .. v15}, Lcom/google/android/videos/ui/CursorHelper$WatchNowEpisodesCursorHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/Config;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/ui/SyncHelper;IZZLcom/google/android/videos/utils/DownloadedOnlyManager;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->newTvCursorHelper:Lcom/google/android/videos/ui/CursorHelper$WatchNowEpisodesCursorHelper;

    .line 237
    new-instance v5, Lcom/google/android/videos/ui/CursorHelper$NewMoviesCursorHelper;

    move-object/from16 v6, p4

    move-object/from16 v7, p1

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    move-object/from16 v11, p12

    move-object/from16 v12, p20

    invoke-direct/range {v5 .. v12}, Lcom/google/android/videos/ui/CursorHelper$NewMoviesCursorHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/Config;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/ui/SyncHelper;Lcom/google/android/videos/utils/DownloadedOnlyManager;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->newMoviesCursorHelper:Lcom/google/android/videos/ui/CursorHelper$NewMoviesCursorHelper;

    .line 240
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->cursorHelpers:Ljava/util/Map;

    .line 241
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->cursorHelpers:Ljava/util/Map;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/ui/WatchNowHelper;->newMoviesCursorHelper:Lcom/google/android/videos/ui/CursorHelper$NewMoviesCursorHelper;

    new-instance v7, Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/videos/ui/WatchNowHelper;->newMoviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    const/4 v10, 0x0

    invoke-direct {v7, v9, v10}, Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;-><init>(Lcom/google/android/videos/adapter/VideosDataSource;Lcom/google/android/videos/ui/WatchNowHelper$1;)V

    invoke-interface {v5, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->cursorHelpers:Ljava/util/Map;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/ui/WatchNowHelper;->newTvCursorHelper:Lcom/google/android/videos/ui/CursorHelper$WatchNowEpisodesCursorHelper;

    new-instance v7, Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/videos/ui/WatchNowHelper;->newTvDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    const/4 v10, 0x0

    invoke-direct {v7, v9, v10}, Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;-><init>(Lcom/google/android/videos/adapter/VideosDataSource;Lcom/google/android/videos/ui/WatchNowHelper$1;)V

    invoke-interface {v5, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    new-instance v9, Lcom/google/android/videos/ui/WatchNowFlowHelper;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/ui/WatchNowHelper;->welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/videos/ui/WatchNowHelper;->watchNowDataSource:Lcom/google/android/videos/adapter/WatchNowDataSource;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/ui/WatchNowHelper;->watchNowRecommendationDataSource:Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;

    new-instance v15, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v5}, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;-><init>(Lcom/google/android/videos/ui/WatchNowHelper;Lcom/google/android/videos/ui/WatchNowHelper$1;)V

    new-instance v16, Lcom/google/android/videos/ui/WatchNowHelper$SuggestionItemClickListener;

    const/4 v5, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v5}, Lcom/google/android/videos/ui/WatchNowHelper$SuggestionItemClickListener;-><init>(Lcom/google/android/videos/ui/WatchNowHelper;Lcom/google/android/videos/ui/WatchNowHelper$1;)V

    new-instance v17, Lcom/google/android/videos/ui/WatchNowHelper$SeeMoreOnClickListener;

    const/4 v5, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v5}, Lcom/google/android/videos/ui/WatchNowHelper$SeeMoreOnClickListener;-><init>(Lcom/google/android/videos/ui/WatchNowHelper;Lcom/google/android/videos/ui/WatchNowHelper$1;)V

    move-object/from16 v10, p4

    move-object/from16 v11, p20

    move-object/from16 v19, v4

    invoke-direct/range {v9 .. v19}, Lcom/google/android/videos/ui/WatchNowFlowHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/utils/DownloadedOnlyManager;Lcom/google/android/videos/welcome/WelcomeFlow;Lcom/google/android/videos/adapter/WatchNowDataSource;Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;Landroid/view/View$OnClickListener;Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView$Binder;Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView$Binder;)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/android/videos/ui/WatchNowHelper;->watchNowFlowHelper:Lcom/google/android/videos/ui/WatchNowFlowHelper;

    .line 255
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->watchNowFlowHelper:Lcom/google/android/videos/ui/WatchNowFlowHelper;

    invoke-virtual {v5}, Lcom/google/android/videos/ui/WatchNowFlowHelper;->getFlow()Lcom/google/android/videos/flow/Flow;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/google/android/videos/flow/Flow;->setVisible(Z)V

    .line 257
    new-instance v20, Lcom/google/android/videos/flow/FlowAdapter;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->watchNowFlowHelper:Lcom/google/android/videos/ui/WatchNowFlowHelper;

    invoke-virtual {v5}, Lcom/google/android/videos/ui/WatchNowFlowHelper;->getFlow()Lcom/google/android/videos/flow/Flow;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-direct {v0, v5}, Lcom/google/android/videos/flow/FlowAdapter;-><init>(Lcom/google/android/videos/flow/Flow;)V

    .line 258
    .local v20, "flowAdapter":Lcom/google/android/videos/flow/FlowAdapter;
    const/4 v5, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Landroid/support/v7/widget/RecyclerView$Adapter;->setHasStableIds(Z)V

    .line 259
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    new-instance v6, Lcom/google/android/videos/ui/DebugFlowLayoutManager;

    const-string v7, "WatchNowFragment"

    invoke-direct {v6, v7}, Lcom/google/android/videos/ui/DebugFlowLayoutManager;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 260
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 261
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/ui/WatchNowHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(Landroid/support/v7/widget/RecyclerView$ItemAnimator;)V

    .line 262
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/ui/WatchNowHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/WatchNowHelper;

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/google/android/videos/ui/WatchNowHelper;->allowCursorChanges()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/logging/UiEventLoggingHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/WatchNowHelper;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/videos/ui/WatchNowHelper;[Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;)[Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/ui/WatchNowHelper;
    .param p1, "x1"    # [Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/google/android/videos/ui/WatchNowHelper;->pendingVideoSuggestions:[Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/activity/HomeActivity;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/WatchNowHelper;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/ui/PinHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/WatchNowHelper;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper;->pinHelper:Lcom/google/android/videos/ui/PinHelper;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/logging/EventLogger;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/WatchNowHelper;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/Config;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/WatchNowHelper;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper;->config:Lcom/google/android/videos/Config;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/videos/ui/WatchNowHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/WatchNowHelper;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper;->suggestionsAccount:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/store/StoreStatusMonitor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/WatchNowHelper;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/WatchNowHelper;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper;->suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/WatchNowHelper;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper;->watchNowRecommendationDataSource:Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/ui/SyncHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/WatchNowHelper;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    return-object v0
.end method

.method private allowCursorChanges()Z
    .locals 1

    .prologue
    .line 388
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/HomeActivity;->isTransitioning()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->isShown()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private maybeUpdateSuggestions(Ljava/lang/String;)V
    .locals 7
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 402
    iget-boolean v1, p0, Lcom/google/android/videos/ui/WatchNowHelper;->suggestionsEnabled:Z

    if-nez v1, :cond_1

    .line 418
    :cond_0
    :goto_0
    return-void

    .line 405
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    invoke-virtual {v1, p1}, Lcom/google/android/videos/store/ConfigurationStore;->anyVerticalEnabled(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 406
    :cond_2
    invoke-direct {p0}, Lcom/google/android/videos/ui/WatchNowHelper;->resetSuggestions()V

    goto :goto_0

    .line 407
    :cond_3
    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper;->suggestionsAccount:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 408
    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->videoSuggestionsDataSourceUpdater:Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;

    invoke-virtual {v1, v2}, Lcom/google/android/videos/store/StoreStatusMonitor;->addListener(Lcom/google/android/videos/store/StoreStatusMonitor$Listener;)V

    .line 409
    iput-object p1, p0, Lcom/google/android/videos/ui/WatchNowHelper;->suggestionsAccount:Ljava/lang/String;

    .line 410
    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->suggestionsAccount:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/videos/store/ConfigurationStore;->getPlayCountry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 411
    .local v6, "playCountry":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper;->unpurchasedEpisodesHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->suggestionsAccount:Ljava/lang/String;

    invoke-virtual {v1, v2, v6}, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->init(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper;->videoSuggestionsDataSourceUpdater:Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;

    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->suggestionsAccount:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;->onAccountUpdated(Ljava/lang/String;)V

    .line 413
    new-instance v0, Lcom/google/android/videos/api/VideoCollectionListRequest;

    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    invoke-virtual {v1, p1}, Lcom/google/android/videos/store/ConfigurationStore;->getPlayCountry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper;->config:Lcom/google/android/videos/Config;

    invoke-interface {v1}, Lcom/google/android/videos/Config;->maxSuggestionClusters()I

    move-result v4

    iget v5, p0, Lcom/google/android/videos/ui/WatchNowHelper;->maxVideosPerSuggestionCluster:I

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/api/VideoCollectionListRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;II)V

    .line 416
    .local v0, "request":Lcom/google/android/videos/api/VideoCollectionListRequest;
    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper;->videoCollectionsHelper:Lcom/google/android/videos/ui/VideoCollectionHelper;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/ui/VideoCollectionHelper;->init(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private refreshCursors()V
    .locals 3

    .prologue
    .line 316
    invoke-direct {p0}, Lcom/google/android/videos/ui/WatchNowHelper;->allowCursorChanges()Z

    move-result v2

    if-nez v2, :cond_1

    .line 322
    :cond_0
    return-void

    .line 319
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->cursorHelpers:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/ui/CursorHelper;

    .line 320
    .local v1, "key":Lcom/google/android/videos/ui/CursorHelper;, "Lcom/google/android/videos/ui/CursorHelper<*>;"
    invoke-virtual {p0, v1}, Lcom/google/android/videos/ui/WatchNowHelper;->onCursorChanged(Lcom/google/android/videos/ui/CursorHelper;)V

    goto :goto_0
.end method

.method private resetSuggestions()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 421
    iget-boolean v0, p0, Lcom/google/android/videos/ui/WatchNowHelper;->suggestionsEnabled:Z

    if-nez v0, :cond_0

    .line 430
    :goto_0
    return-void

    .line 424
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper;->unpurchasedEpisodesHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->reset()V

    .line 425
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper;->videoSuggestionsDataSourceUpdater:Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/store/StoreStatusMonitor;->removeListener(Lcom/google/android/videos/store/StoreStatusMonitor$Listener;)V

    .line 426
    iput-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->suggestionsAccount:Ljava/lang/String;

    .line 427
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper;->videoCollectionsHelper:Lcom/google/android/videos/ui/VideoCollectionHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/VideoCollectionHelper;->reset()V

    .line 428
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper;->videoSuggestionsDataSourceUpdater:Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/SuggestionDataSourceUpdater;->reset()V

    .line 429
    iput-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->pendingVideoSuggestions:[Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;

    goto :goto_0
.end method

.method private updateVisibilities()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 354
    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/ui/SyncHelper;->getState()I

    move-result v0

    .line 355
    .local v0, "syncState":I
    packed-switch v0, :pswitch_data_0

    .line 385
    :goto_0
    return-void

    .line 359
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper;->progressBar:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 360
    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    goto :goto_0

    .line 369
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper;->newMoviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v1}, Lcom/google/android/videos/adapter/MoviesDataSource;->hasCursor()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper;->newTvDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-virtual {v1}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->hasCursor()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 370
    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper;->progressBar:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 371
    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v3}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 372
    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    invoke-virtual {v1}, Lcom/google/android/videos/activity/HomeActivity;->isTransitioning()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 373
    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper;->watchNowFlowHelper:Lcom/google/android/videos/ui/WatchNowFlowHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/ui/WatchNowFlowHelper;->getFlow()Lcom/google/android/videos/flow/Flow;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/videos/flow/Flow;->setVisible(Z)V

    .line 374
    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    invoke-virtual {v1, p0}, Lcom/google/android/videos/activity/HomeActivity;->markAsReadyForTransitionV21(Ljava/lang/Object;)V

    goto :goto_0

    .line 376
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->watchNowFlowHelper:Lcom/google/android/videos/ui/WatchNowFlowHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/WatchNowFlowHelper;->getFlow()Lcom/google/android/videos/flow/Flow;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/videos/ui/FlowAnimationUtil;->animateFlowAppearing(Landroid/support/v7/widget/RecyclerView;Lcom/google/android/videos/flow/Flow;)V

    goto :goto_0

    .line 379
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 380
    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper;->progressBar:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 355
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public onCursorChanged(Lcom/google/android/videos/ui/CursorHelper;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/ui/CursorHelper",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 326
    .local p1, "source":Lcom/google/android/videos/ui/CursorHelper;, "Lcom/google/android/videos/ui/CursorHelper<*>;"
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->cursorHelpers:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;

    .line 327
    .local v0, "relatedData":Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;
    invoke-direct {p0}, Lcom/google/android/videos/ui/WatchNowHelper;->allowCursorChanges()Z

    move-result v2

    if-nez v2, :cond_1

    .line 328
    const/4 v2, 0x1

    # setter for: Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;->pendingCursor:Z
    invoke-static {v0, v2}, Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;->access$702(Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;Z)Z

    .line 343
    :cond_0
    :goto_0
    return-void

    .line 332
    :cond_1
    # getter for: Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;->dataSource:Lcom/google/android/videos/adapter/VideosDataSource;
    invoke-static {v0}, Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;->access$800(Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;)Lcom/google/android/videos/adapter/VideosDataSource;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/videos/ui/CursorHelper;->getCursor()Landroid/database/Cursor;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/videos/adapter/VideosDataSource;->changeCursor(Landroid/database/Cursor;)V

    .line 333
    invoke-direct {p0}, Lcom/google/android/videos/ui/WatchNowHelper;->updateVisibilities()V

    .line 335
    iget-boolean v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->suggestionsEnabled:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->suggestionsAccount:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 336
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->newTvCursorHelper:Lcom/google/android/videos/ui/CursorHelper$WatchNowEpisodesCursorHelper;

    if-ne p1, v2, :cond_0

    .line 337
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 338
    .local v1, "unpurchasedEpisodeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->newTvDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-virtual {v2, v1}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->getUnpurchasedEpisodes(Ljava/util/List;)I

    move-result v2

    if-eqz v2, :cond_0

    .line 339
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->unpurchasedEpisodesHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    invoke-virtual {v2, v1}, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->processUnpurchasedItems(Ljava/util/List;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 310
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->cursorHelpers:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 311
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/android/videos/ui/CursorHelper<*>;Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/CursorHelper;->onDestroy()V

    goto :goto_0

    .line 313
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/android/videos/ui/CursorHelper<*>;Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;>;"
    :cond_0
    return-void
.end method

.method public onScreenshotUpdated()V
    .locals 1

    .prologue
    .line 346
    invoke-direct {p0}, Lcom/google/android/videos/ui/WatchNowHelper;->allowCursorChanges()Z

    move-result v0

    if-nez v0, :cond_0

    .line 347
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/ui/WatchNowHelper;->pendingScreenshotUpdatedNotification:Z

    .line 351
    :goto_0
    return-void

    .line 350
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper;->newTvDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-virtual {v0}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->notifyChanged()V

    goto :goto_0
.end method

.method public onScreenshotUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 434
    invoke-virtual {p0}, Lcom/google/android/videos/ui/WatchNowHelper;->onScreenshotUpdated()V

    .line 435
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    .line 266
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    invoke-virtual {v2}, Lcom/google/android/videos/activity/HomeActivity;->isTransitioning()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 267
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    invoke-virtual {v2, p0}, Lcom/google/android/videos/activity/HomeActivity;->markAsPreparingForTransitionV21(Ljava/lang/Object;)V

    .line 268
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(Landroid/support/v7/widget/RecyclerView$ItemAnimator;)V

    .line 272
    :goto_0
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v2, p0}, Lcom/google/android/videos/ui/SyncHelper;->addListener(Lcom/google/android/videos/ui/SyncHelper$Listener;)V

    .line 273
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v2, p0}, Lcom/google/android/videos/store/Database;->addListener(Lcom/google/android/videos/store/Database$Listener;)V

    .line 274
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->cursorHelpers:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/CursorHelper;

    .line 275
    .local v0, "cursorHelper":Lcom/google/android/videos/ui/CursorHelper;, "Lcom/google/android/videos/ui/CursorHelper<*>;"
    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/CursorHelper;->addListener(Lcom/google/android/videos/ui/CursorHelper$Listener;)V

    .line 276
    invoke-virtual {v0}, Lcom/google/android/videos/ui/CursorHelper;->onStart()V

    goto :goto_1

    .line 270
    .end local v0    # "cursorHelper":Lcom/google/android/videos/ui/CursorHelper;, "Lcom/google/android/videos/ui/CursorHelper<*>;"
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    new-instance v3, Lcom/google/android/videos/ui/ShuffleAddItemAnimator;

    invoke-direct {v3}, Lcom/google/android/videos/ui/ShuffleAddItemAnimator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(Landroid/support/v7/widget/RecyclerView$ItemAnimator;)V

    goto :goto_0

    .line 278
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    invoke-direct {p0}, Lcom/google/android/videos/ui/WatchNowHelper;->refreshCursors()V

    .line 279
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->watchNowFlowHelper:Lcom/google/android/videos/ui/WatchNowFlowHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/WatchNowFlowHelper;->onStart()V

    .line 280
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;

    invoke-virtual {v2}, Lcom/google/android/videos/welcome/WelcomeFlow;->onStart()V

    .line 281
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v2, p0}, Lcom/google/android/videos/utils/NetworkStatus;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 282
    invoke-virtual {p0}, Lcom/google/android/videos/ui/WatchNowHelper;->update()V

    .line 283
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->watchNowDataSource:Lcom/google/android/videos/adapter/WatchNowDataSource;

    invoke-virtual {v2}, Lcom/google/android/videos/adapter/WatchNowDataSource;->registerWithRemoteTracker()V

    .line 285
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/SyncHelper;->getState()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/videos/ui/WatchNowHelper;->onSyncStateChanged(I)V

    .line 286
    return-void
.end method

.method public onStop()V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/4 v4, 0x0

    .line 290
    invoke-direct {p0}, Lcom/google/android/videos/ui/WatchNowHelper;->resetSuggestions()V

    .line 291
    iput-boolean v4, p0, Lcom/google/android/videos/ui/WatchNowHelper;->pendingScreenshotUpdatedNotification:Z

    .line 292
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 293
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->progressBar:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 294
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->cursorHelpers:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 295
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/android/videos/ui/CursorHelper<*>;Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;

    # setter for: Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;->pendingCursor:Z
    invoke-static {v2, v4}, Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;->access$702(Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;Z)Z

    .line 296
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;->dataSource:Lcom/google/android/videos/adapter/VideosDataSource;
    invoke-static {v2}, Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;->access$800(Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;)Lcom/google/android/videos/adapter/VideosDataSource;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/google/android/videos/adapter/VideosDataSource;->changeCursor(Landroid/database/Cursor;)V

    .line 297
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/CursorHelper;->onStop()V

    .line 298
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {v2, p0}, Lcom/google/android/videos/ui/CursorHelper;->removeListener(Lcom/google/android/videos/ui/CursorHelper$Listener;)V

    goto :goto_0

    .line 300
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/android/videos/ui/CursorHelper<*>;Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;>;"
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v2, p0}, Lcom/google/android/videos/store/Database;->removeListener(Lcom/google/android/videos/store/Database$Listener;)Z

    .line 301
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v2, p0}, Lcom/google/android/videos/ui/SyncHelper;->removeListener(Lcom/google/android/videos/ui/SyncHelper$Listener;)V

    .line 302
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->watchNowFlowHelper:Lcom/google/android/videos/ui/WatchNowFlowHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/WatchNowFlowHelper;->onStop()V

    .line 303
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;

    invoke-virtual {v2}, Lcom/google/android/videos/welcome/WelcomeFlow;->onStop()V

    .line 304
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->watchNowDataSource:Lcom/google/android/videos/adapter/WatchNowDataSource;

    invoke-virtual {v2}, Lcom/google/android/videos/adapter/WatchNowDataSource;->unregisterWithRemoteTracker()V

    .line 305
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v2, p0}, Lcom/google/android/videos/utils/NetworkStatus;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 306
    return-void
.end method

.method public onSyncStateChanged(I)V
    .locals 2
    .param p1, "state"    # I

    .prologue
    .line 393
    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v0

    .line 394
    .local v0, "account":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 395
    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper;->welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/welcome/WelcomeFlow;->setAccount(Ljava/lang/String;)V

    .line 397
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/videos/ui/WatchNowHelper;->maybeUpdateSuggestions(Ljava/lang/String;)V

    .line 398
    invoke-direct {p0}, Lcom/google/android/videos/ui/WatchNowHelper;->updateVisibilities()V

    .line 399
    return-void
.end method

.method public onTransitioningChanged(Z)V
    .locals 6
    .param p1, "isTransitioning"    # Z

    .prologue
    const/4 v5, 0x0

    .line 450
    if-eqz p1, :cond_1

    .line 472
    :cond_0
    :goto_0
    return-void

    .line 454
    :cond_1
    iget-object v3, p0, Lcom/google/android/videos/ui/WatchNowHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    new-instance v4, Lcom/google/android/videos/ui/ShuffleAddItemAnimator;

    invoke-direct {v4}, Lcom/google/android/videos/ui/ShuffleAddItemAnimator;-><init>()V

    invoke-virtual {v3, v4}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(Landroid/support/v7/widget/RecyclerView$ItemAnimator;)V

    .line 456
    iget-object v3, p0, Lcom/google/android/videos/ui/WatchNowHelper;->cursorHelpers:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 457
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/android/videos/ui/CursorHelper<*>;Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;

    .line 458
    .local v0, "cursorHelperRelatedData":Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;
    # getter for: Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;->pendingCursor:Z
    invoke-static {v0}, Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;->access$700(Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 459
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {p0, v3}, Lcom/google/android/videos/ui/WatchNowHelper;->onCursorChanged(Lcom/google/android/videos/ui/CursorHelper;)V

    .line 461
    :cond_2
    # setter for: Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;->pendingCursor:Z
    invoke-static {v0, v5}, Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;->access$702(Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;Z)Z

    goto :goto_1

    .line 464
    .end local v0    # "cursorHelperRelatedData":Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/android/videos/ui/CursorHelper<*>;Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;>;"
    :cond_3
    iget-object v3, p0, Lcom/google/android/videos/ui/WatchNowHelper;->pendingVideoSuggestions:[Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;

    if-eqz v3, :cond_4

    .line 465
    iget-object v3, p0, Lcom/google/android/videos/ui/WatchNowHelper;->watchNowRecommendationDataSource:Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;

    iget-object v4, p0, Lcom/google/android/videos/ui/WatchNowHelper;->pendingVideoSuggestions:[Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;

    invoke-virtual {v3, v4}, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;->updateVideos([Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;)V

    .line 466
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/videos/ui/WatchNowHelper;->pendingVideoSuggestions:[Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;

    .line 468
    :cond_4
    iget-boolean v3, p0, Lcom/google/android/videos/ui/WatchNowHelper;->pendingScreenshotUpdatedNotification:Z

    if-eqz v3, :cond_0

    .line 469
    invoke-virtual {p0}, Lcom/google/android/videos/ui/WatchNowHelper;->onScreenshotUpdated()V

    .line 470
    iput-boolean v5, p0, Lcom/google/android/videos/ui/WatchNowHelper;->pendingScreenshotUpdatedNotification:Z

    goto :goto_0
.end method

.method public update()V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 439
    iget-object v3, p0, Lcom/google/android/videos/ui/WatchNowHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v3}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v1

    .line 440
    .local v1, "isConnected":Z
    iget-object v3, p0, Lcom/google/android/videos/ui/WatchNowHelper;->cursorHelpers:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;

    .line 441
    .local v2, "relatedData":Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;
    # getter for: Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;->dataSource:Lcom/google/android/videos/adapter/VideosDataSource;
    invoke-static {v2}, Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;->access$800(Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;)Lcom/google/android/videos/adapter/VideosDataSource;

    move-result-object v3

    invoke-interface {v3, v1}, Lcom/google/android/videos/adapter/VideosDataSource;->setNetworkConnected(Z)V

    goto :goto_0

    .line 443
    .end local v2    # "relatedData":Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;
    :cond_0
    iget-object v3, p0, Lcom/google/android/videos/ui/WatchNowHelper;->watchNowRecommendationDataSource:Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;

    invoke-virtual {v3, v1}, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;->setNetworkConnected(Z)V

    .line 444
    iget-object v6, p0, Lcom/google/android/videos/ui/WatchNowHelper;->watchNowFlowHelper:Lcom/google/android/videos/ui/WatchNowFlowHelper;

    if-nez v1, :cond_1

    move v3, v4

    :goto_1
    invoke-virtual {v6, v3}, Lcom/google/android/videos/ui/WatchNowFlowHelper;->setSuggestionHeaderDimmed(Z)V

    .line 445
    iget-object v3, p0, Lcom/google/android/videos/ui/WatchNowHelper;->welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;

    if-nez v1, :cond_2

    :goto_2
    invoke-virtual {v3, v4}, Lcom/google/android/videos/welcome/WelcomeFlow;->setDimmed(Z)V

    .line 446
    return-void

    :cond_1
    move v3, v5

    .line 444
    goto :goto_1

    :cond_2
    move v4, v5

    .line 445
    goto :goto_2
.end method
