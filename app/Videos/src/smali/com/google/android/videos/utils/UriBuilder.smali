.class public final Lcom/google/android/videos/utils/UriBuilder;
.super Ljava/lang/Object;
.source "UriBuilder.java"


# static fields
.field private static final HEX_DIGITS:[C


# instance fields
.field private final encoded:Ljava/lang/StringBuilder;

.field private fragmentIndex:I

.field private parameterEndIndex:I

.field private parameterStartIndex:I

.field private parameterValueStartIndex:I

.field private pathIndex:I

.field private queryIndex:I

.field private schemeColonIndex:I

.field private final uri:Ljava/lang/StringBuilder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    const/16 v0, 0x10

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/videos/utils/UriBuilder;->HEX_DIGITS:[C

    return-void

    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x41s
        0x42s
        0x43s
        0x44s
        0x45s
        0x46s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x6

    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "about:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/videos/utils/UriBuilder;->uri:Ljava/lang/StringBuilder;

    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/utils/UriBuilder;->encoded:Ljava/lang/StringBuilder;

    .line 132
    const/4 v0, 0x5

    iput v0, p0, Lcom/google/android/videos/utils/UriBuilder;->schemeColonIndex:I

    .line 133
    iput v2, p0, Lcom/google/android/videos/utils/UriBuilder;->pathIndex:I

    .line 134
    iput v2, p0, Lcom/google/android/videos/utils/UriBuilder;->queryIndex:I

    .line 135
    iput v2, p0, Lcom/google/android/videos/utils/UriBuilder;->fragmentIndex:I

    .line 136
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 11
    .param p1, "uriString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/16 v10, 0x2f

    const/4 v6, 0x0

    const/4 v9, -0x1

    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    .line 148
    .local v2, "length":I
    const/16 v8, 0x3a

    invoke-virtual {p1, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    .line 149
    .local v5, "schemeColonIndex":I
    const/16 v8, 0x3f

    invoke-virtual {p1, v8, v5}, Ljava/lang/String;->indexOf(II)I

    move-result v4

    .line 150
    .local v4, "queryIndex":I
    const/16 v8, 0x23

    invoke-virtual {p1, v8, v5}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 151
    .local v0, "fragmentIndex":I
    if-ne v0, v9, :cond_0

    .line 152
    move v0, v2

    .line 154
    :cond_0
    if-eq v4, v9, :cond_1

    if-ge v0, v4, :cond_2

    .line 156
    :cond_1
    move v4, v0

    .line 160
    :cond_2
    add-int/lit8 v8, v5, 0x2

    if-ge v8, v4, :cond_7

    add-int/lit8 v8, v5, 0x1

    invoke-virtual {p1, v8}, Ljava/lang/String;->charAt(I)C

    move-result v8

    if-ne v8, v10, :cond_7

    add-int/lit8 v8, v5, 0x2

    invoke-virtual {p1, v8}, Ljava/lang/String;->charAt(I)C

    move-result v8

    if-ne v8, v10, :cond_7

    move v1, v7

    .line 164
    .local v1, "hasAuthority":Z
    :goto_0
    if-eqz v1, :cond_8

    .line 165
    add-int/lit8 v8, v5, 0x3

    invoke-virtual {p1, v10, v8}, Ljava/lang/String;->indexOf(II)I

    move-result v3

    .line 166
    .local v3, "pathIndex":I
    if-eq v3, v9, :cond_3

    if-le v3, v4, :cond_4

    .line 167
    :cond_3
    move v3, v4

    .line 174
    :cond_4
    :goto_1
    invoke-static {p1, v6, v5}, Lcom/google/android/videos/utils/UriBuilder;->isValidScheme(Ljava/lang/CharSequence;II)Z

    move-result v8

    const-string v9, "Invalid scheme in %s"

    invoke-static {v8, v9, p1}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;Ljava/lang/Object;)V

    .line 176
    if-eqz v1, :cond_5

    add-int/lit8 v8, v5, 0x3

    invoke-static {p1, v8, v3}, Lcom/google/android/videos/utils/UriBuilder;->isValidAuthority(Ljava/lang/CharSequence;II)Z

    move-result v8

    if-eqz v8, :cond_6

    :cond_5
    move v6, v7

    :cond_6
    const-string v7, "Invalid authority in %s"

    invoke-static {v6, v7, p1}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;Ljava/lang/Object;)V

    .line 179
    invoke-static {p1, v3, v4, v1}, Lcom/google/android/videos/utils/UriBuilder;->isValidPath(Ljava/lang/String;IIZ)Z

    move-result v6

    const-string v7, "Invalid path in %s"

    invoke-static {v6, v7, p1}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;Ljava/lang/Object;)V

    .line 181
    add-int/lit8 v6, v4, 0x1

    invoke-static {p1, v6, v0}, Lcom/google/android/videos/utils/UriBuilder;->isValidQuery(Ljava/lang/String;II)Z

    move-result v6

    const-string v7, "Invalid query in %s"

    invoke-static {v6, v7, p1}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;Ljava/lang/Object;)V

    .line 183
    add-int/lit8 v6, v0, 0x1

    invoke-static {p1, v6, v2}, Lcom/google/android/videos/utils/UriBuilder;->isValidFragment(Ljava/lang/CharSequence;II)Z

    move-result v6

    const-string v7, "Invalid fragment in %s"

    invoke-static {v6, v7, p1}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;Ljava/lang/Object;)V

    .line 187
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v6, p0, Lcom/google/android/videos/utils/UriBuilder;->uri:Ljava/lang/StringBuilder;

    .line 188
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v6, p0, Lcom/google/android/videos/utils/UriBuilder;->encoded:Ljava/lang/StringBuilder;

    .line 189
    iput v5, p0, Lcom/google/android/videos/utils/UriBuilder;->schemeColonIndex:I

    .line 190
    iput v3, p0, Lcom/google/android/videos/utils/UriBuilder;->pathIndex:I

    .line 191
    iput v4, p0, Lcom/google/android/videos/utils/UriBuilder;->queryIndex:I

    .line 192
    iput v0, p0, Lcom/google/android/videos/utils/UriBuilder;->fragmentIndex:I

    .line 193
    return-void

    .end local v1    # "hasAuthority":Z
    .end local v3    # "pathIndex":I
    :cond_7
    move v1, v6

    .line 160
    goto :goto_0

    .line 170
    .restart local v1    # "hasAuthority":Z
    :cond_8
    add-int/lit8 v3, v5, 0x1

    .restart local v3    # "pathIndex":I
    goto :goto_1
.end method

.method private addQueryParameterEncodedInternal(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "paramKey"    # Ljava/lang/String;
    .param p2, "paramValue"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 516
    if-nez p2, :cond_0

    const-string v6, ""

    .line 517
    .local v6, "paramValueNotNull":Ljava/lang/String;
    :goto_0
    iget v3, p0, Lcom/google/android/videos/utils/UriBuilder;->fragmentIndex:I

    .line 518
    .local v3, "insertIndex":I
    iget v7, p0, Lcom/google/android/videos/utils/UriBuilder;->queryIndex:I

    if-ne v3, v7, :cond_1

    const/16 v1, 0x3f

    .line 519
    .local v1, "delimiter":C
    :goto_1
    if-nez p1, :cond_2

    const/4 v0, 0x1

    .line 520
    .local v0, "bare":Z
    :goto_2
    if-eqz v0, :cond_3

    .line 521
    .local v4, "paramKeyLength":I
    :goto_3
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v5

    .line 522
    .local v5, "paramValueLength":I
    if-eqz v0, :cond_4

    add-int/lit8 v2, v5, 0x1

    .line 525
    .local v2, "delta":I
    :goto_4
    iget-object v7, p0, Lcom/google/android/videos/utils/UriBuilder;->uri:Ljava/lang/StringBuilder;

    invoke-static {v7, v2}, Lcom/google/android/videos/utils/UriBuilder;->ensureExtraCapacity(Ljava/lang/StringBuilder;I)V

    .line 526
    iget-object v7, p0, Lcom/google/android/videos/utils/UriBuilder;->uri:Ljava/lang/StringBuilder;

    invoke-virtual {v7, v3, v1}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 527
    if-eqz v0, :cond_5

    .line 528
    iget-object v7, p0, Lcom/google/android/videos/utils/UriBuilder;->uri:Ljava/lang/StringBuilder;

    add-int/lit8 v8, v3, 0x1

    invoke-virtual {v7, v8, v6}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 534
    :goto_5
    iget v7, p0, Lcom/google/android/videos/utils/UriBuilder;->fragmentIndex:I

    add-int/2addr v7, v2

    iput v7, p0, Lcom/google/android/videos/utils/UriBuilder;->fragmentIndex:I

    .line 535
    return-void

    .end local v0    # "bare":Z
    .end local v1    # "delimiter":C
    .end local v2    # "delta":I
    .end local v3    # "insertIndex":I
    .end local v4    # "paramKeyLength":I
    .end local v5    # "paramValueLength":I
    .end local v6    # "paramValueNotNull":Ljava/lang/String;
    :cond_0
    move-object v6, p2

    .line 516
    goto :goto_0

    .line 518
    .restart local v3    # "insertIndex":I
    .restart local v6    # "paramValueNotNull":Ljava/lang/String;
    :cond_1
    const/16 v1, 0x26

    goto :goto_1

    .restart local v1    # "delimiter":C
    :cond_2
    move v0, v4

    .line 519
    goto :goto_2

    .line 520
    .restart local v0    # "bare":Z
    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    goto :goto_3

    .line 522
    .restart local v4    # "paramKeyLength":I
    .restart local v5    # "paramValueLength":I
    :cond_4
    add-int/lit8 v7, v4, 0x2

    add-int v2, v7, v5

    goto :goto_4

    .line 530
    .restart local v2    # "delta":I
    :cond_5
    iget-object v7, p0, Lcom/google/android/videos/utils/UriBuilder;->uri:Ljava/lang/StringBuilder;

    add-int/lit8 v8, v3, 0x1

    invoke-virtual {v7, v8, p1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    add-int/lit8 v8, v3, 0x1

    add-int/2addr v8, v4

    const/16 v9, 0x3d

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    move-result-object v7

    add-int/lit8 v8, v3, 0x2

    add-int/2addr v8, v4

    invoke-virtual {v7, v8, v6}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5
.end method

.method private addSegmentEncodedInternal(Ljava/lang/String;)V
    .locals 6
    .param p1, "segment"    # Ljava/lang/String;

    .prologue
    const/16 v5, 0x2f

    .line 350
    iget v1, p0, Lcom/google/android/videos/utils/UriBuilder;->queryIndex:I

    .line 351
    .local v1, "insertIndex":I
    iget v3, p0, Lcom/google/android/videos/utils/UriBuilder;->pathIndex:I

    if-eq v3, v1, :cond_0

    iget-object v3, p0, Lcom/google/android/videos/utils/UriBuilder;->uri:Ljava/lang/StringBuilder;

    add-int/lit8 v4, v1, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v3

    if-eq v3, v5, :cond_2

    :cond_0
    const/4 v2, 0x1

    .line 353
    .local v2, "slashNeeded":Z
    :goto_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 354
    if-eqz v2, :cond_1

    .line 355
    iget-object v3, p0, Lcom/google/android/videos/utils/UriBuilder;->uri:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1, v5}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 356
    iget v3, p0, Lcom/google/android/videos/utils/UriBuilder;->queryIndex:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/videos/utils/UriBuilder;->queryIndex:I

    .line 357
    iget v3, p0, Lcom/google/android/videos/utils/UriBuilder;->fragmentIndex:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/videos/utils/UriBuilder;->fragmentIndex:I

    .line 370
    :cond_1
    :goto_1
    return-void

    .line 351
    .end local v2    # "slashNeeded":Z
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 360
    .restart local v2    # "slashNeeded":Z
    :cond_3
    if-eqz v2, :cond_4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v0, v3, 0x1

    .line 361
    .local v0, "delta":I
    :goto_2
    iget-object v3, p0, Lcom/google/android/videos/utils/UriBuilder;->uri:Ljava/lang/StringBuilder;

    invoke-static {v3, v0}, Lcom/google/android/videos/utils/UriBuilder;->ensureExtraCapacity(Ljava/lang/StringBuilder;I)V

    .line 362
    if-eqz v2, :cond_5

    .line 363
    iget-object v3, p0, Lcom/google/android/videos/utils/UriBuilder;->uri:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1, v5}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v1, 0x1

    invoke-virtual {v3, v4, p1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 367
    :goto_3
    iget v3, p0, Lcom/google/android/videos/utils/UriBuilder;->queryIndex:I

    add-int/2addr v3, v0

    iput v3, p0, Lcom/google/android/videos/utils/UriBuilder;->queryIndex:I

    .line 368
    iget v3, p0, Lcom/google/android/videos/utils/UriBuilder;->fragmentIndex:I

    add-int/2addr v3, v0

    iput v3, p0, Lcom/google/android/videos/utils/UriBuilder;->fragmentIndex:I

    goto :goto_1

    .line 360
    .end local v0    # "delta":I
    :cond_4
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_2

    .line 365
    .restart local v0    # "delta":I
    :cond_5
    iget-object v3, p0, Lcom/google/android/videos/utils/UriBuilder;->uri:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1, p1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3
.end method

.method private appendPercentEncoded(I)V
    .locals 3
    .param p1, "byteValue"    # I

    .prologue
    .line 769
    iget-object v0, p0, Lcom/google/android/videos/utils/UriBuilder;->encoded:Ljava/lang/StringBuilder;

    const/16 v1, 0x25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/videos/utils/UriBuilder;->HEX_DIGITS:[C

    shr-int/lit8 v2, p1, 0x4

    and-int/lit8 v2, v2, 0xf

    aget-char v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/videos/utils/UriBuilder;->HEX_DIGITS:[C

    and-int/lit8 v2, p1, 0xf

    aget-char v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 772
    return-void
.end method

.method private appendUtf8EncodedCharacter(Ljava/lang/String;I)I
    .locals 3
    .param p1, "string"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 745
    invoke-virtual {p1, p2}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    .line 746
    .local v0, "codePoint":I
    const/16 v1, 0x7f

    if-gt v0, v1, :cond_0

    .line 747
    iget-object v1, p0, Lcom/google/android/videos/utils/UriBuilder;->encoded:Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/videos/utils/UriBuilder;->ensureExtraCapacity(Ljava/lang/StringBuilder;I)V

    .line 748
    invoke-direct {p0, v0}, Lcom/google/android/videos/utils/UriBuilder;->appendPercentEncoded(I)V

    .line 765
    :goto_0
    const/4 v1, 0x1

    invoke-virtual {p1, p2, v1}, Ljava/lang/String;->offsetByCodePoints(II)I

    move-result v1

    return v1

    .line 749
    :cond_0
    const/16 v1, 0x7ff

    if-gt v0, v1, :cond_1

    .line 750
    iget-object v1, p0, Lcom/google/android/videos/utils/UriBuilder;->encoded:Ljava/lang/StringBuilder;

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/google/android/videos/utils/UriBuilder;->ensureExtraCapacity(Ljava/lang/StringBuilder;I)V

    .line 751
    shr-int/lit8 v1, v0, 0x6

    or-int/lit16 v1, v1, 0xc0

    invoke-direct {p0, v1}, Lcom/google/android/videos/utils/UriBuilder;->appendPercentEncoded(I)V

    .line 752
    and-int/lit8 v1, v0, 0x3f

    or-int/lit16 v1, v1, 0x80

    invoke-direct {p0, v1}, Lcom/google/android/videos/utils/UriBuilder;->appendPercentEncoded(I)V

    goto :goto_0

    .line 753
    :cond_1
    const v1, 0xffff

    if-gt v0, v1, :cond_2

    .line 754
    iget-object v1, p0, Lcom/google/android/videos/utils/UriBuilder;->encoded:Ljava/lang/StringBuilder;

    const/16 v2, 0x9

    invoke-static {v1, v2}, Lcom/google/android/videos/utils/UriBuilder;->ensureExtraCapacity(Ljava/lang/StringBuilder;I)V

    .line 755
    shr-int/lit8 v1, v0, 0xc

    or-int/lit16 v1, v1, 0xe0

    invoke-direct {p0, v1}, Lcom/google/android/videos/utils/UriBuilder;->appendPercentEncoded(I)V

    .line 756
    shr-int/lit8 v1, v0, 0x6

    and-int/lit8 v1, v1, 0x3f

    or-int/lit16 v1, v1, 0x80

    invoke-direct {p0, v1}, Lcom/google/android/videos/utils/UriBuilder;->appendPercentEncoded(I)V

    .line 757
    and-int/lit8 v1, v0, 0x3f

    or-int/lit16 v1, v1, 0x80

    invoke-direct {p0, v1}, Lcom/google/android/videos/utils/UriBuilder;->appendPercentEncoded(I)V

    goto :goto_0

    .line 759
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/utils/UriBuilder;->encoded:Ljava/lang/StringBuilder;

    const/16 v2, 0xc

    invoke-static {v1, v2}, Lcom/google/android/videos/utils/UriBuilder;->ensureExtraCapacity(Ljava/lang/StringBuilder;I)V

    .line 760
    shr-int/lit8 v1, v0, 0x12

    or-int/lit16 v1, v1, 0xf0

    invoke-direct {p0, v1}, Lcom/google/android/videos/utils/UriBuilder;->appendPercentEncoded(I)V

    .line 761
    shr-int/lit8 v1, v0, 0xc

    and-int/lit8 v1, v1, 0x3f

    or-int/lit16 v1, v1, 0x80

    invoke-direct {p0, v1}, Lcom/google/android/videos/utils/UriBuilder;->appendPercentEncoded(I)V

    .line 762
    shr-int/lit8 v1, v0, 0x6

    and-int/lit8 v1, v1, 0x3f

    or-int/lit16 v1, v1, 0x80

    invoke-direct {p0, v1}, Lcom/google/android/videos/utils/UriBuilder;->appendPercentEncoded(I)V

    .line 763
    and-int/lit8 v1, v0, 0x3f

    or-int/lit16 v1, v1, 0x80

    invoke-direct {p0, v1}, Lcom/google/android/videos/utils/UriBuilder;->appendPercentEncoded(I)V

    goto :goto_0
.end method

.method private encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "string"    # Ljava/lang/String;
    .param p2, "allowedSymbols"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 706
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 725
    .end local p1    # "string":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .line 709
    .restart local p1    # "string":Ljava/lang/String;
    :cond_1
    iget-object v3, p0, Lcom/google/android/videos/utils/UriBuilder;->encoded:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 710
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 711
    .local v1, "length":I
    const/4 v2, 0x0

    .line 712
    .local v2, "processed":I
    :goto_1
    if-ge v2, v1, :cond_4

    .line 713
    invoke-static {p1, v2, v1, p2, v4}, Lcom/google/android/videos/utils/UriBuilder;->getFirstInvalidCharIndex(Ljava/lang/CharSequence;IILjava/lang/String;Z)I

    move-result v0

    .line 715
    .local v0, "index":I
    if-nez v2, :cond_2

    if-eq v0, v1, :cond_0

    .line 718
    :cond_2
    iget-object v3, p0, Lcom/google/android/videos/utils/UriBuilder;->encoded:Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1, v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    .line 719
    if-ge v0, v1, :cond_3

    .line 720
    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/utils/UriBuilder;->appendUtf8EncodedCharacter(Ljava/lang/String;I)I

    move-result v2

    goto :goto_1

    .line 722
    :cond_3
    move v2, v0

    goto :goto_1

    .line 725
    .end local v0    # "index":I
    :cond_4
    iget-object v3, p0, Lcom/google/android/videos/utils/UriBuilder;->encoded:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private static ensureExtraCapacity(Ljava/lang/StringBuilder;I)V
    .locals 1
    .param p0, "stringBuilder"    # Ljava/lang/StringBuilder;
    .param p1, "delta"    # I

    .prologue
    .line 775
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/2addr v0, p1

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->ensureCapacity(I)V

    .line 776
    return-void
.end method

.method private findQueryParameterEncoded(ILjava/lang/String;)V
    .locals 10
    .param p1, "start"    # I
    .param p2, "paramKey"    # Ljava/lang/String;

    .prologue
    const/4 v9, -0x1

    .line 575
    move v3, p1

    .line 576
    .local v3, "paramStart":I
    iget v4, p0, Lcom/google/android/videos/utils/UriBuilder;->fragmentIndex:I

    .line 577
    .local v4, "queryEnd":I
    :goto_0
    if-gt v3, v4, :cond_5

    .line 578
    iget-object v7, p0, Lcom/google/android/videos/utils/UriBuilder;->uri:Ljava/lang/StringBuilder;

    invoke-static {v7, v3, v4}, Lcom/google/android/videos/utils/UriBuilder;->indexOfNextParameterDelimiter(Ljava/lang/CharSequence;II)I

    move-result v2

    .line 579
    .local v2, "paramEnd":I
    iget-object v7, p0, Lcom/google/android/videos/utils/UriBuilder;->uri:Ljava/lang/StringBuilder;

    const-string v8, "="

    invoke-virtual {v7, v8, v3}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 580
    .local v0, "equalIndex":I
    if-eq v0, v9, :cond_0

    if-lt v0, v2, :cond_1

    .line 582
    :cond_0
    if-nez p2, :cond_4

    .line 583
    iput v3, p0, Lcom/google/android/videos/utils/UriBuilder;->parameterStartIndex:I

    .line 584
    iput v3, p0, Lcom/google/android/videos/utils/UriBuilder;->parameterValueStartIndex:I

    .line 585
    iput v2, p0, Lcom/google/android/videos/utils/UriBuilder;->parameterEndIndex:I

    .line 607
    .end local v0    # "equalIndex":I
    .end local v2    # "paramEnd":I
    :goto_1
    return-void

    .line 588
    .restart local v0    # "equalIndex":I
    .restart local v2    # "paramEnd":I
    :cond_1
    if-eqz p2, :cond_4

    sub-int v7, v0, v3

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v8

    if-ne v7, v8, :cond_4

    .line 590
    const/4 v5, 0x1

    .line 591
    .local v5, "same":Z
    const/4 v1, 0x0

    .local v1, "keyIndex":I
    move v6, v3

    .local v6, "uriIndex":I
    :goto_2
    if-eqz v5, :cond_3

    if-ge v6, v0, :cond_3

    .line 593
    invoke-virtual {p2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v7

    iget-object v8, p0, Lcom/google/android/videos/utils/UriBuilder;->uri:Ljava/lang/StringBuilder;

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v8

    if-ne v7, v8, :cond_2

    const/4 v5, 0x1

    .line 592
    :goto_3
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 593
    :cond_2
    const/4 v5, 0x0

    goto :goto_3

    .line 595
    :cond_3
    if-eqz v5, :cond_4

    .line 596
    iput v3, p0, Lcom/google/android/videos/utils/UriBuilder;->parameterStartIndex:I

    .line 597
    add-int/lit8 v7, v0, 0x1

    iput v7, p0, Lcom/google/android/videos/utils/UriBuilder;->parameterValueStartIndex:I

    .line 598
    iput v2, p0, Lcom/google/android/videos/utils/UriBuilder;->parameterEndIndex:I

    goto :goto_1

    .line 602
    .end local v1    # "keyIndex":I
    .end local v5    # "same":Z
    .end local v6    # "uriIndex":I
    :cond_4
    add-int/lit8 v3, v2, 0x1

    .line 603
    goto :goto_0

    .line 604
    .end local v0    # "equalIndex":I
    .end local v2    # "paramEnd":I
    :cond_5
    iput v9, p0, Lcom/google/android/videos/utils/UriBuilder;->parameterStartIndex:I

    .line 605
    iput v9, p0, Lcom/google/android/videos/utils/UriBuilder;->parameterValueStartIndex:I

    .line 606
    iput v9, p0, Lcom/google/android/videos/utils/UriBuilder;->parameterEndIndex:I

    goto :goto_1
.end method

.method private static getFirstInvalidCharIndex(Ljava/lang/CharSequence;IILjava/lang/String;Z)I
    .locals 4
    .param p0, "chars"    # Ljava/lang/CharSequence;
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "allowedSymbols"    # Ljava/lang/String;
    .param p4, "allowPctEncoded"    # Z

    .prologue
    .line 881
    move v1, p1

    .line 882
    .local v1, "index":I
    :goto_0
    if-ge v1, p2, :cond_5

    .line 883
    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 884
    .local v0, "c":C
    const/16 v2, 0x41

    if-gt v2, v0, :cond_0

    const/16 v2, 0x5a

    if-le v0, v2, :cond_2

    :cond_0
    const/16 v2, 0x61

    if-gt v2, v0, :cond_1

    const/16 v2, 0x7a

    if-le v0, v2, :cond_2

    :cond_1
    const/16 v2, 0x30

    if-gt v2, v0, :cond_3

    const/16 v2, 0x39

    if-gt v0, v2, :cond_3

    .line 885
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 886
    :cond_3
    invoke-virtual {p3, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_4

    .line 887
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 888
    :cond_4
    if-eqz p4, :cond_6

    const/16 v2, 0x25

    if-ne v0, v2, :cond_6

    add-int/lit8 v2, v1, 0x2

    if-ge v2, p2, :cond_6

    add-int/lit8 v2, v1, 0x1

    invoke-interface {p0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/google/android/videos/utils/UriBuilder;->isHexDigit(C)Z

    move-result v2

    if-eqz v2, :cond_6

    add-int/lit8 v2, v1, 0x2

    invoke-interface {p0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/google/android/videos/utils/UriBuilder;->isHexDigit(C)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 890
    add-int/lit8 v1, v1, 0x3

    goto :goto_0

    .end local v0    # "c":C
    :cond_5
    move v1, p2

    .line 895
    .end local v1    # "index":I
    :cond_6
    return v1
.end method

.method private static indexOfNextParameterDelimiter(Ljava/lang/CharSequence;II)I
    .locals 3
    .param p0, "chars"    # Ljava/lang/CharSequence;
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 616
    move v1, p1

    .local v1, "index":I
    :goto_0
    if-ge v1, p2, :cond_2

    .line 617
    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 618
    .local v0, "c":C
    const/16 v2, 0x26

    if-eq v0, v2, :cond_0

    const/16 v2, 0x3b

    if-ne v0, v2, :cond_1

    .line 622
    .end local v0    # "c":C
    .end local v1    # "index":I
    :cond_0
    :goto_1
    return v1

    .line 616
    .restart local v0    # "c":C
    .restart local v1    # "index":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .end local v0    # "c":C
    :cond_2
    move v1, p2

    .line 622
    goto :goto_1
.end method

.method private static isHexDigit(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 899
    const/16 v0, 0x30

    if-gt v0, p0, :cond_0

    const/16 v0, 0x39

    if-le p0, v0, :cond_2

    :cond_0
    const/16 v0, 0x41

    if-gt v0, p0, :cond_1

    const/16 v0, 0x46

    if-le p0, v0, :cond_2

    :cond_1
    const/16 v0, 0x61

    if-gt v0, p0, :cond_3

    const/16 v0, 0x66

    if-gt p0, v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isValidAuthority(Ljava/lang/CharSequence;II)Z
    .locals 2
    .param p0, "chars"    # Ljava/lang/CharSequence;
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 795
    const-string v0, "-._~!$&\'()*+,;=:@"

    const/4 v1, 0x1

    invoke-static {p0, p1, p2, v0, v1}, Lcom/google/android/videos/utils/UriBuilder;->isValidRange(Ljava/lang/CharSequence;IILjava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static isValidFragment(Ljava/lang/CharSequence;II)Z
    .locals 2
    .param p0, "chars"    # Ljava/lang/CharSequence;
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 861
    const-string v0, "-._~!$&\'()*+,;=:@/?"

    const/4 v1, 0x1

    invoke-static {p0, p1, p2, v0, v1}, Lcom/google/android/videos/utils/UriBuilder;->isValidRange(Ljava/lang/CharSequence;IILjava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static isValidPath(Ljava/lang/String;IIZ)Z
    .locals 6
    .param p0, "chars"    # Ljava/lang/String;
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "hasAuthority"    # Z

    .prologue
    const/4 v2, 0x1

    const/16 v5, 0x2f

    const/4 v3, 0x0

    .line 799
    if-gt p2, p1, :cond_1

    .line 831
    :cond_0
    :goto_0
    return v2

    .line 803
    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-ne v4, v5, :cond_6

    .line 805
    add-int/lit8 v4, p1, 0x1

    if-ge v4, p2, :cond_5

    add-int/lit8 v4, p1, 0x1

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-ne v4, v5, :cond_5

    .line 807
    if-nez p3, :cond_2

    move v2, v3

    .line 808
    goto :goto_0

    .line 810
    :cond_2
    add-int/lit8 v1, p1, 0x2

    .line 821
    .local v1, "segmentStart":I
    :goto_1
    if-ge v1, p2, :cond_0

    .line 822
    invoke-virtual {p0, v5, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 823
    .local v0, "segmentEnd":I
    const/4 v4, -0x1

    if-eq v0, v4, :cond_3

    if-le v0, p2, :cond_4

    .line 824
    :cond_3
    move v0, p2

    .line 826
    :cond_4
    invoke-static {p0, v1, v0}, Lcom/google/android/videos/utils/UriBuilder;->isValidSegment(Ljava/lang/CharSequence;II)Z

    move-result v4

    if-nez v4, :cond_8

    move v2, v3

    .line 827
    goto :goto_0

    .line 812
    .end local v0    # "segmentEnd":I
    .end local v1    # "segmentStart":I
    :cond_5
    add-int/lit8 v1, p1, 0x1

    .restart local v1    # "segmentStart":I
    goto :goto_1

    .line 816
    .end local v1    # "segmentStart":I
    :cond_6
    if-eqz p3, :cond_7

    move v2, v3

    .line 817
    goto :goto_0

    .line 819
    :cond_7
    move v1, p1

    .restart local v1    # "segmentStart":I
    goto :goto_1

    .line 829
    .restart local v0    # "segmentEnd":I
    :cond_8
    add-int/lit8 v1, v0, 0x1

    .line 830
    goto :goto_1
.end method

.method private static isValidQuery(Ljava/lang/String;II)Z
    .locals 3
    .param p0, "chars"    # Ljava/lang/String;
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 839
    move v1, p1

    .line 840
    .local v1, "paramStart":I
    :goto_0
    if-ge v1, p2, :cond_1

    .line 841
    invoke-static {p0, v1, p2}, Lcom/google/android/videos/utils/UriBuilder;->indexOfNextParameterDelimiter(Ljava/lang/CharSequence;II)I

    move-result v0

    .line 842
    .local v0, "paramEnd":I
    invoke-static {p0, v1, v0}, Lcom/google/android/videos/utils/UriBuilder;->isValidQueryParameter(Ljava/lang/String;II)Z

    move-result v2

    if-nez v2, :cond_0

    .line 843
    const/4 v2, 0x0

    .line 847
    .end local v0    # "paramEnd":I
    :goto_1
    return v2

    .line 845
    .restart local v0    # "paramEnd":I
    :cond_0
    add-int/lit8 v1, v0, 0x1

    .line 846
    goto :goto_0

    .line 847
    .end local v0    # "paramEnd":I
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method

.method private static isValidQueryParameter(Ljava/lang/String;II)Z
    .locals 4
    .param p0, "chars"    # Ljava/lang/String;
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const/4 v1, 0x1

    .line 851
    const/16 v2, 0x3d

    invoke-virtual {p0, v2, p1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 852
    .local v0, "equalsIndex":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    if-lt v0, p2, :cond_2

    .line 853
    :cond_0
    const-string v2, "-._~!$\'()*+,:@/?"

    invoke-static {p0, p1, p2, v2, v1}, Lcom/google/android/videos/utils/UriBuilder;->isValidRange(Ljava/lang/CharSequence;IILjava/lang/String;Z)Z

    move-result v1

    .line 855
    :cond_1
    :goto_0
    return v1

    :cond_2
    const-string v2, "-._~!$\'()*+,:@/?"

    invoke-static {p0, p1, v0, v2, v1}, Lcom/google/android/videos/utils/UriBuilder;->isValidRange(Ljava/lang/CharSequence;IILjava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_3

    add-int/lit8 v2, v0, 0x1

    const-string v3, "-._~!$\'()*+,=:@/?"

    invoke-static {p0, v2, p2, v3, v1}, Lcom/google/android/videos/utils/UriBuilder;->isValidRange(Ljava/lang/CharSequence;IILjava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static isValidRange(Ljava/lang/CharSequence;IILjava/lang/String;Z)Z
    .locals 1
    .param p0, "chars"    # Ljava/lang/CharSequence;
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "allowedSymbols"    # Ljava/lang/String;
    .param p4, "allowPctEncoded"    # Z

    .prologue
    .line 866
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/videos/utils/UriBuilder;->getFirstInvalidCharIndex(Ljava/lang/CharSequence;IILjava/lang/String;Z)I

    move-result v0

    if-ne v0, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isValidScheme(Ljava/lang/CharSequence;II)Z
    .locals 4
    .param p0, "chars"    # Ljava/lang/CharSequence;
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const/4 v1, 0x0

    .line 782
    if-gt p2, p1, :cond_1

    .line 791
    :cond_0
    :goto_0
    return v1

    .line 786
    :cond_1
    invoke-interface {p0, p1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 787
    .local v0, "c":C
    const/16 v2, 0x41

    if-gt v2, v0, :cond_2

    const/16 v2, 0x5a

    if-le v0, v2, :cond_3

    :cond_2
    const/16 v2, 0x61

    if-gt v2, v0, :cond_0

    const/16 v2, 0x7a

    if-gt v0, v2, :cond_0

    .line 791
    :cond_3
    add-int/lit8 v2, p1, 0x1

    const-string v3, "+-."

    invoke-static {p0, v2, p2, v3, v1}, Lcom/google/android/videos/utils/UriBuilder;->isValidRange(Ljava/lang/CharSequence;IILjava/lang/String;Z)Z

    move-result v1

    goto :goto_0
.end method

.method private static isValidSegment(Ljava/lang/CharSequence;II)Z
    .locals 2
    .param p0, "chars"    # Ljava/lang/CharSequence;
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 835
    const-string v0, "-._~!$&\'()*+,;=:@"

    const/4 v1, 0x1

    invoke-static {p0, p1, p2, v0, v1}, Lcom/google/android/videos/utils/UriBuilder;->isValidRange(Ljava/lang/CharSequence;IILjava/lang/String;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public addQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;
    .locals 3
    .param p1, "paramKey"    # Ljava/lang/String;
    .param p2, "paramValue"    # Ljava/lang/String;

    .prologue
    .line 508
    const-string v2, "-._~!$\'()*+,:@/?"

    invoke-direct {p0, p1, v2}, Lcom/google/android/videos/utils/UriBuilder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 509
    .local v0, "paramKeyEncoded":Ljava/lang/String;
    if-nez p2, :cond_0

    const-string v1, ""

    .line 511
    .local v1, "paramValueEncoded":Ljava/lang/String;
    :goto_0
    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/utils/UriBuilder;->addQueryParameterEncodedInternal(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    return-object p0

    .line 509
    .end local v1    # "paramValueEncoded":Ljava/lang/String;
    :cond_0
    if-nez p1, :cond_1

    const-string v2, "-._~!$\'()*+,:@/?"

    :goto_1
    invoke-direct {p0, p2, v2}, Lcom/google/android/videos/utils/UriBuilder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    const-string v2, "-._~!$\'()*+,=:@/?"

    goto :goto_1
.end method

.method public addSegment(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;
    .locals 1
    .param p1, "segment"    # Ljava/lang/String;

    .prologue
    .line 327
    const-string v0, "-._~!$&\'()*+,;=:@"

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/utils/UriBuilder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/utils/UriBuilder;->addSegmentEncodedInternal(Ljava/lang/String;)V

    .line 328
    return-object p0
.end method

.method public addSegmentEncoded(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;
    .locals 2
    .param p1, "segment"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 343
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/google/android/videos/utils/UriBuilder;->isValidSegment(Ljava/lang/CharSequence;II)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    const-string v1, "Invalid segment %s"

    invoke-static {v0, v1, p1}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;Ljava/lang/Object;)V

    .line 345
    invoke-direct {p0, p1}, Lcom/google/android/videos/utils/UriBuilder;->addSegmentEncodedInternal(Ljava/lang/String;)V

    .line 346
    return-object p0
.end method

.method public appendToQueryParameter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;
    .locals 7
    .param p1, "paramKey"    # Ljava/lang/String;
    .param p2, "paramValue"    # Ljava/lang/String;
    .param p3, "separator"    # Ljava/lang/String;

    .prologue
    .line 480
    const-string v4, "-._~!$\'()*+,:@/?"

    invoke-direct {p0, p1, v4}, Lcom/google/android/videos/utils/UriBuilder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 481
    .local v1, "paramKeyEncoded":Ljava/lang/String;
    if-nez p2, :cond_0

    const-string v2, ""

    .line 483
    .local v2, "paramValueEncoded":Ljava/lang/String;
    :goto_0
    if-nez p3, :cond_2

    const-string v3, ""

    .line 485
    .local v3, "separatorEncoded":Ljava/lang/String;
    :goto_1
    iget v4, p0, Lcom/google/android/videos/utils/UriBuilder;->queryIndex:I

    add-int/lit8 v4, v4, 0x1

    invoke-direct {p0, v4, v1}, Lcom/google/android/videos/utils/UriBuilder;->findQueryParameterEncoded(ILjava/lang/String;)V

    .line 486
    iget v4, p0, Lcom/google/android/videos/utils/UriBuilder;->parameterStartIndex:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_4

    .line 487
    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/utils/UriBuilder;->addQueryParameterEncodedInternal(Ljava/lang/String;Ljava/lang/String;)V

    .line 495
    :goto_2
    return-object p0

    .line 481
    .end local v2    # "paramValueEncoded":Ljava/lang/String;
    .end local v3    # "separatorEncoded":Ljava/lang/String;
    :cond_0
    if-nez p1, :cond_1

    const-string v4, "-._~!$\'()*+,:@/?"

    :goto_3
    invoke-direct {p0, p2, v4}, Lcom/google/android/videos/utils/UriBuilder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_1
    const-string v4, "-._~!$\'()*+,=:@/?"

    goto :goto_3

    .line 483
    .restart local v2    # "paramValueEncoded":Ljava/lang/String;
    :cond_2
    if-nez p1, :cond_3

    const-string v4, "-._~!$\'()*+,:@/?"

    :goto_4
    invoke-direct {p0, p3, v4}, Lcom/google/android/videos/utils/UriBuilder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_3
    const-string v4, "-._~!$\'()*+,=:@/?"

    goto :goto_4

    .line 489
    .restart local v3    # "separatorEncoded":Ljava/lang/String;
    :cond_4
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    add-int v0, v4, v5

    .line 490
    .local v0, "delta":I
    iget-object v4, p0, Lcom/google/android/videos/utils/UriBuilder;->uri:Ljava/lang/StringBuilder;

    invoke-static {v4, v0}, Lcom/google/android/videos/utils/UriBuilder;->ensureExtraCapacity(Ljava/lang/StringBuilder;I)V

    .line 491
    iget-object v4, p0, Lcom/google/android/videos/utils/UriBuilder;->uri:Ljava/lang/StringBuilder;

    iget v5, p0, Lcom/google/android/videos/utils/UriBuilder;->parameterEndIndex:I

    invoke-virtual {v4, v5, v3}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/google/android/videos/utils/UriBuilder;->parameterEndIndex:I

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {v4, v5, v2}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 493
    iget v4, p0, Lcom/google/android/videos/utils/UriBuilder;->fragmentIndex:I

    add-int/2addr v4, v0

    iput v4, p0, Lcom/google/android/videos/utils/UriBuilder;->fragmentIndex:I

    goto :goto_2
.end method

.method public build()Ljava/lang/String;
    .locals 1

    .prologue
    .line 676
    iget-object v0, p0, Lcom/google/android/videos/utils/UriBuilder;->uri:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public deleteQueryParameters(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;
    .locals 5
    .param p1, "paramKey"    # Ljava/lang/String;

    .prologue
    .line 545
    const-string v3, "-._~!$\'()*+,:@/?"

    invoke-direct {p0, p1, v3}, Lcom/google/android/videos/utils/UriBuilder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 546
    .local v1, "paramKeyEncoded":Ljava/lang/String;
    iget v3, p0, Lcom/google/android/videos/utils/UriBuilder;->queryIndex:I

    add-int/lit8 v3, v3, 0x1

    invoke-direct {p0, v3, v1}, Lcom/google/android/videos/utils/UriBuilder;->findQueryParameterEncoded(ILjava/lang/String;)V

    .line 547
    :goto_0
    iget v3, p0, Lcom/google/android/videos/utils/UriBuilder;->parameterStartIndex:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    .line 548
    iget v2, p0, Lcom/google/android/videos/utils/UriBuilder;->parameterStartIndex:I

    .line 549
    .local v2, "startIndex":I
    iget v0, p0, Lcom/google/android/videos/utils/UriBuilder;->parameterEndIndex:I

    .line 550
    .local v0, "endIndex":I
    iget v3, p0, Lcom/google/android/videos/utils/UriBuilder;->fragmentIndex:I

    if-ge v0, v3, :cond_0

    .line 552
    iget-object v3, p0, Lcom/google/android/videos/utils/UriBuilder;->uri:Ljava/lang/StringBuilder;

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {v3, v2, v4}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 560
    :goto_1
    iget v3, p0, Lcom/google/android/videos/utils/UriBuilder;->fragmentIndex:I

    sub-int v4, v0, v2

    add-int/lit8 v4, v4, 0x1

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/google/android/videos/utils/UriBuilder;->fragmentIndex:I

    .line 563
    invoke-direct {p0, v2, v1}, Lcom/google/android/videos/utils/UriBuilder;->findQueryParameterEncoded(ILjava/lang/String;)V

    goto :goto_0

    .line 557
    :cond_0
    iget-object v3, p0, Lcom/google/android/videos/utils/UriBuilder;->uri:Ljava/lang/StringBuilder;

    add-int/lit8 v4, v2, -0x1

    invoke-virtual {v3, v4, v0}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 565
    .end local v0    # "endIndex":I
    .end local v2    # "startIndex":I
    :cond_1
    return-object p0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 689
    if-ne p0, p1, :cond_0

    .line 690
    const/4 v0, 0x1

    .line 695
    :goto_0
    return v0

    .line 692
    :cond_0
    instance-of v0, p1, Lcom/google/android/videos/utils/UriBuilder;

    if-nez v0, :cond_1

    .line 693
    const/4 v0, 0x0

    goto :goto_0

    .line 695
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/videos/utils/UriBuilder;->build()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 700
    const-class v0, Lcom/google/android/videos/utils/UriBuilder;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/videos/utils/UriBuilder;->build()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/videos/utils/UriBuilder;->uri:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    return v0
.end method

.method public scheme(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;
    .locals 4
    .param p1, "scheme"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 211
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {p1, v3, v1}, Lcom/google/android/videos/utils/UriBuilder;->isValidScheme(Ljava/lang/CharSequence;II)Z

    move-result v1

    const-string v2, "Invalid scheme %s"

    invoke-static {v1, v2, p1}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;Ljava/lang/Object;)V

    .line 213
    iget-object v1, p0, Lcom/google/android/videos/utils/UriBuilder;->uri:Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/google/android/videos/utils/UriBuilder;->schemeColonIndex:I

    invoke-virtual {v1, v3, v2, p1}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    iget v2, p0, Lcom/google/android/videos/utils/UriBuilder;->schemeColonIndex:I

    sub-int v0, v1, v2

    .line 215
    .local v0, "delta":I
    iget v1, p0, Lcom/google/android/videos/utils/UriBuilder;->schemeColonIndex:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/google/android/videos/utils/UriBuilder;->schemeColonIndex:I

    .line 216
    iget v1, p0, Lcom/google/android/videos/utils/UriBuilder;->pathIndex:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/google/android/videos/utils/UriBuilder;->pathIndex:I

    .line 217
    iget v1, p0, Lcom/google/android/videos/utils/UriBuilder;->queryIndex:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/google/android/videos/utils/UriBuilder;->queryIndex:I

    .line 218
    iget v1, p0, Lcom/google/android/videos/utils/UriBuilder;->fragmentIndex:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/google/android/videos/utils/UriBuilder;->fragmentIndex:I

    .line 219
    return-object p0
.end method

.method public setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;
    .locals 6
    .param p1, "paramKey"    # Ljava/lang/String;
    .param p2, "paramValue"    # Ljava/lang/String;

    .prologue
    .line 452
    const-string v2, "-._~!$\'()*+,:@/?"

    invoke-direct {p0, p1, v2}, Lcom/google/android/videos/utils/UriBuilder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 453
    .local v0, "paramKeyEncoded":Ljava/lang/String;
    if-nez p2, :cond_0

    const-string v1, ""

    .line 455
    .local v1, "paramValueEncoded":Ljava/lang/String;
    :goto_0
    iget v2, p0, Lcom/google/android/videos/utils/UriBuilder;->queryIndex:I

    add-int/lit8 v2, v2, 0x1

    invoke-direct {p0, v2, v0}, Lcom/google/android/videos/utils/UriBuilder;->findQueryParameterEncoded(ILjava/lang/String;)V

    .line 456
    iget v2, p0, Lcom/google/android/videos/utils/UriBuilder;->parameterStartIndex:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    .line 457
    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/utils/UriBuilder;->addQueryParameterEncodedInternal(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    :goto_1
    return-object p0

    .line 453
    .end local v1    # "paramValueEncoded":Ljava/lang/String;
    :cond_0
    if-nez p1, :cond_1

    const-string v2, "-._~!$\'()*+,:@/?"

    :goto_2
    invoke-direct {p0, p2, v2}, Lcom/google/android/videos/utils/UriBuilder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    const-string v2, "-._~!$\'()*+,=:@/?"

    goto :goto_2

    .line 459
    .restart local v1    # "paramValueEncoded":Ljava/lang/String;
    :cond_2
    iget-object v2, p0, Lcom/google/android/videos/utils/UriBuilder;->uri:Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/google/android/videos/utils/UriBuilder;->parameterValueStartIndex:I

    iget v4, p0, Lcom/google/android/videos/utils/UriBuilder;->parameterEndIndex:I

    invoke-virtual {v2, v3, v4, v1}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 460
    iget v2, p0, Lcom/google/android/videos/utils/UriBuilder;->fragmentIndex:I

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    iget v4, p0, Lcom/google/android/videos/utils/UriBuilder;->parameterEndIndex:I

    iget v5, p0, Lcom/google/android/videos/utils/UriBuilder;->parameterValueStartIndex:I

    sub-int/2addr v4, v5

    sub-int/2addr v3, v4

    add-int/2addr v2, v3

    iput v2, p0, Lcom/google/android/videos/utils/UriBuilder;->fragmentIndex:I

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 684
    invoke-virtual {p0}, Lcom/google/android/videos/utils/UriBuilder;->build()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
