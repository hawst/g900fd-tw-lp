.class public final Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;
.super Ljava/lang/Object;
.source "DefaultGcmRegistrationManager.java"

# interfaces
.implements Lcom/google/android/videos/gcm/GcmRegistrationManager;


# static fields
.field private static final SENDER_IDS:[Ljava/lang/String;


# instance fields
.field private final accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

.field private final context:Landroid/content/Context;

.field private final gcmCreateNotificationKeySyncRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/GcmCreateUserNotificationKeyRequest;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final gcmRegisterSyncRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/GcmRegisterRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final googleCloudMessaging:Lcom/google/android/gms/gcm/GoogleCloudMessaging;

.field private final networkExecutor:Ljava/util/concurrent/Executor;

.field private final projectId:Ljava/lang/String;

.field private final registerWithServerRunnable:Ljava/lang/Runnable;

.field private final scope:Ljava/lang/String;

.field private final sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 67
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "859429584213"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "68971793635"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "316244322451"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->SENDER_IDS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/videos/accounts/AccountManagerWrapper;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/GcmRegisterRequest;",
            "Ljava/lang/Void;",
            ">;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/GcmCreateUserNotificationKeyRequest;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 87
    .local p3, "gcmRegisterSyncRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/GcmRegisterRequest;Ljava/lang/Void;>;"
    .local p4, "gcmCreateNotificationKeySyncRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/GcmCreateUserNotificationKeyRequest;Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/accounts/AccountManagerWrapper;

    iput-object v0, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    .line 91
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->context:Landroid/content/Context;

    .line 92
    invoke-static {p1}, Lcom/google/android/gms/gcm/GoogleCloudMessaging;->getInstance(Landroid/content/Context;)Lcom/google/android/gms/gcm/GoogleCloudMessaging;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->googleCloudMessaging:Lcom/google/android/gms/gcm/GoogleCloudMessaging;

    .line 93
    iput-object p3, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->gcmRegisterSyncRequester:Lcom/google/android/videos/async/Requester;

    .line 94
    iput-object p4, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->gcmCreateNotificationKeySyncRequester:Lcom/google/android/videos/async/Requester;

    .line 96
    new-instance v0, Lcom/google/android/videos/utils/PriorityThreadFactory;

    const-string v1, "gcmreg"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/google/android/videos/utils/PriorityThreadFactory;-><init>(Ljava/lang/String;I)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->networkExecutor:Ljava/util/concurrent/Executor;

    .line 99
    new-instance v0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager$1;-><init>(Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;)V

    iput-object v0, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->registerWithServerRunnable:Ljava/lang/Runnable;

    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_gcm"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 110
    invoke-direct {p0}, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->isSignedWithReleaseCertificate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    const-string v0, "68971793635"

    iput-object v0, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->projectId:Ljava/lang/String;

    .line 112
    const-string v0, "audience:server:client_id:68971793635-1f5jd7sdkmfrvhbpuqpbc2o7c33chs0s.apps.googleusercontent.com"

    iput-object v0, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->scope:Ljava/lang/String;

    .line 117
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->ensureRegistered()V

    .line 118
    return-void

    .line 114
    :cond_0
    const-string v0, "316244322451"

    iput-object v0, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->projectId:Ljava/lang/String;

    .line 115
    const-string v0, "audience:server:client_id:316244322451-tni940i36hd76fh3v9ddesri49i41osf.apps.googleusercontent.com"

    iput-object v0, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->scope:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->registerWithServer()V

    return-void
.end method

.method private clearNotificationKey(Ljava/lang/String;)V
    .locals 3
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_notification_key"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 225
    return-void
.end method

.method private clearRegistrationId(Ljava/lang/String;)V
    .locals 3
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_registration_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 210
    return-void
.end method

.method private getLatestRegistrationId()Ljava/lang/String;
    .locals 6

    .prologue
    .line 228
    iget-object v3, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v4, "lastest_registration_id"

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 229
    .local v2, "registrationId":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 230
    const-string v2, ""

    .line 241
    .end local v2    # "registrationId":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v2

    .line 235
    .restart local v2    # "registrationId":Ljava/lang/String;
    :cond_1
    iget-object v3, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v4, "gcm_latest_registered_app_version"

    const/high16 v5, -0x80000000

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 237
    .local v1, "registeredVersion":I
    iget-object v3, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->context:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/videos/VideosGlobals;->getApplicationVersionCode()I

    move-result v0

    .line 238
    .local v0, "currentVersion":I
    if-eq v1, v0, :cond_0

    .line 239
    const-string v2, ""

    goto :goto_0
.end method

.method private getRegistrationId(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->sharedPreferences:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_registration_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private isSignedWithReleaseCertificate()Z
    .locals 9

    .prologue
    const/4 v5, 0x1

    .line 255
    :try_start_0
    iget-object v6, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->context:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x40

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    iget-object v4, v6, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    .line 257
    .local v4, "signatures":[Landroid/content/pm/Signature;
    move-object v0, v4

    .local v0, "arr$":[Landroid/content/pm/Signature;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 258
    .local v3, "signature":Landroid/content/pm/Signature;
    const-string v6, "24BB24C05E47E0AEFA68A58A766179D9B613A600"

    new-instance v7, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v3}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-static {v7}, Lcom/google/android/videos/utils/Hashing;->sha1(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-eqz v6, :cond_0

    .line 268
    .end local v0    # "arr$":[Landroid/content/pm/Signature;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "signature":Landroid/content/pm/Signature;
    .end local v4    # "signatures":[Landroid/content/pm/Signature;
    :goto_1
    return v5

    .line 257
    .restart local v0    # "arr$":[Landroid/content/pm/Signature;
    .restart local v1    # "i$":I
    .restart local v2    # "len$":I
    .restart local v3    # "signature":Landroid/content/pm/Signature;
    .restart local v4    # "signatures":[Landroid/content/pm/Signature;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 263
    .end local v3    # "signature":Landroid/content/pm/Signature;
    :cond_1
    const/4 v5, 0x0

    goto :goto_1

    .line 265
    .end local v0    # "arr$":[Landroid/content/pm/Signature;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v4    # "signatures":[Landroid/content/pm/Signature;
    :catch_0
    move-exception v6

    goto :goto_1

    .line 264
    :catch_1
    move-exception v6

    goto :goto_1
.end method

.method private registerWithServer()V
    .locals 11

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->getLatestRegistrationId()Ljava/lang/String;

    move-result-object v7

    .line 134
    .local v7, "registrationId":Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 136
    :try_start_0
    iget-object v9, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->googleCloudMessaging:Lcom/google/android/gms/gcm/GoogleCloudMessaging;

    sget-object v10, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->SENDER_IDS:[Ljava/lang/String;

    invoke-virtual {v9, v10}, Lcom/google/android/gms/gcm/GoogleCloudMessaging;->register([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 137
    invoke-direct {p0, v7}, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->setLatestRegistrationId(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 142
    :cond_0
    iget-object v9, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v9}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getAccounts()[Landroid/accounts/Account;

    move-result-object v2

    .local v2, "arr$":[Landroid/accounts/Account;
    array-length v6, v2

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_3

    aget-object v0, v2, v5

    .line 143
    .local v0, "account":Landroid/accounts/Account;
    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 144
    .local v1, "accountName":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->getRegistrationId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 145
    invoke-direct {p0, v1}, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->clearNotificationKey(Ljava/lang/String;)V

    .line 146
    invoke-direct {p0, v1}, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->clearRegistrationId(Ljava/lang/String;)V

    .line 150
    new-instance v8, Lcom/google/android/videos/api/GcmRegisterRequest;

    invoke-direct {v8, v1, v7}, Lcom/google/android/videos/api/GcmRegisterRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    .local v8, "request":Lcom/google/android/videos/api/GcmRegisterRequest;
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v3

    .line 152
    .local v3, "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/api/GcmRegisterRequest;Ljava/lang/Void;>;"
    iget-object v9, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->gcmRegisterSyncRequester:Lcom/google/android/videos/async/Requester;

    invoke-interface {v9, v8, v3}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 154
    :try_start_1
    invoke-virtual {v3}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;

    .line 155
    invoke-direct {p0, v1, v7}, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->setRegistrationId(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_1

    .line 161
    .end local v3    # "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/api/GcmRegisterRequest;Ljava/lang/Void;>;"
    .end local v8    # "request":Lcom/google/android/videos/api/GcmRegisterRequest;
    :cond_1
    :goto_1
    invoke-virtual {p0, v1}, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->getNotificationKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 164
    invoke-direct {p0, v1, v7}, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->updateNotificationKey(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 138
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accountName":Ljava/lang/String;
    .end local v2    # "arr$":[Landroid/accounts/Account;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    :catch_0
    move-exception v4

    .line 167
    :cond_3
    return-void

    .line 156
    .restart local v0    # "account":Landroid/accounts/Account;
    .restart local v1    # "accountName":Ljava/lang/String;
    .restart local v2    # "arr$":[Landroid/accounts/Account;
    .restart local v3    # "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/api/GcmRegisterRequest;Ljava/lang/Void;>;"
    .restart local v5    # "i$":I
    .restart local v6    # "len$":I
    .restart local v8    # "request":Lcom/google/android/videos/api/GcmRegisterRequest;
    :catch_1
    move-exception v9

    goto :goto_1
.end method

.method private setLatestRegistrationId(Ljava/lang/String;)V
    .locals 3
    .param p1, "latestRegistrationId"    # Ljava/lang/String;

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "lastest_registration_id"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "gcm_latest_registered_app_version"

    iget-object v2, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getApplicationVersionCode()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 250
    return-void
.end method

.method private setNotificationKey(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "notificationKey"    # Ljava/lang/String;

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_notification_key"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 221
    return-void
.end method

.method private setRegistrationId(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "registrationId"    # Ljava/lang/String;

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_registration_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 206
    return-void
.end method

.method private updateNotificationKey(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "registrationId"    # Ljava/lang/String;

    .prologue
    .line 172
    :try_start_0
    iget-object v3, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->context:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->scope:Ljava/lang/String;

    invoke-static {v3, p1, v4}, Lcom/google/android/gms/auth/GoogleAuthUtil;->getToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/gms/auth/UserRecoverableAuthException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/GoogleAuthException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 184
    .local v0, "authToken":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v1

    .line 185
    .local v1, "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/api/GcmCreateUserNotificationKeyRequest;Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->gcmCreateNotificationKeySyncRequester:Lcom/google/android/videos/async/Requester;

    new-instance v4, Lcom/google/android/videos/api/GcmCreateUserNotificationKeyRequest;

    iget-object v5, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->projectId:Ljava/lang/String;

    invoke-direct {v4, v5, p1, v0, p2}, Lcom/google/android/videos/api/GcmCreateUserNotificationKeyRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v4, v1}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 189
    :try_start_1
    invoke-virtual {v1}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {p0, p1, v3}, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->setNotificationKey(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_3

    .line 193
    .end local v0    # "authToken":Ljava/lang/String;
    .end local v1    # "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/api/GcmCreateUserNotificationKeyRequest;Ljava/lang/String;>;"
    :goto_0
    return-void

    .line 173
    :catch_0
    move-exception v2

    .line 174
    .local v2, "e":Lcom/google/android/gms/auth/UserRecoverableAuthException;
    const-string v3, "Cannot get user auth"

    invoke-static {v3, v2}, Lcom/google/android/videos/L;->i(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 176
    .end local v2    # "e":Lcom/google/android/gms/auth/UserRecoverableAuthException;
    :catch_1
    move-exception v2

    .line 177
    .local v2, "e":Lcom/google/android/gms/auth/GoogleAuthException;
    const-string v3, "Cannot get user auth"

    invoke-static {v3, v2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 179
    .end local v2    # "e":Lcom/google/android/gms/auth/GoogleAuthException;
    :catch_2
    move-exception v2

    .line 180
    .local v2, "e":Ljava/io/IOException;
    const-string v3, "Cannot get user auth"

    invoke-static {v3, v2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 190
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v0    # "authToken":Ljava/lang/String;
    .restart local v1    # "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/api/GcmCreateUserNotificationKeyRequest;Ljava/lang/String;>;"
    :catch_3
    move-exception v3

    goto :goto_0
.end method


# virtual methods
.method public ensureRegistered()V
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->networkExecutor:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->registerWithServerRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 126
    return-void
.end method

.method public getNotificationKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;->sharedPreferences:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_notification_key"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
