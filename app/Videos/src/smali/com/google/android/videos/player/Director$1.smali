.class Lcom/google/android/videos/player/Director$1;
.super Ljava/lang/Object;
.source "Director.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/player/Director;->initKnowledgeCallback()Lcom/google/android/videos/async/Callback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/tagging/KnowledgeRequest;",
        "Lcom/google/android/videos/tagging/KnowledgeBundle;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/player/Director;


# direct methods
.method constructor <init>(Lcom/google/android/videos/player/Director;)V
    .locals 0

    .prologue
    .line 772
    iput-object p1, p0, Lcom/google/android/videos/player/Director$1;->this$0:Lcom/google/android/videos/player/Director;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/tagging/KnowledgeRequest;Ljava/lang/Exception;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/tagging/KnowledgeRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 788
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error getting knowledge for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 790
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 772
    check-cast p1, Lcom/google/android/videos/tagging/KnowledgeRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/player/Director$1;->onError(Lcom/google/android/videos/tagging/KnowledgeRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/tagging/KnowledgeRequest;Lcom/google/android/videos/tagging/KnowledgeBundle;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/tagging/KnowledgeRequest;
    .param p2, "response"    # Lcom/google/android/videos/tagging/KnowledgeBundle;

    .prologue
    .line 775
    iget-object v0, p0, Lcom/google/android/videos/player/Director$1;->this$0:Lcom/google/android/videos/player/Director;

    # getter for: Lcom/google/android/videos/player/Director;->currentKnowledgeRequest:Lcom/google/android/videos/tagging/KnowledgeRequest;
    invoke-static {v0}, Lcom/google/android/videos/player/Director;->access$000(Lcom/google/android/videos/player/Director;)Lcom/google/android/videos/tagging/KnowledgeRequest;

    move-result-object v0

    if-eq p1, v0, :cond_0

    .line 785
    :goto_0
    return-void

    .line 778
    :cond_0
    if-nez p2, :cond_1

    .line 779
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "No knowledge data for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 781
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Got knowledge data for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 782
    iget-object v0, p0, Lcom/google/android/videos/player/Director$1;->this$0:Lcom/google/android/videos/player/Director;

    # getter for: Lcom/google/android/videos/player/Director;->knowledgeViewHelper:Lcom/google/android/videos/tagging/KnowledgeViewHelper;
    invoke-static {v0}, Lcom/google/android/videos/player/Director;->access$100(Lcom/google/android/videos/player/Director;)Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->setKnowledgeBundle(Lcom/google/android/videos/tagging/KnowledgeBundle;)V

    .line 783
    iget-object v0, p0, Lcom/google/android/videos/player/Director$1;->this$0:Lcom/google/android/videos/player/Director;

    # getter for: Lcom/google/android/videos/player/Director;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;
    invoke-static {v0}, Lcom/google/android/videos/player/Director;->access$200(Lcom/google/android/videos/player/Director;)Lcom/google/android/videos/player/overlay/ControllerOverlay;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setHasKnowledge(Z)V

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 772
    check-cast p1, Lcom/google/android/videos/tagging/KnowledgeRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/videos/tagging/KnowledgeBundle;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/player/Director$1;->onResponse(Lcom/google/android/videos/tagging/KnowledgeRequest;Lcom/google/android/videos/tagging/KnowledgeBundle;)V

    return-void
.end method
