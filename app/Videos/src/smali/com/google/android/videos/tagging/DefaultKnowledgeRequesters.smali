.class Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters;
.super Ljava/lang/Object;
.source "DefaultKnowledgeRequesters.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$1;,
        Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;,
        Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;,
        Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;,
        Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;,
        Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;,
        Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;,
        Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;,
        Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponseConverter;,
        Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequestConverter;,
        Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;,
        Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequest;
    }
.end annotation


# direct methods
.method public static createAssetResourcesRequester(Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;)",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;"
        }
    .end annotation

    .prologue
    .line 177
    .local p0, "apiAssetsCachingRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;>;"
    new-instance v0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;

    invoke-direct {v0, p0}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;-><init>(Lcom/google/android/videos/async/Requester;)V

    return-object v0
.end method

.method public static createFileStoreBackedKnowledgeComponentRequester(Lcom/google/android/videos/store/AbstractFileStore;ZJLjava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;)Lcom/google/android/videos/async/Requester;
    .locals 8
    .param p1, "removeByPrefix"    # Z
    .param p2, "refreshAfterMillis"    # J
    .param p4, "localExecutor"    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;",
            "E:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/videos/store/AbstractFileStore",
            "<",
            "Ljava/lang/String;",
            "TS;>;ZJ",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/videos/async/Requester",
            "<TT;TS;>;",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler",
            "<TR;TE;TT;TS;>;)",
            "Lcom/google/android/videos/async/Requester",
            "<TR;TE;>;"
        }
    .end annotation

    .prologue
    .line 333
    .local p0, "fileStore":Lcom/google/android/videos/store/AbstractFileStore;, "Lcom/google/android/videos/store/AbstractFileStore<Ljava/lang/String;TS;>;"
    .local p5, "targetRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TT;TS;>;"
    .local p6, "requestHandler":Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;, "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler<TR;TE;TT;TS;>;"
    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 334
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 335
    new-instance v1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;

    move-object v2, p0

    move v3, p1

    move-wide v4, p2

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;-><init>(Lcom/google/android/videos/store/AbstractFileStore;ZJLcom/google/android/videos/async/Requester;Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;)V

    .line 337
    .local v1, "fileStoreBackedRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TR;TE;>;"
    invoke-static {p4, v1}, Lcom/google/android/videos/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/AsyncRequester;

    move-result-object v0

    return-object v0
.end method

.method public static createKnowledgeComponentRequester(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;)Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;",
            "E:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/videos/async/Requester",
            "<TT;TS;>;",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler",
            "<TR;TE;TT;TS;>;)",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester",
            "<TR;TE;TT;TS;>;"
        }
    .end annotation

    .prologue
    .line 325
    .local p0, "targetRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TT;TS;>;"
    .local p1, "requestHandler":Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;, "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler<TR;TE;TT;TS;>;"
    new-instance v1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-wide/16 v4, 0x0

    move-object v6, p0

    move-object v7, p1

    invoke-direct/range {v1 .. v7}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;-><init>(Lcom/google/android/videos/store/AbstractFileStore;ZJLcom/google/android/videos/async/Requester;Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;)V

    return-object v1
.end method

.method public static createTagStreamHttpRequester(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Ljava/util/concurrent/Executor;)Lcom/google/android/videos/async/Requester;
    .locals 3
    .param p0, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .param p1, "httpClient"    # Lorg/apache/http/client/HttpClient;
    .param p2, "networkExecutor"    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/accounts/AccountManagerWrapper;",
            "Lorg/apache/http/client/HttpClient;",
            "Ljava/util/concurrent/Executor;",
            ")",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequest;",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 87
    new-instance v0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequestConverter;

    invoke-direct {v0, v2}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequestConverter;-><init>(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$1;)V

    new-instance v1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponseConverter;

    invoke-direct {v1, v2}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponseConverter;-><init>(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$1;)V

    invoke-static {p1, v1}, Lcom/google/android/videos/async/HttpRequester;->create(Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/async/HttpRequester;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/videos/async/HttpAuthenticatingRequester;->create(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/converter/RequestConverter;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/HttpAuthenticatingRequester;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/google/android/videos/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/AsyncRequester;

    move-result-object v0

    return-object v0
.end method
