.class public Lcom/google/android/videos/remote/RemotePlaybackHelper;
.super Ljava/lang/Object;
.source "RemotePlaybackHelper.java"

# interfaces
.implements Lcom/google/android/videos/player/PlaybackHelper;
.implements Lcom/google/android/videos/remote/MediaRouteManager$Listener;
.implements Lcom/google/android/videos/remote/RemoteControlListener;
.implements Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;
    }
.end annotation


# instance fields
.field private final account:Ljava/lang/String;

.field private final activity:Landroid/app/Activity;

.field private audioTrackList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AudioInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final captionPreferences:Lcom/google/android/videos/player/CaptionPreferences;

.field private controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

.field private final directorRetryAction:Lcom/google/android/videos/utils/RetryAction;

.field private disableUiUpdate:Z

.field private haveAudioInDeviceLanguage:Z

.field private initializationFailed:Z

.field private final isTrailer:Z

.field private knowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

.field private final listener:Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;

.field private final mediaRouteManager:Lcom/google/android/videos/remote/MediaRouteManager;

.field private final playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

.field private final preferences:Landroid/content/SharedPreferences;

.field private final preferredLanguage:Ljava/lang/String;

.field private pushTimeout:Lcom/google/android/videos/remote/Timeout;

.field private remoteControl:Lcom/google/android/videos/remote/RemoteControl;

.field private remoteScreenPanelHelper:Lcom/google/android/videos/ui/RemoteScreenPanelHelper;

.field private final seasonId:Ljava/lang/String;

.field private shouldInitializeSubtitles:Z

.field private final showId:Ljava/lang/String;

.field private showTitle:Ljava/lang/String;

.field private state:I

.field private subtitleDefaultLanguage:Ljava/lang/String;

.field private subtitleMode:I

.field private videoDurationMillis:I

.field private final videoId:Ljava/lang/String;

.field private videoTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/content/SharedPreferences;Lcom/google/android/videos/remote/MediaRouteManager;Lcom/google/android/videos/player/PlaybackResumeState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/videos/utils/RetryAction;Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;Lcom/google/android/videos/player/CaptionPreferences;Ljava/lang/String;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "preferences"    # Landroid/content/SharedPreferences;
    .param p3, "mediaRouteManager"    # Lcom/google/android/videos/remote/MediaRouteManager;
    .param p4, "playbackResumeState"    # Lcom/google/android/videos/player/PlaybackResumeState;
    .param p5, "videoId"    # Ljava/lang/String;
    .param p6, "seasonId"    # Ljava/lang/String;
    .param p7, "showId"    # Ljava/lang/String;
    .param p8, "isTrailer"    # Z
    .param p9, "directorRetryAction"    # Lcom/google/android/videos/utils/RetryAction;
    .param p10, "listener"    # Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;
    .param p11, "captionPreferences"    # Lcom/google/android/videos/player/CaptionPreferences;
    .param p12, "account"    # Ljava/lang/String;

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    iput-object p1, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->activity:Landroid/app/Activity;

    .line 119
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->preferences:Landroid/content/SharedPreferences;

    .line 120
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/remote/MediaRouteManager;

    iput-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->mediaRouteManager:Lcom/google/android/videos/remote/MediaRouteManager;

    .line 121
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/player/PlaybackResumeState;

    iput-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    .line 122
    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->videoId:Ljava/lang/String;

    .line 123
    if-nez p7, :cond_0

    if-nez p6, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "ShowId cannot be null when seasonId is not null"

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 125
    iput-object p6, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->seasonId:Ljava/lang/String;

    .line 126
    iput-object p7, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->showId:Ljava/lang/String;

    .line 127
    iput-boolean p8, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->isTrailer:Z

    .line 128
    iput-object p9, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->directorRetryAction:Lcom/google/android/videos/utils/RetryAction;

    .line 129
    invoke-static {p10}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;

    iput-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->listener:Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;

    .line 130
    invoke-static {p11}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/player/CaptionPreferences;

    iput-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->captionPreferences:Lcom/google/android/videos/player/CaptionPreferences;

    .line 131
    iput-object p12, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->account:Ljava/lang/String;

    .line 132
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->getPreferredLanguage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->preferredLanguage:Ljava/lang/String;

    .line 134
    if-nez p8, :cond_1

    if-eqz p12, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_1
    const-string v1, "userAuth cannot be null when not playing trailers"

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 137
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->state:I

    .line 138
    return-void

    .line 123
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 134
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic access$002(Lcom/google/android/videos/remote/RemotePlaybackHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/remote/RemotePlaybackHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->shouldInitializeSubtitles:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/videos/remote/RemotePlaybackHelper;)Lcom/google/android/videos/player/overlay/ControllerOverlay;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/remote/RemotePlaybackHelper;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/remote/RemotePlaybackHelper;)Lcom/google/android/videos/remote/RemoteControl;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/remote/RemotePlaybackHelper;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    return-object v0
.end method

.method private getPreferredLanguage()Ljava/lang/String;
    .locals 4

    .prologue
    .line 687
    iget-object v2, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0238

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 688
    .local v1, "originalAudioPreference":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->activity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->preferences:Landroid/content/SharedPreferences;

    invoke-static {v2, v3}, Lcom/google/android/videos/utils/SettingsUtil;->getAudioPreferenceValue(Landroid/content/Context;Landroid/content/SharedPreferences;)Ljava/lang/String;

    move-result-object v0

    .line 689
    .local v0, "audioPreference":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 690
    const-string v2, "$ORIGINAL"

    .line 692
    :goto_0
    return-object v2

    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private static getStateName(I)Ljava/lang/String;
    .locals 1
    .param p0, "state"    # I

    .prologue
    .line 585
    packed-switch p0, :pswitch_data_0

    .line 595
    const-string v0, "unknown_state"

    :goto_0
    return-object v0

    .line 587
    :pswitch_0
    const-string v0, "idle"

    goto :goto_0

    .line 589
    :pswitch_1
    const-string v0, "waiting_for_load"

    goto :goto_0

    .line 591
    :pswitch_2
    const-string v0, "waiting_for_fling_ack"

    goto :goto_0

    .line 593
    :pswitch_3
    const-string v0, "active"

    goto :goto_0

    .line 585
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private resetTimeout()V
    .locals 1

    .prologue
    .line 433
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->pushTimeout:Lcom/google/android/videos/remote/Timeout;

    if-eqz v0, :cond_0

    .line 434
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->pushTimeout:Lcom/google/android/videos/remote/Timeout;

    invoke-virtual {v0}, Lcom/google/android/videos/remote/Timeout;->reset()V

    .line 435
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->pushTimeout:Lcom/google/android/videos/remote/Timeout;

    .line 437
    :cond_0
    return-void
.end method

.method private setActive()V
    .locals 1

    .prologue
    .line 480
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->resetTimeout()V

    .line 481
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->setState(I)V

    .line 482
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->showControls()V

    .line 483
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v0}, Lcom/google/android/videos/remote/RemoteControl;->getVideoInfo()Lcom/google/android/videos/remote/RemoteVideoInfo;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->onVideoInfoChanged(Lcom/google/android/videos/remote/RemoteVideoInfo;)V

    .line 484
    return-void
.end method

.method private setState(I)V
    .locals 2
    .param p1, "newState"    # I

    .prologue
    .line 603
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 604
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "Transitioning from state \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 605
    iget v1, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->state:I

    invoke-static {v1}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->getStateName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 606
    const-string v1, "\' to \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 607
    invoke-static {p1}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->getStateName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 608
    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 609
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 610
    iput p1, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->state:I

    .line 611
    return-void
.end method

.method private setVideoTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "videoTitle"    # Ljava/lang/String;

    .prologue
    .line 670
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->videoTitle:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 675
    :goto_0
    return-void

    .line 673
    :cond_0
    iput-object p1, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->videoTitle:Ljava/lang/String;

    .line 674
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->listener:Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;

    invoke-interface {v0, p1}, Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;->onRemoteVideoTitleChanged(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private showErrorToast()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 409
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->activity:Landroid/app/Activity;

    const v2, 0x7f0b015e

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v4}, Lcom/google/android/videos/remote/RemoteControl;->getScreenName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/google/android/videos/utils/Util;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    .line 411
    return-void
.end method

.method private updateControls(Lcom/google/android/videos/remote/RemoteVideoInfo;Lcom/google/android/videos/remote/PlayerState;)V
    .locals 7
    .param p1, "videoInfo"    # Lcom/google/android/videos/remote/RemoteVideoInfo;
    .param p2, "playerState"    # Lcom/google/android/videos/remote/PlayerState;

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 526
    invoke-static {}, Lcom/google/android/videos/utils/Preconditions;->checkMainThread()V

    .line 527
    if-eqz p1, :cond_0

    move v2, v3

    :goto_0
    invoke-static {v2}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    .line 528
    if-eqz p2, :cond_1

    move v2, v3

    :goto_1
    invoke-static {v2}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    .line 530
    iget-boolean v2, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->disableUiUpdate:Z

    if-eqz v2, :cond_2

    .line 579
    :goto_2
    return-void

    :cond_0
    move v2, v4

    .line 527
    goto :goto_0

    :cond_1
    move v2, v4

    .line 528
    goto :goto_1

    .line 534
    :cond_2
    iget-object v2, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteScreenPanelHelper:Lcom/google/android/videos/ui/RemoteScreenPanelHelper;

    iget v6, p2, Lcom/google/android/videos/remote/PlayerState;->state:I

    invoke-virtual {v2, v6}, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->setPlayerState(I)V

    .line 535
    iget-object v2, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteScreenPanelHelper:Lcom/google/android/videos/ui/RemoteScreenPanelHelper;

    iget-object v6, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->videoId:Ljava/lang/String;

    invoke-virtual {v2, v6}, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->setVideoId(Ljava/lang/String;)V

    .line 537
    iget-object v2, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v2}, Lcom/google/android/videos/remote/RemoteControl;->getErrorMessage()Ljava/lang/String;

    move-result-object v0

    .line 538
    .local v0, "error":Ljava/lang/String;
    :goto_3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 539
    const/4 v1, 0x0

    .line 540
    .local v1, "retryAction":Lcom/google/android/videos/utils/RetryAction;
    iget-object v2, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    if-eqz v2, :cond_3

    .line 541
    iget-object v2, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v2}, Lcom/google/android/videos/remote/RemoteControl;->isAttemptingReconnect()Z

    .line 542
    iget-object v2, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v2}, Lcom/google/android/videos/remote/RemoteControl;->getRetryAction()Lcom/google/android/videos/utils/RetryAction;

    move-result-object v1

    .line 545
    :cond_3
    iget-object v2, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v2, v0, v1}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setErrorState(Ljava/lang/String;Lcom/google/android/videos/utils/RetryAction;)V

    goto :goto_2

    .end local v0    # "error":Ljava/lang/String;
    .end local v1    # "retryAction":Lcom/google/android/videos/utils/RetryAction;
    :cond_4
    move-object v0, v5

    .line 537
    goto :goto_3

    .line 549
    .restart local v0    # "error":Ljava/lang/String;
    :cond_5
    iget-object v2, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    iget-object v6, p2, Lcom/google/android/videos/remote/PlayerState;->subtitleTrack:Lcom/google/android/videos/subtitles/SubtitleTrack;

    invoke-interface {v2, v6}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setSelectedSubtitleTrack(Lcom/google/android/videos/subtitles/SubtitleTrack;)V

    .line 551
    iget v2, p2, Lcom/google/android/videos/remote/PlayerState;->state:I

    packed-switch v2, :pswitch_data_0

    .line 577
    :goto_4
    iget-object v2, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    iget v3, p2, Lcom/google/android/videos/remote/PlayerState;->time:I

    iget v4, p1, Lcom/google/android/videos/remote/RemoteVideoInfo;->durationMillis:I

    iget v5, p2, Lcom/google/android/videos/remote/PlayerState;->bufferedPercentage:I

    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setTimes(III)V

    goto :goto_2

    .line 553
    :pswitch_0
    iget-object v2, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->listener:Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;

    invoke-interface {v2, v4, v4, v4}, Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;->onPlayerPause(III)V

    .line 554
    iget-object v2, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v2, v3, v3}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setState(ZZ)V

    goto :goto_4

    .line 557
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->listener:Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;

    invoke-interface {v2}, Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;->onPlayerPlay()V

    .line 559
    iget-object v2, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteScreenPanelHelper:Lcom/google/android/videos/ui/RemoteScreenPanelHelper;

    invoke-virtual {v2, v4}, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->setKeepScreenOn(Z)V

    .line 560
    iget-object v2, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v2, v3, v4}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setState(ZZ)V

    goto :goto_4

    .line 563
    :pswitch_2
    iget-object v2, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->listener:Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;

    invoke-interface {v2, v4, v4, v4}, Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;->onPlayerPause(III)V

    .line 564
    iget-object v2, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v2, v4, v4}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setState(ZZ)V

    goto :goto_4

    .line 567
    :pswitch_3
    iget-object v2, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->listener:Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;

    invoke-interface {v2}, Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;->onPlayerStop()V

    .line 568
    iget-object v2, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v2, v4, v4}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setState(ZZ)V

    goto :goto_4

    .line 571
    :pswitch_4
    iget-object v2, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->listener:Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;

    invoke-interface {v2}, Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;->onPlayerStop()V

    .line 572
    iget-boolean v2, p2, Lcom/google/android/videos/remote/PlayerState;->canRetry:Z

    if-eqz v2, :cond_6

    iget-object v1, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->directorRetryAction:Lcom/google/android/videos/utils/RetryAction;

    .line 573
    .restart local v1    # "retryAction":Lcom/google/android/videos/utils/RetryAction;
    :goto_5
    iget-object v2, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    iget-object v3, p2, Lcom/google/android/videos/remote/PlayerState;->errorMessage:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setErrorState(Ljava/lang/String;Lcom/google/android/videos/utils/RetryAction;)V

    goto :goto_4

    .end local v1    # "retryAction":Lcom/google/android/videos/utils/RetryAction;
    :cond_6
    move-object v1, v5

    .line 572
    goto :goto_5

    .line 551
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method private updateState(Lcom/google/android/videos/remote/PlayerState;)V
    .locals 3
    .param p1, "playerState"    # Lcom/google/android/videos/remote/PlayerState;

    .prologue
    const/4 v2, 0x5

    .line 443
    if-nez p1, :cond_1

    .line 473
    :cond_0
    :goto_0
    return-void

    .line 447
    :cond_1
    iget v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->state:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    iget v0, p1, Lcom/google/android/videos/remote/PlayerState;->state:I

    if-nez v0, :cond_2

    .line 448
    const-string v0, "Video stopped abruptly"

    invoke-static {v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 449
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->listener:Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;->onExitRemotePlayback()V

    goto :goto_0

    .line 453
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->videoId:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/videos/remote/PlayerState;->videoId:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 455
    iget v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->state:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 457
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->setActive()V

    .line 460
    :pswitch_2
    iget v0, p1, Lcom/google/android/videos/remote/PlayerState;->state:I

    if-ne v0, v2, :cond_0

    .line 461
    const-string v0, "Video finished"

    invoke-static {v0}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 462
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->listener:Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;->onExitRemotePlayback()V

    goto :goto_0

    .line 466
    :pswitch_3
    iget v0, p1, Lcom/google/android/videos/remote/PlayerState;->state:I

    if-eq v0, v2, :cond_0

    .line 467
    const-string v0, "Fling acknowledged"

    invoke-static {v0}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 468
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->setActive()V

    goto :goto_0

    .line 455
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private updateSubtitleTrackList(Lcom/google/android/videos/remote/SubtitleTrackList;)V
    .locals 9
    .param p1, "trackList"    # Lcom/google/android/videos/remote/SubtitleTrackList;

    .prologue
    const/4 v8, 0x0

    .line 414
    if-nez p1, :cond_0

    .line 415
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->clearSubtitles()V

    .line 427
    :goto_0
    return-void

    .line 417
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    iget-object v2, p1, Lcom/google/android/videos/remote/SubtitleTrackList;->tracks:Ljava/util/List;

    iget v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->subtitleMode:I

    const/4 v3, 0x3

    if-eq v0, v3, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-interface {v1, v2, v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setSubtitles(Ljava/util/List;Z)V

    .line 419
    iget-boolean v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->shouldInitializeSubtitles:Z

    if-eqz v0, :cond_1

    .line 420
    iget-object v0, p1, Lcom/google/android/videos/remote/SubtitleTrackList;->tracks:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->captionPreferences:Lcom/google/android/videos/player/CaptionPreferences;

    iget-object v2, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->preferences:Landroid/content/SharedPreferences;

    iget v3, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->subtitleMode:I

    iget-object v4, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->subtitleDefaultLanguage:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->haveAudioInDeviceLanguage:Z

    iget-object v6, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    invoke-static/range {v0 .. v6}, Lcom/google/android/videos/subtitles/TrackSelectionUtil;->selectTrack(Ljava/util/List;Lcom/google/android/videos/player/CaptionPreferences;Landroid/content/SharedPreferences;ILjava/lang/String;ZLcom/google/android/videos/player/PlaybackResumeState;)Lcom/google/android/videos/subtitles/SubtitleTrack;

    move-result-object v7

    .line 423
    .local v7, "track":Lcom/google/android/videos/subtitles/SubtitleTrack;
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v0, v7}, Lcom/google/android/videos/remote/RemoteControl;->setSubtitles(Lcom/google/android/videos/subtitles/SubtitleTrack;)Z

    .line 425
    .end local v7    # "track":Lcom/google/android/videos/subtitles/SubtitleTrack;
    :cond_1
    iput-boolean v8, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->shouldInitializeSubtitles:Z

    goto :goto_0

    :cond_2
    move v0, v8

    .line 417
    goto :goto_1
.end method

.method private updateUi()V
    .locals 2

    .prologue
    .line 492
    iget v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->state:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 493
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->updateUiWithRemoteControlState()V

    .line 497
    :goto_0
    return-void

    .line 495
    :cond_0
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->updateUiWithRemotePlaybackHelperState()V

    goto :goto_0
.end method

.method private updateUiWithRemoteControlState()V
    .locals 3

    .prologue
    .line 503
    iget-object v2, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v2}, Lcom/google/android/videos/remote/RemoteControl;->getVideoInfo()Lcom/google/android/videos/remote/RemoteVideoInfo;

    move-result-object v1

    .line 504
    .local v1, "videoInfo":Lcom/google/android/videos/remote/RemoteVideoInfo;
    iget-object v2, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v2}, Lcom/google/android/videos/remote/RemoteControl;->getPlayerState()Lcom/google/android/videos/remote/PlayerState;

    move-result-object v0

    .line 505
    .local v0, "playerState":Lcom/google/android/videos/remote/PlayerState;
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 506
    invoke-direct {p0, v1, v0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->updateControls(Lcom/google/android/videos/remote/RemoteVideoInfo;Lcom/google/android/videos/remote/PlayerState;)V

    .line 508
    :cond_0
    return-void
.end method

.method private updateUiWithRemotePlaybackHelperState()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 515
    new-instance v0, Lcom/google/android/videos/remote/RemoteVideoInfo;

    iget-object v1, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->videoId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->videoTitle:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->showTitle:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->isTrailer:Z

    iget v5, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->videoDurationMillis:I

    const-string v6, ""

    iget-object v7, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->preferredLanguage:Ljava/lang/String;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/remote/RemoteVideoInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;Ljava/lang/String;)V

    .line 517
    .local v0, "videoInfo":Lcom/google/android/videos/remote/RemoteVideoInfo;
    new-instance v1, Lcom/google/android/videos/remote/PlayerState;

    iget-object v2, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->videoId:Ljava/lang/String;

    const/4 v3, 0x1

    const/4 v6, 0x0

    const-string v7, ""

    move v4, v9

    move v5, v9

    move v8, v9

    invoke-direct/range {v1 .. v8}, Lcom/google/android/videos/remote/PlayerState;-><init>(Ljava/lang/String;IIILcom/google/android/videos/subtitles/SubtitleTrack;Ljava/lang/String;Z)V

    .line 518
    .local v1, "playerState":Lcom/google/android/videos/remote/PlayerState;
    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->updateControls(Lcom/google/android/videos/remote/RemoteVideoInfo;Lcom/google/android/videos/remote/PlayerState;)V

    .line 519
    return-void
.end method


# virtual methods
.method public getDisplayType()I
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    instance-of v0, v0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPlayerTimeMillis()I
    .locals 1

    .prologue
    .line 679
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    if-nez v0, :cond_0

    .line 680
    const/4 v0, -0x1

    .line 682
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v0}, Lcom/google/android/videos/remote/RemoteControl;->calculateExtrapolatedPlayerTime()I

    move-result v0

    goto :goto_0
.end method

.method public init(Z)V
    .locals 3
    .param p1, "playWhenInitialized"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/player/PlaybackHelper$DisplayNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 167
    invoke-direct {p0, v2}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->setState(I)V

    .line 168
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0, v2}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setUseScrubPad(Z)V

    .line 169
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0, v1}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setHideable(Z)V

    .line 170
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0, v1}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setHasKnowledge(Z)V

    .line 171
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->clearSubtitles()V

    .line 172
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0, v1}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setSupportsQualityToggle(Z)V

    .line 173
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->mediaRouteManager:Lcom/google/android/videos/remote/MediaRouteManager;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/remote/MediaRouteManager;->register(Lcom/google/android/videos/remote/MediaRouteManager$Listener;)V

    .line 174
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    if-eqz v0, :cond_1

    .line 175
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteScreenPanelHelper:Lcom/google/android/videos/ui/RemoteScreenPanelHelper;

    iget-object v1, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v1}, Lcom/google/android/videos/remote/RemoteControl;->getScreenName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->init(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    iget-boolean v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->initializationFailed:Z

    if-eqz v0, :cond_0

    .line 177
    new-instance v0, Lcom/google/android/videos/player/PlaybackHelper$DisplayNotFoundException;

    invoke-direct {v0}, Lcom/google/android/videos/player/PlaybackHelper$DisplayNotFoundException;-><init>()V

    throw v0

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v0}, Lcom/google/android/videos/remote/RemoteControl;->getPlayerState()Lcom/google/android/videos/remote/PlayerState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->onPlayerStateChanged(Lcom/google/android/videos/remote/PlayerState;)V

    .line 180
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v0}, Lcom/google/android/videos/remote/RemoteControl;->getVideoInfo()Lcom/google/android/videos/remote/RemoteVideoInfo;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->onVideoInfoChanged(Lcom/google/android/videos/remote/RemoteVideoInfo;)V

    .line 181
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->updateUi()V

    .line 183
    :cond_1
    return-void
.end method

.method public initKnowledgeViewHelper(Lcom/google/android/videos/tagging/KnowledgeViewHelper;)V
    .locals 3
    .param p1, "knowledgeViewHelper"    # Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    .prologue
    .line 161
    const/4 v0, 0x0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->knowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    invoke-virtual {p1, v0, v1, v2, p0}, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->init(FLcom/google/android/videos/tagging/KnowledgeView;Lcom/google/android/videos/tagging/KnowledgeView;Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;)V

    .line 162
    return-void
.end method

.method public load(Lcom/google/android/videos/player/DirectorInitializer$InitializationData;)V
    .locals 23
    .param p1, "initData"    # Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    .prologue
    .line 213
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->videoTitle:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->setVideoTitle(Ljava/lang/String;)V

    .line 214
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->showTitle:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->showTitle:Ljava/lang/String;

    .line 215
    move-object/from16 v0, p1

    iget v3, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->durationMillis:I

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->videoDurationMillis:I

    .line 216
    move-object/from16 v0, p1

    iget v3, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->subtitleMode:I

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->subtitleMode:I

    .line 217
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->subtitleDefaultLanguage:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->subtitleDefaultLanguage:Ljava/lang/String;

    .line 218
    move-object/from16 v0, p1

    iget-boolean v3, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->haveAudioInDeviceLanguage:Z

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->haveAudioInDeviceLanguage:Z

    .line 220
    new-instance v3, Lcom/google/android/videos/remote/ClientState$Builder;

    invoke-direct {v3}, Lcom/google/android/videos/remote/ClientState$Builder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->videoId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/videos/remote/ClientState$Builder;->videoId(Ljava/lang/String;)Lcom/google/android/videos/remote/ClientState$Builder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->showId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/videos/remote/ClientState$Builder;->showId(Ljava/lang/String;)Lcom/google/android/videos/remote/ClientState$Builder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->seasonId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/videos/remote/ClientState$Builder;->seasonId(Ljava/lang/String;)Lcom/google/android/videos/remote/ClientState$Builder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->account:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/videos/remote/ClientState$Builder;->account(Ljava/lang/String;)Lcom/google/android/videos/remote/ClientState$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/videos/remote/ClientState$Builder;->build()Lcom/google/android/videos/remote/ClientState;

    move-result-object v21

    .line 226
    .local v21, "clientState":Lcom/google/android/videos/remote/ClientState;
    invoke-virtual/range {v21 .. v21}, Lcom/google/android/videos/remote/ClientState;->toUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    .line 228
    .local v8, "opaque":Ljava/lang/String;
    new-instance v2, Lcom/google/android/videos/remote/RemoteVideoInfo;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->videoId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->videoTitle:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->showTitle:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->isTrailer:Z

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->videoDurationMillis:I

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->preferredLanguage:Ljava/lang/String;

    invoke-direct/range {v2 .. v9}, Lcom/google/android/videos/remote/RemoteVideoInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;Ljava/lang/String;)V

    .line 231
    .local v2, "videoInfo":Lcom/google/android/videos/remote/RemoteVideoInfo;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->showId:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v14, 0x1

    .line 233
    .local v14, "isEpisode":Z
    :goto_0
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->account:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v12, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->resumeTimeMillis:I

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->isTrailer:Z

    move-object/from16 v0, p1

    iget v3, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->pinnedStorage:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v15, 0x1

    :goto_1
    move-object v11, v2

    invoke-virtual/range {v9 .. v15}, Lcom/google/android/videos/remote/RemoteControl;->fling(Ljava/lang/String;Lcom/google/android/videos/remote/RemoteVideoInfo;IZZZ)I

    move-result v22

    .line 236
    .local v22, "flingResult":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "RemoteControl.fling() returned "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v22

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 238
    packed-switch v22, :pswitch_data_0

    .line 268
    :goto_2
    invoke-direct/range {p0 .. p0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->updateUi()V

    .line 269
    return-void

    .line 231
    .end local v14    # "isEpisode":Z
    .end local v22    # "flingResult":I
    :cond_0
    const/4 v14, 0x0

    goto :goto_0

    .line 233
    .restart local v14    # "isEpisode":Z
    :cond_1
    const/4 v15, 0x0

    goto :goto_1

    .line 240
    .restart local v22    # "flingResult":I
    :pswitch_0
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->shouldInitializeSubtitles:Z

    .line 241
    const/4 v3, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->setState(I)V

    .line 242
    invoke-direct/range {p0 .. p0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->resetTimeout()V

    .line 243
    new-instance v15, Lcom/google/android/videos/remote/RemotePlaybackHelper$1;

    new-instance v17, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-direct {v0, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    const-wide/16 v18, 0x4e20

    move-object/from16 v16, p0

    move-object/from16 v20, p1

    invoke-direct/range {v15 .. v20}, Lcom/google/android/videos/remote/RemotePlaybackHelper$1;-><init>(Lcom/google/android/videos/remote/RemotePlaybackHelper;Landroid/os/Handler;JLcom/google/android/videos/player/DirectorInitializer$InitializationData;)V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->pushTimeout:Lcom/google/android/videos/remote/Timeout;

    .line 258
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->pushTimeout:Lcom/google/android/videos/remote/Timeout;

    invoke-virtual {v3}, Lcom/google/android/videos/remote/Timeout;->start()V

    goto :goto_2

    .line 261
    :pswitch_1
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->shouldInitializeSubtitles:Z

    .line 262
    invoke-direct/range {p0 .. p0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->setActive()V

    goto :goto_2

    .line 238
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public maybeJoinCurrentPlayback(Ljava/lang/String;)Z
    .locals 6
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 198
    iget-object v4, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    if-eqz v4, :cond_2

    .line 199
    iget-object v4, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v4}, Lcom/google/android/videos/remote/RemoteControl;->getPlayerState()Lcom/google/android/videos/remote/PlayerState;

    move-result-object v0

    .line 200
    .local v0, "playerState":Lcom/google/android/videos/remote/PlayerState;
    if-eqz p1, :cond_0

    if-eqz v0, :cond_1

    iget v4, v0, Lcom/google/android/videos/remote/PlayerState;->state:I

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    iget-object v4, v0, Lcom/google/android/videos/remote/PlayerState;->videoId:Ljava/lang/String;

    invoke-static {p1, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    move v1, v2

    .line 203
    .local v1, "shouldJoinPlayback":Z
    :goto_0
    if-eqz v1, :cond_2

    .line 204
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->setActive()V

    .line 208
    .end local v0    # "playerState":Lcom/google/android/videos/remote/PlayerState;
    .end local v1    # "shouldJoinPlayback":Z
    :goto_1
    return v2

    .restart local v0    # "playerState":Lcom/google/android/videos/remote/PlayerState;
    :cond_1
    move v1, v3

    .line 200
    goto :goto_0

    .end local v0    # "playerState":Lcom/google/android/videos/remote/PlayerState;
    :cond_2
    move v2, v3

    .line 208
    goto :goto_1
.end method

.method public onAudioTracksChanged(Ljava/util/List;I)V
    .locals 1
    .param p2, "selectedAudioTrackIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AudioInfo;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 403
    .local p1, "audioTrackList":Ljava/util/List;, "Ljava/util/List<Lcom/google/wireless/android/video/magma/proto/AudioInfo;>;"
    iput-object p1, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->audioTrackList:Ljava/util/List;

    .line 404
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0, p1}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setAudioTracks(Ljava/util/List;)V

    .line 405
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0, p2}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setSelectedAudioTrackIndex(I)V

    .line 406
    return-void
.end method

.method public onDisconnected()V
    .locals 1

    .prologue
    .line 666
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->listener:Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;->onExitRemotePlayback()V

    .line 667
    return-void
.end method

.method public onHQ()V
    .locals 0

    .prologue
    .line 315
    return-void
.end method

.method public onPlayerStateChanged(Lcom/google/android/videos/remote/PlayerState;)V
    .locals 0
    .param p1, "playerState"    # Lcom/google/android/videos/remote/PlayerState;

    .prologue
    .line 392
    invoke-direct {p0, p1}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->updateState(Lcom/google/android/videos/remote/PlayerState;)V

    .line 393
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->updateUi()V

    .line 394
    return-void
.end method

.method public onRemoteControlError(ILcom/google/android/videos/utils/RetryAction;)V
    .locals 2
    .param p1, "error"    # I
    .param p2, "retryAction"    # Lcom/google/android/videos/utils/RetryAction;

    .prologue
    .line 378
    iget-object v1, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v1}, Lcom/google/android/videos/remote/RemoteControl;->getErrorMessage()Ljava/lang/String;

    move-result-object v0

    .line 379
    .local v0, "message":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 380
    iget-object v1, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v1, v0, p2}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setErrorState(Ljava/lang/String;Lcom/google/android/videos/utils/RetryAction;)V

    .line 382
    :cond_0
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->resetTimeout()V

    .line 383
    return-void
.end method

.method public onRemoteControlErrorCleared()V
    .locals 0

    .prologue
    .line 387
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->updateUi()V

    .line 388
    return-void
.end method

.method public onRemoteControlSelected(Lcom/google/android/videos/remote/RemoteControl;)V
    .locals 2
    .param p1, "remoteControl"    # Lcom/google/android/videos/remote/RemoteControl;

    .prologue
    const/4 v1, 0x0

    .line 361
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    if-eqz v0, :cond_0

    .line 362
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/remote/RemoteControl;->removeListener(Lcom/google/android/videos/remote/RemoteControlListener;)V

    .line 363
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->resetTimeout()V

    .line 365
    :cond_0
    iput-object p1, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    .line 366
    if-eqz p1, :cond_1

    .line 367
    invoke-virtual {p1, p0}, Lcom/google/android/videos/remote/RemoteControl;->addListener(Lcom/google/android/videos/remote/RemoteControlListener;)V

    .line 368
    invoke-virtual {p1}, Lcom/google/android/videos/remote/RemoteControl;->getSubtitleTrackList()Lcom/google/android/videos/remote/SubtitleTrackList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->updateSubtitleTrackList(Lcom/google/android/videos/remote/SubtitleTrackList;)V

    .line 369
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteScreenPanelHelper:Lcom/google/android/videos/ui/RemoteScreenPanelHelper;

    invoke-virtual {p1}, Lcom/google/android/videos/remote/RemoteControl;->getScreenName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->setScreenName(Ljava/lang/String;)V

    .line 374
    :goto_0
    return-void

    .line 371
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0, v1, v1}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setState(ZZ)V

    .line 372
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->showControls()V

    goto :goto_0
.end method

.method public onScrubbingCanceled()V
    .locals 0

    .prologue
    .line 309
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->updateUi()V

    .line 310
    return-void
.end method

.method public onScrubbingStart(Z)V
    .locals 1
    .param p1, "isFineScrubbing"    # Z

    .prologue
    .line 293
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->listener:Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;->onPlayerScrubbingStart()V

    .line 294
    return-void
.end method

.method public onSeekTo(ZIZ)V
    .locals 2
    .param p1, "isFineScrubbing"    # Z
    .param p2, "time"    # I
    .param p3, "scrubbingEnded"    # Z

    .prologue
    .line 298
    if-nez p3, :cond_1

    .line 305
    :cond_0
    :goto_0
    return-void

    .line 302
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    iget-object v1, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/videos/remote/RemoteControl;->seekTo(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 303
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->showErrorToast()V

    goto :goto_0
.end method

.method public onSelectedAudioTrackIndexChanged(I)V
    .locals 2
    .param p1, "trackIndex"    # I

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->audioTrackList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->audioTrackList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 340
    :cond_0
    :goto_0
    return-void

    .line 335
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    if-eqz v0, :cond_0

    .line 336
    iget-object v1, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->audioTrackList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/remote/RemoteControl;->setAudioTrack(Lcom/google/wireless/android/video/magma/proto/AudioInfo;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 337
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->showErrorToast()V

    goto :goto_0
.end method

.method public onSelectedSubtitleTrackChanged(Lcom/google/android/videos/subtitles/SubtitleTrack;)V
    .locals 2
    .param p1, "track"    # Lcom/google/android/videos/subtitles/SubtitleTrack;

    .prologue
    .line 319
    iget-object v1, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    if-eqz v1, :cond_0

    .line 320
    iget-object v1, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v1, p1}, Lcom/google/android/videos/remote/RemoteControl;->setSubtitles(Lcom/google/android/videos/subtitles/SubtitleTrack;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 321
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->showErrorToast()V

    .line 324
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    if-eqz v1, :cond_1

    .line 325
    if-eqz p1, :cond_2

    iget-object v0, p1, Lcom/google/android/videos/subtitles/SubtitleTrack;->languageCode:Ljava/lang/String;

    .line 326
    .local v0, "languageCode":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/player/PlaybackResumeState;->setSubtitleLanguage(Ljava/lang/String;)V

    .line 328
    .end local v0    # "languageCode":Ljava/lang/String;
    :cond_1
    return-void

    .line 325
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSubtitleTracksChanged(Lcom/google/android/videos/remote/SubtitleTrackList;)V
    .locals 0
    .param p1, "subtitleTrackList"    # Lcom/google/android/videos/remote/SubtitleTrackList;

    .prologue
    .line 398
    invoke-direct {p0, p1}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->updateSubtitleTrackList(Lcom/google/android/videos/remote/SubtitleTrackList;)V

    .line 399
    return-void
.end method

.method public onVideoInfoChanged(Lcom/google/android/videos/remote/RemoteVideoInfo;)V
    .locals 3
    .param p1, "videoInfo"    # Lcom/google/android/videos/remote/RemoteVideoInfo;

    .prologue
    const/4 v2, 0x3

    .line 615
    if-nez p1, :cond_1

    .line 616
    iget v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->state:I

    if-ne v0, v2, :cond_0

    .line 617
    const-string v0, "Video info cleared abruptly"

    invoke-static {v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 618
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->listener:Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;->onExitRemotePlayback()V

    .line 648
    :cond_0
    :goto_0
    return-void

    .line 623
    :cond_1
    iget-object v0, p1, Lcom/google/android/videos/remote/RemoteVideoInfo;->videoId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->videoId:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 624
    iget v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->state:I

    if-eq v0, v2, :cond_2

    iget v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->state:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 625
    :cond_2
    const-string v0, "Remote player playing a different video; closing activity."

    invoke-static {v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 626
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->listener:Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;->onExitRemotePlayback()V

    goto :goto_0

    .line 631
    :cond_3
    iget v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->state:I

    if-nez v0, :cond_4

    .line 634
    invoke-direct {p0, v2}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->setState(I)V

    .line 637
    :cond_4
    iget v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->state:I

    if-ne v0, v2, :cond_0

    .line 641
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v0}, Lcom/google/android/videos/remote/RemoteControl;->requestAudioTracks()Z

    .line 643
    iget-object v0, p1, Lcom/google/android/videos/remote/RemoteVideoInfo;->videoTitle:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->setVideoTitle(Ljava/lang/String;)V

    .line 645
    iget-object v0, p1, Lcom/google/android/videos/remote/RemoteVideoInfo;->showTitle:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->showTitle:Ljava/lang/String;

    .line 647
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->updateUi()V

    goto :goto_0
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 286
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    iget-object v1, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/remote/RemoteControl;->pause(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 287
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->showErrorToast()V

    .line 289
    :cond_0
    return-void
.end method

.method public play(Z)V
    .locals 2
    .param p1, "onInitialized"    # Z

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->listener:Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;->onPlayerPlay()V

    .line 274
    if-nez p1, :cond_0

    .line 275
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    iget-object v1, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/remote/RemoteControl;->play(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 276
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->showErrorToast()V

    .line 282
    :cond_0
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 187
    const-string v0, "release() called"

    invoke-static {v0}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 188
    invoke-direct {p0, v1}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->setState(I)V

    .line 189
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->mediaRouteManager:Lcom/google/android/videos/remote/MediaRouteManager;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/remote/MediaRouteManager;->unregister(Lcom/google/android/videos/remote/MediaRouteManager$Listener;)V

    .line 190
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->onRemoteControlSelected(Lcom/google/android/videos/remote/RemoteControl;)V

    .line 191
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->reset()V

    .line 192
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteScreenPanelHelper:Lcom/google/android/videos/ui/RemoteScreenPanelHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->reset()V

    .line 193
    iput-boolean v1, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->disableUiUpdate:Z

    .line 194
    return-void
.end method

.method public setErrorState(Ljava/lang/String;Lcom/google/android/videos/utils/RetryAction;)V
    .locals 2
    .param p1, "errorMessage"    # Ljava/lang/String;
    .param p2, "retryAction"    # Lcom/google/android/videos/utils/RetryAction;

    .prologue
    .line 344
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteScreenPanelHelper:Lcom/google/android/videos/ui/RemoteScreenPanelHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->setKeepScreenOn(Z)V

    .line 345
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->disableUiUpdate:Z

    .line 346
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0, p1, p2}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setErrorState(Ljava/lang/String;Lcom/google/android/videos/utils/RetryAction;)V

    .line 347
    return-void
.end method

.method public setState(ZZ)V
    .locals 1
    .param p1, "playWhenReady"    # Z
    .param p2, "loading"    # Z

    .prologue
    .line 351
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteScreenPanelHelper:Lcom/google/android/videos/ui/RemoteScreenPanelHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->setKeepScreenOn(Z)V

    .line 352
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->disableUiUpdate:Z

    .line 353
    if-eqz p1, :cond_0

    .line 354
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteControl:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v0}, Lcom/google/android/videos/remote/RemoteControl;->prepareForFling()V

    .line 356
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0, p1, p2}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setState(ZZ)V

    .line 357
    return-void
.end method

.method public setViews(Lcom/google/android/videos/player/PlayerView;Lcom/google/android/videos/player/overlay/ControllerOverlay;Lcom/google/android/videos/player/overlay/SubtitlesOverlay;Lcom/google/android/videos/ui/RemoteScreenPanelHelper;Lcom/google/android/videos/tagging/KnowledgeView;Lcom/google/android/videos/tagging/KnowledgeView;)V
    .locals 1
    .param p1, "playerView"    # Lcom/google/android/videos/player/PlayerView;
    .param p2, "controllerOverlay"    # Lcom/google/android/videos/player/overlay/ControllerOverlay;
    .param p3, "subtitlesOverlay"    # Lcom/google/android/videos/player/overlay/SubtitlesOverlay;
    .param p4, "remoteScreenPanelHelper"    # Lcom/google/android/videos/ui/RemoteScreenPanelHelper;
    .param p5, "localKnowledgeView"    # Lcom/google/android/videos/tagging/KnowledgeView;
    .param p6, "localTaglessKnowledgeView"    # Lcom/google/android/videos/tagging/KnowledgeView;

    .prologue
    .line 144
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/player/overlay/ControllerOverlay;

    iput-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->controllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    .line 145
    iput-object p5, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->knowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    .line 146
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;

    iput-object v0, p0, Lcom/google/android/videos/remote/RemotePlaybackHelper;->remoteScreenPanelHelper:Lcom/google/android/videos/ui/RemoteScreenPanelHelper;

    .line 147
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->hideControls(Z)V

    .line 148
    invoke-direct {p0}, Lcom/google/android/videos/remote/RemotePlaybackHelper;->updateUi()V

    .line 149
    return-void
.end method
