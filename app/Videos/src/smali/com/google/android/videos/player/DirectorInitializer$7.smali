.class Lcom/google/android/videos/player/DirectorInitializer$7;
.super Ljava/lang/Object;
.source "DirectorInitializer.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/player/DirectorInitializer;->initSyncPurchaseCallback()Lcom/google/android/videos/async/Callback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/player/DirectorInitializer;

.field final synthetic val$freshPurchaseCallback:Lcom/google/android/videos/async/Callback;


# direct methods
.method constructor <init>(Lcom/google/android/videos/player/DirectorInitializer;Lcom/google/android/videos/async/Callback;)V
    .locals 0

    .prologue
    .line 810
    iput-object p1, p0, Lcom/google/android/videos/player/DirectorInitializer$7;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    iput-object p2, p0, Lcom/google/android/videos/player/DirectorInitializer$7;->val$freshPurchaseCallback:Lcom/google/android/videos/async/Callback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Ljava/lang/Exception;)V
    .locals 3
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 821
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer$7;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    const/4 v1, 0x2

    const/4 v2, 0x1

    # invokes: Lcom/google/android/videos/player/DirectorInitializer;->onInitializationError(ILjava/lang/Exception;Z)V
    invoke-static {v0, v1, p2, v2}, Lcom/google/android/videos/player/DirectorInitializer;->access$2900(Lcom/google/android/videos/player/DirectorInitializer;ILjava/lang/Exception;Z)V

    .line 822
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 810
    check-cast p1, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/player/DirectorInitializer$7;->onError(Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Ljava/lang/Void;)V
    .locals 4
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;
    .param p2, "response"    # Ljava/lang/Void;

    .prologue
    .line 813
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer$7;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;
    invoke-static {v0}, Lcom/google/android/videos/player/DirectorInitializer;->access$3200(Lcom/google/android/videos/player/DirectorInitializer;)Lcom/google/android/videos/store/PurchaseStore;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->account:Ljava/lang/String;

    sget-object v2, Lcom/google/android/videos/player/DirectorInitializer$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/player/DirectorInitializer$7;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->videoId:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/videos/player/DirectorInitializer;->access$100(Lcom/google/android/videos/player/DirectorInitializer;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/videos/store/PurchaseRequests;->createPurchaseRequestForUser(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/player/DirectorInitializer$7;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    iget-object v3, p0, Lcom/google/android/videos/player/DirectorInitializer$7;->val$freshPurchaseCallback:Lcom/google/android/videos/async/Callback;

    # invokes: Lcom/google/android/videos/player/DirectorInitializer;->decorateCallback(Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/Callback;
    invoke-static {v2, v3}, Lcom/google/android/videos/player/DirectorInitializer;->access$3100(Lcom/google/android/videos/player/DirectorInitializer;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/Callback;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/videos/async/Callback;)V

    .line 817
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer$7;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # invokes: Lcom/google/android/videos/player/DirectorInitializer;->onTaskCompleted()V
    invoke-static {v0}, Lcom/google/android/videos/player/DirectorInitializer;->access$3300(Lcom/google/android/videos/player/DirectorInitializer;)V

    .line 818
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 810
    check-cast p1, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/lang/Void;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/player/DirectorInitializer$7;->onResponse(Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Ljava/lang/Void;)V

    return-void
.end method
