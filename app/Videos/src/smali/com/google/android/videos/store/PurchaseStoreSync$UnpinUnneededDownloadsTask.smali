.class Lcom/google/android/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;
.super Lcom/google/android/videos/store/SyncTaskManager$SyncTask;
.source "PurchaseStoreSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/PurchaseStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UnpinUnneededDownloadsTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

.field private final whereArgs:[Ljava/lang/String;

.field private final whereClause:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/store/PurchaseStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V
    .locals 1
    .param p2, "priority"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/store/SyncTaskManager$SharedStates",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 935
    .local p3, "states":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<*>;"
    iput-object p1, p0, Lcom/google/android/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    .line 936
    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->syncTaskManager:Lcom/google/android/videos/store/SyncTaskManager;
    invoke-static {p1}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$300(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/SyncTaskManager;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;-><init>(Lcom/google/android/videos/store/SyncTaskManager;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V

    .line 937
    const-string v0, "asset_type IN (6,20) AND (pinned IS NOT NULL AND pinned > 0) AND (hidden IN (1, 3) OR purchase_status != 2)"

    iput-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->whereClause:Ljava/lang/String;

    .line 938
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->whereArgs:[Ljava/lang/String;

    .line 939
    return-void
.end method

.method public constructor <init>(Lcom/google/android/videos/store/PurchaseStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V
    .locals 3
    .param p2, "priority"    # I
    .param p4, "account"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/store/SyncTaskManager$SharedStates",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 927
    .local p3, "states":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<*>;"
    iput-object p1, p0, Lcom/google/android/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    .line 928
    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->syncTaskManager:Lcom/google/android/videos/store/SyncTaskManager;
    invoke-static {p1}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$300(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/SyncTaskManager;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;-><init>(Lcom/google/android/videos/store/SyncTaskManager;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V

    .line 929
    const-string v0, "account = ? AND (asset_type IN (6,20) AND (pinned IS NOT NULL AND pinned > 0) AND (hidden IN (1, 3) OR purchase_status != 2))"

    iput-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->whereClause:Ljava/lang/String;

    .line 930
    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v1, v2

    iput-object v1, p0, Lcom/google/android/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->whereArgs:[Ljava/lang/String;

    .line 933
    return-void
.end method


# virtual methods
.method protected doSync()V
    .locals 12

    .prologue
    const/4 v5, 0x0

    .line 943
    const/4 v10, 0x0

    .line 944
    .local v10, "shouldPingTransferService":Z
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v0}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$100(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "purchased_assets"

    sget-object v2, Lcom/google/android/videos/store/PurchaseStoreSync$ShouldUnpinQuery;->PROJECTION:[Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->whereClause:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->whereArgs:[Ljava/lang/String;

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 947
    .local v9, "cursor":Landroid/database/Cursor;
    :goto_0
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 948
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 949
    .local v8, "account":Ljava/lang/String;
    const/4 v0, 0x1

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 950
    .local v11, "videoId":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v0}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$100(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v0

    invoke-static {v0, v8, v11}, Lcom/google/android/videos/pinning/PinningDbHelper;->unpin(Lcom/google/android/videos/store/Database;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    or-int/2addr v10, v0

    .line 952
    goto :goto_0

    .line 954
    .end local v8    # "account":Ljava/lang/String;
    .end local v11    # "videoId":Ljava/lang/String;
    :cond_0
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 955
    if-eqz v10, :cond_1

    .line 956
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$700(Lcom/google/android/videos/store/PurchaseStoreSync;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->context:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$700(Lcom/google/android/videos/store/PurchaseStoreSync;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/pinning/TransferService;->createIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 959
    :cond_1
    return-void

    .line 954
    :catchall_0
    move-exception v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 955
    if-eqz v10, :cond_2

    .line 956
    iget-object v1, p0, Lcom/google/android/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->context:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$700(Lcom/google/android/videos/store/PurchaseStoreSync;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->context:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$700(Lcom/google/android/videos/store/PurchaseStoreSync;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/pinning/TransferService;->createIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_2
    throw v0
.end method
