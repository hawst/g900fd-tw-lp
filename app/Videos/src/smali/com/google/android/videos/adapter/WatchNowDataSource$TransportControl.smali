.class Lcom/google/android/videos/adapter/WatchNowDataSource$TransportControl;
.super Lcom/google/android/videos/remote/TransportControl;
.source "WatchNowDataSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/adapter/WatchNowDataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TransportControl"
.end annotation


# instance fields
.field private lastState:I

.field private lastTime:I

.field private lastVideoId:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/videos/adapter/WatchNowDataSource;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/adapter/WatchNowDataSource;Lcom/google/android/videos/remote/RemoteTracker;)V
    .locals 0
    .param p2, "remoteTracker"    # Lcom/google/android/videos/remote/RemoteTracker;

    .prologue
    .line 265
    iput-object p1, p0, Lcom/google/android/videos/adapter/WatchNowDataSource$TransportControl;->this$0:Lcom/google/android/videos/adapter/WatchNowDataSource;

    .line 266
    invoke-direct {p0, p2}, Lcom/google/android/videos/remote/TransportControl;-><init>(Lcom/google/android/videos/remote/RemoteTracker;)V

    .line 267
    return-void
.end method


# virtual methods
.method public onPlayerStateChanged()V
    .locals 6

    .prologue
    .line 271
    const/4 v2, 0x0

    .line 272
    .local v2, "newVideoId":Ljava/lang/String;
    const/4 v0, 0x0

    .line 273
    .local v0, "newState":I
    const/4 v1, 0x0

    .line 275
    .local v1, "newTime":I
    iget-object v4, p0, Lcom/google/android/videos/adapter/WatchNowDataSource$TransportControl;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v4}, Lcom/google/android/videos/remote/RemoteTracker;->getPlayerState()Lcom/google/android/videos/remote/PlayerState;

    move-result-object v3

    .line 276
    .local v3, "playerState":Lcom/google/android/videos/remote/PlayerState;
    if-eqz v3, :cond_0

    .line 277
    iget-object v2, v3, Lcom/google/android/videos/remote/PlayerState;->videoId:Ljava/lang/String;

    .line 278
    iget v0, v3, Lcom/google/android/videos/remote/PlayerState;->state:I

    .line 279
    iget v1, v3, Lcom/google/android/videos/remote/PlayerState;->time:I

    .line 282
    :cond_0
    iget-object v4, p0, Lcom/google/android/videos/adapter/WatchNowDataSource$TransportControl;->lastVideoId:Ljava/lang/String;

    invoke-static {v4, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/google/android/videos/adapter/WatchNowDataSource$TransportControl;->lastState:I

    if-ne v4, v0, :cond_1

    iget v4, p0, Lcom/google/android/videos/adapter/WatchNowDataSource$TransportControl;->lastTime:I

    sub-int v4, v1, v4

    const/16 v5, 0x7530

    if-le v4, v5, :cond_2

    .line 285
    :cond_1
    iput-object v2, p0, Lcom/google/android/videos/adapter/WatchNowDataSource$TransportControl;->lastVideoId:Ljava/lang/String;

    .line 286
    iput v0, p0, Lcom/google/android/videos/adapter/WatchNowDataSource$TransportControl;->lastState:I

    .line 287
    iput v1, p0, Lcom/google/android/videos/adapter/WatchNowDataSource$TransportControl;->lastTime:I

    .line 289
    iget-object v4, p0, Lcom/google/android/videos/adapter/WatchNowDataSource$TransportControl;->this$0:Lcom/google/android/videos/adapter/WatchNowDataSource;

    invoke-virtual {v4}, Lcom/google/android/videos/adapter/WatchNowDataSource;->notifyChanged()V

    .line 291
    :cond_2
    return-void
.end method

.method public onVideoInfoChanged()V
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/videos/adapter/WatchNowDataSource$TransportControl;->this$0:Lcom/google/android/videos/adapter/WatchNowDataSource;

    invoke-virtual {v0}, Lcom/google/android/videos/adapter/WatchNowDataSource;->notifyChanged()V

    .line 296
    return-void
.end method
