.class Lcom/google/android/videos/pano/datasource/MyWishlistDataSource$2;
.super Ljava/lang/Object;
.source "MyWishlistDataSource.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/api/AssetsRequest;",
        "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;


# direct methods
.method constructor <init>(Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource$2;->this$0:Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/api/AssetsRequest;Ljava/lang/Exception;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/api/AssetsRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource$2;->this$0:Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->updateArray([Ljava/lang/Object;)V

    .line 98
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 77
    check-cast p1, Lcom/google/android/videos/api/AssetsRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource$2;->onError(Lcom/google/android/videos/api/AssetsRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;)V
    .locals 7
    .param p1, "request"    # Lcom/google/android/videos/api/AssetsRequest;
    .param p2, "response"    # Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    .prologue
    const/4 v6, 0x0

    .line 81
    iget-object v0, p2, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 82
    .local v0, "assets":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    array-length v4, v0

    new-array v2, v4, [Lcom/google/android/videos/pano/model/VideoItem;

    .line 83
    .local v2, "items":[Lcom/google/android/videos/pano/model/VideoItem;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, v0

    if-ge v1, v4, :cond_1

    .line 85
    aget-object v4, v0, v1

    iget-object v4, v4, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget v4, v4, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    const/4 v5, 0x6

    if-ne v4, v5, :cond_0

    .line 86
    iget-object v4, p0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource$2;->this$0:Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;

    iget-object v4, v4, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->context:Landroid/content/Context;

    aget-object v5, v0, v1

    invoke-static {v4, v5, v6}, Lcom/google/android/videos/pano/model/MovieItem;->createMovieItem(Landroid/content/Context;Lcom/google/wireless/android/video/magma/proto/AssetResource;Z)Lcom/google/android/videos/pano/model/VideoItem;

    move-result-object v3

    .line 90
    .local v3, "result":Lcom/google/android/videos/pano/model/VideoItem;
    :goto_1
    aput-object v3, v2, v1

    .line 83
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 88
    .end local v3    # "result":Lcom/google/android/videos/pano/model/VideoItem;
    :cond_0
    iget-object v4, p0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource$2;->this$0:Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;

    iget-object v4, v4, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->context:Landroid/content/Context;

    aget-object v5, v0, v1

    invoke-static {v4, v5, v6}, Lcom/google/android/videos/pano/model/ShowItem;->createShowItem(Landroid/content/Context;Lcom/google/wireless/android/video/magma/proto/AssetResource;Z)Lcom/google/android/videos/pano/model/ShowItem;

    move-result-object v3

    .restart local v3    # "result":Lcom/google/android/videos/pano/model/VideoItem;
    goto :goto_1

    .line 92
    .end local v3    # "result":Lcom/google/android/videos/pano/model/VideoItem;
    :cond_1
    iget-object v4, p0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource$2;->this$0:Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;

    invoke-virtual {v4, v2}, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->updateArray([Ljava/lang/Object;)V

    .line 93
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 77
    check-cast p1, Lcom/google/android/videos/api/AssetsRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource$2;->onResponse(Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;)V

    return-void
.end method
