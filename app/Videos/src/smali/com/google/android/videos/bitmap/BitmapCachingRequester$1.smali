.class Lcom/google/android/videos/bitmap/BitmapCachingRequester$1;
.super Ljava/lang/Object;
.source "BitmapCachingRequester.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/bitmap/BitmapCachingRequester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<TR;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/bitmap/BitmapCachingRequester;

.field final synthetic val$cacheKey:Ljava/lang/String;

.field final synthetic val$callback:Lcom/google/android/videos/async/Callback;

.field final synthetic val$request:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/google/android/videos/bitmap/BitmapCachingRequester;Ljava/lang/String;Lcom/google/android/videos/async/Callback;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 35
    .local p0, "this":Lcom/google/android/videos/bitmap/BitmapCachingRequester$1;, "Lcom/google/android/videos/bitmap/BitmapCachingRequester.1;"
    iput-object p1, p0, Lcom/google/android/videos/bitmap/BitmapCachingRequester$1;->this$0:Lcom/google/android/videos/bitmap/BitmapCachingRequester;

    iput-object p2, p0, Lcom/google/android/videos/bitmap/BitmapCachingRequester$1;->val$cacheKey:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/videos/bitmap/BitmapCachingRequester$1;->val$callback:Lcom/google/android/videos/async/Callback;

    iput-object p4, p0, Lcom/google/android/videos/bitmap/BitmapCachingRequester$1;->val$request:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2
    .param p2, "exception"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    .prologue
    .line 43
    .local p0, "this":Lcom/google/android/videos/bitmap/BitmapCachingRequester$1;, "Lcom/google/android/videos/bitmap/BitmapCachingRequester.1;"
    .local p1, "innerRequest":Ljava/lang/Object;, "TR;"
    iget-object v0, p0, Lcom/google/android/videos/bitmap/BitmapCachingRequester$1;->val$callback:Lcom/google/android/videos/async/Callback;

    iget-object v1, p0, Lcom/google/android/videos/bitmap/BitmapCachingRequester$1;->val$request:Ljava/lang/Object;

    invoke-interface {v0, v1, p2}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 44
    return-void
.end method

.method public onResponse(Ljava/lang/Object;Landroid/graphics/Bitmap;)V
    .locals 2
    .param p2, "response"    # Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Landroid/graphics/Bitmap;",
            ")V"
        }
    .end annotation

    .prologue
    .line 38
    .local p0, "this":Lcom/google/android/videos/bitmap/BitmapCachingRequester$1;, "Lcom/google/android/videos/bitmap/BitmapCachingRequester.1;"
    .local p1, "innerRequest":Ljava/lang/Object;, "TR;"
    iget-object v0, p0, Lcom/google/android/videos/bitmap/BitmapCachingRequester$1;->this$0:Lcom/google/android/videos/bitmap/BitmapCachingRequester;

    # getter for: Lcom/google/android/videos/bitmap/BitmapCachingRequester;->cache:Lcom/google/android/videos/bitmap/BitmapLruCache;
    invoke-static {v0}, Lcom/google/android/videos/bitmap/BitmapCachingRequester;->access$000(Lcom/google/android/videos/bitmap/BitmapCachingRequester;)Lcom/google/android/videos/bitmap/BitmapLruCache;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/bitmap/BitmapCachingRequester$1;->val$cacheKey:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/videos/bitmap/BitmapLruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    iget-object v0, p0, Lcom/google/android/videos/bitmap/BitmapCachingRequester$1;->val$callback:Lcom/google/android/videos/async/Callback;

    iget-object v1, p0, Lcom/google/android/videos/bitmap/BitmapCachingRequester$1;->val$request:Ljava/lang/Object;

    invoke-interface {v0, v1, p2}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 40
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 35
    .local p0, "this":Lcom/google/android/videos/bitmap/BitmapCachingRequester$1;, "Lcom/google/android/videos/bitmap/BitmapCachingRequester.1;"
    check-cast p2, Landroid/graphics/Bitmap;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/bitmap/BitmapCachingRequester$1;->onResponse(Ljava/lang/Object;Landroid/graphics/Bitmap;)V

    return-void
.end method
