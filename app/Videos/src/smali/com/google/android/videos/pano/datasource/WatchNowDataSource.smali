.class public final Lcom/google/android/videos/pano/datasource/WatchNowDataSource;
.super Lcom/google/android/videos/pano/datasource/BaseDataSource;
.source "WatchNowDataSource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;,
        Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;,
        Lcom/google/android/videos/pano/datasource/WatchNowDataSource$DatabaseUpdateNotifier;,
        Lcom/google/android/videos/pano/datasource/WatchNowDataSource$ItemsProcessor;,
        Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;,
        Lcom/google/android/videos/pano/datasource/WatchNowDataSource$MoviesProcessor;,
        Lcom/google/android/videos/pano/datasource/WatchNowDataSource$StoreStatusListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/pano/datasource/BaseDataSource",
        "<",
        "Lcom/google/android/videos/pano/model/VideoItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final cancelableCallbacks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/videos/async/CancelableCallback",
            "<",
            "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
            "Landroid/database/Cursor;",
            ">;>;"
        }
    .end annotation
.end field

.field private final config:Lcom/google/android/videos/Config;

.field private currentAccount:Ljava/lang/String;

.field private final database:Lcom/google/android/videos/store/Database;

.field private final databaseUpdateListener:Lcom/google/android/videos/pano/datasource/WatchNowDataSource$DatabaseUpdateNotifier;

.field private final episodesProcessor:Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;

.field private forcedUpdate:Z

.field private final localeObservable:Lcom/google/android/repolib/observers/Observable;

.field private final localeUpdatable:Lcom/google/android/repolib/observers/Updatable;

.field private final moviesProcessor:Lcom/google/android/videos/pano/datasource/WatchNowDataSource$MoviesProcessor;

.field private final networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

.field private pendingUpdate:Z

.field private final promoProcessor:Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;

.field private final purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

.field private final purchaseStoreMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/Config;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/ConfigurationStore;Landroid/content/SharedPreferences;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/repolib/observers/Observable;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "signInManager"    # Lcom/google/android/videos/accounts/SignInManager;
    .param p3, "purchaseStore"    # Lcom/google/android/videos/store/PurchaseStore;
    .param p4, "database"    # Lcom/google/android/videos/store/Database;
    .param p5, "config"    # Lcom/google/android/videos/Config;
    .param p7, "configStore"    # Lcom/google/android/videos/store/ConfigurationStore;
    .param p8, "preferences"    # Landroid/content/SharedPreferences;
    .param p9, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;
    .param p10, "localeObservable"    # Lcom/google/android/repolib/observers/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/videos/accounts/SignInManager;",
            "Lcom/google/android/videos/store/PurchaseStore;",
            "Lcom/google/android/videos/store/Database;",
            "Lcom/google/android/videos/Config;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/PromotionsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;",
            ">;",
            "Lcom/google/android/videos/store/ConfigurationStore;",
            "Landroid/content/SharedPreferences;",
            "Lcom/google/android/videos/utils/NetworkStatus;",
            "Lcom/google/android/repolib/observers/Observable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 76
    .local p6, "promotionRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/PromotionsRequest;Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;>;"
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/pano/datasource/BaseDataSource;-><init>(Landroid/content/Context;Lcom/google/android/videos/accounts/SignInManager;)V

    .line 59
    new-instance v1, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$MoviesProcessor;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$MoviesProcessor;-><init>(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;Lcom/google/android/videos/pano/datasource/WatchNowDataSource$1;)V

    iput-object v1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->moviesProcessor:Lcom/google/android/videos/pano/datasource/WatchNowDataSource$MoviesProcessor;

    .line 60
    new-instance v1, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;-><init>(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;Lcom/google/android/videos/pano/datasource/WatchNowDataSource$1;)V

    iput-object v1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->episodesProcessor:Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;

    .line 78
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/store/PurchaseStore;

    iput-object v1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    .line 79
    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/Config;

    iput-object v1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->config:Lcom/google/android/videos/Config;

    .line 80
    iput-object p4, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->database:Lcom/google/android/videos/store/Database;

    .line 81
    new-instance v1, Lcom/google/android/videos/store/StoreStatusMonitor;

    const/4 v2, 0x0

    invoke-direct {v1, p1, p4, p3, v2}, Lcom/google/android/videos/store/StoreStatusMonitor;-><init>(Landroid/content/Context;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/WishlistStore;)V

    iput-object v1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->purchaseStoreMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    .line 82
    new-instance v1, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;

    iget-object v6, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->purchaseStoreMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    iget-object v7, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->handler:Landroid/os/Handler;

    const/4 v8, 0x0

    move-object v2, p0

    move-object v3, p6

    move-object/from16 v4, p7

    move-object/from16 v5, p8

    invoke-direct/range {v1 .. v8}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;-><init>(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/ConfigurationStore;Landroid/content/SharedPreferences;Lcom/google/android/videos/store/StoreStatusMonitor;Landroid/os/Handler;Lcom/google/android/videos/pano/datasource/WatchNowDataSource$1;)V

    iput-object v1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->promoProcessor:Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;

    .line 84
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->cancelableCallbacks:Ljava/util/ArrayList;

    .line 85
    new-instance v1, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$DatabaseUpdateNotifier;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$DatabaseUpdateNotifier;-><init>(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;Lcom/google/android/videos/pano/datasource/WatchNowDataSource$1;)V

    iput-object v1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->databaseUpdateListener:Lcom/google/android/videos/pano/datasource/WatchNowDataSource$DatabaseUpdateNotifier;

    .line 86
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 87
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->localeObservable:Lcom/google/android/repolib/observers/Observable;

    .line 88
    new-instance v1, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$1;

    invoke-direct {v1, p0}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$1;-><init>(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;)V

    iput-object v1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->localeUpdatable:Lcom/google/android/repolib/observers/Updatable;

    .line 94
    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;)Lcom/google/android/videos/store/PurchaseStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/datasource/WatchNowDataSource;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/pano/datasource/WatchNowDataSource;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->mergeAndUpdate()V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/pano/datasource/WatchNowDataSource;
    .param p1, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->onError(Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;)Lcom/google/android/videos/Config;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/datasource/WatchNowDataSource;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->config:Lcom/google/android/videos/Config;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/Callback;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/datasource/WatchNowDataSource;
    .param p1, "x1"    # Lcom/google/android/videos/async/Callback;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->decorateCallback(Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/Callback;

    move-result-object v0

    return-object v0
.end method

.method private cancelAllCallbacks()V
    .locals 3

    .prologue
    .line 129
    iget-object v2, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->cancelableCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/CancelableCallback;

    .line 130
    .local v0, "callback":Lcom/google/android/videos/async/CancelableCallback;, "Lcom/google/android/videos/async/CancelableCallback<**>;"
    invoke-virtual {v0}, Lcom/google/android/videos/async/CancelableCallback;->cancel()V

    goto :goto_0

    .line 132
    .end local v0    # "callback":Lcom/google/android/videos/async/CancelableCallback;, "Lcom/google/android/videos/async/CancelableCallback<**>;"
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->cancelableCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 133
    return-void
.end method

.method private decorateCallback(Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/Callback;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
            "Landroid/database/Cursor;",
            ">;)",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122
    .local p1, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;>;"
    invoke-static {p1}, Lcom/google/android/videos/async/CancelableCallback;->create(Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/CancelableCallback;

    move-result-object v0

    .line 124
    .local v0, "cancelableCallback":Lcom/google/android/videos/async/CancelableCallback;, "Lcom/google/android/videos/async/CancelableCallback<Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;>;"
    iget-object v1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->cancelableCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    iget-object v1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->handler:Landroid/os/Handler;

    invoke-static {v1, v0}, Lcom/google/android/videos/async/HandlerCallback;->create(Landroid/os/Handler;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/HandlerCallback;

    move-result-object v1

    return-object v1
.end method

.method private mergeAndUpdate()V
    .locals 18

    .prologue
    .line 136
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->moviesProcessor:Lcom/google/android/videos/pano/datasource/WatchNowDataSource$MoviesProcessor;

    invoke-virtual {v13}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$MoviesProcessor;->isReady()Z

    move-result v13

    if-eqz v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->episodesProcessor:Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;

    invoke-virtual {v13}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;->isReady()Z

    move-result v13

    if-eqz v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->promoProcessor:Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;

    invoke-virtual {v13}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;->isReady()Z

    move-result v13

    if-nez v13, :cond_1

    .line 188
    :cond_0
    :goto_0
    return-void

    .line 140
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->promoProcessor:Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;

    # getter for: Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;->itemsList:Ljava/util/List;
    invoke-static {v13}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;->access$500(Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;)Ljava/util/List;

    move-result-object v11

    .line 141
    .local v11, "promoList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/pano/model/VideoItem;>;"
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->moviesProcessor:Lcom/google/android/videos/pano/datasource/WatchNowDataSource$MoviesProcessor;

    iget-object v10, v13, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$MoviesProcessor;->itemsList:Ljava/util/ArrayList;

    .line 142
    .local v10, "moviesList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;>;"
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->episodesProcessor:Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;

    iget-object v4, v13, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;->itemsList:Ljava/util/ArrayList;

    .line 144
    .local v4, "episodesList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;>;"
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v13

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v14

    add-int/2addr v13, v14

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v14

    add-int/2addr v13, v14

    new-array v5, v13, [Lcom/google/android/videos/pano/model/VideoItem;

    .line 145
    .local v5, "merged":[Lcom/google/android/videos/pano/model/VideoItem;
    const/4 v6, 0x0

    .line 147
    .local v6, "mergedCount":I
    :goto_1
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v13

    if-ge v6, v13, :cond_2

    .line 148
    invoke-interface {v11, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/videos/pano/model/VideoItem;

    aput-object v13, v5, v6

    .line 149
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 152
    :cond_2
    const/4 v9, 0x0

    .line 153
    .local v9, "movieIndex":I
    const/4 v3, 0x0

    .line 154
    .local v3, "episodeIndex":I
    :cond_3
    :goto_2
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v13

    if-ge v9, v13, :cond_5

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v13

    if-ge v3, v13, :cond_5

    .line 155
    invoke-interface {v10, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;

    .line 156
    .local v8, "movie":Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;

    .line 157
    .local v2, "episode":Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;
    iget-wide v14, v8, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;->lastActivityTimestamp:J

    iget-wide v0, v2, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;->lastActivityTimestamp:J

    move-wide/from16 v16, v0

    cmp-long v13, v14, v16

    if-lez v13, :cond_4

    .line 158
    add-int/lit8 v7, v6, 0x1

    .end local v6    # "mergedCount":I
    .local v7, "mergedCount":I
    iget-object v13, v8, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;->item:Lcom/google/android/videos/pano/model/VideoItem;

    aput-object v13, v5, v6

    .line 159
    add-int/lit8 v9, v9, 0x1

    move v6, v7

    .end local v7    # "mergedCount":I
    .restart local v6    # "mergedCount":I
    goto :goto_2

    .line 162
    :cond_4
    iget-object v13, v2, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;->item:Lcom/google/android/videos/pano/model/VideoItem;

    iget-object v12, v13, Lcom/google/android/videos/pano/model/VideoItem;->id:Ljava/lang/String;

    .line 163
    .local v12, "showId":Ljava/lang/String;
    :goto_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v13

    if-ge v3, v13, :cond_3

    .line 164
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "episode":Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;
    check-cast v2, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;

    .line 165
    .restart local v2    # "episode":Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;
    iget-object v13, v2, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;->item:Lcom/google/android/videos/pano/model/VideoItem;

    iget-object v13, v13, Lcom/google/android/videos/pano/model/VideoItem;->id:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 168
    add-int/lit8 v7, v6, 0x1

    .end local v6    # "mergedCount":I
    .restart local v7    # "mergedCount":I
    iget-object v13, v2, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;->item:Lcom/google/android/videos/pano/model/VideoItem;

    aput-object v13, v5, v6

    .line 169
    add-int/lit8 v3, v3, 0x1

    move v6, v7

    .end local v7    # "mergedCount":I
    .restart local v6    # "mergedCount":I
    goto :goto_3

    .line 174
    .end local v2    # "episode":Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;
    .end local v8    # "movie":Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;
    .end local v12    # "showId":Ljava/lang/String;
    :cond_5
    :goto_4
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v13

    if-ge v9, v13, :cond_6

    .line 175
    add-int/lit8 v7, v6, 0x1

    .end local v6    # "mergedCount":I
    .restart local v7    # "mergedCount":I
    invoke-interface {v10, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;

    iget-object v13, v13, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;->item:Lcom/google/android/videos/pano/model/VideoItem;

    aput-object v13, v5, v6

    .line 176
    add-int/lit8 v9, v9, 0x1

    move v6, v7

    .end local v7    # "mergedCount":I
    .restart local v6    # "mergedCount":I
    goto :goto_4

    .line 179
    :cond_6
    :goto_5
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v13

    if-ge v3, v13, :cond_7

    .line 180
    add-int/lit8 v7, v6, 0x1

    .end local v6    # "mergedCount":I
    .restart local v7    # "mergedCount":I
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;

    iget-object v13, v13, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$TimestampedItem;->item:Lcom/google/android/videos/pano/model/VideoItem;

    aput-object v13, v5, v6

    .line 181
    add-int/lit8 v3, v3, 0x1

    move v6, v7

    .end local v7    # "mergedCount":I
    .restart local v6    # "mergedCount":I
    goto :goto_5

    .line 184
    :cond_7
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->moviesProcessor:Lcom/google/android/videos/pano/datasource/WatchNowDataSource$MoviesProcessor;

    invoke-virtual {v13}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$MoviesProcessor;->clear()V

    .line 185
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->episodesProcessor:Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;

    invoke-virtual {v13}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;->clear()V

    .line 186
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->updateArray([Ljava/lang/Object;)V

    .line 187
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->forcedUpdate:Z

    goto/16 :goto_0
.end method

.method private onError(Ljava/lang/Exception;)V
    .locals 2
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 191
    const-string v0, "Failed to fetch purchases"

    invoke-virtual {p1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 192
    invoke-direct {p0}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->cancelAllCallbacks()V

    .line 193
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->moviesProcessor:Lcom/google/android/videos/pano/datasource/WatchNowDataSource$MoviesProcessor;

    invoke-virtual {v0}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$MoviesProcessor;->clear()V

    .line 194
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->episodesProcessor:Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;

    invoke-virtual {v0}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;->clear()V

    .line 195
    iget-boolean v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->forcedUpdate:Z

    if-eqz v0, :cond_0

    .line 196
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->updateArray([Ljava/lang/Object;)V

    .line 197
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->forcedUpdate:Z

    .line 199
    :cond_0
    return-void
.end method


# virtual methods
.method public firstUpdatableAdded()V
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->database:Lcom/google/android/videos/store/Database;

    iget-object v1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->databaseUpdateListener:Lcom/google/android/videos/pano/datasource/WatchNowDataSource$DatabaseUpdateNotifier;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/store/Database;->addListener(Lcom/google/android/videos/store/Database$Listener;)V

    .line 211
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->localeObservable:Lcom/google/android/repolib/observers/Observable;

    iget-object v1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->localeUpdatable:Lcom/google/android/repolib/observers/Updatable;

    invoke-interface {v0, v1}, Lcom/google/android/repolib/observers/Observable;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 212
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0, p0}, Lcom/google/android/videos/utils/NetworkStatus;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 213
    return-void
.end method

.method public lastUpdatableRemoved()V
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->database:Lcom/google/android/videos/store/Database;

    iget-object v1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->databaseUpdateListener:Lcom/google/android/videos/pano/datasource/WatchNowDataSource$DatabaseUpdateNotifier;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/store/Database;->removeListener(Lcom/google/android/videos/store/Database$Listener;)Z

    .line 218
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0, p0}, Lcom/google/android/videos/utils/NetworkStatus;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 219
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->localeObservable:Lcom/google/android/repolib/observers/Observable;

    iget-object v1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->localeUpdatable:Lcom/google/android/repolib/observers/Updatable;

    invoke-interface {v0, v1}, Lcom/google/android/repolib/observers/Observable;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 220
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->purchaseStoreMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    invoke-virtual {v0}, Lcom/google/android/videos/store/StoreStatusMonitor;->reset()V

    .line 221
    return-void
.end method

.method public update()V
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->pendingUpdate:Z

    if-eqz v0, :cond_0

    .line 204
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->scheduleUpdate(Z)V

    .line 206
    :cond_0
    return-void
.end method

.method protected updateInternal(Z)V
    .locals 5
    .param p1, "forced"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 98
    invoke-direct {p0}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->cancelAllCallbacks()V

    .line 99
    iget-boolean v3, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->forcedUpdate:Z

    or-int/2addr v3, p1

    iput-boolean v3, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->forcedUpdate:Z

    .line 100
    invoke-virtual {p0}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->getAccount()Ljava/lang/String;

    move-result-object v0

    .line 101
    .local v0, "account":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 102
    iget-boolean v3, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->forcedUpdate:Z

    if-eqz v3, :cond_0

    .line 103
    invoke-virtual {p0, v4}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->updateArray([Ljava/lang/Object;)V

    .line 104
    iput-boolean v2, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->forcedUpdate:Z

    .line 106
    :cond_0
    iput-boolean v1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->pendingUpdate:Z

    .line 118
    :goto_0
    return-void

    .line 109
    :cond_1
    iget-object v3, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v3}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v3

    if-nez v3, :cond_3

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->pendingUpdate:Z

    .line 110
    iget-object v1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->currentAccount:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 111
    iput-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->currentAccount:Ljava/lang/String;

    .line 112
    iget-object v1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->purchaseStoreMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/store/StoreStatusMonitor;->init(Ljava/lang/String;)V

    .line 113
    iget-object v1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->purchaseStoreMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    new-instance v2, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$StoreStatusListener;

    invoke-direct {v2, p0, v4}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$StoreStatusListener;-><init>(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;Lcom/google/android/videos/pano/datasource/WatchNowDataSource$1;)V

    invoke-virtual {v1, v2}, Lcom/google/android/videos/store/StoreStatusMonitor;->addListener(Lcom/google/android/videos/store/StoreStatusMonitor$Listener;)V

    .line 115
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->moviesProcessor:Lcom/google/android/videos/pano/datasource/WatchNowDataSource$MoviesProcessor;

    invoke-virtual {v1}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$MoviesProcessor;->getPurchases()V

    .line 116
    iget-object v1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->episodesProcessor:Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;

    invoke-virtual {v1}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$EpisodesProcessor;->getPurchases()V

    .line 117
    iget-object v1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->promoProcessor:Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$PromoProcessor;->updatePromo(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move v1, v2

    .line 109
    goto :goto_1
.end method
