.class final Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;
.super Ljava/lang/Object;
.source "LegacyPlayerInternal.java"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Lcom/google/android/videos/async/Callback;
.implements Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Handler$Callback;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/player/VideoInfo;",
        "Lcom/google/android/videos/streams/LegacyStreamSelection;",
        ">;",
        "Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;"
    }
.end annotation


# static fields
.field private static final FATAL_ERROR_CODES:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private volatile bufferedPercentage:I

.field private buffering:Z

.field private final context:Landroid/content/Context;

.field private final drmManager:Lcom/google/android/videos/drm/DrmManager;

.field private volatile durationMs:I

.field private final enableVirtualizer:Z

.field private final eventHandler:Landroid/os/Handler;

.field private hq:Z

.field private volatile internalState:I

.field private final isGoogleTv:Z

.field private isWidevineOnGoogleTv:Z

.field private lastRetrySystemTimeMs:J

.field private maskState:Z

.field private mediaPlayer:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

.field private final networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

.field private pendingSeekEnsureAccurateTime:Z

.field private pendingSeekPosition:I

.field private playWhenReady:Z

.field private playbackPosition:I

.field private final playbackPositionLock:Ljava/lang/Object;

.field private playbackPositionMaybeAdvancing:Z

.field private playbackPositionStabilityIncrement:I

.field private playbackPositionState:I

.field private playbackPositionSystemTimestamp:J

.field private final playerHandler:Landroid/os/Handler;

.field private final playerThread:Landroid/os/HandlerThread;

.field private final preferences:Landroid/content/SharedPreferences;

.field private released:Z

.field private retries:I

.field private selectedStreamIndex:I

.field private selectionCancelableCallback:Lcom/google/android/videos/async/CancelableCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/CancelableCallback",
            "<",
            "Lcom/google/android/videos/player/VideoInfo;",
            "Lcom/google/android/videos/streams/LegacyStreamSelection;",
            ">;"
        }
    .end annotation
.end field

.field private streamSelection:Lcom/google/android/videos/streams/LegacyStreamSelection;

.field private final streamsSelector:Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;

.field private surfaceHolder:Landroid/view/SurfaceHolder;

.field private videoInfo:Lcom/google/android/videos/player/VideoInfo;

.field private virtualizer:Landroid/media/audiofx/Virtualizer;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 123
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 124
    .local v0, "fatalErrorCodes":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    const/16 v1, -0x3e81

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 125
    const/16 v1, -0x7d2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 126
    const/16 v1, -0x7d1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 127
    sget v1, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_AUTHENTICATION_FAILURE:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 128
    sget v1, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_NO_ACTIVE_PURCHASE_AGREEMENT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 129
    sget v1, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_CONCURRENT_PLAYBACK:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 130
    sget v1, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_UNUSUAL_ACTIVITY:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 131
    sget v1, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_STREAMING_UNAVAILABLE:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 132
    sget v1, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_CANNOT_ACTIVATE_RENTAL:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 133
    sget v1, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_CONCURRENT_PLAYBACKS_BY_ACCOUNT:I

    const v2, 0x7fffffff

    if-eq v1, v2, :cond_0

    .line 134
    sget v1, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_CONCURRENT_PLAYBACKS_BY_ACCOUNT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 136
    :cond_0
    sget v1, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_TERMINATE_REQUESTED:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 137
    const/16 v1, 0x1f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 138
    const/16 v1, 0x20

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 139
    const/16 v1, 0x21

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 140
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    sput-object v1, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->FATAL_ERROR_CODES:Ljava/util/Set;

    .line 141
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Landroid/view/Display;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/videos/drm/DrmManager;Lcom/google/android/videos/streams/LegacyStreamsSelector;ZZZLandroid/os/Handler;Lcom/google/android/videos/player/PlaybackResumeState;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "preferences"    # Landroid/content/SharedPreferences;
    .param p3, "display"    # Landroid/view/Display;
    .param p4, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;
    .param p5, "drmManager"    # Lcom/google/android/videos/drm/DrmManager;
    .param p6, "streamsSelector"    # Lcom/google/android/videos/streams/LegacyStreamsSelector;
    .param p7, "surroundSound"    # Z
    .param p8, "enableVirtualizer"    # Z
    .param p9, "displaySupportsProtectedBuffers"    # Z
    .param p10, "eventHandler"    # Landroid/os/Handler;
    .param p11, "playbackResumeState"    # Lcom/google/android/videos/player/PlaybackResumeState;

    .prologue
    .line 194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 195
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->context:Landroid/content/Context;

    .line 196
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/SharedPreferences;

    iput-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->preferences:Landroid/content/SharedPreferences;

    .line 197
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/utils/NetworkStatus;

    iput-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 198
    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/drm/DrmManager;

    iput-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->drmManager:Lcom/google/android/videos/drm/DrmManager;

    .line 199
    invoke-static/range {p10 .. p10}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Handler;

    iput-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->eventHandler:Landroid/os/Handler;

    .line 200
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/utils/Util;->isGoogleTv(Landroid/content/pm/PackageManager;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->isGoogleTv:Z

    .line 201
    iput-boolean p8, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->enableVirtualizer:Z

    .line 202
    const/4 v1, 0x1

    iput v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->internalState:I

    .line 203
    new-instance v1, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;

    move-object v2, p3

    move-object v3, p6

    move-object v4, p5

    move v5, p7

    move/from16 v6, p9

    invoke-direct/range {v1 .. v6}, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;-><init>(Landroid/view/Display;Lcom/google/android/videos/streams/LegacyStreamsSelector;Lcom/google/android/videos/drm/DrmManager;ZZ)V

    iput-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->streamsSelector:Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;

    .line 205
    const/4 v1, -0x1

    move-object/from16 v0, p11

    invoke-virtual {v0, v1}, Lcom/google/android/videos/player/PlaybackResumeState;->getSelectedAudioTrackIndex(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->selectedStreamIndex:I

    .line 206
    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->pendingSeekPosition:I

    .line 207
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playbackPositionLock:Ljava/lang/Object;

    .line 208
    new-instance v1, Landroid/os/HandlerThread;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":Handler"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playerThread:Landroid/os/HandlerThread;

    .line 209
    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 210
    new-instance v1, Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playerThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playerHandler:Landroid/os/Handler;

    .line 211
    return-void
.end method

.method private enabledVirtualizerInternalV18()V
    .locals 2

    .prologue
    .line 580
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->virtualizer:Landroid/media/audiofx/Virtualizer;

    if-eqz v0, :cond_0

    .line 581
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->virtualizer:Landroid/media/audiofx/Virtualizer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/audiofx/Virtualizer;->setEnabled(Z)I

    .line 583
    :cond_0
    return-void
.end method

.method private initMediaPlayer(Lcom/google/android/videos/streams/MediaStream;)Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
    .locals 6
    .param p1, "stream"    # Lcom/google/android/videos/streams/MediaStream;

    .prologue
    const/4 v5, 0x1

    .line 425
    new-instance v1, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;

    invoke-direct {v1}, Lcom/google/android/videos/player/legacy/FrameworkMediaPlayer;-><init>()V

    .line 426
    .local v1, "mediaPlayer":Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
    iget-boolean v3, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->isGoogleTv:Z

    if-eqz v3, :cond_0

    .line 427
    new-instance v2, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;

    invoke-direct {v2, v1}, Lcom/google/android/videos/player/legacy/TvWidevineMediaPlayer;-><init>(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;)V

    .end local v1    # "mediaPlayer":Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
    .local v2, "mediaPlayer":Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
    move-object v1, v2

    .line 429
    .end local v2    # "mediaPlayer":Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
    .restart local v1    # "mediaPlayer":Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
    :cond_0
    iget-object v3, p1, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    iget v0, v3, Lcom/google/android/videos/streams/ItagInfo;->drmType:I

    .line 430
    .local v0, "drmType":I
    if-ne v0, v5, :cond_3

    .line 431
    new-instance v2, Lcom/google/android/videos/player/legacy/DrmMediaPlayer;

    iget-object v3, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->context:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->drmManager:Lcom/google/android/videos/drm/DrmManager;

    invoke-direct {v2, v1, v3, v4, v5}, Lcom/google/android/videos/player/legacy/DrmMediaPlayer;-><init>(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;Landroid/content/Context;Lcom/google/android/videos/drm/DrmManager;Z)V

    .end local v1    # "mediaPlayer":Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
    .restart local v2    # "mediaPlayer":Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
    move-object v1, v2

    .line 435
    .end local v2    # "mediaPlayer":Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
    .restart local v1    # "mediaPlayer":Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
    :cond_1
    :goto_0
    iget-boolean v3, p1, Lcom/google/android/videos/streams/MediaStream;->isOffline:Z

    if-eqz v3, :cond_2

    .line 436
    new-instance v2, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;

    iget-object v3, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    iget-object v4, p1, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-wide v4, v4, Lcom/google/android/videos/proto/StreamInfo;->sizeInBytes:J

    invoke-direct {v2, v1, v3, v4, v5}, Lcom/google/android/videos/player/legacy/OfflineMediaPlayer;-><init>(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;Lcom/google/android/videos/utils/NetworkStatus;J)V

    .end local v1    # "mediaPlayer":Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
    .restart local v2    # "mediaPlayer":Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
    move-object v1, v2

    .line 439
    .end local v2    # "mediaPlayer":Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
    .restart local v1    # "mediaPlayer":Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
    :cond_2
    return-object v1

    .line 432
    :cond_3
    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    .line 433
    new-instance v2, Lcom/google/android/videos/player/legacy/DrmMediaPlayer;

    iget-object v3, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->context:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->drmManager:Lcom/google/android/videos/drm/DrmManager;

    const/4 v5, 0x0

    invoke-direct {v2, v1, v3, v4, v5}, Lcom/google/android/videos/player/legacy/DrmMediaPlayer;-><init>(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;Landroid/content/Context;Lcom/google/android/videos/drm/DrmManager;Z)V

    .end local v1    # "mediaPlayer":Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
    .restart local v2    # "mediaPlayer":Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
    move-object v1, v2

    .end local v2    # "mediaPlayer":Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
    .restart local v1    # "mediaPlayer":Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
    goto :goto_0
.end method

.method private notifyMediaPlayerError(ZLcom/google/android/videos/player/legacy/MediaPlayerException;)V
    .locals 4
    .param p1, "fatal"    # Z
    .param p2, "exception"    # Lcom/google/android/videos/player/legacy/MediaPlayerException;

    .prologue
    const/4 v1, 0x0

    .line 763
    iget-object v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->eventHandler:Landroid/os/Handler;

    const/4 v3, 0x2

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v3, v0, v1, p2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 764
    return-void

    :cond_0
    move v0, v1

    .line 763
    goto :goto_0
.end method

.method private prepareInternal(Lcom/google/android/videos/player/VideoInfo;)V
    .locals 3
    .param p1, "videoInfo"    # Lcom/google/android/videos/player/VideoInfo;

    .prologue
    const/4 v2, 0x0

    .line 357
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->maskState:Z

    invoke-direct {p0, v0, v2, v1}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->setStateInternal(IZZ)V

    .line 358
    iget-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->maskState:Z

    if-nez v0, :cond_0

    .line 359
    iput v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->retries:I

    .line 362
    :cond_0
    iput-object p1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    .line 363
    invoke-static {p0}, Lcom/google/android/videos/async/CancelableCallback;->create(Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/CancelableCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->selectionCancelableCallback:Lcom/google/android/videos/async/CancelableCallback;

    .line 364
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->streamsSelector:Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;

    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playerHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->selectionCancelableCallback:Lcom/google/android/videos/async/CancelableCallback;

    invoke-static {v1, v2}, Lcom/google/android/videos/async/HandlerCallback;->create(Landroid/os/Handler;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/HandlerCallback;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;->getStreams(Lcom/google/android/videos/player/VideoInfo;Lcom/google/android/videos/async/Callback;)V

    .line 366
    return-void
.end method

.method private preparePlayerInternal()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 372
    iget v4, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->internalState:I

    if-eq v4, v8, :cond_0

    .line 408
    :goto_0
    return-void

    .line 377
    :cond_0
    iget-boolean v4, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->hq:Z

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->streamSelection:Lcom/google/android/videos/streams/LegacyStreamSelection;

    iget-object v4, v4, Lcom/google/android/videos/streams/LegacyStreamSelection;->hi:Ljava/util/List;

    iget v7, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->selectedStreamIndex:I

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/videos/streams/MediaStream;

    move-object v2, v4

    .line 379
    .local v2, "stream":Lcom/google/android/videos/streams/MediaStream;
    :goto_1
    iget-boolean v4, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->isGoogleTv:Z

    if-eqz v4, :cond_4

    iget-object v4, v2, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    iget v4, v4, Lcom/google/android/videos/streams/ItagInfo;->drmType:I

    if-ne v4, v8, :cond_4

    move v4, v5

    :goto_2
    iput-boolean v4, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->isWidevineOnGoogleTv:Z

    .line 381
    invoke-direct {p0, v2}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->initMediaPlayer(Lcom/google/android/videos/streams/MediaStream;)Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->mediaPlayer:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    .line 382
    iget-object v4, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->mediaPlayer:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    const/4 v7, 0x3

    invoke-interface {v4, v7}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->setAudioStreamType(I)V

    .line 383
    iget-object v4, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->mediaPlayer:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    invoke-interface {v4, p0}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->setListener(Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;)V

    .line 384
    iget-boolean v4, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->enableVirtualizer:Z

    if-eqz v4, :cond_1

    sget v4, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v7, 0x12

    if-lt v4, v7, :cond_1

    .line 385
    iget-object v4, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->mediaPlayer:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    invoke-interface {v4}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->getAudioSessionId()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/videos/player/PlayerUtil;->createVirtualizerIfAvailableV18(I)Landroid/media/audiofx/Virtualizer;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->virtualizer:Landroid/media/audiofx/Virtualizer;

    .line 386
    invoke-direct {p0}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->enabledVirtualizerInternalV18()V

    .line 391
    :cond_1
    :try_start_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 392
    .local v1, "headers":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v4, "x-disconnect-at-highwatermark"

    const-string v7, "1"

    invoke-virtual {v1, v4, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 393
    iget-object v4, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->mediaPlayer:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    iget-object v7, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->context:Landroid/content/Context;

    iget-object v8, v2, Lcom/google/android/videos/streams/MediaStream;->uri:Landroid/net/Uri;

    invoke-interface {v4, v7, v8, v1}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    .line 396
    iget-object v4, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->mediaPlayer:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    iget-object v7, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->surfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v4, v7}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 397
    iget-object v4, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->mediaPlayer:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    const/4 v7, 0x1

    invoke-interface {v4, v7}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->setScreenOnWhilePlaying(Z)V

    .line 398
    sget v4, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v7, 0x10

    if-lt v4, v7, :cond_5

    iget-object v4, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->mediaPlayer:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    invoke-direct {p0, v4}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->setVideoScalingModeInternalV16(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;)Z

    move-result v3

    .line 400
    .local v3, "zoomSupported":Z
    :goto_3
    iget-object v4, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->eventHandler:Landroid/os/Handler;

    const/4 v7, 0x5

    if-eqz v3, :cond_2

    move v6, v5

    :cond_2
    const/4 v8, 0x0

    invoke-virtual {v4, v7, v6, v8}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/Message;->sendToTarget()V

    .line 403
    iget-object v4, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->mediaPlayer:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    invoke-interface {v4}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->prepareAsync()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 404
    .end local v1    # "headers":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v3    # "zoomSupported":Z
    :catch_0
    move-exception v0

    .line 405
    .local v0, "e":Ljava/io/IOException;
    new-instance v4, Lcom/google/android/videos/player/legacy/MediaPlayerException;

    const/16 v6, -0x4269

    invoke-direct {v4, v6, v0}, Lcom/google/android/videos/player/legacy/MediaPlayerException;-><init>(ILjava/lang/Throwable;)V

    invoke-direct {p0, v5, v4}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->notifyMediaPlayerError(ZLcom/google/android/videos/player/legacy/MediaPlayerException;)V

    goto/16 :goto_0

    .line 377
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "stream":Lcom/google/android/videos/streams/MediaStream;
    :cond_3
    iget-object v4, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->streamSelection:Lcom/google/android/videos/streams/LegacyStreamSelection;

    iget-object v4, v4, Lcom/google/android/videos/streams/LegacyStreamSelection;->lo:Ljava/util/List;

    iget v7, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->selectedStreamIndex:I

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/videos/streams/MediaStream;

    move-object v2, v4

    goto/16 :goto_1

    .restart local v2    # "stream":Lcom/google/android/videos/streams/MediaStream;
    :cond_4
    move v4, v6

    .line 379
    goto/16 :goto_2

    .restart local v1    # "headers":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_5
    move v3, v6

    .line 398
    goto :goto_3
.end method

.method private preparedInternal()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 456
    iget v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->internalState:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 480
    :cond_0
    :goto_0
    return-void

    .line 460
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->isWidevineOnGoogleTv:Z

    if-eqz v0, :cond_3

    .line 463
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->mediaPlayer:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    invoke-interface {v0}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->start()V

    .line 464
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->mediaPlayer:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    invoke-interface {v0}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->getDuration()I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->durationMs:I

    .line 465
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->mediaPlayer:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    invoke-interface {v0}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->pause()V

    .line 469
    :goto_1
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->buffering:Z

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->setStateInternal(IZZ)V

    .line 470
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->updatePlaybackPositionInternal(Z)V

    .line 471
    iget v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->pendingSeekPosition:I

    if-nez v0, :cond_4

    .line 473
    iput v3, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->pendingSeekPosition:I

    .line 477
    :cond_2
    :goto_2
    iget-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playWhenReady:Z

    if-eqz v0, :cond_0

    .line 478
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->mediaPlayer:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    invoke-interface {v0}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->start()V

    goto :goto_0

    .line 467
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->mediaPlayer:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    invoke-interface {v0}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->getDuration()I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->durationMs:I

    goto :goto_1

    .line 474
    :cond_4
    iget v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->pendingSeekPosition:I

    if-eq v0, v3, :cond_2

    .line 475
    iget v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->pendingSeekPosition:I

    iget-boolean v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->pendingSeekEnsureAccurateTime:Z

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->seekToInternal(IZ)V

    goto :goto_2
.end method

.method private declared-synchronized releaseInternal()V
    .locals 1

    .prologue
    .line 545
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, v0}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->resetInternal(Z)V

    .line 546
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->released:Z

    .line 547
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 548
    monitor-exit p0

    return-void

    .line 545
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private releaseVirtualizerInternalV18()V
    .locals 1

    .prologue
    .line 572
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->virtualizer:Landroid/media/audiofx/Virtualizer;

    if-eqz v0, :cond_0

    .line 573
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->virtualizer:Landroid/media/audiofx/Virtualizer;

    invoke-virtual {v0}, Landroid/media/audiofx/Virtualizer;->release()V

    .line 574
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->virtualizer:Landroid/media/audiofx/Virtualizer;

    .line 576
    :cond_0
    return-void
.end method

.method private repreparePlayerInternal()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 414
    iget v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->internalState:I

    if-eq v0, v2, :cond_0

    iget v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->internalState:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->streamSelection:Lcom/google/android/videos/streams/LegacyStreamSelection;

    if-nez v0, :cond_1

    .line 422
    :cond_0
    :goto_0
    return-void

    .line 418
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->pendingSeekPosition:I

    .line 419
    invoke-direct {p0, v2}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->stopInternal(Z)V

    .line 420
    const/4 v0, 0x2

    invoke-direct {p0, v0, v2, v2}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->setStateInternal(IZZ)V

    .line 421
    invoke-direct {p0}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->preparePlayerInternal()V

    goto :goto_0
.end method

.method private resetInternal(Z)V
    .locals 4
    .param p1, "maskState"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 551
    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playbackPositionLock:Ljava/lang/Object;

    monitor-enter v1

    .line 552
    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playbackPositionState:I

    .line 553
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playbackPositionStabilityIncrement:I

    .line 554
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playbackPositionMaybeAdvancing:Z

    .line 555
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 556
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    .line 557
    invoke-direct {p0}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->releaseVirtualizerInternalV18()V

    .line 559
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->selectionCancelableCallback:Lcom/google/android/videos/async/CancelableCallback;

    if-eqz v0, :cond_1

    .line 560
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->selectionCancelableCallback:Lcom/google/android/videos/async/CancelableCallback;

    invoke-virtual {v0}, Lcom/google/android/videos/async/CancelableCallback;->cancel()V

    .line 561
    iput-object v3, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->selectionCancelableCallback:Lcom/google/android/videos/async/CancelableCallback;

    .line 563
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->mediaPlayer:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    if-eqz v0, :cond_2

    .line 564
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->mediaPlayer:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    invoke-interface {v0}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->release()V

    .line 565
    iput-object v3, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->mediaPlayer:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    .line 567
    :cond_2
    const/4 v0, 0x1

    invoke-direct {p0, v0, v2, p1}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->setStateInternal(IZZ)V

    .line 568
    return-void

    .line 555
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private seekToInternal(IZ)V
    .locals 3
    .param p1, "seekTimeMs"    # I
    .param p2, "ensureAccurateTime"    # Z

    .prologue
    .line 516
    iput p1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->pendingSeekPosition:I

    .line 517
    iput-boolean p2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->pendingSeekEnsureAccurateTime:Z

    .line 518
    iget v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->internalState:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 537
    :goto_0
    return-void

    .line 521
    :cond_0
    iget v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->internalState:I

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->maskState:Z

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->setStateInternal(IZZ)V

    .line 522
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->mediaPlayer:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    invoke-interface {v0, p1}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->seekTo(I)V

    .line 526
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->mediaPlayer:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    invoke-interface {v0}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_1

    .line 527
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->mediaPlayer:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    invoke-interface {v0}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->start()V

    .line 529
    const-wide/16 v0, 0xa

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 533
    :goto_1
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->mediaPlayer:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    invoke-interface {v0}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->pause()V

    .line 535
    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->updatePlaybackPositionInternal(Z)V

    .line 536
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->pendingSeekPosition:I

    goto :goto_0

    .line 530
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method private setHqInternal(Z)V
    .locals 1
    .param p1, "hq"    # Z

    .prologue
    .line 500
    iget-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->hq:Z

    if-ne v0, p1, :cond_0

    .line 505
    :goto_0
    return-void

    .line 503
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->hq:Z

    .line 504
    invoke-direct {p0}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->repreparePlayerInternal()V

    goto :goto_0
.end method

.method private setPlayWhenReadyInternal(Z)V
    .locals 3
    .param p1, "playWhenReady"    # Z

    .prologue
    .line 483
    iget-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playWhenReady:Z

    if-ne v0, p1, :cond_1

    .line 497
    :cond_0
    :goto_0
    return-void

    .line 486
    :cond_1
    iput-boolean p1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playWhenReady:Z

    .line 487
    iget v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->internalState:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 490
    iget v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->internalState:I

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->maskState:Z

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->setStateInternal(IZZ)V

    .line 491
    if-eqz p1, :cond_2

    .line 492
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->mediaPlayer:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    invoke-interface {v0}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->start()V

    .line 496
    :goto_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->updatePlaybackPositionInternal(Z)V

    goto :goto_0

    .line 494
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->mediaPlayer:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    invoke-interface {v0}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->pause()V

    goto :goto_1
.end method

.method private setSelectedStreamIndexInternal(I)V
    .locals 1
    .param p1, "selectedStreamIndex"    # I

    .prologue
    .line 508
    iget v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->selectedStreamIndex:I

    if-ne v0, p1, :cond_0

    .line 513
    :goto_0
    return-void

    .line 511
    :cond_0
    iput p1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->selectedStreamIndex:I

    .line 512
    invoke-direct {p0}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->repreparePlayerInternal()V

    goto :goto_0
.end method

.method private setStateInternal(IZZ)V
    .locals 7
    .param p1, "internalState"    # I
    .param p2, "buffering"    # Z
    .param p3, "maskState"    # Z

    .prologue
    const/4 v4, 0x1

    .line 334
    iget v3, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->internalState:I

    if-ne v3, p1, :cond_1

    iget-boolean v3, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->buffering:Z

    if-ne v3, p2, :cond_1

    iget-boolean v3, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->maskState:Z

    if-ne v3, p3, :cond_1

    .line 351
    :cond_0
    :goto_0
    return-void

    .line 339
    :cond_1
    iget v3, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->internalState:I

    iget-boolean v5, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->buffering:Z

    iget-boolean v6, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->maskState:Z

    invoke-static {v3, v5, v6}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->toPlayerState(IZZ)I

    move-result v1

    .line 340
    .local v1, "oldPlayerState":I
    iget-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->maskState:Z

    .line 342
    .local v0, "oldMaskState":Z
    iput p1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->internalState:I

    .line 343
    iput-boolean p2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->buffering:Z

    .line 344
    iput-boolean p3, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->maskState:Z

    .line 346
    invoke-static {p1, p2, p3}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->toPlayerState(IZZ)I

    move-result v2

    .line 347
    .local v2, "playerState":I
    if-ne v2, v1, :cond_2

    if-eq p3, v0, :cond_0

    .line 348
    :cond_2
    iget-object v5, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->eventHandler:Landroid/os/Handler;

    if-eqz p3, :cond_3

    move v3, v4

    :goto_1
    invoke-virtual {v5, v4, v2, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private setVideoScalingModeInternalV16(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;)Z
    .locals 2
    .param p1, "mediaPlayer"    # Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    .prologue
    .line 445
    const/4 v1, 0x2

    :try_start_0
    invoke-interface {p1, v1}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->setVideoScalingModeV16(I)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 447
    const/4 v1, 0x1

    .line 451
    :goto_0
    return v1

    .line 448
    :catch_0
    move-exception v0

    .line 450
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v1, "Failed to set video scaling mode"

    invoke-static {v1}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 451
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private declared-synchronized stopInternal(Z)V
    .locals 1
    .param p1, "maskState"    # Z

    .prologue
    .line 540
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->resetInternal(Z)V

    .line 541
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 542
    monitor-exit p0

    return-void

    .line 540
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static toPlayerState(IZZ)I
    .locals 1
    .param p0, "internalState"    # I
    .param p1, "buffering"    # Z
    .param p2, "maskState"    # Z

    .prologue
    .line 769
    if-nez p2, :cond_0

    const/4 v0, 0x4

    if-ne p0, v0, :cond_1

    if-eqz p1, :cond_1

    .line 770
    :cond_0
    const/4 p0, 0x3

    .line 772
    .end local p0    # "internalState":I
    :cond_1
    return p0
.end method

.method private updatePlaybackPositionInternal(Z)V
    .locals 13
    .param p1, "forceReset"    # Z

    .prologue
    const/4 v9, 0x2

    const/16 v10, 0x9

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 586
    iget v11, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->internalState:I

    const/4 v12, 0x4

    if-eq v11, v12, :cond_0

    .line 642
    :goto_0
    return-void

    .line 589
    :cond_0
    iget-boolean v11, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playWhenReady:Z

    if-eqz v11, :cond_3

    iget-boolean v11, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->buffering:Z

    if-nez v11, :cond_3

    move v0, v7

    .line 592
    .local v0, "maybeAdvancing":Z
    :goto_1
    iget-object v11, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->mediaPlayer:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    invoke-interface {v11}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->getCurrentPosition()I

    move-result v2

    .line 597
    .local v2, "newPlaybackPosition":I
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 598
    .local v4, "newPlaybackPositionSystemTimestamp":J
    iget-object v11, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playbackPositionLock:Ljava/lang/Object;

    monitor-enter v11

    .line 599
    if-nez p1, :cond_1

    :try_start_0
    iget-boolean v12, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playbackPositionMaybeAdvancing:Z

    if-eq v12, v0, :cond_4

    .line 600
    :cond_1
    const/4 v7, 0x0

    iput v7, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playbackPositionState:I

    .line 601
    const/4 v7, 0x0

    iput v7, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playbackPositionStabilityIncrement:I

    .line 602
    iput-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playbackPositionMaybeAdvancing:Z

    .line 624
    :cond_2
    :goto_2
    iput v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playbackPosition:I

    .line 625
    iput-wide v4, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playbackPositionSystemTimestamp:J

    .line 626
    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 627
    iget-object v7, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playerHandler:Landroid/os/Handler;

    invoke-virtual {v7, v10}, Landroid/os/Handler;->removeMessages(I)V

    .line 628
    iget v7, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playbackPositionState:I

    packed-switch v7, :pswitch_data_0

    goto :goto_0

    .line 630
    :pswitch_0
    iget-object v7, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playerHandler:Landroid/os/Handler;

    iget-boolean v8, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->isWidevineOnGoogleTv:Z

    if-eqz v8, :cond_b

    const-wide/16 v8, 0x1f4

    :goto_3
    invoke-virtual {v7, v10, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .end local v0    # "maybeAdvancing":Z
    .end local v2    # "newPlaybackPosition":I
    .end local v4    # "newPlaybackPositionSystemTimestamp":J
    :cond_3
    move v0, v8

    .line 589
    goto :goto_1

    .line 604
    .restart local v0    # "maybeAdvancing":Z
    .restart local v2    # "newPlaybackPosition":I
    .restart local v4    # "newPlaybackPositionSystemTimestamp":J
    :cond_4
    if-eqz v0, :cond_6

    :try_start_1
    iget v12, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playbackPosition:I

    if-le v2, v12, :cond_5

    move v3, v7

    .line 606
    .local v3, "stabilizing":Z
    :goto_4
    if-eqz v3, :cond_a

    .line 607
    if-eqz v0, :cond_8

    move v6, v7

    .line 609
    .local v6, "targetStableState":I
    :goto_5
    iget v8, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playbackPositionState:I

    if-eq v8, v6, :cond_2

    .line 610
    iget v8, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playbackPositionStabilityIncrement:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playbackPositionStabilityIncrement:I

    .line 611
    iget v8, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playbackPositionStabilityIncrement:I

    if-lt v8, v9, :cond_2

    .line 612
    iput v6, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playbackPositionState:I

    .line 613
    if-ne v6, v7, :cond_9

    const/16 v1, 0x8

    .line 615
    .local v1, "messageType":I
    :goto_6
    iget-object v7, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->eventHandler:Landroid/os/Handler;

    const/4 v8, 0x0

    invoke-virtual {v7, v1, v2, v8}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v7}, Landroid/os/Message;->sendToTarget()V

    goto :goto_2

    .line 626
    .end local v1    # "messageType":I
    .end local v3    # "stabilizing":Z
    .end local v6    # "targetStableState":I
    :catchall_0
    move-exception v7

    monitor-exit v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v7

    :cond_5
    move v3, v8

    .line 604
    goto :goto_4

    :cond_6
    :try_start_2
    iget v12, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playbackPosition:I

    if-ne v2, v12, :cond_7

    move v3, v7

    goto :goto_4

    :cond_7
    move v3, v8

    goto :goto_4

    .restart local v3    # "stabilizing":Z
    :cond_8
    move v6, v9

    .line 607
    goto :goto_5

    .restart local v6    # "targetStableState":I
    :cond_9
    move v1, v10

    .line 613
    goto :goto_6

    .line 619
    .end local v6    # "targetStableState":I
    :cond_a
    const/4 v7, 0x0

    iput v7, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playbackPositionState:I

    .line 620
    const/4 v7, 0x0

    iput v7, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playbackPositionStabilityIncrement:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 630
    .end local v3    # "stabilizing":Z
    :cond_b
    const-wide/16 v8, 0x64

    goto :goto_3

    .line 635
    :pswitch_1
    iget-object v7, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playerHandler:Landroid/os/Handler;

    const-wide/16 v8, 0x3e8

    invoke-virtual {v7, v10, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 628
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method declared-synchronized blockingStop()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 263
    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->internalState:I

    if-eq v1, v3, :cond_0

    .line 264
    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playerHandler:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 265
    :goto_0
    iget v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->internalState:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v1, v3, :cond_0

    .line 267
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 268
    :catch_0
    move-exception v0

    .line 269
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 263
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 273
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public getBufferedPercentage()I
    .locals 1

    .prologue
    .line 218
    iget v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->bufferedPercentage:I

    return v0
.end method

.method public getCurrentPosition()I
    .locals 8

    .prologue
    .line 226
    iget v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->pendingSeekPosition:I

    .line 227
    .local v0, "pendingSeekPosition":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 231
    .end local v0    # "pendingSeekPosition":I
    :goto_0
    return v0

    .line 230
    .restart local v0    # "pendingSeekPosition":I
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playbackPositionLock:Ljava/lang/Object;

    monitor-enter v2

    .line 231
    :try_start_0
    iget v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playbackPositionState:I

    const/4 v3, 0x1

    if-ne v1, v3, :cond_1

    iget v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playbackPosition:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playbackPositionSystemTimestamp:J

    sub-long/2addr v4, v6

    long-to-int v3, v4

    add-int v0, v1, v3

    .end local v0    # "pendingSeekPosition":I
    :goto_1
    monitor-exit v2

    goto :goto_0

    .line 235
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 231
    .restart local v0    # "pendingSeekPosition":I
    :cond_1
    :try_start_1
    iget v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playbackPosition:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public declared-synchronized handleMessage(Landroid/os/Message;)Z
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 292
    monitor-enter p0

    :try_start_0
    iget-boolean v3, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->released:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v3, :cond_0

    move v1, v2

    .line 330
    :goto_0
    monitor-exit p0

    return v1

    .line 296
    :cond_0
    :try_start_1
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 298
    :pswitch_0
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/videos/player/VideoInfo;

    invoke-direct {p0, v1}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->prepareInternal(Lcom/google/android/videos/player/VideoInfo;)V

    move v1, v2

    .line 299
    goto :goto_0

    .line 301
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->preparedInternal()V

    move v1, v2

    .line 302
    goto :goto_0

    .line 304
    :pswitch_2
    iget v3, p1, Landroid/os/Message;->arg1:I

    if-eqz v3, :cond_1

    move v1, v2

    :cond_1
    invoke-direct {p0, v1}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->setPlayWhenReadyInternal(Z)V

    move v1, v2

    .line 305
    goto :goto_0

    .line 307
    :pswitch_3
    iget v3, p1, Landroid/os/Message;->arg1:I

    if-eqz v3, :cond_2

    move v1, v2

    :cond_2
    invoke-direct {p0, v1}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->setHqInternal(Z)V

    move v1, v2

    .line 308
    goto :goto_0

    .line 310
    :pswitch_4
    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-direct {p0, v1}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->setSelectedStreamIndexInternal(I)V

    move v1, v2

    .line 311
    goto :goto_0

    .line 313
    :pswitch_5
    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    if-eqz v4, :cond_3

    move v1, v2

    :cond_3
    invoke-direct {p0, v3, v1}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->seekToInternal(IZ)V

    move v1, v2

    .line 314
    goto :goto_0

    .line 316
    :pswitch_6
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->stopInternal(Z)V

    move v1, v2

    .line 317
    goto :goto_0

    .line 319
    :pswitch_7
    invoke-direct {p0}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->releaseInternal()V

    move v1, v2

    .line 320
    goto :goto_0

    .line 322
    :pswitch_8
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->updatePlaybackPositionInternal(Z)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v1, v2

    .line 323
    goto :goto_0

    .line 325
    :catch_0
    move-exception v0

    .line 327
    .local v0, "e":Ljava/lang/IllegalStateException;
    :try_start_2
    const-string v1, "Error calling mediaPlayer"

    invoke-static {v1, v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v1, v2

    .line 328
    goto :goto_0

    .line 292
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 296
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public onBufferingUpdate(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;I)V
    .locals 2
    .param p1, "mediaPlayer"    # Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
    .param p2, "newPercentage"    # I

    .prologue
    .line 691
    iget-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->released:Z

    if-eqz v0, :cond_0

    .line 700
    :goto_0
    return-void

    .line 696
    :cond_0
    const/16 v0, 0x5a

    if-le p2, v0, :cond_2

    iget v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->bufferedPercentage:I

    if-eq v0, p2, :cond_1

    iget v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->bufferedPercentage:I

    const/16 v1, 0x64

    if-ne v0, v1, :cond_2

    .line 697
    :cond_1
    const/16 p2, 0x64

    .line 699
    :cond_2
    iput p2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->bufferedPercentage:I

    goto :goto_0
.end method

.method public onCompletion(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;)V
    .locals 2
    .param p1, "mediaPlayer"    # Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    .prologue
    const/4 v1, 0x0

    .line 709
    iget-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->released:Z

    if-eqz v0, :cond_0

    .line 714
    :goto_0
    return-void

    .line 713
    :cond_0
    const/4 v0, 0x5

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->setStateInternal(IZZ)V

    goto :goto_0
.end method

.method public onError(Lcom/google/android/videos/player/VideoInfo;Ljava/lang/Exception;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/player/VideoInfo;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 661
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->selectionCancelableCallback:Lcom/google/android/videos/async/CancelableCallback;

    .line 662
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->eventHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 663
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->stopInternal(Z)V

    .line 664
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 48
    check-cast p1, Lcom/google/android/videos/player/VideoInfo;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->onError(Lcom/google/android/videos/player/VideoInfo;Ljava/lang/Exception;)V

    return-void
.end method

.method public onError(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;II)Z
    .locals 10
    .param p1, "mediaPlayer"    # Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 718
    iget-boolean v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->released:Z

    if-eqz v2, :cond_0

    .line 738
    :goto_0
    return v3

    .line 722
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MediaPlayer error [what="

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", extra="

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "]"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 723
    iget v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->pendingSeekPosition:I

    const/4 v5, -0x1

    if-ne v2, v5, :cond_1

    .line 726
    invoke-virtual {p0}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->getCurrentPosition()I

    move-result v2

    iput v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->pendingSeekPosition:I

    .line 728
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->lastRetrySystemTimeMs:J

    sub-long/2addr v6, v8

    const-wide/32 v8, 0x493e0

    cmp-long v2, v6, v8

    if-lez v2, :cond_4

    move v1, v3

    .line 730
    .local v1, "resetRetryCount":Z
    :goto_1
    if-eqz v1, :cond_5

    move v2, v3

    :goto_2
    iput v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->retries:I

    .line 731
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->lastRetrySystemTimeMs:J

    .line 732
    iget v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->retries:I

    const/4 v5, 0x3

    if-gt v2, v5, :cond_3

    if-ne p2, v3, :cond_2

    sget-object v2, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->FATAL_ERROR_CODES:Ljava/util/Set;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    iget-object v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-boolean v2, v2, Lcom/google/android/videos/player/VideoInfo;->isOffline:Z

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v2}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v2

    if-nez v2, :cond_6

    :cond_3
    move v0, v3

    .line 735
    .local v0, "fatal":Z
    :goto_3
    new-instance v2, Lcom/google/android/videos/player/legacy/MediaPlayerException;

    invoke-direct {v2, p2, p3}, Lcom/google/android/videos/player/legacy/MediaPlayerException;-><init>(II)V

    invoke-direct {p0, v0, v2}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->notifyMediaPlayerError(ZLcom/google/android/videos/player/legacy/MediaPlayerException;)V

    .line 736
    if-nez v0, :cond_7

    move v2, v3

    :goto_4
    invoke-direct {p0, v2}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->stopInternal(Z)V

    .line 737
    iget-object v5, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->eventHandler:Landroid/os/Handler;

    const/4 v6, 0x7

    if-eqz v0, :cond_8

    move v2, v3

    :goto_5
    invoke-virtual {v5, v6, v2, v4}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    .end local v0    # "fatal":Z
    .end local v1    # "resetRetryCount":Z
    :cond_4
    move v1, v4

    .line 728
    goto :goto_1

    .line 730
    .restart local v1    # "resetRetryCount":Z
    :cond_5
    iget v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->retries:I

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_6
    move v0, v4

    .line 732
    goto :goto_3

    .restart local v0    # "fatal":Z
    :cond_7
    move v2, v4

    .line 736
    goto :goto_4

    :cond_8
    move v2, v4

    .line 737
    goto :goto_5
.end method

.method public onInfo(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;II)Z
    .locals 4
    .param p1, "mediaPlayer"    # Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 743
    iget-boolean v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->released:Z

    if-eqz v2, :cond_0

    .line 757
    :goto_0
    return v0

    .line 747
    :cond_0
    packed-switch p2, :pswitch_data_0

    :goto_1
    move v0, v1

    .line 757
    goto :goto_0

    .line 749
    :pswitch_0
    iget v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->internalState:I

    iget-boolean v3, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->maskState:Z

    invoke-direct {p0, v2, v0, v3}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->setStateInternal(IZZ)V

    .line 750
    invoke-direct {p0, v1}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->updatePlaybackPositionInternal(Z)V

    goto :goto_1

    .line 753
    :pswitch_1
    iget v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->internalState:I

    iget-boolean v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->maskState:Z

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->setStateInternal(IZZ)V

    .line 754
    invoke-direct {p0, v1}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->updatePlaybackPositionInternal(Z)V

    goto :goto_1

    .line 747
    :pswitch_data_0
    .packed-switch 0x2bd
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPrepared(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;)V
    .locals 2
    .param p1, "mediaPlayer"    # Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    .prologue
    .line 670
    iget-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->released:Z

    if-eqz v0, :cond_0

    .line 675
    :goto_0
    return-void

    .line 674
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playerHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public onResponse(Lcom/google/android/videos/player/VideoInfo;Lcom/google/android/videos/streams/LegacyStreamSelection;)V
    .locals 4
    .param p1, "request"    # Lcom/google/android/videos/player/VideoInfo;
    .param p2, "response"    # Lcom/google/android/videos/streams/LegacyStreamSelection;

    .prologue
    .line 648
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->selectionCancelableCallback:Lcom/google/android/videos/async/CancelableCallback;

    .line 649
    iput-object p2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->streamSelection:Lcom/google/android/videos/streams/LegacyStreamSelection;

    .line 650
    iget v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->selectedStreamIndex:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->selectedStreamIndex:I

    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->streamSelection:Lcom/google/android/videos/streams/LegacyStreamSelection;

    iget-object v1, v1, Lcom/google/android/videos/streams/LegacyStreamSelection;->hi:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 651
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->streamSelection:Lcom/google/android/videos/streams/LegacyStreamSelection;

    iget-object v0, v0, Lcom/google/android/videos/streams/LegacyStreamSelection;->hi:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->preferences:Landroid/content/SharedPreferences;

    invoke-static {v0, v1, v2}, Lcom/google/android/videos/utils/AudioInfoUtil;->getPreferredStreamIndex(Ljava/util/List;Landroid/content/Context;Landroid/content/SharedPreferences;)I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->selectedStreamIndex:I

    .line 654
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->eventHandler:Landroid/os/Handler;

    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->selectedStreamIndex:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3, p2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 656
    invoke-direct {p0}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->preparePlayerInternal()V

    .line 657
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 48
    check-cast p1, Lcom/google/android/videos/player/VideoInfo;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/videos/streams/LegacyStreamSelection;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->onResponse(Lcom/google/android/videos/player/VideoInfo;Lcom/google/android/videos/streams/LegacyStreamSelection;)V

    return-void
.end method

.method public onSeekComplete(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;)V
    .locals 0
    .param p1, "mediaPlayer"    # Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    .prologue
    .line 705
    return-void
.end method

.method public onVideoSizeChanged(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;II)V
    .locals 2
    .param p1, "mediaPlayer"    # Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 679
    iget-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->released:Z

    if-eqz v0, :cond_1

    .line 687
    :cond_0
    :goto_0
    return-void

    .line 683
    :cond_1
    if-lez p2, :cond_0

    if-lez p3, :cond_0

    .line 686
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->eventHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, p2, p3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method public prepare(Lcom/google/android/videos/player/VideoInfo;)V
    .locals 2
    .param p1, "videoInfo"    # Lcom/google/android/videos/player/VideoInfo;

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playerHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 240
    return-void
.end method

.method public declared-synchronized release()V
    .locals 3

    .prologue
    .line 276
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->released:Z

    if-nez v1, :cond_1

    .line 277
    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playerHandler:Landroid/os/Handler;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 278
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->released:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 280
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 281
    :catch_0
    move-exception v0

    .line 282
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 276
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 285
    :cond_0
    :try_start_3
    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playerHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 286
    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->quit()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 288
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public seekTo(IZ)V
    .locals 3
    .param p1, "timeMillis"    # I
    .param p2, "ensureAccurateTime"    # Z

    .prologue
    const/4 v2, 0x6

    .line 258
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 259
    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playerHandler:Landroid/os/Handler;

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, p1, v0}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 260
    return-void

    .line 259
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setHq(Z)V
    .locals 4
    .param p1, "hq"    # Z

    .prologue
    const/4 v3, 0x4

    const/4 v1, 0x0

    .line 248
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 249
    iget-object v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playerHandler:Landroid/os/Handler;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 250
    return-void

    :cond_0
    move v0, v1

    .line 249
    goto :goto_0
.end method

.method public setPlayWhenReady(Z)V
    .locals 4
    .param p1, "playWhenReady"    # Z

    .prologue
    const/4 v3, 0x3

    const/4 v1, 0x0

    .line 243
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 244
    iget-object v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playerHandler:Landroid/os/Handler;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 245
    return-void

    :cond_0
    move v0, v1

    .line 244
    goto :goto_0
.end method

.method public setSelectedStreamIndex(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    const/4 v2, 0x5

    .line 253
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 254
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->playerHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, p1, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 255
    return-void
.end method

.method public setSurfaceHolder(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1, "surfaceHolder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 214
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceHolder;

    iput-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerInternal;->surfaceHolder:Landroid/view/SurfaceHolder;

    .line 215
    return-void
.end method
