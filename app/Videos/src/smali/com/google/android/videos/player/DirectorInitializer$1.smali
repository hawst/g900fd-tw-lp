.class Lcom/google/android/videos/player/DirectorInitializer$1;
.super Lcom/google/android/videos/player/DirectorInitializer$DirectorAuthenticatee;
.source "DirectorInitializer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/player/DirectorInitializer;->onTrailerAssetsResponse(Lcom/google/wireless/android/video/magma/proto/AssetListResponse;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/player/DirectorInitializer;

.field final synthetic val$decoratedStreamsCallback:Lcom/google/android/videos/async/Callback;


# direct methods
.method constructor <init>(Lcom/google/android/videos/player/DirectorInitializer;Lcom/google/android/videos/async/Callback;)V
    .locals 1

    .prologue
    .line 307
    iput-object p1, p0, Lcom/google/android/videos/player/DirectorInitializer$1;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    iput-object p2, p0, Lcom/google/android/videos/player/DirectorInitializer$1;->val$decoratedStreamsCallback:Lcom/google/android/videos/async/Callback;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/player/DirectorInitializer$DirectorAuthenticatee;-><init>(Lcom/google/android/videos/player/DirectorInitializer;Lcom/google/android/videos/player/DirectorInitializer$1;)V

    return-void
.end method


# virtual methods
.method public onAuthenticated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "authToken"    # Ljava/lang/String;

    .prologue
    .line 310
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Getting mpd of "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/player/DirectorInitializer$1;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->videoId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/videos/player/DirectorInitializer;->access$100(Lcom/google/android/videos/player/DirectorInitializer;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 311
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer$1;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;
    invoke-static {v0}, Lcom/google/android/videos/player/DirectorInitializer;->access$200(Lcom/google/android/videos/player/DirectorInitializer;)Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->recordTaskStart(I)V

    .line 312
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer$1;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->streamsRequester:Lcom/google/android/videos/async/Requester;
    invoke-static {v0}, Lcom/google/android/videos/player/DirectorInitializer;->access$400(Lcom/google/android/videos/player/DirectorInitializer;)Lcom/google/android/videos/async/Requester;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/player/DirectorInitializer$1;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    const/4 v2, 0x1

    # invokes: Lcom/google/android/videos/player/DirectorInitializer;->generateStreamsRequest(Z)Lcom/google/android/videos/api/MpdGetRequest;
    invoke-static {v1, v2}, Lcom/google/android/videos/player/DirectorInitializer;->access$300(Lcom/google/android/videos/player/DirectorInitializer;Z)Lcom/google/android/videos/api/MpdGetRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/player/DirectorInitializer$1;->val$decoratedStreamsCallback:Lcom/google/android/videos/async/Callback;

    invoke-interface {v0, v1, v2}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 313
    return-void
.end method
