.class public Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;
.super Ljava/lang/Object;
.source "NonAdaptiveQualityDropHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper$TimeRange;
    }
.end annotation


# instance fields
.field private alreadyDroppedQuality:Z

.field private final bufferingEvents:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper$TimeRange;",
            ">;"
        }
    .end annotation
.end field

.field private final eventCountForDrop:I

.field private final eventWindowMillis:J

.field private final initialIgnoreWindowMillis:J

.field private newPlayback:Z

.field private playbackStartTime:J

.field private final player:Lcom/google/android/videos/player/VideosPlayer;

.field private selectionSupportsQualityToggle:Z


# direct methods
.method public constructor <init>(Lcom/google/android/videos/player/VideosPlayer;Lcom/google/android/videos/Config;)V
    .locals 2
    .param p1, "player"    # Lcom/google/android/videos/player/VideosPlayer;
    .param p2, "config"    # Lcom/google/android/videos/Config;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    .line 30
    invoke-interface {p2}, Lcom/google/android/videos/Config;->legacyBufferingEventsForQualityDrop()I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->eventCountForDrop:I

    .line 31
    invoke-interface {p2}, Lcom/google/android/videos/Config;->legacyBufferingEventWindowMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->eventWindowMillis:J

    .line 32
    invoke-interface {p2}, Lcom/google/android/videos/Config;->legacyBufferingEventInitialIgnoreWindowMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->initialIgnoreWindowMillis:J

    .line 33
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->bufferingEvents:Ljava/util/LinkedList;

    .line 34
    return-void
.end method

.method private onBuffering()Z
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 85
    iget-boolean v4, p0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->alreadyDroppedQuality:Z

    if-nez v4, :cond_0

    iget-boolean v4, p0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->selectionSupportsQualityToggle:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    invoke-interface {v4}, Lcom/google/android/videos/player/VideosPlayer;->getHq()Z

    move-result v4

    if-nez v4, :cond_1

    .line 98
    :cond_0
    :goto_0
    return v2

    .line 88
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 89
    .local v0, "now":J
    iget-wide v4, p0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->playbackStartTime:J

    sub-long v4, v0, v4

    iget-wide v6, p0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->initialIgnoreWindowMillis:J

    cmp-long v4, v4, v6

    if-ltz v4, :cond_0

    .line 92
    iget-object v4, p0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->bufferingEvents:Ljava/util/LinkedList;

    new-instance v5, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper$TimeRange;

    invoke-direct {v5, v0, v1}, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper$TimeRange;-><init>(J)V

    invoke-virtual {v4, v5}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 93
    invoke-direct {p0}, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->removeOldEvents()V

    .line 94
    iget-object v4, p0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->bufferingEvents:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    iget v5, p0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->eventCountForDrop:I

    if-lt v4, v5, :cond_0

    .line 95
    iput-boolean v3, p0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->alreadyDroppedQuality:Z

    move v2, v3

    .line 96
    goto :goto_0
.end method

.method private onNewPlayback()V
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->newPlayback:Z

    .line 69
    iget-object v0, p0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->bufferingEvents:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 70
    return-void
.end method

.method private onPlayerReady()V
    .locals 6

    .prologue
    .line 73
    iget-boolean v1, p0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->newPlayback:Z

    if-eqz v1, :cond_1

    .line 74
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->newPlayback:Z

    .line 75
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->playbackStartTime:J

    .line 82
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->bufferingEvents:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 77
    iget-object v1, p0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->bufferingEvents:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper$TimeRange;

    .line 78
    .local v0, "bufferingEvent":Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper$TimeRange;
    iget-wide v2, v0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper$TimeRange;->upperBound:J

    iget-wide v4, v0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper$TimeRange;->lowerBound:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 79
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper$TimeRange;->upperBound:J

    goto :goto_0
.end method

.method private removeOldEvents()V
    .locals 10

    .prologue
    .line 102
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 103
    .local v4, "nextEventStartTime":J
    const-wide/16 v6, 0x0

    .line 104
    .local v6, "playingMillisSinceEvent":J
    const/4 v1, 0x0

    .line 105
    .local v1, "eventsWithinWindow":I
    iget-object v3, p0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->bufferingEvents:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper$TimeRange;

    .line 106
    .local v0, "event":Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper$TimeRange;
    iget-wide v8, v0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper$TimeRange;->upperBound:J

    sub-long v8, v4, v8

    add-long/2addr v6, v8

    .line 107
    iget-wide v8, p0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->eventWindowMillis:J

    cmp-long v3, v6, v8

    if-lez v3, :cond_1

    .line 113
    .end local v0    # "event":Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper$TimeRange;
    :cond_0
    :goto_1
    iget-object v3, p0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->bufferingEvents:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 114
    iget-object v3, p0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->bufferingEvents:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    goto :goto_1

    .line 110
    .restart local v0    # "event":Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper$TimeRange;
    :cond_1
    iget-wide v4, v0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper$TimeRange;->lowerBound:J

    .line 111
    add-int/lit8 v1, v1, 0x1

    .line 112
    goto :goto_0

    .line 116
    .end local v0    # "event":Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper$TimeRange;
    :cond_2
    return-void
.end method


# virtual methods
.method public onStateChanged(IZZ)Z
    .locals 1
    .param p1, "state"    # I
    .param p2, "stateIsMasked"    # Z
    .param p3, "playWhenReady"    # Z

    .prologue
    const/4 v0, 0x0

    .line 44
    if-nez p3, :cond_1

    .line 45
    invoke-direct {p0}, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->onNewPlayback()V

    .line 63
    :cond_0
    :goto_0
    return v0

    .line 48
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 52
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->onNewPlayback()V

    goto :goto_0

    .line 55
    :pswitch_1
    if-nez p2, :cond_0

    .line 56
    invoke-direct {p0}, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->onBuffering()Z

    move-result v0

    goto :goto_0

    .line 60
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->onPlayerReady()V

    goto :goto_0

    .line 48
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public onStreamsSelected(Z)V
    .locals 0
    .param p1, "selectionSupportsQualityToggle"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;->selectionSupportsQualityToggle:Z

    .line 38
    return-void
.end method
