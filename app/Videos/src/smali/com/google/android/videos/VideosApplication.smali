.class public Lcom/google/android/videos/VideosApplication;
.super Landroid/app/Application;
.source "VideosApplication.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/VideosApplication$1;,
        Lcom/google/android/videos/VideosApplication$SearchCollapseListener;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 27
    return-void
.end method

.method public static createCommonMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1
    .param p0, "menu"    # Landroid/view/Menu;
    .param p1, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 77
    const/high16 v0, 0x7f140000

    invoke-virtual {p1, v0, p0}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 78
    return-void
.end method

.method public static createSearchMenu(Landroid/view/Menu;Landroid/view/MenuInflater;Landroid/app/Activity;)Landroid/view/MenuItem;
    .locals 7
    .param p0, "menu"    # Landroid/view/Menu;
    .param p1, "inflater"    # Landroid/view/MenuInflater;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v5, 0x0

    .line 61
    const-string v6, "search"

    invoke-virtual {p2, v6}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/SearchManager;

    .line 62
    .local v3, "searchManager":Landroid/app/SearchManager;
    invoke-virtual {p2}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/app/SearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    move-result-object v1

    .line 63
    .local v1, "searchInfo":Landroid/app/SearchableInfo;
    if-eqz v1, :cond_0

    .line 64
    const v6, 0x7f140003

    invoke-virtual {p1, v6, p0}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 65
    const v6, 0x7f0f0224

    invoke-interface {p0, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 66
    .local v2, "searchItem":Landroid/view/MenuItem;
    invoke-static {v2}, Landroid/support/v4/view/MenuItemCompat;->getActionView(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/support/v7/widget/SearchView;

    .line 67
    .local v4, "searchView":Landroid/support/v7/widget/SearchView;
    invoke-virtual {v4, v1}, Landroid/support/v7/widget/SearchView;->setSearchableInfo(Landroid/app/SearchableInfo;)V

    .line 68
    new-instance v0, Lcom/google/android/videos/VideosApplication$SearchCollapseListener;

    invoke-direct {v0, v2, v5}, Lcom/google/android/videos/VideosApplication$SearchCollapseListener;-><init>(Landroid/view/MenuItem;Lcom/google/android/videos/VideosApplication$1;)V

    .line 69
    .local v0, "listener":Lcom/google/android/videos/VideosApplication$SearchCollapseListener;
    invoke-virtual {v4, v0}, Landroid/support/v7/widget/SearchView;->setOnQueryTextListener(Landroid/support/v7/widget/SearchView$OnQueryTextListener;)V

    .line 70
    invoke-virtual {v4, v0}, Landroid/support/v7/widget/SearchView;->setOnSuggestionListener(Landroid/support/v7/widget/SearchView$OnSuggestionListener;)V

    .line 73
    .end local v0    # "listener":Lcom/google/android/videos/VideosApplication$SearchCollapseListener;
    .end local v2    # "searchItem":Landroid/view/MenuItem;
    .end local v4    # "searchView":Landroid/support/v7/widget/SearchView;
    :goto_0
    return-object v2

    :cond_0
    move-object v2, v5

    goto :goto_0
.end method

.method public static onCommonOptionsItemSelected(Landroid/view/MenuItem;Landroid/app/Activity;)Z
    .locals 1
    .param p0, "item"    # Landroid/view/MenuItem;
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 81
    invoke-interface {p0}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 86
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 83
    :pswitch_0
    invoke-static {p1}, Lcom/google/android/videos/activity/SettingsActivity;->createIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 84
    const/4 v0, 0x1

    goto :goto_0

    .line 81
    :pswitch_data_0
    .packed-switch 0x7f0f0220
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public onTrimMemory(I)V
    .locals 1
    .param p1, "level"    # I

    .prologue
    .line 24
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/videos/VideosGlobals;->onTrimMemory(I)V

    .line 25
    return-void
.end method
