.class public interface abstract Lcom/google/android/videos/cast/CastMediaRouter;
.super Ljava/lang/Object;
.source "CastMediaRouter.java"


# virtual methods
.method public abstract createRemote(Landroid/support/v7/media/MediaRouter$RouteInfo;Lcom/google/android/videos/remote/MediaRouteManager$Listener;)V
.end method

.method public abstract getRouteSelector()Landroid/support/v7/media/MediaRouteSelector;
.end method

.method public abstract getUserDisplayName()Ljava/lang/String;
.end method

.method public abstract isCastDevice(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z
.end method

.method public abstract start()V
.end method
