.class Lcom/google/android/videos/ui/DefaultItemAnimator$6;
.super Ljava/lang/Object;
.source "DefaultItemAnimator.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/ui/DefaultItemAnimator;->runPendingAnimations()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/ui/DefaultItemAnimator;

.field final synthetic val$moves:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/google/android/videos/ui/DefaultItemAnimator;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 350
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator$6;, "Lcom/google/android/videos/ui/DefaultItemAnimator.6;"
    iput-object p1, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$6;->this$0:Lcom/google/android/videos/ui/DefaultItemAnimator;

    iput-object p2, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$6;->val$moves:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 353
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator$6;, "Lcom/google/android/videos/ui/DefaultItemAnimator.6;"
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$6;->val$moves:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;

    .line 354
    .local v7, "moveInfo":Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;, "Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo<TT;>;"
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$6;->this$0:Lcom/google/android/videos/ui/DefaultItemAnimator;

    iget-object v1, v7, Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;->holder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    iget v2, v7, Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;->fromX:I

    iget v3, v7, Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;->fromY:I

    iget v4, v7, Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;->toX:I

    iget v5, v7, Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;->toY:I

    # invokes: Lcom/google/android/videos/ui/DefaultItemAnimator;->animateMoveImpl(Landroid/support/v7/widget/RecyclerView$ViewHolder;IIII)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/ui/DefaultItemAnimator;->access$400(Lcom/google/android/videos/ui/DefaultItemAnimator;Landroid/support/v7/widget/RecyclerView$ViewHolder;IIII)V

    goto :goto_0

    .line 357
    .end local v7    # "moveInfo":Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo;, "Lcom/google/android/videos/ui/DefaultItemAnimator$MoveInfo<TT;>;"
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$6;->val$moves:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 358
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$6;->this$0:Lcom/google/android/videos/ui/DefaultItemAnimator;

    # getter for: Lcom/google/android/videos/ui/DefaultItemAnimator;->mMovesList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/videos/ui/DefaultItemAnimator;->access$500(Lcom/google/android/videos/ui/DefaultItemAnimator;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$6;->val$moves:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 359
    return-void
.end method
