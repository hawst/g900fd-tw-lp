.class Lcom/google/android/videos/remote/RemoteControl$UpdateHandler;
.super Landroid/os/Handler;
.source "RemoteControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/remote/RemoteControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UpdateHandler"
.end annotation


# instance fields
.field private final owner:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/videos/remote/RemoteControl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/videos/remote/RemoteControl;)V
    .locals 1
    .param p1, "control"    # Lcom/google/android/videos/remote/RemoteControl;

    .prologue
    .line 472
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 473
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/videos/remote/RemoteControl$UpdateHandler;->owner:Ljava/lang/ref/WeakReference;

    .line 474
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 478
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteControl$UpdateHandler;->owner:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/remote/RemoteControl;

    .line 479
    .local v0, "owner":Lcom/google/android/videos/remote/RemoteControl;
    if-nez v0, :cond_0

    .line 488
    :goto_0
    return-void

    .line 483
    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 484
    # invokes: Lcom/google/android/videos/remote/RemoteControl;->onMessagePlayingUpdate()V
    invoke-static {v0}, Lcom/google/android/videos/remote/RemoteControl;->access$300(Lcom/google/android/videos/remote/RemoteControl;)V

    goto :goto_0

    .line 486
    :cond_1
    invoke-virtual {v0, p1}, Lcom/google/android/videos/remote/RemoteControl;->handleMessage(Landroid/os/Message;)V

    goto :goto_0
.end method
