.class public Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;
.super Landroid/view/ViewGroup;
.source "DefaultSubtitlesOverlay.java"

# interfaces
.implements Lcom/google/android/videos/player/overlay/SubtitlesOverlay;


# instance fields
.field private captionStyle:Lcom/google/android/exoplayer/text/CaptionStyleCompat;

.field private fontScale:F

.field private final minReadableFontSize:F

.field private final windowSnapshots:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;",
            ">;"
        }
    .end annotation
.end field

.field private final windowViews:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/exoplayer/text/SubtitleView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 52
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->windowSnapshots:Landroid/util/SparseArray;

    .line 53
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->windowViews:Landroid/util/SparseArray;

    .line 54
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->fontScale:F

    .line 55
    sget-object v0, Lcom/google/android/exoplayer/text/CaptionStyleCompat;->DEFAULT:Lcom/google/android/exoplayer/text/CaptionStyleCompat;

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->captionStyle:Lcom/google/android/exoplayer/text/CaptionStyleCompat;

    .line 56
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e01d7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->minReadableFontSize:F

    .line 58
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->hide()V

    .line 59
    return-void
.end method

.method private createSubtitleView(Ljava/lang/Object;Ljava/lang/CharSequence;)Lcom/google/android/exoplayer/text/SubtitleView;
    .locals 2
    .param p1, "tag"    # Ljava/lang/Object;
    .param p2, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 276
    new-instance v0, Lcom/google/android/exoplayer/text/SubtitleView;

    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/text/SubtitleView;-><init>(Landroid/content/Context;)V

    .line 277
    .local v0, "view":Lcom/google/android/exoplayer/text/SubtitleView;
    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer/text/SubtitleView;->setTag(Ljava/lang/Object;)V

    .line 278
    invoke-virtual {v0, p2}, Lcom/google/android/exoplayer/text/SubtitleView;->setText(Ljava/lang/CharSequence;)V

    .line 279
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->captionStyle:Lcom/google/android/exoplayer/text/CaptionStyleCompat;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer/text/SubtitleView;->setStyle(Lcom/google/android/exoplayer/text/CaptionStyleCompat;)V

    .line 280
    return-object v0
.end method

.method private layoutWindowView(Landroid/view/View;Lcom/google/android/videos/subtitles/SubtitleWindowSettings;IIII)V
    .locals 9
    .param p1, "windowView"    # Landroid/view/View;
    .param p2, "windowSettings"    # Lcom/google/android/videos/subtitles/SubtitleWindowSettings;
    .param p3, "offsetX"    # I
    .param p4, "offsetY"    # I
    .param p5, "subtitleAreaWidth"    # I
    .param p6, "subtitleAreaHeight"    # I

    .prologue
    .line 239
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    .line 240
    .local v4, "windowWidth":I
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    .line 242
    .local v3, "windowHeight":I
    iget v0, p2, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;->anchorPoint:I

    .line 243
    .local v0, "anchorPoint":I
    iget v7, p2, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;->anchorHorizontalPos:I

    mul-int/2addr v7, p5

    div-int/lit8 v1, v7, 0x64

    .line 244
    .local v1, "anchorX":I
    iget v7, p2, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;->anchorVerticalPos:I

    mul-int/2addr v7, p6

    div-int/lit8 v2, v7, 0x64

    .line 247
    .local v2, "anchorY":I
    and-int/lit8 v7, v0, 0x1

    if-eqz v7, :cond_0

    .line 248
    move v5, v1

    .line 259
    .local v5, "windowX":I
    :goto_0
    and-int/lit8 v7, v0, 0x8

    if-eqz v7, :cond_3

    .line 260
    move v6, v2

    .line 269
    .local v6, "windowY":I
    :goto_1
    add-int/2addr v5, p3

    .line 270
    add-int/2addr v6, p4

    .line 272
    add-int v7, v5, v4

    add-int v8, v6, v3

    invoke-virtual {p1, v5, v6, v7, v8}, Landroid/view/View;->layout(IIII)V

    .line 273
    return-void

    .line 249
    .end local v5    # "windowX":I
    .end local v6    # "windowY":I
    :cond_0
    and-int/lit8 v7, v0, 0x2

    if-eqz v7, :cond_1

    .line 250
    div-int/lit8 v7, v4, 0x2

    sub-int v5, v1, v7

    .restart local v5    # "windowX":I
    goto :goto_0

    .line 251
    .end local v5    # "windowX":I
    :cond_1
    and-int/lit8 v7, v0, 0x4

    if-eqz v7, :cond_2

    .line 252
    sub-int v5, v1, v4

    .restart local v5    # "windowX":I
    goto :goto_0

    .line 255
    .end local v5    # "windowX":I
    :cond_2
    const/4 v5, 0x0

    .restart local v5    # "windowX":I
    goto :goto_0

    .line 261
    :cond_3
    and-int/lit8 v7, v0, 0x10

    if-eqz v7, :cond_4

    .line 262
    div-int/lit8 v7, v3, 0x2

    sub-int v6, v2, v7

    .restart local v6    # "windowY":I
    goto :goto_1

    .line 263
    .end local v6    # "windowY":I
    :cond_4
    and-int/lit8 v7, v0, 0x20

    if-eqz v7, :cond_5

    .line 264
    sub-int v6, v2, v3

    .restart local v6    # "windowY":I
    goto :goto_1

    .line 267
    .end local v6    # "windowY":I
    :cond_5
    const/4 v6, 0x0

    .restart local v6    # "windowY":I
    goto :goto_1
.end method

.method private measureWindowView(Landroid/view/View;Lcom/google/android/videos/subtitles/SubtitleWindowSettings;II)V
    .locals 7
    .param p1, "windowView"    # Landroid/view/View;
    .param p2, "windowSettings"    # Lcom/google/android/videos/subtitles/SubtitleWindowSettings;
    .param p3, "subtitleAreaWidth"    # I
    .param p4, "subtitleAreaHeight"    # I

    .prologue
    const/high16 v6, -0x80000000

    .line 186
    iget v0, p2, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;->anchorPoint:I

    .line 187
    .local v0, "anchorPoint":I
    iget v5, p2, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;->anchorHorizontalPos:I

    mul-int/2addr v5, p3

    div-int/lit8 v1, v5, 0x64

    .line 188
    .local v1, "anchorX":I
    iget v5, p2, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;->anchorVerticalPos:I

    mul-int/2addr v5, p4

    div-int/lit8 v2, v5, 0x64

    .line 191
    .local v2, "anchorY":I
    and-int/lit8 v5, v0, 0x1

    if-eqz v5, :cond_0

    .line 192
    sub-int v4, p3, v1

    .line 203
    .local v4, "maxWindowWidth":I
    :goto_0
    and-int/lit8 v5, v0, 0x8

    if-eqz v5, :cond_3

    .line 204
    sub-int v3, p4, v2

    .line 214
    .local v3, "maxWindowHeight":I
    :goto_1
    invoke-static {v4, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {p1, v5, v6}, Landroid/view/View;->measure(II)V

    .line 216
    return-void

    .line 193
    .end local v3    # "maxWindowHeight":I
    .end local v4    # "maxWindowWidth":I
    :cond_0
    and-int/lit8 v5, v0, 0x2

    if-eqz v5, :cond_1

    .line 194
    sub-int v5, p3, v1

    invoke-static {v1, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    mul-int/lit8 v4, v5, 0x2

    .restart local v4    # "maxWindowWidth":I
    goto :goto_0

    .line 195
    .end local v4    # "maxWindowWidth":I
    :cond_1
    and-int/lit8 v5, v0, 0x4

    if-eqz v5, :cond_2

    .line 196
    move v4, v1

    .restart local v4    # "maxWindowWidth":I
    goto :goto_0

    .line 199
    .end local v4    # "maxWindowWidth":I
    :cond_2
    const/4 v4, 0x0

    .restart local v4    # "maxWindowWidth":I
    goto :goto_0

    .line 205
    :cond_3
    and-int/lit8 v5, v0, 0x10

    if-eqz v5, :cond_4

    .line 206
    sub-int v5, p4, v2

    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    mul-int/lit8 v3, v5, 0x2

    .restart local v3    # "maxWindowHeight":I
    goto :goto_1

    .line 207
    .end local v3    # "maxWindowHeight":I
    :cond_4
    and-int/lit8 v5, v0, 0x20

    if-eqz v5, :cond_5

    .line 208
    move v3, v2

    .restart local v3    # "maxWindowHeight":I
    goto :goto_1

    .line 211
    .end local v3    # "maxWindowHeight":I
    :cond_5
    const/4 v3, 0x0

    .restart local v3    # "maxWindowHeight":I
    goto :goto_1
.end method

.method private updateSubtitlesTextSize()V
    .locals 5

    .prologue
    .line 154
    iget v2, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->minReadableFontSize:F

    const v3, 0x3d5a511a    # 0.0533f

    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->getHeight()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 158
    .local v1, "textSize":F
    iget v2, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->fontScale:F

    mul-float/2addr v1, v2

    .line 159
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->windowViews:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 160
    iget-object v2, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->windowViews:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer/text/SubtitleView;

    invoke-virtual {v2, v1}, Lcom/google/android/exoplayer/text/SubtitleView;->setTextSize(F)V

    .line 159
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 162
    :cond_0
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->removeAllViews()V

    .line 122
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->windowSnapshots:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 123
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->windowViews:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 124
    return-void
.end method

.method public generateLayoutParams()Lcom/google/android/videos/player/PlayerView$LayoutParams;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 68
    new-instance v0, Lcom/google/android/videos/player/PlayerView$LayoutParams;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, v1}, Lcom/google/android/videos/player/PlayerView$LayoutParams;-><init>(IIZ)V

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 63
    return-object p0
.end method

.method public hide()V
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->setVisibility(I)V

    .line 132
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 9
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 220
    sub-int v5, p4, p2

    .line 221
    .local v5, "width":I
    sub-int v6, p5, p3

    .line 222
    .local v6, "height":I
    mul-int/lit8 v0, v5, 0xf

    div-int/lit8 v0, v0, 0x64

    div-int/lit8 v3, v0, 0x2

    .line 223
    .local v3, "offsetX":I
    mul-int/lit8 v0, v6, 0xf

    div-int/lit8 v0, v0, 0x64

    div-int/lit8 v4, v0, 0x2

    .line 225
    .local v4, "offsetY":I
    mul-int/lit8 v0, v5, 0x55

    div-int/lit8 v5, v0, 0x64

    .line 226
    mul-int/lit8 v0, v6, 0x55

    div-int/lit8 v6, v0, 0x64

    .line 228
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->windowSnapshots:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v7, v0, :cond_1

    .line 229
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->windowViews:Landroid/util/SparseArray;

    iget-object v8, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->windowSnapshots:Landroid/util/SparseArray;

    invoke-virtual {v8, v7}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v8

    invoke-virtual {v0, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 230
    .local v1, "subtitleWindow":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->windowSnapshots:Landroid/util/SparseArray;

    invoke-virtual {v0, v7}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;

    iget-object v2, v0, Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;->settings:Lcom/google/android/videos/subtitles/SubtitleWindowSettings;

    .local v2, "windowSettings":Lcom/google/android/videos/subtitles/SubtitleWindowSettings;
    move-object v0, p0

    .line 232
    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->layoutWindowView(Landroid/view/View;Lcom/google/android/videos/subtitles/SubtitleWindowSettings;IIII)V

    .line 228
    .end local v2    # "windowSettings":Lcom/google/android/videos/subtitles/SubtitleWindowSettings;
    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 235
    .end local v1    # "subtitleWindow":Landroid/view/View;
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 166
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 167
    .local v3, "width":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 168
    .local v0, "height":I
    invoke-virtual {p0, v3, v0}, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->setMeasuredDimension(II)V

    .line 170
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->updateSubtitlesTextSize()V

    .line 172
    mul-int/lit8 v4, v3, 0x55

    div-int/lit8 v3, v4, 0x64

    .line 173
    mul-int/lit8 v4, v0, 0x55

    div-int/lit8 v0, v4, 0x64

    .line 175
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->windowSnapshots:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 176
    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->windowViews:Landroid/util/SparseArray;

    iget-object v5, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->windowSnapshots:Landroid/util/SparseArray;

    invoke-virtual {v5, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 177
    .local v2, "subtitleWindow":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_0

    .line 178
    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->windowSnapshots:Landroid/util/SparseArray;

    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;

    iget-object v4, v4, Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;->settings:Lcom/google/android/videos/subtitles/SubtitleWindowSettings;

    invoke-direct {p0, v2, v4, v3, v0}, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->measureWindowView(Landroid/view/View;Lcom/google/android/videos/subtitles/SubtitleWindowSettings;II)V

    .line 175
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 181
    .end local v2    # "subtitleWindow":Landroid/view/View;
    :cond_1
    return-void
.end method

.method public setCaptionStyle(Lcom/google/android/exoplayer/text/CaptionStyleCompat;)V
    .locals 2
    .param p1, "captionStyle"    # Lcom/google/android/exoplayer/text/CaptionStyleCompat;

    .prologue
    .line 145
    iput-object p1, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->captionStyle:Lcom/google/android/exoplayer/text/CaptionStyleCompat;

    .line 146
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->windowViews:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 147
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->windowViews:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer/text/SubtitleView;

    invoke-virtual {v1, p1}, Lcom/google/android/exoplayer/text/SubtitleView;->setStyle(Lcom/google/android/exoplayer/text/CaptionStyleCompat;)V

    .line 146
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 149
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->requestLayout()V

    .line 150
    return-void
.end method

.method public setFontScale(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 139
    iput p1, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->fontScale:F

    .line 140
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->requestLayout()V

    .line 141
    return-void
.end method

.method public update(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "snapshots":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;>;"
    const/4 v8, 0x0

    .line 77
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 78
    .local v2, "oldWindowIds":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v6, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->windowSnapshots:Landroid/util/SparseArray;

    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I

    move-result v6

    if-ge v0, v6, :cond_0

    .line 79
    iget-object v6, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->windowSnapshots:Landroid/util/SparseArray;

    invoke-virtual {v6, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 78
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 81
    :cond_0
    const/4 v0, 0x0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    if-ge v0, v6, :cond_6

    .line 82
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;

    .line 83
    .local v3, "snapshot":Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;
    iget v6, v3, Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;->windowId:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 84
    iget-object v6, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->windowViews:Landroid/util/SparseArray;

    iget v7, v3, Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;->windowId:I

    invoke-virtual {v6, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/exoplayer/text/SubtitleView;

    .line 85
    .local v4, "subtitleWindow":Lcom/google/android/exoplayer/text/SubtitleView;
    iget-object v6, v3, Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;->text:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    iget-object v6, v3, Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;->settings:Lcom/google/android/videos/subtitles/SubtitleWindowSettings;

    iget-boolean v6, v6, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;->visible:Z

    if-nez v6, :cond_3

    .line 86
    :cond_1
    if-eqz v4, :cond_2

    .line 87
    const/16 v6, 0x8

    invoke-virtual {v4, v6}, Lcom/google/android/exoplayer/text/SubtitleView;->setVisibility(I)V

    .line 81
    :cond_2
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 90
    :cond_3
    iget-object v6, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->windowSnapshots:Landroid/util/SparseArray;

    iget v7, v3, Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;->windowId:I

    invoke-virtual {v6, v7, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 91
    if-nez v4, :cond_4

    .line 92
    iget-object v6, v3, Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;->text:Ljava/lang/String;

    iget-object v7, v3, Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;->text:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v7

    invoke-direct {p0, v6, v7}, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->createSubtitleView(Ljava/lang/Object;Ljava/lang/CharSequence;)Lcom/google/android/exoplayer/text/SubtitleView;

    move-result-object v4

    .line 93
    invoke-virtual {p0, v4}, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->addView(Landroid/view/View;)V

    .line 94
    iget-object v6, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->windowViews:Landroid/util/SparseArray;

    iget v7, v3, Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;->windowId:I

    invoke-virtual {v6, v7, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_2

    .line 98
    :cond_4
    iget-object v6, v3, Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;->text:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/google/android/exoplayer/text/SubtitleView;->getTag()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 99
    iget-object v6, v3, Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;->text:Ljava/lang/String;

    invoke-virtual {v4, v6}, Lcom/google/android/exoplayer/text/SubtitleView;->setTag(Ljava/lang/Object;)V

    .line 100
    iget-object v6, v3, Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;->text:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/google/android/exoplayer/text/SubtitleView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    :cond_5
    invoke-virtual {v4, v8}, Lcom/google/android/exoplayer/text/SubtitleView;->setVisibility(I)V

    goto :goto_2

    .line 108
    .end local v3    # "snapshot":Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;
    .end local v4    # "subtitleWindow":Lcom/google/android/exoplayer/text/SubtitleView;
    :cond_6
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 109
    .local v5, "windowId":I
    iget-object v6, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->windowViews:Landroid/util/SparseArray;

    invoke-virtual {v6, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    invoke-virtual {p0, v6}, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->removeView(Landroid/view/View;)V

    .line 110
    iget-object v6, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->windowSnapshots:Landroid/util/SparseArray;

    invoke-virtual {v6, v5}, Landroid/util/SparseArray;->remove(I)V

    .line 111
    iget-object v6, p0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->windowViews:Landroid/util/SparseArray;

    invoke-virtual {v6, v5}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_3

    .line 113
    .end local v5    # "windowId":I
    :cond_7
    invoke-virtual {p0, v8}, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;->setVisibility(I)V

    .line 114
    return-void
.end method
