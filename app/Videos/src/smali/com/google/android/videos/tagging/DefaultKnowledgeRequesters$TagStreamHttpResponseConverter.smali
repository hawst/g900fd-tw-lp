.class final Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponseConverter;
.super Lcom/google/android/videos/converter/HttpResponseConverter;
.source "DefaultKnowledgeRequesters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "TagStreamHttpResponseConverter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/converter/HttpResponseConverter",
        "<",
        "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 140
    invoke-direct {p0}, Lcom/google/android/videos/converter/HttpResponseConverter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$1;

    .prologue
    .line 140
    invoke-direct {p0}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponseConverter;-><init>()V

    return-void
.end method


# virtual methods
.method public convertResponse(Lorg/apache/http/HttpResponse;)Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;
    .locals 12
    .param p1, "response"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    const/4 v8, 0x0

    .line 147
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v9

    invoke-interface {v9}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v9

    const/16 v10, 0x194

    if-ne v9, v10, :cond_0

    .line 148
    new-instance v9, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;

    const/4 v10, 0x1

    invoke-direct {v9, v10, v8, v8, v8}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;-><init>(ZLjava/lang/String;Ljava/lang/String;[B)V

    move-object v8, v9

    .line 168
    :goto_0
    return-object v8

    .line 151
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponseConverter;->checkHttpError(Lorg/apache/http/HttpResponse;)V

    .line 152
    const-string v9, "Last-Modified"

    invoke-interface {p1, v9}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v3

    .line 153
    .local v3, "header":Lorg/apache/http/Header;
    if-nez v3, :cond_1

    move-object v6, v8

    .line 154
    .local v6, "lastModified":Ljava/lang/String;
    :goto_1
    const-string v9, "Content-Language"

    invoke-interface {p1, v9}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v3

    .line 155
    if-nez v3, :cond_2

    move-object v2, v8

    .line 156
    .local v2, "contentLanguage":Ljava/lang/String;
    :goto_2
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v7

    .line 158
    .local v7, "responseEntity":Lorg/apache/http/HttpEntity;
    if-nez v7, :cond_3

    .line 159
    new-array v1, v11, [B

    .line 168
    .local v1, "content":[B
    :goto_3
    new-instance v8, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;

    invoke-direct {v8, v11, v6, v2, v1}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;-><init>(ZLjava/lang/String;Ljava/lang/String;[B)V

    goto :goto_0

    .line 153
    .end local v1    # "content":[B
    .end local v2    # "contentLanguage":Ljava/lang/String;
    .end local v6    # "lastModified":Ljava/lang/String;
    .end local v7    # "responseEntity":Lorg/apache/http/HttpEntity;
    :cond_1
    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    .line 155
    .restart local v6    # "lastModified":Ljava/lang/String;
    :cond_2
    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 161
    .restart local v2    # "contentLanguage":Ljava/lang/String;
    .restart local v7    # "responseEntity":Lorg/apache/http/HttpEntity;
    :cond_3
    invoke-interface {v7}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v4

    .line 162
    .local v4, "contentLength":J
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const-wide/16 v8, 0x0

    cmp-long v8, v8, v4

    if-gtz v8, :cond_4

    const-wide/32 v8, 0x7fffffff

    cmp-long v8, v4, v8

    if-gez v8, :cond_4

    long-to-int v8, v4

    :goto_4
    invoke-direct {v0, v8}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 165
    .local v0, "buffer":Ljava/io/ByteArrayOutputStream;
    invoke-interface {v7, v0}, Lorg/apache/http/HttpEntity;->writeTo(Ljava/io/OutputStream;)V

    .line 166
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .restart local v1    # "content":[B
    goto :goto_3

    .line 162
    .end local v0    # "buffer":Ljava/io/ByteArrayOutputStream;
    .end local v1    # "content":[B
    :cond_4
    const/16 v8, 0x4000

    goto :goto_4
.end method

.method public bridge synthetic convertResponse(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 140
    check-cast p1, Lorg/apache/http/HttpResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponseConverter;->convertResponse(Lorg/apache/http/HttpResponse;)Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic convertResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 140
    invoke-virtual {p0, p1}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponseConverter;->convertResponse(Lorg/apache/http/HttpResponse;)Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;

    move-result-object v0

    return-object v0
.end method
