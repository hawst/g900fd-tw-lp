.class interface abstract Lcom/google/android/videos/pinning/LegacyLicenseTask$LicenseInfoQuery;
.super Ljava/lang/Object;
.source "LegacyLicenseTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pinning/LegacyLicenseTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "LicenseInfoQuery"
.end annotation


# static fields
.field public static final PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 367
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "license_key_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "license_asset_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "license_system_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "license_file_path_key"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/videos/pinning/LegacyLicenseTask$LicenseInfoQuery;->PROJECTION:[Ljava/lang/String;

    return-void
.end method
