.class public Lcom/google/android/videos/ui/WishlistHelper;
.super Lcom/google/android/videos/store/Database$BaseListener;
.source "WishlistHelper.java"

# interfaces
.implements Lcom/google/android/repolib/observers/Updatable;
.implements Lcom/google/android/videos/activity/HomeFragment$HomeFragmentHelper;
.implements Lcom/google/android/videos/ui/CursorHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/WishlistHelper$1;,
        Lcom/google/android/videos/ui/WishlistHelper$WishlistItemClickListener;
    }
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private final activity:Lcom/google/android/videos/activity/HomeActivity;

.field private final configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

.field private final database:Lcom/google/android/videos/store/Database;

.field private final listView:Landroid/support/v7/widget/RecyclerView;

.field private final networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

.field private final progressBar:Landroid/view/View;

.field private final suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

.field private final syncHelper:Lcom/google/android/videos/ui/SyncHelper;

.field private final uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

.field private final unpurchasedMoviesHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

.field private final unpurchasedShowsHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

.field private final wishlistFlowHelper:Lcom/google/android/videos/ui/WishlistFlowHelper;

.field private final wishlistMoviesCursorHelper:Lcom/google/android/videos/ui/CursorHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/ui/CursorHelper",
            "<*>;"
        }
    .end annotation
.end field

.field private final wishlistMoviesDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;

.field private final wishlistShowsCursorHelper:Lcom/google/android/videos/ui/CursorHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/ui/CursorHelper",
            "<*>;"
        }
    .end annotation
.end field

.field private final wishlistShowsDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/activity/HomeActivity;Landroid/view/View;Lcom/google/android/videos/ui/CursorHelper;Lcom/google/android/videos/ui/CursorHelper;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PosterStore;Lcom/google/android/videos/ui/SyncHelper;Lcom/google/android/videos/api/ApiRequesters;Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/logging/UiElementNode;Lcom/google/android/videos/logging/UiEventLoggingHelper;Lcom/google/android/videos/utils/NetworkStatus;)V
    .locals 10
    .param p1, "activity"    # Lcom/google/android/videos/activity/HomeActivity;
    .param p2, "mainView"    # Landroid/view/View;
    .param p5, "database"    # Lcom/google/android/videos/store/Database;
    .param p6, "posterStore"    # Lcom/google/android/videos/store/PosterStore;
    .param p7, "syncHelper"    # Lcom/google/android/videos/ui/SyncHelper;
    .param p8, "apiRequesters"    # Lcom/google/android/videos/api/ApiRequesters;
    .param p9, "suggestionOverflowMenuHelper"    # Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;
    .param p10, "configurationStore"    # Lcom/google/android/videos/store/ConfigurationStore;
    .param p11, "parentNode"    # Lcom/google/android/videos/logging/UiElementNode;
    .param p12, "uiEventLoggingHelper"    # Lcom/google/android/videos/logging/UiEventLoggingHelper;
    .param p13, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/activity/HomeActivity;",
            "Landroid/view/View;",
            "Lcom/google/android/videos/ui/CursorHelper",
            "<*>;",
            "Lcom/google/android/videos/ui/CursorHelper",
            "<*>;",
            "Lcom/google/android/videos/store/Database;",
            "Lcom/google/android/videos/store/PosterStore;",
            "Lcom/google/android/videos/ui/SyncHelper;",
            "Lcom/google/android/videos/api/ApiRequesters;",
            "Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;",
            "Lcom/google/android/videos/store/ConfigurationStore;",
            "Lcom/google/android/videos/logging/UiElementNode;",
            "Lcom/google/android/videos/logging/UiEventLoggingHelper;",
            "Lcom/google/android/videos/utils/NetworkStatus;",
            ")V"
        }
    .end annotation

    .prologue
    .line 77
    .local p3, "wishlistMoviesCursorHelper":Lcom/google/android/videos/ui/CursorHelper;, "Lcom/google/android/videos/ui/CursorHelper<*>;"
    .local p4, "wishlistShowsCursorHelper":Lcom/google/android/videos/ui/CursorHelper;, "Lcom/google/android/videos/ui/CursorHelper<*>;"
    invoke-direct {p0}, Lcom/google/android/videos/store/Database$BaseListener;-><init>()V

    .line 78
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/activity/HomeActivity;

    iput-object v3, p0, Lcom/google/android/videos/ui/WishlistHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    .line 79
    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/store/Database;

    iput-object v3, p0, Lcom/google/android/videos/ui/WishlistHelper;->database:Lcom/google/android/videos/store/Database;

    .line 80
    invoke-static/range {p7 .. p7}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/ui/SyncHelper;

    iput-object v3, p0, Lcom/google/android/videos/ui/WishlistHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    .line 81
    invoke-static/range {p9 .. p9}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    iput-object v3, p0, Lcom/google/android/videos/ui/WishlistHelper;->suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    .line 82
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/ui/CursorHelper;

    iput-object v3, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistMoviesCursorHelper:Lcom/google/android/videos/ui/CursorHelper;

    .line 83
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/ui/CursorHelper;

    iput-object v3, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistShowsCursorHelper:Lcom/google/android/videos/ui/CursorHelper;

    .line 84
    invoke-static/range {p10 .. p10}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/store/ConfigurationStore;

    iput-object v3, p0, Lcom/google/android/videos/ui/WishlistHelper;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    .line 85
    invoke-static/range {p13 .. p13}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/utils/NetworkStatus;

    iput-object v3, p0, Lcom/google/android/videos/ui/WishlistHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 86
    invoke-static/range {p12 .. p12}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/logging/UiEventLoggingHelper;

    iput-object v3, p0, Lcom/google/android/videos/ui/WishlistHelper;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    .line 88
    const v3, 0x7f0f0120

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/ui/WishlistHelper;->progressBar:Landroid/view/View;

    .line 90
    const v3, 0x7f0f0029

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v7/widget/RecyclerView;

    iput-object v3, p0, Lcom/google/android/videos/ui/WishlistHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    .line 91
    iget-object v3, p0, Lcom/google/android/videos/ui/WishlistHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    new-instance v4, Lcom/google/android/videos/ui/ShuffleAddItemAnimator;

    invoke-direct {v4}, Lcom/google/android/videos/ui/ShuffleAddItemAnimator;-><init>()V

    invoke-virtual {v3, v4}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(Landroid/support/v7/widget/RecyclerView$ItemAnimator;)V

    .line 93
    new-instance v3, Lcom/google/android/videos/adapter/WishlistDataSource;

    invoke-direct {v3}, Lcom/google/android/videos/adapter/WishlistDataSource;-><init>()V

    iput-object v3, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistMoviesDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;

    .line 94
    iget-object v3, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistMoviesDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;

    invoke-interface/range {p8 .. p8}, Lcom/google/android/videos/api/ApiRequesters;->getAssetsCachingRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v4

    invoke-static {p1, v3, v4}, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->createForMovies(Landroid/app/Activity;Lcom/google/android/videos/adapter/DataSource;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/ui/WishlistHelper;->unpurchasedMoviesHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    .line 99
    new-instance v3, Lcom/google/android/videos/adapter/WishlistDataSource;

    invoke-direct {v3}, Lcom/google/android/videos/adapter/WishlistDataSource;-><init>()V

    iput-object v3, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistShowsDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;

    .line 100
    iget-object v3, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistShowsDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;

    invoke-interface/range {p8 .. p8}, Lcom/google/android/videos/api/ApiRequesters;->getAssetsCachingRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v4

    invoke-static {p1, v3, v4}, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->createForShows(Landroid/app/Activity;Lcom/google/android/videos/adapter/DataSource;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/ui/WishlistHelper;->unpurchasedShowsHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    .line 106
    new-instance v2, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView$Binder;

    const/4 v3, 0x0

    move-object/from16 v0, p6

    invoke-virtual {v0, v3}, Lcom/google/android/videos/store/PosterStore;->getRequester(I)Lcom/google/android/videos/async/Requester;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/videos/ui/WishlistHelper;->unpurchasedMoviesHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    const/4 v5, 0x1

    move-object/from16 v0, p6

    invoke-virtual {v0, v5}, Lcom/google/android/videos/store/PosterStore;->getRequester(I)Lcom/google/android/videos/async/Requester;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/videos/ui/WishlistHelper;->unpurchasedShowsHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    new-instance v7, Lcom/google/android/videos/logging/GenericUiElementNode;

    const/16 v8, 0x9

    move-object/from16 v0, p11

    move-object/from16 v1, p12

    invoke-direct {v7, v8, v0, v1}, Lcom/google/android/videos/logging/GenericUiElementNode;-><init>(ILcom/google/android/videos/logging/UiElementNode;Lcom/google/android/videos/logging/UiEventLoggingHelper;)V

    invoke-direct/range {v2 .. v7}, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView$Binder;-><init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;Lcom/google/android/videos/logging/UiElementNode;)V

    .line 114
    .local v2, "wishlistItemBinder":Lcom/google/android/videos/ui/playnext/WishlistClusterItemView$Binder;
    new-instance v3, Lcom/google/android/videos/ui/WishlistFlowHelper;

    iget-object v5, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistMoviesDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;

    iget-object v6, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistShowsDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;

    new-instance v7, Lcom/google/android/videos/ui/WishlistHelper$WishlistItemClickListener;

    const/4 v4, 0x0

    invoke-direct {v7, p0, v4}, Lcom/google/android/videos/ui/WishlistHelper$WishlistItemClickListener;-><init>(Lcom/google/android/videos/ui/WishlistHelper;Lcom/google/android/videos/ui/WishlistHelper$1;)V

    move-object v4, p1

    move-object v8, v2

    invoke-direct/range {v3 .. v8}, Lcom/google/android/videos/ui/WishlistFlowHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/adapter/WishlistDataSource;Lcom/google/android/videos/adapter/WishlistDataSource;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;Lcom/google/android/videos/ui/playnext/WishlistClusterItemView$Binder;)V

    iput-object v3, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistFlowHelper:Lcom/google/android/videos/ui/WishlistFlowHelper;

    .line 120
    iget-object v3, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistFlowHelper:Lcom/google/android/videos/ui/WishlistFlowHelper;

    invoke-virtual {v3}, Lcom/google/android/videos/ui/WishlistFlowHelper;->getFlow()Lcom/google/android/videos/flow/Flow;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/android/videos/flow/Flow;->setVisible(Z)V

    .line 122
    new-instance v9, Lcom/google/android/videos/flow/FlowAdapter;

    iget-object v3, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistFlowHelper:Lcom/google/android/videos/ui/WishlistFlowHelper;

    invoke-virtual {v3}, Lcom/google/android/videos/ui/WishlistFlowHelper;->getFlow()Lcom/google/android/videos/flow/Flow;

    move-result-object v3

    invoke-direct {v9, v3}, Lcom/google/android/videos/flow/FlowAdapter;-><init>(Lcom/google/android/videos/flow/Flow;)V

    .line 123
    .local v9, "flowAdapter":Lcom/google/android/videos/flow/FlowAdapter;
    const/4 v3, 0x1

    invoke-virtual {v9, v3}, Lcom/google/android/videos/flow/FlowAdapter;->setHasStableIds(Z)V

    .line 124
    iget-object v3, p0, Lcom/google/android/videos/ui/WishlistHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    new-instance v4, Lcom/google/android/videos/ui/DebugFlowLayoutManager;

    const-string v5, "WishlistFragment"

    invoke-direct {v4, v5}, Lcom/google/android/videos/ui/DebugFlowLayoutManager;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 125
    iget-object v3, p0, Lcom/google/android/videos/ui/WishlistHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3, v9}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 126
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/videos/ui/WishlistHelper;)Lcom/google/android/videos/adapter/WishlistDataSource;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/WishlistHelper;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistMoviesDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/ui/WishlistHelper;)Lcom/google/android/videos/adapter/WishlistDataSource;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/WishlistHelper;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistShowsDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/ui/WishlistHelper;)Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/WishlistHelper;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->unpurchasedMoviesHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/videos/ui/WishlistHelper;)Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/WishlistHelper;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/videos/ui/WishlistHelper;)Lcom/google/android/videos/logging/UiEventLoggingHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/WishlistHelper;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/videos/ui/WishlistHelper;)Lcom/google/android/videos/activity/HomeActivity;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/WishlistHelper;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/videos/ui/WishlistHelper;)Lcom/google/android/videos/ui/SyncHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/WishlistHelper;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    return-object v0
.end method

.method private maybeInitUnpurchasedAssetsHelpers()V
    .locals 3

    .prologue
    .line 166
    iget-object v2, p0, Lcom/google/android/videos/ui/WishlistHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v0

    .line 167
    .local v0, "newAccount":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/videos/ui/WishlistHelper;->account:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 174
    :goto_0
    return-void

    .line 170
    :cond_0
    iput-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->account:Ljava/lang/String;

    .line 171
    iget-object v2, p0, Lcom/google/android/videos/ui/WishlistHelper;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    invoke-virtual {v2, v0}, Lcom/google/android/videos/store/ConfigurationStore;->getPlayCountry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 172
    .local v1, "playCountry":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/videos/ui/WishlistHelper;->unpurchasedMoviesHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->init(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    iget-object v2, p0, Lcom/google/android/videos/ui/WishlistHelper;->unpurchasedShowsHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->init(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private refreshMoviesCursor()V
    .locals 3

    .prologue
    .line 186
    iget-object v2, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistMoviesCursorHelper:Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/CursorHelper;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 187
    .local v0, "cursor":Landroid/database/Cursor;
    iget-object v2, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistMoviesDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;

    invoke-virtual {v2, v0}, Lcom/google/android/videos/adapter/WishlistDataSource;->changeCursor(Landroid/database/Cursor;)V

    .line 188
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 189
    .local v1, "movieIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistMoviesDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;

    invoke-virtual {v2, v1}, Lcom/google/android/videos/adapter/WishlistDataSource;->getAllWishlistedMovies(Ljava/util/List;)V

    .line 190
    iget-object v2, p0, Lcom/google/android/videos/ui/WishlistHelper;->unpurchasedMoviesHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    invoke-virtual {v2, v1}, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->processUnpurchasedItems(Ljava/util/List;)V

    .line 191
    invoke-direct {p0}, Lcom/google/android/videos/ui/WishlistHelper;->updateVisibilities()V

    .line 192
    return-void
.end method

.method private refreshShowsCursor()V
    .locals 3

    .prologue
    .line 177
    iget-object v2, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistShowsCursorHelper:Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/CursorHelper;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 178
    .local v0, "cursor":Landroid/database/Cursor;
    iget-object v2, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistShowsDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;

    invoke-virtual {v2, v0}, Lcom/google/android/videos/adapter/WishlistDataSource;->changeCursor(Landroid/database/Cursor;)V

    .line 179
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 180
    .local v1, "showIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistShowsDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;

    invoke-virtual {v2, v1}, Lcom/google/android/videos/adapter/WishlistDataSource;->getAllWishlistedShows(Ljava/util/List;)V

    .line 181
    iget-object v2, p0, Lcom/google/android/videos/ui/WishlistHelper;->unpurchasedShowsHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    invoke-virtual {v2, v1}, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->processUnpurchasedItems(Ljava/util/List;)V

    .line 182
    invoke-direct {p0}, Lcom/google/android/videos/ui/WishlistHelper;->updateVisibilities()V

    .line 183
    return-void
.end method

.method private updateVisibilities()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/16 v5, 0x8

    const/4 v2, 0x0

    .line 195
    iget-object v4, p0, Lcom/google/android/videos/ui/WishlistHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v4}, Lcom/google/android/videos/ui/SyncHelper;->getState()I

    move-result v1

    .line 196
    .local v1, "syncState":I
    iget-object v4, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistMoviesDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;

    invoke-virtual {v4}, Lcom/google/android/videos/adapter/WishlistDataSource;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistShowsDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;

    invoke-virtual {v4}, Lcom/google/android/videos/adapter/WishlistDataSource;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    move v0, v3

    .line 197
    .local v0, "hasContent":Z
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 238
    :goto_1
    return-void

    .end local v0    # "hasContent":Z
    :cond_1
    move v0, v2

    .line 196
    goto :goto_0

    .line 201
    .restart local v0    # "hasContent":Z
    :pswitch_0
    iget-object v2, p0, Lcom/google/android/videos/ui/WishlistHelper;->progressBar:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 202
    iget-object v2, p0, Lcom/google/android/videos/ui/WishlistHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v5}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 203
    iget-object v2, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistFlowHelper:Lcom/google/android/videos/ui/WishlistFlowHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/WishlistFlowHelper;->hideNoWishlistedMovies()V

    goto :goto_1

    .line 212
    :pswitch_1
    iget-object v4, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistMoviesDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;

    invoke-virtual {v4}, Lcom/google/android/videos/adapter/WishlistDataSource;->hasCursor()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistShowsDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;

    invoke-virtual {v4}, Lcom/google/android/videos/adapter/WishlistDataSource;->hasCursor()Z

    move-result v4

    if-nez v4, :cond_3

    .line 213
    :cond_2
    iget-object v3, p0, Lcom/google/android/videos/ui/WishlistHelper;->progressBar:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 214
    iget-object v2, p0, Lcom/google/android/videos/ui/WishlistHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v5}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 215
    iget-object v2, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistFlowHelper:Lcom/google/android/videos/ui/WishlistFlowHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/WishlistFlowHelper;->hideNoWishlistedMovies()V

    goto :goto_1

    .line 216
    :cond_3
    if-nez v0, :cond_4

    .line 217
    iget-object v4, p0, Lcom/google/android/videos/ui/WishlistHelper;->progressBar:Landroid/view/View;

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 218
    iget-object v4, p0, Lcom/google/android/videos/ui/WishlistHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v4, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 219
    iget-object v2, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistFlowHelper:Lcom/google/android/videos/ui/WishlistFlowHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/WishlistFlowHelper;->getFlow()Lcom/google/android/videos/flow/Flow;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/google/android/videos/flow/Flow;->setVisible(Z)V

    .line 220
    iget-object v2, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistFlowHelper:Lcom/google/android/videos/ui/WishlistFlowHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/WishlistFlowHelper;->showNoWishlistedMovies()V

    goto :goto_1

    .line 222
    :cond_4
    iget-object v3, p0, Lcom/google/android/videos/ui/WishlistHelper;->progressBar:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 223
    iget-object v3, p0, Lcom/google/android/videos/ui/WishlistHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistFlowHelper:Lcom/google/android/videos/ui/WishlistFlowHelper;

    invoke-virtual {v4}, Lcom/google/android/videos/ui/WishlistFlowHelper;->getFlow()Lcom/google/android/videos/flow/Flow;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/videos/ui/FlowAnimationUtil;->animateFlowAppearing(Landroid/support/v7/widget/RecyclerView;Lcom/google/android/videos/flow/Flow;)V

    .line 224
    iget-object v3, p0, Lcom/google/android/videos/ui/WishlistHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 225
    iget-object v3, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistFlowHelper:Lcom/google/android/videos/ui/WishlistFlowHelper;

    invoke-virtual {v3}, Lcom/google/android/videos/ui/WishlistFlowHelper;->hideNoWishlistedMovies()V

    .line 226
    iget-object v3, p0, Lcom/google/android/videos/ui/WishlistHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    invoke-virtual {v3}, Lcom/google/android/videos/activity/HomeActivity;->takeWishlistSectionToSelect()I

    move-result v3

    packed-switch v3, :pswitch_data_1

    goto :goto_1

    .line 228
    :pswitch_2
    iget-object v3, p0, Lcom/google/android/videos/ui/WishlistHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3, v2}, Landroid/support/v7/widget/RecyclerView;->scrollToPosition(I)V

    goto :goto_1

    .line 231
    :pswitch_3
    iget-object v2, p0, Lcom/google/android/videos/ui/WishlistHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistFlowHelper:Lcom/google/android/videos/ui/WishlistFlowHelper;

    invoke-virtual {v3}, Lcom/google/android/videos/ui/WishlistFlowHelper;->getShowsHeadingPosition()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->scrollToPosition(I)V

    goto/16 :goto_1

    .line 197
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 226
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public onCursorChanged(Lcom/google/android/videos/ui/CursorHelper;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/ui/CursorHelper",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 249
    .local p1, "source":Lcom/google/android/videos/ui/CursorHelper;, "Lcom/google/android/videos/ui/CursorHelper<*>;"
    invoke-direct {p0}, Lcom/google/android/videos/ui/WishlistHelper;->maybeInitUnpurchasedAssetsHelpers()V

    .line 250
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistMoviesCursorHelper:Lcom/google/android/videos/ui/CursorHelper;

    if-ne p1, v0, :cond_0

    .line 251
    invoke-direct {p0}, Lcom/google/android/videos/ui/WishlistHelper;->refreshMoviesCursor()V

    .line 257
    :goto_0
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistFlowHelper:Lcom/google/android/videos/ui/WishlistFlowHelper;

    iget-object v1, p0, Lcom/google/android/videos/ui/WishlistHelper;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    iget-object v2, p0, Lcom/google/android/videos/ui/WishlistHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/videos/store/ConfigurationStore;->showsVerticalEnabled(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/WishlistFlowHelper;->updateNoWishlistedMoviesText(Z)V

    .line 259
    return-void

    .line 253
    :cond_0
    invoke-direct {p0}, Lcom/google/android/videos/ui/WishlistHelper;->refreshShowsCursor()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistMoviesCursorHelper:Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/CursorHelper;->onDestroy()V

    .line 162
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistShowsCursorHelper:Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/CursorHelper;->onDestroy()V

    .line 163
    return-void
.end method

.method public onPosterUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 265
    invoke-virtual {p0}, Lcom/google/android/videos/ui/WishlistHelper;->onPostersUpdated()V

    .line 266
    return-void
.end method

.method public onPostersUpdated()V
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistMoviesDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;

    invoke-virtual {v0}, Lcom/google/android/videos/adapter/WishlistDataSource;->notifyChanged()V

    .line 275
    return-void
.end method

.method public onShowPosterUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 270
    invoke-virtual {p0}, Lcom/google/android/videos/ui/WishlistHelper;->onShowPostersUpdated()V

    .line 271
    return-void
.end method

.method public onShowPostersUpdated()V
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistShowsDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;

    invoke-virtual {v0}, Lcom/google/android/videos/adapter/WishlistDataSource;->notifyChanged()V

    .line 279
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/store/Database;->addListener(Lcom/google/android/videos/store/Database$Listener;)V

    .line 131
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistMoviesCursorHelper:Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/CursorHelper;->addListener(Lcom/google/android/videos/ui/CursorHelper$Listener;)V

    .line 132
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistMoviesCursorHelper:Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/CursorHelper;->onStart()V

    .line 133
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistShowsCursorHelper:Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/CursorHelper;->addListener(Lcom/google/android/videos/ui/CursorHelper$Listener;)V

    .line 134
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistShowsCursorHelper:Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/CursorHelper;->onStart()V

    .line 135
    invoke-direct {p0}, Lcom/google/android/videos/ui/WishlistHelper;->maybeInitUnpurchasedAssetsHelpers()V

    .line 136
    invoke-direct {p0}, Lcom/google/android/videos/ui/WishlistHelper;->refreshShowsCursor()V

    .line 137
    invoke-direct {p0}, Lcom/google/android/videos/ui/WishlistHelper;->refreshMoviesCursor()V

    .line 138
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0, p0}, Lcom/google/android/videos/utils/NetworkStatus;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 139
    invoke-virtual {p0}, Lcom/google/android/videos/ui/WishlistHelper;->update()V

    .line 140
    return-void
.end method

.method public onStop()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 144
    iput-object v1, p0, Lcom/google/android/videos/ui/WishlistHelper;->account:Ljava/lang/String;

    .line 145
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->unpurchasedMoviesHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->reset()V

    .line 146
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->unpurchasedShowsHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->reset()V

    .line 147
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistMoviesDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/WishlistDataSource;->changeCursor(Landroid/database/Cursor;)V

    .line 148
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistShowsDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/WishlistDataSource;->changeCursor(Landroid/database/Cursor;)V

    .line 149
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 150
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->progressBar:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 151
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistMoviesCursorHelper:Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/CursorHelper;->removeListener(Lcom/google/android/videos/ui/CursorHelper$Listener;)V

    .line 152
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistMoviesCursorHelper:Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/CursorHelper;->onStop()V

    .line 153
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistShowsCursorHelper:Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/CursorHelper;->removeListener(Lcom/google/android/videos/ui/CursorHelper$Listener;)V

    .line 154
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistShowsCursorHelper:Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/CursorHelper;->onStop()V

    .line 155
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/store/Database;->removeListener(Lcom/google/android/videos/store/Database$Listener;)Z

    .line 156
    iget-object v0, p0, Lcom/google/android/videos/ui/WishlistHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0, p0}, Lcom/google/android/videos/utils/NetworkStatus;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 157
    return-void
.end method

.method public onTransitioningChanged(Z)V
    .locals 0
    .param p1, "isTransitioning"    # Z

    .prologue
    .line 243
    return-void
.end method

.method public update()V
    .locals 2

    .prologue
    .line 283
    iget-object v1, p0, Lcom/google/android/videos/ui/WishlistHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v1}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v0

    .line 284
    .local v0, "isConnected":Z
    iget-object v1, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistMoviesDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/adapter/WishlistDataSource;->setNetworkConnected(Z)V

    .line 285
    iget-object v1, p0, Lcom/google/android/videos/ui/WishlistHelper;->wishlistShowsDataSource:Lcom/google/android/videos/adapter/WishlistDataSource;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/adapter/WishlistDataSource;->setNetworkConnected(Z)V

    .line 286
    return-void
.end method
