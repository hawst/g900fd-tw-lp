.class public Lcom/google/android/videos/pinning/Downloader$SetupResult;
.super Ljava/lang/Object;
.source "Downloader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pinning/Downloader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "SetupResult"
.end annotation


# instance fields
.field public final dashStreamTimestampForKnowledge:J

.field public final mediaStream:Lcom/google/android/videos/streams/MediaStream;

.field public final storyboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

.field public final subtitleTracks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/videos/streams/MediaStream;JLjava/util/List;Lcom/google/wireless/android/video/magma/proto/Storyboard;)V
    .locals 0
    .param p1, "mediaStream"    # Lcom/google/android/videos/streams/MediaStream;
    .param p2, "dashStreamTimestampForKnowledge"    # J
    .param p5, "storyboard"    # Lcom/google/wireless/android/video/magma/proto/Storyboard;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/streams/MediaStream;",
            "J",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;",
            "Lcom/google/wireless/android/video/magma/proto/Storyboard;",
            ")V"
        }
    .end annotation

    .prologue
    .line 68
    .local p4, "subtitleTracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/subtitles/SubtitleTrack;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/google/android/videos/pinning/Downloader$SetupResult;->mediaStream:Lcom/google/android/videos/streams/MediaStream;

    .line 70
    iput-wide p2, p0, Lcom/google/android/videos/pinning/Downloader$SetupResult;->dashStreamTimestampForKnowledge:J

    .line 71
    iput-object p4, p0, Lcom/google/android/videos/pinning/Downloader$SetupResult;->subtitleTracks:Ljava/util/List;

    .line 72
    iput-object p5, p0, Lcom/google/android/videos/pinning/Downloader$SetupResult;->storyboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

    .line 73
    return-void
.end method
