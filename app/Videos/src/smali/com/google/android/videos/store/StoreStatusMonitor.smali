.class public Lcom/google/android/videos/store/StoreStatusMonitor;
.super Lcom/google/android/videos/store/Database$BaseListener;
.source "StoreStatusMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/store/StoreStatusMonitor$PurchasesQuery;,
        Lcom/google/android/videos/store/StoreStatusMonitor$Listener;
    }
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private currentPurchasesRequest:Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

.field private currentWishlistRequest:Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

.field private final database:Lcom/google/android/videos/store/Database;

.field private final listeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/videos/store/StoreStatusMonitor$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private pendingPurchasesUpdate:Z

.field private pendingWishlistUpdate:Z

.field private final purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

.field private final purchasesCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final statuses:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final wishlistCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/store/WishlistStore$WishlistRequest;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final wishlistStore:Lcom/google/android/videos/store/WishlistStore;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/WishlistStore;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "database"    # Lcom/google/android/videos/store/Database;
    .param p3, "purchaseStore"    # Lcom/google/android/videos/store/PurchaseStore;
    .param p4, "wishlistStore"    # Lcom/google/android/videos/store/WishlistStore;

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/google/android/videos/store/Database$BaseListener;-><init>()V

    .line 78
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/store/Database;

    iput-object v1, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->database:Lcom/google/android/videos/store/Database;

    .line 79
    iput-object p3, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    .line 80
    iput-object p4, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->wishlistStore:Lcom/google/android/videos/store/WishlistStore;

    .line 81
    new-instance v1, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v1, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->listeners:Ljava/util/Set;

    .line 82
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->statuses:Ljava/util/Map;

    .line 84
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 85
    .local v0, "handler":Landroid/os/Handler;
    new-instance v1, Lcom/google/android/videos/store/StoreStatusMonitor$1;

    invoke-direct {v1, p0}, Lcom/google/android/videos/store/StoreStatusMonitor$1;-><init>(Lcom/google/android/videos/store/StoreStatusMonitor;)V

    invoke-static {v0, v1}, Lcom/google/android/videos/async/HandlerCallback;->create(Landroid/os/Handler;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/HandlerCallback;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->purchasesCallback:Lcom/google/android/videos/async/Callback;

    .line 96
    new-instance v1, Lcom/google/android/videos/store/StoreStatusMonitor$2;

    invoke-direct {v1, p0}, Lcom/google/android/videos/store/StoreStatusMonitor$2;-><init>(Lcom/google/android/videos/store/StoreStatusMonitor;)V

    invoke-static {v0, v1}, Lcom/google/android/videos/async/HandlerCallback;->create(Landroid/os/Handler;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/HandlerCallback;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->wishlistCallback:Lcom/google/android/videos/async/Callback;

    .line 107
    return-void
.end method

.method public static isPurchased(I)Z
    .locals 1
    .param p0, "status"    # I

    .prologue
    .line 59
    and-int/lit8 v0, p0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isWishlisted(I)Z
    .locals 1
    .param p0, "status"    # I

    .prologue
    .line 55
    and-int/lit8 v0, p0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private mergeQueryResult(Ljava/util/Set;I)V
    .locals 10
    .param p2, "theStatus"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;>;I)V"
        }
    .end annotation

    .prologue
    .line 236
    .local p1, "itemsWithStatus":Ljava/util/Set;, "Ljava/util/Set<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    iget-object v8, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->statuses:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 238
    .local v7, "statusesIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;Ljava/lang/Integer;>;>;"
    const/4 v3, 0x0

    .line 239
    .local v3, "hasChanges":Z
    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 240
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 241
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;Ljava/lang/Integer;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 242
    .local v0, "currentItem":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 243
    .local v1, "currentStatus":Ljava/lang/Integer;
    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 244
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v8

    and-int/2addr v8, p2

    if-eq v8, p2, :cond_0

    .line 245
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v8

    or-int/2addr v8, p2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v2, v8}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    const/4 v3, 0x1

    goto :goto_0

    .line 248
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v8

    if-ne v8, p2, :cond_2

    .line 250
    invoke-interface {v7}, Ljava/util/Iterator;->remove()V

    .line 251
    const/4 v3, 0x1

    goto :goto_0

    .line 252
    :cond_2
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v8

    and-int/2addr v8, p2

    if-eqz v8, :cond_0

    .line 254
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v8

    xor-int/2addr v8, p2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v2, v8}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    const/4 v3, 0x1

    goto :goto_0

    .line 258
    .end local v0    # "currentItem":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v1    # "currentStatus":Ljava/lang/Integer;
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;Ljava/lang/Integer;>;"
    :cond_3
    iget-object v8, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->statuses:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {p1, v8}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 259
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_5

    .line 260
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/util/Pair;

    .line 261
    .local v6, "newItem":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v8, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->statuses:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v6, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 263
    .end local v6    # "newItem":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_4
    const/4 v3, 0x1

    .line 265
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_5
    if-eqz v3, :cond_6

    .line 266
    iget-object v8, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->listeners:Ljava/util/Set;

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .restart local v4    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/videos/store/StoreStatusMonitor$Listener;

    .line 267
    .local v5, "listener":Lcom/google/android/videos/store/StoreStatusMonitor$Listener;
    invoke-interface {v5, p0}, Lcom/google/android/videos/store/StoreStatusMonitor$Listener;->onStoreStatusChanged(Lcom/google/android/videos/store/StoreStatusMonitor;)V

    goto :goto_2

    .line 270
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "listener":Lcom/google/android/videos/store/StoreStatusMonitor$Listener;
    :cond_6
    return-void
.end method

.method private updatePurchases()V
    .locals 4

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->account:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    if-nez v0, :cond_1

    .line 159
    :cond_0
    :goto_0
    return-void

    .line 152
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->currentPurchasesRequest:Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    if-eqz v0, :cond_2

    .line 153
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->pendingPurchasesUpdate:Z

    goto :goto_0

    .line 156
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->account:Ljava/lang/String;

    sget-object v1, Lcom/google/android/videos/store/StoreStatusMonitor$PurchasesQuery;->COLUMNS:[Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/videos/store/PurchaseRequests;->createActivePurchasesRequestForUser(Ljava/lang/String;[Ljava/lang/String;J)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->currentPurchasesRequest:Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .line 158
    iget-object v0, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    iget-object v1, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->currentPurchasesRequest:Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    iget-object v2, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->purchasesCallback:Lcom/google/android/videos/async/Callback;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/videos/async/Callback;)V

    goto :goto_0
.end method

.method private updateWishlist()V
    .locals 3

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->account:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->wishlistStore:Lcom/google/android/videos/store/WishlistStore;

    if-nez v0, :cond_1

    .line 170
    :cond_0
    :goto_0
    return-void

    .line 164
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->currentWishlistRequest:Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    if-eqz v0, :cond_2

    .line 165
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->pendingWishlistUpdate:Z

    goto :goto_0

    .line 168
    :cond_2
    new-instance v0, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    iget-object v1, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->account:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->currentWishlistRequest:Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    .line 169
    iget-object v0, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->wishlistStore:Lcom/google/android/videos/store/WishlistStore;

    iget-object v1, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->currentWishlistRequest:Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    iget-object v2, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->wishlistCallback:Lcom/google/android/videos/async/Callback;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/store/WishlistStore;->loadWishlist(Lcom/google/android/videos/store/WishlistStore$WishlistRequest;Lcom/google/android/videos/async/Callback;)V

    goto :goto_0
.end method


# virtual methods
.method public addListener(Lcom/google/android/videos/store/StoreStatusMonitor$Listener;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/videos/store/StoreStatusMonitor$Listener;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->listeners:Ljava/util/Set;

    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 132
    return-void
.end method

.method public getStatus(Ljava/lang/String;I)I
    .locals 3
    .param p1, "itemId"    # Ljava/lang/String;
    .param p2, "itemType"    # I

    .prologue
    .line 145
    iget-object v1, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->statuses:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p1, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 146
    .local v0, "status":Ljava/lang/Integer;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method public init(Ljava/lang/String;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 110
    invoke-virtual {p0}, Lcom/google/android/videos/store/StoreStatusMonitor;->reset()V

    .line 111
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->account:Ljava/lang/String;

    .line 112
    iget-object v0, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/store/Database;->addListener(Lcom/google/android/videos/store/Database$Listener;)V

    .line 113
    invoke-direct {p0}, Lcom/google/android/videos/store/StoreStatusMonitor;->updatePurchases()V

    .line 114
    invoke-direct {p0}, Lcom/google/android/videos/store/StoreStatusMonitor;->updateWishlist()V

    .line 115
    return-void
.end method

.method public onMovieMetadataUpdated(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 288
    .local p1, "movieIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/google/android/videos/store/StoreStatusMonitor;->updatePurchases()V

    .line 289
    return-void
.end method

.method public onPurchasesUpdated(Ljava/lang/String;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 274
    iget-object v0, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->account:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 275
    invoke-direct {p0}, Lcom/google/android/videos/store/StoreStatusMonitor;->updatePurchases()V

    .line 277
    :cond_0
    return-void
.end method

.method public onShowMetadataUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1, "showId"    # Ljava/lang/String;

    .prologue
    .line 293
    invoke-direct {p0}, Lcom/google/android/videos/store/StoreStatusMonitor;->updatePurchases()V

    .line 294
    return-void
.end method

.method public onWishlistUpdated(Ljava/lang/String;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 281
    iget-object v0, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->account:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 282
    invoke-direct {p0}, Lcom/google/android/videos/store/StoreStatusMonitor;->updateWishlist()V

    .line 284
    :cond_0
    return-void
.end method

.method processPurchasesQueryResult(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V
    .locals 6
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v5, 0x0

    .line 174
    :try_start_0
    iget-object v4, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->currentPurchasesRequest:Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq p1, v4, :cond_1

    .line 197
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    .line 204
    :cond_0
    :goto_0
    return-void

    .line 177
    :cond_1
    const/4 v4, 0x0

    :try_start_1
    iput-object v4, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->currentPurchasesRequest:Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .line 179
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 180
    .local v0, "purchasedItems":Ljava/util/Set;, "Ljava/util/Set<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    :cond_2
    :goto_1
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 181
    const/4 v4, 0x0

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 182
    .local v3, "videoId":Ljava/lang/String;
    const/4 v4, 0x1

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 183
    .local v1, "seasonId":Ljava/lang/String;
    const/4 v4, 0x2

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 184
    .local v2, "showId":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 185
    const/4 v4, 0x6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 197
    .end local v0    # "purchasedItems":Ljava/util/Set;, "Ljava/util/Set<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    .end local v1    # "seasonId":Ljava/lang/String;
    .end local v2    # "showId":Ljava/lang/String;
    .end local v3    # "videoId":Ljava/lang/String;
    :catchall_0
    move-exception v4

    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    throw v4

    .line 187
    .restart local v0    # "purchasedItems":Ljava/util/Set;, "Ljava/util/Set<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    .restart local v1    # "seasonId":Ljava/lang/String;
    .restart local v2    # "showId":Ljava/lang/String;
    .restart local v3    # "videoId":Ljava/lang/String;
    :cond_3
    const/16 v4, 0x14

    :try_start_2
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 188
    const/16 v4, 0x13

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 190
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 191
    const/16 v4, 0x12

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 195
    .end local v1    # "seasonId":Ljava/lang/String;
    .end local v2    # "showId":Ljava/lang/String;
    .end local v3    # "videoId":Ljava/lang/String;
    :cond_4
    const/4 v4, 0x2

    invoke-direct {p0, v0, v4}, Lcom/google/android/videos/store/StoreStatusMonitor;->mergeQueryResult(Ljava/util/Set;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 197
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    .line 200
    iget-boolean v4, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->pendingPurchasesUpdate:Z

    if-eqz v4, :cond_0

    .line 201
    iput-boolean v5, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->pendingPurchasesUpdate:Z

    .line 202
    invoke-direct {p0}, Lcom/google/android/videos/store/StoreStatusMonitor;->updatePurchases()V

    goto :goto_0
.end method

.method processWishlistQueryResult(Lcom/google/android/videos/store/WishlistStore$WishlistRequest;Landroid/database/Cursor;)V
    .locals 5
    .param p1, "request"    # Lcom/google/android/videos/store/WishlistStore$WishlistRequest;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v4, 0x0

    .line 208
    :try_start_0
    iget-object v3, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->currentWishlistRequest:Lcom/google/android/videos/store/WishlistStore$WishlistRequest;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq p1, v3, :cond_1

    .line 221
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    .line 228
    :cond_0
    :goto_0
    return-void

    .line 211
    :cond_1
    const/4 v3, 0x0

    :try_start_1
    iput-object v3, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->currentWishlistRequest:Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    .line 213
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 214
    .local v2, "wishlistedItems":Ljava/util/Set;, "Ljava/util/Set<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    :goto_1
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 215
    const/4 v3, 0x0

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 216
    .local v0, "itemId":Ljava/lang/String;
    const/4 v3, 0x1

    invoke-static {p2, v3}, Lcom/google/android/videos/utils/DbUtils;->getIntOrNull(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v1

    .line 217
    .local v1, "itemType":Ljava/lang/Integer;
    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 221
    .end local v0    # "itemId":Ljava/lang/String;
    .end local v1    # "itemType":Ljava/lang/Integer;
    .end local v2    # "wishlistedItems":Ljava/util/Set;, "Ljava/util/Set<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    :catchall_0
    move-exception v3

    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    throw v3

    .line 219
    .restart local v2    # "wishlistedItems":Ljava/util/Set;, "Ljava/util/Set<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    :cond_2
    const/4 v3, 0x1

    :try_start_2
    invoke-direct {p0, v2, v3}, Lcom/google/android/videos/store/StoreStatusMonitor;->mergeQueryResult(Ljava/util/Set;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 221
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    .line 224
    iget-boolean v3, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->pendingWishlistUpdate:Z

    if-eqz v3, :cond_0

    .line 225
    iput-boolean v4, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->pendingWishlistUpdate:Z

    .line 226
    invoke-direct {p0}, Lcom/google/android/videos/store/StoreStatusMonitor;->updateWishlist()V

    goto :goto_0
.end method

.method public removeListener(Lcom/google/android/videos/store/StoreStatusMonitor$Listener;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/videos/store/StoreStatusMonitor$Listener;

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->listeners:Ljava/util/Set;

    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 136
    return-void
.end method

.method public reset()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 118
    iget-object v0, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->account:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 119
    iput-object v1, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->account:Ljava/lang/String;

    .line 120
    iput-object v1, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->currentPurchasesRequest:Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .line 121
    iput-boolean v2, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->pendingPurchasesUpdate:Z

    .line 122
    iput-object v1, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->currentWishlistRequest:Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    .line 123
    iput-boolean v2, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->pendingWishlistUpdate:Z

    .line 124
    iget-object v0, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/store/Database;->removeListener(Lcom/google/android/videos/store/Database$Listener;)Z

    .line 125
    iget-object v0, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->listeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 126
    iget-object v0, p0, Lcom/google/android/videos/store/StoreStatusMonitor;->statuses:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 128
    :cond_0
    return-void
.end method
