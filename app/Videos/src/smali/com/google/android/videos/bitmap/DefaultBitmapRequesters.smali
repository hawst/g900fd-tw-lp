.class public final Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;
.super Ljava/lang/Object;
.source "DefaultBitmapRequesters.java"

# interfaces
.implements Lcom/google/android/videos/bitmap/BitmapRequesters;


# instance fields
.field private final bitmapBytesRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;"
        }
    .end annotation
.end field

.field private final bitmapRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final controllableBitmapRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final posterArtRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final syncBitmapBytesMemoryCacheRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;"
        }
    .end annotation
.end field

.field private final syncBitmapBytesRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;"
        }
    .end annotation
.end field

.field private final syncBitmapRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ljava/lang/String;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/Config;Lcom/google/android/videos/bitmap/BitmapLruCache;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "localStoreExecutor"    # Ljava/util/concurrent/Executor;
    .param p3, "networkExecutor"    # Ljava/util/concurrent/Executor;
    .param p4, "cpuExecutor"    # Ljava/util/concurrent/Executor;
    .param p5, "persistentCachePath"    # Ljava/lang/String;
    .param p6, "httpClient"    # Lorg/apache/http/client/HttpClient;
    .param p7, "config"    # Lcom/google/android/videos/Config;
    .param p8, "bitmapCache"    # Lcom/google/android/videos/bitmap/BitmapLruCache;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    invoke-static {p6}, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;->createBitmapBytesNetworkRequester(Lorg/apache/http/client/HttpClient;)Lcom/google/android/videos/async/Requester;

    move-result-object v1

    .line 63
    .local v1, "bitmapBytesNetworkRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArray;>;"
    new-instance v3, Lcom/google/android/videos/cache/ByteArrayConverter;

    invoke-direct {v3}, Lcom/google/android/videos/cache/ByteArrayConverter;-><init>()V

    invoke-static {v3}, Lcom/google/android/videos/cache/TimestampedConverter;->create(Lcom/google/android/videos/cache/Converter;)Lcom/google/android/videos/cache/TimestampedConverter;

    move-result-object v2

    .line 65
    .local v2, "converter":Lcom/google/android/videos/cache/TimestampedConverter;, "Lcom/google/android/videos/cache/TimestampedConverter<Lcom/google/android/videos/utils/ByteArray;>;"
    new-instance v0, Lcom/google/android/videos/cache/PersistentCache;

    const-string v3, ".bm"

    invoke-direct {v0, v2, p5, v3}, Lcom/google/android/videos/cache/PersistentCache;-><init>(Lcom/google/android/videos/cache/Converter;Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    .local v0, "bitmapByteCache":Lcom/google/android/videos/cache/PersistentCache;, "Lcom/google/android/videos/cache/PersistentCache<Landroid/net/Uri;Lcom/google/android/videos/async/Timestamped<Lcom/google/android/videos/utils/ByteArray;>;>;"
    invoke-virtual {v0, p2}, Lcom/google/android/videos/cache/PersistentCache;->init(Ljava/util/concurrent/Executor;)Lcom/google/android/videos/cache/PersistentCache;

    .line 70
    invoke-static {v1, v0}, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;->createSyncBitmapBytesRequester(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/cache/PersistentCache;)Lcom/google/android/videos/async/Requester;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;->syncBitmapBytesRequester:Lcom/google/android/videos/async/Requester;

    .line 72
    invoke-static {v0}, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;->createSyncBitmapBytesMemoryCacheRequester(Lcom/google/android/videos/cache/PersistentCache;)Lcom/google/android/videos/async/Requester;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;->syncBitmapBytesMemoryCacheRequester:Lcom/google/android/videos/async/Requester;

    .line 75
    iget-object v3, p0, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;->syncBitmapBytesRequester:Lcom/google/android/videos/async/Requester;

    invoke-static {p3, p4, v3, p8, p1}, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;->createPosterArtRequester(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/bitmap/BitmapLruCache;Landroid/content/Context;)Lcom/google/android/videos/async/Requester;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;->posterArtRequester:Lcom/google/android/videos/async/Requester;

    .line 77
    iget-object v3, p0, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;->syncBitmapBytesRequester:Lcom/google/android/videos/async/Requester;

    invoke-static {p3, p4, v3, p8}, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;->createBitmapRequester(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/bitmap/BitmapLruCache;)Lcom/google/android/videos/async/Requester;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    .line 79
    iget-object v3, p0, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;->syncBitmapBytesRequester:Lcom/google/android/videos/async/Requester;

    invoke-static {v3, p8}, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;->createSyncBitmapRequester(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/bitmap/BitmapLruCache;)Lcom/google/android/videos/async/Requester;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;->syncBitmapRequester:Lcom/google/android/videos/async/Requester;

    .line 80
    iget-object v3, p0, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;->syncBitmapBytesRequester:Lcom/google/android/videos/async/Requester;

    invoke-static {p3, p4, v3, p8}, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;->createControllableBitmapRequester(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/bitmap/BitmapLruCache;)Lcom/google/android/videos/async/Requester;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;->controllableBitmapRequester:Lcom/google/android/videos/async/Requester;

    .line 82
    iget-object v3, p0, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;->syncBitmapBytesRequester:Lcom/google/android/videos/async/Requester;

    invoke-static {p3, v3}, Lcom/google/android/videos/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/AsyncRequester;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;->bitmapBytesRequester:Lcom/google/android/videos/async/Requester;

    .line 83
    return-void
.end method

.method private static createBitmapBytesNetworkRequester(Lorg/apache/http/client/HttpClient;)Lcom/google/android/videos/async/Requester;
    .locals 3
    .param p0, "httpClient"    # Lorg/apache/http/client/HttpClient;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/client/HttpClient;",
            ")",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;"
        }
    .end annotation

    .prologue
    .line 125
    new-instance v0, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters$1;

    invoke-direct {v0}, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters$1;-><init>()V

    .line 131
    .local v0, "converter":Lcom/google/android/videos/converter/RequestConverter;, "Lcom/google/android/videos/converter/RequestConverter<Landroid/net/Uri;Lorg/apache/http/client/methods/HttpUriRequest;>;"
    new-instance v1, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters$2;

    invoke-direct {v1}, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters$2;-><init>()V

    .line 137
    .local v1, "responseConverter":Lcom/google/android/videos/converter/HttpResponseConverter;, "Lcom/google/android/videos/converter/HttpResponseConverter<Lcom/google/android/videos/utils/ByteArray;>;"
    invoke-static {p0, v0, v1}, Lcom/google/android/videos/async/HttpRequester;->create(Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/RequestConverter;Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/async/HttpRequester;

    move-result-object v2

    return-object v2
.end method

.method private static createBitmapRequester(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/bitmap/BitmapLruCache;)Lcom/google/android/videos/async/Requester;
    .locals 4
    .param p0, "networkExecutor"    # Ljava/util/concurrent/Executor;
    .param p1, "cpuExecutor"    # Ljava/util/concurrent/Executor;
    .param p3, "bitmapCache"    # Lcom/google/android/videos/bitmap/BitmapLruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;",
            "Lcom/google/android/videos/bitmap/BitmapLruCache;",
            ")",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 179
    .local p2, "syncBitmapBytesRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArray;>;"
    new-instance v1, Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;

    const/4 v3, 0x1

    invoke-direct {v1, v3}, Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;-><init>(Z)V

    .line 181
    .local v1, "bytesToBitmapConverter":Lcom/google/android/videos/converter/ResponseConverter;, "Lcom/google/android/videos/converter/ResponseConverter<Lcom/google/android/videos/utils/ByteArray;Landroid/graphics/Bitmap;>;"
    invoke-static {p2, v1, p1}, Lcom/google/android/videos/async/ConvertingRequester;->create(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/converter/ResponseConverter;Ljava/util/concurrent/Executor;)Lcom/google/android/videos/async/Requester;

    move-result-object v2

    .line 183
    .local v2, "convertingRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Landroid/graphics/Bitmap;>;"
    invoke-static {p0, v2}, Lcom/google/android/videos/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/AsyncRequester;

    move-result-object v0

    .line 185
    .local v0, "asyncRequester":Lcom/google/android/videos/async/AsyncRequester;, "Lcom/google/android/videos/async/AsyncRequester<Landroid/net/Uri;Landroid/graphics/Bitmap;>;"
    new-instance v3, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters$4;

    invoke-direct {v3, v0, p3}, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters$4;-><init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/bitmap/BitmapLruCache;)V

    return-object v3
.end method

.method private static createControllableBitmapRequester(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/bitmap/BitmapLruCache;)Lcom/google/android/videos/async/Requester;
    .locals 4
    .param p0, "networkExecutor"    # Ljava/util/concurrent/Executor;
    .param p1, "cpuExecutor"    # Ljava/util/concurrent/Executor;
    .param p3, "bitmapCache"    # Lcom/google/android/videos/bitmap/BitmapLruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;",
            "Lcom/google/android/videos/bitmap/BitmapLruCache;",
            ")",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 211
    .local p2, "syncBitmapBytesRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArray;>;"
    new-instance v1, Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;

    const/4 v3, 0x1

    invoke-direct {v1, v3}, Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;-><init>(Z)V

    .line 213
    .local v1, "bytesToBitmapConverter":Lcom/google/android/videos/converter/ResponseConverter;, "Lcom/google/android/videos/converter/ResponseConverter<Lcom/google/android/videos/utils/ByteArray;Landroid/graphics/Bitmap;>;"
    invoke-static {p2, v1, p1}, Lcom/google/android/videos/async/ConvertingRequester;->create(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/converter/ResponseConverter;Ljava/util/concurrent/Executor;)Lcom/google/android/videos/async/Requester;

    move-result-object v2

    .line 215
    .local v2, "convertingRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Landroid/graphics/Bitmap;>;"
    invoke-static {p0, v2}, Lcom/google/android/videos/async/ControllableAsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/ControllableAsyncRequester;

    move-result-object v0

    .line 217
    .local v0, "asyncRequester":Lcom/google/android/videos/async/ControllableAsyncRequester;, "Lcom/google/android/videos/async/ControllableAsyncRequester<Landroid/net/Uri;Landroid/graphics/Bitmap;>;"
    new-instance v3, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters$6;

    invoke-direct {v3, v0, p3}, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters$6;-><init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/bitmap/BitmapLruCache;)V

    return-object v3
.end method

.method private static createPosterArtRequester(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/bitmap/BitmapLruCache;Landroid/content/Context;)Lcom/google/android/videos/async/Requester;
    .locals 5
    .param p0, "networkExecutor"    # Ljava/util/concurrent/Executor;
    .param p1, "cpuExecutor"    # Ljava/util/concurrent/Executor;
    .param p3, "bitmapCache"    # Lcom/google/android/videos/bitmap/BitmapLruCache;
    .param p4, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;",
            "Lcom/google/android/videos/bitmap/BitmapLruCache;",
            "Landroid/content/Context;",
            ")",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 160
    .local p2, "syncBitmapBytesRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArray;>;"
    new-instance v1, Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;

    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e017b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;-><init>(IZ)V

    .line 163
    .local v1, "bytesToBitmapConverter":Lcom/google/android/videos/converter/ResponseConverter;, "Lcom/google/android/videos/converter/ResponseConverter<Lcom/google/android/videos/utils/ByteArray;Landroid/graphics/Bitmap;>;"
    invoke-static {p2, v1, p1}, Lcom/google/android/videos/async/ConvertingRequester;->create(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/converter/ResponseConverter;Ljava/util/concurrent/Executor;)Lcom/google/android/videos/async/Requester;

    move-result-object v2

    .line 165
    .local v2, "convertingRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Landroid/graphics/Bitmap;>;"
    invoke-static {p0, v2}, Lcom/google/android/videos/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/AsyncRequester;

    move-result-object v0

    .line 167
    .local v0, "asyncRequester":Lcom/google/android/videos/async/AsyncRequester;, "Lcom/google/android/videos/async/AsyncRequester<Landroid/net/Uri;Landroid/graphics/Bitmap;>;"
    new-instance v3, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters$3;

    invoke-direct {v3, v0, p3}, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters$3;-><init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/bitmap/BitmapLruCache;)V

    return-object v3
.end method

.method private static createSyncBitmapBytesMemoryCacheRequester(Lcom/google/android/videos/cache/PersistentCache;)Lcom/google/android/videos/async/Requester;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/cache/PersistentCache",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/videos/async/Timestamped",
            "<",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;>;)",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;"
        }
    .end annotation

    .prologue
    .local p0, "persistentBitmapCache":Lcom/google/android/videos/cache/PersistentCache;, "Lcom/google/android/videos/cache/PersistentCache<Landroid/net/Uri;Lcom/google/android/videos/async/Timestamped<Lcom/google/android/videos/utils/ByteArray;>;>;"
    const-wide/32 v4, 0x48190800

    .line 149
    invoke-static {p0, v4, v5}, Lcom/google/android/videos/async/TimestampedCachingRequester;->create(Lcom/google/android/videos/cache/Cache;J)Lcom/google/android/videos/async/TimestampedCachingRequester;

    move-result-object v1

    .line 151
    .local v1, "requester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArray;>;"
    new-instance v0, Lcom/google/android/videos/cache/InMemoryLruCache;

    const/4 v2, 0x5

    invoke-direct {v0, v2}, Lcom/google/android/videos/cache/InMemoryLruCache;-><init>(I)V

    .line 153
    .local v0, "memoryCache":Lcom/google/android/videos/cache/Cache;, "Lcom/google/android/videos/cache/Cache<Landroid/net/Uri;Lcom/google/android/videos/async/Timestamped<Lcom/google/android/videos/utils/ByteArray;>;>;"
    const/4 v2, 0x0

    invoke-static {v0, v1, v4, v5, v2}, Lcom/google/android/videos/async/TimestampedCachingRequester;->create(Lcom/google/android/videos/cache/Cache;Lcom/google/android/videos/async/Requester;JZ)Lcom/google/android/videos/async/TimestampedCachingRequester;

    move-result-object v2

    return-object v2
.end method

.method private static createSyncBitmapBytesRequester(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/cache/PersistentCache;)Lcom/google/android/videos/async/Requester;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;",
            "Lcom/google/android/videos/cache/PersistentCache",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/videos/async/Timestamped",
            "<",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;>;)",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;"
        }
    .end annotation

    .prologue
    .line 143
    .local p0, "bitmapBytesNetworkRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArray;>;"
    .local p1, "persistentBitmapCache":Lcom/google/android/videos/cache/PersistentCache;, "Lcom/google/android/videos/cache/PersistentCache<Landroid/net/Uri;Lcom/google/android/videos/async/Timestamped<Lcom/google/android/videos/utils/ByteArray;>;>;"
    const-wide/32 v0, 0x48190800

    const/4 v2, 0x0

    invoke-static {p1, p0, v0, v1, v2}, Lcom/google/android/videos/async/TimestampedCachingRequester;->create(Lcom/google/android/videos/cache/Cache;Lcom/google/android/videos/async/Requester;JZ)Lcom/google/android/videos/async/TimestampedCachingRequester;

    move-result-object v0

    return-object v0
.end method

.method private static createSyncBitmapRequester(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/bitmap/BitmapLruCache;)Lcom/google/android/videos/async/Requester;
    .locals 3
    .param p1, "bitmapCache"    # Lcom/google/android/videos/bitmap/BitmapLruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;",
            "Lcom/google/android/videos/bitmap/BitmapLruCache;",
            ")",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 195
    .local p0, "syncBitmapBytesRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArray;>;"
    new-instance v0, Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;-><init>(Z)V

    .line 197
    .local v0, "bytesToBitmapConverter":Lcom/google/android/videos/converter/ResponseConverter;, "Lcom/google/android/videos/converter/ResponseConverter<Lcom/google/android/videos/utils/ByteArray;Landroid/graphics/Bitmap;>;"
    invoke-static {p0, v0}, Lcom/google/android/videos/async/ConvertingRequester;->create(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/converter/ResponseConverter;)Lcom/google/android/videos/async/Requester;

    move-result-object v1

    .line 199
    .local v1, "convertingRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Landroid/graphics/Bitmap;>;"
    new-instance v2, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters$5;

    invoke-direct {v2, v1, p1}, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters$5;-><init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/bitmap/BitmapLruCache;)V

    return-object v2
.end method


# virtual methods
.method public getBitmapBytesRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;->bitmapBytesRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getBitmapRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getControllableBitmapRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;->controllableBitmapRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getPosterArtRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;->posterArtRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getSyncBitmapBytesMemoryCacheRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;->syncBitmapBytesMemoryCacheRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getSyncBitmapBytesRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;->syncBitmapBytesRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method public getSyncBitmapRequester()Lcom/google/android/videos/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;->syncBitmapRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method
