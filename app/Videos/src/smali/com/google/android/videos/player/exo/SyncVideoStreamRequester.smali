.class public Lcom/google/android/videos/player/exo/SyncVideoStreamRequester;
.super Ljava/lang/Object;
.source "SyncVideoStreamRequester.java"


# instance fields
.field private final account:Ljava/lang/String;

.field private cachedStreams:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;"
        }
    .end annotation
.end field

.field private final isEpisode:Z

.field private final streamsRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/MpdGetRequest;",
            "Lcom/google/android/videos/streams/Streams;",
            ">;"
        }
    .end annotation
.end field

.field private final useSsl:Z

.field private final videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/videos/async/Requester;Z)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "isEpisode"    # Z
    .param p5, "useSsl"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/MpdGetRequest;",
            "Lcom/google/android/videos/streams/Streams;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 32
    .local p4, "streamsRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/android/videos/streams/Streams;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/google/android/videos/player/exo/SyncVideoStreamRequester;->account:Ljava/lang/String;

    .line 34
    iput-object p2, p0, Lcom/google/android/videos/player/exo/SyncVideoStreamRequester;->videoId:Ljava/lang/String;

    .line 35
    iput-boolean p3, p0, Lcom/google/android/videos/player/exo/SyncVideoStreamRequester;->isEpisode:Z

    .line 36
    iput-object p4, p0, Lcom/google/android/videos/player/exo/SyncVideoStreamRequester;->streamsRequester:Lcom/google/android/videos/async/Requester;

    .line 37
    iput-boolean p5, p0, Lcom/google/android/videos/player/exo/SyncVideoStreamRequester;->useSsl:Z

    .line 38
    return-void
.end method

.method private initStreams()Landroid/util/SparseArray;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/concurrent/ExecutionException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 60
    new-instance v0, Lcom/google/android/videos/api/MpdGetRequest;

    iget-object v1, p0, Lcom/google/android/videos/player/exo/SyncVideoStreamRequester;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/player/exo/SyncVideoStreamRequester;->videoId:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/videos/player/exo/SyncVideoStreamRequester;->isEpisode:Z

    iget-boolean v6, p0, Lcom/google/android/videos/player/exo/SyncVideoStreamRequester;->useSsl:Z

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    move v5, v4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/api/MpdGetRequest;-><init>(Ljava/lang/String;Ljava/lang/String;ZZZZLjava/util/Locale;)V

    .line 62
    .local v0, "request":Lcom/google/android/videos/api/MpdGetRequest;
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v8

    .line 63
    .local v8, "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/android/videos/streams/Streams;>;"
    iget-object v1, p0, Lcom/google/android/videos/player/exo/SyncVideoStreamRequester;->streamsRequester:Lcom/google/android/videos/async/Requester;

    invoke-interface {v1, v0, v8}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 64
    invoke-virtual {v8}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/streams/Streams;

    iget-object v12, v1, Lcom/google/android/videos/streams/Streams;->mediaStreams:Ljava/util/List;

    .line 65
    .local v12, "streams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    new-instance v11, Landroid/util/SparseArray;

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v11, v1}, Landroid/util/SparseArray;-><init>(I)V

    .line 66
    .local v11, "streamArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/google/android/videos/streams/MediaStream;>;"
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v1

    if-ge v9, v1, :cond_0

    .line 67
    invoke-interface {v12, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/videos/streams/MediaStream;

    .line 68
    .local v10, "stream":Lcom/google/android/videos/streams/MediaStream;
    iget-object v1, v10, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget v1, v1, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    invoke-virtual {v11, v1, v10}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 66
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 70
    .end local v10    # "stream":Lcom/google/android/videos/streams/MediaStream;
    :cond_0
    return-object v11
.end method


# virtual methods
.method public declared-synchronized getStream(I)Lcom/google/android/videos/streams/MediaStream;
    .locals 2
    .param p1, "itag"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 49
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/videos/player/exo/SyncVideoStreamRequester;->cachedStreams:Landroid/util/SparseArray;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 51
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/videos/player/exo/SyncVideoStreamRequester;->initStreams()Landroid/util/SparseArray;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/player/exo/SyncVideoStreamRequester;->cachedStreams:Landroid/util/SparseArray;
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 56
    :cond_0
    :try_start_2
    iget-object v1, p0, Lcom/google/android/videos/player/exo/SyncVideoStreamRequester;->cachedStreams:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/streams/MediaStream;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v1

    .line 52
    :catch_0
    move-exception v0

    .line 53
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    :try_start_3
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/Exception;

    if-eqz v1, :cond_1

    .end local v0    # "e":Ljava/util/concurrent/ExecutionException;
    :goto_0
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 49
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 53
    .restart local v0    # "e":Ljava/util/concurrent/ExecutionException;
    :cond_1
    :try_start_4
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    check-cast v1, Ljava/lang/Exception;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-object v0, v1

    goto :goto_0
.end method
