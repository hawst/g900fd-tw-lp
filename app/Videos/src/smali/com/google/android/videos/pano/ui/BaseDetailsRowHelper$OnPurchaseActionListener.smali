.class public interface abstract Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$OnPurchaseActionListener;
.super Ljava/lang/Object;
.source "BaseDetailsRowHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnPurchaseActionListener"
.end annotation


# virtual methods
.method public abstract onEpisodePurchase(Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;)V
.end method

.method public abstract onMoviePurchase(Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;)V
.end method

.method public abstract onSeasonPassPurchase(Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;)V
.end method
