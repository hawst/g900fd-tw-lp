.class Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$4;
.super Lcom/google/android/videos/ui/DefaultItemAnimator$VpaListenerAdapter;
.source "SecondScreenEntitiesAnimator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->startScaleChange(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;

.field final synthetic val$holder:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

.field final synthetic val$isOld:Z

.field final synthetic val$scaleAnimator:Landroid/support/v4/view/ViewPropertyAnimatorCompat;


# direct methods
.method constructor <init>(Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;ZLandroid/support/v4/view/ViewPropertyAnimatorCompat;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$4;->this$0:Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;

    iput-object p2, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$4;->val$holder:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    iput-boolean p3, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$4;->val$isOld:Z

    iput-object p4, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$4;->val$scaleAnimator:Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    invoke-direct {p0}, Lcom/google/android/videos/ui/DefaultItemAnimator$VpaListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$4;->val$scaleAnimator:Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setListener(Landroid/support/v4/view/ViewPropertyAnimatorListener;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    .line 138
    iget-object v0, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$4;->this$0:Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;

    iget-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$4;->val$holder:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    iget-boolean v2, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$4;->val$isOld:Z

    # invokes: Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->onChangeAnimationEnd(Landroid/support/v7/widget/RecyclerView$ViewHolder;Z)V
    invoke-static {v0, v1, v2}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->access$200(Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;Landroid/support/v7/widget/RecyclerView$ViewHolder;Z)V

    .line 139
    return-void
.end method

.method public onAnimationStart(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$4;->this$0:Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;

    iget-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$4;->val$holder:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    iget-boolean v2, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$4;->val$isOld:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->dispatchChangeStarting(Landroid/support/v7/widget/RecyclerView$ViewHolder;Z)V

    .line 133
    return-void
.end method
