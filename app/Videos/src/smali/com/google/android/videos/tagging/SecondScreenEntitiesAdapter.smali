.class public Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "SecondScreenEntitiesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;,
        Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$OnEntityClickListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final avatarCropTransformation:Lcom/google/android/play/image/AvatarCropTransformation;

.field private final dimImageScale:F

.field private final dimmedEntities:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity;",
            ">;"
        }
    .end annotation
.end field

.field private final imageDimension:I

.field private imageRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final knowledgeEntities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity;",
            ">;"
        }
    .end annotation
.end field

.field private onTagClickListener:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$OnEntityClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 51
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e01f7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 53
    .local v0, "dimmedImageDimension":F
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e01f6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->imageDimension:I

    .line 55
    iget v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->imageDimension:I

    int-to-float v1, v1

    div-float v1, v0, v1

    iput v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->dimImageScale:F

    .line 56
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->knowledgeEntities:Ljava/util/List;

    .line 57
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->dimmedEntities:Ljava/util/Set;

    .line 58
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/play/image/AvatarCropTransformation;->getFullAvatarCrop(Landroid/content/res/Resources;)Lcom/google/android/play/image/AvatarCropTransformation;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->avatarCropTransformation:Lcom/google/android/play/image/AvatarCropTransformation;

    .line 59
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;)Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$OnEntityClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->onTagClickListener:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$OnEntityClickListener;

    return-object v0
.end method

.method private requestImage(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;Lcom/google/android/videos/tagging/KnowledgeEntity;Lcom/google/android/videos/async/Requester;)V
    .locals 6
    .param p1, "holder"    # Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;
    .param p2, "entity"    # Lcom/google/android/videos/tagging/KnowledgeEntity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;",
            "Lcom/google/android/videos/tagging/KnowledgeEntity;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "imageRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;>;"
    const/4 v5, 0x1

    .line 199
    iget-object v0, p2, Lcom/google/android/videos/tagging/KnowledgeEntity;->image:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    if-nez v0, :cond_1

    .line 200
    instance-of v0, p2, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->imageView:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    check-cast p2, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    .end local p2    # "entity":Lcom/google/android/videos/tagging/KnowledgeEntity;
    invoke-static {v0, p2, v5}, Lcom/google/android/videos/tagging/Cards;->setDefaultActorImage(Landroid/widget/TextView;Lcom/google/android/videos/tagging/KnowledgeEntity$Person;Z)V

    .line 205
    :goto_0
    iget-object v0, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->imageView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 217
    :goto_1
    return-void

    .line 203
    .restart local p2    # "entity":Lcom/google/android/videos/tagging/KnowledgeEntity;
    :cond_0
    iget-object v0, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->imageView:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/google/android/videos/tagging/Cards;->setDefaultSongImage(Landroid/widget/ImageView;)V

    goto :goto_0

    .line 208
    :cond_1
    instance-of v0, p2, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    if-eqz v0, :cond_2

    .line 209
    new-instance v0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$ActorBitmapView;

    iget-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->avatarCropTransformation:Lcom/google/android/play/image/AvatarCropTransformation;

    move-object v2, p2

    check-cast v2, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    iget-object v3, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->imageView:Landroid/view/View;

    check-cast v3, Landroid/widget/TextView;

    iget v4, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->imageDimension:I

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$ActorBitmapView;-><init>(Lcom/google/android/play/image/AvatarCropTransformation;Lcom/google/android/videos/tagging/KnowledgeEntity$Person;Landroid/widget/TextView;IZ)V

    iput-object v0, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->bitmapView:Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;

    .line 216
    :goto_2
    iget-object v0, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->bitmapView:Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;

    iget-object v1, p2, Lcom/google/android/videos/tagging/KnowledgeEntity;->image:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    invoke-static {v0, p3, v1}, Lcom/google/android/videos/ui/BitmapLoader;->setBitmapAsync(Lcom/google/android/videos/ui/BitmapLoader$BitmapView;Lcom/google/android/videos/async/Requester;Ljava/lang/Object;)V

    goto :goto_1

    .line 213
    :cond_2
    new-instance v2, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$SongBitmapView;

    move-object v0, p2

    check-cast v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;

    iget-object v1, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->imageView:Landroid/view/View;

    check-cast v1, Landroid/widget/ImageView;

    iget v3, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->imageDimension:I

    invoke-direct {v2, v0, v1, v3}, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$SongBitmapView;-><init>(Lcom/google/android/videos/tagging/KnowledgeEntity$Song;Landroid/widget/ImageView;I)V

    iput-object v2, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->bitmapView:Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;

    goto :goto_2
.end method

.method private resetView(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 121
    const/4 v0, 0x0

    invoke-static {p1, v0}, Landroid/support/v4/view/ViewCompat;->setTransitionName(Landroid/view/View;Ljava/lang/String;)V

    .line 122
    invoke-static {p1}, Landroid/support/v4/view/ViewCompat;->animate(Landroid/view/View;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->cancel()V

    .line 123
    invoke-static {p1, v1}, Landroid/support/v4/view/ViewCompat;->setScaleX(Landroid/view/View;F)V

    .line 124
    invoke-static {p1, v1}, Landroid/support/v4/view/ViewCompat;->setScaleY(Landroid/view/View;F)V

    .line 125
    invoke-static {p1, v1}, Landroid/support/v4/view/ViewCompat;->setAlpha(Landroid/view/View;F)V

    .line 126
    return-void
.end method


# virtual methods
.method public addItem(Lcom/google/android/videos/tagging/KnowledgeEntity;)V
    .locals 2
    .param p1, "knowledgeEntity"    # Lcom/google/android/videos/tagging/KnowledgeEntity;

    .prologue
    .line 137
    iget-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->knowledgeEntities:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 138
    iget-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->knowledgeEntities:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 139
    .local v0, "insertPosition":I
    iget-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->knowledgeEntities:Ljava/util/List;

    invoke-interface {v1, v0, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 140
    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->notifyItemInserted(I)V

    .line 142
    .end local v0    # "insertPosition":I
    :cond_0
    return-void
.end method

.method public clear()V
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->knowledgeEntities:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->knowledgeEntities:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 176
    iget-object v0, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->dimmedEntities:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 177
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->knowledgeEntities:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->notifyItemRangeRemoved(II)V

    .line 179
    :cond_0
    return-void
.end method

.method public dimItem(Lcom/google/android/videos/tagging/KnowledgeEntity;)V
    .locals 2
    .param p1, "knowledgeEntity"    # Lcom/google/android/videos/tagging/KnowledgeEntity;

    .prologue
    .line 155
    iget-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->knowledgeEntities:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 156
    .local v0, "index":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 161
    :goto_0
    return-void

    .line 159
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->dimmedEntities:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 160
    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->notifyItemChanged(I)V

    goto :goto_0
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->knowledgeEntities:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->knowledgeEntities:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;

    if-eqz v0, :cond_0

    .line 69
    const v0, 0x7f0400bd

    .line 71
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0400bc

    goto :goto_0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "x1"    # I

    .prologue
    .line 33
    check-cast p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    .end local p1    # "x0":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->onBindViewHolder(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;I)V
    .locals 5
    .param p1, "holder"    # Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 77
    iget-object v3, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->knowledgeEntities:Ljava/util/List;

    invoke-interface {v3, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/tagging/KnowledgeEntity;

    .line 78
    .local v1, "knowledgeEntity":Lcom/google/android/videos/tagging/KnowledgeEntity;
    iget-object v3, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->dimmedEntities:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 79
    iget v3, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->dimImageScale:F

    iput v3, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->targetScale:F

    .line 83
    :goto_0
    instance-of v3, v1, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    if-eqz v3, :cond_2

    .line 84
    iget-object v3, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->imageFrame:Landroid/widget/FrameLayout;

    iget-object v4, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->circularForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    move-object v0, v1

    .line 85
    check-cast v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    .line 86
    .local v0, "actor":Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    iget-object v3, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->imageRequester:Lcom/google/android/videos/async/Requester;

    invoke-direct {p0, p1, v0, v3}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->requestImage(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;Lcom/google/android/videos/tagging/KnowledgeEntity;Lcom/google/android/videos/async/Requester;)V

    .line 87
    iget-object v3, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->secondLineTextView:Landroid/widget/TextView;

    iget-object v4, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->secondLineTextView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/google/android/videos/tagging/Cards;->buildCharacterNamesString(Landroid/content/res/Resources;Lcom/google/android/videos/tagging/KnowledgeEntity$Person;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    .end local v0    # "actor":Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    :cond_0
    :goto_1
    iget-object v3, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->firstLineTextView:Landroid/widget/TextView;

    iget-object v4, v1, Lcom/google/android/videos/tagging/KnowledgeEntity;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v3, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->imageFrame:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v1}, Landroid/widget/FrameLayout;->setTag(Ljava/lang/Object;)V

    .line 97
    iget-object v3, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->textFrame:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v1}, Landroid/widget/FrameLayout;->setTag(Ljava/lang/Object;)V

    .line 98
    return-void

    .line 81
    :cond_1
    const/high16 v3, 0x3f800000    # 1.0f

    iput v3, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->targetScale:F

    goto :goto_0

    .line 89
    :cond_2
    instance-of v3, v1, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;

    if-eqz v3, :cond_0

    .line 90
    iget-object v3, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->imageFrame:Landroid/widget/FrameLayout;

    iget-object v4, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->retangularForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    move-object v2, v1

    .line 91
    check-cast v2, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;

    .line 92
    .local v2, "song":Lcom/google/android/videos/tagging/KnowledgeEntity$Song;
    iget-object v3, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->imageRequester:Lcom/google/android/videos/async/Requester;

    invoke-direct {p0, p1, v2, v3}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->requestImage(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;Lcom/google/android/videos/tagging/KnowledgeEntity;Lcom/google/android/videos/async/Requester;)V

    .line 93
    iget-object v3, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->secondLineTextView:Landroid/widget/TextView;

    iget-object v4, v2, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;->performer:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1
    .param p1, "x0"    # Landroid/view/ViewGroup;
    .param p2, "x1"    # I

    .prologue
    .line 33
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;
    .locals 3
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 102
    new-instance v0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p2, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;-><init>(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;Landroid/view/View;)V

    return-object v0
.end method

.method public bridge synthetic onViewRecycled(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 33
    check-cast p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    .end local p1    # "x0":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->onViewRecycled(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;)V

    return-void
.end method

.method public onViewRecycled(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;)V
    .locals 3
    .param p1, "holder"    # Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    .prologue
    const/4 v2, 0x0

    .line 108
    iget-object v0, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->bitmapView:Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->bitmapView:Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;

    invoke-static {v0}, Lcom/google/android/videos/ui/BitmapLoader;->cancel(Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;)V

    .line 110
    iput-object v2, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->bitmapView:Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;

    .line 112
    :cond_0
    iget-object v0, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->imageFrame:Landroid/widget/FrameLayout;

    invoke-direct {p0, v0}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->resetView(Landroid/view/View;)V

    .line 113
    iget-object v0, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->imageView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->resetView(Landroid/view/View;)V

    .line 114
    iget-object v0, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->imageView:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 115
    iget-object v0, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->imageView:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 116
    iget-object v0, p1, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->imageView:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 118
    :cond_1
    return-void
.end method

.method public removeItem(Lcom/google/android/videos/tagging/KnowledgeEntity;)V
    .locals 2
    .param p1, "knowledgeEntity"    # Lcom/google/android/videos/tagging/KnowledgeEntity;

    .prologue
    .line 145
    iget-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->knowledgeEntities:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 146
    .local v0, "index":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 152
    :goto_0
    return-void

    .line 149
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->knowledgeEntities:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 150
    iget-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->dimmedEntities:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 151
    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->notifyItemRemoved(I)V

    goto :goto_0
.end method

.method public setImageRequester(Lcom/google/android/videos/async/Requester;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 133
    .local p1, "imageRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;>;"
    iput-object p1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->imageRequester:Lcom/google/android/videos/async/Requester;

    .line 134
    return-void
.end method

.method public setOnEntityClickListener(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$OnEntityClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$OnEntityClickListener;

    .prologue
    .line 129
    iput-object p1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->onTagClickListener:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$OnEntityClickListener;

    .line 130
    return-void
.end method

.method public undimItem(Lcom/google/android/videos/tagging/KnowledgeEntity;)V
    .locals 2
    .param p1, "knowledgeEntity"    # Lcom/google/android/videos/tagging/KnowledgeEntity;

    .prologue
    .line 164
    iget-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->knowledgeEntities:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 165
    .local v0, "index":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 166
    invoke-virtual {p0, p1}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->addItem(Lcom/google/android/videos/tagging/KnowledgeEntity;)V

    .line 171
    :goto_0
    return-void

    .line 168
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->dimmedEntities:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 169
    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->notifyItemChanged(I)V

    goto :goto_0
.end method
