.class Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$PiecewiseQueueSizeToChunkSizeMapper;
.super Ljava/lang/Object;
.source "BbaOneVideoChunkSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PiecewiseQueueSizeToChunkSizeMapper"
.end annotation


# instance fields
.field private final minQueueSize:I

.field private final slope:I


# direct methods
.method constructor <init>(II)V
    .locals 0
    .param p1, "minQueueSize"    # I
    .param p2, "slope"    # I

    .prologue
    .line 243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244
    iput p1, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$PiecewiseQueueSizeToChunkSizeMapper;->minQueueSize:I

    .line 245
    iput p2, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$PiecewiseQueueSizeToChunkSizeMapper;->slope:I

    .line 246
    return-void
.end method


# virtual methods
.method public getChunkSize(III)I
    .locals 3
    .param p1, "minChunkSize"    # I
    .param p2, "maxChunkSize"    # I
    .param p3, "queueSize"    # I

    .prologue
    .line 249
    if-lez p1, :cond_0

    if-gtz p2, :cond_2

    .line 250
    :cond_0
    const/4 p1, -0x1

    .line 256
    .end local p1    # "minChunkSize":I
    .end local p2    # "maxChunkSize":I
    :cond_1
    :goto_0
    return p1

    .line 252
    .restart local p1    # "minChunkSize":I
    .restart local p2    # "maxChunkSize":I
    :cond_2
    iget v1, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$PiecewiseQueueSizeToChunkSizeMapper;->minQueueSize:I

    if-le p3, v1, :cond_1

    .line 255
    iget v1, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$PiecewiseQueueSizeToChunkSizeMapper;->slope:I

    iget v2, p0, Lcom/google/android/videos/player/exo/adaptive/BbaOneVideoChunkSource$PiecewiseQueueSizeToChunkSizeMapper;->minQueueSize:I

    sub-int v2, p3, v2

    mul-int/2addr v1, v2

    add-int v0, v1, p1

    .line 256
    .local v0, "chunkSize":I
    if-le v0, p2, :cond_3

    .end local p2    # "maxChunkSize":I
    :goto_1
    move p1, p2

    goto :goto_0

    .restart local p2    # "maxChunkSize":I
    :cond_3
    move p2, v0

    goto :goto_1
.end method
