.class public Lcom/google/android/videos/ui/playnext/MovieClusterItemView;
.super Lcom/google/android/videos/ui/playnext/CardItemView;
.source "MovieClusterItemView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/playnext/MovieClusterItemView$Binder;
    }
.end annotation


# instance fields
.field private downloadView:Lcom/google/android/videos/ui/DownloadView;

.field private statusView:Landroid/widget/TextView;

.field private thumbnailBitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

.field private titleView:Landroid/widget/TextView;

.field private videoId:Ljava/lang/String;

.field private watchProgress:Landroid/widget/ProgressBar;

.field private yearAndDurationView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/playnext/CardItemView;-><init>(Landroid/content/Context;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/ui/playnext/CardItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/ui/playnext/CardItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    return-void
.end method

.method private getYearAndDurationText(Landroid/content/res/Resources;II)Ljava/lang/String;
    .locals 7
    .param p1, "resources"    # Landroid/content/res/Resources;
    .param p2, "year"    # I
    .param p3, "duration"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 152
    if-nez p2, :cond_0

    move-object v1, v0

    .line 153
    .local v1, "yearText":Ljava/lang/String;
    :goto_0
    if-nez p3, :cond_1

    .line 155
    .local v0, "durationText":Ljava/lang/String;
    :goto_1
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    aput-object v1, v2, v6

    aput-object v0, v2, v5

    invoke-static {p1, v5, v2}, Lcom/google/android/videos/utils/Util;->buildListString(Landroid/content/res/Resources;Z[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 152
    .end local v0    # "durationText":Ljava/lang/String;
    .end local v1    # "yearText":Ljava/lang/String;
    :cond_0
    invoke-static {p2}, Lcom/google/android/videos/utils/TimeUtil;->getStandaloneYearString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 153
    .restart local v1    # "yearText":Ljava/lang/String;
    :cond_1
    const v2, 0x7f0b0138

    new-array v3, v5, [Ljava/lang/Object;

    div-int/lit8 v4, p3, 0x3c

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method bind(Lcom/google/android/videos/adapter/MoviesDataSource;ILcom/google/android/videos/async/Requester;ZLcom/google/android/videos/ui/playnext/MovieClusterItemView$Binder;)V
    .locals 33
    .param p1, "dataSource"    # Lcom/google/android/videos/adapter/MoviesDataSource;
    .param p2, "index"    # I
    .param p4, "allowDownloads"    # Z
    .param p5, "binder"    # Lcom/google/android/videos/ui/playnext/MovieClusterItemView$Binder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/adapter/MoviesDataSource;",
            "I",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;Z",
            "Lcom/google/android/videos/ui/playnext/MovieClusterItemView$Binder;",
            ")V"
        }
    .end annotation

    .prologue
    .line 76
    .local p3, "posterRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v31

    .line 77
    .local v31, "resources":Landroid/content/res/Resources;
    invoke-virtual/range {p1 .. p2}, Lcom/google/android/videos/adapter/MoviesDataSource;->getItem(I)Landroid/database/Cursor;

    move-result-object v18

    .line 79
    .local v18, "cursor":Landroid/database/Cursor;
    const/16 v24, 0x0

    .line 80
    .local v24, "expireStatus":Ljava/lang/String;
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getExpirationTimestamp(Landroid/database/Cursor;)J

    move-result-wide v22

    .line 82
    .local v22, "expirationTimestamp":J
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/MoviesDataSource;->isActive(Landroid/database/Cursor;)Z

    move-result v6

    if-nez v6, :cond_7

    const/16 v25, 0x1

    .line 83
    .local v25, "expired":Z
    :goto_0
    if-eqz v25, :cond_8

    .line 84
    const v6, 0x7f0b01fa

    move-object/from16 v0, v31

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 93
    :cond_0
    :goto_1
    if-eqz v24, :cond_9

    .line 94
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->statusView:Landroid/widget/TextView;

    move-object/from16 v0, v24

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->statusView:Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 96
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->yearAndDurationView:Landroid/widget/TextView;

    if-eqz v6, :cond_1

    .line 97
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->yearAndDurationView:Landroid/widget/TextView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 106
    :cond_1
    :goto_2
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->watchProgress:Landroid/widget/ProgressBar;

    if-eqz v6, :cond_2

    .line 107
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->watchProgress:Landroid/widget/ProgressBar;

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getDurationSeconds(Landroid/database/Cursor;)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 108
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->watchProgress:Landroid/widget/ProgressBar;

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getResumeTimestamp(Landroid/database/Cursor;)I

    move-result v7

    div-int/lit16 v7, v7, 0x3e8

    invoke-virtual {v6, v7}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 111
    :cond_2
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getHaveLicense(Landroid/database/Cursor;)Z

    move-result v26

    .line 112
    .local v26, "haveLicense":Z
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getDownloadSize(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v20

    .line 113
    .local v20, "downloadSize":Ljava/lang/Long;
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getBytesDownloaded(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v17

    .line 115
    .local v17, "bytesDownloaded":Ljava/lang/Long;
    if-nez v25, :cond_3

    if-nez p4, :cond_a

    :cond_3
    const/16 v21, 0x1

    .line 116
    .local v21, "downloadViewHidden":Z
    :goto_3
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getPinningStatus(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v10

    .line 117
    .local v10, "pinningStatus":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->downloadView:Lcom/google/android/videos/ui/DownloadView;

    if-eqz v21, :cond_b

    const/16 v6, 0x8

    :goto_4
    invoke-virtual {v7, v6}, Lcom/google/android/videos/ui/DownloadView;->setVisibility(I)V

    .line 118
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->downloadView:Lcom/google/android/videos/ui/DownloadView;

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/videos/ui/DownloadView;->setTitle(Ljava/lang/String;)V

    .line 119
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->downloadView:Lcom/google/android/videos/ui/DownloadView;

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getAccount(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/MoviesDataSource;->isPinned(Landroid/database/Cursor;)Z

    move-result v9

    move/from16 v0, v26

    move-object/from16 v1, v20

    move-object/from16 v2, v17

    invoke-static {v0, v1, v2}, Lcom/google/android/videos/pinning/PinningStatusHelper;->getProgressFraction(ZLjava/lang/Long;Ljava/lang/Long;)F

    move-result v11

    invoke-virtual/range {v6 .. v11}, Lcom/google/android/videos/ui/DownloadView;->update(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Integer;F)V

    .line 123
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->titleView:Landroid/widget/TextView;

    if-eqz v6, :cond_c

    .line 124
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->titleView:Landroid/widget/TextView;

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    :goto_5
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->yearAndDurationView:Landroid/widget/TextView;

    if-eqz v6, :cond_4

    .line 132
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getReleaseYear(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v32

    .line 133
    .local v32, "year":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->yearAndDurationView:Landroid/widget/TextView;

    if-nez v32, :cond_d

    const/4 v6, 0x0

    :goto_6
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getDurationSeconds(Landroid/database/Cursor;)I

    move-result v8

    move-object/from16 v0, p0

    move-object/from16 v1, v31

    invoke-direct {v0, v1, v6, v8}, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->getYearAndDurationText(Landroid/content/res/Resources;II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    .end local v32    # "year":Ljava/lang/Integer;
    :cond_4
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->videoId:Ljava/lang/String;

    .line 138
    const v12, 0x7f0f00c9

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->thumbnailBitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

    const/4 v15, 0x0

    const-class v16, Ljava/lang/String;

    move-object/from16 v11, p0

    move-object/from16 v14, p3

    invoke-virtual/range {v11 .. v16}, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->registerBitmapView(ILcom/google/android/videos/ui/BitmapLoader$BitmapView;Lcom/google/android/videos/async/Requester;Landroid/graphics/Bitmap;Ljava/lang/Class;)V

    .line 141
    if-eqz v10, :cond_e

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const/4 v7, 0x3

    if-ne v6, v7, :cond_e

    const/16 v27, 0x1

    .line 143
    .local v27, "isDownloaded":Z
    :goto_7
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/videos/adapter/MoviesDataSource;->isNetworkConnected()Z

    move-result v6

    if-nez v6, :cond_5

    if-eqz v27, :cond_6

    :cond_5
    if-eqz v25, :cond_f

    :cond_6
    const/4 v6, 0x1

    :goto_8
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->setDimmedStyle(Z)V

    .line 145
    const v6, 0x7f0b0075

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->videoId:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v6, v7}, Lcom/google/android/videos/ui/TransitionUtil;->encodeAndSetTransitionName(Landroid/view/View;ILjava/lang/String;)V

    .line 146
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->thumbnailView:Landroid/widget/ImageView;

    const v7, 0x7f0b0077

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->videoId:Ljava/lang/String;

    invoke-static {v6, v7, v8}, Lcom/google/android/videos/ui/TransitionUtil;->encodeAndSetTransitionName(Landroid/view/View;ILjava/lang/String;)V

    .line 148
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->videoId:Ljava/lang/String;

    invoke-static {v6}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->assetInfoFromMovieId(Ljava/lang/String;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v6

    move-object/from16 v0, p5

    move-object/from16 v1, p0

    invoke-virtual {v0, v1, v6}, Lcom/google/android/videos/ui/playnext/MovieClusterItemView$Binder;->childImpression(Lcom/google/android/videos/ui/playnext/ClusterItemView;Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;)V

    .line 149
    return-void

    .line 82
    .end local v10    # "pinningStatus":Ljava/lang/Integer;
    .end local v17    # "bytesDownloaded":Ljava/lang/Long;
    .end local v20    # "downloadSize":Ljava/lang/Long;
    .end local v21    # "downloadViewHidden":Z
    .end local v25    # "expired":Z
    .end local v26    # "haveLicense":Z
    .end local v27    # "isDownloaded":Z
    :cond_7
    const/16 v25, 0x0

    goto/16 :goto_0

    .line 85
    .restart local v25    # "expired":Z
    :cond_8
    const-wide v6, 0x7fffffffffffffffL

    cmp-long v6, v22, v6

    if-eqz v6, :cond_0

    .line 86
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v28

    .line 87
    .local v28, "nowTimestamp":J
    move-wide/from16 v0, v22

    move-wide/from16 v2, v28

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/videos/utils/TimeUtil;->getRemainingDays(JJ)I

    move-result v30

    .line 88
    .local v30, "remainingDays":I
    const/16 v6, 0x3c

    move/from16 v0, v30

    if-gt v0, v6, :cond_0

    .line 89
    move-wide/from16 v0, v22

    move-wide/from16 v2, v28

    move-object/from16 v4, v31

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/videos/utils/TimeUtil;->getTimeToExpirationString(JJLandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v24

    goto/16 :goto_1

    .line 100
    .end local v28    # "nowTimestamp":J
    .end local v30    # "remainingDays":I
    :cond_9
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->statusView:Landroid/widget/TextView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 101
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->yearAndDurationView:Landroid/widget/TextView;

    if-eqz v6, :cond_1

    .line 102
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->yearAndDurationView:Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 115
    .restart local v17    # "bytesDownloaded":Ljava/lang/Long;
    .restart local v20    # "downloadSize":Ljava/lang/Long;
    .restart local v26    # "haveLicense":Z
    :cond_a
    const/16 v21, 0x0

    goto/16 :goto_3

    .line 117
    .restart local v10    # "pinningStatus":Ljava/lang/Integer;
    .restart local v21    # "downloadViewHidden":Z
    :cond_b
    const/4 v6, 0x0

    goto/16 :goto_4

    .line 126
    :cond_c
    const/4 v6, 0x1

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v24, v7, v8

    const/4 v8, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    move-object/from16 v0, v31

    invoke-static {v0, v6, v7}, Lcom/google/android/videos/utils/Util;->buildListString(Landroid/content/res/Resources;Z[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 128
    .local v19, "description":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 133
    .end local v19    # "description":Ljava/lang/String;
    .restart local v32    # "year":Ljava/lang/Integer;
    :cond_d
    invoke-virtual/range {v32 .. v32}, Ljava/lang/Integer;->intValue()I

    move-result v6

    goto/16 :goto_6

    .line 141
    .end local v32    # "year":Ljava/lang/Integer;
    :cond_e
    const/16 v27, 0x0

    goto/16 :goto_7

    .line 143
    .restart local v27    # "isDownloaded":Z
    :cond_f
    const/4 v6, 0x0

    goto/16 :goto_8
.end method

.method protected collectClickableViews([Landroid/view/View;)V
    .locals 2
    .param p1, "clickableViews"    # [Landroid/view/View;

    .prologue
    .line 168
    const/4 v0, 0x0

    aput-object p0, p1, v0

    .line 169
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->downloadView:Lcom/google/android/videos/ui/DownloadView;

    aput-object v1, p1, v0

    .line 170
    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .prologue
    .line 58
    invoke-super {p0}, Lcom/google/android/videos/ui/playnext/CardItemView;->onFinishInflate()V

    .line 59
    const v0, 0x7f0f00d8

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->statusView:Landroid/widget/TextView;

    .line 60
    const v0, 0x7f0f00d1

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/DownloadView;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->downloadView:Lcom/google/android/videos/ui/DownloadView;

    .line 61
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->thumbnailView:Landroid/widget/ImageView;

    const v1, 0x7f0a00d7

    invoke-static {v0, v1}, Lcom/google/android/videos/ui/BitmapLoader;->createDefaultBitmapView(Landroid/widget/ImageView;I)Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->thumbnailBitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

    .line 63
    const v0, 0x7f0f00a8

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->titleView:Landroid/widget/TextView;

    .line 64
    const v0, 0x7f0f00cb

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->yearAndDurationView:Landroid/widget/TextView;

    .line 65
    const v0, 0x7f0f00ce

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->watchProgress:Landroid/widget/ProgressBar;

    .line 66
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->yearAndDurationView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->statusView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    new-instance v1, Landroid/graphics/LightingColorFilter;

    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0079

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    const/high16 v3, 0x200000

    invoke-direct {v1, v2, v3}, Landroid/graphics/LightingColorFilter;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 70
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->thumbnailView:Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/google/android/videos/utils/ViewUtil;->removeOutlineProvider(Landroid/view/View;)V

    .line 71
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->downloadView:Lcom/google/android/videos/ui/DownloadView;

    invoke-static {v0}, Lcom/google/android/videos/utils/ViewUtil;->removeOutlineProvider(Landroid/view/View;)V

    .line 72
    return-void
.end method

.method protected onGenerateBitmapViewRequest(ILjava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .param p1, "id"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Q:",
            "Ljava/lang/Object;",
            ">(I",
            "Ljava/lang/Class",
            "<TQ;>;)TQ;"
        }
    .end annotation

    .prologue
    .line 160
    .local p2, "requestType":Ljava/lang/Class;, "Ljava/lang/Class<TQ;>;"
    const v0, 0x7f0f00c9

    if-ne p1, v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/MovieClusterItemView;->videoId:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 163
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/videos/ui/playnext/CardItemView;->onGenerateBitmapViewRequest(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method
