.class public final Lcom/google/android/videos/pano/model/GroupedPromoItem$GroupedPromoItemFactory;
.super Ljava/lang/Object;
.source "GroupedPromoItem.java"

# interfaces
.implements Lcom/google/android/repolib/common/Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/model/GroupedPromoItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GroupedPromoItemFactory"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Factory",
        "<",
        "Lcom/google/android/videos/pano/model/GroupedPromoItem;",
        "Lcom/google/android/videos/pano/model/PromoItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final groupId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "groupId"    # I

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput p2, p0, Lcom/google/android/videos/pano/model/GroupedPromoItem$GroupedPromoItemFactory;->groupId:I

    .line 40
    iput-object p1, p0, Lcom/google/android/videos/pano/model/GroupedPromoItem$GroupedPromoItemFactory;->context:Landroid/content/Context;

    .line 41
    return-void
.end method


# virtual methods
.method public createFrom(Lcom/google/android/videos/pano/model/PromoItem;)Lcom/google/android/videos/pano/model/GroupedPromoItem;
    .locals 3
    .param p1, "videoItem"    # Lcom/google/android/videos/pano/model/PromoItem;

    .prologue
    .line 46
    new-instance v1, Lcom/google/android/videos/pano/model/GroupedPromoItem;

    iget v0, p0, Lcom/google/android/videos/pano/model/GroupedPromoItem$GroupedPromoItemFactory;->groupId:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/pano/model/GroupedPromoItem$GroupedPromoItemFactory;->context:Landroid/content/Context;

    iget v2, p0, Lcom/google/android/videos/pano/model/GroupedPromoItem$GroupedPromoItemFactory;->groupId:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v2, 0x0

    invoke-direct {v1, p1, v0, v2}, Lcom/google/android/videos/pano/model/GroupedPromoItem;-><init>(Lcom/google/android/videos/pano/model/PromoItem;Ljava/lang/String;Lcom/google/android/videos/pano/model/GroupedPromoItem$1;)V

    return-object v1

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public bridge synthetic createFrom(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 32
    check-cast p1, Lcom/google/android/videos/pano/model/PromoItem;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/pano/model/GroupedPromoItem$GroupedPromoItemFactory;->createFrom(Lcom/google/android/videos/pano/model/PromoItem;)Lcom/google/android/videos/pano/model/GroupedPromoItem;

    move-result-object v0

    return-object v0
.end method
