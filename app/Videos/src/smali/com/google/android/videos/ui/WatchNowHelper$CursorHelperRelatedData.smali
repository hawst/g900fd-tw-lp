.class final Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;
.super Ljava/lang/Object;
.source "WatchNowHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/WatchNowHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "CursorHelperRelatedData"
.end annotation


# instance fields
.field private final dataSource:Lcom/google/android/videos/adapter/VideosDataSource;

.field private pendingCursor:Z


# direct methods
.method private constructor <init>(Lcom/google/android/videos/adapter/VideosDataSource;)V
    .locals 0
    .param p1, "dataSource"    # Lcom/google/android/videos/adapter/VideosDataSource;

    .prologue
    .line 689
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 690
    iput-object p1, p0, Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;->dataSource:Lcom/google/android/videos/adapter/VideosDataSource;

    .line 691
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/adapter/VideosDataSource;Lcom/google/android/videos/ui/WatchNowHelper$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/adapter/VideosDataSource;
    .param p2, "x1"    # Lcom/google/android/videos/ui/WatchNowHelper$1;

    .prologue
    .line 684
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;-><init>(Lcom/google/android/videos/adapter/VideosDataSource;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;

    .prologue
    .line 684
    iget-boolean v0, p0, Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;->pendingCursor:Z

    return v0
.end method

.method static synthetic access$702(Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;
    .param p1, "x1"    # Z

    .prologue
    .line 684
    iput-boolean p1, p0, Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;->pendingCursor:Z

    return p1
.end method

.method static synthetic access$800(Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;)Lcom/google/android/videos/adapter/VideosDataSource;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;

    .prologue
    .line 684
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper$CursorHelperRelatedData;->dataSource:Lcom/google/android/videos/adapter/VideosDataSource;

    return-object v0
.end method
