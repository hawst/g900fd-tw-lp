.class final Lcom/google/android/videos/api/VideoGetConverter;
.super Lcom/google/android/videos/converter/HttpResponseConverter;
.source "VideoGetConverter.java"

# interfaces
.implements Lcom/google/android/videos/converter/RequestConverter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/converter/HttpResponseConverter",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
        ">;",
        "Lcom/google/android/videos/converter/RequestConverter",
        "<",
        "Lcom/google/android/videos/api/VideoGetRequest;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        ">;"
    }
.end annotation


# instance fields
.field private final baseUri:Ljava/lang/String;

.field private final byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArrayPool;)V
    .locals 2
    .param p1, "baseUri"    # Landroid/net/Uri;
    .param p2, "byteArrayPool"    # Lcom/google/android/videos/utils/ByteArrayPool;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/videos/converter/HttpResponseConverter;-><init>()V

    .line 32
    new-instance v0, Lcom/google/android/videos/utils/UriBuilder;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "video"

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;->addSegment(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/utils/UriBuilder;->build()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/api/VideoGetConverter;->baseUri:Ljava/lang/String;

    .line 33
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/utils/ByteArrayPool;

    iput-object v0, p0, Lcom/google/android/videos/api/VideoGetConverter;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;

    .line 34
    return-void
.end method


# virtual methods
.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 23
    check-cast p1, Lcom/google/android/videos/api/VideoGetRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/api/VideoGetConverter;->convertRequest(Lcom/google/android/videos/api/VideoGetRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public convertRequest(Lcom/google/android/videos/api/VideoGetRequest;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 3
    .param p1, "request"    # Lcom/google/android/videos/api/VideoGetRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 38
    new-instance v1, Lcom/google/android/videos/utils/UriBuilder;

    iget-object v2, p0, Lcom/google/android/videos/api/VideoGetConverter;->baseUri:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/google/android/videos/utils/UriBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/android/videos/api/VideoGetRequest;->videoId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/videos/utils/UriBuilder;->addSegment(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/videos/utils/UriBuilder;->build()Ljava/lang/String;

    move-result-object v0

    .line 39
    .local v0, "uri":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/videos/async/HttpUriRequestFactory;->createGet(Ljava/lang/String;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v1

    return-object v1
.end method

.method public convertResponseEntity(Lorg/apache/http/HttpEntity;)Lcom/google/wireless/android/video/magma/proto/VideoResource;
    .locals 2
    .param p1, "entity"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 45
    new-instance v0, Lcom/google/wireless/android/video/magma/proto/VideoResource;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;-><init>()V

    iget-object v1, p0, Lcom/google/android/videos/api/VideoGetConverter;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;

    invoke-static {v0, p1, v1}, Lcom/google/android/videos/utils/EntityUtils;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;Lorg/apache/http/HttpEntity;Lcom/google/android/videos/utils/ByteArrayPool;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/VideoResource;

    return-object v0
.end method

.method public bridge synthetic convertResponseEntity(Lorg/apache/http/HttpEntity;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23
    invoke-virtual {p0, p1}, Lcom/google/android/videos/api/VideoGetConverter;->convertResponseEntity(Lorg/apache/http/HttpEntity;)Lcom/google/wireless/android/video/magma/proto/VideoResource;

    move-result-object v0

    return-object v0
.end method
