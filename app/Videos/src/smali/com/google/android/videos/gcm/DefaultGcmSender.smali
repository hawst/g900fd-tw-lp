.class public final Lcom/google/android/videos/gcm/DefaultGcmSender;
.super Ljava/lang/Object;
.source "DefaultGcmSender.java"

# interfaces
.implements Lcom/google/android/videos/gcm/GcmSender;


# instance fields
.field private final gcmRegistrationManager:Lcom/google/android/videos/gcm/GcmRegistrationManager;

.field private final googleCloudMessaging:Lcom/google/android/gms/gcm/GoogleCloudMessaging;

.field private final messageId:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/videos/gcm/GcmRegistrationManager;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "gcmRegistrationManager"    # Lcom/google/android/videos/gcm/GcmRegistrationManager;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    iput-object p2, p0, Lcom/google/android/videos/gcm/DefaultGcmSender;->gcmRegistrationManager:Lcom/google/android/videos/gcm/GcmRegistrationManager;

    .line 25
    invoke-static {p1}, Lcom/google/android/gms/gcm/GoogleCloudMessaging;->getInstance(Landroid/content/Context;)Lcom/google/android/gms/gcm/GoogleCloudMessaging;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/gcm/DefaultGcmSender;->googleCloudMessaging:Lcom/google/android/gms/gcm/GoogleCloudMessaging;

    .line 26
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/gcm/DefaultGcmSender;->messageId:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 27
    return-void
.end method


# virtual methods
.method public sendUserNotification(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 31
    iget-object v1, p0, Lcom/google/android/videos/gcm/DefaultGcmSender;->gcmRegistrationManager:Lcom/google/android/videos/gcm/GcmRegistrationManager;

    invoke-interface {v1, p1}, Lcom/google/android/videos/gcm/GcmRegistrationManager;->getNotificationKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 32
    .local v0, "notificationKey":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 42
    :goto_0
    return-void

    .line 37
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/videos/gcm/DefaultGcmSender;->googleCloudMessaging:Lcom/google/android/gms/gcm/GoogleCloudMessaging;

    iget-object v2, p0, Lcom/google/android/videos/gcm/DefaultGcmSender;->messageId:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2, p2}, Lcom/google/android/gms/gcm/GoogleCloudMessaging;->send(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 39
    :catch_0
    move-exception v1

    goto :goto_0
.end method
