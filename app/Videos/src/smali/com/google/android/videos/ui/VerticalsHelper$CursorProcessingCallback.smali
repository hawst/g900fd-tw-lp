.class Lcom/google/android/videos/ui/VerticalsHelper$CursorProcessingCallback;
.super Ljava/lang/Object;
.source "VerticalsHelper.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/VerticalsHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CursorProcessingCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private final targetCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/ui/VerticalsHelper$ContentVerticals;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/videos/async/Callback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/ui/VerticalsHelper$ContentVerticals;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 265
    .local p1, "targetCallback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/videos/ui/VerticalsHelper$ContentVerticals;>;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 266
    iput-object p1, p0, Lcom/google/android/videos/ui/VerticalsHelper$CursorProcessingCallback;->targetCallback:Lcom/google/android/videos/async/Callback;

    .line 267
    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 322
    iget-object v0, p0, Lcom/google/android/videos/ui/VerticalsHelper$CursorProcessingCallback;->targetCallback:Lcom/google/android/videos/async/Callback;

    invoke-interface {v0, p1, p2}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 323
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 260
    check-cast p1, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/VerticalsHelper$CursorProcessingCallback;->onError(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V
    .locals 10
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .param p2, "response"    # Landroid/database/Cursor;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 271
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 274
    .local v6, "userContentVerticals":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/videos/ui/VerticalsHelper$ContentVerticals;>;"
    const/4 v0, 0x0

    .line 275
    .local v0, "account":Ljava/lang/String;
    const/4 v2, 0x0

    .line 276
    .local v2, "content":I
    const/4 v5, 0x0

    .line 277
    .local v5, "pinnedContent":I
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 278
    const/4 v9, 0x0

    invoke-interface {p2, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 279
    .local v3, "currentAccount":Ljava/lang/String;
    if-nez v0, :cond_2

    .line 281
    move-object v0, v3

    .line 289
    :cond_1
    :goto_1
    const/4 v9, 0x1

    invoke-interface {p2, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 290
    .local v1, "assetType":I
    const/4 v9, 0x2

    invoke-interface {p2, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    if-ne v9, v7, :cond_3

    move v4, v7

    .line 291
    .local v4, "pinned":Z
    :goto_2
    sparse-switch v1, :sswitch_data_0

    .line 306
    const-string v9, "Invalid type when analyzing content"

    invoke-static {v9}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 315
    .end local v1    # "assetType":I
    .end local v3    # "currentAccount":Ljava/lang/String;
    .end local v4    # "pinned":Z
    :catchall_0
    move-exception v7

    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    throw v7

    .line 282
    .restart local v3    # "currentAccount":Ljava/lang/String;
    :cond_2
    :try_start_1
    invoke-static {v3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 283
    new-instance v9, Lcom/google/android/videos/ui/VerticalsHelper$ContentVerticals;

    invoke-direct {v9, v2, v5}, Lcom/google/android/videos/ui/VerticalsHelper$ContentVerticals;-><init>(II)V

    invoke-virtual {v6, v0, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    move-object v0, v3

    .line 286
    const/4 v2, 0x0

    .line 287
    const/4 v5, 0x0

    goto :goto_1

    .restart local v1    # "assetType":I
    :cond_3
    move v4, v8

    .line 290
    goto :goto_2

    .line 293
    .restart local v4    # "pinned":Z
    :sswitch_0
    or-int/lit8 v2, v2, 0x1

    .line 294
    if-eqz v4, :cond_0

    .line 295
    or-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 300
    :sswitch_1
    or-int/lit8 v2, v2, 0x2

    .line 301
    if-eqz v4, :cond_0

    .line 302
    or-int/lit8 v5, v5, 0x2

    goto :goto_0

    .line 311
    .end local v1    # "assetType":I
    .end local v3    # "currentAccount":Ljava/lang/String;
    .end local v4    # "pinned":Z
    :cond_4
    if-eqz v0, :cond_5

    .line 312
    new-instance v7, Lcom/google/android/videos/ui/VerticalsHelper$ContentVerticals;

    invoke-direct {v7, v2, v5}, Lcom/google/android/videos/ui/VerticalsHelper$ContentVerticals;-><init>(II)V

    invoke-virtual {v6, v0, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 315
    :cond_5
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    .line 317
    iget-object v7, p0, Lcom/google/android/videos/ui/VerticalsHelper$CursorProcessingCallback;->targetCallback:Lcom/google/android/videos/async/Callback;

    invoke-interface {v7, p1, v6}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 318
    return-void

    .line 291
    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0x13 -> :sswitch_1
        0x14 -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 260
    check-cast p1, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/VerticalsHelper$CursorProcessingCallback;->onResponse(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V

    return-void
.end method
