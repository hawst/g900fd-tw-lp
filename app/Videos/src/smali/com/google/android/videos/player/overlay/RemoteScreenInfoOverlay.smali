.class public Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;
.super Landroid/widget/RelativeLayout;
.source "RemoteScreenInfoOverlay.java"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Lcom/google/android/videos/player/overlay/OverscanSafeOverlay;


# instance fields
.field private final errorView:Landroid/widget/TextView;

.field private final fadeInAnimation:Landroid/view/animation/Animation;

.field private final fadeOutAnimation:Landroid/view/animation/Animation;

.field private final handler:Landroid/os/Handler;

.field private final playStatusView:Landroid/widget/ImageView;

.field private final spinner:Landroid/view/View;

.field private sticky:Z

.field private final titleView:Landroid/widget/TextView;

.field private final watchInfoOverlay:Landroid/widget/RelativeLayout;

.field private watchInfoState:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v4, 0x3e8

    const/16 v2, 0x8

    .line 60
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 61
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400b4

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 62
    const v0, 0x7f0f01e3

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->watchInfoOverlay:Landroid/widget/RelativeLayout;

    .line 63
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->watchInfoOverlay:Landroid/widget/RelativeLayout;

    const v1, 0x7f0f01e5

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->titleView:Landroid/widget/TextView;

    .line 64
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->watchInfoOverlay:Landroid/widget/RelativeLayout;

    const v1, 0x7f0f01e4

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->playStatusView:Landroid/widget/ImageView;

    .line 65
    const v0, 0x7f0f00c5

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->spinner:Landroid/view/View;

    .line 66
    const v0, 0x7f0f01e6

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->errorView:Landroid/widget/TextView;

    .line 68
    const/high16 v0, 0x10a0000

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->fadeInAnimation:Landroid/view/animation/Animation;

    .line 69
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->fadeInAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 70
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->fadeInAnimation:Landroid/view/animation/Animation;

    new-instance v1, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay$1;

    invoke-direct {v1, p0}, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay$1;-><init>(Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 88
    const v0, 0x10a0001

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->fadeOutAnimation:Landroid/view/animation/Animation;

    .line 89
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->fadeOutAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 90
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->fadeOutAnimation:Landroid/view/animation/Animation;

    new-instance v1, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay$2;

    invoke-direct {v1, p0}, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay$2;-><init>(Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 108
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->handler:Landroid/os/Handler;

    .line 109
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->watchInfoState:I

    .line 110
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->watchInfoOverlay:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 111
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->spinner:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 112
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->errorView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 113
    invoke-virtual {p0, v2}, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->setVisibility(I)V

    .line 114
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->watchInfoOverlay:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;
    .param p1, "x1"    # I

    .prologue
    .line 27
    iput p1, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->watchInfoState:I

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->updateHideTime()V

    return-void
.end method

.method private hideSpinner()V
    .locals 2

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->handler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 168
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->spinner:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 169
    return-void
.end method

.method private hideVideoInfo()V
    .locals 2

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->watchInfoOverlay:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 197
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->watchInfoOverlay:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setAnimation(Landroid/view/animation/Animation;)V

    .line 198
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->watchInfoState:I

    .line 199
    return-void
.end method

.method private showVideoInfo(Z)V
    .locals 3
    .param p1, "fadeIn"    # Z

    .prologue
    const/4 v2, 0x2

    .line 172
    iget v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->watchInfoState:I

    packed-switch v0, :pswitch_data_0

    .line 193
    :goto_0
    :pswitch_0
    return-void

    .line 174
    :pswitch_1
    if-eqz p1, :cond_0

    .line 175
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->watchInfoOverlay:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->fadeInAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->watchInfoOverlay:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 178
    iput v2, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->watchInfoState:I

    goto :goto_0

    .line 182
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->watchInfoOverlay:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setAnimation(Landroid/view/animation/Animation;)V

    .line 183
    iput v2, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->watchInfoState:I

    .line 184
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->updateHideTime()V

    goto :goto_0

    .line 187
    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->updateHideTime()V

    goto :goto_0

    .line 172
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private updateHideTime()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 202
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 203
    iget-boolean v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->sticky:Z

    if-nez v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->handler:Landroid/os/Handler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 206
    :cond_0
    return-void
.end method


# virtual methods
.method public generateLayoutParams()Lcom/google/android/videos/player/PlayerView$LayoutParams;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 133
    new-instance v0, Lcom/google/android/videos/player/PlayerView$LayoutParams;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v2, v1}, Lcom/google/android/videos/player/PlayerView$LayoutParams;-><init>(IIZ)V

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 118
    return-object p0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 210
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 218
    :goto_0
    return v0

    .line 212
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->watchInfoOverlay:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->fadeOutAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 215
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->spinner:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 210
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public makeSafeForOverscan(II)V
    .locals 3
    .param p1, "horizontalOverscan"    # I
    .param p2, "verticalOverscan"    # I

    .prologue
    const/4 v2, 0x0

    .line 123
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->errorView:Landroid/widget/TextView;

    invoke-virtual {v1, p1, p2, p1, p2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 125
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->watchInfoOverlay:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 127
    .local v0, "watchInfoLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, p1, v2, v2, p2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 128
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->watchInfoOverlay:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 129
    return-void
.end method

.method public setErrorState(Ljava/lang/String;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 159
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->errorView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->errorView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 161
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->hideVideoInfo()V

    .line 162
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->hideSpinner()V

    .line 163
    invoke-virtual {p0, v1}, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->setVisibility(I)V

    .line 164
    return-void
.end method

.method public setState(ZZ)V
    .locals 6
    .param p1, "playing"    # Z
    .param p2, "loading"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 141
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->errorView:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 142
    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->sticky:Z

    .line 143
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->showVideoInfo(Z)V

    .line 144
    if-eqz p1, :cond_2

    .line 145
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->playStatusView:Landroid/widget/ImageView;

    const v3, 0x7f020102

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 146
    if-eqz p2, :cond_1

    .line 147
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->handler:Landroid/os/Handler;

    const-wide/16 v4, 0x32

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 155
    :goto_1
    invoke-virtual {p0, v2}, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->setVisibility(I)V

    .line 156
    return-void

    :cond_0
    move v0, v2

    .line 142
    goto :goto_0

    .line 149
    :cond_1
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->hideSpinner()V

    goto :goto_1

    .line 152
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->playStatusView:Landroid/widget/ImageView;

    const v1, 0x7f020101

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 153
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->hideSpinner()V

    goto :goto_1
.end method

.method public setVideoTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->titleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    return-void
.end method
