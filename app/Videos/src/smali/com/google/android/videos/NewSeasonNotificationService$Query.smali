.class abstract Lcom/google/android/videos/NewSeasonNotificationService$Query;
.super Ljava/lang/Object;
.source "NewSeasonNotificationService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/NewSeasonNotificationService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "Query"
.end annotation


# static fields
.field public static final COLUMNS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 210
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "shows_title"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "season_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/videos/NewSeasonNotificationService$Query;->COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public static createPurchaseRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 10
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "showId"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x1

    .line 219
    new-instance v0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    const-string v2, "purchased_assets, videos, seasons, shows ON asset_type = 20 AND asset_id = video_id AND episode_season_id = season_id AND show_id = shows_id"

    sget-object v3, Lcom/google/android/videos/NewSeasonNotificationService$Query;->COLUMNS:[Ljava/lang/String;

    const-string v4, "rating_id"

    const-string v5, "account = ? AND show_id = ?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object p0, v6, v8

    aput-object p1, v6, v1

    const/4 v9, -0x1

    move-object v8, v7

    invoke-direct/range {v0 .. v9}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method
