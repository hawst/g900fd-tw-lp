.class public Lcom/google/android/videos/pano/activity/ShowDetailsActivity;
.super Lcom/google/android/videos/pano/activity/BaseDetailsActivity;
.source "ShowDetailsActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;-><init>()V

    return-void
.end method

.method public static createIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "showId"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-static {p0, p1, v0, v0}, Lcom/google/android/videos/pano/activity/ShowDetailsActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "showId"    # Ljava/lang/String;
    .param p2, "seasonId"    # Ljava/lang/String;

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/videos/pano/activity/ShowDetailsActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "showId"    # Ljava/lang/String;
    .param p2, "seasonId"    # Ljava/lang/String;
    .param p3, "episodeId"    # Ljava/lang/String;

    .prologue
    .line 38
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/videos/pano/activity/ShowDetailsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "show_id"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 40
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 41
    const-string v1, "season_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 43
    :cond_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 44
    const-string v1, "video_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 46
    :cond_1
    return-object v0
.end method


# virtual methods
.method protected addRows(Ljava/util/ArrayList;Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;)V
    .locals 3
    .param p2, "detailsRowHelper"    # Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v17/leanback/widget/Row;",
            ">;",
            "Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;",
            ")V"
        }
    .end annotation

    .prologue
    .line 86
    .local p1, "rows":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/support/v17/leanback/widget/Row;>;"
    move-object v0, p2

    check-cast v0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;

    .line 87
    .local v0, "helper":Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;
    invoke-virtual {v0, p1}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->setEpisodeListRows(Ljava/util/ArrayList;)V

    .line 89
    invoke-static {p0}, Lcom/google/android/videos/pano/ui/PanoHelper;->getDefaultPosterHeight(Landroid/content/Context;)I

    move-result v2

    .line 90
    .local v2, "size":I
    invoke-virtual {p2, v2, v2}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->getRelatedListRow(II)Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v1

    .line 91
    .local v1, "related":Landroid/support/v17/leanback/widget/ListRow;
    if-eqz v1, :cond_0

    .line 92
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 101
    :cond_0
    return-void
.end method

.method protected getListItemHelper()Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;
    .locals 18

    .prologue
    .line 65
    invoke-static/range {p0 .. p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v17

    .line 66
    .local v17, "videosGlobals":Lcom/google/android/videos/VideosGlobals;
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/videos/VideosGlobals;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v16

    .line 67
    .local v16, "apiRequesters":Lcom/google/android/videos/api/ApiRequesters;
    new-instance v1, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/pano/activity/ShowDetailsActivity;->account:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/pano/activity/ShowDetailsActivity;->itemId:Ljava/lang/String;

    invoke-interface/range {v16 .. v16}, Lcom/google/android/videos/api/ApiRequesters;->getAssetsCachingRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v6

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/videos/VideosGlobals;->getRecommendationsRequestFactory()Lcom/google/android/videos/api/RecommendationsRequest$Factory;

    move-result-object v7

    invoke-interface/range {v16 .. v16}, Lcom/google/android/videos/api/ApiRequesters;->getRecommendationsRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v8

    invoke-interface/range {v16 .. v16}, Lcom/google/android/videos/api/ApiRequesters;->getReviewsRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v9

    invoke-interface/range {v16 .. v16}, Lcom/google/android/videos/api/ApiRequesters;->getCategoryListRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v10

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStore()Lcom/google/android/videos/store/PurchaseStore;

    move-result-object v11

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/videos/VideosGlobals;->getWishlistStore()Lcom/google/android/videos/store/WishlistStore;

    move-result-object v12

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/videos/VideosGlobals;->getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v13

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/videos/VideosGlobals;->getBitmapRequesters()Lcom/google/android/videos/bitmap/BitmapRequesters;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/videos/bitmap/BitmapRequesters;->getControllableBitmapRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v14

    const v2, 0x1020002

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/videos/pano/activity/ShowDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v15

    move-object/from16 v2, p0

    move-object/from16 v5, p0

    invoke-direct/range {v1 .. v15}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$OnPurchaseActionListener;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/api/RecommendationsRequest$Factory;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/WishlistStore;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/async/Requester;Landroid/view/View;)V

    return-object v1
.end method

.method protected getMainItemId(Landroid/content/Intent;)Ljava/lang/String;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 60
    const-string v0, "show_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstance"    # Landroid/os/Bundle;

    .prologue
    .line 51
    invoke-super {p0, p1}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->onCreate(Landroid/os/Bundle;)V

    .line 56
    return-void
.end method
