.class public Lcom/google/android/videos/player/exo/VideoRenderer;
.super Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer;
.source "VideoRenderer.java"


# instance fields
.field private final forceCodecIsAdaptive:Z

.field private volatile lastOutputBufferTimestamp:J


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/SampleSource;Lcom/google/android/exoplayer/drm/DrmSessionManager;ZLandroid/os/Handler;Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer$FrameReleaseTimeHelper;Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer$EventListener;IJIZ)V
    .locals 14
    .param p1, "source"    # Lcom/google/android/exoplayer/SampleSource;
    .param p2, "drmSessionManager"    # Lcom/google/android/exoplayer/drm/DrmSessionManager;
    .param p3, "playClearSamplesWithoutKeys"    # Z
    .param p4, "eventHandler"    # Landroid/os/Handler;
    .param p5, "frameReleaseTimeHelper"    # Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer$FrameReleaseTimeHelper;
    .param p6, "eventListener"    # Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer$EventListener;
    .param p7, "maxDroppedFrameCountToNotify"    # I
    .param p8, "allowedJoiningTimeMs"    # J
    .param p10, "videoScalingMode"    # I
    .param p11, "forceCodecIsAdaptive"    # Z

    .prologue
    .line 32
    move-object v3, p0

    move-object v4, p1

    move-object/from16 v5, p2

    move/from16 v6, p3

    move/from16 v7, p10

    move-wide/from16 v8, p8

    move-object/from16 v10, p5

    move-object/from16 v11, p4

    move-object/from16 v12, p6

    move/from16 v13, p7

    invoke-direct/range {v3 .. v13}, Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer;-><init>(Lcom/google/android/exoplayer/SampleSource;Lcom/google/android/exoplayer/drm/DrmSessionManager;ZIJLcom/google/android/exoplayer/MediaCodecVideoTrackRenderer$FrameReleaseTimeHelper;Landroid/os/Handler;Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer$EventListener;I)V

    .line 35
    move/from16 v0, p11

    iput-boolean v0, p0, Lcom/google/android/videos/player/exo/VideoRenderer;->forceCodecIsAdaptive:Z

    .line 36
    return-void
.end method


# virtual methods
.method protected canReconfigureCodec(Landroid/media/MediaCodec;ZLcom/google/android/exoplayer/MediaFormat;Lcom/google/android/exoplayer/MediaFormat;)Z
    .locals 1
    .param p1, "codec"    # Landroid/media/MediaCodec;
    .param p2, "codecIsAdaptive"    # Z
    .param p3, "oldFormat"    # Lcom/google/android/exoplayer/MediaFormat;
    .param p4, "newFormat"    # Lcom/google/android/exoplayer/MediaFormat;

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/google/android/videos/player/exo/VideoRenderer;->forceCodecIsAdaptive:Z

    if-nez v0, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-super {p0, p1, v0, p3, p4}, Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer;->canReconfigureCodec(Landroid/media/MediaCodec;ZLcom/google/android/exoplayer/MediaFormat;Lcom/google/android/exoplayer/MediaFormat;)Z

    move-result v0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLastFrameTimestampMs()I
    .locals 4

    .prologue
    .line 60
    iget-wide v0, p0, Lcom/google/android/videos/player/exo/VideoRenderer;->lastOutputBufferTimestamp:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method protected processOutputBuffer(JJLandroid/media/MediaCodec;Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;IZ)Z
    .locals 5
    .param p1, "positionUs"    # J
    .param p3, "elapsedRealtimeUs"    # J
    .param p5, "codec"    # Landroid/media/MediaCodec;
    .param p6, "buffer"    # Ljava/nio/ByteBuffer;
    .param p7, "bufferInfo"    # Landroid/media/MediaCodec$BufferInfo;
    .param p8, "bufferIndex"    # I
    .param p9, "shouldSkip"    # Z

    .prologue
    .line 48
    invoke-super/range {p0 .. p9}, Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer;->processOutputBuffer(JJLandroid/media/MediaCodec;Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;IZ)Z

    move-result v0

    .line 50
    .local v0, "processed":Z
    if-nez p9, :cond_0

    if-eqz v0, :cond_0

    .line 51
    iget-wide v2, p7, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    iput-wide v2, p0, Lcom/google/android/videos/player/exo/VideoRenderer;->lastOutputBufferTimestamp:J

    .line 53
    :cond_0
    return v0
.end method
