.class public Lcom/google/android/videos/store/WishlistStoreSync;
.super Ljava/lang/Object;
.source "WishlistStoreSync.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/store/WishlistStoreSync$CleanupTask;,
        Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;,
        Lcom/google/android/videos/store/WishlistStoreSync$SnapshotTokenQuery;
    }
.end annotation


# static fields
.field private static final ACCOUNT_COLUMN:[Ljava/lang/String;


# instance fields
.field private final accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

.field private final assetStoreSync:Lcom/google/android/videos/store/AssetStoreSync;

.field private final database:Lcom/google/android/videos/store/Database;

.field private final syncTaskManager:Lcom/google/android/videos/store/SyncTaskManager;

.field private final wishlistRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/GetWishlistRequest;",
            "Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 81
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "wishlist_account"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/videos/store/WishlistStoreSync;->ACCOUNT_COLUMN:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/SyncTaskManager;Lcom/google/android/videos/store/AssetStoreSync;)V
    .locals 1
    .param p1, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .param p2, "database"    # Lcom/google/android/videos/store/Database;
    .param p4, "syncTaskManager"    # Lcom/google/android/videos/store/SyncTaskManager;
    .param p5, "assetStoreSync"    # Lcom/google/android/videos/store/AssetStoreSync;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/accounts/AccountManagerWrapper;",
            "Lcom/google/android/videos/store/Database;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/GetWishlistRequest;",
            "Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;",
            ">;",
            "Lcom/google/android/videos/store/SyncTaskManager;",
            "Lcom/google/android/videos/store/AssetStoreSync;",
            ")V"
        }
    .end annotation

    .prologue
    .line 104
    .local p3, "wishlistRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/GetWishlistRequest;Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/accounts/AccountManagerWrapper;

    iput-object v0, p0, Lcom/google/android/videos/store/WishlistStoreSync;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    .line 107
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/Database;

    iput-object v0, p0, Lcom/google/android/videos/store/WishlistStoreSync;->database:Lcom/google/android/videos/store/Database;

    .line 108
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/store/WishlistStoreSync;->wishlistRequester:Lcom/google/android/videos/async/Requester;

    .line 109
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/SyncTaskManager;

    iput-object v0, p0, Lcom/google/android/videos/store/WishlistStoreSync;->syncTaskManager:Lcom/google/android/videos/store/SyncTaskManager;

    .line 110
    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/AssetStoreSync;

    iput-object v0, p0, Lcom/google/android/videos/store/WishlistStoreSync;->assetStoreSync:Lcom/google/android/videos/store/AssetStoreSync;

    .line 111
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/store/WishlistStoreSync;)Lcom/google/android/videos/store/SyncTaskManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/WishlistStoreSync;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/videos/store/WishlistStoreSync;->syncTaskManager:Lcom/google/android/videos/store/SyncTaskManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/store/WishlistStoreSync;)Lcom/google/android/videos/store/Database;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/WishlistStoreSync;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/videos/store/WishlistStoreSync;->database:Lcom/google/android/videos/store/Database;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/store/WishlistStoreSync;)Lcom/google/android/videos/async/Requester;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/WishlistStoreSync;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/videos/store/WishlistStoreSync;->wishlistRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/store/WishlistStoreSync;)Lcom/google/android/videos/store/AssetStoreSync;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/WishlistStoreSync;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/videos/store/WishlistStoreSync;->assetStoreSync:Lcom/google/android/videos/store/AssetStoreSync;

    return-object v0
.end method

.method static synthetic access$400()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/videos/store/WishlistStoreSync;->ACCOUNT_COLUMN:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/videos/store/WishlistStoreSync;)Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/WishlistStoreSync;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/videos/store/WishlistStoreSync;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    return-object v0
.end method

.method public static storeSnapshotToken(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "transaction"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "snapshotToken"    # Ljava/lang/String;

    .prologue
    .line 146
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 147
    .local v0, "snapshotTokenValues":Landroid/content/ContentValues;
    const-string v1, "user_account"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    const-string v1, "wishlist_snapshot_token"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    const-string v1, "user_data"

    sget-object v2, Lcom/google/android/videos/store/WishlistStoreSync$SnapshotTokenQuery;->EQUAL_COLUMNS:[Ljava/lang/String;

    invoke-static {p0, v1, v0, v2}, Lcom/google/android/videos/utils/DbUtils;->updateOrReplace(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;[Ljava/lang/String;)J

    .line 151
    return-void
.end method


# virtual methods
.method public cleanup()V
    .locals 3

    .prologue
    .line 141
    new-instance v0, Lcom/google/android/videos/store/WishlistStoreSync$CleanupTask;

    const/16 v1, 0xbb8

    invoke-static {}, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->create()Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/videos/store/WishlistStoreSync$CleanupTask;-><init>(Lcom/google/android/videos/store/WishlistStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V

    invoke-virtual {v0}, Lcom/google/android/videos/store/WishlistStoreSync$CleanupTask;->schedule()V

    .line 142
    return-void
.end method

.method public cleanup(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/videos/async/NewCallback",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 136
    .local p1, "request":Lcom/google/android/videos/async/ControllableRequest;, "Lcom/google/android/videos/async/ControllableRequest<Ljava/lang/Integer;>;"
    .local p2, "callback":Lcom/google/android/videos/async/NewCallback;, "Lcom/google/android/videos/async/NewCallback<Ljava/lang/Integer;Ljava/lang/Void;>;"
    new-instance v0, Lcom/google/android/videos/store/WishlistStoreSync$CleanupTask;

    const/16 v1, 0xbb8

    invoke-static {p1, p2}, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->create(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/videos/store/WishlistStoreSync$CleanupTask;-><init>(Lcom/google/android/videos/store/WishlistStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V

    invoke-virtual {v0}, Lcom/google/android/videos/store/WishlistStoreSync$CleanupTask;->schedule()V

    .line 138
    return-void
.end method

.method public syncAssetForWishlistMovies(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/google/android/videos/async/NewCallback",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 122
    .local p1, "request":Lcom/google/android/videos/async/ControllableRequest;, "Lcom/google/android/videos/async/ControllableRequest<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    .local p2, "callback":Lcom/google/android/videos/async/NewCallback;, "Lcom/google/android/videos/async/NewCallback<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;Ljava/lang/Void;>;"
    iget-object v2, p0, Lcom/google/android/videos/store/WishlistStoreSync;->assetStoreSync:Lcom/google/android/videos/store/AssetStoreSync;

    const/16 v3, 0x3e8

    invoke-static {p1, p2}, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->create(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    move-result-object v4

    iget-object v0, p1, Lcom/google/android/videos/async/ControllableRequest;->data:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/videos/async/ControllableRequest;->data:Ljava/lang/Object;

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/String;

    invoke-static {v1, v5}, Lcom/google/android/videos/utils/CollectionUtil;->newArrayList(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v2, v3, v4, v0, v1}, Lcom/google/android/videos/store/AssetStoreSync;->syncMovieMetadata(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;Ljava/util/List;)V

    .line 125
    return-void
.end method

.method public syncAssetForWishlistShows(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/google/android/videos/async/NewCallback",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 130
    .local p1, "request":Lcom/google/android/videos/async/ControllableRequest;, "Lcom/google/android/videos/async/ControllableRequest<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    .local p2, "callback":Lcom/google/android/videos/async/NewCallback;, "Lcom/google/android/videos/async/NewCallback<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;Ljava/lang/Void;>;"
    iget-object v2, p0, Lcom/google/android/videos/store/WishlistStoreSync;->assetStoreSync:Lcom/google/android/videos/store/AssetStoreSync;

    const/16 v3, 0x3e8

    invoke-static {p1, p2}, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->create(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    move-result-object v4

    iget-object v0, p1, Lcom/google/android/videos/async/ControllableRequest;->data:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/videos/async/ControllableRequest;->data:Ljava/lang/Object;

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v0, v1}, Lcom/google/android/videos/store/AssetStoreSync;->syncShowMetadata(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    return-void
.end method

.method public syncWishlist(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/videos/async/NewCallback",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 115
    .local p1, "request":Lcom/google/android/videos/async/ControllableRequest;, "Lcom/google/android/videos/async/ControllableRequest<Ljava/lang/String;>;"
    .local p2, "callback":Lcom/google/android/videos/async/NewCallback;, "Lcom/google/android/videos/async/NewCallback<Ljava/lang/String;Ljava/lang/Void;>;"
    new-instance v1, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;

    const/16 v2, 0x7d0

    invoke-static {p1, p2}, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->create(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    move-result-object v3

    iget-object v0, p1, Lcom/google/android/videos/async/ControllableRequest;->data:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, p0, v2, v3, v0}, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;-><init>(Lcom/google/android/videos/store/WishlistStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->schedule()V

    .line 117
    return-void
.end method
