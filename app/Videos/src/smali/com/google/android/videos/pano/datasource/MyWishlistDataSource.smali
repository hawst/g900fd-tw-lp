.class public Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;
.super Lcom/google/android/videos/pano/datasource/BaseDataSource;
.source "MyWishlistDataSource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/pano/datasource/MyWishlistDataSource$Query;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/pano/datasource/BaseDataSource",
        "<",
        "Lcom/google/android/videos/pano/model/VideoItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final assetRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final assetResponseCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final databaseListener:Lcom/google/android/videos/store/Database$BaseListener;

.field private final updateArrayRunnable:Ljava/lang/Runnable;

.field private final wishlistResponseCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/store/WishlistStore$WishlistRequest;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final wishlistStore:Lcom/google/android/videos/store/WishlistStore;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/WishlistStore;Lcom/google/android/videos/async/Requester;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "signInManager"    # Lcom/google/android/videos/accounts/SignInManager;
    .param p3, "wishlistStore"    # Lcom/google/android/videos/store/WishlistStore;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/videos/accounts/SignInManager;",
            "Lcom/google/android/videos/store/WishlistStore;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 112
    .local p4, "assetRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;>;"
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/pano/datasource/BaseDataSource;-><init>(Landroid/content/Context;Lcom/google/android/videos/accounts/SignInManager;)V

    .line 37
    new-instance v0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource$1;

    invoke-direct {v0, p0}, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource$1;-><init>(Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;)V

    iput-object v0, p0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->wishlistResponseCallback:Lcom/google/android/videos/async/Callback;

    .line 76
    new-instance v0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource$2;

    invoke-direct {v0, p0}, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource$2;-><init>(Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;)V

    iput-object v0, p0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->assetResponseCallback:Lcom/google/android/videos/async/Callback;

    .line 101
    new-instance v0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource$3;

    invoke-direct {v0, p0}, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource$3;-><init>(Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;)V

    iput-object v0, p0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->updateArrayRunnable:Ljava/lang/Runnable;

    .line 113
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/WishlistStore;

    iput-object v0, p0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->wishlistStore:Lcom/google/android/videos/store/WishlistStore;

    .line 114
    iput-object p4, p0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->assetRequester:Lcom/google/android/videos/async/Requester;

    .line 115
    new-instance v0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource$4;

    invoke-direct {v0, p0}, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource$4;-><init>(Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;)V

    iput-object v0, p0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->databaseListener:Lcom/google/android/videos/store/Database$BaseListener;

    .line 121
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;)Lcom/google/android/videos/async/Callback;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->assetResponseCallback:Lcom/google/android/videos/async/Callback;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;)Lcom/google/android/videos/async/Requester;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->assetRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->updateArrayRunnable:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method public firstUpdatableAdded()V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->wishlistStore:Lcom/google/android/videos/store/WishlistStore;

    invoke-virtual {v0}, Lcom/google/android/videos/store/WishlistStore;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->databaseListener:Lcom/google/android/videos/store/Database$BaseListener;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/store/Database;->addListener(Lcom/google/android/videos/store/Database$Listener;)V

    .line 133
    return-void
.end method

.method public lastUpdatableRemoved()V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->wishlistStore:Lcom/google/android/videos/store/WishlistStore;

    invoke-virtual {v0}, Lcom/google/android/videos/store/WishlistStore;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->databaseListener:Lcom/google/android/videos/store/Database$BaseListener;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/store/Database;->removeListener(Lcom/google/android/videos/store/Database$Listener;)Z

    .line 138
    return-void
.end method

.method protected updateInternal(Z)V
    .locals 4
    .param p1, "forced"    # Z

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->getAccount()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource$Query;->PROJECTION:[Ljava/lang/String;

    const-string v3, "wishlist_item_order"

    invoke-static {v1, v2, v3}, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;->createRequestForMoviesAndShows(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    move-result-object v0

    .line 127
    .local v0, "request":Lcom/google/android/videos/store/WishlistStore$WishlistRequest;
    iget-object v1, p0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->wishlistStore:Lcom/google/android/videos/store/WishlistStore;

    iget-object v2, p0, Lcom/google/android/videos/pano/datasource/MyWishlistDataSource;->wishlistResponseCallback:Lcom/google/android/videos/async/Callback;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/videos/store/WishlistStore;->loadWishlist(Lcom/google/android/videos/store/WishlistStore$WishlistRequest;Lcom/google/android/videos/async/Callback;)V

    .line 128
    return-void
.end method
