.class public final Lcom/google/android/videos/api/AssetResourceUtil;
.super Ljava/lang/Object;
.source "AssetResourceUtil.java"


# direct methods
.method public static assetInfoFromAssetResourceId(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;
    .locals 3
    .param p0, "assetResourceId"    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .prologue
    .line 126
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    .line 127
    .local v1, "id":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 128
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->mid:Ljava/lang/String;

    .line 129
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 130
    const-string v1, ""

    .line 134
    :cond_0
    new-instance v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    invoke-direct {v0}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;-><init>()V

    .line 135
    .local v0, "assetInfo":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;
    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    sparse-switch v2, :sswitch_data_0

    .line 157
    const/4 v2, 0x0

    iput v2, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    .line 158
    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->assetId:Ljava/lang/String;

    .line 161
    :goto_0
    return-object v0

    .line 137
    :sswitch_0
    const/4 v2, 0x1

    iput v2, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    .line 138
    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->assetId:Ljava/lang/String;

    goto :goto_0

    .line 141
    :sswitch_1
    const/4 v2, 0x2

    iput v2, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    .line 142
    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->assetId:Ljava/lang/String;

    goto :goto_0

    .line 145
    :sswitch_2
    const/4 v2, 0x4

    iput v2, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    .line 146
    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->seasonId:Ljava/lang/String;

    goto :goto_0

    .line 149
    :sswitch_3
    const/4 v2, 0x5

    iput v2, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    .line 150
    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->showId:Ljava/lang/String;

    goto :goto_0

    .line 153
    :sswitch_4
    const/4 v2, 0x6

    iput v2, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    .line 154
    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->assetId:Ljava/lang/String;

    goto :goto_0

    .line 135
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_4
        0x6 -> :sswitch_0
        0x12 -> :sswitch_3
        0x13 -> :sswitch_2
        0x14 -> :sswitch_1
    .end sparse-switch
.end method

.method private static deviation(FFZFFF)D
    .locals 9
    .param p0, "imageWidth"    # F
    .param p1, "imageHeight"    # F
    .param p2, "resizable"    # Z
    .param p3, "desiredWidth"    # F
    .param p4, "desiredHeight"    # F
    .param p5, "minScale"    # F

    .prologue
    const/4 v7, 0x0

    const-wide/high16 v4, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    const/high16 v8, 0x3f800000    # 1.0f

    .line 303
    cmpg-float v6, p0, v7

    if-lez v6, :cond_0

    cmpg-float v6, p1, v7

    if-gtz v6, :cond_1

    .line 328
    :cond_0
    :goto_0
    return-wide v4

    .line 310
    :cond_1
    div-float v6, p0, p3

    div-float v7, p1, p4

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 311
    .local v1, "scale":F
    cmpg-float v6, v1, p5

    if-ltz v6, :cond_0

    .line 314
    div-float v3, p0, v1

    .line 315
    .local v3, "scaledWidth":F
    div-float v2, p1, v1

    .line 316
    .local v2, "scaledHeight":F
    if-eqz p2, :cond_3

    cmpl-float v4, v1, v8

    if-lez v4, :cond_3

    .line 318
    const/high16 v1, 0x3f800000    # 1.0f

    .line 325
    :cond_2
    :goto_1
    div-float v4, v3, p3

    div-float v5, v2, p4

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 328
    .local v0, "ratioDifference":F
    float-to-double v4, v0

    float-to-double v6, v1

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v4, v6

    goto :goto_0

    .line 319
    .end local v0    # "ratioDifference":F
    :cond_3
    cmpg-float v4, v1, v8

    if-gez v4, :cond_2

    .line 321
    div-float v4, v8, v1

    float-to-double v4, v4

    const-wide/high16 v6, 0x4008000000000000L    # 3.0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    double-to-float v1, v4

    goto :goto_1
.end method

.method public static findBestImage([Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;IIIF)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .locals 16
    .param p0, "images"    # [Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .param p1, "imageType"    # I
    .param p2, "desiredWidth"    # I
    .param p3, "desiredHeight"    # I
    .param p4, "minScale"    # F

    .prologue
    .line 249
    move-object/from16 v0, p0

    array-length v12, v0

    .line 250
    .local v12, "imageCount":I
    if-nez v12, :cond_1

    .line 251
    const/4 v8, 0x0

    .line 270
    :cond_0
    :goto_0
    return-object v8

    .line 253
    :cond_1
    const/4 v13, 0x0

    .line 254
    .local v13, "selectedImage":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    const-wide/high16 v14, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    .line 255
    .local v14, "minDeviation":D
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    if-ge v9, v12, :cond_4

    .line 256
    aget-object v8, p0, v9

    .line 257
    .local v8, "currentImage":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    iget v2, v8, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->type:I

    move/from16 v0, p1

    if-eq v0, v2, :cond_3

    .line 255
    :cond_2
    :goto_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 260
    :cond_3
    if-lez p2, :cond_0

    if-lez p3, :cond_0

    .line 263
    iget v2, v8, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->width:I

    int-to-float v2, v2

    iget v3, v8, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->height:I

    int-to-float v3, v3

    iget-boolean v4, v8, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->resizable:Z

    move/from16 v0, p2

    int-to-float v5, v0

    move/from16 v0, p3

    int-to-float v6, v0

    move/from16 v7, p4

    invoke-static/range {v2 .. v7}, Lcom/google/android/videos/api/AssetResourceUtil;->deviation(FFZFFF)D

    move-result-wide v10

    .line 265
    .local v10, "deviation":D
    cmpg-double v2, v10, v14

    if-gez v2, :cond_2

    .line 266
    move-wide v14, v10

    .line 267
    move-object v13, v8

    goto :goto_2

    .end local v8    # "currentImage":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .end local v10    # "deviation":D
    :cond_4
    move-object v8, v13

    .line 270
    goto :goto_0
.end method

.method public static findBestImageUrl(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;IIIFZ)Ljava/lang/String;
    .locals 6
    .param p0, "assetMetadata"    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    .param p1, "imageType"    # I
    .param p2, "desiredWidth"    # I
    .param p3, "desiredHeight"    # I
    .param p4, "minScale"    # F
    .param p5, "crop"    # Z

    .prologue
    .line 180
    if-eqz p0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/api/AssetResourceUtil;->findBestImageUrl([Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;IIIFZ)Ljava/lang/String;

    move-result-object v0

    .line 184
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static findBestImageUrl([Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;IIIFZ)Ljava/lang/String;
    .locals 2
    .param p0, "images"    # [Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .param p1, "imageType"    # I
    .param p2, "desiredWidth"    # I
    .param p3, "desiredHeight"    # I
    .param p4, "minScale"    # F
    .param p5, "crop"    # Z

    .prologue
    .line 203
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/videos/api/AssetResourceUtil;->findBestImage([Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;IIIF)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    move-result-object v0

    .line 205
    .local v0, "image":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    if-nez v0, :cond_0

    .line 206
    const/4 v1, 0x0

    .line 212
    :goto_0
    return-object v1

    .line 208
    :cond_0
    iget-boolean v1, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->resizable:Z

    if-eqz v1, :cond_1

    if-lez p2, :cond_1

    if-gtz p3, :cond_2

    .line 209
    :cond_1
    iget-object v1, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->url:Ljava/lang/String;

    goto :goto_0

    .line 212
    :cond_2
    invoke-static {v0, p2, p3, p5}, Lcom/google/android/videos/api/AssetResourceUtil;->getImageUrl(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;IIZ)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static findTrailerId(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/lang/String;
    .locals 6
    .param p0, "movieAsset"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    const/4 v3, 0x0

    .line 345
    if-nez p0, :cond_1

    move-object v2, v3

    .line 346
    .local v2, "trailerIds":[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    :goto_0
    if-eqz v2, :cond_0

    array-length v4, v2

    if-nez v4, :cond_2

    .line 355
    :cond_0
    :goto_1
    return-object v3

    .line 345
    .end local v2    # "trailerIds":[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    :cond_1
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    goto :goto_0

    .line 349
    .restart local v2    # "trailerIds":[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    :cond_2
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    array-length v4, v2

    if-ge v1, v4, :cond_0

    .line 350
    aget-object v0, v2, v1

    .line 351
    .local v0, "candidate":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    const/4 v5, 0x6

    if-ne v4, v5, :cond_3

    .line 352
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    goto :goto_1

    .line 349
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method public static getAggregatedUserRating([Lcom/google/wireless/android/video/magma/proto/ViewerRating;II)Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    .locals 8
    .param p0, "aggregatedRatings"    # [Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    .param p1, "userType"    # I
    .param p2, "ratingType"    # I

    .prologue
    const/4 v2, 0x0

    .line 282
    if-nez p0, :cond_1

    move-object v1, v2

    .line 292
    :cond_0
    :goto_0
    return-object v1

    .line 285
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v3, p0

    if-ge v0, v3, :cond_3

    .line 286
    aget-object v1, p0, v0

    .line 287
    .local v1, "viewerRating":Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    iget v3, v1, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->type:I

    if-ne v3, p2, :cond_2

    iget v3, v1, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->userType:I

    if-ne v3, p1, :cond_2

    iget-wide v4, v1, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->ratingsCount:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-gtz v3, :cond_0

    .line 285
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v1    # "viewerRating":Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    :cond_3
    move-object v1, v2

    .line 292
    goto :goto_0
.end method

.method public static getImageUrl(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;IIZ)Ljava/lang/String;
    .locals 4
    .param p0, "image"    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .param p1, "desiredWidth"    # I
    .param p2, "desiredHeight"    # I
    .param p3, "crop"    # Z

    .prologue
    .line 220
    if-nez p0, :cond_0

    .line 221
    const/4 v1, 0x0

    .line 231
    :goto_0
    return-object v1

    .line 223
    :cond_0
    new-instance v0, Lcom/google/android/videos/utils/FIFEOptionsBuilder;

    invoke-direct {v0}, Lcom/google/android/videos/utils/FIFEOptionsBuilder;-><init>()V

    .line 224
    .local v0, "options":Lcom/google/android/videos/utils/FIFEOptionsBuilder;
    if-eqz p3, :cond_1

    .line 225
    invoke-virtual {v0, p1}, Lcom/google/android/videos/utils/FIFEOptionsBuilder;->setWidth(I)Lcom/google/android/videos/utils/FIFEOptionsBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/videos/utils/FIFEOptionsBuilder;->setHeight(I)Lcom/google/android/videos/utils/FIFEOptionsBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/videos/utils/FIFEOptionsBuilder;->setCrop()Lcom/google/android/videos/utils/FIFEOptionsBuilder;

    .line 231
    :goto_1
    invoke-virtual {v0}, Lcom/google/android/videos/utils/FIFEOptionsBuilder;->build()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->url:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/videos/utils/FIFEUtil;->setImageUrlOptions(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 226
    :cond_1
    int-to-float v1, p1

    int-to-float v2, p2

    div-float/2addr v1, v2

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->width:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->height:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_2

    .line 227
    invoke-virtual {v0, p1}, Lcom/google/android/videos/utils/FIFEOptionsBuilder;->setWidth(I)Lcom/google/android/videos/utils/FIFEOptionsBuilder;

    goto :goto_1

    .line 229
    :cond_2
    invoke-virtual {v0, p2}, Lcom/google/android/videos/utils/FIFEOptionsBuilder;->setHeight(I)Lcom/google/android/videos/utils/FIFEOptionsBuilder;

    goto :goto_1
.end method

.method public static getYearIfAvailable(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)I
    .locals 4
    .param p0, "assetMetadata"    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    .prologue
    .line 335
    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->releaseDateTimestampSec:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->releaseDateTimestampSec:J

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/TimeUtil;->getYear(J)I

    move-result v0

    goto :goto_0
.end method

.method public static idFromAlbumId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "albumId"    # Ljava/lang/String;

    .prologue
    .line 59
    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sj:album:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static idFromAssetResourceId(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Ljava/lang/String;
    .locals 4
    .param p0, "assetResourceId"    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .prologue
    .line 103
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    .line 104
    .local v1, "id":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 105
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    invoke-static {v3, v1}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromAssetTypeAndId(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 106
    .local v0, "assetId":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 122
    .end local v0    # "assetId":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 111
    :cond_0
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->mid:Ljava/lang/String;

    .line 112
    .local v2, "mid":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 113
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    sparse-switch v3, :sswitch_data_0

    .line 122
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 115
    :sswitch_0
    invoke-static {v2}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromMovieMid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 117
    :sswitch_1
    invoke-static {v2}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromShowMid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 113
    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0x12 -> :sswitch_1
    .end sparse-switch
.end method

.method public static idFromAssetTypeAndId(ILjava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "assetType"    # I
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 74
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 75
    sparse-switch p0, :sswitch_data_0

    .line 90
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 77
    :sswitch_0
    invoke-static {p1}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromMovieId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 79
    :sswitch_1
    invoke-static {p1}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromEpisodeId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 81
    :sswitch_2
    invoke-static {p1}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromSeasonId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 83
    :sswitch_3
    invoke-static {p1}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromShowId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 85
    :sswitch_4
    invoke-static {p1}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromAlbumId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 87
    :sswitch_5
    invoke-static {p1}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromSongId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 75
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_4
        0x4 -> :sswitch_5
        0x6 -> :sswitch_0
        0x12 -> :sswitch_3
        0x13 -> :sswitch_2
        0x14 -> :sswitch_1
    .end sparse-switch
.end method

.method public static idFromEidrId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "eidrId"    # Ljava/lang/String;

    .prologue
    .line 38
    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "eidr:movie:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static idFromEpisodeId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "videoId"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "yt:episode:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static idFromMovieId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "videoId"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 29
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "yt:movie:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static idFromMovieMid(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "mid"    # Ljava/lang/String;

    .prologue
    .line 165
    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 166
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "m:movie:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static idFromSeasonId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "seasonId"    # Ljava/lang/String;

    .prologue
    .line 49
    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "yt:season:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static idFromShowId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "showId"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "yt:show:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static idFromShowMid(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "mid"    # Ljava/lang/String;

    .prologue
    .line 170
    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 171
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "m:show:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static idFromSongId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "songId"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sj:song:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static toYtMovieId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "assetResourceId"    # Ljava/lang/String;

    .prologue
    .line 33
    const-string v0, "yt:movie:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "invalid assetResourceId"

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;)V

    .line 34
    const-string v0, "yt:movie:"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
