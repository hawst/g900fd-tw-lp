.class Lcom/google/android/videos/remote/RemoteControl$1;
.super Ljava/lang/Object;
.source "RemoteControl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/remote/RemoteControl;->onError(ILcom/google/android/videos/utils/RetryAction;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/remote/RemoteControl;

.field final synthetic val$error:I

.field final synthetic val$retryAction:Lcom/google/android/videos/utils/RetryAction;


# direct methods
.method constructor <init>(Lcom/google/android/videos/remote/RemoteControl;ILcom/google/android/videos/utils/RetryAction;)V
    .locals 0

    .prologue
    .line 168
    iput-object p1, p0, Lcom/google/android/videos/remote/RemoteControl$1;->this$0:Lcom/google/android/videos/remote/RemoteControl;

    iput p2, p0, Lcom/google/android/videos/remote/RemoteControl$1;->val$error:I

    iput-object p3, p0, Lcom/google/android/videos/remote/RemoteControl$1;->val$retryAction:Lcom/google/android/videos/utils/RetryAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 171
    iget-object v2, p0, Lcom/google/android/videos/remote/RemoteControl$1;->this$0:Lcom/google/android/videos/remote/RemoteControl;

    iget v3, p0, Lcom/google/android/videos/remote/RemoteControl$1;->val$error:I

    # setter for: Lcom/google/android/videos/remote/RemoteControl;->error:I
    invoke-static {v2, v3}, Lcom/google/android/videos/remote/RemoteControl;->access$002(Lcom/google/android/videos/remote/RemoteControl;I)I

    .line 172
    iget-object v2, p0, Lcom/google/android/videos/remote/RemoteControl$1;->this$0:Lcom/google/android/videos/remote/RemoteControl;

    iget-object v3, p0, Lcom/google/android/videos/remote/RemoteControl$1;->val$retryAction:Lcom/google/android/videos/utils/RetryAction;

    # setter for: Lcom/google/android/videos/remote/RemoteControl;->retryAction:Lcom/google/android/videos/utils/RetryAction;
    invoke-static {v2, v3}, Lcom/google/android/videos/remote/RemoteControl;->access$102(Lcom/google/android/videos/remote/RemoteControl;Lcom/google/android/videos/utils/RetryAction;)Lcom/google/android/videos/utils/RetryAction;

    .line 173
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/android/videos/remote/RemoteControl$1;->this$0:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v3}, Lcom/google/android/videos/remote/RemoteControl;->getErrorMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, p0, Lcom/google/android/videos/remote/RemoteControl$1;->val$retryAction:Lcom/google/android/videos/utils/RetryAction;

    if-eqz v2, :cond_0

    const-string v2, " - retry offered"

    :goto_0
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 175
    iget-object v2, p0, Lcom/google/android/videos/remote/RemoteControl$1;->this$0:Lcom/google/android/videos/remote/RemoteControl;

    # invokes: Lcom/google/android/videos/remote/RemoteControl;->getListeners()[Lcom/google/android/videos/remote/RemoteControlListener;
    invoke-static {v2}, Lcom/google/android/videos/remote/RemoteControl;->access$200(Lcom/google/android/videos/remote/RemoteControl;)[Lcom/google/android/videos/remote/RemoteControlListener;

    move-result-object v1

    .line 176
    .local v1, "listeners":[Lcom/google/android/videos/remote/RemoteControlListener;
    array-length v2, v1

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_1
    if-ltz v0, :cond_1

    .line 177
    aget-object v2, v1, v0

    iget v3, p0, Lcom/google/android/videos/remote/RemoteControl$1;->val$error:I

    iget-object v4, p0, Lcom/google/android/videos/remote/RemoteControl$1;->val$retryAction:Lcom/google/android/videos/utils/RetryAction;

    invoke-interface {v2, v3, v4}, Lcom/google/android/videos/remote/RemoteControlListener;->onRemoteControlError(ILcom/google/android/videos/utils/RetryAction;)V

    .line 176
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 173
    .end local v0    # "i":I
    .end local v1    # "listeners":[Lcom/google/android/videos/remote/RemoteControlListener;
    :cond_0
    const-string v2, " - no retry"

    goto :goto_0

    .line 179
    .restart local v0    # "i":I
    .restart local v1    # "listeners":[Lcom/google/android/videos/remote/RemoteControlListener;
    :cond_1
    return-void
.end method
