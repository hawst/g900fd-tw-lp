.class public Lcom/google/android/videos/utils/ImmersionHelper;
.super Ljava/lang/Object;
.source "ImmersionHelper.java"


# static fields
.field private static final setImmersiveMethod:Ljava/lang/reflect/Method;


# instance fields
.field private final activity:Landroid/app/Activity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    invoke-static {}, Lcom/google/android/videos/utils/ImmersionHelper;->getSetImmersiveMethod()Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/google/android/videos/utils/ImmersionHelper;->setImmersiveMethod:Ljava/lang/reflect/Method;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/videos/utils/ImmersionHelper;->activity:Landroid/app/Activity;

    .line 25
    return-void
.end method

.method private static getSetImmersiveMethod()Ljava/lang/reflect/Method;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 47
    sget v3, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v4, 0x12

    if-lt v3, v4, :cond_0

    .line 57
    .local v0, "activityClass":Ljava/lang/Class;, "Ljava/lang/Class<Landroid/app/Activity;>;"
    :goto_0
    return-object v2

    .line 52
    .end local v0    # "activityClass":Ljava/lang/Class;, "Ljava/lang/Class<Landroid/app/Activity;>;"
    :cond_0
    :try_start_0
    const-class v0, Landroid/app/Activity;

    .line 53
    .restart local v0    # "activityClass":Ljava/lang/Class;, "Ljava/lang/Class<Landroid/app/Activity;>;"
    const-string v3, "setImmersive"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 54
    :catch_0
    move-exception v1

    .line 56
    .local v1, "exception":Ljava/lang/NoSuchMethodException;
    const-string v3, "Unable to find setImmersive() method"

    invoke-static {v3}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setImmersiveV18(Z)V
    .locals 1
    .param p1, "isImmersive"    # Z

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/videos/utils/ImmersionHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->setImmersive(Z)V

    .line 44
    return-void
.end method


# virtual methods
.method public setImmersive(Z)V
    .locals 6
    .param p1, "isImmersive"    # Z

    .prologue
    .line 28
    sget v1, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_1

    .line 29
    invoke-direct {p0, p1}, Lcom/google/android/videos/utils/ImmersionHelper;->setImmersiveV18(Z)V

    .line 39
    :cond_0
    :goto_0
    return-void

    .line 30
    :cond_1
    sget-object v1, Lcom/google/android/videos/utils/ImmersionHelper;->setImmersiveMethod:Ljava/lang/reflect/Method;

    if-eqz v1, :cond_0

    .line 32
    :try_start_0
    sget-object v1, Lcom/google/android/videos/utils/ImmersionHelper;->setImmersiveMethod:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/google/android/videos/utils/ImmersionHelper;->activity:Landroid/app/Activity;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 33
    :catch_0
    move-exception v0

    .line 36
    .local v0, "exception":Ljava/lang/Exception;
    const-string v1, "Bad method invocation when calling setImmersive()"

    invoke-static {v1, v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
