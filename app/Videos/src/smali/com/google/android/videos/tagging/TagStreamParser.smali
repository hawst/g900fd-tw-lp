.class final Lcom/google/android/videos/tagging/TagStreamParser;
.super Ljava/lang/Object;
.source "TagStreamParser.java"


# instance fields
.field private final actorFilmography:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Film;",
            ">;"
        }
    .end annotation
.end field

.field private final characterNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final contentLanguage:Ljava/lang/String;

.field private final crops:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[I>;"
        }
    .end annotation
.end field

.field private filmographyIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private framesPerSecond:F

.field private header:Lcom/google/android/videos/proto/TagProtos$Header;

.field private final lastModified:Ljava/lang/String;

.field private parseException:Ljava/io/IOException;

.field private parsed:Z

.field private final source:[B

.field private usedReferencedFilmIndices:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final videoId:Ljava/lang/String;

.field private final videoItag:I

.field private final videoTimestamp:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;[B)V
    .locals 1
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "contentLanguage"    # Ljava/lang/String;
    .param p3, "lastModified"    # Ljava/lang/String;
    .param p4, "videoItag"    # I
    .param p5, "videoTimestamp"    # Ljava/lang/String;
    .param p6, "source"    # [B

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/google/android/videos/tagging/TagStreamParser;->videoId:Ljava/lang/String;

    .line 61
    iput-object p2, p0, Lcom/google/android/videos/tagging/TagStreamParser;->contentLanguage:Ljava/lang/String;

    .line 62
    iput-object p3, p0, Lcom/google/android/videos/tagging/TagStreamParser;->lastModified:Ljava/lang/String;

    .line 63
    iput p4, p0, Lcom/google/android/videos/tagging/TagStreamParser;->videoItag:I

    .line 64
    iput-object p5, p0, Lcom/google/android/videos/tagging/TagStreamParser;->videoTimestamp:Ljava/lang/String;

    .line 65
    iput-object p6, p0, Lcom/google/android/videos/tagging/TagStreamParser;->source:[B

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/tagging/TagStreamParser;->characterNames:Ljava/util/List;

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/tagging/TagStreamParser;->actorFilmography:Ljava/util/List;

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/tagging/TagStreamParser;->crops:Ljava/util/List;

    .line 70
    return-void
.end method

.method private buildActors([Lcom/google/android/videos/proto/FilmProtos$Person;[Lcom/google/android/videos/tagging/KnowledgeEntity$Film;Ljava/util/List;Ljava/util/Map;Ljava/util/Set;)V
    .locals 20
    .param p1, "actorProtos"    # [Lcom/google/android/videos/proto/FilmProtos$Person;
    .param p2, "referencedFilms"    # [Lcom/google/android/videos/tagging/KnowledgeEntity$Film;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/google/android/videos/proto/FilmProtos$Person;",
            "[",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Film;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 309
    .local p3, "knowledgeEntities":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/KnowledgeEntity;>;"
    .local p4, "images":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/videos/tagging/KnowledgeEntity$Image;>;"
    .local p5, "pretchImages":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/videos/tagging/KnowledgeEntity$Image;>;"
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_0
    move-object/from16 v0, p1

    array-length v2, v0

    move/from16 v0, v18

    if-ge v0, v2, :cond_6

    .line 310
    aget-object v13, p1, v18

    .line 311
    .local v13, "actorProto":Lcom/google/android/videos/proto/FilmProtos$Person;
    iget v3, v13, Lcom/google/android/videos/proto/FilmProtos$Person;->localId:I

    .line 312
    .local v3, "localId":I
    iget-object v4, v13, Lcom/google/android/videos/proto/FilmProtos$Person;->splitId:[I

    .line 313
    .local v4, "splitIds":[I
    iget-object v2, v13, Lcom/google/android/videos/proto/FilmProtos$Person;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    if-eqz v2, :cond_2

    iget-object v2, v13, Lcom/google/android/videos/proto/FilmProtos$Person;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "actor."

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v0, v2, v5, v1}, Lcom/google/android/videos/tagging/TagStreamParser;->buildImage(Lcom/google/android/videos/proto/FilmProtos$Image;Ljava/lang/String;Ljava/util/Map;)Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    move-result-object v6

    .line 317
    .local v6, "image":Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
    :goto_1
    if-eqz v6, :cond_0

    .line 318
    move-object/from16 v0, p5

    invoke-interface {v0, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 320
    :cond_0
    iget-object v2, v13, Lcom/google/android/videos/proto/FilmProtos$Person;->appearance:[B

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/videos/tagging/TagStreamParser;->buildAppearances([B)[I

    move-result-object v7

    .line 321
    .local v7, "appearances":[I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/tagging/TagStreamParser;->characterNames:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 322
    iget-object v2, v13, Lcom/google/android/videos/proto/FilmProtos$Person;->character:[Lcom/google/android/videos/proto/FilmProtos$Character;

    array-length v14, v2

    .line 323
    .local v14, "characterCount":I
    const/16 v19, 0x0

    .local v19, "j":I
    :goto_2
    move/from16 v0, v19

    if-ge v0, v14, :cond_3

    .line 324
    iget-object v2, v13, Lcom/google/android/videos/proto/FilmProtos$Person;->character:[Lcom/google/android/videos/proto/FilmProtos$Character;

    aget-object v15, v2, v19

    .line 325
    .local v15, "characterProto":Lcom/google/android/videos/proto/FilmProtos$Character;
    iget-object v2, v15, Lcom/google/android/videos/proto/FilmProtos$Character;->name:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 326
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/tagging/TagStreamParser;->characterNames:Ljava/util/List;

    iget-object v5, v15, Lcom/google/android/videos/proto/FilmProtos$Character;->name:Ljava/lang/String;

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 323
    :cond_1
    add-int/lit8 v19, v19, 0x1

    goto :goto_2

    .line 313
    .end local v6    # "image":Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
    .end local v7    # "appearances":[I
    .end local v14    # "characterCount":I
    .end local v15    # "characterProto":Lcom/google/android/videos/proto/FilmProtos$Character;
    .end local v19    # "j":I
    :cond_2
    const/4 v6, 0x0

    goto :goto_1

    .line 329
    .restart local v6    # "image":Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
    .restart local v7    # "appearances":[I
    .restart local v14    # "characterCount":I
    .restart local v19    # "j":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/tagging/TagStreamParser;->actorFilmography:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 330
    iget-object v2, v13, Lcom/google/android/videos/proto/FilmProtos$Person;->filmographyIndex:[I

    array-length v0, v2

    move/from16 v17, v0

    .line 331
    .local v17, "filmographyIndexCount":I
    const/16 v19, 0x0

    :goto_3
    move/from16 v0, v19

    move/from16 v1, v17

    if-ge v0, v1, :cond_5

    .line 332
    iget-object v2, v13, Lcom/google/android/videos/proto/FilmProtos$Person;->filmographyIndex:[I

    aget v16, v2, v19

    .line 333
    .local v16, "filmographyIndex":I
    if-ltz v16, :cond_4

    move-object/from16 v0, p2

    array-length v2, v0

    move/from16 v0, v16

    if-ge v0, v2, :cond_4

    .line 334
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/tagging/TagStreamParser;->actorFilmography:Ljava/util/List;

    aget-object v5, p2, v16

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 335
    const/4 v2, 0x5

    move/from16 v0, v19

    if-ge v0, v2, :cond_4

    aget-object v2, p2, v16

    iget-object v2, v2, Lcom/google/android/videos/tagging/KnowledgeEntity$Film;->image:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    if-eqz v2, :cond_4

    .line 337
    aget-object v2, p2, v16

    iget-object v2, v2, Lcom/google/android/videos/tagging/KnowledgeEntity$Film;->image:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 331
    :cond_4
    add-int/lit8 v19, v19, 0x1

    goto :goto_3

    .line 341
    .end local v16    # "filmographyIndex":I
    :cond_5
    new-instance v2, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    iget-object v5, v13, Lcom/google/android/videos/proto/FilmProtos$Person;->name:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/videos/tagging/TagStreamParser;->characterNames:Ljava/util/List;

    iget-object v9, v13, Lcom/google/android/videos/proto/FilmProtos$Person;->dateOfBirth:Ljava/lang/String;

    iget-object v10, v13, Lcom/google/android/videos/proto/FilmProtos$Person;->dateOfDeath:Ljava/lang/String;

    iget-object v11, v13, Lcom/google/android/videos/proto/FilmProtos$Person;->placeOfBirth:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/tagging/TagStreamParser;->actorFilmography:Ljava/util/List;

    invoke-direct/range {v2 .. v12}, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;-><init>(I[ILjava/lang/String;Lcom/google/android/videos/tagging/KnowledgeEntity$Image;[ILjava/util/Collection;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;)V

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 352
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/tagging/TagStreamParser;->characterNames:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 353
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/tagging/TagStreamParser;->actorFilmography:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 309
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_0

    .line 355
    .end local v3    # "localId":I
    .end local v4    # "splitIds":[I
    .end local v6    # "image":Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
    .end local v7    # "appearances":[I
    .end local v13    # "actorProto":Lcom/google/android/videos/proto/FilmProtos$Person;
    .end local v14    # "characterCount":I
    .end local v17    # "filmographyIndexCount":I
    .end local v19    # "j":I
    :cond_6
    return-void
.end method

.method private buildAppearances([B)[I
    .locals 9
    .param p1, "appearanceByteString"    # [B

    .prologue
    const/4 v7, 0x0

    .line 443
    if-eqz p1, :cond_0

    array-length v8, p1

    if-nez v8, :cond_2

    :cond_0
    move-object v1, v7

    .line 466
    :cond_1
    :goto_0
    return-object v1

    .line 447
    :cond_2
    :try_start_0
    invoke-static {p1}, Lcom/google/android/videos/proto/FilmProtos$Appearance;->parseFrom([B)Lcom/google/android/videos/proto/FilmProtos$Appearance;

    move-result-object v0

    .line 449
    .local v0, "appearanceProto":Lcom/google/android/videos/proto/FilmProtos$Appearance;
    iget-object v8, v0, Lcom/google/android/videos/proto/FilmProtos$Appearance;->offset:[I

    array-length v5, v8

    .line 450
    .local v5, "offsetCount":I
    if-nez v5, :cond_3

    move-object v1, v7

    .line 451
    goto :goto_0

    .line 453
    :cond_3
    new-array v1, v5, [I

    .line 454
    .local v1, "appearances":[I
    const/4 v6, 0x0

    .line 455
    .local v6, "sum":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v5, :cond_1

    .line 456
    iget-object v8, v0, Lcom/google/android/videos/proto/FilmProtos$Appearance;->offset:[I

    aget v4, v8, v3

    .line 457
    .local v4, "offset":I
    if-gez v4, :cond_4

    move-object v1, v7

    .line 459
    goto :goto_0

    .line 461
    :cond_4
    add-int/2addr v6, v4

    .line 462
    invoke-direct {p0, v6}, Lcom/google/android/videos/tagging/TagStreamParser;->toMillis(I)I

    move-result v8

    aput v8, v1, v3
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 455
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 465
    .end local v0    # "appearanceProto":Lcom/google/android/videos/proto/FilmProtos$Appearance;
    .end local v1    # "appearances":[I
    .end local v3    # "i":I
    .end local v4    # "offset":I
    .end local v5    # "offsetCount":I
    .end local v6    # "sum":I
    :catch_0
    move-exception v2

    .local v2, "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    move-object v1, v7

    .line 466
    goto :goto_0
.end method

.method private buildChunks(I)Ljava/util/Collection;
    .locals 12
    .param p1, "sourceLength"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/videos/tagging/TagStream$ChunkInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 507
    iget-object v9, p0, Lcom/google/android/videos/tagging/TagStreamParser;->header:Lcom/google/android/videos/proto/TagProtos$Header;

    iget-object v9, v9, Lcom/google/android/videos/proto/TagProtos$Header;->chunk:[Lcom/google/android/videos/proto/TagProtos$Chunk;

    array-length v1, v9

    .line 508
    .local v1, "chunkCount":I
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 509
    .local v2, "chunks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/TagStream$ChunkInfo;>;"
    iget-object v9, p0, Lcom/google/android/videos/tagging/TagStreamParser;->header:Lcom/google/android/videos/proto/TagProtos$Header;

    iget-object v9, v9, Lcom/google/android/videos/proto/TagProtos$Header;->chunk:[Lcom/google/android/videos/proto/TagProtos$Chunk;

    const/4 v10, 0x0

    aget-object v3, v9, v10

    .line 510
    .local v3, "firstChunkProto":Lcom/google/android/videos/proto/TagProtos$Chunk;
    iget v9, v3, Lcom/google/android/videos/proto/TagProtos$Chunk;->start:I

    invoke-direct {p0, v9}, Lcom/google/android/videos/tagging/TagStreamParser;->toMillis(I)I

    move-result v8

    .line 511
    .local v8, "startMillis":I
    iget v0, v3, Lcom/google/android/videos/proto/TagProtos$Chunk;->byteOffset:I

    .line 512
    .local v0, "byteOffset":I
    const/4 v4, 0x1

    .local v4, "i":I
    :goto_0
    if-ge v4, v1, :cond_0

    .line 513
    iget-object v9, p0, Lcom/google/android/videos/tagging/TagStreamParser;->header:Lcom/google/android/videos/proto/TagProtos$Header;

    iget-object v9, v9, Lcom/google/android/videos/proto/TagProtos$Header;->chunk:[Lcom/google/android/videos/proto/TagProtos$Chunk;

    aget-object v6, v9, v4

    .line 514
    .local v6, "nextChunkProto":Lcom/google/android/videos/proto/TagProtos$Chunk;
    iget v9, v6, Lcom/google/android/videos/proto/TagProtos$Chunk;->start:I

    invoke-direct {p0, v9}, Lcom/google/android/videos/tagging/TagStreamParser;->toMillis(I)I

    move-result v7

    .line 515
    .local v7, "nextStartMillis":I
    iget v5, v6, Lcom/google/android/videos/proto/TagProtos$Chunk;->byteOffset:I

    .line 516
    .local v5, "nextByteOffset":I
    new-instance v9, Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

    sub-int v10, v5, v0

    invoke-direct {v9, v8, v7, v0, v10}, Lcom/google/android/videos/tagging/TagStream$ChunkInfo;-><init>(IIII)V

    invoke-interface {v2, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 518
    move v8, v7

    .line 519
    move v0, v5

    .line 512
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 521
    .end local v5    # "nextByteOffset":I
    .end local v6    # "nextChunkProto":Lcom/google/android/videos/proto/TagProtos$Chunk;
    .end local v7    # "nextStartMillis":I
    :cond_0
    new-instance v9, Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

    const v10, 0x7fffffff

    sub-int v11, p1, v0

    invoke-direct {v9, v8, v10, v0, v11}, Lcom/google/android/videos/tagging/TagStream$ChunkInfo;-><init>(IIII)V

    invoke-interface {v2, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 523
    return-object v2
.end method

.method private buildImage(Lcom/google/android/videos/proto/FilmProtos$Image;Ljava/lang/String;Ljava/util/Map;)Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
    .locals 10
    .param p1, "imageProto"    # Lcom/google/android/videos/proto/FilmProtos$Image;
    .param p2, "localImageId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/proto/FilmProtos$Image;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            ">;)",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;"
        }
    .end annotation

    .prologue
    .line 424
    .local p3, "imagesByUrl":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/videos/tagging/KnowledgeEntity$Image;>;"
    iget-object v2, p1, Lcom/google/android/videos/proto/FilmProtos$Image;->url:Ljava/lang/String;

    .line 425
    .local v2, "url":Ljava/lang/String;
    invoke-interface {p3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 426
    invoke-interface {p3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    .line 439
    :goto_0
    return-object v1

    .line 428
    :cond_0
    iget-object v1, p1, Lcom/google/android/videos/proto/FilmProtos$Image;->referrerUrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p1, Lcom/google/android/videos/proto/FilmProtos$Image;->referrerUrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    .line 430
    .local v3, "source":Ljava/lang/String;
    :goto_1
    iget-object v1, p0, Lcom/google/android/videos/tagging/TagStreamParser;->crops:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 431
    iget-object v1, p1, Lcom/google/android/videos/proto/FilmProtos$Image;->cropRegion:[Lcom/google/android/videos/proto/FilmProtos$Rectangle;

    array-length v6, v1

    .line 432
    .local v6, "cropRegionCount":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_2
    if-ge v7, v6, :cond_2

    .line 433
    iget-object v1, p1, Lcom/google/android/videos/proto/FilmProtos$Image;->cropRegion:[Lcom/google/android/videos/proto/FilmProtos$Rectangle;

    aget-object v8, v1, v7

    .line 434
    .local v8, "rect":Lcom/google/android/videos/proto/FilmProtos$Rectangle;
    iget-object v1, p0, Lcom/google/android/videos/tagging/TagStreamParser;->crops:Ljava/util/List;

    const/4 v4, 0x4

    new-array v4, v4, [I

    const/4 v5, 0x0

    iget v9, v8, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->left:I

    aput v9, v4, v5

    const/4 v5, 0x1

    iget v9, v8, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->top:I

    aput v9, v4, v5

    const/4 v5, 0x2

    iget v9, v8, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->right:I

    aput v9, v4, v5

    const/4 v5, 0x3

    iget v9, v8, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->bottom:I

    aput v9, v4, v5

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 432
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 428
    .end local v3    # "source":Ljava/lang/String;
    .end local v6    # "cropRegionCount":I
    .end local v7    # "i":I
    .end local v8    # "rect":Lcom/google/android/videos/proto/FilmProtos$Rectangle;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    .line 436
    .restart local v3    # "source":Ljava/lang/String;
    .restart local v6    # "cropRegionCount":I
    .restart local v7    # "i":I
    :cond_2
    new-instance v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/videos/tagging/TagStreamParser;->crops:Ljava/util/List;

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;FLjava/util/Collection;)V

    .line 437
    .local v0, "image":Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
    iget-object v1, p0, Lcom/google/android/videos/tagging/TagStreamParser;->crops:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 438
    invoke-interface {p3, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 439
    goto :goto_0
.end method

.method private buildKnowledgeEntities(Lcom/google/android/videos/proto/FilmProtos$Film;[Lcom/google/android/videos/tagging/KnowledgeEntity$Film;Ljava/util/Map;Ljava/util/Set;)Ljava/util/List;
    .locals 6
    .param p1, "filmProto"    # Lcom/google/android/videos/proto/FilmProtos$Film;
    .param p2, "referencedFilms"    # [Lcom/google/android/videos/tagging/KnowledgeEntity$Film;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/proto/FilmProtos$Film;",
            "[",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Film;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 245
    .local p3, "images":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/videos/tagging/KnowledgeEntity$Image;>;"
    .local p4, "prefetchImages":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/videos/tagging/KnowledgeEntity$Image;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 246
    .local v3, "knowledgeEntities":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/KnowledgeEntity;>;"
    iget-object v1, p1, Lcom/google/android/videos/proto/FilmProtos$Film;->actor:[Lcom/google/android/videos/proto/FilmProtos$Person;

    move-object v0, p0

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/tagging/TagStreamParser;->buildActors([Lcom/google/android/videos/proto/FilmProtos$Person;[Lcom/google/android/videos/tagging/KnowledgeEntity$Film;Ljava/util/List;Ljava/util/Map;Ljava/util/Set;)V

    .line 247
    iget-object v0, p1, Lcom/google/android/videos/proto/FilmProtos$Film;->song:[Lcom/google/android/videos/proto/FilmProtos$Song;

    invoke-direct {p0, v0, v3, p3, p4}, Lcom/google/android/videos/tagging/TagStreamParser;->buildSongs([Lcom/google/android/videos/proto/FilmProtos$Song;Ljava/util/List;Ljava/util/Map;Ljava/util/Set;)V

    .line 248
    return-object v3
.end method

.method private buildReferencedFilms([Lcom/google/android/videos/proto/FilmProtos$Film;Ljava/util/Set;Ljava/util/Map;ILjava/util/Map;)[Lcom/google/android/videos/tagging/KnowledgeEntity$Film;
    .locals 16
    .param p1, "referencedFilmProtos"    # [Lcom/google/android/videos/proto/FilmProtos$Film;
    .param p4, "desiredPosterWidth"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/google/android/videos/proto/FilmProtos$Film;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            ">;)[",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Film;"
        }
    .end annotation

    .prologue
    .line 171
    .local p2, "usedReferencedFilmIndices":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .local p3, "assetResources":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/AssetResource;>;"
    .local p5, "images":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/videos/tagging/KnowledgeEntity$Image;>;"
    move-object/from16 v0, p1

    array-length v15, v0

    .line 172
    .local v15, "size":I
    new-array v14, v15, [Lcom/google/android/videos/tagging/KnowledgeEntity$Film;

    .line 173
    .local v14, "referencedFilms":[Lcom/google/android/videos/tagging/KnowledgeEntity$Film;
    invoke-interface/range {p2 .. p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    .line 174
    .local v10, "i":Ljava/lang/Integer;
    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aget-object v13, p1, v1

    .line 175
    .local v13, "referencedFilmProto":Lcom/google/android/videos/proto/FilmProtos$Film;
    iget-object v12, v13, Lcom/google/android/videos/proto/FilmProtos$Film;->mid:Ljava/lang/String;

    .line 176
    .local v12, "mid":Ljava/lang/String;
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v2, 0x0

    .line 177
    .local v2, "assetResource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :goto_1
    if-eqz v2, :cond_1

    iget-object v1, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v1, :cond_1

    iget-object v1, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v9, v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    .line 179
    .local v9, "youTubeId":Ljava/lang/String;
    :goto_2
    iget-boolean v1, v13, Lcom/google/android/videos/proto/FilmProtos$Film;->isTv:Z

    if-eqz v1, :cond_2

    .line 180
    const/4 v3, 0x1

    const/high16 v5, 0x3f800000    # 1.0f

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "poster."

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v1, p0

    move/from16 v4, p4

    move-object/from16 v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/google/android/videos/tagging/TagStreamParser;->getPosterFromAssetResource(Lcom/google/wireless/android/video/magma/proto/AssetResource;IIFLjava/lang/String;Ljava/util/Map;)Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    move-result-object v8

    .line 183
    .local v8, "image":Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v1

    new-instance v3, Lcom/google/android/videos/tagging/KnowledgeEntity$TvShow;

    iget-object v4, v13, Lcom/google/android/videos/proto/FilmProtos$Film;->title:Ljava/lang/String;

    iget-object v5, v13, Lcom/google/android/videos/proto/FilmProtos$Film;->releaseDate:Ljava/lang/String;

    iget-object v6, v13, Lcom/google/android/videos/proto/FilmProtos$Film;->endDate:Ljava/lang/String;

    iget-object v7, v13, Lcom/google/android/videos/proto/FilmProtos$Film;->googlePlayUrl:Ljava/lang/String;

    invoke-direct/range {v3 .. v9}, Lcom/google/android/videos/tagging/KnowledgeEntity$TvShow;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Ljava/lang/String;)V

    aput-object v3, v14, v1

    goto :goto_0

    .line 176
    .end local v2    # "assetResource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .end local v8    # "image":Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
    .end local v9    # "youTubeId":Ljava/lang/String;
    :cond_0
    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-object v2, v1

    goto :goto_1

    .line 177
    .restart local v2    # "assetResource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :cond_1
    const/4 v9, 0x0

    goto :goto_2

    .line 191
    .restart local v9    # "youTubeId":Ljava/lang/String;
    :cond_2
    const/4 v3, 0x3

    const v5, 0x3f31a787

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "poster."

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v1, p0

    move/from16 v4, p4

    move-object/from16 v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/google/android/videos/tagging/TagStreamParser;->getPosterFromAssetResource(Lcom/google/wireless/android/video/magma/proto/AssetResource;IIFLjava/lang/String;Ljava/util/Map;)Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    move-result-object v8

    .line 194
    .restart local v8    # "image":Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v1

    new-instance v3, Lcom/google/android/videos/tagging/KnowledgeEntity$Movie;

    iget-object v4, v13, Lcom/google/android/videos/proto/FilmProtos$Film;->title:Ljava/lang/String;

    iget-object v5, v13, Lcom/google/android/videos/proto/FilmProtos$Film;->releaseDate:Ljava/lang/String;

    iget v6, v13, Lcom/google/android/videos/proto/FilmProtos$Film;->runtimeMinutes:I

    iget-object v7, v13, Lcom/google/android/videos/proto/FilmProtos$Film;->googlePlayUrl:Ljava/lang/String;

    invoke-direct/range {v3 .. v9}, Lcom/google/android/videos/tagging/KnowledgeEntity$Movie;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Ljava/lang/String;)V

    aput-object v3, v14, v1

    goto/16 :goto_0

    .line 203
    .end local v2    # "assetResource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .end local v8    # "image":Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
    .end local v9    # "youTubeId":Ljava/lang/String;
    .end local v10    # "i":Ljava/lang/Integer;
    .end local v12    # "mid":Ljava/lang/String;
    .end local v13    # "referencedFilmProto":Lcom/google/android/videos/proto/FilmProtos$Film;
    :cond_3
    return-object v14
.end method

.method private buildSongs([Lcom/google/android/videos/proto/FilmProtos$Song;Ljava/util/List;Ljava/util/Map;Ljava/util/Set;)V
    .locals 10
    .param p1, "songProtos"    # [Lcom/google/android/videos/proto/FilmProtos$Song;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/google/android/videos/proto/FilmProtos$Song;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "knowledgeEntities":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/KnowledgeEntity;>;"
    .local p3, "images":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/videos/tagging/KnowledgeEntity$Image;>;"
    .local p4, "prefetchImages":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/videos/tagging/KnowledgeEntity$Image;>;"
    const/4 v9, 0x0

    .line 394
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    array-length v0, p1

    if-ge v7, v0, :cond_3

    .line 395
    aget-object v8, p1, v7

    .line 396
    .local v8, "songProto":Lcom/google/android/videos/proto/FilmProtos$Song;
    iget v1, v8, Lcom/google/android/videos/proto/FilmProtos$Song;->localId:I

    .line 397
    .local v1, "localId":I
    iget-object v0, v8, Lcom/google/android/videos/proto/FilmProtos$Song;->albumArt:Lcom/google/android/videos/proto/FilmProtos$Image;

    if-eqz v0, :cond_1

    iget-object v0, v8, Lcom/google/android/videos/proto/FilmProtos$Song;->albumArt:Lcom/google/android/videos/proto/FilmProtos$Image;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "song."

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2, p3}, Lcom/google/android/videos/tagging/TagStreamParser;->buildImage(Lcom/google/android/videos/proto/FilmProtos$Image;Ljava/lang/String;Ljava/util/Map;)Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    move-result-object v3

    .line 401
    .local v3, "albumArt":Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
    :goto_1
    if-eqz v3, :cond_0

    .line 402
    invoke-interface {p4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 404
    :cond_0
    iget-object v0, v8, Lcom/google/android/videos/proto/FilmProtos$Song;->appearance:[B

    array-length v0, v0

    if-eqz v0, :cond_2

    iget-object v0, v8, Lcom/google/android/videos/proto/FilmProtos$Song;->appearance:[B

    invoke-direct {p0, v0}, Lcom/google/android/videos/tagging/TagStreamParser;->buildAppearances([B)[I

    move-result-object v4

    .line 406
    .local v4, "appearances":[I
    :goto_2
    new-instance v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;

    iget-object v2, v8, Lcom/google/android/videos/proto/FilmProtos$Song;->title:Ljava/lang/String;

    iget-object v5, v8, Lcom/google/android/videos/proto/FilmProtos$Song;->performer:Ljava/lang/String;

    iget-object v6, v8, Lcom/google/android/videos/proto/FilmProtos$Song;->googlePlayUrl:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;-><init>(ILjava/lang/String;Lcom/google/android/videos/tagging/KnowledgeEntity$Image;[ILjava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 394
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .end local v3    # "albumArt":Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
    .end local v4    # "appearances":[I
    :cond_1
    move-object v3, v9

    .line 397
    goto :goto_1

    .restart local v3    # "albumArt":Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
    :cond_2
    move-object v4, v9

    .line 404
    goto :goto_2

    .line 414
    .end local v1    # "localId":I
    .end local v3    # "albumArt":Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
    .end local v8    # "songProto":Lcom/google/android/videos/proto/FilmProtos$Song;
    :cond_3
    return-void
.end method

.method private getPosterFromAssetResource(Lcom/google/wireless/android/video/magma/proto/AssetResource;IIFLjava/lang/String;Ljava/util/Map;)Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
    .locals 8
    .param p1, "assetResource"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p2, "imageType"    # I
    .param p3, "desiredWidth"    # I
    .param p4, "aspectRatio"    # F
    .param p5, "localImageId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            "IIF",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            ">;)",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;"
        }
    .end annotation

    .prologue
    .local p6, "imagesByUrl":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/videos/tagging/KnowledgeEntity$Image;>;"
    const/4 v3, 0x0

    .line 209
    const/4 v2, 0x0

    .line 210
    .local v2, "url":Ljava/lang/String;
    const/4 v7, 0x0

    .line 211
    .local v7, "imageProto":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    if-eqz p1, :cond_0

    iget-object v1, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    if-eqz v1, :cond_0

    .line 212
    int-to-float v1, p3

    div-float/2addr v1, p4

    float-to-int v6, v1

    .line 213
    .local v6, "desiredHeight":I
    iget-object v1, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v1, v1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    const/4 v4, 0x0

    invoke-static {v1, p2, p3, v6, v4}, Lcom/google/android/videos/api/AssetResourceUtil;->findBestImage([Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;IIIF)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    move-result-object v7

    .line 215
    const/4 v1, 0x0

    invoke-static {v7, p3, v6, v1}, Lcom/google/android/videos/api/AssetResourceUtil;->getImageUrl(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;IIZ)Ljava/lang/String;

    move-result-object v2

    .line 217
    .end local v6    # "desiredHeight":I
    :cond_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 225
    :goto_0
    return-object v3

    .line 219
    :cond_1
    invoke-interface {p6, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 220
    invoke-interface {p6, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    move-object v3, v1

    goto :goto_0

    .line 222
    :cond_2
    new-instance v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    iget v1, v7, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->width:I

    int-to-float v1, v1

    const/high16 v4, 0x3f800000    # 1.0f

    mul-float/2addr v1, v4

    iget v4, v7, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->height:I

    int-to-float v4, v4

    div-float v4, v1, v4

    iget-object v5, p0, Lcom/google/android/videos/tagging/TagStreamParser;->crops:Ljava/util/List;

    move-object v1, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;FLjava/util/Collection;)V

    .line 224
    .local v0, "image":Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
    invoke-interface {p6, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v3, v0

    .line 225
    goto :goto_0
.end method

.method private parseActors([Lcom/google/android/videos/proto/FilmProtos$Person;ILjava/util/Set;Ljava/util/Set;)Ljava/util/Set;
    .locals 13
    .param p1, "actorProtos"    # [Lcom/google/android/videos/proto/FilmProtos$Person;
    .param p2, "referencedFilmCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/google/android/videos/proto/FilmProtos$Person;",
            "I",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 261
    .local p3, "seenLocalIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .local p4, "seenSplitIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    .line 262
    .local v9, "usedReferencedFilmIndices":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v10, p1

    if-ge v4, v10, :cond_8

    .line 263
    aget-object v1, p1, v4

    .line 264
    .local v1, "actorProto":Lcom/google/android/videos/proto/FilmProtos$Person;
    iget v6, v1, Lcom/google/android/videos/proto/FilmProtos$Person;->localId:I

    .line 265
    .local v6, "localId":I
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 266
    new-instance v10, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Actor invalid - duplicate local ID "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 269
    :cond_0
    iget-object v10, v1, Lcom/google/android/videos/proto/FilmProtos$Person;->splitId:[I

    array-length v8, v10

    .line 270
    .local v8, "splitIdCount":I
    if-nez v8, :cond_1

    .line 271
    new-instance v10, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Actor invalid - no split IDs for LID "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 274
    :cond_1
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_1
    if-ge v5, v8, :cond_3

    .line 275
    iget-object v10, v1, Lcom/google/android/videos/proto/FilmProtos$Person;->splitId:[I

    aget v10, v10, v5

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 276
    .local v7, "splitId":Ljava/lang/Integer;
    move-object/from16 v0, p4

    invoke-interface {v0, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 277
    new-instance v10, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Actor invalid - duplicate split ID "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 274
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 281
    .end local v7    # "splitId":Ljava/lang/Integer;
    :cond_3
    iget-object v10, v1, Lcom/google/android/videos/proto/FilmProtos$Person;->name:Ljava/lang/String;

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 282
    new-instance v10, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Actor invalid - no name for LID "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 284
    :cond_4
    iget-object v10, v1, Lcom/google/android/videos/proto/FilmProtos$Person;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    if-eqz v10, :cond_5

    .line 285
    iget-object v10, v1, Lcom/google/android/videos/proto/FilmProtos$Person;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    iget-object v11, v1, Lcom/google/android/videos/proto/FilmProtos$Person;->name:Ljava/lang/String;

    invoke-direct {p0, v10, v11}, Lcom/google/android/videos/tagging/TagStreamParser;->parseImage(Lcom/google/android/videos/proto/FilmProtos$Image;Ljava/lang/String;)V

    .line 287
    :cond_5
    iget-object v10, v1, Lcom/google/android/videos/proto/FilmProtos$Person;->filmographyIndex:[I

    array-length v3, v10

    .line 288
    .local v3, "filmographyIndexCount":I
    const/4 v5, 0x0

    :goto_2
    if-ge v5, v3, :cond_7

    .line 289
    iget-object v10, v1, Lcom/google/android/videos/proto/FilmProtos$Person;->filmographyIndex:[I

    aget v2, v10, v5

    .line 290
    .local v2, "filmographyIndex":I
    if-ltz v2, :cond_6

    if-ge v2, p2, :cond_6

    .line 291
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 288
    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 262
    .end local v2    # "filmographyIndex":I
    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 295
    .end local v1    # "actorProto":Lcom/google/android/videos/proto/FilmProtos$Person;
    .end local v3    # "filmographyIndexCount":I
    .end local v5    # "j":I
    .end local v6    # "localId":I
    .end local v8    # "splitIdCount":I
    :cond_8
    return-object v9
.end method

.method private parseChunks(I)V
    .locals 10
    .param p1, "sourceLength"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 471
    iget-object v7, p0, Lcom/google/android/videos/tagging/TagStreamParser;->header:Lcom/google/android/videos/proto/TagProtos$Header;

    iget-object v7, v7, Lcom/google/android/videos/proto/TagProtos$Header;->chunk:[Lcom/google/android/videos/proto/TagProtos$Chunk;

    array-length v1, v7

    .line 472
    .local v1, "chunkCount":I
    if-nez v1, :cond_0

    .line 473
    new-instance v7, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;

    const-string v8, "Header invalid - no chunks defined"

    invoke-direct {v7, v8}, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 475
    :cond_0
    const/high16 v5, -0x80000000

    .line 476
    .local v5, "lastStart":I
    const/high16 v4, -0x80000000

    .line 477
    .local v4, "lastByteOffset":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_6

    .line 478
    iget-object v7, p0, Lcom/google/android/videos/tagging/TagStreamParser;->header:Lcom/google/android/videos/proto/TagProtos$Header;

    iget-object v7, v7, Lcom/google/android/videos/proto/TagProtos$Header;->chunk:[Lcom/google/android/videos/proto/TagProtos$Chunk;

    aget-object v2, v7, v3

    .line 479
    .local v2, "chunkProto":Lcom/google/android/videos/proto/TagProtos$Chunk;
    iget v7, v2, Lcom/google/android/videos/proto/TagProtos$Chunk;->start:I

    if-gez v7, :cond_1

    .line 480
    new-instance v7, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Header invalid - chunk["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]\'s start invalid"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 483
    :cond_1
    iget v6, v2, Lcom/google/android/videos/proto/TagProtos$Chunk;->start:I

    .line 484
    .local v6, "start":I
    if-gt v6, v5, :cond_2

    .line 485
    new-instance v7, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Header invalid - chunk["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]\'s start not increasing"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 488
    :cond_2
    move v5, v6

    .line 489
    iget v7, v2, Lcom/google/android/videos/proto/TagProtos$Chunk;->byteOffset:I

    if-gtz v7, :cond_3

    .line 490
    new-instance v7, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Header invalid - chunk["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]\'s byte offset invalid"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 493
    :cond_3
    iget v0, v2, Lcom/google/android/videos/proto/TagProtos$Chunk;->byteOffset:I

    .line 494
    .local v0, "byteOffset":I
    if-gt v0, v4, :cond_4

    .line 495
    new-instance v7, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Header invalid - chunk["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]\'s byte offset not increasing"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 498
    :cond_4
    if-lt v0, p1, :cond_5

    .line 499
    new-instance v7, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Header invalid - chunk["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]\'s byte offset beyond file size"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 502
    :cond_5
    move v4, v0

    .line 477
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 504
    .end local v0    # "byteOffset":I
    .end local v2    # "chunkProto":Lcom/google/android/videos/proto/TagProtos$Chunk;
    .end local v6    # "start":I
    :cond_6
    return-void
.end method

.method private parseImage(Lcom/google/android/videos/proto/FilmProtos$Image;Ljava/lang/String;)V
    .locals 3
    .param p1, "imageProto"    # Lcom/google/android/videos/proto/FilmProtos$Image;
    .param p2, "what"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 417
    iget-object v0, p1, Lcom/google/android/videos/proto/FilmProtos$Image;->url:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 418
    new-instance v0, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Image of \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' invalid - no url"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 420
    :cond_0
    return-void
.end method

.method private parseKnowledgeEntities(Lcom/google/android/videos/proto/FilmProtos$Film;)Ljava/util/Set;
    .locals 5
    .param p1, "filmProto"    # Lcom/google/android/videos/proto/FilmProtos$Film;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/proto/FilmProtos$Film;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 231
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 232
    .local v0, "seenLocalIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 233
    .local v1, "seenSplitIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    iget-object v3, p1, Lcom/google/android/videos/proto/FilmProtos$Film;->actor:[Lcom/google/android/videos/proto/FilmProtos$Person;

    iget-object v4, p1, Lcom/google/android/videos/proto/FilmProtos$Film;->referencedFilm:[Lcom/google/android/videos/proto/FilmProtos$Film;

    array-length v4, v4

    invoke-direct {p0, v3, v4, v0, v1}, Lcom/google/android/videos/tagging/TagStreamParser;->parseActors([Lcom/google/android/videos/proto/FilmProtos$Person;ILjava/util/Set;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    .line 235
    .local v2, "usedReferencedFilmIndices":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    iget-object v3, p1, Lcom/google/android/videos/proto/FilmProtos$Film;->song:[Lcom/google/android/videos/proto/FilmProtos$Song;

    invoke-direct {p0, v3, v0, v1}, Lcom/google/android/videos/tagging/TagStreamParser;->parseSongs([Lcom/google/android/videos/proto/FilmProtos$Song;Ljava/util/Set;Ljava/util/Set;)V

    .line 236
    return-object v2
.end method

.method private parseReferencedFilms([Lcom/google/android/videos/proto/FilmProtos$Film;Ljava/util/Set;Ljava/util/List;)V
    .locals 6
    .param p1, "referencedFilmProtos"    # [Lcom/google/android/videos/proto/FilmProtos$Film;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/google/android/videos/proto/FilmProtos$Film;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 147
    .local p2, "usedReferencedFilmIndices":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .local p3, "filmography":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 148
    .local v0, "i":Ljava/lang/Integer;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    aget-object v3, p1, v4

    .line 149
    .local v3, "referencedFilmProto":Lcom/google/android/videos/proto/FilmProtos$Film;
    iget-object v4, v3, Lcom/google/android/videos/proto/FilmProtos$Film;->title:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 150
    new-instance v4, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;

    const-string v5, "Referenced film invalid - no title"

    invoke-direct {v4, v5}, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 152
    :cond_1
    iget-object v4, v3, Lcom/google/android/videos/proto/FilmProtos$Film;->mid:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 153
    iget-object v2, v3, Lcom/google/android/videos/proto/FilmProtos$Film;->mid:Ljava/lang/String;

    .line 154
    .local v2, "mid":Ljava/lang/String;
    iget-boolean v4, v3, Lcom/google/android/videos/proto/FilmProtos$Film;->isTv:Z

    if-eqz v4, :cond_2

    .line 155
    invoke-static {v2}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromShowMid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 157
    :cond_2
    invoke-static {v2}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromMovieMid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 161
    .end local v0    # "i":Ljava/lang/Integer;
    .end local v2    # "mid":Ljava/lang/String;
    .end local v3    # "referencedFilmProto":Lcom/google/android/videos/proto/FilmProtos$Film;
    :cond_3
    return-void
.end method

.method private parseSongs([Lcom/google/android/videos/proto/FilmProtos$Song;Ljava/util/Set;Ljava/util/Set;)V
    .locals 6
    .param p1, "songProtos"    # [Lcom/google/android/videos/proto/FilmProtos$Song;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/google/android/videos/proto/FilmProtos$Song;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 365
    .local p2, "seenLocalIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .local p3, "seenSplitIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_4

    .line 366
    aget-object v2, p1, v0

    .line 367
    .local v2, "songProto":Lcom/google/android/videos/proto/FilmProtos$Song;
    iget v1, v2, Lcom/google/android/videos/proto/FilmProtos$Song;->localId:I

    .line 368
    .local v1, "localId":I
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 369
    new-instance v3, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Song invalid - duplicate local ID "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 372
    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {p3, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 373
    new-instance v3, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Song invalid - duplicate split ID "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 376
    :cond_1
    iget-object v3, v2, Lcom/google/android/videos/proto/FilmProtos$Song;->title:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 377
    new-instance v3, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Song invalid - no title for LID "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 379
    :cond_2
    iget-object v3, v2, Lcom/google/android/videos/proto/FilmProtos$Song;->albumArt:Lcom/google/android/videos/proto/FilmProtos$Image;

    if-eqz v3, :cond_3

    .line 380
    iget-object v3, v2, Lcom/google/android/videos/proto/FilmProtos$Song;->albumArt:Lcom/google/android/videos/proto/FilmProtos$Image;

    iget-object v4, v2, Lcom/google/android/videos/proto/FilmProtos$Song;->title:Ljava/lang/String;

    invoke-direct {p0, v3, v4}, Lcom/google/android/videos/tagging/TagStreamParser;->parseImage(Lcom/google/android/videos/proto/FilmProtos$Image;Ljava/lang/String;)V

    .line 365
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 383
    .end local v1    # "localId":I
    .end local v2    # "songProto":Lcom/google/android/videos/proto/FilmProtos$Song;
    :cond_4
    return-void
.end method

.method private toMillis(I)I
    .locals 2
    .param p1, "frame"    # I

    .prologue
    .line 527
    int-to-float v0, p1

    iget v1, p0, Lcom/google/android/videos/tagging/TagStreamParser;->framesPerSecond:F

    div-float/2addr v0, v1

    const/high16 v1, 0x447a0000    # 1000.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method


# virtual methods
.method public build(Ljava/util/Map;I)Lcom/google/android/videos/tagging/TagStream;
    .locals 24
    .param p2, "desiredPosterWidth"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;I)",
            "Lcom/google/android/videos/tagging/TagStream;"
        }
    .end annotation

    .prologue
    .line 128
    .local p1, "assetResources":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/AssetResource;>;"
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/videos/tagging/TagStreamParser;->parsed:Z

    const-string v5, "Source not yet parsed"

    invoke-static {v4, v5}, Lcom/google/android/videos/utils/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 129
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/tagging/TagStreamParser;->parseException:Ljava/io/IOException;

    if-nez v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    const-string v5, "Source did not succeed parsing"

    invoke-static {v4, v5}, Lcom/google/android/videos/utils/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 130
    invoke-static/range {p1 .. p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/tagging/TagStreamParser;->header:Lcom/google/android/videos/proto/TagProtos$Header;

    iget-object v0, v4, Lcom/google/android/videos/proto/TagProtos$Header;->film:Lcom/google/android/videos/proto/FilmProtos$Film;

    move-object/from16 v22, v0

    .line 133
    .local v22, "headerFilm":Lcom/google/android/videos/proto/FilmProtos$Film;
    new-instance v9, Ljava/util/TreeMap;

    invoke-direct {v9}, Ljava/util/TreeMap;-><init>()V

    .line 134
    .local v9, "images":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/videos/tagging/KnowledgeEntity$Image;>;"
    move-object/from16 v0, v22

    iget-object v5, v0, Lcom/google/android/videos/proto/FilmProtos$Film;->referencedFilm:[Lcom/google/android/videos/proto/FilmProtos$Film;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/tagging/TagStreamParser;->usedReferencedFilmIndices:Ljava/util/Set;

    move-object/from16 v4, p0

    move-object/from16 v7, p1

    move/from16 v8, p2

    invoke-direct/range {v4 .. v9}, Lcom/google/android/videos/tagging/TagStreamParser;->buildReferencedFilms([Lcom/google/android/videos/proto/FilmProtos$Film;Ljava/util/Set;Ljava/util/Map;ILjava/util/Map;)[Lcom/google/android/videos/tagging/KnowledgeEntity$Film;

    move-result-object v23

    .line 137
    .local v23, "referencedFilms":[Lcom/google/android/videos/tagging/KnowledgeEntity$Film;
    new-instance v20, Ljava/util/HashSet;

    invoke-direct/range {v20 .. v20}, Ljava/util/HashSet;-><init>()V

    .line 138
    .local v20, "prefetchImages":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/videos/tagging/KnowledgeEntity$Image;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    move-object/from16 v3, v20

    invoke-direct {v0, v1, v2, v9, v3}, Lcom/google/android/videos/tagging/TagStreamParser;->buildKnowledgeEntities(Lcom/google/android/videos/proto/FilmProtos$Film;[Lcom/google/android/videos/tagging/KnowledgeEntity$Film;Ljava/util/Map;Ljava/util/Set;)Ljava/util/List;

    move-result-object v19

    .line 140
    .local v19, "knowledgeEntities":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/KnowledgeEntity;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/tagging/TagStreamParser;->source:[B

    array-length v4, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/videos/tagging/TagStreamParser;->buildChunks(I)Ljava/util/Collection;

    move-result-object v21

    .line 141
    .local v21, "chunks":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/videos/tagging/TagStream$ChunkInfo;>;"
    new-instance v10, Lcom/google/android/videos/tagging/TagStream;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/videos/tagging/TagStreamParser;->videoId:Ljava/lang/String;

    move-object/from16 v0, v22

    iget-object v12, v0, Lcom/google/android/videos/proto/FilmProtos$Film;->title:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/videos/tagging/TagStreamParser;->contentLanguage:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/tagging/TagStreamParser;->lastModified:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/videos/tagging/TagStreamParser;->videoItag:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/TagStreamParser;->videoTimestamp:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/tagging/TagStreamParser;->source:[B

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/videos/tagging/TagStreamParser;->framesPerSecond:F

    move/from16 v18, v0

    invoke-direct/range {v10 .. v21}, Lcom/google/android/videos/tagging/TagStream;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;[BFLjava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;)V

    return-object v10

    .line 129
    .end local v9    # "images":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/videos/tagging/KnowledgeEntity$Image;>;"
    .end local v19    # "knowledgeEntities":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/KnowledgeEntity;>;"
    .end local v20    # "prefetchImages":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/videos/tagging/KnowledgeEntity$Image;>;"
    .end local v21    # "chunks":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/videos/tagging/TagStream$ChunkInfo;>;"
    .end local v22    # "headerFilm":Lcom/google/android/videos/proto/FilmProtos$Film;
    .end local v23    # "referencedFilms":[Lcom/google/android/videos/tagging/KnowledgeEntity$Film;
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public getFilmographyIds()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/google/android/videos/tagging/TagStreamParser;->parsed:Z

    const-string v1, "Source not yet parsed"

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 117
    iget-object v0, p0, Lcom/google/android/videos/tagging/TagStreamParser;->filmographyIds:Ljava/util/List;

    return-object v0
.end method

.method public parse()Lcom/google/android/videos/tagging/TagStreamParser;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 79
    iget-boolean v4, p0, Lcom/google/android/videos/tagging/TagStreamParser;->parsed:Z

    if-eqz v4, :cond_0

    .line 80
    iget-object v4, p0, Lcom/google/android/videos/tagging/TagStreamParser;->parseException:Ljava/io/IOException;

    if-eqz v4, :cond_3

    .line 81
    iget-object v4, p0, Lcom/google/android/videos/tagging/TagStreamParser;->parseException:Ljava/io/IOException;

    throw v4

    .line 86
    :cond_0
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 87
    .local v2, "filmography":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/google/android/videos/tagging/TagStreamParser;->source:[B

    invoke-static {v4}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->newInstance([B)Lcom/google/protobuf/nano/CodedInputByteBufferNano;

    move-result-object v0

    .line 88
    .local v0, "buffer":Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    new-instance v4, Lcom/google/android/videos/proto/TagProtos$Header;

    invoke-direct {v4}, Lcom/google/android/videos/proto/TagProtos$Header;-><init>()V

    iput-object v4, p0, Lcom/google/android/videos/tagging/TagStreamParser;->header:Lcom/google/android/videos/proto/TagProtos$Header;

    .line 89
    iget-object v4, p0, Lcom/google/android/videos/tagging/TagStreamParser;->header:Lcom/google/android/videos/proto/TagProtos$Header;

    invoke-virtual {v0, v4}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 90
    iget-object v4, p0, Lcom/google/android/videos/tagging/TagStreamParser;->header:Lcom/google/android/videos/proto/TagProtos$Header;

    iget v4, v4, Lcom/google/android/videos/proto/TagProtos$Header;->fps:F

    const/4 v5, 0x0

    cmpg-float v4, v4, v5

    if-gtz v4, :cond_1

    .line 91
    new-instance v4, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;

    const-string v5, "Header invalid - no FPS"

    invoke-direct {v4, v5}, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 103
    .end local v0    # "buffer":Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .end local v2    # "filmography":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v1

    .line 104
    .local v1, "e":Ljava/io/IOException;
    :try_start_1
    iput-object v1, p0, Lcom/google/android/videos/tagging/TagStreamParser;->parseException:Ljava/io/IOException;

    .line 105
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 107
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    iput-boolean v6, p0, Lcom/google/android/videos/tagging/TagStreamParser;->parsed:Z

    throw v4

    .line 93
    .restart local v0    # "buffer":Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .restart local v2    # "filmography":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    :try_start_2
    iget-object v4, p0, Lcom/google/android/videos/tagging/TagStreamParser;->header:Lcom/google/android/videos/proto/TagProtos$Header;

    iget v4, v4, Lcom/google/android/videos/proto/TagProtos$Header;->fps:F

    iput v4, p0, Lcom/google/android/videos/tagging/TagStreamParser;->framesPerSecond:F

    .line 94
    iget-object v4, p0, Lcom/google/android/videos/tagging/TagStreamParser;->header:Lcom/google/android/videos/proto/TagProtos$Header;

    iget-object v4, v4, Lcom/google/android/videos/proto/TagProtos$Header;->film:Lcom/google/android/videos/proto/FilmProtos$Film;

    if-nez v4, :cond_2

    .line 95
    new-instance v4, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;

    const-string v5, "Header invalid - no film"

    invoke-direct {v4, v5}, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 97
    :cond_2
    iget-object v4, p0, Lcom/google/android/videos/tagging/TagStreamParser;->header:Lcom/google/android/videos/proto/TagProtos$Header;

    iget-object v3, v4, Lcom/google/android/videos/proto/TagProtos$Header;->film:Lcom/google/android/videos/proto/FilmProtos$Film;

    .line 98
    .local v3, "headerFilm":Lcom/google/android/videos/proto/FilmProtos$Film;
    invoke-direct {p0, v3}, Lcom/google/android/videos/tagging/TagStreamParser;->parseKnowledgeEntities(Lcom/google/android/videos/proto/FilmProtos$Film;)Ljava/util/Set;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/videos/tagging/TagStreamParser;->usedReferencedFilmIndices:Ljava/util/Set;

    .line 99
    iget-object v4, v3, Lcom/google/android/videos/proto/FilmProtos$Film;->referencedFilm:[Lcom/google/android/videos/proto/FilmProtos$Film;

    iget-object v5, p0, Lcom/google/android/videos/tagging/TagStreamParser;->usedReferencedFilmIndices:Ljava/util/Set;

    invoke-direct {p0, v4, v5, v2}, Lcom/google/android/videos/tagging/TagStreamParser;->parseReferencedFilms([Lcom/google/android/videos/proto/FilmProtos$Film;Ljava/util/Set;Ljava/util/List;)V

    .line 100
    iget-object v4, p0, Lcom/google/android/videos/tagging/TagStreamParser;->source:[B

    array-length v4, v4

    invoke-direct {p0, v4}, Lcom/google/android/videos/tagging/TagStreamParser;->parseChunks(I)V

    .line 101
    invoke-static {v2}, Lcom/google/android/videos/utils/CollectionUtil;->immutableCopyOf(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/videos/tagging/TagStreamParser;->filmographyIds:Ljava/util/List;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 107
    iput-boolean v6, p0, Lcom/google/android/videos/tagging/TagStreamParser;->parsed:Z

    .end local v0    # "buffer":Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .end local v2    # "filmography":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "headerFilm":Lcom/google/android/videos/proto/FilmProtos$Film;
    :cond_3
    return-object p0
.end method
