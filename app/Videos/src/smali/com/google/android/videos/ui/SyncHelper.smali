.class public Lcom/google/android/videos/ui/SyncHelper;
.super Ljava/lang/Object;
.source "SyncHelper.java"

# interfaces
.implements Lcom/google/android/repolib/observers/Observable;
.implements Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/SyncHelper$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/observers/Observable;",
        "Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Ljava/lang/Integer;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private final accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

.field private accountWithAuthenticationFailure:Ljava/lang/String;

.field private final activity:Landroid/app/Activity;

.field private cancelableAddAccountCallback:Lcom/google/android/videos/async/CancelableCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/CancelableCallback",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private cancelableAuthenticatee:Lcom/google/android/videos/async/CancelableAuthenticatee;

.field private cancelableSyncPurchasesCallback:Lcom/google/android/videos/async/CancelableCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/CancelableCallback",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private cancelableSyncWishlistCallback:Lcom/google/android/videos/async/CancelableCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/CancelableCallback",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final gcmRegistrationManager:Lcom/google/android/videos/gcm/GcmRegistrationManager;

.field private final listeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/videos/ui/SyncHelper$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private pendingAccount:Ljava/lang/String;

.field private pendingAccountSelectionResult:Z

.field private pendingAuthenticationResult:Z

.field private pendingChooseAccountCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private pendingError:Ljava/lang/Exception;

.field private final purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

.field private purchasesSynced:Z

.field private final signInManager:Lcom/google/android/videos/accounts/SignInManager;

.field private state:I

.field private volatile syncTaskControl:Lcom/google/android/videos/async/TaskControl;

.field private final updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

.field private final wishlistStoreSync:Lcom/google/android/videos/store/WishlistStoreSync;

.field private wishlistSynced:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/videos/gcm/GcmRegistrationManager;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/PurchaseStoreSync;Lcom/google/android/videos/store/WishlistStoreSync;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "gcmRegistrationManager"    # Lcom/google/android/videos/gcm/GcmRegistrationManager;
    .param p3, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .param p4, "signInManager"    # Lcom/google/android/videos/accounts/SignInManager;
    .param p5, "purchaseStoreSync"    # Lcom/google/android/videos/store/PurchaseStoreSync;
    .param p6, "wishlistStoreSync"    # Lcom/google/android/videos/store/WishlistStoreSync;

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    invoke-static {}, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->asyncUpdateDispatcher()Lcom/google/android/repolib/observers/UpdateDispatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    .line 96
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->activity:Landroid/app/Activity;

    .line 97
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/gcm/GcmRegistrationManager;

    iput-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->gcmRegistrationManager:Lcom/google/android/videos/gcm/GcmRegistrationManager;

    .line 99
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/accounts/AccountManagerWrapper;

    iput-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    .line 101
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/accounts/SignInManager;

    iput-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    .line 102
    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/PurchaseStoreSync;

    iput-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

    .line 104
    invoke-static {p6}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/WishlistStoreSync;

    iput-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->wishlistStoreSync:Lcom/google/android/videos/store/WishlistStoreSync;

    .line 106
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->listeners:Ljava/util/Set;

    .line 107
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/ui/SyncHelper;->state:I

    .line 108
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/ui/SyncHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/SyncHelper;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/google/android/videos/ui/SyncHelper;->wishlistSynced:Z

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/videos/ui/SyncHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/ui/SyncHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/google/android/videos/ui/SyncHelper;->wishlistSynced:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/videos/ui/SyncHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/SyncHelper;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/google/android/videos/ui/SyncHelper;->purchasesSynced:Z

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/videos/ui/SyncHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/ui/SyncHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/google/android/videos/ui/SyncHelper;->purchasesSynced:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/videos/ui/SyncHelper;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/ui/SyncHelper;
    .param p1, "x1"    # I

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/SyncHelper;->setState(I)V

    return-void
.end method

.method private cancelSyncTasks()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 220
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->cancelableSyncPurchasesCallback:Lcom/google/android/videos/async/CancelableCallback;

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->cancelableSyncPurchasesCallback:Lcom/google/android/videos/async/CancelableCallback;

    invoke-virtual {v0}, Lcom/google/android/videos/async/CancelableCallback;->cancel()V

    .line 222
    iput-object v1, p0, Lcom/google/android/videos/ui/SyncHelper;->cancelableSyncPurchasesCallback:Lcom/google/android/videos/async/CancelableCallback;

    .line 224
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->cancelableSyncWishlistCallback:Lcom/google/android/videos/async/CancelableCallback;

    if-eqz v0, :cond_1

    .line 225
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->cancelableSyncWishlistCallback:Lcom/google/android/videos/async/CancelableCallback;

    invoke-virtual {v0}, Lcom/google/android/videos/async/CancelableCallback;->cancel()V

    .line 226
    iput-object v1, p0, Lcom/google/android/videos/ui/SyncHelper;->cancelableSyncWishlistCallback:Lcom/google/android/videos/async/CancelableCallback;

    .line 228
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->syncTaskControl:Lcom/google/android/videos/async/TaskControl;

    invoke-static {v0}, Lcom/google/android/videos/async/TaskControl;->cancel(Lcom/google/android/videos/async/TaskControl;)V

    .line 229
    return-void
.end method

.method private createSyncPurchasesCallback()Lcom/google/android/videos/async/CancelableCallback;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/CancelableCallback",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 367
    new-instance v0, Lcom/google/android/videos/ui/SyncHelper$2;

    invoke-direct {v0, p0}, Lcom/google/android/videos/ui/SyncHelper$2;-><init>(Lcom/google/android/videos/ui/SyncHelper;)V

    invoke-static {v0}, Lcom/google/android/videos/async/CancelableCallback;->create(Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/CancelableCallback;

    move-result-object v0

    return-object v0
.end method

.method private createSyncWishlistcallback()Lcom/google/android/videos/async/CancelableCallback;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/async/CancelableCallback",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 351
    new-instance v0, Lcom/google/android/videos/ui/SyncHelper$1;

    invoke-direct {v0, p0}, Lcom/google/android/videos/ui/SyncHelper$1;-><init>(Lcom/google/android/videos/ui/SyncHelper;)V

    invoke-static {v0}, Lcom/google/android/videos/async/CancelableCallback;->create(Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/CancelableCallback;

    move-result-object v0

    return-object v0
.end method

.method private handleAccountSelectionResult(Ljava/lang/String;Ljava/lang/Exception;Z)V
    .locals 3
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "exception"    # Ljava/lang/Exception;
    .param p3, "allowDefer"    # Z

    .prologue
    .line 267
    iget v1, p0, Lcom/google/android/videos/ui/SyncHelper;->state:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/videos/ui/SyncHelper;->state:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_1

    :cond_0
    if-eqz p3, :cond_1

    .line 268
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/videos/ui/SyncHelper;->pendingAccountSelectionResult:Z

    .line 269
    iput-object p1, p0, Lcom/google/android/videos/ui/SyncHelper;->pendingAccount:Ljava/lang/String;

    .line 270
    iput-object p2, p0, Lcom/google/android/videos/ui/SyncHelper;->pendingError:Ljava/lang/Exception;

    .line 286
    :goto_0
    return-void

    .line 271
    :cond_1
    if-nez p2, :cond_2

    .line 272
    iget-object v1, p0, Lcom/google/android/videos/ui/SyncHelper;->syncTaskControl:Lcom/google/android/videos/async/TaskControl;

    invoke-static {v1}, Lcom/google/android/videos/async/TaskControl;->cancel(Lcom/google/android/videos/async/TaskControl;)V

    .line 273
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/SyncHelper;->startAuthenticationFlow(Ljava/lang/String;)V

    goto :goto_0

    .line 274
    :cond_2
    instance-of v1, p2, Landroid/accounts/OperationCanceledException;

    if-eqz v1, :cond_4

    .line 275
    iget-object v1, p0, Lcom/google/android/videos/ui/SyncHelper;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    invoke-virtual {v1}, Lcom/google/android/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v0

    .line 276
    .local v0, "signedInAccount":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/videos/ui/SyncHelper;->accountWithAuthenticationFailure:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 279
    invoke-direct {p0, v0}, Lcom/google/android/videos/ui/SyncHelper;->startAuthenticationFlow(Ljava/lang/String;)V

    goto :goto_0

    .line 281
    :cond_3
    const/4 v1, 0x5

    invoke-direct {p0, v1}, Lcom/google/android/videos/ui/SyncHelper;->setState(I)V

    goto :goto_0

    .line 284
    .end local v0    # "signedInAccount":Ljava/lang/String;
    :cond_4
    const/4 v1, 0x6

    invoke-direct {p0, v1}, Lcom/google/android/videos/ui/SyncHelper;->setState(I)V

    goto :goto_0
.end method

.method private handleAuthenticationResult(Ljava/lang/String;Ljava/lang/Exception;Z)V
    .locals 4
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "exception"    # Ljava/lang/Exception;
    .param p3, "allowDefer"    # Z

    .prologue
    .line 314
    iget v0, p0, Lcom/google/android/videos/ui/SyncHelper;->state:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/videos/ui/SyncHelper;->state:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    :cond_0
    if-eqz p3, :cond_1

    .line 315
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/ui/SyncHelper;->pendingAuthenticationResult:Z

    .line 316
    iput-object p1, p0, Lcom/google/android/videos/ui/SyncHelper;->pendingAccount:Ljava/lang/String;

    .line 317
    iput-object p2, p0, Lcom/google/android/videos/ui/SyncHelper;->pendingError:Ljava/lang/Exception;

    .line 348
    :goto_0
    return-void

    .line 318
    :cond_1
    if-nez p2, :cond_2

    .line 319
    iput-object p1, p0, Lcom/google/android/videos/ui/SyncHelper;->account:Ljava/lang/String;

    .line 320
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->gcmRegistrationManager:Lcom/google/android/videos/gcm/GcmRegistrationManager;

    invoke-interface {v0}, Lcom/google/android/videos/gcm/GcmRegistrationManager;->ensureRegistered()V

    .line 321
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/accounts/SignInManager;->setSignedInAccount(Ljava/lang/String;)V

    .line 322
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/videos/ui/SyncHelper;->setState(I)V

    .line 323
    new-instance v0, Lcom/google/android/videos/async/TaskControl;

    invoke-direct {v0}, Lcom/google/android/videos/async/TaskControl;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->syncTaskControl:Lcom/google/android/videos/async/TaskControl;

    .line 324
    invoke-direct {p0}, Lcom/google/android/videos/ui/SyncHelper;->createSyncPurchasesCallback()Lcom/google/android/videos/async/CancelableCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->cancelableSyncPurchasesCallback:Lcom/google/android/videos/async/CancelableCallback;

    .line 325
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

    iget-object v1, p0, Lcom/google/android/videos/ui/SyncHelper;->syncTaskControl:Lcom/google/android/videos/async/TaskControl;

    invoke-static {p1, v1}, Lcom/google/android/videos/async/ControllableRequest;->create(Ljava/lang/Object;Lcom/google/android/videos/async/TaskStatus;)Lcom/google/android/videos/async/ControllableRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/ui/SyncHelper;->activity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/videos/ui/SyncHelper;->cancelableSyncPurchasesCallback:Lcom/google/android/videos/async/CancelableCallback;

    invoke-static {v2, v3}, Lcom/google/android/videos/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/ActivityCallback;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/store/PurchaseStoreSync;->syncPurchases(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V

    .line 328
    invoke-direct {p0}, Lcom/google/android/videos/ui/SyncHelper;->createSyncWishlistcallback()Lcom/google/android/videos/async/CancelableCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->cancelableSyncWishlistCallback:Lcom/google/android/videos/async/CancelableCallback;

    .line 329
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->wishlistStoreSync:Lcom/google/android/videos/store/WishlistStoreSync;

    iget-object v1, p0, Lcom/google/android/videos/ui/SyncHelper;->syncTaskControl:Lcom/google/android/videos/async/TaskControl;

    invoke-static {p1, v1}, Lcom/google/android/videos/async/ControllableRequest;->create(Ljava/lang/Object;Lcom/google/android/videos/async/TaskStatus;)Lcom/google/android/videos/async/ControllableRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/ui/SyncHelper;->activity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/videos/ui/SyncHelper;->cancelableSyncWishlistCallback:Lcom/google/android/videos/async/CancelableCallback;

    invoke-static {v2, v3}, Lcom/google/android/videos/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/ActivityCallback;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/store/WishlistStoreSync;->syncWishlist(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V

    goto :goto_0

    .line 344
    :cond_2
    iput-object p1, p0, Lcom/google/android/videos/ui/SyncHelper;->account:Ljava/lang/String;

    .line 345
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/accounts/SignInManager;->setSignedInAccount(Ljava/lang/String;)V

    .line 346
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/videos/ui/SyncHelper;->setState(I)V

    goto :goto_0
.end method

.method private newChooseAccountIntent(Z)Landroid/content/Intent;
    .locals 10
    .param p1, "canSkipAddAccount"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 395
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 396
    .local v7, "options":Landroid/os/Bundle;
    const-string v2, "allowSkip"

    invoke-virtual {v7, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 397
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 399
    .local v1, "accountsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/accounts/Account;>;"
    iget-object v2, p0, Lcom/google/android/videos/ui/SyncHelper;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v2}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getAccounts()[Landroid/accounts/Account;

    move-result-object v9

    .line 400
    .local v9, "accounts":[Landroid/accounts/Account;
    iget-object v2, p0, Lcom/google/android/videos/ui/SyncHelper;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    iget-object v6, p0, Lcom/google/android/videos/ui/SyncHelper;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    invoke-virtual {v6}, Lcom/google/android/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 401
    .local v0, "account":Landroid/accounts/Account;
    iget-object v2, p0, Lcom/google/android/videos/ui/SyncHelper;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v2}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getAccountType()Ljava/lang/String;

    move-result-object v8

    .line 402
    .local v8, "accountType":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/videos/ui/SyncHelper;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v2}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getScope()Ljava/lang/String;

    move-result-object v5

    .line 404
    .local v5, "scope":Ljava/lang/String;
    invoke-static {v1, v9}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 405
    new-array v2, v3, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v8, v2, v6

    move-object v6, v4

    invoke-static/range {v0 .. v7}, Landroid/accounts/AccountManager;->newChooseAccountIntent(Landroid/accounts/Account;Ljava/util/ArrayList;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v2

    return-object v2
.end method

.method private resetSync()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 214
    iput-boolean v0, p0, Lcom/google/android/videos/ui/SyncHelper;->purchasesSynced:Z

    .line 215
    iput-boolean v0, p0, Lcom/google/android/videos/ui/SyncHelper;->wishlistSynced:Z

    .line 216
    invoke-direct {p0}, Lcom/google/android/videos/ui/SyncHelper;->cancelSyncTasks()V

    .line 217
    return-void
.end method

.method private setState(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 385
    iget v2, p0, Lcom/google/android/videos/ui/SyncHelper;->state:I

    if-eq v2, p1, :cond_1

    .line 386
    iput p1, p0, Lcom/google/android/videos/ui/SyncHelper;->state:I

    .line 387
    iget-object v2, p0, Lcom/google/android/videos/ui/SyncHelper;->listeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/ui/SyncHelper$Listener;

    .line 388
    .local v1, "listener":Lcom/google/android/videos/ui/SyncHelper$Listener;
    invoke-interface {v1, p1}, Lcom/google/android/videos/ui/SyncHelper$Listener;->onSyncStateChanged(I)V

    goto :goto_0

    .line 390
    .end local v1    # "listener":Lcom/google/android/videos/ui/SyncHelper$Listener;
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/ui/SyncHelper;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v2}, Lcom/google/android/repolib/observers/UpdateDispatcher;->update()V

    .line 392
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    return-void
.end method

.method private startAuthenticationFlow(Ljava/lang/String;)V
    .locals 3
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 291
    iput-object p1, p0, Lcom/google/android/videos/ui/SyncHelper;->account:Ljava/lang/String;

    .line 292
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->accountWithAuthenticationFailure:Ljava/lang/String;

    .line 293
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/videos/ui/SyncHelper;->setState(I)V

    .line 294
    new-instance v0, Lcom/google/android/videos/async/CancelableAuthenticatee;

    invoke-direct {v0, p0}, Lcom/google/android/videos/async/CancelableAuthenticatee;-><init>(Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;)V

    iput-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->cancelableAuthenticatee:Lcom/google/android/videos/async/CancelableAuthenticatee;

    .line 295
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    iget-object v1, p0, Lcom/google/android/videos/ui/SyncHelper;->activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/videos/ui/SyncHelper;->cancelableAuthenticatee:Lcom/google/android/videos/async/CancelableAuthenticatee;

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getAuthToken(Ljava/lang/String;Landroid/app/Activity;Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;)V

    .line 296
    return-void
.end method

.method private startSelectAccountFlow()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 239
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/videos/ui/SyncHelper;->setState(I)V

    .line 240
    invoke-static {p0}, Lcom/google/android/videos/async/CancelableCallback;->create(Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/CancelableCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->cancelableAddAccountCallback:Lcom/google/android/videos/async/CancelableCallback;

    .line 241
    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->cancelableAddAccountCallback:Lcom/google/android/videos/async/CancelableCallback;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Landroid/accounts/OperationCanceledException;

    invoke-direct {v2}, Landroid/accounts/OperationCanceledException;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/async/CancelableCallback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 253
    :goto_0
    return-void

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->pendingChooseAccountCallback:Lcom/google/android/videos/async/Callback;

    if-eqz v0, :cond_1

    .line 248
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->pendingChooseAccountCallback:Lcom/google/android/videos/async/Callback;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Landroid/accounts/OperationCanceledException;

    invoke-direct {v2}, Landroid/accounts/OperationCanceledException;-><init>()V

    invoke-interface {v0, v1, v2}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 251
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->cancelableAddAccountCallback:Lcom/google/android/videos/async/CancelableCallback;

    iput-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->pendingChooseAccountCallback:Lcom/google/android/videos/async/Callback;

    .line 252
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->activity:Landroid/app/Activity;

    invoke-direct {p0, v3}, Lcom/google/android/videos/ui/SyncHelper;->newChooseAccountIntent(Z)Landroid/content/Intent;

    move-result-object v1

    const/16 v2, 0x387

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method


# virtual methods
.method public addListener(Lcom/google/android/videos/ui/SyncHelper$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/videos/ui/SyncHelper$Listener;

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->listeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 123
    return-void
.end method

.method public addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 114
    return-void
.end method

.method public clearPendingResults()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 232
    iput-boolean v0, p0, Lcom/google/android/videos/ui/SyncHelper;->pendingAccountSelectionResult:Z

    .line 233
    iput-boolean v0, p0, Lcom/google/android/videos/ui/SyncHelper;->pendingAuthenticationResult:Z

    .line 234
    return-void
.end method

.method public getAccount()Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->account:Ljava/lang/String;

    return-object v0
.end method

.method public getState()I
    .locals 1

    .prologue
    .line 130
    iget v0, p0, Lcom/google/android/videos/ui/SyncHelper;->state:I

    return v0
.end method

.method public init(Ljava/lang/String;)V
    .locals 3
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 138
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->cancelableAuthenticatee:Lcom/google/android/videos/async/CancelableAuthenticatee;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->cancelableAuthenticatee:Lcom/google/android/videos/async/CancelableAuthenticatee;

    invoke-virtual {v0}, Lcom/google/android/videos/async/CancelableAuthenticatee;->cancel()V

    .line 140
    iput-object v1, p0, Lcom/google/android/videos/ui/SyncHelper;->cancelableAuthenticatee:Lcom/google/android/videos/async/CancelableAuthenticatee;

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->cancelableAddAccountCallback:Lcom/google/android/videos/async/CancelableCallback;

    if-eqz v0, :cond_1

    .line 143
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->cancelableAddAccountCallback:Lcom/google/android/videos/async/CancelableCallback;

    invoke-virtual {v0}, Lcom/google/android/videos/async/CancelableCallback;->cancel()V

    .line 144
    iput-object v1, p0, Lcom/google/android/videos/ui/SyncHelper;->cancelableAddAccountCallback:Lcom/google/android/videos/async/CancelableCallback;

    .line 147
    :cond_1
    invoke-direct {p0}, Lcom/google/android/videos/ui/SyncHelper;->resetSync()V

    .line 149
    iget-boolean v0, p0, Lcom/google/android/videos/ui/SyncHelper;->pendingAccountSelectionResult:Z

    if-eqz v0, :cond_3

    .line 150
    iput-boolean v2, p0, Lcom/google/android/videos/ui/SyncHelper;->pendingAccountSelectionResult:Z

    .line 151
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->pendingError:Ljava/lang/Exception;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    iget-object v1, p0, Lcom/google/android/videos/ui/SyncHelper;->pendingAccount:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->accountExists(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 152
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->pendingAccount:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/videos/ui/SyncHelper;->pendingError:Ljava/lang/Exception;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/videos/ui/SyncHelper;->handleAccountSelectionResult(Ljava/lang/String;Ljava/lang/Exception;Z)V

    .line 176
    :goto_0
    return-void

    .line 157
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/videos/ui/SyncHelper;->pendingAuthenticationResult:Z

    if-eqz v0, :cond_4

    .line 158
    iput-boolean v2, p0, Lcom/google/android/videos/ui/SyncHelper;->pendingAuthenticationResult:Z

    .line 159
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    iget-object v1, p0, Lcom/google/android/videos/ui/SyncHelper;->pendingAccount:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->accountExists(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 160
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->pendingAccount:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/videos/ui/SyncHelper;->pendingError:Ljava/lang/Exception;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/videos/ui/SyncHelper;->handleAuthenticationResult(Ljava/lang/String;Ljava/lang/Exception;Z)V

    goto :goto_0

    .line 165
    :cond_4
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 166
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    invoke-virtual {v0}, Lcom/google/android/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object p1

    .line 168
    :cond_5
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 169
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    invoke-virtual {v0}, Lcom/google/android/videos/accounts/SignInManager;->chooseFirstAccount()Ljava/lang/String;

    move-result-object p1

    .line 171
    :cond_6
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 172
    invoke-direct {p0}, Lcom/google/android/videos/ui/SyncHelper;->startSelectAccountFlow()V

    goto :goto_0

    .line 174
    :cond_7
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/SyncHelper;->startAuthenticationFlow(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public markAsStale()V
    .locals 1

    .prologue
    .line 209
    invoke-direct {p0}, Lcom/google/android/videos/ui/SyncHelper;->cancelSyncTasks()V

    .line 210
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/videos/ui/SyncHelper;->setState(I)V

    .line 211
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)Z
    .locals 6
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 179
    iget-object v3, p0, Lcom/google/android/videos/ui/SyncHelper;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v3, p1, p2, p3}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->onActivityResult(IILandroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 199
    :goto_0
    return v1

    .line 182
    :cond_0
    const/16 v3, 0x387

    if-ne p1, v3, :cond_1

    iget-object v3, p0, Lcom/google/android/videos/ui/SyncHelper;->pendingChooseAccountCallback:Lcom/google/android/videos/async/Callback;

    if-nez v3, :cond_2

    :cond_1
    move v1, v2

    .line 183
    goto :goto_0

    .line 186
    :cond_2
    const/4 v3, -0x1

    if-ne p2, v3, :cond_3

    if-eqz p3, :cond_3

    .line 187
    const-string v3, "authAccount"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 188
    .local v0, "account":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/videos/ui/SyncHelper;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v3, v0}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->accountExists(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 189
    iget-object v3, p0, Lcom/google/android/videos/ui/SyncHelper;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    invoke-virtual {v3, v0}, Lcom/google/android/videos/accounts/SignInManager;->setSignedInAccount(Ljava/lang/String;)V

    .line 190
    iget-object v3, p0, Lcom/google/android/videos/ui/SyncHelper;->pendingChooseAccountCallback:Lcom/google/android/videos/async/Callback;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2, v0}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 191
    iput-object v5, p0, Lcom/google/android/videos/ui/SyncHelper;->pendingChooseAccountCallback:Lcom/google/android/videos/async/Callback;

    goto :goto_0

    .line 197
    .end local v0    # "account":Ljava/lang/String;
    :cond_3
    iget-object v3, p0, Lcom/google/android/videos/ui/SyncHelper;->pendingChooseAccountCallback:Lcom/google/android/videos/async/Callback;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v4, Landroid/accounts/OperationCanceledException;

    invoke-direct {v4}, Landroid/accounts/OperationCanceledException;-><init>()V

    invoke-interface {v3, v2, v4}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 198
    iput-object v5, p0, Lcom/google/android/videos/ui/SyncHelper;->pendingChooseAccountCallback:Lcom/google/android/videos/async/Callback;

    goto :goto_0
.end method

.method public onAuthenticated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "authToken"    # Ljava/lang/String;

    .prologue
    .line 310
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/videos/ui/SyncHelper;->handleAuthenticationResult(Ljava/lang/String;Ljava/lang/Exception;Z)V

    .line 311
    return-void
.end method

.method public onAuthenticationError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 305
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/videos/ui/SyncHelper;->handleAuthenticationResult(Ljava/lang/String;Ljava/lang/Exception;Z)V

    .line 306
    return-void
.end method

.method public onError(Ljava/lang/Integer;Ljava/lang/Exception;)V
    .locals 2
    .param p1, "request"    # Ljava/lang/Integer;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 262
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, p2, v1}, Lcom/google/android/videos/ui/SyncHelper;->handleAccountSelectionResult(Ljava/lang/String;Ljava/lang/Exception;Z)V

    .line 263
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 45
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/SyncHelper;->onError(Ljava/lang/Integer;Ljava/lang/Exception;)V

    return-void
.end method

.method public onNotAuthenticated(Ljava/lang/String;)V
    .locals 2
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 300
    new-instance v0, Landroid/accounts/OperationCanceledException;

    invoke-direct {v0}, Landroid/accounts/OperationCanceledException;-><init>()V

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/videos/ui/SyncHelper;->handleAuthenticationResult(Ljava/lang/String;Ljava/lang/Exception;Z)V

    .line 301
    return-void
.end method

.method public onResponse(Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 2
    .param p1, "request"    # Ljava/lang/Integer;
    .param p2, "account"    # Ljava/lang/String;

    .prologue
    .line 257
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p2, v0, v1}, Lcom/google/android/videos/ui/SyncHelper;->handleAccountSelectionResult(Ljava/lang/String;Ljava/lang/Exception;Z)V

    .line 258
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 45
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/lang/String;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/SyncHelper;->onResponse(Ljava/lang/Integer;Ljava/lang/String;)V

    return-void
.end method

.method public removeListener(Lcom/google/android/videos/ui/SyncHelper$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/videos/ui/SyncHelper$Listener;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->listeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 127
    return-void
.end method

.method public removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 119
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 203
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/ui/SyncHelper;->account:Ljava/lang/String;

    .line 204
    invoke-direct {p0}, Lcom/google/android/videos/ui/SyncHelper;->resetSync()V

    .line 205
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/videos/ui/SyncHelper;->setState(I)V

    .line 206
    return-void
.end method
