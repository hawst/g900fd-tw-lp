.class public Lcom/google/android/videos/ui/RestrictionsHelper;
.super Ljava/lang/Object;
.source "RestrictionsHelper.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/google/android/videos/async/Callback;
.implements Lcom/google/android/videos/ui/StatusHelper$OnRetryListener;
.implements Lcom/google/android/videos/ui/SyncHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/RestrictionsHelper$Receiver;,
        Lcom/google/android/videos/ui/RestrictionsHelper$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/preference/Preference$OnPreferenceChangeListener;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Ljava/lang/String;",
        "Lcom/google/wireless/android/video/magma/proto/UserConfiguration;",
        ">;",
        "Lcom/google/android/videos/ui/StatusHelper$OnRetryListener;",
        "Lcom/google/android/videos/ui/SyncHelper$Listener;"
    }
.end annotation


# instance fields
.field private final activity:Landroid/app/Activity;

.field private allowedIdsChanged:Z

.field private final listener:Lcom/google/android/videos/ui/RestrictionsHelper$Listener;

.field private final ratingSchemes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;",
            ">;"
        }
    .end annotation
.end field

.field private receivedResponse:Z

.field private final restrictionsBundle:Landroid/os/Bundle;

.field private statusHelper:Lcom/google/android/videos/ui/StatusHelper;

.field private final syncHelper:Lcom/google/android/videos/ui/SyncHelper;

.field private final videosGlobals:Lcom/google/android/videos/VideosGlobals;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/videos/ui/RestrictionsHelper$Listener;Landroid/view/View;)V
    .locals 8
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "listener"    # Lcom/google/android/videos/ui/RestrictionsHelper$Listener;
    .param p3, "statusView"    # Landroid/view/View;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->activity:Landroid/app/Activity;

    .line 62
    iput-object p2, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->listener:Lcom/google/android/videos/ui/RestrictionsHelper$Listener;

    .line 64
    invoke-static {p1}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    .line 65
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->ratingSchemes:Ljava/util/HashMap;

    .line 67
    if-eqz p3, :cond_0

    .line 68
    invoke-static {p1, p3, p0}, Lcom/google/android/videos/ui/StatusHelper;->createFromParent(Landroid/content/Context;Landroid/view/View;Lcom/google/android/videos/ui/StatusHelper$OnRetryListener;)Lcom/google/android/videos/ui/StatusHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->statusHelper:Lcom/google/android/videos/ui/StatusHelper;

    .line 69
    iget-object v0, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->statusHelper:Lcom/google/android/videos/ui/StatusHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/StatusHelper;->init()V

    .line 73
    :cond_0
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.intent.extra.restrictions_bundle"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v7

    .line 74
    .local v7, "bundle":Landroid/os/Bundle;
    if-eqz v7, :cond_1

    .end local v7    # "bundle":Landroid/os/Bundle;
    :goto_0
    iput-object v7, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->restrictionsBundle:Landroid/os/Bundle;

    .line 76
    invoke-virtual {p0}, Lcom/google/android/videos/ui/RestrictionsHelper;->updateResult()V

    .line 77
    new-instance v0, Lcom/google/android/videos/ui/SyncHelper;

    iget-object v1, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getGcmRegistrationManager()Lcom/google/android/videos/gcm/GcmRegistrationManager;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v4

    iget-object v1, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStoreSync()Lcom/google/android/videos/store/PurchaseStoreSync;

    move-result-object v5

    iget-object v1, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getWishlistStoreSync()Lcom/google/android/videos/store/WishlistStoreSync;

    move-result-object v6

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/ui/SyncHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/gcm/GcmRegistrationManager;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/PurchaseStoreSync;Lcom/google/android/videos/store/WishlistStoreSync;)V

    iput-object v0, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    .line 84
    iget-object v0, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/SyncHelper;->addListener(Lcom/google/android/videos/ui/SyncHelper$Listener;)V

    .line 85
    return-void

    .line 74
    .restart local v7    # "bundle":Landroid/os/Bundle;
    :cond_1
    new-instance v7, Landroid/os/Bundle;

    .end local v7    # "bundle":Landroid/os/Bundle;
    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/videos/ui/RestrictionsHelper;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/RestrictionsHelper;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->activity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/ui/RestrictionsHelper;)Lcom/google/android/videos/VideosGlobals;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/RestrictionsHelper;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    return-object v0
.end method

.method private fetchRestrictions()V
    .locals 9

    .prologue
    .line 126
    iget-object v7, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v7}, Lcom/google/android/videos/VideosGlobals;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v1

    .line 127
    .local v1, "accountManager":Lcom/google/android/videos/accounts/AccountManagerWrapper;
    invoke-virtual {v1}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getAccounts()[Landroid/accounts/Account;

    move-result-object v2

    .line 129
    .local v2, "accounts":[Landroid/accounts/Account;
    iget-object v7, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->activity:Landroid/app/Activity;

    invoke-static {v7, p0}, Lcom/google/android/videos/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/ActivityCallback;

    move-result-object v4

    .line 130
    .local v4, "callback":Lcom/google/android/videos/async/ActivityCallback;, "Lcom/google/android/videos/async/ActivityCallback<Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/UserConfiguration;>;"
    move-object v3, v2

    .local v3, "arr$":[Landroid/accounts/Account;
    array-length v6, v3

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_0

    aget-object v0, v3, v5

    .line 131
    .local v0, "account":Landroid/accounts/Account;
    iget-object v7, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v7}, Lcom/google/android/videos/VideosGlobals;->getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v7

    iget-object v8, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v7, v8, v4}, Lcom/google/android/videos/store/ConfigurationStore;->requestUserConfiguration(Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V

    .line 130
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 133
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_0
    return-void
.end method

.method private getAllowedIds()[Ljava/lang/String;
    .locals 11

    .prologue
    .line 150
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 151
    .local v6, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v9, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->restrictionsBundle:Landroid/os/Bundle;

    invoke-virtual {v9}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 152
    .local v3, "key":Ljava/lang/String;
    const-string v9, "rating_scheme_"

    invoke-virtual {v3, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 153
    const-string v9, "rating_scheme_"

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v3, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 154
    .local v8, "schemeId":Ljava/lang/String;
    iget-object v9, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->ratingSchemes:Ljava/util/HashMap;

    invoke-virtual {v9, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    .line 155
    .local v7, "scheme":Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;
    if-eqz v7, :cond_0

    .line 160
    iget-object v0, v7, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    .local v0, "arr$":[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v5, v0, v2

    .line 161
    .local v5, "rating":Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;
    iget-object v9, v5, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->contentRatingId:Ljava/lang/String;

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162
    iget-object v9, v5, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->name:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->restrictionsBundle:Landroid/os/Bundle;

    invoke-virtual {v10, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 160
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 169
    .end local v0    # "arr$":[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;
    .end local v2    # "i$":I
    .end local v3    # "key":Ljava/lang/String;
    .end local v4    # "len$":I
    .end local v5    # "rating":Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;
    .end local v7    # "scheme":Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;
    .end local v8    # "schemeId":Ljava/lang/String;
    :cond_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v9

    new-array v9, v9, [Ljava/lang/String;

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/String;

    return-object v9
.end method

.method private getSelectedRating(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "schemeId"    # Ljava/lang/String;

    .prologue
    .line 275
    iget-object v0, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->restrictionsBundle:Landroid/os/Bundle;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "rating_scheme_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private onAuthenticationError()V
    .locals 3

    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->receivedResponse:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->statusHelper:Lcom/google/android/videos/ui/StatusHelper;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->statusHelper:Lcom/google/android/videos/ui/StatusHelper;

    const v1, 0x7f0b0145

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/ui/StatusHelper;->setErrorMessage(IZ)V

    .line 123
    :cond_0
    return-void
.end method

.method private setAllowUnrated(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 286
    iget-object v0, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->restrictionsBundle:Landroid/os/Bundle;

    const-string v1, "allow_unrated"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 287
    return-void
.end method

.method private setSelectedRating(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "schemeId"    # Ljava/lang/String;
    .param p2, "rating"    # Ljava/lang/String;

    .prologue
    .line 279
    iget-object v0, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->restrictionsBundle:Landroid/os/Bundle;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "rating_scheme_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    iget-object v0, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->restrictionsBundle:Landroid/os/Bundle;

    const-string v1, "allowed_ids"

    invoke-direct {p0}, Lcom/google/android/videos/ui/RestrictionsHelper;->getAllowedIds()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 282
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->allowedIdsChanged:Z

    .line 283
    return-void
.end method

.method private startAsyncRequests()V
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->statusHelper:Lcom/google/android/videos/ui/StatusHelper;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->statusHelper:Lcom/google/android/videos/ui/StatusHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/StatusHelper;->setLoading()V

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/SyncHelper;->init(Ljava/lang/String;)V

    .line 117
    return-void
.end method

.method private syncUserConfiguration(Ljava/lang/String;)V
    .locals 3
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 256
    new-instance v0, Lcom/google/android/videos/ui/RestrictionsHelper$1;

    invoke-direct {v0, p0}, Lcom/google/android/videos/ui/RestrictionsHelper$1;-><init>(Lcom/google/android/videos/ui/RestrictionsHelper;)V

    .line 270
    .local v0, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Ljava/lang/String;Ljava/lang/Void;>;"
    iget-object v1, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->activity:Landroid/app/Activity;

    invoke-static {v2, v0}, Lcom/google/android/videos/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/ActivityCallback;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/google/android/videos/store/ConfigurationStore;->syncUserConfiguration(Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V

    .line 272
    return-void
.end method


# virtual methods
.method public createAllowUnratedPreference()Landroid/preference/CheckBoxPreference;
    .locals 3

    .prologue
    .line 245
    new-instance v0, Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->activity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    .line 246
    .local v0, "preference":Landroid/preference/CheckBoxPreference;
    const-string v1, "allow_unrated"

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setKey(Ljava/lang/String;)V

    .line 247
    iget-object v1, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->activity:Landroid/app/Activity;

    const v2, 0x7f0b0202

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 248
    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 249
    invoke-virtual {p0}, Lcom/google/android/videos/ui/RestrictionsHelper;->getAllowUnrated()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 250
    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOrder(I)V

    .line 251
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setPersistent(Z)V

    .line 252
    return-object v0
.end method

.method public getAllowUnrated()Z
    .locals 3

    .prologue
    .line 290
    iget-object v0, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->restrictionsBundle:Landroid/os/Bundle;

    const-string v1, "allow_unrated"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)Z
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/videos/ui/SyncHelper;->onActivityResult(IILandroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 40
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/RestrictionsHelper;->onError(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public onError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 3
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 295
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Couldn\'t fetch config for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/videos/utils/Hashing;->sha1(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 296
    iget-boolean v1, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->receivedResponse:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->statusHelper:Lcom/google/android/videos/ui/StatusHelper;

    if-eqz v1, :cond_0

    .line 297
    iget-object v1, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getErrorHelper()Lcom/google/android/videos/utils/ErrorHelper;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/videos/utils/ErrorHelper;->humanize(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    .line 298
    .local v0, "error":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->statusHelper:Lcom/google/android/videos/ui/StatusHelper;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/google/android/videos/ui/StatusHelper;->setErrorMessage(Ljava/lang/CharSequence;Z)V

    .line 301
    .end local v0    # "error":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/SyncHelper;->reset()V

    .line 93
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 305
    instance-of v0, p1, Landroid/preference/ListPreference;

    if-eqz v0, :cond_1

    instance-of v0, p2, Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 306
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    move-object v0, p2

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/google/android/videos/ui/RestrictionsHelper;->setSelectedRating(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p1, p2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 311
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/videos/ui/RestrictionsHelper;->updateResult()V

    .line 312
    const/4 v0, 0x1

    return v0

    .line 308
    .restart local p2    # "value":Ljava/lang/Object;
    :cond_1
    instance-of v0, p2, Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 309
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/ui/RestrictionsHelper;->setAllowUnrated(Z)V

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 40
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/RestrictionsHelper;->onResponse(Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/UserConfiguration;)V

    return-void
.end method

.method public onResponse(Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/UserConfiguration;)V
    .locals 19
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "response"    # Lcom/google/wireless/android/video/magma/proto/UserConfiguration;

    .prologue
    .line 174
    if-nez p2, :cond_1

    .line 175
    invoke-direct/range {p0 .. p1}, Lcom/google/android/videos/ui/RestrictionsHelper;->syncUserConfiguration(Ljava/lang/String;)V

    .line 242
    :cond_0
    return-void

    .line 179
    :cond_1
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/videos/ui/RestrictionsHelper;->receivedResponse:Z

    .line 181
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/RestrictionsHelper;->statusHelper:Lcom/google/android/videos/ui/StatusHelper;

    move-object/from16 v17, v0

    if-eqz v17, :cond_2

    .line 182
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/RestrictionsHelper;->statusHelper:Lcom/google/android/videos/ui/StatusHelper;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/videos/ui/StatusHelper;->hide()V

    .line 185
    :cond_2
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->contentRatingScheme:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    move-object/from16 v17, v0

    if-eqz v17, :cond_0

    .line 186
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->contentRatingScheme:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    .local v2, "arr$":[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;
    array-length v9, v2

    .local v9, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    move v6, v5

    .end local v2    # "arr$":[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;
    .end local v5    # "i$":I
    .end local v9    # "len$":I
    .local v6, "i$":I
    :goto_0
    if-ge v6, v9, :cond_0

    aget-object v13, v2, v6

    .line 187
    .local v13, "scheme":Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/RestrictionsHelper;->ratingSchemes:Ljava/util/HashMap;

    move-object/from16 v17, v0

    iget-object v0, v13, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->schemeId:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    if-eqz v17, :cond_4

    .line 186
    .end local v6    # "i$":I
    :cond_3
    :goto_1
    add-int/lit8 v5, v6, 0x1

    .restart local v5    # "i$":I
    move v6, v5

    .end local v5    # "i$":I
    .restart local v6    # "i$":I
    goto :goto_0

    .line 193
    :cond_4
    const/4 v7, 0x0

    .line 194
    .local v7, "isMovies":Z
    const/4 v8, 0x0

    .line 195
    .local v8, "isTv":Z
    iget-object v3, v13, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->types:[I

    .local v3, "arr$":[I
    array-length v10, v3

    .local v10, "len$":I
    const/4 v5, 0x0

    .end local v6    # "i$":I
    .restart local v5    # "i$":I
    :goto_2
    if-ge v5, v10, :cond_7

    aget v16, v3, v5

    .line 196
    .local v16, "type":I
    const/16 v17, 0x6

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_5

    const/16 v17, 0x1

    :goto_3
    or-int v7, v7, v17

    .line 197
    const/16 v17, 0x12

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_6

    const/16 v17, 0x1

    :goto_4
    or-int v8, v8, v17

    .line 195
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 196
    :cond_5
    const/16 v17, 0x0

    goto :goto_3

    .line 197
    :cond_6
    const/16 v17, 0x0

    goto :goto_4

    .line 199
    .end local v16    # "type":I
    :cond_7
    if-eqz v7, :cond_8

    if-eqz v8, :cond_8

    .line 200
    const v15, 0x7f0b0205

    .line 211
    .local v15, "titleId":I
    :goto_5
    iget-object v0, v13, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->schemeId:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/google/android/videos/ui/RestrictionsHelper;->getSelectedRating(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 212
    .local v14, "selected":Ljava/lang/String;
    iget-object v0, v13, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    new-array v11, v0, [Ljava/lang/String;

    .line 213
    .local v11, "options":[Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_6
    array-length v0, v11

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v4, v0, :cond_b

    .line 214
    iget-object v0, v13, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    move-object/from16 v17, v0

    aget-object v17, v17, v4

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->name:Ljava/lang/String;

    move-object/from16 v17, v0

    aput-object v17, v11, v4

    .line 213
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 201
    .end local v4    # "i":I
    .end local v11    # "options":[Ljava/lang/String;
    .end local v14    # "selected":Ljava/lang/String;
    .end local v15    # "titleId":I
    :cond_8
    if-eqz v7, :cond_9

    .line 202
    const v15, 0x7f0b0203

    .restart local v15    # "titleId":I
    goto :goto_5

    .line 203
    .end local v15    # "titleId":I
    :cond_9
    if-eqz v8, :cond_a

    .line 204
    const v15, 0x7f0b0204

    .restart local v15    # "titleId":I
    goto :goto_5

    .line 207
    .end local v15    # "titleId":I
    :cond_a
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Rating scheme "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " not for movies or shows!"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 217
    .restart local v4    # "i":I
    .restart local v11    # "options":[Ljava/lang/String;
    .restart local v14    # "selected":Ljava/lang/String;
    .restart local v15    # "titleId":I
    :cond_b
    if-nez v14, :cond_c

    .line 219
    array-length v0, v11

    move/from16 v17, v0

    add-int/lit8 v17, v17, -0x1

    aget-object v14, v11, v17

    .line 220
    iget-object v0, v13, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->schemeId:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v14}, Lcom/google/android/videos/ui/RestrictionsHelper;->setSelectedRating(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    :cond_c
    new-instance v12, Landroid/preference/ListPreference;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/RestrictionsHelper;->activity:Landroid/app/Activity;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-direct {v12, v0}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    .line 224
    .local v12, "preference":Landroid/preference/ListPreference;
    iget-object v0, v13, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->schemeId:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/preference/ListPreference;->setKey(Ljava/lang/String;)V

    .line 225
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/RestrictionsHelper;->activity:Landroid/app/Activity;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 226
    invoke-virtual {v12, v11}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 227
    invoke-virtual {v12, v11}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 228
    move-object/from16 v0, p0

    invoke-virtual {v12, v0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 229
    const/16 v17, 0x2

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/preference/ListPreference;->setOrder(I)V

    .line 230
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/preference/ListPreference;->setPersistent(Z)V

    .line 231
    invoke-virtual {v12, v14}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 232
    invoke-virtual {v12, v14}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 233
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/RestrictionsHelper;->listener:Lcom/google/android/videos/ui/RestrictionsHelper$Listener;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v12}, Lcom/google/android/videos/ui/RestrictionsHelper$Listener;->onRestrictionPreferenceCreated(Landroid/preference/ListPreference;)V

    .line 235
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/videos/ui/RestrictionsHelper;->allowedIdsChanged:Z

    move/from16 v17, v0

    if-eqz v17, :cond_3

    .line 238
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/ui/RestrictionsHelper;->updateResult()V

    goto/16 :goto_1
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/google/android/videos/ui/RestrictionsHelper;->startAsyncRequests()V

    .line 89
    return-void
.end method

.method public onRetry()V
    .locals 0

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/google/android/videos/ui/RestrictionsHelper;->startAsyncRequests()V

    .line 98
    return-void
.end method

.method public onSyncStateChanged(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 102
    packed-switch p1, :pswitch_data_0

    .line 110
    :goto_0
    :pswitch_0
    return-void

    .line 104
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/videos/ui/RestrictionsHelper;->fetchRestrictions()V

    goto :goto_0

    .line 108
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/videos/ui/RestrictionsHelper;->onAuthenticationError()V

    goto :goto_0

    .line 102
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public updateResult()V
    .locals 3

    .prologue
    .line 140
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 141
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.extra.restrictions_bundle"

    iget-object v2, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->restrictionsBundle:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 142
    iget-object v1, p0, Lcom/google/android/videos/ui/RestrictionsHelper;->activity:Landroid/app/Activity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 143
    return-void
.end method
