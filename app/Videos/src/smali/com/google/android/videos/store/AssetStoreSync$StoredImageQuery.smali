.class abstract Lcom/google/android/videos/store/AssetStoreSync$StoredImageQuery;
.super Ljava/lang/Object;
.source "AssetStoreSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/AssetStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "StoredImageQuery"
.end annotation


# static fields
.field private static final PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 196
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "image_uri"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "image_etag"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "image_last_modified"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/videos/store/AssetStoreSync$StoredImageQuery;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public static projection(Ljava/lang/String;)[Ljava/lang/String;
    .locals 2
    .param p0, "itemIdColumn"    # Ljava/lang/String;

    .prologue
    .line 209
    sget-object v1, Lcom/google/android/videos/store/AssetStoreSync$StoredImageQuery;->PROJECTION:[Ljava/lang/String;

    invoke-virtual {v1}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 210
    .local v0, "columns":[Ljava/lang/String;
    const/4 v1, 0x0

    aput-object p0, v0, v1

    .line 211
    return-object v0
.end method
