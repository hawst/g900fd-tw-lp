.class public final Lcom/google/android/videos/ui/CursorHelper$WatchNowEpisodesCursorHelper;
.super Lcom/google/android/videos/ui/CursorHelper$HomeShowPurchaseCursorHelper;
.source "CursorHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/CursorHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "WatchNowEpisodesCursorHelper"
.end annotation


# instance fields
.field private final allowUnpurchased:Z

.field private final excludeFinishedLastWatched:Z

.field private final maxEpisodesPerShow:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/videos/Config;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/ui/SyncHelper;IZZLcom/google/android/videos/utils/DownloadedOnlyManager;)V
    .locals 9
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "config"    # Lcom/google/android/videos/Config;
    .param p3, "uiHandler"    # Landroid/os/Handler;
    .param p4, "database"    # Lcom/google/android/videos/store/Database;
    .param p5, "purchaseStore"    # Lcom/google/android/videos/store/PurchaseStore;
    .param p6, "syncHelper"    # Lcom/google/android/videos/ui/SyncHelper;
    .param p7, "maxEpisodesPerShow"    # I
    .param p8, "allowUnpurchased"    # Z
    .param p9, "excludeFinishedLastWatched"    # Z
    .param p10, "downloadedOnlyManager"    # Lcom/google/android/videos/utils/DownloadedOnlyManager;

    .prologue
    .line 495
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    move-object/from16 v8, p10

    invoke-direct/range {v1 .. v8}, Lcom/google/android/videos/ui/CursorHelper$HomeShowPurchaseCursorHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/Config;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/ui/SyncHelper;Lcom/google/android/videos/utils/DownloadedOnlyManager;)V

    .line 497
    move/from16 v0, p7

    iput v0, p0, Lcom/google/android/videos/ui/CursorHelper$WatchNowEpisodesCursorHelper;->maxEpisodesPerShow:I

    .line 498
    move/from16 v0, p8

    iput-boolean v0, p0, Lcom/google/android/videos/ui/CursorHelper$WatchNowEpisodesCursorHelper;->allowUnpurchased:Z

    .line 499
    move/from16 v0, p9

    iput-boolean v0, p0, Lcom/google/android/videos/ui/CursorHelper$WatchNowEpisodesCursorHelper;->excludeFinishedLastWatched:Z

    .line 500
    return-void
.end method


# virtual methods
.method protected createCursorRequest(Ljava/lang/String;Z)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 7
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "downloadedOnly"    # Z

    .prologue
    .line 504
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v4, p0, Lcom/google/android/videos/ui/CursorHelper$WatchNowEpisodesCursorHelper;->config:Lcom/google/android/videos/Config;

    invoke-interface {v4}, Lcom/google/android/videos/Config;->recentActiveMillis()J

    move-result-wide v4

    sub-long v2, v0, v4

    .line 505
    .local v2, "activeSinceTimestamp":J
    iget-boolean v4, p0, Lcom/google/android/videos/ui/CursorHelper$WatchNowEpisodesCursorHelper;->excludeFinishedLastWatched:Z

    iget-boolean v0, p0, Lcom/google/android/videos/ui/CursorHelper$WatchNowEpisodesCursorHelper;->allowUnpurchased:Z

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    const/4 v5, 0x1

    :goto_0
    iget v6, p0, Lcom/google/android/videos/ui/CursorHelper$WatchNowEpisodesCursorHelper;->maxEpisodesPerShow:I

    move-object v0, p1

    move v1, p2

    invoke-static/range {v0 .. v6}, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow;->createRequest(Ljava/lang/String;ZJZZI)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic createCursorRequest(Ljava/lang/String;Z)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Z

    .prologue
    .line 485
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/CursorHelper$WatchNowEpisodesCursorHelper;->createCursorRequest(Ljava/lang/String;Z)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v0

    return-object v0
.end method
