.class public Lcom/google/android/videos/ui/playnext/WelcomeCardView;
.super Landroid/widget/RelativeLayout;
.source "WelcomeCardView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/videos/ui/BitmapLoader$BitmapView;


# instance fields
.field private actionButtonSeparators:[Landroid/view/View;

.field private actionButtons:[Landroid/widget/Button;

.field private detailMessageView:Landroid/widget/TextView;

.field private dimmedOverlay:Landroid/view/View;

.field private imageView:Landroid/widget/ImageView;

.field private setThumbnailCallIsAsync:Z

.field private thumbnailTag:Ljava/lang/Object;

.field private titleView:Landroid/widget/TextView;

.field private welcome:Lcom/google/android/videos/welcome/Welcome;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 54
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/videos/ui/playnext/WelcomeCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/videos/ui/playnext/WelcomeCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 62
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 63
    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;

    move-result-object v0

    invoke-interface {v0, p0, p1, p2, p3}, Lcom/google/android/play/cardview/CardViewGroupDelegate;->initialize(Landroid/view/View;Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 64
    return-void
.end method

.method private clearAllActions()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 147
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 148
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->actionButtons:[Landroid/widget/Button;

    aget-object v1, v1, v0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 149
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->actionButtonSeparators:[Landroid/view/View;

    aget-object v1, v1, v0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 147
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 151
    :cond_0
    return-void
.end method

.method private clearImage()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 171
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->imageView:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 172
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 173
    iput-object v2, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->thumbnailTag:Ljava/lang/Object;

    .line 174
    return-void
.end method

.method private setAction(III)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "iconResId"    # I
    .param p3, "titleResId"    # I

    .prologue
    const/4 v1, 0x0

    .line 154
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->actionButtons:[Landroid/widget/Button;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2, v1, v1, v1}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 155
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->actionButtons:[Landroid/widget/Button;

    aget-object v0, v0, p1

    invoke-virtual {v0, p3}, Landroid/widget/Button;->setText(I)V

    .line 156
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->actionButtons:[Landroid/widget/Button;

    aget-object v0, v0, p1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 157
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->actionButtonSeparators:[Landroid/view/View;

    aget-object v0, v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 158
    return-void
.end method

.method private setImage(I)V
    .locals 2
    .param p1, "imageResId"    # I

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 178
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->imageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 179
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->thumbnailTag:Ljava/lang/Object;

    .line 180
    return-void
.end method

.method private setImage(Lcom/google/android/videos/async/Requester;Landroid/net/Uri;I)V
    .locals 4
    .param p2, "imageUri"    # Landroid/net/Uri;
    .param p3, "missingImageResId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/net/Uri;",
            "I)V"
        }
    .end annotation

    .prologue
    .local p1, "requester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Landroid/graphics/Bitmap;>;"
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 184
    iput-boolean v3, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->setThumbnailCallIsAsync:Z

    .line 185
    if-nez p3, :cond_0

    move-object v0, v1

    .line 187
    .local v0, "missingImage":Landroid/graphics/Bitmap;
    :goto_0
    invoke-static {p0, p1, p2, v0, v1}, Lcom/google/android/videos/ui/BitmapLoader;->setBitmapAsync(Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;Lcom/google/android/videos/async/Requester;Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;)V

    .line 188
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 189
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->setThumbnailCallIsAsync:Z

    .line 190
    return-void

    .line 185
    .end local v0    # "missingImage":Landroid/graphics/Bitmap;
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2, p3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method private setText(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "textView"    # Landroid/widget/TextView;
    .param p2, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 138
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 139
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 140
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 144
    :goto_0
    return-void

    .line 142
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private startImageAnimation(Landroid/graphics/drawable/Drawable;)V
    .locals 5
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/4 v4, 0x1

    .line 219
    iget-object v3, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 220
    .local v0, "currentDrawable":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    .line 221
    const/4 v3, 0x2

    new-array v1, v3, [Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x0

    aput-object v0, v1, v3

    aput-object p1, v1, v4

    .line 222
    .local v1, "layers":[Landroid/graphics/drawable/Drawable;
    new-instance v2, Landroid/graphics/drawable/TransitionDrawable;

    invoke-direct {v2, v1}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 223
    .local v2, "transition":Landroid/graphics/drawable/TransitionDrawable;
    invoke-virtual {v2, v4}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    .line 224
    const/16 v3, 0x3e8

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 225
    move-object p1, v2

    .line 227
    .end local v1    # "layers":[Landroid/graphics/drawable/Drawable;
    .end local v2    # "transition":Landroid/graphics/drawable/TransitionDrawable;
    :cond_0
    iget-object v3, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v3, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 228
    return-void
.end method


# virtual methods
.method public getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/google/android/play/cardview/CardViewGroupDelegates;->IMPL:Lcom/google/android/play/cardview/CardViewGroupDelegate;

    return-object v0
.end method

.method public getThumbnailTag()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->thumbnailTag:Ljava/lang/Object;

    return-object v0
.end method

.method public init(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/welcome/Welcome;)V
    .locals 8
    .param p2, "welcome"    # Lcom/google/android/videos/welcome/Welcome;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/google/android/videos/welcome/Welcome;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Landroid/graphics/Bitmap;>;"
    const/4 v5, 0x0

    .line 112
    iput-object p2, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->welcome:Lcom/google/android/videos/welcome/Welcome;

    .line 114
    iget-object v6, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->titleView:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/android/videos/welcome/Welcome;->getTitle()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-direct {p0, v6, v7}, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->setText(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 115
    iget-object v6, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->detailMessageView:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/android/videos/welcome/Welcome;->getDetailMessage()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-direct {p0, v6, v7}, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->setText(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 117
    invoke-direct {p0}, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->clearAllActions()V

    .line 118
    invoke-virtual {p2}, Lcom/google/android/videos/welcome/Welcome;->getActionResIds()[I

    move-result-object v1

    .line 119
    .local v1, "actionResIds":[I
    if-nez v1, :cond_1

    move v0, v5

    .line 120
    .local v0, "actionCount":I
    :goto_0
    const/4 v6, 0x3

    if-gt v0, v6, :cond_0

    const/4 v5, 0x1

    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "view does not support "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "actions"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/play/utils/Assertions;->checkState(ZLjava/lang/String;)V

    .line 122
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v0, :cond_2

    .line 123
    mul-int/lit8 v5, v3, 0x2

    aget v5, v1, v5

    mul-int/lit8 v6, v3, 0x2

    add-int/lit8 v6, v6, 0x1

    aget v6, v1, v6

    invoke-direct {p0, v3, v5, v6}, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->setAction(III)V

    .line 122
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 119
    .end local v0    # "actionCount":I
    .end local v3    # "i":I
    :cond_1
    array-length v6, v1

    div-int/lit8 v0, v6, 0x2

    goto :goto_0

    .line 126
    .restart local v0    # "actionCount":I
    .restart local v3    # "i":I
    :cond_2
    invoke-virtual {p2}, Lcom/google/android/videos/welcome/Welcome;->getDefaultBitmapResId()I

    move-result v2

    .line 127
    .local v2, "defaultBitmapResId":I
    invoke-virtual {p2}, Lcom/google/android/videos/welcome/Welcome;->getNetworkBitmapUri()Landroid/net/Uri;

    move-result-object v4

    .line 128
    .local v4, "networkBitmapUri":Landroid/net/Uri;
    if-eqz v4, :cond_3

    .line 129
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/videos/async/Requester;

    invoke-direct {p0, v5, v4, v2}, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->setImage(Lcom/google/android/videos/async/Requester;Landroid/net/Uri;I)V

    .line 135
    :goto_2
    return-void

    .line 130
    :cond_3
    if-lez v2, :cond_4

    .line 131
    invoke-direct {p0, v2}, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->setImage(I)V

    goto :goto_2

    .line 133
    :cond_4
    invoke-direct {p0}, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->clearImage()V

    goto :goto_2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 162
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 163
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->actionButtons:[Landroid/widget/Button;

    aget-object v1, v1, v0

    if-ne p1, v1, :cond_1

    .line 164
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->welcome:Lcom/google/android/videos/welcome/Welcome;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/welcome/Welcome;->onAction(I)V

    .line 168
    :cond_0
    return-void

    .line 162
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 87
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 88
    const v0, 0x7f0f0219

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->titleView:Landroid/widget/TextView;

    .line 89
    const v0, 0x7f0f021a

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->detailMessageView:Landroid/widget/TextView;

    .line 90
    const v0, 0x7f0f0218

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->imageView:Landroid/widget/ImageView;

    .line 91
    const v0, 0x7f0f01f9

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->dimmedOverlay:Landroid/view/View;

    .line 93
    new-array v0, v5, [Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->actionButtons:[Landroid/widget/Button;

    .line 94
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->actionButtons:[Landroid/widget/Button;

    const v0, 0x7f0f0212

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    aput-object v0, v1, v2

    .line 95
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->actionButtons:[Landroid/widget/Button;

    aget-object v0, v0, v2

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->actionButtons:[Landroid/widget/Button;

    const v0, 0x7f0f0214

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    aput-object v0, v1, v3

    .line 97
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->actionButtons:[Landroid/widget/Button;

    aget-object v0, v0, v3

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->actionButtons:[Landroid/widget/Button;

    const v0, 0x7f0f0216

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    aput-object v0, v1, v4

    .line 99
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->actionButtons:[Landroid/widget/Button;

    aget-object v0, v0, v4

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    new-array v0, v5, [Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->actionButtonSeparators:[Landroid/view/View;

    .line 102
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->actionButtonSeparators:[Landroid/view/View;

    const v1, 0x7f0f0211

    invoke-virtual {p0, v1}, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v2

    .line 103
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->actionButtonSeparators:[Landroid/view/View;

    const v1, 0x7f0f0213

    invoke-virtual {p0, v1}, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v3

    .line 104
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->actionButtonSeparators:[Landroid/view/View;

    const v1, 0x7f0f0215

    invoke-virtual {p0, v1}, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v4

    .line 105
    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Lcom/google/android/play/cardview/CardViewGroupDelegate;->setBackgroundColor(Landroid/view/View;I)V

    .line 74
    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 1
    .param p1, "resid"    # I

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Lcom/google/android/play/cardview/CardViewGroupDelegate;->setBackgroundResource(Landroid/view/View;I)V

    .line 83
    return-void
.end method

.method public setDimmed(Z)V
    .locals 2
    .param p1, "dimmed"    # Z

    .prologue
    .line 108
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->dimmedOverlay:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 109
    return-void

    .line 108
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setThumbnail(Landroid/graphics/Bitmap;Z)V
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "missingBitmap"    # Z

    .prologue
    .line 204
    if-nez p1, :cond_0

    .line 205
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->imageView:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 216
    :goto_0
    return-void

    .line 209
    :cond_0
    const/16 v1, 0xa0

    invoke-virtual {p1, v1}, Landroid/graphics/Bitmap;->setDensity(I)V

    .line 210
    iget-boolean v1, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->setThumbnailCallIsAsync:Z

    if-eqz v1, :cond_1

    .line 211
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 212
    .local v0, "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-direct {p0, v0}, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->startImageAnimation(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 214
    .end local v0    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public bridge synthetic setThumbnail(Ljava/lang/Object;Z)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Z

    .prologue
    .line 36
    check-cast p1, Landroid/graphics/Bitmap;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->setThumbnail(Landroid/graphics/Bitmap;Z)V

    return-void
.end method

.method public setThumbnailTag(Ljava/lang/Object;)V
    .locals 0
    .param p1, "tag"    # Ljava/lang/Object;

    .prologue
    .line 199
    iput-object p1, p0, Lcom/google/android/videos/ui/playnext/WelcomeCardView;->thumbnailTag:Ljava/lang/Object;

    .line 200
    return-void
.end method
