.class public Lcom/google/android/videos/activity/WishlistFragment;
.super Lcom/google/android/videos/activity/HomeFragment;
.source "WishlistFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/videos/activity/HomeFragment;-><init>(I)V

    .line 26
    return-void
.end method


# virtual methods
.method protected getHeaderBottomMargin()I
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/google/android/videos/activity/WishlistFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/ui/WishlistFlowHelper;->getHeaderBottomMargin(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method protected getHeaderHeight()I
    .locals 1

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/google/android/videos/activity/WishlistFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/ui/WishlistFlowHelper;->getHeaderHeight(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method protected getTitleResourceId()I
    .locals 1

    .prologue
    .line 62
    const v0, 0x7f0b0097

    return v0
.end method

.method protected getUiElementType()I
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x5

    return v0
.end method

.method protected onCreateHelper(Lcom/google/android/videos/activity/HomeActivity;Lcom/google/android/videos/VideosGlobals;)Lcom/google/android/videos/activity/HomeFragment$HomeFragmentHelper;
    .locals 23
    .param p1, "homeActivity"    # Lcom/google/android/videos/activity/HomeActivity;
    .param p2, "videosGlobals"    # Lcom/google/android/videos/VideosGlobals;

    .prologue
    .line 31
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v3

    .line 32
    .local v3, "config":Lcom/google/android/videos/Config;
    new-instance v4, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v4, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 33
    .local v4, "mainHandler":Landroid/os/Handler;
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getWishlistStore()Lcom/google/android/videos/store/WishlistStore;

    move-result-object v6

    .line 34
    .local v6, "wishlistStore":Lcom/google/android/videos/store/WishlistStore;
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v5

    .line 35
    .local v5, "database":Lcom/google/android/videos/store/Database;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/videos/activity/HomeActivity;->getSyncHelper()Lcom/google/android/videos/ui/SyncHelper;

    move-result-object v7

    .line 36
    .local v7, "syncHelper":Lcom/google/android/videos/ui/SyncHelper;
    new-instance v1, Lcom/google/android/videos/ui/CursorHelper$WishlistMoviesCursorHelper;

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v7}, Lcom/google/android/videos/ui/CursorHelper$WishlistMoviesCursorHelper;-><init>(Lcom/google/android/videos/activity/HomeActivity;Lcom/google/android/videos/Config;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/WishlistStore;Lcom/google/android/videos/ui/SyncHelper;)V

    .line 38
    .local v1, "moviesWishlistCursorHelper":Lcom/google/android/videos/ui/CursorHelper$WishlistMoviesCursorHelper;
    new-instance v8, Lcom/google/android/videos/ui/CursorHelper$WishlistShowsCursorHelper;

    move-object/from16 v9, p1

    move-object v10, v3

    move-object v11, v4

    move-object v12, v5

    move-object v13, v6

    move-object v14, v7

    invoke-direct/range {v8 .. v14}, Lcom/google/android/videos/ui/CursorHelper$WishlistShowsCursorHelper;-><init>(Lcom/google/android/videos/activity/HomeActivity;Lcom/google/android/videos/Config;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/WishlistStore;Lcom/google/android/videos/ui/SyncHelper;)V

    .line 40
    .local v8, "showsWishlistCursorHelper":Lcom/google/android/videos/ui/CursorHelper$WishlistShowsCursorHelper;
    new-instance v9, Lcom/google/android/videos/ui/WishlistHelper;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/activity/WishlistFragment;->getView()Landroid/view/View;

    move-result-object v11

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v14

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getPosterStore()Lcom/google/android/videos/store/PosterStore;

    move-result-object v15

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v17

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/videos/activity/HomeActivity;->getSuggestionsOverflowMenuHelper()Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    move-result-object v18

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/WishlistFragment;->rootUiElementNode:Lcom/google/android/videos/logging/RootUiElementNode;

    move-object/from16 v20, v0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getUiEventLoggingHelper()Lcom/google/android/videos/logging/UiEventLoggingHelper;

    move-result-object v21

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v22

    move-object/from16 v10, p1

    move-object v12, v1

    move-object v13, v8

    move-object/from16 v16, v7

    invoke-direct/range {v9 .. v22}, Lcom/google/android/videos/ui/WishlistHelper;-><init>(Lcom/google/android/videos/activity/HomeActivity;Landroid/view/View;Lcom/google/android/videos/ui/CursorHelper;Lcom/google/android/videos/ui/CursorHelper;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PosterStore;Lcom/google/android/videos/ui/SyncHelper;Lcom/google/android/videos/api/ApiRequesters;Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/logging/UiElementNode;Lcom/google/android/videos/logging/UiEventLoggingHelper;Lcom/google/android/videos/utils/NetworkStatus;)V

    return-object v9
.end method
