.class public Lcom/google/android/videos/activity/ShowsFragment;
.super Lcom/google/android/videos/activity/HomeFragment;
.source "ShowsFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/videos/activity/HomeFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected getTitleResourceId()I
    .locals 1

    .prologue
    .line 74
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected getUiElementType()I
    .locals 1

    .prologue
    .line 63
    const/16 v0, 0x1c

    return v0
.end method

.method protected onCreateHelper(Lcom/google/android/videos/activity/HomeActivity;Lcom/google/android/videos/VideosGlobals;)Lcom/google/android/videos/activity/HomeFragment$HomeFragmentHelper;
    .locals 23
    .param p1, "homeActivity"    # Lcom/google/android/videos/activity/HomeActivity;
    .param p2, "videosGlobals"    # Lcom/google/android/videos/VideosGlobals;

    .prologue
    .line 34
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v3

    .line 35
    .local v3, "config":Lcom/google/android/videos/Config;
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStore()Lcom/google/android/videos/store/PurchaseStore;

    move-result-object v6

    .line 36
    .local v6, "purchaseStore":Lcom/google/android/videos/store/PurchaseStore;
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v5

    .line 37
    .local v5, "database":Lcom/google/android/videos/store/Database;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/videos/activity/HomeActivity;->getSyncHelper()Lcom/google/android/videos/ui/SyncHelper;

    move-result-object v7

    .line 39
    .local v7, "syncHelper":Lcom/google/android/videos/ui/SyncHelper;
    new-instance v4, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v4, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 40
    .local v4, "mainHandler":Landroid/os/Handler;
    new-instance v1, Lcom/google/android/videos/ui/CursorHelper$ShowsCursorHelper;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/activity/ShowsFragment;->getDownloadedOnlyManager()Lcom/google/android/videos/utils/DownloadedOnlyManager;

    move-result-object v8

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v8}, Lcom/google/android/videos/ui/CursorHelper$ShowsCursorHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/Config;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/ui/SyncHelper;Lcom/google/android/videos/utils/DownloadedOnlyManager;)V

    .line 42
    .local v1, "showsCursorHelper":Lcom/google/android/videos/ui/CursorHelper$ShowsCursorHelper;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/activity/ShowsFragment;->getView()Landroid/view/View;

    move-result-object v10

    .line 43
    .local v10, "mainView":Landroid/view/View;
    new-instance v8, Lcom/google/android/videos/ui/ShowsHelper;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/activity/ShowsFragment;->getDownloadedOnlyManager()Lcom/google/android/videos/utils/DownloadedOnlyManager;

    move-result-object v13

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getPosterStore()Lcom/google/android/videos/store/PosterStore;

    move-result-object v15

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getErrorHelper()Lcom/google/android/videos/utils/ErrorHelper;

    move-result-object v16

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getBitmapRequesters()Lcom/google/android/videos/bitmap/BitmapRequesters;

    move-result-object v18

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v19

    new-instance v20, Lcom/google/android/videos/logging/GenericUiElementNode;

    const/4 v2, 0x3

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/videos/activity/ShowsFragment;->rootUiElementNode:Lcom/google/android/videos/logging/RootUiElementNode;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/videos/activity/ShowsFragment;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    move-object/from16 v0, v20

    invoke-direct {v0, v2, v9, v11}, Lcom/google/android/videos/logging/GenericUiElementNode;-><init>(ILcom/google/android/videos/logging/UiElementNode;Lcom/google/android/videos/logging/UiEventLoggingHelper;)V

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v21

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getUiEventLoggingHelper()Lcom/google/android/videos/logging/UiEventLoggingHelper;

    move-result-object v22

    move-object/from16 v9, p1

    move-object v11, v1

    move-object v12, v3

    move-object v14, v5

    move-object/from16 v17, v7

    invoke-direct/range {v8 .. v22}, Lcom/google/android/videos/ui/ShowsHelper;-><init>(Lcom/google/android/videos/activity/HomeActivity;Landroid/view/View;Lcom/google/android/videos/ui/CursorHelper$ShowsCursorHelper;Lcom/google/android/videos/Config;Lcom/google/android/videos/utils/DownloadedOnlyManager;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PosterStore;Lcom/google/android/videos/utils/ErrorHelper;Lcom/google/android/videos/ui/SyncHelper;Lcom/google/android/videos/bitmap/BitmapRequesters;Lcom/google/android/videos/api/ApiRequesters;Lcom/google/android/videos/logging/UiElementNode;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/videos/logging/UiEventLoggingHelper;)V

    return-object v8
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedState"    # Landroid/os/Bundle;

    .prologue
    .line 68
    const v0, 0x7f0400b8

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
