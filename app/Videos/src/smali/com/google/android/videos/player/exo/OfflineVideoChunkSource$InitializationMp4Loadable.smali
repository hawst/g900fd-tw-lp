.class Lcom/google/android/videos/player/exo/OfflineVideoChunkSource$InitializationMp4Loadable;
.super Lcom/google/android/exoplayer/chunk/Chunk;
.source "OfflineVideoChunkSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/exo/OfflineVideoChunkSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InitializationMp4Loadable"
.end annotation


# instance fields
.field private final extractor:Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/exoplayer/upstream/DataSpec;ILcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;Lcom/google/android/exoplayer/chunk/Format;)V
    .locals 0
    .param p1, "dataSource"    # Lcom/google/android/exoplayer/upstream/DataSource;
    .param p2, "dataSpec"    # Lcom/google/android/exoplayer/upstream/DataSpec;
    .param p3, "trigger"    # I
    .param p4, "extractor"    # Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    .param p5, "format"    # Lcom/google/android/exoplayer/chunk/Format;

    .prologue
    .line 180
    invoke-direct {p0, p1, p2, p5, p3}, Lcom/google/android/exoplayer/chunk/Chunk;-><init>(Lcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/exoplayer/upstream/DataSpec;Lcom/google/android/exoplayer/chunk/Format;I)V

    .line 181
    iput-object p4, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource$InitializationMp4Loadable;->extractor:Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;

    .line 182
    return-void
.end method


# virtual methods
.method protected consumeStream(Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;)V
    .locals 3
    .param p1, "stream"    # Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 186
    iget-object v1, p0, Lcom/google/android/videos/player/exo/OfflineVideoChunkSource$InitializationMp4Loadable;->extractor:Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->read(Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;Lcom/google/android/exoplayer/SampleHolder;)I

    move-result v0

    .line 187
    .local v0, "result":I
    const/16 v1, 0x1a

    if-eq v0, v1, :cond_0

    .line 188
    new-instance v1, Lcom/google/android/exoplayer/ParserException;

    const-string v2, "Invalid initialization data"

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 190
    :cond_0
    return-void
.end method
