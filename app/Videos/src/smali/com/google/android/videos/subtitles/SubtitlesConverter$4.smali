.class final Lcom/google/android/videos/subtitles/SubtitlesConverter$4;
.super Lcom/google/android/videos/utils/XmlParser$Rule;
.source "SubtitlesConverter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/subtitles/SubtitlesConverter;->addFormat2Rules(Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/google/android/videos/utils/XmlParser$Rule;-><init>()V

    return-void
.end method


# virtual methods
.method public end(Lcom/google/android/videos/utils/Stack;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 18
    .param p2, "attrs"    # Lorg/xml/sax/Attributes;
    .param p3, "chars"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/utils/Stack",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Lorg/xml/sax/Attributes;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 104
    .local p1, "stack":Lcom/google/android/videos/utils/Stack;, "Lcom/google/android/videos/utils/Stack<Ljava/lang/Object;>;"
    const-class v15, Lcom/google/android/videos/subtitles/Subtitles$Builder;

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/google/android/videos/utils/Stack;->peek(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/videos/subtitles/Subtitles$Builder;

    .line 105
    .local v10, "builder":Lcom/google/android/videos/subtitles/Subtitles$Builder;
    const/4 v15, 0x3

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    const-string v17, "w"

    aput-object v17, v15, v16

    const/16 v16, 0x1

    const-string v17, "win"

    aput-object v17, v15, v16

    const/16 v16, 0x2

    const-string v17, "id"

    aput-object v17, v15, v16

    move-object/from16 v0, p2

    # invokes: Lcom/google/android/videos/subtitles/SubtitlesConverter;->getValue(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, v15}, Lcom/google/android/videos/subtitles/SubtitlesConverter;->access$000(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x0

    invoke-static/range {v15 .. v16}, Lcom/google/android/videos/utils/Util;->parseInt(Ljava/lang/String;I)I

    move-result v14

    .line 106
    .local v14, "windowId":I
    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    const-string v17, "t"

    aput-object v17, v15, v16

    const/16 v16, 0x1

    const-string v17, "start"

    aput-object v17, v15, v16

    move-object/from16 v0, p2

    # invokes: Lcom/google/android/videos/subtitles/SubtitlesConverter;->getValue(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, v15}, Lcom/google/android/videos/subtitles/SubtitlesConverter;->access$000(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v12

    .line 107
    .local v12, "startTimeMillis":I
    const-string v15, "op"

    move-object/from16 v0, p2

    invoke-interface {v0, v15}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 108
    .local v11, "op":Ljava/lang/String;
    if-eqz v11, :cond_4

    const-string v15, "define"

    invoke-virtual {v11, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 109
    const/16 v2, 0x22

    .line 110
    .local v2, "anchorPoint":I
    const/16 v3, 0x32

    .line 111
    .local v3, "anchorHorizontalPos":I
    const/16 v4, 0x5f

    .line 112
    .local v4, "anchorVerticalPos":I
    const/4 v5, 0x1

    .line 114
    .local v5, "visible":Z
    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    const-string v17, "ap"

    aput-object v17, v15, v16

    move-object/from16 v0, p2

    # invokes: Lcom/google/android/videos/subtitles/SubtitlesConverter;->getValue(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, v15}, Lcom/google/android/videos/subtitles/SubtitlesConverter;->access$000(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 115
    .local v8, "ap":Ljava/lang/String;
    if-eqz v8, :cond_0

    .line 116
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v15

    # invokes: Lcom/google/android/videos/subtitles/SubtitlesConverter;->anchorPointFromRawValue(I)I
    invoke-static {v15}, Lcom/google/android/videos/subtitles/SubtitlesConverter;->access$200(I)I

    move-result v2

    .line 118
    :cond_0
    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    const-string v17, "ah"

    aput-object v17, v15, v16

    move-object/from16 v0, p2

    # invokes: Lcom/google/android/videos/subtitles/SubtitlesConverter;->getValue(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, v15}, Lcom/google/android/videos/subtitles/SubtitlesConverter;->access$000(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 119
    .local v7, "ah":Ljava/lang/String;
    if-eqz v7, :cond_1

    .line 120
    const/4 v15, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Integer;->intValue()I

    move-result v16

    const/16 v17, 0x64

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->min(II)I

    move-result v16

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 123
    :cond_1
    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    const-string v17, "av"

    aput-object v17, v15, v16

    move-object/from16 v0, p2

    # invokes: Lcom/google/android/videos/subtitles/SubtitlesConverter;->getValue(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, v15}, Lcom/google/android/videos/subtitles/SubtitlesConverter;->access$000(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 124
    .local v9, "av":Ljava/lang/String;
    if-eqz v9, :cond_2

    .line 125
    const/4 v15, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Integer;->intValue()I

    move-result v16

    const/16 v17, 0x64

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->min(II)I

    move-result v16

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 128
    :cond_2
    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    const-string v17, "vs"

    aput-object v17, v15, v16

    move-object/from16 v0, p2

    # invokes: Lcom/google/android/videos/subtitles/SubtitlesConverter;->getValue(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, v15}, Lcom/google/android/videos/subtitles/SubtitlesConverter;->access$000(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 129
    .local v13, "vs":Ljava/lang/String;
    if-eqz v13, :cond_3

    .line 130
    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 132
    :cond_3
    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    const-string v17, "sd"

    aput-object v17, v15, v16

    move-object/from16 v0, p2

    # invokes: Lcom/google/android/videos/subtitles/SubtitlesConverter;->getValue(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, v15}, Lcom/google/android/videos/subtitles/SubtitlesConverter;->access$000(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    const/16 v16, -0x1

    invoke-static/range {v15 .. v16}, Lcom/google/android/videos/utils/Util;->parseInt(Ljava/lang/String;I)I

    move-result v6

    .line 134
    .local v6, "scrollDirection":I
    new-instance v1, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;

    invoke-direct/range {v1 .. v6}, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;-><init>(IIIZI)V

    .line 136
    .local v1, "settings":Lcom/google/android/videos/subtitles/SubtitleWindowSettings;
    invoke-virtual {v10, v14, v12, v1}, Lcom/google/android/videos/subtitles/Subtitles$Builder;->addSettingsToWindow(IILcom/google/android/videos/subtitles/SubtitleWindowSettings;)Lcom/google/android/videos/subtitles/Subtitles$Builder;

    .line 138
    .end local v1    # "settings":Lcom/google/android/videos/subtitles/SubtitleWindowSettings;
    .end local v2    # "anchorPoint":I
    .end local v3    # "anchorHorizontalPos":I
    .end local v4    # "anchorVerticalPos":I
    .end local v5    # "visible":Z
    .end local v6    # "scrollDirection":I
    .end local v7    # "ah":Ljava/lang/String;
    .end local v8    # "ap":Ljava/lang/String;
    .end local v9    # "av":Ljava/lang/String;
    .end local v13    # "vs":Ljava/lang/String;
    :cond_4
    return-void
.end method
