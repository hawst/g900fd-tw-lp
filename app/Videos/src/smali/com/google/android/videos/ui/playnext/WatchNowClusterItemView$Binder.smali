.class public Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView$Binder;
.super Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;
.source "WatchNowClusterItemView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Binder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder",
        "<",
        "Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;",
        "Lcom/google/android/videos/adapter/WatchNowDataSource;",
        ">;"
    }
.end annotation


# instance fields
.field private final allowDownloads:Z

.field private final screenshotRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/videos/async/Requester;ZLcom/google/android/videos/logging/UiElementNode;)V
    .locals 1
    .param p2, "allowDownloads"    # Z
    .param p3, "clusterUiElementNode"    # Lcom/google/android/videos/logging/UiElementNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;Z",
            "Lcom/google/android/videos/logging/UiElementNode;",
            ")V"
        }
    .end annotation

    .prologue
    .line 264
    .local p1, "screenshotRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    const/4 v0, 0x3

    invoke-direct {p0, p3, v0}, Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;-><init>(Lcom/google/android/videos/logging/UiElementNode;I)V

    .line 265
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView$Binder;->screenshotRequester:Lcom/google/android/videos/async/Requester;

    .line 266
    iput-boolean p2, p0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView$Binder;->allowDownloads:Z

    .line 267
    return-void
.end method


# virtual methods
.method protected bridge synthetic onBind(Lcom/google/android/videos/ui/playnext/ClusterItemView;Lcom/google/android/videos/adapter/DataSource;I)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/ui/playnext/ClusterItemView;
    .param p2, "x1"    # Lcom/google/android/videos/adapter/DataSource;
    .param p3, "x2"    # I

    .prologue
    .line 256
    check-cast p1, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;

    .end local p1    # "x0":Lcom/google/android/videos/ui/playnext/ClusterItemView;
    check-cast p2, Lcom/google/android/videos/adapter/WatchNowDataSource;

    .end local p2    # "x1":Lcom/google/android/videos/adapter/DataSource;
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView$Binder;->onBind(Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;Lcom/google/android/videos/adapter/WatchNowDataSource;I)V

    return-void
.end method

.method protected onBind(Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;Lcom/google/android/videos/adapter/WatchNowDataSource;I)V
    .locals 6
    .param p1, "view"    # Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;
    .param p2, "dataSource"    # Lcom/google/android/videos/adapter/WatchNowDataSource;
    .param p3, "index"    # I

    .prologue
    .line 271
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView$Binder;->screenshotRequester:Lcom/google/android/videos/async/Requester;

    iget-boolean v4, p0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView$Binder;->allowDownloads:Z

    move-object v0, p1

    move-object v2, p2

    move v3, p3

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->bind(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/adapter/WatchNowDataSource;IZLcom/google/android/videos/ui/playnext/WatchNowClusterItemView$Binder;)V

    .line 272
    return-void
.end method
