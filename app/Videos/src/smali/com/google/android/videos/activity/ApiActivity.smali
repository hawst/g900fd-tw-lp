.class public Lcom/google/android/videos/activity/ApiActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "ApiActivity.java"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;
.implements Lcom/google/android/videos/ui/EidrIdConverterHelper$EidrIdConverterListener;
.implements Lcom/google/android/videos/ui/PinHelper$PinListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/activity/ApiActivity$MaxWidthLinearLayout;,
        Lcom/google/android/videos/activity/ApiActivity$PostPurchaseDialog;,
        Lcom/google/android/videos/activity/ApiActivity$AddAccountDialog;,
        Lcom/google/android/videos/activity/ApiActivity$NotFoundDialog;,
        Lcom/google/android/videos/activity/ApiActivity$InProgressDialogFragment;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/app/FragmentActivity;",
        "Landroid/accounts/AccountManagerCallback",
        "<",
        "Landroid/os/Bundle;",
        ">;",
        "Lcom/google/android/videos/ui/EidrIdConverterHelper$EidrIdConverterListener;",
        "Lcom/google/android/videos/ui/PinHelper$PinListener;"
    }
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private accountManager:Lcom/google/android/videos/accounts/AccountManagerWrapper;

.field private action:I

.field private directPurchaseFlowResultHandler:Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler;

.field private eidrId:Ljava/lang/String;

.field private eidrIdConverterHelper:Lcom/google/android/videos/ui/EidrIdConverterHelper;

.field private eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private paused:Z

.field private pendingTask:I

.field private referer:Ljava/lang/String;

.field private videoId:Ljava/lang/String;

.field private videosGlobals:Lcom/google/android/videos/VideosGlobals;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 95
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/activity/ApiActivity;->action:I

    .line 567
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/activity/ApiActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/activity/ApiActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/videos/activity/ApiActivity;->eidrId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/activity/ApiActivity;)Lcom/google/android/videos/ui/EidrIdConverterHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/activity/ApiActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/videos/activity/ApiActivity;->eidrIdConverterHelper:Lcom/google/android/videos/ui/EidrIdConverterHelper;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/activity/ApiActivity;ILjava/lang/Throwable;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/activity/ApiActivity;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/Throwable;

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/activity/ApiActivity;->finishWithFailure(ILjava/lang/Throwable;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/videos/activity/ApiActivity;)Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/activity/ApiActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/videos/activity/ApiActivity;->accountManager:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/videos/activity/ApiActivity;)Lcom/google/android/videos/logging/EventLogger;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/activity/ApiActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/videos/activity/ApiActivity;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/videos/activity/ApiActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/activity/ApiActivity;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/android/videos/activity/ApiActivity;->finishWithSuccess()V

    return-void
.end method

.method private checkAccount()V
    .locals 4

    .prologue
    .line 281
    iget-object v1, p0, Lcom/google/android/videos/activity/ApiActivity;->accountManager:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    iget-object v2, p0, Lcom/google/android/videos/activity/ApiActivity;->account:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->accountExists(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 282
    iget-object v1, p0, Lcom/google/android/videos/activity/ApiActivity;->account:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/google/android/videos/activity/ApiActivity$AddAccountDialog;->showInstance(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;)V

    .line 299
    :goto_0
    return-void

    .line 285
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/activity/ApiActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->hasPremiumError()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 286
    iget-object v1, p0, Lcom/google/android/videos/activity/ApiActivity;->account:Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {p0, v1, v2, v3}, Lcom/google/android/videos/activity/HomeActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/videos/activity/ApiActivity;->startActivity(Landroid/content/Intent;)V

    .line 287
    const/4 v1, 0x2

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/activity/ApiActivity;->finishWithFailure(ILjava/lang/Throwable;)V

    goto :goto_0

    .line 292
    :cond_1
    iget v1, p0, Lcom/google/android/videos/activity/ApiActivity;->action:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    iget v1, p0, Lcom/google/android/videos/activity/ApiActivity;->action:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 293
    :cond_2
    const/4 v0, 0x2

    .line 297
    .local v0, "eidrIdSource":I
    :goto_1
    new-instance v1, Lcom/google/android/videos/ui/EidrIdConverterHelper;

    iget-object v2, p0, Lcom/google/android/videos/activity/ApiActivity;->account:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/activity/ApiActivity;->eidrId:Ljava/lang/String;

    invoke-direct {v1, p0, v2, v3, p0}, Lcom/google/android/videos/ui/EidrIdConverterHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/ui/EidrIdConverterHelper$EidrIdConverterListener;)V

    iput-object v1, p0, Lcom/google/android/videos/activity/ApiActivity;->eidrIdConverterHelper:Lcom/google/android/videos/ui/EidrIdConverterHelper;

    .line 298
    iget-object v1, p0, Lcom/google/android/videos/activity/ApiActivity;->eidrIdConverterHelper:Lcom/google/android/videos/ui/EidrIdConverterHelper;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/ui/EidrIdConverterHelper;->convert(I)V

    goto :goto_0

    .line 295
    .end local v0    # "eidrIdSource":I
    :cond_3
    const/4 v0, 0x1

    .restart local v0    # "eidrIdSource":I
    goto :goto_1
.end method

.method private finishWithFailure(ILjava/lang/Throwable;)V
    .locals 4
    .param p1, "resultCode"    # I
    .param p2, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 348
    iget-object v0, p0, Lcom/google/android/videos/activity/ApiActivity;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget v1, p0, Lcom/google/android/videos/activity/ApiActivity;->action:I

    iget-object v2, p0, Lcom/google/android/videos/activity/ApiActivity;->eidrId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/activity/ApiActivity;->referer:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3, p2}, Lcom/google/android/videos/logging/EventLogger;->onExternalApiCallFailed(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 349
    invoke-virtual {p0, p1}, Lcom/google/android/videos/activity/ApiActivity;->setResult(I)V

    .line 350
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ApiActivity;->finish()V

    .line 351
    return-void
.end method

.method private finishWithSuccess()V
    .locals 4

    .prologue
    .line 342
    iget-object v0, p0, Lcom/google/android/videos/activity/ApiActivity;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget v1, p0, Lcom/google/android/videos/activity/ApiActivity;->action:I

    iget-object v2, p0, Lcom/google/android/videos/activity/ApiActivity;->eidrId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/activity/ApiActivity;->referer:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/videos/logging/EventLogger;->onExternalApiCallSuccess(ILjava/lang/String;Ljava/lang/String;)V

    .line 343
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/ApiActivity;->setResult(I)V

    .line 344
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ApiActivity;->finish()V

    .line 345
    return-void
.end method

.method private logDirectPurchaseResult(I)V
    .locals 7
    .param p1, "result"    # I

    .prologue
    const/4 v5, 0x0

    .line 276
    iget-object v0, p0, Lcom/google/android/videos/activity/ApiActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v0

    const/16 v1, 0xc

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/videos/activity/ApiActivity;->videoId:Ljava/lang/String;

    move v2, p1

    move-object v6, v5

    invoke-interface/range {v0 .. v6}, Lcom/google/android/videos/logging/EventLogger;->onDirectPurchase(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "result"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v1, -0x1

    .line 211
    iget-object v0, p0, Lcom/google/android/videos/activity/ApiActivity;->directPurchaseFlowResultHandler:Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/videos/activity/ApiActivity;->directPurchaseFlowResultHandler:Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler;->onActivityResult(IILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 213
    if-eq p2, v1, :cond_0

    .line 214
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/google/android/videos/activity/ApiActivity;->logDirectPurchaseResult(I)V

    .line 215
    const/4 v0, 0x0

    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/activity/ApiActivity;->finishWithFailure(ILjava/lang/Throwable;)V

    .line 229
    :goto_0
    return-void

    .line 217
    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/videos/activity/ApiActivity;->logDirectPurchaseResult(I)V

    .line 222
    invoke-direct {p0}, Lcom/google/android/videos/activity/ApiActivity;->finishWithSuccess()V

    goto :goto_0

    .line 227
    :cond_1
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x1

    .line 109
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 111
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/activity/ApiActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    .line 112
    iget-object v5, p0, Lcom/google/android/videos/activity/ApiActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v5}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/activity/ApiActivity;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 114
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ApiActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 115
    .local v2, "packageManager":Landroid/content/pm/PackageManager;
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ApiActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/videos/activity/ApiActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v6}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v6

    invoke-static {v5, v2, v6}, Lcom/google/android/videos/utils/TrustedAppUtil;->getTrustedAppReferer(Ljava/lang/String;Landroid/content/pm/PackageManager;Lcom/google/android/videos/Config;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/activity/ApiActivity;->referer:Ljava/lang/String;

    .line 117
    iget-object v5, p0, Lcom/google/android/videos/activity/ApiActivity;->referer:Ljava/lang/String;

    if-nez v5, :cond_0

    .line 118
    new-instance v5, Ljava/lang/Throwable;

    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    invoke-direct {p0, v9, v5}, Lcom/google/android/videos/activity/ApiActivity;->finishWithFailure(ILjava/lang/Throwable;)V

    .line 165
    :goto_0
    return-void

    .line 122
    :cond_0
    iget-object v5, p0, Lcom/google/android/videos/activity/ApiActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v5}, Lcom/google/android/videos/VideosGlobals;->updatePremiumStatus()I

    move-result v3

    .line 123
    .local v3, "premiumStatus":I
    iget-object v5, p0, Lcom/google/android/videos/activity/ApiActivity;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget-object v6, p0, Lcom/google/android/videos/activity/ApiActivity;->referer:Ljava/lang/String;

    invoke-interface {v5, p0, v3, v6}, Lcom/google/android/videos/logging/EventLogger;->onVideosStart(Landroid/app/Activity;ILjava/lang/String;)V

    .line 125
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ApiActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 126
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 127
    .local v0, "actionString":Ljava/lang/String;
    const-string v5, "com.google.android.videos.intent.action.PLAY"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 128
    iput v9, p0, Lcom/google/android/videos/activity/ApiActivity;->action:I

    .line 141
    :goto_1
    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    .line 142
    .local v4, "uri":Landroid/net/Uri;
    if-nez v4, :cond_5

    .line 143
    new-instance v5, Ljava/lang/Throwable;

    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    invoke-direct {p0, v9, v5}, Lcom/google/android/videos/activity/ApiActivity;->finishWithFailure(ILjava/lang/Throwable;)V

    goto :goto_0

    .line 129
    .end local v4    # "uri":Landroid/net/Uri;
    :cond_1
    const-string v5, "com.google.android.videos.intent.action.PIN"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 130
    const/4 v5, 0x2

    iput v5, p0, Lcom/google/android/videos/activity/ApiActivity;->action:I

    goto :goto_1

    .line 131
    :cond_2
    const-string v5, "com.google.android.videos.intent.action.UNPIN"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 132
    const/4 v5, 0x3

    iput v5, p0, Lcom/google/android/videos/activity/ApiActivity;->action:I

    goto :goto_1

    .line 133
    :cond_3
    const-string v5, "com.google.android.videos.intent.action.PURCHASE"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 134
    const/4 v5, 0x4

    iput v5, p0, Lcom/google/android/videos/activity/ApiActivity;->action:I

    goto :goto_1

    .line 136
    :cond_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unexpected action: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 137
    new-instance v5, Ljava/lang/Throwable;

    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    invoke-direct {p0, v9, v5}, Lcom/google/android/videos/activity/ApiActivity;->finishWithFailure(ILjava/lang/Throwable;)V

    goto :goto_0

    .line 147
    .restart local v4    # "uri":Landroid/net/Uri;
    :cond_5
    const-string v5, "eidr"

    invoke-virtual {v4, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/videos/utils/EidrId;->convertToCompactEidr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/activity/ApiActivity;->eidrId:Ljava/lang/String;

    .line 148
    iget-object v5, p0, Lcom/google/android/videos/activity/ApiActivity;->eidrId:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/videos/utils/EidrId;->isValidCompactEidr(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 149
    iget-object v5, p0, Lcom/google/android/videos/activity/ApiActivity;->eidrId:Ljava/lang/String;

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/google/android/videos/activity/ApiActivity;->eidrId:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v6, 0x1b

    if-le v5, v6, :cond_6

    .line 151
    iget-object v5, p0, Lcom/google/android/videos/activity/ApiActivity;->eidrId:Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/videos/activity/ApiActivity;->eidrId:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    const/16 v8, 0x36

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/activity/ApiActivity;->eidrId:Ljava/lang/String;

    .line 153
    :cond_6
    new-instance v5, Ljava/lang/Throwable;

    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    invoke-direct {p0, v9, v5}, Lcom/google/android/videos/activity/ApiActivity;->finishWithFailure(ILjava/lang/Throwable;)V

    goto/16 :goto_0

    .line 157
    :cond_7
    const-string v5, "authAccount"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/activity/ApiActivity;->account:Ljava/lang/String;

    .line 158
    iget-object v5, p0, Lcom/google/android/videos/activity/ApiActivity;->account:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 159
    new-instance v5, Ljava/lang/Throwable;

    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    invoke-direct {p0, v9, v5}, Lcom/google/android/videos/activity/ApiActivity;->finishWithFailure(ILjava/lang/Throwable;)V

    goto/16 :goto_0

    .line 163
    :cond_8
    iget-object v5, p0, Lcom/google/android/videos/activity/ApiActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v5}, Lcom/google/android/videos/VideosGlobals;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/activity/ApiActivity;->accountManager:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    .line 164
    invoke-direct {p0}, Lcom/google/android/videos/activity/ApiActivity;->checkAccount()V

    goto/16 :goto_0
.end method

.method public onEidrIdConversionFailed()V
    .locals 1

    .prologue
    .line 333
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/ApiActivity;->performTask(I)V

    .line 334
    return-void
.end method

.method public onEidrIdConversionFinished(Ljava/lang/String;)V
    .locals 7
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x3

    const/4 v2, 0x1

    .line 305
    iput-object p1, p0, Lcom/google/android/videos/activity/ApiActivity;->videoId:Ljava/lang/String;

    .line 306
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Found "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/activity/ApiActivity;->eidrId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 307
    invoke-static {p0}, Lcom/google/android/videos/activity/ApiActivity$InProgressDialogFragment;->dismiss(Landroid/support/v4/app/FragmentActivity;)V

    .line 309
    iget v0, p0, Lcom/google/android/videos/activity/ApiActivity;->action:I

    if-ne v0, v2, :cond_1

    .line 310
    iget-object v1, p0, Lcom/google/android/videos/activity/ApiActivity;->account:Ljava/lang/String;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/activity/WatchActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/ApiActivity;->startActivity(Landroid/content/Intent;)V

    .line 311
    invoke-direct {p0}, Lcom/google/android/videos/activity/ApiActivity;->finishWithSuccess()V

    .line 329
    :cond_0
    :goto_0
    return-void

    .line 312
    :cond_1
    iget v0, p0, Lcom/google/android/videos/activity/ApiActivity;->action:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 313
    new-instance v0, Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler;

    iget-object v1, p0, Lcom/google/android/videos/activity/ApiActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStoreSync()Lcom/google/android/videos/store/PurchaseStoreSync;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler;-><init>(Lcom/google/android/videos/store/PurchaseStoreSync;)V

    iput-object v0, p0, Lcom/google/android/videos/activity/ApiActivity;->directPurchaseFlowResultHandler:Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler;

    .line 315
    iget-object v0, p0, Lcom/google/android/videos/activity/ApiActivity;->account:Ljava/lang/String;

    invoke-static {p0, p1, v0, v2}, Lcom/google/android/videos/utils/PlayStoreUtil;->startMovieDirectPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v6

    .line 317
    .local v6, "result":I
    const/4 v0, -0x1

    if-eq v6, v0, :cond_0

    .line 318
    invoke-direct {p0, v6}, Lcom/google/android/videos/activity/ApiActivity;->logDirectPurchaseResult(I)V

    .line 319
    new-instance v0, Ljava/lang/Throwable;

    invoke-direct {v0}, Ljava/lang/Throwable;-><init>()V

    invoke-direct {p0, v4, v0}, Lcom/google/android/videos/activity/ApiActivity;->finishWithFailure(ILjava/lang/Throwable;)V

    goto :goto_0

    .line 322
    .end local v6    # "result":I
    :cond_2
    iget v0, p0, Lcom/google/android/videos/activity/ApiActivity;->action:I

    if-ne v0, v4, :cond_3

    .line 323
    iget-object v0, p0, Lcom/google/android/videos/activity/ApiActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getPinHelper()Lcom/google/android/videos/ui/PinHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/activity/ApiActivity;->account:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/videos/ui/PinHelper;->unpinVideo(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ApiActivity;->onUnpinRequested()V

    goto :goto_0

    .line 326
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/activity/ApiActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getPinHelper()Lcom/google/android/videos/ui/PinHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/activity/ApiActivity;->account:Ljava/lang/String;

    invoke-virtual {v0, p0, v1, p1}, Lcom/google/android/videos/ui/PinHelper;->pinVideo(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onNetworkRequest()V
    .locals 1

    .prologue
    .line 338
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/ApiActivity;->performTask(I)V

    .line 339
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 169
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onPause()V

    .line 170
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/activity/ApiActivity;->paused:Z

    .line 171
    return-void
.end method

.method public onPinCanceled()V
    .locals 2

    .prologue
    .line 267
    const/4 v0, 0x0

    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/activity/ApiActivity;->finishWithFailure(ILjava/lang/Throwable;)V

    .line 268
    return-void
.end method

.method public onPinFailed()V
    .locals 2

    .prologue
    .line 272
    const/4 v0, 0x3

    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/activity/ApiActivity;->finishWithFailure(ILjava/lang/Throwable;)V

    .line 273
    return-void
.end method

.method public onPinRequested()V
    .locals 0

    .prologue
    .line 257
    invoke-direct {p0}, Lcom/google/android/videos/activity/ApiActivity;->finishWithSuccess()V

    .line 258
    return-void
.end method

.method protected onResumeFragments()V
    .locals 1

    .prologue
    .line 175
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResumeFragments()V

    .line 176
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/activity/ApiActivity;->paused:Z

    .line 177
    iget v0, p0, Lcom/google/android/videos/activity/ApiActivity;->pendingTask:I

    if-eqz v0, :cond_0

    .line 178
    iget v0, p0, Lcom/google/android/videos/activity/ApiActivity;->pendingTask:I

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/ApiActivity;->performTask(I)V

    .line 180
    :cond_0
    return-void
.end method

.method public onUnpinRequested()V
    .locals 0

    .prologue
    .line 262
    invoke-direct {p0}, Lcom/google/android/videos/activity/ApiActivity;->finishWithSuccess()V

    .line 263
    return-void
.end method

.method performTask(I)V
    .locals 4
    .param p1, "task"    # I

    .prologue
    .line 183
    iput p1, p0, Lcom/google/android/videos/activity/ApiActivity;->pendingTask:I

    .line 184
    iget-boolean v0, p0, Lcom/google/android/videos/activity/ApiActivity;->paused:Z

    if-eqz v0, :cond_0

    .line 207
    :goto_0
    return-void

    .line 187
    :cond_0
    iget v0, p0, Lcom/google/android/videos/activity/ApiActivity;->pendingTask:I

    packed-switch v0, :pswitch_data_0

    .line 206
    :goto_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/activity/ApiActivity;->pendingTask:I

    goto :goto_0

    .line 189
    :pswitch_0
    invoke-static {p0}, Lcom/google/android/videos/activity/ApiActivity$InProgressDialogFragment;->showInstance(Lcom/google/android/videos/activity/ApiActivity;)V

    goto :goto_1

    .line 193
    :pswitch_1
    invoke-static {p0}, Lcom/google/android/videos/activity/ApiActivity$InProgressDialogFragment;->dismiss(Landroid/support/v4/app/FragmentActivity;)V

    .line 194
    invoke-static {p0}, Lcom/google/android/videos/activity/ApiActivity$NotFoundDialog;->showInstance(Landroid/support/v4/app/FragmentActivity;)V

    goto :goto_1

    .line 198
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/videos/activity/ApiActivity;->checkAccount()V

    goto :goto_1

    .line 202
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/videos/activity/ApiActivity;->account:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/videos/activity/ApiActivity;->videoId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/activity/ApiActivity;->referer:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/videos/activity/ApiActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v0, v1, v2, v3}, Lcom/google/android/videos/activity/ApiActivity$PostPurchaseDialog;->showInstance(Lcom/google/android/videos/activity/ApiActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 187
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "future":Landroid/accounts/AccountManagerFuture;, "Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    const/4 v2, 0x3

    .line 236
    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2

    .line 250
    invoke-virtual {p0, v2}, Lcom/google/android/videos/activity/ApiActivity;->performTask(I)V

    .line 251
    :goto_0
    return-void

    .line 237
    :catch_0
    move-exception v0

    .line 238
    .local v0, "e":Landroid/accounts/OperationCanceledException;
    const-string v1, "added account canceled"

    invoke-static {v1}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 239
    const/4 v1, 0x0

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/activity/ApiActivity;->finishWithFailure(ILjava/lang/Throwable;)V

    goto :goto_0

    .line 241
    .end local v0    # "e":Landroid/accounts/OperationCanceledException;
    :catch_1
    move-exception v0

    .line 242
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "added account failed"

    invoke-static {v1, v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 243
    invoke-direct {p0, v2, v0}, Lcom/google/android/videos/activity/ApiActivity;->finishWithFailure(ILjava/lang/Throwable;)V

    goto :goto_0

    .line 245
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 246
    .local v0, "e":Landroid/accounts/AuthenticatorException;
    const-string v1, "added account failed"

    invoke-static {v1, v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 247
    invoke-direct {p0, v2, v0}, Lcom/google/android/videos/activity/ApiActivity;->finishWithFailure(ILjava/lang/Throwable;)V

    goto :goto_0
.end method
