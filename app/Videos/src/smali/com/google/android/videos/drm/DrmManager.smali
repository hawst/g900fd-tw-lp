.class public abstract Lcom/google/android/videos/drm/DrmManager;
.super Ljava/lang/Object;
.source "DrmManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/drm/DrmManager$DrmRequestRunnable;,
        Lcom/google/android/videos/drm/DrmManager$Identifiers;,
        Lcom/google/android/videos/drm/DrmManager$Listener;,
        Lcom/google/android/videos/drm/DrmManager$StopReason;,
        Lcom/google/android/videos/drm/DrmManager$Provider;
    }
.end annotation


# static fields
.field private static final DRM_ERROR_REGEX:Ljava/util/regex/Pattern;


# instance fields
.field private final authenticatingRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/drm/DrmRequest;",
            "Lcom/google/android/videos/drm/DrmResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final eventDispatchHandler:Landroid/os/Handler;

.field private listener:Lcom/google/android/videos/drm/DrmManager$Listener;

.field private final nonAuthenticatingRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/drm/DrmRequest;",
            "Lcom/google/android/videos/drm/DrmResponse;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 230
    const-string v0, "(code = |lmResult=|wvstatus=)(\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/videos/drm/DrmManager;->DRM_ERROR_REGEX:Ljava/util/regex/Pattern;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/videos/accounts/AccountManagerWrapper;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "networkExecutor"    # Ljava/util/concurrent/Executor;
    .param p3, "drmExecutor"    # Ljava/util/concurrent/Executor;
    .param p4, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;

    .prologue
    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    invoke-direct {p0, p3}, Lcom/google/android/videos/drm/DrmManager;->createNonAuthenticatingRequester(Ljava/util/concurrent/Executor;)Lcom/google/android/videos/async/Requester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/drm/DrmManager;->nonAuthenticatingRequester:Lcom/google/android/videos/async/Requester;

    .line 151
    new-instance v0, Lcom/google/android/videos/drm/DrmAuthenticatingRequester;

    iget-object v1, p0, Lcom/google/android/videos/drm/DrmManager;->nonAuthenticatingRequester:Lcom/google/android/videos/async/Requester;

    invoke-direct {v0, p4, v1}, Lcom/google/android/videos/drm/DrmAuthenticatingRequester;-><init>(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/async/Requester;)V

    invoke-static {p2, v0}, Lcom/google/android/videos/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/AsyncRequester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/drm/DrmManager;->authenticatingRequester:Lcom/google/android/videos/async/Requester;

    .line 153
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/videos/drm/DrmManager;->eventDispatchHandler:Landroid/os/Handler;

    .line 154
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/drm/DrmManager;)Lcom/google/android/videos/drm/DrmManager$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/drm/DrmManager;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/videos/drm/DrmManager;->listener:Lcom/google/android/videos/drm/DrmManager$Listener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/drm/DrmManager;Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/async/Callback;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/drm/DrmManager;
    .param p1, "x1"    # Lcom/google/android/videos/drm/DrmRequest;
    .param p2, "x2"    # Lcom/google/android/videos/async/Callback;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/drm/DrmManager;->requestLicense(Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/async/Callback;)V

    return-void
.end method

.method private final createNonAuthenticatingRequester(Ljava/util/concurrent/Executor;)Lcom/google/android/videos/async/Requester;
    .locals 1
    .param p1, "drmExecutor"    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            ")",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/drm/DrmRequest;",
            "Lcom/google/android/videos/drm/DrmResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 290
    new-instance v0, Lcom/google/android/videos/drm/DrmManager$3;

    invoke-direct {v0, p0, p1}, Lcom/google/android/videos/drm/DrmManager$3;-><init>(Lcom/google/android/videos/drm/DrmManager;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method

.method private static isWidevineEnabled(Landroid/content/Context;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/16 v8, 0x10

    .line 104
    new-instance v1, Landroid/drm/DrmManagerClient;

    invoke-direct {v1, p0}, Landroid/drm/DrmManagerClient;-><init>(Landroid/content/Context;)V

    .line 106
    .local v1, "drmClient":Landroid/drm/DrmManagerClient;
    :try_start_0
    invoke-virtual {v1}, Landroid/drm/DrmManagerClient;->getAvailableDrmEngines()[Ljava/lang/String;

    move-result-object v3

    .line 107
    .local v3, "drmEngines":[Ljava/lang/String;
    move-object v0, v3

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v2, v0, v4

    .line 108
    .local v2, "drmEngine":Ljava/lang/String;
    if-eqz v2, :cond_1

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "widevine"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    if-eqz v6, :cond_1

    .line 109
    const/4 v6, 0x1

    .line 114
    sget v7, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    if-lt v7, v8, :cond_0

    .line 115
    invoke-static {v1}, Lcom/google/android/videos/drm/DrmManager;->releaseDrmManagerClient(Landroid/drm/DrmManagerClient;)V

    .end local v2    # "drmEngine":Ljava/lang/String;
    :cond_0
    :goto_1
    return v6

    .line 107
    .restart local v2    # "drmEngine":Ljava/lang/String;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 112
    .end local v2    # "drmEngine":Ljava/lang/String;
    :cond_2
    const/4 v6, 0x0

    .line 114
    sget v7, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    if-lt v7, v8, :cond_0

    .line 115
    invoke-static {v1}, Lcom/google/android/videos/drm/DrmManager;->releaseDrmManagerClient(Landroid/drm/DrmManagerClient;)V

    goto :goto_1

    .line 114
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v3    # "drmEngines":[Ljava/lang/String;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :catchall_0
    move-exception v6

    sget v7, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    if-lt v7, v8, :cond_3

    .line 115
    invoke-static {v1}, Lcom/google/android/videos/drm/DrmManager;->releaseDrmManagerClient(Landroid/drm/DrmManagerClient;)V

    :cond_3
    throw v6
.end method

.method public static newDrmManager(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/Config;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/videos/logging/EventLogger;Z)Lcom/google/android/videos/drm/DrmManager;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "networkExecutor"    # Ljava/util/concurrent/Executor;
    .param p2, "drmExecutor"    # Ljava/util/concurrent/Executor;
    .param p3, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .param p4, "config"    # Lcom/google/android/videos/Config;
    .param p5, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;
    .param p6, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;
    .param p7, "forceAppLevelDrm"    # Z

    .prologue
    .line 94
    if-nez p7, :cond_0

    invoke-static {p0}, Lcom/google/android/videos/drm/DrmManager;->isWidevineEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 95
    :cond_0
    new-instance v0, Lcom/google/android/videos/drm/DrmManagerV8;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/drm/DrmManagerV8;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/Config;Lcom/google/android/videos/logging/EventLogger;)V

    .line 98
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/google/android/videos/drm/DrmManagerV12;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/drm/DrmManagerV12;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/Config;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/videos/logging/EventLogger;)V

    goto :goto_0
.end method

.method private static releaseDrmManagerClient(Landroid/drm/DrmManagerClient;)V
    .locals 0
    .param p0, "client"    # Landroid/drm/DrmManagerClient;

    .prologue
    .line 122
    invoke-virtual {p0}, Landroid/drm/DrmManagerClient;->release()V

    .line 123
    return-void
.end method

.method private requestLicense(Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/async/Callback;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/drm/DrmRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/drm/DrmRequest;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/drm/DrmRequest;",
            "Lcom/google/android/videos/drm/DrmResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 222
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/drm/DrmResponse;>;"
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    iget-object v0, p1, Lcom/google/android/videos/drm/DrmRequest;->type:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    iget-boolean v0, v0, Lcom/google/android/videos/drm/DrmRequest$RequestType;->isOffline:Z

    if-eqz v0, :cond_0

    .line 224
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/drm/DrmManager;->requestOfflineRights(Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/async/Callback;)V

    .line 228
    :goto_0
    return-void

    .line 226
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/android/videos/drm/DrmManager;->requestRights(Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/drm/DrmResponse;Lcom/google/android/videos/async/Callback;)V

    goto :goto_0
.end method


# virtual methods
.method protected getDrmError(I)Lcom/google/android/videos/drm/DrmException$DrmError;
    .locals 1
    .param p1, "errorCode"    # I

    .prologue
    .line 252
    sparse-switch p1, :sswitch_data_0

    .line 284
    sget-object v0, Lcom/google/android/videos/drm/DrmException$DrmError;->UNKNOWN:Lcom/google/android/videos/drm/DrmException$DrmError;

    :goto_0
    return-object v0

    .line 256
    :sswitch_0
    sget-object v0, Lcom/google/android/videos/drm/DrmException$DrmError;->LICENSE_PINNED:Lcom/google/android/videos/drm/DrmException$DrmError;

    goto :goto_0

    .line 260
    :sswitch_1
    sget-object v0, Lcom/google/android/videos/drm/DrmException$DrmError;->AUTHENTICATION_FAILED:Lcom/google/android/videos/drm/DrmException$DrmError;

    goto :goto_0

    .line 262
    :sswitch_2
    sget-object v0, Lcom/google/android/videos/drm/DrmException$DrmError;->USER_GEO_RESTRICTED:Lcom/google/android/videos/drm/DrmException$DrmError;

    goto :goto_0

    .line 264
    :sswitch_3
    sget-object v0, Lcom/google/android/videos/drm/DrmException$DrmError;->NO_LICENSE:Lcom/google/android/videos/drm/DrmException$DrmError;

    goto :goto_0

    .line 266
    :sswitch_4
    sget-object v0, Lcom/google/android/videos/drm/DrmException$DrmError;->TOO_MANY_ACTIVE_DEVICES_FOR_ACCOUNT:Lcom/google/android/videos/drm/DrmException$DrmError;

    goto :goto_0

    .line 268
    :sswitch_5
    sget-object v0, Lcom/google/android/videos/drm/DrmException$DrmError;->TOO_MANY_ACCOUNTS_ON_DEVICE:Lcom/google/android/videos/drm/DrmException$DrmError;

    goto :goto_0

    .line 270
    :sswitch_6
    sget-object v0, Lcom/google/android/videos/drm/DrmException$DrmError;->TOO_MANY_DEVICE_DEACTIVATIONS_ON_ACCOUNT:Lcom/google/android/videos/drm/DrmException$DrmError;

    goto :goto_0

    .line 272
    :sswitch_7
    sget-object v0, Lcom/google/android/videos/drm/DrmException$DrmError;->TOO_MANY_ACTIVATIONS_ON_DEVICE:Lcom/google/android/videos/drm/DrmException$DrmError;

    goto :goto_0

    .line 274
    :sswitch_8
    sget-object v0, Lcom/google/android/videos/drm/DrmException$DrmError;->STREAMING_DEVICES_QUOTA_EXCEEDED:Lcom/google/android/videos/drm/DrmException$DrmError;

    goto :goto_0

    .line 278
    :sswitch_9
    sget-object v0, Lcom/google/android/videos/drm/DrmException$DrmError;->UNPIN_SUCCESSFUL:Lcom/google/android/videos/drm/DrmException$DrmError;

    goto :goto_0

    .line 252
    :sswitch_data_0
    .sparse-switch
        0x258 -> :sswitch_3
        0x259 -> :sswitch_9
        0x321 -> :sswitch_2
        0x331 -> :sswitch_0
        0x334 -> :sswitch_1
        0x336 -> :sswitch_1
        0x339 -> :sswitch_0
        0x33a -> :sswitch_4
        0x33b -> :sswitch_5
        0x33c -> :sswitch_6
        0x33d -> :sswitch_7
        0x33e -> :sswitch_9
        0x33f -> :sswitch_8
    .end sparse-switch
.end method

.method public abstract getDrmLevel()I
.end method

.method public abstract getPlayableUri(Landroid/net/Uri;Z)Landroid/net/Uri;
.end method

.method public final isDisabled()Z
    .locals 1

    .prologue
    .line 172
    invoke-virtual {p0}, Lcom/google/android/videos/drm/DrmManager;->getDrmLevel()I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected notifyHeartbeatError(Ljava/lang/String;I)V
    .locals 2
    .param p1, "assetPathKey"    # Ljava/lang/String;
    .param p2, "errorCode"    # I

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/videos/drm/DrmManager;->eventDispatchHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/videos/drm/DrmManager$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/videos/drm/DrmManager$1;-><init>(Lcom/google/android/videos/drm/DrmManager;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 201
    return-void
.end method

.method protected notifyPlaybackStopped(Ljava/lang/String;Lcom/google/android/videos/drm/DrmManager$StopReason;)V
    .locals 2
    .param p1, "assetPathKey"    # Ljava/lang/String;
    .param p2, "reason"    # Lcom/google/android/videos/drm/DrmManager$StopReason;

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/videos/drm/DrmManager;->eventDispatchHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/videos/drm/DrmManager$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/videos/drm/DrmManager$2;-><init>(Lcom/google/android/videos/drm/DrmManager;Ljava/lang/String;Lcom/google/android/videos/drm/DrmManager$StopReason;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 212
    return-void
.end method

.method protected parseErrorCode(Ljava/lang/String;)I
    .locals 2
    .param p1, "error"    # Ljava/lang/String;

    .prologue
    .line 234
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 235
    const/4 v1, -0x1

    .line 246
    :goto_0
    return v1

    .line 236
    :cond_0
    const-string v1, "ok"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 237
    const/4 v1, 0x0

    goto :goto_0

    .line 238
    :cond_1
    const-string v1, "LM Response code = 18"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 240
    const/16 v1, 0x258

    goto :goto_0

    .line 242
    :cond_2
    sget-object v1, Lcom/google/android/videos/drm/DrmManager;->DRM_ERROR_REGEX:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 243
    .local v0, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 244
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0

    .line 246
    :cond_3
    const/4 v1, -0x2

    goto :goto_0
.end method

.method public request(Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/async/Callback;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/drm/DrmRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/drm/DrmRequest;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/drm/DrmRequest;",
            "Lcom/google/android/videos/drm/DrmResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 178
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/drm/DrmResponse;>;"
    iget-object v0, p1, Lcom/google/android/videos/drm/DrmRequest;->account:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/google/android/videos/drm/DrmManager;->nonAuthenticatingRequester:Lcom/google/android/videos/async/Requester;

    invoke-interface {v0, p1, p2}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 184
    :goto_0
    return-void

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/drm/DrmManager;->authenticatingRequester:Lcom/google/android/videos/async/Requester;

    invoke-interface {v0, p1, p2}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    goto :goto_0
.end method

.method protected abstract requestOfflineRights(Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/async/Callback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/drm/DrmRequest;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/drm/DrmRequest;",
            "Lcom/google/android/videos/drm/DrmResponse;",
            ">;)V"
        }
    .end annotation
.end method

.method protected abstract requestRights(Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/drm/DrmResponse;Lcom/google/android/videos/async/Callback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/drm/DrmRequest;",
            "Lcom/google/android/videos/drm/DrmResponse;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/drm/DrmRequest;",
            "Lcom/google/android/videos/drm/DrmResponse;",
            ">;)V"
        }
    .end annotation
.end method

.method public setListener(Lcom/google/android/videos/drm/DrmManager$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/videos/drm/DrmManager$Listener;

    .prologue
    .line 157
    iput-object p1, p0, Lcom/google/android/videos/drm/DrmManager;->listener:Lcom/google/android/videos/drm/DrmManager$Listener;

    .line 158
    return-void
.end method

.method protected final stripQueryParameters(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 2
    .param p1, "input"    # Landroid/net/Uri;

    .prologue
    .line 215
    invoke-virtual {p1}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->query(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    .end local p1    # "input":Landroid/net/Uri;
    :cond_0
    return-object p1
.end method
