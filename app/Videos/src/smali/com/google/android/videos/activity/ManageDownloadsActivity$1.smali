.class Lcom/google/android/videos/activity/ManageDownloadsActivity$1;
.super Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;
.source "ManageDownloadsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/activity/ManageDownloadsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/activity/ManageDownloadsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/videos/activity/ManageDownloadsActivity;Landroid/content/Context;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/videos/activity/ManageDownloadsActivity$1;->this$0:Lcom/google/android/videos/activity/ManageDownloadsActivity;

    invoke-direct {p0, p2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected addContentView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 55
    const v1, 0x7f0400b8

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 56
    .local v0, "contentLayout":Landroid/view/View;
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 57
    return-void
.end method

.method protected alwaysUseFloatingBackground()Z
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x1

    return v0
.end method

.method protected getHeaderBottomMargin()I
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/videos/activity/ManageDownloadsActivity$1;->this$0:Lcom/google/android/videos/activity/ManageDownloadsActivity;

    invoke-static {v0}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->getHeaderBottomMargin(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method protected getHeaderHeight()I
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/videos/activity/ManageDownloadsActivity$1;->this$0:Lcom/google/android/videos/activity/ManageDownloadsActivity;

    invoke-static {v0}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->getHeaderHeight(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method protected getTabMode()I
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x2

    return v0
.end method

.method protected getToolbarTitleMode()I
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x1

    return v0
.end method

.method protected hasViewPager()Z
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    return v0
.end method

.method protected useBuiltInActionBar()Z
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x1

    return v0
.end method
