.class Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper$TimeRange;
.super Ljava/lang/Object;
.source "NonAdaptiveQualityDropHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TimeRange"
.end annotation


# instance fields
.field public final lowerBound:J

.field public upperBound:J


# direct methods
.method public constructor <init>(J)V
    .locals 1
    .param p1, "initialTime"    # J

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    iput-wide p1, p0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper$TimeRange;->lowerBound:J

    .line 125
    iput-wide p1, p0, Lcom/google/android/videos/player/NonAdaptiveQualityDropHelper$TimeRange;->upperBound:J

    .line 126
    return-void
.end method
