.class public Lcom/google/android/videos/ui/RatioBar;
.super Landroid/view/View;
.source "RatioBar.java"


# instance fields
.field private barColors:[I

.field private final barPaint:Landroid/graphics/Paint;

.field private final barRect:Landroid/graphics/RectF;

.field private ratios:[F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v5, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 26
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    iput-object v5, p0, Lcom/google/android/videos/ui/RatioBar;->ratios:[F

    .line 23
    iput-object v5, p0, Lcom/google/android/videos/ui/RatioBar;->barColors:[I

    .line 28
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5, v6, v6, v8, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v5, p0, Lcom/google/android/videos/ui/RatioBar;->barRect:Landroid/graphics/RectF;

    .line 29
    new-instance v5, Landroid/graphics/Paint;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v5, p0, Lcom/google/android/videos/ui/RatioBar;->barPaint:Landroid/graphics/Paint;

    .line 30
    iget-object v5, p0, Lcom/google/android/videos/ui/RatioBar;->barPaint:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 32
    sget-object v5, Lcom/google/android/videos/R$styleable;->RatioBar:[I

    invoke-virtual {p1, p2, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 33
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v7, v7}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 34
    .local v3, "colorArrayResource":I
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 36
    if-eqz v3, :cond_1

    .line 37
    invoke-virtual {p0}, Lcom/google/android/videos/ui/RatioBar;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 38
    .local v2, "colorArray":Landroid/content/res/TypedArray;
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->length()I

    move-result v5

    new-array v1, v5, [I

    .line 39
    .local v1, "barColors":[I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->length()I

    move-result v5

    if-ge v4, v5, :cond_0

    .line 40
    invoke-virtual {v2, v4, v7}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v5

    aput v5, v1, v4

    .line 39
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 42
    :cond_0
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 43
    invoke-virtual {p0, v1}, Lcom/google/android/videos/ui/RatioBar;->setBarColors([I)V

    .line 45
    .end local v1    # "barColors":[I
    .end local v2    # "colorArray":Landroid/content/res/TypedArray;
    .end local v4    # "i":I
    :cond_1
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 49
    iget-object v2, p0, Lcom/google/android/videos/ui/RatioBar;->ratios:[F

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/videos/ui/RatioBar;->barColors:[I

    if-nez v2, :cond_1

    .line 64
    :cond_0
    return-void

    .line 52
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/ui/RatioBar;->ratios:[F

    array-length v2, v2

    iget-object v3, p0, Lcom/google/android/videos/ui/RatioBar;->barColors:[I

    array-length v3, v3

    if-ne v2, v3, :cond_2

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 54
    iget-object v2, p0, Lcom/google/android/videos/ui/RatioBar;->barRect:Landroid/graphics/RectF;

    const/4 v3, 0x0

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 55
    iget-object v2, p0, Lcom/google/android/videos/ui/RatioBar;->barRect:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/google/android/videos/ui/RatioBar;->getHeight()I

    move-result v3

    int-to-float v3, v3

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    .line 56
    invoke-virtual {p0}, Lcom/google/android/videos/ui/RatioBar;->getWidth()I

    move-result v2

    int-to-float v1, v2

    .line 58
    .local v1, "width":F
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/videos/ui/RatioBar;->ratios:[F

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 59
    iget-object v2, p0, Lcom/google/android/videos/ui/RatioBar;->barRect:Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/google/android/videos/ui/RatioBar;->barRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 60
    iget-object v2, p0, Lcom/google/android/videos/ui/RatioBar;->barRect:Landroid/graphics/RectF;

    iget v3, v2, Landroid/graphics/RectF;->right:F

    iget-object v4, p0, Lcom/google/android/videos/ui/RatioBar;->ratios:[F

    aget v4, v4, v0

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 61
    iget-object v2, p0, Lcom/google/android/videos/ui/RatioBar;->barPaint:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/google/android/videos/ui/RatioBar;->barColors:[I

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 62
    iget-object v2, p0, Lcom/google/android/videos/ui/RatioBar;->barRect:Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/google/android/videos/ui/RatioBar;->barPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 58
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 52
    .end local v0    # "i":I
    .end local v1    # "width":F
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setBarColors([I)V
    .locals 0
    .param p1, "barColors"    # [I

    .prologue
    .line 79
    iput-object p1, p0, Lcom/google/android/videos/ui/RatioBar;->barColors:[I

    .line 80
    invoke-virtual {p0}, Lcom/google/android/videos/ui/RatioBar;->invalidate()V

    .line 81
    return-void
.end method

.method public varargs setRatios([F)V
    .locals 0
    .param p1, "ratios"    # [F

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/android/videos/ui/RatioBar;->ratios:[F

    .line 72
    invoke-virtual {p0}, Lcom/google/android/videos/ui/RatioBar;->invalidate()V

    .line 73
    return-void
.end method
