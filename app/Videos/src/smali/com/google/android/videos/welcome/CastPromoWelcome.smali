.class public Lcom/google/android/videos/welcome/CastPromoWelcome;
.super Lcom/google/android/videos/welcome/PromoWelcome;
.source "CastPromoWelcome.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/welcome/CastPromoWelcome$1;,
        Lcom/google/android/videos/welcome/CastPromoWelcome$MediaRouteCallback;
    }
.end annotation


# static fields
.field private static final ACTION_RES_IDS:[I


# instance fields
.field private final activity:Landroid/app/Activity;

.field private final configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

.field private detailMessage:Ljava/lang/String;

.field private isEligible:Z

.field private final mediaRouteCallback:Lcom/google/android/videos/welcome/CastPromoWelcome$MediaRouteCallback;

.field private final mediaRouteProvider:Lcom/google/android/videos/remote/MediaRouteProvider;

.field private final mediaRouter:Landroid/support/v7/media/MediaRouter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/videos/welcome/CastPromoWelcome;->ACTION_RES_IDS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f02004b
        0x7f0b00e5
        0x7f020070
        0x7f0b00f9
    .end array-data
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/app/Activity;Landroid/content/SharedPreferences;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/remote/MediaRouteProvider;)V
    .locals 2
    .param p1, "verticalId"    # Ljava/lang/String;
    .param p2, "activity"    # Landroid/app/Activity;
    .param p3, "preferences"    # Landroid/content/SharedPreferences;
    .param p4, "configurationStore"    # Lcom/google/android/videos/store/ConfigurationStore;
    .param p5, "mediaRouteProvider"    # Lcom/google/android/videos/remote/MediaRouteProvider;

    .prologue
    const/4 v1, 0x0

    .line 38
    const-string v0, "cast"

    invoke-direct {p0, p1, v0, p3}, Lcom/google/android/videos/welcome/PromoWelcome;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/SharedPreferences;)V

    .line 39
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/videos/welcome/CastPromoWelcome;->activity:Landroid/app/Activity;

    .line 40
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/ConfigurationStore;

    iput-object v0, p0, Lcom/google/android/videos/welcome/CastPromoWelcome;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    .line 41
    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/remote/MediaRouteProvider;

    iput-object v0, p0, Lcom/google/android/videos/welcome/CastPromoWelcome;->mediaRouteProvider:Lcom/google/android/videos/remote/MediaRouteProvider;

    .line 43
    invoke-interface {p5}, Lcom/google/android/videos/remote/MediaRouteProvider;->getMediaRouter()Landroid/support/v7/media/MediaRouter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/welcome/CastPromoWelcome;->mediaRouter:Landroid/support/v7/media/MediaRouter;

    .line 44
    iget-object v0, p0, Lcom/google/android/videos/welcome/CastPromoWelcome;->mediaRouter:Landroid/support/v7/media/MediaRouter;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lcom/google/android/videos/welcome/CastPromoWelcome;->mediaRouteCallback:Lcom/google/android/videos/welcome/CastPromoWelcome$MediaRouteCallback;

    .line 45
    return-void

    .line 44
    :cond_0
    new-instance v0, Lcom/google/android/videos/welcome/CastPromoWelcome$MediaRouteCallback;

    invoke-direct {v0, p0, v1}, Lcom/google/android/videos/welcome/CastPromoWelcome$MediaRouteCallback;-><init>(Lcom/google/android/videos/welcome/CastPromoWelcome;Lcom/google/android/videos/welcome/CastPromoWelcome$1;)V

    goto :goto_0
.end method

.method private hasCastRoute()Z
    .locals 4

    .prologue
    .line 117
    iget-object v2, p0, Lcom/google/android/videos/welcome/CastPromoWelcome;->mediaRouteProvider:Lcom/google/android/videos/remote/MediaRouteProvider;

    invoke-interface {v2}, Lcom/google/android/videos/remote/MediaRouteProvider;->getMediaRouter()Landroid/support/v7/media/MediaRouter;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v7/media/MediaRouter;->getRoutes()Ljava/util/List;

    move-result-object v1

    .line 118
    .local v1, "routes":Ljava/util/List;, "Ljava/util/List<Landroid/support/v7/media/MediaRouter$RouteInfo;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 119
    iget-object v2, p0, Lcom/google/android/videos/welcome/CastPromoWelcome;->activity:Landroid/app/Activity;

    invoke-static {v2}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getMediaRouteManager()Lcom/google/android/videos/remote/MediaRouteManager;

    move-result-object v3

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v3, v2}, Lcom/google/android/videos/remote/MediaRouteManager;->isCastDevice(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 120
    const/4 v2, 0x1

    .line 123
    :goto_1
    return v2

    .line 118
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 123
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method


# virtual methods
.method public getActionResIds()[I
    .locals 1

    .prologue
    .line 81
    sget-object v0, Lcom/google/android/videos/welcome/CastPromoWelcome;->ACTION_RES_IDS:[I

    return-object v0
.end method

.method public getDefaultBitmapResId()I
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    return v0
.end method

.method public getDetailMessage()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/videos/welcome/CastPromoWelcome;->detailMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getNetworkBitmapUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/videos/welcome/CastPromoWelcome;->activity:Landroid/app/Activity;

    const v1, 0x7f0b00e2

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onAction(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 86
    if-nez p1, :cond_0

    .line 87
    iget-object v0, p0, Lcom/google/android/videos/welcome/CastPromoWelcome;->mediaRouteProvider:Lcom/google/android/videos/remote/MediaRouteProvider;

    invoke-interface {v0}, Lcom/google/android/videos/remote/MediaRouteProvider;->showPicker()V

    .line 91
    :goto_0
    return-void

    .line 89
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/welcome/CastPromoWelcome;->markDismissed()V

    goto :goto_0
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/videos/welcome/CastPromoWelcome;->mediaRouter:Landroid/support/v7/media/MediaRouter;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/google/android/videos/welcome/CastPromoWelcome;->mediaRouter:Landroid/support/v7/media/MediaRouter;

    iget-object v1, p0, Lcom/google/android/videos/welcome/CastPromoWelcome;->mediaRouteProvider:Lcom/google/android/videos/remote/MediaRouteProvider;

    invoke-interface {v1}, Lcom/google/android/videos/remote/MediaRouteProvider;->getSupportedRouteTypes()Landroid/support/v7/media/MediaRouteSelector;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/welcome/CastPromoWelcome;->mediaRouteCallback:Lcom/google/android/videos/welcome/CastPromoWelcome$MediaRouteCallback;

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/media/MediaRouter;->addCallback(Landroid/support/v7/media/MediaRouteSelector;Landroid/support/v7/media/MediaRouter$Callback;)V

    .line 97
    invoke-virtual {p0}, Lcom/google/android/videos/welcome/CastPromoWelcome;->updateEligibility()V

    .line 99
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/videos/welcome/CastPromoWelcome;->mediaRouter:Landroid/support/v7/media/MediaRouter;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/google/android/videos/welcome/CastPromoWelcome;->mediaRouter:Landroid/support/v7/media/MediaRouter;

    iget-object v1, p0, Lcom/google/android/videos/welcome/CastPromoWelcome;->mediaRouteCallback:Lcom/google/android/videos/welcome/CastPromoWelcome$MediaRouteCallback;

    invoke-virtual {v0, v1}, Landroid/support/v7/media/MediaRouter;->removeCallback(Landroid/support/v7/media/MediaRouter$Callback;)V

    .line 106
    :cond_0
    return-void
.end method

.method public preparePromoIfEligible(Ljava/lang/String;ZZ)Z
    .locals 3
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "contentInVertical"    # Z
    .param p3, "downloadedOnly"    # Z

    .prologue
    .line 50
    iget-boolean v1, p0, Lcom/google/android/videos/welcome/CastPromoWelcome;->isEligible:Z

    if-nez v1, :cond_0

    .line 51
    const/4 v1, 0x0

    .line 56
    :goto_0
    return v1

    .line 53
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/welcome/CastPromoWelcome;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    invoke-virtual {v1, p1}, Lcom/google/android/videos/store/ConfigurationStore;->showsVerticalEnabled(Ljava/lang/String;)Z

    move-result v0

    .line 54
    .local v0, "showsLaunched":Z
    iget-object v2, p0, Lcom/google/android/videos/welcome/CastPromoWelcome;->activity:Landroid/app/Activity;

    if-eqz v0, :cond_1

    const v1, 0x7f0b00e4

    :goto_1
    invoke-virtual {v2, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/welcome/CastPromoWelcome;->detailMessage:Ljava/lang/String;

    .line 56
    const/4 v1, 0x1

    goto :goto_0

    .line 54
    :cond_1
    const v1, 0x7f0b00e3

    goto :goto_1
.end method

.method protected updateEligibility()V
    .locals 2

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/google/android/videos/welcome/CastPromoWelcome;->hasCastRoute()Z

    move-result v0

    .line 110
    .local v0, "hasCastRoute":Z
    iget-boolean v1, p0, Lcom/google/android/videos/welcome/CastPromoWelcome;->isEligible:Z

    if-eq v1, v0, :cond_0

    .line 111
    iput-boolean v0, p0, Lcom/google/android/videos/welcome/CastPromoWelcome;->isEligible:Z

    .line 112
    invoke-virtual {p0}, Lcom/google/android/videos/welcome/CastPromoWelcome;->notifyEligibilityChanged()V

    .line 114
    :cond_0
    return-void
.end method
