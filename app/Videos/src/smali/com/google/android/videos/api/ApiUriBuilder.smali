.class public Lcom/google/android/videos/api/ApiUriBuilder;
.super Ljava/lang/Object;
.source "ApiUriBuilder.java"


# instance fields
.field private country:Ljava/lang/String;

.field private flags:I

.field private locale:Ljava/util/Locale;

.field private final uriBuilder:Lcom/google/android/videos/utils/UriBuilder;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "baseUri"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Lcom/google/android/videos/utils/UriBuilder;

    invoke-direct {v0, p1}, Lcom/google/android/videos/utils/UriBuilder;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/videos/api/ApiUriBuilder;->uriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    .line 37
    return-void
.end method

.method public static create(Ljava/lang/String;)Lcom/google/android/videos/api/ApiUriBuilder;
    .locals 1
    .param p0, "uri"    # Ljava/lang/String;

    .prologue
    .line 40
    new-instance v0, Lcom/google/android/videos/api/ApiUriBuilder;

    invoke-direct {v0, p0}, Lcom/google/android/videos/api/ApiUriBuilder;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public addFlags(I)Lcom/google/android/videos/api/ApiUriBuilder;
    .locals 1
    .param p1, "flags"    # I

    .prologue
    .line 64
    iget v0, p0, Lcom/google/android/videos/api/ApiUriBuilder;->flags:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/videos/api/ApiUriBuilder;->flags:I

    .line 65
    return-object p0
.end method

.method public appendEncodedSegment(Ljava/lang/String;)Lcom/google/android/videos/api/ApiUriBuilder;
    .locals 1
    .param p1, "newSegment"    # Ljava/lang/String;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/videos/api/ApiUriBuilder;->uriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/utils/UriBuilder;->addSegmentEncoded(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 45
    return-object p0
.end method

.method public appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/api/ApiUriBuilder;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/videos/api/ApiUriBuilder;->uriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/utils/UriBuilder;->addQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 50
    return-object p0
.end method

.method public build()Landroid/net/Uri;
    .locals 4

    .prologue
    .line 75
    iget-object v1, p0, Lcom/google/android/videos/api/ApiUriBuilder;->country:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 76
    iget-object v1, p0, Lcom/google/android/videos/api/ApiUriBuilder;->uriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    const-string v2, "cr"

    invoke-virtual {v1, v2}, Lcom/google/android/videos/utils/UriBuilder;->deleteQueryParameters(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 81
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/api/ApiUriBuilder;->locale:Ljava/util/Locale;

    if-nez v1, :cond_8

    .line 82
    iget-object v1, p0, Lcom/google/android/videos/api/ApiUriBuilder;->uriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    const-string v2, "lr"

    invoke-virtual {v1, v2}, Lcom/google/android/videos/utils/UriBuilder;->deleteQueryParameters(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 87
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 88
    .local v0, "value":Ljava/lang/StringBuilder;
    iget v1, p0, Lcom/google/android/videos/api/ApiUriBuilder;->flags:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 89
    const/16 v1, 0x6d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 91
    :cond_0
    iget v1, p0, Lcom/google/android/videos/api/ApiUriBuilder;->flags:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 92
    const/16 v1, 0x69

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 94
    :cond_1
    iget v1, p0, Lcom/google/android/videos/api/ApiUriBuilder;->flags:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 95
    const/16 v1, 0x62

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 97
    :cond_2
    iget v1, p0, Lcom/google/android/videos/api/ApiUriBuilder;->flags:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 98
    const/16 v1, 0x65

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 100
    const/16 v1, 0x6f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 102
    :cond_3
    iget v1, p0, Lcom/google/android/videos/api/ApiUriBuilder;->flags:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 103
    const/16 v1, 0x72

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 105
    :cond_4
    iget v1, p0, Lcom/google/android/videos/api/ApiUriBuilder;->flags:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 106
    const/16 v1, 0x63

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 108
    :cond_5
    iget v1, p0, Lcom/google/android/videos/api/ApiUriBuilder;->flags:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_6

    .line 109
    const/16 v1, 0x74

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 111
    :cond_6
    iget-object v1, p0, Lcom/google/android/videos/api/ApiUriBuilder;->uriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    const-string v2, "if"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 113
    iget-object v1, p0, Lcom/google/android/videos/api/ApiUriBuilder;->uriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    invoke-virtual {v1}, Lcom/google/android/videos/utils/UriBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    return-object v1

    .line 78
    .end local v0    # "value":Ljava/lang/StringBuilder;
    :cond_7
    iget-object v1, p0, Lcom/google/android/videos/api/ApiUriBuilder;->uriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    const-string v2, "cr"

    iget-object v3, p0, Lcom/google/android/videos/api/ApiUriBuilder;->country:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    goto/16 :goto_0

    .line 84
    :cond_8
    iget-object v1, p0, Lcom/google/android/videos/api/ApiUriBuilder;->uriBuilder:Lcom/google/android/videos/utils/UriBuilder;

    const-string v2, "lr"

    iget-object v3, p0, Lcom/google/android/videos/api/ApiUriBuilder;->locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    goto/16 :goto_1
.end method

.method public restrictCountry(Ljava/lang/String;)Lcom/google/android/videos/api/ApiUriBuilder;
    .locals 0
    .param p1, "country"    # Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/videos/api/ApiUriBuilder;->country:Ljava/lang/String;

    .line 55
    return-object p0
.end method

.method public restrictLocale(Ljava/util/Locale;)Lcom/google/android/videos/api/ApiUriBuilder;
    .locals 0
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/android/videos/api/ApiUriBuilder;->locale:Ljava/util/Locale;

    .line 60
    return-object p0
.end method
