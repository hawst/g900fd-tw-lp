.class public final Lcom/google/android/videos/drm/DrmAuthenticatingRequester;
.super Lcom/google/android/videos/async/AuthenticatingRequester;
.source "DrmAuthenticatingRequester.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/async/AuthenticatingRequester",
        "<",
        "Lcom/google/android/videos/drm/DrmRequest;",
        "Lcom/google/android/videos/drm/DrmRequest;",
        "Lcom/google/android/videos/drm/DrmResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final targetRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/drm/DrmRequest;",
            "Lcom/google/android/videos/drm/DrmResponse;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/async/Requester;)V
    .locals 0
    .param p1, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/accounts/AccountManagerWrapper;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/drm/DrmRequest;",
            "Lcom/google/android/videos/drm/DrmResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p2, "targetRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/drm/DrmResponse;>;"
    invoke-direct {p0, p1}, Lcom/google/android/videos/async/AuthenticatingRequester;-><init>(Lcom/google/android/videos/accounts/AccountManagerWrapper;)V

    .line 22
    iput-object p2, p0, Lcom/google/android/videos/drm/DrmAuthenticatingRequester;->targetRequester:Lcom/google/android/videos/async/Requester;

    .line 23
    return-void
.end method


# virtual methods
.method public bridge synthetic canRetry(Lcom/google/android/videos/async/Request;Ljava/lang/Exception;)Z
    .locals 1
    .param p1, "x0"    # Lcom/google/android/videos/async/Request;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 13
    check-cast p1, Lcom/google/android/videos/drm/DrmRequest;

    .end local p1    # "x0":Lcom/google/android/videos/async/Request;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/drm/DrmAuthenticatingRequester;->canRetry(Lcom/google/android/videos/drm/DrmRequest;Ljava/lang/Exception;)Z

    move-result v0

    return v0
.end method

.method public canRetry(Lcom/google/android/videos/drm/DrmRequest;Ljava/lang/Exception;)Z
    .locals 3
    .param p1, "request"    # Lcom/google/android/videos/drm/DrmRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 33
    instance-of v1, p2, Lcom/google/android/videos/drm/DrmException;

    if-eqz v1, :cond_0

    move-object v0, p2

    .line 34
    check-cast v0, Lcom/google/android/videos/drm/DrmException;

    .line 35
    .local v0, "drmException":Lcom/google/android/videos/drm/DrmException;
    iget-object v1, v0, Lcom/google/android/videos/drm/DrmException;->drmError:Lcom/google/android/videos/drm/DrmException$DrmError;

    sget-object v2, Lcom/google/android/videos/drm/DrmException$DrmError;->AUTHENTICATION_FAILED:Lcom/google/android/videos/drm/DrmException$DrmError;

    if-ne v1, v2, :cond_0

    .line 36
    const/4 v1, 0x1

    .line 39
    .end local v0    # "drmException":Lcom/google/android/videos/drm/DrmException;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic makeAuthenticatedRequest(Lcom/google/android/videos/async/Request;Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/async/Request;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 13
    check-cast p1, Lcom/google/android/videos/drm/DrmRequest;

    .end local p1    # "x0":Lcom/google/android/videos/async/Request;
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/videos/drm/DrmAuthenticatingRequester;->makeAuthenticatedRequest(Lcom/google/android/videos/drm/DrmRequest;Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V

    return-void
.end method

.method protected makeAuthenticatedRequest(Lcom/google/android/videos/drm/DrmRequest;Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/drm/DrmRequest;
    .param p2, "authToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/drm/DrmRequest;",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/drm/DrmRequest;",
            "Lcom/google/android/videos/drm/DrmResponse;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 28
    .local p3, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/drm/DrmResponse;>;"
    iget-object v0, p0, Lcom/google/android/videos/drm/DrmAuthenticatingRequester;->targetRequester:Lcom/google/android/videos/async/Requester;

    invoke-virtual {p1, p2}, Lcom/google/android/videos/drm/DrmRequest;->copyWithToken(Ljava/lang/String;)Lcom/google/android/videos/drm/DrmRequest;

    move-result-object v1

    invoke-interface {v0, v1, p3}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 29
    return-void
.end method
