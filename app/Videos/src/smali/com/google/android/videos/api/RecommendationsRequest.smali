.class public Lcom/google/android/videos/api/RecommendationsRequest;
.super Lcom/google/android/videos/async/Request;
.source "RecommendationsRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/api/RecommendationsRequest$1;,
        Lcom/google/android/videos/api/RecommendationsRequest$DefaultFactory;,
        Lcom/google/android/videos/api/RecommendationsRequest$Factory;
    }
.end annotation


# instance fields
.field public final country:Ljava/lang/String;

.field public final flags:I

.field public final locale:Ljava/util/Locale;

.field public final max:I

.field public final mccMnc:Ljava/lang/String;

.field public final queryAssetId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

.field public final type:I


# direct methods
.method private constructor <init>(Ljava/lang/String;IILcom/google/wireless/android/video/magma/proto/AssetResourceId;Ljava/lang/String;Ljava/util/Locale;Ljava/lang/String;I)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "max"    # I
    .param p3, "type"    # I
    .param p4, "queryAssetId"    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .param p5, "mccMnc"    # Ljava/lang/String;
    .param p6, "locale"    # Ljava/util/Locale;
    .param p7, "country"    # Ljava/lang/String;
    .param p8, "flags"    # I

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/videos/async/Request;-><init>(Ljava/lang/String;)V

    .line 37
    iput p2, p0, Lcom/google/android/videos/api/RecommendationsRequest;->max:I

    .line 38
    iput p3, p0, Lcom/google/android/videos/api/RecommendationsRequest;->type:I

    .line 39
    iput-object p4, p0, Lcom/google/android/videos/api/RecommendationsRequest;->queryAssetId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 40
    iput-object p5, p0, Lcom/google/android/videos/api/RecommendationsRequest;->mccMnc:Ljava/lang/String;

    .line 41
    iput-object p6, p0, Lcom/google/android/videos/api/RecommendationsRequest;->locale:Ljava/util/Locale;

    .line 42
    iput-object p7, p0, Lcom/google/android/videos/api/RecommendationsRequest;->country:Ljava/lang/String;

    .line 43
    iput p8, p0, Lcom/google/android/videos/api/RecommendationsRequest;->flags:I

    .line 44
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IILcom/google/wireless/android/video/magma/proto/AssetResourceId;Ljava/lang/String;Ljava/util/Locale;Ljava/lang/String;ILcom/google/android/videos/api/RecommendationsRequest$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # I
    .param p3, "x2"    # I
    .param p4, "x3"    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .param p5, "x4"    # Ljava/lang/String;
    .param p6, "x5"    # Ljava/util/Locale;
    .param p7, "x6"    # Ljava/lang/String;
    .param p8, "x7"    # I
    .param p9, "x8"    # Lcom/google/android/videos/api/RecommendationsRequest$1;

    .prologue
    .line 16
    invoke-direct/range {p0 .. p8}, Lcom/google/android/videos/api/RecommendationsRequest;-><init>(Ljava/lang/String;IILcom/google/wireless/android/video/magma/proto/AssetResourceId;Ljava/lang/String;Ljava/util/Locale;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 120
    if-ne p0, p1, :cond_1

    .line 132
    :cond_0
    :goto_0
    return v1

    .line 121
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    goto :goto_0

    .line 122
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 123
    check-cast v0, Lcom/google/android/videos/api/RecommendationsRequest;

    .line 124
    .local v0, "other":Lcom/google/android/videos/api/RecommendationsRequest;
    iget-object v3, p0, Lcom/google/android/videos/api/RecommendationsRequest;->account:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/api/RecommendationsRequest;->account:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    goto :goto_0

    .line 125
    :cond_4
    iget-object v3, p0, Lcom/google/android/videos/api/RecommendationsRequest;->country:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/api/RecommendationsRequest;->country:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    goto :goto_0

    .line 126
    :cond_5
    iget-object v3, p0, Lcom/google/android/videos/api/RecommendationsRequest;->locale:Ljava/util/Locale;

    iget-object v4, v0, Lcom/google/android/videos/api/RecommendationsRequest;->locale:Ljava/util/Locale;

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    goto :goto_0

    .line 127
    :cond_6
    iget v3, p0, Lcom/google/android/videos/api/RecommendationsRequest;->max:I

    iget v4, v0, Lcom/google/android/videos/api/RecommendationsRequest;->max:I

    if-eq v3, v4, :cond_7

    move v1, v2

    goto :goto_0

    .line 128
    :cond_7
    iget-object v3, p0, Lcom/google/android/videos/api/RecommendationsRequest;->mccMnc:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/api/RecommendationsRequest;->mccMnc:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    move v1, v2

    goto :goto_0

    .line 129
    :cond_8
    iget-object v3, p0, Lcom/google/android/videos/api/RecommendationsRequest;->queryAssetId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v4, v0, Lcom/google/android/videos/api/RecommendationsRequest;->queryAssetId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/Util;->areProtosEqual(Lcom/google/protobuf/nano/MessageNano;Lcom/google/protobuf/nano/MessageNano;)Z

    move-result v3

    if-nez v3, :cond_9

    move v1, v2

    goto :goto_0

    .line 130
    :cond_9
    iget v3, p0, Lcom/google/android/videos/api/RecommendationsRequest;->type:I

    iget v4, v0, Lcom/google/android/videos/api/RecommendationsRequest;->type:I

    if-eq v3, v4, :cond_a

    move v1, v2

    goto :goto_0

    .line 131
    :cond_a
    iget v3, p0, Lcom/google/android/videos/api/RecommendationsRequest;->flags:I

    iget v4, v0, Lcom/google/android/videos/api/RecommendationsRequest;->flags:I

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 105
    const/16 v0, 0x1f

    .line 106
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 107
    .local v1, "result":I
    iget-object v2, p0, Lcom/google/android/videos/api/RecommendationsRequest;->account:Ljava/lang/String;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 108
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/api/RecommendationsRequest;->country:Ljava/lang/String;

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    .line 109
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/api/RecommendationsRequest;->locale:Ljava/util/Locale;

    if-nez v2, :cond_2

    move v2, v3

    :goto_2
    add-int v1, v4, v2

    .line 110
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/google/android/videos/api/RecommendationsRequest;->max:I

    add-int v1, v2, v4

    .line 111
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/google/android/videos/api/RecommendationsRequest;->mccMnc:Ljava/lang/String;

    if-nez v4, :cond_3

    :goto_3
    add-int v1, v2, v3

    .line 112
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lcom/google/android/videos/api/RecommendationsRequest;->queryAssetId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-static {v3}, Lcom/google/android/videos/utils/Util;->protoHashCode(Lcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int v1, v2, v3

    .line 113
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/google/android/videos/api/RecommendationsRequest;->type:I

    add-int v1, v2, v3

    .line 114
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/google/android/videos/api/RecommendationsRequest;->flags:I

    add-int v1, v2, v3

    .line 115
    return v1

    .line 107
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/api/RecommendationsRequest;->account:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 108
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/api/RecommendationsRequest;->country:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 109
    :cond_2
    iget-object v2, p0, Lcom/google/android/videos/api/RecommendationsRequest;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->hashCode()I

    move-result v2

    goto :goto_2

    .line 111
    :cond_3
    iget-object v3, p0, Lcom/google/android/videos/api/RecommendationsRequest;->mccMnc:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_3
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RecommendationsRequest [max="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/videos/api/RecommendationsRequest;->max:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/videos/api/RecommendationsRequest;->type:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", queryAssetId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/api/RecommendationsRequest;->queryAssetId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mccMnc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/api/RecommendationsRequest;->mccMnc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", locale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/api/RecommendationsRequest;->locale:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", country="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/api/RecommendationsRequest;->country:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", flags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/videos/api/RecommendationsRequest;->flags:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
