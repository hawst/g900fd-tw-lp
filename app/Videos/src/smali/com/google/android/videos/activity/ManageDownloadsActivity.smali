.class public Lcom/google/android/videos/activity/ManageDownloadsActivity;
.super Landroid/support/v7/app/ActionBarActivity;
.source "ManageDownloadsActivity.java"

# interfaces
.implements Lcom/google/android/videos/ui/SyncHelper$Listener;


# instance fields
.field private headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

.field private helper:Lcom/google/android/videos/ui/ManageDownloadsHelper;

.field private rootUiElementNode:Lcom/google/android/videos/logging/RootUiElementNode;

.field private syncHelper:Lcom/google/android/videos/ui/SyncHelper;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivity;-><init>()V

    return-void
.end method

.method public static createIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/videos/activity/ManageDownloadsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 38
    .local v0, "intent":Landroid/content/Intent;
    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 43
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    const v0, 0x7f040042

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/ManageDownloadsActivity;->setContentView(I)V

    .line 46
    const v0, 0x7f0f011f

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/ManageDownloadsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iput-object v0, p0, Lcom/google/android/videos/activity/ManageDownloadsActivity;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 47
    iget-object v0, p0, Lcom/google/android/videos/activity/ManageDownloadsActivity;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Lcom/google/android/videos/activity/ManageDownloadsActivity$1;

    invoke-direct {v1, p0, p0}, Lcom/google/android/videos/activity/ManageDownloadsActivity$1;-><init>(Lcom/google/android/videos/activity/ManageDownloadsActivity;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->configure(Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;)V

    .line 89
    iget-object v0, p0, Lcom/google/android/videos/activity/ManageDownloadsActivity;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    const v1, 0x7f0a0079

    invoke-static {p0, v1}, Lcom/google/android/videos/ui/DogfoodHelper;->addPawsIfNeeded(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setFloatingControlsBackground(Landroid/graphics/drawable/Drawable;)V

    .line 92
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ManageDownloadsActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 94
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v7

    .line 96
    .local v7, "globals":Lcom/google/android/videos/VideosGlobals;
    new-instance v0, Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v7}, Lcom/google/android/videos/VideosGlobals;->getGcmRegistrationManager()Lcom/google/android/videos/gcm/GcmRegistrationManager;

    move-result-object v2

    invoke-virtual {v7}, Lcom/google/android/videos/VideosGlobals;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v3

    invoke-virtual {v7}, Lcom/google/android/videos/VideosGlobals;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v4

    invoke-virtual {v7}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStoreSync()Lcom/google/android/videos/store/PurchaseStoreSync;

    move-result-object v5

    invoke-virtual {v7}, Lcom/google/android/videos/VideosGlobals;->getWishlistStoreSync()Lcom/google/android/videos/store/WishlistStoreSync;

    move-result-object v6

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/ui/SyncHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/gcm/GcmRegistrationManager;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/PurchaseStoreSync;Lcom/google/android/videos/store/WishlistStoreSync;)V

    iput-object v0, p0, Lcom/google/android/videos/activity/ManageDownloadsActivity;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    .line 103
    iget-object v0, p0, Lcom/google/android/videos/activity/ManageDownloadsActivity;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/SyncHelper;->addListener(Lcom/google/android/videos/ui/SyncHelper$Listener;)V

    .line 104
    iget-object v0, p0, Lcom/google/android/videos/activity/ManageDownloadsActivity;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v7}, Lcom/google/android/videos/VideosGlobals;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/SyncHelper;->init(Ljava/lang/String;)V

    .line 106
    new-instance v0, Lcom/google/android/videos/logging/RootUiElementNodeImpl;

    const/4 v1, 0x4

    invoke-virtual {v7}, Lcom/google/android/videos/VideosGlobals;->getUiEventLoggingHelper()Lcom/google/android/videos/logging/UiEventLoggingHelper;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/videos/logging/RootUiElementNodeImpl;-><init>(ILcom/google/android/videos/logging/UiEventLoggingHelper;)V

    iput-object v0, p0, Lcom/google/android/videos/activity/ManageDownloadsActivity;->rootUiElementNode:Lcom/google/android/videos/logging/RootUiElementNode;

    .line 109
    new-instance v0, Lcom/google/android/videos/ui/ManageDownloadsHelper;

    iget-object v1, p0, Lcom/google/android/videos/activity/ManageDownloadsActivity;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    iget-object v2, p0, Lcom/google/android/videos/activity/ManageDownloadsActivity;->rootUiElementNode:Lcom/google/android/videos/logging/RootUiElementNode;

    invoke-direct {v0, p0, v7, v1, v2}, Lcom/google/android/videos/ui/ManageDownloadsHelper;-><init>(Lcom/google/android/videos/activity/ManageDownloadsActivity;Lcom/google/android/videos/VideosGlobals;Lcom/google/android/videos/ui/SyncHelper;Lcom/google/android/videos/logging/UiElementNode;)V

    iput-object v0, p0, Lcom/google/android/videos/activity/ManageDownloadsActivity;->helper:Lcom/google/android/videos/ui/ManageDownloadsHelper;

    .line 110
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/videos/activity/ManageDownloadsActivity;->helper:Lcom/google/android/videos/ui/ManageDownloadsHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->onDestroy()V

    .line 139
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onDestroy()V

    .line 140
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 126
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onPause()V

    .line 127
    iget-object v0, p0, Lcom/google/android/videos/activity/ManageDownloadsActivity;->rootUiElementNode:Lcom/google/android/videos/logging/RootUiElementNode;

    invoke-interface {v0}, Lcom/google/android/videos/logging/RootUiElementNode;->flushImpression()V

    .line 128
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 120
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onResume()V

    .line 121
    iget-object v0, p0, Lcom/google/android/videos/activity/ManageDownloadsActivity;->rootUiElementNode:Lcom/google/android/videos/logging/RootUiElementNode;

    invoke-interface {v0}, Lcom/google/android/videos/logging/RootUiElementNode;->startNewImpression()V

    .line 122
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 114
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onStart()V

    .line 115
    iget-object v0, p0, Lcom/google/android/videos/activity/ManageDownloadsActivity;->helper:Lcom/google/android/videos/ui/ManageDownloadsHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->onStart()V

    .line 116
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 132
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onStop()V

    .line 133
    iget-object v0, p0, Lcom/google/android/videos/activity/ManageDownloadsActivity;->helper:Lcom/google/android/videos/ui/ManageDownloadsHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->onStop()V

    .line 134
    return-void
.end method

.method public onSyncStateChanged(I)V
    .locals 3
    .param p1, "syncState"    # I

    .prologue
    .line 144
    iget-object v1, p0, Lcom/google/android/videos/activity/ManageDownloadsActivity;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v0

    .line 145
    .local v0, "account":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Sync state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/videos/utils/Hashing;->sha1(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 147
    packed-switch p1, :pswitch_data_0

    .line 156
    :goto_0
    const/4 v1, 0x5

    if-ne p1, v1, :cond_0

    .line 157
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ManageDownloadsActivity;->finish()V

    .line 159
    :cond_0
    return-void

    .line 152
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/videos/activity/ManageDownloadsActivity;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getSwipeRefreshLayout()Landroid/support/v4/widget/SwipeRefreshLayout;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    goto :goto_0

    .line 147
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
