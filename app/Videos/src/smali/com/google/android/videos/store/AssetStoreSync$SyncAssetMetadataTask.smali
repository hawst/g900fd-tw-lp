.class abstract Lcom/google/android/videos/store/AssetStoreSync$SyncAssetMetadataTask;
.super Lcom/google/android/videos/store/SyncTaskManager$SyncTask;
.source "AssetStoreSync.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/AssetStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "SyncAssetMetadataTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/store/SyncTaskManager$SyncTask;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/api/AssetsRequest;",
        "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
        ">;"
    }
.end annotation


# instance fields
.field protected final account:Ljava/lang/String;

.field private hasError:Z

.field protected final playCountry:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/videos/store/AssetStoreSync;


# direct methods
.method protected constructor <init>(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V
    .locals 1
    .param p2, "priority"    # I
    .param p4, "account"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/store/SyncTaskManager$SharedStates",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 688
    .local p3, "states":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<*>;"
    iput-object p1, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncAssetMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    .line 689
    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->syncTaskManager:Lcom/google/android/videos/store/SyncTaskManager;
    invoke-static {p1}, Lcom/google/android/videos/store/AssetStoreSync;->access$200(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/SyncTaskManager;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;-><init>(Lcom/google/android/videos/store/SyncTaskManager;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V

    .line 690
    iput-object p4, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncAssetMetadataTask;->account:Ljava/lang/String;

    .line 691
    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;
    invoke-static {p1}, Lcom/google/android/videos/store/AssetStoreSync;->access$300(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/google/android/videos/store/ConfigurationStore;->getPlayCountry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncAssetMetadataTask;->playCountry:Ljava/lang/String;

    .line 692
    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/api/AssetsRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/api/AssetsRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 745
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncAssetMetadataTask;->hasError:Z

    .line 746
    invoke-virtual {p0, p2}, Lcom/google/android/videos/store/AssetStoreSync$SyncAssetMetadataTask;->maybeReportRequiredDataLevelError(Ljava/lang/Exception;)V

    .line 747
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 680
    check-cast p1, Lcom/google/android/videos/api/AssetsRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/store/AssetStoreSync$SyncAssetMetadataTask;->onError(Lcom/google/android/videos/api/AssetsRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method protected final requestAssets(Ljava/util/List;ZZ)Z
    .locals 8
    .param p2, "includeChildren"    # Z
    .param p3, "reportMalformedId"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;ZZ)Z"
        }
    .end annotation

    .prologue
    .local p1, "assetIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v4, 0x0

    .line 709
    iput-boolean v4, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncAssetMetadataTask;->hasError:Z

    .line 710
    new-instance v3, Lcom/google/android/videos/api/AssetsRequest$Builder;

    invoke-direct {v3}, Lcom/google/android/videos/api/AssetsRequest$Builder;-><init>()V

    iget-object v5, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncAssetMetadataTask;->account:Ljava/lang/String;

    invoke-virtual {v3, v5}, Lcom/google/android/videos/api/AssetsRequest$Builder;->account(Ljava/lang/String;)Lcom/google/android/videos/api/AssetsRequest$Builder;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncAssetMetadataTask;->playCountry:Ljava/lang/String;

    invoke-virtual {v3, v5}, Lcom/google/android/videos/api/AssetsRequest$Builder;->userCountry(Ljava/lang/String;)Lcom/google/android/videos/api/AssetsRequest$Builder;

    move-result-object v3

    const/4 v5, 0x7

    invoke-virtual {v3, v5}, Lcom/google/android/videos/api/AssetsRequest$Builder;->addFlags(I)Lcom/google/android/videos/api/AssetsRequest$Builder;

    move-result-object v0

    .line 715
    .local v0, "assetsRequestBuilder":Lcom/google/android/videos/api/AssetsRequest$Builder;
    if-eqz p2, :cond_0

    .line 716
    const/16 v3, 0x20

    invoke-virtual {v0, v3}, Lcom/google/android/videos/api/AssetsRequest$Builder;->addFlags(I)Lcom/google/android/videos/api/AssetsRequest$Builder;

    .line 718
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    .line 719
    .local v1, "count":I
    const/4 v2, 0x0

    .line 720
    .local v2, "i":I
    :goto_0
    iget-boolean v3, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncAssetMetadataTask;->hasError:Z

    if-nez v3, :cond_4

    if-ge v2, v1, :cond_4

    .line 722
    :goto_1
    if-ge v2, v1, :cond_1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/android/videos/api/AssetsRequest$Builder;->maybeAddId(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 723
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 726
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/videos/api/AssetsRequest$Builder;->getIdCount()I

    move-result v3

    if-eqz v3, :cond_2

    .line 727
    iget-object v3, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncAssetMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->syncAssetsRequester:Lcom/google/android/videos/async/Requester;
    invoke-static {v3}, Lcom/google/android/videos/store/AssetStoreSync;->access$400(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/async/Requester;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/videos/api/AssetsRequest$Builder;->build()Lcom/google/android/videos/api/AssetsRequest;

    move-result-object v5

    invoke-interface {v3, v5, p0}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 728
    invoke-virtual {v0}, Lcom/google/android/videos/api/AssetsRequest$Builder;->clearIds()V

    goto :goto_0

    .line 732
    :cond_2
    if-eqz p3, :cond_3

    .line 733
    const/16 v5, 0x14

    new-instance v6, Lcom/google/android/videos/store/SyncTaskManager$DataException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Malformed id: "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v6, v3}, Lcom/google/android/videos/store/SyncTaskManager$DataException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v5, v6}, Lcom/google/android/videos/store/AssetStoreSync$SyncAssetMetadataTask;->onSyncError(ILjava/lang/Exception;)V

    .line 740
    :goto_2
    return v4

    .line 737
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 740
    :cond_4
    iget-boolean v3, p0, Lcom/google/android/videos/store/AssetStoreSync$SyncAssetMetadataTask;->hasError:Z

    if-nez v3, :cond_5

    const/4 v3, 0x1

    :goto_3
    move v4, v3

    goto :goto_2

    :cond_5
    move v3, v4

    goto :goto_3
.end method
