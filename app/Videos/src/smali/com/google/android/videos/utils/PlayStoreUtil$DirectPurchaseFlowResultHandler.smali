.class public Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler;
.super Ljava/lang/Object;
.source "PlayStoreUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/utils/PlayStoreUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DirectPurchaseFlowResultHandler"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler$Callback;
    }
.end annotation


# instance fields
.field private final purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/store/PurchaseStoreSync;)V
    .locals 1
    .param p1, "purchaseStoreSync"    # Lcom/google/android/videos/store/PurchaseStoreSync;

    .prologue
    .line 432
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 433
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/PurchaseStoreSync;

    iput-object v0, p0, Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler;->purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

    .line 434
    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)Z
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 440
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler;->onActivityResult(IILandroid/content/Intent;Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler$Callback;)Z

    move-result v0

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler$Callback;)Z
    .locals 10
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;
    .param p4, "callback"    # Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler$Callback;

    .prologue
    const/4 v9, 0x0

    const/16 v8, 0x13

    const/4 v5, 0x1

    const/4 v7, -0x1

    .line 448
    const/16 v6, 0x389

    if-eq p1, v6, :cond_1

    .line 449
    const/4 v5, 0x0

    .line 491
    :cond_0
    :goto_0
    return v5

    .line 452
    :cond_1
    if-eq p2, v7, :cond_2

    .line 454
    if-eqz p4, :cond_0

    .line 455
    invoke-interface {p4}, Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler$Callback;->onSyncNotStarted()V

    goto :goto_0

    .line 460
    :cond_2
    const-string v6, "authAccount"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 461
    .local v0, "account":Ljava/lang/String;
    const-string v6, "backend"

    invoke-virtual {p3, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 462
    .local v1, "backend":I
    const-string v6, "backend_docid"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 463
    .local v3, "id":Ljava/lang/String;
    const-string v6, "document_type"

    invoke-virtual {p3, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 464
    .local v2, "documentType":I
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    const/4 v6, 0x4

    if-ne v1, v6, :cond_3

    const/4 v6, 0x6

    if-eq v2, v6, :cond_4

    const/16 v6, 0x14

    if-eq v2, v6, :cond_4

    if-eq v2, v8, :cond_4

    .line 469
    :cond_3
    const-string v6, "Unexpected data"

    invoke-static {v6}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 470
    if-eqz p4, :cond_0

    .line 471
    invoke-interface {p4}, Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler$Callback;->onSyncNotStarted()V

    goto :goto_0

    .line 477
    :cond_4
    if-eqz p4, :cond_5

    .line 478
    invoke-interface {p4}, Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler$Callback;->onSyncStarted()V

    .line 481
    :cond_5
    invoke-static {v0, v3}, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->createForId(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    move-result-object v4

    .line 483
    .local v4, "syncRequest":Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;
    if-ne v2, v8, :cond_6

    .line 484
    iget-object v6, p0, Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler;->purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

    invoke-static {v4, v9}, Lcom/google/android/videos/async/ControllableRequest;->create(Ljava/lang/Object;Lcom/google/android/videos/async/TaskStatus;)Lcom/google/android/videos/async/ControllableRequest;

    move-result-object v7

    new-instance v8, Lcom/google/android/videos/utils/PlayStoreUtil$PurchaseStoreCallback;

    invoke-direct {v8, p4}, Lcom/google/android/videos/utils/PlayStoreUtil$PurchaseStoreCallback;-><init>(Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler$Callback;)V

    invoke-virtual {v6, v7, v8}, Lcom/google/android/videos/store/PurchaseStoreSync;->syncPurchasesForSeason(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V

    goto :goto_0

    .line 487
    :cond_6
    iget-object v6, p0, Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler;->purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

    invoke-static {v4, v9}, Lcom/google/android/videos/async/ControllableRequest;->create(Ljava/lang/Object;Lcom/google/android/videos/async/TaskStatus;)Lcom/google/android/videos/async/ControllableRequest;

    move-result-object v7

    new-instance v8, Lcom/google/android/videos/utils/PlayStoreUtil$PurchaseStoreCallback;

    invoke-direct {v8, p4}, Lcom/google/android/videos/utils/PlayStoreUtil$PurchaseStoreCallback;-><init>(Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler$Callback;)V

    invoke-virtual {v6, v7, v8}, Lcom/google/android/videos/store/PurchaseStoreSync;->syncPurchasesForVideo(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V

    goto :goto_0
.end method
