.class public Lcom/google/android/videos/store/CancellationSignalHolder;
.super Ljava/lang/Object;
.source "CancellationSignalHolder.java"


# instance fields
.field public final cancellationSignal:Landroid/os/CancellationSignal;


# direct methods
.method public constructor <init>(Landroid/os/CancellationSignal;)V
    .locals 1
    .param p1, "cancellationSignal"    # Landroid/os/CancellationSignal;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/CancellationSignal;

    iput-object v0, p0, Lcom/google/android/videos/store/CancellationSignalHolder;->cancellationSignal:Landroid/os/CancellationSignal;

    .line 20
    return-void
.end method
