.class public Lcom/google/android/videos/async/StoreCachingRequester;
.super Ljava/lang/Object;
.source "StoreCachingRequester.java"

# interfaces
.implements Lcom/google/android/videos/async/Requester;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/async/StoreCachingRequester$StoringCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Requester",
        "<",
        "Landroid/util/Pair",
        "<TR;",
        "Ljava/lang/Integer;",
        ">;TE;>;"
    }
.end annotation


# instance fields
.field private final fileStore:Lcom/google/android/videos/store/AbstractFileStore;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/store/AbstractFileStore",
            "<TR;TE;>;"
        }
    .end annotation
.end field

.field private final target:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<TR;TE;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/videos/store/AbstractFileStore;Lcom/google/android/videos/async/Requester;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/store/AbstractFileStore",
            "<TR;TE;>;",
            "Lcom/google/android/videos/async/Requester",
            "<TR;TE;>;)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p0, "this":Lcom/google/android/videos/async/StoreCachingRequester;, "Lcom/google/android/videos/async/StoreCachingRequester<TR;TE;>;"
    .local p1, "fileStore":Lcom/google/android/videos/store/AbstractFileStore;, "Lcom/google/android/videos/store/AbstractFileStore<TR;TE;>;"
    .local p2, "target":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TR;TE;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/AbstractFileStore;

    iput-object v0, p0, Lcom/google/android/videos/async/StoreCachingRequester;->fileStore:Lcom/google/android/videos/store/AbstractFileStore;

    .line 37
    iput-object p2, p0, Lcom/google/android/videos/async/StoreCachingRequester;->target:Lcom/google/android/videos/async/Requester;

    .line 38
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/async/StoreCachingRequester;)Lcom/google/android/videos/store/AbstractFileStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/async/StoreCachingRequester;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/videos/async/StoreCachingRequester;->fileStore:Lcom/google/android/videos/store/AbstractFileStore;

    return-object v0
.end method

.method public static create(Lcom/google/android/videos/store/AbstractFileStore;)Lcom/google/android/videos/async/StoreCachingRequester;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/videos/store/AbstractFileStore",
            "<TR;TE;>;)",
            "Lcom/google/android/videos/async/StoreCachingRequester",
            "<TR;TE;>;"
        }
    .end annotation

    .prologue
    .line 26
    .local p0, "fileStore":Lcom/google/android/videos/store/AbstractFileStore;, "Lcom/google/android/videos/store/AbstractFileStore<TR;TE;>;"
    new-instance v0, Lcom/google/android/videos/async/StoreCachingRequester;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/videos/async/StoreCachingRequester;-><init>(Lcom/google/android/videos/store/AbstractFileStore;Lcom/google/android/videos/async/Requester;)V

    return-object v0
.end method

.method public static create(Lcom/google/android/videos/store/AbstractFileStore;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/StoreCachingRequester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/videos/store/AbstractFileStore",
            "<TR;TE;>;",
            "Lcom/google/android/videos/async/Requester",
            "<TR;TE;>;)",
            "Lcom/google/android/videos/async/StoreCachingRequester",
            "<TR;TE;>;"
        }
    .end annotation

    .prologue
    .line 31
    .local p0, "fileStore":Lcom/google/android/videos/store/AbstractFileStore;, "Lcom/google/android/videos/store/AbstractFileStore<TR;TE;>;"
    .local p1, "target":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TR;TE;>;"
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    new-instance v0, Lcom/google/android/videos/async/StoreCachingRequester;

    invoke-direct {v0, p0, p1}, Lcom/google/android/videos/async/StoreCachingRequester;-><init>(Lcom/google/android/videos/store/AbstractFileStore;Lcom/google/android/videos/async/Requester;)V

    return-object v0
.end method


# virtual methods
.method public request(Landroid/util/Pair;Lcom/google/android/videos/async/Callback;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<TR;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Landroid/util/Pair",
            "<TR;",
            "Ljava/lang/Integer;",
            ">;TE;>;)V"
        }
    .end annotation

    .prologue
    .line 43
    .local p0, "this":Lcom/google/android/videos/async/StoreCachingRequester;, "Lcom/google/android/videos/async/StoreCachingRequester<TR;TE;>;"
    .local p1, "request":Landroid/util/Pair;, "Landroid/util/Pair<TR;Ljava/lang/Integer;>;"
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Landroid/util/Pair<TR;Ljava/lang/Integer;>;TE;>;"
    :try_start_0
    iget-object v3, p0, Lcom/google/android/videos/async/StoreCachingRequester;->fileStore:Lcom/google/android/videos/store/AbstractFileStore;

    iget-object v4, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v2, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v3, v4, v2}, Lcom/google/android/videos/store/AbstractFileStore;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v1

    .line 44
    .local v1, "stored":Ljava/lang/Object;, "TE;"
    if-eqz v1, :cond_0

    .line 45
    invoke-interface {p2, p1, v1}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 54
    .end local v1    # "stored":Ljava/lang/Object;, "TE;"
    :goto_0
    return-void

    .line 46
    .restart local v1    # "stored":Ljava/lang/Object;, "TE;"
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/async/StoreCachingRequester;->target:Lcom/google/android/videos/async/Requester;

    if-eqz v2, :cond_1

    .line 47
    iget-object v2, p0, Lcom/google/android/videos/async/StoreCachingRequester;->target:Lcom/google/android/videos/async/Requester;

    iget-object v3, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    new-instance v4, Lcom/google/android/videos/async/StoreCachingRequester$StoringCallback;

    invoke-direct {v4, p0, p1, p2}, Lcom/google/android/videos/async/StoreCachingRequester$StoringCallback;-><init>(Lcom/google/android/videos/async/StoreCachingRequester;Landroid/util/Pair;Lcom/google/android/videos/async/Callback;)V

    invoke-interface {v2, v3, v4}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    :try_end_0
    .catch Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 51
    .end local v1    # "stored":Ljava/lang/Object;, "TE;"
    :catch_0
    move-exception v0

    .line 52
    .local v0, "e":Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;
    invoke-interface {p2, p1, v0}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0

    .line 49
    .end local v0    # "e":Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;
    .restart local v1    # "stored":Ljava/lang/Object;, "TE;"
    :cond_1
    :try_start_1
    new-instance v2, Lcom/google/android/videos/async/NotFoundException;

    invoke-direct {v2}, Lcom/google/android/videos/async/NotFoundException;-><init>()V

    invoke-interface {p2, p1, v2}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    :try_end_1
    .catch Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public bridge synthetic request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/google/android/videos/async/Callback;

    .prologue
    .line 20
    .local p0, "this":Lcom/google/android/videos/async/StoreCachingRequester;, "Lcom/google/android/videos/async/StoreCachingRequester<TR;TE;>;"
    check-cast p1, Landroid/util/Pair;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/async/StoreCachingRequester;->request(Landroid/util/Pair;Lcom/google/android/videos/async/Callback;)V

    return-void
.end method
